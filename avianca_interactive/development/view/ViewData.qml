import QtQuick 1.1


Item {
    //MainMenu model
    property variant homeModel;
    signal homeModelReady()
    function homeReady (dModel) {
        homeModel=dModel;
        homeModelReady();
        core.debug("ViewData.qml | homeModelReady Called")
    }

    property variant mainMenuModel;
    signal mainMenuModelReady()
    function mainMenuReady (dModel) {
        mainMenuModel=dModel;
        viewHelper.mainMenuIndex=viewHelper.isAviancaBrasilAir?core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id","tv"):core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id","movies")
        if(viewHelper.mainMenuIndex==-1){
            viewHelper.mainMenuIndex=viewHelper.isAviancaBrasilAir?core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id","kids_tv_listing"):core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id","kids_movies_listing")
        }
        if(dModel.count==3){
            viewHelper.mainMenuIndex=1  ;
        }else if(viewHelper.mainMenuIndex==-1){
            viewHelper.mainMenuIndex=viewHelper.isAviancaBrasilAir?2:3;
        }
        mainMenuModelReady();
        core.debug("ViewData.qml |  mainMenuModelReady Called")
    }

    //for CG
    property variant cgModel: viewHelper.blankModel
    signal cgModelReady()
    function setCgData(dModel,status,modelId,errNum){
        core.log("ViewData | setCgData called"+dModel.count);
        if(errNum==0 && dModel.count > 0 && status == true){
            core.log("ViewData | setCgData called"+modelId);
            if(modelId=="cgModel"){
                core.debug("ViewData.qml |  cgModelReady called ")
                cgModel=dModel
                // viewHelper.isFetchDataCalled=true
                cgModelReady()
            }
        }
        else {
            viewHelper.isFetchDataCalled=false
            viewController.updateSystemPopupQ(7,true)
        }
    }
    property variant shoppingModel: viewHelper.blankModel
    signal shoppingModelReady()
    function setShopppingData(dModel){
        core.log("ViewData | shoppingModelReady dataReady called"+dModel.count);
        shoppingModel=dModel
        shoppingModelReady()
    }


    property variant usbSubMenuModel;//SubMenu Model

    signal usbSubMenuModelReady();
    function usbSubMenuReady (dModel) {
        usbSubMenuModel=dModel;
        usbSubMenuModelReady();
    }
}
