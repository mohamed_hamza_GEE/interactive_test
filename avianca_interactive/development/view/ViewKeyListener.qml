import QtQuick 1.1
import Panasonic.Pif 1.0
Item {
    property int fwdKey;
    property int rewKey;
    property int playKey;
    property int stopKey;
    property int homeKey;
    property int backKey;
    property int movieKey;
    property int musicKey;
    property int tvKey;
    property int helpKey;
    property int gamesKey;
    property int androidHomeKey;
    property int androidBackKey;
    property int specialEnterKey;
    property alias blockKeyEvents:blockKeyEvents.enabled;


    function assignKeyMap(){
        if(core.pc){
            fwdKey=41;      // Key F
            rewKey=27;      // Key R;
            playKey=33;     // Key P;
            stopKey=39;     // Key S;
            homeKey=43;     // Key H;
            backKey=56;    //Key B
            androidHomeKey=44;
            androidBackKey = 88;
            movieKey=24  // Key Q
            musicKey=25 // Key W
            tvKey=26 // Key E
            gamesKey=28 // Key T
            helpKey=29  // Key Y
            specialEnterKey=45;

        }else{
            fwdKey=90;
            rewKey=89;
            playKey=85;
            stopKey=86;
            homeKey=(pif.getHandsetType()==pif.cPREMIUM_HANDSET)?3 :171;
            backKey =(pif.getHandsetType()==pif.cPREMIUM_HANDSET)? 102: 161;
            androidHomeKey=3;
            androidBackKey = 88;
            specialEnterKey=100;

            /*
1) Movie- 243
2) TV-240
3) Music - 242
4) Games-241
5) i-152	


            */
            movieKey=243
            musicKey=242
            tvKey=240
            gamesKey=241
            helpKey=152
        }
    }

    function keyOnPressed(event){
        core.debug("ViewKeyListener.qml | event.nativeScanCode "+event.nativeScanCode)
        if(!pif.getOpenFlight() || !pif.getEntOn() || !pif.getBacklightState() || viewController.getActiveSystemPopupID() || viewController.getActiveScreenPopupID() || pif.getExternalAppActive()) return;
        switch(event.nativeScanCode){
        case fwdKey:{
            if(widgetList.isFlightMapOn())return;
            if(pif.lastSelectedVideo.getMediaState()!=pif.lastSelectedVideo.cMEDIA_STOP){
                widgetList.getVideoRef().controlSelect(0)
            }
            else if(pif.lastSelectedAudio.getMediaState()!=pif.lastSelectedAudio.cMEDIA_STOP ){
                customHandsetSignal("fwd")
                if(genreSongIndex<0)
                    pif.lastSelectedAudio.playNext();
            }

            break;
        }
        case rewKey:{
            if(widgetList.isFlightMapOn())return;
            if(pif.lastSelectedVideo.getMediaState()!=pif.lastSelectedVideo.cMEDIA_STOP){
                widgetList.getVideoRef().controlSelect(1)
            }
            else   if(pif.lastSelectedAudio.getMediaState()!=pif.lastSelectedAudio.cMEDIA_STOP){
                customHandsetSignal("rwd")
                if(genreSongIndex<0)
                    pif.lastSelectedAudio.playPrevious();
            }
            break;
        }
        case playKey:{
            if(widgetList.isFlightMapOn())return;
            if(pif.lastSelectedVideo.getMediaState()!=pif.lastSelectedVideo.cMEDIA_STOP){
                widgetList.getVideoRef().controlSelect(2)

            }else if(pif.lastSelectedAudio.getMediaState()!=pif.lastSelectedAudio.cMEDIA_STOP){
                viewHelper.aodHandsetControls=true;
                if(pif.lastSelectedAudio.getMediaState()==pif.lastSelectedAudio.cMEDIA_PLAY)
                    pif.lastSelectedAudio.pause()
                else{
                    pif.lastSelectedAudio.resume()
                }
            }
            break;
        }
        case stopKey:{
            if(widgetList.isFlightMapOn())return;
            if(pif.lastSelectedVideo.getMediaState()!=pif.lastSelectedVideo.cMEDIA_STOP){
                widgetList.getVideoRef().controlSelect(3)

            }
            else if(pif.lastSelectedAudio.getMediaState()!=pif.lastSelectedAudio.cMEDIA_STOP){
                viewHelper.aodHandsetControls=true;
                pif.lastSelectedAudio.stop()
            }
            break;
        }

        case androidHomeKey:{
            if(blockKeyEvents.enabled)return;
            viewHelper.isHomeFromAndroid=true
            homeAction()
            break;
        }

        case androidBackKey:{
            if(blockKeyEvents.enabled)return;
            backaction();
            break
        }

        case homeKey:{
            if(blockKeyEvents.enabled)return;
            viewHelper.isHomeFromAndroid=true
            homeAction()
            break;
        }

        case backKey:{
            if(blockKeyEvents.enabled)return;
            backaction();
            break;
        }
        case movieKey:{
            if(blockKeyEvents.enabled)return;
            if(isKids(current.template_id))shortcutForHotKeys("kids_movies_listing")
            else shortcutForHotKeys("movies")
            break;
        }
        case musicKey:{
            if(blockKeyEvents.enabled)return;
            if(isKids(current.template_id))shortcutForHotKeys("kids_music_listing")
            else shortcutForHotKeys("music")
            break;
        }
        case tvKey:{
            if(blockKeyEvents.enabled)return;
            if(isKids(current.template_id))shortcutForHotKeys("kids_tv_listing")
            else shortcutForHotKeys("tv")
            break;
        }
        case gamesKey:{
            if(blockKeyEvents.enabled)return;
            if(isKids(current.template_id))shortcutForHotKeys("kids_games_listing")
            else   shortcutForHotKeys("games")
            break;
        }

        }
    }
    function keyOnReleased(event){
        if((!pif.getOpenFlight()))return;

        switch(event.nativeScanCode){
        case 96:
        case 97:
        case 99:
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.postKeyReleaseEvent(Qt.Key_Enter,36);
            }
            break;
        case specialEnterKey:{
            pif.postKeyReleaseEvent(Qt.Key_Enter,36);
        }
        break;
        case helpKey:{
            if(blockKeyEvents.enabled)return;
            if(viewHelper.checkBeforeLaunchingShortcuts())return
            widgetList.showHelp()
            break;
        }
        }
    }

    UserEventFilter{
        id: blockKeyEvents;
        enabled: false;
        onKeyPressed: {  core.debug("Sorry key pressed is blocked");event.accepted=true;}
        onKeyReleased: { core.debug("Sorry key released is blocked");event.accepted=true; }
        onMousePressed: {core.debug("Sorry mouse pressed is blocked" );event.accepted=true;}
        onMouseReleased: {core.debug("Sorry mouse released is blocked") ;event.accepted=true;}
    }

    UserEventFilter{
        id: helpScreen;
        enabled: widgetList?((widgetList.getHelpStatus()==1)?true:false):false
        onKeyPressed: {  core.debug("HelperScreen onKeyPressed ");event.accepted=true;}
        onKeyReleased: { core.debug("HelperScreen onKeyReleased ");widgetList.setHelpStatus(false);event.accepted=true;}
        onMousePressed: {core.debug("HelperScreen onMousePressed " );event.accepted=true;}
        onMouseReleased: {core.debug("HelperScreen onMouseReleased") ;widgetList.setHelpStatus(false);event.accepted=true;}
    }
    UserEventFilter{
        id:vkbScreen
        onKeyPressed:{viewHelper.showHighlight=true}
        onMousePressed:{viewHelper.showHighlight=false}
    }

    Component.onCompleted: {
        assignKeyMap();
        viewController.setNativeKeyPressed(keyOnPressed);
        viewController.setNativeKeyReleased(keyOnReleased);
    }
}
