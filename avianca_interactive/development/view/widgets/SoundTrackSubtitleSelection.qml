import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader{
    id:soundTrackScreen
    property bool isVideoScreen:widgetList.isVideoVisible()
    property variant configTagSubtitle: configTool.config
    property variant adsTagSubtitle: configTool.language.movies
    property variant subtitleLVIndex
    property variant soundTrackLVIndex
    property variant aspectLVIndex
    property variant tmplId
    property string sndtemplateId:"movies";
    property variant compSelected:[]
    signal readyToPlayVideo
    Timer{
        id:delay
        interval: 10
        onTriggered: readyToPlayVideo();
    }
    onVisibleChanged: {
        if(visible)init()
        else {
            if(subtitleLoaderobj==undefined)return;
            // soundTrackScreen.soundTrackLVIndex= subtitleLoaderobj.loadSubtitle.item.soundTrackIndex
            // soundTrackScreen.subtitleLVIndex=viewHelper.getSubtitleModel().count>1?subtitleLoaderobj.loadSubtitle.item.subtitleIndex:-1
        }
    }


    ListModel{
        id:aspectModel
        ListElement{label_name:"4:3";aspect_lid:"0"}
        ListElement{label_name:"";aspect_lid:"1"}
    }

    function setSndtemplateId(tid){
        if(tid!='adult'||tid!='kids')
            sndtemplateId='movies';
        else
            sndtemplateId=tid;
    }

    function load(){
        push(overlay,"overlayobj")
        push(subtitleLoader,"subtitleLoaderobj")
    }
    function init(){
        tmplId=widgetList.getCidTidMainMenu()[1]
        core.debug("SoundTrackSubtitleSelection.qml | init ")
        aspectModel.setProperty(1,"label_name",configTool.language.movies.fullscreen)
        subtitleLoaderobj.loadSubtitle.sourceComponent=blankComp
        if(pif.vod.getAspectRatio()=="4x3Adjustable" && viewHelper.getSubtitleModel().count>=2 && viewHelper.getSoundtrackModel().count>=1 && isVideoScreen && !viewHelper.isFromTrailer)
            compSelected=["triple","soundTrack","subtitle","aspect"];
        else if(pif.vod.getAspectRatio()=="4x3Adjustable" && viewHelper.getSubtitleModel().count>=2 && isVideoScreen && !viewHelper.isFromTrailer)
            compSelected=["double","subtitle","aspect"]
        else if(pif.vod.getAspectRatio()=="4x3Adjustable" &&  viewHelper.getSoundtrackModel().count>=1 && isVideoScreen && !viewHelper.isFromTrailer)
            compSelected=["double","soundTrack","aspect"]
        else if(viewHelper.getSubtitleModel().count>=2 &&  viewHelper.getSoundtrackModel().count>=1)
            compSelected=["double","soundTrack","subtitle"]
        else if(viewHelper.getSubtitleModel().count>=2)
            compSelected=["single","subtitle"]
        else if(viewHelper.getSoundtrackModel().count>=1)
            compSelected=["single","soundTrack"]
        else if(pif.vod.getAspectRatio()=="4x3Adjustable" && isVideoScreen && !viewHelper.isFromTrailer)
            compSelected=["single","aspect"]
        else close(true)

        chooseComponent()
    }
    function chooseComponent(){

        switch(compSelected[0]){
        case "single":
            subtitleLoaderobj.loadSubtitle.sourceComponent=singleOption
            if(compSelected[1]=="soundTrack") setSoundTrackModel("One")
            else if(compSelected[1]=="subtitle")setSubTitleModel("One")
            else if(compSelected[1]=="aspect")setAspectModel("One")
            subtitleLoaderobj.loadSubtitle.item.layer2.forceActiveFocus()
            break;
        case "double":
            subtitleLoaderobj.loadSubtitle.sourceComponent=twoOption
            if(compSelected[1]=="soundTrack") setSoundTrackModel("One")
            else if(compSelected[1]=="subtitle")setSubTitleModel("One")

            if(compSelected[2]=="soundTrack")setSoundTrackModel("Two")
            else if(compSelected[2]=="subtitle")setSubTitleModel("Two")
            else if(compSelected[2]=="aspect")setAspectModel("Two")
            subtitleLoaderobj.loadSubtitle.item.layer3.forceActiveFocus()
            break;
        case "triple":
            subtitleLoaderobj.loadSubtitle.sourceComponent=threeOption
            setSoundTrackModel("One")
            setSubTitleModel("Two")
            setAspectModel("Three")
            subtitleLoaderobj.loadSubtitle.item.layer4.forceActiveFocus()
            break;
        }
    }
    function setSoundTrackModel(layerNo){
        subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].setModel(viewHelper.getSoundtrackModel())
        subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].setDelegate(compTracksTitles)
        soundTrackLVIndex=core.coreHelper.searchEntryInModel(viewHelper.getSoundtrackModel(),"soundtrack_lid",viewHelper.getSoundTracklid())
        subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LVIndex"]= soundTrackLVIndex
    }
    function setSubTitleModel(layerNo){
        subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].setModel(viewHelper.getSubtitleModel())
        subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].setDelegate(compSubTitles)
        for(var i=0;i<viewHelper.getSubtitleModel().count;i++){
            if(viewHelper.getSubtitleModel().get(i).subtitle_lid== pif.vod.getSubtitleLid()){
                subtitleLVIndex=i;
                subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LVIndex"]= subtitleLVIndex
                break;
            }
        }
    }
    function getSubTitlesQuery(){
        dataController.getSubtitlesWithLabel([viewHelper.playingVODmid],chooseComponent,configTool.language.movies.none);
    }
    function setAspectModel(layerNo){
         if(pif.vod.getAllMediaPlayInfo().stretch)viewHelper.setAspectlid(1)
        else  viewHelper.setAspectlid(0)
        aspectLVIndex=viewHelper.getAspectlid()
        subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].setModel(aspectModel)
        subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].setDelegate(compAspectTitle)
        subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LVIndex"]= aspectLVIndex
    }
    function reset(){
        subtitleLVIndex=0
        soundTrackLVIndex=0
    }
    function getTags(no,params){
        if(compSelected[no]==params && params=="soundTrack" && compSelected[no]!=undefined)
            return    isVideoScreen  && !viewHelper.isFromTrailer?adsTagSubtitle.vod_change_snd:adsTagSubtitle.soundtrack
        else if(compSelected[no]==params && params=="subtitle" && compSelected[no]!=undefined)
            return        (isVideoScreen && !viewHelper.isFromTrailer)?adsTagSubtitle.vod_change_subcc:adsTagSubtitle.subtitle_cc
        else if(compSelected[no]==params && params=="aspect" && compSelected[no]!=undefined)
            return        adsTagSubtitle.vod_change_screensize
    }
    function showsoundTrackSubtitle(flag){
        if(soundTrackScreen.visible!=flag)
            soundTrackScreen.visible=flag
    }
    function close(param){
        showsoundTrackSubtitle(false)
        if(!param){
            if(viewHelper.isFromTrailer || !isVideoScreen){
                if(viewHelper.isFromTrailer && !core.pc) pif.vod.stop();
                delay.restart()
            }
            if(viewHelper.isFromTrailer)return;
        }
        if(isVideoScreen ){
            if(viewHelper.statusOfSoundtrackPopup!=""){
                if(viewHelper.statusOfSoundtrackPopup=="Forward")pif.vod.forward(true)
                else if(viewHelper.statusOfSoundtrackPopup=="Rewind")pif.vod.rewind(true)
                viewHelper.statusOfSoundtrackPopup=""
                widgetList.focusToVideo("soundtrack")
            }else  pif.vod.resume()
        }
        if(param){
            if(!isVideoScreen) viewController.getActiveScreenRef().forceActiveFocus()
            return;
        }
        if(compSelected.indexOf("soundTrack")!=-1)setSoundTrackLID(compSelected.indexOf("soundTrack"))
        if(compSelected.indexOf("subtitle")!=-1)setSubTitleLID(compSelected.indexOf("subtitle"))
        if(compSelected.indexOf("aspect")!=-1)seAspectLID(compSelected.indexOf("aspect"))
        if(!isVideoScreen) viewController.getActiveScreenRef().forceActiveFocus()

    }
    function setSoundTrackLID(index){
        var layerNo=getLayerNo(index)
        soundTrackLVIndex=  subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].currentIndex
        var soundtrack_lid=viewHelper.getSoundtrackModel().getValue(soundTrackLVIndex,"soundtrack_lid")
        viewHelper.setSoundTracklid(soundtrack_lid)
        if(isVideoScreen)pif.vod.setPlayingSoundtrack(soundtrack_lid)
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedSoundtrackUpdate",{soundtrackIndex:soundTrackLVIndex,subtitleIndex:-1})
    }
    function setSubTitleLID(index){
        var layerNo=getLayerNo(index)
        subtitleLVIndex=subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].currentIndex
        var subtitle_lid=viewHelper.getSubtitleModel().get(subtitleLVIndex).subtitle_lid
        viewHelper.setSubtitlelid(subtitle_lid)
        if(isVideoScreen)pif.vod.setPlayingSubtitle(subtitle_lid)
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedSoundtrackUpdate",{soundtrackIndex:soundTrackLVIndex,subtitleIndex:subtitleLVIndex})
    }
    function seAspectLID(index){
        var layerNo=getLayerNo(index)
        aspectLVIndex=  subtitleLoaderobj.loadSubtitle.item["option"+layerNo+"LV"].currentIndex
        viewHelper.setAspectlid(aspectLVIndex)

        if(pif.vod.getAspectRatio()=="4x3Adjustable"){
            //if stretch
            if(aspectLVIndex==0 && pif.vod.getStretchState()){
                pif.vod.toggleStretchState()
            }else if(aspectLVIndex==1 && !pif.vod.getStretchState()){
                pif.vod.toggleStretchState()
            }
        }

    }
    function getLayerNo(index){
        if(index==0)return "Zero"
        else if(index==1)return "One"
        else if(index==2)return "Two"
        else if(index==3)return "Three"
    }
    function focusTosoundTrackSubtitle(){
        subtitleLoaderobj.loadSubtitle.item.forceActiveFocus()
    }
    property QtObject overlayobj
    Component{
        id:overlay
        Item{
            anchors.fill: parent
            Rectangle{
                anchors.fill: parent
                visible: (!isVideoScreen)
                color:qsTranslate('','common_overlay_color')
                opacity: qsTranslate('','common_overlay_opacity')
                MouseArea{
                    anchors.fill: parent
                    onPressed: {}
                    onReleased: {}
                }
            }
            Image {
                visible: isVideoScreen
                source: viewController.settings.assetImagePath+qsTranslate('','common_overlay_netted_source')
                MouseArea{
                    anchors.fill: parent
                    onPressed: {}
                    onReleased: {}
                }
            }
        }
    }
    property QtObject subtitleLoaderobj
    Component{
        id:subtitleLoader
        Loader{
            id:loadSubtitle
            property alias loadSubtitle: loadSubtitle
            anchors.fill: parent
            sourceComponent: blankComp
            onStatusChanged:{
                if(sourceComponent==blankComp)return
                if (loadSubtitle.status == Loader.Ready){
                    soundTrackLVIndex=core.coreHelper.searchEntryInModel(viewHelper.getSoundtrackModel(),"soundtrack_lid",viewHelper.getSoundTracklid())
                }
            }
        }
    }
    Component{id:blankComp;Item{}}
    Component{
        id:singleOption
        FocusScope{
            anchors.fill: parent
            property alias layer1: layer1
            property alias layer2: layer2
            property alias optionOneLV: optionOneLV
            property int optionOneLVIndex: optionOneLV.currentIndex
            Image{
                id:singleOptionBG
                anchors.left: parent.left
                anchors.leftMargin:isVideoScreen?(viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_trail_bg_lm'):qsTranslate('','soundtrackcc_vod_bg_lm')):qsTranslate('','soundtrackcc_synop_bg_lm')
                anchors.bottom: parent.bottom
                anchors.bottomMargin:isVideoScreen?(viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_trail_bg_bm'):qsTranslate('','soundtrackcc_vod_bg_bm')):qsTranslate('','soundtrackcc_synop_bg_bm')
                source: viewHelper.configToolImagePath+(isVideoScreen && !viewHelper.isFromTrailer?configTagSubtitle[sndtemplateId].sndtrk_cc.vod_popup_bg2:configTagSubtitle[sndtemplateId].sndtrk_cc.synop_popup_bg2)
            }
            SimpleNavButton{
                id:singleOptionBGBTN
                visible: isVideoScreen  && !viewHelper.isFromTrailer?true:false
                isSelected: true
                anchors.bottom: singleOptionBG.bottom
                anchors.bottomMargin:qsTranslate('','soundtrackcc_vod_btns_bm')
                anchors.left: singleOptionBG.left
                anchors.leftMargin:qsTranslate('','soundtrackcc_vod_btns_lm')
                normImg:  viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_n
                highlightimg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_h
                selectedImg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_p
                pressedImg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_p
                onEnteredReleased: close(true)
            }
            ViewText{
                id:playMovieIn
                opacity: isVideoScreen && !viewHelper.isFromTrailer?0:1
                anchors.horizontalCenter: singleOptionBG.horizontalCenter
                width: qsTranslate('','soundtrackcc_headtext_w')
                height: qsTranslate('','soundtrackcc_headtext_h')
                anchors.top:singleOptionBG.top
                anchors.topMargin:qsTranslate('','soundtrackcc_headtext_tm')
                varText: [(widgetList.getCidTidMainMenu()[1]=="movies")?adsTagSubtitle.play_movie_in:configTool.language.tv.play_video_in,configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,qsTranslate('','soundtrackcc_headtext_fontsize')]
            }
            ViewText{
                id:singleOptionTitle
                width: isVideoScreen  && !viewHelper.isFromTrailer ?qsTranslate('','soundtrackcc_vod_title_w'):qsTranslate('','soundtrackcc_title_w')
                height: isVideoScreen  && !viewHelper.isFromTrailer ?qsTranslate('','soundtrackcc_vod_title_h'):qsTranslate('','soundtrackcc_title_h')
                anchors.top:playMovieIn.bottom
                anchors.topMargin: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_title_tm'):qsTranslate('','soundtrackcc_title_tm2')
                anchors.left: singleOptionBG.left
                anchors.leftMargin: isVideoScreen?qsTranslate('','soundtrackcc_vod_title_lm'):qsTranslate('','soundtrackcc_title_lm')
                varText: [getTags(1,compSelected[1]),configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,isVideoScreen?qsTranslate('','soundtrackcc_vod_title_fontsize'):qsTranslate('','soundtrackcc_title_fontsize')]
                maximumLineCount: 2
                wrapMode: Text.Wrap
                lineHeightMode: Text.FixedHeight
                lineHeight:isVideoScreen?qsTranslate('','soundtrackcc_vod_title_lh'):qsTranslate('','soundtrackcc_title_h')
            }
            FocusScope{
                id:layer1
                width:  isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_w'):qsTranslate('','soundtrackcc_langcont_w')
                height:  isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_h'):qsTranslate('','soundtrackcc_langcont_h')
                anchors.verticalCenter: singleOptionTitle.verticalCenter
                anchors.left: singleOptionTitle.right
                anchors.leftMargin: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_lm'):qsTranslate('','soundtrackcc_langcont_lm')
//                ListView{
//                    id:optionOneLV
//                    width:  isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
//                    height:isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
//                    interactive: false
//                    anchors.centerIn:  parent
//                    clip: true
//                    orientation: ListView.Horizontal
//                    preferredHighlightBegin:0;
//                    preferredHighlightEnd:width;
//                    highlightMoveDuration: 10
//                    highlightRangeMode:ListView.StrictlyEnforceRange;
//                    //    model:viewHelper.getSoundtrackModel()
//                    //   delegate: compTracksTitles
//                    currentIndex: optionOneLVIndex
//                    function setModel(dModel){
//                        optionOneLV.model=dModel
//                    }
//                    function setDelegate(dDel){
//                        optionOneLV.delegate=dDel
//                    }
//                }
                Path{
                    id:menupath
                    //first point
                    startX: 0
                    startY: optionOneLV.height/2
//                        PathAttribute{name: 'pathOpacity'; value:1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value:qsTranslate('','settings_lang_area_fontsize')}
                    //second point
                    PathLine{x:optionOneLV.width; y:optionOneLV.height*.5}
                    PathAttribute{name: 'pathOpacity'; value:1 }
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                    //third point
//                        PathLine{x:languageLV.width/2; y:languageLV.height}
//                        PathAttribute{name: 'pathOpacity'; value: 1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                }
                PathView{
                    id:optionOneLV
                    focus: true
                    anchors.centerIn:  parent
                    width:  isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
                    height:isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
                    interactive: false
                    clip: true
                    path:menupath;
                    highlightRangeMode:PathView.StrictlyEnforceRange
                    preferredHighlightBegin:.5
                    preferredHighlightEnd: .5
                    highlightMoveDuration: 600
                    pathItemCount:1//1(model.count<5)?3:5;
//                    model:viewHelper.languageModel
                    currentIndex: optionOneLVIndex
                    onCurrentIndexChanged: {
                        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  on currentIndex = "+currentIndex)
                        optionOneLV.positionViewAtIndex(0,PathView.End )
                    }

                    function setModel(dModel){
                        optionOneLV.model=dModel
                    }
                    function setDelegate(dDel){
                        optionOneLV.delegate=dDel
                    }
                }
                SimpleNavButton{
                    id:right_arrow_optionOne
                    focus: optionOneLV.count>1
                    anchors.left: optionOneLV.right
                    anchors.verticalCenter: optionOneLV.verticalCenter
                    isHighlight: activeFocus
//                    isDisable:optionOneLV.currentIndex==(optionOneLV.count-1)?true:false
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    nav:[!left_arrow_optionOne.isDisable?left_arrow_optionOne:"","","",layer2]
                    onEnteredReleased: optionOneLV.incrementCurrentIndex()
//                    onIsDisableChanged:  if(isDisable)left_arrow_optionOne.forceActiveFocus()
                }
                SimpleNavButton{
                    id:left_arrow_optionOne
                    anchors.right: optionOneLV.left
                    anchors.verticalCenter: optionOneLV.verticalCenter
//                    isDisable: optionOneLV.currentIndex==0?true:false
                    isHighlight: activeFocus
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    nav:["","",!right_arrow_optionOne.isDisable?right_arrow_optionOne:"",layer2]
                    onEnteredReleased: optionOneLV.decrementCurrentIndex()
//                    onIsDisableChanged:  if(isDisable)right_arrow_optionOne.forceActiveFocus()
                }
            }
            FocusScope{
                id:layer2
                anchors.fill: singleOptionBG
                SimpleNavBrdrBtn{
                    id:proceed
                    property string textColor: proceed.activeFocus?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_h:(proceed.isPressed?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_p:configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_n)
                    focus: true
                    width: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_w'):qsTranslate('','soundtrackcc_btn_w')
                    height: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_h'):qsTranslate('','soundtrackcc_btn_h')
                    anchors.right: cancel.left
                    anchors.rightMargin: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_hgap'):qsTranslate('','soundtrackcc_btn_hgap')
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin:   isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_bm'):qsTranslate('','soundtrackcc_btn_bm')
                    isHighlight: activeFocus
                    btnTextWidth:parseInt(isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btntext_w'):qsTranslate('','soundtrackcc_btntext_w'),10);
                    buttonText: [adsTagSubtitle.accept,proceed.textColor,isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btntext_fontsize'):qsTranslate('','soundtrackcc_btntext_fontsize')]
                    normalImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_n
                    highlightImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_h
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_p
                    normalBorder:  [isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    highlightBorder: [isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    nav:["",layer1,cancel]
                    onEnteredReleased:    close()
                }
                SimpleNavBrdrBtn{
                    id:cancel
                    property string textColor: cancel.activeFocus?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_h:(cancel.isPressed?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_p:configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_n)
                    width: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_w'):qsTranslate('','soundtrackcc_btn_w')
                    height: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_h'):qsTranslate('','soundtrackcc_btn_h')
                    anchors.right: parent.right
                    anchors.rightMargin: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_rm'):qsTranslate('','soundtrackcc_btn_rm')
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin:   isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_bm'):qsTranslate('','soundtrackcc_btn_bm')
                    isHighlight: activeFocus
                    btnTextWidth:parseInt(isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btntext_w'):qsTranslate('','soundtrackcc_btntext_w'),10);
                    buttonText: [adsTagSubtitle.cancel,cancel.textColor,isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btntext_fontsize'):qsTranslate('','soundtrackcc_btntext_fontsize')]
                    normalImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_n
                    highlightImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_h
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_p
                    normalBorder:  [isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    highlightBorder: [isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    nav:[proceed,layer1]
                    onEnteredReleased: {
                        close(true)
                    }
                }
            }
        }
    }
    Component{
        id:twoOption
        FocusScope{
            anchors.fill: parent
            property alias layer1: layer1
            property alias layer3: layer3
            property alias optionOneLV: optionOneLV
            property alias optionTwoLV: optionTwoLV
            property int optionOneLVIndex: optionOneLV.currentIndex
            property int optionTwoLVIndex: optionTwoLV.currentIndex
            Image{
                id:twoOptionBG
                anchors.left: parent.left
                anchors.leftMargin:isVideoScreen?(viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_trail_bg_lm'):qsTranslate('','soundtrackcc_vod_bg_lm')):qsTranslate('','soundtrackcc_synop_bg_lm')
                anchors.bottom: parent.bottom
                anchors.bottomMargin:isVideoScreen?(viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_trail_bg_bm'):qsTranslate('','soundtrackcc_vod_bg_bm')):qsTranslate('','soundtrackcc_synop_bg_bm')
                source: viewHelper.configToolImagePath+(isVideoScreen && !viewHelper.isFromTrailer?configTagSubtitle[sndtemplateId].sndtrk_cc.vod_popup_bg1:configTagSubtitle[sndtemplateId].sndtrk_cc.synop_popup_bg1)
            }
            SimpleNavButton{
                id:twoOptionBGBTN
                visible: isVideoScreen  && !viewHelper.isFromTrailer?true:false
                isSelected: true
                anchors.bottom: twoOptionBG.bottom
                anchors.bottomMargin:qsTranslate('','soundtrackcc_vod_btns_bm')
                anchors.left: twoOptionBG.left
                anchors.leftMargin:qsTranslate('','soundtrackcc_vod_btns_lm')
                normImg:  viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_n
                highlightimg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_h
                selectedImg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_p
                pressedImg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_p
                onEnteredReleased: close(true)
            }
            ViewText{
                id:playMovieIn
                opacity: isVideoScreen && !viewHelper.isFromTrailer?0:1
                anchors.horizontalCenter: twoOptionBG.horizontalCenter
                width: qsTranslate('','soundtrackcc_headtext_w')
                height: qsTranslate('','soundtrackcc_headtext_h')
                anchors.top:twoOptionBG.top
                anchors.topMargin:qsTranslate('','soundtrackcc_headtext_tm')
                varText: [(widgetList.getCidTidMainMenu()[1]=="movies")?adsTagSubtitle.play_movie_in:configTool.language.tv.play_video_in,configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,qsTranslate('','soundtrackcc_headtext_fontsize')]
            }
            ViewText{
                id:optionOneTitle
                width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_w'):qsTranslate('','soundtrackcc_title_w')
                height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_h'):qsTranslate('','soundtrackcc_title_h')
                anchors.top:isVideoScreen && !viewHelper.isFromTrailer?twoOptionBG.top:playMovieIn.bottom
                anchors.topMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_tm'):qsTranslate('','soundtrackcc_title_tm1')
                anchors.left: isVideoScreen?twoOptionBG.left:twoOptionBG.left
                anchors.leftMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_lm'):qsTranslate('','soundtrackcc_title_lm')
                varText: [getTags(1,compSelected[1]),configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,isVideoScreen?qsTranslate('','soundtrackcc_vod_title_fontsize'):qsTranslate('','soundtrackcc_title_fontsize')]
                maximumLineCount: 2
                wrapMode: Text.Wrap
                lineHeightMode: Text.FixedHeight
                lineHeight:(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_lh'):qsTranslate('','soundtrackcc_title_h')
            }
            ViewText{
                id:optionTwoTitle
                width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_w'):qsTranslate('','soundtrackcc_title_w')
                height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_h'):qsTranslate('','soundtrackcc_title_h')
                anchors.top:optionOneTitle.bottom
                anchors.topMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_vgap'):qsTranslate('','soundtrackcc_title_vgap')
                anchors.left: twoOptionBG.left
                anchors.leftMargin: isVideoScreen?qsTranslate('','soundtrackcc_vod_title_lm'):qsTranslate('','soundtrackcc_title_lm')
                varText: [getTags(2,compSelected[2]),configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,isVideoScreen?qsTranslate('','soundtrackcc_vod_title_fontsize'):qsTranslate('','soundtrackcc_title_fontsize')]
                maximumLineCount: 2
                wrapMode: Text.Wrap
                lineHeightMode: Text.FixedHeight
                lineHeight:isVideoScreen?qsTranslate('','soundtrackcc_vod_title_lh'):qsTranslate('','soundtrackcc_title_h')
            }
            FocusScope{
                id:layer1
                width:  isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_w'):qsTranslate('','soundtrackcc_langcont_w')
                height:  isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_h'):qsTranslate('','soundtrackcc_langcont_h')
                anchors.verticalCenter: optionOneTitle.verticalCenter
                anchors.left: optionOneTitle.right
                anchors.leftMargin: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_lm'):qsTranslate('','soundtrackcc_langcont_lm')
//                ListView{
//                    id:optionOneLV
//                    width:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
//                    height:(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
//                    interactive: false
//                    anchors.centerIn:  parent
//                    clip: true
//                    orientation: ListView.Horizontal
//                    preferredHighlightBegin:0;
//                    preferredHighlightEnd:width;
//                    highlightMoveDuration: 10
//                    highlightRangeMode:ListView.StrictlyEnforceRange;
//                    model:viewHelper.getSoundtrackModel()
//                    delegate: compTracksTitles
//                    currentIndex: optionOneLVIndex
//                    function setModel(dModel){
//                        optionOneLV.model=dModel
//                    }
//                    function setDelegate(dDel){
//                        optionOneLV.delegate=dDel
//                    }
//                }

                Path{
                    id:menupath
                    //first point
                    startX: 0
                    startY: optionOneLV.height/2
//                        PathAttribute{name: 'pathOpacity'; value:1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value:qsTranslate('','settings_lang_area_fontsize')}
                    //second point
                    PathLine{x:optionOneLV.width; y:optionOneLV.height*.5}
                    PathAttribute{name: 'pathOpacity'; value:1 }
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                    //third point
//                        PathLine{x:languageLV.width/2; y:languageLV.height}
//                        PathAttribute{name: 'pathOpacity'; value: 1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                }
                PathView{
                    id:optionOneLV
                    focus: true
                    anchors.centerIn:  parent
                    width:  isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
                    height:isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
                    interactive: false
                    clip: true
                    path:menupath;
                    highlightRangeMode:PathView.StrictlyEnforceRange
                    preferredHighlightBegin:.5
                    preferredHighlightEnd: .5
                    highlightMoveDuration: 600
                    pathItemCount:1//1(model.count<5)?3:5;
//                    model:viewHelper.languageModel
                    currentIndex: optionOneLVIndex

                    function setModel(dModel){
                        optionOneLV.model=dModel
                    }
                    function setDelegate(dDel){
                        optionOneLV.delegate=dDel
                    }
                }
                SimpleNavButton{
                    id:right_arrow_optionOne
                    focus: optionOneLV.count>1
                    anchors.left: optionOneLV.right
                    anchors.verticalCenter: optionOneLV.verticalCenter
                    isHighlight: activeFocus
                   // isDisable:optionOneLV.currentIndex==(optionOneLV.count-1)?true:false
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    nav:[!left_arrow_optionOne.isDisable?left_arrow_optionOne:"","","",layer2.visible?layer2:layer3]
                    onEnteredReleased: optionOneLV.incrementCurrentIndex()
                   // onIsDisableChanged:  if(isDisable)left_arrow_optionOne.forceActiveFocus()
                }
                SimpleNavButton{
                    id:left_arrow_optionOne
                    anchors.right: optionOneLV.left
                    anchors.verticalCenter: optionOneLV.verticalCenter
                   // isDisable: optionOneLV.currentIndex==0?true:false
                    isHighlight: activeFocus
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    nav:["","",!right_arrow_optionOne.isDisable?right_arrow_optionOne:"",layer2.visible?layer2:layer3]
                    onEnteredReleased: optionOneLV.decrementCurrentIndex()
                 //   onIsDisableChanged:  if(isDisable)right_arrow_optionOne.forceActiveFocus()
                }
            }
            FocusScope{
                id:layer2
                width:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_w'):qsTranslate('','soundtrackcc_langcont_w')
                height:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_h'):qsTranslate('','soundtrackcc_langcont_h')
                anchors.verticalCenter: optionTwoTitle.verticalCenter
                anchors.left: optionTwoTitle.right
                anchors.leftMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_lm'):qsTranslate('','soundtrackcc_langcont_lm')
//                ListView{
//                    id:optionTwoLV
//                    width:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
//                    height:(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
//                    interactive: false
//                    clip: true
//                    anchors.centerIn: parent
//                    preferredHighlightBegin:0;
//                    preferredHighlightEnd:width;
//                    highlightMoveDuration: 10
//                    highlightRangeMode:ListView.StrictlyEnforceRange;
//                    orientation: ListView.Horizontal
//                    //             model:viewHelper.getSubtitleModel()
//                    //          delegate: compSubTitles
//                    currentIndex: optionTwoLVIndex
//                    function setModel(dModel){
//                        optionTwoLV.model=dModel
//                    }
//                    function setDelegate(dDel){
//                        optionTwoLV.delegate=dDel
//                    }
//                }

                Path{
                    id:menupath2
                    //first point
                    startX: 0
                    startY: optionTwoLV.height/2
//                        PathAttribute{name: 'pathOpacity'; value:1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value:qsTranslate('','settings_lang_area_fontsize')}
                    //second point
                    PathLine{x:optionTwoLV.width; y:optionTwoLV.height*.5}
                    PathAttribute{name: 'pathOpacity'; value:1 }
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                    //third point
//                        PathLine{x:languageLV.width/2; y:languageLV.height}
//                        PathAttribute{name: 'pathOpacity'; value: 1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                }
                PathView{
                    id:optionTwoLV
                    focus: true
                    anchors.centerIn:  parent
                    width:  isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
                    height:isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
                    interactive: false
                    clip: true
                    path:menupath2;
                    highlightRangeMode:PathView.StrictlyEnforceRange
                    preferredHighlightBegin:.5
                    preferredHighlightEnd: .5
                    highlightMoveDuration: 600
                    pathItemCount:1//1(model.count<5)?3:5;
//                    model:viewHelper.languageModel
                    currentIndex: optionTwoLVIndex
                    function setModel(dModel){
                        optionTwoLV.model=dModel
                    }
                    function setDelegate(dDel){
                        optionTwoLV.delegate=dDel
                    }
                }





                SimpleNavButton{
                    id:right_arrow_optionTwo
                    focus: true
                    anchors.left: optionTwoLV.right
                    anchors.verticalCenter: optionTwoLV.verticalCenter
                    isHighlight: activeFocus
                   // isDisable:optionTwoLV.currentIndex==(optionTwoLV.count-1)?true:false
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    nav:[!left_arrow_optionTwo.isDisable?left_arrow_optionTwo:"",optionTwoLV.count>0?layer1:"","",layer3]
                    onEnteredReleased: optionTwoLV.incrementCurrentIndex()
                //    onIsDisableChanged: if(isDisable)left_arrow_optionTwo.forceActiveFocus()
                }
                SimpleNavButton{
                    id:left_arrow_optionTwo
                    anchors.right: optionTwoLV.left
                    anchors.verticalCenter: optionTwoLV.verticalCenter
                 //   isDisable: optionTwoLV.currentIndex==0 ?true:false
                    isHighlight: activeFocus
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    nav:["",optionOneLV.count>0?layer1:"",!right_arrow_optionTwo.isDisable?right_arrow_optionTwo:"",layer3]
                    onEnteredReleased: optionTwoLV.decrementCurrentIndex()
                   // onIsDisableChanged: if(isDisable)right_arrow_optionTwo.forceActiveFocus()
                }
            }
            FocusScope{
                id:layer3
                anchors.fill: twoOptionBG
                SimpleNavBrdrBtn{
                    id:proceed
                    property string textColor: proceed.activeFocus?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_h:(proceed.isPressed?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_p:configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_n)
                    focus: true
                    width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_w'):qsTranslate('','soundtrackcc_btn_w')
                    height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_h'):qsTranslate('','soundtrackcc_btn_h')
                    anchors.right: cancel.left
                    anchors.rightMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_hgap'):qsTranslate('','soundtrackcc_btn_hgap')
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin:   (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_bm'):qsTranslate('','soundtrackcc_btn_bm')
                    isHighlight: activeFocus
                    btnTextWidth:parseInt(isVideoScreen?qsTranslate('','soundtrackcc_vod_btntext_w'):qsTranslate('','soundtrackcc_btntext_w'),10);
                    buttonText: [adsTagSubtitle.accept,proceed.textColor,isVideoScreen?qsTranslate('','soundtrackcc_vod_btntext_fontsize'):qsTranslate('','soundtrackcc_btntext_fontsize')]
                    normalImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_n
                    highlightImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_h
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_p
                    normalBorder:  [(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    highlightBorder: [(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    nav:["",layer2.visible?layer2:(soundTrackLV.count>1)?layer1:"",cancel]
                    onEnteredReleased: {
                        close()
                        //                        var soundtrack_lid=soundTrackLV.model.getValue(soundTrackLV.currentIndex,"soundtrack_lid")
                        //                        if(widgetList.isVideoVisible())
                        //                            pif.vod.setPlayingSoundtrack(soundtrack_lid)
                        //                        else  {
                        //                            pif.vod.setPlayingSoundtrack(soundtrack_lid)
                        //                        }
                        //                        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                        //                            pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedSoundtrackUpdate",{soundtrackIndex:soundTrackLV.currentIndex,subtitleIndex:subtitleLoaderobj.loadSubtitle.item.subtitleLV.currentIndex})

                    }
                }
                SimpleNavBrdrBtn{
                    id:cancel
                    property string textColor: cancel.activeFocus?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_h:(cancel.isPressed?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_p:configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_n)
                    width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_w'):qsTranslate('','soundtrackcc_btn_w')
                    height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_h'):qsTranslate('','soundtrackcc_btn_h')
                    anchors.right: parent.right
                    anchors.rightMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_rm'):qsTranslate('','soundtrackcc_btn_rm')
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin:   (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_bm'):qsTranslate('','soundtrackcc_btn_bm')
                    isHighlight: activeFocus
                    btnTextWidth:parseInt((isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btntext_w'):qsTranslate('','soundtrackcc_btntext_w'),10);
                    buttonText: [adsTagSubtitle.cancel,cancel.textColor,isVideoScreen?qsTranslate('','soundtrackcc_vod_btntext_fontsize'):qsTranslate('','soundtrackcc_btntext_fontsize')]
                    normalImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_n
                    highlightImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_h
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_p
                    normalBorder:  [(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    highlightBorder: [(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    nav:[proceed,layer2.visible?layer2:(soundTrackLV.count>1)?layer1:""]
                    onEnteredReleased: {
                        close(true)
                    }
                }
            }
        }
    }
    Component{
        id:threeOption
        FocusScope{
            anchors.fill: parent
            property alias layer1: layer1
            property alias layer4: layer4
            property alias optionOneLV: optionOneLV
            property alias optionTwoLV: optionTwoLV
            property alias optionThreeLV: optionThreeLV
            property int optionOneLVIndex: optionOneLV.currentIndex
            property int optionTwoLVIndex: optionTwoLV.currentIndex
            property int optionThreeLVIndex: optionThreeLV.currentIndex
            Image{
                id:twoOptionBG
                anchors.left: parent.left
                anchors.leftMargin:isVideoScreen?(viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_trail_bg_lm'):qsTranslate('','soundtrackcc_vod_bg_lm')):qsTranslate('','soundtrackcc_synop_bg_lm')
                anchors.bottom: parent.bottom
                anchors.bottomMargin:isVideoScreen?(viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_trail_bg_bm'):qsTranslate('','soundtrackcc_vod_bg_bm')):qsTranslate('','soundtrackcc_synop_bg_bm')
                source: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.vod_popup_bg3
            }
            SimpleNavButton{
                id:twoOptionBGBTN
                visible: isVideoScreen
                isSelected: true
                anchors.bottom: twoOptionBG.bottom
                anchors.bottomMargin:qsTranslate('','soundtrackcc_vod_btns_bm')
                anchors.left: twoOptionBG.left
                anchors.leftMargin:qsTranslate('','soundtrackcc_vod_btns_lm')
                normImg:  viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_n
                highlightimg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_h
                selectedImg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_p
                pressedImg: viewHelper.configToolImagePath+configTool.config.global.video.soundtrackcc_p
                onEnteredReleased: close(true)
            }
            ViewText{
                id:playMovieIn
                opacity: isVideoScreen && !viewHelper.isFromTrailer?0:1
                anchors.horizontalCenter: twoOptionBG.horizontalCenter
                width: qsTranslate('','soundtrackcc_headtext_w')
                height: qsTranslate('','soundtrackcc_headtext_h')
                anchors.top:twoOptionBG.top
                anchors.topMargin:qsTranslate('','soundtrackcc_headtext_tm')
                varText: [(widgetList.getCidTidMainMenu()[1]=="movies")?adsTagSubtitle.play_movie_in:configTool.language.tv.play_video_in,configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,qsTranslate('','soundtrackcc_headtext_fontsize')]
            }
            ViewText{
                id:optionOneTitle
                width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_w'):qsTranslate('','soundtrackcc_title_w')
                height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_h'):qsTranslate('','soundtrackcc_title_h')
                anchors.top:isVideoScreen?twoOptionBG.top:playMovieIn.bottom
                anchors.topMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_tm'):qsTranslate('','soundtrackcc_title_tm1')
                anchors.left: isVideoScreen?twoOptionBG.left:twoOptionBG.left
                anchors.leftMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_lm'):qsTranslate('','soundtrackcc_title_lm')
                varText: [getTags(1,compSelected[1]),configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,isVideoScreen?qsTranslate('','soundtrackcc_vod_title_fontsize'):qsTranslate('','soundtrackcc_title_fontsize')]
                maximumLineCount: 2
                wrapMode: Text.Wrap
                lineHeightMode: Text.FixedHeight
                lineHeight:(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_lh'):qsTranslate('','soundtrackcc_title_h')
            }
            ViewText{
                id:optionTwoTitle
                width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_w'):qsTranslate('','soundtrackcc_title_w')
                height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_h'):qsTranslate('','soundtrackcc_title_h')
                anchors.top:optionOneTitle.bottom
                anchors.topMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_vgap'):qsTranslate('','soundtrackcc_title_vgap')
                anchors.left: twoOptionBG.left
                anchors.leftMargin: isVideoScreen?qsTranslate('','soundtrackcc_vod_title_lm'):qsTranslate('','soundtrackcc_title_lm')
                varText: [getTags(2,compSelected[2]),configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,isVideoScreen?qsTranslate('','soundtrackcc_vod_title_fontsize'):qsTranslate('','soundtrackcc_title_fontsize')]
                maximumLineCount: 2
                wrapMode: Text.Wrap
                lineHeightMode: Text.FixedHeight
                lineHeight:isVideoScreen?qsTranslate('','soundtrackcc_vod_title_lh'):qsTranslate('','soundtrackcc_title_h')
            }
            ViewText{
                id:optionThreeTitle
                width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_w'):qsTranslate('','soundtrackcc_title_w')
                height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_h'):qsTranslate('','soundtrackcc_title_h')
                anchors.top:optionTwoTitle.bottom
                anchors.topMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_title_vgap'):qsTranslate('','soundtrackcc_title_vgap')
                anchors.left: twoOptionBG.left
                anchors.leftMargin: isVideoScreen?qsTranslate('','soundtrackcc_vod_title_lm'):qsTranslate('','soundtrackcc_title_lm')
                varText: [getTags(3,compSelected[3]),configTagSubtitle[sndtemplateId].sndtrk_cc.popup_text_color,isVideoScreen?qsTranslate('','soundtrackcc_vod_title_fontsize'):qsTranslate('','soundtrackcc_title_fontsize')]
                maximumLineCount: 2
                wrapMode: Text.Wrap
                lineHeightMode: Text.FixedHeight
                lineHeight:isVideoScreen?qsTranslate('','soundtrackcc_vod_title_lh'):qsTranslate('','soundtrackcc_title_h')
            }
            FocusScope{
                id:layer1
                width:  isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_w'):qsTranslate('','soundtrackcc_langcont_w')
                height:  isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_h'):qsTranslate('','soundtrackcc_langcont_h')
                anchors.verticalCenter: optionOneTitle.verticalCenter
                anchors.left: optionOneTitle.right
                anchors.leftMargin: isVideoScreen && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langcont_lm'):qsTranslate('','soundtrackcc_langcont_lm')
//                ListView{
//                    id:optionOneLV
//                    width:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
//                    height:(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
//                    interactive: false
//                    anchors.centerIn:  parent
//                    clip: true
//                    orientation: ListView.Horizontal
//                    preferredHighlightBegin:0;
//                    preferredHighlightEnd:width;
//                    highlightMoveDuration: 10
//                    highlightRangeMode:ListView.StrictlyEnforceRange;
//                    model:viewHelper.getSoundtrackModel()
//                    delegate: compTracksTitles
//                    currentIndex: optionOneLVIndex
//                    function setModel(dModel){
//                        optionOneLV.model=dModel
//                    }
//                    function setDelegate(dDel){
//                        optionOneLV.delegate=dDel
//                    }
//                }
                Path{
                    id:menupath
                    //first point
                    startX: 0
                    startY: optionOneLV.height/2
//                        PathAttribute{name: 'pathOpacity'; value:1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value:qsTranslate('','settings_lang_area_fontsize')}
                    //second point
                    PathLine{x:optionOneLV.width; y:optionOneLV.height*.5}
                    PathAttribute{name: 'pathOpacity'; value:1 }
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                    //third point
//                        PathLine{x:languageLV.width/2; y:languageLV.height}
//                        PathAttribute{name: 'pathOpacity'; value: 1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                }
                PathView{
                    id:optionOneLV
                    focus: true
                    anchors.centerIn:  parent
                    width:  isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
                    height:isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
                    interactive: false
                    clip: true
                    path:menupath;
                    highlightRangeMode:PathView.StrictlyEnforceRange
                    preferredHighlightBegin:.5
                    preferredHighlightEnd: .5
                    highlightMoveDuration: 600
                    pathItemCount:1//1(model.count<5)?3:5;
//                    model:viewHelper.languageModel
                    currentIndex: optionOneLVIndex
                    onCurrentIndexChanged: {
                        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  on currentIndex = "+currentIndex)
                        optionOneLV.positionViewAtIndex(0,PathView.End )
                    }

                    function setModel(dModel){
                        optionOneLV.model=dModel
                    }
                    function setDelegate(dDel){
                        optionOneLV.delegate=dDel
                    }
                }
                SimpleNavButton{
                    id:right_arrow_optionOne
                    focus: optionOneLV.count>1
                    anchors.left: optionOneLV.right
                    anchors.verticalCenter: optionOneLV.verticalCenter
                    isHighlight: activeFocus
                   // isDisable:optionOneLV.currentIndex==(optionOneLV.count-1)?true:false
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    nav:[!left_arrow_optionOne.isDisable?left_arrow_optionOne:"","","",layer2.visible?layer2:layer3]
                    onEnteredReleased: optionOneLV.incrementCurrentIndex()
                  //  onIsDisableChanged:  if(isDisable)left_arrow_optionOne.forceActiveFocus()
                }
                SimpleNavButton{
                    id:left_arrow_optionOne
                    anchors.right: optionOneLV.left
                    anchors.verticalCenter: optionOneLV.verticalCenter
                    //isDisable: optionOneLV.currentIndex==0?true:false
                    isHighlight: activeFocus
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    nav:["","",!right_arrow_optionOne.isDisable?right_arrow_optionOne:"",layer2.visible?layer2:layer3]
                    onEnteredReleased: optionOneLV.decrementCurrentIndex()
                   // onIsDisableChanged:  if(isDisable)right_arrow_optionOne.forceActiveFocus()
                }
            }
            FocusScope{
                id:layer2
                width:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_w'):qsTranslate('','soundtrackcc_langcont_w')
                height:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_h'):qsTranslate('','soundtrackcc_langcont_h')
                anchors.verticalCenter: optionTwoTitle.verticalCenter
                anchors.left: optionTwoTitle.right
                anchors.leftMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_lm'):qsTranslate('','soundtrackcc_langcont_lm')
//                ListView{
//                    id:optionTwoLV
//                    width:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
//                    height:(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
//                    interactive: false
//                    clip: true
//                    anchors.centerIn: parent
//                    preferredHighlightBegin:0;
//                    preferredHighlightEnd:width;
//                    highlightMoveDuration: 10
//                    highlightRangeMode:ListView.StrictlyEnforceRange;
//                    orientation: ListView.Horizontal
//                    //  model:viewHelper.getSubtitleModel()
//                    //     delegate: compSubTitles
//                    currentIndex: optionTwoLVIndex
//                    function setModel(dModel){
//                        optionTwoLV.model=dModel
//                    }
//                    function setDelegate(dDel){
//                        optionTwoLV.delegate=dDel
//                    }
//                }

                Path{
                    id:menupath2
                    //first point
                    startX: 0
                    startY: optionTwoLV.height/2
//                        PathAttribute{name: 'pathOpacity'; value:1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value:qsTranslate('','settings_lang_area_fontsize')}
                    //second point
                    PathLine{x:optionTwoLV.width; y:optionTwoLV.height*.5}
                    PathAttribute{name: 'pathOpacity'; value:1 }
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                    //third point
//                        PathLine{x:languageLV.width/2; y:languageLV.height}
//                        PathAttribute{name: 'pathOpacity'; value: 1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                }
                PathView{
                    id:optionTwoLV
                    focus: true
                    anchors.centerIn:  parent
                    width:  isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
                    height:isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
                    interactive: false
                    clip: true
                    path:menupath2;
                    highlightRangeMode:PathView.StrictlyEnforceRange
                    preferredHighlightBegin:.5
                    preferredHighlightEnd: .5
                    highlightMoveDuration: 600
                    pathItemCount:1//1(model.count<5)?3:5;
//                    model:viewHelper.languageModel
                    currentIndex: optionTwoLVIndex
                    function setModel(dModel){
                        optionTwoLV.model=dModel
                    }
                    function setDelegate(dDel){
                        optionTwoLV.delegate=dDel
                    }
                }
                SimpleNavButton{
                    id:right_arrow_optionTwo
                    focus: true
                    anchors.left: optionTwoLV.right
                    anchors.verticalCenter: optionTwoLV.verticalCenter
                    isHighlight: activeFocus
                  //  isDisable:optionTwoLV.currentIndex==(optionTwoLV.count-1)?true:false
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    nav:[!left_arrow_optionTwo.isDisable?left_arrow_optionTwo:"",optionOneLV.count>0?layer1:"","",layer3]
                    onEnteredReleased: optionTwoLV.incrementCurrentIndex()
                  //  onIsDisableChanged: if(isDisable)left_arrow_optionTwo.forceActiveFocus()
                }
                SimpleNavButton{
                    id:left_arrow_optionTwo
                    anchors.right: optionTwoLV.left
                    anchors.verticalCenter: optionTwoLV.verticalCenter
                  //  isDisable: optionTwoLV.currentIndex==0 ?true:false
                    isHighlight: activeFocus
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    nav:["",optionOneLV.count>0?layer1:"",!right_arrow_optionTwo.isDisable?right_arrow_optionTwo:"",layer3]
                    onEnteredReleased: optionTwoLV.decrementCurrentIndex()
                  //  onIsDisableChanged: if(isDisable)right_arrow_optionTwo.forceActiveFocus()
                }
            }
            FocusScope{
                id:layer3
                width:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_w'):qsTranslate('','soundtrackcc_langcont_w')
                height:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_h'):qsTranslate('','soundtrackcc_langcont_h')
                anchors.verticalCenter: optionThreeTitle.verticalCenter
                anchors.left: optionThreeTitle.right
                anchors.leftMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langcont_lm'):qsTranslate('','soundtrackcc_langcont_lm')
//                ListView{
//                    id:optionThreeLV
//                    width:  (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
//                    height:(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
//                    interactive: false
//                    clip: true
//                    anchors.centerIn: parent
//                    preferredHighlightBegin:0;
//                    preferredHighlightEnd:width;
//                    highlightMoveDuration: 10
//                    highlightRangeMode:ListView.StrictlyEnforceRange;
//                    orientation: ListView.Horizontal
//                    //    model:viewHelper.getSubtitleModel()
//                    //   delegate: compSubTitles
//                    currentIndex: optionThreeLVIndex
//                    function setModel(dModel){
//                        optionThreeLV.model=dModel
//                    }
//                    function setDelegate(dDel){
//                        optionThreeLV.delegate=dDel
//                    }
//                }

                Path{
                    id:menupath3
                    //first point
                    startX: 0
                    startY: optionThreeLV.height/2
//                        PathAttribute{name: 'pathOpacity'; value:1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value:qsTranslate('','settings_lang_area_fontsize')}
                    //second point
                    PathLine{x:optionThreeLV.width; y:optionThreeLV.height*.5}
                    PathAttribute{name: 'pathOpacity'; value:1 }
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                    //third point
//                        PathLine{x:languageLV.width/2; y:languageLV.height}
//                        PathAttribute{name: 'pathOpacity'; value: 1/*qsTranslate('','Language_container_opacity')*/}
//                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                }
                PathView{
                    id:optionThreeLV
                    focus: true
                    anchors.centerIn:  parent
                    width:  isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
                    height:isVideoScreen  && !viewHelper.isFromTrailer?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
                    interactive: false
                    clip: true
                    path:menupath3;
                    highlightRangeMode:PathView.StrictlyEnforceRange
                    preferredHighlightBegin:.5
                    preferredHighlightEnd: .5
                    highlightMoveDuration: 600
                    pathItemCount:1//1(model.count<5)?3:5;
//                    model:viewHelper.languageModel
                    currentIndex: optionThreeLVIndex
                    function setModel(dModel){
                        optionTwoLV.model=dModel
                    }
                    function setDelegate(dDel){
                        optionTwoLV.delegate=dDel
                    }
                }



                SimpleNavButton{
                    id:right_arrow_optionThree
                    focus: true
                    anchors.left: optionThreeLV.right
                    anchors.verticalCenter: optionThreeLV.verticalCenter
                    isHighlight: activeFocus
                   // isDisable:optionThreeLV.currentIndex==(optionThreeLV.count-1)?true:false
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.right_arrow_p
                    nav:[!left_arrow_optionThree.isDisable?left_arrow_optionThree:"",optionTwoLV.count>0?layer2:layer1,"",layer4]
                    onEnteredReleased: optionThreeLV.incrementCurrentIndex()
                  //  onIsDisableChanged: if(isDisable)left_arrow_optionThree.forceActiveFocus()
                }
                SimpleNavButton{
                    id:left_arrow_optionThree
                    anchors.right: optionThreeLV.left
                    anchors.verticalCenter: optionThreeLV.verticalCenter
                  //  isDisable: optionThreeLV.currentIndex==0 ?true:false
                    isHighlight: activeFocus
                    normImg:  viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_n
                    highlightimg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_h
                    selectedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.left_arrow_p
                    nav:["",optionTwoLV.count>0?layer2:layer1,!right_arrow_optionThree.isDisable?right_arrow_optionThree:"",layer4]
                    onEnteredReleased: optionThreeLV.decrementCurrentIndex()
                  //  onIsDisableChanged: if(isDisable)right_arrow_optionThree.forceActiveFocus()
                }
            }
            FocusScope{
                id:layer4
                anchors.fill: twoOptionBG
                SimpleNavBrdrBtn{
                    id:proceed
                    property string textColor: proceed.activeFocus?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_h:(proceed.isPressed?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_p:configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_n)
                    focus: true
                    width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_w'):qsTranslate('','soundtrackcc_btn_w')
                    height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_h'):qsTranslate('','soundtrackcc_btn_h')
                    anchors.right: cancel.left
                    anchors.rightMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_hgap'):qsTranslate('','soundtrackcc_btn_hgap')
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin:   (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_bm'):qsTranslate('','soundtrackcc_btn_bm')
                    isHighlight: activeFocus
                    btnTextWidth:parseInt(isVideoScreen?qsTranslate('','soundtrackcc_vod_btntext_w'):qsTranslate('','soundtrackcc_btntext_w'),10);
                    buttonText: [adsTagSubtitle.accept,proceed.textColor,isVideoScreen?qsTranslate('','soundtrackcc_vod_btntext_fontsize'):qsTranslate('','soundtrackcc_btntext_fontsize')]
                    normalImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_n
                    highlightImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_h
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_p
                    normalBorder:  [(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    highlightBorder: [(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    nav:["",optionThreeLV.count>1?layer3:(optionTwoLV.count>1)?layer2:layer1,cancel]
                    onEnteredReleased:close()
                }
                SimpleNavBrdrBtn{
                    id:cancel
                    property string textColor: cancel.activeFocus?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_h:(cancel.isPressed?configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_p:configTagSubtitle[sndtemplateId].sndtrk_cc.button_text_n)
                    width: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_w'):qsTranslate('','soundtrackcc_btn_w')
                    height: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_h'):qsTranslate('','soundtrackcc_btn_h')
                    anchors.right: parent.right
                    anchors.rightMargin: (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_rm'):qsTranslate('','soundtrackcc_btn_rm')
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin:   (isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_bm'):qsTranslate('','soundtrackcc_btn_bm')
                    isHighlight: activeFocus
                    btnTextWidth:parseInt((isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btntext_w'):qsTranslate('','soundtrackcc_btntext_w'),10);
                    buttonText: [adsTagSubtitle.cancel,cancel.textColor,isVideoScreen?qsTranslate('','soundtrackcc_vod_btntext_fontsize'):qsTranslate('','soundtrackcc_btntext_fontsize')]
                    normalImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_n
                    highlightImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_h
                    pressedImg: viewHelper.configToolImagePath+configTagSubtitle[sndtemplateId].sndtrk_cc.button_p
                    normalBorder:  [(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    highlightBorder: [(isVideoScreen && !viewHelper.isFromTrailer)?qsTranslate('','soundtrackcc_vod_btn_margin'):qsTranslate('','soundtrackcc_btn_margin')]
                    nav:[proceed,optionThreeLV.count>1?layer3:(optionTwoLV.count>1)?layer2:layer1]
                    onEnteredReleased: {
                        close(true)
                    }
                }
            }
        }
    }
    Component{
        id:compTracksTitles
        ViewText{
            width:  isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
            height:isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            varText: [label_name,(viewHelper.getSoundTracklid())==soundtrack_lid ?configTagSubtitle[sndtemplateId].sndtrk_cc.language_text_color_s:configTagSubtitle[sndtemplateId].sndtrk_cc.language_text_color_n,isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_fontsize'):qsTranslate('','soundtrackcc_langtext_fontsize')]
        }
    }
    Component{
        id:compSubTitles
        ViewText{
            width:  isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
            height:isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            varText: [label_name,(viewHelper.getSubtitlelid())==subtitle_lid ?configTagSubtitle[sndtemplateId].sndtrk_cc.language_text_color_s:configTagSubtitle[sndtemplateId].sndtrk_cc.language_text_color_n,isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_fontsize'):qsTranslate('','soundtrackcc_langtext_fontsize')]
        }
    }
    Component{
        id:compAspectTitle
        ViewText{
            width:  isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_w'):qsTranslate('','soundtrackcc_langtext_w')
            height:isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_h'):qsTranslate('','soundtrackcc_langtext_h')
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            varText: [label_name,(viewHelper.getAspectlid())==aspect_lid ?configTagSubtitle[sndtemplateId].sndtrk_cc.language_text_color_s:configTagSubtitle[sndtemplateId].sndtrk_cc.language_text_color_n,isVideoScreen?qsTranslate('','soundtrackcc_vod_langtext_fontsize'):qsTranslate('','soundtrackcc_langtext_fontsize')]
        }
    }

}
