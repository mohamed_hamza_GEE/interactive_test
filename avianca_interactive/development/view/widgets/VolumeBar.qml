import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
SmartLoader {
    id:volumeBarScreen
    property bool isVolumeBar: true
    property variant configTagHandsetBar: configTool.config.global.volume
    property string tmpId:current.template_id
    signal navigation(string dir)
    /*****************************************************************************************************/
    Timer{
        id:closeTimer
        interval: 3000
        onTriggered: showVolumeBar(false)
    }
    Connections{
        target: visible?pif:null;
        onVolumeChanged: changeLeverBar(pif.getVolumeLevel())
    }
    /*****************************************************************************************************/
    function reset(){isVolumeBar=false}
    function load(){
        push(bgColor,"bgColorObj")
        push(volumeBar,"volumeObj")
    }
    function init(){
        core.debug('VolumeBar.qml | init')
        if(current.template_id=="usb_music") tmpId="common_volume_bar_3"
        else  if(viewController.getActiveScreenRef().getCidTidForSubCat()[1]=="music_listing" || viewHelper.isKids(current.template_id)) tmpId="common_volume_bar_1"
        else  if(viewController.getActiveScreenRef().getCidTidForSubCat()[1]=="media_listing") tmpId="common_volume_bar_4"
        else  tmpId="common_volume_bar_2"
        changeLeverBar(pif.getVolumeLevel())
        volumeObj.forceActiveFocus()
    }
    function clearonExit(){
        closeTimer.stop()
        viewController.getActiveScreenRef().forceActiveFocus()
        core.debug('VolumeBar.qml | clearonExit')
    }
    function changeLeverBar(value){
        closeTimer.restart()
        if(viewController.getActiveScreenPopupID()!=0 || viewController.getActiveSystemPopupID()!=0)return
        if(widgetList.isVideoVisible() || (widgetList.getisFlightInfoOpen()))return;
        volumeObj.volumeSliderComp.setInitailValue=value
    }

    function isVisble(){return volumeBarScreen.visible;}
    function showVolumeBar(flag){
        if(viewController.getActiveScreenPopupID()!=0 || viewController.getActiveSystemPopupID()!=0)return
        if(widgetList.isVideoVisible() || (widgetList.getisFlightInfoOpen()))return;
        volumeBarScreen.visible=flag
        if(flag) init()
        else clearonExit()

    }
    /*****************************************************************************************************/
    property QtObject bgColorObj
    Component{
        id:bgColor
        Rectangle{
            anchors.fill: parent
            color:qsTranslate('','common_overlay_color')
            opacity: qsTranslate('','common_overlay_opacity')
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    showVolumeBar(false)
                }
            }
        }
    }
    property QtObject volumeObj
    Component{
        id:volumeBar
        VolumeSlider{
            id:volSlider;
            anchors.bottom: parent.bottom;
            anchors.bottomMargin:qsTranslate('',tmpId+'_bm')
            anchors.left: parent.left;
            anchors.leftMargin: qsTranslate('',tmpId+'_lm')
            source: viewHelper.configToolImagePath+configTool.config.global.volume.bg;
            volConfigTag:configTool.config.global.volume;
            volSliderComp:[qsTranslate('','common_volume_bar_h'),qsTranslate('','common_volume_bar_w')]//(tmpId=="usb_music")?[qsTranslate('','usb_volume_bar_h'),qsTranslate('','usb_volume_bar_w')]:[qsTranslate('','common_volume_bar_h'),qsTranslate('','common_volume_bar_w')]
            onPadReleased: closeTimer.restart()
            onSliderEnds: navigation(dir)
            onDragActive: closeTimer.restart()
        }
    }
}
