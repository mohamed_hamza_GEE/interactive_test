import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
SmartLoader {
    id:handsetBarScreen
    property bool isVolumeBar: false
    property variant configTagHandsetBar: isVolumeBar?configTool.config.global.volume:configTool.config.global.brightness
    Timer{
        id:closeTimer
        interval: 3000
        onTriggered: isVisble(false)
    }
    Connections{
        target: isVolumeVisible() || viewHelper.isFirstTimeBrightness?null:pif;
        onVolumeChanged: changeLeverBar("vol",pif.getVolumeLevel())
        onBrightnessChanged: changeLeverBar("bri",pif.getBrightnessLevel())
    }
    function reset(){isVolumeBar=false}
    function load(){
        push(volumeBar,"volumeObj")
    }
    function init(){}
    function changeLeverBar(type,value){
        if(viewHelper.isVolumeFirsttime ||  viewHelper.isMusicControlVolumeOpen){
            viewHelper.isVolumeFirsttime=false
            viewHelper.isMusicControlVolumeOpen=false
            return
        }
        closeTimer.restart()
        if(viewController.getActiveScreenPopupID()!=0 || viewController.getActiveSystemPopupID()!=0)return
        if(widgetList.isVideoVisible() || (widgetList.getisFlightInfoOpen()))return;
        else isVisble(true)
        if(type=="vol")isVolumeBar=true
        else isVolumeBar=false
        volumeObj.volumeSliderComp.setInitailValue=value
    }
    function isVisble(flag){
        if(handsetBarScreen.visible!=flag)
            handsetBarScreen.visible=flag;
    }
    property QtObject volumeObj
    Component{
        id:volumeBar
        Image{
            id:volumeSlider
            property alias volumeSliderComp: volumeSliderComp
            anchors.left: parent.left
            anchors.leftMargin:  qsTranslate('','popups_volume_bar_lm')
            anchors.bottom: parent.bottom
            anchors.bottomMargin: qsTranslate('','popups_volume_bar_bm')
            source:  viewHelper.configToolImagePath+configTagHandsetBar.bg
            SimpleButton{
                id:volumeMinus
                anchors.top: volumeSliderComp.bottom
                isFocusRequired:false
                normImg: (isVolumeBar)?viewHelper.configToolImagePath+configTagHandsetBar.vol_dcr_n:viewHelper.configToolImagePath+configTagHandsetBar.bri_dcr_n
            }
            Slider{
                id:volumeSliderComp
                anchors.centerIn: parent
                height: qsTranslate('','popups_volume_bar_h')
                width:  qsTranslate('','popups_volume_bar_w')
                orientation: Qt.Vertical
                isReverseSliding:true
                sliderbg:configTagHandsetBar.base
                sliderFillingtItems: [configTagHandsetBar.fill_color,configTagHandsetBar.indicator]
                stepsValue: isVolumeBar?pif.getVolumeStep():pif.getBrightnessStep()
                isFill: true
                maxLevel:100
            }
            SimpleButton{
                id:volumePlus
                anchors.bottom: volumeSliderComp.top
                isFocusRequired:false
                normImg: (isVolumeBar)?viewHelper.configToolImagePath+configTagHandsetBar.vol_incr_n:viewHelper.configToolImagePath+configTagHandsetBar.bri_incr_n
            }
        }
    }
}
