import QtQuick 1.1

import "../../framework/viewcontroller"
import "../components"
SmartLoader {
    id:mainMenu
    property alias mainmenuPathView: mainmenuPathView
    property alias curtainsTypeAnim: curtainsTypeAnim
    property alias revwelcomeAnim: revwelcomeAnim
    property int topMargin: 0
    property int lastTopMargin: 0
    property int leftMargin: 0
    property int mainAnimIndex: -1
    property int indexPos: 0
    property bool animReq:false
    property bool isCurtainUp:false
    property int counter: 1
    property string headTitlePaxus;
    property string configMenu:(viewHelper.isKids(current.template_id))?"kids_menu":"menu";

    Connections{
        target:viewController
        onScreenPopupClosed:{
            if(id==7){
                mainmenuPathView.forceActiveFocus()
            }
        }
    }

    Connections{
        target:viewData
        onShoppingModelReady:{
            if(viewData.shoppingModel.count<=0 || viewData.shoppingModel.count==undefined){
                console.log("widgets/Mainmenu.qml | onShoppingModelReady | viewData.shoppingModel.count "+viewData.shoppingModel.count)
                viewController.showScreenPopup(7)

            }else{
                console.log("widgets/Mainmenu.qml | onShoppingModelReady | viewData.shoppingModel.count = "+viewData.shoppingModel.count)
                viewController.loadScreen("shopping");
            }
        }
    }
    Timer{id:paxusTimer; onTriggered:{}/*pif.paxus.interactiveScreenLog(headTitlePaxus);*/}
    Timer{
        id:animTimer
        interval:150
        repeat: true
        onTriggered:{
            if(indexPos==0)
                mainAnimIndex=viewHelper.mainMenuIndex
            else if(indexPos==1){
                mainAnimIndex=viewHelper.mainMenuIndex==(mainmenuPathView.count-1)?0:viewHelper.mainMenuIndex+1
                mainAnimIndex=viewHelper.mainMenuIndex==0?mainmenuPathView.count-1:viewHelper.mainMenuIndex-1
            }
            else if(indexPos==2){
                mainAnimIndex=viewHelper.mainMenuIndex==(mainmenuPathView.count-1)?0:(viewHelper.mainMenuIndex+2>(mainmenuPathView.count-1))?0:viewHelper.mainMenuIndex+2
                mainAnimIndex=viewHelper.mainMenuIndex==0?mainmenuPathView.count-2:(viewHelper.mainMenuIndex-2)==-1?mainmenuPathView.count-1:viewHelper.mainMenuIndex-2
            }
            else if(indexPos==3){
                mainAnimIndex=viewHelper.mainMenuIndex==(mainmenuPathView.count-1)?1:(viewHelper.mainMenuIndex+3>(mainmenuPathView.count-1))?0:viewHelper.mainMenuIndex+3
                mainAnimIndex=viewHelper.mainMenuIndex==0?mainmenuPathView.count-3:(viewHelper.mainMenuIndex-3)==-2?mainmenuPathView.count-2:viewHelper.mainMenuIndex-3
            }
            if(indexPos>=Math.floor(mainmenuPathView.count/2) || indexPos==3){
                animTimer.stop()
                indexPos=0
                mainAnimIndex=-1
                setMainMenuCurrentIndex(viewHelper.mainMenuIndex)
                return;
            }
            indexPos++
        }
    }
    function reset(){
        lastTopMargin=qsTranslate('','mainmenu_menu_tm')
        topMargin=0
        revcurtainsTypeAnim.restart()
        mainAnimIndex=-1
        mainmenuPathView.model=viewHelper.blankModel;
        isCurtainUp=false
        animReq=false
        welcomeTxt.opacity=0
        welcomeDesc.opacity=0
    }
    function showMainMenu(flag){
        if(mainMenu.visible!=flag)
            mainMenu.visible=flag
    }
    function mainMenuPosition(){
        if(topMargin==parseInt(qsTranslate('','mainmenu_menu_tm1'),10)){return true}
        else return false
    }
    function setCurrentIndexForUsb(){
        var index = core.coreHelper.searchEntryInModel(viewData.mainMenuModel,"category_attr_template_id","usb")
        setMainMenuCurrentIndex(index);viewHelper.mainMenuIndex=index
    }
    function setCurrentIndexForDiscover(){
        var index = core.coreHelper.searchEntryInModel(viewData.mainMenuModel,"category_attr_template_id","discover")
        setMainMenuCurrentIndex(index);viewHelper.mainMenuIndex=index
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>  setCurrentIndexForDiscover index "+index)
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>  setCurrentIndexForDiscover viewHelper.mainMenuIndex "+viewHelper.mainMenuIndex)
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>  setCurrentIndexForDiscover mainmenuPathView.currentIndex "+mainmenuPathView.currentIndex)
    }

    function setBlankModel(){
        mainmenuPathView.model=viewHelper.blankModel;
    }

    function setMainMenuParameters(newtopmargin,newleftmargin,animreq,cIndex,isModelUpdated){
        console.log(" Mainmenu.qml | Widget | newtopmargin "+newtopmargin+" newleftmargin "+newleftmargin+" animreq "+animreq+" cIndex  "+cIndex+" isModelUpdated "+isModelUpdated)
        mainmenuPathView.model=viewData.mainMenuModel;
        lastTopMargin=topMargin
        topMargin=newtopmargin
        leftMargin=newleftmargin
        animReq=animreq
        viewHelper.blockKeysForSeconds(1.5)
        setMainMenuCurrentIndex(cIndex)
        if(animReq){
            if(isModelUpdated==undefined || !isModelUpdated || isModelUpdated=="")animTimer.restart()
            welcomeTxt.opacity=0
            welcomeDesc.opacity=0
            welcomeAnim.restart()
        }
        else  if(!animReq){
            if(viewController.getActiveScreen()=="MainMenu" ){
                welcomeTxt.opacity=0
                welcomeDesc.opacity=0
                welcomeAnim.restart()
                revcurtainsTypeAnim.restart()
            }else if(!isCurtainUp){
                welcomeTxt.opacity=0;
                welcomeDesc.opacity=0
                curtainsTypeAnim.restart()
            }
        }
    }
    function setMainMenuCurrentIndex (index){ mainmenuPathView.currentIndex=index;}
    function getMainMenuCurrentIndex(){return mainmenuPathView.currentIndex}
    function enterReleased(index){
        if(mainmenuPathView.model.getValue(index,"category_attr_template_id")==current.template_id)return;
        if(dataController.mediaServices.getCidBlockStatus(mainmenuPathView.model.getValue(index,"cid"))){
            viewController.updateSystemPopupQ(7,true)
            return;
        }
        if(curtainsTypeAnim.running)return;

        var tempId = mainmenuPathView.model.getValue(index,"category_attr_template_id");
        if(tempId=="usb" && pif.usb.getUsbConnected()==false){
            viewController.updateSystemPopupQ(6,true);
            return;
        }
        else if(tempId=="usb" && pif.usb.getUsbConnected()==true){
            if(viewController.getActiveScreen()=="USB" || viewController.getActiveScreen()=="UsbListing"  || viewController.getActiveScreen()=="UsbThumb" )return;
            var temp=core.dataController.mediaServices.getCategoryDataByTagName("usb");
            var cid=temp.cid
            var tid=temp.tid
            viewController.jumpTo(cid,tid)
            if(!widgetList.isCurtainUp()){
                widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex,false)
                viewHelper.isCurtainFirstTime=false
            }
        }
        if(tempId=="shopping"){
            if(!core.dataController.shopping){viewController.updateSystemPopupQ(7,true);return;}
            var curLangISO=core.settings.languageISO.toUpperCase()
            var langIndex =core.coreHelper.searchEntryInModel(core.dataController.shopping.getLanguageModel(),"language_code",curLangISO)
            core.debug("Shopping.qml | languageUpdate |  viewHelper.defaultLang = "+viewHelper.defaultLang+" langIndex = "+langIndex+ " curLangISO = "+curLangISO)
            if(langIndex!=-1){
                core.debug("Mainmenu.qml submenu curLangISO |%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+curLangISO)
                core.dataController.shopping.fetchData(curLangISO,viewData.setShoppingModelReady)
            }else{
                core.debug("Mainmenu.qml submenu curLangISO | eng >>>>>>>>>>>>"+curLangISO+ "viewHelper.defaultLang = "+viewHelper.defaultLang )
                core.dataController.shopping.fetchData(viewHelper.defaultLang,viewData.setShoppingModelReady)
            }

            var cid1=mainmenuPathView.model.getValue(index,"cid")
            var tid1=mainmenuPathView.model.getValue(index,"category_attr_template_id")
            viewController.jumpTo(cid1,tid1)
            viewHelper.mainMenuIndex=index

        }else{
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":""/*,"showContOnKarma":viewHelper.showContOnKarma*/});
            }
            viewHelper.mainMenuIndex=index
            viewController.loadNext(mainmenuPathView.model,index);
        }
        var headTitle1=mainmenuPathView.model.getValue(index,"title");
        headTitlePaxus=headTitle1;
        paxusTimer.restart();


    }
    function getCidTid(){
        var param=[]
        if(mainmenuPathView.count<=0)return;
        param[0]=mainmenuPathView.model.getValue(viewHelper.mainMenuIndex,"cid")
        param[1]=mainmenuPathView.model.getValue(viewHelper.mainMenuIndex,"category_attr_template_id")
        return param;
    }
    function focustTomainMenu(){if(mainmenuPathView.model.count>0)mainmenuPathView.forceActiveFocus();}
    Image{
        id:mainmenuScreenBG
        source:  viewHelper.configToolImagePath+configTool.config[configMenu].main.bg
        anchors.top:parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
    }
    Image{
        id:mainmenuScreenBG1
        source:  viewHelper.configToolImagePath+configTool.config[configMenu].main.bg
        anchors.top:parent.top
        anchors.left: parent.left
        visible: viewController.getActiveScreen()=="MainMenu" && !revcurtainsTypeAnim.running && !curtainsTypeAnim.running?true:false
    }
    ViewText{
        id:welcomeTxt
        visible:(viewController.getActiveScreen()=='MainMenu' && mainMenu.visible)?true:false
        opacity: 0
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: qsTranslate('','mainmenu_title_tm')
        anchors.horizontalCenter:parent.horizontalCenter
        width: qsTranslate('','mainmenu_title_w')
        varText: [viewHelper.isKids(current.template_id)?configTool.language.main_menu.mainmenu_kids_title:configTool.language.main_menu.mainmenu_title,configTool.config[configMenu].main.wel_color,qsTranslate('','mainmenu_title_fontsize')]
        horizontalAlignment: Text.AlignHCenter
        font.family:viewHelper.isKids(current.template_id)?viewHelper.kidsFont:viewHelper.thinFont
        font.pixelSize: /*(viewHelper.isKids(current.template_id))?qsTranslate('','mainmenu_title_kids_fontsize'):*/qsTranslate('','mainmenu_title_fontsize');
        font.bold:(!viewHelper.isKids(current.template_id))
    }
    ViewText{
        id:welcomeDesc
        visible:(viewController.getActiveScreen()=='MainMenu' && mainMenu.visible)?true:false
        opacity: 0
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: qsTranslate('','mainmenu_desc_tm')
        anchors.horizontalCenter:parent.horizontalCenter
        width: qsTranslate('','mainmenu_desc_w')
        varText: [viewHelper.isKids(current.template_id)?configTool.language.main_menu.mainmenu_kids_description:configTool.language.main_menu.mainmenu_description,configTool.config[configMenu].main.desc_color,qsTranslate('','mainmenu_desc_fontsize')]
        horizontalAlignment: Text.AlignHCenter
    }
    Item{
        id:mainmenuItem
        anchors.top:parent.top
        anchors.topMargin: topMargin

        width:core.width
        anchors.horizontalCenter: parent.horizontalCenter;
        height:qsTranslate('','mainmenu_menu_h')
        ListView{
            id:mainmenuPathView
            width: (mainmenuPathView.count>=7)?qsTranslate('','mainmenu_menu_w'):mainmenuPathView.count*qsTranslate('','mainmenu_menu_itemw');
            height:parent.height
            interactive: count<=7?false:true
            anchors.centerIn: parent
            boundsBehavior: ListView.StopAtBounds
            snapMode: ListView.SnapToItem
            highlightMoveDuration: 200
            delegate: mainMenuDel
            orientation: ListView.Horizontal
            model:viewHelper.blankModel;
            onMovementEnded: {
                if(currentItem.x<(contentX+width) && currentItem.x>contentX){
                }else{
                    if(currentItem.x>=contentX+width)
                        currentIndex= viewHelper.mainMenuIndex=indexAt(contentX+(width-10),10)
                    else
                        currentIndex= viewHelper.mainMenuIndex=indexAt(contentX+10,10)
                }
            }
            Keys.onRightPressed: {
                incrementCurrentIndex()
                if(current.template_id=="adult" || current.template_id=="kids")viewHelper.mainMenuIndex=currentIndex
            }
            Keys.onLeftPressed: {
                decrementCurrentIndex()
                if(current.template_id=="adult" || current.template_id=="kids")viewHelper.mainMenuIndex=currentIndex
            }
            Keys.onDownPressed: {
                if(viewController.getActiveScreenRef().focusFromMainMenu)
                    viewController.getActiveScreenRef().focusFromMainMenu()
            }
            Keys.onUpPressed: {
                widgetList.focusToHeader()
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    enterReleased(currentIndex)
                }
            }
        }
    }
    Component{
        id:mainMenuDel
        Item{
            id:thisDelRect
            property string textColor: (mainmenuPathView.currentIndex==index && mainmenuPathView.activeFocus?configTool.config[configMenu].main.text_color_h:(thisDel.isPressed?configTool.config[configMenu].main.text_color_p:configTool.config[configMenu].main.text_color_n))
            property bool isPressed: false
            width:qsTranslate('','mainmenu_menu_itemw');
            height: qsTranslate('','mainmenu_menu_itemh');

            visible: vChange?true:!animTimer.running?true:false
            property int startAnimIndex:mainAnimIndex
            property bool vChange: false
            onStartAnimIndexChanged: {
                if(index==startAnimIndex)
                    delAnim.start()
            }
            Image{
                id:thisDelSel
                //visible:(viewHelper.mainMenuIndex==index && mainmenuPathView.currentIndex==index  && mainmenuPathView.activeFocus?false:(viewHelper.mainMenuIndex==index && (current.template_id!="adult" && current.template_id!="kids" )?true:false))
                visible: viewHelper.mainMenuIndex==index && (current.template_id!="adult" && current.template_id!="kids" )?true:false
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                source:viewHelper.mainMenuIndex==index?viewHelper.configToolImagePath+configTool.config[configMenu].main.btn_s:""
                opacity: widgetList.isCurtainUp()?1:0
            }
            Image{
                id:thisDel
                anchors.top:thisDelRect.top
                anchors.topMargin:qsTranslate('','mainmenu_menu_icon_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                source:mainmenuPathView.currentIndex==index && mainmenuPathView.activeFocus?
                           viewHelper.cMediaImagePath+synopsisposter:
                           viewHelper.cMediaImagePath+poster;
            }
            ViewText {
                anchors.top: thisDelRect.top
                anchors.topMargin: qsTranslate('','mainmenu_menu_text_tm')
                anchors.horizontalCenter: thisDel.horizontalCenter
                width: thisDelRect.width
                horizontalAlignment: Text.AlignHCenter
                varText: [title,textColor,qsTranslate('','mainmenu_menu_text_fontsize')]
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                opacity: (mainmenuPathView.currentIndex==index && mainmenuPathView.activeFocus) || (viewHelper.mainMenuIndex==index)?1:0.5

            }
            MouseArea{
                anchors.fill: parent
                onPositionChanged: thisDelRect.isPressed=false
                onPressed: thisDelRect.isPressed=true
                onReleased: {
                    //   if(!thisDelRect.isPressed)return
                    mainmenuPathView.forceActiveFocus()
                    mainmenuPathView.currentIndex=index
                    enterReleased(index)
                    thisDelRect.isPressed=false
                }
            }
            ParallelAnimation{
                id:delAnim
                SequentialAnimation{
                    NumberAnimation { target: thisDelRect; property: "y";from:core.height-50;to:index==mainAnimIndex?thisDelRect.y:thisDelRect.y-50 ; duration: 500}
                    NumberAnimation { target: thisDelRect; property: "y";from:index==mainAnimIndex?thisDelRect.y:thisDelRect.y-50;to:thisDelRect.y ; duration:  index==mainAnimIndex?0:400}
                }
                NumberAnimation { target: thisDelRect; property: "opacity";from:0.2;to:1; duration: 1500}
                ScriptAction{
                    script:{
                        thisDelRect.vChange=true
                    }
                }
            }
        }
    }
    ParallelAnimation{
        id:curtainsTypeAnim
        SequentialAnimation{
            NumberAnimation { target: mainmenuItem; property: "anchors.topMargin";from:lastTopMargin;to:topMargin-20; duration:675;}
            NumberAnimation { target: mainmenuItem; property: "anchors.topMargin";from:topMargin-20;to:topMargin ; duration:600;easing.type: Easing.OutElastic}
        }
        SequentialAnimation{

            ParallelAnimation{
                NumberAnimation { target: mainmenuScreenBG; property: "anchors.topMargin";from:0;to:-(core.height/2); duration:500;}
            }
            ParallelAnimation{
                NumberAnimation { target:mainmenuScreenBG; property: "anchors.topMargin";from:-(core.height/2);to:-(core.height); duration:500;}
                NumberAnimation { target: mainmenuScreenBG; property: "opacity";from:1;to:0; duration:250;}
            }
            ScriptAction{
                script: {
                    isCurtainUp=true
                }
            }
        }

    }
    SequentialAnimation{
        id:revcurtainsTypeAnim
        NumberAnimation { target: mainmenuScreenBG; property: "opacity";from:0;to:1; duration:0;}
        NumberAnimation { target:mainmenuScreenBG; property: "anchors.topMargin";from:-core.height;to:0; duration:0;}
        NumberAnimation { target: mainmenuItem; property: "opacity";from:1;to:0 ; duration:5}
        NumberAnimation { target: mainmenuItem; property: "anchors.topMargin";from:lastTopMargin;to:topMargin ; duration:5}
        NumberAnimation { target: mainmenuItem; property: "opacity";from:0;to:1 ; duration:750}
        ScriptAction{
            script: {
                isCurtainUp=false
            }
        }
    }
    ParallelAnimation{
        id:welcomeAnim
        NumberAnimation { target: welcomeTxt; property: "opacity";from:welcomeTxt.opacity?1:0;to:welcomeTxt.opacity?0:1 ; duration:2000}
        NumberAnimation { target: welcomeDesc; property: "opacity";from:welcomeDesc.opacity?1:0;to:welcomeDesc.opacity?0:1 ; duration:2000}
    }
    ParallelAnimation{
        id:revwelcomeAnim
        NumberAnimation { target: welcomeTxt; property: "opacity";from:1;to:0 ; duration:1}
        NumberAnimation { target: welcomeDesc; property: "opacity";from:1;to:0; duration:1}
    }
}
