import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../../framework/components"
import "../components"

SmartLoader {
    id: voyager;
    height: core.height;width: core.width;
    property int voyagerClosedCalled: 0
    /***************************************************************************************************************************************************/
    Connections{
        target: voyager.visible?pif:null
        onSeatRatingChanged:{
            core.debug("Voyager.qml | onSeatRatingChanged  ");
            voyagerLoader.sourceComponent=blankComponent
            if(pif.launchApp.launchId==viewHelper.voyager3dAppName && (pif.getEntOn()==0 || pif.getOpenFlight()==0 )) {
                core.debug("Voyager.qml | Closing the Voyager ForceFully  since Ent is OFF/Close Flight  ");
                core.debug("Voyager.qml | Closing the Voyager ForceFully  since Ent is OFF/Close Flight | viewHelper.voyager3dAppName  "+ viewHelper.voyager3dAppName);
                pif.launchApp.forceStopPackage(viewHelper.voyager3dAppName)
                //                pif.launchApp.closeApp();
            }
            init()
        }
    }
    /***************************************************************************************************************************************************/
    function load(){}
    function init(){
        if(pif.getEntOn()==0 || pif.getOpenFlight()==0 ) {core.debug("Voyager.qml | Not Allowing to go in init() since Ent is OFF/Close Flight  ");return;}
        console.log("Voyager.qml | init  ");
        var catnode = dataController.mediaServices.getCategoryDataByTagName("flightmap");
        core.dataController.getNextMenu([catnode["cid"],catnode["tid"]],__loadVoyager);
    }
    function clearOnExit(){
        console.log("Voyager.qml | clearOnExit ");
        if(voyagerLoader.sourceComponent==blankComponent)return;
        voyagerLoader.item.clearLoader()
    }

    function loadPrevScreen(){
        clearOnExit()
    }

    function __loadVoyager(dModel){
        console.log("dModel.count : "+dModel.count)
        if(dModel.count>0){
            if(dModel.getValue(0,"categorymedia_purpose")=="broadcast"){
                var params=new Object();
                params["channelID"]=parseInt(dModel.getValue(0,"categorymedia_channel"),10);
                if(params["channelID"]==0)params["channelID"]=1;
                params["displayChNo"]=dModel.getValue(0,"categorymedia_channelnum");
                params["pipID"]=viewHelper.getVideoPipId();
                params["cid"]=current.node_id;
                params["mediaType"]="broadcastVideo";
                params["withBGAudio"]="true";
                params["ignoreSystemAspectRatio"]=true
                if(pif.lastSelectedAudio.getMediaState()!=pif.lastSelectedAudio.cMEDIA_STOP){
                    var sendSig=true;
                }else{
                    var sendSig=false;
                }
                if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                    pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedBradcastVoyager",{"voyagerBroadcast":true});
                }
                pif.broadcastVideo.play(params)
                if(sendSig){
                    delaySig.restart();
                }
                voyagerLoader.sourceComponent=voyagerBroadcast
            }else{
                if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                    pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedBradcastVoyager",{"voyagerBroadcast":true});
                }
                voyagerLoader.sourceComponent=voyager3D
            }
        }
    }

    function showFlightMap(flag){
        if(voyager.visible==flag)return;
        if(flag){init();}
        else{clearOnExit();}
    }

    /**************************************************************************************************************************/
    Loader{
        id:voyagerLoader
        anchors.fill:parent;
        sourceComponent: blankComponent
        property alias voyagerLoader: voyagerLoader
        onStatusChanged: {
            if(voyagerLoader.sourceComponent==blankComponent)return;
            if (voyagerLoader.status == Loader.Ready){
                voyagerLoader.item.initialize()
                voyagerLoader.item.forceActiveFocus()
            }
        }
    }
    Component{
        id:blankComponent
        Item{}
    }
    Component{
        id:voyagerBroadcast
        FocusScope{
            width:parent.width
            height:parent.height
            focus:true;
            Rectangle{
                anchors.fill:parent;
                color:pif.getPIPColorKey();
            }

            function clearLoader(){
                hideCloseButton.stop();
                if(pif.getHandsetType()==pif.cSTD_HANDSET)pif.standardHandsetDisplay("   ")
                if(pif.lastSelectedAudio.getMediaState()!=pif.lastSelectedAudio.cMEDIA_STOP){
                    var sendSig=true;
                }else{
                    var sendSig=false;
                }
                if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                    pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedBradcastVoyager",{"voyagerBroadcast":false});
                pif.broadcastVideo.stop();
                if(sendSig){
                    delaySig.restart();
                }
                pif.paxus.stopContentLog()
                voyager.visible = false
                viewHelper.voyagerLaunched=false
                viewController.getActiveScreenRef().forceActiveFocus()
                voyagerLoader.sourceComponent=blankComponent
                //                viewHelper.homeAction()
            }
            function initialize(){
                core.debug("Voyager.qml | BroadCast Launch |  initialize  ");
                loadImg.visible=true
                showVoyagerTimer.restart();
                voyagerClosedCalled = 0;
                voyager.visible=true
                viewHelper.voyagerLaunched=true
                // voyager.forceActiveFocus();

                closeBTN.forceActiveFocus();
                closeBTN.opacity=0;
                pif.paxus.startContentLog("Moving Map")
            }
            Timer{
                id:showVoyagerTimer;interval:5000;
                onTriggered:{
                    loadImg.visible=false
                }
            }
            Timer{
                id:delaySig;
                interval:500;
                onTriggered:{
                    if(pif.getEntOn()==0)return;
                    pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedBroadCastWithNoAudio",{"mediaState":pif.aod.getMediaState()});

                }
            }
            AnimatedImage{
                id:loadImg
                anchors.centerIn:parent;
                source:(visible)?(viewHelper.isKids(current.template_id)?viewHelper.configToolImagePath+configTool.config.kids_popup.gamelaunch.loading:viewHelper.configToolImagePath+configTool.config.popup.gamelaunch.loading):"";
            }
            MouseArea{
                anchors.fill:parent;
                onClicked:{
                    if(closeBTN.opacity==0){
                        closeBTN.opacity=1;
                        hideCloseButton.restart();
                    }else{
                        closeBTN.opacity=0;
                        hideCloseButton.stop();
                    }
                }

            }
            SimpleNavButton{
                id:closeBTN
                anchors.right: parent.right
                anchors.top:parent.top
                anchors.rightMargin: qsTranslate('','common_map_close_rm')
                anchors.topMargin: qsTranslate('','common_map_close_tm')
                isHighlight: activeFocus
                normImg:  viewHelper.configToolImagePath+configTool.config.movies.synopsis.close_btn_n
                highlightimg:viewHelper.configToolImagePath+configTool.config.movies.synopsis.close_btn_h
                pressedImg: viewHelper.configToolImagePath+configTool.config.movies.synopsis.close_btn_p
                nav:["","","",""]
                onEnteredReleased:{loadPrevScreen();}
                focus:true;
                Keys.onPressed:{
                    hideCloseButton.restart();
                    if(closeBTN.opacity==0){closeBTN.opacity=1;}
                }
            }
            Timer{
                id:hideCloseButton;
                interval:5000;
                onTriggered:{
                    closeBTN.opacity=0;
                }
            }
        }
    }
    Component{
        id:voyager3D
        Item{
            id:voyagerItem
            width: core.width
            height: core.height
            Connections{
                target:voyager.visible?pif.launchApp:null;
                onSigAppExit:{
                    widgetList.showAppLoading(false)
                    voyagerItem.executeExit()
                }
            }
            Timer{
                id:hideLoadingScreen;
                interval:5000;
                onTriggered:{
                    widgetList.showAppLoading(false)
                }
            }
            Timer{
                id:simulateVoyager3DExit;
                interval:5000;
                onTriggered:{
                    pif.launchApp.sigAppExit("games",4,"eng")
                }
            }
            function clearLoader(){
                console.log("clearLoader >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> = viewHelper.voyager3dAppName "+viewHelper.voyager3dAppName)
                if(pif.launchApp.launchId==viewHelper.voyager3dAppName){
                    pif.launchApp.forceStopPackage(viewHelper.voyager3dAppName)
                    //pif.launchApp.forceAppToQuit()
                    return;
                }
                executeExit()
            }
            function executeExit(){
                widgetList.showAppLoading(false)
                if(pif.getHandsetType()==pif.cSTD_HANDSET)pif.standardHandsetDisplay("   ")
                pif.paxus.stopContentLog()
                viewHelper.voyagerLaunched=false
                voyager.visible = false
                viewController.getActiveScreenRef().forceActiveFocus()
                voyagerLoader.sourceComponent=blankComponent
                if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                    pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedBradcastVoyager",{"voyagerBroadcast":false});
                //                viewHelper.homeAction()
            }
            function initialize(){
                pif.launchApp.launchApp("",pif.launchApp.cUSE_ANDROID_ACCESS_HANDLER,viewHelper.voyager3dAppName,"",true);
                voyager.visible=true
                voyagerClosedCalled = 0;
                viewHelper.voyagerLaunched=true
                var catnode = dataController.mediaServices.getCategoryDataByTagName("flightmap");
                pif.paxus.startContentLog("Moving Map")
                widgetList.showAppLoading(true)
                hideLoadingScreen.restart();
                if(core.pc)simulateVoyager3DExit.restart()
            }
            Rectangle{
                id:bg
                //    x:qsTranslate('','popups_gamesload_bg_lm')
                y:qsTranslate('','popups_gamesload_bg_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                height: qsTranslate('','popups_gamesload_bg_h')
                width: qsTranslate('','popups_gamesload_bg_w')
                radius: qsTranslate('','popups_gamesload_bg_radius')
                color:configTool.config.popup.generic.bg_color;
                Item{
                    anchors.centerIn:bg;
                    height: qsTranslate('','popups_gamesload_content_h')
                    width: (loadImg.width+parseInt(qsTranslate('','popups_gamesload_text_lm'),10)+desc.paintedWidth)
                    AnimatedImage{
                        id:loadImg
                        anchors.left:parent.left;
                        anchors.verticalCenter:parent.verticalCenter;
                        source:(appLoading.visible)?(viewHelper.isKids(current.template_id)?viewHelper.configToolImagePath+configTool.config.kids_popup.gamelaunch.loading:viewHelper.configToolImagePath+configTool.config.popup.gamelaunch.loading):"";
                        paused:(!appLoading.visible)
                    }
                    ViewText{
                        id:desc
                        anchors.left:loadImg.right;
                        anchors.leftMargin:qsTranslate('','popups_gamesload_text_lm');
                        height: qsTranslate('','popups_gamesload_text_h');
                        width: qsTranslate('','popups_gamesload_text_w');
                        anchors.verticalCenter:parent.verticalCenter;
                        varText:[configTool.language.games.loading,configTool.config.popup.generic.text_color,qsTranslate('','popups_gamesload_text_fontsize')]
                    }
                }
            }
        }
    }
}
