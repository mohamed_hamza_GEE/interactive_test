import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import "../../framework/components/"
import Panasonic.Pif 1.0
import "../../framework/blank.js" as ImageData

SmartLoader {
    id: slideShow
    /**************************************************************************************************************************/
    property int curIndex : viewHelper.usbIndexSelected
    property int slideCount : imageSequencer.slideShowModel.count
    property int _rotateAngle;
    property int ctrl;
    property int nth_angle: 1
    property int nth_mod;
    property bool inRepeat: true;
    property bool playClick:false;
    property bool isFullScreen:true;
    property bool isBacklightOff:false;
    property bool isplaying:false;
    property variant configTag:configTool.config.usb.screen;
    property variant configVal:core.configTool.config.music.tracklist;
    /**************************************************************************************************************************/
    Connections{
        id:sequence
        target:visible?imageSequencer:null
        onSigPlayedIndex:{ slideshowItem.loadCurIndex(index);}
        onSigSequencerCompleted:{}
        onSigSequencerPlayBegan:{}
        onSigSequencerPaused:{}
        onSigSequencerResumed:{}
        onSigSequencerPlayStopped:{
            timer.running = false;
            timer.stop();curIndex=0;
        }
        onSigSequencerNextIndex:{slideshowItem.loadCurIndex(index);}
        onSigSequencerPreviousIndex:{slideshowItem.loadCurIndex(index);}
    }
    Connections{
        target: visible?pif:null
        onPaStateChanged:{
            if(paState>=1)stopSS();
            else{
                if(!isplaying){
                    stopSS()
                }else resumeSS()
            }
        }
    }
    Connections{
        target: visible?pif:null
        onBacklightStateChanged: {
            if(backlightState  && isBacklightOff){
                resumeSS()
            }
            else if(!backlightState){
                if(isplaying){
                    pauseSS()
                    isBacklightOff=true
                    return;
                }
            }
            isBacklightOff=false
            //else pif.setBacklightOn();
        }
    }

    /**************************************************************************************************************************/
    function load(){
        push(compBlackBg,'blackbg')
        push(compSlideshowItem,'slideshowItem')
        // push(compListView,'listView')
        push(compPanelObj,"panel");
        push(closeComp,"close");
        push(compPanelItems,"panelItems");
    }
    function showUsbSlideshow(flag){
        if(visible==flag)return;
        if(flag){init();}
        else{clearOnExit();}
    }
    function init(){
        playClick = false;
        sequence.target = imageSequencer
        viewHelper.usbSlideshow = true;
        imageSequencer.populateSlideShowModel(viewHelper.filePathName)
        __delayLoad.restart()
    }

    Timer{
        id:__delayLoad
        interval:400;
        onTriggered: {
            imageSequencer.initiateSequencer(5,inRepeat,false);
            viewHelper.usbSelectedIndex =core.coreHelper.searchEntryInModel(imageSequencer.slideShowModel,"filenameonly",viewHelper.usbFileName)
            imageSequencer.playSequencer(viewHelper.usbSelectedIndex);
            timer.running = true;
            slideShow.visible=true;
            isplaying=true
            panelItems.focusToPlay()
            imgInfo.init();pif.screenSaver.stopScreenSaver();
            panelItems.getShuffleRef().cstate=false
        }
    }

    function clearOnExit(){
        core.debug("UsbSlideShow.qml  | clearOnExit ")
        imageSequencer.stopSequencer();
        sequence.target = null
        timer.running = false;
        timer.stop();/*isFullScreen=false*/
        playClick = false
        slideshowItem.mainImg.source=""
        viewHelper.usbSlideshow = false;
        viewHelper.usbSlideShowCLosed()
        visible=false;
        pif.screenSaver.startScreenSaver();
        imgInfo.clearOnExit();
        isBacklightOff=false
    }
    function stopSS(){
        if(imageSequencer.sequenceState == imageSequencer.cSEQUENCEPLAY){
            imageSequencer.pauseSequencer();
        }
        timer.running = false;
        timer.stop();
    }
    function closeSlideShow(){
        clearOnExit()
    }
    function nth_rotate_angle(){
        nth_angle = (_rotateAngle / 90)
        nth_mod = (nth_angle % 2)
    }
    function pauseSS(){
        stopSS()
    }
    function resumeSS(){
        imageSequencer.resumeSequencer();
        timer.start();
    }
    function getMainImg(index){
        if(slideCount> 0){
            return imageSequencer.slideShowModel.getValue(index,"pathfilename");
        }
        return "";
    }
    function initiateSequencer(index){
        imageSequencer.initiateSequencer(imageSequencer.slideShowModel.count,viewHelper.slideShowInterval,inRepeat,0)
        viewHelper.usbSelectedIndex = index;
        imageSequencer.playSequencer(viewHelper.usbSelectedIndex);
    }
    function restartTimer(a){
        ctrl = a;
        if(a == 1){
            showAnim.start()
            isFullScreen=true
            panelItems.focusToPlay()
            timer.start();
        }else{
            hideAnim.start()
            isFullScreen=true
            blackbg.forceActiveFocus()
            timer.start();
        }
    }
    function nextAllowed(){
        if(inRepeat==true) return true;
        var model = imageSequencer.slideShowModel
        var cnt = model.count
        if(curIndex >= cnt-1) return false;
        else return true;
    }
    function prevAllowed(){
        if(inRepeat==true) return true;
        if(curIndex <=0 && inRepeat==false) return false;
        else return true;
    }
    function intervalchange(sec){
        imageSequencer.reinitialiseSequencer(sec,0);
        restartTimer(0);
    }
    function menuDisplay(){
        if(ctrl == 1){
            restartTimer(0);
        }else{
            restartTimer(1);
        }
    }
    /**************************************************************************************************************************/
    property QtObject blackbg
    Component{
        id:compBlackBg
        Rectangle{
            color:configTag["slide_show_panel_fill"]
            opacity:1
            width: parent.width
            height:parent.height
            MouseArea{
                anchors.fill:parent
                onClicked: {
                    menuDisplay()
                }
            }
            Keys.onPressed: {
                menuDisplay()
            }
        }
    }
    property QtObject close
    Component{
        id:closeComp
        SimpleNavButton{
            id:closeObj;
            normImg:viewHelper.configToolImagePath+configTag["close_btn_n"];
            highlightimg:viewHelper.configToolImagePath+configTag["close_btn_h"];
            pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
            anchors.right:blackbg.right;
            anchors.rightMargin: qsTranslate('',"connectgate_close_rm")
            anchors.top:blackbg.top;
            anchors.topMargin: qsTranslate('',"connectgate_close_tm")
            opacity: panel.opacity
            nav:["","","",""]
            onEnteredReleased:{
                showUsbSlideshow(false)
            }
            onNoItemFound:{
                if(dir=="down"){panelItems.focusToPlay()}
            }
        }
    }
    property QtObject slideshowItem
    Component{
        id:compSlideshowItem
        FocusScope {
            id: mainItem
            height:!isFullScreen?qsTranslate('','usb_image_small_h'):qsTranslate('','usb_fullscreen_h')
            width:!isFullScreen?qsTranslate('','usb_image_small_w'):qsTranslate('','usb_fullscreen_w')
            anchors.top: blackbg.top
            anchors.topMargin: !isFullScreen?qsTranslate('','usb_image_small_tm'):qsTranslate('','usb_fullscreen_tm')
            anchors.horizontalCenter: blackbg.horizontalCenter
            property variant angle: 0
            property alias mainImg: mainImg
            property int aw: mainImg.sourceSizeWidth
            property int ah: mainImg.sourceSizeHeight
            property variant sourceFileName:getMainImg(viewHelper.usbSelectedIndex);
            function loadCurIndex(index){
                viewHelper.usbSelectedIndex = index;
                curIndex = index;
                var src = getMainImg(curIndex)
                var angle = imgInfo.getRotationValue(src);
                angle = (angle%360)
                if(angle < 0) angle += 360
                mainImg.rotation=angle;
                mainItem.angle = angle;
                _rotateAngle = angle
                mainImg.width=undefined;
                mainImg.height = undefined;
                mainImg.source = "";
                mainImg.source = src;
                nth_rotate_angle()
                if(nth_mod !== 0){
                    mainImg.rotation_changed()
                }
                else
                {
                    mainImg.status_Changed()
                }
            }
            function rotateImage(direction){
                var src = mainImg.source;
                mainImg.source = "";
                mainImg.source = src;
                var curDir = mainItem.angle
                if(direction == 1)curDir+=90;
                else curDir -= 90;
                mainImg.rotation=curDir;
                mainItem.angle = curDir;
                if(direction == 1)_rotateAngle = parseInt(_rotateAngle) + 90;
                else _rotateAngle = parseInt(_rotateAngle) - 90;
                nth_rotate_angle()
                if(nth_mod !== 0)
                {
                    mainImg.rotation_changed()
                }
                else
                {
                    mainImg.status_Changed()
                }
                if(imageSequencer.sequenceState != imageSequencer.cSEQUENCEPAUSE){ pauseSS();}
            }
            onSourceFileNameChanged: {
                mainImg.source = "";
                mainImg.source = sourceFileName;

            }
            focus:true
            Image {
                id: mainImg
                property bool painted: false
                property  int usbImgHeight:mainItem.height;
                property int usbImgWidth: mainItem.width;
                anchors.centerIn: parent;
                sourceSize.width : usbImgWidth;
                onSourceChanged:{
                    width=undefined;
                    height=undefined;
                    if(!playClick){pauseSS();playClick = true;}
                }

                function rotation_changed(){
                    if(source=="")return;
                    if(status!=Image.Ready)return;
                    adjustImageDim(rotation);
                    imgInfo.storeRotationValue(getMainImg(curIndex),rotation)
                }

                function _widthRules(){
                    width = usbImgWidth;
                    height = usbImgWidth*(width/height);
                }

                function _heightRules(){
                    height = usbImgHeight;
                    width= usbImgHeight*(width/height);
                }

                function _widthRules1(){
                    width = usbImgHeight;
                    height = usbImgHeight*(height/width);
                }

                function _heightRules1(){
                    height = usbImgWidth;
                    width = usbImgWidth*(height/width);

                }



                function adjustImageDim(angle){
                    core.debug("angle : "+angle)
                    core.debug("width : "+width)
                    core.debug("height : "+height)
                    if(angle%180==0){
                        var w=width;
                        var h=height;
                        if(w>usbImgWidth && h>usbImgHeight){
                            if(w<h){
                                _widthRules();
                            }else{
                                _heightRules();
                            }
                        }else if(w>usbImgWidth){
                            _widthRules();
                        }else if(h>usbImgHeight){
                            _heightRules();
                        }
                    }else{
                        var w=height;
                        var h=width;
                        if(w>usbImgWidth&& h>usbImgHeight){
                            if(w<h){
                                core.debug("_heightRules()")
                                _heightRules1();

                            }else{
                                core.debug("_widthRules();")
                                _widthRules1();
                            }
                        }else if(w>usbImgHeight){
                            core.debug("_heightRules() 2")
                            _widthRules1();

                        }else if(h>usbImgWidth){
                            core.debug("_widthRules(); 2")
                            _heightRules1();

                        }
                    }
                    core.debug(" usbImgWidth : "+usbImgWidth);
                    core.debug(" usbImgHeight : "+usbImgHeight);


                    core.debug(" width : "+width);
                    core.debug(" height : "+height);
                }
                function status_Changed(){
                    if(source=="")return;
                    if(status==Image.Loading){
                        if(imageSequencer.sequenceState == imageSequencer.cSEQUENCEPLAY){
                            imageSequencer.pauseSequencer();
                        }
                    }else{
                        imageSequencer.resumeSequencer();
                    }
                    if(status!=Image.Ready)return;
                    adjustImageDim(rotation)

                }
                Behavior on height{
                    NumberAnimation {id:hAnim; duration: 1500;}
                }
                Behavior on width{
                    NumberAnimation { id:wAnim;duration: 1500;}
                }
                Behavior on anchors.topMargin {
                    NumberAnimation { id:tmAnim;duration: 150;}
                }

                rotation: 0;
                smooth: false;
                //playing: true;
                asynchronous: true;
                onStatusChanged:{
                    if(mainImg.status == Image.Error){
                    }else if(mainImg.status == Image.Ready){
                        status_Changed();
                    }
                }
                fillMode: Image.PreserveAspectFit
            }
        }
    }
    property QtObject listView
    Component{
        id:compListView
        ListView{
            id:listView
            model:imageSequencer.slideShowModel
            snapMode:"SnapOneItem";
            width:parent.width
            height:parent.height
            anchors.centerIn: parent
            boundsBehavior:ListView.StopAtBounds;
            preferredHighlightBegin:0;
            preferredHighlightEnd:width;
            highlightRangeMode:ListView.StrictlyEnforceRange;
            orientation:ListView.Horizontal
            clip:true;
            interactive: true
            cacheBuffer: width*2
            onMovementEnded:{
                imageSequencer.setNextIndex();
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {menuDisplay()}
            }
            Keys.onPressed: {menuDisplay()}
            delegate:slideshowItem
        }
    }
    property QtObject panel
    Component{
        id:compPanelObj
        Image{
            id:panelBG
            anchors.horizontalCenter: blackbg.horizontalCenter
            anchors.bottom:blackbg.bottom;
            anchors.bottomMargin: qsTranslate('','usb_cntrl_bg_bm')
            source:viewHelper.configToolImagePath+"slide_show_control_bg.png"
            opacity:1
        }
    }
    property QtObject panelItems
    Component{
        id:compPanelItems
        Item{
            id:panelItems
            anchors.fill: panel
            opacity:panel.opacity
            property variant fileName:imageSequencer.slideShowModel.getValue(viewHelper.usbSelectedIndex,"filenameonly")
            property variant fileSize:imageSequencer.slideShowModel.getValue(viewHelper.usbSelectedIndex,"filesize")
            function focusToPlay(){playNpause.forceActiveFocus()}
            function getShuffleRef(){return shuffleOnOff}
            function formatSize(bytes){
                bytes=bytes*1;var GB=1073741824;var MB=1048576;var KB=1024;
                var suffix="";var div;var size;
                if (bytes >= GB){
                    div=GB;suffix=" GB";
                }else if(bytes >= MB){
                    div=MB;suffix=" MB";
                }else{
                    div=KB;suffix=" KB";
                }
                size=(Math.round(bytes/div*100000)/100000);
                size=(size.toFixed(2)) + suffix;
                return size;
            }
            ViewText{
                id:imgName
                anchors.verticalCenter: parent.verticalCenter
                anchors.left:parent.left
                anchors.leftMargin: qsTranslate('','usb_cntrl_bg_text_lm')
                width:qsTranslate('',"usb_cntrl_bg_w");
                varText:[panelItems.fileName+" | "+panelItems.formatSize(panelItems.fileSize),configTag["slide_show_text_color"],qsTranslate('',"usb_cntrl_bg_fontsize")]
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignLeft
                maximumLineCount:1
            }
            Row{
                spacing:qsTranslate('','usb_cntrl_gap')
                anchors.left:parent.left;
                anchors.leftMargin: qsTranslate('','usb_cntrl_lm')
                anchors.verticalCenter: parent.verticalCenter
                Item{
                    width:qsTranslate('','usb_cntrl_w');
                    height:qsTranslate('','usb_cntrl_h');
                    ToggleNavButton{
                        id:playNpause
                        normImg1:viewHelper.configToolImagePath+configTag["pause_button_n"];
                        normImg2:viewHelper.configToolImagePath+configTag["play_button_n"];
                        highImg1:viewHelper.configToolImagePath+configTag["pause_button_h"];
                        highImg2:viewHelper.configToolImagePath+configTag["play_button_h"];
                        pressImg1: viewHelper.configToolImagePath+configTag["pause_button_p"];
                        pressImg2: viewHelper.configToolImagePath+configTag["pause_button_p"];
                        isHighlight: activeFocus && !isPressed?true:false
                        anchors.centerIn:parent;
                        increaseTouchArea:true;
                        increaseValue:parent.width/4;
                        focus:true;
                        nav:["","","",""];
                        onNoItemFound:{
                            if(dir=="up"){close.forceActiveFocus()}
                            else if(dir=="left")return
                            else if(dir=="right"){prev.forceActiveFocus()}
                        }
                        cstate:(imageSequencer.sequenceState == imageSequencer.cSEQUENCEPLAY)?true:false
                        onEnteredReleased:{
                            if(cstate){
                                stopSS()
                                isplaying=false
                            }
                            else{
                                if(curIndex==imageSequencer.slideShowModel.count-1 && inRepeat==false){imageSequencer.playSequencer(0)}
                                else
                                    resumeSS()
                                isplaying=true
                            }
                        }
                    }
                }
                Item{
                    width:qsTranslate('','usb_cntrl_w');
                    height:qsTranslate('','usb_cntrl_h');
                    SimpleNavButton{
                        id:prev
                        normImg:viewHelper.configToolImagePath+configTag["prev_button_n"]
                        highlightimg:viewHelper.configToolImagePath+configTag["prev_button_h"]
                        pressedImg:viewHelper.configToolImagePath+configTag["prev_button_p"]
                        anchors.centerIn:parent;
                        increaseTouchArea:true;
                        increaseValue:parent.width/4;
                        nav:["","","",""];
                        onNoItemFound:{
                            if(dir=="up"){close.forceActiveFocus()}
                            else if(dir=="left"){playNpause.forceActiveFocus()}
                            else if(dir=="right"){next.forceActiveFocus()}
                        }
                        onEnteredReleased:{
                            imageSequencer.setPreviousIndex()
                        }
                    }
                }
                Item{
                    width:qsTranslate('','usb_cntrl_w');
                    height:qsTranslate('','usb_cntrl_h');
                    SimpleNavButton{
                        id:next
                        normImg:viewHelper.configToolImagePath+configTag["next_button_n"];
                        highlightimg:viewHelper.configToolImagePath+configTag["next_button_h"];
                        pressedImg:viewHelper.configToolImagePath+configTag["next_button_p"];
                        anchors.centerIn:parent;
                        increaseTouchArea:true;
                        increaseValue:parent.width/4;
                        nav:["","","",""];
                        onNoItemFound:{
                            if(dir=="up"){close.forceActiveFocus()}
                            else if(dir=="left"){prev.forceActiveFocus()}
                            else if(dir=="right"){rotateRight.forceActiveFocus()}
                        }
                        onEnteredReleased:{
                            imageSequencer.setNextIndex();
                        }
                    }
                }
                Item{
                    width:qsTranslate('','usb_cntrl_w');
                    height:qsTranslate('','usb_cntrl_h');
                    SimpleNavButton{
                        id:rotateRight
                        normImg:viewHelper.configToolImagePath+configTag["rotate1_n"]
                        highlightimg:viewHelper.configToolImagePath+configTag["rotate1_h"]
                        pressedImg:viewHelper.configToolImagePath+configTag["rotate1_p"]
                        anchors.centerIn:parent;
                        increaseTouchArea:true;
                        increaseValue:parent.width/4;
                        nav:["","","",""];
                        onNoItemFound:{
                            if(dir=="up"){close.forceActiveFocus()}
                            else if(dir=="left"){next.forceActiveFocus()}
                            else if(dir=="right"){rotateLeft.forceActiveFocus()}
                        }
                        onEnteredReleased:{
                            slideshowItem.rotateImage(1)
                        }
                    }
                }
                Item{
                    width:qsTranslate('','usb_cntrl_w');
                    height:qsTranslate('','usb_cntrl_h');
                    SimpleNavButton{
                        id:rotateLeft
                        normImg:viewHelper.configToolImagePath+configTag["rotate2_n"];
                        highlightimg:viewHelper.configToolImagePath+configTag["rotate2_h"];
                        pressedImg:viewHelper.configToolImagePath+configTag["rotate2_p"];
                        anchors.centerIn:parent;
                        increaseTouchArea:true;
                        increaseValue:parent.width/4;
                        nav:["","","",""];
                        onNoItemFound:{
                            if(dir=="up"){close.forceActiveFocus()}
                            else if(dir=="left"){rotateRight.forceActiveFocus()}
                            else if(dir=="right"){repeatOnOff.forceActiveFocus()}
                        }
                        onEnteredReleased:{
                            slideshowItem.rotateImage(-1)
                        }
                    }
                }
                Item{
                    width:qsTranslate('','usb_cntrl_w');
                    height:qsTranslate('','usb_cntrl_h');
                    ToggleNavButton{
                        id:repeatOnOff
                        normImg1:viewHelper.configToolImagePath+configVal["repeat_on_n"];
                        normImg2:viewHelper.configToolImagePath+configVal["repeat_off_n"];
                        highImg1:viewHelper.configToolImagePath+configVal["repeat_on_h"];
                        highImg2:viewHelper.configToolImagePath+configVal["repeat_off_h"];
                        pressImg1: viewHelper.configToolImagePath+configVal["repeat_on_p"];
                        pressImg2: viewHelper.configToolImagePath+configVal["repeat_off_p"];
                        isHighlight: activeFocus && !isPressed?true:false
                        anchors.centerIn:parent;
                        selectedImg1:pressImg1;
                        selectedImg2:pressImg2;
                        isSelected:cstate
                        increaseTouchArea:true;
                        increaseValue:parent.width/4;
                        nav:["","","",""];
                        onNoItemFound:{
                            if(dir=="up"){close.forceActiveFocus()}
                            else if(dir=="left"){rotateLeft.forceActiveFocus()}
                            else if(dir=="right"){shuffleOnOff.forceActiveFocus()}
                        }
                        cstate:inRepeat;
                        onEnteredReleased:{
                            if(!cstate){
                                inRepeat=true
                                imageSequencer.setRepeat(inRepeat);
                            }
                            else{
                                inRepeat=false
                                imageSequencer.setRepeat(inRepeat);
                            }
                        }
                    }
                }
                Item{
                    width:qsTranslate('','usb_cntrl_w');
                    height:qsTranslate('','usb_cntrl_h');
                    ToggleNavButton{
                        id:shuffleOnOff
                        normImg1:viewHelper.configToolImagePath+configVal["shuffle_on_n"];
                        normImg2:viewHelper.configToolImagePath+configVal["shuffle_off_n"];
                        highImg1:viewHelper.configToolImagePath+configVal["shuffle_on_h"];
                        highImg2:viewHelper.configToolImagePath+configVal["shuffle_off_h"];
                        pressImg1: viewHelper.configToolImagePath+configVal["shuffle_on_p"];
                        pressImg2: viewHelper.configToolImagePath+configVal["shuffle_off_p"];
                        anchors.centerIn:parent;
                        increaseTouchArea:true;
                        increaseValue:parent.width/4;
                        selectedImg1:pressImg1;
                        selectedImg2:pressImg2;
                        isSelected:cstate
                        isHighlight: activeFocus && !isPressed?true:false
                        nav:["","","",""];
                        onNoItemFound:{
                            if(dir=="up"){close.forceActiveFocus()}
                            else if(dir=="left"){repeatOnOff.forceActiveFocus()}
                            else if(dir=="right")return
                        }
                        cstate:false;
                        onEnteredReleased:{
                            if(cstate){
                                imageSequencer.setShuffle(false)
                                cstate=false
                            }
                            else{
                                imageSequencer.setShuffle(true);cstate=true
                            }
                        }
                    }
                }
            }
        }
    }
    Item{
        id:imgInfo
        function init(){
            ImageData.imgInfo=new Array();
        }
        function clearOnExit(){
            ImageData.imgInfo=new Array();
        }
        function storeRotationValue(fullPath,value){
            ImageData.imgInfo[fullPath]=value;
        }
        function getRotationValue(fullPath){
            if(ImageData.imgInfo && ImageData.imgInfo[fullPath]){
                return ImageData.imgInfo[fullPath];
            }
            return 0;
        }
    }
    Sequencer{id:imageSequencer;}
    ParallelAnimation{
        id:showAnim;
        NumberAnimation {target: close; property: "anchors.rightMargin"; duration:750; to:qsTranslate('',"connectgate_close_rm"); from:-50 }
        NumberAnimation {target: panel; property: "anchors.bottomMargin"; duration:700; to:qsTranslate('','usb_cntrl_bg_bm'); from: -20 }
    }
    ParallelAnimation{
        id:hideAnim;
        NumberAnimation {target: close; property: "anchors.rightMargin"; duration: 750; to:-50; from: qsTranslate('',"connectgate_close_rm"); }
        NumberAnimation {target: panel; property: "anchors.bottomMargin"; duration: 700; to:-20; from: qsTranslate('','usb_cntrl_bg_bm'); }
    }
    UserEventFilter{
        enabled: visible?true:false
        onKeyPressed: {timer.restart()}
        onKeyReleased: {}
        onMousePressed: {timer.running=false;timer.stop()}
        onMouseReleased: {timer.restart()}
    }
    Timer{
        id:timer;
        running:false;
        interval:10000;
        onTriggered:{
            restartTimer(0);
        }
    }
    /**************************************************************************************************************************/
}
