import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: header
    anchors.fill: parent
    signal restartVODTimer
    signal stopVODTimer(bool param)
    /**************************************************************************************************************************/
    property bool isFlightInfoOpen: false
    property variant configHeaderTag:viewHelper.isKids(current.template_id)?configTool.config.kids_global.header:configTool.config.global.header;
    property variant airPortDetails: []
    property variant viewMapsDetails: []
    property variant flightConnectionDetails: []
    property double distanceCovered: 0
    property variant configTabSettings:viewHelper.isKids(current.template_id)?configTool.config.kids_global.settings:configTool.config.global.settings;
    property string configMenu:(viewHelper.isKids(current.template_id))?"kids_menu":"menu";
    property string sourceTriggered:''
    onIsFlightInfoOpenChanged: {
        if(!isFlightInfoOpen){flightInfoobj.closeAnim.restart();}
        if(isFlightInfoOpen)init()
        if(sourceTriggered=='settings')return
        stopVODTimer(isFlightInfoOpen)
    }
    Timer{
        id:timerToBlank
        interval: 450
        onTriggered: {
            flightInfoobj.flightInfoLoader.sourceComponent=blankComp
            openFlightInfo(false)
        }
    }
    Timer{
        id:timeDelay
        interval: 550
        onTriggered: headerBtnsobj.flightInfoTab.isHighlightDelayed=false
    }
    Connections{
        target: viewData
        onMainMenuModelReady:{
            callingExternal.restart()

        }
    }
    Timer{id:callingExternal;interval: 500;onTriggered:{if( viewHelper.directJumpFromKids!=""){
                if(!widgetList.isVideoVisible()) {viewHelper.externalEvents(viewHelper.directJumpFromKids)
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedMenuJump",{sectionIndex:viewHelper.adultIndex,iso:""})
                }
                viewHelper.directJumpFromKids=""
            }}}
    Connections{
        target: pif
        onTimeToDestinationChanged:{distanceCoveredByPlane();}
        onTimeSinceTakeOffChanged:{distanceCoveredByPlane();}
        onArrivalTimeChanged: viewMapsDetails=[getFlightData(4),getFlightData(5),getFlightData(6)]
        onTimeAtOriginChanged: viewMapsDetails=[getFlightData(4),getFlightData(5),getFlightData(6)]
        onTimeAtDestinationChanged: viewMapsDetails=[getFlightData(4),getFlightData(5),getFlightData(6)]
        onAltitudeChanged:   flightConnectionDetails=[getFlightData(7),getFlightData(8),getFlightData(9)]
        onGroundSpeedChanged:   flightConnectionDetails=[getFlightData(7),getFlightData(8),getFlightData(9)]
        onOutsideAirTempChanged: flightConnectionDetails=[getFlightData(7),getFlightData(8),getFlightData(9)]
        onSourceIATAChanged: headerBtnsobj.sourceATA.text=sourceIATA
        onDestinationIATAChanged:   headerBtnsobj.destinationATA.text=destinationIATA
    }
    Connections{
        target: flightInfoobj.flightInfoLoader.sourceComponent==settings?pif:null;
        onVolumeChanged: {
            if(!visible)return;
            flightInfoobj.flightInfoLoader.item.volumeSliderComp.setInitailValue=pif.getVolumeLevel()
        }
        onBrightnessChanged: {
            if(!visible)return;

            flightInfoobj.flightInfoLoader.item.brightnessSliderComp.setInitailValue=pif.getBrightnessLevel()
        }
    }

    /**************************************************************************************************************************/
    function load(){
        push(compnetted,"nettedobj")
        push(compFlightInfo,"flightInfoobj")
        push(compHeaderBtns,"headerBtnsobj")
    }
    function init(){
        //   airPortDetails=[getFlightData(1),getFlightData(2),getFlightData(3)]
        viewMapsDetails=[getFlightData(4),getFlightData(5),getFlightData(6)]
        flightConnectionDetails=[getFlightData(7),getFlightData(8),getFlightData(9)]
        distanceCoveredByPlane();
    }
    function clearOnExit(){ core.debug("Header.qml | clearOnExit ") }
    function reset(){
        isFlightInfoOpen=false
        airPortDetails=[]
        viewMapsDetails=[]
        flightConnectionDetails=[]
        distanceCovered=0
    }
    function showHelp(){
        sourceTriggered='settings';
        if(isFlightInfoOpen)isFlightInfoOpen=false
        stopVODTimer(true)
        widgetList.setHelpStatus(true)
        if(widgetList.isVideoVisible()){
            if(widgetList.getVideoRef().showHeaderFooter)widgetList.getVideoRef().hidePanel.restart();
            else widgetList.getVideoRef().hideonlyHeader.restart();
        }
        viewHelper.showHelpOnMainSeatChat()
    }
    /*************************************************************************************************/
    function showHeader(flag){
        if(header.visible!=flag)
            header.visible=flag
    }
    function downPressed(){
        if(isFlightInfoOpen){
            isFlightInfoOpen=false
        }
        if(widgetList.isVideoVisible())widgetList.focusToVideo()
        else if(widgetList.getMainMenuRef().visible) widgetList.focustTomainMenu()
        else if(viewController.getActiveScreenRef().focusFromHeader) viewController.getActiveScreenRef().focusFromHeader();
    }
    function focusToHeader(){ if(visible)headerBtnsobj.forceActiveFocus() }
    function restartVideoTimer(){
        if(widgetList.isVideoVisible())restartVODTimer()
    }
    function getFlightData(param){
        switch (param){
        case 1:{
            //var elapseTime=parseInt(core.pif.getTimeSinceTakeOff(),10);
            var remainingTime=parseInt(core.pif.getTimeToDestination(),10)
            return ''+getTime((remainingTime),2)
        }
        break;
        case 2:   return ''+(core.pif.getTimeSinceTakeOff().substring(0,2))+":"+(core.pif.getTimeSinceTakeOff().substring(2,4));break;
        case 3:return ''+getDuration();  break;
        case 4:  return  getTime(getMinsFromHHMM(core.pif.getTimeAtOrigin()));  break;
        case 5: return getTime(getMinsFromHHMM(core.pif.getArrivalTime())); break;
        case 6: return getTime(getMinsFromHHMM(core.pif.getTimeAtDestination())); break;
        case 7: return core.pif.getAltitude() +" "+ "feet"; break;
        case 8: return core.coreHelper.convertDistance(core.pif.getGroundSpeed(),"mph","knot",2)+" "+"mph"; break;
        case 9:  return coreHelper.convertCelsiusToFahrenheit(core.pif.getOutsideAirTemperature())+"ºF"; break;
        }
    }

    function distanceCoveredByPlane(){
        airPortDetails=[getFlightData(1),getFlightData(2),getFlightData(3)]
        var travelingImgWidth=(headerBtnsobj.chat_msg_Icon.visible)?qsTranslate('','header_flightinfo_w_chat'):qsTranslate('','header_flightinfo_w')
        var planeWidth=parseInt(headerBtnsobj.airplaneIcon.width,10)


        var elapseTimeString=core.pif.getTimeSinceTakeOff();
        var elapseTime=parseInt(elapseTimeString.substring(0,2),10)*60 + parseInt(elapseTimeString.substring(2,4),10);

        var remainingTime=parseInt(core.pif.getTimeToDestination(),10)
        var totaltime=elapseTime+remainingTime
        var percentageForRemainingTime=(elapseTime/totaltime)
        distanceCovered=(percentageForRemainingTime  *  (travelingImgWidth-planeWidth))
    }
    function getDuration(){
        var ttd = parseInt(core.pif.getTimeToDestination(),10);     //in MINS
        var timetakeoff = core.pif.getTimeSinceTakeOff();           // in HHMM
        var timesincetakeoffinmins = ((timetakeoff.substring(0,2)*60)*1) + ((timetakeoff.substring(2,4))*1);
        var totaltime = ttd + timesincetakeoffinmins;
        var duration = core.coreHelper.convertSecToHMLeadZero(totaltime*60)
        return duration;

    }
    function getTime(mins,format){
        var ret = getHHMMFromMins(mins);
        return padZero(ret[0])+":"+padZero(ret[1])
    }
    function padZero(val){
        if(val<10)
            val = "0"+val;
        return val;
    }
    function getHHMMFromMins(mins){
        var arr = new Array()
        arr[0] = Math.floor(parseInt(mins,10)/60);
        arr[1] = parseInt(mins,10)%60;
        return arr
    }
    function getMinsFromHHMM(hhmm){
        var hh = hhmm.substring(0,2);
        var mins = hhmm.substring(2,4);
        return parseInt((hh*60),10) + parseInt(mins,10);
    }
    function openFlightInfo(isFlightInfoTab){
        if(isFlightInfoOpen){
            if(!isFlightInfoTab){
                viewHelper.blockKeysForSeconds(0.6);
                isFlightInfoOpen=false;
                timerToBlank.restart();
                return
            }

        }
        //flightInfoobj.flightInfoLoader.sourceComponent=blankComp
        if(isFlightInfoTab)flightInfoobj.flightInfoLoader.sourceComponent=flightInfo
        else flightInfoobj.flightInfoLoader.sourceComponent=settings
        isFlightInfoOpen=true
        focusToHeader();

    }
    function helpBtnFocus(){
        isFlightInfoOpen=false;
        headerBtnsobj.flightInfoTab.isHighlightDelayed=true
        timeDelay.restart()
        if(flightInfoobj.flightInfoLoader.sourceComponent==settings)
            headerBtnsobj.settingBtn.forceActiveFocus()
        else
            headerBtnsobj.flightInfoTab.forceActiveFocus()
    }
    function getHeaderBtnFocus(){
        return headerBtnsobj.activeFocus
    }
    function changeLanguage(){
        var iso = flightInfoobj.flightInfoLoader.item.languageLV.model.get(flightInfoobj.flightInfoLoader.item.languageLV.currentIndex).ISO639
        var lid = flightInfoobj.flightInfoLoader.item.languageLV.model.get(flightInfoobj.flightInfoLoader.item.languageLV.currentIndex).LID
        var locale = flightInfoobj.flightInfoLoader.item.languageLV.model.get(flightInfoobj.flightInfoLoader.item.languageLV.currentIndex).language_locale
        viewHelper.setHelpLanguage(iso)
        core.setInteractiveLanguage(lid,iso,"",locale);
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedLangIndex",{langIndex:flightInfoobj.flightInfoLoader.item.languageLV.currentIndex})
            pif.sharedModelServerInterface.sendApiData("changeLanguage",{languageIso:iso})
        }
        viewHelper.languageIndex=flightInfoobj.flightInfoLoader.item.languageLV.currentIndex
        helpBtnFocus()
        viewHelper.welComeIndex=flightInfoobj.flightInfoLoader.item.languageLV.currentIndex
    }
    function closeLoader(){
        flightInfoobj.flightInfoLoader.sourceComponent=blankComp
    }
    /**************************************************************************************************************************/

    property QtObject nettedobj
    Component{
        id:compnetted
        Item{
            anchors.fill: parent
            Image{
                visible: widgetList.isVideoVisible() && isFlightInfoOpen
                source: viewController.settings.assetImagePath+qsTranslate('','common_overlay_netted_source')

            }
            Rectangle{
                anchors.fill: parent
                color:qsTranslate('','common_overlay_color')
                opacity: qsTranslate('','common_overlay_opacity')
                visible: !widgetList.isVideoVisible() && isFlightInfoOpen
                Behavior on visible{
                    NumberAnimation{duration: visible?350:200}
                }
            }
            MouseArea{
                anchors.fill: parent
                enabled: isFlightInfoOpen
                onPressed: {}
                onReleased: {
                    if(isFlightInfoOpen) helpBtnFocus()
                }
            }
        }
    }
    property QtObject headerBtnsobj
    Component{
        id:compHeaderBtns
        FocusScope{
            id:btn_Fs
            anchors.fill: parent
            property alias helpIcon: helpIcon
            property alias flightInfoTab: flightInfoTab
            property alias airplaneIcon: airplaneIcon
            property alias settingBtn: settingBtn
            property alias sourceATA: sourceATA
            property alias destinationATA: destinationATA
            property alias chat_msg_Icon: chat_msg_Icon
            Item{
                width:core.width
                height:qsTranslate('','header_header_div_tm1')
                clip: true
                Image{
                    anchors.top:parent.top
                    opacity:1;//isFlightInfoOpen?1:0;
                    source:viewHelper.configToolImagePath+configTool.config[configMenu].main.bg

                }
                Image{
                    id:videoIMG
                    anchors.top:parent.top
                    opacity:0;
                    source:viewHelper.configToolImagePath+configTool.config.global.video.vod_header
                    Rectangle{
                        color:core.configTool.config.popup.generic["bg_color"];
                        anchors.fill:parent;
                    }
                }
                Image{
                    visible:false// widgetList.getVideoRef().visible && !isFlightInfoOpen
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','video_logo_tm')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','video_logo_lm')
                    source:viewHelper.configToolImagePath+configHeaderTag.vod_logo
                }
            }
            Image{
                id:headerdiv
                anchors.horizontalCenter:  parent.horizontalCenter
                anchors.top:parent.top
                anchors.topMargin: /*isFlightInfoOpen?qsTranslate('','header_header_div_tm2'):*/qsTranslate('','header_header_div_tm1')
                source: viewController.settings.assetImagePath+qsTranslate('','header_header_div_source')
                Behavior on anchors.topMargin{
                    NumberAnimation{duration: 500}
                }
            }
            ViewText{
                id:sourceATA
                anchors.right: leftCircle.left
                anchors.rightMargin: qsTranslate('','header_flightinfo_gap')
                anchors.verticalCenter: travellingImg.verticalCenter
                width: qsTranslate('','header_flightinfo_textw')
                horizontalAlignment: Text.AlignRight
                text:pif.getSourceIATA()
                varText: [text,configHeaderTag.flight_text_color,qsTranslate('','header_flightinfo_fontsize')]
            }
            Image{
                id:leftCircle
                anchors.verticalCenter: travellingImg.verticalCenter
                anchors.right: travellingImg.left
                source:  viewHelper.configToolImagePath+configHeaderTag.flight_path_circle_image_h
            }
            Image{
                id:travellingImg
                width:(chat_msg_Icon.visible)?qsTranslate('','header_flightinfo_w_chat'):qsTranslate('','header_flightinfo_w')
                anchors.left:parent.left
                anchors.leftMargin:qsTranslate('','header_flightinfo_lm')
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','header_flightinfo_tm')
                source: viewHelper.configToolImagePath+configHeaderTag.flight_path_base_image
                Rectangle{
                    id:planeFillColor
                    anchors.left: travellingImg.left
                    width: distanceCovered
                    height: parent.height
                    color: configHeaderTag.flight_path_fill_color
                }
                Image{
                    id:airplaneIcon
                    anchors.verticalCenter:  parent.verticalCenter
                    anchors.left: planeFillColor.right
                    source: viewHelper.configToolImagePath+configHeaderTag.airplane_icon
                }
            }
            Image{
                id:rightCircle
                anchors.verticalCenter: travellingImg.verticalCenter
                anchors.left: travellingImg.right
                source:  viewHelper.configToolImagePath+(widgetList.getVideoRef().visible?(widgetList.getisFlightInfoOpen()?configHeaderTag.flight_path_circle_image_n:configHeaderTag.flight_path_circle_image_n):configHeaderTag.flight_path_circle_image_n)
            }
            ViewText{
                id:destinationATA
                anchors.left: rightCircle.right
                anchors.leftMargin: qsTranslate('','header_flightinfo_gap')
                anchors.verticalCenter: travellingImg.verticalCenter
                width: qsTranslate('','header_flightinfo_textw')
                text:pif.getDestinationIATA()
                varText: [text,configHeaderTag.flight_text_color,qsTranslate('','header_flightinfo_fontsize')]
            }
            SimpleBorderBtn {
                id: flightInfoTabHighlight
                property bool isEven: flightInfoTabHighlight.x%2==0?true:false
                normalImg: viewHelper.configToolImagePath+configHeaderTag.header_button_n
                highlightImg:  viewHelper.configToolImagePath+(widgetList.isVideoVisible() && !isFlightInfoOpen ?(flightInfoTabHighlight.isEven?configHeaderTag.header_button_h:configHeaderTag.header_button_h):configHeaderTag.header_button_h)
                pressedImg:  viewHelper.configToolImagePath+(widgetList.isVideoVisible()  && !isFlightInfoOpen ?(flightInfoTabHighlight.isEven?configHeaderTag.header_button_p:configHeaderTag.header_button_p):configHeaderTag.header_button_p)
                isHighlight: flightInfoTab.activeFocus
                width:/*(isFlightInfoOpen)?core.width:*/(chat_msg_Icon.visible)?qsTranslate('','header_flightinfo_total_w_chat'):qsTranslate('','header_flightinfo_total_w')
                height: flightInfoTab.activeFocus?flightInfoTabHighlight.sourceSize.height:0
                anchors.bottom:headerdiv.top
                anchors.horizontalCenter:travellingImg.horizontalCenter
                opacity: !flightInfoTab.isHighlightDelayed?1:0
                isHorizantalTileReq:true
                onEnteredReleased:flightInfoTab.forceActiveFocus()
                Behavior on opacity{
                    NumberAnimation{duration: !flightInfoTab.isHighlightDelayed?500:0}
                    //                    ScriptAction{script: {flightInfoTab.isHighlightDelayed=false}
                    //                    }
                }
            }


            SimpleNavButton{
                property bool  isHighlightDelayed:false
                property bool  isMousePressed:false
                id:flightInfoTab
                focus:true
                normImg:viewHelper.configToolImagePath+(isFlightInfoOpen?(configHeaderTag.flight_info_close_icon): (widgetList.isVideoVisible()?configHeaderTag.flight_info_open_icon:configHeaderTag.flight_info_open_icon))
                highlightimg: viewHelper.configToolImagePath+(isFlightInfoOpen?configHeaderTag.flight_info_close_icon:(widgetList.isVideoVisible()?configHeaderTag.flight_info_open_icon:configHeaderTag.flight_info_open_icon))
                pressedImg:isHighlight?highlightimg: normImg
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom:headerdiv.top
                anchors.bottomMargin: qsTranslate('','header_dropdown_btn_bm')
                increaseTouchArea:true
                increaseValue:width*2
                isHighlight: activeFocus && isFlightInfoOpen?true:false
                onActionPressed: {
                    if(isFlightInfoOpen && flightInfoTab.isPressed)  flightInfoTab.isMousePressed=true
                }
                onEnteredReleased: {
                    viewHelper.blockKeysForSeconds(0.6);
                    sourceTriggered='flightInfo';
                    if(isFlightInfoOpen){
                        //openFlightInfo(false)
                        helpBtnFocus();
                        //                        if(!flightInfoTab.isMousePressed) helpBtnFocus()
                        flightInfoTab.isMousePressed=false
                        return;
                    }
                    flightInfoTab.isMousePressed=false
                    openFlightInfo(true)
                }
                onNoItemFound: {
                    restartVideoTimer()
                    if(dir=="down"){
                        if(!isFlightInfoOpen){downPressed()}
                        else{flightInfoobj.forceActiveFocus();}}
                    //else if(isFlightInfoOpen && dir=="up")flightInfoobj.forceActiveFocus()
                    else  if(dir=="right"){
                        if(chat_msg_Icon.visible){
                            chat_msg_Icon.forceActiveFocus()
                        }else{
                            helpIcon.forceActiveFocus()
                        }
                    }
                }
                onPadReleased:restartVideoTimer() ;
            }
            SimpleNavBrdrBtn{
                id:chat_msg_Icon
                anchors.left:parent.left
                anchors.top:parent.top
                anchors.leftMargin:qsTranslate('','header_chat_notify_lm')
                anchors.topMargin: qsTranslate('','header_chat_notify_tm')
                isHighlight: activeFocus
                //                    increaseTouchArea:true
                //                    increaseValue:20
                btnTextWidth:qsTranslate('','header_chat_notify_text_w');
                normalImg: viewHelper.configToolImagePath+configHeaderTag["chat_notify_n"]//(widgetList.isVideoVisible()?configHeaderTag.chat_notify_n:configHeaderTag.chat_notify_n)
                highlightImg: viewHelper.configToolImagePath+configHeaderTag["chat_notify_h"]//(widgetList.isVideoVisible()?configHeaderTag.chat_notify_h:configHeaderTag.chat_notify_h)
                pressedImg: viewHelper.configToolImagePath+configHeaderTag["chat_notify_p"]//(widgetList.isVideoVisible()?configHeaderTag.chat_notify_p:configHeaderTag.chat_notify_p)
                nav:[flightInfoTab,"",helpIcon,(isFlightInfoOpen)?flightInfoobj:""]
                buttonText: [pif.seatChat.totalPrivateSessionUnReadMessages,configHeaderTag["chat_notify_text_color"]]
                visible:(pif.seatChat.totalPrivateSessionUnReadMessages>0)?true:false
                onEnteredReleased:{}
                onNoItemFound: {
                    restartVideoTimer()
                    if(dir=="down") downPressed()
                }

            }
            SimpleNavButton{
                id:helpIcon
                anchors.left:parent.left
                anchors.top:parent.top
                anchors.leftMargin: qsTranslate('','header_help_lm')
                anchors.topMargin: qsTranslate('','header_help_tm')
                isHighlight: activeFocus
                increaseTouchArea:true
                increaseValue:20
                normImg: viewHelper.configToolImagePath+(widgetList.isVideoVisible()?configHeaderTag.help_button_n:configHeaderTag.help_button_n)
                highlightimg: viewHelper.configToolImagePath+(widgetList.isVideoVisible()?configHeaderTag.help_button_h:configHeaderTag.help_button_h)
                pressedImg: viewHelper.configToolImagePath+(widgetList.isVideoVisible()?configHeaderTag.help_button_p:configHeaderTag.help_button_p)
                nav:[(chat_msg_Icon.visible)?chat_msg_Icon:flightInfoTab,"",kidsBtn,(isFlightInfoOpen)?flightInfoobj:""]
                onEnteredReleased:  showHelp()
                onNoItemFound: {
                    restartVideoTimer()
                    if(dir=="down") downPressed()
                }
                onPadReleased: restartVideoTimer() ;
            }
            SimpleNavBrdrBtn{
                id:kidsBtn
                property bool isEven: kidsBtn.x%2==0?true:false
                anchors.right:settingBtn.left
                anchors.rightMargin: qsTranslate('','header_lang_gap')
                isExternalWidth:true
                isHorizantalTileReq:true
                increaseTouchArea:true
                increaseValue:-10
                btnTextWidth:width;
                //btnTextWidth: btnText.paintedWidth+(parseInt(qsTranslate('','common_border_margin'),10) *2 )
                width:parseInt(qsTranslate('','header_lang_w'),10)
                height: qsTranslate('','header_lang_h')
                normalImg: viewHelper.configToolImagePath+configHeaderTag.header_button_n
                highlightImg:  viewHelper.configToolImagePath+(widgetList.isVideoVisible()  && !isFlightInfoOpen?(kidsBtn.isEven?configHeaderTag.header_button_h:configHeaderTag.header_button_h):configHeaderTag.header_button_h)
                pressedImg:  viewHelper.configToolImagePath+(widgetList.isVideoVisible()  && !isFlightInfoOpen?(kidsBtn.isEven?configHeaderTag.header_button_p:configHeaderTag.header_button_p):configHeaderTag.header_button_p)
                buttonText: [viewHelper.isKids(current.template_id)?configTool.language.global_elements.button_mainmenu_text:configTool.language.global_elements.button_kids_text,
                                                                     kidsBtn.activeFocus?configHeaderTag.text_color_h:(isPressed?configHeaderTag.text_color_p:configHeaderTag.text_color_n),qsTranslate('','header_lang_fontsize')]
                isHighlight: activeFocus
                nav:[helpIcon,"",settingBtn,(isFlightInfoOpen)?flightInfoobj:""]
                onEnteredReleased: {
                    if(viewHelper.isBlock())return;
                    viewHelper.blockKeysForSeconds(2)
                    sourceTriggered='settings';
                    if(isFlightInfoOpen)isFlightInfoOpen=false
                    stopVODTimer(true)
                    if(widgetList.isVideoVisible()){
                        widgetList.getVideoRef().bckTakeFocus=false
                        widgetList.getVideoRef().hideonlyHeader.restart();
                    }
                    if(viewHelper.isKids(current.template_id)){
                        var ind = core.coreHelper.searchEntryInModel(viewData.homeModel,"category_attr_template_id","adult")
                    }else{
                        var ind = core.coreHelper.searchEntryInModel(viewData.homeModel,"category_attr_template_id","kids")
                    }
                    viewHelper.switchSection(ind);

                }
                onNoItemFound: {
                    restartVideoTimer()
                    if(dir=="down") downPressed()
                }
                onPadReleased: restartVideoTimer() ;
            }
            Image{
                anchors.right: parent.right
                anchors.rightMargin: qsTranslate('','header_btn_div_rm')
                source: viewController.settings.assetImagePath+qsTranslate('','header_btn_div_source')
            }
            SimpleNavBrdrBtn{
                id:settingBtn
                property bool isEven: settingBtn.x%2==0?true:false
                anchors.right:parent.right
                anchors.rightMargin: qsTranslate('','header_lang_rm')
                isExternalWidth:true
                isHorizantalTileReq:true
                increaseTouchArea:true
                increaseValue:-10
                btnTextWidth:width; /*btnText.paintedWidth+(parseInt(qsTranslate('','common_border_margin'),10)*2)*/
                width:parseInt(qsTranslate('','header_lang_w'),10)
                height: qsTranslate('','header_lang_h')
                normalImg: viewHelper.configToolImagePath+configHeaderTag.header_button_n
                highlightImg:  viewHelper.configToolImagePath+(widgetList.isVideoVisible()  && !isFlightInfoOpen?(settingBtn.isEven?configHeaderTag.header_button_h:configHeaderTag.header_button_h):configHeaderTag.header_button_h)
                pressedImg:  viewHelper.configToolImagePath+(widgetList.isVideoVisible()  && !isFlightInfoOpen?(settingBtn.isEven?configHeaderTag.header_button_p:configHeaderTag.header_button_p):configHeaderTag.header_button_p)
                buttonText: [configTool.language.global_elements.button_settings_text,settingBtn.activeFocus?configHeaderTag.text_color_h:(isPressed?configHeaderTag.text_color_p:configHeaderTag.text_color_n),qsTranslate('','header_lang_fontsize')]
                isHighlight: activeFocus
                nav:[kidsBtn,"","",(isFlightInfoOpen)?flightInfoobj:""]
                onEnteredReleased: {
                    viewHelper.blockKeysForSeconds(0.6);
                    if(isFlightInfoOpen && flightInfoobj.flightInfoLoader.sourceComponent!=settings)sourceTriggered='settings'
                    else sourceTriggered='flightInfo'
                    if(flightInfoobj.flightInfoLoader.sourceComponent==settings && isFlightInfoOpen){
                        isFlightInfoOpen=false;
                        return;
                    }

                    openFlightInfo(false)
                }
                onNoItemFound: {
                    restartVideoTimer()
                    if(dir=="down")downPressed()
                }
                onPadReleased: restartVideoTimer() ;
            }
        }
    }
    property QtObject flightInfoobj
    Component{
        id:compFlightInfo
        Loader{
            id:flightInfoLoader
            property alias flightInfoLoader: flightInfoLoader
            property alias closeAnim: closeAnim
            width:parent.width;
            height:0;

            SequentialAnimation{
                id:openAnim;
                PropertyAnimation{target:flightInfoLoader;from:0; to:(flightInfoobj.flightInfoLoader.sourceComponent==settings)?(viewHelper.seatchat_enabled)?parseInt(qsTranslate('','header_mask_seatchat_h'),10)+parseInt(qsTranslate('','header_mask_tm'),10):parseInt(qsTranslate('','header_mask_h'),10)+parseInt(qsTranslate('','header_mask_tm'),10):parseInt(qsTranslate('','header_mask_h'),10)+parseInt(qsTranslate('','header_mask_tm'),10);property:"height";duration:450;}
            }

            SequentialAnimation{
                id:closeAnim;
                PropertyAnimation{target:flightInfoLoader;to:0;property:"height";duration:450;}
                ScriptAction{
                    script:{
                        flightInfoLoader.sourceComponent=blankComp
                    }
                }
            }

            sourceComponent: blankComp;
            onStatusChanged: {
                if(sourceComponent==blankComp)return;
                if (flightInfoLoader.status == Loader.Ready){
                    flightInfoLoader.item.forceActiveFocus();
                    openAnim.restart();

                }
            }
        }
    }
    Component{
        id:blankComp
        Item{}
    }
    Component{
        id:settings
        FocusScope{
            id:setting
            //            anchors.top:parent.top
            //            anchors.left: parent.left
            property alias mask:mask;
            width: core.width

            height:parent.height;//mask.sourceSize.height+parseInt(qsTranslate('','header_mask_tm'),10);
            clip: true
            property alias languageLV: languageLV
            property alias volumeSliderComp: volumeSliderComp
            property alias brightnessSliderComp: brightnessSliderComp

            MouseArea{anchors.fill:parent;onClicked:{}}
            Image{
                id:mask
                anchors.top:parent.top
                anchors.topMargin:parseInt(qsTranslate('','settings_mask_tm'),10)
                source: viewController.settings.assetImagePath+qsTranslate('','settings_mask_source')
                clip:true
            }
            Image{
                anchors.top:mask.top
                anchors.topMargin:-parseInt(qsTranslate('','header_mask_tm'),10)
                source:viewHelper.configToolImagePath+configTool.config[configMenu].main.bg
                asynchronous: false
            }


            Image{
                id:chat_settings
                width:parseInt(qsTranslate('','settings_chat_w'),10)
                anchors.horizontalCenter: mask.horizontalCenter
                anchors.top:mask.top
                anchors.topMargin:parseInt(qsTranslate('','settings_chat_tm'),10)
                source:viewController.settings.assetImagePath+"chat_tab.png"
                asynchronous: false
                visible:viewHelper.seatchat_enabled

                ViewText{
                    id:chat_tilte
                    width:qsTranslate('','settings_chattitle_txt_w')
                    //paintedWidth>qsTranslate('','settings_chattitle_txt_w')?paintedWidth:qsTranslate('','settings_chattitle_txt_w')
                    height: qsTranslate('','settings_chattitle_txt_h')
                    anchors.left:parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment:Text.AlignVCenter
                    maximumLineCount: 2
                    wrapMode: Text.Wrap
                    anchors.leftMargin:  qsTranslate('','settings_chattitle_txt_lm')
                    varText: [configTool.language.settings.chattitle_txt,configTabSettings.text_color,qsTranslate('','settings_chattitle_txt_fontsize')]

                }
                ViewText{
                    id:chat_disable_txt
                    width: qsTranslate('','settings_disblemsg_txt_w')
                    height:qsTranslate('','settings_disblemsg_txt_h')
                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment:Text.AlignVCenter
                    anchors.left: chat_tilte.right
                    anchors.leftMargin:  qsTranslate('','settings_disblemsg_txt_lm')
                    maximumLineCount: 2
                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                    varText: [(viewHelper.getBlockInvitationStatus()?configTool.language.settings.chat_enablemsg_txt:configTool.language.settings.chat_disablemsg_txt),configTabSettings.text_color,qsTranslate('','settings_disblemsg_txt_fontsize')]
                }

                SimpleNavBrdrBtn{
                    id:chat_enable_disable_btn
                    anchors.right:parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.rightMargin: qsTranslate('','settings_disable_btn_rm')
                    btnTextWidth:qsTranslate('','settings_disable_btn_txtw')
                    width:qsTranslate('','settings_disable_btn_w')
                    height: qsTranslate('','settings_disable_btn_h')
                    normalImg: viewHelper.configToolImagePath+configTabSettings.disable_btn_n
                    highlightImg:  viewHelper.configToolImagePath+configTabSettings.disable_btn_h
                    pressedImg:  viewHelper.configToolImagePath+configTabSettings.disable_btn_p
                    buttonText: [(viewHelper.getBlockInvitationStatus()?configTool.language.settings.chat_enable_txt:configTool.language.settings.chat_disable_txt),chat_enable_disable_btn.activeFocus?configTabSettings.disable_text_h:(isPressed?configTabSettings.disable_text_p:configTabSettings.disable_text_n),qsTranslate('','settings_disable_btn_fontsize')]
                    isHighlight: activeFocus
                    nav:["",volBriFS,"",""]
                    onEnteredReleased: {
                        viewHelper.setBlockInvitationStatus(!viewHelper.getBlockInvitationStatus());
                        //                            if(viewHelper.getBlockInvitationStatus()){
                        //                                //pif.seatChat.exitChat();
                        //                            }else{
                        //                                core.info("Header.qml | chat_enable_disable_btn ")
                        //                                pif.seatChat.setAppActive();
                        //                            }
                    }
                    //                        onNoItemFound: {
                    //                            restartVideoTimer()
                    //                            if(dir=="down") downPressed()
                    //                        }
                }


            }
            Image{
                id:language
                anchors.left: mask.left
                anchors.top: mask.top
                anchors.topMargin: qsTranslate('','settings_tab_tm')
                anchors.leftMargin:qsTranslate('','settings_tab_gap')
                source: viewController.settings.assetImagePath+qsTranslate('','settings_lang_source')
                ViewText{
                    width: qsTranslate('','settings_lang_w')
                    anchors.top:parent.top
                    anchors.left: parent.left
                    anchors.topMargin:  qsTranslate('','settings_lang_tm')
                    anchors.leftMargin:  qsTranslate('','settings_lang_lm')
                    varText: [configTool.language.settings.change_language,configTabSettings.text_color,qsTranslate('','settings_lang_fontsize')]
                }
            }
            Image{
                id:volBri
                anchors.top: mask.top
                anchors.topMargin: qsTranslate('','settings_tab_tm')
                anchors.horizontalCenter:  mask.horizontalCenter
                source: viewController.settings.assetImagePath+qsTranslate('','settings_vol_source')
            }
            Image{
                id:screenOFF
                anchors.top: mask.top
                anchors.topMargin: qsTranslate('','settings_tab_tm')
                anchors.right: mask.right
                anchors.rightMargin: qsTranslate('','settings_tab_gap')
                source: viewController.settings.assetImagePath+qsTranslate('','settings_vol_source')
            }
            FocusScope{
                id:languageFS
                anchors.fill: language
                FocusScope{
                    id:languageBtns
                    anchors.top:parent.top
                    anchors.topMargin:  qsTranslate('','settings_lang_area_tm')
                    anchors.horizontalCenter: parent.horizontalCenter
                    width:  qsTranslate('','settings_lang_area_w')
                    height: qsTranslate('','settings_lang_area_h')
                    focus: true

                    //                    ListView{
                    //                        id:languageLV
                    //                        anchors.top:parent.top
                    //                        anchors.horizontalCenter: parent.horizontalCenter
                    //                        width:  qsTranslate('','settings_lang_area_w')
                    //                        height: parent.height
                    //                        interactive: false
                    //                        clip: true
                    //                        orientation: ListView.Horizontal
                    //                        model:viewHelper.languageModel
                    //                        highlightRangeMode:ListView.StrictlyEnforceRange
                    //                        currentIndex: viewHelper.languageIndex

                    //                    }
                    Path{
                        id:menupath
                        //first point
                        startX: 0
                        startY: languageLV.height/2
                        //                        PathAttribute{name: 'pathOpacity'; value:1/*qsTranslate('','Language_container_opacity')*/}
                        //                        PathAttribute{name: 'fontSize'; value:qsTranslate('','settings_lang_area_fontsize')}
                        //second point
                        PathLine{x:languageLV.width; y:languageLV.height*.5}
                        PathAttribute{name: 'pathOpacity'; value:1 }
                        //                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                        //third point
                        //                        PathLine{x:languageLV.width/2; y:languageLV.height}
                        //                        PathAttribute{name: 'pathOpacity'; value: 1/*qsTranslate('','Language_container_opacity')*/}
                        //                        PathAttribute{name: 'fontSize'; value: qsTranslate('','settings_lang_area_fontsize')}
                    }
                    PathView{
                        id:languageLV
                        focus: true
                        anchors.top:parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        width:  qsTranslate('','settings_lang_area_w')
                        height: qsTranslate('','settings_lang_area_h')
                        interactive: false
                        clip: true
                        path:menupath;
                        highlightRangeMode:PathView.StrictlyEnforceRange
                        preferredHighlightBegin:.5
                        preferredHighlightEnd: .5
                        highlightMoveDuration: 600
                        pathItemCount:1//1(model.count<5)?3:5;
                        model:viewHelper.languageModel
                        currentIndex: viewHelper.languageIndex
                        delegate:
                            ViewText{
                            width:qsTranslate('','settings_lang_area_w')
                            height:qsTranslate('','settings_lang_area_h')
                            horizontalAlignment: Text.AlignHCenter
                            varText: [name, viewHelper.languageIndex==index?configTabSettings.language_text_h:configTabSettings.language_text_n, qsTranslate('','settings_lang_area_fontsize')]
                        }
                    }
                    //                    }
                    SimpleNavButton{
                        id:left_arrow
                        anchors.right: languageLV.left
                        anchors.verticalCenter: languageLV.verticalCenter
                        isHighlight: activeFocus
                        //  isDisable:languageLV.currentIndex==0?true:false
                        normImg:  viewHelper.configToolImagePath+configTabSettings.left_arrow_n
                        highlightimg: viewHelper.configToolImagePath+configTabSettings.left_arrow_h
                        selectedImg: viewHelper.configToolImagePath+configTabSettings.left_arrow_p
                        pressedImg: viewHelper.configToolImagePath+configTabSettings.left_arrow_p
                        nav:["","",!right_arrow.isDisable?right_arrow:volBriFS,acceptBTN]
                        onNoItemFound: if(dir=="up")helpBtnFocus()
                        onEnteredReleased: languageLV.decrementCurrentIndex()
                        //onIsDisableChanged:  if(isDisable && languageBtns.focus)right_arrow.forceActiveFocus()
                    }
                    SimpleNavButton{
                        id:right_arrow
                        focus: true
                        anchors.left: languageLV.right
                        anchors.verticalCenter: languageLV.verticalCenter
                        isHighlight: activeFocus
                        //                        isDisable:languageLV.currentIndex==(languageLV.count-1)?true:false
                        normImg:  viewHelper.configToolImagePath+configTabSettings.right_arrow_n
                        highlightimg: viewHelper.configToolImagePath+configTabSettings.right_arrow_h
                        selectedImg: viewHelper.configToolImagePath+configTabSettings.right_arrow_p
                        pressedImg: viewHelper.configToolImagePath+configTabSettings.right_arrow_p
                        nav:[!left_arrow.isDisable?left_arrow:"","",volBriFS,acceptBTN]
                        onNoItemFound: if(dir=="up")helpBtnFocus()
                        onEnteredReleased: {
                            languageLV.incrementCurrentIndex()
                            //                            if(languageLV.currentIndex==(languageLV.count-1)){
                            //                                languageLV.positionViewAtIndex(0,ListView.Beginning)
                            //                                languageLV.currentIndex=0;
                            //                            }else{
                            //                            }

                        }
                        //                        onIsDisableChanged:  if(isDisable && languageBtns.focus)left_arrow.forceActiveFocus()
                    }
                }
                SimpleNavButton{
                    id:acceptBTN
                    property string textColor: acceptBTN.activeFocus?configTabSettings.accept_text_h:(acceptBTN.isPressed?configTabSettings.accept_text_p:configTabSettings.accept_text_n)
                    anchors.left:parent.left
                    anchors.leftMargin: qsTranslate('','settings_lang_btn_lm')
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','settings_lang_btn_tm')
                    normImg: viewHelper.configToolImagePath+configTabSettings.accept_btn_n
                    highlightimg: viewHelper.configToolImagePath+configTabSettings.accept_btn_h
                    pressedImg: viewHelper.configToolImagePath+configTabSettings.accept_btn_p
                    nav: ["",languageBtns,volBriFS,(chat_enable_disable_btn.visible)?chat_enable_disable_btn:""]
                    onEnteredReleased: changeLanguage()
                    ViewText{
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        varText: [configTool.language.movies.accept,acceptBTN.textColor,qsTranslate('','settings_lang_btn_fontsize')]
                    }
                }
            }
            FocusScope{
                id:volBriFS
                anchors.fill: volBri
                focus: true
                FocusScope{
                    id:volFS
                    focus: true
                    width: qsTranslate('','settings_vol_w')
                    height:  qsTranslate('','settings_vol_h')
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','settings_vol_tm')
                    anchors.horizontalCenter: parent.horizontalCenter
                    SimpleNavButton{
                        id:volumeMinus
                        anchors.verticalCenter: volumeSliderComp.verticalCenter
                        anchors.right: volumeSliderComp.left
                        isHighlight: activeFocus
                        normImg:  viewHelper.configToolImagePath+configTabSettings.volume_dcr_n
                        highlightimg:  viewHelper.configToolImagePath+configTabSettings.volume_dcr_h
                        pressedImg: viewHelper.configToolImagePath+configTabSettings.volume_dcr_p
                        nav:[languageFS,"",volumePlus,briFS]
                        onNoItemFound: if(dir=="up")helpBtnFocus()
                        onEnteredReleased: {volumeSliderComp.prevPage()}
                    }
                    Slider{
                        id:volumeSliderComp
                        anchors.centerIn: parent
                        height: qsTranslate('','settings_vol_base_margin')
                        width:  qsTranslate('','settings_vol_base_w')
                        sliderbg:configTabSettings.base_image
                        sliderFillingtItems: [configTabSettings.base_fill,configTabSettings.indicator]
                        stepsValue: pif.getVolumeStep()
                        isFill: true
                        maxLevel:100
                        onValueChanged: {
                            var volumeLevel=pif.getVolumeLevel()
                            var remainder=(volumeLevel%stepsValue)
                            if(value=="drag") {
                                pif.setVolumeByValue(shiftedValue)
                                return
                            }
                            if(remainder!=0){
                                var setValue =remainder>(stepsValue/2)?(volumeLevel - remainder)+stepsValue:volumeLevel-(remainder)
                                pif.setVolumeByValue(setValue)
                                return;
                            }
                            if(shiftedValue>volumeLevel)
                                pif.setVolumeByStep(1)
                            else if(shiftedValue<volumeLevel)
                                pif.setVolumeByStep(-1)
                        }
                    }
                    SimpleNavButton{
                        id:volumePlus
                        focus:true
                        anchors.verticalCenter: volumeSliderComp.verticalCenter
                        anchors.left: volumeSliderComp.right
                        isHighlight: activeFocus
                        normImg:  viewHelper.configToolImagePath+configTabSettings.volume_incr_n
                        highlightimg:  viewHelper.configToolImagePath+configTabSettings.volume_incr_h
                        pressedImg: viewHelper.configToolImagePath+configTabSettings.volume_incr_p
                        nav:[volumeMinus,"",(viewHelper.isAircraft787()?crewBttn:crewBtn),briFS]
                        onNoItemFound: if(dir=="up")helpBtnFocus()
                        onEnteredReleased: {volumeSliderComp.nextPage()}
                    }
                }
                FocusScope{
                    id:briFS
                    width: qsTranslate('','settings_vol_w')
                    height:  qsTranslate('','settings_vol_h')
                    anchors.top:volFS.bottom
                    anchors.topMargin: qsTranslate('','settings_vol_vgap')
                    anchors.horizontalCenter: parent.horizontalCenter
                    SimpleNavButton{
                        id:brightnessMinus
                        anchors.verticalCenter: brightnessSliderComp.verticalCenter
                        anchors.right: brightnessSliderComp.left
                        isHighlight: activeFocus
                        normImg:  viewHelper.configToolImagePath+configTabSettings.brightness_dcr_n
                        highlightimg:  viewHelper.configToolImagePath+configTabSettings.brightness_dcr_h
                        pressedImg: viewHelper.configToolImagePath+configTabSettings.brightness_dcr_p
                        nav:[languageFS,volFS,brightnessPlus,chat_enable_disable_btn.visible?chat_enable_disable_btn:""]
                        onEnteredReleased:{ pif.setBrightnessByStep(-1)}
                    }
                    Slider{
                        id:brightnessSliderComp
                        anchors.centerIn: parent
                        height: qsTranslate('','settings_vol_base_margin')
                        width:  qsTranslate('','settings_vol_base_w')
                        orientation: Qt.Horizontal
                        sliderbg:configTabSettings.base_image
                        sliderFillingtItems: [configTabSettings.base_fill,configTabSettings.indicator]
                        stepsValue: pif.getBrightnessStep()
                        isFill: true
                        maxLevel:100

                        onValueChanged: {
                            if(value>=maxLevel)value=maxLevel;

                            if(value=="drag") {
                                pif.setBrightnessByValue(shiftedValue)
                                return
                            }
                        }
                    }
                    SimpleNavButton{
                        id:brightnessPlus
                        focus:true
                        anchors.verticalCenter: brightnessSliderComp.verticalCenter
                        anchors.left: brightnessSliderComp.right
                        isHighlight: activeFocus
                        normImg:  viewHelper.configToolImagePath+configTabSettings.brightness_incr_n
                        highlightimg:  viewHelper.configToolImagePath+configTabSettings.brightness_incr_h
                        pressedImg: viewHelper.configToolImagePath+configTabSettings.brightness_incr_p
                        nav:[brightnessMinus,volFS,(viewHelper.isAircraft787()?readLightBtn:crewBtn),chat_enable_disable_btn.visible?chat_enable_disable_btn:""]
                        onEnteredReleased: {                                pif.setBrightnessByStep(1)

                        }
                    }
                }
            }

            FocusScope{
                id:non787Sett
                width: screenOFF.width;
                height:screenOFF.height;
                anchors.left: screenOFF.left;
                anchors.top: screenOFF.top;
                visible:(!viewHelper.isAircraft787());
                ViewText{
                    id:crewText
                    width: qsTranslate('','settings_crew_w')
                    height: qsTranslate('','settings_crew_h')
                    anchors.left:parent.left
                    anchors.leftMargin:     qsTranslate('','settings_crew_lm')
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','settings_crew_tm')
                    varText: [configTool.language.settings.call_crew,configTabSettings.text_color,qsTranslate('','settings_crew_fontsize')]
                    maximumLineCount: 2
                    lineHeightMode: Text.FixedHeight
                    lineHeight: qsTranslate('','settings_crew_lh')
                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                }
                ViewText{
                    id:readingtext
                    width: qsTranslate('','settings_crew_w')
                    height: qsTranslate('','settings_crew_h')
                    anchors.left:crewText.right
                    anchors.leftMargin:     qsTranslate('','settings_crew_hgap')
                    anchors.top:crewText.top
                    varText: [configTool.language.settings.reading_light,configTabSettings.text_color,qsTranslate('','settings_crew_fontsize')]
                    maximumLineCount: 2
                    lineHeightMode: Text.FixedHeight
                    lineHeight: qsTranslate('','settings_crew_lh')
                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                }
                ViewText{
                    id:screenoffText
                    width: qsTranslate('','settings_crew_w')
                    height: qsTranslate('','settings_crew_h')
                    anchors.left:readingtext.right
                    anchors.leftMargin:     qsTranslate('','settings_crew_hgap')
                    anchors.top:crewText.top
                    varText: [configTool.language.settings.screen_off,configTabSettings.text_color,qsTranslate('','settings_crew_fontsize')]
                    maximumLineCount: 2
                    lineHeightMode: Text.FixedHeight
                    lineHeight: qsTranslate('','settings_crew_lh')
                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                }
                ToggleNavButton{
                    id:crewBtn
                    anchors.top:crewText.bottom
                    anchors.left: crewText.left
                    anchors.topMargin: qsTranslate('','settings_crew_btn_tm')
                    anchors.leftMargin: qsTranslate('','settings_crew_btn_lm')
                    isHighlight: activeFocus
                    normImg2:  viewHelper.configToolImagePath+configTabSettings.call_crew_n
                    highImg2: viewHelper.configToolImagePath+configTabSettings.call_crew_h
                    pressImg2: viewHelper.configToolImagePath+configTabSettings.call_crew_p
                    normImg1:  viewHelper.configToolImagePath+configTabSettings.call_crewcancel_n
                    highImg1: viewHelper.configToolImagePath+configTabSettings.call_crewcancel_h
                    pressImg1: viewHelper.configToolImagePath+configTabSettings.call_crewcancel_p
                    nav: [volBriFS,"",readingBtn,chat_enable_disable_btn.visible?chat_enable_disable_btn:""]
                    cstate:pif.getAttendantCall()
                    onNoItemFound: if(dir=="up")helpBtnFocus()
                    onEnteredReleased:  {cstate=!pif.getAttendantCall();pif.setAttendantCall(!pif.getAttendantCall())}
                }
                ToggleNavButton{
                    id:readingBtn
                    anchors.top:readingtext.bottom
                    anchors.left: readingtext.left
                    anchors.topMargin: qsTranslate('','settings_crew_btn_tm')
                    anchors.leftMargin: qsTranslate('','settings_crew_btn_lm')
                    isHighlight: activeFocus
                    normImg2:  viewHelper.configToolImagePath+configTabSettings.reading_light_n
                    highImg2: viewHelper.configToolImagePath+configTabSettings.reading_light_h
                    pressImg2:viewHelper.configToolImagePath+configTabSettings.reading_light_p
                    normImg1:  viewHelper.configToolImagePath+configTabSettings.reading_lightoff_n
                    highImg1: viewHelper.configToolImagePath+configTabSettings.reading_lightoff_h
                    pressImg1:viewHelper.configToolImagePath+configTabSettings.reading_lightoff_p
                    cstate:pif.getReadingLight()


                    nav: [crewBtn,"",screenoffBtn,chat_enable_disable_btn.visible?chat_enable_disable_btn:""]
                    onNoItemFound: if(dir=="up")helpBtnFocus()
                    onEnteredReleased:{ cstate=!pif.getReadingLight();pif.setReadingLight(!pif.getReadingLight())}
                }
                SimpleNavButton{
                    id:screenoffBtn
                    anchors.top:screenoffText.bottom
                    anchors.left: screenoffText.left
                    anchors.topMargin: qsTranslate('','settings_crew_btn_tm')
                    anchors.leftMargin: qsTranslate('','settings_crew_btn_lm')
                    isHighlight: activeFocus
                    normImg:  viewHelper.configToolImagePath+configTabSettings.screen_off_n
                    highlightimg: viewHelper.configToolImagePath+configTabSettings.screen_off_h
                    pressedImg: viewHelper.configToolImagePath+configTabSettings.screen_off_p
                    nav: [readingBtn,"","",chat_enable_disable_btn.visible?chat_enable_disable_btn:""]
                    onNoItemFound: if(dir=="up")helpBtnFocus()
                    onEnteredReleased: {
                        if(isFlightInfoOpen) helpBtnFocus();
                        if(pif.getBacklightState()) pif.setBacklightOff();
                        else pif.setBacklightOn();
                    }
                }
            }
            FocusScope{
                id:sett787
                width: screenOFF.width;
                height:screenOFF.height;
                anchors.left: screenOFF.left;
                anchors.top: screenOFF.top;
                visible:(viewHelper.isAircraft787());

                Grid{
                    id:sett787Grid
                    width:parseInt(qsTranslate('','settings_crewgrid_w'),10) ;
                    height:parseInt(qsTranslate('','settings_crewgrid_h'),10) ;
                    columns: 2
                    anchors.top:parent.top;
                    anchors.topMargin:parseInt(qsTranslate('','settings_crewgrid_tm'),10) ;
                    layoutDirection: "LeftToRight"

                    property variant cellWidth:parseInt(qsTranslate('','settings_crewgridcell_w'),10) ;
                    property variant cellHeight:parseInt(qsTranslate('','settings_crewgridcell_h'),10) ;
                    property variant txtWidth:parseInt(qsTranslate('','settings_crewgridcell_txt_w'),10) ;
                    property variant txtHeight:parseInt(qsTranslate('','settings_crewgridcell_txt_h'),10) ;

                    Item{
                        id:crewCell
                        width:parent.cellWidth;
                        height:parent.cellHeight;

                        ToggleNavButton{
                            id:crewBttn
                            anchors.left: parent.left
                            anchors.leftMargin: parseInt(qsTranslate('','settings_crewgridcell_btn_lm'),10) ;
                            normImg2:  viewHelper.configToolImagePath+configTabSettings.call_crew1_n
                            highImg2: viewHelper.configToolImagePath+configTabSettings.call_crew1_h
                            pressImg2: viewHelper.configToolImagePath+configTabSettings.call_crew1_p
                            normImg1:  viewHelper.configToolImagePath+configTabSettings.call_crewcancel1_n
                            highImg1: viewHelper.configToolImagePath+configTabSettings.call_crewcancel1_h
                            pressImg1: viewHelper.configToolImagePath+configTabSettings.call_crewcancel1_p
                            nav: [volBriFS,"",scrnOffBtn,readLightBtn]
                            cstate:pif.getAttendantCall()
                            onNoItemFound: if(dir=="up")helpBtnFocus()
                            onEnteredReleased:  {cstate=!pif.getAttendantCall();pif.setAttendantCall(!pif.getAttendantCall())}
                        }
                        ViewText{
                            id:crewTxt
                            width: sett787Grid.txtWidth;
                            height: sett787Grid.txtHeight;
                            anchors.left: crewBttn.right
                            anchors.leftMargin:parseInt(qsTranslate('','settings_crewgridcell_hgap'),10);
                            anchors.verticalCenter: parent.verticalCenter
                            varText: [configTool.language.settings.call_crew,configTabSettings.text_color,qsTranslate('','settings_crewgridcell_fontsize')]
                            maximumLineCount:2
                            lineHeightMode: Text.FixedHeight
                            lineHeight: qsTranslate('','settings_crewgridcell_lh')
                            wrapMode: Text.WordWrap
                            elide: Text.ElideRight
                        }
                    }
                    Item{
                        id:scrOffCell
                        width:parent.cellWidth;
                        height:parent.cellHeight;
                        SimpleNavButton{
                            id:scrnOffBtn
                            anchors.left: parent.left
                            anchors.leftMargin: parseInt(qsTranslate('','settings_crewgridcell_btn_lm'),10) ;
                            isHighlight: activeFocus
                            normImg:  viewHelper.configToolImagePath+configTabSettings.screen_off1_n
                            highlightimg: viewHelper.configToolImagePath+configTabSettings.screen_off1_h
                            pressedImg: viewHelper.configToolImagePath+configTabSettings.screen_off1_p
                            nav: [crewBttn,"","",winDimmBtn]
                            onNoItemFound: if(dir=="up")helpBtnFocus()
                            onEnteredReleased: {
                                if(isFlightInfoOpen) helpBtnFocus();
                                if(pif.getBacklightState()) pif.setBacklightOff();
                                else pif.setBacklightOn();
                            }
                        }
                        ViewText{
                            id:scrnOffText
                            width:sett787Grid.txtWidth;
                            height:sett787Grid.txtHeight;
                            anchors.left:scrnOffBtn.right
                            anchors.leftMargin: parseInt(qsTranslate('','settings_crewgridcell_hgap'),10);
                            anchors.verticalCenter: parent.verticalCenter
                            varText: [configTool.language.settings.screen_off,configTabSettings.text_color,qsTranslate('','settings_crewgridcell_fontsize')]
                            maximumLineCount: 2
                            lineHeightMode: Text.FixedHeight
                            lineHeight: qsTranslate('','settings_crewgridcell_lh')
                            wrapMode: Text.WordWrap
                            elide: Text.ElideRight
                        }
                    }

                    Item{
                        id:readLightCell
                        width:parent.cellWidth;
                        height:parent.cellHeight;

                        ToggleNavButton{
                            id:readLightBtn
                            anchors.left: parent.left;
                            anchors.leftMargin: parseInt(qsTranslate('','settings_crewgridcell_btn_lm'),10) ;
                            isHighlight: activeFocus
                            normImg2:  viewHelper.configToolImagePath+configTabSettings.reading_light1_n
                            highImg2: viewHelper.configToolImagePath+configTabSettings.reading_light1_h
                            pressImg2:viewHelper.configToolImagePath+configTabSettings.reading_light1_p
                            normImg1:  viewHelper.configToolImagePath+configTabSettings.reading_lightoff1_n
                            highImg1: viewHelper.configToolImagePath+configTabSettings.reading_lightoff1_h
                            pressImg1:viewHelper.configToolImagePath+configTabSettings.reading_lightoff1_p
                            cstate:pif.getReadingLight()
                            nav: [volBriFS,crewBttn,winDimmBtn,chat_enable_disable_btn.visible?chat_enable_disable_btn:""]
                            onNoItemFound: if(dir=="up")helpBtnFocus()
                            onEnteredReleased:{ cstate=!pif.getReadingLight();pif.setReadingLight(!pif.getReadingLight())}
                        }
                        ViewText{
                            id:readLightTxt
                            width:sett787Grid.txtWidth
                            height:sett787Grid.txtHeight
                            anchors.left:readLightBtn.right
                            anchors.leftMargin:parseInt(qsTranslate('','settings_crewgridcell_hgap'),10);
                            anchors.verticalCenter: parent.verticalCenter
                            varText: [configTool.language.settings.reading_light,configTabSettings.text_color,qsTranslate('','settings_crewgridcell_fontsize')]
                            maximumLineCount: 2
                            lineHeightMode: Text.FixedHeight
                            lineHeight: qsTranslate('','settings_crewgridcell_lh')
                            wrapMode: Text.WordWrap
                            elide: Text.ElideRight
                        }
                    }

                    Item{
                        id:winDimmCell
                        width:parent.cellWidth;
                        height:parent.cellHeight;

                        SimpleNavButton{
                            id:winDimmBtn
                            anchors.left: parent.left
                            anchors.leftMargin: parseInt(qsTranslate('','settings_crewgridcell_btn_lm'),10) ;
                            isHighlight: activeFocus
                            normImg:  viewHelper.configToolImagePath+configTabSettings.window_dim1_n
                            highlightimg: viewHelper.configToolImagePath+configTabSettings.window_dim1_h
                            pressedImg: viewHelper.configToolImagePath+configTabSettings.window_dim1_p
                            nav: [readLightBtn,scrnOffBtn,"",chat_enable_disable_btn]
                            onNoItemFound: if(dir=="up")helpBtnFocus()
                            onEnteredReleased: {
                                if(isFlightInfoOpen)isFlightInfoOpen=false;
                                viewController.showScreenPopup(13);
                            }
                        }
                        ViewText{
                            id:winDimmTxt
                            width:sett787Grid.txtWidth
                            height:sett787Grid.txtHeight
                            anchors.left:winDimmBtn.right
                            anchors.leftMargin:parseInt(qsTranslate('','settings_crewgridcell_hgap'),10);
                            anchors.verticalCenter: parent.verticalCenter
                            varText: [configTool.language.settings.windowdimmable_button,configTabSettings.text_color,qsTranslate('','settings_crewgridcell_fontsize')]
                            maximumLineCount: 2
                            lineHeightMode: Text.FixedHeight
                            lineHeight: qsTranslate('','settings_crewgridcell_lh')
                            wrapMode: Text.WordWrap
                            elide: Text.ElideRight
                        }
                    }
                }
                Image{
                    id:gridDiv
                    anchors.horizontalCenter: parent.horizontalCenter;
                    anchors.verticalCenter: parent.verticalCenter;
                    source:viewHelper.configToolImagePath+configTabSettings.crew_vdivider
                }
            }
            Component.onCompleted: {
                core.debug(" pif.getBrightnessLevel() : "+pif.getBrightnessLevel())
                volumeSliderComp.setInitailValue=pif.getVolumeLevel()
                brightnessSliderComp.setInitailValue=pif.getBrightnessLevel()
                volumePlus.forceActiveFocus()
            }
        }
    }
    Component{
        id:flightInfo
        FocusScope{
            width: parent.width;
            property alias mask:mask;
            height:parent.height;
            clip: true
            MouseArea{anchors.fill:parent;onClicked:{}}
            Image{
                id:mask
                anchors.top:parent.top
                anchors.topMargin:parseInt(qsTranslate('','header_mask_tm'),10)
                source: viewController.settings.assetImagePath+qsTranslate('','header_mask_source')
            }
            Image{
                anchors.top:mask.top
                anchors.topMargin:-parseInt(qsTranslate('','header_mask_tm'),10)
                source:viewHelper.configToolImagePath+configTool.config[configMenu].main.bg
                asynchronous: false
            }
            Image{
                id:airportMapOverLay
                anchors.left: mask.left
                anchors.top: mask.top
                anchors.topMargin: qsTranslate('','header_tab_tm')
                anchors.leftMargin:qsTranslate('','header_tab_gap')
                source:  viewHelper.configToolImagePath+configHeaderTag.overlay_icon1
                ViewText{
                    id:flightDurationLabel
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','header_tab_text_tm')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.flight_duration,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:flightDurationDetails
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','header_tab_time_tm')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [airPortDetails[2],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
                ViewText{
                    id:elapsedTimeLabel
                    anchors.top:flightDurationLabel.bottom
                    anchors.topMargin: qsTranslate('','header_tab_text_vgap')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.flight_elapse_time,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:elapsedTimeDetails
                    anchors.top:flightDurationDetails.bottom
                    anchors.topMargin: qsTranslate('','header_tab_time_vgap')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [airPortDetails[1],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
                ViewText{
                    id:remainingTimeLabel
                    anchors.top:elapsedTimeLabel.bottom
                    anchors.topMargin: qsTranslate('','header_tab_text_vgap')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.flight_remain_time,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:remainingTimeDetails
                    anchors.top:elapsedTimeDetails.bottom
                    anchors.topMargin: qsTranslate('','header_tab_time_vgap')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [airPortDetails[0],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
            }
            Image{
                id:viewMapOverLay
                anchors.top: mask.top
                anchors.topMargin: qsTranslate('','header_tab_tm')
                anchors.horizontalCenter:  mask.horizontalCenter
                source:  viewHelper.configToolImagePath+configHeaderTag.overlay_icon2
                ViewText{
                    id:localTimeOriginLabel
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','header_tab_text_tm')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.flight_local_time_org,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:localTimeOriginDetails
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','header_tab_time_tm')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [viewMapsDetails[0],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
                ViewText{
                    id:estimatedTimeOfArrivalLabel
                    anchors.top:localTimeOriginLabel.bottom
                    anchors.topMargin: qsTranslate('','header_tab_text_vgap')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.flight_estimate_time,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:estimatedTimeOfArrivalDetails
                    anchors.top:localTimeOriginDetails.bottom
                    anchors.topMargin: qsTranslate('','header_tab_time_vgap')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [viewMapsDetails[1],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
                ViewText{
                    id:localTimeDestinatinLabels
                    anchors.top:estimatedTimeOfArrivalLabel.bottom
                    anchors.topMargin: qsTranslate('','header_tab_text_vgap')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.flight_local_time_dest,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:localTimeDestinatinDetails
                    anchors.top:estimatedTimeOfArrivalDetails.bottom
                    anchors.topMargin: qsTranslate('','header_tab_time_vgap')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [viewMapsDetails[2],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
            }
            Image{
                id:flightConnectionOverLay
                anchors.top: mask.top
                anchors.topMargin: qsTranslate('','header_tab_tm')
                anchors.right: mask.right
                anchors.rightMargin: qsTranslate('','header_tab_gap')
                source:  viewHelper.configToolImagePath+configHeaderTag.overlay_icon3
                ViewText{
                    id:altitudeLabels
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','header_tab_text_tm')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.altitude,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:altitudeDetails
                    anchors.top:parent.top
                    anchors.topMargin: qsTranslate('','header_tab_time_tm')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [flightConnectionDetails[0],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
                ViewText{
                    id:groundSpeedLabels
                    anchors.top:altitudeLabels.bottom
                    anchors.topMargin: qsTranslate('','header_tab_text_vgap')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.ground_speed,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:groundSpeedDetails
                    anchors.top:altitudeDetails.bottom
                    anchors.topMargin: qsTranslate('','header_tab_time_vgap')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [flightConnectionDetails[1],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
                ViewText{
                    id:outsideTemperatureLabels
                    anchors.top:groundSpeedLabels.bottom
                    anchors.topMargin: qsTranslate('','header_tab_text_vgap')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','header_tab_text_lm')
                    width: qsTranslate('','header_tab_text_w')
                    varText: [configTool.language.global_elements.outside_temp,configHeaderTag.text_color_n,qsTranslate('','header_tab_text_fontsize')]
                }
                ViewText{
                    id:outsideTemperatureDetails
                    anchors.top:groundSpeedDetails.bottom
                    anchors.topMargin: qsTranslate('','header_tab_time_vgap')
                    anchors.right: parent.right
                    anchors.rightMargin:  qsTranslate('','header_tab_time_rm')
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','header_tab_time_w')
                    varText: [flightConnectionDetails[2],configHeaderTag.text_color_n,qsTranslate('','header_tab_time_fontsize')]
                }
            }
            SimpleNavButton {
                id:airportMapBTN
                //focus: true
                anchors.top:airportMapOverLay.bottom
                anchors.topMargin: qsTranslate('','header_tab_btn_tm')
                anchors.horizontalCenter:  airportMapOverLay.horizontalCenter
                normImg: viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_n
                highlightimg:  viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_h
                pressedImg:  viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_p
                nav:["","",viewMapBTN,/*headerBtnsobj*/""]
                onNoItemFound: if(dir=="up")helpBtnFocus()
                onEnteredReleased: {
                    viewController.updateSystemPopupQ(7,true)
                    //for this ATP
                    //                    if(dataController.mediaServices.getServiceBlockStatus("svcSeatIntPVIS")){
                    //                        viewController.updateSystemPopupQ(7,true)
                    //                        return;
                    //                    }else{

                    //                    }
                }
                Rectangle{
                    anchors.centerIn: airportMapBTN
                    color: "transparent"
                    width: airportMapIcon.width+airportMapIconText.paintedWidth+parseInt(qsTranslate('','header_tab_btn_gap'),10)
                    height: parent.height
                    Image{
                        id:airportMapIcon
                        anchors.verticalCenter: parent.verticalCenter
                        source: viewHelper.configToolImagePath+(airportMapBTN.activeFocus?configHeaderTag.airport_map_btn_h:(airportMapBTN.isPressed?configHeaderTag.airport_map_btn_p:configHeaderTag.airport_map_btn_n))
                    }
                    ViewText{
                        id:airportMapIconText
                        anchors.left: airportMapIcon.right
                        anchors.leftMargin: qsTranslate('','header_tab_btn_gap')
                        anchors.verticalCenter: airportMapIcon.verticalCenter
                        varText: [configTool.language.global_elements.airport_map,airportMapBTN.activeFocus?configHeaderTag.text_color_h:(airportMapBTN.isPressed?configHeaderTag.text_color_p:configHeaderTag.text_color_n),qsTranslate('','header_tab_btn_fontsize')]
                    }
                }
            }
            SimpleNavButton {
                id:viewMapBTN
                focus:true;
                anchors.top:viewMapOverLay.bottom
                anchors.topMargin: qsTranslate('','header_tab_btn_tm')
                anchors.horizontalCenter:  viewMapOverLay.horizontalCenter
                normImg: viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_n
                highlightimg:  viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_h
                pressedImg:  viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_p
                nav:[airportMapBTN,"",flightConnectionBTN,""]
                onNoItemFound: if(dir=="up")helpBtnFocus()
                onEnteredReleased: {
                    if(viewHelper.checkForExternalBlock("Voyager")) return;
                    var tmpID=viewHelper.isKids(current.template_id)
                    if(tmpID && !widgetList.isVideoVisible()){
                        //    viewHelper.directJumpFromKids="Voyager"
                        //    viewHelper.refreshToMainSection();
                        viewHelper.externalEvents("Voyager")
                    }
                    if(widgetList.isVideoVisible()){
                        viewHelper.forceFullyCloseWidgets();
                        widgetList.stopVod();
                        viewHelper.isExternalOnVOD="Voyager"
                        return;
                    }
                    else  if(!tmpID)viewHelper.externalEvents("Voyager")
                }
                Rectangle{
                    anchors.centerIn: viewMapBTN
                    color: "transparent"
                    width: viewMapIcon.width+viewMapIconText.paintedWidth+parseInt(qsTranslate('','header_tab_btn_gap'),10)
                    height: parent.height
                    Image{
                        id:viewMapIcon
                        anchors.verticalCenter: parent.verticalCenter
                        source: viewHelper.configToolImagePath+(viewMapBTN.activeFocus?configHeaderTag.view_map_btn_h:(viewMapBTN.isPressed?configHeaderTag.view_map_btn_p:configHeaderTag.view_map_btn_n))
                    }
                    ViewText{
                        id:viewMapIconText
                        anchors.left: viewMapIcon.right
                        anchors.leftMargin: qsTranslate('','header_tab_btn_gap')
                        anchors.verticalCenter: viewMapIcon.verticalCenter
                        varText: [configTool.language.global_elements.view_map,viewMapBTN.activeFocus?configHeaderTag.text_color_h:(viewMapBTN.isPressed?configHeaderTag.text_color_p:configHeaderTag.text_color_n),qsTranslate('','header_tab_btn_fontsize')]
                    }
                }
            }
            SimpleNavButton {
                id:flightConnectionBTN
                anchors.top:flightConnectionOverLay.bottom
                anchors.topMargin: qsTranslate('','header_tab_btn_tm')
                anchors.horizontalCenter: flightConnectionOverLay.horizontalCenter
                normImg: viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_n
                highlightimg:  viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_h
                pressedImg:  viewHelper.configToolImagePath+configHeaderTag.flight_info_btn_p
                nav:[viewMapBTN,"","",""]
                onNoItemFound: if(dir=="up")helpBtnFocus()
                onEnteredReleased: {
                    if(viewHelper.checkForExternalBlock("CG")) return;
                    var cgStat=dataController.connectingGate.getCgStatus()
                    if(!cgStat){
                        viewController.updateSystemPopupQ(7,true)
                        return
                    }
                    var tmpID=viewHelper.isKids(current.template_id)
                    if(tmpID){
                        viewHelper.directJumpFromKids="CG"
                        viewHelper.refreshToMainSection();
                    }
                    if(widgetList.isVideoVisible()){
                        viewHelper.forceFullyCloseWidgets()
                        widgetList.stopVod()
                        viewHelper.isExternalOnVOD="CG"
                        return;
                    }
                    else  if(!tmpID)viewHelper.externalEvents("CG")
                }
                Rectangle{
                    anchors.centerIn: flightConnectionBTN
                    color: "transparent"
                    width: flightConnectionIcon.width+flightConnectionIconText.paintedWidth+parseInt(qsTranslate('','header_tab_btn_gap'),10)
                    height: parent.height
                    Image{
                        id:flightConnectionIcon
                        anchors.verticalCenter: parent.verticalCenter
                        source: viewHelper.configToolImagePath+(flightConnectionBTN.activeFocus?configHeaderTag.flight_connect_btn_h:(flightConnectionBTN.isPressed?configHeaderTag.flight_connect_btn_p:configHeaderTag.flight_connect_btn_n))
                    }
                    ViewText{
                        id:flightConnectionIconText
                        anchors.left: flightConnectionIcon.right
                        anchors.leftMargin: qsTranslate('','header_tab_btn_gap')
                        anchors.verticalCenter: flightConnectionIcon.verticalCenter
                        varText: [configTool.language.global_elements.flight_connection,flightConnectionBTN.activeFocus?configHeaderTag.text_color_h:(flightConnectionBTN.isPressed?configHeaderTag.text_color_p:configHeaderTag.text_color_n),qsTranslate('','header_tab_btn_fontsize')]
                    }
                }
            }
        }
    }
}
