import Qt 4.7
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: footer
    /**************************************************************************************************************************/
    signal backPressed
    signal homePressed
    /**************************************************************************************************************************/
    function load(){
        push(compLogo,"logo")
        push(compFooterButtons,"footerButtons")
    }
    function init(){
        core.debug("Footer.qml | init ")
    }
    function clearOnExit(){ core.debug("Footer.qml | clearOnExit ") }
    /**************************************************************************************************************************/

    /**************************************************************************************************************************/
    property QtObject logo
    Component{
        id:compLogo
        Image{
            width:parent.width
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            source: viewController.settings.assetImagePath+"footer.png"
            height:35;
        }
    }
    property QtObject footerButtons
    Component{
        id:compFooterButtons
        FocusScope {
            id:footerinscreen
            width: parent.width
            anchors.bottom: parent.bottom
            SimpleButton{
                id:backBtn
                width: 44
                height: 44
                isFocusRequired:false
                anchors.left:parent.left
                anchors.leftMargin:core.settings.resolutionIndex==1?5:25
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 4
                onEnteredReleased:  backPressed()

            }
            SimpleButton{
                id:homeBtn
                width: 44
                height: 44
                isFocusRequired:false
                anchors.left:backBtn.right
                anchors.leftMargin:20
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 4
                onEnteredReleased: homePressed()

            }
        }
    }
}
