// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0
SmartLoader{
    id:vodScreen
    property string btnSelcted: "play"
    property alias vodScreen: vodScreen
    property alias hideonlyHeader: hideonlyHeader
    property alias hidePanel: hidePanel
    property int speedVal: 0
    property bool isPaused:true
    property bool isAspect4isto3:false
    property bool showHeaderFooter:true
    property bool isSoundTrackOpen: false
    property bool isVolumeOpen: false
    property bool isBrightnessOpen: false
    property bool isSpeed: false
    property bool isExitPopup: false
    property bool isEpisodeChanged: false
    property bool isModelUpdated: false
    property bool isFooterBack: false
    property bool isVideoPlayable: false
    property bool bckTakeFocus: true
    property variant configVideoTag:viewHelper.isKids(current.template_id)?configTool.config.kids_global.video:configTool.config.global.video;
    property string aspectRatio: ""
    property string earlierHelpScreen:"main_menu";
    width:core.settings.screenWidth;
    height:core.settings.screenHeight;

    /**************************************************************************************************************/
    Timer{
        id:vodclose
        interval: 200
        running: false
        onTriggered: {
            viewHelper.unblockKeys()
            showVideo(false)
            if(viewController.getActiveScreenPopupID() != 0 || viewController.getActiveSystemPopupID() != 0)return
            viewController.getActiveScreenRef().forceActiveFocus()
            if(viewHelper.isShortCutForPremium)viewHelper.shortcutForHotKeys(viewHelper.shortCutPremiumParams)
            if(widgetList.getHelpStatus()==1)widgetList.setHelpStatus(false)
        }
    }
    Timer{
        id:vodPanelTimer
        interval:viewController.getActiveScreenPopupID() != 0 && viewController.getActiveSystemPopupID() != 0?2000:5000
        onTriggered:{
            if(viewController.getActiveScreenPopupID() != 0 && viewController.getActiveSystemPopupID() != 0){
                vodPanelTimer.restart()
                return;
            }
            hidePanel.restart()
        }
    }

    onVisibleChanged:{
        if(visible){viewHelper.isExternalOnVOD="";init();}
    }
    onIsPausedChanged: {
        if(isPaused) actiononTimer(true)
        else if(showHeaderFooter) actiononTimer(isFooterBack)
        if(widgetList.isSoundTrackSubtitleOpen()){
            if(!showHeaderFooter){
                hidePanel.restart()
                widgetList.focusTosoundTrackSubtitle()
            }
        }
    }
    Connections{
        target:visible?widgetList.getSoundTrackSubtitleRef():null
        onReadyToPlayVideo:{
            viewHelper.setVideoParams()
        }
    }
    Connections{
        target:(visible)?widgetList.getHeaderRef():null
        onRestartVODTimer: {
            if(!isSpeed)actiononTimer()
        }
        onStopVODTimer :{
            if(!visible)return;
            if(param) {
                viewHelper.blockKeysForSeconds(1.5)
                if(isSpeed)  checkForPreviousState()
                else  pif.vod.pause()
                isPaused=true
                if(widgetList.getisFlightInfoOpen()) hideonlyVideoPanel.restart()
            }
            else {
                resumeInpreviousMode()
                if(!showHeaderFooter && widgetList.getHeaderRef().anchors.topMargin==0)hideonlyHeader.restart()
            }
        }
    }
    Connections{
        target: (visible)?viewHelper:null
        onPlayEpisode:{
            if(index>viewHelper.getTvSeriesIndex()) {
                viewHelper.setTvSeriesIndex((viewHelper.getTvSeriesIndex()+1))
                pif.vod.playNext()
            }
            else {
                viewHelper.setTvSeriesIndex((viewHelper.getTvSeriesIndex()-1))
                pif.vod.playPrevious()
            }

        }
    }
    Connections{
        target: (visible)?pif.vod:null;
        onSigMediaElapsed: {
            if(videoPanelLoaderObj.videoPanelLoader.item.progressBar.isDragged)return
            videoPanelLoaderObj.videoPanelLoader.item.elapsedTime.text=core.coreHelper.getTimeFormatFromSecs(elapseTime, 2)
            core.debug("endTime "+endTime)
            videoPanelLoaderObj.videoPanelLoader.item.progressBar.externalCalc(elapseTime,endTime)
        }
        onSigMediaForward:{
            isSpeed=true
            isPaused=true
            btnSelcted="forward"
            speedVal=speed
            if(isSpeed && !showHeaderFooter)hidePanel.restart()
        }
        onSigMediaRewind:{
            btnSelcted="rewind"
            isSpeed=true
            isPaused=true
            speedVal=speed
            if(isSpeed && !showHeaderFooter)hidePanel.restart()
        }
        onSigAggPlayCompleted:{
            viewHelper.updatePremiumHandset(4,false);
            if(pif.vod.getMediaState()==pif.vod.cMEDIA_STOP)
                exitVod();
        }
        onSigMidPlayBegan:{
            isVideoPlayable=true
            viewHelper.updatePremiumHandset(4,true);
            //            if(viewController.getActiveScreenPopupID() != 0) {pif.vod.pause();return;} //actionOnVOD(false)
            if(vodScreen.isModelUpdated ||(widgetList.getisFlightInfoOpen() || widgetList.isSoundTrackSubtitleOpen() || isExitPopup)){
                pif.vod.pause()
                vodScreen.isModelUpdated=false
            }
            isPaused=false
            isExitPopup=false
            isSpeed=-false
            isFooterBack=false
            setAspectRatio();
            if(!widgetList.getHeaderBtnFocus() && !viewHelper.isTvSeries){
                if(videoPanelLoaderObj.videoPanelLoader.sourceComponent !=blankComp && showHeaderFooter)
                    videoPanelLoaderObj.videoPanelLoader.item.videoPanelBG.forceActiveFocus()
                else if(!(widgetList.getisFlightInfoOpen() && widgetList.isSoundTrackSubtitleOpen() && isExitPopup))
                    videoAreaObj.forceActiveFocus()
            }
            if(isSoundTrackOpen){isSoundTrackOpen=false;}
            if(isEpisodeChanged){
                var sndParams=[]
                sndParams[0]=cmid;
                sndParams[2]=dataController.getLidByLanguageISO(viewHelper.getDefaultSndLang());
                dataController.getSoundTracks(sndParams,viewHelper.setSoundTrackQuery);isEpisodeChanged=false;
            }
            if(viewHelper.isTvSeries){
                if(viewHelper.isPreviousEnabled){
                    viewHelper.isPreviousEnabled=false
                    viewHelper.setTvSeriesIndex(0)
                }else {
                    viewHelper.setTvSeriesIndex((viewHelper.getTvSeriesIndex()+1))
                }
            }
        }
        onSigMediaPaused:{
            pauseVideo()
        }
        onSigMidPlayStopped:{
            viewHelper.updatePremiumHandset(4,false);
            if(viewHelper.isVideoAudioServiceBlocked){
                if(viewController.getActiveScreenPopupID())viewController.hideScreenPopup();
                exitVod()
                viewHelper.homeAction()
                viewController.updateSystemPopupQ(7,true);
                return;
            }
            if(viewHelper.isLaunchingAppFromKarma){
                core.dataController.getMediaByMid([viewHelper.isLaunchingAppIdFromKarma],karmaGameCallbk)
            }
            if(pif.vod.getMediaState()==pif.vod.cMEDIA_STOP && !viewHelper.isFromTrailer) exitVod()
            //            if(viewHelper.isFromKarma && viewHelper.isFromTrailer)exitVod()
            if((viewHelper.isFromKarma && viewHelper.isFromTrailer) || (viewHelper.isMicroAppVideo))exitVod()
            else  if(viewHelper.isFromTrailer && !viewHelper.isTrailerPressed){videoPanelLoaderObj.videoPanelLoader.sourceComponent=videoPanel;viewHelper.isFromTrailer=false}
            if(viewHelper.isExternalOnVOD!=""){
                viewHelper.externalEvents(viewHelper.isExternalOnVOD)
                viewHelper.isExternalOnVOD=""
            }
            viewHelper.isTrailerPressed=false
            videoPanelLoaderObj.videoPanelLoader.item.progressbar.resetSlider()

        }
        onSigStretchState: {
            setAspectRatio(stretch)
        }
        //        onSigStretchState: {
        //            if(stretch) isAspect4isto3=false
        //            else isAspect4isto3=true
        //        }
    }
    Connections{
        target: visible?pif:null;
        onVolumeChanged: {
            if(widgetList.getisFlightInfoOpen() || widgetList.isSoundTrackSubtitleOpen() || isExitPopup)return;
            setIndicator("vol",true);
            videoPanelLoaderObj.videoPanelLoader.item.volumeSlider.volumeSliderComp.setInitailValue=pif.getVolumeLevel()
            videoPanelLoaderObj.videoPanelLoader.item.volumeSlider.forceActiveFocus()
            if(!isPaused)actiononTimer()
        }
        onBrightnessChanged: {
            if(widgetList.getisFlightInfoOpen()  || widgetList.isSoundTrackSubtitleOpen() || isExitPopup)return;
            setIndicator("bri",true);
            videoPanelLoaderObj.videoPanelLoader.item.brightnessSlider.volumeSliderComp.setInitailValue=pif.getBrightnessLevel()
            videoPanelLoaderObj.videoPanelLoader.item.brightnessSlider.forceActiveFocus()
            if(!isPaused)actiononTimer()
        }
    }
    Connections{
        target:(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET && visible)?pif.sharedModelServerInterface:null;
        onSigDataReceived:{
            if(api=="userDefinedDraggingOn"){
                if(!showHeaderFooter){
                    hidePanel.restart()
                    actiononTimer(false)
                }else if(showHeaderFooter) actiononTimer(false)
            }else if(api=="userDefinedSpecialCall"){
                if(videoPanelLoaderObj.videoPanelLoader.sourceComponent==videoPanel)return;
                videoPanelLoaderObj.videoPanelLoader.sourceComponent=videoPanel
                videoPanelLoaderObj.videoPanelLoader.item.forceActiveFocus()
            }
        }
    }
    /**************************************************************************************************************/
    function load(){
        push(videoArea,"videoAreaObj")
        push(indicators,"indicatorsObj")
        push(videoPanelLoaderArea,"videoPanelLoaderObj")
    }
    function init(){
        core.debug("Video.qml | Init");
        earlierHelpScreen=viewHelper.getHelpTemplate()
        if(viewHelper.isTvSeries){
            viewHelper.setHelpTemplate("tv_vod");
        }else if(viewHelper.getVodType()==1){
            viewHelper.setHelpTemplate("movie_trailer");
        }
        else{
            viewHelper.setHelpTemplate("movie_vod");
        }

        videoPanelLoaderObj.videoPanelLoader.sourceComponent=blankComp
        if(viewHelper.getVodType()==1) videoPanelLoaderObj.videoPanelLoader.sourceComponent=videoPanelTrailer
        else videoPanelLoaderObj.videoPanelLoader.sourceComponent=videoPanel
        btnSelcted="play"
        if(viewHelper.isTvSeries)isEpisodeChanged=true
        if(isPaused!=false){
            isPaused=false
        }

        actiononTimer()
        if(viewHelper.getVodType()==1){
            videoPanelLoaderObj.videoPanelLoader.item.forceActiveFocus();
            viewHelper.hideAndroidBar();
        }
    }
    function backBtnPressed(param){
        if(showHeaderFooter)hidePanel.restart()
        isExitPopup=true
        checkForPreviousState()
        if (pif.vod.getMediaState()==pif.vod.cMEDIA_PLAY  || pif.vod.getMediaState()==pif.vod.cMEDIA_FORWARD || pif.vod.getMediaState()==pif.vod.cMEDIA_REWIND)
            pif.vod.pause()
        else  pauseVideo()
        // pauseVideo()
        isFooterBack=true
        if(param)viewHelper.isHomePressed=true
    }
    function reset(){
        speedVal=0
        isPaused=true
        isAspect4isto3=false
        showHeaderFooter=true
        isSoundTrackOpen=false
        isVolumeOpen=false
        isBrightnessOpen=false
        isSpeed=false
        isExitPopup=false
        isEpisodeChanged=false
        isModelUpdated=false
        isVideoPlayable=false
        if(hideonlyHeader.running) hideonlyHeader.complete()
        if(hidePanel.running)hidePanel.complete()
        if(hideonlyVideoPanel.running)hideonlyVideoPanel.complete()
        viewHelper.isMicroAppVideo = false;
        widgetList.getHeaderRef().anchors.topMargin=0
        indicatorsObj.episodeFS.anchors.topMargin=qsTranslate('','video_tv_btn_tm')
        widgetList.showsoundTrackSubtitle(false)
        //        if(videoPanelLoaderObj.videoPanelLoader.sourceComponent!=blankComp)
        //            videoPanelLoaderObj.videoPanelLoader.item.videoPanelBGBack.anchors.bottomMargin=qsTranslate('','video_bg_bm')
    }
    function actiononTimer(param){
        if(param=="special")
            hidePanel.restart()
        else if(param || isPaused)
            vodPanelTimer.stop()
        else
            vodPanelTimer.restart()
    }
    function setIndicator(type,param){
        if(param && !showHeaderFooter)hidePanel.restart()
        if(type=="bri"){
            isBrightnessOpen=param
            isVolumeOpen=false
        }else if(type=="vol"){
            isVolumeOpen=param
            isBrightnessOpen=false
        }
    }
    function showVideo(flag){
        if(vodScreen.visible!=flag)
            vodScreen.visible=flag
    }
    function focusToVideo(param){
        if(param=="soundtrack"){
            isSoundTrackOpen=false
            videoPanelLoaderObj.videoPanelLoader.item.soundTrack.forceActiveFocus()
        }
        else {
            if(viewHelper.isTvSeries){
                if(viewHelper.getTvSeriesModel().count==1)videoPanelLoaderObj.videoPanelLoader.item.videoPanelBG.forceActiveFocus()
                else  indicatorsObj.episodeFS.forceActiveFocus()
            }
            else videoPanelLoaderObj.videoPanelLoader.item.videoPanelBG.forceActiveFocus()
        }
    }
    function soundTrackPopup(){
        btnSelcted="soundTrack"
        if(!showHeaderFooter){
            hidePanel.restart()
        }
        if(isSpeed){
            isSoundTrackOpen=true
            checkForPreviousState()
            openSoundTrackPopup()
        }
        else openSoundTrackPopup()
    }
    function checkForPreviousState(){
        if(pif.vod.getMediaState()==pif.vod.cMEDIA_FORWARD) {viewHelper.statusOfSoundtrackPopup="Forward"}
        else if(pif.vod.getMediaState()==pif.vod.cMEDIA_REWIND) { viewHelper.statusOfSoundtrackPopup="Rewind"}
        isSpeed=false
        pif.vod.pause()
    }
    function openSoundTrackPopup(){
        widgetList.showsoundTrackSubtitle(true)
        pif.vod.pause()
    }
    function actionOnVOD(param){
        viewController.hideScreenPopup()
        viewHelper.isFromTrailer=false
        isExitPopup=false
        if(param){
            isFooterBack=true;
            if(viewHelper.isHomePressed) viewHelper.homeAction()
            pif.vod.stop()
        }
        else {
            resumeInpreviousMode()
            if(showHeaderFooter)
                videoPanelLoaderObj.videoPanelLoader.item.videoPanelBG.forceActiveFocus()
            else
                videoAreaObj.forceActiveFocus()
            if(viewHelper.isShortCutForPremium){
                viewHelper.isShortCutForPremium=false
                viewHelper.shortCutPremiumParams=""
            }
        }
        viewHelper.isHomePressed=false
    }
    function exitVod(){
        core.debug("Video.qml | exitVod ")
        if(!core.pc){
            viewHelper.showAndroidBar();
        }//pif.android.setSystemBarVisibility(pif.android.cBAR_VISIBLE)
        viewHelper.statusOfSoundtrackPopup=""
        reset()
        viewHelper.blockKeysForSeconds(2)
        vodclose.restart()
        viewHelper.setHelpTemplate(earlierHelpScreen);
    }
    function moveUp(){
        if(viewHelper.isTvSeries){
            if(viewHelper.getTvSeriesModel().count==1)widgetList.focusToHeader();
            else  indicatorsObj.episodeFS.forceActiveFocus();
            return;
        }
        else widgetList.focusToHeader();
    }
    function playMovieFromTrailer(dModel){
        if(viewHelper.getMediaListingModel().count==undefined || viewHelper.getMediaListingModel==""){
            viewHelper.setMediaListingModel(dModel)
            viewHelper.setMediaListingIndex(0)
        }
        viewHelper.initializeVOD("restart",3,"",false,true)
    }
    function languageUpdate(){
        vodScreen.isModelUpdated=true
        var sndParams=[]
        sndParams[0]=viewHelper.playingVODmid;
        sndParams[2]=dataController.getLidByLanguageISO(viewHelper.getDefaultSndLang());
        dataController.getSoundTracks(sndParams,viewHelper.setSoundTrackQuery);
        if(isSoundTrackOpen)widgetList.soundTrackinit()
    }
    function controlSelect(no){
        if(widgetList.isSoundTrackSubtitleOpen())return;
        if(no==3){
            if(showHeaderFooter)hidePanel.restart()
        } else if(!showHeaderFooter)hidePanel.restart()

        actiononTimer(true);
        videoPanelLoaderObj.videoPanelLoader.item.controlSelect(no);
    }
    function setAspectRatio(param){
        aspectRatio=pif.vod.getAspectRatio()
        if(aspectRatio=="4x3Adjustable"){
            if(pif.vod.getAllMediaPlayInfo().stretch) isAspect4isto3=true
            else isAspect4isto3=false
        }else if(aspectRatio=="16x9Adjustable"){
            isAspect4isto3=false
        }else if(aspectRatio=="4x3Fixed"){
            isAspect4isto3=true
        }else if(aspectRatio=="16x9Fixed"){
            isAspect4isto3=false;
        }
        core.debug("isAspect4isto3 "+isAspect4isto3)
    }
    function pauseVideo(){
        //if(viewHelper.isBlock())return
        if(viewController.getActiveScreenPopupID() != 0 || viewController.getActiveSystemPopupID()!=0 ||isExitPopup){
            if(showHeaderFooter)hidePanel.restart();
        }
        viewHelper.blockKeysForSeconds(1)
        if (pif.vod.getMediaState()==pif.vod.cMEDIA_PAUSE) isPaused=true
        if(isExitPopup) {
            viewHelper.blockKeysForSeconds(0)
            if(viewController.getActiveSystemPopupID()!=0)return;
            //            if(!showHeaderFooter)hidePanel.restart();
            //            else actiononTimer(true)
            viewController.showScreenPopup(1)
        }

    }
    function resumeInpreviousMode(){
        if(viewHelper.statusOfSoundtrackPopup!=""){
            if(viewHelper.statusOfSoundtrackPopup=="Forward")pif.vod.forward(true)
            else if(viewHelper.statusOfSoundtrackPopup=="Rewind")pif.vod.rewind(true)
            viewHelper.statusOfSoundtrackPopup=""
            if(!showHeaderFooter)hidePanel.restart()
        }
        else     pif.vod.resume()
        if(showHeaderFooter)isFooterBack=false
        else isFooterBack=true

    }
    function stopVod(){
        showHeaderFooter=false
        isExitPopup=true
        if (pif.vod.getMediaState()==pif.vod.cMEDIA_PLAY || isSpeed){
            checkForPreviousState()
        }
        else pauseVideo()
    }
    function karmaGameCallbk(dModel){
        viewHelper.isLaunchingAppFromKarma=false;
        viewHelper.karmaGameModel=dModel
        viewHelper.launchKarmaGame()
    }
    /**************************************************************************************************************/
    property QtObject videoAreaObj
    Component{
        id:videoArea
        Rectangle{
            id:screenOnTop;
            width:core.settings.screenWidth;
            height:core.settings.screenHeight;
            color:pif.getPIPColorKey();
            Keys.onReleased:{
                if(hidePanel.running)return
                if(!isPaused){
                    hidePanel.restart()
                    vodPanelTimer.restart()
                }
            }
            MouseArea{
                anchors.fill:parent
                onReleased: {
                    if(hidePanel.running)return
                    if(!isPaused){
                        hidePanel.restart()
                        if(showHeaderFooter) actiononTimer()
                        else   actiononTimer(true)
                    }
                    else if(isPaused)actiononTimer("special")
                }
            }
        }
    }
    property QtObject indicatorsObj
    Component{
        id:indicators
        Item{
            anchors.fill: parent
            property alias episodeFS: episodeFS
            Image{
                id:incatorPopup
                visible: isSpeed
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','video_speed_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                source: viewHelper.configToolImagePath+configTool.config.global.video.rewfor_popup
                ViewText{
                    anchors.centerIn: parent
                    varText: [speedVal+"x",configVideoTag.text_color,qsTranslate('','video_speed_fontsize')]
                }
            }
            FocusScope{
                width: parent.width
                height: parent.height
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','video_tv_btn_tm')
                id:episodeFS
                SimpleNavBrdrBtn{
                    id:prevEpisode
                    visible: viewHelper.isTvSeries
                    isDisable:viewHelper.tvSeriesIndex==0
                    width:  qsTranslate('','video_tv_btn_w')
                    height: qsTranslate('','video_tv_btn_h')
                    anchors.left:parent.left
                    anchors.leftMargin:  qsTranslate('','video_tv_btn_gap')
                    isHighlight: activeFocus
                    normalImg:   viewHelper.configToolImagePath+configVideoTag.vod_btn_n
                    highlightImg: viewHelper.configToolImagePath+(viewHelper.isKids(current.template_id)?configVideoTag.vod_tv_btn_h:configVideoTag.vod_btn_h)
                    pressedImg: viewHelper.configToolImagePath+configVideoTag.vod_btn_p
                    normalBorder: [qsTranslate('','video_tv_btn_margin')]
                    highlightBorder:  [qsTranslate('','video_tv_btn_margin')]
                    ViewText{
                        anchors.centerIn: prevEpisode
                        varText: [text,configVideoTag.text_color,qsTranslate('','video_tv_btn_fontsize')]
                        text:configTool.language.tv.previous_episode
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                    onIsDisableChanged: if(isDisable)nextEpisode.forceActiveFocus()
                    onEnteredReleased: {
                        actiononTimer(false)
                        isEpisodeChanged=true
                        viewHelper.setTvSeriesIndex((viewHelper.getTvSeriesIndex()-1))
                        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)pif.sharedModelServerInterface.sendApiData("userDefinedPlayEpisode",{playIndex:viewHelper.getTvSeriesIndex()});
                        actiononTimer()
                        pif.vod.playPrevious()
                    }
                    onNoItemFound: {
                        actiononTimer()
                        if(dir=="right"){
                            if(!nextEpisode.isDisable)nextEpisode.forceActiveFocus()
                        }
                        else  if(dir=="up")widgetList.focusToHeader();
                        else if(dir=="down")videoPanelLoaderObj.videoPanelLoader.item.videoPanelBG.forceActiveFocus()
                    }
                }
                SimpleNavBrdrBtn{
                    id:nextEpisode
                    visible: viewHelper.isTvSeries
                    isDisable:viewHelper.tvSeriesIndex==(viewHelper.getTvSeriesModel().count-1)
                    focus: true
                    width:  qsTranslate('','video_tv_btn_w')
                    height: qsTranslate('','video_tv_btn_h')
                    anchors.right:parent.right
                    anchors.rightMargin:  qsTranslate('','video_tv_btn_gap')
                    isHighlight: activeFocus
                    normalImg:   viewHelper.configToolImagePath+configVideoTag.vod_btn_n
                    highlightImg:  viewHelper.configToolImagePath+(viewHelper.isKids(current.template_id)?configVideoTag.vod_tv_btn_h:configVideoTag.vod_btn_h)
                    pressedImg: viewHelper.configToolImagePath+configVideoTag.vod_btn_p
                    normalBorder: [qsTranslate('','video_tv_btn_margin')]
                    highlightBorder:  [qsTranslate('','video_tv_btn_margin')]
                    ViewText{
                        anchors.centerIn: nextEpisode
                        varText: [text,configVideoTag.text_color,qsTranslate('','video_tv_btn_fontsize')]
                        text:configTool.language.tv.next_episode
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                    onEnteredReleased: {
                        actiononTimer(false)
                        isEpisodeChanged=true
                        viewHelper.setTvSeriesIndex((viewHelper.getTvSeriesIndex()+1))
                        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)pif.sharedModelServerInterface.sendApiData("userDefinedPlayEpisode",{playIndex:viewHelper.getTvSeriesIndex()});
                        actiononTimer()
                        pif.vod.playNext()
                    }
                    onIsDisableChanged: if(isDisable)prevEpisode.forceActiveFocus()
                    onNoItemFound: {
                        actiononTimer()
                        if(dir=="left"){
                            if(!prevEpisode.isDisable)prevEpisode.forceActiveFocus()
                        }
                        else  if(dir=="up")widgetList.focusToHeader();
                        else if(dir=="down")videoPanelLoaderObj.videoPanelLoader.item.videoPanelBG.forceActiveFocus()
                    }
                }
            }
        }
    }
    property QtObject videoPanelLoaderObj
    Component{
        id:videoPanelLoaderArea
        Loader{
            property alias videoPanelLoader: videoPanelLoader
            id:videoPanelLoader
            anchors.fill: parent
            //  sourceComponent: videoPanel
            onStatusChanged: {
                if(sourceComponent==blankComp)return
                if(status==Loader.Ready)videoPanelLoader.item.forceActiveFocus()
            }
            onSourceComponentChanged: {
                core.debug("Source comp changed "+sourceComponent)
            }
        }
    }
    Component{
        id:videoPanel
        FocusScope{
            id:videoPanelBG
            anchors.fill: parent
            property alias videoPanelBG: videoPanelBG
            property alias videoPanelBGBack: videoPanelBGBack
            property alias play: play
            property alias soundTrack: soundTrack
            property alias progressBar: progressBar
            property alias elapsedTime: elapsedTime
            property alias totalTime: totalTime
            property alias brightness: brightness
            property alias volume: volume
            property alias volumeSlider: volumeSlider
            property alias brightnessSlider: brightnessSlider
            property variant controls:[forward,rewind,play,stop];
            function controlSelect(no){
                if(controls[no]=="" || controls[no]==undefined ||  widgetList.getisFlightInfoOpen() )return;
                controls[no].forceActiveFocus();
                controls[no].enteredReleased();
            }

            Image{
                id:videoPanelBGBack
                anchors.bottom:parent.bottom
                anchors.bottomMargin:qsTranslate('','video_bg_bm')
                anchors.horizontalCenter: parent.horizontalCenter
                source: viewHelper.configToolImagePath+configTool.config.global.video.controller_bg
                property bool hide:(anchors.bottomMargin<82)

                Rectangle{
                    color:core.configTool.config.popup.generic["bg_color"];
                    anchors.fill:parent;
                    radius:25;
                }
                MouseArea{
                    anchors.fill: parent
                    onPressed: actiononTimer(true)
                    onReleased: actiononTimer(false)
                }
                onHideChanged:{
                    if(viewHelper.getVodType()==1)return;
                    if(!hide){
                        viewHelper.showAndroidBar();
                        //pif.android.setSystemBarVisibility(pif.android.cBAR_VISIBLE)
                    }else{
                        viewHelper.hideAndroidBar();
                        //pif.android.setSystemBarVisibility(pif.android.cBAR_HIDE)
                    }
                }

            }
            SimpleNavButton{
                id:rewind
                anchors.left:videoPanelBGBack.left
                anchors.leftMargin: qsTranslate('','video_btns_lm')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus?true:false
                isSelected: (isSpeed && btnSelcted=="rewind" && !activeFocus)
                normImg:  viewHelper.configToolImagePath+configVideoTag.rewind_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.rewind_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.rewind_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.rewind_p
                nav:["","",play,""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: {pif.vod.rewind();}
            }
            ToggleNavButton{
                id:play
                focus: true
                anchors.left:rewind.right
                anchors.leftMargin: qsTranslate('','video_btns_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus?true:false
                cstate: isPaused
                isSelected: false//isPaused && !isExitPopup?true:false
                normImg1: viewHelper.configToolImagePath+configVideoTag.play_n
                highImg1:  viewHelper.configToolImagePath+configVideoTag.play_h
                pressImg1: viewHelper.configToolImagePath+configVideoTag.play_p
                selectedImg1:  viewHelper.configToolImagePath+configVideoTag.play_p
                normImg2: viewHelper.configToolImagePath+configVideoTag.pause_n
                highImg2: viewHelper.configToolImagePath+configVideoTag.pause_h
                pressImg2: viewHelper.configToolImagePath+configVideoTag.pause_p
                selectedImg2: viewHelper.configToolImagePath+configVideoTag.pause_p
                nav:[rewind,"",stop,""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: {
                    isSpeed=false
                    if (pif.vod.getMediaState()==pif.vod.cMEDIA_PLAY) pif.vod.pause();
                    else  pif.vod.resume()
                }
            }
            SimpleNavButton{
                id:stop
                anchors.left:play.right
                anchors.leftMargin: qsTranslate('','video_btns_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus
                isSelected: isSpeed?false:(isExitPopup &&  !activeFocus)
                normImg:  viewHelper.configToolImagePath+configVideoTag.stop_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.stop_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.stop_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.stop_p
                nav:[play,"",forward,""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: {
                    stopVod()
                }
            }
            SimpleNavButton{
                id:forward
                anchors.left:stop.right
                anchors.leftMargin: qsTranslate('','video_btns_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus?true:false
                isSelected: (isSpeed && btnSelcted=="forward" && !activeFocus)
                normImg:  viewHelper.configToolImagePath+configVideoTag.forward_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.forward_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.forward_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.forward_p
                nav:[stop,"",apectratio.isDisable?(soundTrack.isDisable?brightness:soundTrack):apectratio,""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: pif.vod.forward();
            }
            ToggleNavButton{
                id:apectratio
                anchors.left:forward.right
                anchors.leftMargin: qsTranslate('','video_btns_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus?true:false
                cstate: isAspect4isto3
                isDisable:true
                isSelected: aspectRatio=="4x3Adjustable"?false:true;
                normImg1: viewHelper.configToolImagePath+configVideoTag.aspect1_n
                highImg1:  viewHelper.configToolImagePath+configVideoTag.aspect1_h
                pressImg1: viewHelper.configToolImagePath+configVideoTag.aspect1_p
                selectedImg1:  viewHelper.configToolImagePath+configVideoTag.aspect1_p
                normImg2: viewHelper.configToolImagePath+configVideoTag.aspect2_n
                highImg2: viewHelper.configToolImagePath+configVideoTag.aspect2_h
                pressImg2: viewHelper.configToolImagePath+configVideoTag.aspect2_p
                selectedImg2: viewHelper.configToolImagePath+configVideoTag.aspect2_p
                disableImg: isDisable?(cstate?viewHelper.configToolImagePath+configVideoTag.aspect2_p:viewHelper.configToolImagePath+configVideoTag.aspect1_p):""
                nav:[forward,"","",""]
                opacity:0;
                onNoItemFound:{
                    if(dir=="up")moveUp();
                    else if(!soundTrack.isDisable && dir=="right") soundTrack.forceActiveFocus()
                    else if(dir=="right")brightness.forceActiveFocus()
                }
                onPadReleased: actiononTimer()
                onEnteredReleased:{pif.vod.toggleStretchState();actiononTimer()}
            }
            Slider {
                id: progressBar
                anchors.left: videoPanelBGBack.left
                anchors.leftMargin:  parseInt(qsTranslate('','video_progress_lm'),10)-apectratio.width;
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                height: qsTranslate('','video_progress_h')
                width:  parseInt(qsTranslate('','video_progress_w'),10)+apectratio.width
                sliderbg:configVideoTag.base_image
                sliderFillingtItems: [configVideoTag.fill_color,configVideoTag.indicator]
                stepsValue: 1
                maxLevel: pif.vod.getMediaEndTime();
                isFill: true
                enableDragging: isVideoPlayable
                onValueChanged:{
                    var count=Math.round(sliderDotImg.anchors.leftMargin/progressBar.shiftingPerClick)
                    if(progressBar.stepsValue*count>=(maxLevel-3)){
                        pif.vod.setMediaPosition((maxLevel-3))
                    }else{
                        pif.vod.setMediaPosition(progressBar.stepsValue*count)
                    }
                    if(!isDragged)actiononTimer()

                }
                onDragingOn: {
                    var elapseTime=pif.vod.getMediaEndTime()-shiftingValueForDragging
                    elapsedTime.text=core.coreHelper.getTimeFormatFromSecs(elapseTime, 2)
                }
            }
            ViewText{
                id:elapsedTime
                anchors.right: progressBar.left
                anchors.rightMargin: qsTranslate('','video_progress_time_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                varText: ['',configVideoTag.text_color,qsTranslate('','video_progress_time_fontsize')]
            }
            ViewText{
                id:totalTime
                anchors.left: progressBar.right
                anchors.leftMargin: qsTranslate('','video_progress_time_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                varText: ["- "+(progressBar.isDragged?core.coreHelper.convertSecToHMSLeadZero(progressBar.remainingValue):core.coreHelper.convertSecToHMSLeadZero(pif.vod.getmediaRemainTime())),configVideoTag.text_color,qsTranslate('','video_progress_time_fontsize')]
            }
            SimpleNavButton {
                id: soundTrack
                anchors.left: videoPanelBGBack.left
                anchors.leftMargin: qsTranslate('','video_btns_lm1')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isDisable:viewHelper.soundTrackModel.count>1 && vodScreen.visible?false:true
                isHighlight: activeFocus && !isSoundTrackOpen?true:false
                isSelected: btnSelcted=="soundTrack"  && parent.activeFocus && isSoundTrackOpen?true:false
                normImg:  viewHelper.configToolImagePath+configVideoTag.soundtrackcc_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.soundtrackcc_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.soundtrackcc_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.soundtrackcc_p
                nav:[apectratio.isDisable?forward:apectratio,"",brightness,""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: {
                    soundTrackPopup()
                }
            }
            SimpleNavButton {
                id: brightness
                anchors.right: volume.left
                anchors.rightMargin: qsTranslate('','video_btns_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus?true:false
                isSelected: btnSelcted=="brightness"  && parent.activeFocus && isVolumeOpen?true:false
                normImg:  viewHelper.configToolImagePath+configVideoTag.brightness_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.brightness_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.brightness_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.brightness_p
                nav:["","",volume,""]
                onNoItemFound:{
                    actiononTimer()
                    if(dir=="up")moveUp();
                    else  if(!soundTrack.isDisable && dir=="left") soundTrack.forceActiveFocus()
                    else if(dir=="left"){
                        if(apectratio.isDisable)forward.forceActiveFocus()
                        else apectratio.forceActiveFocus()
                    }
                }
                onPadReleased: actiononTimer()
                onEnteredReleased: {
                    btnSelcted="brightness"
                    setIndicator("bri",true)
                    actiononTimer(false)
                    brightnessSlider.forceActiveFocus()
                }
            }
            SimpleNavButton {
                id: volume
                anchors.right: videoPanelBGBack.right
                anchors.rightMargin: qsTranslate('','video_btns_lm')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus?true:false
                isSelected:  btnSelcted=="volume" && parent.activeFocus && isVolumeOpen?true:false
                normImg:  viewHelper.configToolImagePath+configVideoTag.volume_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.volume_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.volume_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.volume_p
                nav:[brightness,"","",""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: {
                    btnSelcted="volume"
                    setIndicator("vol",true)
                    actiononTimer(false)
                    volumeSlider.forceActiveFocus()
                }
            }
            VolumeSlider{
                id:volumeSlider
                anchors.bottom: volume.bottom
                anchors.bottomMargin: qsTranslate('','video_volube_bar_bm')
                anchors.left: volume.left
                source: viewHelper.configToolImagePath+configTool.config.global.video.volume_popup
                volSliderComp:[qsTranslate('','video_volube_bar_h'),qsTranslate('','video_volube_bar_w')]
                volConfigTag:configVideoTag;
                visible: isVolumeOpen;
                forVod: true;
                onSliderEnds:{
                    if(dir=="left"){
                        setIndicator("vol",false)
                        brightness.forceActiveFocus();
                    }
                }
                onPadReleased:{
                    actiononTimer(false);
                }
                onVisibleChanged: if(visible)volumeSliderComp.setInitailValue=pif.getVolumeLevel()
                onActiveFocusChanged: if(!activeFocus){if(!hidePanel.running)isVolumeOpen=false}
            }
            VolumeSlider{
                id:brightnessSlider
                isBriSlider: true
                visible: isBrightnessOpen
                anchors.bottom: brightness.bottom
                anchors.left: brightness.left
                anchors.leftMargin: qsTranslate('','video_volube_bar_lm')
                anchors.bottomMargin: qsTranslate('','video_volube_bar_bm')
                source: viewHelper.configToolImagePath+configTool.config.global.video.volume_popup
                volSliderComp:[qsTranslate('','video_volube_bar_h'),qsTranslate('','video_volube_bar_w')]
                volConfigTag:configVideoTag;
                forVod: true;
                onSliderEnds:{
                    if(dir=="left"){
                        setIndicator("bri",false)
                        if(!soundTrack.isDisable) soundTrack.forceActiveFocus();
                        else apectratio.forceActiveFocus()
                    }
                    else if(dir=="right"){
                        setIndicator("bri",false)
                        volume.forceActiveFocus();
                    }
                }
                onPadReleased:{
                    actiononTimer(false);
                }
                onVisibleChanged: if(visible)volumeSliderComp.setInitailValue=pif.getBrightnessLevel()
                onActiveFocusChanged: if(!activeFocus){if(!hidePanel.running)isBrightnessOpen=false}
            }
        }
    }
    Component{
        id:videoPanelTrailer
        FocusScope{
            id:videoPanelBG

            anchors.fill: parent
            property alias videoPanelBG: videoPanelBG
            property alias videoPanelBGBack: videoPanelBGBack
            property alias playMovie: playMovie
            property alias volume: volume
            property alias progressBar: progressBar
            property alias elapsedTime: elapsedTime
            property alias totalTime: totalTime
            property alias volumeSlider: volumeSlider
            property variant controls:["","","",stop];
            function controlSelect(no){
                if(controls[no]=="" || controls[no]==undefined ||  widgetList.getisFlightInfoOpen() )return;
                controls[no].forceActiveFocus();
                controls[no].enteredReleased();
            }

            Image{
                id:videoPanelBGBack
                anchors.bottom:parent.bottom
                anchors.bottomMargin:(viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_bg_bm') : qsTranslate('','video_bg_bm')
                anchors.horizontalCenter: parent.horizontalCenter
                source: viewHelper.configToolImagePath+configTool.config.global.video.controller_bg
                MouseArea{
                    anchors.fill: parent
                    onPressed: actiononTimer(true)
                    onReleased: actiononTimer(false)
                }
            }
            SimpleNavButton{
                id:repeat
                anchors.left:videoPanelBGBack.left
                anchors.leftMargin: (viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_btns_lm') : qsTranslate('','video_trailer_btns_lm')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                focus: (viewHelper.isMicroAppVideo) ? true : false;
                isHighlight: activeFocus
                normImg:  viewHelper.configToolImagePath+configVideoTag.replay_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.replay_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.replay_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.replay_p
                nav:["","",stop,""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: pif.vod.setMediaPosition(0)
            }
            SimpleNavButton{
                id:stop
                anchors.left:repeat.right
                anchors.leftMargin: (viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_btns_gap') : qsTranslate('','video_trailer_btns_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus
                isSelected: (isExitPopup &&  !activeFocus)
                normImg:  viewHelper.configToolImagePath+configVideoTag.stop_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.stop_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.stop_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.stop_p
                nav:[repeat,"",volume,""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: {
                    isExitPopup=true
                    if (pif.vod.getMediaState()==pif.vod.cMEDIA_PLAY ) pif.vod.pause();
                    else pauseVideo()
                }
            }
            Slider {
                id: progressBar
                anchors.left: videoPanelBGBack.left
                anchors.leftMargin: (viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_progress_lm') :  qsTranslate('','video_trailer_progress_lm')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                width: (viewHelper.isMicroAppVideo) ? parseInt(qsTranslate('','aviapp_video_progress_w'),10) : qsTranslate('','video_trailer_progress_w')
                height: (viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_progress_h') : qsTranslate('','video_trailer_progress_h')
                sliderbg:(viewHelper.isMicroAppVideo) ? configTool.config.aviancaapp.screen.app_progress_empty :configVideoTag.base_image_trailer
                sliderFillingtItems: [configVideoTag.fill_color_trailer,configVideoTag.indicator]
                stepsValue: 1
                maxLevel: pif.vod.getMediaEndTime()
                isFill: true
                enableDragging: isVideoPlayable
                onValueChanged:{
                    var count=Math.round(sliderDotImg.anchors.leftMargin/progressBar.shiftingPerClick)
                    pif.vod.setMediaPosition(progressBar.stepsValue * count)
                    if(!isDragged)actiononTimer()
                }
                onDragingOn: {
                    var elapseTime=pif.vod.getMediaEndTime()-shiftingValueForDragging
                    elapsedTime.text=core.coreHelper.getTimeFormatFromSecs(elapseTime, 2)
                }
            }
            ViewText{
                id:elapsedTime
                anchors.right: progressBar.left
                anchors.rightMargin: (viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_progress_time_gap') : qsTranslate('','video_trailer_progress_time_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                varText: [
                    "",
                    configVideoTag.text_color,
                    (viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_progress_time_fontsize') : qsTranslate('','video_trailer_progress_time_fontsize')
                ]

            }
            ViewText{
                id:totalTime
                anchors.left: progressBar.right
                anchors.leftMargin: qsTranslate('','video_trailer_progress_time_gap')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                varText: [
                    "- "+(progressBar.isDragged?core.coreHelper.convertSecToHMSLeadZero(progressBar.remainingValue):core.coreHelper.convertSecToHMSLeadZero(pif.vod.getmediaRemainTime())),
                    configVideoTag.text_color,
                    (viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_progress_time_fontsize') : qsTranslate('','video_trailer_progress_time_fontsize')
                ]
            }
            SimpleNavButton {
                id: volume
                anchors.left:videoPanelBGBack.left
                anchors.leftMargin: (viewHelper.isMicroAppVideo) ? qsTranslate('','aviapp_video_btns_lm1') : qsTranslate('','video_trailer_btns_lm1')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus
                isSelected:  btnSelcted=="volume" && parent.activeFocus && isVolumeOpen?true:false
                normImg:  viewHelper.configToolImagePath+configVideoTag.volume_n
                highlightimg: viewHelper.configToolImagePath+configVideoTag.volume_h
                selectedImg: viewHelper.configToolImagePath+configVideoTag.volume_p
                pressedImg: viewHelper.configToolImagePath+configVideoTag.volume_p
                nav:[stop,"",(viewHelper.isMicroAppVideo)?"":playMovie,""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased: {
                    btnSelcted="volume"
                    setIndicator("vol",true)
                    actiononTimer(false)
                    volumeSlider.forceActiveFocus()
                }
            }
            VolumeSlider{
                id:volumeSlider
                visible: isVolumeOpen
                anchors.bottom: volume.bottom
                anchors.left: volume.left
                anchors.leftMargin:  qsTranslate('','video_volube_bar_lm')
                anchors.bottomMargin: (viewHelper.isMicroAppVideo)? qsTranslate('','aviapp_video_volube_bar_bm') : qsTranslate('','video_volube_bar_bm')
                source: viewHelper.configToolImagePath+configTool.config.global.video.volume_popup
                volSliderComp:[
                    (viewHelper.isMicroAppVideo)? qsTranslate('','aviapp_video_volube_bar_h') : qsTranslate('','video_volube_bar_h'),
                                                  (viewHelper.isMicroAppVideo)? qsTranslate('','aviapp_video_volube_bar_w') : qsTranslate('','video_volube_bar_w')
                ]
                volConfigTag:configVideoTag;
                forVod: true;
                onSliderEnds:{
                    if(dir=="left"){
                        setIndicator("vol",false)
                        stop.forceActiveFocus();
                    }
                    //                    else if(dir=="right"){
                    //                        setIndicator("vol",false)
                    //                        playMovie.forceActiveFocus();
                    //                    }
                    else if((dir=="right")&&(viewHelper.isMicroAppVideo == "false")){
                        setIndicator("vol",false)
                        playMovie.forceActiveFocus();
                    }
                }
                onPadReleased:{
                    actiononTimer(false);
                }
                onVisibleChanged: if(visible)volumeSliderComp.setInitailValue=pif.getVolumeLevel()
                onActiveFocusChanged: if(!activeFocus){if(!hidePanel.running)isVolumeOpen=false}
            }
            SimpleNavBrdrBtn{
                id:playMovie
                //                focus: true;
                opacity: (viewHelper.isMicroAppVideo) ? 0 : 1;
                focus: (viewHelper.isMicroAppVideo) ? false : true;
                width:qsTranslate('','video_trailer_play_w')
                height: qsTranslate('','video_trailer_play_h')
                btnTextWidth:parseInt(qsTranslate('',"video_trailer_play_textw"),10);
                buttonText: [configTool.language.movies.play,configVideoTag.text_color,qsTranslate('','video_trailer_play_fontsize')]
                anchors.left: videoPanelBGBack.left
                anchors.leftMargin: qsTranslate('','video_trailer_play_lm')
                anchors.verticalCenter: videoPanelBGBack.verticalCenter
                isHighlight: activeFocus
                normalImg:   viewHelper.configToolImagePath+configVideoTag.vod_btn_n
                highlightImg:  viewHelper.configToolImagePath+configVideoTag.vod_btn_h
                               //(viewHelper.isKids(current.template_id)?configVideoTag.vod_mv_btn_h:configVideoTag.vod_btn_h)
                pressedImg: viewHelper.configToolImagePath+configVideoTag.vod_btn_p
                normalBorder: [qsTranslate('',"video_trailer_play_margin")]
                highlightBorder: [qsTranslate('',"video_trailer_play_margin")]
                nav:[volume,"","",""]
                onNoItemFound:{ actiononTimer(); if(dir=="up")moveUp();}
                onPadReleased: actiononTimer()
                onEnteredReleased:  {
                    core.dataController.getMediaByMid([viewHelper.getTrailerParentMid()],playMovieFromTrailer)
                }
            }
        }
    }
    Component{
        id:blankComp
        Item{}
    }
    ParallelAnimation{
        id:hidePanel
        SequentialAnimation{
            ScriptAction{
                script: {
                    showHeaderFooter=!showHeaderFooter
                    if(viewController.getActiveSystemPopupID()==0){
                        if(!showHeaderFooter && !isSoundTrackOpen) videoAreaObj.forceActiveFocus()
                        else if(!isSoundTrackOpen) videoPanelLoaderObj.videoPanelLoader.item.forceActiveFocus()
                    }

                    if(!showHeaderFooter){
                        if(isVolumeOpen){ isVolumeOpen=false; videoPanelLoaderObj.videoPanelLoader.item.volume.focus=true;}
                        if(isBrightnessOpen){isBrightnessOpen=false; videoPanelLoaderObj.videoPanelLoader.item.brightness.focus=true;}
                    }
                }
            }
            ParallelAnimation{
                NumberAnimation{target: widgetList.getHeaderRef();property: "anchors.topMargin";from:showHeaderFooter?0:(viewHelper.isTvSeries?-200:-100);to:showHeaderFooter?(viewHelper.isTvSeries?-200:-100):0;duration: 800}
                NumberAnimation{target: videoPanelLoaderObj.videoPanelLoader.item.videoPanelBGBack;property: "anchors.bottomMargin";from:showHeaderFooter?82:-100;to:showHeaderFooter?-100:82;duration:800}
                NumberAnimation{target: indicatorsObj.episodeFS;property: "anchors.topMargin";from:showHeaderFooter?qsTranslate('','video_tv_btn_tm'):-100;to:showHeaderFooter?-100:qsTranslate('','video_tv_btn_tm');duration:800}
            }
        }
    }
    ParallelAnimation{
        id:hideonlyVideoPanel
        SequentialAnimation{
            ScriptAction{
                script: {
                    showHeaderFooter=!showHeaderFooter
                }
            }
            ParallelAnimation{
                NumberAnimation{target: videoPanelLoaderObj.videoPanelLoader.item.videoPanelBGBack;property: "anchors.bottomMargin";from:showHeaderFooter?82:-100;to:showHeaderFooter?-100:82;duration:800}
                NumberAnimation{target: indicatorsObj.episodeFS;property:"anchors.topMargin";from:showHeaderFooter?qsTranslate('','video_tv_btn_tm'):-100;to:showHeaderFooter?-100:qsTranslate('','video_tv_btn_tm');duration:800}
            }
        }
    }
    ParallelAnimation{
        id:hideonlyHeader
        SequentialAnimation{
            ParallelAnimation{
                NumberAnimation{target: widgetList.getHeaderRef();property: "anchors.topMargin";from:0;to:viewHelper.isTvSeries?-200:-100;duration: 400}
                //NumberAnimation{target: indicatorsObj.episodeFS;property: "anchors.topMargin";from:showHeaderFooter?qsTranslate('','video_tv_btn_tm'):-100;to:showHeaderFooter?-100:qsTranslate('','video_tv_btn_tm');duration:800}
            }
            ScriptAction{
                script: {
                    if(viewController.getActiveScreenPopupID())return;
                    if(bckTakeFocus)videoAreaObj.forceActiveFocus()
                    else bckTakeFocus=true
                }
            }
        }
    }

    UserEventFilter{
        enabled: visible?true:false
        onKeyPressed: {}
        onKeyReleased: {}
        onMousePressed: {actiononTimer(true)}
        onMouseReleased: {}
    }
}

