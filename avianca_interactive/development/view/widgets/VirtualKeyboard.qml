import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: virtualKeyboard
    visible:false
    /*****************************************************************************************************/
    Connections{
        target:(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)?pif.sharedModelServerInterface:null
        onSigDataReceived:{
            core.debug("VKB | onSigDataReceived "+api);
            switch(api){
            case "keyboardData":{
                sendSignal = false;
                if(keyBoard.inputText.text == params.keyboardData)return;
                else{
                    keyBoard.inputText.text = params.keyboardData;
                    keyBoard.inputText.cursorPosition=keyBoard.inputText.text.length;
                }
            }
            break;
            case "userDefinedVKBData":{
                core.debug("params.vkbData ========================  "+params.vkbData)
                core.debug("params.vkbData ========================  "+params.identifier)
                core.debug("params.vkbData ========================  "+viewHelper.identifier)
                confirmData(params.vkbData,viewHelper.identifier);
            }
            break;
            }
        }
    }
    /*****************************************************************************************************/
    property variant configVkbTag: configTool.config.vkb.screen

    property variant vkbNormalModel;
    property variant vkbShiftModel;
    property variant nav:[-1,-1,-1,-1];
    property bool sendSignal : false
    property bool navigableKeyboard: false
    property variant currentModel:(toggle)?vkbShiftModel:vkbNormalModel;
    property TextEdit textControl;
    property bool enterToPress:false
    property bool vkbGenerated:false;
    property variant modelArray:{"eng":[engRegular,shiftModel]}
    property Component buttonDelegate:btnDel
    property variant jsnRef1;
    property bool toggle : false;
    property string langISO: "eng";
    property variant listViewsRef;
    property bool isSuggPopupOpen: false
    property int selectedRowNumber: 0;
    property int curItemX:0
    property int curItemWidth:0
    property string alphabets: "abcdefghijklmnopqrstuvwxyz";
    property int selectedColNumber:0;
    property bool fromSendKey:false;
    property bool isShiftON: false;
    property bool isCapsON: false;
    property int pressHoldTimerFlag:0;
    property variant suggestion:extendedModel.model;
    property int characterLimit:(viewHelper.identifier=="chat_nickname" )?15:150
    property int lineLimit:(viewHelper.identifier=="chat_nickname")?1:3
    property int  valueForGap:0
    property int valueForY:((parseInt(qsTranslate('','vkb_btn_alpha_tm'),10)+parseInt(qsTranslate('','vkb_btn_alpha_tm1'),10)+parseInt(qsTranslate('','vkb_btn_alpha_tm2'),10)+parseInt(qsTranslate('','vkb_btn_alpha_tm3'),10))/3)+parseInt(qsTranslate('','vkb_btn_area_border'))-parseInt(qsTranslate('','vkb_btn_area_vgap'),10)
    signal specialKeyPressed(variant keyCode)
    signal performAction()
    signal closeAllSuggetions()
    VKBEnglishModel{id:engRegular}
    VKBEnglishShiftModel{id:shiftModel}
    VKBExtendedKeyModel{id:extendedModel}
    /************FROM basic virtual keyboard comp***************/
    property bool capsKeyStatus : false;
    property bool shiftKeyStatus : false;

    /**************************************************************************************************************************/
    function load(){
        push(compOverlay,'overlay')
        push(compKeyBoard,"keyBoard")
    }
    function loadVkb(){
        keyBoard.inputText.text=viewHelper.vkbInputData
        visible=true
        createKeyboard()
    }

    function init(){
        core.debug("VKB.qml | init")
        loadVkb()
        setTextBoxParameters();
    }

    function reload(){}
    function clearOnExit(){
        core.debug("VKB.qml | clearOnExit "+"")
        visible=false
        toggle=false
        isShiftON=false
        isCapsON=false
        viewHelper.vkbClosed(viewHelper.identifier)
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    Timer{
        id:pressHoldTimer;
        interval: 1500;
        onTriggered: {
            if(pressHoldTimerFlag == 1){
                if(preSendKey(getSelectedValues().key_code))
                    pressHoldTimerFlag = 0;
            }
        }
    }
    onPerformAction: {core.debug("onPerformAction | identifier = "+viewHelper.identifier);confirmData(keyBoard.inputText.text,viewHelper.identifier)}
    function confirmData(text, identifier){
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:false});
        }
        viewHelper.vkbData(text,identifier)
    }
    function createKeyboard(){
        textControl=keyBoard.inputText
        textControl.forceActiveFocus()
        core.debug("CompVKB.qml | createKeyboard()")
        if(vkbGenerated == true){
            core.debug("Notice : Virtual keyboard already created.......");
            return false;
        }
        if(modelArray == [] || buttonDelegate == undefined || jsnRef1.length == 0){
            core.debug("Error : Delegate not set or Models not set for keyboard");
            return false;
        }
        keyBoard.inputText.cursorPosition = keyBoard.inputText.text.length;
        vkbNormalModel = modelArray[langISO][0];
        vkbShiftModel = modelArray[langISO][1];
        var i,tmpListView_N,tmpListView_S;
        var tmplistViewsRef;
        var thisRowModel,thisRowHeight,thisRowWidth,thisRowX,thisRowY,thisSpacing;
        var thisStyle1,thisStyle2;
        core.debug("Generating Keyboard with " + jsnRef1.length + " rows")
        tmplistViewsRef = new Array();
        for(i=0;i<jsnRef1.length;i++){
            thisStyle1 = jsnRef1[i][0];
            if(jsnRef1[i].length >= 1)
                thisStyle2 = jsnRef1[i][1];
            else
                thisStyle2 = thisStyle1;
            tmpListView_N = Qt.createQmlObject('import QtQuick 1.1; ListView{ id:thisList; objectName:"buttonListView' + (i+1) + '_1"; interactive:false; boundsBehavior:Flickable.StopAtBounds;property int listRowNum:' + i + ';model:vkbNormalModel.model[listRowNum];property variant style:[0,0,0,0];x:style[0];y:style[1];width:style[2];spacing:style[3];}',keyBoard.normalPanel, ("buttonListView" + (i+1) + "_1"));
            tmpListView_N.style = thisStyle1;
            tmpListView_N.orientation = ListView.Horizontal;
            tmpListView_N.delegate = buttonDelegate;
            tmpListView_S = Qt.createQmlObject('import QtQuick 1.1; ListView{ id:thisList;  objectName:"buttonListView' + (i+1) + '_2"; interactive:false; boundsBehavior:Flickable.StopAtBounds;property int listRowNum:' + i + ';model:vkbShiftModel.model[listRowNum];property variant style:[0,0,0,0];x:style[0];y:style[1];width:style[2];spacing:style[3];}',keyBoard.shiftPanel, ("buttonListView" + (i+1) + "_2"));
            tmpListView_S.style = thisStyle2;
            tmpListView_S.orientation = ListView.Horizontal;
            tmpListView_S.delegate = buttonDelegate;
            tmplistViewsRef[i] = new Array();
            tmplistViewsRef[i] = [tmpListView_N,tmpListView_S];
        }
        listViewsRef = tmplistViewsRef;
        vkbGenerated = true;
        if(  core.pif.vkb!= undefined){
            core.debug("CompVKB.qml | createKeyboard() | Calling selectLanguageVKB('" + langISO + "','" + modelArray[langISO][0] + "','" + modelArray[langISO][1] + "')");
            core.pif.vkb.selectLanguageVKB(langISO,modelArray[langISO][0],modelArray[langISO][1]);
        }
        else{
            core.debug("~~~~~~~~~~~~~~~~ core.pif.vkb.selectLanguageVKB UNDEFINED ~~~~~~~~~~~~~~~~~~~~~"+vkb);
        }
        return true;
    }
    function getCapsCharacter(character){
        if((alphabets.indexOf(character) != -1) && (isCapsON == true))
            return character.toUpperCase();
        else if(character==" ")
            return ""
        else
            return character;
    }
    function setFocus(){
        textControl.focus = true;
        textControl.forceActiveFocus()
        keyBoard.isFocused = true;
    }
    function preSendKey(key_code){
        var isList = (getSuggestionList(key_code));
        if(isList != false){
            showSuggPopup(isList);
            return true;
        }
        return false;
    }
    function enterPressCall(){
        if(getSelectedValues().key_code == "0x01000004"){
            enterToPress = true;
        }
        pressHoldTimerFlag = 1;
        pressHoldTimer.restart();
    }
    function enterCall(){
        if(pressHoldTimerFlag == 1){
            pressHoldTimerFlag = 2;
            fromSendKey = true;
            click();
        }else{
            if(pressHoldTimerFlag == 0)
                preSendKey(getSelectedValues().key_code);
        }
    }
    function onReleased(event){
        if((!navigableKeyboard) || (!keyBoard.isFocused))
            return false;
        if(fromSendKey){
            fromSendKey = false;
            return false;
        }
        if(event.key ==  Qt.Key_Enter || event.key ==  Qt.Key_Return){
            enterCall()
        }
        return false;
    }
    function moveUpDown(dir){ //reference AMX // dir : 0 -> up, 1 -> down
        if(dir == 0)
            keyBoard.inputText.cursorPosition = keyBoard.inputText.positionAt(keyBoard.inputText.cursorRectangle.x,keyBoard.inputText.cursorRectangle.y)
        else
            keyBoard.inputText.cursorPosition = keyBoard.inputText.positionAt(keyBoard.inputText.cursorRectangle.x,keyBoard.inputText.cursorRectangle.y+2*keyBoard.inputText.cursorRectangle.height)
    }
    function capsPressed(status){
        if(status == undefined) isCapsON = (!isCapsON)
        else isCapsON = status;
    }
    function toggleKeys(status){
        if(status == undefined)toggle = (!toggle)
        else toggle = status;
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
           // pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedToggleKeyBoard",{toggleKeyBoard:toggle});
        }

    }

    function onPressed(event){

        if((!navigableKeyboard) || (!keyBoard.isFocused)){
            return false;
        }
        if(fromSendKey){
            fromSendKey = false;
            return false;
        }
        var curRowModel;
        curRowModel = currentModel.model[selectedRowNumber];
        switch(event.key){
        case Qt.Key_Left:{
            if(selectedColNumber>0){
                selectedColNumber--;
                return true;
            }else if(nav[0] != -1){
                keyBoard.isFocused = false;
                nav[0].focus = true;
                return true;
            }
            break;
        }
        case Qt.Key_Right:{
            if(selectedColNumber<(curRowModel.count-1)){
                selectedColNumber++;
                return true;
            }else if(selectedRowNumber == 2 && selectedColNumber == (curRowModel.count-1)){
                if(!toggle){
                    selectedRowNumber = 1;
                    selectedColNumber = 10;
                    return true;
                }else{
                    selectedRowNumber = 1;
                    selectedColNumber = 10;
                    return true;
                }
            }else if(selectedColNumber==(curRowModel.count-1))return true;
            else if(nav[2] != -1){
                keyBoard.isFocused = false;
                nav[2].focus = true;
                return true;
            }
            break;
        }
        case Qt.Key_Down:{
            if(selectedRowNumber<(currentModel.model.length-1)){
                if(!toggle){
                    if(selectedRowNumber == 2 && (selectedColNumber >= 0 && selectedColNumber <= 1))
                        selectedColNumber = 0;
                    else if(selectedRowNumber == 2 && (selectedColNumber >= 2 && selectedColNumber <= 5))
                        selectedColNumber = 1;
                    else if(selectedRowNumber == 2 && (selectedColNumber >= 6))
                        selectedColNumber = (selectedColNumber - 4);
                    else if(selectedRowNumber == 1 && selectedColNumber == 10){
                        selectedRowNumber++;
                        selectedColNumber = 6;
                    }
                }else{
                    if(selectedRowNumber == 2 && (selectedColNumber >= 0 && selectedColNumber <= 1))
                        selectedColNumber = 0;
                    else if(selectedRowNumber == 2 && (selectedColNumber >= 2 && selectedColNumber <= 5))
                        selectedColNumber = 1;
                    else if(selectedRowNumber == 2 && (selectedColNumber >= 6))
                        selectedColNumber = (selectedColNumber - 4);
                    else if(selectedRowNumber == 1 && selectedColNumber == 10){
                        selectedRowNumber++;
                        selectedColNumber = 6;
                    }
                }
                selectedRowNumber++;
                return true;
            }else if(nav[1] != -1){
                keyBoard.isFocused = false;
                nav[1].focus = true;
                return true;
            }
            break;
        }
        case Qt.Key_Up:{
            if(selectedRowNumber>0){
                if(!toggle){
                    if(selectedRowNumber == 3 && selectedColNumber == 1)
                        selectedColNumber = 2;
                    else if(selectedRowNumber == 3 && (selectedColNumber >= 2 && selectedColNumber <=5))
                        selectedColNumber = selectedColNumber + 4;
                    else if(selectedRowNumber == 3 && selectedColNumber == 6){
                        selectedRowNumber--;
                        selectedColNumber = 10;
                    }
                }else{
                    if(selectedRowNumber == 3 && selectedColNumber == 1)
                        selectedColNumber = 2;
                    else if(selectedRowNumber == 3 && (selectedColNumber >= 2 && selectedColNumber <=5))
                        selectedColNumber = selectedColNumber + 4;
                    else if(selectedRowNumber == 3 && selectedColNumber == 6){
                        selectedRowNumber--;
                        selectedColNumber = 10;
                    }
                }
                selectedRowNumber--;
                return true;
            }else if(nav[3] != -1){
                keyBoard.isFocused = false;
                nav[3].focus = true;
                return true;
            }
            break;
        }
        case Qt.Key_Enter:{
            enterPressCall()
            break;
        }
        case Qt.Key_Return:{
            enterPressCall()
            break;
        }
        }
        return false;
    }
    function setSelected(row,col){
        if(row != undefined && row != -1)
            selectedRowNumber = row;
        if(col != undefined && col != -1)
            selectedColNumber = col;
    }
    function sendKey(keyCode,character,modifier){
        core.log("~~~~~~~~~~~~~~~~sendKeys(" + modifier + "," + keyCode + ")")
        if(isShiftON == true)
            shiftPressed(false);
        if(modifier == "CAPS"){
            capsKeyStatus = !capsKeyStatus;
            shiftPressed(capsKeyStatus);
        }else if(modifier == "SHIFT"){
            shiftKeyStatus = true;
            shiftPressed(!capsKeyStatus);
        }else{
            if(modifier==1){
                character = character.toUpperCase();
            }

            keyBoard.postQKeyEvent(keyCode,character,modifier)
            if(shiftKeyStatus==true){
                shiftKeyStatus = false;
                shiftPressed(capsKeyStatus);
            }
        }

    }

    function getSuggestionList(keyCode){
        core.debug("~~~~~~~~~~~keyCode : " + keyCode + "(" + (suggestion[keyCode] != undefined) + ")~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        var modVal;
        modVal = (isCapsON)?1:0;
        if(suggestion[keyCode] != undefined){
            if(suggestion[keyCode][modVal] != undefined)
                return (suggestion[keyCode][modVal]);
            else
                return (suggestion[keyCode]);
        }
        else
            return false;
    }
    function showSuggPopup(model){
        calculateX();
        keyBoard.suggestion_popup.y = (parseInt(keyBoard.brdrBG.y +valueForY+ parseInt(qsTranslate('','vkb_btn_area_tm')) - ( keyBoard.suggestion_popup.height +parseInt( qsTranslate('','vkb_vkb_popup_y'))) +parseInt(getListViewReference().y)));
        keyBoard.suggList.model = model;
        keyBoard.suggestion_popup.visible = true;
    }
    function getSelectedValues(rowNum,colNum){
        if(rowNum == undefined)
            rowNum = selectedRowNumber;
        if(colNum == undefined)
            colNum = selectedColNumber;
        return currentModel.model[rowNum].get(colNum);
    }
    function getListViewReference(rowNumber){
        if(rowNumber == undefined)
            rowNumber = selectedRowNumber;
        if(toggle == false)
            return listViewsRef[rowNumber][0];
        else
            return listViewsRef[rowNumber][1];
    }
    function click(rowIndex,colIndex){
        if(rowIndex != undefined && rowIndex != -1)
            selectedRowNumber = rowIndex;
        if(colIndex != undefined && colIndex != -1)
            selectedColNumber = colIndex;
        var modifier,keyCode,isSpecial, character;
        isSpecial = getSelectedValues().isSpecialKey;
        modifier = getSelectedValues().modifier;
        keyCode = getSelectedValues().key_code;
        character = getSelectedValues().character;
        if(isSpecial == 1){
            if(fromSendKey)
                fromSendKey = false;
            specialKeyPressed(keyCode);
        }else{
            var pressedChar = currentModel.model[selectedRowNumber].get(selectedColNumber).character;
            if((pressedChar.length == 1) && (alphabets.indexOf(pressedChar) != -1) && (isCapsON == true))
                modifier = 1;
            sendKey(keyCode,character,modifier)
        }
    }
    function calculateX(){
        var actualX = (parseInt(keyBoard.brdrBG.x /*+parseInt(keyBoard.mainKeyboard.x)*/ + (curItemX),10));
        var diff = ((actualX - Math.floor(keyBoard.suggestion_popup.width/2)) + Math.ceil(Math.abs(curItemWidth/2)))+valueForGap;
        if(diff < 0){
            keyBoard.suggestion_popup.x = 0;
        }
        else if((diff + keyBoard.suggestion_popup.width) > core.screenWidth){
            diff = (diff - (core.screenWidth - (diff + keyBoard.suggestion_popup.width)))
            keyBoard.suggestion_popup.x = diff;
        }
        else{
            keyBoard.suggestion_popup.x = diff;
        }
    }
    function shiftPressed(status){
        if(status == undefined) isShiftON = (!isShiftON)
        else isShiftON = status;
        capsPressed(isShiftON);
    }
    jsnRef1: [
        [
            [qsTranslate('','vkb_btn_alpha_lm'),qsTranslate('','vkb_btn_alpha_tm'),qsTranslate('','vkb_btn_alpha_w'),qsTranslate('','vkb_btn_area_hgap')],
            [qsTranslate('','vkb_btn_num_lm'),qsTranslate('','vkb_btn_alpha_tm'),qsTranslate('','vkb_btn_num_w'),qsTranslate('','vkb_btn_area_hgap')]
        ],
        [
            [qsTranslate('','vkb_btn_alpha_lm1'),qsTranslate('','vkb_btn_alpha_tm1'),qsTranslate('','vkb_btn_alpha_w1'),qsTranslate('','vkb_btn_area_hgap1')],
            [qsTranslate('','vkb_btn_num_lm1'),qsTranslate('','vkb_btn_alpha_tm1'),qsTranslate('','vkb_btn_num_w1'),qsTranslate('','vkb_btn_area_hgap1')]
        ],
        [
            [qsTranslate('','vkb_btn_alpha_lm2'),qsTranslate('','vkb_btn_alpha_tm2'),qsTranslate('','vkb_btn_alpha_w2'),qsTranslate('','vkb_btn_area_hgap2')],
            [qsTranslate('','vkb_btn_num_lm'),qsTranslate('','vkb_btn_alpha_tm2'),qsTranslate('','vkb_btn_num_w1'),qsTranslate('','vkb_btn_area_hgap2')]
        ],
        [
            [qsTranslate('','vkb_btn_alpha_lm3'),qsTranslate('','vkb_btn_alpha_tm3'),qsTranslate('','vkb_btn_alpha_w3'),qsTranslate('','vkb_btn_area_hgap3')],
            [qsTranslate('','vkb_btn_num_lm3'),qsTranslate('','vkb_btn_alpha_tm3'),qsTranslate('','vkb_btn_num_w3'),qsTranslate('','vkb_btn_area_hgap3')]
        ]
    ];
    onSpecialKeyPressed: {
        switch(keyCode){
        case "upperCase":
        {
            capsPressed();
            break;
        }
        case 0:{
            viewController.hideScreenPopup()
            break;
        }
        case "TOGGLE":{
            toggleKeys();
            break;
        }
        case "SHIFT":{
            shiftPressed();
            break;
        }
        case "dArrow":{
            moveUpDown(1)
            break;
        }
        case "uArrow":{
            moveUpDown(0)
            break;
        }
        case "lArrow":{
            if(keyBoard.inputText.cursorPosition>0)
                keyBoard.inputText.cursorPosition--;
            break;
        }
        case "rArrow":{
            if(keyBoard.inputText.cursorPosition<keyBoard.inputText.text.length)
                keyBoard.inputText.cursorPosition++;
            break;
        }
        case "hide_key":{
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:false});
            }
            clearOnExit()
            break;
        }
        }
    }
    /*****************************************************************************************************************************/
    function confirmBtnSelected(){
        core.debug("confirmBtnSelected | identifier= "+viewHelper.identifier)
        confirmData(keyBoard.inputText.text,viewHelper.identifier)
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:false});
        }
        clearOnExit()
    }
    function cancelBtnSelected(){
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:false});
        }
        clearOnExit()
    }
    function showVkb(val,identifier){
        if(val){
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("keyboardData",{keyboardData:keyBoard.inputText.text});
                keyBoard.inputText.cursorPosition = keyBoard.inputText.text.length;
                pif.sharedModelServerInterface.sendApiData("userDefinedVKBLimit",{vkbCharLimit:characterLimit,vkbLineLimit:lineLimit,vkb_identifier:identifier})
                pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:true});
            }else{
                init()
            }
        }
        else{
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:false});
            }

            clearOnExit()
        }
    }
    function setTextBoxParameters(){
        // keyBoard.inputText.text =""// viewHelper.vkbInput
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendApiData("keyboardData",{keyboardData:keyBoard.inputText.text});
        }
        nav=[-1,-1,-1,keyBoard.inputText]
        navigableKeyboard= true;
        keyBoard.inputText.cursorPosition = keyBoard.inputText.text.length;
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendApiData("userDefinedVKBLimit",{vkbCharLimit:characterLimit,vkbLineLimit:lineLimit,vkb_identifier:viewHelper.identifier})
            pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:true});
        }
    }
    /**************************************************************************************************************************/
    property QtObject overlay
    Component{
        id:compOverlay
        Rectangle{
            width: core.width
            height: core.height
            color:"#000000";
            opacity:0.8;
            MouseArea{
                anchors.fill:parent;
                onClicked:{

                }
            }
        }
    }
    property QtObject keyBoard
    Component{
        id:compKeyBoard
        Item{
            anchors.fill:parent
            property bool isFocused: activeFocus
            property alias normalPanel: normalPanel
            property alias shiftPanel: shiftPanel
            property alias inputText: inputText
            property alias suggestion_popup: suggestion_popup
            property alias  brdrBG:vkbBgPanel
            property alias suggList: suggList
            property alias  mainKeyboard:mainKeyboard
            function postQKeyEvent(keyCode,character,modifierVal){
                var pos=parseInt(inputText.cursorPosition,10);
                if(keyCode=="0x01000003"){
                    var a=inputText.text.substring(0,(inputText.cursorPosition-1));
                    var b=inputText.text.substring(inputText.cursorPosition,inputText.length);
                    inputText.text = a+b;
                    inputText.cursorPosition=pos-1;
                    return;
                }else if(keyCode=="0x01000004" && inputText.cursorRectangle.y<=(inputText.cursorRectangle.height)){
                    inputText.text+="\n";
                    inputText.cursorPosition=inputText.text.length
                    return;
                }
                if(inputText.text.length >= characterLimit)return;
                if(pos!=inputText.length){
                    var temp=inputText.text
                    var d= temp.substring(0,pos);
                    inputText.text =d+character+temp.substring(pos,temp.length);
                }else
                    inputText.text+=character;
                inputText.cursorPosition=pos+1;

            }
            Image{
                id:vkbBgPanel
                source:viewHelper.configToolImagePath+configVkbTag.vkb_panel
                anchors.centerIn:parent
            }
            Image{
                id:textBoxBg
                source:viewHelper.configToolImagePath+configVkbTag.text_bg
                anchors.top:vkbBgPanel.top;anchors.topMargin:qsTranslate('','vkb_text_tm')
                anchors.left:vkbBgPanel.left;anchors.leftMargin:qsTranslate('','vkb_text_lm')
            }
            Text {
                id:rem_Txt
                text: configTool.language.survey.remaining_text
                font.family:viewHelper.adultFont
                font.pixelSize:qsTranslate('','vkb_text_remaining_fontsize')
                color:configVkbTag.limit_text
                anchors.right:rem_count.left
                anchors.bottom: textBoxBg.bottom;anchors.bottomMargin: qsTranslate('','vkb_text_remaining_bm')
            }
            Text {
                id:rem_count;
                text:(characterLimit - inputText.text.length);
                font.family:viewHelper.adultFont
                font.pixelSize:qsTranslate('','vkb_text_remaining_fontsize')
                color:configVkbTag.limit_text
                anchors.right:textBoxBg.right;anchors.rightMargin: qsTranslate('','vkb_text_remaining_rm')
                anchors.bottom: textBoxBg.bottom;anchors.bottomMargin: qsTranslate('','vkb_text_remaining_bm')


            }
            TextEdit{
                id:inputText
                width: qsTranslate('','vkb_text_w')
                height:  qsTranslate('','vkb_text_h')
                anchors.centerIn: textBoxBg
                activeFocusOnPress:false
                cursorVisible: true;
                font.family:viewHelper.adultFont
                font.pixelSize:qsTranslate('','vkb_text_fontsize')
                color:configVkbTag.text_color
                clip: true;
                text: ""
                wrapMode: TextEdit.Wrap;
                onTextChanged: {
                    if(inputText.lineCount > lineLimit  ){
                        inputText.text = inputText.text.substring(0,inputText.cursorPosition-1) + inputText.text.substring(inputText.cursorPosition,inputText.text.length)
                        cursorPosition=inputText.text.length
                    }
                    else if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET && sendSignal==true){
                        pif.sharedModelServerInterface.sendApiData("keyboardData",{keyboardData:keyBoard.inputText.text});
                    }
                    sendSignal =false
                }
                Keys.onReleased: {
                    if(virtualKeyboard.onReleased(event)){
                        event.accepted = true
                    }
                }
                Keys.onPressed:{
                    sendSignal = true;
                    if(virtualKeyboard.onPressed(event)){
                        event.accepted = true;
                    }
                    else{
                        switch(event.key){
                        case Qt.Key_Right:{
                            if((inputText.positionToRectangle(inputText.cursorPosition).x+5)>inputText.paintedWidth){
                                if(confirm_button.isActive)
                                    confirm_button.focus = true;
                                else
                                    cancel_button.focus = true;
                                event.accepted = true; // so use vkb enter button
                            }
                            break;
                        }
                        case Qt.Key_Down:{
                            var curRect = inputText.positionToRectangle(inputText.cursorPosition);
                            if((curRect.y == 0 && inputText.lineCount == 1) || ((curRect.y) >= (inputText.paintedHeight - curRect.height)))
                            {setFocus()}
                            break;
                        }
                        case Qt.Key_Left:
                        case Qt.Key_Up:
                        case Qt.Key_Delete:
                        case Qt.Key_Backspace:
                        case Qt.Key_Shift:
                        case Qt.Key_CapsLock:{
                            break;
                        }
                        case Qt.Key_Return:{
                            if(!enterToPress && keyBoard.isFocused){
                                event.accepted = true; // so use vkb enter button
                            }
                            enterToPress = false
                            break;
                        }
                        case Qt.Key_Enter:{
                            if(!enterToPress && keyBoard.isFocused){
                                event.accepted = true; // so use vkb enter button
                            }
                            enterToPress = false
                            break;
                        }
                        default:{
                            if(characterLimit >= 0){
                                if(inputText.text.length == characterLimit){
                                    event.accepted = true;
                                }
                            }
                            break;
                        }
                        }
                    }
                }

            }

            SimpleNavButton{
                id:confirm_button
                normImg: viewHelper.configToolImagePath+configVkbTag.confirm_n
                highlightimg: viewHelper.configToolImagePath+configVkbTag.confirm_h
                pressedImg: viewHelper.configToolImagePath+configVkbTag.confirm_p
                isDisable: ( inputText.text.trim().length >= 1)?false:true
                anchors.top:vkbBgPanel.top;anchors.topMargin: qsTranslate('','vkb_accept_tm')
                anchors.right:vkbBgPanel.right;anchors.rightMargin: qsTranslate('','vkb_accept_rm')
                onActionReleased: {confirmBtnSelected()}
                nav:[inputText,"","",cancel_button]
            }
            SimpleNavButton{
                id:cancel_button
                normImg: viewHelper.configToolImagePath+configVkbTag.close_btn_n
                highlightimg: viewHelper.configToolImagePath+configVkbTag.close_btn_h
                pressedImg: viewHelper.configToolImagePath+configVkbTag.close_btn_p
                anchors.top:confirm_button.bottom;anchors.topMargin: qsTranslate('','vkb_accept_vgap')
                anchors.right:vkbBgPanel.right;anchors.rightMargin: qsTranslate('','vkb_accept_rm')
                onActionReleased: {cancelBtnSelected()}
                nav:[inputText,(confirm_button.isDisable)?"":confirm_button,"",keyBoard]
                onPadReleased: {
                    if(dir=="down"){
                        textControl.focus = true;
                        textControl.forceActiveFocus()
                        keyBoard.isFocused=true
                    }
                }
            }
            FocusScope{ //focus scope where actual keys will come
                id:mainKeyboard
                height: qsTranslate('','vkb_btn_area_h')
                width:  qsTranslate('','vkb_btn_area_w')
                anchors.horizontalCenter: vkbBgPanel.horizontalCenter
                anchors.top:textBoxBg.bottom;anchors.topMargin: qsTranslate('','vkb_btn_area_tm')
                FocusScope{
                    id:normalPanel;
                    anchors.fill: parent;
                    visible: (!(toggle));
                }
                FocusScope{
                    id:shiftPanel;
                    anchors.fill: parent;
                    visible: toggle;
                }
            }
            Rectangle{  //transparent rect for closing any suggesttion boxes
                height: core.height;
                width: core.width;
                color: "transparent";
                visible: suggestion_popup.visible;
                MouseArea{
                    anchors.fill: parent;
                    onClicked: {
                        suggestion_popup.closeSuggPopup();
                    }
                }
            }
            Image{
                id:extended_arrow
                source:  viewHelper.configToolImagePath+configVkbTag.btn_popup_indicator
                visible:  suggestion_popup.visible;
                anchors.horizontalCenter: suggestion_popup.horizontalCenter
                anchors.top: suggestion_popup.bottom;
            }
            BorderImage {
                id: suggestion_popup
                visible: false;
                source : viewHelper.configToolImagePath+configVkbTag.btn_popup_bg
                width: parseInt((Math.ceil(suggList.count/2)*(qsTranslate('','vkb_vkb_popup_w1'))) )
                onWidthChanged: {calculateX();}
                height:qsTranslate('','vkb_vkb_popup_h')
                border{
                    left:qsTranslate('','vkb_vkb_popup_border')
                    right:qsTranslate('','vkb_vkb_popup_border')
                    top:qsTranslate('','vkb_vkb_popup_border')
                    bottom:qsTranslate('','vkb_vkb_popup_border')
                }
                function sendSuggKey(index){
                    var modelData;
                    if(index == undefined)
                        index = suggList.currentIndex;
                    modelData = suggList.model[index];
                    sendKey(modelData["key_code"],modelData["character"],modelData["modifier"])
                    closeSuggPopup()
                }
                onVisibleChanged: {
                    if(visible == true){
                        suggestion_popup.forceActiveFocus();
                        suggList.focus = true;
                        suggList.currentIndex = 0;
                    }
                }
                function closeSuggPopup(){
                    setFocus();
                    suggestion_popup.visible = false;
                }
                GridView{
                    id:suggList
                    anchors.centerIn: parent
                    width:(qsTranslate('','vkb_vkb_popup_w1') * Math.ceil(count/2));
                    height:(qsTranslate('','vkb_vkb_popup_h1') * 2);
                    cellHeight: qsTranslate('','vkb_vkb_popup_h1');
                    cellWidth:qsTranslate('','vkb_vkb_popup_w1')
                    focus:  suggestion_popup.visible;
                    interactive:false;
                    property bool pressedHere: false
                    Repeater{
                        model:(Math.ceil(suggList.count/2)-1)
                        Image{
                            width: 1;height:sourceSize.height*2
                            x:(suggList.cellWidth * (index+1))
                            source:viewController.settings.assetImagePath+qsTranslate('','vkb_vkb_popup_source_h')
                        }
                    }
                    Image{
                        width: suggestion_popup.width;height: 1;anchors.centerIn: parent
                        source:viewController.settings.assetImagePath+qsTranslate('','vkb_vkb_popup_source_v')
                    }
                    Keys.onPressed: {
                        pressedHere = true;
                        switch(event.key){
                        case Qt.Key_Right:{
                            suggList.moveCurrentIndexRight();
                            break;
                        }
                        case Qt.Key_Left:{
                            suggList.moveCurrentIndexLeft();
                            break;
                        }
                        case Qt.Key_Up:{
                            suggList.moveCurrentIndexUp();
                            break;
                        }
                        case Qt.Key_Down:{
                            if(suggList.currentIndex + (Math.ceil(suggList.count/2)) <= (suggList.count-1))
                                suggList.currentIndex=suggList.currentIndex +(Math.ceil(suggList.count/2))
                            else   keyBoard.suggestion_popup.closeSuggPopup();
                            event.accepted = true;
                            break;
                        }
                        }
                    }
                    Keys.onReleased: {
                        if(pressedHere == false) return;
                        pressedHere = false;
                        if(event.isAutoRepeat == true) return;
                        if(event.key == Qt.Key_Enter ||event.key == Qt.Key_Return ){
                            keyBoard.suggestion_popup.sendSuggKey();
                            event.accepted = true;
                        }
                    }
                    delegate: Rectangle{
                        id:thisDel
                        width:qsTranslate('','vkb_vkb_popup_w1')
                        height:qsTranslate('','vkb_vkb_popup_h1')
                        property bool isFocused:true//(GridView.isCurrentItem);
                        color: "transparent"
                        Image {
                            source:viewHelper.configToolImagePath+configVkbTag.btn_popup_h
                            anchors.centerIn: thisDel;
                            visible: suggList.currentIndex==index && viewHelper.showHighlight
                        }
                        Text {
                            id:sugg_txt
                            text:modelData["character"];
                            width: paintedWidth;
                            height: paintedHeight;
                            anchors.centerIn: parent;
                            visible: (text != "")?true:false;
                            font.family:viewHelper.adultFont
                            font.pixelSize:qsTranslate('','vkb_text_fontsize')
                            color:"white"
                            textFormat: Text.RichText;
                        }

                        MouseArea{
                            anchors.fill: parent;
                            onPressed: {
                                suggList.currentIndex = index;
                            }
                            onReleased: {
                                keyBoard.suggestion_popup.sendSuggKey(index);
                            }
                        }
                    }
                }
            }

        }
    }

    Component{
        id:btnDel;
        Item {
            id: thisItem
            height:qsTranslate('',curJsnRef+'_h')
            width: qsTranslate('',curJsnRef+'_w')
            property string btnLabel: (character != undefined)?character:"";
            property string iconSource :(icon != undefined)?icon:"";

            property bool changeCharCase:true/* (changeCase != undefined)?changeCase:true*/;
            property string buttonType;
            property variant curJsnRef:'vkb_'+jsnRef
            property variant parentView:ListView.view
            property bool isFocused:(keyBoard.isFocused == true && isSuggPopupOpen == false /*&& curIndex == index */&& selectedRowNumber == parentView.listRowNum) ;
            property bool isCurXItem:(selectedColNumber == index &&selectedRowNumber == parentView.listRowNum)
            property variant viewCurrentItem: ListView.view;
            BorderImage{
                id:tmpImg;
                height: qsTranslate('',curJsnRef+'_h')
                width: qsTranslate('',curJsnRef+'_w')
                anchors{
                    horizontalCenter: parent.horizontalCenter;
                    top:parent.top;
                    horizontalCenterOffset: (qsTranslate('',curJsnRef+'_lm') != undefined)?parseInt(qsTranslate('',curJsnRef+'_lm')):0;
                }
                border{
                    left: qsTranslate('','vkb_btn_area_border')
                    right: qsTranslate('','vkb_btn_area_border')
                    top: qsTranslate('','vkb_btn_area_border')
                    bottom: qsTranslate('','vkb_btn_area_border')
                }
                source:(curJsnRef)?((msArea.pressed)?((keyBoard.suggestion_popup.visible)?(viewHelper.configToolImagePath+configVkbTag[jsnRef+"_s"] ):(viewHelper.configToolImagePath+configVkbTag[jsnRef+"_h"] )):(keyBoard.suggestion_popup.visible && isCurXItem)?(viewHelper.configToolImagePath+configVkbTag[jsnRef+"_s"] ):(viewHelper.configToolImagePath+configVkbTag[jsnRef+"_n"])):""
                BorderImage {
                    source:viewHelper.configToolImagePath+configVkbTag[jsnRef+"_h"]
                    height: tmpImg.height
                    width: tmpImg.width
                    anchors.centerIn: tmpImg;
                    border{
                        left: qsTranslate('','vkb_btn_area_border')
                        right: qsTranslate('','vkb_btn_area_border')
                        top: qsTranslate('','vkb_btn_area_border')
                        bottom: qsTranslate('','vkb_btn_area_border')
                    }

                    visible:viewHelper.showHighlight && isCurXItem&& (keyBoard.suggestion_popup.visible == false) &&isFocused  && (keyPopupImg.visible==false)
                }
                Text {
                    id:label_txt
                    text:getCapsCharacter(btnLabel);
                    anchors.centerIn: parent;
                    visible: (text != "")?true:false;
                    font.family:viewHelper.adultFont
                    font.pixelSize:qsTranslate('','vkb_text_fontsize')
                    color:"white"
                    textFormat: Text.RichText;
                }

                Image {
                    id: iconImage
                    source: (iconSource != "")?(msArea.pressed)?( viewHelper.configToolImagePath+configVkbTag[iconSource+"_h"]):( viewHelper.configToolImagePath+configVkbTag[iconSource+"_n"]):"";
                    anchors.centerIn: parent;
                    visible: (source != "")?true:false;
                }

                Image{
                    id:keyPopupImg;
                    visible: false;
                    property bool popupToDisplay: (popup_txt.text.length == 1 || popup_txt.text.substring(0,1) == "&") ?true:false;
                    source:viewHelper.configToolImagePath+configVkbTag.vkb_btn_enlarge
                    Text {
                        id:popup_txt
                        anchors.horizontalCenter: parent.horizontalCenter;
                        horizontalAlignment: Text.AlignHCenter;
                        text: label_txt.text;
                        wrapMode: Text.Wrap;
                        height: qsTranslate('','vkb_text_h')
                        y: qsTranslate('','vkb_text_tm')
                        font.family:viewHelper.adultFont
                        font.pixelSize:qsTranslate('','vkb_enlarge_btn_fontsize')
                        color:"white"
                        textFormat: Text.RichText;
                    }
                    anchors.horizontalCenter: parent.horizontalCenter;
                    anchors.bottom: parent.bottom;
                    anchors.bottomMargin:qsTranslate('','vkb_enlarge_btn_bm')
                }
                Connections{
                    target: virtualKeyboard;
                    onCloseAllSuggetions:{
                        keyBoard.suggestion_popup.visible = false;
                        if(keyPopupImg.visible == true){
                            keyPopupImg.visible = false
                        }
                    }
                }
                MouseArea{
                    id:msArea
                    anchors.fill: parent;
                    onPressed: {
                        curItemX = thisItem.x;
                        curItemWidth = thisItem.width;
                        valueForGap=parentView.style[3]

                        sendSignal = true;
                        closeAllSuggetions();
                        setFocus();
                        setSelected(parentView.listRowNum,index)
                        if(keyPopupImg.popupToDisplay){
                            keyPopupImg.visible = true;
                        }
                    }
                    onPressAndHold: {
                        closeAllSuggetions();
                        var isList = (getSuggestionList(key_code));
                        if(isList != false)
                            showSuggPopup(isList);
                    }
                    onReleased: {

                        if(keyPopupImg.popupToDisplay && keyPopupImg.visible == true){
                            keyPopupImg.visible = false;
                        }
                        if(!keyBoard.suggestion_popup.visible)
                            click();
                    }
                }

            }
        }
    }
    Component.onCompleted:{
        loadVkb();
        clearOnExit();
    }
}
