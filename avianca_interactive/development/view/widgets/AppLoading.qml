import QtQuick 1.0
import "../../framework/viewcontroller"
import "../components/"
SmartLoader{
    id:appLoading
    onVisibleChanged:{
        if(visible){
            bg.forceActiveFocus();
            hideAppLoad.restart();
        }else{
            hideAppLoad.stop();
        }
    }
    Connections{
        target:pif.launchApp;
        onSigAppExit:{
            viewHelper.gameMid=-1;
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedGameExit",{launch_id:launchid})
                viewHelper.isLaunchingAppFromKarma=false;

            if(widgetList.getSoundTrackSubtitleRef().visible)
                widgetList.getSoundTrackSubtitleRef().focusTosoundTrackSubtitle()
            else viewController.getActiveScreenRef().focus=true;

            if(pif.launchApp.launchId==viewHelper.voyager3dAppName){
                if(pif.launchApp.forceStopPackage)pif.launchApp.forceStopPackage(viewHelper.voyager3dAppName)
            }else{
                if(pif.launchApp.forceStopPackage)pif.launchApp.forceStopPackage(viewHelper.gamePath)
            }
           // viewController.getActiveScreenRef().focus=true;
            visible=false;
        }
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {}
    }
    Rectangle{
        anchors.fill:parent;
        color:"#000000"
        opacity:0.8;
    }
    Rectangle{
        id:bg
        //    x:qsTranslate('','popups_gamesload_bg_lm')
        y:qsTranslate('','popups_gamesload_bg_tm')
        anchors.horizontalCenter: parent.horizontalCenter
        height: qsTranslate('','popups_gamesload_bg_h')
        width: qsTranslate('','popups_gamesload_bg_w')
        radius: qsTranslate('','popups_gamesload_bg_radius')
        color:configTool.config.popup.generic.bg_color;
        Item{
            anchors.centerIn:bg;
            height: qsTranslate('','popups_gamesload_content_h')
            width: (loadImg.width+parseInt(qsTranslate('','popups_gamesload_text_lm'),10)+desc.paintedWidth)
            AnimatedImage{
                id:loadImg
                anchors.left:parent.left;
                anchors.verticalCenter:parent.verticalCenter;
                source:(appLoading.visible)?(viewHelper.isKids(current.template_id)?viewHelper.configToolImagePath+configTool.config.kids_popup.gamelaunch.loading:viewHelper.configToolImagePath+configTool.config.popup.gamelaunch.loading):"";
                paused:(!appLoading.visible)

            }
            ViewText{
                id:desc
                anchors.left:loadImg.right;
                anchors.leftMargin:qsTranslate('','popups_gamesload_text_lm');
                height: qsTranslate('','popups_gamesload_text_h');
                width: qsTranslate('','popups_gamesload_text_w');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[configTool.language.games.loading,configTool.config.popup.generic.text_color,qsTranslate('','popups_gamesload_text_fontsize')]

            }
        }
    }

    Timer{
        id:hideAppLoad;
        interval:20000
        onTriggered:{
            viewController.getActiveScreenRef().focus=true;
            visible=false;
        }
    }
}
