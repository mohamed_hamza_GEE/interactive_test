import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
SmartLoader {
    id:meal
    anchors.fill:parent;

    property variant configTag:configTool.config.meal.listing;
    property variant configSynopTag:configTool.config.meal.synopsis;


    /***************************************************************************************/

    function showMealMenu(flag){
        console.log(">>>>>>>>>>> Visibility fo mainmenu flag "+flag)
        if(flag==meal.visible){
            if(catalogMenu.selValue==viewHelper.storingCatalog_id){
                categoryMenu.visible=false;scroller.opacity=0;
            }
            return;
        }
        if(flag){meal.visible=true;init()}
        else{clearOnExit();meal.visible=false;}
    }

    /***************************************************************************************/

    function load(){
        push(compBackground,"background");
        push(compCloseBTN,"closeBTN");
        push(compCatalogMenu,"catalogMenu");
        push(compSynopsisPoster,"synopsisPoster");
        push(compCategoryBg,"categoryBg");
        push(compCategoryMenu,"categoryMenu");
        push(compScroller,"scroller");
    }

    /***************************************************************************************/

    function init(){
        core.debug("Meal | init ")
        catalogMenu.setModel(dataController.hospitality.getCatalogModel());
        catalogMenu.setDelegate(dataController.hospitality.getAllCatalogModel());
        catalogMenu.forceActiveFocus();
        core.debug("catalogMenu.activeFocus : "+catalogMenu.activeFocus)
        pif.paxus.startContentLog('on board menu')
    }

    /***************************************************************************************/

    function itemModelReady(dmodel){
        core.debug("dmodel.count : "+dmodel.count)

    }

    /***************************************************************************************/

    function clearOnExit(){
        viewController.getActiveScreenRef().forceActiveFocus();
        categoryMenu.visible=false;
        scroller.opacity=0;
        catalogMenu.selValue=-1;
        catalogMenu.setModel(0)
        categoryMenu.setModel(0);
        viewHelper.storingCatalog_id=-1
        pif.paxus.stopContentLog()
    }

    /***************************************************************************************/

    function categoryModelReady(dmodel){
        core.debug("categoryModelReady | dmodel.count : "+dmodel.count)
        categoryMenu.setModel(dmodel)
        categoryMenu.selIndex=0;
        categoryMenu.visible=true;
        if(categoryMenu.count>0){
            categoryMenu.forceActiveFocus();
        }

    }

    function mealBack(){
        if(categoryMenu.visible){
            catalogMenu.selIndex=-1;
            catalogMenu.selValue=-1;
            catalogMenu.forceActiveFocus();
            categoryMenu.visible=false;
            scroller.opacity=0;
            categoryMenu.setModel(0);
        }else{
            showMealMenu(false);
        }
    }

    /***************************************************************************************/




    /***************************************************************************************/

    MouseArea{
        anchors.fill:parent;
        onClicked:{}
    }

    property QtObject background
    Component{
        id:compBackground
        Image{
            id:bg;
            source:viewHelper.configToolImagePath+configTag.background;
            Image{
                id:logo;
                anchors.top:parent.top;
                anchors.left:parent.left;
                anchors.topMargin:qsTranslate('','meal_logo_tm')
                anchors.leftMargin:qsTranslate('','meal_logo_lm')
                source:viewHelper.configToolImagePath+configTag.logo;
            }

        }
    }

    property QtObject closeBTN
    Component{
        id:compCloseBTN
        SimpleNavButton{
            id:closeBTN
            anchors.right: parent.right
            anchors.top:parent.top
            anchors.rightMargin: qsTranslate('','meal_close_rm')
            anchors.topMargin: qsTranslate('','meal_close_tm')
            isHighlight: activeFocus
            normImg:  viewHelper.configToolImagePath+configTag.close_btn_n
            highlightimg:viewHelper.configToolImagePath+configTag.close_btn_h
            pressedImg: viewHelper.configToolImagePath+configTag.close_btn_p
            nav:[catalogMenu,"","",catalogMenu]
            onEnteredReleased:{
                viewController.showScreenPopup(1);
//                showMealMenu(false)
            }
        }
    }

    /***************************************************************************************/

    property QtObject catalogMenu
    Component{
        id:compCatalogMenu;

        ListView{
            id:lv
            focus:true;
            anchors.top:parent.top;
            anchors.topMargin:qsTranslate('','meal_meallist_tm')
            anchors.horizontalCenter:parent.horizontalCenter;
            width:qsTranslate('','meal_meallist_w');
            height:qsTranslate('','meal_meallist_h')
            spacing:qsTranslate('','meal_meallist_hgap')
            //            delegate:count<=3?catMenuBig:catMenuSmall
            interactive:false
            clip:true;
            property int selIndex:-1;
            property variant selValue;

            onActiveFocusChanged:{
                core.debug("activeFocus : "+activeFocus)
            }
            function setModel(catmodel){
                model=catmodel
            }
            function setDelegate(catmodel){
                //if(!viewHelper.isHospitalityCountDefault){
                //viewHelper.isHospitalityCountDefault=true
                viewHelper.totalHospitalityCountDefault=catmodel.count
                //}
                if(viewHelper.totalHospitalityCountDefault<=3)lv.delegate=catMenuBig
                else lv.delegate=catMenuSmall
            }

            Keys.onDownPressed:{if(lv.currentIndex!=(lv.count-1)){lv.currentIndex++;}}

            Keys.onUpPressed:{
                if(lv.currentIndex==0){closeBTN.forceActiveFocus()}
                else{lv.currentIndex--;}
            }

            Keys.onRightPressed:{
                if(categoryMenu.count>0 && categoryMenu.visible){
                    categoryMenu.forceActiveFocus();
                }else if(scroller.opacity==1){
                    scroller.forceActiveFocus();
                }else{
                    closeBTN.forceActiveFocus();
                }
            }

            function getNormIcon(typ){
                if(typ=="lunch"){return configTag.menu_lunch_n}
                else if(typ=="breakfast"){return configTag.menu_breakfast_n}
                else if(typ=="wines"){return configTag.menu_liquors_n}
                else if(typ=="beverages"){return configTag.menu_beverages_n}
            }

            function getHighIcon(typ){
                if(typ=="lunch"){return configTag.menu_lunch_h}
                else if(typ=="breakfast"){return configTag.menu_breakfast_h}
                else if(typ=="wines"){return configTag.menu_liquors_h}
                else if(typ=="beverages"){return configTag.menu_beverages_h}
            }
            function getSelIcon(typ){
                if(typ=="lunch"){return configTag.menu_lunch_s}
                else if(typ=="breakfast"){return configTag.menu_breakfast_s}
                else if(typ=="wines"){return configTag.menu_liquors_s}
                else if(typ=="beverages"){return configTag.menu_beverages_s}
            }

            function getPressIcon(typ){
                if(typ=="lunch"){return configTag.menu_lunch_p}
                else if(typ=="breakfast"){return configTag.menu_breakfast_p}
                else if(typ=="wines"){return configTag.menu_liquors_p}
                else if(typ=="beverages"){return configTag.menu_beverages_p}
            }

            function select(){
                selIndex=currentIndex;
                var value = model.at(selIndex)
                if(selValue==value.catalog_id){
                    categoryMenu.forceActiveFocus();
                    return;
                }
                selValue=value.catalog_id;
                core.dataController.hospitality.getNextList(value,value.catalog_id,"","",categoryModelReady)

            }

            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    select();
                }
            }
        }
    }

    /***************************************************************************************/

    Component{
        id:catMenuBig
        Item{
            width:qsTranslate('','meal_meallist_itemw');
            height:qsTranslate('','meal_meallist_itemh_big')

            Image{
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('','meal_meallist_curve_gap');
                anchors.left:parent.left;
                anchors.leftMargin:qsTranslate('','meal_meallist_curve_big_lm');
                source:viewHelper.configToolImagePath+configTag.curve_image;
                visible:(catalogMenu.selIndex==index && index!=0 && categoryMenu.visible)
            }

            Image{
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('','meal_meallist_curve_gap');
                visible:(catalogMenu.selIndex==index && index!=(catalogMenu.count-1) && categoryMenu.visible)
                rotation:180;
                mirror:true;
                anchors.left:parent.left;
                anchors.leftMargin:qsTranslate('','meal_meallist_curve_big_lm');
                source:viewHelper.configToolImagePath+configTag.curve_image;

            }


            SimpleButton{
                id:menuButton
                normImg:viewHelper.configToolImagePath+configTag.menubtn_big_n;
                highlightimg: viewHelper.configToolImagePath+configTag.menubtn_big_h;
                pressedImg: viewHelper.configToolImagePath+configTag.menubtn_big_p;
                selectedImg:viewHelper.configToolImagePath+configTag.menubtn_big_s;
                anchors.verticalCenter:parent.verticalCenter;
                isHighlight:(catalogMenu.currentIndex==index && catalogMenu.activeFocus)
                isSelected: (catalogMenu.selIndex==index)
                onActionReleased:{
                    catalogMenu.currentIndex=index;
                    catalogMenu.select()

                }
                opacity:(categoryMenu.visible && !isSelected && !isHighlight)?0.8:1;
            }
            SimpleButton{
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('','meal_meallist_icon_big_tm')
                anchors.left:parent.left;
                anchors.leftMargin: qsTranslate('','meal_meallist_icon_big_lm')
                normImg:viewHelper.configToolImagePath+catalogMenu.getNormIcon(value.catalog_type);
                highlightimg: viewHelper.configToolImagePath+catalogMenu.getHighIcon(value.catalog_type);
                pressedImg: viewHelper.configToolImagePath+catalogMenu.getPressIcon(value.catalog_type);
                selectedImg:viewHelper.configToolImagePath+catalogMenu.getSelIcon(value.catalog_type);
                isHighlight:menuButton.isHighlight;
                isPressed:menuButton.isPressed
                isSelected:menuButton.isSelected;
                width:qsTranslate('','meal_meallist_w_icon')
                onActionReleased: menuButton.actionReleased()

                ViewText{
                    id:catalogName
                    anchors.horizontalCenter:parent.horizontalCenter;
                    anchors.top:parent.bottom;
                    horizontalAlignment:Text.AlignHCenter
                    width:qsTranslate('','meal_meallist_text_big_w')
                    height:qsTranslate('','meal_meallist_text_big_h');
                    maximumLineCount:2;
                    wrapMode:Text.WordWrap
                    elide:Text.ElideRight
//                    lineHeight:qsTranslate('','meal_meallist_text_big_lh')
//                    lineHeightMode: Text.FixedHeight
                    lineHeight:0.9
                  //  lineHeightMode :Text.ProportionalHeight

                    property string txtColor:(parent.isPressed)?configTag["menu_btn_text_color_p"]:
                                                                 (parent.isHighlight)?configTag["menu_btn_text_color_h"]:
                                                                                       (parent.isSelected)?configTag["menu_btn_text_color_s"]:
                                                                                                            configTag["menu_btn_text_color_n"]
                    varText:[value.title,txtColor,qsTranslate('',"meal_meallist_text_big_fontsize")]
                }
            }
            Image{
                source:value.catalog_image_thumb;
                width:qsTranslate('',"meal_meallist_post_w");
                height:qsTranslate('',"meal_meallist_post_h_big");
                anchors.right:parent.right;
                anchors.rightMargin:qsTranslate('',"meal_meallist_post_rm");
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('',"meal_meallist_post_tm");
            }
        }
    }

    /***************************************************************************************/

    Component{
        id:catMenuSmall
        Item{
            width:qsTranslate('','meal_meallist_itemw');
            height:qsTranslate('','meal_meallist_itemh_small')

            Image{
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('','meal_meallist_curve_gap');
                anchors.left:parent.left;
                anchors.leftMargin:qsTranslate('','meal_meallist_curve_small_lm');
                source:viewHelper.configToolImagePath+configTag.curve_image_small;
                visible:(catalogMenu.selIndex==index && index!=0 && categoryMenu.visible)
            }

            Image{
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('','meal_meallist_curve_gap');
                visible:(catalogMenu.selIndex==index && index!=(catalogMenu.count-1) && categoryMenu.visible)
                rotation:180;
                mirror:true;
                anchors.left:parent.left;
                anchors.leftMargin:qsTranslate('','meal_meallist_curve_small_lm');
                source:viewHelper.configToolImagePath+configTag.curve_image_small;

            }


            SimpleButton{
                id:menuButton
                normImg:viewHelper.configToolImagePath+configTool.config.meal.listing.menubtn_small_n;
                highlightimg: viewHelper.configToolImagePath+configTool.config.meal.listing.menubtn_small_h;
                pressedImg: viewHelper.configToolImagePath+configTool.config.meal.listing.menubtn_small_p;
                selectedImg:viewHelper.configToolImagePath+configTool.config.meal.listing.menubtn_small_s;
                anchors.verticalCenter:parent.verticalCenter;
                isHighlight:(catalogMenu.currentIndex==index && catalogMenu.activeFocus)
                isSelected: (catalogMenu.selIndex==index)
                opacity:(categoryMenu.visible && !isSelected && !isHighlight)?0.8:1;
                onActionReleased:{
                    catalogMenu.currentIndex=index;
                    catalogMenu.select()
                }

            }
            SimpleButton{
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('','meal_meallist_icon_small_tm')
                anchors.left:parent.left;
                anchors.leftMargin: qsTranslate('','meal_meallist_icon_small_lm')
                normImg:viewHelper.configToolImagePath+catalogMenu.getNormIcon(value.catalog_type);
                highlightimg: viewHelper.configToolImagePath+catalogMenu.getHighIcon(value.catalog_type);
                pressedImg: viewHelper.configToolImagePath+catalogMenu.getPressIcon(value.catalog_type);
                selectedImg:viewHelper.configToolImagePath+catalogMenu.getSelIcon(value.catalog_type);
                isPressed:menuButton.isPressed
                isSelected:menuButton.isSelected;
                isHighlight:menuButton.isHighlight;
                onActionReleased: menuButton.actionReleased()

                ViewText{
                    id:catalogName
                    anchors.verticalCenter:parent.verticalCenter;
                    anchors.left:parent.right;
                    verticalAlignment:Text.AlignVCenter;
                    anchors.leftMargin: qsTranslate('','meal_meallist_text_small_lm')
                    width:qsTranslate('','meal_meallist_text_small_w')
                    height:qsTranslate('','meal_meallist_text_small_h')
                    maximumLineCount:2;
                    wrapMode:Text.WordWrap
                    elide:Text.ElideRight
                    lineHeight:qsTranslate('','meal_meallist_text_small_lh')
                    lineHeightMode: Text.FixedHeight
                    property string txtColor:(parent.isPressed)?configTag["menu_btn_text_color_p"]:
                                                                 (parent.isHighlight)?configTag["menu_btn_text_color_h"]:
                                                                                       (parent.isSelected)?configTag["menu_btn_text_color_s"]:
                                                                                                            configTag["menu_btn_text_color_n"]
                    varText:[value.title,txtColor,qsTranslate('',"meal_meallist_text_small_fontsize")]
                }
            }
            Image{
                source:value.catalog_image_thumb;
                width:qsTranslate('',"meal_meallist_post_w");
                height:qsTranslate('',"meal_meallist_post_h_small");
                anchors.right:parent.right;
                anchors.rightMargin:qsTranslate('',"meal_meallist_post_rm");
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('',"meal_meallist_post_tm");
            }

        }
    }

    /***************************************************************************************/

    property QtObject categoryBg
    Component{
        id:compCategoryBg;
        Image{
            visible:categoryMenu.visible;
            source:viewHelper.configToolImagePath+configSynopTag.synop_bg;
            anchors.top:parent.top;
            anchors.topMargin:qsTranslate('','meal_syn_bg_tm')
            anchors.left:parent.left;
            anchors.leftMargin:qsTranslate('','meal_syn_bg_lm')
            MouseArea{
                anchors.fill:parent;
                onClicked:{}
            }
        }
    }

    /***************************************************************************************/

    property QtObject categoryMenu
    Component{
        id:compCategoryMenu;
        ListView{
            id:lv
            visible:false;
            highlightMoveDuration:10;
            anchors.top:categoryBg.top;
            anchors.topMargin:qsTranslate('','meal_syn_cont_tm')
            anchors.left:categoryBg.left;
            anchors.leftMargin:qsTranslate('','meal_syn_cont_lm')
            width:qsTranslate('','meal_syn_cont_w');
            height:parseInt(qsTranslate('','meal_syn_cont_h'),10)
            delegate:catMenu2
            interactive:false;
            property int selIndex;
            spacing:qsTranslate('','meal_syn_optnbtn_txt_btn_vgap');
            property int selX;
            property int selY;
            property int scrollYOffset:qsTranslate('','meal_scrollcont_tm');

            property variant targetData:currentItem.getTargetData()


            signal setScrollValue();
            clip:true;




            function setModel(catmodel){
                lv.model=0;
                lv.model=catmodel
                scroller.y=y+scrollYOffset
            }

            property variant heightValues:[0,qsTranslate('','meal_syn_cont_h'),
                qsTranslate('','meal_syn_cont_h1'),
                qsTranslate('','meal_syn_cont_h2'),
                qsTranslate('','meal_syn_cont_h3'),
                qsTranslate('','meal_syn_cont_h4')]

            property variant descValues:[0,
                qsTranslate('','meal_syn_menu_des_max_h'),
                qsTranslate('','meal_syn_menu_des_max_h1'),
                qsTranslate('','meal_syn_menu_des_max_h2'),
                qsTranslate('','meal_syn_menu_des_max_h3'),
                qsTranslate('','meal_syn_menu_des_max_h4')]


            property variant scrollValues:[0,qsTranslate('','meal_scrollcont_h'),
                qsTranslate('','meal_scrollcont_h1'),
                qsTranslate('','meal_scrollcont_h2'),
                qsTranslate('','meal_scrollcont_h3'),
                qsTranslate('','meal_scrollcont_h4')]


            property variant baseValues:[0,qsTranslate('','meal_scrollcont_base_h'),
                qsTranslate('','meal_scrollcont_base_h1'),
                qsTranslate('','meal_scrollcont_base_h2'),
                qsTranslate('','meal_scrollcont_base_h3'),
                qsTranslate('','meal_scrollcont_base_h4')]



            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    select();
                }
            }

            Keys.onUpPressed:{
                if(currentIndex==0){closeBTN.forceActiveFocus()}
                else{lv.currentIndex--;}
            }

            Keys.onDownPressed:{

                if(currentIndex==(count-1))return;
                incrementCurrentIndex()

            }

            Keys.onRightPressed:{
                if(scroller.visible){
                    scroller.forceActiveFocus();
                }
            }

            Keys.onLeftPressed:{
                catalogMenu.forceActiveFocus();
            }

            function select(){
                selIndex=currentIndex;

                //scroller.opacity=0;

                //scroller.x=currentItem.x;

            }

            function redrawScroll(){
                setScrollTimer.restart()
            }

            Timer{
                interval:100;
                id:setScrollTimer
                onTriggered:{
                    scroller.y=y+currentItem.y+scrollYOffset
                    targetData=currentItem.getTargetData();
                    setScrollTimer.restart()
                    scroller.opacity=1;
                }
            }
        }
    }

    /***************************************************************************************/

    Component{
        id:catMenu2
        Item{
            width:qsTranslate('','meal_syn_cont_w');
            height:(categoryMenu.selIndex==index)?categoryMenu.heightValues[categoryMenu.count]:optBtn.height;
            clip:true;
            function getTargetData(){
                return loader.item;
            }

            SimpleButton{
                id:optBtn;
                normImg:viewHelper.configToolImagePath+configSynopTag.optionbtn_n;
                highlightimg:viewHelper.configToolImagePath+configSynopTag.optionbtn_h;
                pressedImg:viewHelper.configToolImagePath+configSynopTag.optionbtn_p;
                selectedImg:viewHelper.configToolImagePath+configSynopTag.optionbtn_s;
                isHighlight:(categoryMenu.activeFocus && categoryMenu.currentIndex==index)
                isSelected:(categoryMenu.selIndex==index)


                ViewText{
                    height:qsTranslate('','meal_syn_optnbtn_txt_h');
                    width:qsTranslate('','meal_syn_optnbtn_txt_w');
                    anchors.top:parent.top;
                    anchors.topMargin:qsTranslate('','meal_syn_optnbtn_txt_tm');
                    anchors.horizontalCenter:parent.horizontalCenter;
                    //opacity:(categoryMenu.count>1)?1:0;
                    property string txtColor:(parent.isPressed)?configSynopTag["optionbtn_text_p"]:
                                                                 (parent.isHighlight)?configSynopTag["optionbtn_text_h"]:
                                                                                       (parent.isSelected)?configSynopTag["optionbtn_text_s"]:
                                                                                                            configSynopTag["optionbtn_text_n"]
                    varText:[value.title,txtColor,qsTranslate('','meal_syn_optnbtn_txt_fontsize')]
                }
                onActionReleased: {
                    categoryMenu.currentIndex=index;
                    categoryMenu.select();

                }

            }
            ViewText{
                id:subTitleLabel
                height:qsTranslate('','meal_syn_optn_title_h');
                width:qsTranslate('','meal_syn_optn_title_w');
                maximumLineCount:2;
                lineHeight:qsTranslate('','meal_syn_optn_title_lh');
                lineHeightMode: Text.FixedHeight
                //anchors.top:(categoryMenu.count>1)?optBtn.bottom:optBtn.top;
                anchors.top:optBtn.bottom
                //anchors.topMargin:(categoryMenu.count>1)?qsTranslate('','meal_syn_optn_title_tm'):qsTranslate('','meal_syn_optnbtn_txt_tm');
                anchors.topMargin:qsTranslate('','meal_syn_optn_title_tm')
                anchors.left: optBtn.left;
                anchors.leftMargin:qsTranslate('','meal_syn_optn_title_lm');
                visible:(categoryMenu.selIndex==index)
                varText:[value.sub_title,configSynopTag.title_color,qsTranslate('','meal_syn_optn_title_fontsize')]
            }



            Loader{
                id:loader
                anchors.top:subTitleLabel.bottom;
                anchors.topMargin:qsTranslate('','meal_syn_menu_title_tm');
                anchors.left: subTitleLabel.left;
                sourceComponent:(categoryMenu.selIndex==index)?synopsisCont:emptyDel
                width:parent.width;
                height:(categoryMenu.selIndex==index)?categoryMenu.descValues[categoryMenu.count]:0;
                onStatusChanged:{
                    if(loader.status==Loader.Ready){
                        item.init();
                    }
                }

            }

        }
    }

    /***************************************************************************************/

    Component{
        id:emptyDel;
        Item{function init(){}

        }
    }

    /***************************************************************************************/

    Component{
        id:synopsisCont;
        ListView{
            id:synopsisView
            height:categoryMenu.descValues[categoryMenu.count];
            width:qsTranslate('','meal_syn_menu_des_w');
            clip:true;


            function init(){
                var value = categoryMenu.model.at(categoryMenu.selIndex);
                core.dataController.hospitality.getNextList(value,value.category_node_id,"","",setItemModel)
            }
            function setItemModel(dmodel){
                core.debug(" dmodel.count : "+dmodel.count)
                core.debug(" status : "+status)
                model=dmodel;
            }
            spacing:qsTranslate('','meal_syn_menu_des_vgap')

            delegate:Item{
                width:parent.width;
                height:(itemTitle.paintedHeight+itemDesc.paintedHeight+3)
                ViewText{
                    id:itemTitle
                    varText:[value.title,configSynopTag.menu_title_color,qsTranslate('','meal_syn_menu_title_fontsize')]
                    width:(paintedWidth>qsTranslate('','meal_syn_menu_title_w'))?qsTranslate('','meal_syn_menu_title_w'):paintedWidth;
                    height:qsTranslate('','meal_syn_menu_title_h');

                }

                ViewText{
                    id:itemCalorie
                    varText:[(value.calories&&value.calories!="")?" - "+value.calories+" cal.":"",configSynopTag.menu_title_color,qsTranslate('','meal_syn_menu_cal_fontsize')]
                    anchors.left:itemTitle.right;
                    anchors.top:itemTitle.top;
                    anchors.topMargin:qsTranslate('','meal_syn_menu_cal_tm');
                    anchors.baselineOffset:qsTranslate('','meal_syn_menu_cal_lm');
                    width:qsTranslate('','meal_syn_menu_cal_w');
                    height:qsTranslate('','meal_syn_menu_cal_h');
                }

                ViewText{
                    id:itemDesc
                    anchors.top:itemTitle.bottom
                    anchors.topMargin: qsTranslate('','meal_syn_menu_des_tm');
                    width:qsTranslate('','meal_syn_menu_des_w');
                    varText:[value.description_short,configSynopTag.menu_desc_color,qsTranslate('','meal_syn_menu_des_fontsize')]
                    wrapMode: Text.WordWrap




                }

            }

            Image{
                source:viewHelper.configToolImagePath+configSynopTag.scroll_fadeimage;
                opacity:(synopsisView.height>synopsisView.contentHeight)?0:1
                visible:((synopsisView.contentY+synopsisView.height)<synopsisView.contentHeight)
                anchors.bottom: parent.bottom;
            }
            Component.onCompleted:{
                //scroller.targetData=synopsisView
                scroller.opacity=0
                categoryMenu.redrawScroll();
            }
        }
    }

    /***************************************************************************************/

    property QtObject scroller
    Component{
        id:compScroller
        Scroller{
            id:scroller
            //x:(categoryMenu.x+categoryMenu.selX)+538;
            anchors.right:categoryMenu.right;
            //y:(categoryMenu.y+categoryMenu.selY)+100;



            opacity:0
            targetData:categoryMenu.targetData;
            sliderbg:[qsTranslate('','meal_scrollcont_base_margin'),qsTranslate('','meal_scrollcont_w'),categoryMenu.baseValues[categoryMenu.count],configSynopTag["scroll_base"]]
            scrollerDot:configSynopTag["scroll_button"]
            upArrowDetails:[configSynopTag["up_arrow_n"],configSynopTag["up_arrow_h"],configSynopTag["up_arrow_p"]]
            downArrowDetails:[configSynopTag["dn_arrow_n"],configSynopTag["dn_arrow_h"],configSynopTag["dn_arrow_p"]]
            height:categoryMenu.scrollValues[categoryMenu.count]
            width: qsTranslate('','meal_scrollcont_w');

            onSliderNavigation:{
                if(direction=="right"){
                    closeBTN.forceActiveFocus();
                }else if(direction=="up"){
                    if(categoryMenu.currentIndex!=0){
                        categoryMenu.decrementCurrentIndex();
                        categoryMenu.forceActiveFocus();
                    }else{
                        closeBTN.forceActiveFocus();
                    }
                }else if(direction=="left"){
                    if(categoryMenu.count>0)
                        categoryMenu.forceActiveFocus();
                    else
                        catalogMenu.forceActiveFocus();
                }
                else if(direction=="down"){
                    if(categoryMenu.currentIndex!=(categoryMenu.count-1)){
                        categoryMenu.incrementCurrentIndex();
                        categoryMenu.forceActiveFocus();
                    }

                }


            }


        }

    }

    /***************************************************************************************/

    property QtObject synopsisPoster;
    Component{
        id:compSynopsisPoster;
        Image{
            id:synopsisPoster;
            visible:categoryMenu.visible;
            source:catalogMenu.model.at(catalogMenu.selIndex).catalog_image===undefined?'':catalogMenu.model.at(catalogMenu.selIndex).catalog_image;
            anchors.left:categoryBg.right;
            height:categoryBg.height;
            anchors.top:categoryBg.top;
            MouseArea{
                anchors.fill:parent;
                onClicked:{}
            }
        }
    }

}

