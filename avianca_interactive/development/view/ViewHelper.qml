import QtQuick 1.1
import Panasonic.Pif 1.0
import "components"
import "../framework/components"
import "../framework/blank.js" as Code

// Contains Interactive specific functions and variables.
Item {
    /**********************Alias**************************/
    property alias blankModel: blankModel
    property alias blankComp: blankComp
    property alias audioPlayer:audioPlayer
    /**********************Int**************************/
    property int languageIndex: -1
    property int mainMenuIndex:isKids(current.template_id)?2:2
    property int subCategoryMenuIndex:0
    property int welComeIndex:0
    property int resumeSecs: 10
    property int vodType: -1
    property int soundTracklid:-1;
    property int subtitlelid:-1;
    property int aspectlid: 0
    property int playingVODmid:-1;
    property int tvSeriesIndex:-1;
    property int mediaListing:-1;
    property int adultIndex:0
    property int kidsIndex:1
    property int genreSongIndex: -1;
    property int fullPlayId:0;
    property int surveyId: -1
    property int queNumber: -1
    property string usbIndex:"usb_images"
    property int usbIndexSelected:-1
    property int surveyCategoryIndex: -1
    property int surveyListingIndex: -1
    property int parentMidTvSeries: -1
    property int  cidFromKarma: 0
    property int subCatCid:-1
    property int parentTrailerMid: -1
    property int windowMinLevel:pif.windowDimmable.getMinLevel();
    property int windowMaxLevel:pif.windowDimmable.getMaxLevel();
    property int windowCurLevel:pif.windowDimmable.getDimLevel();
    property int  storingCatalog_id: -1
    property int totalHospitalityCountDefault: -1
    property int expCid : 0;

    /**********************string**************************/
    property string configToolImagePath: core.configTool.path + "images/";
    property string cMediaImagePath: core.settings.contentPath+'/media/images/';
    property string viewAssetPath: core.settings.assetPath+"/images/"; // added for MICROAPP VIDEO PANEL BG IMAGE
    property string phsAssetPath: '/tmp/interactive/viewAssets/assets0/premiumHandset/images/';
    property string vodAction: ""
    property string qID: "";
    property string aID: "";
    property string statusOfSoundtrackPopup:""
    property string isExternalOnVOD: ""
    property string directJumpFromKids: ""
    property string usbFileName: ""
    property string vkbInputData: ""
    property string folderPathLocally: ""
    //------------- string property related to MicroApp ----------------------
    property string addInfoXMLSource: "";
    property string addInfoscreenID: "";
    property string addImagesPath: "";
    //-------------------------- END -------------------------------

    /**********************Variant**************************/
    property variant  languageModel: []
    property variant tvSeriesModel:[]
    property variant subtitleModel:[]
    property variant soundTrackModel:[]
    property variant vodParams:[]
    property variant mediaListingModel: []
    property variant paramsFromKarma: []
    property variant filePathName;
    property variant prevSurveyParams: []
    property variant usbSelectedIndex;
    property variant surveyDone: []
    property variant  lastSurveyVisited: []
    property variant gameCodes:{"eng":"","spa":"es_*","por":"pt_*","deu":"de_*"}
    property variant gameObj:{"lang":gameCodes[core.settings.languageISO.toLowerCase()]}
    property variant cVoyagerURL : "http://172.17.0.24/";//check
    property variant multipleSamePopupInstance: []
    property variant shopValue;
    property variant shop_catalog_id;
    property variant shop_category_id;
    property variant shop_item_id;
    property variant __delayLaunchParams:[];  //path,type,gameAudio
    property variant videoAfterGameParams:[];   //params , trailer-'trailer'

    /**********************Bool**************************/
    property bool isFromExperience:false;
    property bool isFromWelComeScreen: false
    property bool isTriggeredSurvey: false
    property bool isResume: false
    property bool isTvSeries: false
    property bool isFromKarma: false
    property bool isFromTrailer: false
    property bool isCurtainFirstTime: true
    property bool isDirectMainMenu: false
    property bool keepSynopsisOpen: false
    property bool trackStop:true;
    property bool isHomePressed: false;
    property bool aodHandsetControls:false;
    property bool isFetchDataCalled: false // for CG
    property bool cgFromHeader:false
    property bool usbTerms: false // for usb
    property bool seatchatTerms: false
    property bool usbSlideshow: false
    property bool usbTrackPlayed:false
    property bool isPAonVoyager:false
    property bool voyagerLaunched: false
    property bool isVolumeFirstimeonWelcome: true
    property bool isMusicControlVolumeOpen: false
    property bool showHighlight: false
    property bool isTrailerPressed:false
    property bool isPreviousEnabled:false
    property bool isVideoAudioServiceBlocked: false
    property bool isBackFromMainMenu: false
    property bool isHomeFromAndroid: false
    property bool isShortCutForPremium: false
    property bool isVoyager3D: true;
    property bool userRepeatOn: false
    property bool storePreviousStateofBacklight: true
    property bool isAviancaBrasilAir: false
    property bool isMusicGameAudioDetails: false
    property bool specialCaseForSeatchatJump: false
    //------------------- bool property related to MicroApp ----------------------
    property bool isMicroAppVideo: false
    //-------------------------- END -------------------------------
    property bool isHospitalityCountDefault: false
    property string defaultLang:core.settings.defaultISO
    property string voyager3dAppName: "com.rockwellcollins.asxi.airshow"
    property string language:"english"
    property string shortCutPremiumParams: ""
    property string helpTemplate:"main_menu"
    property string section:isKids(current.template_id)?"kids_help":"help";
    property string thinFont:"FS Elliot Thin";
    property string adultFont:"FS Elliot";
    property string kidsFont:"FS Elliot"
    property string paxTempId:""//current.template_id
    property string gamePath: ""
    property int gameMid:-1;
    property int gameMidA:-1;

    signal usbSlideShowCLosed()
    signal usbBackPressed()
    signal customHandsetSignal(string hstStr)
    signal vkbData(string vkbData , string identifier)
    signal vkbClosed(string identifier)
    signal themeManagerRefresh()
    /***************************SeatChat Variables*************/
    property bool seatchat_enabled: true;    // To make seat Available
    property variant cSessionID;
    property bool shortcutJumpSeatChat:false
    property bool showNotification_txt:false
    property bool loadFromSettings:false;
    property variant tSeatNo//Used to temporary hold seat numbers
    property variant singleBlockSeat;
    property string popupTSeatNo:"";
    property string nickBoxInput:pif.seatChat.getSeatNickname()
    property bool fromInvitePopup: false
    property bool chatActive: false
    property variant buddyList: []
    property variant  participantNames
    property variant buddyLen
    property string participant
    property variant unreadmsgCount
    property alias chatSessionModel: chatSessionModel
    signal exitseatChat()
    property int viewSessionIndex
    property int maxSessionCount:(core.pc)?5:15
    property string identifier:""
    property int update_UserNotification:-1
    property variant localeLanguages: []
    property int actualSessionCount:0;
    property int invitationSent:0;
    signal toblockExitSingleBudddy()
    signal acceptInviataionfromPopup()
    signal seatChatServiceBlock()
    signal setFocusUnblockPopupClosed()
    signal seatchatKarmaActionPerform()
    signal seatchaNickNametKarmaActionPerform()
    signal showHelpOnMainSeatChat()

    property variant subcategoryModel

    property bool blockInvitation:false;

    property alias gameTimerToAvoidPA: gameTimerToAvoidPA
    property bool gameLoadInProgress: false
    property bool showCtMsgAfterGameLoad: false
    Timer{
        id: gameTimerToAvoidPA
        interval: 12000
        onTriggered: {
            gameLoadInProgress = false
            if(showCtMsgAfterGameLoad) ctToSeatMsgLaunch()
            if(pif.getPAState()==1) paLaunch()
        }
    }

    function paLaunch(){
        if(gameLoadInProgress) {core.log("paLaunch | Game is loading return"); return}
        viewHelper.updatePremiumHandset(3,true);
        viewController.updateSystemPopupQ(1,true)
        viewHelper.isPAonVoyager = true;
        viewController.sendMsgToExtPopup(1,qsTranslate('',configTool.language.global_elements.pa+"|"+configTool.config.popup.generic.bg_color+"|"+configTool.config.popup.generic.text_color+"|"+viewHelper.isKids(current.template_id)))
        if(pif.getExternalAppActive()){pif.setBacklightOn();}
    }

    function ctToSeatMsgLaunch(){
        if(gameLoadInProgress) {core.log("ctToSeatMsgLaunch | Game is loading return"); showCtMsgAfterGameLoad = true; return}
        showCtMsgAfterGameLoad = false
        if(viewController.getActiveSystemPopupID()==2){
            viewController.updateSystemPopupQ(2,false);
           //viewHelper.multipleSamePopupInstance.push(true)
        }
        else  viewHelper.storePreviousStateofBacklight=pif.getBacklightState()
        viewController.updateSystemPopupQ(2,true);
        viewController.sendMsgToExtPopup(2,qsTranslate('',pif.ctMessage.ctMessage+"|"+configTool.config.popup.generic.bg_color+"|"+configTool.config.popup.generic.text_color+"|"+viewHelper.isKids(current.template_id)+"|"+pif.ctMessage.ctTimeout+"|"+viewAssetPath+'announcement.png'+"|"+ parseInt(qsTranslate('','popups_bg_tm'),10)))
        if(pif.getExternalAppActive()){pif.setBacklightOn();}
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedCtMessageinProgress",{"status":true});
    }

    function getBlockInvitationStatus(){
        return blockInvitation;
    }

    function setBlockInvitationStatus(val){
        blockInvitation=val;
    }

    function maxSessionTimer(){
        console.log("maxSessionTimer >>>>>>>>>>>>>>>>>>>>>>>> pif.seatChat.pendingInvitationList.count | before = "+pif.seatChat.pendingInvitationList.count)
        if(pif.seatChat.pendingInvitationList.count>0){
            console.log("maxSessionTimer >>>>>>>>>>>>>>>>>>>>>>>> pif.seatChat.pendingInvitationList.count | inside = "+pif.seatChat.pendingInvitationList.count)
            popupTSeatNo=pif.seatChat.pendingInvitationList.getValue(0,"seat")
            cSessionID=pif.seatChat.pendingInvitationList.getValue(0,"session")
            nottoshowInvitationPopup.restart();
        }else{
            nottoshowInvitationPopup.stop();
        }

        console.log("maxSessionTimer >>>>>>>>>>>>>>>>>>>>>>>> pif.seatChat.pendingInvitationList.count | after = "+pif.seatChat.pendingInvitationList.count)
    }



    Timer{
        id:nottoshowInvitationPopup
        interval: 2000
        onTriggered: {
            viewController.updateSystemPopupQ(13,true)
        }
    }


    Connections{
        target: (visible)?viewController:null
        onSystemPopupDisplayed:{
            if (activeSystemPopupID==1||activeSystemPopupID==2) return;
            if(viewController.getActiveScreenPopupID() >0 && !widgetList.isVideoVisible()){
                core.debug("qml | viewController.getActiveScreenPopupID() INSIDE iF = "+viewController.getActiveScreenPopupID())
                viewController.hideScreenPopup();return;
            }
            core.debug("qml | activeSystemPopupID() = "+activeSystemPopupID)
        }
    }





    onUserRepeatOnChanged:{
        pif.sharedModelServerInterface.sendApiData("userDefinedRepeatOn",{repeatStatus:userRepeatOn});
    }
    onPaxTempIdChanged:{
        core.log("pravin  Interactive logging the screen is : "+Code.screenLogArray[paxTempId])
        if(Code.screenLogArray[paxTempId]){
            pif.paxus.interactiveScreenLog(Code.screenLogArray[paxTempId]);
        }else{
            pif.paxus.interactiveScreenLog(paxTempId);
        }
    }
    function updatePremiumHandset(id, status){
        if(pif.getHandsetType()==pif.cPREMIUM_HANDSET) pif.premiumHandset.updatePremiumHandsetQ(id, status);
    }

    function showHelpElements(){return " section : "+section+"  language  : "+language+"  helpTemplate : "+helpTemplate}
    function getHelpImage(){core.debug( " section : "+section+"  language  : "+language+"  helpTemplate : "+helpTemplate);return configTool.config[section][language][helpTemplate]}
    function setHelpLanguage(lang){
        lang=lang.toLowerCase();
        if(lang=="eng")
            language="english"
        else if(lang=="spa")
            language="spanish"
        else if(lang=="por")
            language="portuguese"
        else if(lang=="deu")
            language="germany"
    }
    function getHelpLanguage(){return language}
    function setHelpTemplate(ht){helpTemplate=ht;}
    function getHelpTemplate(){return helpTemplate;}

    function setSubCatCid(val){subCatCid=val}
    function getSubCatCid(){return subCatCid}

    function setCidFromKarma(val){cidFromKarma=val}
    function getCidFromKarma(){return cidFromKarma}
    function getVideoPipId(){if (core.settings.resolutionIndex==1) return 100; if (core.settings.resolutionIndex==2) return 200;}
    function getTrailerPipId(){if (core.settings.resolutionIndex==1) return 300; if (core.settings.resolutionIndex==2) return 400;}
    function setVodType(type){vodType =  (type=="tvseries")?0:type=="trailer"?1:type=="episode"?2:3}
    function getVodType(){return vodType;}
    function setVodAction(action){soundTrackModel = action;}
    function getVodAction(){return vodAction;}
    function setSoundtrackModel(model){soundTrackModel = model;}
    function getSoundtrackModel(){return soundTrackModel;}
    function setSubtitleModel(model){subtitleModel = model;}
    function getSubtitleModel(){return subtitleModel;}
    function setSoundTracklid(val){ soundTracklid=parseInt(val,10);}
    function getSoundTracklid(){ return soundTracklid;}
    function setSubtitlelid(val){ subtitlelid=parseInt(val,10);}
    function getSubtitlelid(){ return subtitlelid;}
    function setAspectlid(val){ aspectlid=val;}
    function getAspectlid(){ return aspectlid;}
    function setTvSeriesModel(model){tvSeriesModel = model;}
    function getTvSeriesModel(){return tvSeriesModel;}
    function setTvSeriesIndex(param){tvSeriesIndex = param;}
    function getTvSeriesIndex(){return tvSeriesIndex;}
    function setMediaListingModel(model){mediaListingModel = model;}
    function getMediaListingModel(){return mediaListingModel;}
    function setMediaListingIndex(param){mediaListing = param;}
    function getMediaListingIndex(){return mediaListing;}
    function getUsbTerms(){return usbTerms}
    function setUsbTerms(val){usbTerms=val}
    function getSeatChatTerms(){return seatchatTerms}
    function setSeatChatTerms(val){seatchatTerms=val}
    function isValidSearchString(str){
        core.debug("str "+str)
        if(str == undefined) str = "";
        if(str.trim().length == 0){
            core.debug("qml | isValidSearchString() | Not valid string for Search "+str)
            return false;
        }
        return true;
    }
    function isAircraft787(){
        return ((core.dataController.getAircraftType().indexOf("787") != -1)  && (pif.getNoOfDimmableWindows()>0))?true:false;
    }

    function isAircraftType787(){
        return ((core.dataController.getAircraftType().indexOf("787") != -1))?true:false;
    }
    function isAircraftType330(){
        return ((core.dataController.getAircraftType().indexOf("330") != -1))?true:false;
    }

    function setDimLevel(val){
        if(windowCurLevel<windowMaxLevel&&val>0){
            var dimLvl = windowCurLevel + windowMinLevel;
            pif.windowDimmable.setDimLevel(dimLvl);

        }
        else if(windowCurLevel>windowMinLevel&&val<0){
            var dimLvl = windowCurLevel - windowMinLevel;
            pif.windowDimmable.setDimLevel(dimLvl);
        }
        console.log('window dimming || dim value changed || current value : '+windowCurLevel)
    }

    function mapMenuTid(params,isTvMovieSeperate,isTvseries){

        if(isTvseries=="tvseries")return "episode"
        if(isTvMovieSeperate!=undefined && !isTvMovieSeperate)  return params
        if(params=="movies" || params=="kids_movies_listing")  return "movies"
        else if(params=="tv" || params=="kids_tv_listing")return "movies"
        else if(params=="music" || params=="kids_music_listing") return "music"
        else if(params=="shopping") return "movies"
        else return "games"
    }


    function mapMenuConfigTid(params){
        if(params=="usb"){return "movies"
        }else  if(params=="shopping"){return "movies"};
        return params;
    }

    function setHelpTemplateForListing(params){

        if(params=="movies" || params=="kids_movies_listing")  helpTemplate= "movie_listing"
        else if(params=="tv" || params=="kids_tv_listing")helpTemplate= "tv_listing"
        else if(params=="music" || "kids_music_listing") helpTemplate= "music_listing"

    }

    function isKids(tnode){
        if(tnode=="kids"||
                tnode=="kids_movies_listing"||
                tnode=="kids_music_listing"||
                tnode=="kids_tv_listing"||
                tnode=="kids_games_listing"){
            return true;}
        return false;
    }
    /**************************************Premium Handset **********************************/
    function shortcutForHotKeys(params){
        if(checkBeforeLaunchingShortcuts())return
        core.debug("qml | shortcutForHotKeys | params "+params)
        var index= core.coreHelper.searchEntryInModel(viewData.mainMenuModel,"category_attr_template_id",params)
        if(index==-1)return
        blockKeysForSeconds(2)
        if(widgetList.isVideoVisible()){
            shortCutPremiumParams=params
            isShortCutForPremium=true
            widgetList.stopVod()
            return
        }
        if(widgetList.isSoundTrackSubtitleOpen()){widgetList.getSoundTrackSubtitleRef().visible=false}
        widgetList.mainMenuEnterReleased(index)
        isShortCutForPremium=false
        shortCutPremiumParams=""
    }
    function checkBeforeLaunchingShortcuts(){
        if(widgetList.isFlightMapOn())widgetList.showFlightMap(false)
        if(viewController.getActiveScreenPopupID() != 0 || viewController.getActiveSystemPopupID() != 0 || viewController.getActiveScreen()=="Welcome")
            return true
        else return false
    }

    /**************************************External Survey ************************************/

    function surveyCategoryList(){
        queNumber=lastSurveyVisited[surveyId]!=undefined?lastSurveyVisited[surveyId]:-1
        surveyCategoryIndex=core.coreHelper.searchEntryInModel(dataController.survey.getSurveyModel(),"surveyID",surveyId)
    }
    function subCategoryReadyForSurvey(dModel){
        var index = core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id","survey")
        mediaListing=index
        dataController.survey.getAllSurvey(surveyCategoryList);
        viewController.loadNext(dModel,index);
        widgetList.getHeaderRef().isFlightInfoOpen=false
        isTriggeredSurvey=true
        if(!widgetList.isCurtainUp())widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,mainMenuIndex,false)
    }
    function forceFullyCloseWidgets(){
        if(widgetList.isSoundTrackSubtitleOpen())widgetList.showsoundTrackSubtitle(false)
        if(widgetList.isFlightMapOn())widgetList.showFlightMap(false)
        if(widgetList.isUsbSlideShowOn())widgetList.showUsbSlideshow(false)
        if(widgetList.getisFlightInfoOpen())widgetList.setisFlightInfoOpen(false)
    }
    function checkForExternalBlock(params){
        var customName=""
        if(params=="USB")customName="svcSeatUSBMediaPlayer"
        else if(params=="Survey")customName="svcSeatAdvSurveys"
        else if(params=="CG")customName="svcSeatGateConnect"
        else if(params=="Voyager")customName="svcSeatBDVChannelMap"
        else if(params=="Hospitality")customName="svcSeatOnboardHospitality"  //its a dummy name
        else if(params=="SeatChat")customName="svcSeatChat"
        if(customName && dataController.mediaServices.getServiceBlockStatus(customName)){
            viewController.updateSystemPopupQ(7,true)
            return true;
        }
        else return false;
    }
    function externalEvents(params){
        if(checkForExternalBlock(params)) return;
        forceFullyCloseWidgets()
        if(params=="SeatChat")  {
            widgetList.getMainMenuRef().setCurrentIndexForDiscover()
            isCurtainFirstTime=false
            console.log(">>>>>>>>>>>>>>>widgetList.getCidTidMainMenu()[0]  "+widgetList.getCidTidMainMenu()[0]+" widgetList.getCidTidMainMenu()[1] "+widgetList.getCidTidMainMenu()[1])
            dataController.getNextMenu([widgetList.getCidTidMainMenu()[0] ,widgetList.getCidTidMainMenu()[1] ],jumpseatChat);
            if(current.template_id!="seatchat")specialCaseForSeatchatJump=true
        }else if(params=="USB")  jumpToUSB()
        else if(params=="Survey"){
            widgetList.getMainMenuRef().setCurrentIndexForDiscover()
            isCurtainFirstTime=false
            dataController.getNextMenu([widgetList.getCidTidMainMenu()[0] ,widgetList.getCidTidMainMenu()[1] ],subCategoryReadyForSurvey);
        }
        else if(params=="Voyager") widgetList.showFlightMap(true)
        else if(params=="CG"){
            widgetList.getMainMenuRef().setCurrentIndexForDiscover();
            isCurtainFirstTime=false
            console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>> widgetList.getCidTidMainMenu()[0] "+widgetList.getCidTidMainMenu()[0])
            console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>> widgetList.getCidTidMainMenu()[1] "+widgetList.getCidTidMainMenu()[1])
            dataController.getNextMenu([widgetList.getCidTidMainMenu()[0] ,widgetList.getCidTidMainMenu()[1] ],subCategoryReadyForCG);
        }
    }
    function subCategoryReadyForCG(dModel){
        var index = core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id","connecting_gate")
        mediaListing=index;
        viewController.loadNext(dModel,index);
        cgFromHeader=true
        if(!widgetList.isCurtainUp()) widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,mainMenuIndex,false)
    }

    function jumpseatChat(dModel){
        var index = core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id","seatchat")
        mediaListing=index;
        console.log("jumpseatChat >>>>>>>>>>>>>>>  mediaListing "+ mediaListing +"index "+index )
        viewController.loadNext(dModel,index);
        timerDelay.restart()
        shortcutJumpSeatChat=true
        //        fromInvitePopup=true

    }
    Timer{
        id:timerDelay
        interval: 400
        onTriggered:{
            if(!widgetList.isCurtainUp()) widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,mainMenuIndex,false)
            timerDelay.stop()
        }
    }

    function jumpToUSB(){
        var temp=core.dataController.mediaServices.getCategoryDataByTagName("usb");
        var cid=temp.cid
        var tid=temp.tid
        viewController.jumpTo(cid,tid)
        widgetList.setCurrentIndexForUsb();
        if(isCurtainFirstTime){
            widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,mainMenuIndex,false)
        }
        isCurtainFirstTime=false
    }

    /********************************Usb Functions***********************************************************/
    function jumToUsbScreen(val){
        core.debug(" val : "+val)
        var temp=core.dataController.mediaServices.getCategoryDataByTagName(val);
        core.debug(" temp.cid : "+temp.cid)
        core.debug(" temp.tid: "+temp.tid)
        var cid=temp.cid
        var tid=temp.tid
        viewController.jumpTo(cid,tid)
        switch(val){
        case "usb_images":
            pif.paxus.startContentLog('Usb Pictures')
            break;
        case "usb_doc":
            var temp1=core.dataController.mediaServices.getCategoryDataByTagName("usbmusic");
            pif.paxus.startContentLog('Usb Documents')
            break;
        case "usb_music":
            pif.paxus.startContentLog('Usb Music')

            break;
        }
    }
    function stopUsbMp3(){
        usbTrackPlayed=false;pif.usbMp3.stop();/*trackStop=true*/
    }
    /***********************************Survey*****************************************************************/
    function setSurveystatus(surveyId){
        var temp=surveyDone
        temp.push(surveyId)
        surveyDone=temp
    }
    function getSurveystatus(surveyId){
        var temp=surveyDone
        var status=-1
        for(var i=0;i<temp.length;i++){
            if(temp[i]==surveyId){
                status=i;break;
            }
        }
        return status==-1?false:true
    }

    /******************************SoundTrack Subtitle & VOD Queries*****************************************/
    function initializeExpVod(dModel){
        playingVODmid=dModel.getValue(0,'mid')
        setVodType("movie");
        setMediaListingModel(dModel)
        setMediaListingIndex(0);
        widgetList.getSoundTrackSubtitleRef().subtitleLVIndex=0;
        widgetList.getSoundTrackSubtitleRef().soundTrackLVIndex=0;
        var sndParams=[]
        sndParams[0]=playingVODmid;
        sndParams[2]=dataController.getLidByLanguageISO(getDefaultSndLang());
        dataController.getSoundTracks(sndParams,setSoundTrackQuery);
    }


    function initializeVOD(action,type,params,fromKarma,fromTrailer){
        core.debug("initializeVOD called")
        isFromKarma=fromKarma
        isFromTrailer= fromTrailer
        setVodAction(action)

        if(!isFromKarma){setVodType(type);isTrailerPressed=false}
        paramsFromKarma=params
        if(action=="resume")isResume=true
        // else isResume=false
        if(isFromKarma){
            playingVODmid=params["mid"];
        }
        else if(isFromTrailer)playingVODmid=getTrailerParentMid()
        else  playingVODmid=(getVodType()==2)?getTvSeriesModel().getValue(getTvSeriesIndex(),"mid"):(getVodType()==1)?getMediaListingModel().getValue(getMediaListingIndex(),"trailer_mid"):getMediaListingModel().getValue(getMediaListingIndex(),"mid")

        widgetList.setSndtemplateId(current.template_id)
        var sndParams=[]
        sndParams[0]=playingVODmid;
        sndParams[2]=dataController.getLidByLanguageISO(getDefaultSndLang());
        dataController.getSoundTracks(sndParams,setSoundTrackQuery);
        //        }
    }
    function setSoundTrackQuery(dModel){
        widgetList.getSoundTrackSubtitleRef().soundTrackLVIndex=0;
        setSoundtrackModel(dModel);
        getSubTitlesQuery();
    }
    function getSubTitlesQuery(){
        dataController.getSubtitlesWithLabel([playingVODmid],setSubtitleQuery,configTool.language.movies.none);
    }
    function setSubtitleQuery(dModel){

        setSubtitleModel(dModel);
        //        if(isFromKarma==false){
        var subtitle_lid=isResume?pif.getMidInfo(playingVODmid,'subtitleLid'):(dModel.count>1?dModel.get(widgetList.getSoundTrackSubtitleRef().subtitleLVIndex).subtitle_lid:-1)
        var soundtrack_lid=isResume?
                    pif.getMidInfo(playingVODmid,'sndtrkLid'):
                    (getSoundtrackModel().count>0?
                         getSoundtrackModel().getValue(widgetList.getSoundTrackSubtitleRef().soundTrackLVIndex,"soundtrack_lid")

                       :-1)
        if(isFromKarma){
            subtitle_lid=paramsFromKarma["subtitleLid"]
            soundtrack_lid=paramsFromKarma["sndtrkLid"]
        }
        setSubtitlelid(subtitle_lid)
        setSoundTracklid(soundtrack_lid)
        //        }

        if(widgetList.isFlightMapOn()){widgetList.showFlightMap(false)}
        pif.vod.setPlayingSubtitle(subtitle_lid)
        pif.vod.setPlayingSoundtrack(soundtrack_lid)
        if(widgetList.getVideoRef().isModelUpdated)return;
        if(widgetList.isVideoVisible() && !isFromTrailer) widgetList.closeSoundTrack()
        else   if(getVodType()==0 && isFromKarma)   core.dataController.getNextMenu([paramsFromKarma['aggregateMid'],"tvseries"],callSoundtrackPopup)
        else     core.dataController.getMediaByMid([playingVODmid],callSoundtrackPopup)
    }
    function callSoundtrackPopup(dModel){

        if(getVodType()==0 && isFromKarma){
            setTvSeriesModel(dModel);
            setTvSeriesIndex(core.coreHelper.searchEntryInModel(dModel,"mid",playingVODmid))
            core.dataController.getMediaByMid([playingVODmid],checkForSoundTrackVisibility)
        }
        else   checkForSoundTrackVisibility(dModel)
    }
    function checkForSoundTrackVisibility(dModel){
        if(isFromKarma){
            setMediaListingModel(dModel)
            setMediaListingIndex(0)
            setVideoParamsFromKarma(dModel)
            return;
        }
        //        if(getSubtitleModel().count<=1 && getSoundtrackModel().count<2) setVideoParams()
        if(getSubtitleModel().count<=1 && getSoundtrackModel().count<2 && isMicroAppVideo==false ) setVideoParams()
        // else if(getSoundtrackModel().count<2)setVideoParams()//If bymistake subtitle is morethan 1 but soundtrack is less than 2
        else if(isMicroAppVideo){
            setSoundTracklid(getSoundtrackModel().getValue(0,'soundtrack_lid'))
            for(var k =0;k<getSoundtrackModel().count;k++){
                if(getSoundtrackModel().getValue(k,'label_iso').toLowerCase()==(core.settings.languageISO).toLowerCase()){
                    setSoundTracklid(getSoundtrackModel().getValue(k,'soundtrack_lid'))
                    break;
                }
            }
            setVideoParams()
        }else  if(!isResume && !isFromKarma && !isFromExperience){
             console.log(" Going in checkForSoundTrackVisibility | !isResume && !isFromKarma ");
            if(widgetList.isVideoVisible()) widgetList.getHeaderRef().stopVODTimer(true);
            widgetList.showsoundTrackSubtitle(true)
        }
        else {setVideoParams();}
    }
    function setVideoParams(){

        var mid_status = dataController.mediaServices.getMidBlockStatus(playingVODmid,getMediaListingModel().getValue(getMediaListingIndex(),"rating"));
        if(mid_status){viewController.updateSystemPopupQ(7,true);return;}

        //var cid = (current.template_id == 'tv')?getSubCatCid():widgetList.getCidTidMainMenu()[0]

        isTvSeries=false
        var params=new Object();
        params["pipID"]=(getVodType()==1)?getTrailerPipId():getVideoPipId();
        params["cid"]= isFromExperience?expCid:(viewController.getActiveScreenRef().getCidTidForSubCat?viewController.getActiveScreenRef().getCidTidForSubCat()[0]:widgetList.getCidTidMainMenu()[0])
        params["elapseTimeFormat"]=2;
        params["aspectRatio"]=getMediaListingModel().getValue(getMediaListingIndex(),"aspect");
        params["mediaType"]=getMediaListingModel().getValue(getMediaListingIndex(),"media_type");
        params["amid"]=playingVODmid;
        params["duration"]=getVodType()==1?getMediaListingModel().getValue(getMediaListingIndex(),"trailer_duration"):(getVodType()==0 || getVodType()==2?getTvSeriesModel().getValue(getTvSeriesIndex(),"duration"):getMediaListingModel().getValue(getMediaListingIndex(),"duration"));
        if(params["duration"]=="")return
        params["ignoreSystemAspectRatio"]=true
        params["subtitleLid"]= getSubtitlelid();
        params["sndtrkLid"]=getSoundTracklid()
        params["rating"]=getMediaListingModel().getValue(getMediaListingIndex(),"rating");

        //  if(getVodType()==2) params["playIndex"]=getTvSeriesIndex()
        //else
        if(getVodType()==0) {params["playIndex"]=isResume?getTvSeriesIndex():0;isTvSeries=true;}
        //  setTvSeriesIndex(params["playIndex"]!=undefined?params["playIndex"]:-1)
        playVideo(params)
    }
    function setVideoParamsFromKarma(dModel){
        //        var sndParams=[]
        //        sndParams[0]=playingVODmid;
        //        sndParams[2]=dataController.getLidByLanguageISO(getDefaultSndLang());
        //        dataController.getSoundTracks(sndParams,setSoundTrackQuery);
        console.log(">>>>>>>>>>> setVideoParamsFromKarma SNdtrlid "+getSoundTracklid())
        var params=new Object();
        isTvSeries=false
        params=new Object();
        params["pipID"]=(getVodType()==1)?getTrailerPipId():getVideoPipId();
        params["cid"]=getCidFromKarma()
        params["elapseTimeFormat"]=2;
        params["aspectRatio"]=dModel.getValue(getVodType()==0?getTvSeriesIndex():0,"aspect");
        params["mediaType"]=getVodType()==0?dModel.getValue(getTvSeriesIndex(),"media_type"):"videoAggregate"//0,"media_type")*/
        params["amid"]=getVodType()==0?paramsFromKarma['aggregateMid']:playingVODmid;
        params["duration"]=getVodType()==0?(dModel.getValue(getTvSeriesIndex(),"duration")):(dModel.getValue(0,"duration"));
        params["ignoreSystemAspectRatio"]=true
        params["rating"]=dModel.getValue(0,"rating")
        params["subtitleLid"]= getSubtitlelid();
        params["sndtrkLid"]=getSoundTracklid()
        if(getVodType()==0) {params["playIndex"]=isResume?getTvSeriesIndex():0;isTvSeries=true;}
        playVideo(params)
    }
    function getDefaultSndLang(){
        if(isAviancaBrasilAir){
            return "por";
        }else{
            return "spa";
        }
    }
    function storeTrailerParentMid(parentMid){
        parentTrailerMid=parentMid
    }
    function getTrailerParentMid(){
        return  parentTrailerMid
    }

    Timer{
        id:delaySetLanguage;
        interval:1000;
        onTriggered:{
            pif.vod.setPlayingSoundtrack(getSoundTracklid())
            pif.vod.setPlayingSubtitle(getSubtitlelid())
        }
    }

    function playVideo(params){
        isFromExperience=false;
        var status=false
        if(getVodType()==1) status=pif.vod.playTrailer(playingVODmid,params)
        else if(getVodType()==0) status=pif.vod.playByModel(getTvSeriesModel(),params,isResume)
        else status=pif.vod.play(playingVODmid,params,isResume);
        core.info("  qml | playVideo | status= "+status)
        if(status){
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedplayFromSeat",{isFromSeat:true,vodType:getVodType()})
            }
            if(!widgetList.isVideoVisible()){
                delaySetLanguage.restart();
            }
            widgetList.showsoundTrackSubtitle(false)
            widgetList.showVideo(true)
            isResume=false
            vodParams=params
            if(isTvSeries){
                setHelpTemplate("tv_vod");
            }else if(getVodType()==1){
                setHelpTemplate("movie_trailer");
            }
            else{
                setHelpTemplate("movie_vod");
            }
        }
    }
    /******************************************Footer***********************************************************/
    function backaction(){
        if(viewController.getActiveScreen()=="Welcome")return
        if(widgetList.getisFlightInfoOpen())widgetList.setisFlightInfoOpen(false)
        else if(widgetList.isVideoVisible())  widgetList.videobackBtnPressed(false)
        else if(widgetList.getMealStatus()){widgetList.mealBack()}
        else if(widgetList.isUsbSlideShowOn())widgetList.closeSlideShow()
        else if(widgetList.isFlightMapOn()){widgetList.showFlightMap(false)}
        else if(viewController.getActiveScreenRef().backBtnPressed)viewController.getActiveScreenRef().backBtnPressed()
    }

    function setLocale(iso){
        core.debug("ViewHelper.qml | setLocale iso = "+ iso)
        core.debug("ViewHelper.qml | setLocale locale = "+viewHelper.localeLanguages[iso.toLowerCase()])
        if(pif.android)pif.android.setLocale(localeLanguages[iso.toLowerCase()]);
    }

    function homeAction(){
        if(widgetList.isFlightMapOn()){widgetList.showFlightMap(false)}
        if(widgetList.getMealStatus()){widgetList.showMealMenu(false)}
        if(viewController.getActiveScreen()=="Welcome" || (viewController.getActiveScreen()=="MainMenu" && isHomeFromAndroid)){
            isHomeFromAndroid=false;
            return;
        }
        if(widgetList.getisFlightInfoOpen())widgetList.setisFlightInfoOpen(false)
        if(widgetList.isVideoVisible() && !isHomePressed && !isVideoAudioServiceBlocked) {widgetList.videobackBtnPressed(true);return}
        if(widgetList.isUsbSlideShowOn()){widgetList.closeSlideShow()}
        if(widgetList.isSoundTrackSubtitleOpen()){widgetList.getSoundTrackSubtitleRef().visible=false}

        var cid=viewData.homeModel.getValue(adultIndex,"cid")
        var tid=viewData.homeModel.getValue(adultIndex,"category_attr_template_id")

        isDirectMainMenu=true
        viewController.jumpTo(cid,tid);
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedMenuJump",{sectionIndex:adultIndex,iso:""})
            sectionSelected=viewData.homeModel.getValue(adultIndex,"category_attr_template_id")
        }
        isCurtainFirstTime=true;
        if(isVideoAudioServiceBlocked)isVideoAudioServiceBlocked=false
        isHomeFromAndroid=false;
    }
    /*********************************************Blocking*******************************************************/

    function refreshToMainSection(){
        var cats=["shopping,games,music,movies,tv,discover,usb"];
        dataController.getCategoryByTemplateId(cats,viewData.mainMenuReady);
        adultIndex=core.coreHelper.searchEntryInModel(viewData.homeModel,"category_attr_template_id","adult");
        isFromWelComeScreen=true;
        if(viewController.getActiveScreen()=="MainMenu"){
            viewController.loadNext(viewData.homeModel,adultIndex);
            delayForkids.restart()
        }

    }
    Timer{
        id:delayForkids
        interval: 250
        onTriggered:viewController.getActiveScreenRef().setParameters()
    }

    function switchSection(ind){
        if(!widgetList.isVideoVisible())widgetList.setBlankModel()
        adultIndex=ind;
        isFromWelComeScreen=true;
        homeAction();
        widgetList.setMainMenuCurrentIndex(mainMenuIndex);
    }

    function actionOnServiceBlock(status,serviceCode,cidList){
        switch(serviceCode){
        case "svcSeatAODMenus":{
            if(status!="blocked")return
            pif.aodPlaylist.removeAllTracks(-1)
            pif.aodPlaylist.removeAllTracks(-2)
            pif.aodPlaylist.removeAllTracks(-3)
            if(pif.aod.getMediaState()!=pif.aod.cMEDIA_STOP) pif.aod.stop();
            if((widgetList.getCidTidMainMenu()[1]=="music" || widgetList.getCidTidMainMenu()[1]=="kids_music_listing") && status=="blocked"){
                isVideoAudioServiceBlocked=true
                homeAction()
                viewController.updateSystemPopupQ(7,true);
            }
            break;
        }

        case "svcSeatVODMovies":{
            if(pif.vod.getMediaState()!=pif.vod.cMEDIA_STOP && status=="blocked"){
                isVideoAudioServiceBlocked=true
                pif.vod.stop();
            }else if(status=="blocked" && (widgetList.getCidTidMainMenu()[1]=="movies" || widgetList.getCidTidMainMenu()[1]=="tv" || widgetList.getCidTidMainMenu()[1]=="kids_movies_listing" || widgetList.getCidTidMainMenu()[1]=="kids_tv_listing")){
                if(viewController.getActiveScreen()=="Welcome" || !widgetList.mainMenu.mainMenuPosition())return;
                homeAction()
                viewController.updateSystemPopupQ(7,true);
            }

            break;
        }
        case "svcSeatUSBMediaPlayer":{
            if(pif.launchApp.launchId=="USBPDF" || pif.launchApp.launchId == 2)pif.launchApp.closeApp();
            if( pif.lastSelectedAudio.mediaSource=="usb" &&  pif.lastSelectedAudio.getMediaState()!=pif.lastSelectedAudio.cMEDIA_STOP)  pif.lastSelectedAudio.stop();
            if(widgetList.getCidTidMainMenu()[1]=="usb"){
                homeAction()
                viewController.updateSystemPopupQ(7,true);
            }
            break;
        }
        case "svcSeatGames":{
            if(status!="blocked")return

            if(pif.launchApp.launchId=="games") {pif.launchApp.forceStopPackage(viewHelper.gamePath)/* pif.launchApp.closeApp()*/; }
            if(status=="blocked" && (widgetList.getCidTidMainMenu()[1]=="games" || widgetList.getCidTidMainMenu()[1]=="kids_games_listing" )){
                homeAction()
                viewController.updateSystemPopupQ(7,true);
            }
            break;
        }
        case "svcSeatAdvSurveys":{
            if(current.template_id=="survey"){ homeAction(); viewController.updateSystemPopupQ(7,true);}
            break;
        }
        case "svcSeatCatalogShopping":{
            console.log("ViewHelper.qml | Calling actionOnServiceBlock | case = svcSeatCatalogShopping ")
            if(status!="blocked")return
            console.log("ViewHelper.qml | Calling actionOnServiceBlock | case = svcSeatCatalogShopping & status is not Blocked")
            if(current.template_id=="shopping") {
                console.log("ViewHelper.qml | Calling actionOnServiceBlock | case = svcSeatCatalogShopping template id is shopping")
                homeAction();
                viewController.updateSystemPopupQ(7,true);
            }
            break;
        }
        case "svcSeatHospitality":{
            if(status!="blocked")return
            if(widgetList.getMealStatus()){
                console.log(">>>>>>>>>>>>>>>>>>svcSeatOnboardHospitality")
                widgetList.showMealMenu(false);homeAction(); viewController.updateSystemPopupQ(7,true);}
            break;
        }
        case "svcSeatChat":{
            if(status!="blocked")return
            //            console.log("viewController.getActiveScreenRef().returnSourceComponent()>>>>> = "+viewController.getActiveScreenRef().returnSourceComponent())
            //            if(current.template_id=="seatchat") {
            //            console.log("viewController.getActiveScreenRef().returnSourceComponent() = "+viewController.getActiveScreen())
            //                if(viewController.getActiveScreenRef().returnSourceComponent()==true){
            //                    console.log("viewController.getActiveScreenRef().returnSourceComponent() Inside = "+viewController.getActiveScreenRef().returnSourceComponent())
            //                    homeAction();
            //                    viewController.updateSystemPopupQ(7,true);
            //                    return;
            //                }
            seatChatServiceBlock()
            //            }
            break;
        }
        case "svcSeatGateConnect":{
            if(status!="blocked")return
            if(current.template_id=="connecting_gate") {homeAction(); viewController.updateSystemPopupQ(7,true);}
            break;
        }
        case "svcSeatBDVChannelMap":{
            if(status!="blocked")return
            if(current.template_id=="maps"){ homeAction(); viewController.updateSystemPopupQ(7,true);}
            else if(widgetList.isFlightMapOn()){widgetList.showFlightMap(false)}
            break;
        }
        }
    }
    function actionOnMidBlock(mid,status,rating){
        var mid_status = dataController.mediaServices.getMidBlockStatus(mid,rating);
        if(!mid_status) status = "unblocked";
        else status = "blocked"
        var amid = -1;
        var curr_playing_audio_mid = pif.aod.getPlayingMid()
        if(curr_playing_audio_mid==mid && pif.aod.getMediaState()!=pif.aod.cMEDIA_STOP && status=="blocked") pif.aod.stop();
        var curr_playing_video_mid =pif.vod.getPlayingMid()
        if(curr_playing_video_mid==mid && pif.vod.getMediaState()!=pif.vod.cMEDIA_STOP && status=="blocked"){
            pif.vod.stop();
            viewController.getActiveScreenRef().focus=true;
            viewController.updateSystemPopupQ(7,true)
        }

        if(pif.vod.getMediaState()!=pif.vod.cMEDIA_STOP && status=="blocked" && mid==parentMidTvSeries && curr_playing_video_mid!=mid ){
            pif.vod.stop();
            viewController.getActiveScreenRef().focus=true;
            viewController.updateSystemPopupQ(7,true)
        }
        core.debug("gameMid : "+gameMid);
        core.debug("pif.launchApp.launchId : "+pif.launchApp.launchId)
        core.debug("status : "+status);
        if(pif.launchApp.launchId=="games" && gameMid==mid && status=="blocked"){
            core.debug(" call close the game done");
            core.debug(" call close the game done | viewHelper.gamePath = "+viewHelper.gamePath);
            pif.launchApp.forceStopPackage(viewHelper.gamePath)
            //  pif.launchApp.closeApp()
        }
        if(viewController.getActiveScreenPopupID() != 0) viewController.hideScreenPopup();
        parentMidTvSeries=-1
    }
    /*******************************************Karma Variables************************************************************************/
    property bool bootUp: false
    property bool shortcutCalled: false
    property bool isLaunchingAppFromKarma: false
    property int isLaunchingAppIdFromKarma: -1
    property string sectionSelected: ''
    property string playingCategory: ''
    property variant pcid
    property variant ptid
    property variant maincid
    property variant tid
    property variant karmaAodBrdcastModel
    property variant karmaGameModel
    property int  karmaCurrentPlayinMid: 0
    property int aodBroadcastMid:0
    property int radioAlbumSelected: 0
    signal surveyFromKarma()
    signal playEpisode(int index)
    signal jumpToWelcome(string section,string lang)
    signal playAudioBroadcast()
    signal showUsb(string usbStatus)
    signal launchKarmaGame()
    onLaunchKarmaGame:{
        gameMid=karmaGameModel.getValue(0,"mid");
        var path = karmaGameModel.getValue(0,"filename");
        launchGames(path);
    }
    onPlayAudioBroadcast:{
        core.debug("pif.aod.getPlayingAlbumId() : "+pif.aod.getPlayingAlbumId())
        if(pif.aod.getPlayingAlbumId() != aodBroadcastMid ){
            var params;
            params = {
                "amid" : aodBroadcastMid,
                "duration" :karmaAodBrdcastModel.getValue(0,"duration"),
                "elapseTimeFormat" : 2,
                "cid" : 0,
                "playIndex" : 0,
                "elapseTime" : 0,
                "mediaType": "audioAggregate",
                "repeatType":0,
                "playType":0,
                "shuffle":false
            };
            pif.aod.playByModel(karmaAodBrdcastModel,params);
        }else{
            pif.lastSelectedAudio.setPlayingIndex(radioAlbumSelected);
        }
    }

    function loadShortcutByCid(catnode){
        loadShortCutTest(catnode);
    }
    function loadShortCutTest(catnode){
        shortcutCalled = true
        pcid = catnode["pcid"];
        ptid = catnode["ptid"];
        maincid = catnode["cid"];
        tid = catnode["tid"];
        console.log("pcid"+pcid )
        console.log("ptid"+ptid )
        console.log("maincid"+maincid )
        console.log("tid"+tid )
        if(pif.getExternalAppActive()){
            pif.launchApp.closeApp()
        }
        if(ptid=="discover" && (tid=='airport_map'||tid=='survey'||tid=='milesapp'||tid=='airline_info' ||tid=='seatchat'||tid=="airline_tour")){

            //            if(viewController.getActiveScreenRef().getCidTidForListingKarma){
            //                console.log("viewController.getActiveScreenRef().getCidTidForListing()[1] = "+viewController.getActiveScreenRef().getCidTidForListingKarma()[0])
            //                if(viewController.getActiveScreenRef().getCidTidForListingKarma()[0]=="seatchat" && tid=="seatchat")return
            //                else if(viewController.getActiveScreenRef().getCidTidForListingKarma()[0]=="airport_map" && tid=="airport_map")return
            //                else if(viewController.getActiveScreenRef().getCidTidForListingKarma()[0]=="survey" && tid=="survey")return
            //                else if(viewController.getActiveScreenRef().getCidTidForListingKarma()[0]=="airline_info" && tid=="airline_info")return
            //                else if(viewController.getActiveScreenRef().getCidTidForListingKarma()[0]=="airline_tour" && tid=="airline_tour")return
            //                else if(viewController.getActiveScreenRef().getCidTidForListingKarma()[0]=="milesapp" && tid=="milesapp")return
            //            }
            widgetList.setCurrentIndexForDiscover()
            viewController.jumpTo(pcid,ptid)
        }else if(tid=="usb"){
            widgetList.setCurrentIndexForUsb()
            viewController.jumpTo(maincid,tid)
            if(isCurtainFirstTime){
                widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,mainMenuIndex,false)
            }
            isCurtainFirstTime=false
        }else if(tid=='maps'){
            widgetList.showFlightMap(true)
            shortcutCalled=false
        }
    }
    function resetVariables(){
        directJumpFromKids=""
        isExternalOnVOD=""

    }
    /*******************************************************************************************************************/
    SimpleModel{id:blankModel;}
    Component{
        id:blankComp
        Item{}
    }
    property bool isFirstTimeBrightness: true
    Timer{
        id:timeDelayForBrightness
        interval: 2000;
        onTriggered: {
            isFirstTimeBrightness=false;
        }
    }
    function loadEntOnScreen(){
        isFirstTimeBrightness=true;
        timeDelayForBrightness.restart();
        showAndroidBarTimer.restart()
        widgetList.showVkb(false)
        setInitialLocale()
        pif.setButtonInput(true)
        core.pif.setBrightnessByValue(core.pif.getDefaultBrightness())
        pif.disablePssAlertState()
        updatePremiumHandset(2,false);

        isAviancaBrasilAir = pif.getLruData("Airline_ICAO")=="ONE"?true:false;

        core.debug(" isAviancaBrasilAir : "+isAviancaBrasilAir)
        // updateAndroidFooter()
        pif.setBacklightOn();
        if(settings.resolutionIndex==1){
            core.width=1024;
            core.height=600;
        }
        bootUp=true
        languageModel=dataController.getIntLanguageModel()
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)pif.sharedModelServerInterface.sendApiData("userDefinedsetSeatStatus",{value:"ready",languageIso:core.settings.languageISO});
        else if(pif.getHandsetType()==pif.cSTD_HANDSET)pif.standardHandsetDisplay("   ")
        createPlaylistStack();
        setScreenSaverData(5,60,30)
        config_SeatChat()                //Add code to config seatchat
        //  getSeatLetters()
        viewController.jumpTo(0,"WELC");
        updatePremiumHandset(1,true);
        if(pif.seatInteractive) pif.seatInteractive.setAudioPlayerdataRef(audioPlayer);
        if(pif.getPAState()){viewController.updateSystemPopupQ(1,true);}
        var temp=Array();
        multipleSamePopupInstance=temp;
    }

    //    ListModel{id:seat_alphabets}
    //    property variant  seatLetters: []
    //    property alias seat_alphabets: seat_alphabets
    //    function getSeatLetters(){
    //         seatLetters = pif.getLru1DArrayData("Seat_letters")
    //        if(seatLetters== undefined)return;
    //        core.log("getSeatLetters >>>>>>>>>>> seatLetters = "+seatLetters)
    //        for(var i=0;i<seatLetters.length;i++){
    //            seat_alphabets.append({"letter": seatLetters[i]})
    //            core.log("getSeatLetters >>>>>>>>>>> seat_alphabets [ i ]= "+seat_alphabets.get(i).letter)
    //        }
    //    }

    function config_SeatChat(){
        console.log("ViewHelper.qml | config_SeatChat ")
        core.dataController.getCategoryByTemplateId(["seatchat"],conf_seatchatModel)
    }
    function conf_seatchatModel(dModel){
        core.log("ViewHelper.qml | conf_seatchatModel = "+dModel.count);
        if(dModel.count==0){
            seatchat_enabled=false
        }else{seatchat_enabled=true}
    }

    function updateAndroidFooter(){
        if(pif.android) {
            pif.android.setSystemBarVisibility(pif.android.cBAR_VISIBLE)
            pif.android.setImmersiveMode(pif.android.cBAR_IMMERSIVE_NONE)
        }
    }

    function loadEntOffScreen(){
        hideAndroidBarTimer.restart();
        widgetList.showVkb(false)
        widgetList.resetAllWidgets()
        widgetList.closeAllwidgets()
        pif.setButtonInput(true)
        core.pif.setBrightnessByValue(core.pif.getDefaultBrightness())
        updatePremiumHandset(2,true);
        pif.disablePssAlertState()
        // updateAndroidFooter()
        isAviancaBrasilAir = false;//pif.getLruData("Airline_ICAO")=="ONE"?true:false;
        if(pif.getHandsetType()==pif.cSTD_HANDSET)pif.standardHandsetDisplay("   ")
        widgetList.setHelpStatus(false); //hiding help on ent off screen
        pif.setBacklightOn();
        bootUp=false
        sectionSelected="";
        viewController.jumpTo(0,"EntOff");
        setScreenSaverData(10,60,30)
        resetVariables()
        setUsbTerms(false)
        var temp=Array();
        multipleSamePopupInstance=temp;
        chatSessionModel.clear()
    }
    function loadCloseFlightScreen(){
        hideAndroidBarTimer.restart();
        pif.setButtonInput(true)
        viewController.jumpTo(0,"EntOff");
        pif.setBacklightOn();
        setScreenSaverData(10,60,30)
        core.pif.setBrightnessByValue(core.pif.getDefaultBrightness())
        pif.disablePssAlertState()
        isAviancaBrasilAir = false;//pif.getLruData("Airline_ICAO")=="ONE"?true:false;
        //updateAndroidFooter()
        if(pif.getHandsetType()==pif.cSTD_HANDSET)pif.standardHandsetDisplay("   ")
        widgetList.setHelpStatus(false); //hiding help on ent off screen
        bootUp=false
        //        chatActive=false
        //seat_alphabets.clear()
        chatSessionModel.clear();
        setSeatChatTerms(false)
        fromInvitePopup=false
        isHospitalityCountDefault=false;
        totalHospitalityCountDefault=-1
    }

    Timer{id:hideAndroidBarTimer;interval:1500;onTriggered:{hideAndroidBar();}}
    Timer{id:showAndroidBarTimer;interval:1500;onTriggered:{showAndroidBar();}}

    function hideAndroidBar(){
        console.log(" ViewHelper | Inside hideAndroidBar 1")
        if(pif.android){
            console.log("ViewHelper | Inside hideAndroidBar 2")
            showAndroidBarTimer.stop()
            pif.android.setSystemBarVisibility(pif.android.cBAR_HIDE);
            pif.android.setImmersiveMode(pif.android.cBAR_IMMERSIVE_STICKY);
           //pif.android.setImmersiveMode(pif.android.cBAR_IMMERSIVE);
        }
    }
    function showAndroidBar(){
        console.log(" ViewHelper | Inside showAndroidBar 1")
        if(pif.android){
            console.log(" ViewHelper | Inside showAndroidBar 2")
            hideAndroidBarTimer.stop();
            pif.android.setSystemBarVisibility(pif.android.cBAR_VISIBLE);
            pif.android.setImmersiveMode(pif.android.cBAR_IMMERSIVE_NONE);
        }
    }

    Component.onCompleted :{
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOnScreen,[loadEntOnScreen]);
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOffScreen,[loadEntOffScreen]);
        interactiveStartup.registerToStage(interactiveStartup.cStageCloseFlightScreen,[loadCloseFlightScreen]);
        // if(!core.pc)pif.android.keepScreenOn();
        core.debug(" on Completed ")

        var cfgToolLoaded
        if(pif.getCabinClassName() == "Economy")cfgToolLoaded = coreHelper.fileExists("/tmp/interactive/content/configtool/themes/economy/main.json")
        else if(pif.getCabinClassName() =="FCRC" && pif.getLruData("XResolution")=="1024x600")cfgToolLoaded = coreHelper.fileExists("/tmp/interactive/content/configtool/themes/economy/main.json")
        else if(pif.getCabinClassName() =="FCRC" && pif.getLruData("XResolution")=="1368x768")cfgToolLoaded = coreHelper.fileExists("/tmp/interactive/content/configtool/themes/business/main.json")
        else cfgToolLoaded = coreHelper.fileExists("/tmp/interactive/content/configtool/themes/business/main.json")
        core.debug("ViewHelper.qml | loadEntOnScreen | cfgToolLoaded = "+cfgToolLoaded)
        if(!cfgToolLoaded) {
            core.debug("ViewHelper.qml | loadEntOnScreen Config Tool not ready. Retry")
            pif.restartInteractive();
            return;
        }

        createScreenLogArray();

        if(pif.android){pkTimer.restart();}
    }

    Timer{id:pkTimer;interval:1500;onTriggered:{
            var fileSrc=viewController.settings.assetImagePath+"recent_apps_bg.jpg";
            pif.android.setWallpaperImageFile(fileSrc);
        }}

    /*************Function to Block Keys **************************/
    function blockKeys(){
        core.debug("ViewHelper | blockKeys  | Keys Blocked.....");
        viewKeyListener.blockKeyEvents = true;
    }
    function unblockKeys(){
        core.debug("In ViewHelper | unblockKeys | Keys Unblocked.....");
        viewKeyListener.blockKeyEvents = false;
    }
    function blockKeysForSeconds(sec){
        blockKeys();
        if(!sec)
            unblockKeysTimer.interval = 600;
        else
            unblockKeysTimer.interval = sec * 1000;
        unblockKeysTimer.running = true;
    }
    function isBlock(){
        return unblockKeysTimer.running
    }
    Timer{
        id:unblockKeysTimer
        interval: 1000
        onTriggered: {
            unblockKeys();
        }
    }

    /******************************************************************************/
    function karmaGameCallbk(dmodel){
        var path = dmodel.getValue(0,"filename");
        launchGames(path);
    }

    function launchGames(path){
        gamePath=path
        if(pif.lastSelectedAudio.getMediaState()==pif.lastSelectedAudio.cMEDIA_PLAY && !isLaunchingAppFromKarma){
            viewController.showScreenPopup(9)
        }else if(isLaunchingAppFromKarma){
            widgetList.showAppLoading(true)
            launchGamesonSelection(isMusicGameAudioDetails)
        }else{
            widgetList.showAppLoading(true)
            launchGamesonSelection(false)
        }
        isLaunchingAppFromKarma=false
        isMusicGameAudioDetails=false
        if(core.pc){
            simulateGameExit.restart();
        }
    }
    function launchGamesonSelection(param){
        if(pif.getExternalAppActive()){
            pif.launchApp.closeApp()
            gameMidA = gameMid;
            __delayLaunchParams=[gamePath,'games',param]
            return;
        }
        gameMidA = -1;
        if(gamePath.indexOf(".cfg")!=-1) gamePath = gamePath.substring(0,gamePath.indexOf(".cfg"))
        core.info("Language passed by Interactive "+gameObj["lang"])
        gameLoadInProgress = true
        gameTimerToAvoidPA.restart()
        pif.launchApp.launchApp("/mnt/seatApp.cram/utils/gameload.sh",pif.launchApp.cGAME_USING_PIF,"games",gamePath,param,gameObj);
        /*
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            core.debug("############## gameMid : "+gameMid)
            pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedGameLaunch",{midValue:gameMid})
        }
        */
    }

    function checkGameQueue(){
        core.info('ViewHelper.qml | checkGameQueue() ')
        if(__delayLaunchParams.length>0){
            gameMid = gameMidA;
            gamePath= __delayLaunchParams[0]
            widgetList.showAppLoading(true);
            launchGamesonSelection(__delayLaunchParams[2])
        }
        __delayLaunchParams=[]
    }
    function checkVideoQueue(){
        core.info('ViewHelper.qml | checkVideoQueue() ')
        if(videoAfterGameParams.length>0){
            if(videoAfterGameParams[1]!='trailer'){
                var params=videoAfterGameParams[0]
                if(viewController.getActiveSystemPopupID()==6)viewController.updateSystemPopupQ(6,false);
                var vodParams=new Object()
                vodParams["aggregateMid"]=params.aggregateMid
                vodParams["sndtrkLid"]=params.soundtrackLid
                vodParams["soundtrackType"]=params.soundtrackType
                vodParams["subtitleLid"]=params.subtitleLid
                vodParams["subtitleType"]=params.subtitleType
                vodParams["elapsedTime"]=params.elapsedTime
                vodParams["elapsedTime"]=params.elapsedTime
                viewHelper.setSubtitlelid(params.subtitleLid)
                viewHelper.setSoundTracklid(params.soundtrackLid)
                var action="restart"
                if(params.elapsedTime>0)
                    action="resume"
                var fromTrailer=false
                var vodPlayType
                core.debug("viewHelper.getVodType "+viewHelper.getVodType())
                vodPlayType=(viewHelper.getVodType()==1)?"trailer":(viewHelper.getVodType()==0)?"tvseries":(viewHelper.getVodType()==2)?"episode":"movies"
                viewHelper.initializeVOD(action,vodPlayType,vodParams,true,fromTrailer)
            }else{
                var params=videoAfterGameParams[0];
                viewHelper.isTrailerPressed=true
                var trailerParams=new Object()
                trailerParams["mid"]=params.mid
                trailerParams["aggregateMid"]=params.aggregateMid
                trailerParams["sndtrkLid"]=params.soundtrackLid
                trailerParams["soundtrackType"]=params.soundtrackType
                trailerParams["subtitleLid"]=params.subtitleLid
                trailerParams["subtitleType"]=params.subtitleType
                trailerParams["elapsedTime"]=params.elapsedTime
                var traileraction="restart"
                if(params.elapsedTime>0)
                    traileraction="resume"
                var vodPlayType
                vodPlayType=(viewHelper.getVodType()==1)?"trailer":(viewHelper.getVodType()==0)?"tvseries":(viewHelper.getVodType()==2)?"episode":"movies"
                viewHelper.initializeVOD(traileraction,vodPlayType,trailerParams,true,true)

            }
        }
        videoAfterGameParams=[]
    }
    /*************Functions to be used with the js****************/

    function makeAggList(model){
        Code.aggList=new Array();
        for(var i=0;i<model.count;i++){
            var t = model.getValue(i,"aggregate_parentmid")
            Code.aggList[t]=1;
        }
    }

    function getAggList(){
        return Code.aggList;
    }

    function getScript(){
        return Code
    }
    function createPlaylistStack(){
        Code.playlistAddStack=new Array()
    }
    function addToPlaylistStack(mid){
        Code.playlistAddStack[mid]=1;

    }
    function removeFromPlaylistStack(mid){
        Code.playlistAddStack[mid]=undefined;
    }
    function clearPlaylistStack(){
        Code.playlistAddStack=new Array()
    }
    function inPlaylistStack(mid){
        if(Code.playlistAddStack[mid]){
            return true
        }
        return false;

    }

    function getPlaylistStackLen(){
        return    Code.playlistAddStack.length;
    }

    function appendToGenreSongModel(dmodel){
        genreSongModel.clear();
        genreSongOrgModel.clear();
        genreSongModel.appendModel(dmodel);
        genreSongOrgModel.appendModel(dmodel);
    }
    function getGenreSongModel(){
        return genreSongModel;
    }
    function getGenreSongOrgModel(){
        return genreSongOrgModel;
    }
    function shuffleGenreModel(flag){
        if(flag){
            genreSongModel.shuffle();
        }else{
            genreSongModel.clear();
            genreSongModel.appendModel(genreSongOrgModel);
        }
    }

    function setScreenSaverData(x,y,z){
        pif.screenSaver.setScreenSaverType(1);
        pif.screenSaver.setScreenSaverTimeout(x)
        pif.screenSaver.setDimTimeout(y);
        pif.screenSaver.setDimBrightness(z);
        pif.screenSaver.restartScreenSaver();

    }

    function playGenreFromKarma(model){
        var ind=genreSongIndex;

        genreSongOrgModel.clear();
        genreSongOrgModel.appendModel(model);
        genreSongModel.clear();
        genreSongModel.appendModel(model);
        if(usbTrackPlayed)
            stopUsbMp3()

        var model = getGenreSongOrgModel()
        if(dataController.mediaServices.getMidBlockStatus(model.getValue(ind,"aggregate_parentmid"),model.getValue(ind,"rating"))){
            viewController.updateSystemPopupQ(7,true)
            return;
        }
        if(ind==-1)ind=model.count-1;
        else if(ind==model.count)ind=0;
        var params;
        params = {
            "amid" : model.getValue(ind,"aggregate_parentmid"),
            "duration" : model.getValue(ind,"duration"),
            "elapseTimeFormat" : 6,
            "cid" : widgetList.getCidTidMainMenu()[0],
            "playIndex" : 0,
            "elapseTime" : 0,
            "mediaType": "audioAggregate",
            "repeatType":pif.aod.getMediaRepeat(),
            "playType":2,
            "shuffle":pif.aod.getMediaShuffle()
        };
        core.debug(" params : "+params["amid"])
        var tmid = model.getValue(ind,"mid");
        core.debug(" tmid : "+tmid)
        singleTrack.clear();
        singleTrack.append({"amid":params["amid"],"mid":tmid,"media_type":params["mediaType"],"duration":params["duration"], "cid":params["cid"]});
        pif.aod.playByModel(singleTrack,params)
        playingCategory="music";
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:playingCategory,genre:true})
        genreSongIndex=ind;

    }


    function createScreenLogArray(){
        Code.screenLogArray=new Array();
        Code.screenLogArray["EntOff"]="Entertainment Off Screen";
        Code.screenLogArray["WELC"]="Welcome Screen"
        Code.screenLogArray["kids"]="Kids Main Screen"
        Code.screenLogArray["adult"]="Main Menu Screen"
        Code.screenLogArray["movies"]="Movies Screen"
        Code.screenLogArray["tv"]="TV Screen"
        Code.screenLogArray["music"]="Music Screen"
        Code.screenLogArray["games"]="Games Screen"
        Code.screenLogArray["seatchat"]="Seat Chat Screen"
        Code.screenLogArray["shopping"]="Shopping Screen"
        Code.screenLogArray["discover"]="Discover Screen"
        Code.screenLogArray["connecting_gate"]="Connecting Gate Screen"
        Code.screenLogArray["survey"]="Survey Screen";
        Code.screenLogArray["usb"]="USB"
        Code.screenLogArray["usb_images"]="USB Images Screen"
        Code.screenLogArray["usb_music"]="USB Music Screen"
        Code.screenLogArray["usb_doc"]="USB Document Screen"
        Code.screenLogArray["kids_movies_listing"]="Kids Movies Screen"
        Code.screenLogArray["kids_tv_listing"]="Kids TV Screen"
        Code.screenLogArray["kids_music_listing"]="Kids Music Screen"
        Code.screenLogArray["kids_games_listing"]="Kids Game Screen"
    }


    AudioPlayerdata{id:audioPlayer;}
    SimpleModel{id:genreSongModel}
    SimpleModel{id:genreSongOrgModel}
    SimpleModel{id:singleTrack}
    ViewKeyListener{id:viewKeyListener}

//    Timer{
//        interval:5000;
//        repeat:true;
//        running:(!widgetList.isVideoVisible())
//        onTriggered:{
//            if(core.pc)return;
//            if(widgetList.isVideoVisible())return;
//            if(pif.android.getSystemBarVisibility()==pif.android.cBAR_HIDE){
//                pif.android.setSystemBarVisibility(pif.android.cBAR_VISIBLE);
//            }
//        }
//    }

    Timer{
        id:simulateGameExit;
        interval:5000;
        onTriggered:{
            pif.launchApp.sigAppExit("games",4,"eng")
        }
    }



    /**********************************************************************************************************************/
    /**********************************************************************************************************************/
    /**********************************************************************************************************************/
    /**********************************************************************************************************************/
    /******************************** Added this last minute code for genre to work from karma and handset*****************/

    onCustomHandsetSignal:{
        if(hstStr=="fwd"){
            if(trackStop)return;
            aodHandsetControls=false
            if(genreSongIndex!=-1){
                playedFromGenre((genreSongIndex+1))

            }
        }else if(hstStr=="rwd"){
            if(trackStop)return;
            aodHandsetControls=false
            if(genreSongIndex!=-1){
                playedFromGenre((genreSongIndex-1))
            }

        }else if(hstStr=="shuffle"){
            core.debug("########################### pif.lastSelectedAudio.getMediaShuffle() : "+pif.lastSelectedAudio.getMediaShuffle())
            if(pif.lastSelectedAudio.getMediaShuffle()){
                if(genreSongIndex>=0){
                    shuffleGenreModel(false);
                }
                pif.lastSelectedAudio.setMediaShuffle(false);
            }else{
                if(genreSongIndex>=0){
                    //its a shuffle from genreInit
                    shuffleGenreModel(true);
                }
                pif.lastSelectedAudio.setMediaShuffle(true);
            }
        }else if(hstStr=="repeat"){
            if(pif.lastSelectedAudio.getMediaRepeat()==1){
                pif.lastSelectedAudio.setMediaRepeat(0);
            }else{
                pif.lastSelectedAudio.setMediaRepeat(1);
            }

        }
    }

    function playedFromGenre(ind,node){
        if(usbTrackPlayed)
            stopUsbMp3()
        if(node){
            var model = getGenreSongOrgModel()
        }else{
            var model = getGenreSongModel()
        }
        if(dataController.mediaServices.getMidBlockStatus(model.getValue(ind,"aggregate_parentmid"),model.getValue(ind,"rating"))){
            viewController.updateSystemPopupQ(7,true)
            return;
        }

        if(ind==-1)ind=model.count-1;
        else if(ind==model.count)ind=0;
        var params;
        params = {
            "amid" : model.getValue(ind,"aggregate_parentmid"),
            "duration" : model.getValue(ind,"duration"),
            "elapseTimeFormat" : 6,
            "cid" : widgetList.getCidTidMainMenu()[0],
            "playIndex" : 0,
            "elapseTime" : 0,
            "mediaType": "audioAggregate",
            "repeatType":pif.aod.getMediaRepeat(),
            "playType":2,
            "shuffle":pif.aod.getMediaShuffle()
        };
        core.debug(" params : "+params["amid"])
        var tmid = model.getValue(ind,"mid");
        core.debug(" tmid : "+tmid)
        singleTrack.clear();
        singleTrack.append({"amid":params["amid"],"mid":tmid,"media_type":params["mediaType"],"duration":params["duration"], "cid":params["cid"]});
        pif.aod.playByModel(singleTrack,params)
        playingCategory="music";
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:playingCategory,genre:true})
        genreSongIndex=ind;
    }
    Connections{
        target:pif.lastSelectedAudio;
        onSigMidPlayCompleted:{
            if(genreSongIndex>=0){
                playedGenreSong=true;
                trackStop=false;
                if(genreSongIndex==(getGenreSongModel().count-1)){
                    if(pif.lastSelectedAudio.getMediaRepeat()==1){
                        playedFromGenre(0)
                    }
                }else{
                    playedFromGenre(genreSongIndex+1)
                }
            }
        }
        onSigMidPlayBegan:{
            if(pif.lastSelectedAudio.getPlayingMid()!=pif.lastSelectedAudio.getPlayingAlbumId()){
                trackStop=false;
            }
        }

        onSigAggPlayStopped:{
            if(playedGenreSong){
                playedGenreSong=false;
            }else{
                genreSongIndex=-1;
                trackStop=true;
            }
        }
    }

    property bool playedGenreSong:false;
    /*************************************Seat Chat functions*********************************************************************************/

    function sendChatMessage(text){
        console.log("qml | sendChatMessage | pif.seatChat.getCurrentSessionId() = "+pif.seatChat.getCurrentSessionId())
        pif.seatChat.sendChatMessage(pif.seatChat.getCurrentSessionId(),text)
    }


    ListModel{id:chatSessionModel}


    function updateChatSessionModel(session,seatNum){
        var sessionIndex = searchEntryInListModel(pif.seatChat.sessionListModel,"session",session);
        core.debug(" sessionIndex : "+sessionIndex)
        if(sessionIndex==-1)return
        console.log(">>>>>>>>>>>>>>>> pif.seatChat.sessionListModel.count "+pif.seatChat.sessionListModel.count)
        for(var key in pif.seatChat.sessionListModel.get(sessionIndex))console.log(">>>>>>>>>>> key "+key+" pif.seatChat.sessionListModel "+pif.seatChat.sessionListModel.get(sessionIndex)[key])
        buddyList=pif.seatChat.sessionListModel.get(sessionIndex).participantSeatNo
        core.debug(" buddylist : "+buddyList )
        var arrs=buddyList==""?[]:buddyList.split(",")   //  to get the nick name
        console.log(">>>>>>>>>>>>>arrs.length "+arrs.length)
        //        if(buddyLen[session]==1)
        var tempName=[]
        for(var i=0;i<arrs.length;i++){
            var a=pif.seatChat.getSeatNameArray()[arrs[i]];
            if(arrs.length==1 && a!=""){
                tempName.push(arrs[i])
                tempName.push(a)
                break;
            }
            tempName.push(arrs[i])
        }
        if(arrs.length>0)buddyList=tempName
        console.log(">>>>>>>>>>>>>>>>participantNames "+participantNames)
        if(arrs.length==1 && a!=""){
            participantNames=buddyList!=""?buddyList.join(" | "):""
        }else{
            participantNames=buddyList!=""?buddyList.join(","):""
        }
        var temp=new Object()    // to get buddlen
        var breakin=false
        for(var j=0;j<temp.count;i++){
            if(temp[session]){
                temp[session]=pif.seatChat.sessionListModel.get(j).participantCount;
                breakin=true;
                break;
            }
        }
        if(!breakin)temp[session]=pif.seatChat.sessionListModel.get(sessionIndex).participantCount
        core.debug(" temp[session] : "+temp[session]+ "temp = "+temp.count)
        buddyLen = temp
        core.debug(" buddylen : "+buddyLen.count)
        unreadmsgCount = pif.seatChat.sessionListModel.get(sessionIndex).unReadMsgCount
        core.debug(" unreadmsgCount : "+unreadmsgCount)
        viewSessionIndex=searchEntryInListModel(chatSessionModel,"session",session);
        core.debug(" viewSessionIndex : "+viewSessionIndex)

        if(viewSessionIndex<0){
            console.log(">>>>>>>>>>>>>>>> buddyLen[session] "+buddyLen[session])
            if(buddyLen[session]>0){
                chatSessionModel.append({"participant":participantNames,"session":session,"participantCount":buddyLen[session],"unreadmsgCount":unreadmsgCount})
            }
        }else{
            if(buddyLen[session]==0){
                core.debug(" updateChatSessionModel | >>>>>>>>>>>>>>sessionIndex : "+viewSessionIndex)
                chatSessionModel.remove(viewSessionIndex)
            }else{
                core.debug(" ****************************sessionIndex : "+sessionIndex+ " buddylen = "+buddyLen[session] )
                chatSessionModel.setProperty(viewSessionIndex,"participant",participantNames)
                chatSessionModel.setProperty(viewSessionIndex,"participantCount",buddyLen[session])
                chatSessionModel.setProperty(viewSessionIndex,"unreadmsgCount",unreadmsgCount)

            }
        }
    }

    function removeChatSessionModel(session){
        var viewSessionIndex=searchEntryInListModel(chatSessionModel,"session",session);
        if(viewSessionIndex>-1){
            chatSessionModel.remove(viewSessionIndex)
        }
    }

    property bool fromInv:false;

    function toacceptInvitationPopup(){
        console.log("toacceptInvitationFromPopup |  from popup "+fromInvitePopup)
        if(fromInvitePopup){
            console.log("toacceptInvitationFromPopup |  from popup")
            console.log("toacceptInvitationFromPopup |  cSessionID | "+cSessionID)
            fromInv=true;

            pif.seatChat.setCurrentSessionId(cSessionID)
            pif.seatChat.acceptInvitation(cSessionID,tSeatNo)


        }else{ console.log("toacceptInvitationFromPopup | NOT from popup")}
        acceptInviataionfromPopup();
    }



    /**********************************************************************************************************************/


    function searchEntryInListModel(model,field,value){
        core.debug("qml | searchEntryInListModel = "+model.count+"field = "+field +"value = "+value)
        for(var i = 0;i<model.count;i++){
            core.debug("inside for loop = "+model.count+"field = "+field +"value = "+value +" i "+ i+" Hello "+model.get(i)[""+field])
            if(model.get(i)[""+field]==value){
                core.debug("if loop = "+model.count+"field = "+field +"value = "+value + " i "+ i)
                return i
            }
        }
        return -1
    }

    /**********************************************************************************************************************/

    function setInitialLocale(){
        var temp=new Object()
        for(var i=0;i<dataController.getIntLanguageModel().count;i++){
            temp[dataController.getIntLanguageModel().get(i).ISO639.toLowerCase()]=dataController.getIntLanguageModel().get(i).language_locale
        }
        localeLanguages=temp
    }

    /**********************************************************************************************************************/



}


