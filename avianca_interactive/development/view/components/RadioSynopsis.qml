import QtQuick 1.0

FocusScope {
    id:radioSynopsis

    property variant configValue:configTool.config.music.radio;
    property variant configTag:configTool.config.music.tracklist;
//    property bool isVolumeOpen: false
//    property variant configVideoTag:viewHelper.isKids(current.template_id)?configTool.config.kids_global.video:configTool.config.global.video;
    /**************************************************************************************************************************/
    Connections{
        target:pif.aod;
        onSigMediaElapsed:{
        }
        onSigMidPlayBegan:{
            if(viewHelper.trackStop) playNStop.cstate=true;
            else  playNStop.cstate=false;
            radioView.selIndex=radioView.currentIndex
        }
        onSigMediaPaused:{
            if(viewHelper.trackStop)
                playNStop.cstate=false;
            radioView.selIndex=-1
        }
        onSigMidPlayStopped:{
            radioView.selIndex=-1
            playNStop.cstate=false
        }
    }
    /**************************************************************************************************************************/
    function init(){
        viewHelper.setHelpTemplate("radio_listing");
        radioView.currentIndex=0;
        radioView.model = radioData;
        if(pif.aod.getMediaState()==pif.aod.cMEDIA_PLAY ){
            radioView.selIndex=  core.coreHelper.searchEntryInModel(radioView.model,"mid",pif.aod.getAllMediaPlayInfo().amid)
        }
        radioView.forceActiveFocus();
    }
    function clearOnExit(){
        radioView.selIndex=0;
    }
    function backBtnPressed(){
        viewHelper.homeAction();
    }

    function langUpdate(){
        dataController.refreshModelsByIntLanguage([radioView.model],[donothing]);
    }

    function donothing(){}

    function specialFocus(dir){
        if(dir=="down"){
            radioView.currentIndex=radioView.indexAt(10,radioView.contentY)
            radioView.forceActiveFocus();

        }
    }

    /**************************************************************************************************************************/
    Image{
        id:panel;
        source:viewHelper.configToolImagePath+configValue["panel_bg"];
        anchors.horizontalCenter:parent.horizontalCenter;
        y:qsTranslate('',"artist_art_panel_tm");
   /*     MouseArea{
            anchors.fill:parent
            onClicked: {
                isVolumeOpen=false
            }
        }*/

    }
    Rectangle{
        height:qsTranslate('','common_line_h');
        width:radioView.width;
        anchors.bottom:radioView.top;
        anchors.left:radioView.left;
        color:core.configTool.config.music.tracklist.description1_color;
    }
    ListView{
        id:radioView;
        anchors.left:panel.left;
        anchors.top: panel.top;
        anchors.topMargin:qsTranslate('','radio_radioview_tm')
        anchors.leftMargin:qsTranslate('','radio_radioview_lm')
        width:qsTranslate('','radio_radioview_w');
        height:qsTranslate('','radio_radioview_h');
        snapMode:ListView.SnapToItem;
        boundsBehavior: ListView.StopAtBounds;
        property int selIndex:-1;
        Keys.onUpPressed:{
            if(currentItem.y==contentY){
                subCatobj.forceActiveFocus()
            }else{
                decrementCurrentIndex();
            }
        }
        Keys.onRightPressed: {
            if(radioScroll.visible){
                radioScroll.forceActiveFocus()
            }else{
                playNStop.forceActiveFocus();
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                select();
            }
        }
        clip:true;
        delegate:Item{
            id:radioSection;
            width:qsTranslate('','radio_radio_w');
            height:qsTranslate('','radio_radio_h');
            Image{
                id:high
                source:viewHelper.configToolImagePath+configValue["radio_highlight"];
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('','radio_radio_high_bm')
                height:qsTranslate('','radio_radio_high_h');
                width:parent.width;
                opacity:(radioView.currentIndex==index && radioView.activeFocus)?1:0
            }
            Image{
                id:poster1
                width:qsTranslate('','radio_poster_w');
                height:qsTranslate('','radio_poster_h')
                anchors.verticalCenter: parent.verticalCenter
                source:viewHelper.cMediaImagePath+poster;
            }
            ViewText{
                id:stnTitle
                anchors.left:poster1.right;
                anchors.leftMargin:qsTranslate('','radio_title_lm');
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('','radio_title_tm');
                visible: description==""?false:true
                width:qsTranslate('','radio_title_w');
                height:qsTranslate('','radio_title_h');
                varText:[title,(radioView.selIndex==index)?core.configTool.config.music.tracklist["title_color"]:core.configTool.config.music.tracklist["description1_color"],qsTranslate('','radio_title_fontsize')]
                elide:Text.ElideRight;
            }
            ViewText{
                anchors.left:stnTitle.left;
                anchors.top:stnTitle.bottom;
                anchors.topMargin:qsTranslate('','radio_desc_tm');
                visible: description==""?false:true
                width:qsTranslate('','radio_desc_w');
                height:qsTranslate('','radio_desc_h');
                varText:[description,(radioView.selIndex==index)?core.configTool.config.music.tracklist["title_color"]:core.configTool.config.music.tracklist["description1_color"],qsTranslate('','radio_desc_fontsize')]
                elide:Text.ElideRight;
                maximumLineCount:1;
                wrapMode:"WordWrap"
            }
            ViewText{
                id:stnTitleNoDescription
                visible: !stnTitle.visible?true:false
                anchors.left:poster1.right;
                anchors.leftMargin:qsTranslate('','radio_title_lm');
                anchors.verticalCenter: parent.verticalCenter
                width:qsTranslate('','radio_title_w');
                height:parseInt(qsTranslate('','radio_title_h'),10)
                varText:[title,(radioView.selIndex==index)?core.configTool.config.music.tracklist["title_color"]:core.configTool.config.music.tracklist["description1_color"],qsTranslate('','radio_title_fontsize')]
                verticalAlignment: Text.AlignVCenter
                elide:Text.ElideRight;
            }
            Rectangle{
                height:qsTranslate('','common_line_h');
                width:parent.width;
                anchors.bottom:parent.bottom;
                color:core.configTool.config.music.tracklist.description1_color;
            }
            MouseArea{
                anchors.fill:high
                onClicked:{
                    radioView.currentIndex=index;
                    radioView.select();
                }
            }

        }
        onMovementEnded: radioView.currentIndex=radioView.indexAt(radioView.contentX+5,radioView.contentY+3)
        function select(){
            if(viewHelper.usbTrackPlayed)
                viewHelper.stopUsbMp3()
            var tmid = model.getValue(currentIndex,"mid")? model.getValue(currentIndex,"mid"):-1;
            if(selIndex==currentIndex)return;
            selIndex = currentIndex;
            //            if(pif.aod.getPlayingMid()==tmid)return;
            var params;
            params = {
                "amid" : tmid,
                "duration" : model.getValue(selIndex,"duration"),
                "elapseTimeFormat" : 6,
                "cid" : getCidTidForSubCat()[0],
                "playIndex" : 0,
                "elapseTime" : 0,
                "mediaType": "audioAggregate",
                "repeatType":true,
                "playType":2,
                "shuffle":false
            };
            core.debug("Paxus will be logged with the cid : "+params.cid)
            pif.aod.play(tmid,params);
            viewHelper.trackStop=true;
            viewHelper.playingCategory="music";
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory,genre:false})
        }
    }
    ListModel{id:singleTrack}
    Scroller{
        id:radioScroll
        targetData:radioView;
        sliderbg:[qsTranslate('','radio_scrollcont_base_margin'),qsTranslate('','radio_scrollcont_w'),qsTranslate('','radio_scrollcont_base_h'),configTag["scroll_base"]]
        scrollerDot:configTag["scroll_button"]
        upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
        downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
        anchors.left:radioView.right;
        anchors.leftMargin:qsTranslate('','radio_scrollcont_lm')
        height: qsTranslate('','radio_scrollcont_h');
        width: qsTranslate('','radio_scrollcont_w');
        anchors.verticalCenter: radioView.verticalCenter;
        onSliderNavigation:{
            if(direction=="left"){
                radioView.forceActiveFocus();
            }else if(direction=="right"){
                playNStop.forceActiveFocus();
            }
        }
        onArrowPressed: radioView.currentIndex=radioView.indexAt(radioView.contentX+5,radioView.contentY+3)

    }
    //    Image{
    //        id:synopPoster
    //        source:viewHelper.cMediaImagePath+radioView.model.getValue(radioView.selIndex,"synopsisposter");
    //        anchors.right:panel.right;
    //        anchors.rightMargin: qsTranslate('','radio_synop_poster_rm');
    //        anchors.top:panel.top;
    //        anchors.topMargin: qsTranslate('','radio_synop_poster_tm');
    //        width:qsTranslate('','radio_synop_poster_w');
    //        height:qsTranslate('','radio_synop_poster_h');
    //    }
    //    ViewText{
    //        id:synopTitle
    //        anchors.top:synopPoster.bottom;
    //        anchors.topMargin:qsTranslate('','radio_synop_title_tm');
    //        anchors.horizontalCenter:synopPoster.horizontalCenter;
    //        width:qsTranslate('','radio_synop_title_w');
    //        height:qsTranslate('','radio_synop_title_h');
    //        horizontalAlignment:Text.AlignHCenter
    //        varText: [radioView.model.getValue(radioView.selIndex,"title"),configTag["description1_color"],qsTranslate('','radio_synop_title_fontsize')]
    //    }
    ToggleNavButton{
        id:playNStop
        //  anchors.horizontalCenter: synopPoster.horizontalCenter;
        anchors.top: panel.top;
        anchors.topMargin:qsTranslate('','radio_btns_tm');
        anchors.right:panel.right
        anchors.rightMargin:qsTranslate('','radio_btns_rm');
        normImg2:viewHelper.configToolImagePath+configValue["play_n"]
        normImg1:viewHelper.configToolImagePath+configValue["stop_n"]
        highImg2:viewHelper.configToolImagePath+configValue["play_h"];
        highImg1:viewHelper.configToolImagePath+configValue["stop_h"];
        pressImg2:viewHelper.configToolImagePath+configValue["play_p"];
        pressImg1:viewHelper.configToolImagePath+configValue["stop_p"];
        isHighlight:activeFocus;
        cstate:(viewHelper.trackStop)?(pif.aod.getMediaState()==pif.aod.cMEDIA_PLAY):false;

        nav: ["",subCatobj,"",mute_btn];
        onNoItemFound:{
            if(dir=="left"){
                if(radioScroll.visible){
                    radioScroll.forceActiveFocus()
                }else{
                    radioView.forceActiveFocus();
                }
            }
        }

        onEnteredReleased:{
            if(viewHelper.trackStop && pif.aod.getMediaState()!=pif.aod.cMEDIA_STOP){
                if(cstate) pif.aod.pause()
                else pif.aod.resume()
            }else   radioView.select();

        }
    }
    ToggleNavButton{
        id:mute_btn
        //  anchors.horizontalCenter: synopPoster.horizontalCenter;
        anchors.top: playNStop.bottom;
        anchors.topMargin:qsTranslate('','radio_btns_gap1');
        anchors.horizontalCenter: playNStop.horizontalCenter
        normImg2:viewHelper.configToolImagePath+configValue["mute_off_n"] //mute_on_n
        normImg1:viewHelper.configToolImagePath+configValue["mute_on_n"] //mute_off_n
        highImg2:viewHelper.configToolImagePath+configValue["mute_off_h"]; //mute_on_h
        highImg1:viewHelper.configToolImagePath+configValue["mute_on_h"];//mute_off_h
        pressImg2:viewHelper.configToolImagePath+configValue["mute_off_p"];//mute_on_p
        pressImg1:viewHelper.configToolImagePath+configValue["mute_on_p"];//mute_off_p
        isHighlight:activeFocus;
        cstate:pif.getMuteState()////(viewHelper.trackStop)?(pif.aod.getMediaState()==pif.aod.cMEDIA_PLAY):false;

        nav: ["",playNStop,"",vol_btn];
        onNoItemFound:{
            if(dir=="left"){
                if(radioScroll.visible){
                    radioScroll.forceActiveFocus()
                }else{
                    radioView.forceActiveFocus();
                }
            }
        }
        onEnteredReleased:{
            pif.toggleMuteState()
        }
    }
    SimpleNavButton {
        id: vol_btn
        anchors.top: mute_btn.bottom;
        anchors.topMargin:qsTranslate('','radio_btns_gap2');
        anchors.horizontalCenter: playNStop.horizontalCenter
        isHighlight: activeFocus?true:false
        //        isSelected: isVolumeOpen?true:false
        normImg:  viewHelper.configToolImagePath+configValue["radio_vol_n"]
        highlightimg: viewHelper.configToolImagePath+configValue["radio_vol_h"]
        //        selectedImg: viewHelper.configToolImagePath+configValue["radio_vol_h"]
        //        pressedImg: viewHelper.configToolImagePath+configValue["radio_vol_h"]
        nav:["",mute_btn,"",""]
        onNoItemFound:{
            if(dir=="left"){
                if(radioScroll.visible){
                    radioScroll.forceActiveFocus()
                }else{
                    radioView.forceActiveFocus();
                }
            }
        }
        onEnteredReleased: {
            //            setIndicator("vol",true)
            //            isVolumeOpen=true
            widgetList.showHandsetBar(false)
            widgetList.showVolumeBar(true)
            //            volumeSlider.forceActiveFocus()        }
        }
    }
//    VolumeSlider{
//        id:volumeSlider
//        anchors.bottom: vol_btn.bottom
//        anchors.bottomMargin: qsTranslate('','video_volube_bar_bm')
//        anchors.left: vol_btn.left
//        source: viewHelper.configToolImagePath+configTool.config.global.video.volume_popup
//        volSliderComp:[qsTranslate('','video_volube_bar_h'),qsTranslate('','video_volube_bar_w')]
//        volConfigTag:configVideoTag;
//        visible: isVolumeOpen;
//        forVod: true;
//        onSliderEnds:{
//            if(dir=="left"){
//                if(radioScroll.visible){
//                    radioScroll.forceActiveFocus()
//                }else{
//                    radioView.forceActiveFocus();
//                }
//            }else {

//            }
//        }
//        onPadReleased:{
//        }
//        onVisibleChanged: if(visible)volumeSliderComp.setInitailValue=pif.getVolumeLevel()
//        onActiveFocusChanged: if(!activeFocus){isVolumeOpen=false}
//    }
}
