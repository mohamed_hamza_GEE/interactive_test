import QtQuick 1.1
import "../components"

FocusScope{
    id:volumessliderFS
    width:volbg.width;
    height:volbg.height;
    property variant volConfigTag;
    property alias source:volbg.source;
    property alias volumeSliderComp:volumeSliderComp
    property variant volSliderComp:[]
    property bool isBriSlider: false
    property bool forVod:false;
    signal sliderEnds(string dir)
    signal padReleased()
    signal dragActive()

    Image {
        id:volbg
    }
    SimpleNavButton{
        id:volumeMinus
        anchors.bottom: volumessliderFS.bottom
        isHighlight: activeFocus?true:false
        normImg:  viewHelper.configToolImagePath+(isBriSlider?volConfigTag.bright_dcr_n:volConfigTag.vol_dcr_n)
        highlightimg:  viewHelper.configToolImagePath+(isBriSlider?volConfigTag.bright_dcr_h:volConfigTag.vol_dcr_h)
        pressedImg: viewHelper.configToolImagePath+(isBriSlider?volConfigTag.bright_dcr_p:volConfigTag.vol_dcr_p)
        nav:["",volumePlus,"",""]
        onEnteredReleased: {padReleased("enter");volumeSliderComp.prevPage()}
        onNoItemFound: {
            sliderEnds(dir)
        }
        onPadReleased:{
            volumessliderFS.padReleased(dir)
        }
        focus:true;       
    }
    Slider{
        id:volumeSliderComp
        anchors.centerIn: parent
        height: volSliderComp[0];
        width:  volSliderComp[1];
        orientation: Qt.Vertical
        isReverseSliding:true
        sliderbg:volConfigTag[(forVod)?'popup_base_image':'base']
        sliderFillingtItems: [volConfigTag[(forVod)?'popup_fill_color':'fill_color'],volConfigTag.indicator]
        stepsValue:isBriSlider?pif.getBrightnessStep(): pif.getVolumeStep()
        isFill: true
        maxLevel:100
        onValueChanged: {
            var volumeLevel=isBriSlider?pif.getBrightnessLevel():pif.getVolumeLevel()
            var remainder=(volumeLevel%stepsValue)
            if(value=="drag") {
                if(isBriSlider) pif.setBrightnessByValue(shiftedValue)
                else pif.setVolumeByValue(shiftedValue)
                return
            }
            if(remainder!=0){
                var setValue =remainder>(stepsValue/2)?(volumeLevel - remainder)+stepsValue:volumeLevel-(remainder)
                if(isBriSlider) pif.setBrightnessByValue(setValue)
                else pif.setVolumeByValue(setValue)
                return;
            }
            if(shiftedValue>volumeLevel){
                if(isBriSlider)  pif.setBrightnessByStep(1)
                else pif.setVolumeByStep(1)
            }
            else if(shiftedValue<volumeLevel){
                if(isBriSlider)  pif.setBrightnessByStep(-1)
                else pif.setVolumeByStep(-1)
            }
        }
        onDragingOn: dragActive()
    }
    SimpleNavButton{
        id:volumePlus
        anchors.top: volumessliderFS.top
        isHighlight: activeFocus?true:false
        normImg:  viewHelper.configToolImagePath+(isBriSlider?volConfigTag.bright_incr_n:volConfigTag.vol_incr_n)
        highlightimg:  viewHelper.configToolImagePath+(isBriSlider?volConfigTag.bright_incr_h:volConfigTag.vol_incr_h)
        pressedImg: viewHelper.configToolImagePath+(isBriSlider?volConfigTag.bright_incr_p:volConfigTag.vol_incr_p)
        nav:["","","",volumeMinus]
        onEnteredReleased: {padReleased("enter");volumeSliderComp.nextPage()}
        onNoItemFound: {
            sliderEnds(dir);
        }
        onPadReleased:{
            volumessliderFS.padReleased(dir)
        }
    }

    Component.onCompleted: {
        volumeSliderComp.setInitailValue=isBriSlider?pif.getBrightnessLevel():pif.getVolumeLevel()

    }
}

