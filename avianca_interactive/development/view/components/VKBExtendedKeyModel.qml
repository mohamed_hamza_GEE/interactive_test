import QtQuick 1.0

Item  {
    property variant model:
    {
        26:[
                [
                    {character: "e", modifier: 0, key_code: 26},
                    {character: "\u00E8", modifier: "UNI", key_code: 0x00E8},
                    {character: "\u00E9", modifier: "UNI", key_code: 0x00E9},
                    {character: "\u00EA", modifier: "UNI", key_code: 0x00EA},
                    {character: "\u00EB", modifier: "UNI", key_code: 0x00EB},
                ],
                [
                        {character: "E", modifier: 1, key_code: 26},
                        {character: "\u00C8", modifier: "UNI", key_code: 0x00C8},
                        {character: "\u00C9", modifier: "UNI", key_code: 0x00C9},
                        {character: "\u00CA", modifier: "UNI", key_code: 0x00CA},
                        {character: "\u00CB", modifier: "UNI", key_code: 0x00CB},
                ]
            ],
        29:[
                [
                        {character: "y", modifier: 0, key_code: 29},
                        {character: "\u00FD", modifier: "UNI", key_code: 0x00FD},
                        {character: "\u00FF", modifier: "UNI", key_code: 0x00FF},
                ],
                [
                        {character: "Y", modifier: 1, key_code: 29},
                        {character: "\u00DD", modifier: "UNI", key_code: 0x00DD},
                ],
            ],
        30:[
                [
                    {character: "u", modifier: 0, key_code: 30},
                    {character: "\u00F9", modifier: "UNI", key_code: 0x00F9},
                    {character: "\u00FA", modifier: "UNI", key_code: 0x00FA},
                    {character: "\u00FB", modifier: "UNI", key_code: 0x00FB},
                    {character: "\u00FC", modifier: "UNI", key_code: 0x00FC},
                ],
                [
                    {character: "U", modifier: 1, key_code: 30},
                    {character: "\u00D9", modifier: "UNI", key_code: 0x00D9},
                    {character: "\u00DA", modifier: "UNI", key_code: 0x00DA},
                    {character: "\u00DB", modifier: "UNI", key_code: 0x00DB},
                    {character: "\u00DC", modifier: "UNI", key_code: 0x00DC},
                ],
            ],
        31:[
                [
                    {character: "i", modifier: 0, key_code: 31},
                    {character: "\u00EC", modifier: "UNI", key_code: 0x00EC},
                    {character: "\u00ED", modifier: "UNI", key_code: 0x00ED},
                    {character: "\u00EE", modifier: "UNI", key_code: 0x00EE},
                    {character: "\u00EF", modifier: "UNI", key_code: 0x00EF},
                ],
                [
                    {character: "I", modifier: 1, key_code: 31},
                    {character: "\u00CC", modifier: "UNI", key_code: 0x00CC},
                    {character: "\u00CD", modifier: "UNI", key_code: 0x00CD},
                    {character: "\u00CE", modifier: "UNI", key_code: 0x00CE},
                    {character: "\u00CF", modifier: "UNI", key_code: 0x00CF},
                ],
            ],
        32:[
                [
                    {character: "o", modifier: 0, key_code: 32},
                    {character: "\u00F2", modifier: "UNI", key_code: 0x00F2},
                    {character: "\u00F3", modifier: "UNI", key_code: 0x00F3},
                    {character: "\u00F4", modifier: "UNI", key_code: 0x00F4},
                    {character: "\u00F5", modifier: "UNI", key_code: 0x00F5},
                    {character: "\u00F6", modifier: "UNI", key_code: 0x00F6},
                ],
                [
                    {character: "O", modifier: 1, key_code: 32},
                    {character: "\u00D2", modifier: "UNI", key_code: 0x00D2},
                    {character: "\u00D3", modifier: "UNI", key_code: 0x00D3},
                    {character: "\u00D4", modifier: "UNI", key_code: 0x00D4},
                    {character: "\u00D5", modifier: "UNI", key_code: 0x00D5},
                    {character: "\u00D6", modifier: "UNI", key_code: 0x00D6},
                ],
            ],
        38:[
                [
                    {character: "a", modifier: 0, key_code: 38},
                    {character: "\u00E0", modifier: "UNI", key_code: 0x00E0},
                    {character: "\u00E1", modifier: "UNI", key_code: 0x00E1},
                    {character: "\u00E2", modifier: "UNI", key_code: 0x00E2},
                    {character: "\u00E3", modifier: "UNI", key_code: 0x00E3},
                    {character: "\u00E4", modifier: "UNI", key_code: 0x00E4},
                    {character: "\u00E5", modifier: "UNI", key_code: 0x00E5},
                    {character: "\u00E6", modifier: "UNI", key_code: 0x00E6},
                ],
                [
                    {character: "A", modifier: 1, key_code: 38},
                    {character: "\u00C0", modifier: "UNI", key_code: 0x00C0},
                    {character: "\u00C1", modifier: "UNI", key_code: 0x00C1},
                    {character: "\u00C2", modifier: "UNI", key_code: 0x00C2},
                    {character: "\u00C3", modifier: "UNI", key_code: 0x00C3},
                    {character: "\u00C4", modifier: "UNI", key_code: 0x00C4},
                    {character: "\u00C5", modifier: "UNI", key_code: 0x00C5},
                    {character: "\u00C6", modifier: "UNI", key_code: 0x00C6},
                ]
            ],
        39:[
                [
                    {character: "s", modifier: 0, key_code: 39},
                    {character: "\u015B", modifier: "UNI", key_code: 0x015B},
                    {character: "\u015D", modifier: "UNI", key_code: 0x015D},
                    {character: "\u015F", modifier: "UNI", key_code: 0x015F},
                    {character: "\u0161", modifier: "UNI", key_code: 0x0161},
                ],
                [
                    {character: "S", modifier: 1, key_code: 39},
                    {character: "\u015A", modifier: "UNI", key_code: 0x015A},
                    {character: "\u015C", modifier: "UNI", key_code: 0x015C},
                    {character: "\u015E", modifier: "UNI", key_code: 0x015E},
                    {character: "\u0160", modifier: "UNI", key_code: 0x0160},
                ],
            ],
                54:[
                    [
                        {character: "c", modifier: 0, key_code: 54},
                        {character: "ç", modifier: "UNI", key_code: 'ç'},
                        {character: "ć", modifier: "UNI", key_code: 'ć'},
                        {character: "č", modifier: "UNI", key_code: 'č'},
                    ],
                    [
                        {character: "C", modifier: 1, key_code: 54},
                        {character: "Ç", modifier: "UNI", key_code: 'Ç'},
                        {character: "Ć", modifier: "UNI", key_code: 'Ć'},
                        {character: "Č", modifier: "UNI", key_code: 'Č'},
                    ],
                ],
                46:[
                    [
                        {character: "l", modifier: 0, key_code: 46},
                        {character: "ł", modifier: "UNI", key_code: 'ł'},
                    ],
                    [
                        {character: "L", modifier: 1, key_code: 46},
                        {character: "Ł", modifier: "UNI", key_code: 'Ł'},
                    ],
                ],
                57:[
                    [
                        {character: "n", modifier: 0, key_code: 57},
                        {character: "ñ", modifier: "UNI", key_code: 'ñ'},
                        {character: "ń", modifier: "UNI", key_code: 'ń'},
                    ],
                    [
                        {character: "N", modifier: 1, key_code: 57},
                        {character: "Ñ", modifier: "UNI", key_code: 'Ñ'},
                        {character: "Ń", modifier: "UNI", key_code: 'Ń'},
                    ],
                ],
                52:[
                    [
                        {character: "z", modifier: 0, key_code: 52},
                        {character: "ž", modifier: "UNI", key_code: 'ž'},
                        {character: "ź", modifier: "UNI", key_code: 'ź'},
                        {character: "ż", modifier: "UNI", key_code: 'ż'},
                    ],
                    [
                        {character: "Z", modifier: 1, key_code: 52},
                        {character: "Ž", modifier: "UNI", key_code: 'Ž'},
                        {character: "Ź", modifier: "UNI", key_code: 'Ź'},
                        {character: "Ż", modifier: "UNI", key_code: 'Ż'},
                    ],
                ],
                59:[
                    [
                        {character: ";", modifier: "UNI", key_code: 0x015B},
                        {character: ":", modifier: "UNI", key_code: 0x015B},
                        {character: ":)", modifier: "UNI", key_code: 0x015B},
                        {character: ":(", modifier: "UNI", key_code: 0x015D},
                        {character: ":D", modifier: "UNI", key_code: 0x015F},
                        {character: ":|", modifier: "UNI", key_code: 0x0161},
                    ],
                    [
                        {character: ";", modifier: "UNI", key_code: 0x015B},
                        {character: ":", modifier: "UNI", key_code: 0x015B},
                        {character: ":)", modifier: "UNI", key_code: 0x015B},
                        {character: ":(", modifier: "UNI", key_code: 0x015D},
                        {character: ":D", modifier: "UNI", key_code: 0x015F},
                        {character: ":|", modifier: "UNI", key_code: 0x0161},
                    ],
                ],
                60:[
                    [
                        {character: ".", modifier: "UNI", key_code: 0x015B},
                        {character: ",", modifier: "UNI", key_code: 0x015B},

                    ],
                    [
                        {character: ".", modifier: "UNI", key_code: 0x015B},
                        {character: ",", modifier: "UNI", key_code: 0x015B},
                    ],
                ],

    }
}
