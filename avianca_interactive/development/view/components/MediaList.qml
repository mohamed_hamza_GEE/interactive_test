import QtQuick 1.1
FocusScope{
    anchors.fill: parent
    property alias compVpathListing: compVpathListing
    property int gap:parseInt(qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_hgap'),10)
    property variant upFocus:(subCatobj)?subCatobj.compVpathSubCategory:widgetList.getMainMenuRef();
    /**************************************************************************************************************************/
    function getMediaInfo(tag){
        return compVpathListing.model.getValue(compVpathListing.synopIndex,tag);
    }
    /**************************************************************************************************************************/
    Timer{
        id:lock
        interval:300//compVpathListing.highlightMoveDuration;

    }
    PathView{
        id:compVpathListing
        property int synopIndex:-1;
        function setModel(dModel){
            compVpathListing.model=dModel;
        }
        anchors.top:parent.top
        anchors.topMargin:(viewHelper.isKids(currentTemplateNode))?qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_kids_tm'):qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_tm')
        anchors.horizontalCenter:  parent.horizontalCenter
        anchors.horizontalCenterOffset:(count==2 && currentIndex==1 && isSynopsis)?-parent.width/2 :0
        width:isSynopsis?((count==2)?core.width:1.25*core.width):((count==-1 || count==-1)?((listingsingleimgwidth * (count-1))+(gap *  (count-1))):(count>=7?((listingsingleimgwidth * 8)+gap):(count==2?((listingsingleimgwidth * 2 )+gap):((listingsingleimgwidth * count)+(gap)))))//isSynopsis?((count==2)?core.width:1.25*core.width):((compVpathListing.count>=6)?(core.width+ (2 * qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_hgap'))):((listingsingleimgwidth * compVpathListing.count)+parseInt(qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_hgap'),10)))
        height:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_h')
        visible:(count>0)
        interactive:(isSynopsis)?false:((count==2 || count==1)?false:true)
        clip:count<7?(activeFocus?false:(count==4 || count==6?false:true)):true
        focus:true
        highlightMoveDuration:synopsisAnim.running? 1:200//synopsisAnim.running?1:200
        preferredHighlightBegin:count==2? (isSynopsis?0.5:0):0.5
        preferredHighlightEnd: count==2?(isSynopsis?0.5:0):0.5
        highlightRangeMode: count==2?(isSynopsis?PathView.StrictlyEnforceRange:PathView.NoHighlightRange):PathView.StrictlyEnforceRange
        pathItemCount:isSynopsis?3:(count>=7?8:((count==-1 || count==-1)?count-1:count))//isSynopsis?(count<=3?count:3):(count>=7?7:count)
        //    delegate:iconText
        path:  Path{
            id:hmenuPath
            startX:(compVpathListing.count==2?listingsingleimgwidth*0.6:0);startY:compVpathListing.height/2
            PathLine{
                x:compVpathListing.width+hmenuPath.startX;y:compVpathListing.height/2
            }
        }
        onMovementEnded: {
            if(isSynopsis)return;
            viewHelper.setMediaListingIndex(currentIndex)
        }
        Keys.onRightPressed: {
            if(lock.running)return;
            if(synopsisAnim.running)return;
            lock.restart()
            incrementCurrentIndex()
            compVpathListing.forceActiveFocus()
        }
        Keys.onLeftPressed: {
            if(lock.running)return;
            if(synopsisAnim.running)return;
            lock.restart()
            decrementCurrentIndex()
            compVpathListing.forceActiveFocus()
        }
        Keys.onUpPressed: {
            // if(synopsisAnim.running)return;
            upFocus.forceActiveFocus();

        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                if(lock.running)return;

                select(currentIndex);


            }
        }
        function select(index){
            if(synopsisAnim.running)return;
            if(flicking || moving || lock.running)return;
            viewHelper.blockKeysForSeconds(1)

            selIndex=index;
            currentIndex=index;
            if(compVpathListing.model.getValue(index,"category_attr_template_id")=="airline_exp"){
                viewHelper.isFromExperience=true;
                viewHelper.expCid=compVpathListing.model.getValue(index,"cid")
                dataController.getNextMenu([compVpathListing.model.getValue(index,"cid"),compVpathListing.model.getValue(index,"category_attr_template_id")],viewHelper.initializeExpVod)
                //viewHelper.initializeVOD("restart",3,"",false,false);
                return
            }
            synopsisAnim.restart();

        }

        onCurrentIndexChanged: {
            if(getCidTidForListing()[1]=="tvseries")
                getTvSeries()
        }
    }
    Image{
        id:leftTransparency
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        source: viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode,true)].listing.dark_patch
    }
    Image{
        id:rightTransparency
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        mirror: true
        source: viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode,true)].listing.dark_patch
    }
    MouseArea{
        anchors.fill:parent;
        enabled:(synopsisAnim.running || lock.running)
        onClicked:{}

    }
}
