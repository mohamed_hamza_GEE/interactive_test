import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    id: appOurFleet;
    width: core.width; height: core.height;

    property string currAppImages: microAppDir+currAppName+"/images/"
    property string xmlModelSource: microAppDir+currAppName+"/"+currAppName+"_data.xml"
    property int selectedIndex: -1;
    property string tempTxt:"";

    function init() {
        core.log("AppOurFleet.qml | init");
        fleetListData.source = xmlModelSource;
        fleetInfoData.source = xmlModelSource;
        selectedIndex = -1;
    }
    function reload(){
        core.log("AppOurFleet | reload ")
        fleetListData.source = xmlModelSource;
        fleetInfoData.source = xmlModelSource;
        fleetInfoData.reload();
        fleetListData.reload();
    }
    function clearOnExit() {}

    function setTitleText(obj,Dmodel,ind){
        obj.text = (Dmodel.get(ind).airbus_list_text_cdata !== "") ? Dmodel.get(ind).airbus_list_text_cdata : Dmodel.get(ind).airbus_list_text;
        obj.color = configTool.config.aviancaapp.screen.text_header_title;
        obj.font.pixelSize = qsTranslate('','aviapp_header_title_fontsize')
    }
    function setTitleInfoText(obj,Dmodel,ind){
        obj.text = (Dmodel.get(ind).menuTitle_cdata !== "") ? Dmodel.get(ind).menuTitle_cdata : Dmodel.get(ind).menuTitle;
        obj.color = configTool.config.aviancaapp.screen.text_header_menu;
        obj.font.pixelSize = qsTranslate('','aviapp_header_menu_fontsize');
    }
    function setInfoText(obj,Dmodel,ind,modelName){
        var modelName_cdata = modelName+"_cdata";
        obj.text = (eval("Dmodel.get("+ind+")."+modelName_cdata) !== "") ? eval("Dmodel.get("+ind+")."+modelName_cdata) : eval("Dmodel.get("+ind+")."+modelName);
        obj.color = configTool.config.aviancaapp.screen.text_fleetinfo_text;
        obj.font.pixelSize = qsTranslate('','aviapp_fleetinfo_text_fontsize');
    }
    function setText(obj,Dmodel,ind,modelName){
        var modelName_cdata = modelName+"_cdata";
        obj.text = (eval("Dmodel.get("+ind+")."+modelName_cdata) !== "") ? eval("Dmodel.get("+ind+")."+modelName_cdata) : eval("Dmodel.get("+ind+")."+modelName);
        obj.color = configTool.config.aviancaapp.screen.text_fleet_icons;
        obj.font.pixelSize = qsTranslate('','aviapp_fleet_icons_fontsize')
    }
    function setIcon(obj,Dmodel,ind,modelName){
        obj.source = currAppImages + (eval("Dmodel.get("+ind+")."+modelName)) ;
    }
    function setData(ind){
        setTitleText(headerTitle,fleetListData,ind);
        setTitleInfoText(headerMenuTitle,fleetInfoData,ind);
        setInfoText(fleetInfoText,fleetInfoData,ind,"airbus_type_text");
        setInfoText(fleetInfoText2,fleetInfoData,ind,"airbusSeatType_Bus_text");
        setInfoText(fleetInfoText3,fleetInfoData,ind,"airbusSeatType_Eco_text");

        setIcon(aircraft_icon,fleetInfoData,ind,"aircraft_icon");
        setIcon(businessClass_icon,fleetInfoData,ind,"bc_seat_icon");
        setIcon(economyClass_icon,fleetInfoData,ind,"ec_seat_icon");

        setIcon(restRoomIcon,fleetInfoData,ind,"restroom_icon");
        setIcon(entryExitIcon,fleetInfoData,ind,"entry_exit_icon");
        setIcon(emergencyIcon,fleetInfoData,ind,"emergency_exit_icon");
        setIcon(economyIcon,fleetInfoData,ind,"economy_seat_icon");
        setIcon(businessIcon,fleetInfoData,ind,"business_seat_icon");

        setText(restRoomText,fleetInfoData,ind,"restroom_text");
        setText(entryExitText,fleetInfoData,ind,"entry_exit_text");
        setText(emergencyText,fleetInfoData,ind,"emergency_exit_text");
        setText(economyText,fleetInfoData,ind,"economy_seat_text");
        setText(businessText,fleetInfoData,ind,"business_seat_text");
    }
    function languageUpdate(){
        fleetInfoData.reload()
        fleetListData.reload()
    }
    XmlListModel {
        id: fleetInfoData;
        query: "/application/screen";

        XmlRole { name:"menuTitle"; query: "text2/"+langCode+"/@_value/string()" }
        XmlRole { name:"menuTitle_cdata"; query: "text2/"+langCode+"/string()" }
        XmlRole { name : "airbus_type_text"; query : "text5/"+langCode+"/@_value/string()" }
        XmlRole { name : "airbus_type_text_cdata"; query : "text5/"+langCode+"/string()" }
        XmlRole { name : "airbusSeatType_Bus_text"; query : "text7/"+langCode+"/@_value/string()" }
        XmlRole { name : "airbusSeatType_Bus_text_cdata"; query : "text7/"+langCode+"/@_value/string()" }
        XmlRole { name : "airbusSeatType_Eco_text"; query : "text9/"+langCode+"/@_value/string()" }
        XmlRole { name : "airbusSeatType_Eco_text_cdata"; query : "text9/"+langCode+"/@_value/string()" }
        XmlRole { name : "restroom_text"; query: "text10/"+langCode+"/@_value/string()" }
        XmlRole { name : "restroom_text_cdata"; query: "text10/"+langCode+"/string()" }
        XmlRole { name : "entry_exit_text"; query: "text11/"+langCode+"/@_value/string()" }
        XmlRole { name : "entry_exit_text_cdata"; query: "text11/"+langCode+"/string()" }
        XmlRole { name : "emergency_exit_text"; query: "text12/"+langCode+"/@_value/string()" }
        XmlRole { name : "emergency_exit_text_cdata"; query: "text12/"+langCode+"/string()" }
        XmlRole { name : "economy_seat_text"; query: "text13/"+langCode+"/@_value/string()" }
        XmlRole { name : "economy_seat_text_cdata"; query: "text13/"+langCode+"/string()" }
        XmlRole { name : "business_seat_text"; query: "text14/"+langCode+"/@_value/string()" }
        XmlRole { name : "business_seat_text_cdata"; query: "text14/"+langCode+"/string()" }

        XmlRole { name:"restroom_icon"; query: "image3/@_value/string()" }
        XmlRole { name:"entry_exit_icon"; query: "image4/@_value/string()" }
        XmlRole { name:"emergency_exit_icon"; query: "image5/@_value/string()" }
        XmlRole { name:"economy_seat_icon"; query: "image6/@_value/string()" }
        XmlRole { name:"business_seat_icon"; query: "image7/@_value/string()" }

        XmlRole { name:"aircraft_icon"; query: "image8/@_value/string()" }
        XmlRole { name:"bc_seat_icon"; query: "image9/@_value/string()" }
        XmlRole { name:"ec_seat_icon"; query: "image10/@_value/string()" }

        onCountChanged: {
            setTitleInfoText(headerMenuTitle,fleetInfoData,0);

            setInfoText(fleetInfoText,fleetInfoData,0,"airbus_type_text");
            setInfoText(fleetInfoText2,fleetInfoData,0,"airbusSeatType_Bus_text");
            setInfoText(fleetInfoText3,fleetInfoData,0,"airbusSeatType_Eco_text");

            setIcon(aircraft_icon,fleetInfoData,0,"aircraft_icon");
            setIcon(businessClass_icon,fleetInfoData,0,"bc_seat_icon");
            setIcon(economyClass_icon,fleetInfoData,0,"ec_seat_icon");

            setIcon(restRoomIcon,fleetInfoData,0,"restroom_icon");
            setIcon(entryExitIcon,fleetInfoData,0,"entry_exit_icon");
            setIcon(emergencyIcon,fleetInfoData,0,"emergency_exit_icon");
            setIcon(economyIcon,fleetInfoData,0,"economy_seat_icon");
            setIcon(businessIcon,fleetInfoData,0,"business_seat_icon");

            setText(restRoomText,fleetInfoData,0,"restroom_text");
            setText(entryExitText,fleetInfoData,0,"entry_exit_text");
            setText(emergencyText,fleetInfoData,0,"emergency_exit_text");
            setText(economyText,fleetInfoData,0,"economy_seat_text");
            setText(businessText,fleetInfoData,0,"business_seat_text");
        }

    }

    XmlListModel {
        id: fleetListData;
        query: "/application/screen/list1/listItem";

        XmlRole { name : "airbus_list_text"; query : "text4/"+langCode+"/@_value/string()" }
        XmlRole { name : "airbus_list_text_cdata"; query : "text4/"+langCode+"/string()" }
        XmlRole { name : "airbus_number_text"; query : "text3/"+langCode+"/@_value/string()" }
        XmlRole { name : "airbus_number_text_cdata"; query : "text3/"+langCode+"/string()" }
        XmlRole { name : "numberOf_Bus_Seat_text"; query : "text6/"+langCode+"/@_value/string()" }
        XmlRole { name : "numberOf_Bus_Seat_text_cdata"; query : "text6/"+langCode+"/string()" }
        XmlRole { name : "numberOf_Eco_Seat_text"; query : "text8/"+langCode+"/@_value/string()" }
        XmlRole { name : "numberOf_Eco_Seat_text_cdata"; query : "text8/"+langCode+"/string()" }
        XmlRole { name : "airbus_image"; query : "image1/@_value/string()" }
        XmlRole { name : "airbus_inside_image"; query : "image2/@_value/string()" }

        onCountChanged: {
            childAppList.model = fleetListData;
            setTitleText(headerTitle,fleetListData,0);
        }
    }
    Image {
        id: backgroundImage;
        anchors.fill: parent;
        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.app_bg;
    }
    Image {
        id: headerImage;
        anchors.top: parent.top;
        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.app_header;
    }
    Image {
        id: sideMenuPanelImage;
        anchors.top:headerImage.bottom; anchors.right: parent.right;
        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_panel;
    }
    SimpleButton {
        id: closeBtn;
        increaseTouchArea: true
        increaseValue:-60
        anchors.right: headerImage.right; anchors.top:headerImage.top;
        anchors.rightMargin: qsTranslate('','aviapp_header_close_gap');
        anchors.topMargin: qsTranslate('','aviapp_header_close_gap');
        isHighlight: activeFocus;
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_p;
        onEnteredReleased: {
            isChildAppSelected = true;
            viewController.getActiveScreenRef().actionOnExit(true);

            //viewController.showScreenPopup(23);
        }
        Keys.onDownPressed: {
            if(upArrowContainer.visible){
                upArrowImage.forceActiveFocus();
            }else{
                childAppList.forceActiveFocus();
            }
        }
    }
    ViewText {
        id: headerTitle;
        width: qsTranslate('','aviapp_header_title_w');
        x: qsTranslate('','aviapp_header_title_x');
        anchors.verticalCenter: headerImage.verticalCenter;
        wrapMode: Text.Wrap;
        textFormat: Text.StyledText;
        maximumLineCount:1;
        clip: true;
        //        elide: Text.ElideRight;
    }

    ViewText {
        id: headerMenuTitle;
        width: qsTranslate('','aviapp_header_menu_w');
        anchors.top:headerImage.top; anchors.topMargin: qsTranslate('','aviapp_header_menu_y');
        anchors.right: headerImage.right; anchors.rightMargin: qsTranslate('','aviapp_header_menu_rm');
        wrapMode: Text.Wrap;
        textFormat: Text.StyledText;
        //        elide: Text.ElideRight;
        maximumLineCount:1;
        clip: true;
        MouseArea{
            anchors.fill: parent
            onClicked: {
                isChildAppSelected = true;
                viewController.getActiveScreenRef().actionOnExit(true);
            }
        }
    }
    Image {
        id: airbusImage;
        anchors.left:parent.left;
        anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_img_lm');
        anchors.top:headerImage.bottom;
        anchors.topMargin: qsTranslate('','aviapp_fleetinfo_img_tm');
        source: currAppImages + fleetListData.get(selectedIndex).airbus_image;
    }
    Row{
        id: fleetInfo;
        spacing: qsTranslate('','aviapp_fleetinfo_gap');
        anchors.left: parent.left;
        anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_lm');
        anchors.top: airbusImage.top;
        anchors.topMargin: qsTranslate('','aviapp_fleetinfo_tm');

        Image {
            id: fleetInfoImage1;
            source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.fleet_info;
            Item {
                id: iconInfoContainer1;
                width: qsTranslate('','aviapp_fleetinfo_type_w');
                height: (aircraft_icon.paintedHeight <= numberOfAircraft.paintedHeight)? aircraft_icon.paintedHeight:numberOfAircraft.paintedHeight;
                anchors.left:parent.left;
                anchors.verticalCenter: parent.verticalCenter;
                clip: true;
                Row{
                    id: cont1Row;
                    anchors.centerIn: parent;
                    Image {
                        id: aircraft_icon;
                        anchors.left:parent.left;
                        anchors.verticalCenter: parent.verticalCenter;
                        //                        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.aircraft_icon;
                    }
                    ViewText {
                        id: numberOfAircraft;
                        anchors.left:aircraft_icon.right;
                        anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_type_gap');
                        anchors.verticalCenter: aircraft_icon.verticalCenter;
                        wrapMode: Text.Wrap;
                        textFormat: Text.StyledText;
                        elide: Text.ElideRight;
                        maximumLineCount:1;
                        varText: [
                            (fleetListData.get(selectedIndex).airbus_number_text_cdata != "") ? fleetListData.get(selectedIndex).airbus_number_text_cdata: fleetListData.get(selectedIndex).airbus_number_text,
                                                                                                configTool.config.aviancaapp.screen.text_fleetinfo_text,
                                                                                                qsTranslate('','aviapp_fleetinfo_type_fontsize')
                        ]
                    }
                }
            }
            ViewText{
                id:fleetInfoText;

                anchors.verticalCenter: iconInfoContainer1.verticalCenter;
                anchors.left:iconInfoContainer1.right;
                anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_text_lm');
                width: qsTranslate('','aviapp_fleetinfo_text_w');
                wrapMode: Text.Wrap;
                horizontalAlignment: Text.AlignHCenter;
                verticalAlignment: Text.AlignVCenter;
                textFormat: Text.StyledText;
                elide: Text.ElideRight;
                maximumLineCount:2;
            }
        }
        Image {
            id: fleetInfoImage2;
            source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.fleet_info;
            Item {
                id: iconInfoContainer2;
                width: qsTranslate('','aviapp_fleetinfo_type_w');
                height: (businessClass_icon.paintedHeight <= businessClassSeat.paintedHeight)? businessClass_icon.paintedHeight:businessClassSeat.paintedHeight;
                anchors.left:parent.left;
                anchors.verticalCenter: parent.verticalCenter;
                clip: true

                Row{
                    id: cont2Row;
                    anchors.centerIn: parent;

                    Image {
                        id: businessClass_icon;
                        anchors.left:parent.left;
                        anchors.verticalCenter: parent.verticalCenter;
                        //                        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.business_class_icon;
                    }
                    ViewText {
                        id: businessClassSeat;
                        anchors.left:businessClass_icon.right;
                        anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_type_gap');
                        anchors.verticalCenter: businessClass_icon.verticalCenter;
                        wrapMode: Text.Wrap;
                        textFormat: Text.StyledText;
                        elide: Text.ElideRight;
                        maximumLineCount:1;
                        varText: [
                            (fleetListData.get(selectedIndex).numberOf_Bus_Seat_text_cdata != "")?fleetListData.get(selectedIndex).numberOf_Bus_Seat_text_cdata : fleetListData.get(selectedIndex).numberOf_Bus_Seat_text,
                                                                                                   configTool.config.aviancaapp.screen.text_fleetinfo_text,
                                                                                                   qsTranslate('','aviapp_fleetinfo_type_fontsize')
                        ]
                    }
                }
            }
            ViewText{
                id:fleetInfoText2;

                anchors.verticalCenter: iconInfoContainer2.verticalCenter;
                anchors.left:iconInfoContainer2.right;
                anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_text_lm');
                width: qsTranslate('','aviapp_fleetinfo_text_w');
                textFormat: Text.StyledText;
                wrapMode: Text.Wrap;
                horizontalAlignment: Text.AlignHCenter;
                verticalAlignment: Text.AlignVCenter;
                elide: Text.ElideRight;
                maximumLineCount:2;
            }
        }

        Image {
            id: fleetInfoImage3;
            source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.fleet_info;

            Item {
                id: iconInfoContainer3;
                width: qsTranslate('','aviapp_fleetinfo_type_w');
                height: (economyClass_icon.paintedHeight <= economyClassSeat.paintedHeight)? economyClass_icon.paintedHeight:economyClassSeat.paintedHeight;
                anchors.left:parent.left;
                anchors.verticalCenter: parent.verticalCenter;
                clip: true;

                Row{
                    id: cont3Row;
                    anchors.centerIn: parent;
                    Image {
                        id: economyClass_icon;
                        anchors.left:iconInfoContainer3.left;
                        anchors.leftMargin : qsTranslate('','aviapp_fleetinfo_img_lm');
                        anchors.verticalCenter: iconInfoContainer3.verticalCenter;
                        //                        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.economy_class;

                    }
                    ViewText {
                        id: economyClassSeat;
                        anchors.left:economyClass_icon.right;
                        anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_type_gap');
                        anchors.verticalCenter: economyClass_icon.verticalCenter;
                        wrapMode: Text.Wrap;
                        textFormat: Text.StyledText;
                        elide: Text.ElideRight;
                        maximumLineCount:1;
                        varText: [
                            (fleetListData.get(selectedIndex).numberOf_Eco_Seat_text_cdata != "")?fleetListData.get(selectedIndex).numberOf_Eco_Seat_text_cdata : fleetListData.get(selectedIndex).numberOf_Eco_Seat_text,
                                                                                                   configTool.config.aviancaapp.screen.text_fleetinfo_text,
                                                                                                   qsTranslate('','aviapp_fleetinfo_type_fontsize')
                        ]
                    }
                }
            }

            ViewText{
                id:fleetInfoText3;
                anchors.verticalCenter: iconInfoContainer3.verticalCenter;
                anchors.left:iconInfoContainer3.right;
                anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_text_lm');
                width: qsTranslate('','aviapp_fleetinfo_text_w');
                wrapMode: Text.Wrap;
                textFormat: Text.StyledText;
                elide: Text.ElideRight;
                maximumLineCount:2;
                horizontalAlignment: Text.AlignHCenter;
                verticalAlignment: Text.AlignVCenter;
            }
        }
    }

    Image {
        id: airbusImageInside;
        anchors.left:parent.left;
        anchors.leftMargin: qsTranslate('','aviapp_fleetinfo_img_lm');
        anchors.top: airbusImage.bottom;
        anchors.topMargin: qsTranslate('','aviapp_fleetinfo_img_vgap');
        source: currAppImages + fleetListData.get(selectedIndex).airbus_inside_image;
    }

    Rectangle{
        id: iconInfoContainer;
        height: qsTranslate('','aviapp_fleet_icons_h');
        width: qsTranslate('','aviapp_fleet_icons_w');
        color: qsTranslate('','aviapp_fleet_icons_color');
        anchors.left: parent.left;
        anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_lm');
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: qsTranslate('','aviapp_fleet_icons_bm');
        anchors.top:airbusImageInside.bottom;
        Row{
            id:iconContainerRow;
            anchors.verticalCenter: parent.verticalCenter;
            spacing: qsTranslate('','aviapp_fleet_icons_gap');
            Item {
                id: icon1;
                anchors.verticalCenter: parent.verticalCenter;
                width: restRoomIcon.paintedWidth + restRoomText.paintedWidth;
                height:(restRoomIcon.paintedHeight >= restRoomText.paintedWidth) ? restRoomIcon.paintedHeight : restRoomText.paintedWidth;
                Image {
                    id: restRoomIcon;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_icon_lm');
                }
                ViewText {
                    id: restRoomText;
                    anchors.left: restRoomIcon.right;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_text_gap');
                    anchors.verticalCenter: restRoomIcon.verticalCenter;
                    width: qsTranslate('','aviapp_fleet_icons_text_w');
                    wrapMode: Text.Wrap;
                    textFormat: Text.StyledText;
                    elide: Text.ElideRight;
                    maximumLineCount:2;
                }
            }

            Item {
                id: icon2;
                width: entryExitIcon.paintedWidth + entryExitText.paintedWidth;
                anchors.verticalCenter: icon1.verticalCenter;
                height:(entryExitIcon.paintedHeight >= entryExitText.paintedWidth) ? entryExitIcon.paintedHeight : entryExitText.paintedWidth;
                Image {
                    id: entryExitIcon;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_icon_lm');
                }
                ViewText {
                    id: entryExitText;
                    anchors.left: entryExitIcon.right;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_text_gap');
                    width: qsTranslate('','aviapp_fleet_icons_text_w');
                    anchors.verticalCenter: entryExitIcon.verticalCenter;
                    textFormat: Text.StyledText;
                    wrapMode: Text.Wrap;
                    elide: Text.ElideRight;
                    maximumLineCount:2;
                }

            }
            Item {
                id: icon3;
                anchors.verticalCenter: icon2.verticalCenter;
                width: emergencyIcon.paintedWidth + emergencyText.paintedWidth;
                height:(emergencyIcon.paintedHeight >= emergencyText.paintedWidth) ? emergencyIcon.paintedHeight : emergencyText.paintedWidth;
                Image {
                    id: emergencyIcon;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_icon_lm');
                }
                ViewText {
                    id: emergencyText;
                    anchors.left: emergencyIcon.right;
                    width: qsTranslate('','aviapp_fleet_icons_text_w');
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_text_gap');
                    anchors.verticalCenter: emergencyIcon.verticalCenter;
                    textFormat: Text.StyledText;
                    wrapMode: Text.Wrap;
                    elide: Text.ElideRight;
                    maximumLineCount:2;
                }
            }
            Item {
                id: icon4;
                anchors.verticalCenter: icon3.verticalCenter;
                width: economyIcon.paintedWidth + economyText.paintedWidth;
                height:(economyIcon.paintedHeight >= economyText.paintedWidth) ? economyIcon.paintedHeight : economyText.paintedWidth;
                Image {
                    id: economyIcon;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_icon_lm');
                }
                ViewText {
                    id: economyText;
                    anchors.left: economyIcon.right;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_text_gap');
                    width: qsTranslate('','aviapp_fleet_icons_text_w');
                    anchors.verticalCenter: economyIcon.verticalCenter;
                    wrapMode: Text.Wrap;
                    textFormat: Text.StyledText;
                    elide: Text.ElideRight;
                    maximumLineCount:2;
                }
            }
            Item {
                id: icon5;
                anchors.verticalCenter: icon4.verticalCenter;
                width: businessIcon.paintedWidth + businessText.paintedWidth;
                height:(businessIcon.paintedHeight >= businessText.paintedWidth) ? businessIcon.paintedHeight : businessText.paintedWidth;
                Image {
                    id: businessIcon;
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.left: parent.left;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_icon_lm');
                }
                ViewText {
                    id: businessText;
                    anchors.left: businessIcon.right;
                    anchors.leftMargin: qsTranslate('','aviapp_fleet_icons_text_gap');
                    width: qsTranslate('','aviapp_fleet_icons_text_w');
                    textFormat: Text.StyledText;
                    anchors.verticalCenter: businessIcon.verticalCenter;
                    wrapMode: Text.Wrap;
                    elide: Text.ElideRight;
                    maximumLineCount:2;
                }
            }
        }
    }
    function pageUp(){
        if(childAppList.contentY>0){
            if((childAppList.contentY-childAppList.height)>0){
                childAppList.contentY = childAppList.contentY-childAppList.height
            }else{
                childAppList.contentY = 0
            }
            var firstIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            childAppList.currentIndex = firstIndex;
            childAppList.positionViewAtIndex(firstIndex,ListView.Beginning)
        }
        childAppList.forceActiveFocus()
    }

    function pageDown(){
        if(childAppList.contentY<(childAppList.contentHeight-childAppList.height)){
            if((childAppList.contentY+childAppList.height)<(childAppList.contentHeight-childAppList.height)){
                childAppList.contentY = childAppList.contentY+childAppList.height
            }else{
                childAppList.contentY = childAppList.contentY+((childAppList.contentHeight-childAppList.height)-childAppList.contentY)
            }
            var lastIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            childAppList.currentIndex = lastIndex+1;
            childAppList.positionViewAtIndex(lastIndex,ListView.Beginning)
        }
        childAppList.forceActiveFocus()
    }

    ListView {
        id: childAppList;
        width: qsTranslate('','aviapp_sidemenu_itemw')
        height: qsTranslate('','aviapp_sidemenu_itemh')*6;
        anchors.top:headerImage.bottom;
        anchors.right: parent.right;
        delegate: infoListDelegate;
        boundsBehavior: ListView.StopAtBounds
        clip:true;
        interactive: count>6?true:false;
        onMovementEnded: {
            childAppList.currentIndex=childAppList.indexAt(childAppList.contentX,childAppList.contentY+parseInt(qsTranslate('','aviapp_sidemenu_itemh'),10)+10)
        }

        onCountChanged: {
            selectedIndex=0;
            childAppList.currentIndex = 0;
            childAppList.forceActiveFocus();
        }
        Keys.onUpPressed: {
            var firstIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            if(childAppList.currentIndex == 0 ){
                closeBtn.forceActiveFocus();
            }else if(upArrowContainer.visible && (childAppList.currentIndex == firstIndex+1)){
                upArrowImage.forceActiveFocus();
            }else{
                childAppList.decrementCurrentIndex();
            }
        }
        Keys.onDownPressed: {
            var lastIndex = childAppList.indexAt(childAppList.contentX+10,(childAppList.contentY+childAppList.height-10))
            if(closeBtn.activeFocus){
                childAppList.forceActiveFocus();
            }else if((downArrowContainer.visible && currentIndex == lastIndex) ){
                downArrowImage.forceActiveFocus();
            }else{
                childAppList.incrementCurrentIndex();
            }
        }
        Behavior on contentY {
            NumberAnimation{duration: 50}
        }
    }

    Component{
        id: infoListDelegate;
        Rectangle{
            id: screenlist;
            height: qsTranslate('','aviapp_sidemenu_itemh');
            width: qsTranslate('','aviapp_sidemenu_itemw');
            color: "transparent"
            opacity: ((Math.floor(childAppList.contentY/screenlist.height) == index) && childAppList.contentY > 0) ? 0 : 1;
            Image {
                id: listImage;
                anchors.fill: parent;
                source: selectedIndex===index?viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_s
                          :viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_n;
            }
            Rectangle{
                id: screenHighlightlist;
                height: qsTranslate('','aviapp_sidemenu_itemh');
                width: qsTranslate('','aviapp_sidemenu_itemw');
                color: "transparent"
                visible: (index==childAppList.currentIndex && childAppList.activeFocus);
                Image {
                    id: listHighlightImage;
                    anchors.fill: parent;
                    source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_h;
                }
            }
            ViewText {
                id:listText;
                width: qsTranslate('','aviapp_sidemenu_text_w');
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left; anchors.leftMargin: qsTranslate('','aviapp_sidemenu_text_lm');
                wrapMode: Text.Wrap;

                textFormat: Text.StyledText;
                maximumLineCount:2;
                elide: Text.ElideRight;
                varText: [
                    (fleetListData.get(index).airbus_list_text_cdata !== "")? fleetListData.get(index).airbus_list_text_cdata : fleetListData.get(index).airbus_list_text,
                                                                              selectedIndex===index?configTool.config.aviancaapp.screen.text_sidemenu_s:configTool.config.aviancaapp.screen.text_sidemenu_n,
                                                                                                     qsTranslate('','aviapp_sidemenu_fontsize')
                ]

            }

            MouseArea{
                id:infoListMouseArea;
                anchors.fill: parent;
                onPressed: {
                    childAppList.currentIndex=index;
                    childAppList.forceActiveFocus();
                }
                onReleased: {
                    selectedIndex=index;
                    childAppList.currentIndex=index;
                    setData(selectedIndex);
                }
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    selectedIndex=index;
                    childAppList.currentIndex=index;
                    setData(selectedIndex);
                }
            }
        }
    }
    Rectangle {
        id: upArrowContainer;
        anchors.top: childAppList.top;
        anchors.horizontalCenter: childAppList.horizontalCenter;
        height: qsTranslate('','aviapp_sidemenu_itemh');
        width: qsTranslate('','aviapp_sidemenu_itemw');
        color: "transparent"
        visible: (childAppList.contentY > 0) ? true : false;

        SimpleButton {
            id: upArrowImage;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','aviapp_sidemenu_arrow_tm');
            anchors.horizontalCenter: parent.horizontalCenter;
            isHighlight: activeFocus;
            normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_n;
            highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_h;
            pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_h;
        }
        onVisibleChanged: {
            if(!(upArrowContainer.visible)){
                childAppList.currentIndex =0;
                childAppList.forceActiveFocus();
            }
        }
        MouseArea{
            id:upArrowMouseArea;
            anchors.fill: parent;
            onReleased: {
                pageUp();
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                pageUp();
            }
        }
        Keys.onDownPressed: {childAppList.forceActiveFocus();}
        Keys.onUpPressed: {closeBtn.forceActiveFocus();}
    }

    Rectangle {
        id: downArrowContainer;
        anchors.top: childAppList.bottom;
        anchors.horizontalCenter: childAppList.horizontalCenter;
        height: qsTranslate('','aviapp_sidemenu_itemh');
        width: qsTranslate('','aviapp_sidemenu_itemw');
        visible:(childAppList.contentY+childAppList.height < childAppList.contentHeight);
        color: "transparent";

        onVisibleChanged: {
            if(!(downArrowContainer.visible)){
                childAppList.currentIndex = (childAppList.count -1);
                childAppList.forceActiveFocus();
            }
        }
        SimpleButton {
            id: downArrowImage;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','aviapp_sidemenu_arrow_tm');
            anchors.horizontalCenter: parent.horizontalCenter;
            isHighlight: activeFocus;
            normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_n;
            highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_h;
            pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_h;
        }
        MouseArea{
            id:downArrowMouseArea;
            anchors.fill: parent;
            onReleased:  { pageDown();}
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                pageDown();
            }
        }
        Keys.onUpPressed: {childAppList.forceActiveFocus();}
    }
}
