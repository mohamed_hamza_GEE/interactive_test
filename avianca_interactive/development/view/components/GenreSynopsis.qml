import QtQuick 1.0

FocusScope {
    id:genreSynopsis

    visible:false;
    property variant configValue:configTool.config.music.playlist;
    property variant configTag:configTool.config.music.tracklist;
    /**************************************************************************************************************************/
    Connections{
        target:albumLoader.item;
        onCustomClose:{
            albumClose();
        }
        onCustomLeft:{
            genreView.forceActiveFocus();
        }
    }
    /**************************************************************************************************************************/
    function init(){
        viewHelper.setHelpTemplate("genre");
        core.debug("Genresynopsis.qml | init ")
        var params=new Object();
        params['cid']           = getCidTidForSubCat()[0];
        params['template_id']   = getCidTidForSubCat()[1];
        params['inputSearch']   = ""
        params['searchType']    = "NA";
        params['fieldList']     = 'genre';
        params['searchBy']      = 'genre'
        params['orderBy']       = 'genre';
        params['groupBy']       = 'genre'
        dataController.keywordSearch.getData(params,genreListReady,1);

    }
    function genreListReady(dmodel){
        genreView.model=viewHelper.blankModel
        if(dmodel.count==0){
            viewController.showScreenPopup(7);
            closeLoader();
            return;
        }
        visible=true;
        genreView.currentIndex=0;
        genreView.model=dmodel;
        genreView.select();
    }
    function songListReady(dmodel){

        mediaList = genreSynopsis;
        albumLoader.visible=true
        albumLoader.item.setModel(dmodel);
        albumLoader.item.genreInit(panel.width);
        albumLoader.item.forceActiveFocus();



    }
    function getMediaInfo(tag){
        if(tag=="title")tag="genre";
        return genreView.model.getValue(genreView.selIndex,tag)
    }
    function clearOnExit(){
        genreView.selIndex=-1;
    }
    function backBtnPressed(){
        viewHelper.homeAction();
    }

    function langUpdate(){
        //This timer was added because subCategory taking time to update its nodel due to which further execution fails
        delayForLanguageUpdate.restart()
    }
    function assignLangUpdate(){
        init()
    }

    Timer{
        id:delayForLanguageUpdate
        interval: 250
        onTriggered: assignLangUpdate()
    }

    /**************************************************************************************************************************/
    Image{
        id:panel;
        source:viewHelper.configToolImagePath+configValue["genre_panel"];
        anchors.horizontalCenter:parent.horizontalCenter;
        y:qsTranslate('',"artist_art_panel_tm");

    }
    VerticalView{
        id:genreView;
        anchors.left:panel.left;
        anchors.top: panel.top;
        anchors.topMargin:qsTranslate('','artist_art_view_tm')
        anchors.leftMargin:qsTranslate('','artist_art_view_lm')
        width:qsTranslate('','artist_art_view_w');
        height:qsTranslate('','artist_art_view_h');
        property int selIndex:-1;
        Keys.onUpPressed:{
            if(currentItem.y==contentY){
                subCatobj.forceActiveFocus()
            }else{
                decrementCurrentIndex();
            }
        }

        Keys.onDownPressed:{
            incrementCurrentIndex();

        }

        Keys.onRightPressed: {
            if(genreScroll.visible){
                genreScroll.forceActiveFocus()
            }else{
                albumLoader.item.forceActiveFocus();
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                select();
            }
        }

        clip:true;


        delegate:Item{
            id:artistSection;
            width:qsTranslate('','artist_section_w');
            height:qsTranslate('','artist_section_h');
            Image{
                id:high
                source:viewHelper.configToolImagePath+configValue["playlist_list_highlight"];
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('','artist_tracks_high_bm')
                height:qsTranslate('','artist_tracks_high_h');
                width:parent.width;
                opacity:(genreView.currentIndex==index && genreView.activeFocus)?1:0

            }
            ViewText{
                x:qsTranslate('','artist_sectiontext_lm');
                width:qsTranslate('','artist_sectiontext_w');
                height:qsTranslate('','artist_sectiontext_h');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[genre,(genreView.selIndex==index)?core.configTool.config.music.tracklist["title_color"]:core.configTool.config.music.tracklist["description1_color"],qsTranslate('','artist_sectiontext_fontsize')]
                elide:Text.ElideRight;
            }
            Rectangle{
                height:qsTranslate('','common_line_h');
                width:parent.width;
                anchors.bottom:parent.bottom;
                color:core.configTool.config.music.tracklist.description1_color;
            }
            MouseArea{
                anchors.fill:high
                onClicked:{
                    genreView.currentIndex=index;
                    genreView.select();
                }
            }

        }

        function select(){

            if(selIndex==currentIndex)return;
            selIndex = currentIndex;
            var params=new Object();
            params['cid']           = getCidTidForSubCat()[0];
            params['template_id']   = getCidTidForSubCat()[1];
            params['inputSearch']   = genreView.model.getValue(genreView.currentIndex,"genre").toLowerCase();
            params['searchType']    = "full"  ;
            params['fieldList']     = 'title,artist,mid,rating,aggregate_parentmid,duration,media_type';
            params['searchBy']      = 'genre'
            params['orderBy']       = 'title';
            params['groupBy']       = 'title'
            params['isParentAggSearch'] = true
            dataController.keywordSearch.getData(params,songListReady,2);

        }
    }
    Scroller{
        id:genreScroll
        targetData:genreView;
        sliderbg:[qsTranslate('','tracklist_scrollcont_base_margin'),qsTranslate('','tracklist_scrollcont_w'),qsTranslate('','tracklist_scrollcont_base_h'),configTag["scroll_base"]]
        scrollerDot:configTag["scroll_button"]
        upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
        downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
        anchors.left:genreView.right;
        anchors.leftMargin:qsTranslate('','tracklist_scrollcont_lm')
        height: qsTranslate('','tracklist_scrollcont_h');
        width: qsTranslate('','tracklist_scrollcont_w');
        anchors.verticalCenter: genreView.verticalCenter;
        onSliderNavigation:{
            if(direction=="left"){
                genreView.forceActiveFocus();
            }else if(direction=="right"){
                albumLoader.item.forceActiveFocus();
            }
        }

    }
    Loader{
        id:albumLoader;
        sourceComponent:musicAlbumSynopsis;
        visible:false
        anchors.left:panel.left;
        width:panel.width;
    }
}
