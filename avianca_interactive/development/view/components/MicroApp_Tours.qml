import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    id: toursApp;
    height: core.height; width:core.width;

    property string selectedTemplate : "template1";
    property string selectedScreenId : "screen_1";
    property alias tourAppData : tourAppData;
    property alias selectionListModel : selectionListModel;
    property alias childLoader: childLoader;

    //directories for accessing application data and images
    property string currAppImages: Qt.resolvedUrl(microAppDir+"images/");
    //xmlmodelsource path should be appName_data.xml
    property string dataxml: currAppDataName;
    property string xmlModelSource: microAppDir+tmpCurrAppDataFileName;
    //Selected Language
    property int isSelected;

    Connections{
        target: configTool
        onMicroAppXMLChangedFromThemeMgr:{
            if(dirName != '') origBaseMicroAppDir = "file:///"+dirName;
            if ((fileName != "") && (fileName.indexOf("data") !== -1)){
                var tempAppName = fileName.replace("_data.xml","");
                if(tempAppName != currAppName) return;
                currAppName = '';
                microAppDir = '';
                currAppImages = ''
                xmlModelSource = ''

                microAppDir = "file:///"+childPath+"/";
                currAppImages = "file:///"+childPath+"/images/";
                currAppName = tempAppName;
                xmlModelSource = microAppDir+currAppName+"_data.xml";
                tourAppData.reload();
                selectionListModel.reload();
            }
        }
    }

    function refreshData(){
        selectionListModel.source="";
        selectionListModel.source = currAppName ? xmlModelSource : "";
        tourAppData.source="";
        tourAppData.source = currAppName ? xmlModelSource : "";
    }
    function refreshModels(){
        selectionListModel.reload()
        tourAppData.reload()
    }

    XmlListModel{
        id: tourAppData;
//        source: currAppName ? xmlModelSource : "";
        query: "/application/screen[@_template='"+selectedTemplate+"']";

        XmlRole { name:"welcomeText"; query: "text1/"+langCode+"/@_value/string()"; }
        XmlRole { name:"welcomeText_cdata"; query: "text1/"+langCode+"/string()"; }
        XmlRole { name:"additionalInfoText"; query: "text2/"+langCode+"/@_value/string()"; }
        XmlRole { name:"additionalInfoText_cdata"; query: "text2/"+langCode+"/string()"; }
        XmlRole { name:"bgImage"; query: "image1/@_value/string()"; }
        XmlRole { name:"logoImage"; query: "image2/@_value/string()"; }
        XmlRole { name:"template"; query: "@_id/string()"; }
        XmlRole { name:"screenID"; query: "@_id/string()"; }

        onCountChanged: {
            childLoader.sourceComponent = appSelection;
        }
    }

    XmlListModel{
        id: selectionListModel;
//        source: currAppName ? xmlModelSource : "";
        query: "/application/screen[@_template='"+selectedTemplate+"']/list1/listItem";

        XmlRole { name:"countryName"; query: "text2/"+langCode+"/@_value/string()"; }
        XmlRole { name:"screenLink"; query: "link2/@_link/string()"; }
    }

    Loader{
        id:childLoader;
        height: core.height; width: core.width;
        onStatusChanged: {
            if(childLoader.status==Loader.Ready);
                childLoader.item.init();
        }
    }
}
