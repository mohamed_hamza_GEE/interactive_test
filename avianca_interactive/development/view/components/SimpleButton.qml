import Qt 4.7

Image {
    id:iconBtn
    property bool isHighlight:false;
    property bool increaseTouchArea: false
    property bool isPressed: false
    property bool isDisable: false
    property bool isSelected: false
    property bool isFocusRequired: true
    property bool forcelySelected: false
    property string highlightimg:"";
    property string normImg:"";
    property string selectedImg:"";
    property string pressedImg:"";
    property string disableImg:"";
    property alias iconBtn: iconBtn
    property double disableOpacity: 0.4
    property int increaseValue: -40
    signal actionPressed()
    signal actionReleased()
    signal enteredReleased()
    signal adjustfocus
    fillMode: Image.PreserveAspectFit
    source:((isPressed)?pressedImg:(isSelected?selectedImg:(innerImg.opacity==1)?"":normImg))
    width: sourceSize.width<=0?innerImg.sourceSize.width:sourceSize.width
    height: sourceSize.height<=0?innerImg.sourceSize.height:sourceSize.height
    opacity:(isDisable)?disableOpacity:1;
    Image{
        id:innerImg
        anchors.fill: parent
        source:forcelySelected?selectedImg:((isHighlight)?highlightimg:((isPressed)?pressedImg:((isSelected)?selectedImg:"")))
        opacity:(isHighlight)?1:0;
        Behavior on opacity {
            NumberAnimation{duration:300;}
        }
    }
    onActionReleased:  {
        if(isFocusRequired) iconBtn.forceActiveFocus()
        else adjustfocus()
        enteredReleased()
    }
    Keys.onPressed: {
        if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
            if(isDisable)return;
            if(pressedImg!="")
                isPressed=true
            actionPressed();
        }
    }
    Keys.onReleased: {
        if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
            if(isDisable)return;
            isPressed=false
            actionReleased()
        }
    }
    MouseArea{
        anchors.fill:parent;
        anchors.margins: increaseTouchArea ? -increaseValue : 0
        onPositionChanged: isPressed=false
        onPressed: {
            if(isDisable)return;
            if(pressedImg!="")
                isPressed=true
            actionPressed();
        }
        onReleased:{
            if(isDisable)return;
            isPressed=false
            actionReleased()
        }
    }
}
