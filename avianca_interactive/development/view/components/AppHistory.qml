import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"


FocusScope {
    id: appHistory;
    width: core.width;
    height: core.height

    property string currAppImages: microAppDir+currAppName+"/images/"
    property string xmlModelSource: microAppDir+currAppName+"/"+currAppName+"_data.xml"
    property int selectedIndex: -1;

    function init() {
        core.log("AppHistory | init ");
        historyAppInfoData.source = xmlModelSource;
        historyAppListData.source = xmlModelSource;
        selectedIndex = -1;
    }
    function reload(){
        core.log("AppHistory | reload called ")
        historyAppInfoData.source = xmlModelSource;
        historyAppListData.source = xmlModelSource;
        historyAppInfoData.reload();
        historyAppListData.reload();
    }

    function loadAirlineInfoVOD(dModel) {
        viewHelper.setMediaListingModel(dModel);
        viewHelper.setMediaListingIndex(0);
        viewHelper.storeTrailerParentMid(dModel.getValue(0,"mid"));
        viewHelper.initializeVOD("restart","trailer","",false, true)
    }
    function setTitleText(obj,Dmodel){
        obj.text = (Dmodel.get(0).title_cdata !== "") ? Dmodel.get(0).title_cdata : Dmodel.get(0).title;
        obj.color = configTool.config.aviancaapp.screen.text_header_title;
        obj.font.pixelSize = qsTranslate('','aviapp_header_title_fontsize')
    }
    function setTitleInfoText(obj,Dmodel){
        obj.text = (Dmodel.get(0).menuTitle_cdata !== "") ? Dmodel.get(0).menuTitle_cdata : Dmodel.get(0).menuTitle;
        obj.color = configTool.config.aviancaapp.screen.text_header_menu;
        obj.font.pixelSize = qsTranslate('','aviapp_header_menu_fontsize');
    }
    function setInfoText(obj,Dmodel,ind,modelName){
        var modelName_cdata = modelName+"_cdata";
        obj.text = (eval("Dmodel.get("+ind+")."+modelName_cdata) !== "") ? eval("Dmodel.get("+ind+")."+modelName_cdata) : eval("Dmodel.get("+ind+")."+modelName);
        obj.color = configTool.config.aviancaapp.screen.text_path_info;
        obj.font.pixelSize = qsTranslate('','aviapp_path_info_fontsize')
    }
    function languageUpdate(){
        historyAppInfoData.reload()
        historyAppListData.reload()
    }
    XmlListModel {
        id:historyAppInfoData;
        query: "/application/screen";

        XmlRole { name:"title"; query: "text1/"+langCode+"/@_value/string()" }
        XmlRole { name:"title_cdata"; query: "text1/"+langCode+"/string()" }
        XmlRole { name:"menuTitle"; query: "text2/"+langCode+"/@_value/string()" }
        XmlRole { name:"menuTitle_cdata"; query: "text2/"+langCode+"/string()" }

        onCountChanged: {
            setTitleText(headerTitle, historyAppInfoData);
            setTitleInfoText(headerMenuTitle, historyAppInfoData);
        }
    }
    XmlListModel {
        id: historyAppListData;
        query: "/application/screen/list1/listItem";

        XmlRole { name : "historyAppListText"; query:"text4/"+langCode+"/@_value/string()" }
        XmlRole { name : "historyAppListText_cdata"; query:"text4/"+langCode+"/string()" }
        XmlRole { name : "historyAppListImage"; query:"image2/@_value/string()" }
        XmlRole { name : "historyAppListVideoIcon"; query: "text5/@_value/string()" }
        XmlRole { name : "historyAppListVideo"; query:"text6/"+langCode+"/@_value/string()" }
        XmlRole { name : "info"; query: "text3/"+langCode+"/@_value/string()" }
        XmlRole { name : "info_cdata"; query: "text3/"+langCode+"/string()" }

        onCountChanged: {
            historyPathView.model = historyAppListData;
            setInfoText(infoText,historyAppListData,historyPathView.currentIndex,"info");
        }
    }
    Image {
        id: backgroundImage;
        anchors.top: headerImage.bottom;
        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.app_historybg;
    }
    Image {
        id: headerImage;
        anchors.top: parent.top;
        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.app_header;
    }
    SimpleButton {
        id: closeBtn;

        anchors.right: headerImage.right; anchors.top:headerImage.top;
        anchors.rightMargin: qsTranslate('','aviapp_header_close_gap');
        anchors.topMargin: qsTranslate('','aviapp_header_close_gap');
        increaseTouchArea: true
        increaseValue:-60
        isHighlight: activeFocus;
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_p;
        onEnteredReleased: {
            isChildAppSelected = true;
            viewController.getActiveScreenRef().actionOnExit(true);
            // viewController.showScreenPopup(23);
        }
        Keys.onDownPressed: {
            historyPathView.forceActiveFocus();
        }
    }

    ViewText {
        id: headerTitle;
        width: qsTranslate('','aviapp_header_title_w');
        x: qsTranslate('','aviapp_header_title_x');
        anchors.verticalCenter: headerImage.verticalCenter;
        wrapMode: Text.Wrap;
        maximumLineCount:1;
    }

    ViewText {
        id: headerMenuTitle;
        width: qsTranslate('','aviapp_header_menu_w');
        anchors.top:headerImage.top; anchors.topMargin: qsTranslate('','aviapp_header_menu_y');
        anchors.right: headerImage.right; anchors.rightMargin: qsTranslate('','aviapp_header_menu_rm');
        anchors.verticalCenter: headerImage.verticalCenter;
        wrapMode: Text.Wrap;
        maximumLineCount:1;
        MouseArea{
            anchors.fill: parent
            onClicked: {
                isChildAppSelected = true;
                viewController.getActiveScreenRef().actionOnExit(true);
            }
        }
    }

    PathView {
        id: historyPathView;
        height: qsTranslate('','aviapp_pathview_h');
        width: qsTranslate('','aviapp_pathview_w');
        anchors.top:headerImage.bottom;
        anchors.topMargin: qsTranslate('','aviapp_pathview_tm');
        anchors.horizontalCenter: parent.horizontalCenter;
        highlightRangeMode:PathView.StrictlyEnforceRange;
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        pathItemCount :count>=5?5:count;
        highlightMoveDuration:600
        //        model: historyAppListData;
        delegate: historyDelegate;
        path: hist_Path;
        onCountChanged: {
            selectedIndex = 0;
            historyPathView.currentIndex = 0;
            historyPathView.forceActiveFocus();
        }
        onCurrentIndexChanged: {
            setInfoText(infoText,historyAppListData,historyPathView.currentIndex,"info");
            historyFlickableContainer.contentY = 0;
        }

        Keys.onLeftPressed: {historyPathView.decrementCurrentIndex();}
        Keys.onRightPressed: {historyPathView.incrementCurrentIndex();}
        Keys.onUpPressed: {closeBtn.forceActiveFocus();}
        Keys.onDownPressed: {
            if(scroller.visible){
                scroller.forceActiveFocus();
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                if((historyAppListData.get(historyPathView.currentIndex).historyAppListVideoIcon == "true")){
                    viewHelper.isMicroAppVideo = true;

                    //console.log()
                    var params=["media_attr_enum","",historyAppListData.get(historyPathView.currentIndex).historyAppListVideo];
                    dataController.getMediaByFieldName(params,loadAirlineInfoVOD);
                }else {
                    //                    core.log("appHistory | VIDEO NOT PRESENT");
                }
            }
        }
    }


    Path {
        id: hist_Path;
        startX: Math.round(core.settings.resolutionIndex==2?250:(250/1.33)); startY: (historyPathView.height/2)+Math.round(core.settings.resolutionIndex==2?35:(35/1.33))
        //        PathAttribute{ name: "iconScale";value:0.37; }
        //        PathAttribute{ name: "iconOpacity";value:qsTranslate('','aviapp_pathview_opacity_second'); }
        //        PathAttribute{ name: "iconHeight";value:-(Math.round(historyPathView.height/3.8))}
        //        PathAttribute{ name: "iconOffset";value:0}
        //        PathAttribute{ name: "dividerWidth";value:((Math.floor(historyPathView.height/6))/5)}
        //        PathAttribute{ name: "zValue";value:0}

        //        PathLine{x:Math.round(historyPathView.width*0.044); y:(historyPathView.height/2); }
        PathAttribute{ name: "iconScale";value:0.35; }
        PathAttribute{ name: "iconOpacity";value:qsTranslate('','aviapp_pathview_opacity_second'); }
        PathAttribute{ name: "iconHeight";value:-(Math.round(historyPathView.height/3.8)) }
        PathAttribute{ name: "iconOffset";value:0}
        PathAttribute{ name: "iconVOffset";value:Math.round(core.settings.resolutionIndex==2?35:(35/1.33));}
        PathAttribute{ name: "dividerWidth";value:0}
        PathAttribute{ name: "zValue";value:1}

        PathLine{x:Math.round(core.settings.resolutionIndex==2?-10:(-10/1.33)); y:(historyPathView.height/2)+Math.round(core.settings.resolutionIndex==2?20:(20/1.33)); }
        PathAttribute{ name: "iconScale";value:0.60; }
        PathAttribute{ name: "iconOpacity";value:qsTranslate('','aviapp_pathview_opacity_first'); }
        PathAttribute{ name: "iconHeight";value:-(Math.floor(historyPathView.height/8)) }
        PathAttribute{ name: "iconOffset";value:-(Math.floor(historyPathView.height/6))}
        PathAttribute{ name: "iconVOffset";value:Math.round(core.settings.resolutionIndex==2?20:(20/1.33));}
        PathAttribute{ name: "dividerWidth";value:(Math.floor(historyPathView.height/6))}
        PathAttribute{ name: "zValue";value:2}

        PathLine{x: Math.round(core.settings.resolutionIndex==2?670:(670/1.33)); y:(historyPathView.height/2)+Math.round(core.settings.resolutionIndex==2?10:(10/1.33)); }
        PathAttribute{ name: "iconScale";value:1 }
        PathAttribute{ name: "iconOpacity";value:1; }
        PathAttribute{ name: "iconHeight";value:(Math.round(historyPathView.height/16)) }
        PathAttribute{ name: "iconOffset";value:0}
        PathAttribute{ name: "iconVOffset";value:Math.round(core.settings.resolutionIndex==2?10:(10/1.33));}
        PathAttribute{ name: "dividerWidth";value:(Math.floor(historyPathView.height/6))-((Math.floor(historyPathView.height/6))/10)}
        PathAttribute{ name: "zValue";value:3}

        PathLine{x:Math.round(core.settings.resolutionIndex==2?1310:(1310/1.33)); y:(historyPathView.height/2)+Math.round(core.settings.resolutionIndex==2?20:(20/1.33)); }
        PathAttribute{ name: "iconScale";value:0.60; }
        PathAttribute{ name: "iconOpacity";value:qsTranslate('','aviapp_pathview_opacity_first'); }
        PathAttribute{ name: "iconHeight";value:-(Math.floor(historyPathView.height/8)) }
        PathAttribute{ name: "iconOffset";value:Math.floor(historyPathView.height/6)}
        PathAttribute{ name: "iconVOffset";value:Math.round(core.settings.resolutionIndex==2?20:(20/1.33));}
        PathAttribute{ name: "dividerWidth";value:(-(Math.floor(historyPathView.height/6))-((Math.floor(historyPathView.height/6))/5))}
        PathAttribute{ name: "zValue";value:2}

        PathLine{ x:Math.round(core.settings.resolutionIndex==2?1050:(1050/1.33)); y:(historyPathView.height/2)+Math.round(core.settings.resolutionIndex==2?35:(35/1.33)); }
        PathAttribute{ name: "iconScale";value:0.35; }
        PathAttribute{ name: "iconOpacity";value:qsTranslate('','aviapp_pathview_opacity_second'); }
        PathAttribute{ name: "iconHeight";value:-(Math.round(historyPathView.height/3.8)) }
        PathAttribute{ name: "iconOffset";value:0}
        PathAttribute{ name: "iconVOffset";value:Math.round(core.settings.resolutionIndex==2?35:(35/1.33));}
        PathAttribute{ name: "dividerWidth";value:0}
        PathAttribute{ name: "zValue";value:1}

        //        PathLine{ x:Math.round(historyPathView.width*1.185); y:(historyPathView.height/2); }
        //        PathAttribute{ name: "iconScale";value:0.37; }
        //        PathAttribute{ name: "iconOpacity";value:qsTranslate('','aviapp_pathview_opacity_second'); }
        //        PathAttribute{ name: "iconHeight";value:-(Math.round(historyPathView.height/3.8))}
        //        PathAttribute{ name: "iconOffset";value:0}
        //        PathAttribute{ name: "dividerWidth";value:0}
        //        PathAttribute{ name: "zValue";value:1}
    }

    Flickable{
        id:historyFlickableContainer;
        property alias infoText: infoText;
        width: qsTranslate('','aviapp_path_info_w');
        height: parseInt(qsTranslate('','aviapp_path_info_h'),10);
        anchors.top:historyPathView.bottom; anchors.topMargin: qsTranslate('','aviapp_path_info_tm');
        //        anchors.left: historyPathView.left; anchors.leftMargin: qsTranslate('','aviapp_path_info_lm');
        anchors.left: parent.left; anchors.leftMargin: qsTranslate('','aviapp_path_info_lm');
        contentWidth: width
        contentHeight: infoText.paintedHeight;
        clip:true;

        ViewText {
            id: infoText;
            anchors.top: parent.top;
            anchors.left: parent.left;
            lineHeight: qsTranslate('','aviapp_path_info_lh');
            lineHeightMode:Text.FixedHeight;
            width: qsTranslate('','aviapp_path_info_w');
            height:  parseInt(qsTranslate('','aviapp_path_info_h'),10);
            wrapMode: Text.WordWrap;
            varText: [
                (historyAppListData.get(historyPathView.currentIndex).info_cdata !="") ? historyAppListData.get(historyPathView.currentIndex).info_cdata : historyAppListData.get(historyPathView.currentIndex).info,
                                                                                         configTool.config.aviancaapp.screen.text_path_info,
                                                                                         qsTranslate('','aviapp_path_info_fontsize')
            ]
        }
    }

    Scroller{
        id:scroller
        height: qsTranslate('','aviapp_path_info_scroll_h');
        width: qsTranslate('','aviapp_path_info_scroll_w');
        opacity: (visible)?1:0;
        anchors.top: historyFlickableContainer.top
        anchors.topMargin: qsTranslate('','aviapp_path_info_scroll_tm');
        anchors.left:historyFlickableContainer.right;
        anchors.leftMargin:qsTranslate('','aviapp_path_info_scroll_lm');
        sliderbg:[qsTranslate('','aviapp_path_info_scroll_base_margin'),qsTranslate('','aviapp_path_info_scroll_w'),qsTranslate('','aviapp_path_info_scroll_base_h'),configTool.config.aviancaapp.screen.scrollbg]
        scrollerDot: configTool.config.aviancaapp.screen.slider
        upArrowDetails:[configTool.config.aviancaapp.screen.app_arwup_n, configTool.config.aviancaapp.screen.app_arwup_h, configTool.config.aviancaapp.screen.app_arwup_h ]
        downArrowDetails:[configTool.config.aviancaapp.screen.app_arwdwn_n, configTool.config.aviancaapp.screen.app_arwdwn_h, configTool.config.aviancaapp.screen.app_arwdwn_h]
        targetData:historyFlickableContainer;
        onSliderNavigation: {
            if(direction == "up"){
                historyPathView.forceActiveFocus();
            }
        }
        onVisibleChanged: {
            if(!visible){
                historyPathView.forceActiveFocus();
            }
        }
    }
    Component{
        id:historyDelegate;
        Item {
            id: testContainer;
            //            height: historyPathView.height; width: historyListImageContainer.paintedWidth;//width: historyPathView.width/5;
            height: historyPathView.height; width: historyPathView.width/5;
            //            z:(index==historyPathView.currentIndex) ? 3 : ((index==(historyPathView.currentIndex - 1)) || (index==(historyPathView.currentIndex + 1)) || ((historyPathView.currentIndex == 0) && (index == (historyPathView.count -1)))) ? 2 : ((index==(historyPathView.currentIndex - 2)) || (index==(historyPathView.currentIndex + 2))) ? 1 : 0;
            z: PathView.zValue;

            Item{
                id: pathViewImage;
                height: historyListImageContainer.paintedHeight;
                width: historyListImageContainer.paintedWidth;
                scale:testContainer.PathView.iconScale;
                anchors.bottom: parent.bottom;
                anchors.bottomMargin: (testContainer.PathView.iconHeight);
                anchors.horizontalCenter: testContainer.horizontalCenter;
                anchors.horizontalCenterOffset: testContainer.PathView.iconOffset;
                Image {
                    id: historyListImage;
                    anchors.horizontalCenter: pathViewImage.horizontalCenter;
                    source: currAppImages + historyAppListData.get(index).historyAppListImage;
                    smooth:true

                    Image {
                        id: playIcon;
                        anchors.right: parent.right;
                        visible:((historyPathView.currentIndex == index) && (historyAppListData.get(index).historyAppListVideoIcon == "true")) ? true : false;
                        anchors.rightMargin: qsTranslate('','aviapp_pathview_play_rm');
                        anchors.bottom: parent.bottom;
                        anchors.bottomMargin: qsTranslate('','aviapp_pathview_play_bm');
                        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.history_play_h;
                        smooth:true
                    }
                    MouseArea{
                        anchors.fill: parent;
                        onReleased: {
                            historyPathView.forceActiveFocus();
                            if((historyPathView.currentIndex == index )&& (historyAppListData.get(index).historyAppListVideoIcon == "true")){
                                viewHelper.isMicroAppVideo = true;
                                var params=["media_attr_enum","",historyAppListData.get(index).historyAppListVideo];
                                dataController.getMediaByFieldName(params,loadAirlineInfoVOD);
                            }else {
                                //                                core.log("VIDEO NOT PRESENT .... ");
                                historyPathView.currentIndex = index;
                            }
                        }
                    }

                    Keys.onReleased: {
                        if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                            if((historyPathView.currentIndex == index )&& (historyAppListData.get(index).historyAppListVideoIcon == "true")){
                                viewHelper.isMicroAppVideo = true;
                                var params=["media_attr_enum","",historyAppListData.get(selectedIndex).historyAppListVideo];
                                dataController.getMediaByFieldName(params,loadAirlineInfoVOD);
                            }else {
                                //                                core.log("VIDEO NOT PRESENT .... ");
                            }
                        }
                    }
                }
                Image {
                    id: historyListImageContainer;
                    anchors.top: historyListImage.top;
                    anchors.horizontalCenter: historyListImage.horizontalCenter;
                    source: (index==historyPathView.currentIndex && historyPathView.activeFocus) ? viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.history_h : viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.history_n;
                    smooth:true
                    opacity: testContainer.PathView.iconOpacity
                }
            }
            Item {
                id: indicator;
                property alias pathIndicator: pathIndicator;
                property alias listDivider: listDivider;
                anchors.top: parent.bottom;
                anchors.horizontalCenter: testContainer.horizontalCenter;

                ViewText{
                    id: pathIndicatorText;
                    width: qsTranslate('','aviapp_path_indicator_text_w');
                    anchors.top: pathIndicator.bottom;
                    anchors.topMargin: qsTranslate('','aviapp_path_indicator_text_tm');
                    anchors.horizontalCenter: pathIndicator.horizontalCenter;
                    elide: Text.ElideRight;
                    maximumLineCount:1;
                    //                    visible: ((index==historyPathView.currentIndex) || (index==(historyPathView.currentIndex - 1)) || (index==(historyPathView.currentIndex + 1)) || ((index==(historyPathView.count -1 )) && (historyPathView.currentIndex == 0)) || ((index==0) && (historyPathView.currentIndex == (historyPathView.count -1))) ) ? true : false
                    horizontalAlignment: Text.AlignHCenter;
                    varText: [
                        //historyAppListText_cdata
                        (historyAppListData.get(index).historyAppListText_cdata != "")?historyAppListData.get(index).historyAppListText_cdata : historyAppListData.get(index).historyAppListText,
                                                                                        configTool.config.aviancaapp.screen.text_path_info,
                                                                                        qsTranslate('','aviapp_path_indicator_text_fontsize')
                    ]
                }

                Rectangle {
                    id:listDivider;
                    height: qsTranslate('','aviapp_divider_h');
                    width: (historyPathView.width) /*+ (testContainer.PathView.dividerWidth)*/
                    //                    z:(pathIndicator.z -2);
                    //                    anchors.left:pathIndicator.left;
                    //                    anchors.leftMargin: (pathIndicator.paintedWidth/2)
                    anchors.verticalCenter: pathIndicator.verticalCenter;
                    color: qsTranslate('','aviapp_divider_color');
                }
                Rectangle{
                    height: qsTranslate('','aviapp_divider_h');
                    width: (historyPathView.width) /*+ (testContainer.PathView.dividerWidth)*/

                    anchors.verticalCenter: pathIndicator.verticalCenter;
                    color: qsTranslate('','aviapp_divider_color');
                    visible: !(historyPathView.count<historyPathView.pathItemCount && index<historyPathView.count-1)
                    anchors.right: parent.right
                }

                Image {
                    id: pathIndicator
                    z:4;
                    anchors.top: parent.top;
                    anchors.topMargin: -testContainer.PathView.iconVOffset;
                    anchors.horizontalCenter: parent.horizontalCenter;
                    anchors.horizontalCenterOffset: testContainer.PathView.iconOffset;
                    source: (historyPathView.currentIndex == index) ? viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.path_s : viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.path_n;
                    smooth:true
                }
            }
        }
    }

}

