// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
FocusScope{
    id:shoppingSynopsis
    anchors.top:parent.top
    anchors.topMargin:parseInt(qsTranslate('','synopsis_shop_panel_tm'),10)
    property alias synopsisCloseBtn: synopsisCloseBtn
    property bool isSliderNavChanged: false
    property int currentMid: -1
    property int modelIndex:0
    property variant model:listingobj.compVpathListing.model
    property variant currentIndex:listingobj.compVpathListing.synopIndex
    property variant synopsisDetails: []
    property variant itemdetails:[]
    property string actualPrice:" ";
    property string detail:" ";
    property string currencyCode:" ";
    signal panelClose()
    signal extremeEnds(string direction)
    signal setFocusToLeft(int section);
    onActiveFocusChanged: {
        console.log("onActiveFocusChanged = "+activeFocus)

    }

    /**************************************************************************************************************************/
    onExtremeEnds:{
        if(direction=="left" && listingobj.compVpathListing.count>1){
            decrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="right" && listingobj.compVpathListing.count>1){
            incrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="up"){
            subCatobj.forceActiveFocus();
        }
    }
    /**************************************************************************************************************************/
    function init(){
        console.log("ShoppingSyopsis.qml | init() | viewHelper.shop_item_id = "+viewHelper.shop_item_id)
        synopsisCloseBtn.forceActiveFocus();
        setSynData();
    }

    function checkItem(obj){
        if(obj==undefined || obj ==""){
            return true;
        }
        return false;
    }

    function setSynData(){
        modelDisplay.model=refModel
        getPrices(itemdetails['price'])
        synopsisTitleBtn.text=""
        synopsisTitleBtn.text=itemdetails.title
        synopsisContentFlickable.synopsisSubDescIMG=""
        synopsisContentFlickable.synopsisSubDescIMG=itemdetails.description_long
        synopsisSubTitleBtn.text=""
        synopsisSubTitleBtn.text=itemdetails.sub_title
        item_detail.text=""
        item_detail.text= "SKU#"+itemdetails.item_sku;
        item_detail.text+=(checkItem(itemdetails.unit_size)?"":"   |   ")+
                (checkItem(itemdetails.unit_size)?"":itemdetails.unit_size)+(checkItem(itemdetails.unit_quantity)?"":"   |   ")+(checkItem(itemdetails.unit_quantity)?"":itemdetails.unit_quantity)
    }
    function getPrices(detailsPrice){
        for(var i=0;i<detailsPrice.length;i++){
            if(detailsPrice[i].item_currency_code == "USD"){
                actualPrice = detailsPrice[i].item_currency_symbol_left +detailsPrice[i].item_price
                currencyCode = detailsPrice[i].item_currency_code
            }
        }
        dollar.text=""
        currency.text=""
        dollar.text=currencyCode
        currency.text=actualPrice
    }
    function backBtnPressed(){
        closeLoader()
    }


    Repeater{
        id:modelDisplay
        Item {
            id:modelItem
            Component.onCompleted:{
                itemdetails = value
            }
        }
    }

    function clearOnExit(){
        shoppingSynopsis.visible=false
    }

    /**************************************************************************************************************************/
    Image{
        id:synopsisIMG
        source:viewHelper.configToolImagePath+configTool.config.shop.synopsis.panel
        anchors.horizontalCenter: parent.horizontalCenter
    }
    SimpleNavButton{
        id:synopsisCloseBtn
        anchors.right: synopsisIMG.right
        anchors.rightMargin: qsTranslate('','synopsis_close_rm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_close_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTool.config.shop.synopsis.close_btn_n;
        highlightimg:  viewHelper.configToolImagePath+configTool.config.shop.synopsis.close_btn_h;
        pressedImg: viewHelper.configToolImagePath+configTool.config.shop.synopsis.close_btn_p;
        nav:["",subCatobj,extremeEnds(right),slider.visible?slider:synopsisCloseBtn]
        onEnteredReleased:{closeLoader();}
        onPadReleased:extremeEnds(dir)
        onNoItemFound:  extremeEnds(dir)
    }
    Image{
        id:posterIMGBtn
        width: qsTranslate('','synopsis_shop_poster_w')
        height: qsTranslate('','synopsis_shop_poster_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin:  qsTranslate('','synopsis_shop_poster_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:  qsTranslate('','synopsis_shop_poster_tm')
        source:getPoster()
        //itemdetails.item_thumb
    }
    Image{
        id:overlayIMGBtn
        anchors.left:posterIMGBtn.left
        anchors.top:posterIMGBtn.top
        source:viewHelper.configToolImagePath+configTool.config.shop.synopsis.poster_overlay
    }
    ViewText{
        id:dollar
        anchors.top:posterIMGBtn.top
        anchors.topMargin:qsTranslate('','synopsis_shop_currency_tm')
        anchors.left:posterIMGBtn.left
        anchors.leftMargin:qsTranslate('','synopsis_shop_currency_lm')
        width: qsTranslate('','synopsis_shop_currency_w')
        height: qsTranslate('','synopsis_shop_currency_h')
        varText: [text,configTool.config.shop.synopsis.currency_color,qsTranslate('','synopsis_shop_currency_fontsize')]
        wrapMode: Text.Wrap
        textFormat :Text.StyledText
        maximumLineCount:1
    }
    ViewText{
        id:currency
        anchors.top:posterIMGBtn.top
        anchors.topMargin: qsTranslate('','synopsis_shop_price_tm')
        anchors.right:posterIMGBtn.right
        anchors.rightMargin: qsTranslate('','synopsis_shop_price_rm')
        width: qsTranslate('','synopsis_shop_price_w')
        height:  qsTranslate('','synopsis_shop_price_h')
        varText: [text,configTool.config.shop.synopsis.price_color,qsTranslate('','synopsis_shop_price_fontsize')]
        wrapMode: Text.Wrap
        textFormat :Text.StyledText
        maximumLineCount:1
        horizontalAlignment:Text.AlignRight
        font.bold:true

    }
    ViewText{
        id:item_detail
        anchors.left:synopsisIMG.left
        anchors.leftMargin:qsTranslate('','synopsis_shop_title_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_shop_desc2_tm')
        width: qsTranslate('','synopsis_shop_desc2_w')
        height:  qsTranslate('','synopsis_shop_desc2_h')
        varText: [text,configTool.config.shop.synopsis.details_color,qsTranslate('','synopsis_shop_desc2_fontsize')]
        wrapMode: Text.Wrap
        textFormat :Text.StyledText
        maximumLineCount:1
        font.bold:true
    }

    ViewText{
        id:synopsisTitleBtn
        width:qsTranslate('','synopsis_shop_title_w')
        height:paintedHeight//(lineCount==1)?parseInt(qsTranslate('','synopsis_movies_title_lh'),10):qsTranslate('','synopsis_shop_title_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin:qsTranslate('','synopsis_shop_title_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_shop_title_tm')
        varText: [text,configTool.config.shop.synopsis.title_color,qsTranslate('','synopsis_shop_title_fontsize')]
        maximumLineCount: 2
        elide: Text.ElideRight
        wrapMode: Text.WordWrap
        lineHeight:parseInt(qsTranslate('','synopsis_movies_title_lh'),10)+4
        lineHeightMode: Text.FixedHeight
    }
    ViewText{
        id:synopsisSubTitleBtn
        width:qsTranslate('','synopsis_shop_subtitle_w')
        anchors.left:synopsisIMG.left
        anchors.leftMargin:qsTranslate('','synopsis_shop_title_lm')
        anchors.top:synopsisTitleBtn.bottom
        anchors.topMargin:qsTranslate('','synopsis_shop_subtitle_tm')
        varText: [text,configTool.config.shop.synopsis.short_title_color,qsTranslate('','synopsis_shop_subtitle_fontsize')]
        elide: Text.ElideRight
        maximumLineCount: 1
        wrapMode: Text.Wrap
    }
    Flickable{
        id : synopsisContentFlickable
        width:qsTranslate('','synopsis_shop_desc1_w')
        height:(synopsisTitleBtn.lineCount==1)?qsTranslate('','synopsis_shop_desc1_h'):qsTranslate('','synopsis_shop_desc1_h1')
        contentWidth: width
        contentHeight: synopsisSubDescIMG.paintedHeight
        anchors.top:synopsisSubTitleBtn.bottom
        anchors.topMargin:qsTranslate('','synopsis_shop_desc1_tm')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: qsTranslate('','synopsis_shop_title_lm')
        clip:true
        interactive:contentHeight>height
        property alias synopsisSubDescIMG: synopsisSubDescIMG.text
        ViewText{
            id:synopsisSubDescIMG
            width:parent.width
            height:parent.height
            varText:[text,configTool.config.shop.synopsis.description1_color, qsTranslate('','synopsis_shop_desc1_fontsize')]
            wrapMode: Text.WordWrap
        }
    }
    Image{
        id:textOverlay
        visible:slider.visible?(slider.downArrow.isDisable?false:true):false
        width:synopsisContentFlickable.width
        //        normalBorder: [qsTranslate('','common_scrollfade_extra_margin')]
        anchors.bottom: synopsisContentFlickable.bottom
        anchors.horizontalCenter: synopsisContentFlickable.horizontalCenter
        source:viewHelper.configToolImagePath+configTool.config.shop.synopsis.fading_image

    }
    Scroller{
        id:slider
        anchors.top:synopsisIMG.top; anchors.topMargin: qsTranslate('','synopsis_slider_shop_tm')
        anchors.right: synopsisIMG.right ;anchors.rightMargin: qsTranslate('','synopsis_slider_rm')
        width: qsTranslate('','synopsis_slider_w')
        height: qsTranslate('','synopsis_slider_shop_h')
        targetData:visible?synopsisContentFlickable:null
        sliderbg:[qsTranslate('','synopsis_slider_base_margin'),qsTranslate('','synopsis_slider_w'),qsTranslate('','synopsis_slider_shopbase_h'),configTool.config.shop.synopsis.scroll_base,]
        scrollerDot:configTool.config.shop.synopsis.scroll_button
        downArrowDetails:  [configTool.config.shop.synopsis.dn_arrow_n,configTool.config.shop.synopsis.dn_arrow_h,configTool.config.shop.synopsis.dn_arrow_p]
        upArrowDetails: [configTool.config.shop.synopsis.up_arrow_n,configTool.config.shop.synopsis.up_arrow_h,configTool.config.shop.synopsis.up_arrow_p]
        visible: synopsisContentFlickable.contentHeight>synopsisContentFlickable.height
        onSliderNavigation: {
            if(direction=="down" ){
                slider.forceActiveFocus()
            }else if(direction=="up")
                synopsisCloseBtn.forceActiveFocus()
            else if(direction=="right" || direction=="left"){
                isSliderNavChanged=true
                extremeEnds(direction)
            }
        }
    }
}
