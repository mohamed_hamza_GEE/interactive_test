import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    id:lifeMileEarnScreen
    width: core.height;
    height: core.width;
    opacity: 0
    property string dataXML : childAppName;
    property string childAppImages:''//microappMain.childAppImages
    property variant parentArr;
    property variant seInd;
    property variant arr: [];
    property string scrollDir;
    property int lastSeletedParentIndex : 0;
    property int lastSeletedChildIndex : 0;
    property int falseImgSize: (microAppName == "aviancaLifeMile_business") ? 40 : 35;//(pif.getCabinClassName() === "Business" ) ? 40 : 35;
    property string focusDir:'right'

    function init(){
        core.log("LifeMilesEarn | init ");
        lifeMileEarnScreen.opacity=0;
        viewHelper.blockKeysForSeconds();
        pSTimer.restart();
    }
    Timer{id:pSTimer;interval:750;onTriggered:{isChildIconAvailable();}}

    function setPosterImage(obj, Dmodel){
        obj.source = childAppImages + Dmodel.get(0).posterImage;
    }

    function setTitleText(obj,Dmodel){
        obj.text = (Dmodel.get(0).titleText_cdata !== "") ? Dmodel.get(0).titleText_cdata : Dmodel.get(0).titleText;
        obj.color = configTool.config.lifemiles.screen.text_title;
        obj.font.pixelSize = qsTranslate('','lifemiles_title_fontsize');
    }

    function setTitleInfoText(obj,Dmodel){
        obj.text = (Dmodel.get(0).titleInfoText_cdata !== "") ? Dmodel.get(0).titleInfoText_cdata : Dmodel.get(0).titleInfoText;
        obj.color = configTool.config.lifemiles.screen.text_title_info;
        obj.font.pixelSize = qsTranslate('','lifemiles_title_fontsize1');
    }
    function setFocus(dir){
        focusDir = dir;
        if(earnFlickableData.isIconAvailable){
            earnFlickableData.currentIndex=lastSeletedParentIndex;
            earnFlickableData.currentItem.childDataContainer.currentIndex=lastSeletedChildIndex;
            earnFlickableData.forceActiveFocus();}
        else {
            if(scroller.visible){
                scroller.forceActiveFocus();
            }else{
                appCloseBtn.forceActiveFocus();
            }
        }
    }

    function setRightFocus(){
        lastSeletedParentIndex = earnFlickableData.currentIndex;
        lastSeletedChildIndex = earnFlickableData.currentItem.childDataContainer.currentIndex;
        if(scroller.visible){
            scroller.forceActiveFocus();
        }else{
            appCloseBtn.forceActiveFocus();
        }
    }

    function setLeftFocus(){
        lastSeletedParentIndex = earnFlickableData.currentIndex;
        lastSeletedChildIndex = earnFlickableData.currentItem.childDataContainer.currentIndex;
        microappMain.menuContainer.forceActiveFocus();
    }

    function setFocusFromChild(){
        if(focusDir == "right"){
            setRightFocus();
        }else{
            setLeftFocus();
        }
    }

    function reloadData(){
        commonDataModel.reload();
        earnList.reload();
    }


    function isChildIconAvailable(){
        var cnt = earnFlickableData.count;
        for(var i=0;i<cnt;i++){
            earnFlickableData.currentIndex = i;
            var childCnt = earnFlickableData.currentItem.childDataContainer.count;
            for(var p=0;p<childCnt;p++){
                earnFlickableData.currentItem.childDataContainer.currentIndex = p;
                if(earnFlickableData.currentItem.childDataContainer.currentItem.addIconOpacity){
                    lastSeletedParentIndex = i;
                    lastSeletedChildIndex = p;
                    earnFlickableData.isIconAvailable=true;
                    setF();
                    return;
                }
            }
        }
        setF();
    }

    function setF(){
        earnFlickableData.currentIndex = 0;
        lifeMileEarnScreen.opacity = 1;
        microappMain.menuContainer.forceActiveFocus();
    }


    XmlListModel {
        id: commonDataModel;
        source: dataXML;
        query:"/application/screen[@_template='template2']";
        XmlRole { name: "posterImage"; query:"image1/@_value/string()" }
        XmlRole { name: "titleText"; query:"text1/"+langCode+"/@_value/string()" }
        XmlRole { name: "titleInfoText"; query:"text2/"+langCode+"/@_value/string()" }
        XmlRole { name: "titleText_cdata"; query:"text1/"+langCode+"/string()" }
        XmlRole { name: "titleInfoText_cdata"; query:"text2/"+langCode+"/string()" }

        onCountChanged: {
            setPosterImage(screenPosterImage, commonDataModel);
            setTitleText(title, commonDataModel);
            setTitleInfoText(titleinfo, commonDataModel);
        }
    }

    XmlListModel {
        id: earnList;
        source: dataXML;
        query:"/application/screen/list1/listItem";
        XmlRole { name: "link"; query:"link1/@_link/string()" }
    }

    Timer{id:delay; interval: 50;
        onTriggered: {
            blankRect.opacity=0
        }
    }

    Rectangle{
        id: blankRect;
        height: screenBgImage.paintedHeight;
        width: screenBgImage.paintedWidth;
        color:"WHITE"
        z:1;
        opacity: 1;
        anchors.fill: screenBgImage;
    }

    Image {
        id:screenBgImage;
        source: viewHelper.configToolImagePath + configTool.config.lifemiles.screen.discover_bg;
    }

    SimpleButton {
        id: appCloseBtn;
        property alias appCloseBtn:appCloseBtn;
        z:1;
        increaseTouchArea: true
        increaseValue:-60
        anchors.right: screenBgImage.right;
        anchors.top: screenBgImage.top;
        anchors.rightMargin: qsTranslate('','lifemiles_close_y');
        anchors.topMargin: qsTranslate('','lifemiles_close_x');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;
        onEnteredReleased: {
            viewController.showScreenPopup(23);
        }
        Keys.onLeftPressed: {
            if(scroller.visible){
                scroller.forceActiveFocus();
            }else{
              earnFlickableData.lastSelectedIndex(lastSeletedParentIndex,lastSeletedChildIndex);
            }

        }
    }

    Image {
        id: screenPosterImage;
        anchors.right:screenBgImage.right;
        anchors.verticalCenter: screenBgImage.verticalCenter;
    }

    ViewText{
        id:title;
        anchors.top: screenBgImage.top;
        anchors.left: screenBgImage.left;
        anchors.topMargin: qsTranslate('','lifemiles_title_tm');
        anchors.leftMargin: qsTranslate('','lifemiles_title_lm');
        width: qsTranslate('','lifemiles_title_w');
        wrapMode: Text.Wrap;
        elide: Text.ElideRight;
        maximumLineCount:1;
    }

    ViewText{
        id:titleinfo;
        anchors.top: title.bottom;
        anchors.left: title.left;
        anchors.topMargin: qsTranslate('','lifemiles_title_info_vgap');
        width: qsTranslate('','lifemiles_title_info_w');
        height: qsTranslate('','lifemiles_title_info_h');
        //        wrapMode: Text.Wrap;
        wrapMode: Text.WordWrap;
        //        lineHeight:qsTranslate('','lifemiles_title_lh');
        //        lineHeightMode: Text.FixedHeight
        elide: Text.ElideRight;
        maximumLineCount:3;
    }

    ListView{
        id: earnFlickableData;
        property bool isIconAvailable: false
        height: qsTranslate('','lifemiles_info_h');
        width: qsTranslate('','lifemiles_info_w');
        anchors.top:titleinfo.bottom;
        anchors.topMargin: qsTranslate('','lifemiles_info_tm');
        anchors.left: titleinfo.left;
        delegate: childListDataDelegate;
        model: earnList;
        clip:true
        spacing: qsTranslate('','lifemiles_info_linegap');
        cacheBuffer: core.height*2; //parseInt(qsTranslate('','lifemiles_info_h'))*count;

        Behavior on contentY {
            SequentialAnimation{
                NumberAnimation{duration:200}
                ScriptAction{
                    script: earnFlickableData.getplusIconIndex(scrollDir);
                }
            }
        }

        function lastSelectedIndex(p_index, c_index){
            focusDir = "left";
            earnFlickableData.currentIndex = p_index;
            earnFlickableData.currentItem.childDataContainer.currentIndex = c_index;
            earnFlickableData.forceActiveFocus();
            if(!earnFlickableData.currentItem.childDataContainer.currentItem.addIconOpacity)earnFlickableData.currentItem.childDataContainer.moveDown();
        }

        function getplusIconIndex(dir){
            earnFlickableData.currentIndex = (earnFlickableData.indexAt(earnFlickableData.contentX,earnFlickableData.contentY+10)+1)
            earnFlickableData.currentItem.childDataContainer.currentIndex = earnFlickableData.currentItem.childDataContainer.indexAt(earnFlickableData.currentItem.childDataContainer.contentX+10,earnFlickableData.currentItem.childDataContainer.contentY+5);//0
            if(!earnFlickableData.currentItem.childDataContainer.currentItem.addIconOpacity){
                if(dir == "down"){
                    earnFlickableData.currentItem.childDataContainer.moveDown();
                }else{
                    earnFlickableData.currentItem.childDataContainer.moveUp();
                }
            }
            lastSeletedChildIndex = earnFlickableData.currentItem.childDataContainer.currentIndex;
            lastSeletedParentIndex = earnFlickableData.currentIndex;
        }
    }

    Component{
        id: childListDataDelegate;
        Item {
            id: childlistItem;
            property bool isFocus: (earnFlickableData.currentIndex==index)
            property alias childDataContainer:childDataContainer;
            property int parentIndex:index;
            height: parseInt(dividerLine.height,10) + parseInt(childDataContainer.height,10)+ parseInt(qsTranslate('','lifemiles_info_earntxt_vgap'),10);
            width: dividerLine.width;
            Rectangle{
                id: dividerLine;
                anchors.top: parent.top;
                anchors.left: parent.left;
                height: qsTranslate('','lifemiles_line_h');
                width: qsTranslate('','lifemiles_info_w');
                color: qsTranslate('','lifemiles_line_color');
            }

            XmlListModel{
                id: chilDataModel;
                source: dataXML;
                query: "/application/screen[@_id='"+earnList.get(index).link+"']/list2/listItem";
                XmlRole { name: "headerText"; query:"text3/"+langCode+"/@_value/string()" }
                XmlRole { name: "headerText_cdata"; query:"text3/"+langCode+"/string()" }
                XmlRole { name: "headerInfoText"; query:"text5/"+langCode+"/@_value/string()" }
                XmlRole { name: "headerInfoText_cdata"; query:"text5/"+langCode+"/string()" }
                XmlRole { name: "addBtnVisibility"; query:"text4/@_value/string()" }
                XmlRole { name: "icon"; query:"image2/@_value/string()" }
                XmlRole { name: "addScreenID"; query:"link2/@_link/string()" }
                onCountChanged: {
                    falseIconImg.source = childAppImages + chilDataModel.get(0).icon;
                }
            }

            ListView{
                id: childDataContainer;
                // instead of 40 take value from Design team about each icon height for both resolutions.
                // Here we have take height of icon image in 1368 resolution.
                // 40 is the height of icon image in 1368 res and 35 is the height in 1024 resoultion

                height: (falseIconImg.paintedHeight == 0) ? ((falseImgSize * (count+1)) + parseInt(qsTranslate('','lifemiles_info_linegap'))) : falseIconImg.paintedHeight * (count+1) + parseInt(qsTranslate('','lifemiles_info_linegap'));
                width: parent.width;
                anchors.top: dividerLine.bottom;
                anchors.topMargin: qsTranslate('','lifemiles_info_linegap');
                anchors.left: parent.left;
                spacing: qsTranslate('','lifemiles_info_icon_tm');
                model: chilDataModel;
                delegate: childDataDelegate;
                interactive: false;
                focus: isFocus

                onContentHeightChanged: {
                    if(contentHeight > 0)childDataContainer.height = contentHeight + (parseInt(qsTranslate('','lifemiles_info_linegap'),10));
                }

                function moveDown(){
                    if(earnFlickableData.currentItem.childDataContainer.currentIndex==(earnFlickableData.currentItem.childDataContainer.count-1)){
                        if(earnFlickableData.currentIndex==(earnFlickableData.count-1)){
                            if(!earnFlickableData.isIconAvailable)setFocusFromChild();return;
                        }
                        earnFlickableData.incrementCurrentIndex();
                        earnFlickableData.currentItem.childDataContainer.currentIndex = 0;
                        if(!earnFlickableData.currentItem.childDataContainer.currentItem.addIconOpacity){moveDown();}
                        else {earnFlickableData.isIconAvailable=true;}
                    }else{
                        earnFlickableData.currentItem.childDataContainer.incrementCurrentIndex();
                    }

                    if(earnFlickableData.currentItem.childDataContainer.currentIndex == (earnFlickableData.currentItem.childDataContainer.count-1) && earnFlickableData.currentIndex == (earnFlickableData.count-1) && !earnFlickableData.isIconAvailable){
                        setFocusFromChild();
                        return;
                    }

                    if(!earnFlickableData.currentItem.childDataContainer.currentItem.addIconOpacity) {
                        var parentIndx = earnFlickableData.currentIndex==(earnFlickableData.count-1)
                        var childIndx = earnFlickableData.currentItem.childDataContainer.currentIndex==(earnFlickableData.currentItem.childDataContainer.count-1)
                        if(parentIndx && childIndx && !earnFlickableData.currentItem.childDataContainer.currentItem.addIconOpacity){
                            moveUp();
                        }else{
                            moveDown();
                        }
                    }else {
                        earnFlickableData.isIconAvailable=true;
                    }

                }

                function moveUp(){
                    if(earnFlickableData.currentItem.childDataContainer.currentIndex==0 && earnFlickableData.currentIndex == 0){
                        if(!earnFlickableData.isIconAvailable)setFocusFromChild();
                        return;
                    }
                    if(earnFlickableData.currentItem.childDataContainer.currentIndex==0){
                        earnFlickableData.decrementCurrentIndex();
                        earnFlickableData.currentItem.childDataContainer.currentIndex = earnFlickableData.currentItem.childDataContainer.count-1;
                        if(!earnFlickableData.currentItem.childDataContainer.currentItem.addIconOpacity)moveUp();
                        else earnFlickableData.isIconAvailable=true;
                    }else {
                        earnFlickableData.currentItem.childDataContainer.decrementCurrentIndex();
                    }

                    if(earnFlickableData.currentItem.childDataContainer.currentIndex == 0 && earnFlickableData.currentIndex == 0 && !earnFlickableData.isIconAvailable)return;

                    if(!earnFlickableData.currentItem.childDataContainer.currentItem.addIconOpacity)moveUp();
                    else earnFlickableData.isIconAvailable=true;
                }


                Keys.onDownPressed: {
                    moveDown();
                }

                Keys.onUpPressed: {
                    moveUp();
                }

                Keys.onRightPressed: {
                    setRightFocus();
                }

                Keys.onLeftPressed: {
                    setLeftFocus()
                }

                Keys.onReleased: {
                    if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                        viewHelper.addInfoXMLSource = dataXML;
                        viewHelper.addInfoscreenID = chilDataModel.get(childDataContainer.currentIndex).addScreenID;
                        viewHelper.addImagesPath = microappMain.childAppImages;

                        lastSeletedParentIndex = earnFlickableData.currentIndex;
                        lastSeletedChildIndex = earnFlickableData.currentItem.childDataContainer.currentIndex;

                        viewController.showScreenPopup(24);
                    }
                }
            }

            Image {
                id: falseIconImg;
                opacity: 0;
            }

            Component{
                id:childDataDelegate;
                Item {
                    id: childDataItem;
                    property alias addIconOpacity: addIcon.opacity;
                    property int childIndex: index;
                    property int childTextHeight: (headingText.paintedHeight + headingInfoText.paintedHeight + parseInt(qsTranslate('','lifemiles_info_earntxt_tm'),10) + parseInt(qsTranslate('','lifemiles_info_earntxt_vgap'),10))

                    height: (iconImg.paintedHeight > (childTextHeight)) ? iconImg.height : (childTextHeight);
                    width: iconImg.paintedWidth + qsTranslate('','lifemiles_info_earntxt_w');

                    function setChildFocus(){
                        addIcon.forceActiveFocus();
                    }
                    function getChildFocus(){
                        return addIcon.opacity;
                    }

                    Image {
                        id: iconImg;
                        anchors.top: parent.top;
                        anchors.left: parent.left;
                        //                        anchors.topMargin: qsTranslate('','lifemiles_info_icon_tm');
                        anchors.leftMargin: qsTranslate('','lifemiles_info_icon_lm');
                        source: childAppImages + chilDataModel.get(index).icon;
                    }

                    ViewText{
                        id:headingText;
                        anchors.top: iconImg.top;
                        anchors.topMargin: qsTranslate('','lifemiles_info_earntxt_tm');
                        anchors.left: iconImg.right;
                        anchors.leftMargin: qsTranslate('','lifemiles_info_earntxt_lm');
                        width: qsTranslate('','lifemiles_info_earntxt_w');
                        wrapMode: Text.Wrap;
                        elide: Text.ElideRight;
                        maximumLineCount:1;
                        varText: [ ((chilDataModel.get(index).headerText_cdata !== "") ? chilDataModel.get(index).headerText_cdata : chilDataModel.get(index).headerText),
                            configTool.config.lifemiles.screen.text_title,
                            qsTranslate('','lifemiles_info_fontsize')
                        ]
                    }

                    ViewText{
                        id:headingInfoText;
                        anchors.top: headingText.bottom;
                        anchors.topMargin: qsTranslate('','lifemiles_info_earntxt_vgap');
                        anchors.left: headingText.left;
                        width: qsTranslate('','lifemiles_info_earntxt_w');
                        wrapMode: Text.Wrap;
                        //                        elide: Text.ElideRight;
                        //                        maximumLineCount:1;
                        varText: [ (chilDataModel.get(index).headerInfoText_cdata !== "") ? chilDataModel.get(index).headerInfoText_cdata : chilDataModel.get(index).headerInfoText,
                                                                                            configTool.config.lifemiles.screen.text_title_info,
                                                                                            qsTranslate('','lifemiles_info_fontsize')
                        ]
                    }

                    Image {
                        id: addIcon;
                        anchors.left: headingInfoText.right;
                        anchors.leftMargin: qsTranslate('','lifemiles_info_plusbtn_rm');
                        anchors.verticalCenter: iconImg.verticalCenter;
                        opacity:(chilDataModel.get(index).addBtnVisibility == "true") ? 1 : 0;
                        source:(isFocus && childDataContainer.activeFocus && childDataContainer.currentIndex == index)? viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.earn_plusbtn_h : viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.earn_plusbtn_n;

                        MouseArea{
                            anchors.fill: parent;
                            onReleased: {
                                lastSeletedParentIndex = childlistItem.parentIndex;
                                lastSeletedChildIndex = index;
                                earnFlickableData.lastSelectedIndex(lastSeletedParentIndex,lastSeletedChildIndex);
                                viewHelper.addInfoXMLSource = dataXML;
                                viewHelper.addInfoscreenID = chilDataModel.get(index).addScreenID;
                                viewHelper.addImagesPath = microappMain.childAppImages;
                                viewController.showScreenPopup(24);
                            }
                        }
                    }
                    Component.onCompleted: {
                        delay.restart();

                    }
                }
            }
        }
    }

    Scroller{
        id:scroller
        height: qsTranslate('','lifemiles_scroll_short_h');
        width: qsTranslate('','lifemiles_scroll_short_w');
        anchors.left:earnFlickableData.right
        anchors.leftMargin: qsTranslate('','lifemiles_scroll_short_lm');
        anchors.top :earnFlickableData.top
        anchors.topMargin: qsTranslate('','lifemiles_scroll_short_tm')
        sliderbg:[qsTranslate('','lifemiles_scroll_short_base_margin'),qsTranslate('','lifemiles_scroll_short_w'),qsTranslate('','lifemiles_scroll_short_base_h'),configTool.config.lifemiles.screen.scrollbg]
        scrollerDot: configTool.config.lifemiles.screen.slider
        upArrowDetails:[configTool.config.lifemiles.screen.app_arwup_n, configTool.config.lifemiles.screen.app_arwup_h, configTool.config.lifemiles.screen.app_arwup_h ]
        downArrowDetails:[configTool.config.lifemiles.screen.app_arwdwn_n, configTool.config.lifemiles.screen.app_arwdwn_h, configTool.config.lifemiles.screen.app_arwdwn_h]
        targetData:earnFlickableData;
        elementSize:parseInt(earnFlickableData.height*0.75)
        onArrowPressed: {
            scrollDir = direction;
            console.log(" onArrowPressed scrollDir "+scrollDir)
            earnFlickableData.getplusIconIndex(direction)
        }
        Keys.onRightPressed: {
            appCloseBtn.forceActiveFocus();
        }
        Keys.onLeftPressed: {
            console.log(" Scroller onLeftPressed ")
            if(earnFlickableData.isIconAvailable){
                earnFlickableData.forceActiveFocus();
            }else{
                microappMain.menuContainer.forceActiveFocus();
            }
            //earnFlickableData.lastSelectedIndex(lastSeletedParentIndex,lastSeletedChildIndex);
        }
    }

}

