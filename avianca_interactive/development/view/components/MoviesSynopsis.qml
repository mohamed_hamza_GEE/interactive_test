// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
FocusScope{
    id:synopsisContentBtn
    anchors.fill: parent
    property alias playBtn: playBtn
    property alias synopsisContentFlickable: synopsisContentFlickable
    property double criticsScore: model.getValue(currentIndex,"critic_score")==undefined || model.getValue(currentIndex,"critic_score")==""?0:model.getValue(currentIndex,"critic_score")
    /**************************************************************************************************************************/
    onActiveFocusChanged: {
        if(!playBtn.focus && !resumeButton.focus && !restartButton.focus)
            playBtn.forceActiveFocus()
    }
    /**************************************************************************************************************************/
    Flickable{
        id : synopsisContentFlickable
        width:qsTranslate('','synopsis_movies_desc1_w')
        height:qsTranslate('','synopsis_movies_desc1_h')
        contentWidth: width
        contentHeight: synopsisSubDescIMG.paintedHeight
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','synopsis_movies_desc1_tm')
        anchors.left:parent.left
        anchors.leftMargin: qsTranslate('','synopsis_'+viewHelper.mapMenuTid(widgetList.getCidTidMainMenu()[1],true)+'_title_lm')
        clip:true
        interactive: contentHeight>height
        ViewText{
            id:synopsisSubDescIMG
            width:parent.width
            height:parent.height
            varText:[text,configTool.config[currentTemplateNode].synopsis.description1_color, qsTranslate('','synopsis_movies_desc1_fontsize')]
            wrapMode: Text.WordWrap
            text:model.count>0?model.getValue(currentIndex,"description"):""
        }
    }
    SimpleBorderBtn{
        id:textOverlay
        visible:slider.visible?(slider.downArrow.isDisable?false:true):false
        width:synopsisContentFlickable.width+(parseInt(qsTranslate('','common_scrollfade_extra_w'),10))
        normalBorder: [qsTranslate('','common_scrollfade_extra_margin')]
        anchors.bottom: synopsisContentFlickable.bottom
        anchors.horizontalCenter: synopsisContentFlickable.horizontalCenter
        source:  viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1],true)].synopsis.fading_image
    }
    ViewText{
        id:synopsisDetailsText
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','synopsis_movies_desc2_tm')
        anchors.left:parent.left
        anchors.leftMargin: qsTranslate('','synopsis_'+viewHelper.mapMenuTid(widgetList.getCidTidMainMenu()[1],true)+'_title_lm')
        width: qsTranslate('','synopsis_movies_desc2_w')
        height:  qsTranslate('','synopsis_movies_desc2_h')
        varText: [text,configTool.config[currentTemplateNode].synopsis.description2_color,qsTranslate('','synopsis_movies_desc2_fontsize')]
        text:synopsisDetails["details"]?synopsisDetails["details"]:""
        wrapMode: Text.Wrap
        textFormat :Text.StyledText
        maximumLineCount:2
//        font.bold:true
    }
    Row {
        id:filledStar
        anchors.top:parent.top
        anchors.left: parent.left
        anchors.topMargin: qsTranslate('','synopsis_movies_star_tm')
        anchors.leftMargin:  qsTranslate('','synopsis_movies_star_lm')
        spacing: qsTranslate('','synopsis_movies_star_gap')
        Repeater {
            model: 5
            Image {
                source:viewHelper.configToolImagePath+(((index+1)<=criticsScore)?configTool.config[currentTemplateNode].synopsis.star_icon_s:((criticsScore%1!=0 && (index+1) <=((criticsScore/1)+1))?configTool.config[currentTemplateNode].synopsis.halfstar_icon_s:configTool.config[currentTemplateNode].synopsis.star_icon_n))
            }
        }
    }
    Image{
        id:durationIcon
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','synopsis_movies_desc3_tm')
        anchors.left:parent.left
        anchors.leftMargin: qsTranslate('','synopsis_'+viewHelper.mapMenuTid(widgetList.getCidTidMainMenu()[1],true)+'_title_lm')
        source: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.duration_icon
    }
    ViewText{
        id:synopsisExtraDetailsText
        anchors.verticalCenter: durationIcon.verticalCenter
        anchors.left: durationIcon.right
        anchors.leftMargin:qsTranslate('','synopsis_movies_desc3_gap')
        width: qsTranslate('','synopsis_movies_desc3_w')
        height: qsTranslate('','synopsis_movies_desc3_h')
        varText: [text,configTool.config[currentTemplateNode].synopsis.description3_color,qsTranslate('','synopsis_movies_desc3_fontsize')]
        text:synopsisDetails["extraDetails"]?synopsisDetails["extraDetails"]:""
    }
    SimpleNavBrdrBtn{
        id:trailerBtn
        visible: model.getValue(currentIndex,"trailer_mid")<=0?false:true
        width: qsTranslate('','synopsis_trailer_w')
        height: qsTranslate('','synopsis_trailer_h')
        btnTextWidth:qsTranslate('','synopsis_trailer_w')
        anchors.left:parent.left
        anchors.leftMargin: qsTranslate('','synopsis_trailer_lm')
        anchors.bottom:parent.bottom
        anchors.bottomMargin:  qsTranslate('','synopsis_trailer_bm')
        normalImg:   viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.trailor_btn_n
        highlightImg:    viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.trailor_btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.trailor_btn_p
        normalBorder: [qsTranslate('','synopsis_trailer_margin')]
        highlightBorder: [qsTranslate('','synopsis_trailer_margin')]
        isHighlight: activeFocus
        nav:[trailerBtn,"",playBtn.visible?playBtn:restartButton,""]
        onPadReleased: if(dir=="left"){extremeEnds("left")}
        onEnteredReleased: {
            viewHelper.storeTrailerParentMid(model.getValue(currentIndex,"mid"))
            loadVod("play","trailer")
        }
        SimpleButton{
            anchors.centerIn: trailerBtn
//            isHighlight: trailerBtn.activeFocus
            normImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_n
            highlightimg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_h
            pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_p
        }
    }
    ViewText{
        id:trailerText
        visible: model.getValue(currentIndex,"trailer_mid")<=0?false:true
        width:qsTranslate('','synopsis_trailer_w')
        anchors.left:trailerBtn.right
        anchors.leftMargin: qsTranslate('','synopsis_trailer_gap')
        anchors.verticalCenter:trailerBtn.verticalCenter
        varText: [text,configTool.config[currentTemplateNode].synopsis.watch_trailer_color,qsTranslate('','synopsis_trailer_fontsize')]
        text:configTool.language.movies.trailer
    }
    SimpleNavBrdrBtn{
        id:playBtn
        property string textColor: playBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(playBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
        visible: !widgetList.video.vodScreen.visible?!(pif.getMidInfo(model.getValue(currentIndex,"mid"),'elapseTime')>viewHelper.resumeSecs):true
        width: qsTranslate('','synopsis_play_w')
        height: qsTranslate('','synopsis_play_h')
        onVisibleChanged: {
            if(synopsisContentBtn.activeFocus){
                if(!visible) resumeButton.forceActiveFocus()
                else playBtn.forceActiveFocus()
            }
            else {
                if(!visible) resumeButton.focus=true
                else playBtn.focus=true
            }
        }
        anchors.right: parent.right
        anchors.rightMargin:qsTranslate('','synopsis_play_rm')
        anchors.bottom: parent.bottom
        anchors.bottomMargin: qsTranslate('','synopsis_play_bm')
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
        normalBorder: [qsTranslate('','synopsis_play_margin')]
        highlightBorder: [qsTranslate('','synopsis_play_margin')]
        nav:[trailerBtn.visible?trailerBtn:"",!slider.downArrow.isDisable?slider.downArrow:((!slider.upArrow.isDisable)?slider.upArrow:synopsisCloseBtn),playBtn,""]
        onPadReleased:{
            if(dir=="right"){extremeEnds("right")}
        }
        onNoItemFound: if(dir=="left" && !trailerBtn.visible){extremeEnds("left")}
        onEnteredReleased:{
            viewHelper.playingCategory="movies";
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory})
            loadVod("play","movies")
        }
        SimpleButton{
            anchors.left: parent.left
            anchors.leftMargin:qsTranslate('','synopsis_play_icon_lm')
            anchors.verticalCenter: parent.verticalCenter
//            isHighlight: playBtn.activeFocus
            normImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_n
            highlightimg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_h
            pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_p
        }
        ViewText{
            anchors.left: playBtn.left
            anchors.leftMargin:qsTranslate('','synopsis_play_text_lm')
            anchors.verticalCenter: parent.verticalCenter
            width:qsTranslate('','synopsis_play_textw')
            varText: [text,playBtn.textColor,qsTranslate('','synopsis_play_fontsize')]
            text:configTool.language.movies.play
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
    SimpleNavBrdrBtn{
        id:restartButton
        property string textColor: restartButton.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(restartButton.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
        width: qsTranslate('','synopsis_resume_w')
        height: qsTranslate('','synopsis_resume_h')
        visible:   !playBtn.visible
        btnTextWidth:parseInt(qsTranslate('',"synopsis_resume_w"),10);
        buttonText: [configTool.language.movies.restart,restartButton.textColor,qsTranslate('','synopsis_resume_fontsize')]
        anchors.right: resumeButton.left
        anchors.rightMargin: qsTranslate('','synopsis_resume_gap')
        anchors.bottom: parent.bottom
        anchors.bottomMargin: qsTranslate('','synopsis_resume_bm')
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
        normalBorder: [qsTranslate('','synopsis_resume_margin')]
        highlightBorder: [qsTranslate('','synopsis_resume_margin')]
        nav:[trailerBtn.visible?trailerBtn:"",!slider.downArrow.isDisable?slider.downArrow:((!slider.upArrow.isDisable)?slider.upArrow:synopsisCloseBtn),resumeButton,""]
        onNoItemFound:  if(dir=="left" && !trailerBtn.visible){extremeEnds("left")}
        onEnteredReleased:loadVod("restart","movies")
    }
    SimpleNavBrdrBtn{
        id:resumeButton
        property string textColor: resumeButton.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(resumeButton.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
        width: qsTranslate('','synopsis_resume_w')
        height: qsTranslate('','synopsis_resume_h')
        btnTextWidth:parseInt(qsTranslate('',"synopsis_resume_w"),10);
        buttonText: [configTool.language.movies.resume,resumeButton.textColor,qsTranslate('','synopsis_resume_fontsize')]
        visible:   !playBtn.visible
        anchors.right: parent.right
        anchors.rightMargin:  qsTranslate('','synopsis_resume_rm')
        anchors.bottom: parent.bottom
        anchors.bottomMargin: qsTranslate('','synopsis_resume_bm')
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
        normalBorder: [qsTranslate('','synopsis_resume_margin')]
        highlightBorder: [qsTranslate('','synopsis_resume_margin')]
        nav:[restartButton,!slider.downArrow.isDisable?slider.downArrow:((!slider.upArrow.isDisable)?slider.upArrow:synopsisCloseBtn),resumeButton,""]
        onPadReleased: if(dir=="right"){extremeEnds("right")}
        onEnteredReleased:  loadVod("resume","movies")
    }
}
