import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    id: appNews;
    width: core.width;
    height: core.height

    property string currAppImages: microAppDir+currAppName+"/images/"
    property string xmlModelSource: microAppDir+currAppName+"/"+currAppName+"_data.xml"
    property int selectedIndex: -1;

    function init() {
        core.log("In AppNews.qml | init");
        newsInfoData.source = xmlModelSource;
        newsListData.source = xmlModelSource;
        selectedIndex = -1;
    }

    function reload(){
        core.log("AppNews | reload | called");
        newsInfoData.source = xmlModelSource;
        newsListData.source = xmlModelSource;
        newsInfoData.reload();
        newsListData.reload();
    }

    function setTitleText(obj,Dmodel,ind){
        obj.text = (Dmodel.get(ind).title_cdata !== "") ? Dmodel.get(ind).title_cdata : Dmodel.get(ind).title;
        obj.color = configTool.config.aviancaapp.screen.text_header_title;
        obj.font.pixelSize = qsTranslate('','aviapp_header_title_fontsize')
    }
    function setMainMenuTitleText(obj,Dmodel,ind){
        obj.text = (Dmodel.get(ind).menuTitle_cdata != "") ? Dmodel.get(ind).menuTitle_cdata : Dmodel.get(ind).menuTitle;
        obj.color = configTool.config.aviancaapp.screen.text_header_menu;
        obj.font.pixelSize = qsTranslate('','aviapp_header_menu_fontsize');
    }
    function loadAirlineInfoVOD(dModel) {
        viewHelper.setMediaListingModel(dModel);
        viewHelper.setMediaListingIndex(0)
        viewHelper.storeTrailerParentMid(dModel.getValue(0,"mid"));
        viewHelper.initializeVOD("restart","trailer","",false,true);
    }
    function pageUp(){
        if(childAppList.contentY>0){
            if((childAppList.contentY-childAppList.height)>0){
                childAppList.contentY = childAppList.contentY-childAppList.height
            }else{
                childAppList.contentY = 0
            }
            var firstIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            childAppList.currentIndex = firstIndex;
            childAppList.positionViewAtIndex(firstIndex,ListView.Beginning)
        }
        childAppList.forceActiveFocus()
    }
    function pageDown(){
        if(childAppList.contentY<(childAppList.contentHeight-childAppList.height)){
            if((childAppList.contentY+childAppList.height)<(childAppList.contentHeight-childAppList.height)){
                childAppList.contentY = childAppList.contentY+childAppList.height
            }else{
                childAppList.contentY = childAppList.contentY+((childAppList.contentHeight-childAppList.height)-childAppList.contentY)
            }
            var lastIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            childAppList.currentIndex = lastIndex+1;
            childAppList.positionViewAtIndex(lastIndex,ListView.Beginning)
        }
        childAppList.forceActiveFocus()
    }
    function languageUpdate(){
        selectedIndex = -1;
        newsListData.reload()
        newsInfoData.reload()
    }

    XmlListModel {
        id: newsInfoData;
        query: "/application/screen";

        XmlRole { name:"title"; query: "text1/"+langCode+"/@_value/string()" }
        XmlRole { name:"title_cdata"; query: "text1/"+langCode+"/string()" }
        XmlRole { name:"menuTitle"; query: "text2/"+langCode+"/@_value/string()" }
        XmlRole { name:"menuTitle_cdata"; query: "text2/"+langCode+"/string()" }

        onCountChanged: {
            setTitleText(newsBackground.headerTitle,newsInfoData,0);
            setMainMenuTitleText(newsBackground.headerMenuTitle,newsInfoData,0);
        }
    }
    XmlListModel {
        id: newsListData;
        query: "/application/screen/list1/listItem";

        XmlRole { name:"news_list_text"; query: "text4/"+langCode+"/@_value/string()" }
        XmlRole { name:"news_list_text_cdata"; query: "text4/"+langCode+"/string()" }
        XmlRole { name:"news_list_header_text"; query: "text3/"+langCode+"/@_value/string()" }
        XmlRole { name:"news_list_header_text_cdata"; query: "text3/"+langCode+"/string()" }
        XmlRole { name:"news_header_text"; query: "text5/"+langCode+"/@_value/string()" }
        XmlRole { name:"news_header_text_cdata"; query: "text5/"+langCode+"/string()" }
        XmlRole { name:"news_info_text"; query: "text6/"+langCode+"/@_value/string()" }
        XmlRole { name:"news_info_text_cdata"; query: "text6/"+langCode+"/string()" }
        XmlRole { name:"news_image"; query: "image2/@_value/string()" }
        XmlRole { name:"news_video"; query: "text7/"+langCode+"/@_value/string()" }
        onCountChanged: {           
            childAppList.model="";
            childAppList.model = newsListData;            
        }
    }

    AviancaAppBg {
        id: newsBackground;

        appBackgroundImage: configTool.config.aviancaapp.screen.app_bg;//"app_bg.png";
        appHeaderImage: configTool.config.aviancaapp.screen.app_header;//"app_header.png";
        appSideMenuPanelImage: configTool.config.aviancaapp.screen.sidemenu_panel;//"sidemenu_panel.png";
        appHeaderTitle: (newsInfoData.get(selectedIndex).title_cdata != "")?newsInfoData.get(selectedIndex).title_cdata : newsInfoData.get(selectedIndex).title;
        appHeaderMenuTitle: (newsInfoData.get(selectedIndex).menuTitle_cdata != "" )?newsInfoData.get(selectedIndex).menuTitle_cdata : newsInfoData.get(selectedIndex).menuTitle;
        Keys.onDownPressed: {
            if(upArrowContainer.visible){
                upArrowImage.forceActiveFocus()
            }else{
                childAppList.forceActiveFocus();
            }
        }
    }   

    Flickable {
        id: newsFlickAbleData;
        height: qsTranslate('','aviapp_airport_img_h');
        width: qsTranslate('','aviapp_airport_img_w');
        x: qsTranslate('','aviapp_airport_title_x')
        contentWidth: width;
        contentHeight: infoText.y +infoText.paintedHeight+fadeImage.paintedHeight;
        anchors.top:parent.top;
        anchors.topMargin: newsBackground.headerImage.height;
        clip:true
        interactive: contentHeight<=height?false:true


        FocusScope{
            id:newsDataContainer;
            height: parent.height;
            width: parent.width;
            ViewText {
                id: headerText;
                anchors.left: parent.left;
                anchors.top:parent.top;
                anchors.topMargin: qsTranslate('','aviapp_airport_title_tm')
                width: qsTranslate('','aviapp_airport_title_w')
                wrapMode: Text.Wrap;
                textFormat: Text.StyledText;
                elide: Text.ElideRight;
                maximumLineCount:1;
                varText: [
                    (newsListData.get(selectedIndex).news_header_text_cdata !== "")?newsListData.get(selectedIndex).news_header_text_cdata : newsListData.get(selectedIndex).news_header_text,
                                                                                     configTool.config.aviancaapp.screen.text_airport_title,
                                                                                     qsTranslate('','aviapp_airport_title_fontsize')
                ]
            }
            Image {
                id: newsImage;
                anchors.top: headerText.bottom; anchors.topMargin: qsTranslate('','aviapp_airport_img_tm');
                anchors.left: headerText.left;
                source: currAppImages + newsListData.get(selectedIndex).news_image;
                SimpleButton {
                    id: playBtn;
                    anchors.right: parent.right; anchors.rightMargin: qsTranslate('','aviapp_news_play_rm');
                    anchors.bottom: parent.bottom; anchors.bottomMargin:qsTranslate('','aviapp_news_play_bm');
                    isHighlight: activeFocus
                    visible: (newsListData.get(selectedIndex).news_video != "") ? true :  false;
                    normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.history_play_n;
                    highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.history_play_h;
                    pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.history_play_h;
                    Keys.onRightPressed: {
                        if(scroller.visible){
                            scroller.forceActiveFocus();
                        }else {
                            childAppList.forceActiveFocus();
                        }
                    }
                    onEnteredReleased: {
                        viewHelper.isMicroAppVideo = true;
                        var params=["media_attr_enum","",newsListData.get(selectedIndex).news_video];
                        dataController.getMediaByFieldName(params,loadAirlineInfoVOD);
                    }
                }
                MouseArea {
                    id:newsImageMouseArea;
                    anchors.fill: parent;
                    onReleased: {
                        viewHelper.isMicroAppVideo = true;
                        var params=["media_attr_enum","",newsListData.get(selectedIndex).news_video];
                        dataController.getMediaByFieldName(params,loadAirlineInfoVOD);
                    }
                }
                Keys.onReleased: {
                    if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                        viewHelper.isMicroAppVideo = true;
                        var params=["media_attr_enum","",newsListData.get(selectedIndex).news_video];
                        dataController.getMediaByFieldName(params,loadAirlineInfoVOD);
                    }
                }
                Keys.onLeftPressed: {
                    if(scroller.visible){
                        scroller.forceActiveFocus();
                    }else {
                        childAppList.forceActiveFocus();
                    }
                }
            }
            Rectangle {
                id:imageBorder;
                anchors.fill: newsImage;
                height: newsImage.height; width: newsImage.width;
                color:"transparent";
                border.width: qsTranslate('','aviapp_airport_img_border_w');
                border.color: qsTranslate('','aviapp_airport_img_border_color');
            }

            ViewText{
                id: infoTextHeader;
                width: qsTranslate('','aviapp_airport_txt_w');
                anchors.top:newsImage.bottom;
                anchors.topMargin: qsTranslate('','aviapp_airport_txt_tm');
                anchors.left: newsImage.left;
                wrapMode: Text.Wrap;
                textFormat: Text.StyledText;
                elide: Text.ElideRight;
                font.bold: true;
                maximumLineCount:1;
                varText: [
                    (newsListData.get(selectedIndex).news_list_header_text_cdata !== "")? newsListData.get(selectedIndex).news_list_header_text_cdata : newsListData.get(selectedIndex).news_list_header_text,
                                                                                          configTool.config.aviancaapp.screen.text_airport_txt,
                                                                                          qsTranslate('','aviapp_airport_txt_fontsize')
                ]
            }
            ViewText{
                id: infoText;
                anchors.top:infoTextHeader.bottom;
                anchors.topMargin: qsTranslate('','aviapp_airport_txt_vgap');
                anchors.left: newsImage.left;
                horizontalAlignment: Text.AlignLeft;
                //                textFormat: Text.StyledText;
                width: qsTranslate('','aviapp_airport_txt_w');
                wrapMode: Text.Wrap;
                varText: [
                    (newsListData.get(selectedIndex).news_info_text_cdata !== "") ? newsListData.get(selectedIndex).news_info_text_cdata : newsListData.get(selectedIndex).news_info_text,
                                                                                    configTool.config.aviancaapp.screen.text_airport_txt,
                                                                                    qsTranslate('','aviapp_airport_txt_fontsize')]
            }
        }
        Behavior on contentY {
            NumberAnimation{duration: 50}
        }
    }
    Image {
        id: fadeImage;
        anchors.left: newsFlickAbleData.left;
        anchors.bottom: newsFlickAbleData.bottom;
        anchors.bottomMargin: qsTranslate('','aviapp_airport_img_vgap');
        width: newsFlickAbleData.width;
        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_fadeimage;
    }
    ListView {
        id: childAppList;
        width: qsTranslate('','aviapp_sidemenu_itemw')
        height: qsTranslate('','aviapp_sidemenu_itemh')*6;
        anchors.top:parent.top;
        anchors.topMargin: newsBackground.headerImage.height;
        anchors.right: parent.right;
        //        model:newsListData;
        delegate: infoListDelegate;
//        highlightRangeMode:ListView.StrictlyEnforceRange;
//        preferredHighlightBegin: (childAppList.contentY > 0) ? upArrowContainer.height : 0;
//        preferredHighlightEnd: childAppList.height;
        //        highlight: listHighlight;
        boundsBehavior: ListView.StopAtBounds
        clip:true;
        interactive: count>6?true:false;
        onMovementEnded: {
            childAppList.currentIndex=childAppList.indexAt(childAppList.contentX,childAppList.contentY+parseInt(qsTranslate('','aviapp_sidemenu_itemh'),10)+10)
        }
        onCountChanged: {
            selectedIndex=0;
            childAppList.currentIndex = 0;
            childAppList.forceActiveFocus();
        }
        Keys.onUpPressed: {
            var firstIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            if(childAppList.currentIndex == 0 ){
                newsBackground.closeBtn.forceActiveFocus();
            }else if(upArrowContainer.visible && (childAppList.currentIndex == firstIndex+1)){
                upArrowImage.forceActiveFocus();
            }else{
                childAppList.decrementCurrentIndex();
            }
        }
        Keys.onDownPressed: {
            var lastIndex = childAppList.indexAt(childAppList.contentX+10,(childAppList.contentY+childAppList.height-10))
            if(newsBackground.closeBtn.activeFocus){
                childAppList.forceActiveFocus();
            }else if((downArrowContainer.visible && currentIndex == lastIndex) ){
                downArrowImage.forceActiveFocus();
            }else{
                childAppList.incrementCurrentIndex();
            }
        }
        Keys.onLeftPressed: {
            if(scroller.visible){
                scroller.forceActiveFocus();
            }else{
                if(playBtn.visible) playBtn.forceActiveFocus();
            }
        }
    }

    Component{
        id: infoListDelegate;
        FocusScope{
            id: screenlist;
            height: qsTranslate('','aviapp_sidemenu_itemh');
            width: qsTranslate('','aviapp_sidemenu_itemw');
            opacity: ((Math.floor(childAppList.contentY/screenlist.height) == index) && childAppList.contentY > 0) ? 0 : 1;
            Image {
                id: listImage;
                anchors.fill: parent;
                source: selectedIndex===index?viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_s
                          :viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_n;
            }
            Rectangle{
                id: screenHighlightlist;
                height: qsTranslate('','aviapp_sidemenu_itemh');
                width: qsTranslate('','aviapp_sidemenu_itemw');
                color: "transparent"
                visible: (index==childAppList.currentIndex && childAppList.activeFocus)
                Image {
                    id: listHighlightImage;
                    anchors.fill: parent;
                    source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_h;
                }
            }

            ViewText {
                id:listText;
                width: qsTranslate('','aviapp_sidemenu_text_w');
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left; anchors.leftMargin: qsTranslate('','aviapp_sidemenu_text_lm');
//                opacity: (Math.floor((childAppList.contentY)/screenlist.height == index && (index > 0)) && childAppList.contentY > 0) ? false : true;
                //                textFormat: Text.StyledText;
                elide: Text.ElideRight;
                maximumLineCount:2;
                wrapMode: Text.Wrap;
                varText: [
                    (newsListData.get(index).news_list_text_cdata !== "")?newsListData.get(index).news_list_text_cdata : newsListData.get(index).news_list_text,
                                                                           selectedIndex===index?configTool.config.aviancaapp.screen.text_sidemenu_s:configTool.config.aviancaapp.screen.text_sidemenu_n,
                                                                                                  qsTranslate('','aviapp_sidemenu_fontsize')
                ]
            }


            MouseArea{
                id:infoListMouseArea;
                anchors.fill: parent;
                onPressed: {
                    childAppList.currentIndex=index;
                    childAppList.forceActiveFocus();
                }
                onClicked: {
                    selectedIndex=index;
                    childAppList.currentIndex=index;
                    newsFlickAbleData.contentY = 0;
                    setMainMenuTitleText(newsBackground.headerMenuTitle,newsInfoData,selectedIndex);
                }
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    selectedIndex=index;
                    childAppList.currentIndex=index;
                    newsFlickAbleData.contentY = 0;
                    setMainMenuTitleText(newsBackground.headerMenuTitle,newsInfoData,selectedIndex);
                }
            }
        }
    }

    //    Component{
    //        id:listHighlight;
    //        Rectangle{
    //            id: screenHighlightlist;
    //            height: qsTranslate('','aviapp_sidemenu_itemh');
    //            width: qsTranslate('','aviapp_sidemenu_itemw');
    //            color: "transparent"
    ////            visible: (index==childAppList.currentIndex && childAppList.activeFocus)

    //            Image {
    //                id: listHighlightImage;
    //                anchors.fill: parent;
    //                source: (childAppList.activeFocus)?viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_h:"";
    //            }
    //        }
    //    }


    Rectangle {
        id: upArrowContainer;
        anchors.top: childAppList.top;
        anchors.horizontalCenter: childAppList.horizontalCenter;
        height: qsTranslate('','aviapp_sidemenu_itemh');
        width: qsTranslate('','aviapp_sidemenu_itemw');
        color: "transparent"
        visible: (childAppList.contentY > 0) ? true : false;

        SimpleButton {
            id: upArrowImage;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','aviapp_sidemenu_arrow_tm');
            anchors.horizontalCenter: parent.horizontalCenter;
            isHighlight: activeFocus;
            normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_n;
            highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_h;
            pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_h;
        }
        onVisibleChanged: {
            if(!(upArrowContainer.visible)){
                childAppList.currentIndex =0;
                childAppList.forceActiveFocus();
            }
        }
        MouseArea{
            id:upArrowMouseArea;
            anchors.fill: parent;
            onClicked:  {
                pageUp()
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                pageUp()
            }
        }
        Keys.onDownPressed: {
            childAppList.forceActiveFocus();
        }
        Keys.onUpPressed: {
            newsBackground.closeBtn.forceActiveFocus();
        }
    }
    Rectangle {
        id: downArrowContainer;
        anchors.top: childAppList.bottom;
        anchors.horizontalCenter: childAppList.horizontalCenter;
        height: qsTranslate('','aviapp_sidemenu_itemh');
        width: qsTranslate('','aviapp_sidemenu_itemw');
        visible:(childAppList.contentY+childAppList.height < childAppList.contentHeight); //((childAppList.count >= 6 ) && (!(childAppList.currentIndex == (childAppList.count - 1)))) ? true : false;
        color: "transparent"

        onVisibleChanged: {
            if(!(downArrowContainer.visible)){
                childAppList.currentIndex = (childAppList.count -1);
                childAppList.forceActiveFocus();
            }
        }
        SimpleButton {
            id: downArrowImage;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','aviapp_sidemenu_arrow_tm');
            anchors.horizontalCenter: parent.horizontalCenter;
            isHighlight: activeFocus;
            normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_n;
            highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_h;
            pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_h;
        }
        MouseArea{
            id:downArrowMouseArea;
            anchors.fill: parent;
            onClicked:  {
                pageDown();
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                pageDown();
            }
        }
        Keys.onUpPressed: {
            childAppList.forceActiveFocus();
        }
    }
    Scroller{
        id:scroller
        height: qsTranslate('','aviapp_airport_scroll_h');
        width: qsTranslate('','aviapp_airport_scroll_w');
        anchors.left:newsFlickAbleData.right;
        anchors.leftMargin:qsTranslate('','aviapp_airport_scroll_lm');
        anchors.verticalCenter: newsFlickAbleData.verticalCenter;
        sliderbg:[qsTranslate('','aviapp_airport_scroll_base_margin'),qsTranslate('','aviapp_airport_scroll_w'),qsTranslate('','aviapp_airport_scroll_base_h'),configTool.config.aviancaapp.screen.scrollbg]
        scrollerDot: configTool.config.aviancaapp.screen.slider
        upArrowDetails:[configTool.config.aviancaapp.screen.app_arwup_n, configTool.config.aviancaapp.screen.app_arwup_h, configTool.config.aviancaapp.screen.app_arwup_h ]
        downArrowDetails:[configTool.config.aviancaapp.screen.app_arwdwn_n, configTool.config.aviancaapp.screen.app_arwdwn_h, configTool.config.aviancaapp.screen.app_arwdwn_h]

        targetData:newsFlickAbleData;

        Keys.onRightPressed: {
            if(downArrowContainer.visible){
                downArrowContainer.forceActiveFocus();
            }else{
                childAppList.forceActiveFocus();
            }
        }
        Keys.onLeftPressed: {
            if(playBtn.visible) playBtn.forceActiveFocus();
        }
    }
}
