import QtQuick 1.1
FocusScope{
    id:scrollerFS
    property Flickable targetData
    property alias upArrow: upArrow
    property alias downArrow: downArrow
    property variant sliderbg
    property variant scrollerDot:[]
    property variant upArrowDetails:[]
    property variant downArrowDetails:[]
    property int offset: qsTranslate('','common_scrollbtn_margin')
    signal sliderNavigation(string direction)
    signal arrowPressed(string direction)
    property int targetConWeight:targetData?targetData.contentWidth:0;
    clip:true;
    visible:(targetConWeight>targetData.width)
    /**************************************************************************************************************************/
    onTargetDataChanged:{
        downArrow.focus=true;
    }
    /**************************************************************************************************************************/
    function nextPage(){
        if((targetData.contentX+targetData.width)>(targetConWeight-targetData.width)){
            targetData.contentX=(targetData.contentX+(targetConWeight-(targetData.contentX+targetData.width)));
        }else{
            targetData.contentX = targetData.contentX+targetData.width;
        }
    }
    function prevPage(){
        if((targetData.contentX-targetData.width)<0){
            targetData.contentX=0;
        }else{
            targetData.contentX = targetData.contentX-targetData.width;
        }
    }
    /**************************************************************************************************************************/
    BorderImage {
        id: slider_bg
        border.left: sliderbg[0]; border.top:sliderbg[0]
        border.right:sliderbg[0]; border.bottom: sliderbg[0]
        width: sliderbg[1]
        clip:true;
        source:viewHelper.configToolImagePath+sliderbg[3]
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter;
        BorderImage {
            id: slider_main
            source: viewHelper.configToolImagePath+ scrollerDot
            border.left: scrollerFS.offset; border.top:scrollerFS.offset
            border.right: scrollerFS.offset; border.bottom: scrollerFS.offset
            smooth:true

            anchors.verticalCenter: slider_bg.verticalCenter
            x:{targetData?Math.ceil((targetData.contentX/targetConWeight)*parent.width):0}
            width: {
                if(visible){
                    if(targetData==undefined)return;
                    if(targetData.width<targetConWeight)
                        return (targetData.width/targetConWeight)*slider_bg.width
                    else
                        return 0
                }
            }

            BorderImage {
                id: slider_dot
                source:viewController.settings.assetImagePath+"slider_dot1.png"
                smooth: true
                width: (slider_main.width<sourceSize.width)?sourceSize.width/2:sourceSize.width
                border.left: 5; border.top: 5
                border.right: 5; border.bottom: 5
                anchors.centerIn: parent
                opacity:0;
            }
        }
    }
    SimpleNavButton{
        id:upArrow
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+upArrowDetails[0]
        highlightimg: viewHelper.configToolImagePath+upArrowDetails[1]
        pressedImg: viewHelper.configToolImagePath+upArrowDetails[2]
        isDisable:  targetData!=null?(targetData.contentX<=0?true:false) :true
        anchors.left:parent.left
        anchors.verticalCenter: slider_bg.verticalCenter
        nav:["","","",!downArrow.isDisable?downArrow:""]
        onPadReleased:{if(dir!="down")sliderNavigation(dir)}
        onNoItemFound: {sliderNavigation(dir)}
        onEnteredReleased: {prevPage();arrowPressed("up");}
        onIsDisableChanged: {
            if(activeFocus && isDisable)
                downArrow.forceActiveFocus()
            else if(isDisable) downArrow.focus=true
        }
    }
    SimpleNavButton{
        id:downArrow
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+downArrowDetails[0]
        highlightimg: viewHelper.configToolImagePath+downArrowDetails[1]
        pressedImg: viewHelper.configToolImagePath+downArrowDetails[2]
        isDisable:  targetData!=null?((targetData.contentX>=targetConWeight-targetData.width )?true:false):true
        anchors.right:parent.right;
        anchors.verticalCenter: slider_bg.verticalCenter
        nav:["",!upArrow.isDisable?upArrow:"",downArrow,""]
        onPadReleased:{if(dir!="up")sliderNavigation(dir)}
        onNoItemFound: {sliderNavigation(dir)}
        onEnteredReleased:{ nextPage();arrowPressed("down");}
        onIsDisableChanged: {
            if(activeFocus && isDisable )
                upArrow.forceActiveFocus()
            else if(isDisable) upArrow.focus=true
        }
    }
}
