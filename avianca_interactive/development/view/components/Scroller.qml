import QtQuick 1.1
FocusScope{
    id:scrollerFS
    property Flickable targetData
    property alias upArrow: upArrow
    property alias downArrow: downArrow
    property variant sliderbg
    property variant scrollerDot:[]
    property variant upArrowDetails:[]
    property variant downArrowDetails:[]
    property int offset: qsTranslate('','common_scrollbtn_margin')
    property int targetConHeight:targetData?targetData.contentHeight:0;
    property int elementSize :(targetData)?targetData.height:0;
    signal sliderNavigation(string direction)
    signal arrowPressed(string direction)
    clip:true;
    visible:(targetConHeight>targetData.height)
    /**************************************************************************************************************************/
    onTargetDataChanged:{
        downArrow.focus=true;
    }
    /**************************************************************************************************************************/
    function nextPage(){
        if((targetData.contentY+targetData.height)>(targetConHeight-elementSize)){
            targetData.contentY=(targetData.contentY+(targetConHeight-(targetData.contentY+targetData.height)));
        }else{
            targetData.contentY = targetData.contentY+elementSize;
        }
        arrowPressed("down");
    }
    function prevPage(){
        if((targetData.contentY-elementSize)<0  ){
            targetData.contentY=0;
        }else{
            targetData.contentY = targetData.contentY-elementSize;
            if(targetData.contentY <0)targetData.contentY =0
        }
    }
    /**************************************************************************************************************************/
    BorderImage {
        id: slider_bg
        border.left: sliderbg[0]; border.top:sliderbg[0]
        border.right:sliderbg[0]; border.bottom: sliderbg[0]
        height: sliderbg[2]
        clip:true;
        source:viewHelper.configToolImagePath+sliderbg[3]
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter;
        BorderImage {
            id: slider_main
            source: viewHelper.configToolImagePath+ scrollerDot
            border.left: scrollerFS.offset; border.top:scrollerFS.offset
            border.right: scrollerFS.offset; border.bottom: scrollerFS.offset
            smooth:true
            anchors.horizontalCenter: slider_bg.horizontalCenter
            y:{targetData?Math.ceil((targetData.contentY/(targetConHeight-targetData.height))*(parent.height- (slider_main.height ))):0}
            height: {
                if(visible){
                    if(targetData==undefined)return;
                    if(targetData.height<targetConHeight){
                        if(((targetData.height/targetConHeight)*slider_bg.height) < sourceSize.height) return sourceSize.height
                        else  return (targetData.height/targetConHeight)*slider_bg.height
                    }
                    else
                        return 0
                }
            }
            BorderImage {
                id: slider_dot
                source:viewController.settings.assetImagePath+"slider_dot.png"
                height: (slider_main.height<sourceSize.height)?sourceSize.height/2:sourceSize.height
                border.left: 5; border.top: 5
                border.right: 5; border.bottom: 5
                anchors.centerIn: parent
                opacity:(current.template_id=="movies"||current.template_id=="tv"||current.template_id=="music")?1:0;
            }
        }
    }
    SimpleNavButton{
        id:upArrow
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+upArrowDetails[0]
        highlightimg: viewHelper.configToolImagePath+upArrowDetails[1]
        pressedImg: viewHelper.configToolImagePath+upArrowDetails[2]

        isDisable:  targetData!=null?(targetData.contentY<=0?true:false) :true
        anchors.top:parent.top
        anchors.horizontalCenter: slider_bg.horizontalCenter
        nav:["","","",!downArrow.isDisable?downArrow:""]
        onPadReleased:{if(dir!="down")sliderNavigation(dir)}
        onNoItemFound: {sliderNavigation(dir)}
        onEnteredReleased: {prevPage();arrowPressed("up");}
        onIsDisableChanged: {
            if(activeFocus && isDisable)
                downArrow.forceActiveFocus()
            else if(isDisable && !downArrow.isDisable) downArrow.focus=true
        }
    }
    SimpleNavButton{
        id:downArrow
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+downArrowDetails[0]
        highlightimg: viewHelper.configToolImagePath+downArrowDetails[1]
        pressedImg: viewHelper.configToolImagePath+downArrowDetails[2]
        //disableImg:viewHelper.configToolImagePath+downArrowDetails[3]
        isDisable:  targetData!=null?((targetData.contentY>=targetConHeight-targetData.height )?true:false):true
        anchors.bottom:parent.bottom;
        anchors.horizontalCenter: slider_bg.horizontalCenter
        nav:["",!upArrow.isDisable?upArrow:"",downArrow,""]
        onPadReleased:{if(dir!="up")sliderNavigation(dir)}
        onNoItemFound: {sliderNavigation(dir)}
        onEnteredReleased:{ nextPage();}
        onIsDisableChanged: {
            if(activeFocus && isDisable )
                upArrow.forceActiveFocus()
            else if(isDisable && !upArrow.isDisable) upArrow.focus=true
        }
    }
}
