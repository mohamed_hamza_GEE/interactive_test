import QtQuick 1.1
import Panasonic.Pif 1.0

FocusScope {
    id:playlistSynopsis

    property variant configValue:configTool.config.music.playlist;
    property variant configTag:configTool.config.music.tracklist;
    property variant labelTag:core.configTool.language.music;
    property int playlistId;
    property string appearance:"tracklist";
    SimpleModel{id:stack }
    /**************************************************************************************************************************/
    Connections{
        id:catchScreenPop
        target:null
        property variant button:removeFromplaylisttbn;
        onScreenPopupClosed:{
            catchScreenPop.target=null;
            if(retArray==1)return;
            if(catchScreenPop.button.select){
                catchScreenPop.button.select();
            }
        }
    }
    Connections{
        target:(visible && pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)?pif.sharedModelServerInterface:null
        onSigDataReceived:{
            core.debug("In playlist Synopsis "+api+" params.playListRefresh "+params.playListRefresh)
            if(params.playListRefresh){
                core.debug("playlistView.currentIndex "+playlistView.currentIndex)
                core.debug("params.index "+params.index)
                var ind=params.index
                if(ind==0)ind=1
                core.debug("params.index |after if check "+ind)
                if((playlistView.currentIndex+1)==ind){
                    playlistView.selIndex=-1
                    playlistView.select()
                }
            }
        }

    }

    Connections{
        target:(playlistSynopsis.visible)?dataController.mediaServices:null;
        onSigMidBlockChanged: checkList.restart();
        onSigMidPaidStatusChanged:checkList.restart();

    }

    Connections{
        target:(playlistSynopsis.visible)?core.pif:null;
        onSeatRatingChanged: checkList.restart();
    }



    /**************************************************************************************************************************/
    function init(){
        viewHelper.clearPlaylistStack();
        playlistView.select();


    }


    function clearOnExit(){
        playlistView.selIndex=-1;
        playlistempty.visible=false;
        stack.clear();
        viewHelper.clearPlaylistStack();

    }
    function getPlaylistData(ind){
        return " ("+pif.aodPlaylist.getTotalTracks(((ind+1)*-1))+"/99)"

    }
    function checkPartOfStack(){
        for(var i=0;i<trackView.model.count;i++){
            var tmid=trackView.model.getValue(i,"mid");
            console.log(">>>>>>>>>>>>>>>>>>>>>> pif.aodPlaylist.isTrackInPlaylist(tmid) "+pif.aodPlaylist.isTrackInPlaylist(tmid)+" viewHelper.inPlaylistStack(tmid) "+viewHelper.inPlaylistStack(tmid)+" i= "+i)
            if(pif.aodPlaylist.isTrackInPlaylist(tmid) && viewHelper.inPlaylistStack(tmid)){
                addall.cstate=1;
            }else{
                addall.cstate=0;
                break;
            }
        }
    }
    function playlistModelReady(dmodel){


        trackView.currentIndex=0;
        trackView.model=dmodel;
        trackView.forceActiveFocus();
        core.debug("trackView.model | playlistModelReady "+trackView.model.count)

    }
    function backBtnPressed(){
        viewHelper.homeAction();
    }
    /**************************************************************************************************************************/
    Image{
        id:panel;
        source:viewHelper.configToolImagePath+(playlistempty.visible?configValue["playlist_empty_panel"]:configValue["playlist_panel"]);
        anchors.horizontalCenter:parent.horizontalCenter;
        y:qsTranslate('',"playlist_panel_tm");
    }
    ViewText{
        id:nowplaying_txt
        width:qsTranslate('','playlist_nowplay_text_w');
        height:qsTranslate('','playlist_nowplay_text_h');
        anchors.left:panel.left
        anchors.leftMargin:qsTranslate('','playlist_nowplay_text_lm');
        anchors.top:panel.top
        anchors.topMargin:qsTranslate('','playlist_nowplay_text_tm');
        visible:((getCidTidForSubCat()[1]=="playlist")&& !(viewHelper.trackStop) && !playlistempty.visible)
        varText:[core.configTool.language.music.now_playing+" : "+viewHelper.audioPlayer.midTitle,configValue["now_play_text"],qsTranslate('','playlist_nowplay_text_fontsize')]
        elide:Text.ElideRight;
        maximumLineCount:1;
        wrapMode: "WordWrap"
    }


    Column{
        id:playlistView;
        anchors.left:panel.left;
        anchors.top: panel.top;
        anchors.topMargin:qsTranslate('','playlist_playlist_tm')
        anchors.leftMargin:qsTranslate('','playlist_playlist_lm')
        width:qsTranslate('','playlist_playlist_w');

        property variant playlistName:[labelTag["playlist1"],labelTag["playlist2"],labelTag["playlist3"]]
        property variant emptyTitle:[labelTag["playlist1_empty_title"],labelTag["playlist2_empty_title"],labelTag["playlist3_empty_title"]]
        property int currentIndex:0;
        property int selIndex:-1;

        Keys.onUpPressed:{
            if(currentIndex==0){
                console.log("Keys.onUpPressed <>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                addall.forceActiveFocus()
            }else{
                currentIndex--;
            }
        }

        Keys.onRightPressed:{
            if(trackView.visible){
                trackView.forceActiveFocus();
            }else{
                goAlbm.forceActiveFocus();
            }
        }

        Keys.onDownPressed:{
            if(currentIndex!=2){
                currentIndex++;
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                select();
            }
        }

        Repeater{
            id:playlistRepeat
            model:3;
            Item{
                id:playlist;
                width:qsTranslate('','playlist_playlist_w');
                height:qsTranslate('','playlist_playlist_h');
                Image{
                    id:high1
                    source:viewHelper.configToolImagePath+configValue["playlist_list_highlight"];
                    anchors.bottom:parent.bottom;
                    anchors.bottomMargin:qsTranslate('','playlist_tracks_high_bm')
                    height:qsTranslate('','playlist_tracks_high_h');
                    width:parent.width;
                    opacity:(playlistView.currentIndex==index && playlistView.activeFocus)?1:0
                }
                ViewText{
                    x:qsTranslate('','playlist_playlisttext_lm');
                    width:qsTranslate('','playlist_playlisttext_w');
                    height:qsTranslate('','playlist_playlisttext_h');
                    anchors.verticalCenter:parent.verticalCenter;
                    varText:[playlistView.playlistName[index]+getPlaylistData(index),(index==playlistView.selIndex)?core.configTool.config.music.tracklist["title_color"]:core.configTool.config.music.tracklist["description1_color"],qsTranslate('','artist_sectiontext_fontsize')]
                    elide:Text.ElideRight;
                }
                Rectangle{
                    height:qsTranslate('','common_line_h');
                    width:parent.width;
                    anchors.bottom:parent.bottom;
                    color:core.configTool.config.music.tracklist.description1_color;
                }
                MouseArea{
                    anchors.fill:high1
                    onClicked:{
                        playlistView.forceActiveFocus();
                        playlistView.currentIndex=index;
                        playlistView.select()

                    }
                }

            }
        }


        function select(){
            //            if(viewHelper.usbTrackPlayed)
            //                viewHelper.stopUsbMp3()
            if(selIndex==currentIndex)return;
            stack.clear();
            viewHelper.clearPlaylistStack()
            selIndex = currentIndex;
            playlistId = (selIndex+1)*-1
            addall.cstate=false
            if(pif.aodPlaylist.getTotalTracks(playlistId)==0){
                viewHelper.setHelpTemplate("playlist_empty")
                core.debug("playlist count is zero")
                playlistempty.visible=true;
                goAlbm.forceActiveFocus();
                trackView.model=viewHelper.blankModel;

            }else{
                core.debug("playlist count is more than 0")
                viewHelper.setHelpTemplate("playlist")
                playlistempty.visible=false;
                var params=[pif.aodPlaylist.getAllPlaylistMids(playlistId),""];
                dataController.getPlaylistData(params,playlistModelReady);
            }
            trackView.allRefresh=false
            trackView.allRefresh=true
        }
    }
    Item{
        id:playlistempty;
        visible:false;
        anchors.top:panel.top;
        anchors.left:panel.left;
        onVisibleChanged: {
            if(visible)viewHelper.setHelpTemplate("playlist_empty")
            else viewHelper.setHelpTemplate("playlist")
        }

        ViewText{
            id:emptyTitle;
            anchors.top:parent.top;
            anchors.left:parent.left;
            anchors.leftMargin:qsTranslate('','playlist_empty_title_lm')
            anchors.topMargin:qsTranslate('','playlist_empty_title_tm')
            width:qsTranslate('','playlist_empty_title_w')
            height: qsTranslate('','playlist_empty_title_h')
            varText:[playlistView.emptyTitle[playlistView.selIndex],configTag["description1_color"],qsTranslate('','playlist_empty_title_fontsize')]

        }
        Flickable{
            id:descArea
            anchors.top:emptyTitle.bottom;
            anchors.left:emptyTitle.left;
            anchors.topMargin:qsTranslate('','playlist_empty_desc_tm')
            width:qsTranslate('','playlist_empty_desc_w')
            height: qsTranslate('','playlist_empty_desc_h')
            contentHeight:emptyDesc.paintedHeight;
            flickableDirection:Flickable.VerticalFlick;
            interactive:(scrollerDesc.visible)
            clip:true;
            Text{
                id:emptyDesc;
                property string imgstring:"✔"
                text:(labelTag["playlist_empty_desc"]).replace("[image]",imgstring);
                color:configTag["description1_color"]
                font.pixelSize:qsTranslate('','playlist_empty_desc_fontsize')
                lineHeight:qsTranslate('','playlist_empty_desc_lh')
                lineHeightMode:Text.FixedHeight;
                font.family:viewHelper.adultFont;
                wrapMode: "WordWrap";
                textFormat:"StyledText"
                width:parent.width;
                height:parent.height;
            }

        }

        SimpleNavBrdrBtn{
            id:goAlbm
            anchors.top: descArea.bottom;
            anchors.topMargin: qsTranslate('','playlist_albumbtn_tm')
            anchors.horizontalCenter:descArea.horizontalCenter;
            normalImg: viewHelper.configToolImagePath+configValue["remove_button_n"];
            highlightImg:viewHelper.configToolImagePath+configValue["remove_button_h"];
            pressedImg:viewHelper.configToolImagePath+configValue["remove_button_p"];
            normalBorder:[qsTranslate('','playlist_albumbtn_margin')]
            highlightBorder: normalBorder;
            height:qsTranslate('',"playlist_albumbtn_h");
            width:qsTranslate('',"playlist_albumbtn_w");
            buttonText: [labelTag["go_to_albums"],(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','playlist_albumbtn_text_fontsize')]
            nav:[playlistView,subCatobj,"",""]
            onEnteredReleased:{
                findAlbumAndJump();
            }
            onNoItemFound:{
                if(dir=="right"){
                    if(scrollerDesc.visible){
                        scrollerDesc.forceActiveFocus();
                    }
                }
            }
        }
        //        Component.onCompleted:  goAlbm.btnText.anchors.verticalCenterOffset=parseInt(qsTranslate('',"playlist_button_text_tm"),10);
    }
    Scroller{
        id:scrollerDesc
        targetData:descArea;
        sliderbg:[qsTranslate('','playlist_empty_scroll_base_margin'),qsTranslate('','playlist_empty_scroll_w'),qsTranslate('','playlist_empty_scroll_base_h'),configValue["scroll_base"]]
        scrollerDot:configValue["scroll_button"]
        upArrowDetails:[configValue["up_arrow_n"],configValue["up_arrow_h"],configValue["up_arrow_p"]]
        downArrowDetails:[configValue["dn_arrow_n"],configValue["dn_arrow_h"],configValue["dn_arrow_p"]]
        anchors.right:panel.right;
        anchors.rightMargin:qsTranslate('','playlist_empty_scroll_rm')
        height: qsTranslate('','playlist_empty_scroll_h');
        width: qsTranslate('','playlist_empty_scroll_w');
        anchors.verticalCenter: panel.verticalCenter;
        opacity:(playlistempty.visible)?1:0;
        onSliderNavigation:{
            if(direction=="up"){
                subCatobj.forceActiveFocus();

            }else if(direction=="left"){
                goAlbm.forceActiveFocus();
            }
        }
    }
    Rectangle{
        height:qsTranslate('','common_line_h')
        width:trackView.width;
        color:configTag["description1_color"];
        anchors.bottom: trackView.top;
        anchors.left:trackView.left;
        visible:trackView.visible
    }
    VerticalView{
        id:trackView;
        height:qsTranslate('','playlist_trackview_h');
        width:qsTranslate('','playlist_trackview_w');

        anchors.top:panel.top;
        anchors.topMargin:qsTranslate('','playlist_trackview_tm');
        anchors.left:panel.left;
        anchors.leftMargin:qsTranslate('','playlist_trackview_lm');
        snapMode:ListView.SnapToItem;
        boundsBehavior:ListView.StopAtBounds;
        preferredHighlightBegin:0;
        preferredHighlightEnd: height;
        highlightRangeMode: "StrictlyEnforceRange"
        clip:true;
        model:0;
        property int currCol:1;
        property int maxCol:2;
        property bool allRefresh:true;
        visible:(!playlistempty.visible && model.count>0)
        onModelChanged: {
            core.debug("onModelChanged : "+model.count)
        }
        delegate: Item{
            id:del
            height:qsTranslate('','playlist_tracks_h');
            width:qsTranslate('','playlist_tracks_w');

            property bool currRefresh:true;
            function getMidState(){return pif.aodPlaylist.isTrackInPlaylist(mid);}
            function getMid(){return mid;}

            function updateCheckBox(){
                currRefresh=false;
                currRefresh=true;

            }


            Image{
                id:high
                source:viewHelper.configToolImagePath+configTag["listing_highlight"];
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('','playlist_tracks_high_bm')
                height:qsTranslate('','playlist_tracks_high_h');
                width:qsTranslate('','playlist_tracks_high_w');
                opacity:(trackView.currentIndex==index && trackView.activeFocus && trackView.currCol==1 )?1:0

            }
            ViewText{
                id:tracktitle
                x:qsTranslate('','playlist_tracktitle_lm');
                height:qsTranslate('','playlist_tracktitle_h');
                width:qsTranslate('','playlist_tracktitle_w');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[title,configTag["description1_color"],qsTranslate('','playlist_tracktitle_fontsize')]
                elide:Text.ElideRight

            }

            ViewText{
                id:artText
                anchors.left:tracktitle.right;
                anchors.leftMargin:qsTranslate('','playlist_duration_lm');
                height:qsTranslate('','playlist_trackartist_h');
                width:qsTranslate('','playlist_trackartist_w');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[artist,configTag["description1_color"],qsTranslate('','playlist_duration_fontsize')]
                maximumLineCount:1;
                wrapMode:Text.WordWrap;
                elide:Text.ElideRight;
                clip:true;
            }
            Image{
                id:plicon
                anchors.left:artText.right;
                anchors.leftMargin:qsTranslate('','playlist_nowplaying_lm');
                anchors.verticalCenter:parent.verticalCenter;
                source:viewHelper.configToolImagePath+configTag["now_playing_icon"];
                visible:(pif.aod.getPlayingMid()==mid)

            }
            ViewText{
                anchors.right:ccbox.left;
                //anchors.leftMargin:qsTranslate('','playlist_duration_lm');
                height:qsTranslate('','playlist_tracktitle_h');
                width:qsTranslate('','playlist_duration_w');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[coreHelper.convertSecToMSLeadZero(coreHelper.convertHMSToSec(duration)),configTag["description1_color"],qsTranslate('','playlist_duration_fontsize')]
            }

            ToggleButton{
                id:ccbox
                normImg1:viewHelper.configToolImagePath+configTag["tick_box_n"];
                normImg2: viewHelper.configToolImagePath+configTag["check_box_n"];
                highImg1:viewHelper.configToolImagePath+configTag["tick_box_h"];
                highImg2: viewHelper.configToolImagePath+configTag["check_box_h"];
                pressImg1: viewHelper.configToolImagePath+configTag["check_box_p"];
                pressImg2:viewHelper.configToolImagePath+configTag["tick_box_p"];
                anchors.right:parent.right;
                anchors.verticalCenter:parent.verticalCenter;
                anchors.rightMargin:qsTranslate('','playlist_checkbox_rm');
                isHighlight:(trackView.currentIndex==index && trackView.activeFocus && trackView.currCol==2 )
                cstate:(currRefresh && trackView.allRefresh)?(viewHelper.inPlaylistStack(mid)):false;
                MouseArea{
                    anchors.fill:parent;
                    onClicked:{
                        trackView.currCol=2;
                        trackView.currentIndex=index;
                        trackView.select();
                    }
                }
            }
            Rectangle{
                height:qsTranslate('','common_line_h')
                width:parent.width;
                color:tracktitle.color;
                anchors.bottom: parent.bottom;
            }
            MouseArea{
                anchors.fill:high;
                onClicked:{
                    trackView.currCol=1;
                    trackView.currentIndex=index;
                    trackView.select();
                }
            }
        }
        Keys.onUpPressed:{
            if(currentItem.y==contentY){
                //                subCatobj.forceActiveFocus();
                addall.forceActiveFocus()
            }else{
                decrementCurrentIndex();
            }
        }
        Keys.onDownPressed:{
            if((currentItem.y+currentItem.height)>=(contentY+height) || (currentIndex==count-1)){
//                if (viewHelper.trackStop){
//                }else{
                    playAllSongs.forceActiveFocus()
//                }
            }else{
                incrementCurrentIndex();
            }

        }
        Keys.onRightPressed:{
            if(currCol==maxCol){
                if(count<5){
                }else{
                    scroller.forceActiveFocus();
                }
            }else{
                currCol++;
            }

        }
        Keys.onLeftPressed:{
            if(currCol==1){
                playlistView.forceActiveFocus();
            }else{
                currCol--;
            }

        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                select();
            }
        }

        function select(){
            if(viewHelper.usbTrackPlayed && currCol!=2)
                viewHelper.stopUsbMp3()
            trackView.forceActiveFocus();
            var tmid=trackView.currentItem.getMid();
            if(currCol==1){
                if(pif.aod.getPlayingAlbumId() != playlistId||pif.lastSelectedAudio!=pif.aod){
                    var params;
                    params = {
                        "amid" : playlistId,
                        "duration" : trackView.model.getValue(trackView.currentIndex,"duration"),
                        "elapseTimeFormat" : 6,
                        "cid" :widgetList.getCidTidMainMenu()[0],
                        "playIndex" : trackView.currentIndex,
                        "elapseTime" : 0,
                        "mediaType": "audioAggregate",
                        "repeatType":pif.aod.getMediaRepeat(),
                        "playType":2,
                        "shuffle":pif.aod.getMediaShuffle()
                    };
                    pif.aod.playByModel(pif.aodPlaylist.getPlaylistModel(playlistId),params)
                }else{
                    pif.lastSelectedAudio.setPlayingIndex(trackView.currentIndex);
                }
                viewHelper.trackStop=false;
                viewHelper.playingCategory="music";
                if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                    pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory,genre:false})
                viewHelper.genreSongIndex=-1;
            }else{
                if(viewHelper.inPlaylistStack(tmid)){
                    stack.removeByProperty("index",trackView.currentIndex,1);
                    viewHelper.removeFromPlaylistStack(tmid)
                }else{
                    viewHelper.addToPlaylistStack(tmid);
                    stack.append({"index":trackView.currentIndex,"trackmid":tmid})

                }
                checkPartOfStack()
                trackView.currentItem.updateCheckBox()

            }
            pif.paxus.playlistPlayLog()
        }
    }
    BorderImage{
        visible:trackView.visible
        source:viewHelper.configToolImagePath+configTag["fading_image"];
        anchors.bottom:trackView.bottom;
        anchors.horizontalCenter: trackView.horizontalCenter;
        border.left:qsTranslate('','common_scrollfade_margin')
        border.top:qsTranslate('','common_scrollfade_margin')
        border.right:qsTranslate('','common_scrollfade_margin')
        border.bottom:qsTranslate('','common_scrollfade_margin')
        width:(parseInt(qsTranslate('','playlist_trackview_w'),10)+parseInt(qsTranslate('','common_scrollfade_extra_w'),10));
        opacity:((trackView.currentItem.y+trackView.currentItem.height)>=(trackView.contentY+trackView.height)||(trackView.currentIndex==trackView.count-1))?0:1;
    }
    Scroller{
        id:scroller
        targetData:trackView;
        sliderbg:[qsTranslate('','playlist_scrollcont_base_margin'),qsTranslate('','playlist_scrollcont_w'),qsTranslate('','playlist_scrollcont_base_h'),configTag["scroll_base"]]
        scrollerDot:configTag["scroll_button"]
        upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
        downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
        anchors.left:trackView.right;
        anchors.leftMargin:qsTranslate('','playlist_scrollcont_lm')
        height: qsTranslate('','playlist_scrollcont_h');
        width: qsTranslate('','playlist_scrollcont_w');
        anchors.verticalCenter: trackView.verticalCenter;
        onSliderNavigation:{
            //console.log("pravin  direction  "+direction)
            if(direction=="up"){
                subCatobj.forceActiveFocus();
            }else if(direction=="down"){
                if(removeFromplaylisttbn.isDisable){
                    if(!viewHelper.trackStop)   {
                        musicControl.forceActiveFocus();
                    }
                }else{
                    //console.log(" U r here @@@@@@@@@@@@@@@")
                    /*if(clearRemoveFS.visible)clearRemoveFS.forceActiveFocus();
                    else*/ playAllSongs.forceActiveFocus();
                }
            }else if(direction=="left"){
                trackView.forceActiveFocus();
            }else if(direction=="right"){
                //change to next tracklist
            }
        }

    }
    ListView{id:empty}
    FocusScope{
        id:clearRemoveFS
        height:qsTranslate('',"playlist_remove_button_h");
        width:qsTranslate('','playlist_remove_button_w')
        anchors.top:panel.top;
        anchors.right:panel.right;
        anchors.topMargin:qsTranslate('',"playlist_remove_button_tm")
        anchors.rightMargin: qsTranslate('',"playlist_remove_button_rm")
        visible:trackView.visible
        SimpleNavBrdrBtn{
            id:removeFromplaylisttbn
            normalImg: viewHelper.configToolImagePath+configValue["remove_button_n"];
            highlightImg:viewHelper.configToolImagePath+configValue["remove_button_h"];
            pressedImg:viewHelper.configToolImagePath+configValue["remove_button_p"];
            //  disableImg:viewHelper.configToolImagePath+configValue["remove_button_d"];
            height:qsTranslate('',"playlist_remove_button_h");
            width:qsTranslate('',"playlist_remove_button_w");
            btnTextWidth:parseInt(qsTranslate('',"playlist_remove_button_textw"),10);
            buttonText: [labelTag["remove_from_playlist"],(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','playlist_button_text_fontsize')]
            anchors.right:parent.right;
            anchors.top:parent.top;
            normalBorder:[qsTranslate('',"playlist_remove_button_margin")];
            highlightBorder:[qsTranslate('',"playlist_remove_button_margin")];
            elide:Text.ElideRight
            visible:trackView.visible
            nav: ["",trackView,addall,trackView]
            //isDisable:(stack.count==0)
            //            onIsDisableChanged: if(isDisable)removeFromplaylisttbn.focus=true
            //            opacity:(isDisable)?0:1;
            onEnteredReleased:{
                if(stack.count==0)return;
                trackView.forceActiveFocus();
                viewController.showScreenPopup(6);
                catchScreenPop.button = removeFromplaylisttbn;
                catchScreenPop.target = viewController;
            }
            function select(){
                var flag=false;
                for(var i=0;i<stack.count;i++){
                    var tmid = stack.getValue(i,"trackmid");
                    var ind =  stack.getValue(i,"index");
                    if(viewHelper.inPlaylistStack(tmid)){
                        pif.aodPlaylist.removeTrack(tmid);
                        trackView.model.removeByProperty("mid",tmid,1);
                    }
                    if(ind==0){flag=true}
                }
                if(flag){
                    var a = trackView.model;
                    trackView.model=viewHelper.blankModel;
                    trackView.model=a;
                }
                stack.clear();
                viewHelper.clearPlaylistStack()
                addall.cstate=false
                if(pif.aodPlaylist.getTotalTracks(playlistId)==0){
                    playlistempty.visible=true;
                    playlistView.forceActiveFocus();
                }else{
                    trackView.forceActiveFocus();
                }
            }
            onNoItemFound:{
                if(dir=="right"){


                }
            }
        }

        BorderImage{
            id:disable_remove_songs_btn
            visible:(stack.count==0)
            source:viewHelper.configToolImagePath+configValue["remove_button_d"];
            anchors.right:parent.right;
            anchors.top:parent.top;
            border.left:qsTranslate('',"playlist_remove_button_margin")
            border.top:qsTranslate('',"playlist_remove_button_margin")
            border.right:qsTranslate('',"playlist_remove_button_margin")
            border.bottom:qsTranslate('',"playlist_remove_button_margin")
            height:qsTranslate('',"playlist_remove_button_h");
            width:qsTranslate('',"playlist_remove_button_w");
            MouseArea{
                anchors.fill: parent
                onClicked: {

                }
            }


            ViewText{
                id:remove_songs_txt
                width:parseInt(qsTranslate('',"playlist_remove_button_textw"),10);
                visible:(disable_remove_songs_btn.visible)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter:  parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                varText: [labelTag["remove_from_playlist"],configTag["btn_text_n"],qsTranslate('','playlist_button_text_fontsize')]
                elide:Text.ElideRight;
                maximumLineCount:1;
                wrapMode: "WordWrap"
            }
        }

        Component.onCompleted:{
            //            removeFromplaylisttbn.btnText.anchors.verticalCenterOffset=parseInt(qsTranslate('',"playlist_button_text_tm"),10);
            //            clearplaylistbtn. btnText.anchors.verticalCenterOffset=parseInt(qsTranslate('',"playlist_button_text_tm"),10);
        }
    }
    SimpleNavBrdrBtn{
        id:playAllSongs
        normalImg: viewHelper.configToolImagePath+configTool.config.movies.synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config.movies.synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config.movies.synopsis.btn_p
        height:qsTranslate('',"playlist_buttons_h");
        width:qsTranslate('',"playlist_buttons_w");
        btnTextWidth:parseInt(qsTranslate('',"playlist_button_text_w"),10);
        buttonText: [configTool.language.music.play_all_songs,(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','playlist_button_text_fontsize')]
        anchors.right:panel.right;
        anchors.rightMargin: qsTranslate('',"playlist_buttons_rm")
        anchors.bottom:panel.bottom;
        anchors.bottomMargin:qsTranslate('',"playlist_buttons_bm")
        normalBorder:[qsTranslate('',"playlist_buttons_margin")];
        highlightBorder:[qsTranslate('',"playlist_buttons_margin")];
        elide:Text.ElideRight
        nav: [viewHelper.trackStop?"":musicControl,trackView,"",""]
        visible:!playlistempty.visible;
      
        onEnteredReleased:{
            trackView.currentIndex=0;
            if(viewHelper.usbTrackPlayed)
                viewHelper.stopUsbMp3()
            trackView.forceActiveFocus();
            var tmid=trackView.currentItem.getMid();
            if(pif.aod.getPlayingAlbumId() != playlistId||pif.lastSelectedAudio!=pif.aod){
                var params;
                params = {
                    "amid" : playlistId,
                    "duration" : trackView.model.getValue(trackView.currentIndex,"duration"),
                    "elapseTimeFormat" : 6,
                    "cid" :widgetList.getCidTidMainMenu()[0],
                    "playIndex" : trackView.currentIndex,
                    "elapseTime" : 0,
                    "mediaType": "audioAggregate",
                    "repeatType":pif.aod.getMediaRepeat(),
                    "playType":2,
                    "shuffle":pif.aod.getMediaShuffle()
                };
                pif.aod.playByModel(pif.aodPlaylist.getPlaylistModel(playlistId),params)
            }else{
                pif.lastSelectedAudio.setPlayingIndex(trackView.currentIndex);
            }
            viewHelper.trackStop=false;
            viewHelper.playingCategory="music";
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory,genre:false})
            viewHelper.genreSongIndex=-1;

            pif.paxus.playlistPlayLog()
        }

        onNoItemFound:{
            if(dir=="right"){}
        }
    }

    ToggleButton{
        id:addall;
        normImg2:viewHelper.configToolImagePath+configTag["check_box_all_n"];
        normImg1: viewHelper.configToolImagePath+configTag["tickbox_all_n"];
        highImg2:viewHelper.configToolImagePath+configTag["check_box_all_h"];
        highImg1: viewHelper.configToolImagePath+configTag["tickbox_all_h"];
        pressImg2: viewHelper.configToolImagePath+configTag["check_box_all_p"];
        pressImg1:viewHelper.configToolImagePath+configTag["tickbox_all_p"];
        anchors.right:panel.right;
        anchors.rightMargin:qsTranslate('','playlist_remove_chekbox_rm');
        anchors.top:panel.top;
        anchors.topMargin: qsTranslate('',"playlist_remove_chekbox_tm");
        isHighlight:activeFocus;
        visible:(currentTemplateNode!="kids_music_listing" && trackView.visible)
        onEnteredReleased:{
            select()
        }

        function select(){
            core.debug(" Select function works : isDisable : "+isDisable)
            core.debug(" Select function works : cstate : "+cstate)
            if(isDisable)return;
            if(cstate==false){

                for(var i=0;i<trackView.model.count;i++){
                    var tmid = trackView.model.getValue(i,"mid");
                    if(!viewHelper.inPlaylistStack(tmid)){
                        viewHelper.addToPlaylistStack(tmid);
                        stack.append({"index":i,"trackmid":tmid})
                    }
                }
            }else{

                stack.clear();
                viewHelper.clearPlaylistStack()

            }
            trackView.allRefresh=false;
            trackView.allRefresh=true;
        }

        Keys.onDownPressed:{
            trackView.forceActiveFocus();
        }
        Keys.onLeftPressed:{
            if(!disable_remove_songs_btn.visible)
                removeFromplaylisttbn.forceActiveFocus()
        }
        Keys.onUpPressed:{
            subCatobj.forceActiveFocus();
        }
    }




    MusicControls{
        id:musicControl
        anchors.bottom:panel.bottom;
        anchors.left:panel.left;
        height:tmpId=="playlist"?qsTranslate('','playlist_control_cont_h'):qsTranslate('','tracklist_control_cont_h')
        width:qsTranslate('','tracklist_control_cont_w')
        anchors.leftMargin:qsTranslate('','playlist_control_cont_lm');
        upFocus:[trackView,trackView,"",playlistView]
        nowPlayW:(noButton)?qsTranslate('','playlist_nowplay_text_w'):width;
        visible:(trackView.visible)
        tmpId:"playlist";
        noButton: (!removeFromplaylisttbn.isDisable)
        onExtremeEnds:{
            if(dir=="right")playAllSongs.forceActiveFocus()
        }
    }

    Timer{
        id:checkList;
        interval:100;
        onTriggered:{
            if(pif.aodPlaylist.getTotalTracks(playlistId)!=trackView.count){
                playlistView.selIndex=-1;
                playlistView.select();
            }
        }
    }
}
