import Qt 4.7
SimpleButton{
    property bool isSelected: false
    property variant nav:[]

    signal padReleased(string dir)
    signal noItemFound(string dir)
    isHighlight:activeFocus
    Keys.onLeftPressed:{
        if(nav[0]!=undefined && nav[0]!=""){
            if(nav[0].visible)
                nav[0].forceActiveFocus()
            padReleased("left")
        }
        else noItemFound("left")
        event.accepted=false
    }
    Keys.onUpPressed: {
        if(nav[1]!=undefined && nav[1]!=""){
            if(nav[1].visible)nav[1].forceActiveFocus()
            padReleased("up")
        }
        else noItemFound("up")
        event.accepted=false
    }
    Keys.onRightPressed:{
        if(nav[2]!=undefined && nav[2]!=""){
            if(nav[2].visible)nav[2].forceActiveFocus()
            padReleased("right")
        }
        else noItemFound("right")
        event.accepted=false
    }
    Keys.onDownPressed:  {
        if(nav[3]!=undefined && nav[3]!=""){
            if(nav[3].visible)
                nav[3].forceActiveFocus()
            padReleased("down")
        }
        else noItemFound("down")
        event.accepted=false
    }
}


