// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5 
import QtQuick 1.1
import "../components/"

FocusScope{
    id:popupImg
    property variant configTag:configTool.config.seatchat.invite_popup;
    property int itemsPerRow: 3;
    property int maxSeatNoLimit: 4
    property bool isError: false
    property bool isPopup: false
    signal upPressed();
    property string inputTextInvite: ""

    function init(){

        dummyInviteModel.clear()
        //        sendInvite_button.isDisable=sendInviteIsdisable()
        if(systempopupId==12){
            add_button.visible=true;

        }else{
            add_button.visible=false;
        }
        sendInviteIsdisable()
    }
    function setFocus(){
        textBox.forceActiveFocus();
    }
    function sendInviteIsdisable (){
        if(systempopupId==12){
            if(dummyInviteModel.count>0){
                return false
            }else{
                return true
            }
        }else {
            if(textBox.text.length>0)
                return false
            else return true
        }

    }
    function clearAll(){
        isError=false;
        textBox.text="";
    }
    function addToSeatNum(val){
        if(isError)isError=false;
        var txt = textBox.text;
        if(txt.length == maxSeatNoLimit) return
        textBox.text += val;

    }
    function deleteNumber(){
        if(isError)isError=false;
        var n = textBox.text
        textBox.text = n.substring(0,n.length-1)
        textBox.cursorPosition = n.length-1
    }
    function sendInvite(){
        core.info("Dialpad | sendInvite | viewHelper.cSessionID = "+viewHelper.cSessionID)
        var a = textBox.text.toUpperCase();
        core.info("Dialpad | sendInvite | core.coreHelper.validateSeatNoForApps(a) = "+core.coreHelper.validateSeatNoForApps(a))
        if(textBox.text!="" &&  (core.pc==1 || pif.seatChat.validateSeatNum(a,viewHelper.cSessionID)) && a != pif.getSeatNumber().toUpperCase() && core.coreHelper.validateSeatNoForApps(a)){
            var seatNo=new Array();
            seatNo[0]=a.toUpperCase()
            viewHelper.tSeatNo=a
            core.info("Dialpad | before sendInvite | viewHelper.tSeatNo = "+viewHelper.tSeatNo)
            viewHelper.invitationSent++;
            pif.seatChat.sendInvitation(viewHelper.cSessionID,seatNo)
            viewController.hideScreenPopup();
            viewController.showScreenPopup(17);

        }else{
            textBox.forceActiveFocus()
            isError=true;

        }
    }

    function addInvites(){
        var tempseatno=textBox.text.toUpperCase()
        if(tempseatno== pif.getSeatNumber())return;
        else {
            dummyInviteModel.append({"seatnum":tempseatno});
            console.log("<<<<<<<<<<<< seatnum = "+tempseatno)
            textBox.text=""
        }
    }
    function forMutipleInvite(){
        var tempArr = new Array();
        for(var i = 0; i < dummyInviteModel.count; i++){
            console.debug("____________dummyInviteModel.get(i).seat_________________: "+dummyInviteModel.get(i).seatnum)
            tempArr[i] =(dummyInviteModel.get(i).seatnum).toUpperCase();
            viewHelper.tSeatNo=tempArr
            console.debug("____________tempArr[i]_________________: "+tempArr[i]+"viewHelper.tSeatNo = "+viewHelper.tSeatNo)
        }
        pif.seatChat.sendInvitation(viewHelper.cSessionID,viewHelper.tSeatNo)
        viewController.hideScreenPopup();
        viewController.showScreenPopup(19);
    }


    function checkLocalInviteModel(seatNo){
        core.log(">>>>>>>>>>>>>>>checkLocalInviteModel seatNo = "+seatNo)
        for(var indx = 0 ;indx<dummyInviteModel.count;indx++){
            core.log(">>>>>>>>>>>>>>>checkLocalInviteModel chatSetting.dummyInvite.get(indx).seat = "+dummyInviteModel.get(indx).seatnum)
            if(dummyInviteModel.get(indx).seatnum == seatNo) return true;
        }
        core.log(">>>>>>>>>>>>>>>checkLocalInviteModel seatNo = "+seatNo+" does not exist")
        return false;
    }

    Rectangle{
        id:black_bg
        color:"#000000"
        opacity:0.4;
        anchors.fill:parent

        MouseArea{
            anchors.fill: parent
            onClicked:{

            }
        }
    }

    Rectangle{
        id:invite_bg
        width:qsTranslate('','seatchat_invitepop_bg_w')
        height:qsTranslate('','seatchat_invitepop_bg_h')
        anchors.horizontalCenter: black_bg.horizontalCenter
        radius:parseInt(qsTranslate('','seatchat_invitepop_bg_radius'),10)
        color:configTag["bg_color"];
        anchors.top:black_bg.top
        anchors.topMargin:qsTranslate('','seatchat_invitepop_bg_tm')
        MouseArea{
            anchors.fill: parent
            onClicked:{

            }
        }
    }
    SimpleNavButton{
        id:close_button
        anchors.right: invite_bg.right
        anchors.rightMargin: qsTranslate('','seatchat_close_btn_rm')
        anchors.top:invite_bg.top
        anchors.topMargin:qsTranslate('','seatchat_close_btn_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTag["close_btn_n"];
        highlightimg:  viewHelper.configToolImagePath+configTag["close_btn_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
        nav:["","","",textBox]
        onEnteredReleased:{viewController.hideScreenPopup();}

    }

    ViewText{
        id:invite_Title
        anchors.top: invite_bg.top;
        anchors.left:invite_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_invite_title_lm')
        anchors.topMargin:qsTranslate('','seatchat_invite_title_tm')
        width:qsTranslate('','seatchat_invite_title_w')
        height:qsTranslate('','seatchat_invite_title_h')
        varText:[configTool.language.seatchat.enter_seat_title,configTag["title_color"],qsTranslate('',"seatchat_invite_title_fontsize")]
    }

    ViewText{
        id:invite_SubTitle
        anchors.top: invite_Title.bottom;
        anchors.left:invite_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_invite_title_lm')
        anchors.topMargin:qsTranslate('','seatchat_invite_msg_tm')
        width:qsTranslate('','seatchat_invite_msg_w')
        height:qsTranslate('','seatchat_invite_msg_h')
        varText:[configTool.language.seatchat.enter_seat_subtitle,configTag["title_color"],qsTranslate('',"seatchat_invite_msg_fontsize")]
    }


    FocusScope{
        id:dialpadContainer
        anchors.left:invite_bg.left;
        anchors.top:invite_bg.top;
        anchors.leftMargin:qsTranslate('','seatchat_gridbox_lm')
        anchors.topMargin:qsTranslate('','seatchat_gridbox_tm');
    }
    //Model to form dialpad
    ListModel{ id:lettePadModel319_321
        ListElement{
            letter:"A"
        }
        ListElement{
            letter:"B"
        }
        ListElement{
            letter:"C"
        }
        ListElement{
            letter:"D"
        }
        ListElement{
            letter:"E"
        }
        ListElement{
            letter:"K"
        }

    }
    ListModel{ id:lettePadModel787;
        ListElement{
            letter:"A"
        }
        ListElement{
            letter:"B"
        }
        ListElement{
            letter:"C"
        }
        ListElement{
            letter:"D"
        }
        ListElement{
            letter:"E"
        }
        ListElement{
            letter:"F"
        }
        ListElement{
            letter:"G"
        }
        ListElement{
            letter:"J"
        }
        ListElement{
            letter:"K"
        }
    }
    ListModel{ id:lettePadModel330;
        ListElement{
            letter:"A"
        }
        ListElement{
            letter:"C"
        }
        ListElement{
            letter:"D"
        }
        ListElement{
            letter:"E"
        }
        ListElement{
            letter:"F"
        }
        ListElement{
            letter:"G"
        }
        ListElement{
            letter:"J"
        }
        ListElement{
            letter:"K"
        }
    }

    //Numpad grid
    GridView{
        id:numberPad;
        focus:true;
        anchors.top:dialpadContainer.top
        anchors.left: dialpadContainer.left
        height: 4*cellHeight;width:3*cellWidth
        cellHeight: qsTranslate('','seatchat_gridcell_h');
        cellWidth: qsTranslate('','seatchat_gridcell_w');
        clip:true;
        interactive: false;

        function addNumber(index,text){
            if(index==11) { deleteNumber() }
            else{ addToSeatNum(text); }
        }

        Component{
            id:gridDelegate;
            SimpleNavBrdrBtn{
                id:padBut
                width: qsTranslate('','seatchat_gridcell_h');
                height: qsTranslate('','seatchat_gridcell_w');
                btnTextWidth:parseInt(qsTranslate('',"seatchat_leave_unblock_btn_w"),10);
                buttonText: [(index==11)?"":((index==10)?"0":(index+1)),"white",qsTranslate('','seatchat_leave_unblock_btn_fontsize')]
                isHighlight: activeFocus
                //        isDisable:(pif.seatchat.sessionListModel.count==0)?true:false
                normalImg: viewHelper.configToolImagePath+configTag["dailnum_n"]
                highlightImg: viewHelper.configToolImagePath+configTag["dailnum_h"]
                pressedImg: viewHelper.configToolImagePath+configTag["dailnum_n"]
                normalBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
                highlightBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
                opacity: (index==9)?0:1;
                onEnteredReleased: {if(padBut.opacity!=1)return;
                    numberPad.addNumber(index,buttonText[0])
                }
                nav:["","","",""]
                onNoItemFound: {
                    if(dir=="up"){
                        //                            nickName_text_box.forceActiveFocus()
                    } else{ }
                }

                Image{
                    id:icon
                    anchors.centerIn: padBut
                    source: (index==11)?viewHelper.configToolImagePath+configTag["backspace_icon_n"]:""
                }
            }

        }

        onCurrentIndexChanged:{
            if(currentIndex == 9) currentIndex = 10;
        }

        Component.onCompleted: {
            numberPad.delegate=gridDelegate;
            numberPad.model=12;
        }

        Keys.onPressed: {
            switch(event.key){
            case(Qt.Key_Left):{
                if((numberPad.currentIndex%itemsPerRow) !== 0 || numberPad.currentIndex==10){
                    numberPad.moveCurrentIndexLeft();
                }
                event.accepted = true;
            }
            break;

            case(Qt.Key_Right):
            {
                if((numberPad.currentIndex%itemsPerRow+1) === itemsPerRow){
                    switch(numberPad.currentIndex) {
                    case 11:{
                        sendInvite_button.focus=true;
                        numberPad.focus=false;
                    }break;
                    case 2:{letterPad.currentIndex=0;}break;
                    case 5:{letterPad.currentIndex=3;}break;
                    case 8:{letterPad.currentIndex=6;}break;
                    }
                    letterPad.focus=true;
                    numberPad.focus=false;
                }else{
                    numberPad.moveCurrentIndexRight();
                }
                event.accepted = true;
            }
            break;

            case(Qt.Key_Up):
            {
                if(numberPad.currentIndex==0 || numberPad.currentIndex == 2 || numberPad.currentIndex == 1) {
                    dialpadContainer.focus=false;
                    close_button.focus=true;

                }
                else numberPad.moveCurrentIndexUp()
                event.accepted = true;
            }
            break;

            case(Qt.Key_Down):
            {
                switch(numberPad.currentIndex)
                {
                case 6:{numberPad.currentIndex=10}break;
                default:{numberPad.moveCurrentIndexDown()}break;
                }
                event.accepted = true;
            }
            break;
            }
        }

    }

    //letter pad
    GridView{
        id:letterPad;
        focus:false;
        anchors.top:dialpadContainer.top
        anchors.left: numberPad.right
        anchors.leftMargin:qsTranslate('','seatchat_gridbox_hgap')
        height: 4*cellHeight;width:3*cellWidth
        cellHeight: qsTranslate('','seatchat_gridcell_h');
        cellWidth: qsTranslate('','seatchat_gridcell_w');
        clip:true;
        interactive: false;

        function addLetter(index){
//            if(viewHelper.seatLetters!= []||viewHelper.seatLetters!= undefined){
//                core.log("addLetter | viewHelper.seatLetters | if exists")
//                addToSeatNum(viewHelper.seat_alphabets.get(index).letter)
//            }else{
                if(viewHelper.isAircraftType787()){
                    addToSeatNum(lettePadModel787.get(index).letter)
                }else if(viewHelper.isAircraftType330()){
                    addToSeatNum(lettePadModel330.get(index).letter)
                }else {
                    addToSeatNum(lettePadModel319_321.get(index).letter)
                }
//            }
        }


        Component{
            id:gridDelegate1;
            SimpleNavBrdrBtn{
                id:padBut
                width: qsTranslate('','seatchat_gridcell_h');
                height: qsTranslate('','seatchat_gridcell_w');
                btnTextWidth:parseInt(qsTranslate('',"seatchat_leave_unblock_btn_w"),10);
                buttonText: [letter,"white",qsTranslate('','seatchat_leave_unblock_btn_fontsize')]
                isHighlight: activeFocus
                //        isDisable:(pif.seatchat.sessionListModel.count==0)?true:false
                normalImg: viewHelper.configToolImagePath+configTag["dailnum_n"]
                highlightImg: viewHelper.configToolImagePath+configTag["dailnum_h"]
                pressedImg: viewHelper.configToolImagePath+configTag["dailnum_n"]
                normalBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
                highlightBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
                opacity: (index==10 || index ==11 || index ==9)?0:1;
                onEnteredReleased: {if(padBut.opacity!=1)return;
                    letterPad.addLetter(index)
                }
                nav:["","","",""]
                onNoItemFound: {
                    if(dir=="up"){
                        //                            nickName_text_box.forceActiveFocus()
                    } else{ }
                }
            }
        }

        Keys.onPressed: {
            switch(event.key){
            case(Qt.Key_Left):{
                if((letterPad.currentIndex%itemsPerRow) == 0){
                    letterPad.focus=false;
                    numberPad.focus=true;
                    switch(letterPad.currentIndex)
                    {
                    case 9:{numberPad.currentIndex=11;}break;
                    case 0:{numberPad.currentIndex=2;}break;
                    case 3:{numberPad.currentIndex=5;}break;
                    case 6:{numberPad.currentIndex=8;}break;
                    }
                }  else letterPad.moveCurrentIndexLeft();
                event.accepted = true;
            }
            break;

            case(Qt.Key_Right):  {
                if(((letterPad.currentIndex%itemsPerRow+1) == itemsPerRow)){
                    textBox.focus=true;
                    letterPad.focus=false;
                }
                else letterPad.moveCurrentIndexRight();
                event.accepted = true;
            }
            break;

            case(Qt.Key_Up):{
                switch(letterPad.currentIndex)
                {
                case 0:
                case 1:
                case 2: {
                    dialpadContainer.focus=false
                    close_button.focus=true;

                }
                break;

                default:{
                    letterPad.moveCurrentIndexUp();
                }break;
                }
                event.accepted = true;
            }
            break;

            case(Qt.Key_Down):{
                if(letterPad.currentIndex==7 || letterPad.currentIndex==8 || letterPad.currentIndex==6);
                else letterPad.moveCurrentIndexDown();
                event.accepted = true;
            }
            break;
            }
        }

        Component.onCompleted: {
            console.log(">>>>>>>>>>>>>>Component.onCompleted ="+core.dataController.getAircraftType()+ " viewHelper.isAircraftType787() ="+viewHelper.isAircraftType787())
//            console.log(">>>>>>>>>>>>>>Component.onCompleted | viewHelper.seatLetters="+viewHelper.seatLetters)
//            if(viewHelper.seatLetters!= [] ||viewHelper.seatLetters!= undefined){
//                core.log("Component.onCompleted | viewHelper.seatLetters | if exists")
//                letterPad.model=viewHelper.seat_alphabets;
//            }else{
                core.log("Component.onCompleted | viewHelper.seatLetters | if not exists ")
                if(viewHelper.isAircraftType787()){
                    letterPad.model=lettePadModel787;
                }else if(viewHelper.isAircraftType330()){
                    letterPad.model=lettePadModel330;
                }else{
                    letterPad.model=lettePadModel319_321;
                }
//            }
            letterPad.delegate=gridDelegate1;
        }
    }


    SimpleNavBrdrBtn{
        id:inviteName_text_box
        //        property string textColor: continueBtn.activeFocus?configTool.config.discover.synopsis.btn_text_h:(continueBtn.isPressed?configTool.config.discover.synopsis.btn_text_p:configTool.config.discover.synopsis.btn_text_n)
        width: qsTranslate('','seatchat_invite_input_w')
        anchors.top:invite_bg.top
        anchors.topMargin: qsTranslate('','seatchat_invite_input_tm')
        anchors.left:invite_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_invite_input_lm')
        isHighlight: textBox.activeFocus
        normalImg: viewHelper.configToolImagePath+configTag["input_n"];
        highlightImg: viewHelper.configToolImagePath+configTag["input_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["input_p"];
        normalBorder: [qsTranslate('','seatchat_invite_input_margin')]
        highlightBorder: [qsTranslate('','seatchat_invite_input_margin')]
        nav:[letterPad,close_button,add_button.visible?add_button:"",textBox]


        TextInput {
            id: textBox;
            width: qsTranslate('','seatchat_invite_input_txtw');
            height: qsTranslate('','seatchat_invite_input_txth');
            anchors.top:invite_bg.top
            anchors.topMargin: qsTranslate('','seatchat_invite_input_tm')
            anchors.left:inviteName_text_box.left
            anchors.leftMargin: qsTranslate('','seatchat_invite_input_txtlm')
            anchors.verticalCenter: inviteName_text_box.verticalCenter
            font.family:viewHelper.adultFont;
            color:"grey";
            font.pixelSize: qsTranslate('','seatchat_invite_input_fontsize')
            activeFocusOnPress:false
            maximumLength: 4
            onTextChanged: {
                if(text.length == maxSeatNoLimit)deleteNumber();
            }
            onActiveFocusChanged: {
                if(activeFocus)isError=false;
            }

            Keys.onPressed: {
                switch(event.key){
                case Qt.Key_Up: {
                    close_button.forceActiveFocus();
                    event.accepted = true;
                }
                break;
                case Qt.Key_Left: {
                    sendInvite_button.focus=false
                    letterPad.focus=true;
                    event.accepted = true;
                }break;
                case Qt.Key_Down: {
                    textBox.focus=false
                    if(!sendInvite_button.isDisable){
                        sendInvite_button.focus=true;
                    }else return;
                    event.accepted = true;
                }break;
                case Qt.Key_Enter: {
                    textBox.forceActiveFocus()
                    event.accepted = true;
                }break;
                }

            }
        }
    }

    ViewText{
        id:errorMsg
        anchors.top:inviteName_text_box.bottom;
        anchors.left:inviteName_text_box.left
        anchors.leftMargin: qsTranslate('','seatchat_invalidseat_msg_lm')
        width:qsTranslate('','seatchat_invalidseat_msg_w')
        height: paintedHeight//qsTranslate('','seatchat_invalidseat_msg_h')
        varText:[configTool.language.seatchat.invalid_seat_message,configTag["invalidseat_msg"],qsTranslate('',"seatchat_invalidseat_msg_fontsize")]
        font.bold: true
        wrapMode: Text.WordWrap
        maximumLineCount: 2
        visible:isError
    }

    SimpleNavButton{
        id:add_button
        anchors.verticalCenter: inviteName_text_box.verticalCenter
        anchors.right: inviteName_text_box.right
        anchors.rightMargin:qsTranslate('','seatchat_inviteadd_btn_rm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTag["add_tochat_n"];
        highlightimg:  viewHelper.configToolImagePath+configTag["add_tochat_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["add_tochat_p"];
        nav:[textBox,close_button,"",invitelist.visible?invitelist:(sendInvite_button.isDisable)?"":sendInvite_button]
        onVisibleChanged: console.log("0000000000000000000000000000000000000000:"+visible+":"+viewController.getActiveScreenPopupID())

        onEnteredReleased:{

            var a=textBox.text.toUpperCase()
            console.log(">>>>>>>>>>>>>>>>>>>>>>>> core.coreHelper.validateSeatNoForApps(a) = "+core.coreHelper.validateSeatNoForApps(a)+":"+viewController.getActiveScreenPopupID()+":"+visible)
            if(textBox.text!="" &&  (core.pc==1 || pif.seatChat.validateSeatNum(a,viewHelper.cSessionID)) && a != pif.getSeatNumber().toUpperCase() && core.coreHelper.validateSeatNoForApps(a)){
                console.log(">>>>>>>>>>>>>>>>>>>>>>>>  a = "+a)
                if(!checkLocalInviteModel(a))addInvites()          // two check if the same seat exist or not if exists then dont send it again
            }else{
                console.log(">>>>>>>>>>>>>>>>>>>>>>>>  a >> = "+a)
                textBox.forceActiveFocus()
                isError=true
                //                textBox.text=""
            }
        }
    }
    ViewText{
        id:invitation_Msg
        anchors.left:invite_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_invitesendto_txt_lm')
        anchors.bottom: inviteee.top
        width:qsTranslate('','seatchat_invitesendto_txt_w')
        height: qsTranslate('','seatchat_invitesendto_txt_h')
        font.family:viewHelper.adultFont;
        varText:[configTool.language.seatchat.multiple_seat_invitation,configTag["invalidseat_msg"],qsTranslate('',"seatchat_invalidseat_msg_fontsize")]
        visible:(dummyInviteModel.count>0)? true:false
    }

    ListModel{id:dummyInviteModel}

    FocusScope{
        id:inviteee
        width:qsTranslate('','seatchat_invitedseat_cont_w')
        height:qsTranslate('','seatchat_invitedseat_cont_h')
        anchors.top:invite_bg.top
        anchors.topMargin: qsTranslate('','seatchat_invitedseat_cont_tm')
        anchors.right: invite_bg.right
        anchors.rightMargin:qsTranslate('','seatchat_invitedseat_cont_rm')

        SimpleButton{
            id:left_arrow
            opacity:((invitelist.count>3 && invitelist.contentWidth>invitelist.width) && (invitelist.contentX>0))?true:false
            anchors.right:invitelist.left
            anchors.verticalCenter: invitelist.verticalCenter
            normImg: viewHelper.configToolImagePath+configTag["left_arrow_n"]
            highlightimg: viewHelper.configToolImagePath+configTag["left_arrow_h"]
            pressedImg:viewHelper.configToolImagePath+configTag["left_arrow_p"]
            onEnteredReleased: {
                invitelist.forceActiveFocus()
                invitelist.decrementCurrentIndex()
            }
            //            Keys.onRightPressed: {
            //                invitelist.forceActiveFocus()
            //            }
        }

        SimpleButton{
            id:right_arrow
            opacity:((invitelist.count>3 && invitelist.contentWidth>invitelist.width) && (invitelist.contentX<(invitelist.contentWidth-invitelist.width)))?true:false
            anchors.left: invitelist.right
            anchors.verticalCenter: invitelist.verticalCenter
            normImg: viewHelper.configToolImagePath+configTag["right_arrow_n"]
            highlightimg: viewHelper.configToolImagePath+configTag["right_arrow_h"]
            pressedImg:viewHelper.configToolImagePath+configTag["right_arrow_p"]
            onEnteredReleased: {
                invitelist.forceActiveFocus()
                invitelist.incrementCurrentIndex()
            }
            //            Keys.onLeftPressed:{
            //                invitelist.forceActiveFocus()
            //            }
        }

        ListView{
            id:invitelist
            width:qsTranslate('','seatchat_invitedseat_cont_w')
            height:qsTranslate('','seatchat_invitedseat_cont_h')
            boundsBehavior: Flickable.StopAtBounds
            snapMode: ListView.SnapToItem
            interactive: true
            delegate: invite_list_delegate
            model:dummyInviteModel
            orientation :ListView.Horizontal
            visible: (dummyInviteModel.count>0)?true:false
            clip:true
            focus: true
            Keys.onRightPressed: {
                incrementCurrentIndex()
            }
            Keys.onLeftPressed: {
                decrementCurrentIndex()
            }
            Keys.onDownPressed: {
                sendInvite_button.forceActiveFocus()
            }
            Keys.onUpPressed: {
                add_button.forceActiveFocus()
            }
        }
        function removeIndex(curIndex){
            dummyInviteModel.remove(curIndex);
        }

        Component{
            id:invite_list_delegate
            Item{
                width:qsTranslate('','seatchat_invitedseat_btn_w')
                onActiveFocusChanged: {
                    if(activeFocus)invite_button_list.forceActiveFocus()
                }

                SimpleNavBrdrBtn{
                    id:invite_button_list
                    property string textColor: invite_button_list.activeFocus?configTool.config.seatchat.chatroom.btn_text_h:(invite_button_list.isPressed?configTool.config.seatchat.chatroom.btn_text_p:configTool.config.seatchat.chatroom.btn_text_n)
                    width:qsTranslate('','seatchat_invitedseat_btn_w')
                    btnTextWidth:parseInt(qsTranslate('',"seatchat_invitedseat_btn_w"),10);
                    buttonText: [inputTextInvite,invite_button_list.textColor,qsTranslate('','seatchat_invitsend_btn_fontsize')]
                    isHighlight: activeFocus
                    normalImg: viewHelper.configToolImagePath+configTag["seatlist_btn_n"]
                    highlightImg: viewHelper.configToolImagePath+configTag["seatlist_btn_h"]
                    pressedImg: viewHelper.configToolImagePath+configTag["seatlist_btn_p"]
                    //                    nav:[left_arrow,add_button,right_arrow,sendInvite_button]
                    onEnteredReleased:{
                        if(dummyInviteModel.count>0){
                            invite_button_list.forceActiveFocus()
                        }else{
                            add_button.forceActiveFocus()
                        }

                        invitelist.currentIndex=index;
                        inviteee.removeIndex(invitelist.currentIndex)}
                }
                ViewText{
                    id:invite_button_text
                    anchors.top:invite_button_list.top;
                    anchors.topMargin:  qsTranslate('','seatchat_invitedseat_btn_txttm')
                    anchors.horizontalCenter:invite_button_list.horizontalCenter
                    horizontalAlignment:Text.AlignHCenter
                    width:qsTranslate('','seatchat_invitedseat_btn_txtw')
                    height: qsTranslate('','seatchat_invitedseat_btn_txth')
                    font.family:viewHelper.adultFont;
                    varText:[seatnum.toUpperCase(),"grey"/*configTag["seatlist_title"]*/,qsTranslate('',"seatchat_invitedseat_btn_fontsize")]
                    font.bold: true
                }
            }
        }
    }


    SimpleNavBrdrBtn{    // newchat button
        id:sendInvite_button
        focus: true
        property string textColor: sendInvite_button.activeFocus?configTool.config.seatchat.chatroom.btn_text_h:(sendInvite_button.isPressed?configTool.config.seatchat.chatroom.btn_text_p:configTool.config.seatchat.chatroom.btn_text_n)
        anchors.top: invite_bg.top
        anchors.topMargin: qsTranslate('','seatchat_invitsend_btn_tm')
        anchors.left: invite_bg.left
        anchors.leftMargin:qsTranslate('','seatchat_invitsend_btn_lm')
        width: parseInt(qsTranslate('','seatchat_invitsend_btn_w'),10)
        btnTextWidth:parseInt(qsTranslate('',"seatchat_invitsend_btn_w"),10);
        buttonText: [configTool.language.seatchat.send_invitation,sendInvite_button.textColor,qsTranslate('','seatchat_invitsend_btn_fontsize')]
        isHighlight: activeFocus
        //        isDisable:(pif.seatchat.sessionListModel.count==0)?true:false
        normalImg: viewHelper.configToolImagePath+configTag["btn_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["btn_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["btn_p"]
        normalBorder: [qsTranslate('','seatchat_invitsend_btn_margin')]
        highlightBorder: [qsTranslate('','seatchat_invitsend_btn_margin')]
        nav:[letterPad,(invitelist.visible)?invitelist:textBox,"",""]
        isDisable:sendInviteIsdisable()
        onEnteredReleased:{
            if(systempopupId==12){
                core.log(" for mutiple invite")
                forMutipleInvite()
            }else{
                core.log(" for single invite")
                sendInvite()}
        }
    }


    Component.onCompleted:{
        pif.seatChat.getAllusers();
        init()
    }
}
