// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
FocusScope{
    anchors.top:parent.top
    anchors.topMargin:parseInt(qsTranslate('','synopsis_panel_tm'),10)
    property alias synopsisCloseBtn: synopsisCloseBtn
    property alias slider: slider
    property string loaderSource: listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"media_attr_template_id")
    property variant model:listingobj.compVpathListing.model
    property variant tvModel: []
    property variant currentIndex: listingobj.compVpathListing.synopIndex
    property variant synopsisDetails: []
    property bool isSliderNavChanged: false
    property bool isEpisodePlayBtn: false
    property bool isTv:   (currentTemplateNode=="tv" || currentTemplateNode=="kids_tv_listing")
    property int currentMid: -1
    signal extremeEnds(string direction)
    /**************************************************************************************************************************/

    onLoaderSourceChanged: {
        if(!visible)return
        init()
    }
    onExtremeEnds:{

        if(direction=="left" && model.count>1){
            decrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="right" && model.count>1){
            incrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="up")
            subCatobj.forceActiveFocus()
    }
    onCurrentIndexChanged:{
        if(!isSynopsis)return
        if(slider.targetData) slider.targetData.contentY=0
        forceFocus()
    }
    Connections{
        target:visible?widgetList.getSoundTrackSubtitleRef():null
        onReadyToPlayVideo:{
            viewHelper.setVideoParams()
        }
    }
    /**************************************************************************************************************************/
    function init(){    
        synopsisLoader.sourceComponent=blankComp
        if(loaderSource=="tvseries"){
            isEpisodePlayBtn=false
            synopsisLoader.sourceComponent=tvSeriesSynopsis
            viewHelper.setHelpTemplate("episode_listing");
        }
        else if(isTv){
            isEpisodePlayBtn=false
            synopsisLoader.sourceComponent=episodeSynopsis
            viewHelper.setHelpTemplate("tv_synop");
        }
        else{
            synopsisLoader.sourceComponent=moviesSynopsis
            viewHelper.setHelpTemplate("movie_synopsis");
        }
    }
    function backBtnPressed(){
        closeLoader();
    }
    function clearOnExit(){
        visible=false;
        synopsisLoader.sourceComponent=blankComp;
    }

    function langUpdate(){
        if(loaderSource=="tvseries"){
            dataController.refreshModelsByIntLanguage([tvModel],[isTv?getTvDetails:getSynopsisDetails]);
            widgetList.focusToHeader()

        }else{
            if(isTv) getTvDetails()
            else getSynopsisDetails()
        }
    }
    function getTvDetails(){
        var temp=""
        if(loaderSource=="tvseries"){
            temp+=(tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"genre")!='')?tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"genre")+"<br> ":""
            temp +=(tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"rating_name")!='')?tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"rating_name"):""
            temp +=(tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"duration")!='')?" | "+core.coreHelper.convertHMSToMin(tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"duration")):""
        }
        else{
            temp+=(model.getValue(currentIndex,"genre")!='')?model.getValue(currentIndex,"genre")+"<br> ":""
            temp +=(model.getValue(currentIndex,"rating_name")!='')?model.getValue(currentIndex,"rating_name"):""
            temp +=(model.getValue(currentIndex,"duration")!='')?" | "+core.coreHelper.convertHMSToMin(model.getValue(currentIndex,"duration")):""
        }
        synopsisDetails=temp
    }
    function forceFocus(){
        if(slider.upArrow.activeFocus || slider.downArrow.activeFocus || synopsisCloseBtn.activeFocus){
            if(!slider.visible){
                if(!synopsisCloseBtn.activeFocus)synopsisLoader.item.forceActiveFocus()
                else synopsisCloseBtn.forceActiveFocus()
            }
        }
        else if(loaderSource=="tvseries"){
            synopsisLoader.item.synopsisContentFlickable.changeCurrentIndex();
            synopsisLoader.item.synopsisContentFlickable.forceActiveFocus();
        }
        else   synopsisLoader.item.forceActiveFocus();
        if(isTv) getTvDetails()
        else getSynopsisDetails()
    }
    function checkMidForBlocking(action,type){
        var mid=0
        var rating=0
        mid=listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"mid")
        rating=listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"rating");
        if(type=="episode"){
            var parentBlosked=dataController.mediaServices.getMidBlockStatus(mid,rating);
            if(parentBlosked){viewController.updateSystemPopupQ(7,true);return true;}
            mid=tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"mid")
            rating=tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"rating")
        }
        //        var mid=type=="tvseries" ?(tvModel.getValue(0,"mid")):(type=="episode"?tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"mid"):listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"mid"))
        //     var rating=type=="tvseries" ?(tvModel.getValue(0,"rating")):(type=="episode"?tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"rating"):listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"rating"))
        var mid_status = dataController.mediaServices.getMidBlockStatus(mid,rating);
        return mid_status
    }
    function loadVod(action,type){
        if(checkMidForBlocking(action,type)) {viewController.updateSystemPopupQ(7,true);return}
        if(type=="tvseries"){
            viewHelper.setTvSeriesModel(tvModel)
            viewHelper.setTvSeriesIndex(0)
        }
        else if(type=="episode")  {
            viewHelper.setTvSeriesModel(tvModel)
            viewHelper.setTvSeriesIndex(synopsisLoader.item.synopsisContentFlickable.currentIndex)
        }
        viewHelper.setMediaListingModel(listingobj.compVpathListing.model)
        viewHelper.setMediaListingIndex(listingobj.compVpathListing.currentIndex)

        viewHelper.initializeVOD(action,type,"",false,false)
    }
    function getSynopsisDetails(){
        var tempObject=new Object
        var temp=""
        synopsisobj.synopsisItem.item.synopsisDetails=temp
        temp +=(model.getValue(currentIndex,"director")!='')?configTool.language.movies.directed_by+" "+model.getValue(currentIndex,"director")+"<br>":""
        temp +=(model.getValue(currentIndex,"cast")!='')?configTool.language.movies.starring+" "+model.getValue(currentIndex,"cast")+"<br>":""
        //  temp +=(model.getValue(currentIndex,"cast")!='')?configTool.language.movies.produced_by+model.getValue(currentIndex,"cast"):""
        tempObject["details"]=temp.substring(0,(temp.length-1))
        temp=''
        temp+=(model.getValue(currentIndex,"duration")!='')?model.getValue(currentIndex,"duration")+" | ":""
        temp+=(model.getValue(currentIndex,"rating_name")!='')?model.getValue(currentIndex,"rating_name")+" | ":""
        temp+=(model.getValue(currentIndex,"genre")!='')?model.getValue(currentIndex,"genre")+" | ":""
        temp+=(model.getValue(currentIndex,"year")!='')?model.getValue(currentIndex,"year")+" | ":""
        tempObject["extraDetails"]=temp.substring(0,(temp.length-2))
        synopsisDetails=tempObject

    }
    /**************************************************************************************************************************/
    Image{
        id:synopsisIMG
        source:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.panel
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Loader{
        id:synopsisLoader

        anchors.fill: synopsisIMG
        sourceComponent: moviesSynopsis
        onStatusChanged:{
            if(status==Loader.Ready){
                forceFocus();
            }
        }
    }
    SimpleNavButton{
        id:synopsisCloseBtn
        anchors.right: synopsisIMG.right
        anchors.rightMargin: qsTranslate('','synopsis_close_rm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_close_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.close_btn_n;
        highlightimg:  viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.close_btn_h;
        pressedImg: viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.close_btn_p;
        nav:["","","",!slider.upArrow.isDisable?slider.upArrow:(!slider.downArrow.isDisable?slider.downArrow:(loaderSource=="tvseries")?synopsisLoader.item.synopsisContentFlickable:synopsisLoader.item)]
        onEnteredReleased:{closeLoader();}
        onPadReleased:if(dir!="down")extremeEnds(dir)
        onNoItemFound:  if(dir!="left")extremeEnds(dir)
    }
    Image{
        id:posterIMGBtn
        width: loaderSource=="tvseries"?qsTranslate('','episode_episode_poster_w'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_poster_w')
        height: loaderSource=="tvseries"?qsTranslate('','episode_episode_poster_h'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_poster_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: loaderSource=="tvseries"?qsTranslate('','episode_episode_poster_lm'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_poster_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:loaderSource=="tvseries"?qsTranslate('','episode_episode_poster_tm'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_poster_tm')
        source: model.count>0?viewHelper.cMediaImagePath+model.getValue(currentIndex,"poster"):""
    }
    Scroller{
        id:slider
        anchors.top:synopsisIMG.top; anchors.topMargin: qsTranslate('','synopsis_slider_tm')
        anchors.right: synopsisIMG.right ;anchors.rightMargin: qsTranslate('','synopsis_slider_rm')
        width:loaderSource=="tvseries"?qsTranslate('','episode_slider_w'): qsTranslate('','synopsis_slider_w')
        height: loaderSource=="tvseries"?qsTranslate('','episode_slider_h'):qsTranslate('','synopsis_slider_h')
        targetData:visible?synopsisLoader.item.synopsisContentFlickable:null
        sliderbg:[loaderSource=="tvseries"?qsTranslate('','episode_slider_base_margin'):qsTranslate('','synopsis_slider_base_margin'),loaderSource=="tvseries"?qsTranslate('','episode_slider_w'):qsTranslate('','synopsis_slider_w'),loaderSource=="tvseries"?qsTranslate('','episode_slider_base_h'):qsTranslate('','synopsis_slider_base_h'),configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.scroll_base,]
        scrollerDot:configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.scroll_button
        downArrowDetails:  [configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.dn_arrow_n,configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.dn_arrow_h,configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.dn_arrow_p]
        upArrowDetails: [configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.up_arrow_n,configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.up_arrow_h,configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.up_arrow_p]
        visible: synopsisLoader.item.synopsisContentFlickable.contentHeight>synopsisLoader.item.synopsisContentFlickable.height
        onSliderNavigation: {
            var tempModelCount=tvModel.count>0?tvModel.count:0
            if(direction=="down" && loaderSource!="tvseries")
                synopsisLoader.item.forceActiveFocus()
            else if(direction=="up")
                synopsisCloseBtn.forceActiveFocus()
            else if(direction=="right"){
                isSliderNavChanged=true
                extremeEnds(direction)
            }
            else if(direction=="left" && tempModelCount>0 && isTv && loaderSource=="tvseries"){
                synopsisLoader.item.synopsisContentFlickable.forceActiveFocus()
            }
        }
        onVisibleChanged: {
            if(!visible && isSliderNavChanged){
                isSliderNavChanged=false
                init()
            }
        }
        onArrowPressed: {
            if(loaderSource!="tvseries")return;
            if(isEpisodePlayBtn)isEpisodePlayBtn=false
            if(synopsisLoader.item.synopsisContentFlickable.changeCurrentIndex)
                synopsisLoader.item.synopsisContentFlickable.changeCurrentIndex()
        }
    }
    ViewText{
        id:synopsisTitleBtn
        width:loaderSource=="tvseries"?qsTranslate('','episode_episode_title_w'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_title_w')
        height:loaderSource=="tvseries"?(lineCount>1?qsTranslate('','episode_episode_title_h'):paintedHeight):(lineCount>1?qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_title_h'):paintedHeight)
        anchors.left:synopsisIMG.left
        anchors.leftMargin: loaderSource=="tvseries"?qsTranslate('','episode_episode_title_lm'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_title_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:loaderSource=="tvseries"?qsTranslate('','episode_episode_title_tm'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_title_tm')
        varText: [text,configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.title_color,qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_title_fontsize')]
        // verticalAlignment: Text.AlignVCenter
        text:model.count>0?model.getValue(currentIndex,"title"):""
        elide: Text.ElideRight
        maximumLineCount: loaderSource=="tvseries"?1:2
        wrapMode: Text.Wrap

        lineHeight:lineCount>1?qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_title_lh'):parseInt(qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_title_lh'),10)+5
        lineHeightMode: Text.FixedHeight
    }
    ViewText{
        visible: synopsisLoader.sourceComponent==moviesSynopsis || synopsisLoader.sourceComponent==tvSeriesSynopsis?true:false
        width:loaderSource=="tvseries"?qsTranslate('','episode_episode_title_w'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_subtitle_w')
        anchors.top: synopsisTitleBtn.bottom
        anchors.topMargin: loaderSource=="tvseries"?qsTranslate('','episode_episode_subtitle_tm'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_subtitle_tm')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: loaderSource=="tvseries"?qsTranslate('','episode_episode_title_lm'):qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_title_lm')
        varText: [text,configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].synopsis.subtitle_color,qsTranslate('','synopsis_'+viewHelper.mapMenuTid(currentTemplateNode)+'_subtitle_fontsize')]
        elide: Text.ElideRight
        text:model.count>0?(synopsisLoader.sourceComponent==moviesSynopsis)?model.getValue(currentIndex,"short_title"):(synopsisLoader.sourceComponent==tvSeriesSynopsis?synopsisLoader.item.synopsisContentFlickable.count+ " "+(synopsisLoader.item.synopsisContentFlickable.count>1?configTool.language.tv.tv_episodes:configTool.language.tv.tv_episode):""):""
    }
    Component{
        id:moviesSynopsis
        MoviesSynopsis{
            id:movieData
        }
    }
    Component{
        id:episodeSynopsis
        EpisodeSynopsis{
            id: episodeData
        }
    }
    Component{
        id:tvSeriesSynopsis
        TvSeriesSynopsis{
            id:tvData
        }
    }
    Component{
        id:episodeDelegate
        Item{
            width: qsTranslate('','episode_episode_w')
            height: episodeDescription.visible?(highlightIMg.height+divline.height+parseInt(qsTranslate("",'episode_episode_high_bm'),10)): qsTranslate('','episode_episode_h')
            onActiveFocusChanged: if(activeFocus && !isEpisodePlayBtn)highlightIMg.forceActiveFocus()
            property bool currEpisBtn:(synopsisLoader.item.synopsisContentFlickable.currentIndex==index && activeFocus);

            SimpleBorderBtn{
                id:highlightIMg
                anchors.bottom: divline.top
                width: qsTranslate('','episode_episode_high_w')
                height: episodeDescription.visible?(episodeDescription.height+episodeBtnFS.height+episodeTitle.height+parseInt(qsTranslate('','episode_play_tm'),10)+parseInt(qsTranslate('','episode_episodetitle_tm'),10)+parseInt(qsTranslate("",'episode_episode_desc1_tm'),10)):parseInt(qsTranslate('','episode_episode_high_h'),10)
                isHighlight: activeFocus

                normalImg:   activeFocus && !isEpisodePlayBtn?viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.listing_highlight:""
                onEnteredReleased: {
                    core.debug(" synopsisLoader.item.synopsisContentFlickable.currentIndex : "+synopsisLoader.item.synopsisContentFlickable.currentIndex+"......isEpisodePlayBtn : "+isEpisodePlayBtn)
                    if(synopsisLoader.item.synopsisContentFlickable.currentIndex==index){
                        isEpisodePlayBtn=!isEpisodePlayBtn
                    }else{
                        synopsisLoader.item.synopsisContentFlickable.currentIndex=index
                        isEpisodePlayBtn=true
                    }
                    if(isEpisodePlayBtn){
                        if(playBtn.visible){
                            playBtn.forceActiveFocus();
                        }else{
                            resumeBtn.forceActiveFocus();
                        }
                    }

                }
                ViewText{
                    id:episodeIndex
                    width: qsTranslate('','episode_episodetitle_index_w')
                    height: qsTranslate('','episode_episodetitle_h')
                    anchors.top:parent.top
                    anchors.left: parent.left
                    anchors.topMargin: qsTranslate('','episode_episodetitle_tm')
                    anchors.leftMargin: qsTranslate('','episode_episodetitle_lm')
                    varText: [text,configTool.config[currentTemplateNode].synopsis.description1_color,qsTranslate('','episode_episodetitle_fontsize')]
                    text:parseInt(index +1)
                }
                ViewText{
                    id:episodeTitle
                    width:  qsTranslate('','episode_episodetitle_w')
                    height: qsTranslate('','episode_episodetitle_h')
                    anchors.top:parent.top
                    anchors.left: episodeIndex.right
                    anchors.topMargin: qsTranslate('','episode_episodetitle_tm')
                    anchors.leftMargin:  qsTranslate('','episode_episodetitle_gap')
                    text:tvModel.count>0?tvModel.getValue(index,"title"):""//tvModel.get(index).title
                    varText: [text,configTool.config[currentTemplateNode].synopsis.description1_color,qsTranslate('','episode_episodetitle_fontsize')]
                }
                ViewText{
                    id:episodeDescription
                    visible: synopsisLoader.item.synopsisContentFlickable.currentIndex==index && isEpisodePlayBtn
                    anchors.top:episodeTitle.bottom
                    anchors.topMargin: qsTranslate('','episode_episode_desc1_tm')
                    anchors.left: episodeTitle.left
                    width: qsTranslate('','episode_episode_desc1_w')
                    height: paintedHeight//>qsTranslate('','episode_episode_desc1_h')?paintedHeight:qsTranslate('','episode_episode_desc1_h')
                    text:tvModel.count>0?tvModel.getValue(index,"description"):""//tvModel.get(index).description
                    varText: [text,configTool.config[currentTemplateNode].synopsis.description1_color,qsTranslate('','episode_episode_desc1_fontsize')]
                    wrapMode: Text.Wrap
                    onVisibleChanged: if(visible)episodeBtnFS.forceActiveFocus()

                }
                FocusScope{
                    id:episodeBtnFS
                    anchors.top:episodeDescription.bottom
                    anchors.topMargin: qsTranslate('','episode_play_tm')
                    width: parent.width
                    height:  qsTranslate('','episode_play_h')
                    visible:  episodeDescription.visible
                    SimpleNavBrdrBtn{
                        id:playBtn
                        property string textColor: playBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(playBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
                        anchors.right: parent.right
                        anchors.rightMargin: qsTranslate('','episode_play_rm')
                        width: qsTranslate('','episode_play_w')
                        height: qsTranslate('','episode_play_h')
                        visible: !widgetList.video.vodScreen.visible?!(pif.getMidInfo(tvModel.getValue(synopsisLoader.item.synopsisContentFlickable.currentIndex,"mid"),'elapseTime')>viewHelper.resumeSecs):true
                        onVisibleChanged: {
                            if(!isEpisodePlayBtn)return;
                            if(episodeBtnFS.activeFocus){
                                if(!visible) resumeBtn.forceActiveFocus()
                                else playBtn.forceActiveFocus()
                            }
                            else {
                                if(!visible) resumeBtn.focus=true
                                else playBtn.focus=true
                            }
                        }
                        isHighlight: activeFocus
                        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
                        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
                        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
                        normalBorder: [qsTranslate('','episode_play_margin')]
                        highlightBorder: [qsTranslate('','episode_play_margin')]
                        onEnteredReleased:{
                            viewHelper.parentMidTvSeries=listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"mid")
                            loadVod("play","episode")
                        }
                        SimpleButton{
                            anchors.left: parent.left
                            anchors.leftMargin:qsTranslate('','episode_play_icon_lm')
                            anchors.verticalCenter: parent.verticalCenter
                            isHighlight: playBtn.activeFocus
                            normImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_n
                            highlightimg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_h
                            pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_p
                        }
                        ViewText{
                            anchors.left: playBtn.left
                            anchors.leftMargin:qsTranslate('','episode_play_text_lm')
                            anchors.verticalCenter: parent.verticalCenter
                            width:qsTranslate('','episode_play_textw')
                            varText: [text,playBtn.textColor,qsTranslate('','episode_play_fontsize')]
                            text:configTool.language.movies.play
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }

                    //Resume and restart is swapped for customer request//
                    SimpleNavBrdrBtn{
                        id:restartBtn
                        property string textColor: restartBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(restartBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
                        visible:  !playBtn.visible
                        width: qsTranslate('','episode_resume_w')
                        height: qsTranslate('','episode_resume_h')
                        anchors.right: resumeBtn.left
                        anchors.rightMargin :qsTranslate('','episode_resume_gap')
                        btnTextWidth:parseInt(qsTranslate('',"episode_resume_w"),10);
                        buttonText: [configTool.language.movies.restart,restartBtn.textColor,qsTranslate('','episode_resume_fontsize')]
                        isHighlight: activeFocus
                        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
                        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
                        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
                        normalBorder: [qsTranslate('','episode_resume_margin')]
                        highlightBorder: [qsTranslate('','episode_resume_margin')]
                        nav: ["","",resumeBtn,""]
                        onEnteredReleased: {
                            viewHelper.parentMidTvSeries=listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"mid")
                            loadVod("restart","episode")
                        }
                        Keys.onRightPressed:{
                            resumeBtn.forceActiveFocus();
                            event.accepted=true;
                        }
                    }
                    SimpleNavBrdrBtn{
                        id:resumeBtn
                        property string textColor: resumeBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(resumeBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
                        visible: !playBtn.visible
                        //   anchors.top:parent.top
                        anchors.topMargin: qsTranslate('','episode_play_tm')
                        anchors.right: parent.right
                        anchors.rightMargin: qsTranslate('','episode_resume_rm')
                        width: qsTranslate('','episode_resume_w')
                        height: qsTranslate('','episode_resume_h')
                        btnTextWidth:parseInt(qsTranslate('',"episode_resume_w"),10);
                        buttonText: [configTool.language.movies.resume,resumeBtn.textColor,qsTranslate('','episode_resume_fontsize')]
                        nav: [restartBtn,"","",""]
                        isHighlight: activeFocus
                        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
                        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
                        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
                        normalBorder: [qsTranslate('','episode_resume_margin')]
                        highlightBorder: [qsTranslate('','episode_resume_margin')]
                        onEnteredReleased: {
                            viewHelper.parentMidTvSeries=listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"mid")
                            loadVod("resume","episode")

                        }
                        Keys.onLeftPressed:{
                            restartBtn.forceActiveFocus();
                            event.accepted=true;
                        }
                        onNoItemFound: {
                            core.debug(" right : "+right)
                            if(dir=="right"){
                                if(slider.visible){
                                    if(slider.upArrow.isDisable)
                                        slider.downArrow.forceActiveFocus()
                                    else
                                        slider.forceActiveFocus()
                                }
                            }
                        }
                    }
                }
            }
            Rectangle{
                id:divline
                anchors.bottom: parent.bottom
                anchors.bottomMargin:   qsTranslate("",'episode_episode_high_bm')
                width: parent.width
                height: qsTranslate("",'common_line_h')
                color: configTool.config[currentTemplateNode].synopsis.description1_color
                opacity: qsTranslate('','common_disabled_opacity')
            }
        }
    }
}
