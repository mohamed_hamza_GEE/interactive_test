import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    id: appAirports;
    width: core.width; height: core.height;
    focus: true;

    property string currAppImages: microAppDir+currAppName+"/images/"
    property string xmlModelSource: microAppDir+currAppName+"/"+currAppName+"_data.xml"

    property int selectedIndex: -1;

    function init() {
        core.log("AppAirports | init");
        airportsInfoData.source = xmlModelSource;
        airportsListData.source = xmlModelSource;
        selectedIndex = -1;
    }
    function reload(){
        airportsInfoData.source = xmlModelSource;
        airportsListData.source = xmlModelSource;
        airportsInfoData.reload();
        airportsListData.reload();
    }

    function setTitleText(obj,Dmodel,ind){
        obj.text = (Dmodel.get(ind).title_cdata !== "") ? Dmodel.get(ind).title_cdata : Dmodel.get(ind).title;
        obj.color = configTool.config.aviancaapp.screen.text_header_title;
        obj.font.pixelSize = qsTranslate('','aviapp_header_title_fontsize')
    }
    function setMainMenuTitleText(obj,Dmodel,ind){
        obj.text = (Dmodel.get(ind).menuTitle_cdata != "") ? Dmodel.get(ind).menuTitle_cdata : Dmodel.get(ind).menuTitle;
        obj.color = configTool.config.aviancaapp.screen.text_header_menu;
        obj.font.pixelSize = qsTranslate('','aviapp_header_menu_fontsize');
    }
    function pageUp(){
        if(childAppList.contentY>0){
            if((childAppList.contentY-childAppList.height)>0){
                childAppList.contentY = childAppList.contentY-childAppList.height
            }else{
                childAppList.contentY = 0
            }
            var firstIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            childAppList.currentIndex = firstIndex;
            childAppList.positionViewAtIndex(firstIndex,ListView.Beginning)
        }
        childAppList.forceActiveFocus()
    }
    function pageDown(){
        if(childAppList.contentY<(childAppList.contentHeight-childAppList.height)){
            if((childAppList.contentY+childAppList.height)<(childAppList.contentHeight-childAppList.height)){
                childAppList.contentY = childAppList.contentY+childAppList.height
            }else{
                childAppList.contentY = childAppList.contentY+((childAppList.contentHeight-childAppList.height)-childAppList.contentY)
            }
            var lastIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            childAppList.currentIndex = lastIndex+1;
            childAppList.positionViewAtIndex(lastIndex,ListView.Beginning)
        }
        childAppList.forceActiveFocus()
    }
    function languageUpdate(){
        selectedIndex=-1
        airportsInfoData.reload()
        airportsListData.reload()
    }

    XmlListModel {
        id: airportsInfoData;
        //        source:xmlModelSource;
        query: "/application/screen";

        XmlRole { name:"title"; query: "text1/"+langCode+"/@_value/string()" }
        XmlRole { name:"title_cdata"; query: "text1/"+langCode+"/string()" }
        XmlRole { name:"menuTitle"; query: "text2/"+langCode+"/@_value/string()" }
        XmlRole { name:"menuTitle_cdata"; query: "text2/"+langCode+"/string()" }

        onCountChanged: {
            setTitleText(airportsBackground.headerTitle,airportsInfoData,0);
            setMainMenuTitleText(airportsBackground.headerMenuTitle,airportsInfoData,0);
        }
    }
    XmlListModel {
        id: airportsListData;
        //        source:xmlModelSource
        query: "/application/screen/list1/listItem";

        XmlRole { name:"airport_list_text"; query: "text4/"+langCode+"/@_value/string()" }
        XmlRole { name:"airport_list_text_cdata"; query: "text4/"+langCode+"/string()" }
        XmlRole { name:"airport_list_header_text"; query: "text3/"+langCode+"/@_value/string()" }
        XmlRole { name:"airport_list_header_text_cdata"; query: "text3/"+langCode+"/string()" }
        XmlRole { name:"airport_info_text"; query: "text5/"+langCode+"/@_value/string()" }
        XmlRole { name:"airport_info_text_cdata"; query: "text5/"+langCode+"/string()" }
        XmlRole { name:"airport_header_text"; query: "text6/"+langCode+"/@_value/string()" }
        XmlRole { name:"airport_header_text_cdata"; query: "text6/"+langCode+"/string()" }
        XmlRole { name:"airport_image"; query: "image2/@_value/string()" }
        onCountChanged: {
            childAppList.model="";
            childAppList.model=airportsListData;

        }
    }

    AviancaAppBg {
        id: airportsBackground;

        appBackgroundImage: configTool.config.aviancaapp.screen.app_bg;//"app_bg.png";
        appHeaderImage: configTool.config.aviancaapp.screen.app_header;//"app_header.png";
        appSideMenuPanelImage: configTool.config.aviancaapp.screen.sidemenu_panel;//"sidemenu_panel.png";
        appHeaderTitle: (airportsInfoData.get(selectedIndex).title_cdata != "")? airportsInfoData.get(selectedIndex).title_cdata : airportsInfoData.get(selectedIndex).title;
        appHeaderMenuTitle: (airportsInfoData.get(selectedIndex).menuTitle_cdata != "") ? airportsInfoData.get(selectedIndex).menuTitle_cdata : airportsInfoData.get(selectedIndex).menuTitle;
        Keys.onDownPressed: {
            if(upArrowContainer.visible){
                upArrowImage.forceActiveFocus()
            }else{
                childAppList.forceActiveFocus();
            }
        }
    }

    Flickable{
        id:airpotsFlickableData;
        height: qsTranslate('','aviapp_airport_img_h');
        width: qsTranslate('','aviapp_airport_img_w');
        x: qsTranslate('','aviapp_airport_title_x')
        anchors.top:parent.top;
        anchors.topMargin: airportsBackground.headerImage.height;
        contentWidth: width
        contentHeight: infoText.y +infoText.paintedHeight+fadeImage.paintedHeight;
        clip:true

        interactive: contentHeight<=height?false:true

        FocusScope{
            id:newsDataContainer;
            ViewText {
                id: headerText;
                anchors.left: parent.left;
                anchors.top:parent.top;
                anchors.topMargin: qsTranslate('','aviapp_airport_title_tm')
                textFormat: Text.StyledText;
                width: qsTranslate('','aviapp_airport_title_w')
                wrapMode: Text.Wrap;
                elide: Text.ElideRight;
                maximumLineCount:1;
                varText: [
                    (airportsListData.get(selectedIndex).airport_header_text_cdata !== "") ? airportsListData.get(selectedIndex).airport_header_text_cdata : airportsListData.get(selectedIndex).airport_header_text,
                                                                                             configTool.config.aviancaapp.screen.text_airport_title,
                                                                                             qsTranslate('','aviapp_airport_title_fontsize')
                ]
            }
            Image {
                id: airportImage;
                anchors.top: headerText.bottom; anchors.topMargin: qsTranslate('','aviapp_airport_img_tm');
                anchors.left: headerText.left;
                source: currAppImages + airportsListData.get(selectedIndex).airport_image;
            }
            Rectangle {
                id:imageBorder;
                anchors.fill: airportImage;
                height: airportImage.height; width: airportImage.width;
                color:"transparent";
                border.width: qsTranslate('','aviapp_airport_img_border_w');
                border.color: qsTranslate('','aviapp_airport_img_border_color');
            }
            ViewText{
                id: infoTextHeader;
                anchors.top:airportImage.bottom;
                anchors.topMargin: qsTranslate('','aviapp_airport_txt_tm');
                anchors.left: airportImage.left;
                width: qsTranslate('','aviapp_airport_txt_w');
                wrapMode: Text.Wrap;
                font.bold: true;
                textFormat: Text.StyledText;
                maximumLineCount:1;
                elide: Text.ElideRight;
                varText: [
                    (airportsListData.get(selectedIndex).airport_list_header_text_cdata !== "") ? airportsListData.get(selectedIndex).airport_list_header_text_cdata : airportsListData.get(selectedIndex).airport_list_header_text,
                                                                                                  configTool.config.aviancaapp.screen.text_airport_txt,
                                                                                                  qsTranslate('','aviapp_airport_txt_fontsize')
                ]
            }
            ViewText{
                id: infoText;
                anchors.top:infoTextHeader.bottom;
                anchors.topMargin: qsTranslate('','aviapp_airport_txt_vgap');
                anchors.left: airportImage.left;
                horizontalAlignment: Text.AlignLeft;
                width: qsTranslate('','aviapp_airport_txt_w');
                //                textFormat: Text.StyledText;
                wrapMode: Text.Wrap;
                varText: [
                    (airportsListData.get(selectedIndex).airport_info_text_cdata !== "") ? airportsListData.get(selectedIndex).airport_info_text_cdata : airportsListData.get(selectedIndex).airport_info_text,
                                                                                           configTool.config.aviancaapp.screen.text_airport_txt,
                                                                                           qsTranslate('','aviapp_airport_txt_fontsize')
                ]
            }
        }
        Behavior on contentY {
            NumberAnimation{duration: 50}
        }
    }

    Image {
        id: fadeImage;
        anchors.left:airpotsFlickableData.left;
        anchors.bottom: airpotsFlickableData.bottom; anchors.bottomMargin: qsTranslate('','aviapp_airport_img_vgap');
        width: airpotsFlickableData.width;
        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_fadeimage;
    }

    ListView {
        id: childAppList;
        width: qsTranslate('','aviapp_sidemenu_itemw')
        height: qsTranslate('','aviapp_sidemenu_itemh')*6;
        anchors.top:parent.top;
        anchors.topMargin: airportsBackground.headerImage.height;
        anchors.right: parent.right;
        //        model:airportsListData;
        delegate: infoListDelegate;
        //        highlightRangeMode:ListView.StrictlyEnforceRange;
        //        preferredHighlightBegin: (childAppList.contentY > 0) ? upArrowContainer.height : 0;
        //        preferredHighlightEnd: childAppList.height;
        boundsBehavior: ListView.StopAtBounds
        clip:true;
        interactive: count>6?true:false;
        onMovementEnded: {
            childAppList.currentIndex=childAppList.indexAt(childAppList.contentX,childAppList.contentY+parseInt(qsTranslate('','aviapp_sidemenu_itemh'),10)+10)
        }
        onCountChanged: {
            selectedIndex=0;
            childAppList.currentIndex = 0;
            childAppList.forceActiveFocus();
        }

        Keys.onUpPressed: {
            var firstIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            if(childAppList.currentIndex == 0 ){
                airportsBackground.closeBtn.forceActiveFocus();
            }else if(upArrowContainer.visible && (childAppList.currentIndex == firstIndex+1)){
                upArrowImage.forceActiveFocus();
            }else{
                childAppList.decrementCurrentIndex();
            }
        }

        Keys.onDownPressed: {
            var lastIndex = childAppList.indexAt(childAppList.contentX+10,(childAppList.contentY+childAppList.height-10))
            if(airportsBackground.closeBtn.activeFocus){
                childAppList.forceActiveFocus();
            }else if((downArrowContainer.visible && currentIndex == lastIndex) ){
                downArrowImage.forceActiveFocus();
            }else{
                childAppList.incrementCurrentIndex();
            }
        }

        Keys.onLeftPressed: {
            if(scroller.visible){
                scroller.forceActiveFocus();
            }
        }
    }

    Component{
        id: infoListDelegate;
        Rectangle{
            id: screenlist;
            height: qsTranslate('','aviapp_sidemenu_itemh');
            width: qsTranslate('','aviapp_sidemenu_itemw');
            color: "transparent"
            opacity: ((Math.floor(childAppList.contentY/screenlist.height) == index) && childAppList.contentY > 0) ? 0 : 1;
            Image {
                id: listImage;
                anchors.fill: parent;
                source: selectedIndex===index?viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_s
                          :viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_n;
            }
            Rectangle{
                id: screenHighlightlist;
                height: qsTranslate('','aviapp_sidemenu_itemh');
                width: qsTranslate('','aviapp_sidemenu_itemw');
                color: "transparent"
                visible: (index==childAppList.currentIndex && childAppList.activeFocus)

                Image {
                    id: listHighlightImage;
                    anchors.fill: parent;
                    source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_h;
                }
            }
            ViewText {
                id:listText;
                width: qsTranslate('','aviapp_sidemenu_text_w');
                wrapMode: Text.Wrap;
                //                opacity: (Math.floor((childAppList.contentY)/screenlist.height == index && (index > 0)) && childAppList.contentY > 0) ? false : true;
                anchors.verticalCenter: parent.verticalCenter;
                //                textFormat: Text.StyledText;
                anchors.left: parent.left; anchors.leftMargin: qsTranslate('','aviapp_sidemenu_text_lm');
                maximumLineCount:2;
                elide: Text.ElideRight

                varText: [
                    airportsListData.get(index).airport_list_text,
                    selectedIndex===index?configTool.config.aviancaapp.screen.text_sidemenu_s:configTool.config.aviancaapp.screen.text_sidemenu_n,
                                           qsTranslate('','aviapp_sidemenu_fontsize') ]
            }


            MouseArea{
                id:infoListMouseArea;
                anchors.fill: parent;
                onPressed: {
                    childAppList.currentIndex=index;
                    childAppList.forceActiveFocus();
                }
                onReleased: {
                    selectedIndex=index;
                    childAppList.currentIndex=index;
                    airpotsFlickableData.contentY = 0;
                    setTitleText(airportsBackground.headerTitle,airportsInfoData,selectedIndex);
                    setMainMenuTitleText(airportsBackground.headerMenuTitle,airportsInfoData,selectedIndex);

                }
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    selectedIndex=index;
                    childAppList.currentIndex=index;
                    airpotsFlickableData.contentY = 0;
                    setTitleText(airportsBackground.headerTitle,airportsInfoData,selectedIndex);
                    setMainMenuTitleText(airportsBackground.headerMenuTitle,airportsInfoData,selectedIndex);
                }
            }
        }
    }
    Rectangle {
        id: upArrowContainer;
        anchors.top: childAppList.top;
        anchors.horizontalCenter: childAppList.horizontalCenter;
        height: qsTranslate('','aviapp_sidemenu_itemh');
        width: qsTranslate('','aviapp_sidemenu_itemw');
        color: "transparent"
        visible: (childAppList.contentY > 0) ? true : false;

        SimpleButton {
            id: upArrowImage;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','aviapp_sidemenu_arrow_tm');
            anchors.horizontalCenter: parent.horizontalCenter;
            isHighlight: activeFocus;
            normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_n;
            highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_h;
            pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_h;
        }
        onVisibleChanged: {
            if(!(upArrowContainer.visible)){
                childAppList.currentIndex =0;
                childAppList.forceActiveFocus();
            }
        }

        MouseArea{
            id:upArrowMouseArea;
            anchors.fill: parent;
            onClicked:  {
                pageUp();
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                pageUp();
            }
        }
        Keys.onDownPressed: {
            childAppList.forceActiveFocus();
        }
        Keys.onUpPressed: {
            airportsBackground.closeBtn.forceActiveFocus();
        }
    }

    Rectangle {
        id: downArrowContainer;
        anchors.top: childAppList.bottom;
        anchors.horizontalCenter: childAppList.horizontalCenter;
        height: qsTranslate('','aviapp_sidemenu_itemh');
        width: qsTranslate('','aviapp_sidemenu_itemw');
        visible:(childAppList.contentY+childAppList.height < childAppList.contentHeight); //((childAppList.count >= 6 ) && (!(childAppList.currentIndex == (childAppList.count - 1)))) ? true : false;
        color: "transparent"

        onVisibleChanged: {
            if(!(downArrowContainer.visible)){
                childAppList.currentIndex = (childAppList.count -1);
                childAppList.forceActiveFocus();
            }
        }
        SimpleButton {
            id: downArrowImage;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','aviapp_sidemenu_arrow_tm');
            anchors.horizontalCenter: parent.horizontalCenter;
            isHighlight: activeFocus;
            normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_n;
            highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_h;
            pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_h;
        }
        MouseArea{
            id:downArrowMouseArea;
            anchors.fill: parent;
            onClicked:  {
                pageDown()
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                pageDown()
            }
        }
        Keys.onUpPressed: {childAppList.forceActiveFocus();}
    }

    Scroller{
        id:scroller
        height: qsTranslate('','aviapp_airport_scroll_h');
        width: qsTranslate('','aviapp_airport_scroll_w');
        anchors.left:airpotsFlickableData.right;
        anchors.leftMargin:qsTranslate('','aviapp_airport_scroll_lm');
        anchors.verticalCenter: airpotsFlickableData.verticalCenter;
        sliderbg:[qsTranslate('','aviapp_airport_scroll_base_margin'),qsTranslate('','aviapp_airport_scroll_w'),qsTranslate('','aviapp_airport_scroll_base_h'),configTool.config.aviancaapp.screen.scrollbg]
        scrollerDot: configTool.config.aviancaapp.screen.slider
        upArrowDetails:[configTool.config.aviancaapp.screen.app_arwup_n, configTool.config.aviancaapp.screen.app_arwup_h, configTool.config.aviancaapp.screen.app_arwup_h ]
        downArrowDetails:[configTool.config.aviancaapp.screen.app_arwdwn_n, configTool.config.aviancaapp.screen.app_arwdwn_h, configTool.config.aviancaapp.screen.app_arwdwn_h]

        targetData:airpotsFlickableData;

        Keys.onRightPressed: {
            if(downArrowContainer.visible){
                downArrowContainer.forceActiveFocus();
            }else{
                childAppList.forceActiveFocus();
            }
        }
    }
}

