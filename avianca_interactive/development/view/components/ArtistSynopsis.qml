import QtQuick 1.1

FocusScope {
    id:artistSynopsis;
    anchors.horizontalCenter:parent.horizontalCenter;
    property variant configValue:configTool.config.music.artist;
    property variant configTag:configTool.config.music.tracklist;
    property variant labelTag:core.configTool.language.music;
    property variant selector:["","11","21"]
    visible:false;
    ListModel{id:artistModel;}
    /**************************************************************************************************************************/
    Connections{
        target:albumLoader.item;
        onCustomClose:{
            albumClose();
        }
        onCustomLeft:{
            albumView.forceActiveFocus();
        }
        onAnimStart:{
            openAnim.restart();
        }
    }
    /**************************************************************************************************************************/
    function init(){
        core.debug("Albumsynopsis.qml |  init")
        var params=new Object();
        params['cid']           = getAudioCid();
        params['template_id']   = getAudioTid();
        params['inputSearch']   = ""
        params['searchType']    = "NA"  ;
        params['fieldList']     = 'artist';
        params['searchBy']      = 'artist'
        params['orderBy']       = 'artist';
        params['groupBy']       = 'artist'
        dataController.keywordSearch.getData(params,artistListReady,99);
        mediaList = artistSynopsis;
    }
    function getMediaInfo(tag){
        return albumView.model.getValue(albumView.selIndex,tag)
    }
    function artistListReady(dmodel){
        if(dmodel.count==0){
            viewController.showScreenPopup(7);
            closeLoader();
            return;
        }
        visible=true;
        artistModel.clear();
        var b = [];
        for(var i =0;i< dmodel.count;i++){
            var a = dmodel.getValue(i,"artist");
            artistModel.append({"title":a,"alpha":a[0]})
            if(!b[a[0]]){
                b[a[0]]=1;
                artistView.sectionCount++;
            }
        }
        artistView.currentIndex=0;
        artistView.model=artistModel;
        artistView.select();
    }
    function albumListReady(dmodel){
        albumView.currentIndex=0;
        albumView.selIndex =  -1;
        albumView.model=dmodel;
        if(!isModelUpdated)
            albumView.forceActiveFocus();
    }
    function clearOnExit(){
        core.debug("ArtistSynopsis.qml | clearOnExit ")
        albumView.selIndex=-1;
        artistView.selIndex=-1;
    }
    function backBtnPressed(){
        if(albumLoader.item.backBtnPressed){
            albumLoader.item.backBtnPressed();
        }else{
            viewHelper.homeAction();
        }
    }
    function albumClose(){
        closeAnim.restart();
    }

    function langUpdate(){
        if(albumLoader.item.langUpdate){
            albumLoader.item.langUpdate();
        }
    }
    /**************************************************************************************************************************/
    Image{
        id:panel;
        source:viewHelper.configToolImagePath+configValue["list_panel"];
        anchors.horizontalCenter:parent.horizontalCenter;
        y:qsTranslate('',"artist_art_panel_tm");

    }
    VerticalView{
        id:artistView;
        anchors.left:panel.left;
        anchors.top: panel.top;
        anchors.topMargin:qsTranslate('','artist_art_view_tm')
        anchors.leftMargin:qsTranslate('','artist_art_view_lm')
        width:qsTranslate('','artist_art_view_w');
        height:qsTranslate('','artist_art_view_h');
        property int sectionCount;
        property bool isNav: false
        cacheBuffer:(height*2)
        snapMode: ListView.SnapToItem
        highlightRangeMode: ListView.NoHighlightRange
        property int conHeight:(artistView.count+sectionCount)*parseInt(qsTranslate('','artist_section_h'),10)
        property int selIndex:-1;
        Keys.onUpPressed:{
            isNav=true
            if(currentIndex==0){
                subCatobj.forceActiveFocus()
            }else{
                decrementCurrentIndex();
            }
        }
        Keys.onDownPressed: {
            isNav=true
            if(currentIndex!=count-1)incrementCurrentIndex()
        }
        Keys.onRightPressed: {
            if(artistScroll.visible){
                artistScroll.forceActiveFocus()
            }else{
                albumView.forceActiveFocus();
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                select();
            }
        }
        clip:true;
        section.property:"alpha"
        section.criteria: ViewSection.FullString
        section.delegate:Rectangle{
            color:configValue["alpha_bg_color"];
            width:qsTranslate('','artist_section_w');
            height:qsTranslate('','artist_section_h');
            ViewText{
                id:alphaText;
                x:qsTranslate('','artist_sectiontext_lm');
                width:qsTranslate('','artist_sectiontext_w');
                height:qsTranslate('','artist_sectiontext_h');
                anchors.verticalCenter:parent.verticalCenter;
                //text:section;
                elide: Text.ElideRight;
                varText:[section,configValue["alpha_text_color"],qsTranslate('','artist_sectiontext_fontsize')]
            }

        }
        delegate:Item{
            id:artistSection;
            width:qsTranslate('','artist_section_w');
            height:qsTranslate('','artist_section_h');
            Image{
                id:high
                source:viewHelper.configToolImagePath+configValue["list_highlight"];
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('','artist_tracks_high_bm')
                height:qsTranslate('','artist_tracks_high_h');
                width:parent.width;
                opacity:(artistView.currentIndex==index && artistView.activeFocus)?1:0

            }
            ViewText{
                x:qsTranslate('','artist_sectiontext_lm');
                width:qsTranslate('','artist_sectiontext_w');
                height:qsTranslate('','artist_sectiontext_h');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[title,(artistView.selIndex==index)?core.configTool.config.music.tracklist["title_color"]:core.configTool.config.music.tracklist["description1_color"],qsTranslate('','artist_sectiontext_fontsize')]
                elide:Text.ElideRight;
            }
            Rectangle{
                height:qsTranslate('','common_line_h');
                width:parent.width;
                anchors.bottom:parent.bottom;
                color:core.configTool.config.music.tracklist.description1_color;
            }
            MouseArea{
                anchors.fill:high
                onClicked:{
                    artistView.currentIndex=index;
                    artistView.select();
                }
            }

        }
        function select(){

            if(selIndex==currentIndex)return;
            selIndex=currentIndex;
            var params=new Object();
            params['cid']           = getAudioCid();
            params['template_id']   = getAudioTid();
            params['inputSearch']   = (artistView.model.get(artistView.currentIndex).title).toLowerCase();
            params['searchType']    = "full"  ;
            params['fieldList']     = 'title,artist,mid,rating,media_poster_filename_'+selector[core.settings.resolutionIndex]+' as poster';
            params['searchBy']      = 'artist'
            params['orderBy']       = 'title';
            params['groupBy']       = ''

            dataController.keywordSearch.getData(params,albumListReady,99);
            artistTitle.text = artistView.model.get(artistView.currentIndex).title;

        }
        onMovementStarted: isNav=false
        onContentYChanged: {
            if(isNav){return;}
            currentIndex=indexAt(contentX,contentY+5)
            if(currentIndex==-1) currentIndex=indexAt(contentX,contentY+parseInt(qsTranslate('','artist_section_h'),10)+5)
        }
    }
    MouseArea{
        anchors.fill:artistView
        enabled:(albumLoader.sourceComponent != blankComp)
        onClicked:{}
    }
    Rectangle{
        height:qsTranslate('','common_line_h');
        width:albumView.width;
        anchors.bottom:albumView.top;
        anchors.left:albumView.left;
        color:core.configTool.config.music.tracklist.description1_color;
    }
    Scroller{
        id:artistScroll
        targetData:artistView;
        sliderbg:[qsTranslate('','artist_scrollcont_base_margin'),qsTranslate('','artist_scrollcont_w'),qsTranslate('','artist_scrollcont_base_h'),configTag["scroll_base"]]
        scrollerDot:configTag["scroll_button"]
        upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
        downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
        anchors.left:artistView.right;
        anchors.leftMargin:qsTranslate('','artist_scrollcont_lm')
        height: qsTranslate('','artist_scrollcont_h');
        width: qsTranslate('','artist_scrollcont_w');
        anchors.verticalCenter: artistView.verticalCenter;
        targetConHeight:artistView.conHeight;
        property variant ratioY:targetData.contentY/targetConHeight;
        onSliderNavigation:{
            if(direction=="left"){
                artistView.forceActiveFocus()
            }else if(direction=="right"){
                albumView.forceActiveFocus();
            }else if(direction=="up"){
                subCatobj.forceActiveFocus();
            }

        }
        onArrowPressed: artistView.isNav=false

    }
    Image{
        id:albmPanel
        source:viewHelper.configToolImagePath+configValue["albumlist_panel"];
        property int startX:qsTranslate('','artist_albm_panel_lm1');
        property int endX:qsTranslate('','artist_albm_panel_lm2');
        x:startX;

        anchors.top:panel.top;
        opacity:(albumLoader.sourceComponent == blankComp)?0:1;

    }
    ViewText{
        id:artistTitle;
        anchors.left:albmPanel.left
        anchors.leftMargin:qsTranslate('','artist_title_lm');
        anchors.top:panel.top
        anchors.topMargin:qsTranslate('','artist_title_tm');
        height:qsTranslate('','artist_title_h');
        width:qsTranslate('','artist_title_w');
        varText:["",configTag["title_color"],qsTranslate('','artist_title_fontsize')]
    }
    ViewText{
        id:info;
        anchors.left:artistTitle.left
        anchors.top:artistTitle.bottom
        anchors.topMargin:qsTranslate('','artist_subtitle_tm');
        height:qsTranslate('','artist_subtitle_h');
        width:qsTranslate('','artist_subtitle_w');
        varText:[albCnt,configTag["title_color"],qsTranslate('','artist_subtitle_fontsize')]
        property string albCnt:(albumView.model.count>1)?albumView.model.count+" "+labelTag["text_albums"]:albumView.model.count+" Album"
    }
    VerticalView{
        id:albumView;
        anchors.left:artistTitle.left;
        anchors.top: info.bottom;
        anchors.topMargin:qsTranslate('','artist_albumview_tm')
        width:qsTranslate('','artist_albumview_w');
        height:qsTranslate('','artist_albumview_h');
        property int selIndex:-1;
        clip:true;
        Keys.onUpPressed:{
            if(currentItem.y==contentY){
                subCatobj.forceActiveFocus()
            }else{
                decrementCurrentIndex();
            }
        }
        Keys.onLeftPressed: {
            if(albumLoader.sourceComponent != blankComp)return;
            if(artistScroll.visible){
                if(artistScroll.upArrow.isDisable)
                    artistScroll.downArrow.focus=true
                artistScroll.forceActiveFocus()
            }else{
                artistView.forceActiveFocus()
            }
        }

        Keys.onRightPressed:{
            if(albumLoader.sourceComponent!=blankComp){
                albumLoader.item.specialFocus("left");
            }else if(albumScroll.visible){
                albumScroll.forceActiveFocus();
            }
        }

        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                select();
            }
        }



        delegate:Item{
            id:albumSection;
            width:qsTranslate('','artist_albums_w');
            height:qsTranslate('','artist_albums_h');
            Image{
                id:highalbum
                source:viewHelper.configToolImagePath+configValue["albumlist_highlight"];
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('','artist_tracks_high_bm')
                height:qsTranslate('','artist_tracks_high_h');
                width:parent.width;
                opacity:(albumView.currentIndex==index && albumView.activeFocus)?1:0

            }
            ViewText{
                x:qsTranslate('','artist_albumtitle_lm');
                width:qsTranslate('','artist_albumtitle_w');
                height:qsTranslate('','artist_albumtitle_h');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[title,(albumView.selIndex==index)?core.configTool.config.music.tracklist["title_color"]:core.configTool.config.music.tracklist["description1_color"],qsTranslate('','artist_sectiontext_fontsize')]
                elide:Text.ElideRight;
            }
            Rectangle{
                height:qsTranslate('','common_line_h');
                width:parent.width;
                anchors.bottom:parent.bottom;
                color:core.configTool.config.music.tracklist.description1_color;
            }
            MouseArea{
                anchors.fill:highalbum
                onClicked:{
                    albumView.currentIndex=index;
                    albumView.select();
                }
            }

        }

        function select(){
            albumView.forceActiveFocus();
            if(selIndex ==  currentIndex)return;
            selIndex =  currentIndex;
            if(albumLoader.sourceComponent == musicAlbumSynopsis){
                albumLoader.item.init();
            }else{
                albumLoader.sourceComponent = musicAlbumSynopsis;
            }


        }

    }
    Scroller{
        id:albumScroll
        targetData:albumView;
        sliderbg:[qsTranslate('','artist_scrollcont_base_margin'),qsTranslate('','artist_scrollcont_w'),qsTranslate('','artist_scrollcont_base_h'),configTag["scroll_base"]]
        scrollerDot:configTag["scroll_button"]
        upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
        downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
        anchors.left:albumView.right;
        anchors.leftMargin:qsTranslate('','artist_scrollcont_lm')
        height: qsTranslate('','artist_scrollcont_h');
        width: qsTranslate('','artist_scrollcont_w');
        anchors.verticalCenter: artistView.verticalCenter;
        onSliderNavigation:{
            if(direction=="left"){
                albumView.forceActiveFocus()
            }
        }

    }
    Loader{
        id:albumLoader;
        sourceComponent:blankComp;
        property int startX:parseInt(qsTranslate('','artist_trackpanel_lm1'),10)
        property int endX:parseInt(qsTranslate('','artist_trackpanel_lm2'),10)
        x:startX
        opacity:0.5;
        onStatusChanged: {
            if(albumLoader.sourceComponent==blankComp)return;
            if (albumLoader.status == Loader.Ready){
                viewHelper.setHelpTemplate("artist_traklist");
                albumLoader.item.init(true);

                //openAnim.restart();
            }
        }
    }
    SequentialAnimation{
        id:openAnim
        ScriptAction{
            script:{
                albumLoader.x = albumLoader.endX;
                albmPanel.x=albmPanel.endX
                albumLoader.opacity=1;
            }
        }
    }
    SequentialAnimation{
        id:closeAnim
        ScriptAction{
            script:{
                viewHelper.setHelpTemplate("artist_listing");
                albumLoader.sourceComponent = blankComp;
                albumView.selIndex =  -1;
                albumView.forceActiveFocus();
            }
        }
        ScriptAction{
            script:{
                albumLoader.x = albumLoader.startX;
                albmPanel.x=albmPanel.startX
                albumLoader.opacity=0;

            }
        }
        //        ParallelAnimation{
        //            PropertyAnimation{target:albumLoader;from:albumLoader.endX;to:albumLoader.startX;duration:10;property:"x"}
        //            PropertyAnimation{target:albmPanel;from:albmPanel.endX;to:albmPanel.startX;duration:10;property:"x"}
        //            PropertyAnimation{target:albumLoader;from:1;to:0.5;duration:10;property:"opacity"}

        //        }

    }
}
