// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id:appBackground;

    property string appBackgroundImage;
    property string appHeaderImage;
    property string appSideMenuPanelImage;
    property string appHeaderTitle;
    property string appHeaderMenuTitle;

    property alias backgroundImage: backgroundImage;
    property alias headerImage: headerImage;
    property alias sideMenuPanelImage: sideMenuPanelImage;
    property alias closeBtn: closeBtn;
    property alias headerTitle: headerTitle;
    property alias headerMenuTitle: headerMenuTitle;

    height: core.height; width: core.width;
    color: "transparent";

    onAppHeaderTitleChanged: {
        core.log(">>>>> .. App Header Title Changed | value... ")
    }
    onAppHeaderMenuTitleChanged: {
         core.log(">>>>> .. App Header Menu Title Changed | value... ")
    }

    Image {
        id: backgroundImage;
        anchors.fill: parent;
        source: viewHelper.configToolImagePath+appBackgroundImage;
    }
    Image {
        id: headerImage;
        source: viewHelper.configToolImagePath+appHeaderImage;
    }
    Image {
        id: sideMenuPanelImage;
        anchors.top:headerImage.bottom; anchors.right: parent.right;
        source: viewHelper.configToolImagePath+appSideMenuPanelImage;
    }
    SimpleButton {
        id: closeBtn;
        increaseTouchArea: true
        increaseValue:-60
        anchors.right: headerImage.right; anchors.top:headerImage.top;
        anchors.rightMargin: qsTranslate('','aviapp_header_close_gap');
        anchors.topMargin: qsTranslate('','aviapp_header_close_gap');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.app_menubtn_p;
        onEnteredReleased: {
            core.log(">>> AviancaAppBg | Close Button Clicked");
            isChildAppSelected = true;
//            currAppName="mainMenu";
            viewController.getActiveScreenRef().actionOnExit(true);
           // viewController.showScreenPopup(23);
        }        
    }
    ViewText {
        id: headerTitle;
        width: qsTranslate('','aviapp_header_title_w');
        x: qsTranslate('','aviapp_header_title_x');
        anchors.verticalCenter: headerImage.verticalCenter;
        wrapMode: Text.Wrap;
        textFormat: Text.StyledText;
        maximumLineCount:1;
        clip: true;
        varText: [
            appHeaderTitle,
            configTool.config.aviancaapp.screen.text_header_title,
            qsTranslate('','aviapp_header_title_fontsize') ];
    }
    ViewText {
        id: headerMenuTitle;
        width: qsTranslate('','aviapp_header_menu_w');
        anchors.top:headerImage.top; anchors.topMargin: qsTranslate('','aviapp_header_menu_y');
        anchors.right: headerImage.right; anchors.rightMargin: qsTranslate('','aviapp_header_menu_rm');
//        anchors.verticalCenter: headerImage.verticalCenter;
        wrapMode: Text.Wrap;
        textFormat: Text.StyledText;
        maximumLineCount:1;
        clip: true;
        varText: [
            appHeaderMenuTitle,
            configTool.config.aviancaapp.screen.text_header_menu,
            qsTranslate('','aviapp_header_menu_fontsize')
        ];
        MouseArea{
            anchors.fill: parent
            onClicked: {
                isChildAppSelected = true;
                viewController.getActiveScreenRef().actionOnExit(true);
            }
        }
    }
}
