import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

FocusScope {
    width: core.height;
    height: core.width;

    property bool  isContinuouslyPressed:false
    property bool __modelUpdate:true

    function init() {
        core.log("ToursSelection | init")
        vmenu.currentIndex = selCountryScreenIndex;
        microappMain.refreshModels();
        vmenu.forceActiveFocus();
        modelUpdate()
    }
    function setIndex(){
        vmenu.currentIndex = 0;
        vmenu.forceActiveFocus();
    }
    function modelUpdate(){
       __modelUpdate=false;
       __delayUpdate.restart();
    }
    Timer{id:__delayUpdate;interval:500;onTriggered: __modelUpdate=true}
    Image {
        id: backgroungImage;
        source: microappMain.currAppImages + microappMain.tourAppData.get(0).bgImage;
    }

    Image {
        id: conttoursBg;
        anchors.top: backgroungImage.top;
        anchors.right: backgroungImage.right;
        anchors.rightMargin: qsTranslate('','tours_info_x');

        source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.conttours_bg;
    }

    SimpleButton {
        id: closeBtn;
        increaseTouchArea: true
        increaseValue:-60
        anchors.top: backgroungImage.top;
        anchors.right: backgroungImage.right;
        anchors.topMargin: qsTranslate('','tours_close_y');
        anchors.rightMargin: qsTranslate('','tours_close_x');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.close_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.close_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.close_h;

        onEnteredReleased: {viewController.showScreenPopup(23);}
        Keys.onDownPressed: {
            if(listContainer.vmenu.count > 1) upArrow.forceActiveFocus();
            else listContainer.vmenu.forceActiveFocus();
        }
    }

    Image {
        id: logo;
        anchors.horizontalCenter: conttoursBg.horizontalCenter;
        anchors.top: conttoursBg.top;
        anchors.topMargin: qsTranslate('','tours_info_logo_tm');
        //        source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.welcome_logo;
        source: microappMain.currAppImages + microappMain.tourAppData.get(0).logoImage;
    }

    Rectangle{
        id: welcomeTextContainer;
        height: qsTranslate('','tours_info_txt_h');
        width: qsTranslate('','tours_info_txt_w');
        anchors.top: logo.bottom;
        anchors.horizontalCenter: conttoursBg.horizontalCenter;
        anchors.topMargin: qsTranslate('','tours_info_txt_vgap');

        ViewText{
            id: welcomeText;
            horizontalAlignment: Text.AlignHCenter;
            verticalAlignment: Text.AlignVCenter;
            width: parent.width;
            lineHeightMode:Text.FixedHeight;
            lineHeight:qsTranslate('','tours_info_lh');
            wrapMode: Text.Wrap;
            textFormat: Text.StyledText;
            elide: Text.ElideRight;
            maximumLineCount:2;
            varText:(!__modelUpdate)?[]: [ (microappMain.tourAppData.get(0).welcomeText_cdata != "") ? microappMain.tourAppData.get(0).welcomeText_cdata : microappMain.tourAppData.get(0).welcomeText,
                                                                                   configTool.config.aviancatours.screen.text_info,
                                                                                   qsTranslate('','tours_info_fontsize')
            ]
        }

        ViewText{
            id: additionalWelcomeText;
            anchors.top: welcomeText.bottom;
            horizontalAlignment: Text.AlignHCenter;
            verticalAlignment: Text.AlignVCenter;
            width: parent.width;
            wrapMode: Text.Wrap;
            textFormat: Text.StyledText;
            elide: Text.ElideRight;
            maximumLineCount:2;
            varText:(! __modelUpdate)?[]:[ (microappMain.tourAppData.get(0).additionalInfoText_cdata != "") ? microappMain.tourAppData.get(0).additionalInfoText_cdata : microappMain.tourAppData.get(0).additionalInfoText,
                                                                                          configTool.config.aviancatours.screen.text_info,
                                                                                          qsTranslate('','tours_info_fontsize1')
            ]
        }
    }

    SimpleButton {
        id: upArrow;
        anchors.bottom: listContainer.top;
        anchors.horizontalCenter: conttoursBg.horizontalCenter;
        anchors.bottomMargin: qsTranslate('','tours_conttours_vgap');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.up_btn_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.up_btn_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.up_btn_h;
        isDisable: (listContainer.vmenu.count > 1) ? false : true
        visible: (listContainer.vmenu.count > 1)?true:false
        onEnteredReleased: {listContainer.vmenu.decrementCurrentIndex();}
        Keys.onUpPressed: {closeBtn.forceActiveFocus();}
        Keys.onDownPressed: {listContainer.vmenu.forceActiveFocus();}
    }

    SimpleButton {
        id: downArrow;
        anchors.top: listContainer.bottom;
        anchors.horizontalCenter: conttoursBg.horizontalCenter;
        anchors.topMargin: qsTranslate('','tours_conttours_vgap');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.down_btn_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.down_btn_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.down_btn_h;
        isDisable: (listContainer.vmenu.count > 1) ? false : true
        visible: (listContainer.vmenu.count > 1)?true:false
        onEnteredReleased: {listContainer.vmenu.incrementCurrentIndex();}
        Keys.onUpPressed: {listContainer.vmenu.forceActiveFocus();}
    }

    Item{
        id: listContainer;
        property alias vmenu: vmenu;
        anchors.top: conttoursBg.top;
        anchors.horizontalCenter: conttoursBg.horizontalCenter;
        anchors.topMargin:qsTranslate('','tours_conttours_tm')
        width:  qsTranslate('','tours_conttours_w')
        height:  qsTranslate('','tours_conttours_h')

        Path{
            id:menupath
            //first point
            startX: vmenu.width/2
            startY: 0
            PathAttribute{name: 'fontSize'; value:qsTranslate('','tours_conttours_fontsize')}
            //second point
            PathLine{x:vmenu.width/2; y:vmenu.height*0.5}
            PathAttribute{name: 'fontSize'; value: qsTranslate('','tours_conttours_fontsize1')}
            //third point
            PathLine{x:vmenu.width/2; y:vmenu.height}
            PathAttribute{name: 'fontSize'; value: qsTranslate('','tours_conttours_fontsize')}
        }
        PathView{
            id:vmenu
            focus: true
            width:  parent.width;
            height: parent.height;
            path:menupath;
            preferredHighlightBegin: vmenu.count==2?0.60:0.5
            preferredHighlightEnd: vmenu.count==2?0.60:0.5
            highlightMoveDuration: 200
            pathItemCount:3;//(model.count<5)?3:5;
            model:microappMain.selectionListModel;
            delegate: countryDelegate;
            onCountChanged: {
                vmenu.currentIndex = 0;
                vmenu.forceActiveFocus();
            }



            Keys.onReleased: {isContinuouslyPressed=false}
            Keys.onUpPressed: {
                if(isContinuouslyPressed)return
                if(listContainer.vmenu.count > 1) upArrow.forceActiveFocus();
                else closeBtn.forceActiveFocus();
                isContinuouslyPressed=true
            }
            Keys.onDownPressed: {
                if(isContinuouslyPressed)return
                if(listContainer.vmenu.count > 1) downArrow.forceActiveFocus();
                isContinuouslyPressed=true
            }


        }
        Component{
            id: countryDelegate;
            ViewText{
                id: countryText;
                width: qsTranslate('','tours_conttours_txt_w');
                horizontalAlignment: Text.AlignHCenter;
                wrapMode: Text.Wrap;
                maximumLineCount:1;
                elide: Text.ElideRight;
                varText: [
                    microappMain.selectionListModel.get(index).countryName,
                    ( ( (vmenu.currentIndex == index && (vmenu.activeFocus || upArrow.activeFocus || downArrow.activeFocus ))  ) ? configTool.config.aviancatours.screen.text_conttours_h : configTool.config.aviancatours.screen.text_conttours_n),
                    PathView.fontSize
                ]

                MouseArea{
                    anchors.fill: parent
                    onReleased:  {
                        vmenu.currentIndex=index;
                        selCountryScreenIndex = index;
                        selCountryScreenID = microappMain.selectionListModel.get(index).screenLink;
                        microappMain.childLoader.sourceComponent = appListing;
                    }
                }
                Keys.onReleased: {
                    if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                        vmenu.currentIndex=index;
                        selCountryScreenIndex = index;
                        selCountryScreenID = microappMain.selectionListModel.get(index).screenLink;
                        microappMain.childLoader.sourceComponent = appListing;
                    }
                }
            }
        }
    }
}
