import QtQuick 1.0
import "../../framework/viewcontroller"
import "../components"

Item{
    anchors.fill: parent
    property alias synopsisContentFlickable: synopsisContentFlickable
    Rectangle{
        id:divline
        anchors.bottom: synopsisContentFlickable.top
        anchors.bottomMargin:     qsTranslate("",'episode_episode_high_bm')
        anchors.left: synopsisContentFlickable.left
        width: synopsisContentFlickable.width
        height: qsTranslate("",'common_line_h')
        color: configTool.config[currentTemplateNode].synopsis.description1_color
    }
    ListView{
        function changeCurrentIndex(){
            synopsisContentFlickable.currentIndex=indexAt(5,contentY+5)
            synopsisContentFlickable.forceActiveFocus()
        }

        id:synopsisContentFlickable
        clip: true
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','episode_episodeview_tm')
        anchors.left:parent.left
        anchors.leftMargin: qsTranslate('','episode_episode_title_lm')
        width: qsTranslate('','episode_episodeview_w')
        height: qsTranslate('','episode_episodeview_h')
        model:tvModel
        delegate: episodeDelegate
        Keys.onRightPressed: {
            if(slider.visible){
                if(slider.downArrow.isDisable)slider.upArrow.focus=true
                if(slider.upArrow.isDisable)  slider.downArrow.forceActiveFocus()
                else slider.forceActiveFocus()
            }
            else synopsisCloseBtn.forceActiveFocus()
        }
        Keys.onUpPressed: {
            if( synopsisContentFlickable.currentIndex!=0) {
                if(isEpisodePlayBtn) isEpisodePlayBtn=false
                decrementCurrentIndex()
                synopsisContentFlickable.forceActiveFocus()
            }
            else synopsisCloseBtn.forceActiveFocus()
        }
        Keys.onDownPressed:  {
            if(synopsisContentFlickable.currentIndex!=(count-1)) {
                if(isEpisodePlayBtn )isEpisodePlayBtn=false
                incrementCurrentIndex()
                synopsisContentFlickable.forceActiveFocus()
            }
        }
        Keys.onLeftPressed: {
            playAllBtn.forceActiveFocus()
        }
        //onMovementStarted: isEpisodePlayBtn=false //Done for customer request
        onMovementEnded: changeCurrentIndex()
        onCurrentIndexChanged:  getTvDetails()
        onModelChanged: {currentIndex=0;isEpisodePlayBtn=false}
    }
    SimpleNavBrdrBtn{
        id:playAllBtn
        property string textColor: playAllBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(playAllBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','episode_playall_tm')
        anchors.left: parent.left
        anchors.leftMargin: qsTranslate('','episode_playall_lm')
        width:  qsTranslate('','episode_playall_w')
        height:  qsTranslate('','episode_playall_h')
        btnTextWidth:parseInt(qsTranslate('',"episode_playall_textw"),10);
        buttonText: [configTool.language.tv.tv_play_all,playAllBtn.textColor,qsTranslate('','episode_playall_fontsize')]
        isHighlight: activeFocus
        normalImg:  viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
        normalBorder: [qsTranslate('','episode_playall_margin')]
        highlightBorder: [qsTranslate('','episode_playall_margin')]
        nav: ["","",synopsisContentFlickable,""]
        onNoItemFound:  if(dir=="left"){extremeEnds("left")}
        onEnteredReleased:{
            var mid_status = dataController.mediaServices.getMidBlockStatus( listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"mid"),listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"rating"));
            if(mid_status){viewController.updateSystemPopupQ(7,true);return;}
            viewHelper.playingCategory="tv";
            viewHelper.isPreviousEnabled=true
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory})
            if(pif.getMidInfo(model.getValue(currentIndex,"mid"),'elapseTime')>viewHelper.resumeSecs)  viewController.showScreenPopup(2)
            else loadVod("play","tvseries")
        }
    }
    ViewText{
        id:synopsisDetailsText
        anchors.top:parent.top
        anchors.topMargin:   qsTranslate('','episode_episode_desc3_tm')
        anchors.left: parent.left
        anchors.leftMargin: qsTranslate('','synopsis_'+viewHelper.mapMenuTid(widgetList.getCidTidMainMenu()[1],true)+'_poster_lm')
        width:   qsTranslate('','episode_episode_desc3_w')
        height:   qsTranslate('','episode_episode_desc3_h')
        varText: [text,configTool.config[currentTemplateNode].synopsis.description2_color,qsTranslate('','episode_episode_desc3_fontsize')]
        text:synopsisDetails+((tvModel.getValue(synopsisContentFlickable.currentIndex,"duration")!="")?configTool.language.tv.min:"")
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
        maximumLineCount: 2
    }
    SimpleBorderBtn{
        id:textOverlay
        visible:slider.visible?(slider.downArrow.isDisable?false:true):false
        width:synopsisContentFlickable.width+(parseInt(qsTranslate('','common_scrollfade_extra_w'),10))
        normalBorder: [qsTranslate('','common_scrollfade_extra_margin')]
        anchors.bottom: synopsisContentFlickable.bottom
        anchors.horizontalCenter: synopsisContentFlickable.horizontalCenter
        source:  viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1],true)].synopsis.fading_image
        MouseArea{anchors.fill:parent;onClicked:{}}
    }
}
