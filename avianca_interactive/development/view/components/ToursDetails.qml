import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

FocusScope {
    width: core.height;
    height: core.width;

    property string selectedDeatailId : detailScreenId;
    property variant configTour: configTool.config.aviancatours.screen;
    property bool isPressed: false;
    property int lastSelectedFocus : 0; // variable to select last selected focus for close btn
    property int indexOnPaging;
    property bool isPaging: false;

    function init() {
        core.debug("ToursDetails | init ");
        lastSelectedFocus = 0;
        isPaging = false;
        indexOnPaging = 0;
        if(synopsisLV.count > 0){
            synopsisLV.currentIndex = 0;
            synopsisLV.forceActiveFocus();
        }else{
            closeBtn.forceActiveFocus()
        }

    }
    function setListingIndex(){
        indexOnPaging = (synopsisLV.indexAt((synopsisLV.contentX+20),(synopsisLV.contentY+20)));
        if((synopsisLV.currentIndex >= indexOnPaging) && (synopsisLV.currentIndex <= (indexOnPaging+5))){
            isPaging = false;
        }
    }

    function setPosterImage(obj, Dmodel){
        obj.source=""
        obj.source = microappMain.currAppImages + Dmodel.get(0).posterImg;
    }
    function setLogoImage(obj, Dmodel){
        obj.source=""
        obj.source = microappMain.currAppImages + Dmodel.get(0).detailLogoImg;
    }
    function setBgImage(obj, Dmodel){
        obj.source=""
        obj.source = microappMain.currAppImages + Dmodel.get(0).detailBgImg;
    }

    function setHeadingText(obj,Dmodel){
        obj.text = (Dmodel.get(0).detailHeadingText_cdata !== "") ? Dmodel.get(0).detailHeadingText_cdata : Dmodel.get(0).detailHeadingText;
        obj.color = configTool.config.aviancatours.screen.text_title;
        obj.font.pixelSize = qsTranslate('','tours_placesyn_fontsize');
    }
    function modelUpdate(){
       detailScreenHeadingModel.reload()
       detailScreenModel.reload()
       synopsisLV.modelUpdate=false;
       __delayUpdate.restart();
     }
     Timer{id:__delayUpdate;interval:500;onTriggered: synopsisLV.modelUpdate=true}

    XmlListModel{
        id: detailScreenHeadingModel;
        source: currAppName ? xmlModelSourceOrg : "";
        query: "/application/screen[@_id='"+detailScreenId+"']";

        XmlRole { name : "detailHeadingText"; query: "text3/"+langCode+"/@_value/string()" }
        XmlRole { name : "detailHeadingText_cdata"; query: "text3/"+langCode+"/string()" }
        XmlRole { name : "detailBgImg"; query: "image1/@_value/string()" }
        XmlRole { name : "detailLogoImg"; query: "image2/@_value/string()" }
        XmlRole { name : "posterImg"; query: "image3/@_value/string()" }

        onCountChanged: {
            setBgImage(listingBg, detailScreenHeadingModel);
            setPosterImage(detailScreenPoster, detailScreenHeadingModel);
            setLogoImage(detailLogo, detailScreenHeadingModel);
            setHeadingText(headingText, detailScreenHeadingModel);
        }
    }

    XmlListModel{
        id: detailScreenModel;
        source: currAppName ? xmlModelSourceOrg : "";
        query: "/application/screen[@_id='"+detailScreenId+"']/list1/listItem";

        XmlRole { name : "infoHeadingText"; query: "text8/"+langCode+"/@_value/string()" }
        XmlRole { name : "infoHeadingText_cdata"; query: "text8/"+langCode+"/string()" }
        XmlRole { name : "infoText"; query: "text9/"+langCode+"/@_value/string()" }
        XmlRole { name : "infoText_cdata"; query: "text9/"+langCode+"/string()" }
    }

    Image {
        id: listingBg;
        source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.synposis_bg;
    }

    SimpleButton {
        id: closeBtn;
        increaseTouchArea: true
        increaseValue:-60
        anchors.top: listingBg.top;
        anchors.right: listingBg.right;
        anchors.topMargin: qsTranslate('','tours_close_y');
        anchors.rightMargin: qsTranslate('','tours_close_x');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.close_app_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.close_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.close_h;

        onEnteredReleased: {
            viewController.getActiveScreenRef().actionOnExit(true);
            //viewController.showScreenPopup(23);
        }
        Keys.onDownPressed: {
            if((synopsisLV.model.count > 0) && (lastSelectedFocus == 1)){
                synopsisLV.forceActiveFocus();
            }else if((scroller.visible) && (lastSelectedFocus == 0)){
                scroller.forceActiveFocus();
            }else{
                synopsisLV.forceActiveFocus();
            }
        }
    }

    Image {
        id: detailScreenPoster;
        anchors.top: listBg.top;
        anchors.left: listBg.left;
    }

    Image {
        id: detailLogo;
        anchors.top:detailScreenPoster.top;
        anchors.left: detailScreenPoster.left;
        anchors.topMargin: qsTranslate('','tours_placesyn_logo_tm');
        anchors.leftMargin: qsTranslate('','tours_placesyn_logo_lm');
        source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.placelist_logo;
    }
    Image {
        id: listBg;
        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.topMargin: qsTranslate('','tours_placesyn_bg_tm');
        source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.list_bg;
    }

    ViewText{
        id: headingText;
        anchors.left: detailScreenPoster.right;
        anchors.top: listBg.top;
        anchors.topMargin: qsTranslate('','tours_placesyn_title_tm');
        anchors.leftMargin: qsTranslate('','tours_placesyn_title_lm');
        width: qsTranslate('','tours_placesyn_title_w');
        wrapMode: Text.Wrap;
        maximumLineCount:1;
        textFormat: Text.RichText;
        elide: Text.ElideRight;
    }

    ListView{
        id:synopsisLV;

        property int selIndex;
        anchors.top: headingText.bottom;
        anchors.left: headingText.left;
        anchors.topMargin: qsTranslate('','tours_placesyn_list_tm');
        anchors.leftMargin: qsTranslate('','tours_placesyn_list_lm');
        cacheBuffer:synopsisLV.contentHeight// contentHeight;
        boundsBehavior:Flickable.StopAtBounds
        height: qsTranslate('','tours_placesyn_list_h');
        width: qsTranslate('','tours_placesyn_list_w');
        model:modelUpdate?detailScreenModel:viewHelper.blankModel;
        delegate: infoCategory;
        clip:true;
        property bool modelUpdate:true
        onCountChanged: {
            synopsisLV.currentIndex = selIndex;
            synopsisLV.forceActiveFocus();
        }

        Keys.onUpPressed: {
            if(synopsisLV.currentIndex == 0){
                lastSelectedFocus = 1;
                closeBtn.forceActiveFocus();
            }else{
                synopsisLV.decrementCurrentIndex();
            }
        }
        Keys.onDownPressed: {
            synopsisLV.incrementCurrentIndex()
        }
        Keys.onRightPressed: {
            isPaging = false;
            if(scroller.visible){
                scroller.forceActiveFocus();
            }
        }
    }

    Component{
        id: infoCategory;

        Item {
            id: catInfoMenu;
            height: (synopsisLV.currentIndex == index)? (parseInt(subTitleLabel.height) + parseInt(qsTranslate('','tours_placesyn_cell_h'),10)+parseInt(qsTranslate('','tours_placesyn_desc_margin'),10)):qsTranslate('','tours_placesyn_cell_h');
            width: qsTranslate('','tours_placesyn_list_w');
            clip: true

            Behavior on height {
                NumberAnimation{ easing.type: Easing.OutCirc; duration: 100 }
            }
            Image{
                id: btn;
                source: ((synopsisLV.currentIndex == index) && (synopsisLV.activeFocus)) ? viewHelper.configToolImagePath+configTool.config.aviancatours.screen.menu_btn_h : viewHelper.configToolImagePath+configTool.config.aviancatours.screen.menu_btn_n;
                ViewText{
                    height: qsTranslate('','tours_placesyn_cell_h');
                    width: qsTranslate('','tours_placesyn_cell_w');
                    anchors.top: parent.top;
                    anchors.topMargin: qsTranslate('','tours_placesyn_txt_tm');
                    anchors.left:parent.left;
                    anchors.leftMargin: qsTranslate('','tours_placesyn_desc_lm');

                    verticalAlignment: Text.AlignVCenter;
                    wrapMode: Text.Wrap;
                    maximumLineCount:1;
                    elide: Text.ElideRight;
                    property string txtColor:((synopsisLV.currentIndex == index) && (synopsisLV.activeFocus))?configTool.config.aviancatours.screen.text_listview_h: configTool.config.aviancatours.screen.text_listview_n;

                    varText:[(detailScreenModel.get(index).infoHeadingText_cdata !== "") ? detailScreenModel.get(index).infoHeadingText_cdata : detailScreenModel.get(index).infoHeadingText,
                                                                                           txtColor,
                                                                                           qsTranslate('','tours_placesyn_fontsize1')]
                }
                MouseArea{
                    anchors.fill: parent;
                    onPressed: {
                        isPressed=true;
                    }
                    onReleased: {
                        synopsisLV.currentIndex=index;
                        synopsisLV.selIndex=synopsisLV.currentIndex
                        synopsisLV.forceActiveFocus();
                        isPressed=false;
                    }
                }
                Keys.onPressed: {
                    isPressed=true;
                }

                Keys.onReleased: {
                    if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                        synopsisLV.currentIndex=index;
                        synopsisLV.selIndex=synopsisLV.currentIndex
                        isPressed=false;
                    }
                }
            }
            ViewText{
                id:subTitleLabel
                width:qsTranslate('','tours_placesyn_desc_w');
                anchors.top:btn.bottom
                anchors.left: btn.left;
                textFormat: Text.RichText;
                wrapMode: Text.Wrap;
                anchors.leftMargin:qsTranslate('','tours_placesyn_desc_lm');
                anchors.topMargin:qsTranslate('','tours_placesyn_desc_vgap');
                visible: (synopsisLV.currentIndex == index) ? true : false;
                varText:[
                    (detailScreenModel.get(index).infoText_cdata !== "") ? detailScreenModel.get(index).infoText_cdata : detailScreenModel.get(index).infoText,
                                                                           configTool.config.aviancatours.screen.text_listview_info,
                                                                           qsTranslate('','tours_placesyn_fontsize2')
                ]
            }
        }
    }

    Scroller{
        id:scroller
        height: qsTranslate('','tours_scroll_h');
        width: qsTranslate('','tours_scroll_w');
        anchors.right:listBg.right;
        anchors.rightMargin: qsTranslate('','tours_scroll_rm');
        anchors.top: listBg.top;
        anchors.topMargin: qsTranslate('','tours_scroll_tm')
        sliderbg:[qsTranslate('','tours_scroll_base_margin'),qsTranslate('','tours_scroll_w'),qsTranslate('','tours_scroll_base_h'),configTool.config.aviancatours.screen.scrollbg]
        scrollerDot: configTool.config.aviancatours.screen.slider
        upArrowDetails:[configTool.config.aviancatours.screen.app_arwup_n, configTool.config.aviancatours.screen.app_arwup_h, configTool.config.aviancatours.screen.app_arwup_h ]
        downArrowDetails:[configTool.config.aviancatours.screen.app_arwdwn_n, configTool.config.aviancatours.screen.app_arwdwn_h, configTool.config.aviancatours.screen.app_arwdwn_h]

        targetData:synopsisLV;
        onSliderNavigation:{
            console.log(">>>>>>>>>>>>>>>>>>>>> direction "+direction+"upArrow.isDisable  "+upArrow.isDisable+" downArrow.activeFocus "+downArrow.activeFocus)
            if(direction=="up"){
                lastSelectedFocus = 0;
                if(!upArrow.isDisable && downArrow.activeFocus)upArrow.forceActiveFocus()
                else closeBtn.forceActiveFocus();
            }
        }
        onArrowPressed: {

            isPaging = true;
            setListingIndex();
        }

        Keys.onLeftPressed: {
            if(isPaging){
                synopsisLV.currentIndex = indexOnPaging;
            }
            synopsisLV.forceActiveFocus();
        }
        //        Keys.onUpPressed: {

        //            if(!upArrow.isDisable && downArrow.activeFocus)upArrow.forceActiveFocus()
        //            else if(upArrow.activeFocus)closeBtn.forceActiveFocus();
        //        }
    }
}

