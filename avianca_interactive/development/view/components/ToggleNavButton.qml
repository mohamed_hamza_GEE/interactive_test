import QtQuick 1.0

SimpleNavButton {
    property bool cstate: false;
    property string normImg1:"";
    property string normImg2:"";
    property string highImg1:"";
    property string highImg2:"";
    property string pressImg1:"";
    property string pressImg2:"";
    property string selectedImg1:"";
    property string selectedImg2:"";
    normImg:(cstate)?normImg1:normImg2;
    highlightimg:(cstate)?highImg1:highImg2;
    pressedImg:(cstate)?pressImg1:pressImg2;
    selectedImg: (cstate)?selectedImg1:selectedImg2
}
