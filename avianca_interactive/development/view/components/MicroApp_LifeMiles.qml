import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    id:microApp;
    height: core.height; width:core.width;
    property alias background : background;
    property alias appData1 : appData1;
    property alias bgData : bgData;
    property alias appcategoryData : appcategoryData;
    property alias childLoader: childLoader;
    property alias menuContainer: menuContainer;
    //directories for accessing application data and images
    property string currAppImages: Qt.resolvedUrl(microAppDir+"images/");
    property string childAppImages:currAppImages;
    //xmlmodelsource path should be appName_data.xml and child app xmlmodel source should be appName.xml
    property string dataxml: currAppDataName;
    property string childappXml:currAppName+".xml";
    property string xmlModelSource: microAppDir+tmpCurrAppDataFileName;
    property string childappModelSource: microAppDir+childappXml;
    //Selected Language
    property int isSelected;
    property string bgName;
    property string lmIcon;
    property int lastFocusFrom;

    Connections{
        target: configTool
        onMicroAppXMLChangedFromThemeMgr:{
            if(dirName != '') origBaseMicroAppDir = "file:///"+dirName;
            if ((fileName != "") && (fileName.indexOf("data") !== -1)){
                var tempAppName = fileName.replace("_data.xml","");
                if(tempAppName != currAppName) return;
                currAppName = '';
                microAppDir = '';
                currAppImages = ''
                xmlModelSource = ''

                microAppDir = "file:///"+childPath+"/";
                currAppImages = "file:///"+childPath+"/images/";
                currAppName = tempAppName;
                xmlModelSource = microAppDir+currAppName+"_data.xml";
                bgData.reload();
                appcategoryData.reload();
                appData1.reload();
            }
        }
    }

    function selectChild(dataid){
        if(dataid === "discoverLifeMiles"){
            childAppName = microAppDir+dataid+"/"+dataid+"_data.xml";
            childAppImages = microAppDir+dataid+"/images/";
            childLoader.sourceComponent=appDiscover;
        }else if(dataid === "earnLifeMiles"){
            childAppName = microAppDir+dataid+"/"+dataid+"_data.xml";
            childAppImages = microAppDir+dataid+"/images/";
            childLoader.sourceComponent=appEarn;
        }else if(dataid === "enjoyLifeMiles"){
            childAppName = microAppDir+dataid+"/"+dataid+"_data.xml";
            childAppImages = microAppDir+dataid+"/images/";
            childLoader.sourceComponent=appEnjoy;
//            childLoader.sourceComponent=appEarn;
        }else if(dataid === "elightProgram"){
            childAppName = microAppDir+dataid+"/"+dataid+"_data.xml";
            childAppImages = microAppDir+dataid+"/images/";
            childLoader.sourceComponent=appElite;
        }else{
            childLoader.sourceComponent = background;
        }
    }

    Loader{
        id:childLoader;
        property alias childLoader: childLoader;
        x:menuContainerBg.width;

        onStatusChanged: {
            if(childLoader.status==Loader.Ready){
                childLoader.item.init();
                childLoader.item.childAppImages=childAppImages
            }
        }
    }
    //ListModel for application's background image
    XmlListModel{
        id: bgData
        source: xmlModelSource
        query:"/application"
        XmlRole { name:"bg_image";  query:"backgroundImage/@_value/string()" }
        XmlRole { name:"lm_icon";  query:"image1/@_value/string()" }

        onCountChanged: {
            bgName=bgData.get(0).bg_image;
            lmIcon=bgData.get(0).lm_icon;
            childLoader.sourceComponent = background;
            lifeMileLogo.source = currAppImages + lmIcon;
        }
    }
    XmlListModel{
        id: appcategoryData;
        source: currAppName ? xmlModelSource : "";
        query: "/application/app";

        XmlRole{ name:"categoryText"; query:"text1/"+langCode+"/@_value/string()" }
        XmlRole{ name:"categoryText_cdata"; query:"text1/"+langCode+"/@_value/string()" }
        XmlRole{ name:"dataID"; query:"@_dataId/string()" }
        XmlRole{ name:"template"; query: "@_template/string()" }

        onCountChanged: {
            menuContainer.model = appcategoryData;
//            lifeMileLogo.source = "";
            lifeMileLogo.source = currAppImages + lmIcon;
        }
    }
    XmlListModel{
        id: appData1;
        source: currAppName ? xmlModelSource : "";
        query: "/application/screen";

        XmlRole{ name:"headerText"; query:"text1/"+langCode+"/@_value/string()" }
        XmlRole{ name:"headerText_cdata"; query:"text1/"+langCode+"/string()" }
        XmlRole{ name:"categoryText"; query:"text10/"+langCode+"/@_value/string()" }
        XmlRole{ name:"categoryText_cdata"; query:"text10/"+langCode+"/string()" }
        XmlRole{ name:"bottomtextText"; query:"text11/"+langCode+"/@_value/string()" }
        XmlRole{ name:"bottomtextText_cdata"; query:"text11/"+langCode+"/string()" }
        XmlRole{ name:"headerInfoText"; query:"text2/"+langCode+"/@_value/string()" }
        XmlRole{ name:"headerInfoText_cdata"; query:"text2/"+langCode+"/string()" }
        XmlRole{ name:"posterImage"; query:"image1/@_value/string()" }
        //screen/template id's
        XmlRole { name:"screenId";  query:"@_id/string()" }
        XmlRole { name: "template"; query: "@_template/string()" }
    }

    Component{
        id:background;

        Item {
            id: container
            height: mainMenuBackground.paintedHeight;
            width: mainMenuBackground.paintedWidth;
            function init(){
                appcategoryData.reload();
            }
            function setInitialFocus(){
                isSelected = -1;
                lifeMileLogoContainer.forceActiveFocus();
            }
            function setFocus(){
                appCloseBtn.forceActiveFocus()
            }

            Image {
                id: mainMenuBackground
                source: currAppImages + bgName;
            }

            SimpleButton {
                id: appCloseBtn;
                anchors.right: mainMenuBackground.right;
                anchors.top: parent.top;
                z:10;
                anchors.rightMargin: qsTranslate('','lifemiles_close_y');
                anchors.topMargin: qsTranslate('','lifemiles_close_x');
                isHighlight: activeFocus
                normImg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_n;
                highlightimg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;
                pressedImg:viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;
                onEnteredReleased: {
                    viewController.showScreenPopup(23);
                }
                Keys.onLeftPressed: {
                    if(lastFocusFrom == 1){
                        lifeMileLogoContainer.forceActiveFocus();
                    }else{
                        menuContainer.forceActiveFocus();
                    }
                }
            }
        }
    }

    Image {
        id: menuContainerBg;
        anchors.left:parent.left;
        source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.menulist_bgd;
    }
    Image{
        id: lifeMileLogoContainer;
        anchors.top: parent.top;
        source: (isSelected == -1) ? viewHelper.configToolImagePath+configTool.config.lifemiles.screen.logobtn_h : viewHelper.configToolImagePath+configTool.config.lifemiles.screen.logobtn_n;

        Image {
            id: highlight
            visible: (lifeMileLogoContainer.activeFocus) ? true : false;
            anchors.top: parent.top; anchors.left: parent.left;
            source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.logobtn_h;
        }
        Image {
            id: lifeMileLogo;
            anchors.left:parent.left;
            anchors.top: parent.top;
            anchors.leftMargin: qsTranslate('','lifemiles_list_logo_lm');
            anchors.topMargin: qsTranslate('','lifemiles_list_logo_tm');
//            source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.lflogo;
            source: currAppImages + lmIcon;
        }
        MouseArea{
            anchors.fill: parent;
            onReleased: {
                isSelected= -1;
                lifeMileLogoContainer.forceActiveFocus();
                childLoader.sourceComponent = background;
            }
        }
        Keys.onDownPressed: {
            menuContainer.currentIndex = 0;
            menuContainer.forceActiveFocus();
        }
        Keys.onRightPressed: {
            lastFocusFrom = 1;
            childLoader.item.setFocus();
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                isSelected= -1;
                lifeMileLogoContainer.forceActiveFocus();
                childLoader.sourceComponent = background;
            }
        }

        states: [
            State {
                name: "highlighton"
                when: isSelected == -1
                PropertyChanges {
                    target: lifeMileLogoContainer
                    source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.logobtn_p;
                }
            },
            State {
                name: "highlightoff"
                when: (isSelected != -1)
                PropertyChanges {
                    target: lifeMileLogoContainer
                    source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.logobtn_n
                }
            }
        ]
    }

    ListView{
        id: menuContainer
        property alias menuContainer: menuContainer;
        height: qsTranslate('','lifemiles_list_h');
        width: qsTranslate('','lifemiles_list_w');
        anchors.top: parent.top;
        anchors.topMargin: qsTranslate('','lifemiles_list_y');
        //            model: appcategoryData;
        orientation: ListView.Vertical;
        interactive:false
        delegate:menuList;
        //            highlight: listHighlight;
        onCountChanged: {
            isSelected = -1;
            lifeMileLogoContainer.forceActiveFocus();
        }

        Keys.onUpPressed: {
            if(menuContainer.currentIndex == 0){
                lifeMileLogoContainer.forceActiveFocus()
            }else{
                menuContainer.decrementCurrentIndex();
            }
        }
        Keys.onDownPressed: {
            menuContainer.incrementCurrentIndex();
        }
        Keys.onRightPressed: {
            lastFocusFrom = 2;
            childLoader.item.setFocus('right');
        }
    }

    Component{
        id:menuList;
        Image{
            id:delegateContainer;
            height: qsTranslate('','lifemiles_list_cell_h');
            width: qsTranslate('','lifemiles_list_w');
            source: ((isSelected == index)&&((isSelected != -1))) ? viewHelper.configToolImagePath+configTool.config.lifemiles.screen.common_menubtn_p : viewHelper.configToolImagePath+configTool.config.lifemiles.screen.common_menubtn_n;


            Rectangle{
                id: screenHighlightlist;
                height: qsTranslate('','lifemiles_list_cell_h');
                width: qsTranslate('','lifemiles_list_w');
                color: "transparent"
                visible: (index==menuContainer.currentIndex && menuContainer.activeFocus)
                Image {
                    id: listHighlightImage;
                    anchors.fill: parent;
                    source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.common_menubtn_h;
                }
            }
            ViewText{
                id: categoryListText;
                height: qsTranslate('','lifemiles_list_cell_h');
                width: qsTranslate('','lifemiles_list_txt_w');
                wrapMode: Text.Wrap;
                textFormat: Text.RichText;
                horizontalAlignment: Text.AlignHCenter;
                verticalAlignment: Text.AlignVCenter;
                maximumLineCount:2;
                varText: [
                    (categoryText_cdata != "") ? categoryText_cdata : categoryText,
                    configTool.config.lifemiles.screen.text_list,
                    qsTranslate('','lifemiles_list_fontsize')
                ]
            }
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    isSelected= index;
                    menuContainer.currentIndex = index;
                    menuContainer.forceActiveFocus();
                    selectChild(appcategoryData.get(index).dataID);
                }
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    isSelected= index;
                    menuContainer.currentIndex = index;
                    menuContainer.forceActiveFocus();
                    selectChild(appcategoryData.get(index).dataID);
                }
            }
        }
    }
}
