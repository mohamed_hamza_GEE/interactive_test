import QtQuick 1.1

Item {
    id: welcomeContainer;
    property string currAppImages: microAppDir+currAppName+"/images/"
    property string xmlModelSource: microAppDir+currAppName+"/"+currAppName+"_data.xml"
    property int navCount:0;
    height: core.height;
    width: core.width;

    signal switchApp(string chlidAppName);

    function init(){
        core.log("AppMainMenu | Init");
        //        mainMenuListView.currentIndex = lastSelInd;
        mainMenuListView.forceActiveFocus();
    }
    function reload(){
        core.log("AppMainMenu | reload called ")
        appMainMenuListModel.source = xmlModelSource;
        appBackgroundSource.source = xmlModelSource;
        appBackgroundSource.reload();
        appMainMenuListModel.reload();
    }
    function setBg(obj, Dmodel){
        obj.source="";
        obj.source = currAppImages + Dmodel.get(0).background;
    }
    function setLogo(obj, Dmodel){
        obj.source="";
        obj.source = currAppImages + Dmodel.get(0).logo;
    }
    function setWelcomeText(obj,Dmodel){
        obj.text = (Dmodel.get(0).welcomeText_cdata !== "") ? Dmodel.get(0).welcomeText_cdata : Dmodel.get(0).welcomeText;
        obj.color = configTool.config.aviancaapp.screen.text_welcome_title;
        obj.font.pixelSize = qsTranslate('','aviapp_welcome_title_fontsize');
    }
    function setFocus(){
        if(appMainMenuListModel.count > 0){
            mainMenuListView.currentIndex = 0;
            mainMenuListView.forceActiveFocus()
        }else{
            appCloseBtn.forceActiveFocus();
        }
    }
    function languageUpdate(){
        appMainMenuListModel.reload()
        appBackgroundSource.reload()
    }

    XmlListModel {
        id:appMainMenuListModel;
        source: xmlModelSource;
        query: "/application/screen/list1/listItem";

        XmlRole { name : "categoryText"; query: "text2/"+langCode+"/@_value/string()" }
        XmlRole { name : "link"; query: "text3/@_value/string()" }

        onCountChanged: { mainMenuListView.currentIndex = lastSelInd; }
    }

    XmlListModel {
        id: appBackgroundSource;
        source: xmlModelSource;
        query: "/application/screen";

        XmlRole { name:"welcomeText"; query:"text1/"+langCode+"/@_value/string()" }
        XmlRole { name:"welcomeText_cdata"; query:"text1/"+langCode+"/string()" }
        XmlRole { name:"background"; query:"image1/@_value/string()" }
        XmlRole { name:"logo"; query:"image2/@_value/string()" }

        onCountChanged: {
            setBg(appBackgroundIamge,appBackgroundSource);
            setLogo(appLogo,appBackgroundSource);
            setWelcomeText(appWelcomeText,appBackgroundSource);
        }
    }
    Image {
        id: appBackgroundIamge;
        source: currAppImages+appBackgroundSource.get(0).background;
    }
    SimpleButton {
        id: appCloseBtn;
        increaseTouchArea: true
        increaseValue:-60
        anchors.right: parent.right; anchors.top:parent.top;
        anchors.rightMargin: qsTranslate('','aviapp_welcome_close_gap');
        anchors.topMargin: qsTranslate('','aviapp_welcome_close_gap');
        isHighlight: activeFocus;
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.close_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.close_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.close_h;
        onEnteredReleased: {
            isChildAppSelected = false;
            viewController.showScreenPopup(23);
        }
        Keys.onDownPressed: {mainMenuListView.forceActiveFocus();}
    }

    Image {
        id: appLogo;
        anchors.right: parent.right
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','aviapp_welcome_logo_y');
        anchors.rightMargin: qsTranslate('','aviapp_welcome_logo_rm');
        //        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.app_logo;
        source: currAppImages+appBackgroundSource.get(0).logo;
    }
    ViewText{
        id: appWelcomeText;
        anchors {top : appLogo.bottom; topMargin: qsTranslate('','aviapp_welcome_title_tm');right: appLogo.right}
        horizontalAlignment: Text.AlignRight;
        //        textFormat: Text.StyledText;
        height: qsTranslate('','aviapp_welcome_title_h');
        width: qsTranslate('','aviapp_welcome_title_w');
        clip: true;
        wrapMode: Text.Wrap;
        varText:[
            (appBackgroundSource.get(0).welcomeText_cdata != "")?appBackgroundSource.get(0).welcomeText_cdata:appBackgroundSource.get(0).welcomeText,
                                                                  configTool.config.aviancaapp.screen.text_welcome_title,
                                                                  qsTranslate('','aviapp_welcome_title_fontsize')
        ]
    }

    Image {
        id: listViewBgImage;
        width: core.width;
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','aviapp_welcome_menu_y');
        source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.app_menu_bg;
    }

    ListView {
        id: mainMenuListView;
        height: qsTranslate('','aviapp_welcome_menu_h');
        width: qsTranslate('','aviapp_welcome_menu_w');
        anchors.horizontalCenter: listViewBgImage.horizontalCenter;
        anchors.verticalCenter: listViewBgImage.verticalCenter;
        anchors.left: leftArrow.right;
        anchors.leftMargin: qsTranslate('','aviapp_welcome_arrow_gap');
        orientation: ListView.Horizontal;
        highlightMoveDuration : 10;
        model:appMainMenuListModel;
        delegate:mainMenuListViewDelegate;
        interactive: false;
        focus: true;
        clip: true;
        onCountChanged: {mainMenuListView.forceActiveFocus();}
        Keys.onUpPressed: {appCloseBtn.forceActiveFocus();}
        Keys.onLeftPressed: {
            welcomeContainer.navCount = 0;
            if(mainMenuListView.currentIndex!=0){
                mainMenuListView.decrementCurrentIndex();
            }else {
                //                leftArrow.forceActiveFocus();
            }
        }
        Keys.onRightPressed: {
            welcomeContainer.navCount = 0;
            if(mainMenuListView.currentIndex!=(mainMenuListView.count-1)){
                mainMenuListView.incrementCurrentIndex();
            }else {
                //rightArrow.forceActiveFocus();
            }
        }
    }

    SimpleButton {
        id: rightArrow;
        anchors.right: listViewBgImage.right;
        anchors.rightMargin: qsTranslate('','aviapp_welcome_arrow_gap');
        anchors.verticalCenter: listViewBgImage.verticalCenter;
        isDisable:(mainMenuListView.contentX < (mainMenuListView.contentWidth-mainMenuListView.width))?false:true
      //((appMainMenuListModel.count <= 5) || (mainMenuListView.currentIndex == (mainMenuListView.count -1))) ? true : false;
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_right_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_right_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_right_h;
        visible: mainMenuListView.count > 5
        onIsDisableChanged: {
            if(isDisable){
                mainMenuListView.forceActiveFocus();
            }
        }
        onEnteredReleased: {mainMenuListView.incrementCurrentIndex();}
        Keys.onUpPressed: {appCloseBtn.forceActiveFocus();}
        Keys.onLeftPressed: {mainMenuListView.forceActiveFocus();}
    }


    SimpleButton {
        id: leftArrow;
        anchors.left: listViewBgImage.left;
        anchors.leftMargin: qsTranslate('','aviapp_welcome_arrow_gap');
        anchors.verticalCenter: listViewBgImage.verticalCenter;
        isDisable: (mainMenuListView.contentX == 0) ? true : false;
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_left_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_left_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_left_h;
        visible: mainMenuListView.count > 0 &&  mainMenuListView.count > 5
        onIsDisableChanged: {
            if(isDisable){
                mainMenuListView.forceActiveFocus();
            }
        }
        onEnteredReleased: {mainMenuListView.decrementCurrentIndex();}
        Keys.onRightPressed:{mainMenuListView.forceActiveFocus();}
        Keys.onUpPressed: {appCloseBtn.forceActiveFocus();}
    }

    Component {
        id:mainMenuListViewDelegate;
        Rectangle {
            id:container
            height:qsTranslate('','aviapp_welcome_menu_itemh');
            width: qsTranslate('','aviapp_welcome_menu_itemw');
            color: "transparent";
            ViewText {
                id: categoryText;
                z:1
                anchors {horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter }
                varText:[
                    appMainMenuListModel.get(index).categoryText,
                    configTool.config.aviancaapp.screen.text_welcome_menu,
                    qsTranslate('','aviapp_welcome_menu_fontsize')
                ];
            }

            Rectangle{
                id: screenHighlightlist;
                height:qsTranslate('','aviapp_welcome_menu_itemh');
                width: qsTranslate('','aviapp_welcome_menu_itemw');
                color: "transparent"
                visible: (index==mainMenuListView.currentIndex && (!appCloseBtn.activeFocus))
                Image {
                    id: menuListHighlightImage;
                    anchors.fill: parent;
                    source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.app_menu_h;
                }
            }

            MouseArea {
                id:menuMouseArea;
                anchors.fill: parent;
                onPressed: {
                    mainMenuListView.currentIndex = index;
                    mainMenuListView.forceActiveFocus();
                }
                onReleased: {
                    welcomeContainer.navCount = 0;
                    lastSelInd = index;
                    mainMenuListView.currentIndex = index;
                    switchApp(link);
                }
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    welcomeContainer.navCount = 0;
                    lastSelInd = index;
                    mainMenuListView.currentIndex=index;
                    switchApp(link);
                }
            }

            Image {
                id: listViewBgImageDivider;
                visible:(index == (appMainMenuListModel.count - 1)) ? false : true;
                anchors.right:parent.right
                source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.app_menu_divider;
            }
        }
    }
}
