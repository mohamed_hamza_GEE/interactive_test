// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import ImageMask 1.0
FocusScope{
    anchors.fill: parent
    anchors.top:parent.top
    anchors.topMargin:qsTranslate('','synopsis_games_panel_tm')
    property alias synopsisCloseBtn: synopsisCloseBtn
    property variant model:listingobj.compVpathListing.model
    property variant currentIndex: listingobj.compVpathListing.synopIndex
    property variant synopsisDetails: []
    property bool isSliderNavChanged: false
    property bool cgStat:false
    property string origBaseMicroAppDir: core.configTool.base+"microapps/";
    property int currentMid: -1
    signal extremeEnds(string direction)
    /**************************************************************************************************************************/
    onExtremeEnds:{
        if(direction=="left" && listingobj.compVpathListing.count>1){
            decrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="right" && listingobj.compVpathListing.count>1){
            incrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="up")
            widgetList.focustTomainMenu();
    }
    onCurrentIndexChanged:{ }
    /**************************************************************************************************************************/
    function init(){
        cgStat=(dataController.connectingGate)?dataController.connectingGate.getCgStatus():false;

        forceFocus()
    }
    function clearOnExit(){

    }
    function forceFocus(){
        if(errorMessage.visible==true){
            viewHelper.setHelpTemplate("discover_synop_no_button")
            synopsisCloseBtn.forceActiveFocus()
        }
        else continueBtn.forceActiveFocus();
    }
    function getSynopsisDetails(){

    }
    function continueAction(){
        viewHelper.mediaListing=currentIndex
        var tmpId=model.getValue(listingobj.compVpathListing.currentIndex,"category_attr_template_id")
        if(dataController.mediaServices.getCidBlockStatus(model.getValue(listingobj.compVpathListing.currentIndex,"cid"))){
            viewController.updateSystemPopupQ(7,true)
            return;
        }
        if(tmpId == "milesapp" || tmpId == "airline_info" || tmpId == "airline_tour"){
            if(!core.coreHelper.fileExists(origBaseMicroAppDir)){
                viewController.updateSystemPopupQ(7,true)
                return;
            }
            var className;
            if(pif.getCabinClassName()=="FCRC" && pif.getLruData("XResolution")=="1024x600"){
                className = "economy"
            }else if(pif.getCabinClassName()=="FCRC" && pif.getLruData("XResolution")=="1368x768"){
                className = "business"
            }else{
                className = pif.getCabinClassName()
            }

            className = className.toLowerCase();
            var folder = "";
            if(tmpId == "milesapp")folder = "aviancaLifeMile_"+className;
            else if(tmpId == "airline_info")folder = "aviancaApp_"+className;
            else if(tmpId == "airline_tour")folder = "aviancaTour_"+className;

            folder = origBaseMicroAppDir+folder+"/";
            if(!core.coreHelper.fileExists(folder)){
                viewController.updateSystemPopupQ(7,true)
                return;
            }

            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":tmpId/*,"showContOnKarma":viewHelper.showContOnKarma*/});
            }
        }


        if(tmpId =="connecting_gate"){
            if(viewHelper.checkForExternalBlock("CG"))return;
            viewController.loadNext(listingobj.compVpathListing.model,listingobj.compVpathListing.currentIndex);
        }
        else if(tmpId =="maps"){
            if(viewHelper.checkForExternalBlock("Voyager"))return;
            widgetList.showFlightMap(true)
        }else if(tmpId=="hospitality"){
            if(viewHelper.checkForExternalBlock("Hospitality"))return;
            core.debug("hospitality.qml | getCatalogMenu() | core.dataController.shopping.getLanguageModel().count = "+core.dataController.shopping.getLanguageModel().count)
            if(!core.dataController.hospitality){viewController.updateSystemPopupQ(7,true);return;}

            var curLangISO=core.settings.languageISO.toUpperCase()
            widgetList.showAppLoading(true);
            var langIndex =core.coreHelper.searchEntryInModel(core.dataController.hospitality.getLanguageModel(),"language_code",curLangISO)
            core.debug("hospitality.qml | languageUpdate |  viewHelper.defaultLang = "+viewHelper.defaultLang+" langIndex = "+langIndex+ " curLangISO = "+curLangISO)
            if(langIndex!=-1){
                core.debug("Shopping.qml | languageUpdate |  curLangISO = "+curLangISO)
                core.dataController.hospitality.fetchData(curLangISO,mealMenuReady)
            }else{
                dataController.hospitality.fetchData(viewHelper.defaultLang,mealMenuReady)
            }


        }else if(tmpId == "milesapp") {
            core.log(">> | In DiscoverSynopsis.qml  | continueAction | (tmpId == milesapp) | current template ID : "+current.template_id);
            viewController.loadNext(listingobj.compVpathListing.model,listingobj.compVpathListing.currentIndex);
        }else if(tmpId == "airline_info") {
            core.log(">> | In DiscoverSynopsis.qml  | continueAction | (tmpId == airline_info) | current template ID : "+current.template_id);
            viewController.loadNext(listingobj.compVpathListing.model,listingobj.compVpathListing.currentIndex);
        }else if(tmpId == "airline_tour") {
            core.log(">> | In DiscoverSynopsis.qml  | continueAction | (tmpId == airline_tour) | current template ID : "+current.template_id);
            viewController.loadNext(listingobj.compVpathListing.model,listingobj.compVpathListing.currentIndex);
        }
    }
    Connections{
        target:widgetList.getMealStatus()?dataController.hospitality:null
        onSigCatalogStatusChanged:{
            console.log("Hospitality.qml | catalog_id "+catalog_id+ " status= "+status)
            viewHelper.storingCatalog_id=catalog_id
            dataController.hospitality.fetchData(core.settings.languageISO.toUpperCase(),mealMenuReady)
        }
    }
    function mealMenuReady(dModel,status){
        core.debug("mealMenuReady | dModel.count : "+dModel.count)
        core.debug("mealMenuReady | status : "+status);
        widgetList.showAppLoading(false);
        if(dModel.count==0||status==false){
            viewController.showScreenPopup(7);
            widgetList.showMealMenu(false)
            continueBtn.forceActiveFocus();
            return
        }
        widgetList.showMealMenu(true)
    }

    function backBtnPressed(){
        closeLoader()
    }
    /**************************************************************************************************************************/
    Image{
        id:synopsisIMG
        source:viewHelper.configToolImagePath+configTool.config.discover.synopsis.panel
        anchors.horizontalCenter: parent.horizontalCenter
    }
    SimpleNavButton{
        id:synopsisCloseBtn
        anchors.right: synopsisIMG.right
        anchors.rightMargin: qsTranslate('','synopsis_close_rm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_close_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTool.config.discover.synopsis.close_btn_n;
        highlightimg:  viewHelper.configToolImagePath+configTool.config.discover.synopsis.close_btn_h;
        pressedImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.close_btn_p;
        nav:["","","",slider.visible?slider:(continueBtn.visible?continueBtn:"")]
        onEnteredReleased:{closeLoader();}
        onPadReleased:if(dir!="down")extremeEnds(dir)
        onNoItemFound:  if(dir=="left" || dir=="right" || dir == "up")extremeEnds(dir)
    }
    MaskedImage{
        id:posterIMGBtn
        width: qsTranslate('','synopsis_games_poster_w')
        height: qsTranslate('','synopsis_games_poster_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: qsTranslate('','synopsis_games_poster_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_games_poster_tm')
        source: model.count>0?viewHelper.cMediaImagePath+model.getValue(currentIndex,"poster"):""
        //source:"/media/sf_D_DRIVE/games.jpg"
        maskSource:viewController.settings.assetImagePath+qsTranslate('','listing_games_list_source');
    }
    ViewText{
        id:synopsisTitleBtn
        width:qsTranslate('','synopsis_games_title_w')
        height:qsTranslate('','synopsis_games_title_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin:qsTranslate('','synopsis_games_title_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_games_title_tm')
        varText: [text,configTool.config.discover.synopsis.title_color,qsTranslate('','synopsis_games_title_fontsize')]
        //     verticalAlignment: Text.AlignVCenter
        text:model?(model.count>0?model.getValue(currentIndex,"title"):""):""
        elide: Text.ElideRight
        maximumLineCount: widgetList.getCidTidMainMenu()[1]=="games"?1:2
        wrapMode: Text.Wrap

    }
    ViewText{
        id:errorMessage
        width:qsTranslate('','synopsis_note_w')
        height:qsTranslate('','synopsis_note_h')
        visible:(!dataController.connectingGate.getCgStatus() && listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"category_attr_template_id") =="connecting_gate")?true:false
        onVisibleChanged:  forceFocus()
        anchors.left:synopsisIMG.left
        anchors.leftMargin:qsTranslate('','synopsis_note_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_note_tm')
        varText: [text,configTool.config.discover.synopsis.cg_info_text_color,qsTranslate('','synopsis_note_fontsize')]
        wrapMode: Text.Wrap
        elide: Text.ElideRight
        text:configTool.language.connecting_gate.cg_info_notavailable
    }
    Flickable{
        id : synopsisContentFlickable
        width:qsTranslate('','synopsis_games_desc1_w')
        height:qsTranslate('','synopsis_games_desc1_h')
        contentWidth: width
        contentHeight: synopsisSubDescIMG.paintedHeight
        anchors.top:synopsisIMG.top
        anchors.topMargin: qsTranslate('','synopsis_games_desc1_tm')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: qsTranslate('','synopsis_games_title_lm')
        clip:true
        ViewText{
            id:synopsisSubDescIMG
            width:parent.width
            height:parent.height
            varText:[text,configTool.config.discover.synopsis.description1_color, qsTranslate('','synopsis_games_desc1_fontsize')]
            wrapMode: Text.WordWrap
            text:model?(model.count>0?model.getValue(currentIndex,"description"):""):""
        }
    }
    SimpleNavBrdrBtn{
        id:continueBtn
        property string textColor: continueBtn.activeFocus?configTool.config.discover.synopsis.btn_text_h:(continueBtn.isPressed?configTool.config.discover.synopsis.btn_text_p:configTool.config.discover.synopsis.btn_text_n)
        visible: (!errorMessage.visible)?true:false
        width: qsTranslate('','synopsis_play_w')
        height: qsTranslate('','synopsis_play_h')
        anchors.right: synopsisIMG.right
        anchors.rightMargin:qsTranslate('','synopsis_play_rm')
        anchors.bottom: synopsisIMG.bottom
        anchors.bottomMargin: qsTranslate('','synopsis_play_bm')
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_p
        normalBorder: [qsTranslate('','synopsis_play_margin')]
        highlightBorder: [qsTranslate('','synopsis_play_margin')]
        nav:["",!slider.downArrow.isDisable?slider.downArrow:((!slider.upArrow.isDisable)?slider.upArrow:synopsisCloseBtn),continueBtn,""]
        onPadReleased: if(dir=="right"){extremeEnds("right")}
        onNoItemFound:  if(dir=="left" || dir=="right")extremeEnds(dir)
        onEnteredReleased:continueAction()
        SimpleButton{
            anchors.left: parent.left
            anchors.leftMargin:qsTranslate('','synopsis_play_icon_lm')
            anchors.verticalCenter: parent.verticalCenter
            isHighlight: continueBtn.activeFocus
            normImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.play_icon_n
            highlightimg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.play_icon_h
            pressedImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.play_icon_p
        }
        ViewText{
            anchors.horizontalCenter: continueBtn.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width:qsTranslate('','synopsis_play_textw')
            varText: [configTool.language.global_elements.cont,continueBtn.textColor,qsTranslate('','synopsis_play_fontsize')]
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
    Scroller{
        id:slider
        anchors.top:synopsisIMG.top; anchors.topMargin: qsTranslate('','synopsis_slider_games_tm')
        anchors.right: synopsisIMG.right ;anchors.rightMargin: qsTranslate('','synopsis_slider_games_rm')
        width: qsTranslate('','synopsis_slider_games_w')
        height: qsTranslate('','synopsis_slider_games_h')
        targetData:synopsisContentFlickable
        sliderbg:[qsTranslate('','synopsis_slider_games_base_margin'),qsTranslate('','synopsis_slider_games_w'),qsTranslate('','synopsis_slider_games_base_h'),configTool.config.discover.synopsis.scroll_base,]
        scrollerDot:configTool.config.discover.synopsis.scroll_button
        downArrowDetails:  [configTool.config.discover.synopsis.dn_arrow_n,configTool.config.discover.synopsis.dn_arrow_h,configTool.config.discover.synopsis.dn_arrow_p]
        upArrowDetails: [configTool.config.discover.synopsis.up_arrow_n,configTool.config.discover.synopsis.up_arrow_h,configTool.config.discover.synopsis.up_arrow_p]
        visible: synopsisContentFlickable.contentHeight>synopsisContentFlickable.height
        onSliderNavigation: {
            //var tempModelCount=tvModel.count>0?tvModel.count:0
            if(direction=="down" ){
                if(continueBtn.visible)
                    continueBtn.forceActiveFocus()
            }
            else if(direction=="up")
                synopsisCloseBtn.forceActiveFocus()
            else if(direction=="right" || direction=="left"){
                isSliderNavChanged=true
                extremeEnds(direction)
            }
        }
    }
}
