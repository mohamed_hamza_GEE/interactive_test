// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import ImageMask 1.0
FocusScope{
    anchors.fill: parent
    anchors.top:parent.top
    anchors.topMargin:qsTranslate('','synopsis_games_panel_tm')
    property alias synopsisCloseBtn: synopsisCloseBtn
    property variant model:listingobj.compVpathListing.model
    property variant currentIndex:listingobj.compVpathListing.synopIndex
    property variant synopsisDetails: []
    property bool isSliderNavChanged: false
    property int currentMid: -1
    signal extremeEnds(string direction)
    /**************************************************************************************************************************/
    onExtremeEnds:{
        if(direction=="left" && listingobj.compVpathListing.count>1){
            decrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="right" && listingobj.compVpathListing.count>1){
            incrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="up"){
            widgetList.focustTomainMenu();
        }
    }
    /**************************************************************************************************************************/
    function init(){
        forceFocus()
    }
    function clearOnExit(){

    }
    function forceFocus(){
        playBtn.forceActiveFocus();
    }
    function launchGames(){
        if(checkMidForBlocking()) {viewController.updateSystemPopupQ(7,true);return}
         viewHelper.gameMid=getMediaInfo("mid");
        var path = getMediaInfo("filename");
        viewHelper.launchGames(path)

    }
    function backBtnPressed(){
        closeLoader()
    }

    function checkMidForBlocking(){
        var mid=0
        var rating=0
        mid=listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"mid")
        rating=listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"rating");
        var mid_status = dataController.mediaServices.getMidBlockStatus(mid,rating);
        return mid_status
    }

    /**************************************************************************************************************************/
    Image{
        id:synopsisIMG
        source:viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.panel
        anchors.horizontalCenter: parent.horizontalCenter
    }
    SimpleNavButton{
        id:synopsisCloseBtn
        anchors.right: synopsisIMG.right
        anchors.rightMargin: qsTranslate('','synopsis_close_rm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_close_tm')
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.close_btn_n;
        highlightimg:  viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.close_btn_h;
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.close_btn_p;
        nav:["","","",slider.visible?slider:playBtn]
        onEnteredReleased:{closeLoader();}
        onPadReleased:if(dir!="down")extremeEnds(dir)
        onNoItemFound:  if(dir!="left" || dir=="right" || dir == "up")extremeEnds(dir)
    }
    MaskedImage{
        id:posterIMGBtn
        width: qsTranslate('','synopsis_games_poster_w')
        height: qsTranslate('','synopsis_games_poster_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: qsTranslate('','synopsis_games_poster_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_games_poster_tm')
        source: model.count>0?viewHelper.cMediaImagePath+model.getValue(currentIndex,"poster"):""
        maskSource:viewController.settings.assetImagePath+qsTranslate('','listing_games_list_source');

    }
    ViewText{
        id:synopsisTitleBtn
        width:qsTranslate('','synopsis_games_title_w')
        height:qsTranslate('','synopsis_games_title_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin:qsTranslate('','synopsis_games_title_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_games_title_tm')
        varText: [text,configTool.config[currentTemplateNode].synopsis.title_color,qsTranslate('','synopsis_games_title_fontsize')]
        //     verticalAlignment: Text.AlignVCenter
        text:model?(model.count>0?model.getValue(currentIndex,"title"):""):""
        elide: Text.ElideRight
        maximumLineCount: widgetList.getCidTidMainMenu()[1]=="games"?1:2
        wrapMode: Text.Wrap

    }
    Flickable{
        id : synopsisContentFlickable
        width:qsTranslate('','synopsis_games_desc1_w')
        height:qsTranslate('','synopsis_games_desc1_h')
        contentWidth: width
        contentHeight: synopsisSubDescIMG.paintedHeight
        anchors.top:synopsisIMG.top
        anchors.topMargin: qsTranslate('','synopsis_games_desc1_tm')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: qsTranslate('','synopsis_games_title_lm')
        clip:true
        interactive: contentHeight<=height?false:true
        ViewText{
            id:synopsisSubDescIMG
            width:parent.width
            height:parent.height
            varText:[text,configTool.config[currentTemplateNode].synopsis.description1_color, qsTranslate('','synopsis_games_desc1_fontsize')]
            wrapMode: Text.WordWrap
            text:model?(model.count>0?model.getValue(currentIndex,"description"):""):""
        }
    }
    SimpleNavBrdrBtn{
        id:playBtn
        property string textColor: playBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(playBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
        visible: !widgetList.video.vodScreen.visible?!(pif.getMidInfo((model?model.getValue(currentIndex,"mid"):0),'elapseTime')>viewHelper.resumeSecs):true
        width: qsTranslate('','synopsis_play_w')
        height: qsTranslate('','synopsis_play_h')
        anchors.right: synopsisIMG.right
        anchors.rightMargin:qsTranslate('','synopsis_play_rm')
        anchors.bottom: synopsisIMG.bottom
        anchors.bottomMargin: qsTranslate('','synopsis_play_bm')
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
        normalBorder: [qsTranslate('','synopsis_play_margin')]
        highlightBorder: [qsTranslate('','synopsis_play_margin')]
        nav:["",!slider.downArrow.isDisable?slider.downArrow:((!slider.upArrow.isDisable)?slider.upArrow:synopsisCloseBtn),playBtn,""]
        onPadReleased: if(dir=="right"){extremeEnds("right")}
        onNoItemFound: if(dir=="left" || dir=="right")extremeEnds(dir)
        onEnteredReleased:{
            viewHelper.playingCategory="games";
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory})
            launchGames()
        }
        SimpleButton{
            anchors.left: parent.left
            anchors.leftMargin:qsTranslate('','synopsis_play_icon_lm')
            anchors.verticalCenter: parent.verticalCenter
            isHighlight: playBtn.activeFocus
            normImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_n
            highlightimg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_h
            pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_p
        }
        ViewText{
            anchors.centerIn:parent;
            width:qsTranslate('','synopsis_play_textw')
            varText: [configTool.language.games.play,playBtn.textColor,qsTranslate('','synopsis_play_fontsize')]
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
    Scroller{
        id:slider
        anchors.top:synopsisIMG.top; anchors.topMargin: qsTranslate('','synopsis_slider_games_tm')
        anchors.right: synopsisIMG.right ;anchors.rightMargin: qsTranslate('','synopsis_slider_games_rm')
        width: qsTranslate('','synopsis_slider_games_w')
        height: qsTranslate('','synopsis_slider_games_h')
        targetData:synopsisContentFlickable
        sliderbg:[qsTranslate('','synopsis_slider_games_base_margin'),qsTranslate('','synopsis_slider_games_w'),qsTranslate('','synopsis_slider_games_base_h'),configTool.config[currentTemplateNode].synopsis.scroll_base,]
        scrollerDot:configTool.config[currentTemplateNode].synopsis.scroll_button
        downArrowDetails:  [configTool.config[currentTemplateNode].synopsis.dn_arrow_n,configTool.config[currentTemplateNode].synopsis.dn_arrow_h,configTool.config[currentTemplateNode].synopsis.dn_arrow_p]
        upArrowDetails: [configTool.config[currentTemplateNode].synopsis.up_arrow_n,configTool.config[currentTemplateNode].synopsis.up_arrow_h,configTool.config[currentTemplateNode].synopsis.up_arrow_p]
        visible: synopsisContentFlickable.contentHeight>synopsisContentFlickable.height
        onSliderNavigation: {
            if(direction=="down" )
                playBtn.forceActiveFocus()
            else if(direction=="up")
                synopsisCloseBtn.forceActiveFocus()
            else if(direction=="right" || direction=="left"){
                isSliderNavChanged=true
                extremeEnds(direction)
            }

        }


    }

}
