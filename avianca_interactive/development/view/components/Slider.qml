// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

FocusScope{
    id:sliderFS
    property alias sliderDotImg: sliderDotImg
    property variant sliderFillingtItems:[]
    property variant sliderbg
    property bool isFill: false
    property bool isDragged: false
    property bool enableDragging: true
    property bool isReverseSliding: false
    property double totalPages:(orientation==Qt.Horizontal)?slider_bg.width-sliderDotImg.width:slider_bg.height-sliderDotImg.height
    property double perPage:totalPages/stepsValue
    property double maxLevel: 100
    property double slidingCalc:(orientation==Qt.Horizontal)?(totalPages-sliderDotImg.x):(totalPages-sliderDotImg.y)
    property double draggingSlidingCalc: (orientation==Qt.Horizontal)?(sliderDotImg.x):(sliderDotImg.y)
    property double shiftingPerClick:totalPages*(stepsValue/maxLevel)
    property double shiftingValue: (orientation==Qt.Horizontal)?(sliderDotImg.anchors.leftMargin/shiftingPerClick)*stepsValue:(sliderDotImg.anchors.bottomMargin/shiftingPerClick)*stepsValue
    property double valueXY: 0
    property int sliderTouchAreaOffset: 20
    property int orientation: Qt.Horizontal
    property int stepsValue:10
    property int setInitailValue: 0
    property int remainingValue: 0
    signal valueChanged(int shiftedValue , string value)
    signal setDotPosition(double count)
    signal externalCalc(double elapTime,double endTime)
    signal dragingOn(double shiftingValueForDragging)
    signal resetSlider()
    /**************************************************************************************************************************/
    onSetDotPosition: {
        if(orientation==Qt.Horizontal) sliderDotImg.anchors.leftMargin=count//Math.round(count) * (shiftingPerClick<1?1:shiftingPerClick)
        else  sliderDotImg.anchors.bottomMargin=count//(Math.round(count) * (shiftingPerClick<1?1:shiftingPerClick))
    }
    onSetInitailValueChanged: {
        if(setInitailValue==-1 || setInitailValue==undefined)return
        var count=(setInitailValue/maxLevel)*totalPages//Math.round((shiftingPerClick*(setInitailValue/stepsValue))/shiftingPerClick)
        setDotPosition(count)
    }
    onExternalCalc:{
        //        var stepValuesEndTime=(elapTime/stepsValue)
        //        var perPage1=((endTime/stepsValue)/perPage)
        //        var count=Math.round(stepValuesEndTime/Math.ceil(perPage1))
        var position=(elapTime/endTime) * totalPages
        setDotPosition(position)
        //        if(viewHelper.usbTrackPlayed){setDotPosition(stepValuesEndTime)}
        //        else{setDotPosition(count)}
    }
    onResetSlider: setDotPosition(0)
    /**************************************************************************************************************************/
    function nextPage(param){
        if(orientation==Qt.Horizontal){
            if(param) {
                sliderDotImg.anchors.leftMargin=isReverseSliding?slidingCalc:draggingSlidingCalc
                isDragged=false
                valueChanged(shiftingValue,"drag")
                return;
            }
            if(sliderDotImg.anchors.leftMargin< totalPages ){
                var temp=sliderDotImg.anchors.leftMargin+shiftingPerClick
                sliderDotImg.anchors.leftMargin=temp
                if(sliderDotImg.anchors.leftMargin>totalPages)
                    sliderDotImg.anchors.leftMargin=totalPages
                valueChanged(shiftingValue,'')
            }
        }
        else if(orientation==Qt.Vertical){
            if(param) {
                sliderDotImg.anchors.bottomMargin=isReverseSliding?slidingCalc:draggingSlidingCalc
                isDragged=false
                valueChanged(shiftingValue,"drag")
                return;
            }
            if( sliderDotImg.anchors.bottomMargin<totalPages){
                var temp1=sliderDotImg.anchors.bottomMargin+shiftingPerClick
                sliderDotImg.anchors.bottomMargin=temp1
                if(sliderDotImg.anchors.bottomMargin>totalPages)
                    sliderDotImg.anchors.bottomMargin=totalPages
                valueChanged(shiftingValue,'')
            }
        }
    }
    function prevPage(param){
        if(orientation==Qt.Horizontal){
            if(param) {
                sliderDotImg.anchors.leftMargin=isReverseSliding?slidingCalc:draggingSlidingCalc
                isDragged=false
                valueChanged(shiftingValue,"drag")
                return;
            }
            if(sliderDotImg.anchors.leftMargin > 0 ){
                var temp=sliderDotImg.anchors.leftMargin-shiftingPerClick
                sliderDotImg.anchors.leftMargin=temp
                if(sliderDotImg.anchors.leftMargin<0)
                    sliderDotImg.anchors.leftMargin=0
                valueChanged(shiftingValue,'')
            }
            valueChanged(shiftingValue,'')
        }
        else if(orientation==Qt.Vertical){
            if(param) {
                sliderDotImg.anchors.bottomMargin=isReverseSliding?slidingCalc:draggingSlidingCalc
                isDragged=false
                valueChanged(shiftingValue,"drag")
                return;
            }
            if(sliderDotImg.anchors.bottomMargin > 0 ){
                var temp1=sliderDotImg.anchors.bottomMargin-shiftingPerClick
                sliderDotImg.anchors.bottomMargin=temp1
                if(sliderDotImg.anchors.bottomMargin<0)
                    sliderDotImg.anchors.bottomMargin=0
                valueChanged(shiftingValue,'')
            }
        }
    }
    /**************************************************************************************************************************/
    Item{
        id: slider_bg
        //  clip: true
        anchors.fill: parent
        Image {
            id:ch
            anchors.centerIn:parent;
            source:viewHelper.configToolImagePath+sliderbg;
            MouseArea{
                anchors.fill: parent
                anchors.margins: -10
                hoverEnabled: true
                onClicked: {
                    if(!sliderDotImgMA.drag.active){
                        isDragged=true
                        if(orientation==Qt.Horizontal){
                            if (mouseX>sliderDotImg.x){sliderDotImg.x=mouseX; nextPage(true)}
                            else  {sliderDotImg.x=mouseX;prevPage(true) }
                        }
                        else {
                            if(mouseY>sliderDotImg.y){   sliderDotImg.y=mouseY;nextPage(true)}
                            else {  sliderDotImg.y=mouseY;prevPage(true)}}
                    }
                }
            }
        }

    }
    BorderImage{
        id:sliderFillImg
        anchors.bottom: (orientation==Qt.Horizontal)?undefined:slider_bg.bottom
        anchors.bottomMargin: 0
        anchors.left:(orientation==Qt.Horizontal)?slider_bg.left:undefined
        anchors.leftMargin:0
        source: viewHelper.configToolImagePath+sliderFillingtItems[0]
        visible: isFill
        height: (orientation==Qt.Horizontal)?sliderFillImg.sourceSize.height:(slider_bg.height-sliderDotImg.y)
        width:(orientation==Qt.Horizontal)?sliderDotImg.x:sliderFillImg.sourceSize.width
        border.left: (sourceSize.width/2)-1;
        border.top: (sourceSize.height/2)-1;
        border.right:  (sourceSize.width/2)-1;
        border.bottom:  (sourceSize.height/2)-1;
    }
    Image{
        id:sliderDotImg
        anchors.bottom:!isDragged?((orientation==Qt.Horizontal)?undefined:slider_bg.bottom):undefined
        anchors.bottomMargin:0
        anchors.left:!isDragged?((orientation==Qt.Horizontal)? parent.left:undefined):undefined
        anchors.leftMargin:0
        anchors.verticalCenter: (orientation==Qt.Horizontal)?slider_bg.verticalCenter:undefined
        anchors.horizontalCenter: (orientation==Qt.Horizontal)?undefined:slider_bg.horizontalCenter
        visible: isFill
        source:viewHelper.configToolImagePath+sliderFillingtItems[1]
        MouseArea{
            id:sliderDotImgMA
            anchors.fill: parent
            // Rectangle{anchors.fill: parent;color:"red";opacity:0.5}
            anchors.topMargin: (orientation==Qt.Horizontal)?-30:-10;anchors.bottomMargin: (orientation==Qt.Horizontal)?-30:-10
            anchors.rightMargin: (orientation==Qt.Horizontal)?-10:-30;anchors.leftMargin: (orientation==Qt.Horizontal)?-10:-30
            drag.target: enableDragging?sliderDotImg:null
            drag.axis:  (orientation==Qt.Horizontal)?Drag.XAxis:Drag.YAxis
            drag.minimumX: 0
            drag.maximumX:  (orientation==Qt.Horizontal)?totalPages:0
            drag.minimumY:  0
            drag.maximumY: (orientation==Qt.Horizontal)?0:totalPages
            onPositionChanged: {
                remainingValue=(orientation==Qt.Horizontal)?(slidingCalc/shiftingPerClick)*stepsValue:(slidingCalc/shiftingPerClick)*stepsValue
                dragingOn(remainingValue)
            }
            onPressed:{
                isDragged=true
                if(orientation==Qt.Horizontal) valueXY= sliderDotImg.x
                else valueXY=sliderDotImg.y
            }
            onReleased:  {
                if(orientation==Qt.Horizontal) (sliderDotImg.x>valueXY)?nextPage(true): prevPage(true)
                else sliderDotImg.y>valueXY?nextPage(true): prevPage(true)
            }
        }
    }
}

