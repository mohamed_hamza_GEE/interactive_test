// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import ImageMask 1.0
FocusScope{
    //    anchors.fill: parent
    anchors.top:parent.top
    anchors.topMargin:qsTranslate('','synopsis_survey_panel_tm')
    property alias synopsisCloseBtn: synopsisCloseBtn
    property variant model:listingobj.compVpathListing.model
    property variant currentIndex: listingobj.compVpathListing.currentIndex
    property variant synopsisDetails: []
    property bool isSliderNavChanged: false
    property bool isSurveyBTN: false
    property bool isSurveyCategory: false
    property int currentMid: -1

    //    property bool skipOnlyOneSurvey: false
    signal extremeEnds(string direction)
    /**************************************************************************************************************************/
    onExtremeEnds:{
        if(direction=="left" && listingobj.compVpathListing.count>1){
            decrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="right" && listingobj.compVpathListing.count>1){
            incrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="up"){
            widgetList.focustTomainMenu();
        }
    }
    Connections{
        target:(visible)?viewHelper:null
        onSeatchatKarmaActionPerform:{
            launchSeatChat()
        }
    }

    /**************************************************************************************************************************/
    function init(){
        console.log("SeatChatLoaderItem .qml |  init()")
        pif.paxus.startContentLog('seatchat')
        /*        if(viewHelper.fromInvitePopup==true){
            if(!viewHelper.getSeatChatTerms()){
//                synopsisLoader.sourceComponent=blankComp
                synopsisLoader.sourceComponent=seatChatTermsPage
            }else{
//                synopsisLoader.sourceComponent=blankComp
               synopsisLoader.sourceComponent=nickNamePage
            }

        }else{
            seatChatInnerLoader.sourceComponent=blankCompSseatChat
            seatChatInnerLoader.sourceComponent=synopsisPage
        } */
        console.log("SeatChatLoaderItem.qml | viewHelper.shortcutCalled  = >>>>>>>>>>>>>>>"+viewHelper.shortcutCalled)
        if(viewHelper.shortcutCalled){
            launchSeatChat()
            viewHelper.shortcutCalled=false
        }else{
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
               pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":""/*,"showContOnKarma":viewHelper.showContOnKarma*/});
            }
            seatChatInnerLoader.sourceComponent=blankCompSseatChat
            seatChatInnerLoader.sourceComponent=synopsisPage
            forceFocus()
        }
    }
    function clearOnExit(){
        //        viewHelper.fromInvitePopup=false
        pif.paxus.stopContentLog()
        //        isSurveyBTN=false
        //        isSurveyCategory=false
    }
    function forceFocus(){
        seatChatInnerLoader.item.forceActiveFocus()
    }
    function launchSeatChat(){
        core.debug(" terms and condition to be launched ")
        var tmpId=model.getValue(listingobj.compVpathListing.currentIndex,"category_attr_template_id")
        if(dataController.mediaServices.getCidBlockStatus(model.getValue(listingobj.compVpathListing.currentIndex,"cid"))){
            core.debug(" SeatChat Service Blocked")
            viewController.updateSystemPopupQ(7,true)
            return;
        }
        if(viewHelper.checkForExternalBlock("SeatChat"))return;
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
           pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":tmpId});
        }
        viewHelper.mediaListing=listingobj.compVpathListing.currentIndex
        viewController.loadNext(listingobj.compVpathListing.model,listingobj.compVpathListing.currentIndex);
        return;
        //        if(tmpId =="seatchat"){
        //            if(viewHelper.checkForExternalBlock("SeatChat")){return;}
        /*        if(viewHelper.seatchat_enabled){
                core.debug(" enter seat chat | viewHelper.seatchat_enabled = "+viewHelper.seatchat_enabled)
                if(!pif.seatChat.getAppActiveState())pif.seatChat.setAppActive();
                if(viewHelper.getSeatChatTerms()==false){  // first time
                    core.debug(" terms and condition to be launched | nickNamePage= "+viewHelper.getSeatChatTerms())
                    synopsisLoader.sourceComponent=seatChatTermsPage
                }else{
                    synopsisLoader.sourceComponent=nickNamePage
                    core.debug(" terms and condition to be launched | seatChatTermsPage= "+viewHelper.getSeatChatTerms())
                }
            }else{
                core.debug("viewHelper.seatchat_enabled = "+viewHelper.seatchat_enabled)
    //            viewController.showScreenPopup(7)
            }  */
        //        }
    }

    function backBtnPressed(){
        closeLoader()
    }

    /**************************************************************************************************************************/
    Image{
        id:synopsisIMG
        source:viewHelper.configToolImagePath+configTool.config.seatchat.synopsis.panel
        anchors.horizontalCenter: parent.horizontalCenter
    }
    SimpleNavButton{
        id:synopsisCloseBtn
        anchors.right: synopsisIMG.right
        anchors.rightMargin: qsTranslate('','synopsis_close_rm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_close_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTool.config.seatchat.synopsis.close_btn_n;
        highlightimg:  viewHelper.configToolImagePath+configTool.config.seatchat.synopsis.close_btn_h;
        pressedImg: viewHelper.configToolImagePath+configTool.config.seatchat.synopsis.close_btn_p;
        onEnteredReleased:{closeLoader();}
        onPadReleased:extremeEnds(dir)
        onNoItemFound:  {if(dir=="left" || dir=="right" || dir == "up"){
                extremeEnds(dir)
            }else{
                forceFocus()
            }
        }
    }
    MaskedImage{
        id:posterIMGBtn
        visible:!isSurveyCategory
        width: qsTranslate('','synopsis_survey_poster_w')
        height: qsTranslate('','synopsis_survey_poster_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: qsTranslate('','synopsis_survey_poster_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_survey_poster_tm')
        source: model.count>0?viewHelper.cMediaImagePath+model.getValue(currentIndex,"poster"):""
        maskSource:viewController.settings.assetImagePath+qsTranslate('','listing_games_list_source');

    }
    ViewText{
        id:synopsisTitleBtn
        width:isSurveyCategory?qsTranslate('','synopsis_survey_title_w1'):qsTranslate('','synopsis_survey_title_w')
        height:qsTranslate('','synopsis_survey_title_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin:isSurveyCategory?qsTranslate('','synopsis_survey_title_lm1'):qsTranslate('','synopsis_survey_title_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_survey_title_tm')
        varText: [text,configTool.config.discover.synopsis.title_color,qsTranslate('','synopsis_survey_title_fontsize')]
        text:model?(model.count>0?model.getValue(currentIndex,"title"):""):""
        elide: Text.ElideRight
        maximumLineCount: 2
        wrapMode: Text.Wrap

    }
    Component{id:blankCompSseatChat;Item{}}
    Loader{
        id:seatChatInnerLoader
        anchors.fill: synopsisIMG
        sourceComponent:blankCompSseatChat
        onStatusChanged: {
            console.log("seatChatInnerLoader |  loadered>>>>>>>>>>>>>>>>>>>>")
            //            if(seatChatInnerLoader.sourceComponent==blankCompSseatChat)return;
            //                        if (seatChatInnerLoader.status == Loader.Ready){
            //                            seatChatInnerLoader.item.init()

        }

    }
    Component{
        id:synopsisPage
        FocusScope{
            anchors.fill: parent
            property alias continueBtn: continueBtn
            property alias synopsisContentFlickable: synopsisContentFlickable
            Flickable{
                id : synopsisContentFlickable
                width:qsTranslate('','synopsis_survey_desc1_w')
                height:qsTranslate('','synopsis_survey_desc1_h')
                contentWidth: width
                contentHeight: synopsisSubDescIMG.paintedHeight
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','synopsis_survey_desc1_tm')
                anchors.left:parent.left
                anchors.leftMargin: qsTranslate('','synopsis_survey_title_lm')
                clip:true
                ViewText{
                    id:synopsisSubDescIMG
                    width:parent.width
                    height:parent.height
                    varText:[text,configTool.config.seatchat.synopsis.description_color, qsTranslate('','synopsis_survey_desc1_fontsize')]
                    wrapMode: Text.WordWrap
                    text:model?(model.count>0?model.getValue(currentIndex,"description"):""):""
                }
            }
            SimpleNavBrdrBtn{
                id:continueBtn
                focus: true
                property string textColor: continueBtn.activeFocus?configTool.config.seatchat.synopsis.btn_text_h:(continueBtn.isPressed?configTool.config.seatchat.synopsis.btn_text_p:configTool.config.seatchat.synopsis.btn_text_n)
                anchors.bottom:parent.bottom
                anchors.bottomMargin: qsTranslate('','synopsis_survey_play_bm')
                anchors.right: parent.right
                anchors.rightMargin: qsTranslate('','synopsis_survey_play_rm')
                width: qsTranslate('','synopsis_survey_play_w')
                height: qsTranslate('','synopsis_survey_play_h')
                btnTextWidth:parseInt(qsTranslate('',"synopsis_survey_play_w"),10);
                buttonText: [configTool.language.global_elements.cont,continueBtn.textColor,qsTranslate('','synopsis_survey_play_fontsize')]
                isHighlight: activeFocus
                normalImg: viewHelper.configToolImagePath+configTool.config.seatchat.synopsis.btn_n
                highlightImg: viewHelper.configToolImagePath+configTool.config.seatchat.synopsis.btn_h
                pressedImg: viewHelper.configToolImagePath+configTool.config.seatchat.synopsis.btn_p
                normalBorder: [qsTranslate('','synopsis_survey_play_margin')]
                highlightBorder: [qsTranslate('','synopsis_survey_play_margin')]
                onEnteredReleased: {launchSeatChat()}
                onNoItemFound: {
                    if(dir=="up"){
                        synopsisCloseBtn.forceActiveFocus()
                    } else if(dir=="left" || dir=="right") extremeEnds(dir)
                }
            }
        }
    }

}
