import QtQuick 1.1

ListView {
    id:lv
    boundsBehavior: ListView.StopAtBounds;
    snapMode:ListView.SnapToItem;
    preferredHighlightBegin:0;
    preferredHighlightEnd:height;
    highlightRangeMode: "StrictlyEnforceRange"
    clip:true;
    interactive:(contentHeight>height)
    onMovementEnded:{

        if(contentY+height>contentHeight){
            contentYAnim.enabled=true;
            contentY=contentHeight-height;
            contentYAnim.enabled=false;
        }

    }
    Behavior on contentY{
        id:contentYAnim;
        NumberAnimation{duration:200;}
    }
}
