// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"

FocusScope {
    property variant configTag:configTool.config.seatchat.nickname;
    property string freeText: ""
    Connections{
        target:(visible)?viewHelper:null
        onVkbData:{
            if(identifier=="chat_nickname"){
                core.debug("vkbData ======"+vkbData+ " textBox.text = "+textBox.text)
                textBox.text=vkbData
                 launchSeatChatWindow()
            }
        }
        onVkbClosed:{
            nick_vkb.forceActiveFocus()
        }

        onSeatChatServiceBlock:{
            console.log("NickName.qml | onSeatChatServiceBlock  = ")
            viewHelper.homeAction();
            viewController.updateSystemPopupQ(7,true);
        }

        onSeatchaNickNametKarmaActionPerform:{
               console.log("NickName.qml | onSeatchatKarmaActionPerform  = ")
            launchSeatChatWindow()
        }
    }
    Connections{
        target:(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)?pif.sharedModelServerInterface:null
        onSigDataReceived:{
            core.debug("VKB | onSigDataReceived "+api);
            switch(api){
            case "keyboardData":{
                if(textBox.text == params.keyboardData)return;
                else{
                    if(viewHelper.identifier=="chat_nickname"){
                        textBox.text = params.keyboardData;
                        textBox.cursorPosition=textBox.text.length;
                    }
                }
            }
            }
        }
    }

    function init(){
        console.log("NickName.qml | init() = ")
        viewHelper.setHelpTemplate("seatchat_name");
        if(viewHelper.getSeatChatTerms()){
            if(viewHelper.isValidSearchString(textBox.text) && pif.seatChat.setSeatName(textBox.text)==true){
                //                synopsisLoader.sourceComponent=blankComp
                synopsisLoader.sourceComponent=mainSeatChatWindowPage
                pif.seatChat.setSeatName(textBox.text);
            }else{
                if(pif.seatChat.setSeatName(textBox.text)==false){
                    pif.seatChat.setSeatName(textBox.text.text);
                }
            }
        }else{
            if(pif.seatChat.setSeatName(textBox.text)==false){
                pif.seatChat.setSeatName(textBox.text.text);
            }
        }

        textBox.forceActiveFocus()
    }

    function backBtnPressed(){
        closeLoader()
    }

    function clearOnExit(){
        //        visible=false;
        //        synopsisLoader.sourceComponent=blankComp;
    }

    function launchSeatChatWindow(){
        if(viewHelper.getSeatChatTerms()){
            if(viewHelper.isValidSearchString(textBox.text)&& pif.seatChat.setSeatName(textBox.text)==true){
                synopsisLoader.sourceComponent=mainSeatChatWindowPage
                pif.seatChat.setSeatName(textBox.text);}
        }else{
            if(pif.seatChat.setSeatName(textBox.text)==false){
                pif.seatChat.setSeatName(textBox.text.text);
            }

        }

    }


    Item{
        id:posterobj
        anchors.fill: parent
        property alias staticBG: staticBG
        Image{
            id:listingBG
            source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
        }
        Image{
            id:staticBG
            anchors.bottom:parent.bottom
            source:viewHelper.configToolImagePath+configTool.config.seatchat.screen.seatcat_background
            MouseArea{
                anchors.fill:parent
                onClicked: {

                }
            }
        }
    }

    Image{
        id:panelBG
        anchors.horizontalCenter: posterobj.horizontalCenter
        anchors.top:posterobj.top;
        anchors.topMargin: qsTranslate('','seatchat_nickname_panel_tm')
        source:viewHelper.configToolImagePath+configTag["panel"]
        MouseArea{
            anchors.fill:parent
            onClicked: {

            }
        }
    }


    ViewText{
        id:nickName_Title
        anchors.top: panelBG.top;
        anchors.left:panelBG.left
        anchors.leftMargin: qsTranslate('','seatchat_nickname_title_lm')
        anchors.topMargin:qsTranslate('','seatchat_nickname_title_tm')
        width:qsTranslate('','seatchat_nickname_title_w')
        height:qsTranslate('','seatchat_nickname_title_h')
        varText:[configTool.language.seatchat.nickname_title,configTag["title_color"],qsTranslate('',"seatchat_nickname_title_fontsize")]
    }

    ViewText{
        id:nickName_SubTitle
        anchors.top: nickName_Title.bottom;
        anchors.left:panelBG.left
        anchors.leftMargin: qsTranslate('','seatchat_nickname_msg_lm')
        anchors.topMargin:qsTranslate('','seatchat_nickname_msg_tm')
        width:qsTranslate('','seatchat_nickname_msg_w')
        height:qsTranslate('','seatchat_nickname_msg_h')
        varText:[configTool.language.seatchat.nickname_message,configTag["title_color"],qsTranslate('',"seatchat_nickname_msg_fontsize")]
    }

    SimpleNavButton{
        id:synopsisCloseBtn
        anchors.right: panelBG.right
        anchors.rightMargin: qsTranslate('','synopsis_close_rm')
        anchors.top:panelBG.top
        anchors.topMargin:qsTranslate('','synopsis_close_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTag["close_btn_n"];
        highlightimg:  viewHelper.configToolImagePath+configTag["close_btn_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
        nav:["","","",textBox]
        onEnteredReleased:{seatchatScreen.backBtnPressed()}
        onNoItemFound:  {if(dir=="up" ){
                widgetList.focustTomainMenu()
            }
        }
    }

    SimpleNavBrdrBtn{
        id:nickName_text_box
        //        property string textColor: continueBtn.activeFocus?configTool.config.discover.synopsis.btn_text_h:(continueBtn.isPressed?configTool.config.discover.synopsis.btn_text_p:configTool.config.discover.synopsis.btn_text_n)
        width: qsTranslate('','seatchat_nickname_input_w')
        anchors.horizontalCenter: panelBG.horizontalCenter
        anchors.top:panelBG.top
        anchors.topMargin: qsTranslate('','seatchat_nickname_input_tm')
        isHighlight: textBox.activeFocus
        normalImg: viewHelper.configToolImagePath+configTag["input_n"];
        highlightImg: viewHelper.configToolImagePath+configTag["input_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["input_p"];
        normalBorder: [qsTranslate('','seatchat_nickname_input_margin')]
        highlightBorder: [qsTranslate('','seatchat_nickname_input_margin')]
        nav:["",synopsisCloseBtn,"",textBox]
        //        onPadReleased: if(dir=="right"){extremeEnds("right")}
        //        onNoItemFound:  if(dir=="left" || dir=="right")extremeEnds(dir)
        onEnteredReleased:{console.log("nickName_text_box  ===== lanunch vkb")

        }
        focus:true
    }


    Text{
        id:seatno_txt
        width:qsTranslate('','seatchat_seatno_txtw')
        height: qsTranslate('','seatchat_seatno_txth')
        anchors.left:nickName_text_box.left
        anchors.leftMargin: qsTranslate('','seatchat_seatno_txtlm')
        color:configTag["seatnum_color"]
        text:pif.getSeatNumber().toUpperCase()+ " | "
        font.pixelSize:  qsTranslate('','seatchat_seatno_fontsize')
        font.family:viewHelper.adultFont;
        anchors.verticalCenter: nickName_text_box.verticalCenter
    }


    Text {
        id:dummy_txt
        width: qsTranslate('','seatchat_nickname_input_txtw')
        height: qsTranslate('','seatchat_nickname_input_txth')//refMcStyle.nick_text.w
        color:"#5a5b5e"//global.refFontColor[refMcStyle.nick_text.fontcolor]
        anchors.left:nickName_text_box.left
        anchors.leftMargin:qsTranslate('','seatchat_nickname_input_txtlm')
        anchors.verticalCenter:nickName_text_box.verticalCenter
        text:(textBox.activeFocus)?"":""+configTool.language.seatchat.nickname_title
        font.pixelSize:  qsTranslate('','seatchat_nickname_input_fontsize')
        font.family:viewHelper.adultFont;
        wrapMode: Text.Wrap

    }

    TextInput{
        id:textBox
        property int lineLimit :1
        width: qsTranslate('','seatchat_nickname_input_txtw')
        height: qsTranslate('','seatchat_nickname_input_txth')
        color:"black"
        anchors.left:nickName_text_box.left
        anchors.leftMargin:qsTranslate('','seatchat_nickname_input_txtlm')
        anchors.verticalCenter: nickName_text_box.verticalCenter
        font.pixelSize:  qsTranslate('','seatchat_nickname_input_fontsize')
        font.family:viewHelper.adultFont;
        cursorVisible:activeFocus
        activeFocusOnPress:false
        text:viewHelper.nickBoxInput
        maximumLength:15
        onActiveFocusChanged: {
            if((pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)){  // CR changes
                viewHelper.identifier="chat_nickname"
                viewHelper.vkbInputData=textBox.text
                core.debug("onActiveFocusChanged | viewHelper.vkbInputData "+textBox.text)
                widgetList.getVkbRef().launchVKB("chat_nickname",textBox.text)
            }
        }

        onTextChanged: {
            if(textBox.lineCount > lineLimit)
                textBox.text = textBox.text.substring(0,textBox.cursorPosition-1) + textBox.text.substring(textBox.cursorPosition,textBox.text.length)
            if(textBox.text == ""){
                dummy_txt.text =configTool.language.seatchat.nickname_title;
            }else{
                dummy_txt.text=""
            }
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("keyboardData",{keyboardData:textBox.text});
            }
        }

        Keys.onRightPressed: {
            nick_vkb.forceActiveFocus()
        }

        Keys.onUpPressed:{
            if(cursorRectangle.y==0)synopsisCloseBtn.forceActiveFocus()
            else event.accepted=false
        }
        Keys.onDownPressed: {

            if(cursorRectangle.y==0){
                if(continueBtn.isDisable){return
                }else continueBtn.forceActiveFocus();
            }
            else event.accepted=false
        }

        Keys.onReleased: {
            switch(event.key){
            case Qt.Key_Return:
            case Qt.Key_Enter:{
               // widgetList.getVkbRef().launchVKB("chat_nickname",textBox.text)
                launchSeatChatWindow()
                event.accepted = true;
            }break;
            }
        }
        MouseArea{
            anchors.fill:parent;
            onClicked:{
                textBox.forceActiveFocus();
                viewHelper.identifier="chat_nickname"
                viewHelper.vkbInputData=textBox.text
                core.debug("onActionReleased | viewHelper.vkbInputData "+textBox.text)
                widgetList.getVkbRef().launchVKB("chat_nickname",textBox.text)
            }
        }
    }
    SimpleNavBrdrBtn{
        id:nick_vkb
        anchors.right: nickName_text_box.right
        anchors.rightMargin:qsTranslate('','seatchat_nickname_vkb_rm')
        anchors.verticalCenter: nickName_text_box.verticalCenter
        normalImg: viewHelper.configToolImagePath+configTag["vkb_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["vkb_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["vkb_p"]
        nav: [textBox,synopsisCloseBtn,"",continueBtn.isDisable?"":continueBtn]
        onEnteredReleased: {
            viewHelper.identifier="chat_nickname"
            viewHelper.vkbInputData=textBox.text
            core.debug("onActionReleased | viewHelper.vkbInputData "+textBox.text)
            widgetList.getVkbRef().launchVKB("chat_nickname",textBox.text)
        }
    }

    SimpleNavBrdrBtn{
        id:continueBtn
        focus: true
        property string textColor: continueBtn.activeFocus?configTool.config.seatchat.synopsis.btn_text_h:(continueBtn.isPressed?configTool.config.seatchat.synopsis.btn_text_p:configTool.config.seatchat.synopsis.btn_text_n)
        anchors.top:nickName_text_box.bottom
        anchors.topMargin: qsTranslate('','seatchat_continue_btn_bm')
        anchors.right: nickName_text_box.right
        width: parseInt(qsTranslate('','seatchat_continue_btn_w'),10)
        height:parseInt(qsTranslate('','seatchat_continue_btn_h'),10)
        btnTextWidth:parseInt(qsTranslate('',"seatchat_continue_btn_w"),10);
        buttonText: [configTool.language.seatchat.continue_text,continueBtn.textColor,qsTranslate('','seatchat_continue_btn_fontsize')]
        isHighlight: activeFocus
        isDisable:{
            if(viewHelper.getSeatChatTerms()==true ){
                if(viewHelper.isValidSearchString(textBox.text)){return false}else return true
            }else return false
        }

        normalImg: viewHelper.configToolImagePath+configTag["btn_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["btn_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["btn_p"]
        normalBorder: [qsTranslate('','seatchat_continue_btn_margin')]
        highlightBorder: [qsTranslate('','seatchat_continue_btn_margin')]
        onEnteredReleased: {if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:false});
            }launchSeatChatWindow()}
        onNoItemFound: {
            if(dir=="up"){
                textBox.forceActiveFocus()
            } else{ continueBtn.forceActiveFocus()}
        }
    }


}
