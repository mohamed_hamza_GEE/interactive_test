import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    id: appRoutesMap;
    width: core.width; height: core.height;

    property string currAppImages: microAppDir+currAppName+"/images/"
    property string xmlModelSource: microAppDir+currAppName+"/"+currAppName+"_data.xml"

    property real scaleVal : 1;
    property int zoomClickCount: 0;
    property int selectedIndex: -1;

    function init() {
        core.log("AppRoutesMap | init");
        routeInfoData.source = xmlModelSource;
        routeListData.source = xmlModelSource;
        scaleVal = 1;
        selectedIndex = -1;
        zoomClickCount = 0;
    }
    function reload(){
        core.log("AppRoutesMap | reload ");
        routeInfoData.source = xmlModelSource;
        routeListData.source = xmlModelSource;
        routeInfoData.reload();
        routeListData.reload();
    }
    function clearOnExit() {scaleVal = 1;}
    function setMainMenuTitleText(obj,Dmodel,ind){
        obj.text = (Dmodel.get(ind).menuTitle_cdata !== "") ? Dmodel.get(ind).menuTitle_cdata : Dmodel.get(ind).menuTitle;
        obj.color = configTool.config.aviancaapp.screen.text_header_menu;
        obj.font.pixelSize = qsTranslate('','aviapp_header_menu_fontsize');
    }
    function setHeaderTitleText(obj,Dmodel,ind){
        obj.text = (Dmodel.get(ind).title_cdata != "") ? Dmodel.get(ind).title_cdata : Dmodel.get(ind).title;
        obj.color = configTool.config.aviancaapp.screen.text_header_title;
        obj.font.pixelSize = qsTranslate('','aviapp_header_title_fontsize');
    }
    function setText(obj,dmodel,ind,modelName){
        var modelName_cdata = modelName+"_cdata";
        obj.text = (eval("dmodel.get("+ind+")."+modelName_cdata) !== "") ? eval("dmodel.get("+ind+")."+modelName_cdata) : eval("dmodel.get("+ind+")."+modelName);
        obj.color = configTool.config.aviancaapp.screen.text_fleet_icons;
        obj.font.pixelSize = qsTranslate('','aviapp_map_indicator_fontsize')
    }
    function setIcon(obj,Dmodel,ind,modelName){
        obj.source = "";
        obj.source = currAppImages + (eval("Dmodel.get("+ind+")."+modelName)) ;
    }
    function loadAirlineInfoVOD(dModel) {
        viewHelper.setMediaListingModel(dModel);
        viewHelper.setMediaListingIndex(0)
        viewHelper.storeTrailerParentMid(dModel.getValue(0,"mid"));
        viewHelper.initializeVOD("restart","trailer","",false,true);
    }
    function setRouteImage(imageIndex){
        mapImage.source = currAppImages + routeListData.get(imageIndex).list_image;
    }
    function zoomIn(val,scale){
        var maxScale = ((val * scale) + 1);
        var tmpV = scaleVal + val;
        //scaleVal += val
        if(tmpV >= maxScale)scaleVal = maxScale;
        else scaleVal = tmpV;
    }
    function zoomOut(val){
        if(scaleVal > 1)
            scaleVal -= val;
        if(scaleVal<=1)
            scaleVal = 1
    }

    function pageUp(){
        if(childAppList.contentY>0){
            if((childAppList.contentY-childAppList.height)>0){
                childAppList.contentY = childAppList.contentY-childAppList.height
            }else{
                childAppList.contentY = 0
            }
            var firstIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            childAppList.currentIndex = firstIndex;
            childAppList.positionViewAtIndex(firstIndex,ListView.Beginning)
        }
        childAppList.forceActiveFocus()
    }
    function pageDown(){
        if(childAppList.contentY<(childAppList.contentHeight-childAppList.height)){
            if((childAppList.contentY+childAppList.height)<(childAppList.contentHeight-childAppList.height)){
                childAppList.contentY = childAppList.contentY+childAppList.height
            }else{
                childAppList.contentY = childAppList.contentY+((childAppList.contentHeight-childAppList.height)-childAppList.contentY)
            }
            var lastIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            childAppList.currentIndex = lastIndex+1;
            childAppList.positionViewAtIndex(lastIndex,ListView.Beginning)
        }
        childAppList.forceActiveFocus()
    }
    function checkArrow(){
        if( mapImageContainer.contentHeight > mapImageContainer.height){
            navigationButton.visible = true
        } else if( mapImageContainer.contentWidth> mapImageContainer.width){
            navigationButton.visible = true
        } else {
            navigationButton.visible = false
        }
    }
    function languageUpdate(){
        routeInfoData.reload()
        routeListData.reload()
    }
    XmlListModel {
        id: routeInfoData;
        query: "/application/screen";

        //        XmlRole { name:"title1"; query: "text1/"+langCode+"/@_value/string()" }
        //        XmlRole { name:"title1_cdata"; query: "text1/"+langCode+"/string()" }
        XmlRole { name:"menuTitle"; query: "text2/"+langCode+"/@_value/string()" }
        XmlRole { name:"menuTitle_cdata"; query: "text2/"+langCode+"/string()" }
        XmlRole { name : "conCenter_text"; query : "text10/"+langCode+"/@_value/string()" }
        XmlRole { name : "conCenter_text_cdata"; query : "text10/"+langCode+"/string()" }
        XmlRole { name : "routesTACA_text"; query : "text11/"+langCode+"/@_value/string()" }
        XmlRole { name : "routesTACA_text_cdata"; query : "text11/"+langCode+"/string()" }
        XmlRole { name : "routesAvianca_text"; query : "text12/"+langCode+"/@_value/string()" }
        XmlRole { name : "routesAvianca_text_cdata"; query : "text12/"+langCode+"/string()" }
        XmlRole { name : "zoom_text"; query : "text13/"+langCode+"/@_value/string()" }
        XmlRole { name : "zoom_text_cdata"; query : "text13/"+langCode+"/string()" }
        XmlRole { name : "conVideo_text"; query : "text14/"+langCode+"/@_value/string()" }
        XmlRole { name : "conVideo_text_cdata"; query : "text14/"+langCode+"/string()" }
        XmlRole { name : "conCenter_icon"; query : "image3/@_value/string()" }
        XmlRole { name : "routesTACA_icon"; query : "image4/@_value/string()" }
        XmlRole { name : "routesAvianca_icon"; query : "image5/@_value/string()" }

        onCountChanged: {
            setIcon(connCenterImage,routeInfoData,0,"conCenter_icon")
            setIcon(tacaRouteImage,routeInfoData,0,"routesTACA_icon")
            setIcon(aviancaRoutesImage,routeInfoData,0,"routesAvianca_icon")
            setMainMenuTitleText(routesMapBackground.headerMenuTitle,routeInfoData,0);
            setText(connCenterText,routeInfoData,0,"conCenter_text")
            setText(tacaRouteText,routeInfoData,0,"routesTACA_text")
            setText(aviancaRoutesText,routeInfoData,0,"routesAvianca_text")
            setText(zoomText,routeInfoData,0,"zoom_text")
            setText(playBtnText,routeInfoData,0,"conVideo_text")
        }
    }
    XmlListModel {
        id: routeListData;
        query: "/application/screen/list1/listItem";

        XmlRole { name : "route_list_text"; query : "text4/"+langCode+"/@_value/string()" }
        XmlRole { name : "route_list_text_cdata"; query : "text4/"+langCode+"/string()" }
        XmlRole { name : "title"; query : "text5/"+langCode+"/@_value/string()" }
        XmlRole { name : "title_cdata"; query : "text5/"+langCode+"/string()" }
        XmlRole { name : "list_image"; query : "image2/@_value/string()" }
        //        XmlRole { name : "list_image_x2"; query : "image3/@_value/string()" }
        //        XmlRole { name : "list_image_x4"; query : "image4/@_value/string()" }
        //        XmlRole { name : "list_image_x6"; query : "image5/@_value/string()" }
        XmlRole { name : "conVideo"; query : "text6/"+langCode+"/@_value/string()" }

        onCountChanged: {
            childAppList.model = "";
            childAppList.model=routeListData;
            setRouteImage(0);
            setHeaderTitleText(routesMapBackground.headerTitle,routeListData,0);
        }
    }
    AviancaAppBg {
        id: routesMapBackground;
        appBackgroundImage: configTool.config.aviancaapp.screen.app_bg;
        appHeaderImage: configTool.config.aviancaapp.screen.app_header;
        appSideMenuPanelImage: configTool.config.aviancaapp.screen.sidemenu_panel;
        appHeaderTitle: (routeListData.get(selectedIndex).title_cdata !== "") ? routeListData.get(selectedIndex).title_cdata : routeListData.get(selectedIndex).title;
        appHeaderMenuTitle: (routeInfoData.get(selectedIndex).menuTitle_cdata !== "") ? routeInfoData.get(selectedIndex).menuTitle_cdata : routeInfoData.get(selectedIndex).menuTitle;
        Keys.onDownPressed: {
            if(upArrowContainer.visible){
                upArrowImage.forceActiveFocus()
            }else{
                childAppList.forceActiveFocus();
            }
        }
    }

    Item {
        id: routesDataContainer;
        width: core.width; height: core.height;
        anchors.top:parent.top;
        anchors.topMargin: routesMapBackground.headerImage.height;

        Flickable {
            id: mapImageContainer;
            x: qsTranslate('','aviapp_map_x');
            height: (mapImage.source == "")? (microAppName == 'aviancaApp_business')?320 : 240 :qsTranslate('','aviapp_map_h');
            width: (mapImage.source == "")? (microAppName == 'aviancaApp_business')? 920 : 690 :qsTranslate('','aviapp_map_w');
            anchors.top:parent.top;
            anchors.topMargin: qsTranslate('','aviapp_map_tm');
            contentWidth:(mapImage.status == Image.Ready)?mapImage.sourceSize.width*scaleVal:mapImageContainer.width
            contentHeight:(mapImage.status == Image.Ready)?mapImage.sourceSize.height*scaleVal:mapImageContainer.height
            boundsBehavior:Flickable.StopAtBounds
            property int prevContentWidth:0
            property int prevContentHeight:0
            property bool stopAnimation:false

            clip:true;

            Image{
                id: mapImage;
                source: currAppImages + routeListData.get(selectedIndex).list_image;
                onSourceChanged: {
                    if(mapImage.status == Image.Ready){
                        scaleVal = 1
                        mapImageContainer.contentX=0
                        mapImageContainer.contentY=0
                    }
                }
                smooth: true;
                transform: Scale{
                    xScale:scaleVal;
                    yScale:scaleVal;
                }
            }
            onContentWidthChanged: {
                //  console.log(" Content Width Changed >>>>>>>>>>>>> "+contentWidth)
                stopAnimation=true

                var newScaleX=contentX*scaleVal
                var newScaleY=contentY*scaleVal

                if(contentWidth>prevContentWidth){
                    contentX=(newScaleX>=(contentWidth/2))?(contentWidth/2):(newScaleX)
                }else{
                    // zoomout
                }

                prevContentWidth=contentWidth
                if(contentHeight>prevContentHeight)
                    contentY=(newScaleY>=(contentHeight/2))?(contentHeight/2):(newScaleY)
                else {
                    // zoomout
                }

                prevContentHeight=contentHeight

            }



            Behavior on contentX {
                NumberAnimation{id:contentXAnim;duration: mapImageContainer.stopAnimation?1:300;onRunningChanged:if(!running && !contentYAnim.running )mapImageContainer.stopAnimation=false }
            }
            Behavior on contentY {
                NumberAnimation{id:contentYAnim;duration: mapImageContainer.stopAnimation?1:300;onRunningChanged:if(!running&& !contentXAnim.running)mapImageContainer.stopAnimation=false}
            }
        }

        Rectangle {
            id: mapImageBorder;
            height: mapImageContainer.height;
            width: mapImageContainer.width;
            border.width: qsTranslate('','aviapp_map_border_w');
            border.color: qsTranslate('','aviapp_map_border_color');
            anchors.fill: mapImageContainer;
            color: "transparent";
        }
        Item {
            id: navigationButton;
            //            visible: false;
            visible:( mapImageContainer.contentWidth>mapImageContainer.width || mapImageContainer.contentHeight>mapImageContainer.height  )//(scaleVal > 1) ? true : false;
            height: upNavButton.height + downNavButton.height;
            width: leftNavButton.width + rightNavButton.width;
            anchors.left : mapImageBorder.left;
            anchors.bottom: mapImageBorder.bottom;

            SimpleButton {
                id: leftNavButton;
                anchors.left : parent.left;
                anchors.leftMargin: qsTranslate('','aviapp_map_arrow_lm');
                anchors.bottom: parent.bottom;
                anchors.bottomMargin: qsTranslate('','aviapp_map_arrow_bm');
                isHighlight: activeFocus
                isDisable:(mapImageContainer.contentX <= 0)?true:false
                onIsDisableChanged: {if(isDisable)rightNavButton.forceActiveFocus();}
                normImg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_left_n;
                highlightimg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_left_h;
                pressedImg:viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_left_h;

                onEnteredReleased: {
                    var temp = mapImageContainer.contentX - mapImageContainer.width
                    if(temp > 0){
                        mapImageContainer.contentX = temp
                    } else {
                        mapImageContainer.contentX =0
                    }
                }

                Keys.onUpPressed: {
                    if(!upNavButton.isDisable)upNavButton.forceActiveFocus();
                    else routesMapBackground.closeBtn.forceActiveFocus();
                }

                Keys.onDownPressed: {
                    if(!downNavButton.isDisable)downNavButton.forceActiveFocus();
                    else zoomOutBtn.closeBtn.forceActiveFocus();
                }
                Keys.onRightPressed: {
                    if(!rightNavButton.isDisable)rightNavButton.forceActiveFocus();
                    else childAppList.forceActiveFocus();
                }
            }
            SimpleButton {
                id: upNavButton;
                anchors.left : leftNavButton.right;
                anchors.bottom: leftNavButton.top;
                anchors.bottomMargin: qsTranslate('','aviapp_map_arrow_gap');
                anchors.leftMargin: qsTranslate('','aviapp_map_arrow_gap');
                isHighlight: activeFocus
                isDisable:(mapImageContainer.contentY <= 0)?true:false
                onIsDisableChanged: {if(isDisable)downNavButton.forceActiveFocus();}
                normImg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_up_n;
                highlightimg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_up_h;
                pressedImg:viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_up_h;

                onEnteredReleased: {
                    var temp = mapImageContainer.contentY - mapImageContainer.height
                    if(temp > 0){
                        mapImageContainer.contentY = temp
                    } else {
                        mapImageContainer.contentY =0;
                    }
                }

                Keys.onUpPressed: {routesMapBackground.closeBtn.forceActiveFocus();}
                Keys.onDownPressed: {
                    if(!downNavButton.isDisable)downNavButton.forceActiveFocus();
                    else zoomOutBtn.forceActiveFocus();
                }
                Keys.onLeftPressed: {
                    if(!leftNavButton.isDisable)leftNavButton.forceActiveFocus();
                }
                Keys.onRightPressed: {
                    if(!rightNavButton.isDisable)rightNavButton.forceActiveFocus();
                    else childAppList.forceActiveFocus();
                }
            }

            SimpleButton {
                id: rightNavButton;
                anchors.left : upNavButton.right;
                anchors.top: leftNavButton.top;
                anchors.leftMargin: qsTranslate('','aviapp_map_arrow_gap');
                isHighlight: activeFocus
                isDisable:(mapImageContainer.contentX < (mapImageContainer.contentWidth-mapImageContainer.width))?false:true
                onIsDisableChanged: {if(isDisable)leftNavButton.forceActiveFocus();}

                normImg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_right_n;
                highlightimg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_right_h;
                pressedImg:viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_right_h;

                onEnteredReleased: {
                    var temp = mapImageContainer.contentX + mapImageContainer.width
                    if((temp+mapImageContainer.width) < mapImageContainer.contentWidth){
                        mapImageContainer.contentX = temp
                    } else {
                        mapImageContainer.contentX = mapImageContainer.contentWidth - mapImageContainer.width
                    }
                }
                Keys.onUpPressed: {
                    if(!upNavButton.isDisable)upNavButton.forceActiveFocus();
                    else routesMapBackground.closeBtn.forceActiveFocus();
                }
                Keys.onDownPressed: {
                    if(!downNavButton.isDisable)downNavButton.forceActiveFocus();
                    else zoomOutBtn.forceActiveFocus();
                }
                Keys.onLeftPressed: {
                    if(!leftNavButton.isDisable)leftNavButton.forceActiveFocus();
                }
                Keys.onRightPressed: {childAppList.forceActiveFocus();}
            }
            SimpleButton {
                id: downNavButton;
                anchors.left : leftNavButton.right;
                anchors.top: leftNavButton.bottom;
                anchors.topMargin: qsTranslate('','aviapp_map_arrow_gap');
                anchors.leftMargin: qsTranslate('','aviapp_map_arrow_gap');
                isHighlight: activeFocus
                isDisable:(mapImageContainer.contentY < (mapImageContainer.contentHeight-mapImageContainer.height))?false:true
                onIsDisableChanged: {if(isDisable)upNavButton.forceActiveFocus();}

                normImg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_dn_n;
                highlightimg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_dn_h;
                pressedImg:viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.map_dn_h;

                onEnteredReleased: {
                    var temp = mapImageContainer.contentY + mapImageContainer.height
                    if((temp+mapImageContainer.height) < mapImageContainer.contentHeight){
                        mapImageContainer.contentY = temp
                    } else {
                        mapImageContainer.contentY = mapImageContainer.contentHeight - mapImageContainer.height;
                    }
                }

                Keys.onUpPressed: {
                    if(!upNavButton.isDisable)upNavButton.forceActiveFocus();
                    else routesMapBackground.closeBtn.forceActiveFocus();
                }
                Keys.onDownPressed: {zoomOutBtn.forceActiveFocus();}
                Keys.onLeftPressed: {
                    if(!leftNavButton.isDisable)leftNavButton.forceActiveFocus();
                }
                Keys.onRightPressed: {
                    if(!rightNavButton.isDisable)rightNavButton.forceActiveFocus();
                    else childAppList.forceActiveFocus();
                }
            }
        }

        Row{
            id:routeInfo;
            anchors.top: mapImageContainer.bottom;
            anchors.topMargin: qsTranslate('','aviapp_map_indicator_tm');
            anchors.left: mapImageContainer.left;
            anchors.leftMargin: qsTranslate('','aviapp_map_indicator_lm');
            spacing: qsTranslate('','aviapp_map_indicator_item_gap');

            Item{
                id: connCenter;
                height: connCenterText.paintedHeight;
                width: connCenterImage.paintedWidth + connCenterText.paintedWidth + parseInt(qsTranslate('','aviapp_map_indicator_text_gap'));
                Image {
                    id: connCenterImage;
                    //                    source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.connection_center;
                }
                ViewText {
                    id:connCenterText;
                    width: qsTranslate('','aviapp_map_indicator_text_w');
                    anchors.left: connCenterImage.right; anchors.verticalCenter: connCenterImage.verticalCenter;
                    anchors.leftMargin: qsTranslate('','aviapp_map_indicator_text_gap');
                    wrapMode: Text.Wrap;
                    textFormat: Text.StyledText;
                    maximumLineCount:1;
                    elide: Text.ElideRight;
                }
            }
            Item{
                id:tacaRoute;
                height: tacaRouteImage.height;
                width: connCenterImage.width + tacaRouteText.paintedWidth + parseInt(qsTranslate('','aviapp_map_indicator_text_gap'))

                Image {
                    id: tacaRouteImage;
                    //source: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.taca_routes;
                }
                ViewText {
                    id:tacaRouteText;
                    width: qsTranslate('','aviapp_map_indicator_text_w');
                    anchors.left: tacaRouteImage.right; anchors.verticalCenter: tacaRouteImage.verticalCenter;
                    anchors.leftMargin: qsTranslate('','aviapp_map_indicator_text_gap');
                    wrapMode: Text.Wrap;
                    textFormat: Text.StyledText;
                    maximumLineCount:1;
                    elide: Text.ElideRight;
                }
            }
            Item{
                id:avincaRoutes;
                height: aviancaRoutesText.height;
                width: aviancaRoutesImage.width + aviancaRoutesText.paintedWidth + parseInt(qsTranslate('','aviapp_map_indicator_text_gap'))
                Image {
                    id: aviancaRoutesImage;
                    //                    source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.avianca_routes;
                }
                ViewText {
                    id:aviancaRoutesText;
                    width: qsTranslate('','aviapp_map_indicator_text_w');
                    anchors.left: aviancaRoutesImage.right; anchors.verticalCenter: aviancaRoutesImage.verticalCenter;
                    wrapMode: Text.Wrap;
                    anchors.leftMargin: qsTranslate('','aviapp_map_indicator_text_gap');
                    textFormat: Text.StyledText;
                    maximumLineCount:1;
                    elide: Text.ElideRight;
                }
            }
        }

        Rectangle{
            id: verticalDivider;
            height: qsTranslate('','aviapp_map_divider_h');
            width: qsTranslate('','aviapp_map_divider_w');
            anchors.top: routeInfo.bottom;
            anchors.topMargin: qsTranslate('','aviapp_map_divider_tm');
            anchors.left:mapImageContainer.left;
            color: qsTranslate('','aviapp_map_divider_color');
        }

        SimpleButton {
            id: zoomOutBtn;
            anchors.left: verticalDivider.left;
            anchors.top: verticalDivider.bottom;
            anchors.topMargin: qsTranslate('','aviapp_zoom_tm');
            isHighlight: activeFocus
            normImg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.zoom_out_n;
            highlightimg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.zoom_out_h;
            pressedImg:viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.zoom_out_h;
            onEnteredReleased: {
                zoomOut(0.3);
            }
            Keys.onUpPressed: {
                if(navigationButton.visible){
                    downNavButton.forceActiveFocus();
                }else{
                    routesMapBackground.closeBtn.forceActiveFocus();
                }
            }
            Keys.onRightPressed: {zoomInBtn.forceActiveFocus();}
        }
        ViewText {
            id: zoomText;
            anchors.left: zoomOutBtn.right;
            anchors.verticalCenter: zoomOutBtn.verticalCenter;
            width: qsTranslate('','aviapp_zoom_text_w');
            wrapMode: Text.Wrap;
            maximumLineCount:2;
            textFormat: Text.StyledText;
            elide: Text.ElideRight;
        }
        SimpleButton {
            id: zoomInBtn;
            anchors.left: zoomText.right;
            anchors.verticalCenter: zoomText.verticalCenter;
            isHighlight: activeFocus
            normImg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.zoom_in_n
            highlightimg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.zoom_in_h
            pressedImg:viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.zoom_in_h;
            onEnteredReleased: {
                zoomIn(0.3,3);
            }
            Keys.onUpPressed: {
                if(navigationButton.visible){
                    downNavButton.forceActiveFocus();
                }else{
                    routesMapBackground.closeBtn.forceActiveFocus();
                }
            }
            Keys.onLeftPressed: {zoomOutBtn.forceActiveFocus();}
            Keys.onRightPressed: {
                if(!playBtn.isDisable) playBtn.forceActiveFocus();
                else childAppList.forceActiveFocus();
            }
        }

        Rectangle{
            id: horizontalDivider;
            height: qsTranslate('','aviapp_map_divider_small_h');
            width: qsTranslate('','aviapp_map_divider_small_w');
            anchors.verticalCenter: zoomInBtn.verticalCenter;
            anchors.left:zoomInBtn.right;
            anchors.leftMargin: qsTranslate('','aviapp_map_divider_small_gap');
            color: qsTranslate('','aviapp_map_divider_color');
        }

        Item {
            id: connPlayContainer;
            height: (playBtn.paintedHeight > playBtnText) ? playBtn.paintedHeight : playBtnText.paintedHeight;
            width: qsTranslate('','aviapp_map_play_w');
            anchors.left: horizontalDivider.right;
            anchors.leftMargin: qsTranslate("","aviapp_map_play_lm");
            anchors.verticalCenter: horizontalDivider.verticalCenter;

            SimpleButton{
                id: playBtn;
                anchors.left: parent.left;
                anchors.leftMargin: qsTranslate("","aviapp_map_play_lm");
                anchors.verticalCenter: parent.verticalCenter;
                isHighlight: activeFocus
                //                isDisable: (routeListData.get(selectedIndex).conVideo == "")? true : false;
                normImg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.play_map_n
                highlightimg: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.play_map_h
                pressedImg:viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.play_map_h
                onEnteredReleased: {
                    if(routeListData.get(selectedIndex).conVideo == ""){
                        viewController.updateSystemPopupQ(7,true)
                        return;
                    }
                    viewHelper.isMicroAppVideo = true;
                    var params=["media_attr_enum","",routeListData.get(selectedIndex).conVideo];
                    dataController.getMediaByFieldName(params,loadAirlineInfoVOD);
                }
                Keys.onUpPressed: {
                    if(navigationButton.visible){
                        downNavButton.forceActiveFocus();
                    }else{
                        routesMapBackground.closeBtn.forceActiveFocus();
                    }
                }
                Keys.onLeftPressed: {
                    zoomInBtn.forceActiveFocus();
                }
                Keys.onRightPressed: {
                    childAppList.forceActiveFocus();
                }
            }
            ViewText {
                id: playBtnText;
                anchors.left: playBtn.right;
                width: qsTranslate("","aviapp_map_play_txt_w");
                anchors.leftMargin: qsTranslate("","aviapp_map_play_gap");
                anchors.verticalCenter: playBtn.verticalCenter;
                wrapMode: Text.Wrap;
                maximumLineCount:1;
                textFormat: Text.StyledText;
                elide: Text.ElideRight;
            }
        }
    }

    ListView {
        id: childAppList;
        width: qsTranslate('','aviapp_sidemenu_itemw')
        height: qsTranslate('','aviapp_sidemenu_itemh')*6;
        anchors.top:parent.top;
        anchors.topMargin: routesMapBackground.headerImage.height;
        anchors.right: parent.right;
        //        model:routeListData;
        delegate: infoListDelegate;
        highlightRangeMode:ListView.StrictlyEnforceRange;
        preferredHighlightBegin: (childAppList.contentY > 0) ? upArrowContainer.height : 0;
        preferredHighlightEnd: childAppList.height;
        //        highlight: listHighlight;
        boundsBehavior: ListView.StopAtBounds
        clip:true;
        interactive: count>6?true:false;
        onMovementEnded: {
            childAppList.currentIndex=childAppList.indexAt(childAppList.contentX,childAppList.contentY+parseInt(qsTranslate('','aviapp_sidemenu_itemh'),10)+10)
        }
        onCountChanged: {
            selectedIndex = 0;
            scaleVal = 1;
            childAppList.currentIndex = 0;
            childAppList.forceActiveFocus();
        }

        Keys.onUpPressed: {
            var firstIndex = childAppList.indexAt(childAppList.contentX+10,childAppList.contentY+10)
            if(childAppList.currentIndex == 0 ){
                routesMapBackground.closeBtn.forceActiveFocus();
            }else if(upArrowContainer.visible && (childAppList.currentIndex == firstIndex+1)){
                upArrowImage.forceActiveFocus();
            }else{
                childAppList.decrementCurrentIndex();
            }
        }

        Keys.onDownPressed: {
            var lastIndex = childAppList.indexAt(childAppList.contentX+10,(childAppList.contentY+childAppList.height-10))
            if(routesMapBackground.closeBtn.activeFocus){
                childAppList.forceActiveFocus();
            }else if((downArrowContainer.visible && currentIndex == lastIndex) ){
                downArrowImage.forceActiveFocus();
            }else{
                childAppList.incrementCurrentIndex();
            }
        }
        Keys.onLeftPressed: {
            if(!playBtn.isDisable) playBtn.forceActiveFocus();
            else zoomInBtn.forceActiveFocus();
        }

        Behavior on contentY { NumberAnimation{duration: 50}}
    }

    Component{
        id: infoListDelegate;
        Rectangle{
            id: screenlist;
            height: qsTranslate('','aviapp_sidemenu_itemh');
            width: qsTranslate('','aviapp_sidemenu_itemw');
            color: "transparent"
            clip:true;
            opacity: ((Math.floor(childAppList.contentY/screenlist.height) == index) && childAppList.contentY > 0) ? 0 : 1;
            Image {
                id: listImage;
                anchors.fill: parent;
                source: selectedIndex===index?viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_s
                                             :viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_n;
            }
            Rectangle{
                id: screenHighlightlist;
                height: qsTranslate('','aviapp_sidemenu_itemh');
                width: qsTranslate('','aviapp_sidemenu_itemw');
                color: "transparent"
                visible: (index==childAppList.currentIndex && childAppList.activeFocus)

                Image {
                    id: listHighlightImage;
                    anchors.fill: parent;
                    source: viewHelper.configToolImagePath+configTool.config.aviancaapp.screen.sidemenu_h;
                }
            }
            ViewText {
                id:listText;
                width: qsTranslate('','aviapp_sidemenu_text_w');
                wrapMode: Text.Wrap;
                //                opacity: (Math.floor((childAppList.contentY)/screenlist.height == index && (index > 0)) && childAppList.contentY > 0) ? false : true;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left; anchors.leftMargin: qsTranslate('','aviapp_sidemenu_text_lm');
                textFormat: Text.StyledText;
                elide: Text.ElideRight;
                varText: [
                    routeListData.get(index).route_list_text,selectedIndex===index?configTool.config.aviancaapp.screen.text_sidemenu_s : configTool.config.aviancaapp.screen.text_sidemenu_n,
                                                                                    qsTranslate('','aviapp_sidemenu_fontsize')
                ]
            }

            MouseArea{
                id:infoListMouseArea;
                anchors.fill: parent;
                onPressed: {
                    childAppList.currentIndex=index;
                    childAppList.forceActiveFocus();
                }
                onReleased: {
                    selectedIndex=index;
                    childAppList.currentIndex=index;
                    zoomClickCount = 0;
                    //                    setRouteImage(zoomClickCount);
                    scaleVal = 1;
                    setRouteImage(selectedIndex);
                    childAppList.forceActiveFocus();
                    setMainMenuTitleText(routesMapBackground.headerMenuTitle,routeInfoData,selectedIndex);
                    setHeaderTitleText(routesMapBackground.headerTitle,routeListData,selectedIndex);
                    setIcon(connCenterImage,routeInfoData,selectedIndex,"conCenter_icon")
                    setIcon(tacaRouteImage,routeInfoData,selectedIndex,"routesTACA_icon")
                    setIcon(aviancaRoutesImage,routeInfoData,selectedIndex,"routesAvianca_icon")
                    setText(connCenterText,routeInfoData,selectedIndex,"conCenter_text")
                    setText(tacaRouteText,routeInfoData,selectedIndex,"routesTACA_text")
                    setText(aviancaRoutesText,routeInfoData,selectedIndex,"routesAvianca_text")
                    setText(zoomText,routeInfoData,selectedIndex,"zoom_text")
                    setText(playBtnText,routeInfoData,selectedIndex,"conVideo_text")
                }
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    selectedIndex=index;
                    childAppList.currentIndex=index;
                    zoomClickCount = 0;
                    //                    setRouteImage(zoomClickCount);
                    scaleVal = 1;
                    setRouteImage(selectedIndex);
                    childAppList.forceActiveFocus();
                    setMainMenuTitleText(routesMapBackground.headerMenuTitle,routeInfoData,selectedIndex);
                    setHeaderTitleText(routesMapBackground.headerTitle,routeListData,selectedIndex);
                    setIcon(connCenterImage,routeInfoData,selectedIndex,"conCenter_icon")
                    setIcon(tacaRouteImage,routeInfoData,selectedIndex,"routesTACA_icon")
                    setIcon(aviancaRoutesImage,routeInfoData,selectedIndex,"routesAvianca_icon")
                    setText(connCenterText,routeInfoData,selectedIndex,"conCenter_text")
                    setText(tacaRouteText,routeInfoData,selectedIndex,"routesTACA_text")
                    setText(aviancaRoutesText,routeInfoData,selectedIndex,"routesAvianca_text")
                    setText(zoomText,routeInfoData,selectedIndex,"zoom_text")
                    setText(playBtnText,routeInfoData,selectedIndex,"conVideo_text")
                }
            }
        }
    }
    Component{
        id:listHighlight;
        Rectangle{
            id: screenHighlightlist;
            height: qsTranslate('','aviapp_sidemenu_itemh');
            width: qsTranslate('','aviapp_sidemenu_itemw');
            color: "transparent"
            Image {
                id: listHighlightImage;
                anchors.fill: parent;
                source: (childAppList.activeFocus)? viewHelper.configToolImagePath+"sidemenu_h.png":"";
            }
        }
    }

    Rectangle {
        id: upArrowContainer;
        anchors.top: childAppList.top;
        anchors.horizontalCenter: childAppList.horizontalCenter;
        height: qsTranslate('','aviapp_sidemenu_itemh');
        width: qsTranslate('','aviapp_sidemenu_itemw');
        color: "transparent"
        visible: (childAppList.contentY > 0) ? true : false;

        SimpleButton {
            id: upArrowImage;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','aviapp_sidemenu_arrow_tm');
            anchors.horizontalCenter: parent.horizontalCenter;
            isHighlight: activeFocus;
            normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_n;
            highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_h;
            pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_up_h;
        }
        onVisibleChanged: {
            if(!(upArrowContainer.visible)){
                childAppList.currentIndex =0;
                childAppList.forceActiveFocus();
            }
        }
        MouseArea{
            id:upArrowMouseArea;
            anchors.fill: parent;
            onClicked:  {pageUp();}
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                pageUp();
            }
        }
        Keys.onDownPressed: {childAppList.forceActiveFocus();}
        Keys.onUpPressed: {routesMapBackground.closeBtn.forceActiveFocus();}
        Keys.onLeftPressed: {playBtn.forceActiveFocus();}
    }

    Rectangle {
        id: downArrowContainer;
        anchors.top: childAppList.bottom;
        anchors.horizontalCenter: childAppList.horizontalCenter;
        height: qsTranslate('','aviapp_sidemenu_itemh');
        width: qsTranslate('','aviapp_sidemenu_itemw');
        visible:(childAppList.contentY+childAppList.height < childAppList.contentHeight);
        color: "transparent"

        onVisibleChanged: {
            if(!(downArrowContainer.visible)){
                childAppList.currentIndex = (childAppList.count -1);
                childAppList.forceActiveFocus();
            }
        }
        SimpleButton {
            id: downArrowImage;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','aviapp_sidemenu_arrow_tm');
            anchors.horizontalCenter: parent.horizontalCenter;
            isHighlight: activeFocus;
            normImg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_n;
            highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_h;
            pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancaapp.screen.arrow_dn_h;
        }
        MouseArea{
            id:downArrowMouseArea;
            anchors.fill: parent;
            onClicked:  {
                pageDown();
            }
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                pageDown();
            }
        }
        Keys.onUpPressed: {childAppList.forceActiveFocus();}
        Keys.onLeftPressed: {upNavButton.forceActiveFocus();}
    }
}
