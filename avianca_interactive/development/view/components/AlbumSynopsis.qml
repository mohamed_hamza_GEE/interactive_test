import QtQuick 1.1
import Panasonic.Pif 1.0
FocusScope{
    id:albumsynopsis
    property variant configTag:core.configTool.config[current.template_id].tracklist;

    property variant labelTag:core.configTool.language.music;
    property string appearance:"tracklist_";
    signal customClose();
    signal customLeft();
    signal animStart();
    signal extremeEnds(string direction)
    SimpleModel{id:stack }
    SimpleModel{id:singleTrack}
    /**************************************************************************************************************************/
    onExtremeEnds:{
        if(direction=="left" && listingobj.compVpathListing.count>1){
            if(getCidTidForSubCat()[1]=="artist_listing" || getCidTidForSubCat()[1]=="genre_listing")return;
            decrementSelindex();
            synopsisAnim.restart()
        }
        else if(direction=="right" && listingobj.compVpathListing.count>1){
            if(getCidTidForSubCat()[1]=="artist_listing" || getCidTidForSubCat()[1]=="genre_listing")return;
            incrementSelindex(); synopsisAnim.restart()
        }
        else if(direction=="up")
            subCatobj.compVpathSubCategory.forceActiveFocus()
    }
    Connections{
        id:catchScreenPop
        target:null
        property variant button:addtoplaylistbtn;
        onScreenPopupClosed:{
            catchScreenPop.target=null;
            if(catchScreenPop.button.select){
                catchScreenPop.button.select(retArray);
            }
        }
    }
    Connections{
        target:(visible && pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)?pif.sharedModelServerInterface:null
        onSigDataReceived:{
            core.debug("In Album synopsis Synopsis "+api+" params.playListRefresh "+params.playListRefresh)
            trackView.allRefresh=false;
            trackView.allRefresh=true;
        }

    }
    /**************************************************************************************************************************/
    function init(node){
        if(!node)node=false
        mediaSynopPoster.visible=true;
        core.debug("Albumsynopsis.qml | init ")
        viewHelper.clearPlaylistStack();
        dataController.getAudioTrackList([getMediaInfo("mid"),getCidTidForSubCat()[1],getMediaInfo("rating")],trackListReady)
        appearance="tracklist_";
        panel.anchors.horizontalCenter=parent.horizontalCenter;
        addall.cstate=0;
        if(pif.aodPlaylist.isAlbumInPlaylist(getMediaInfo("mid"))){
            addall.isDisable=true;
            addall.cstate=1;
        }else{
            addall.isDisable=false;
        }
        if(currentTemplateNode=="kids_music_listing"){
            addtoplaylistbtn.isDisable=true;
            addtoplaylistbtn.visible=false;
            addall.isDisable=true;
        }
    }
    function genreInit(w){
        panel.source=viewHelper.configToolImagePath+configTool.config.music.artist["list_panel"];
        mediaSynopPoster.visible=false;

        addall.cstate=0;
        panel.opacity=0;
        musicControl.anchors.horizontalCenter=trackView.horizontalCenter;
        appearance = 'genre_'
        close.visible=false;
        checkAddAllPlaylistBtn();
    }
    function checkAddAllPlaylistBtn(){
        for(var i=0;i<trackView.model.count;i++){
            var tmid=trackView.model.getValue(i,"mid");
            if(pif.aodPlaylist.isTrackInPlaylist(tmid)){
                addall.isDisable=true;
                addall.cstate=1;

            }else{
                addall.isDisable=false;
                break;
            }
        }
    }

    function checkPartOfStack(){
        for(var i=0;i<trackView.model.count;i++){
            var tmid=trackView.model.getValue(i,"mid");
            if(pif.aodPlaylist.isTrackInPlaylist(tmid)||viewHelper.inPlaylistStack(tmid)){
                addall.cstate=1;
            }else{
                addall.cstate=0;
                break;
            }
        }
    }

    function addClosingApi(api){
        closingApi=api;
    }
    function trackListReady(dmodel){
        if(dmodel.count==0){
            if(getCidTidForSubCat()[1]=="artist_listing"){
                customClose();
            }else{
                closeLoader();
            }
            viewController.showScreenPopup(7)
            return;
        }
        setModel(dmodel);
        if(getCidTidForSubCat()[1]=="artist_listing"){
            animStart();
        }
    }
    function clearOnExit(){
        core.debug("Albumsynopsis.qml | clearOnExit ")
        trackView.model=0;
        stack.clear();
        viewHelper.clearPlaylistStack()
    }
    function specialFocus(dir){
        if(dir=="left"){
            if(musicControl.focus){
                musicControl.specialFocus("left");
            }else{
                trackView.forceActiveFocus();
            }
        }else if(dir=="down"){
            trackView.currentIndex=trackView.indexAt(10,trackView.contentY)
            if(appearance=="tracklist_"){
                close.forceActiveFocus();
            }else{

                trackView.forceActiveFocus();
            }
        }
    }
    function setModel(dmodel){
        
        trackView.currentIndex=0;
        trackView.model=dmodel;
        if(!albumsynopsis.activeFocus){
            if(!isModelUpdated){
                trackView.forceActiveFocus();
            }
        }
        musicControl.visible=true;
    }

    function backBtnPressed(){
        if(getCidTidForSubCat()[1]=="artist_listing"){
            customClose();
        }else{
            closeLoader();
        }
    }
    function langUpdate(){
        dataController.getAudioTrackList([getMediaInfo("mid"),getCidTidForSubCat()[1],getMediaInfo("rating")],setLangModel)
    }
    function setLangModel(dModel){
        trackView.model=dModel;
    }
    /**************************************************************************************************************************/


    Image{
        id:panel;
        y:qsTranslate('',"tracklist_panel_tm");
        source:(getCidTidForSubCat()[1]=="artist_listing")?viewHelper.configToolImagePath+configTool.config.music.artist["tracklist_panel"]:viewHelper.configToolImagePath+configTag["panel"];
        x:(getCidTidForSubCat()[1]=="music_listing" || current.template_id=="kids_music_listing")?((parent.width/2)-width/2):0;
    }
    SimpleNavButton{
        id:close;
        normImg:viewHelper.configToolImagePath+configTag["close_btn_n"];
        highlightimg:viewHelper.configToolImagePath+configTag["close_btn_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
        anchors.right:panel.right;
        anchors.rightMargin: getCidTidForSubCat()[1]=="artist_listing"?qsTranslate('',"tracklist_close_rm1"):qsTranslate('',"tracklist_close_rm")
        anchors.top:panel.top;
        anchors.topMargin: qsTranslate('',"tracklist_close_tm")
        nav:["",subCatobj,"",scroller.visible?scroller:trackView]
        onEnteredReleased:{
            if(getCidTidForSubCat()[1]=="artist_listing"){
                customClose();
            }else{
                closeLoader();
            }
        }
        onNoItemFound:{
            if(dir!="left" || addall.isDisable)extremeEnds(dir);
            else addall.forceActiveFocus();
        }
    }
    ViewText{
        id:title;
        varText: [getMediaInfo("title"),configTag["title_color"],qsTranslate('',"tracklist_title_fontsize")];
        anchors.top:panel.top;
        anchors.topMargin: qsTranslate('',"tracklist_title_tm")
        anchors.left:panel.left;
        anchors.leftMargin: qsTranslate('',appearance+"title_lm");
        width:qsTranslate('',appearance+"title_w");
        height:qsTranslate('',"tracklist_title_h");

        elide:"ElideRight";
        maximumLineCount:1;
        wrapMode:Text.Wrap
        clip:true;
    }
    ViewText{
        id:synopsisSubTitleBtn
        visible: getCidTidForSubCat()[1]=="artist_listing" || getCidTidForSubCat()[1]=="music_listing"?true:false
        width:  qsTranslate('','tracklist_subtitle_w');
        height:  qsTranslate('','tracklist_subtitle_h');
        anchors.top:title.bottom
        anchors.topMargin: qsTranslate('','tracklist_subtitle_tm');
        anchors.left:panel.left;
        anchors.leftMargin: qsTranslate('',appearance+"title_lm");
        varText: [getMediaInfo("artist"),configTag["title_color"],qsTranslate('','tracklist_subtitle_fontsize')]
//        maximumLineCount: 1
        elide: Text.ElideRight
    }

    Rectangle{
        height:qsTranslate('','common_line_h')
        width:trackView.width;
        color:configTag["description1_color"];
        anchors.bottom: trackView.top;
        anchors.left:trackView.left;
    }

    ToggleButton{
        id:addall;
        normImg2:viewHelper.configToolImagePath+configTag["check_box_all_n"];
        normImg1: viewHelper.configToolImagePath+configTag["tickbox_all_n"];
        highImg2:viewHelper.configToolImagePath+configTag["check_box_all_h"];
        highImg1: viewHelper.configToolImagePath+configTag["tickbox_all_h"];
        pressImg2: viewHelper.configToolImagePath+configTag["check_box_all_p"];
        pressImg1:viewHelper.configToolImagePath+configTag["tickbox_all_p"];
        anchors.right:trackView.right;
        anchors.rightMargin:qsTranslate('',appearance+'checkbox_rm');
        anchors.bottom:trackView.top;
        //anchors.topMargin: qsTranslate('',"tracklist_addto_text_tm");
        isHighlight:activeFocus;
        visible:(currentTemplateNode!="kids_music_listing")
        onEnteredReleased:{
            select()

        }

        function select(){
            core.debug(" Select function works : isDisable : "+isDisable)
            core.debug(" Select function works : cstate : "+cstate)
            if(isDisable)return;
            if(cstate==false){

                for(var i=0;i<trackView.model.count;i++){
                    var tmid = trackView.model.getValue(i,"mid");
                    if(!viewHelper.inPlaylistStack(tmid)){
                        viewHelper.addToPlaylistStack(tmid);
                        stack.append({"index":i,"trackmid":tmid})
                    }
                }
            }else{

                stack.clear();
                viewHelper.clearPlaylistStack()

            }
            trackView.allRefresh=false;
            trackView.allRefresh=true;
        }

        Keys.onDownPressed:{
            trackView.forceActiveFocus();
        }
        Keys.onRightPressed:{
            if(appearance=="tracklist_")
                close.forceActiveFocus()
        }
        Keys.onUpPressed:{
            if(appearance=="tracklist_"){
                close.forceActiveFocus()
            }else{
                subCatobj.forceActiveFocus();
            }
        }
    }

    VerticalView{
        id:trackView;
        height:qsTranslate('',appearance+'trackview_h');
        width:qsTranslate('',appearance+'trackview_w');
        anchors.top:title.bottom;
        anchors.topMargin:qsTranslate('',appearance+'trackview_tm');
        anchors.left:title.left;
        property int currCol:1;
        property int maxCol:(currentTemplateNode=="kids_music_listing")?1:2;
        property bool allRefresh:true;

        delegate: Item{
            id:del
            height:qsTranslate('',appearance+'tracks_h');
            width:qsTranslate('',appearance+'tracks_w');
            property bool currRefresh:true;
            function getMidState(){return pif.aodPlaylist.isTrackInPlaylist(mid);}
            function getMid(){return mid;}
            function updateCheckBox(){
                currRefresh=false;
                currRefresh=true;
            }
            Image{
                id:high
                source:viewHelper.configToolImagePath+configTag["listing_highlight"];
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('',appearance+'tracks_high_bm')
                height:qsTranslate('',appearance+'tracks_high_h');
                width:(current.template_id=="kids_music_listing")?qsTranslate('',appearance+'tracks_high_kids_w'):qsTranslate('',appearance+'tracks_high_w');
                opacity:(trackView.currentIndex==index && trackView.activeFocus && (trackView.currCol==1 || ccbox.isDisable))?1:0
            }
            ViewText{
                id:tracktitle
                x:qsTranslate('',appearance+'tracktitle_lm');
                height:qsTranslate('',appearance+'tracktitle_h');
                width:qsTranslate('',appearance+'tracktitle_w');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[title,configTag["description1_color"],qsTranslate('',appearance+'tracktitle_fontsize')]
                elide:Text.ElideRight

            }
            ViewText{
                id:artText
                anchors.left:plicon.right;
                anchors.leftMargin:qsTranslate('','playlist_duration_lm');
                height:qsTranslate('','playlist_trackartist_h');
                width:qsTranslate('','playlist_trackartist_w');
                anchors.verticalCenter:parent.verticalCenter;
                visible:(appearance!="tracklist_")
                maximumLineCount:1;
                wrapMode:Text.WordWrap;
                elide:Text.ElideRight;
                varText:[(visible)?artist:"",configTag["description1_color"],qsTranslate('','playlist_duration_fontsize')]
            }
            Image{
                id:plicon
                anchors.left:tracktitle.right;
                anchors.leftMargin:qsTranslate('',appearance+'nowplaying_lm');
                anchors.verticalCenter:parent.verticalCenter;
                source:viewHelper.configToolImagePath+configTag["now_playing_icon"];
                visible:(pif.aod.getPlayingMid()==mid && pif.aod.getMediaState()!=pif.aod.cMEDIA_STOP)

            }
            ViewText{
                anchors.right:ccbox.left;
                anchors.leftMargin:qsTranslate('',appearance+'duration_lm');
                height:qsTranslate('',appearance+'tracktitle_h');
                width:qsTranslate('',appearance+'duration_w');
                anchors.verticalCenter:parent.verticalCenter;
                varText:[coreHelper.convertSecToMSLeadZero(coreHelper.convertHMSToSec(duration)),configTag["description1_color"],qsTranslate('',appearance+'duration_fontsize')]
            }
            ToggleButton{
                id:ccbox
                normImg1:viewHelper.configToolImagePath+configTag["tick_box_n"];
                normImg2: viewHelper.configToolImagePath+configTag["check_box_n"];
                highImg1:viewHelper.configToolImagePath+configTag["tick_box_h"];
                highImg2: viewHelper.configToolImagePath+configTag["check_box_h"];
                pressImg1: viewHelper.configToolImagePath+configTag["check_box_p"];
                pressImg2:viewHelper.configToolImagePath+configTag["tick_box_p"];
                anchors.right:parent.right;
                anchors.verticalCenter:parent.verticalCenter;
                anchors.rightMargin:qsTranslate('',appearance+'checkbox_rm');
                isHighlight:(trackView.currentIndex==index && trackView.activeFocus && (trackView.currCol==2 && isDisable==false))
                cstate:(currRefresh && trackView.allRefresh)?(pif.aodPlaylist.isTrackInPlaylist(mid)||viewHelper.inPlaylistStack(mid)):false;
                isDisable:(trackView.allRefresh)?pif.aodPlaylist.isTrackInPlaylist(mid):false;
                visible:(currentTemplateNode!="kids_music_listing")
                MouseArea{
                    anchors.fill:parent;
                    onClicked:{
                        trackView.currCol=2;
                        trackView.currentIndex=index;
                        trackView.select();
                    }
                }
            }
            Rectangle{
                height:qsTranslate('','common_line_h')
                width:parent.width;
                color:tracktitle.color;
                anchors.bottom: parent.bottom;
            }
            MouseArea{
                anchors.fill:high;
                onClicked:{
                    trackView.currCol=1;
                    trackView.currentIndex=index;
                    trackView.select();
                }
            }
        }
        Keys.onUpPressed:{
            if(currentItem.y==contentY){
                if(addall.isDisable){
                    if(appearance=="tracklist_"){
                        close.forceActiveFocus()
                    }else{
                        subCatobj.forceActiveFocus();
                    }
                }else{
                    addall.forceActiveFocus();
                }
            }else{
                decrementCurrentIndex();
            }
        }
        Keys.onDownPressed:{
            if((currentItem.y+currentItem.height)>=(contentY+height)|| (currentIndex==count-1)){
                if(addtoplaylistbtn.isDisable){
                    if(!viewHelper.trackStop){
                        musicControl.forceActiveFocus()    ;
                    }
                }else{
                    addtoplaylistbtn.forceActiveFocus();
                }
            }else{
                incrementCurrentIndex();
            }

        }
        Keys.onRightPressed:{
            if(currCol==maxCol || pif.aodPlaylist.isTrackInPlaylist(trackView.currentItem.getMid())){
                if(count<5){
                    close.forceActiveFocus();
                }else{
                    scroller.forceActiveFocus();
                }
            }else{
                currCol++;
            }

        }
        Keys.onLeftPressed:{
            if(currCol==1){


                if(appearance=="tracklist_"){
                    extremeEnds("left");
                }else{
                    customLeft();
                }
            }else{
                currCol--;
            }

        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                select();
            }
        }
        function select(){
            if(viewHelper.usbTrackPlayed && currCol!=2)
                viewHelper.stopUsbMp3()
            trackView.forceActiveFocus();

            var tmid=trackView.currentItem.getMid();

            if(currCol==1){
                if(appearance=="tracklist_"){
                    if(dataController.mediaServices.getMidBlockStatus(getMediaInfo("mid"),getMediaInfo("rating"))){
                        viewController.updateSystemPopupQ(7,true)
                        return;
                    }
                    if(pif.aod.getPlayingAlbumId() != getMediaInfo("mid") || pif.lastSelectedAudio!=pif.aod){
                        var params;
                        params = {
                            "amid" : getMediaInfo("mid"),
                            "duration" : trackView.model.getValue(trackView.currentIndex,"duration"),
                            "elapseTimeFormat" : 6,
                            "cid" : widgetList.getCidTidMainMenu()[0],
                            "playIndex" : trackView.currentIndex,
                            "elapseTime" : 0,
                            "mediaType": "audioAggregate",
                            "repeatType":pif.aod.getMediaRepeat(),
                            "playType":2,
                            "shuffle":pif.aod.getMediaShuffle()
                        };
                        pif.aod.playByModel(trackView.model,params)
                        viewHelper.userRepeatOn=false

                    }else{
                        pif.lastSelectedAudio.setPlayingIndex(trackView.currentIndex);

                    }
                    viewHelper.genreSongIndex=-1;
                    viewHelper.playingCategory="music";
                    if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                        pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory,genre:false})

                    }
                }else{
                    core.debug(" trackView.model.count : "+trackView.model.count)
                    viewHelper.appendToGenreSongModel(trackView.model)
                    viewHelper.playedFromGenre(trackView.currentIndex,true);
                }
                viewHelper.trackStop=false;
            }else{
                if(trackView.currentItem.getMidState()){
                    core.debug("already added, ideally this log shall not come, check code")
                }else{
                    if(viewHelper.inPlaylistStack(tmid)){
                        viewHelper.removeFromPlaylistStack(tmid)
                        stack.removeByProperty("index",trackView.currentIndex,1);
                    }else{
                        viewHelper.addToPlaylistStack(tmid);
                        stack.append({"index":trackView.currentIndex,"trackmid":tmid})

                    }
                    checkPartOfStack();
                    trackView.currentItem.updateCheckBox()
                }

            }
        }
    }
    BorderImage{
        source:viewHelper.configToolImagePath+configTag["fading_image"];
        anchors.bottom:trackView.bottom;
        anchors.horizontalCenter: trackView.horizontalCenter;
        border.left:qsTranslate('','common_scrollfade_margin')
        border.top:qsTranslate('','common_scrollfade_margin')
        border.right:qsTranslate('','common_scrollfade_margin')
        border.bottom:qsTranslate('','common_scrollfade_margin')
        width:(parseInt(qsTranslate('',appearance+'trackview_w'),10)+parseInt(qsTranslate('','common_scrollfade_extra_w'),10));
        visible:(trackView.contentY+trackView.height<trackView.contentHeight)
        opacity:((trackView.currentItem.y+trackView.currentItem.height)>=(trackView.contentY+trackView.height)||(trackView.currentIndex==trackView.count-1))?0:1;
    }
    Scroller{
        id:scroller
        targetData:trackView;
        sliderbg:[qsTranslate('','tracklist_scrollcont_base_margin'),qsTranslate('','tracklist_scrollcont_w'),qsTranslate('','tracklist_scrollcont_base_h'),configTag["scroll_base"]]
        scrollerDot:configTag["scroll_button"]
        upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
        downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
        anchors.left:trackView.right;
        anchors.leftMargin:qsTranslate('','tracklist_scrollcont_lm')
        height: qsTranslate('','tracklist_scrollcont_h');
        width: qsTranslate('','tracklist_scrollcont_w');
        anchors.verticalCenter: trackView.verticalCenter;
        onSliderNavigation:{
            if(direction=="up"){
                if(appearance=="tracklist_"){
                    close.forceActiveFocus();
                }else{
                    subCatobj.forceActiveFocus();
                }
            }else if(direction=="down"){
                if(addtoplaylistbtn.isDisable){
                    if(!viewHelper.trackStop){
                        musicControl.forceActiveFocus();
                    }
                }else{
                    addtoplaylistbtn.forceActiveFocus();
                }
            }else if(direction=="left"){
                trackView.forceActiveFocus();
            }else if(direction=="right"){
                extremeEnds(direction)
            }
        }

    }
    SimpleNavBrdrBtn{
        id:addtoplaylistbtn
        normalImg: viewHelper.configToolImagePath+configTag["btn_n"];
        highlightImg:viewHelper.configToolImagePath+configTag["btn_h"];
        pressedImg:viewHelper.configToolImagePath+configTag["btn_p"];
        height:qsTranslate('',"tracklist_addtobutton_h");
        width:qsTranslate('',"tracklist_addtobutton_w");
        btnTextWidth:parseInt(qsTranslate('',"tracklist_addtobutton_text_w"),10);
        buttonText: [labelTag["button_add_to_playlist"],(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','tracklist_addall_btn_text_fontsize')]
        anchors.right:panel.right;
        anchors.rightMargin: qsTranslate('',"tracklist_addtobutton_rm")
        anchors.bottom:panel.bottom;
        anchors.bottomMargin:qsTranslate('',"tracklist_addtobutton_bm")
        normalBorder:[qsTranslate('',"tracklist_addtobutton_margin")]
        highlightBorder:[qsTranslate('',"tracklist_addtobutton_margin")]
        opacity:(isDisable)?0:1;
        z:opacity;


        elide:Text.ElideRight
        nav: ["",trackView,"",(viewHelper.trackStop)?"":musicControl]
        isDisable:(stack.count==0);
        onEnteredReleased:{
            if(stack.count==0)return;
            trackView.forceActiveFocus();
            if(stack.count==trackView.count)  viewController.showScreenPopup(5);
            else  viewController.showScreenPopup(3);
            catchScreenPop.target=viewController;
            catchScreenPop.button=addtoplaylistbtn
        }
        Component.onCompleted:{
            btnText.anchors.verticalCenterOffset=parseInt(qsTranslate('',"tracklist_addtobutton_text_tm"),10);

        }



        function select(retArray){
            catchScreenPop.target=null;
            if(retArray[0]==0)return;
            var rat=getMediaInfo("rating")
            var amid=getMediaInfo("mid");
            var cid =widgetList.getCidTidMainMenu()[0];
            for(var i=0;i<stack.count;i++){
                var tmid = stack.getValue(i,"trackmid");
                if(viewHelper.inPlaylistStack(tmid)){
                    var a = pif.aodPlaylist.addTrack(trackView.model,stack.getValue(i,"index"),cid,amid,retArray[0],rat)
                    if(a==1)break;
                }
            }

            stack.clear();
            viewHelper.clearPlaylistStack()
            trackView.allRefresh=false;
            trackView.allRefresh=true;
            if(a==1){
                viewHelper.fullPlayId = (retArray[0]*-1);
                viewController.showScreenPopup(4);
            }
            core.debug(" appearance : "+appearance)
            if(appearance=="genre_"){
                checkAddAllPlaylistBtn();
            }else{
                if(pif.aodPlaylist.isAlbumInPlaylist(getMediaInfo("mid"))){
                    addall.isDisable=true;
                }else{
                    addall.isDisable=false;
                }
            }
        }


        onNoItemFound:{
            if(dir=="right"||dir=="left"){

                extremeEnds(dir)

            }
        }
    }
    Image{
        id:mediaSynopPoster;
        anchors.left:panel.left;
        anchors.leftMargin:qsTranslate('',"tracklist_poster_lm");
        anchors.top:panel.top;
        anchors.topMargin:qsTranslate('',"tracklist_poster_tm");
        height:qsTranslate('',"tracklist_poster_h");
        width:qsTranslate('',"tracklist_poster_w");
        source: viewHelper.cMediaImagePath+getMediaInfo("poster");



    }

    MusicControls{
        id:musicControl
        height:qsTranslate('','tracklist_control_cont_h');
        width:qsTranslate('','tracklist_control_cont_w');
        anchors.bottom:panel.bottom;
        anchors.horizontalCenter:panel.horizontalCenter;
        anchors.horizontalCenterOffset:(getCidTidForSubCat()[1]=="genre_listing")?qsTranslate('','tracklist_control_cont_offset'):0;
        upFocus:[(addtoplaylistbtn.isDisable)?trackView:addtoplaylistbtn,(addtoplaylistbtn.isDisable)?trackView:addtoplaylistbtn,trackView,trackView]
        visible:false
        configVal:(currentTemplateNode=="kids_music_listing")?core.configTool.config.kids_music_listing.tracklist:core.configTool.config.music.tracklist;

    }
    Component.onCompleted:  {
        if(visible)musicControl.progressbar.externalCalc(musicControl.section.getMediaElapseTime(),musicControl.section.getMediaEndTime())
    }
}

