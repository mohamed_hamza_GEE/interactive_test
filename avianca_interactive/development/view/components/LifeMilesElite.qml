import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    width: core.height;
    height: core.width;

    property string dataXML : childAppName;
    property string childAppImages:''

    function init(){
        core.log("LifeMilesElite | init ");
    }
    function setPosterImage(obj, Dmodel){
        obj.source = childAppImages + Dmodel.get(0).posterImage;
    }
    function setTitleText(obj,Dmodel){
        obj.text = (Dmodel.get(0).titleText_cdata !== "") ? Dmodel.get(0).titleText_cdata : Dmodel.get(0).titleText;
        obj.color = configTool.config.lifemiles.screen.text_title;
        obj.font.pixelSize = qsTranslate('','lifemiles_title_fontsize');
    }
    function setTitleInfoText(obj,Dmodel){
        obj.text = (Dmodel.get(0).titleInfoText_cdata !== "") ? Dmodel.get(0).titleInfoText_cdata : Dmodel.get(0).titleInfoText;
        obj.color = configTool.config.lifemiles.screen.text_title_info;
        obj.font.pixelSize = qsTranslate('','lifemiles_title_fontsize1');
    }
    function setFocus(){
        if(scroller.visible){
            scroller.forceActiveFocus()
        }else{
            appCloseBtn.forceActiveFocus();
        }
    }
    function reloadData(){
        commonDataModel.reload();
        eliteListData.reload();
    }


    XmlListModel {
        id: commonDataModel;
        source: dataXML;
        query:"/application/screen[@_template='template4']";

        XmlRole { name: "posterImage"; query:"image1/@_value/string()" }
        XmlRole { name: "titleText"; query:"text1/"+langCode+"/@_value/string()" }
        XmlRole { name: "titleInfoText"; query:"text2/"+langCode+"/@_value/string()" }
        XmlRole { name: "titleText_cdata"; query:"text1/"+langCode+"/string()" }
        XmlRole { name: "titleInfoText_cdata"; query:"text2/"+langCode+"/string()" }

        onCountChanged: {
            setPosterImage(screenPosterImage, commonDataModel);
            setTitleText(titleText, commonDataModel);
            setTitleInfoText(titleInfoText, commonDataModel);
        }
    }

    XmlListModel{
        id: eliteListData;
        source: dataXML;
        query:"/application/screen[@_template='template4']/list1/listItem";

        XmlRole { name: "eliteTitleText"; query: "text3/"+langCode+"/@_value/string()" }
        XmlRole { name: "eliteTitleText_cdata"; query: "text3/@_value/string()" }
        XmlRole { name: "eliteTitleInfoText"; query: "text4/"+langCode+"/@_value/string()" }
        XmlRole { name: "eliteTitleInfoText_cdata"; query: "text4/@_value/string()" }
        XmlRole { name: "eliteInfoText"; query: "text5/"+langCode+"/@_value/string()" }
        XmlRole { name: "eliteInfoText_cdata"; query: "text5/@_value/string()" }
        XmlRole { name: "iconImage"; query: "image2/@_value/string()" }
    }

    Image {
        id: screenBgImage;
        source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.discover_bg;
    }

    SimpleButton {
        id: appCloseBtn;
        property alias appCloseBtn: appCloseBtn;
        z:1;
        increaseTouchArea: true
        increaseValue:-60
        anchors.right: screenBgImage.right;
        anchors.top: screenBgImage.top;
        anchors.rightMargin: qsTranslate('','lifemiles_close_y');
        anchors.topMargin: qsTranslate('','lifemiles_close_x');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;

        onEnteredReleased: {
            viewController.showScreenPopup(23);
        }
        Keys.onLeftPressed: {
            if(scroller.visible){
                scroller.forceActiveFocus();
            }else{
                microappMain.menuContainer.forceActiveFocus();
            }
        }
    }

    Image {
        id: screenPosterImage;
        anchors.right:screenBgImage.right;
        anchors.verticalCenter: screenBgImage.verticalCenter;
    }

    Timer{id:delay; interval: 50; onTriggered: {
            blankRect.opacity=0
        }
    }
    Rectangle{
        id: blankRect;
        height: screenBgImage.paintedHeight;
        width: screenBgImage.paintedWidth;
        color:"WHITE"
        z:1;
        opacity: 1;
        anchors.fill: screenBgImage;
    }

    ViewText{
        id: titleText;
        anchors.top: screenBgImage.top;
        anchors.left: screenBgImage.left;
        anchors.topMargin: qsTranslate('','lifemiles_title_tm');
        anchors.leftMargin: qsTranslate('','lifemiles_title_lm');
        width: qsTranslate('','lifemiles_title_w');
        wrapMode: Text.Wrap;
        elide: Text.ElideRight;
        textFormat: Text.StyledText;
        maximumLineCount:1;
    }

    ViewText{
        id: titleInfoText;
        anchors.top: titleText.bottom;
        anchors.left: titleText.left;
        anchors.topMargin: qsTranslate('','lifemiles_title_info_vgap');
        width: qsTranslate('','lifemiles_title_info_w');
        //        lineHeightMode: Text.FixedHeight
        //        lineHeight: qsTranslate('','lifemiles_title_lh');
        //        wrapMode: Text.Wrap;
        wrapMode: Text.WordWrap;
        elide: Text.ElideRight;
        textFormat: Text.StyledText;
        maximumLineCount:3;
    }

    ListView{
        id: eliteListView;
      //  property int spacingV : qsTranslate('','lifemiles_info_linegap');

        height: qsTranslate('','lifemiles_info_h');
        width: qsTranslate('','lifemiles_info_w');
        anchors.top:titleInfoText.bottom;
        anchors.left: titleInfoText.left;
        anchors.topMargin: qsTranslate('','lifemiles_info_tm');

        boundsBehavior: ListView.StopAtBounds
        snapMode:ListView.SnapToItem;

        model: eliteListData;
        cacheBuffer: contentHeight;
        delegate: elietDelegate;
        clip:true;
        spacing: qsTranslate('','lifemiles_info_linegap');
        Behavior on contentY {
            NumberAnimation{duration: 300}
        }

    }

    Component{
        id: elietDelegate;
        Item {
            id: eliteListItem;
            height: parseInt(dividerLine.height)+ parseInt(eliteProgramText.paintedHeight) + parseInt(eliteProgramTitleInfo.paintedHeight) + parseInt(eliteProgramInfo.paintedHeight)+ parseInt(qsTranslate('','lifemiles_info_linegap'))*eliteListView.count;
            width: parent.width;
            Rectangle{
                id: dividerLine;
                height: qsTranslate('','lifemiles_line_h');
                anchors.top: parent.top;
                width: parent.width;
                color: qsTranslate('','lifemiles_line_color');
            }
            Image {
                id: icon;
                anchors.left: parent.left;
                anchors.top: dividerLine.bottom;
                anchors.topMargin: qsTranslate('','lifemiles_info_img_tm');
                anchors.leftMargin: qsTranslate('','lifemiles_info_img_lm');
                source: childAppImages + eliteListData.get(index).iconImage;
            }
            ViewText{
                id: eliteProgramText;
                anchors.top: dividerLine.bottom;
                anchors.left: parent.left;
                anchors.topMargin: qsTranslate('','lifemiles_info_elitetxt_tm');
                anchors.leftMargin: qsTranslate('','lifemiles_info_elitetxt_lm');
                width: qsTranslate('','lifemiles_info_elitetxt_w');
                wrapMode: Text.Wrap;
                textFormat: Text.StyledText;
                elide: Text.ElideRight;
                maximumLineCount:1;
                varText: [ (eliteListData.get(index).eliteTitleText_cdata != "") ? eliteListData.get(index).eliteTitleText_cdata:eliteListData.get(index).eliteTitleText,
                                                                                   configTool.config.lifemiles.screen.text_elite_list,
                                                                                   qsTranslate('','lifemiles_info_fontsize1')
                ]
            }

            ViewText{
                id: eliteProgramTitleInfo;
                anchors.top: eliteProgramText.bottom;
                anchors.left: parent.left;
                anchors.topMargin: qsTranslate('','lifemiles_info_elitetxt_vgap');
                anchors.leftMargin: qsTranslate('','lifemiles_info_elitetxt_lm');
                width: qsTranslate('','lifemiles_info_elitetxt_w');
                wrapMode: Text.Wrap;
                //                textFormat: Text.StyledText;
                textFormat: Text.RichText;
                elide: Text.ElideRight;
                maximumLineCount: 1;
                font.bold: true;
                varText: [ (eliteListData.get(index).eliteTitleInfoText_cdata != "")?eliteListData.get(index).eliteTitleInfoText_cdata : eliteListData.get(index).eliteTitleInfoText,
                                                                                      configTool.config.lifemiles.screen.text_elite_list,
                                                                                      qsTranslate('','lifemiles_info_fontsize2')
                ]

            }
            ViewText{
                id: eliteProgramInfo;
                anchors.top: eliteProgramTitleInfo.bottom;
                anchors.left: parent.left;
                anchors.topMargin: qsTranslate('','lifemiles_info_elitetxt_vgap');
                anchors.leftMargin: qsTranslate('','lifemiles_info_elitetxt_lm');
                width: qsTranslate('','lifemiles_info_elitetxt_w');
                height: eliteProgramInfo.paintedHeight
                //                wrapMode: Text.Wrap;
                wrapMode: Text.WordWrap;
                //                textFormat: Text.StyledText;
                textFormat: Text.RichText;
                //                lineHeightMode: Text.FixedHeight
                //                lineHeight: 0.9;//qsTranslate('','lifemiles_info_lh');
                //                maximumLineCount: 1;
                varText: [ (eliteListData.get(index).eliteInfoText_cdata != "")? eliteListData.get(index).eliteInfoText_cdata : eliteListData.get(index).eliteInfoText,
                                                                                 configTool.config.lifemiles.screen.text_elite_list,
                                                                                 qsTranslate('','lifemiles_info_fontsize2')
                ]
            }
            Rectangle{
                id: space;
                anchors.top: eliteProgramInfo.bottom;
                height: qsTranslate('','lifemiles_info_linegap');
                width: dividerLine.width;
                color: "transparent";
            }
            Component.onCompleted: {
                delay.restart();
            }
        }
    }
    Scroller{
        id:scroller
        height: qsTranslate('','lifemiles_scroll_short_h');
        width: qsTranslate('','lifemiles_scroll_short_w');
        anchors.left:eliteListView.right
        anchors.leftMargin: qsTranslate('','lifemiles_scroll_short_lm');
        anchors.top :eliteListView.top
        anchors.topMargin: qsTranslate('','lifemiles_scroll_short_tm')
        sliderbg:[qsTranslate('','lifemiles_scroll_short_base_margin'),qsTranslate('','lifemiles_scroll_short_w'),qsTranslate('','lifemiles_scroll_short_base_h'),configTool.config.lifemiles.screen.scrollbg]
        scrollerDot: configTool.config.lifemiles.screen.slider
        upArrowDetails:[configTool.config.lifemiles.screen.app_arwup_n, configTool.config.lifemiles.screen.app_arwup_h, configTool.config.lifemiles.screen.app_arwup_h ]
        downArrowDetails:[configTool.config.lifemiles.screen.app_arwdwn_n, configTool.config.lifemiles.screen.app_arwdwn_h, configTool.config.lifemiles.screen.app_arwdwn_h]

        targetData:eliteListView;
        elementSize:parseInt(eliteListView.height*0.75)
        Keys.onRightPressed: {
            appCloseBtn.forceActiveFocus()
        }
        Keys.onLeftPressed: {
            microappMain.menuContainer.forceActiveFocus();
        }
    }


}

