import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

FocusScope {
    width: core.height;
    height: core.width;

    property string dataXML : childAppName;
    property string childAppImages:''
    function init(){
        core.log("LifeMilesDiscover | init");
    }
    function setFocus(){
        if(scroller.visible){
            scroller.forceActiveFocus()
        }else{
            appCloseBtn.forceActiveFocus();
        }
    }
    function setPosterImage(obj, Dmodel){
        obj.source = microappMain.childAppImages + Dmodel.get(0).posterImage;
    }
    function setbottomText(obj,Dmodel){
        obj.text = (Dmodel.get(0).bottomText_cdata !== "") ? Dmodel.get(0).bottomText_cdata : Dmodel.get(0).bottomText;
        obj.color = configTool.config.lifemiles.screen.text_txt_basline;
        obj.font.pixelSize = qsTranslate('','lifemiles_txt_fontsize');
    }
    function setCloseBtn (obj){
        obj.source = viewHelper.configToolImagePath+configTool.config.lifemiles.screen.close_n;
    }
    function reloadData(){
        commonDataModel.reload();
        discoverHeadingDataModel.reload();
    }

    XmlListModel {
        id: commonDataModel;

        source: dataXML;
        query:"/application/screen[@_template='template1']";

        XmlRole { name: "posterImage"; query:"image1/@_value/string()" }
        XmlRole { name: "bottomText"; query:"text11/"+langCode+"/@_value/string()" }
        XmlRole { name: "bottomText_cdata"; query:"text11/"+langCode+"/string()" }

        onCountChanged: {
            setPosterImage(screenPosterImage, commonDataModel);
            setbottomText(bottomText, commonDataModel);
        }
    }

    XmlListModel {
        id: discoverHeadingDataModel;

        source: dataXML;
        query:"/application/screen/list1/listItem";

        XmlRole { name: "headingText"; query:"text2/"+langCode+"/@_value/string()" }
        XmlRole { name: "headingText_cdata"; query:"text2/"+langCode+"/string()" }
        XmlRole { name: "headingInfoText"; query:"text3/"+langCode+"/@_value/string()" }
        XmlRole { name: "headingInfoText_cdata"; query:"text3/"+langCode+"/string()" }
        XmlRole { name: "link"; query:"link1/@_link/string()" }
    }

    Image {
        id: screenBgImage;
        source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.discover_bg;
    }

    SimpleButton {
        id: appCloseBtn;
        property alias appCloseBtn: appCloseBtn;
        z:1;
        increaseTouchArea: true
        increaseValue:-60
        anchors.right: screenBgImage.right;
        anchors.top: screenBgImage.top;
        anchors.rightMargin: qsTranslate('','lifemiles_close_y');
        anchors.topMargin: qsTranslate('','lifemiles_close_x');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;

        onEnteredReleased: {
            viewController.showScreenPopup(23);
        }
        Keys.onLeftPressed: {
            if(scroller.visible){
                scroller.forceActiveFocus();
            }else{
                microappMain.menuContainer.forceActiveFocus();
            }
        }
    }

    Image {
        id: screenPosterImage;
        anchors.right:screenBgImage.right;
        anchors.verticalCenter: screenBgImage.verticalCenter;
    }

    Timer{id:delay; interval: 50; onTriggered: {
            blankRect.opacity=0
        }
    }
    Rectangle{
        id: blankRect;
        height: screenBgImage.paintedHeight;
        width: screenBgImage.paintedWidth;
        color:"WHITE"
        z:1;
        opacity: 1;
        anchors.fill: screenBgImage;
    }

    ViewText{
        id:bottomText;
        anchors.bottom: screenBgImage.bottom;
        anchors.left: screenBgImage.left;
        anchors.leftMargin: qsTranslate('','lifemiles_discover_cont_lm');
        anchors.bottomMargin: qsTranslate('','lifemiles_txt_tm');
        width: qsTranslate('','lifemiles_txt_w');
        wrapMode: Text.Wrap;
        elide: Text.ElideRight;
        maximumLineCount:1;
    }

    ListView {
        id: discoverLV;
        height: qsTranslate('','lifemiles_discover_cont_h');
        width: qsTranslate('','lifemiles_discover_cont_w');
        anchors.top:screenBgImage.top;
        anchors.topMargin: qsTranslate('','lifemiles_discover_cont_tm');
        anchors.left: screenBgImage.left;
        anchors.leftMargin: qsTranslate('','lifemiles_discover_cont_lm');
        clip:true
        boundsBehavior: Flickable.StopAtBounds;
        cacheBuffer: contentHeight;
        model : discoverHeadingDataModel;
        delegate: discoverDelegate;
        spacing: qsTranslate('','lifemiles_discover_item_vgap');
    }

    Component{
        id: discoverDelegate;
        Item {
            id: titleItem;
            width: parent.width;
            height: childDataColoumn.y +childDataColoumn.implicitHeight /*+parseInt(qsTranslate('','lifemiles_discover_item_vgap'))*/;

            ViewText{
                id: headerTitle;
                width: qsTranslate('','lifemiles_discover_info_w');
                wrapMode: Text.Wrap;
                elide: Text.ElideRight;
                textFormat: Text.RichText;
                maximumLineCount:1;
                varText: [
                    (headingText_cdata !== "") ? headingText_cdata : headingText,
                                                 configTool.config.lifemiles.screen.text_title,
                                                 qsTranslate('','lifemiles_discover_fontsize')
                ]
            }

            ViewText{
                id: headerTitleInfo;
                anchors.left: parent.left;
                anchors.top: headerTitle.bottom;
                anchors.topMargin: qsTranslate('','lifemiles_discover_subtittle_vgap');
                width: qsTranslate('','lifemiles_discover_info_w');
                wrapMode: Text.WordWrap;
                //        wrapMode: Text.Wrap;
                maximumLineCount:3;
                elide: Text.ElideRight;
                //                lineHeight:qsTranslate('','lifemiles_discover_lh');
                //                lineHeightMode: Text.FixedHeight
                varText: [ (headingInfoText_cdata !== "") ? headingInfoText_cdata : headingInfoText,
                                                            configTool.config.lifemiles.screen.text_title_info,
                                                            qsTranslate('','lifemiles_discover_fontsize1')
                ]
            }

            XmlListModel{
                id: childXMLDataModel;
                source: dataXML;
                query:"/application/screen[@_id='"+link+"']/list2/listItem";

                XmlRole { name: "infoText"; query:"text4/"+langCode+"/@_value/string()" }
                XmlRole { name: "infoText_cdata"; query:"text4/"+langCode+"/string()" }
                XmlRole { name: "iconImg"; query:"image2/@_value/string()" }
            }

            Column{
                id: childDataColoumn;
                anchors.top: (headerTitleInfo.text != "")? headerTitleInfo.bottom : headerTitle.bottom;
                anchors.topMargin: qsTranslate('','lifemiles_discover_info_vgap');
                spacing: qsTranslate('','lifemiles_discover_info_vgap');

                Repeater{
                    id: childDataRepeater;
                    model:childXMLDataModel.count;

                    Item {
                        id: childdataContainer;
                        height: (icon.paintedHeight > childInfoText.paintedHeight) ? icon.paintedHeight : childInfoText.paintedHeight;
                        width: icon.paintedWidth + childInfoText.paintedWidth + parseInt(qsTranslate('','lifemiles_discover_txt_lm'));

                        Image {
                            id: icon;
                            anchors.left: parent.left;
                            source:childAppImages+ childXMLDataModel.get(index).iconImg;
                        }
                        ViewText{
                            id:childInfoText;
                            anchors.top: icon.top;
                            anchors.topMargin: qsTranslate('','lifemiles_discover_txt_tm');
                            anchors.left: icon.right;
                            anchors.leftMargin: qsTranslate('','lifemiles_discover_txt_lm');
                            width: qsTranslate('','lifemiles_discover_info_w');
                            //                            wrapMode: Text.Wrap;
                            wrapMode: Text.WordWrap;
                            //                            lineHeight:qsTranslate('','lifemiles_discover_lh');
                            //                            lineHeightMode: Text.FixedHeight
                            varText: [ (childXMLDataModel.get(index).infoText_cdata !== "") ? childXMLDataModel.get(index).infoText_cdata : childXMLDataModel.get(index).infoText_cdata,
                                                                                              configTool.config.lifemiles.screen.text_title_info,
                                                                                              qsTranslate('','lifemiles_discover_fontsize2')
                            ]
                        }
                        Component.onCompleted: {
                            delay.restart();
                        }
                    }
                }
            }
        }
    }

    Scroller{
        id:scroller
        height: qsTranslate('','lifemiles_scroll_h');
        width: qsTranslate('','lifemiles_scroll_w');
        anchors.left:discoverLV.right
        anchors.leftMargin: qsTranslate('','lifemiles_scroll_lm');
        anchors.top: discoverLV.top;
        anchors.topMargin: qsTranslate('','lifemiles_scroll_tm')
        sliderbg:[qsTranslate('','lifemiles_scroll_base_margin'),qsTranslate('','lifemiles_scroll_w'),qsTranslate('','lifemiles_scroll_base_h'),configTool.config.lifemiles.screen.scrollbg]
        scrollerDot: configTool.config.lifemiles.screen.slider
        upArrowDetails:[configTool.config.lifemiles.screen.app_arwup_n, configTool.config.lifemiles.screen.app_arwup_h, configTool.config.lifemiles.screen.app_arwup_h ]
        downArrowDetails:[configTool.config.lifemiles.screen.app_arwdwn_n, configTool.config.lifemiles.screen.app_arwdwn_h, configTool.config.lifemiles.screen.app_arwdwn_h]
        targetData:discoverLV;
        elementSize:parseInt(discoverLV.height*0.75)
        Keys.onRightPressed: {
            appCloseBtn.forceActiveFocus()
        }
        Keys.onLeftPressed: {
            microappMain.menuContainer.forceActiveFocus();
        }
    }

//    Image{
//        anchors.top: discoverLV.top
//        anchors.left:discoverLV.left
//        visible: discoverLV.contentY>0
//        source: viewController.settings.assetImagePath+"lifemiles_fade.png"

//    }
//    Image{
//        anchors.bottom:  discoverLV.bottom
//        anchors.left:discoverLV.left
//        visible: (discoverLV.contentY+discoverLV.height)<(discoverLV.contentHeight)
//        rotation:180;
//        source: viewController.settings.assetImagePath+"lifemiles_fade.png"
//    }

}
