// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

FocusScope {
    id:chatsessionscreen
    property variant currentsession;
    property int listHeight:qsTranslate('','seatchat_chatfrnd_list_h');
    property int cellHeight:qsTranslate('','seatchat_frndlist_cell_h');
    property int cellWidth: qsTranslate('','seatchat_frndlist_cell_w');
    property alias sessionListView: sessionListView
    property int lastContentY: 0
    property int sel: -1


    function getSessionInfo(){
        core.log("ChatSession.qml | getSessionInfo() = "+currentsession+" pif.seatChat.getCurrentSessionId() = "+pif.seatChat.getCurrentSessionId())
        pif.seatChat.setCurrentSessionId(currentsession)
        pif.seatChat.getBuddyListBySessionId(currentsession);
        pif.seatChat.getPrivateChatHistory(currentsession);
    }

    function ifAnyBuddyOrExit(sid){
        core.log("ChatSession.qml | ifAnyBuddyOrExit() sid = "+sid)
        for(var i=0;i<pif.seatChat.sessionListModel.count;i++){
            if(sid==pif.seatChat.sessionListModel.get(i).session){
                break;
            }
        }
        var pcount = getParticipantCount() //    pif.seatChat.sessionListModel.get(i).participantCount;
        if(pcount==0){
            pif.seatChat.exitChatSession(sid);
        }
    }

    function updateActualSessionCount(){
        core.log("ChatSession.qml | updateActualSessionCount() before  |  viewHelper.actualSessionCount= "+ viewHelper.actualSessionCount)
        viewHelper.actualSessionCount=0;
        for(var i=0;i<pif.seatChat.sessionListModel.count;i++){
            var pcount = pif.seatChat.sessionListModel.get(i).participantCount;
            core.log(" pcount : "+pcount)
            if(pcount>=1){
                viewHelper.actualSessionCount++;
            }
        }
        core.log("ChatSession.qml | updateActualSessionCount() after  |  viewHelper.actualSessionCount= "+ viewHelper.actualSessionCount)
    }

    function sessionSelected(){
        core.log("ChatSession.qml | sessionSelected() ")
        return sessionListView.sessionSelected()
    }

    function getParticipantCount(){
        core.log("ChatSession.qml | getParticipantCount() ")
        return sessionListView.getParticipantCount();
    }
    function getParticipantseatno(){
        core.log("ChatSession.qml | getParticipantseatno() ")
        if(sessionListView.getParticipantCount()==1){
            return (pif.seatChat.getSeatName(sessionListView.getParticipantseatno())!="")?(sessionListView.getParticipantseatno() + " | "+pif.seatChat.getSeatName(sessionListView.getParticipantseatno())):sessionListView.getParticipantseatno()
        }else{
            return sessionListView.getParticipantseatno();
        }
    }

    function getParticipantseatDetails(){
        core.log("ChatSession.qml | getParticipantseatDetails() ")
        return sessionListView.getParticipantseatDetails()
    }


    function setCurrIndex(sid){
        core.log("ChatSession.qml | setCurrIndex() ")
        for(var i=0;i<pif.seatChat.sessionListModel.count;i++){
            if(sid==pif.seatChat.sessionListModel.get(i).session){
                sessionListView.currentIndex=i;
                break;
            }
        }
    }

    function setDefault(){
        core.log("ChatSession.qml | setDefault() ")
        for(var i=0;i<pif.seatChat.sessionListModel.count;i++){
            var pcount = pif.seatChat.sessionListModel.get(i).participantCount;
            core.log(" pcount : "+pcount)
            if(pcount>=1){
                pif.seatChat.setCurrentSessionId(pif.seatChat.sessionListModel.get(i).session)
                sessionListView.currentIndex=i;
                break;
            }
        }
    }





    Rectangle{
        id:chat_session
        color:"transparent"
        width :qsTranslate('','seatchat_chatfrnd_list_w')
        height:qsTranslate('','seatchat_chatfrnd_list_cont_h');

        ListView{
            id:sessionListView
            focus: true
            width :qsTranslate('','seatchat_chatfrnd_list_w')
            height:parseInt(qsTranslate('','seatchat_chatfrnd_list_cont_h'),10)//(sessionListView.contentY==0)?parseInt(qsTranslate('','seatchat_chatfrnd_list_h'),10):parseInt(qsTranslate('','seatchat_chatfrnd_list_cont_h'),10)
            cacheBuffer:contentHeight//height*2
            boundsBehavior: Flickable.StopAtBounds
            snapMode: ListView.SnapToItem
            delegate: sessionDelegate
            contentHeight:qsTranslate('','seatchat_chatfrnd_list_cont_h')
            interactive:true
            model:pif.seatChat.sessionListModel//viewHelper.chatSessionModel
            visible:(sessionListView.count>0)?true:false
             clip:true

            onActiveFocusChanged:{
                core.log("SCDebug |  sesionListView.activeFocus : "+activeFocus);
            }
            onModelChanged:{
                //                currentIndex = 0;
                core.log(">>>>>>>>>>>>> onModelChanged >>>>>>>>>>>>"+model.count);
            }
            onCountChanged:{
                core.log(">>>>>>>>>>>>> onCountChanged >>>>>>>>>>>>"+model.count);
                updateActualSessionCount();
                if(viewHelper.actualSessionCount==0)chat_textBox.text=""
            }
            Component.onCompleted:{
                updateActualSessionCount();
            }

            onCurrentIndexChanged: {
                console.log("ChatScreen.qml |CompChatSession.qml | onCurrentIndexChanged | currentIndex = "+currentIndex)
            }
            function sessionSelected(){
                core.log("ChatScreen.qml | CompChatSession.qml | sessionSelected ")
                if(viewHelper.actualSessionCount==0)return;
                core.log("ChatScreen.qml | CompChatSession.qml | sessionSelected  | currentsession before = "+currentsession)
                core.log("ChatScreen.qml | CompChatSession.qml | currentIndex | befoire = "+currentIndex)
                core.log("ChatScreen.qml | CompChatSession.qml | sel | befoire = "+sel)
                sel=currentIndex
                currentsession=model.get(sel).session;
                core.log("ChatScreen.qml | CompChatSession.qml | sessionSelected  | currentsession = "+currentsession)
                core.log("ChatScreen.qml | CompChatSession.qml | sel = "+sel)
                core.log("before currentsession : "+currentsession);
                if(currentsession!=undefined &&currentsession!=0){
                    getSessionInfo()
                    if(viewHelper.fromInvitePopup){sessionListView.positionViewAtEnd();viewHelper.fromInvitePopup=false}
                    core.log("ChatScreen.qml |CompChatSession.qml | sessionSelected | getParticipantCount : "+model.get(currentIndex).participantCount)
                    core.log("ChatScreen.qml |CompChatSession.qml | sessionSelected | getParticipantCount : "+model.get(currentIndex).participantSeatNo)

                }
                core.log("SCDebug | sessionListView.activeFocus : "+sessionListView.activeFocus);
                core.log("SCDebug | sessionListView.currentIndex : "+sessionListView.currentIndex);

            }

            function getParticipantCount(){
                core.log("ChatScreen.qml |CompChatSession.qml|  getParticipantCount() : "+model.get(currentIndex).participantCount)
                return model.get(sel).participantCount
            }

            function getParticipantseatno(){
                core.log(" ChatScreen.qml | CompChatSession.qml | getParticipantseatno() : "+model.get(sel).participantSeatNo)
                return model.get(sel).participantSeatNo
            }
            function getParticipantseatDetails(){
                core.log("ChatScreen.qml | CompChatSession.qml |  getParticipantDetails() : "+model.get(currentIndex).session)
                return viewHelper.participantNames[model.get(currentIndex).session]
            }


            function findPrevIndex(ind){
                core.log("ChatScreen.qml | CompChatSession.qml |  findPrevIndex() ind ="+ind)
                for(var i=(ind-1);i>=0;i--){
                    var pcount = model.get(i).participantCount
                    //    var state = model.get(i).active
                    if(pcount>=1/* || state*/){return i}
                }
                return -1;
            }

            function findNextIndex(ind){
                core.log("ChatScreen.qml | CompChatSession.qml |  findNextIndex() ind = "+ind)
                for(var i=(ind+1);i<model.count;i++){
                    var pcount = model.get(i).participantCount
//                    var state = model.get(i).active
                    if(pcount>=1/* || state*/){return i}
                }
                return -1;
            }
            Keys.onUpPressed: {
                if(sessionListView.currentIndex==0){
                    textBox.forceActiveFocus();
                }
                else{
                    var a = findPrevIndex(currentIndex);
                    if(a==-1){
                        textBox.forceActiveFocus();
                    }else{
                        currentIndex=a;
                    }

                    //sessionListView.decrementCurrentIndex();
                }
            }
            Keys.onDownPressed: {
                if(sessionListView.currentIndex==sessionListView.count-1 ){
                    newSeatchat.forceActiveFocus();
                }
                else{
                    var a = findNextIndex(currentIndex);
                    if(a==-1){
                        if(scroller.visible)scroller.forceActiveFocus()
                        else newSeatchat.forceActiveFocus();
                    }else{
                        currentIndex=a;
                    }
                    //sessionListView.incrementCurrentIndex();
                }
            }
            Keys.onRightPressed: {
                if(scroller.visible){scroller.forceActiveFocus();
                }else{
                    if(chat_text_box.isDisable)
                        close_button.forceActiveFocus()
                    else chat_textBox.forceActiveFocus()
                }
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    sessionListView.sessionSelected()
                    sessionListView.forceActiveFocus();

                }
            }
        }

        Component{
            id:sessionDelegate;
            Rectangle{
                id:thisItem
                height:(participantCount>=1)?cellHeight:0;
                opacity:(participantCount>=1)?1:0;
                width:cellWidth
                color:"transparent"
                //                property variant currSession : currentsession
                //                onCurrSessionChanged:{
                //                    core.log(">>>>>>>>>>>>>>> inside delegate currSession:"+currSession+", index:"+index+", session:"+session);
                //                }
                Image{
                    id:high
                    source:viewHelper.configToolImagePath+configTag["friendlist_highlight"]
                    //                    anchors.bottom:parent.bottom;
                    //                    anchors.bottomMargin:qsTranslate('',appearance+'tracks_high_bm')
                    height:cellHeight//qsTranslate('',appearance+'tracks_high_h');
                    width:parent.width
                    opacity:(sessionListView.currentIndex==index && sessionListView.activeFocus)?1:0
                }

                ViewText{
                    id:buddyDetail
                    width: qsTranslate('','seatchat_frndlist_cell_txtw')
                    height: qsTranslate('','seatchat_frndlist_cell_txth')
                    anchors.horizontalCenter:thisItem.horizontalCenter;//anchors.leftMargin:qsTranslate('','seatchat_seat_txt_lm');
                    anchors.left:thisItem.left
                    anchors.leftMargin: qsTranslate('','seatchat_frndlist_cell_txtlm')
                    anchors.verticalCenter: thisItem.verticalCenter;
//                    textFormat: Text.RichText
                    elide: Text.ElideRight
                    varText:[(participantCount==1)?(nickName)?(participantSeatNo+" | "+ pif.seatChat.getSeatName(participantSeatNo)):participantSeatNo:participantSeatNo,(session==currentsession )?configTag["chatfrnd_name_h"]:configTag["chatfrnd_name_n"],qsTranslate('','seatchat_frndlist_cell_fontsize')]

                }
                Rectangle{
                    id:divline
                    anchors.top: parent.bottom
                    width: qsTranslate('','seatchat_leftpan_title_w')
                    height:qsTranslate("",'seatchat_leftpan_title_divlh')
                    color:configTag["chatfrnd_name_n"]
                }
                Image{
                    id:image
                    anchors.right:thisItem.right
                    anchors.rightMargin: qsTranslate("",'seatchat_notify_icon_rm')
                    anchors.verticalCenter: thisItem.verticalCenter;
                    source:{
                        if(session==currentsession){
                            if(participantCount==1){
                                return viewHelper.configToolImagePath+configTag["chatfrnd_icon_h"]
                            }else if(participantCount>1)
                                return viewHelper.configToolImagePath+configTag["chatconf_icon_h"]
                            else ""
                        }else{
                            if(participantCount==1){
                                return viewHelper.configToolImagePath+configTag["chatfrnd_icon_n"]
                            }else if(participantCount>1)
                                return viewHelper.configToolImagePath+configTag["chatconf_icon_n"]
                            else ""
                        }
                    }
                }
                SimpleNavBrdrBtn{
                    id:chat_msg_Icon
                    anchors.right:image.left
                    anchors.rightMargin: qsTranslate('','seatchat_notify_icon_hgap')
                    anchors.verticalCenter: thisItem.verticalCenter;
                    btnTextWidth:qsTranslate('','header_chat_notify_text_w');
                    normalImg: viewHelper.configToolImagePath+configTag["newmsg_icon_n"]//(widgetList.isVideoVisible()?configHeaderTag.chat_notify_n:configHeaderTag.chat_notify_n)
                    buttonText: [unReadMsgCount, "white",qsTranslate('','seatchat_notify_icon_fontsize')]
                    //                    font.family:viewHelper.adultFont
                    //                    font.pixelSize:qsTranslate('','seatchat_frndlist_cell_fontsize')
                    visible:(unReadMsgCount>0)?true:false
                }

                MouseArea{
                    id:sessionMA
                    anchors.fill: thisItem;
                    onPressed: {

                    }
                    onReleased: {
                        core.log("View/SeatChat | MouseArea | index = "+index +  " session = "+session+ " | pif.seatChat.getCurrentSessionId() = "+pif.seatChat.getCurrentSessionId())
                        sessionListView.currentIndex=index;
                        sessionListView.sessionSelected()
                        sessionListView.forceActiveFocus();

                    }
                }
                Component.onCompleted: {
                    core.log("CompChatSesion | Component.onCompleted | index = "+index + " | session = "+session+ " | pif.seatChat.getCurrentSessionId() = "+pif.seatChat.getCurrentSessionId())
                    console.log("CompChatSesion| viewHelper.chatSessionModel.count = "+sessionListView.count)
                    //                    sessionListView.currentIndex==index


                }
            }
        }
        SimpleBorderBtn{
            id:textOverlay
            opacity:(sessionListView.height+sessionListView.contentY<sessionListView.contentHeight )?1:0
            width:qsTranslate('','seatchat_frndlist_cell_w')
          //  height: qsTranslate('','seatchat_frndlist_cell_h')
            //            normalBorder: [qsTranslate('','common_scrollfade_extra_margin')]
            anchors.bottom: sessionListView.bottom
            source:viewHelper.configToolImagePath+configTag["bottom_fade_bg"]
        }
    }
    Scroller{
        id:scroller
        targetData:sessionListView;
        sliderbg:[qsTranslate('','seatchat_chatroomslider_base_margin'),qsTranslate('','seatchat_chatroomslider_w'),qsTranslate('','seatchat_chatroomslider_base_h'),configTag["scroll_base"]]
        scrollerDot:configTag["scroll_button"]
        upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
        downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
        anchors.left:chat_session.right;
        anchors.leftMargin:qsTranslate('','seatchat_chatroomslider_lm')
        height:qsTranslate('','seatchat_chatroomslider_h')
        width: qsTranslate('','seatchat_chatroomslider_w')
        //            anchors.verticalCenter: sessionListView.verticalCenter;
        //visible: (pif.seatChat.sessionListModel.count>5)?true:false
        visible:(sessionListView.contentHeight>sessionListView.height)
        onSliderNavigation:{
            if(direction=="left"){
                sessionListView.forceActiveFocus()
            }else if(direction=="top"){
                nick_vkb.forceActiveFocus()
            }else if (direction=="down"){
                newSeatchat.forceActiveFocus()
            }else {
                chat_textBox.forceActiveFocus()
            }

        }
    }
}



