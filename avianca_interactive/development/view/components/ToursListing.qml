import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

FocusScope {
    width: core.height;
    height: core.width;

    property string selectedScreenId : selCountryScreenID;

    function init() {
        core.log("ToursListing | init")
        destinationListView.currentIndex = detailScreenIndex;
        destinationListView.forceActiveFocus();
    }

    function setText(obj,Dmodel){
        obj.text = (Dmodel.get(0).addressText_cdata !== "") ? Dmodel.get(0).addressText_cdata : Dmodel.get(0).addressText;
        obj.color = configTool.config.aviancatours.screen.text_txt;
        obj.font.pixelSize = qsTranslate('','tours_txt_fontsize');
    }
    function modelUpdate(){
       listingScreenAddressModel.reload()
       listingScreenModel.reload()
    }
    XmlListModel{
        id: listingScreenAddressModel;
        source:currAppName ? xmlModelSourceOrg : "";
        query: "/application/screen[@_id='"+selCountryScreenID+"']";

        XmlRole { name:"addressText"; query: "text2/"+langCode+"/@_value/string()"; }
        XmlRole { name:"addressText_cdata"; query: "text2/"+langCode+"/string()"; }
        XmlRole { name:"listBgImage"; query: "image1/@_value/string()"; }
        XmlRole { name:"listLogoImage"; query: "image2/@_value/string()"; }

        onCountChanged: {
            setText(listingAddText,listingScreenAddressModel);
            listingBg.source = microappMain.currAppImages + listingScreenAddressModel.get(0).listBgImage;
            logoListing.source = microappMain.currAppImages + listingScreenAddressModel.get(0).listLogoImage;
        }
    }

    XmlListModel{
        id: listingScreenModel;
        source:currAppName ? xmlModelSourceOrg : "";
        query: "/application/screen[@_id='"+selCountryScreenID+"']/list1/listItem";

        XmlRole { name:"destinationText"; query: "text3/"+langCode+"/@_value/string()"; }
        XmlRole { name:"destinationText_cdata"; query: "text3/"+langCode+"/string()"; }
        XmlRole { name:"countryText"; query: "text4/"+langCode+"/@_value/string()"; }
        XmlRole { name:"countryText_cdata"; query: "text4/"+langCode+"/string()"; }
//        XmlRole { name:"currencyText"; query: "text5/"+langCode+"/@_value/string()"; }
        XmlRole { name:"amountText"; query: "text6/"+langCode+"/@_value/string()"; }
        XmlRole { name:"amountText_cdata"; query: "text6/"+langCode+"/string()"; }
        XmlRole { name:"packageInfoText"; query: "text7/"+langCode+"/@_value/string()"; }
        XmlRole { name:"packageInfoText_cdata"; query: "text7/"+langCode+"/string()"; }
        XmlRole { name:"categoryImage"; query: "image2/@_value/string()"; }
        XmlRole { name:"detailScreenID"; query: "link1/@_link/string()"; }
    }

    Image {
        id: listingBg;
//        source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.synposis_bg;
    }

    Image {
        id: logoListing;
        anchors.top: listingBg.top;
        anchors.left: listingBg.left;
        anchors.topMargin: qsTranslate('','tours_logo_y');
        anchors.leftMargin: qsTranslate('','tours_logo_x');
//        source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.tours_logo;
    }

    ViewText{
        id: listingAddText;
        anchors.left: logoListing.right;
        anchors.leftMargin: qsTranslate('','tours_txt_hgap');
        anchors.verticalCenter: logoListing.verticalCenter;
        width: qsTranslate('','tours_txt_w');
        height: qsTranslate('','tours_txt_h');
        elide: Text.ElideRight;
        textFormat: Text.RichText;
        wrapMode: Text.Wrap;
        lineHeightMode:Text.FixedHeight;
        lineHeight:qsTranslate('','tours_txt_lh');
        maximumLineCount:4;
    }

    SimpleButton {
        id: closeBtn;
        increaseTouchArea: true
        increaseValue:-60
        anchors.top: listingBg.top;
        anchors.right: listingBg.right;
        anchors.topMargin: qsTranslate('','tours_close_y');
        anchors.rightMargin: qsTranslate('','tours_close_x');
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.close_app_n;
        highlightimg: viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.close_h;
        pressedImg:viewHelper.configToolImagePath+ configTool.config.aviancatours.screen.close_h;

        onEnteredReleased: {
            viewController.getActiveScreenRef().actionOnExit(true);
            //viewController.showScreenPopup(23);
        }
        Keys.onDownPressed: {
            destinationListView.forceActiveFocus();
        }
    }

    ListView{
        id:destinationListView;
        anchors.top: listingBg.top;
        anchors.left: listingBg.left;
        anchors.topMargin: qsTranslate('','tours_place_y');
        anchors.leftMargin: qsTranslate('','tours_place_x');
        height: qsTranslate('','tours_place_h');
        width: parseInt(qsTranslate('','tours_place_w')) - parseInt(qsTranslate('','tours_place_x'));
        orientation: ListView.Horizontal;
        model: listingScreenModel;
        highlightFollowsCurrentItem: true;
        highlightMoveDuration: 400;
        delegate: destinationDelegate;
        clip: true;
        cacheBuffer: parseInt(qsTranslate('','tours_place_w'))
        onCountChanged: {
            destinationListView.currentIndex = detailScreenIndex;
            destinationListView.forceActiveFocus();
        }
        Keys.onUpPressed: {closeBtn.forceActiveFocus();}
        Keys.onLeftPressed: {destinationListView.decrementCurrentIndex();}
        Keys.onRightPressed: {destinationListView.incrementCurrentIndex();}
//        Keys.onReleased: {
//            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
//                destinationListView.currentIndex = index;
//                detailScreenId = listingScreenModel.get(index).detailScreenID;
//                detailScreenIndex = index;
//                microappMain.childLoader.sourceComponent = appDetails;
//            }
//        }

    }

    Component{
        id: destinationDelegate;
        Item {
            id: container;
            width: qsTranslate('','tours_place_item_w');
            height: highlightImage.height;

            Image {
                id: destinationContainer;
                anchors.centerIn: parent
                source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.place_bg_n;

                MouseArea{
                    anchors.fill: parent;
                    onPressed: {
                        destinationListView.currentIndex = index;
                        destinationListView.forceActiveFocus();
                    }
                    onReleased: {
                        destinationListView.currentIndex = index;
                        detailScreenId = listingScreenModel.get(index).detailScreenID;
                        detailScreenIndex = index;
                        microappMain.childLoader.sourceComponent = appDetails;
                    }
                }
            }
            Image {
                id: poster;
                anchors.top: destinationContainer.top;
                anchors.horizontalCenter: parent.horizontalCenter;
                height: qsTranslate('','tours_place_poster_h');
                width: qsTranslate('','tours_place_poster_w');
                clip: true;
                source: microappMain.currAppImages + listingScreenModel.get(index).categoryImage;
            }

            Image {
                id: overlayImage;
                anchors.bottom: poster.bottom;
                anchors.horizontalCenter: parent.horizontalCenter;
                source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.overlay;

                ViewText{
                    id: destinationText;
                    anchors.left: parent.left;
                    anchors.leftMargin: qsTranslate('','tours_place_txt_lm');
                    horizontalAlignment: Text.AlignLeft;
                    width: qsTranslate('','tours_place_txt_w');
                    elide: Text.ElideRight;
                    wrapMode: Text.WordWrap;
                    maximumLineCount:1;
                    varText: [ (listingScreenModel.get(index).destinationText_cdata != "")? listingScreenModel.get(index).destinationText_cdata : listingScreenModel.get(index).destinationText,
                        configTool.config.aviancatours.screen.text_place,
                        qsTranslate('','tours_place_fontsize')
                    ]
                }
                ViewText{
                    id: countryText;
                    anchors.left: parent.left;
                    anchors.top: destinationText.bottom;
                    anchors.leftMargin: qsTranslate('','tours_place_txt_lm');
                    horizontalAlignment: Text.AlignLeft;
                    width: qsTranslate('','tours_place_txt_w');
                    elide: Text.ElideRight;
                    wrapMode: Text.WordWrap;
                    textFormat: Text.RichText;
                    maximumLineCount:1;
                    varText: [ (listingScreenModel.get(index).countryText_cdata != "") ? listingScreenModel.get(index).countryText_cdata : listingScreenModel.get(index).countryText,
                        configTool.config.aviancatours.screen.text_place,
                        qsTranslate('','tours_place_fontsize1')
                    ]
                }
            }

            ViewText{
                id: currencyText;
                anchors.top: overlayImage.bottom;
                anchors.left: parent.left;
                anchors.topMargin: qsTranslate('','tours_place_amt_tm');
                anchors.leftMargin: qsTranslate('','tours_place_amt_lm');
                width: qsTranslate('','tours_place_amt_w');
                wrapMode: Text.WordWrap;
                elide: Text.ElideRight;
                maximumLineCount:1;
//                varText: [ listingScreenModel.get(index).currencyText+" "+listingScreenModel.get(index).amountText,
                varText: [ (listingScreenModel.get(index).amountText_cdata != "") ? listingScreenModel.get(index).amountText_cdata : listingScreenModel.get(index).amountText,
                    configTool.config.aviancatours.screen.text_place_amount,
                    qsTranslate('','tours_place_fontsize2')
                ]
            }

            ViewText{
                id: packageText;//packageInfoText_cdata
                anchors.top: parent.top;
                anchors.left: parent.left;
                anchors.topMargin: qsTranslate('','tours_place_info_tm');
                anchors.leftMargin: qsTranslate('','tours_place_info_lm');
                height: qsTranslate('','tours_place_info_h');
                width: qsTranslate('','tours_place_info_w');
                maximumLineCount:5;
                clip:true;
                elide: Text.ElideRight;
                wrapMode: Text.Wrap;
                textFormat: Text.RichText;
                lineHeightMode:Text.FixedHeight;
                lineHeight:qsTranslate('','tours_place_info_lh');
                varText: [ (listingScreenModel.get(index).packageInfoText_cdata !== "") ? listingScreenModel.get(index).packageInfoText_cdata : listingScreenModel.get(index).packageInfoText,
                                                                                          configTool.config.aviancatours.screen.text_txt_info,
                                                                                          qsTranslate('','tours_place_fontsize3')
                ]
                onPaintedHeightChanged: {
                    if(paintedHeight > height)
                    {
                        text = text.substring(0,(height-1))+"...";
                    }
                }
            }
            Image {
                id: highlightImage;
                anchors.centerIn: parent;
                visible: ((destinationListView.currentIndex === index)&& (destinationListView.activeFocus)) ? true : false;
                source: viewHelper.configToolImagePath+configTool.config.aviancatours.screen.place_bg_h;
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    destinationListView.currentIndex = index;
                    detailScreenId = listingScreenModel.get(index).detailScreenID;
                    detailScreenIndex = index;
                    microappMain.childLoader.sourceComponent = appDetails;
                }
            }
        }
    }
}
