import QtQuick 1.1
import "../../framework/viewcontroller/"
import "../components/"
SmartLoader{
    id:popup;
    property variant bgDetails:[/*tm,w,h,color,radius*/]
    property variant titleDetails:[/*tm,w,h,text,color,fs*/]
    property variant descDetails:[/*tm,w,h,text,color,fs*/]
    property variant buttonDetails:[/*tm,w,h,hgap,margin,imagetag*/]
    property variant buttonTextDetails:[/*,w,text,colortag,fs*/]
    property variant configTag:refresh?configTool.config.popup.generic:"";
    property alias buttonView:buttonView
    property alias buttonTitle:title
    property alias close_button:close_button
    property int noOfButtons:1
    property int currentIndex:buttonView.currentIndex
    property bool refresh: true
    signal buttonSelect(int btnId);
    /**************************************************************************************************************************/
    Connections{
        target:visible? viewHelper:null
        onThemeManagerRefresh:{
            refresh=false;
            refresh=true;
        }
    }
    onActiveFocusChanged: {
        console.log("onActiveFocusChanged  | activeFocus = "+activeFocus)
        console.log("onActiveFocusChanged  | visible = "+visible)
        if(!activeFocus && visible){
            console.log("onActiveFocusChanged  | systemPopups = "+systemPopups.activeFocus)
//            delay_activeFocus.restart();
            console.log("onActiveFocusChanged  | systemPopups >>>>>>>>>>= "+systemPopups.activeFocus)
        }else{
        }
    }
    Timer{
        id:delay_activeFocus
        interval: 300
        onTriggered: {
            popup.forceActiveFocus();
            console.log("onActiveFocusChanged  | systemPopups >>>>>>>>>>= "+systemPopups.activeFocus)
        }
    }

    Rectangle{
        visible: !widgetList.isVideoVisible()
        color:qsTranslate("",'common_overlay_color')
        opacity:qsTranslate("",'common_overlay_opacity')
        anchors.fill:parent;
        MouseArea{
            anchors.fill: parent
            onClicked:{ }
        }
    }
    Image{
        visible: widgetList.isVideoVisible()
        source: viewController.settings.assetImagePath+qsTranslate('','common_overlay_netted_source')
        MouseArea{
            anchors.fill: parent
            onPressed: {}
            onReleased: {}
        }
    }
    Rectangle{
        id:bg;
        anchors.top:parent.top;
        anchors.topMargin:(bgDetails[0]!=0)?bgDetails[0]:(parent.height/2-height/2);
        width:bgDetails[1]
        height:bgDetails[2]
        color:bgDetails[3];
        radius: bgDetails[4]
        focus: true
        anchors.horizontalCenter:(viewController.getActiveSystemPopupID()==9 || viewController.getActiveSystemPopupID()==10)?undefined:parent.horizontalCenter
        anchors.right: (bgDetails[5]!=undefined && bgDetails[5])?parent.right:undefined
        anchors.rightMargin: (bgDetails[5]!=undefined && bgDetails[5])?bgDetails[5]:0

        SimpleNavButton{
            id:close_button
            anchors.right: parent.right
            anchors.rightMargin: qsTranslate('','popups_close_rm')
            anchors.top:parent.top
            anchors.topMargin:qsTranslate('','popups_close_tm')
            isHighlight: activeFocus
            focus:(viewController.getActiveSystemPopupID()==7)?true:false
            normImg:viewHelper.configToolImagePath+configTag["close_btn_n"];
            highlightimg:  viewHelper.configToolImagePath+configTag["close_btn_h"];
            pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
            nav:["","","",""]
            visible:(viewController.getActiveSystemPopupID()==7||viewController.getActiveSystemPopupID()==14)
            onEnteredReleased:{viewController.hideScreenPopup();}
        }

        ViewText{
            id:title
            anchors.top:parent.top;
            anchors.topMargin:(titleDetails[0]!=0)?titleDetails[0]:(parent.height/2-height/2);
            anchors.horizontalCenter:parent.horizontalCenter;
            width:titleDetails[1]
            height:titleDetails[2]
            varText:[titleDetails[3],titleDetails[4],titleDetails[5]]
            wrapMode:"Wrap";
            horizontalAlignment:(titleDetails[6])?titleDetails[6]:"AlignLeft";
            verticalAlignment: Text.AlignVCenter
            maximumLineCount:(desc.text!="")?1:5;
            lineHeight:(titleDetails[7])?titleDetails[7]:1;
            lineHeightMode:(titleDetails[7])?"FixedHeight":"ProportionalHeight"
            elide: Text.ElideRight

        }
        ViewText{
            id:desc
            anchors.top:title.bottom;
            anchors.topMargin:descDetails[0]
            anchors.horizontalCenter:parent.horizontalCenter;
            width:descDetails[1]
            height:descDetails[2]
            varText:[descDetails[3],descDetails[4],descDetails[5]]
            wrapMode: "Wrap"
            textFormat: "StyledText"
            lineHeight:descDetails[6]
            lineHeightMode: "FixedHeight"

        }
        Row{
            id:buttonView
            anchors.bottom:parent.bottom;
            anchors.bottomMargin:buttonDetails[0];
            width:(parseInt(buttonDetails[1],10))*noOfButtons+(noOfButtons>1?(parseInt(buttonDetails[3],10))*(noOfButtons-1):0)
            height:buttonDetails[2];
            anchors.horizontalCenter: parent.horizontalCenter;
            spacing:buttonDetails[3];
            property int currentIndex:0
            focus:(noOfButtons>0);
            Keys.onLeftPressed:{
                if(currentIndex!=0){
                    currentIndex--;
                }
            }
            Keys.onRightPressed:{
                core.debug("currentIndex| onRightPressed "+currentIndex)
                if(currentIndex!=(noOfButtons-1)){
                    currentIndex++;
                }
            }
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    buttonSelect(currentIndex)
                }
            }

            Repeater{
                model:noOfButtons;
                SimpleBorderBtn{
                    id:btn;
                    normalImg:viewHelper.configToolImagePath+configTag[buttonDetails[5]+"_n"];
                    width:buttonDetails[1];
                    height:buttonDetails[2]
                    normalBorder:[buttonDetails[4]]
                    highlightBorder: [buttonDetails[4]]
                    highlightImg:viewHelper.configToolImagePath+configTag[buttonDetails[5]+"_h"];
                    pressedImg: viewHelper.configToolImagePath+configTag[buttonDetails[5]+"_p"];
                    isHighlight:(buttonView.activeFocus && index==buttonView.currentIndex)
                    btnTextWidth:buttonTextDetails[0]
                    buttonText:[buttonTextDetails[index+3],isPressed?configTag[buttonTextDetails[1]+"_p"]:isHighlight?configTag[buttonTextDetails[1]+"_h"]:configTag[buttonTextDetails[1]+"_n"],buttonTextDetails[2]]
                    MouseArea{
                        anchors.fill:parent;
                        onClicked:{
                            buttonView.currentIndex=index;
                            buttonSelect(index);
                        }
                    }
                }
            }
        }
    }
}
