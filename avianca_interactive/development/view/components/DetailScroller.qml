
import QtQuick 1.1
FocusScope{
    id:scrollerFS
    property Flickable targetData
    property alias upArrow: upArrow
    property alias downArrow: downArrow
    property variant sliderbg
    property variant scrollerDot:[]
    property variant upArrowDetails:[]
    property variant downArrowDetails:[]
    property int offset: qsTranslate('','common_scrollbtn_margin')
    property int targetConHeight:targetData?targetData.contentHeight:0;
    signal sliderNavigation(string direction)
    signal arrowPressed(string direction)
    clip:true;
    visible:(targetConHeight>targetData.height)
    /**************************************************************************************************************************/
    onTargetDataChanged:{
        downArrow.focus=true;
    }
    /**************************************************************************************************************************/
    function nextPage(){
        if((targetData.contentY+targetData.height)>(targetConHeight-targetData.height)){
            targetData.contentY=(targetData.contentY+(targetConHeight-(targetData.contentY+targetData.height)));
        }else{
            targetData.contentY = targetData.contentY+targetData.height;
        }
        arrowPressed("down");
    }
    function prevPage(){
        if((targetData.contentY-targetData.height)<0  ){
            targetData.contentY=0;
        }else{
            targetData.contentY = targetData.contentY-targetData.height;
            if(targetData.contentY <0)targetData.contentY =0
        }
    }
    /**************************************************************************************************************************/
    BorderImage {
        id: slider_bg
        border.left: sliderbg[0]; border.top:sliderbg[0]
        border.right:sliderbg[0]; border.bottom: sliderbg[0]
        height: sliderbg[2]
        clip:true;
        source:viewHelper.configToolImagePath+sliderbg[3]
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter;
        BorderImage {
            id: slider_main
            source: viewHelper.configToolImagePath+ scrollerDot
            border.left: scrollerFS.offset; border.top:scrollerFS.offset
            border.right: scrollerFS.offset; border.bottom: scrollerFS.offset
            smooth:true
            anchors.horizontalCenter: slider_bg.horizontalCenter
            y:{targetData?Math.ceil((targetData.contentY/(targetConHeight-targetData.height))*(parent.height- (slider_main.height ))):0}
            height: {
                if(visible){
                    if(targetData==undefined)return;
                    if(targetData.height<targetConHeight){
                        if(((targetData.height/targetConHeight)*slider_bg.height) < sourceSize.height) return sourceSize.height
                        else  return (targetData.height/targetConHeight)*slider_bg.height
                    }
                    else
                        return 0
                }
            }
            BorderImage {
                id: slider_dot
                source:viewController.settings.assetImagePath+"slider_dot.png"
                height: (slider_main.height<sourceSize.height)?sourceSize.height/2:sourceSize.height
                border.left: 5; border.top: 5
                border.right: 5; border.bottom: 5
                anchors.centerIn: parent
                opacity:(current.template_id=="movies"||current.template_id=="tv"||current.template_id=="music")?1:0;
            }
        }
    }
    SimpleNavButton{
        id:upArrow
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+upArrowDetails[0]
        highlightimg: viewHelper.configToolImagePath+upArrowDetails[1]
        pressedImg: viewHelper.configToolImagePath+upArrowDetails[2]

        isDisable:  targetData!=null?(targetData.contentY<=0?true:false) :true
        anchors.top:parent.top
        anchors.horizontalCenter: slider_bg.horizontalCenter
        nav:["","","",!downArrow.isDisable?downArrow:""]
        onPadReleased:{if(dir!="down")sliderNavigation(dir)}
        onNoItemFound: {sliderNavigation(dir)}
        onEnteredReleased: {prevPage();arrowPressed("up");}
        onIsDisableChanged: {
            if(activeFocus && isDisable)
                downArrow.forceActiveFocus()
            else if(isDisable && !downArrow.isDisable) downArrow.focus=true
        }
    }
    SimpleNavButton{
        id:downArrow
        isHighlight: activeFocus
        normImg: viewHelper.configToolImagePath+downArrowDetails[0]
        highlightimg: viewHelper.configToolImagePath+downArrowDetails[1]
        pressedImg: viewHelper.configToolImagePath+downArrowDetails[2]
        //disableImg:viewHelper.configToolImagePath+downArrowDetails[3]
        isDisable:  targetData!=null?((targetData.contentY>=targetConHeight-targetData.height )?true:false):true
        anchors.bottom:parent.bottom;
        anchors.horizontalCenter: slider_bg.horizontalCenter
        nav:["",!upArrow.isDisable?upArrow:"",downArrow,""]
        onPadReleased:{if(dir!="up")sliderNavigation(dir)}
        onNoItemFound: {sliderNavigation(dir)}
        onEnteredReleased:{ nextPage();}
        onIsDisableChanged: {
            if(activeFocus && isDisable )
                upArrow.forceActiveFocus()
            else if(isDisable && !upArrow.isDisable) upArrow.focus=true
        }
    }
}






//// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
//import QtQuick 1.1

//FocusScope{
//    id:scrollerFS
////    property Flickable targetData
//    property ListView targetData
//    property int targetDataCount:0;
//    property int targetDataIndex:-1;
//    property alias upArrow: upArrow
//    property alias downArrow: downArrow
//    property variant sliderbg
//    property variant scrollerDot:[]
//    property variant upArrowDetails:[]
//    property variant downArrowDetails:[]
//    property int offset: qsTranslate('','common_scrollbtn_margin')
//    property int targetConHeight:targetData?targetData.contentHeight:0;
//    signal sliderNavigation(string direction)
//    signal arrowPressed(string direction)
//    clip:true;
//    visible:(targetConHeight>targetData.height)
//    /**************************************************************************************************************************/
//    onTargetDataCountChanged: {
//        core.log(">>>>>>>>>> targetData Count changed : "+targetDataCount)
//    }
//    onTargetDataIndexChanged: {
//        core.log(">>>>>>>>>> targetData Index changed : "+targetDataIndex)
////        slider_main.y = (Math.ceil((slider_bg.height*targetDataIndex)));
//        core.log(">> slider y : "+slider_main.y)
//    }
//    onTargetDataChanged:{
//        downArrow.focus=true;
//    }
//    /**************************************************************************************************************************/
///*
//    function nextPage(){
//        if((targetData.contentY+targetData.height)>(targetConHeight-targetData.height)){
//            targetData.contentY=(targetData.contentY+(targetConHeight-(targetData.contentY+targetData.height)));
//        }else{
//            targetData.contentY = targetData.contentY+targetData.height;
//        }
//        var lastIndex = targetData.indexAt(targetData.contentX+10,targetData.contentY+10)
//        var firstIndex = targetData.indexAt(targetData.contentX+10,targetData.contentY+10)
//        targetData.currentIndex = firstIndex;
//        console.log('>>>>>>>>>>>>>>>> nextPage firstIndex'+lastIndex)
////        arrowPressed("down");
//    }
//    function prevPage(){
//        if((targetData.contentY-targetData.height)<0  ){
//            targetData.contentY=0;
//        }else{
//            targetData.contentY = targetData.contentY-targetData.height;
//            if(targetData.contentY <0)targetData.contentY =0
//        }
//        var firstIndex = targetData.indexAt(targetData.contentX+10,targetData.contentY+10)
//        console.log('>>>>>>>>>>>>>>>> prevPage firstIndex'+firstIndex)

//    }
//        function processPrevPage(){
//            gviewNumAnim.from = synopsisLV.contentY
//            gviewNumAnim.to = (synopsisLV.contentY-synopsisLV.height) > 0 ? (synopsisLV.contentY-synopsisLV.height): 0;
//            sliderAnim.start();
//        }

//        function processNextPage(param){
//            gviewNumAnim.from = synopsisLV.contentY
//            gviewNumAnim.to = synopsisLV.contentY+synopsisLV.height;
//            sliderAnim.start();
//        }
//*/
//    function nextPage(){
//        core.log(">>>>>>>>>>>>>>>  I nextPage() targetData.contentY : "+targetData.contentY)
//        targetData.processNextPage();
//        /*
//        if((targetData.contentY+targetData.height)>(targetConHeight-targetData.height)){
//            targetData.contentY=(targetData.contentY+(targetConHeight-(targetData.contentY+targetData.height)));
//        }else{
//            targetData.contentY = targetData.contentY+targetData.height;
//        }
//        core.log(">>>>>>>>>>>>>>>  I next paghe targetData.contentY : "+targetData.contentY)
//        //        targetData.contentY = targetData.contentY+targetData.height;
//        core.log(">>>>>>>>>>>>>>>  II next page targetData.contentY : "+targetData.contentY)
//        var lastIndex = targetData.indexAt(targetData.contentX+10,(targetData.contentY+10))
//        console.log('>>>>>>>>>>>>>>>> nextPage firstIndex'+lastIndex)
//        targetData.currentIndex = lastIndex;
//        targetData.positionViewAtIndex(targetData.currentIndex,ListView.Beginning)
//        //        arrowPressed("down");*/
//    }
//    function prevPage(){
//        /*
//        if((targetData.contentY-targetData.height)<0  ){
//            targetData.contentY=0;
//        }else{
//            targetData.contentY = targetData.contentY-targetData.height;
//            if(targetData.contentY <0)targetData.contentY =0
//        }
//        //        targetData.contentY = targetData.contentY-targetData.height;
//        var firstIndex = targetData.indexAt(targetData.contentX+10,targetData.contentY+10)
//        targetData.currentIndex = firstIndex;
//        */
//        core.log(">>>>>>>>>>>>>>>  I prevPage() targetData.contentY : "+targetData.contentY)
//        targetData.processPrevPage();

//    }
////    function prevPage(){
////        //        var ind = targetData.indexAt(0,targetData.contentY)
////        if(targetData.contentY>0){
////            if((targetData.contentY-targetData.height)>0){
////                targetData.contentY = targetData.contentY-targetData.height
////            }else{
////                targetData.contentY = 0
////            }
////            var firstIndex = targetData.indexAt(targetData.contentX+10,targetData.contentY+10)
////            console.log('>>>>>>>>>>>>>>>> prevPage firstIndex'+firstIndex)
////            targetData.currentIndex = firstIndex;
////            targetData.positionViewAtIndex(firstIndex,ListView.Beginning)
////        }
////        targetData.forceActiveFocus()
////    }

////    function nextPage(){
////        if(targetData.contentY<(targetData.contentHeight-targetData.height)){
////            if((targetData.contentY+targetData.height)<(targetData.contentHeight-targetData.height)){
////                targetData.contentY = targetData.contentY+targetData.height
////            }else{
////                targetData.contentY = targetData.contentY+((targetData.contentHeight-targetData.height)-targetData.contentY)
////            }
////            var lastIndex = targetData.indexAt(targetData.contentX+10,targetData.contentY+10)
////            console.log('>>>>>>>>>>>>>>>> pageDown lastIndex: '+lastIndex)
////            targetData.currentIndex = lastIndex+1;
////            targetData.positionViewAtIndex(lastIndex,ListView.Beginning)
////        }
////        targetData.forceActiveFocus()
////    }
//    /**************************************************************************************************************************/
//    BorderImage {
//        id: slider_bg
//        border.left: sliderbg[0]; border.top:sliderbg[0]
//        border.right:sliderbg[0]; border.bottom: sliderbg[0]
//        height: sliderbg[2]
//        clip:true;
//        source:viewHelper.configToolImagePath+sliderbg[3]
//        anchors.verticalCenter: parent.verticalCenter
//        anchors.horizontalCenter: parent.horizontalCenter;
//        BorderImage {
//            id: slider_main
//            source: viewHelper.configToolImagePath+ scrollerDot
//            border.left: scrollerFS.offset; border.top:scrollerFS.offset
//            border.right: scrollerFS.offset; border.bottom: scrollerFS.offset
//            smooth:true
//            anchors.horizontalCenter: slider_bg.horizontalCenter
////                    y:{targetData?Math.ceil((targetData.contentY/(targetConHeight-targetData.height))*(parent.height- (slider_main.height ))):0}
//            y:{Math.ceil((slider_main.height*targetDataIndex))}
//            height: {
//                if(visible){
//                    if(targetData==undefined)return;
//                    if(targetData.height<targetConHeight){
////                                if(((targetData.height/targetConHeight)*slider_bg.height) < sourceSize.height) return sourceSize.height
////                                else  return (targetData.height/targetConHeight)*slider_bg.height
//                        return (slider_bg.height/targetDataCount);
//                    }
//                    else
//                        return 0
//                }
//            }
//            BorderImage {
//                id: slider_dot
//                source:viewController.settings.assetImagePath+"slider_dot.png"
//                height: (slider_main.height<sourceSize.height)?sourceSize.height/2:sourceSize.height
//                border.left: 5; border.top: 5
//                border.right: 5; border.bottom: 5
//                anchors.centerIn: parent
//                opacity:(current.template_id=="movies"||current.template_id=="tv"||current.template_id=="music")?1:0;
//            }
//        }
//    }
//    SimpleNavButton{
//        id:upArrow
//        isHighlight: activeFocus
//        normImg: viewHelper.configToolImagePath+upArrowDetails[0]
//        highlightimg: viewHelper.configToolImagePath+upArrowDetails[1]
//        pressedImg: viewHelper.configToolImagePath+upArrowDetails[2]

//        isDisable:  (targetData.contentY<=0) ? true : false;
////        isDisable:  targetData!=null?(targetData.contentY<=0?true:false) :true
//        anchors.top:parent.top
//        anchors.horizontalCenter: slider_bg.horizontalCenter
//        nav:["","","",!downArrow.isDisable?downArrow:""]
//        onPadReleased:{if(dir!="down")sliderNavigation(dir)}
//        onNoItemFound: {sliderNavigation(dir)}
//        onEnteredReleased: {prevPage();arrowPressed("up");}
//        onIsDisableChanged: {
//            if(activeFocus && isDisable)
//                downArrow.forceActiveFocus()
//            else if(isDisable && !downArrow.isDisable) downArrow.focus=true
//        }
//    }
//    SimpleNavButton{
//        id:downArrow
//        isHighlight: activeFocus
//        normImg: viewHelper.configToolImagePath+downArrowDetails[0]
//        highlightimg: viewHelper.configToolImagePath+downArrowDetails[1]
//        pressedImg: viewHelper.configToolImagePath+downArrowDetails[2]
//        //disableImg:viewHelper.configToolImagePath+downArrowDetails[3]
//        isDisable:  targetData!=null?((targetData.contentY>=targetConHeight-targetData.height )?true:false):true
//        anchors.bottom:parent.bottom;
//        anchors.horizontalCenter: slider_bg.horizontalCenter
//        nav:["",!upArrow.isDisable?upArrow:"",downArrow,""]
//        onPadReleased:{if(dir!="up")sliderNavigation(dir)}
//        onNoItemFound: {sliderNavigation(dir)}
//        onEnteredReleased:{ nextPage();}
//        onIsDisableChanged: {
//            if(activeFocus && isDisable )
//                upArrow.forceActiveFocus()
//            else if(isDisable && !upArrow.isDisable) upArrow.focus=true
//        }
//    }
//}
