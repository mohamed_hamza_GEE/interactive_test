// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id:chatscreen
    property int listHeight:(chat_textBox.lineCount>1)?qsTranslate('','seatchat_chatbox_h')-chat_textBox.height:qsTranslate('','seatchat_chatbox_h');
    property int listWidth:qsTranslate('','seatchat_chatbox_w');
    property int cellWidth:qsTranslate('','seatchat_chating_cell_w');
    property int lastContentY: 0
    property alias chatListView: chatListView.model
    property alias scroller: scroller


    Item{
        id:chat_screen
        width :listWidth
        height:listHeight
        onHeightChanged: {
            chatListView.positionViewAtEnd()
        }

        //        color:"transparent"

        Image{
            id:top_fadeimage
            anchors.top:chat_screen.top
            width :listWidth
            source:viewHelper.configToolImagePath+configTag["top_fade_bg"]
            visible:(chatListView.contentHeight>chatListView.height &&chatListView.count>0 )?true:false//(chatListView.height+chatListView.contentY<chatListView.contentHeight)
            //            Text {
            //                id: id_tx
            //                text:"chatListView.contentHeight"+ chatListView.contentHeight + "chatListView.height "+ chatListView.height+"count = "+ chatListView.count
            //            }
        }

        Image{
            id:bottom_fadeimage
            anchors.bottom:chat_screen.bottom
            width :listWidth
            source:viewHelper.configToolImagePath+configTag["bottom_fade_bg"]
            visible:(chatListView.contentHeight>chatListView.height &&chatListView.count>0)?true:false//(chatListView.contentY >0)
        }

        ListView{
            id:chatListView
            width :listWidth
            height:listHeight
            onHeightChanged: {
                core.info("View/SeatChat | onHeightChanged | height = "+height);
            }
            snapMode:ListView.SnapToItem;
            boundsBehavior: ListView.StopAtBounds
            clip:true
            delegate: listingDelegate
            model:pif.seatChat.chatHistoryModel
            cacheBuffer: contentHeight
            onCountChanged: {
                chatListView.positionViewAtEnd()
                //                lastContentY=contentY//currentItem.y+currentItem.height-height
                core.log("View/SeatChat | onCountChanged | lastContentY = "+lastContentY);//+" | currentItem.y = " +currentItem.y+" | currentItem.height = "+currentItem.height+" | currentItem.height = "+currentItem.height)
            }
        }
        Scroller{
            id:scroller
            targetData:chatListView;
            visible:(chatListView.contentHeight>chatListView.height&&chatListView.count>0)?true:false
            sliderbg:[qsTranslate('','seatchat_chatroomslider_base_margin'),qsTranslate('','seatchat_chatroomslider_w'),qsTranslate('','seatchat_chatroomslider_base_h'),configTag["scroll_base"]]
            scrollerDot:configTag["scroll_button"]
            upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
            downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
            anchors.left:chat_screen.right;
            anchors.leftMargin:qsTranslate('','seatchat_chatroomslider_lm1')
            width: qsTranslate('','seatchat_chatroomslider_w')
            height:qsTranslate('','seatchat_chatroomslider_h')
            //            anchors.verticalCenter: sessionListView.verticalCenter;
            onSliderNavigation:{
                if(direction=="down"){chat_textBox.forceActiveFocus()}
                else if(direction=="up") {close_button.forceActiveFocus()}
            }

        }

        Component{
            id:listingDelegate;
            Item{
                id:thisItem
                height:chatMsgPanel.height
                width:listWidth
                property bool isMyChat : pif.getSeatNumber().toUpperCase()==seat?true:false;
                property alias chatText: chatText
                property alias whoSaidText: whoSaidText
                Rectangle{
                    id:chatMsgPanel
                    width:cellWidth//qsTranslate('','seatchat_chating_cell_w');//whoSaidText.width
                    color:"transparent"//thisItem.isMyChat?"black":"red"
                    opacity:.2
                    anchors.right:(thisItem.isMyChat)?parent.right:undefined
                    anchors.left:(!thisItem.isMyChat)?parent.left:undefined
                    height: chatText.paintedHeight+whoSaidText.height
                }

                ViewText{
                    id:whoSaidText
                    anchors.right:(thisItem.isMyChat)?parent.right:undefined
                    anchors.rightMargin: ((thisItem.isMyChat))?qsTranslate('','seatchat_chatsaidby_txt_rm'):0//(thisItem.isMyChat)?
                    anchors.left:(!thisItem.isMyChat)?parent.left:undefined
                    anchors.leftMargin: (!thisItem.isMyChat)?qsTranslate('','seatchat_chatsaidby_txt_lm'):0
                    width: qsTranslate('','seatchat_chatsaidby_txt_w');
                    wrapMode: Text.WordWrap

                    visible:(seat!="special")?true:false
                    varText:[thisItem.isMyChat?iSaid():otherSaid(),thisItem.isMyChat?configTag["me_txt"]:configTag["said_txt"],qsTranslate('',"seatchat_chatsaidby_txt_fontsize")]
                    //                    anchors.right:parent.right
                    horizontalAlignment: thisItem.isMyChat?Text.AlignRight:Text.AlignLeft
                    //                    visible:(viewHelper.showNotification_txt==true)?false:true


                    function iSaid(){
                        var tag =configTool.language.seatchat.me
                        return tag
                    }

                    function otherSaid(){
                        var tag = seat +" " +configTool.language.seatchat.said
                        return tag
                    }

                    //
                }

                Image {
                    id:divide_image
                    anchors.bottom: chatMsgPanel.bottom
                    anchors.horizontalCenter: chatMsgPanel.horizontalCenter
                    source: thisItem.isMyChat? viewHelper.configToolImagePath+configTag["me_divider"]:viewHelper.configToolImagePath+configTag["said_divider"]
                }

                ViewText{
                    id:chatText
                    //                    anchors.left: chatMsgPanel.left
                    //                    anchors.leftMargin: 30
                    anchors.right:(thisItem.isMyChat)?parent.right:undefined
                    anchors.rightMargin: ((thisItem.isMyChat))?qsTranslate('','seatchat_chatsaidby_txt_rm'):0//(thisItem.isMyChat)?
                    anchors.left:(!thisItem.isMyChat)?parent.left:undefined
                    anchors.leftMargin: (!thisItem.isMyChat)?qsTranslate('','seatchat_chatsaidby_txt_lm'):0
                    horizontalAlignment: thisItem.isMyChat?Text.AlignRight:Text.AlignLeft
                    //                    verticalAlignment: thisItem.isMyChat?Text.AlignRight:Text.AlignLeft
                    anchors.top: whoSaidText.bottom
                    width: qsTranslate('','seatchat_chat_txt_w');
                    height: paintedHeight
                    wrapMode: Text.WWrap
                    //                    maximumLineCount: 3
                    //                    textFormat: Text.StyledText
                    visible:(seat!="special")?true:false
                    varText:[msg.replace(/\n/gi,"<br>"),/*((viewHelper.showNotification_txt==true)?configTag["chat_joined_txt"]:*/configTag["chat_txt"],qsTranslate('',"seatchat_chat_txt_fontsize")]

                }
                ViewText{
                    id:speacialText
                    anchors.centerIn: parent
                    width: qsTranslate('','seatchat_chat_txt_w');
                    wrapMode: Text.Wrap
                    visible:(seat=="special")?true:false
                    varText:[msg,configTag["chat_joined_txt"],qsTranslate('',"seatchat_chat_txt_fontsize")]
                }
            }
        }
    }
}



