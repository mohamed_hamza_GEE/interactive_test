// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"


FocusScope {
    id:mainSeatChatWindow
    property variant configTag:configTool.config.seatchat.chatroom;
    property string freeText: ""
    property int sci:-1
    property alias chatSession_Tilte: chatSession_Tilte
    property alias chatSession_Count: chatSession_Count
    property bool showClose_button: false
    property bool nickName: true
    property variant statusReceivedSession:0;
    Connections{
        target:(visible)?viewHelper:null
        onVkbData:{
            if(identifier=="chat_nickname"){
                textBox.text=vkbData
                core.log("vkbData ======"+vkbData+ " textBox.text = "+textBox.text)
            }else if(identifier=="chat_msg"){
                chat_textBox.text =vkbData
                viewHelper.sendChatMessage(chat_textBox.text);
                chat_textBox.text=""
            }
        }
        onVkbClosed:{
            if(identifier=="chat_nickname"){
                nick_vkb.forceActiveFocus()
            }else if(identifier=="chat_msg"){
                chat_vkb.forceActiveFocus()}
        }
        onExitseatChat:{
            pif.seatChat.exitChat();
            compchatsession.updateActualSessionCount();
            //  getBuddyNames()
            viewHelper.tSeatNo=""
            //            viewHelper.chatSessionModel.clear();
            pif.seatChat.setCurrentSessionId(0)
            viewHelper.actualSessionCount=0
            //            viewHelper.chatActive=false

            viewHelper.fromInvitePopup=false
            seatchatScreen.backBtnPressed()
        }
        onToblockExitSingleBudddy:{
            pif.seatChat.exitChatSession(compchatsession.currentsession);
            viewHelper.setFocusUnblockPopupClosed()
        }

        onSeatChatServiceBlock:{
            core.log("MainSeatChatWindow.qml | onSeatChatServiceBlock  =")
            viewHelper.homeAction();
            viewController.updateSystemPopupQ(7,true);
        }
        //onSetFocusUnblockPopupClosed:{focusfromunblocklist.restart();}


        onShowHelpOnMainSeatChat:{
            if(unblock_Btn.visible){
                viewHelper.setHelpTemplate("seatchat_chatscreen");
            }else{
                viewHelper.setHelpTemplate("seatchat_chatscreen_noblock");
            }
        }
    }
    Connections{
        target:(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)?pif.sharedModelServerInterface:null
        onSigDataReceived:{
            core.debug("VKB | onSigDataReceived "+api);
            switch(api){
            case "keyboardData":{
                if(textBox.text == params.keyboardData)return;
                else{
                    if(viewHelper.identifier=="chat_nickname"){
                        textBox.text = params.keyboardData;
                    }else if(viewHelper.identifier=="chat_msg"){
                        chat_textBox.text = params.keyboardData;

                    }
                }
            }
            }
        }
    }

    Connections{
        target:viewHelper
        onAcceptInviataionfromPopup:{
            core.log("SCDebug | MainSeatChatWindow.qml | onAcceptInviataionfromPopup  ")
            viewHelper.fromInv=true;
        }
    }
    Timer{
        id:focusfromunblocklist
        interval:20;
        onTriggered:{
            core.log("focusfromunblocklist: "+activeFocus);
            if(unblock_Btn.opacity==0){
                if(viewHelper.actualSessionCount==0){
                    console.log("MainSeatChatWindow.qml | onSetFocusUnblockPopupClosed  unblock_Btn = "+activeFocus)
                    newSeatchat.forceActiveFocus()
                }else{}
            }else{
                if(viewHelper.actualSessionCount==0){
                    console.log("MainSeatChatWindow.qml | onSetFocusUnblockPopupClosed newSeatchat ="+activeFocus)
                    unblock_Btn.forceActiveFocus();
                }else{}
            }
        }
    }

    Timer{
        id:buddyRefereshDelay;
        interval:1000;
        onTriggered:{
            core.log("SCDebug | buddyRefreshDelay statusReceivedSession : "+statusReceivedSession);
            pif.seatChat.setCurrentSessionId(statusReceivedSession)//	statusReceivedSession
            compchatsession.setCurrIndex(statusReceivedSession)
            compchatsession.sessionSelected();
        }
    }



    Timer{
        id:delayRemove
        interval:300;
        onTriggered:{
            core.log("MainSeatChatWindow.qml | delayRemove() | sci =  "+sci);
            removeSingleSession()
        }
    }

    property bool fromInvStatus:false;


    Connections{
        target:pif.seatChat
        onSigInvitationReceived:{
            core.log("MainSeatChatWindow.qml | onSigInvitationReceivedi ");
            compchatsession.setCurrIndex(pif.seatChat.getCurrentSessionId())
        }

        onSigInvitationSent:{

        }
        onSigSentInvitationStatus:{
            core.log("onSigSentInvitationStatus | pif.seatChat.chatHistoryModel | before : "+pif.seatChat.chatHistoryModel.count)
            compchatsession.updateActualSessionCount();


            if(status==1){
                core.log("SCDebud| onSigInvitation Status  |  session: "+session)
                fromInvStatus=true;
                statusReceivedSession=session;
                // pif.seatChat.setCurrentSessionId(session)
            }else{
                if(viewHelper.actualSessionCount==0){
                    pif.seatChat.exitChatSession(session);
                }else{
                    core.log("SCDebud| onSigInvitation Status | decline |  session: "+session)
                }
            }

            core.log("onSigSentInvitationStatus | pif.seatChat.chatHistoryModel | after : "+pif.seatChat.chatHistoryModel.count)
        }

        onSigSessionAdded:{


        }

        onSigSessionListRefreshed:{
            core.info("MainSeatChatWindow.qml | onSigSessionListRefreshed ");
            compchatsession.updateActualSessionCount();
        }
        onSigBuddyListRefreshed:{
            core.info("SCDebug | View/SeatChat | onSigBuddyListRefreshed");
            compchatsession.updateActualSessionCount();

            core.info("SCDebug | View/SeatChat | onSigBuddyListRefreshed | viewHelper.actualSessionCount : "+viewHelper.actualSessionCount);

            core.info("SCDebug | View/SeatChat | onSigBuddyListRefreshed | viewHelper.fromInv : "+viewHelper.fromInv)

            if(viewHelper.fromInv){
                viewHelper.fromInv=false;
                init()
            }else if(fromInvStatus){
                fromInvStatus=false;
                if(viewHelper.actualSessionCount==1){
                    buddyRefereshDelay.restart();
                }
            }
        }

        onSigCurrentSessionIdChanged:{
            core.info("MainSeatChatWindow.qml | onSigCurrentSessionIdChanged | sessionId =  "+sessionId + "viewHelper.tSeatNo = "+viewHelper.tSeatNo);
            compchatsession.setCurrIndex(sessionId)
            compchatsession.currentsession=sessionId

        }
        onSigBuddyAdd:{
            core.info("Pifevents.qml | onSigBuddyAdd | session =  "+session+" | seatNum = "+seatNum+ " | seatName = "+seatName);
            core.info("Pifevents.qml | onSigBuddyAdd | viewHelper.tSeatNo  before =  "+viewHelper.tSeatNo);
            if(session==pif.seatChat.getCurrentSessionId()){
                msgADDDelay.seatname=seatName;
                msgADDDelay.seatno=seatNum
                msgADDDelay.restart();
            }
            compchatsession.updateActualSessionCount();
            if(fromInvStatus){
                fromInvStatus=false;
                if(viewHelper.actualSessionCount==1){
                    buddyRefereshDelay.restart();
                }
            }
            nickName=false
            nickName=true
            //            if(pif.seatChat.getCurrentSessionId()==0 || pif.seatChat.getCurrentSessionId()==undefined){
            //                compchatsession.sessionSelected()
            //            }
            core.info("Pifevents.qml | onSigBuddyAdd | viewHelper.tSeatNo  after =  "+viewHelper.tSeatNo)

        }
        onSigBuddyExit:{
            core.info("Pifevents.qml | onSigBuddyExit | session =  "+session+" | seatNum = "+seatNum+" | "+seatName);
            if(session==pif.seatChat.getCurrentSessionId()){
                msgLEFTDelay.seatname=seatName;
                msgLEFTDelay.seatno=seatNum
                msgLEFTDelay.restart();
            }
            if(pif.seatChat.getCurrentSessionId()==0 || pif.seatChat.getCurrentSessionId()==undefined){
                compchatsession.updateActualSessionCount();
                compchatsession.sessionSelected()
            }
            compchatsession.updateActualSessionCount();
            compchatsession.ifAnyBuddyOrExit(session);

        }
        onSigSessionDeleted:{
            core.info("MainSeatChatWindow.qml | onSigSessionDeleted | session =  "+session);
            delayRemove.restart();
            if((viewHelper.actualSessionCount+viewHelper.invitationSent)<viewHelper.maxSessionCount){
                viewHelper.maxSessionTimer()
                console.log("MainSeatChatWindow.qml | onSigSessionDeleted | viewHelper.actualSessionCount =  "+viewHelper.actualSessionCount);
                console.log("MainSeatChatWindow.qml | onSigSessionDeleted | viewHelper.maxSessionCount =  "+viewHelper.maxSessionCount);
            }
            compchatsession.updateActualSessionCount();

        }
        onSigUserInfoUpdated:{
            core.info("Pifevents.qml | onSigUserInfoUpdated | seatName =  "+seatName+" | seatNum = "+seatNum+ " | seatStatus = "+seatStatus)
            //            compchatsession.sessionSelected()
            nickName=false
            nickName=true
        }
    }
    Timer{
        id:msgADDDelay
        interval: 500
        property string seatname;
        property string seatno;
        onTriggered: {
            join_txt_Notifications(seatno,seatname)
        }
    }

    Timer{
        id:msgLEFTDelay
        property string seatname;
        property string seatno;
        interval: 500
        onTriggered: {
            leave_txt_Notifications(seatno,seatname)
        }
    }



    function init(){
        core.info("MainSeatChatWindow.qml | init() ")

        if(!pif.seatChat.getAppActiveState()){pif.seatChat.setAppActive();
            pif.seatChat.getAllBuddiesNSessionList()}
        if(viewHelper.actualSessionCount>0){
            if(pif.seatChat.getCurrentSessionId()==0 || pif.seatChat.getCurrentSessionId()==undefined){
                compchatsession.setDefault();
            }else{
                compchatsession.setCurrIndex(pif.seatChat.getCurrentSessionId())
                compchatsession.currentsession=pif.seatChat.getCurrentSessionId()
            }
            compchatsession.sessionSelected()
            compchatsession.sessionListView.forceActiveFocus();
        }else{
            newSeatchat.forceActiveFocus()
        }
        console.log("MainSeatChatWindow.qml | init() | pif.seatChat.sessionListModel.count = "+pif.seatChat.sessionListModel.count)
    }

    function join_txt_Notifications(seatNum,seatName){
        var join_txt =configTool.language.seatchat.notification_joined_txt
        var join_txt_label=""
        join_txt_label=(seatName=="" )?seatNum+" "+join_txt:seatNum +" | "+seatName + " "+ join_txt
        pif.seatChat.chatHistoryModel.append({"time":"","seat":"special", "seatName":"", "msg":join_txt_label})
    }

    function leave_txt_Notifications(seatNum,seatName){
        var left_txt = " "+configTool.language.seatchat.notification_left_txt
        var left_txt_label=""
        left_txt_label=(seatName=="" )?seatNum+" "+left_txt:seatNum+" | "+seatName + " "+ left_txt
        pif.seatChat.chatHistoryModel.append({"time":"","seat":"special", "seatName":"", "msg":left_txt_label})
    }

    function getBuddyNames(){
        var tempParticipantArrs=new Object()
        for(var j=0;j<pif.seatChat.sessionListModel.count;j++){
            var buddyList=pif.seatChat.sessionListModel.get(j).participantSeatNo
            var arrs=[]
            arrs=(buddyList=="" ||buddyList==undefined)?[]:buddyList.split(",")
            console.log("arrs = "+arrs.length)
            var tempName=[]
            var tempParticipantName
            for(var i=0;i<arrs.length;i++){
                var a=pif.seatChat.getSeatNameArray()[arrs[i]];
                if(arrs.length==1 && a!=""){
                    tempName.push(arrs[i])
                    tempName.push(a)
                    break;
                }
                tempName.push(arrs[i])
            }
            if(arrs.length>0)buddyList=tempName

            console.log("arrs.length = "+arrs.length)
            console.log("After buddyList  "+buddyList)
            if(arrs.length==1 && a!=""){
                tempParticipantName=buddyList!=""?buddyList.join(" | "):""
            }else{
                tempParticipantName=buddyList!=""?buddyList.join(","):""
            }
            console.log("buddyList = "+buddyList)
            console.log("tempParticipantName = "+tempParticipantName)
            tempParticipantArrs[pif.seatChat.sessionListModel.get(j).session]=tempParticipantName==""?"InActive session":tempParticipantName

            console.log("tempParticipantArrs = "+tempParticipantArrs.count )

        }
        viewHelper.participantNames=tempParticipantArrs
        console.log("viewHelper.participantNames = "+viewHelper.participantNames.count )

    }
    function removeChatSessionModel(session){
        core.log("MainSeatChatWindow.qml | removeChatSessionModel "+session )
        var viewSessionIndex=viewHelper.searchEntryInListModel(pif.seatChat.sessionListModel,"session",session);
        if(viewSessionIndex>-1){
            compchatsession.sessionListView.remove(viewSessionIndex)
        }
    }
    function removeSingleSession(){
        core.log("MainSeatChatWindow.qml | removeSingleSession " )
        var a=compchatsession.sessionListView.currentIndex
        core.log(" before | Mainseatchat.qml | Exitchat | compchatsession.sessionListView.currentIndex = "+compchatsession.sessionListView.currentIndex)
        if(a>=0){
            core.log(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  currentIndex = "+compchatsession.sessionListView.currentIndex)
            compchatsession.sessionSelected()
        }else{
            //            viewHelper.chatActive=false
            newSeatchat.forceActiveFocus()
        }
    }


    function backBtnPressed(){
        core.log("MainSeatChatWindow.qml | backBtnPressed " )
        viewHelper.fromInvitePopup=false
        closeLoader()
    }

    function clearOnExit(){
        core.log("MainSeatChatWindow.qml | clearOnExit " )
        pif.seatChat.exitChatScreen(true)
        visible=false;
        synopsisLoader.sourceComponent=blankComp;
        //        viewHelper.fromInvitePopup=false
    }


    Item{
        id:posterobj
        anchors.fill: parent
        property alias staticBG: staticBG
        Image{
            id:listingBG
            source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
        }
        Image{
            id:staticBG
            anchors.bottom:parent.bottom
            source:viewHelper.configToolImagePath+configTool.config.seatchat.screen.seatcat_background
            MouseArea{
                anchors.fill:parent
                onClicked: {

                }
            }
        }
    }

    Image{
        id:panelBG
        anchors.horizontalCenter: posterobj.horizontalCenter
        anchors.top:posterobj.top;
        anchors.topMargin: qsTranslate('','seatchat_chatroom_panel_tm')
        source:viewHelper.configToolImagePath+configTag["panel"]
        MouseArea{
            anchors.fill:parent
            onClicked: {

            }
        }
    }
    SimpleNavBrdrBtn{
        id:nickName_text_box
        width: qsTranslate('','seatchat_chatname_input_w')
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','seatchat_chatname_input_tm')
        anchors.left:panelBG.left
        isHighlight: textBox.activeFocus
        normalImg: viewHelper.configToolImagePath+configTag["input_n"];
        highlightImg: viewHelper.configToolImagePath+configTag["input_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["input_p"];
        normalBorder: [qsTranslate('','seatchat_chatname_input_margin')]
        highlightBorder: [qsTranslate('','seatchat_chatname_input_margin')]
        nav:["","",nick_vkb,textBox]
        //        onPadReleased: if(dir=="right"){extremeEnds("right")}
        onNoItemFound:  if(dir=="up") {widgetList.focustTomainMenu()
                        }
        onEnteredReleased:{console.log("nickName_text_box  ===== lanunch vkb")

        }
        Text{
            id:seatno_txt
            width:qsTranslate('','seatchat_chatseatno_txtw')
            height: qsTranslate('','seatchat_chatseatno_txth')
            anchors.left:nickName_text_box.left
            anchors.leftMargin: qsTranslate('','seatchat_chatseatno_txtlm')
            color:"grey"
            opacity:.5
            text:pif.getSeatNumber().toUpperCase()+" | "
            font.pixelSize:   qsTranslate('','seatchat_chatseatno_fontsize')
            font.family:viewHelper.adultFont;
            anchors.verticalCenter: nickName_text_box.verticalCenter
        }


        Text {
            id:dummy_txt
            width: qsTranslate('','seatchat_chatname_input_txtw')
            height: qsTranslate('','seatchat_chatname_input_txth')
            color:"#5a5b5e"//global.refFontColor[refMcStyle.nick_text.fontcolor]
            anchors.left:nickName_text_box.left
            anchors.leftMargin:qsTranslate('','seatchat_nickname_input_txtlm')
            anchors.verticalCenter:nickName_text_box.verticalCenter
            text:(textBox.activeFocus)?"":"   "+configTool.language.seatchat.nickname_title
            font.pixelSize:   qsTranslate('','seatchat_chatname_input_fontsize')
            font.family:viewHelper.adultFont;
            wrapMode: Text.Wrap
        }

        TextEdit{
            id:textBox
            property int lineLimit :1
            property int txtCharLimit :15
            width: qsTranslate('','seatchat_chatname_input_txtw')
            height: qsTranslate('','seatchat_chatname_input_txth')
            color:"black"
            anchors.verticalCenter: nickName_text_box.verticalCenter
            anchors.left:nickName_text_box.left
            anchors.leftMargin:qsTranslate('','seatchat_nickname_input_txtlm')
            font.pixelSize:  qsTranslate('','seatchat_chatname_input_fontsize')
            font.family:viewHelper.adultFont;
            cursorVisible:activeFocus
            activeFocusOnPress:false
            text:viewHelper.nickBoxInput
            focus:true

            //            maximumLength:15
            onTextChanged: {

                if(textBox.text.length > txtCharLimit)

                    textBox.text = textBox.text.substring(0,textBox.cursorPosition-1) + textBox.text.substring(textBox.cursorPosition,textBox.text.length)
                if(textBox.text == ""){
                    dummy_txt.text=configTool.language.seatchat.nickname_title;
                }else{
                    dummy_txt.text=""
                }
                if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                    pif.sharedModelServerInterface.sendApiData("keyboardData",{keyboardData:textBox.text});
                }
                pif.seatChat.setSeatName(textBox.text)
            }
            onActiveFocusChanged: {
                if(activeFocus){
                    pif.seatChat.setSeatName(textBox.text)
                    if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){   // CR Change
                        viewHelper.identifier="chat_nickname"
                        viewHelper.vkbInputData=textBox.text
                        core.log("onActionReleased | viewHelper.vkbInputData "+viewHelper.vkbInputData)
                        pif.sharedModelServerInterface.sendApiData("userDefinedhideChatFromSeat",{});
                        widgetList.getVkbRef().launchVKB(viewHelper.identifier,textBox.text)
                    }
                }else{
                    if(textBox.text=="")
                        textBox.text=viewHelper.nickBoxInput
                    else pif.seatChat.setSeatName(textBox.text)
                }
            }
            Keys.onReleased: {
                switch(event.key){
                case Qt.Key_Return:
                case Qt.Key_Enter:{
                    viewHelper.identifier="chat_nickname"
                    pif.seatChat.setSeatName(textBox.text)
                    event.accepted = true;
                }break;
                }
            }

            Keys.onUpPressed:{
                if(cursorRectangle.y==0) {nickName_text_box.forceActiveFocus();}
                else event.accepted=false
            }
            Keys.onDownPressed: {
                if(viewHelper.actualSessionCount>0)compchatsession.sessionListView.forceActiveFocus()
                else newSeatchat.forceActiveFocus()

            }
            MouseArea{
                anchors.fill:parent;
                onClicked:{
                    textBox.forceActiveFocus();
                    viewHelper.identifier="chat_nickname"
                    viewHelper.vkbInputData=textBox.text
                    core.log("onActionReleased | viewHelper.vkbInputData "+viewHelper.vkbInputData)
                    widgetList.getVkbRef().launchVKB(viewHelper.identifier,textBox.text)
                    if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                        pif.sharedModelServerInterface.sendApiData("userDefinedhideChatFromSeat",{});
                    }
                }
            }

        }

    }
    SimpleNavBrdrBtn{
        id:nick_vkb
        anchors.right: nickName_text_box.right
        anchors.rightMargin:qsTranslate('','seatchat_chatname_vkb_rm')
        anchors.verticalCenter: nickName_text_box.verticalCenter
        normalImg: viewHelper.configToolImagePath+configTag["vkb_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["vkb_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["vkb_p"]
        isHighlight: activeFocus
        nav: [textBox,"",(unblock_Btn.visible?unblock_Btn:leaveBtn),viewHelper.actualSessionCount>0?compchatsession:newSeatchat]
        onEnteredReleased: {
            viewHelper.identifier="chat_nickname"
            viewHelper.vkbInputData=textBox.text
            core.log("onActionReleased | viewHelper.vkbInputData "+textBox.text)
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("userDefinedhideChatFromSeat",{});
            }
            widgetList.getVkbRef().launchVKB(viewHelper.identifier,textBox.text)
        }
        onNoItemFound: {
            if(dir=="up"){
                widgetList.focustTomainMenu()
            }
        }
    }

    SimpleNavBrdrBtn{  // unblock all seats button
        id:unblock_Btn
        opacity:(pif.seatChat.blockListModel.count>0)?1:0
        property string textColor: unblock_Btn.activeFocus?configTool.config.seatchat.chatroom.btn_text_h:(unblock_Btn.isPressed?configTool.config.seatchat.chatroom.btn_text_p:configTool.config.seatchat.chatroom.btn_text_n)
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','seatchat_leave_unblock_btn_tm')
        anchors.right: leaveBtn.left
        anchors.rightMargin: qsTranslate('','seatchat_leave_unblock_btn_hgap')
        width: parseInt(qsTranslate('','seatchat_leave_unblock_btn_w'),10)
        //        height:parseInt(qsTranslate('','seatchat_leave_unblock_btn_h'),10)
        btnTextWidth:parseInt(qsTranslate('',"seatchat_leave_unblock_btn_txtw"),10);
        buttonText: [configTool.language.seatchat.unblock_seats,leaveBtn.textColor,qsTranslate('','seatchat_leave_unblock_btn_fontsize')]
        isHighlight: activeFocus
        //        isDisable:(pif.seatchat.sessionListModel.count==0)?true:false
        normalImg: viewHelper.configToolImagePath+configTag["btn_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["btn_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["btn_p"]
        normalBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
        highlightBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
        onEnteredReleased: {
            pif.seatChat.getallBuddiesNBlockList();
            viewController.showScreenPopup(11);
        }
        nav:[nick_vkb,"",leaveBtn.visible?leaveBtn:"",viewHelper.actualSessionCount==0?newSeatchat:close_button]
        onNoItemFound: {
            if(dir=="up"){
                widgetList.focustTomainMenu()
            }
        }
        onOpacityChanged:{
            if(opacity==0){
                if(activeFocus)newSeatchat.forceActiveFocus()
                //else compchatsession.forceActiveFocus();
                visible=false;
            }else{
                visible=true;
            }
        }
    }
    SimpleNavBrdrBtn{    // exit chat session
        id:leaveBtn
        focus: true
        property string textColor: leaveBtn.activeFocus?configTool.config.seatchat.chatroom.btn_text_h:(leaveBtn.isPressed?configTool.config.seatchat.chatroom.btn_text_p:configTool.config.seatchat.chatroom.btn_text_n)
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','seatchat_leave_unblock_btn_tm')
        anchors.right: panelBG.right
        width: parseInt(qsTranslate('','seatchat_leave_unblock_btn_w'),10)
        //        height:parseInt(qsTranslate('','seatchat_leave_unblock_btn_h'),10)
        btnTextWidth:parseInt(qsTranslate('',"seatchat_leave_unblock_btn_txtw"),10);
        buttonText: [configTool.language.seatchat.leave_chat_room,leaveBtn.textColor,qsTranslate('','seatchat_leave_unblock_btn_fontsize')]
        isHighlight: activeFocus
        elide:Text.ElideRight
        //        isDisable:(pif.seatchat.sessionListModel.count==0)?true:false
        normalImg: viewHelper.configToolImagePath+configTag["btn_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["btn_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["btn_p"]
        normalBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
        highlightBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
        onEnteredReleased: {
            //            viewController.updateSystemPopupQ(15,true)
            viewController.showScreenPopup(16); // changed to screenPopup;
        }//pif.seatChat.exitChat();closeLoader();}
        nav:[unblock_Btn.visible?unblock_Btn:nickName_text_box,"","",viewHelper.actualSessionCount==0?newSeatchat:close_button]
        onNoItemFound: {
            if(dir=="up"){
                widgetList.focustTomainMenu()
            }
        }
    }
    Item{
        id:sesion_txt
        width:parseInt(qsTranslate('','seatchat_leftpan_title_w'),10)
        height:qsTranslate('','seatchat_leftpan_title_h')
        anchors.top: panelBG.top;
        anchors.left:panelBG.left
        anchors.leftMargin: qsTranslate('','seatchat_leftpan_title_lm')
        anchors.topMargin:qsTranslate('','seatchat_leftpan_title_tm')
        //        color:"blue"
        //        opacity:.5
        ViewText{
            id:chatSession_Tilte
            width:parent.width-chatSession_Count.width
            anchors.verticalCenter: parent.verticalCenter
            wrapMode: Text.Wrap
            elide: Text.ElideRight
            maximumLineCount: 1
            varText:[configTool.language.seatchat.sessions_title,configTag["title_color"],qsTranslate('',"seatchat_leftpan_title_fontsize")]
        }
        ViewText{
            id:chatSession_Count
            width:qsTranslate('','seatchat_usercount_w')
            anchors.right: sesion_txt.right
            anchors.verticalCenter: sesion_txt.verticalCenter
            varText:[(viewHelper.actualSessionCount)+"/"+viewHelper.maxSessionCount,configTag["title_color"],qsTranslate('',"seatchat_usercount_fontsize")]
        }
        Rectangle{
            id:divline
            anchors.top: chatSession_Tilte.bottom
            //            anchors.topMargin:   qsTranslate("",'seatchat_leftpan_title_divline_h')
            width: qsTranslate('','seatchat_leftpan_title_w')
            height:qsTranslate("",'seatchat_leftpan_title_divlh')
            color:configTag["chatfrnd_name_n"]
        }
    }
    ViewText{ //session instruction text
        id:chatSession_instruction
        width:qsTranslate('',"seatchat_tostartchat_txt_w")
        height:paintedHeight
        anchors.top:panelBG.top
        anchors.topMargin: qsTranslate('',"seatchat_tostartchat_txt_tm")
        anchors.left:panelBG.left
        anchors.leftMargin: qsTranslate('',"seatchat_tostartchat_txt_lm")
        varText:[configTool.language.seatchat.session_instructions,"grey",qsTranslate('',"seatchat_tostartchat_txt_fontsize")]
        wrapMode: Text.WordWrap
        lineHeight:parseInt(qsTranslate('','seatchat_tostartchat_txt_lh'),10)
        lineHeightMode: Text.FixedHeight
        visible: (viewHelper.actualSessionCount==0)?true:false
    }
    ViewText{   // welcome text
        id:welcome_txt
        width:qsTranslate('',"seatchat_welcme_txt_w")
        height:qsTranslate('',"seatchat_welcme_txt_h")
        anchors.top:panelBG.top
        anchors.topMargin: qsTranslate('',"seatchat_welcme_txt_tm")
        anchors.left:panelBG.left
        horizontalAlignment: Text.Center
        anchors.leftMargin:qsTranslate('',"seatchat_welcme_txt_lm")
        varText:[configTool.language.seatchat.session_welcome,configTag["welcome_txt"],qsTranslate('',"seatchat_welcme_txt_fontsize")]
        visible:(viewHelper.actualSessionCount==0 )?true:false
    }
    ViewText{
        id:chat_room_empty
        width:qsTranslate('',"seatchat_welcme_txt_w")
        height:qsTranslate('',"seatchat_welcme_txt_h")
        anchors.top:panelBG.top
        anchors.topMargin: qsTranslate('',"seatchat_welcme_txt_tm")
        anchors.left:panelBG.left
        horizontalAlignment: Text.Center
        anchors.leftMargin:qsTranslate('',"seatchat_welcme_txt_lm")
        varText:[configTool.language.seatchat.empty_message,configTag["welcome_txt"],qsTranslate('',"seatchat_welcme_txt_fontsize")]
        //visible:(compchatsession.actualSessionCount==0)?false:true
        visible:false
    }
    ViewText{
        id:chat_instruction
        width:qsTranslate('','seatchat_rightpan_title_w')
        height:qsTranslate('','seatchat_rightpan_title_h')
        anchors.top: panelBG.top;
        anchors.left:panelBG.left
        anchors.leftMargin: qsTranslate('','seatchat_rightpan_title_lm')
        anchors.topMargin:qsTranslate('','seatchat_rightpan_title_tm')
        elide: Text.ElideRight
        wrapMode: Text.Wrap
        maximumLineCount: 1
        textFormat: Text.StyledText
        varText:[configTool.language.seatchat.chatting_with+": "+compchatsession.getParticipantseatno() ,configTag["title_color"],qsTranslate('',"seatchat_rightpan_title_fontsize")]
        visible: (viewHelper.actualSessionCount>0)?true:false
    }


    SimpleNavButton{
        id:close_button
        anchors.right: panelBG.right
        anchors.rightMargin: qsTranslate('','seatchat_navicon_btn_rm')
        anchors.top:panelBG.top
        anchors.topMargin:qsTranslate('','seatchat_navicon_btn_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTag["close_btn_n"];
        highlightimg:  viewHelper.configToolImagePath+configTag["close_btn_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
        nav:[confernce_button,leaveBtn,"",(compchatscren.scroller.visible)?compchatscren.scroller:(chat_text_box.isDisable)?close_button:chat_textBox]
        visible: (viewHelper.actualSessionCount>0)?true:false//viewHelper.chatActive
        onEnteredReleased:{
            console.log("Mainseatchat.qml | Exitchat | compchatsession.currentsession = "+compchatsession.currentsession)
            if(viewHelper.actualSessionCount>0){
                //                viewController.updateSystemPopupQ(22,true) //changed to screen popup
                viewController.showScreenPopup(22);

            }else{
                pif.seatChat.exitChatSession(compchatsession.currentsession);
            }

            //            pif.seatChat.exitChatSession(compchatsession.currentsession);
        }
    }

    SimpleNavButton{
        id:confernce_button
        anchors.right:close_button.left
        anchors.rightMargin: qsTranslate('','seatchat_navicon_btn_hgap')
        anchors.top:panelBG.top
        anchors.topMargin:qsTranslate('','seatchat_navicon_btn_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTag["conference_btn_n"];
        highlightimg:  viewHelper.configToolImagePath+configTag["conference_btn_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["conference_btn_p"];
        nav:[block_per_session_button,leaveBtn,close_button,chat_textBox]
        visible: (viewHelper.actualSessionCount>0)?true:false//viewHelper.chatActive
        onEnteredReleased:{
            viewHelper.cSessionID=compchatsession.currentsession
            core.log("viewHelper.cSessionID = "+viewHelper.cSessionID)
            core.log("compchatsession.currentsession = "+compchatsession.currentsession)
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedInviteMultipleInviteSeat",{isFromSeat:true,cSessionID:viewHelper.cSessionID})
                return;
            }
            viewController.showScreenPopup(12)
        }

    }

    SimpleNavButton{
        id:block_per_session_button
        anchors.right:confernce_button.left
        anchors.rightMargin: qsTranslate('','seatchat_navicon_btn_hgap')
        anchors.top:panelBG.top
        anchors.topMargin:qsTranslate('','seatchat_navicon_btn_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTag["block_btn_n"];
        highlightimg:  viewHelper.configToolImagePath+configTag["block_btn_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["block_btn_p"];
        nav:[(viewHelper.actualSessionCount>0)?compchatsession.sessionListView:"",leaveBtn,confernce_button,chat_textBox]
        visible: (viewHelper.actualSessionCount>0)?true:false//viewHelper.chatActive
        onEnteredReleased:{
            pif.seatChat.setCurrentSessionId(compchatsession.currentsession)
            var sessionIndex = viewHelper.searchEntryInListModel(pif.seatChat.sessionListModel,"session",compchatsession.currentsession);
            console.log("compchatsession.currentsession = "+compchatsession.currentsession)
            console.log("pif.seatChat.sessionListModel.count.get(sessionIndex).session = "+pif.seatChat.sessionListModel.get(sessionIndex).session)
            pif.seatChat.getBuddyListBySessionId(compchatsession.currentsession);
            var buddylen = pif.seatChat.sessionListModel.get(sessionIndex).participantCount
            if(buddylen==1){
                console.log("pif.seatChat.sessionListModel.count = "+buddylen)
                viewHelper.singleBlockSeat=pif.seatChat.sessionListModel.get(sessionIndex).participantSeatNo
                viewController.showScreenPopup(15)  //changed to screenPopup
                //                viewController.updateSystemPopupQ(14,true)
            }else{
                console.log("pif.seatChat.sessionListModel.count | else = "+buddylen)
                viewController.showScreenPopup(14)
            }
        }
        onNoItemFound:  {if(dir=="up" ){
                widgetList.focustTomainMenu()
            }
        }
    }


    SimpleNavBrdrBtn{    // newchat button
        id:newSeatchat
        focus: true
        property string textColor: leaveBtn.activeFocus?configTool.config.seatchat.chatroom.btn_text_h:(leaveBtn.isPressed?configTool.config.seatchat.chatroom.btn_text_p:configTool.config.seatchat.chatroom.btn_text_n)
        anchors.bottom: panelBG.bottom
        anchors.bottomMargin:qsTranslate('','seatchat_newchat_btn_bm')
        anchors.left: panelBG.left
        anchors.leftMargin:qsTranslate('','seatchat_newchat_btn_lm')
        width: parseInt(qsTranslate('','seatchat_newchat_btn_w'),10)
        //        height:parseInt(qsTranslate('','seatchat_newchat_btn_h'),10)
        btnTextWidth:parseInt(qsTranslate('',"seatchat_newchat_btn_w"),10);
        buttonText: [configTool.language.seatchat.new_chat,leaveBtn.textColor,qsTranslate('','seatchat_leave_unblock_btn_fontsize')]
        isHighlight: activeFocus
        //        isDisable:(pif.seatchat.sessionListModel.count==0)?true:false
        normalImg: viewHelper.configToolImagePath+configTag["btn_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["btn_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["btn_p"]
        normalBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
        highlightBorder: [qsTranslate('','seatchat_leave_unblock_btn_margin')]
        nav:["",viewHelper.actualSessionCount>0?compchatsession.sessionListView:textBox,(chat_text_box.isDisable)?newSeatchat:chat_textBox,""]
        onEnteredReleased: {console.log("MainSeatChatScreen.qml | newSeatChatbutton ");
            chat_textBox=""
            viewHelper.cSessionID=0
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                if((viewHelper.actualSessionCount+viewHelper.invitationSent)>=viewHelper.maxSessionCount){
                    viewController.showScreenPopup(21);
                    return;
                }
                widgetList.showVkb(false)
                pif.seatChat.getAllusers();
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinednewChatFromSeat",{isFromSeat:true,cSessionID:viewHelper.cSessionID})
                return;
            }
            core.log("viewHelper.actualSessionCount: "+viewHelper.actualSessionCount)
            core.log("viewHelper.maxSessionCount: "+viewHelper.maxSessionCount);
            core.log("viewHelper.invitationSent: "+viewHelper.invitationSent);
            if((viewHelper.actualSessionCount+viewHelper.invitationSent)>=viewHelper.maxSessionCount){
                core.log("viewHelper.actualSessionCount==============: "+viewHelper.actualSessionCount)
                core.log("viewHelper.maxSessionCount========: "+viewHelper.maxSessionCount);
                //                viewController.updateSystemPopupQ(21,true) // changed to screen popup
                viewController.showScreenPopup(21);
            }else{
                core.log("viewHelper.actualSessionCount:>>>>>>> "+viewHelper.actualSessionCount)
                core.log("viewHelper.maxSessionCount:>>>>>>>>>>>>> "+viewHelper.maxSessionCount);
                viewController.showScreenPopup(10)
            }
        }
    }

    CompChatScreen{
        id:compchatscren
        visible: (viewHelper.actualSessionCount>0)?true:false
        anchors.top:panelBG.top
        anchors.topMargin:  qsTranslate('','seatchat_chatbox_tm');
        anchors.left:panelBG.left
        anchors.leftMargin:  qsTranslate('','seatchat_chatbox_lm');
    }
    ChatSession{
        id:compchatsession
        anchors.top:sesion_txt.bottom
        anchors.left: sesion_txt.left
    }

    SimpleNavBrdrBtn{
        id:chat_text_box

        width: qsTranslate('','seatchat_mychat_input_w')
        height:parseInt(qsTranslate('','seatchat_mychat_input_h'),10) - parseInt(qsTranslate('','seatchat_mychat_input_txth'),10) + chat_textBox.height

        anchors.bottom: panelBG.bottom
        anchors.bottomMargin: qsTranslate('','seatchat_mychat_input_bm')
        anchors.left:panelBG.left
        anchors.leftMargin:  qsTranslate('','seatchat_mychat_input_lm');
        isHighlight: chat_textBox.activeFocus
        normalImg:(isDisable)?viewHelper.configToolImagePath+configTag["input_d"]:viewHelper.configToolImagePath+configTag["input_n"];
        highlightImg: viewHelper.configToolImagePath+configTag["input_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["input_p"];
        normalBorder: [qsTranslate('','seatchat_mychat_input_margin')]
        highlightBorder: [qsTranslate('','seatchat_mychat_input_margin')]
        isDisable:(viewHelper.actualSessionCount>0)?false:true
        nav:["",(compchatscren.scroller.visible)?compchatscren.scroller:"",chat_vkb,chat_textBox]

        onEnteredReleased:{console.log("nickName_text_box  ===== lanunch vkb")
            viewHelper.identifier="chat_msg"
            viewHelper.vkbInputData=chat_textBox.text
            core.log("onActionReleased | viewHelper.vkbInputData "+viewHelper.vkbInputData)
            widgetList.getVkbRef().launchVKB(viewHelper.identifier,chat_textBox.text)
        }

    }
    TextEdit{
        id:chat_textBox
        property int lineLimit :4
        property int txtCharLimit :150
        width: qsTranslate('','seatchat_mychat_input_txtw')
        height:chat_textBox.lineCount*parseInt(qsTranslate('','seatchat_mychat_input_txth'),10)//chat_textBox.lineCount*paintedHeight//paintedHeight
        color:"black"
        anchors.verticalCenter: chat_text_box.verticalCenter
        anchors.left:chat_text_box.left
        anchors.leftMargin:  qsTranslate('','seatchat_mychat_input_txtlm');
        font.pixelSize:  qsTranslate('','seatchat_mychat_input_fontsize')
        font.family:viewHelper.adultFont;
        cursorVisible:activeFocus
        activeFocusOnPress:false
        text:""
        wrapMode: TextEdit.Wrap;
        onActiveFocusChanged: {
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){  // CR changes
                viewHelper.identifier="chat_msg"
                viewHelper.vkbInputData=chat_textBox.text
                console.log("onActionReleased | viewHelper.vkbInputData "+viewHelper.vkbInputData)
                console.log("onActionReleased | viewHelper.identifier "+viewHelper.identifier)
                console.log("onActionReleased | chat_textBox.text "+chat_textBox.text)
                widgetList.getVkbRef().launchVKB(viewHelper.identifier,chat_textBox.text)
                chat_vkb=""
            }
        }

        onTextChanged: {
            if(chat_textBox.txtCharLimit >= 0){
                if(chat_textBox.text.length > txtCharLimit){
                    //                    if(chat_textBox.lineCount > lineLimit){
                    chat_textBox.text = chat_textBox.text.substring(0,chat_textBox.cursorPosition-1) + chat_textBox.text.substring(chat_textBox.cursorPosition,chat_textBox.text.length)
                    //                    chat_textBox.text = chat_textBox.text.substring(0,chat_textBox.cursorPosition-1)
                    cursorPosition=chat_textBox.text.length
                }
            }else return;
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("keyboardData",{keyboardData:chat_textBox.text});
            }
            //            }

        }
        Keys.onReleased: {
            switch(event.key){
            case Qt.Key_Return:
            case Qt.Key_Enter:{
                viewHelper.identifier="chat_msg"
                if(viewHelper.isValidSearchString(chat_textBox.text)){
                    viewHelper.sendChatMessage(chat_textBox.text);
                    chat_textBox.text = " ";
                }else  if(!chat_text_box.isDisable){
                    chat_textBox.forceActiveFocus();
                }
                event.accepted = true;
            }break;
            }
        }
        Keys.onPressed: {
            switch(event.key){

            case Qt.Key_Up:{
                if(cursorRectangle.y==0) close_button.forceActiveFocus()
                else event.accepted=false
            }break;
            case Qt.Key_Down:{
                if(cursorRectangle.y==0){
                    chat_textBox.forceActiveFocus()
                }
                else event.accepted=false
            }break;
            case Qt.Key_Left:{
                if(cursorRectangle.y==0) newSeatchat.forceActiveFocus()
                else event.accepted=false
            }break;
            case Qt.Key_Right:{
                chat_vkb.forceActiveFocus()
            }
            }
        }

        MouseArea{
            anchors.fill:parent;
            onClicked:{
                if(!chat_text_box.isDisable){
                    chat_textBox.forceActiveFocus();
                }else{
                    chat_textBox.text = " ";}
            }
        }

    }


    SimpleNavBrdrBtn{
        id:chat_vkb
        anchors.right: chat_text_box.right
        anchors.rightMargin:qsTranslate('','seatchat_mychat_vkb_rm')
        anchors.bottom: chat_text_box.bottom
        anchors.bottomMargin:qsTranslate('','seatchat_mychat_vkb_bm')
        //        anchors.verticalCenter:(chat_textBox.lineLimit==1)?chat_text_box.verticalCenter:undefined
        normalImg:(chat_text_box.isDisable)? viewHelper.configToolImagePath+configTag["vkb_d"]: viewHelper.configToolImagePath+configTag["vkb_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["vkb_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["vkb_p"]
        isHighlight: activeFocus
        isDisable:(viewHelper.actualSessionCount>0)?false:true
        nav: [chat_textBox,close_button,(send_button.isDisable)?"":send_button,""]

        onEnteredReleased: {
            viewHelper.identifier="chat_msg"
            viewHelper.vkbInputData=chat_textBox.text
            console.log("onActionReleased | viewHelper.vkbInputData "+viewHelper.vkbInputData)
            console.log("onActionReleased | viewHelper.identifier "+viewHelper.identifier)
            console.log("onActionReleased | chat_textBox.text "+chat_textBox.text)
            widgetList.getVkbRef().launchVKB(viewHelper.identifier,chat_textBox.text)
            chat_vkb=""
        }
    }

    SimpleNavBrdrBtn{
        id:send_button
        anchors.right: panelBG.right
        anchors.rightMargin:qsTranslate('','seatchat_chatsend_btn_rm')
        anchors.bottom: panelBG.bottom
        width:qsTranslate('','seatchat_chatsend_btn_w')
        anchors.bottomMargin:qsTranslate('','seatchat_chatsend_btn_bm')
        normalImg:viewHelper.configToolImagePath+configTag["btn_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["btn_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["btn_p"]
        buttonText: [configTool.language.seatchat.send,"white",qsTranslate('','seatchat_leave_unblock_btn_fontsize')]
        nav: [chat_textBox,close_button,"",""]
        isHighlight: activeFocus
        normalBorder: [qsTranslate('','seatchat_chatsend_btn_margin')]
        highlightBorder: [qsTranslate('','seatchat_chatsend_btn_margin')]
        isDisable:(chat_textBox.text.length > 0)?false:true
        visible: (chat_text_box.isDisable)?false:true
        onEnteredReleased: {
            viewHelper.sendChatMessage(chat_textBox.text);
            chat_textBox.text="";
            chat_textBox.forceActiveFocus();
        }

    }

}
