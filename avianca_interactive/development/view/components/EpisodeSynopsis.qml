// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
FocusScope {
    id:synopsisContentBtn
    anchors.fill: parent
    property alias synopsisContentFlickable: synopsisContentFlickable
    property alias synopsisDetailsText: synopsisDetailsText
    /**************************************************************************************************************************/
    onActiveFocusChanged: {
        if(!activeFocus)return
        if(!playBtn.focus && !resumeBtn.focus && !restartBtn.focus){
            if(playBtn.visible)  playBtn.focus=true;
            else if(restartBtn.visible)resumeBtn.focus=true
        }
    }
    /**************************************************************************************************************************/
    Flickable{
        id : synopsisContentFlickable
        width:qsTranslate('','synopsis_tv_desc1_w')
        height:qsTranslate('','synopsis_tv_desc1_h')
        contentWidth: width
        contentHeight: synopsisSubDescIMG.paintedHeight
        interactive: contentHeight>height
        anchors.top:parent.top
        anchors.topMargin:qsTranslate('','synopsis_tv_desc1_tm')
        anchors.left:parent.left
        anchors.leftMargin: qsTranslate('','synopsis_'+viewHelper.mapMenuTid(widgetList.getCidTidMainMenu()[1],true)+'_title_lm')
        clip:true
        ViewText{
            id:synopsisSubDescIMG
            width:parent.width
            height:parent.height
            varText:[text,configTool.config[currentTemplateNode].synopsis.description1_color,qsTranslate('','synopsis_tv_desc1_fontsize')]
            wrapMode: Text.WordWrap
            text:model.count>0?model.getValue(currentIndex,"description"):""

        }
    }
    SimpleBorderBtn{
        id:textOverlay
        visible:slider.visible?(slider.downArrow.isDisable?false:true):false
        width:synopsisContentFlickable.width+parseInt((qsTranslate('','common_scrollfade_extra_w'),10))
        normalBorder: [qsTranslate('','common_scrollfade_extra_margin')]
        anchors.bottom: synopsisContentFlickable.bottom
        anchors.horizontalCenter: synopsisContentFlickable.horizontalCenter
        source:  viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1],true)].synopsis.fading_image
    }
    ViewText{
        id:synopsisDetailsText
        width: qsTranslate('','synopsis_tv_desc3_w')
        height: qsTranslate('','synopsis_tv_desc3_h')
        anchors.top: parent.top
        anchors.topMargin:qsTranslate('','synopsis_tv_desc3_tm')
        anchors.left: parent.left
        anchors.leftMargin: qsTranslate('','synopsis_'+viewHelper.mapMenuTid(widgetList.getCidTidMainMenu()[1])+'_poster_lm')
        varText: [text,configTool.config[currentTemplateNode].synopsis.description2_color,qsTranslate('','synopsis_tv_desc3_fontsize')]
        text:synopsisDetails+((model.getValue(currentIndex,"duration")!="")?configTool.language.tv.min:"")
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
        maximumLineCount: 2
    }
    SimpleNavBrdrBtn{
        property string textColor: playBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(playBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
        id:playBtn
        visible: !widgetList.video.vodScreen.visible?!(pif.getMidInfo(model.getValue(currentIndex,"mid"),'elapseTime')>viewHelper.resumeSecs):true
        onVisibleChanged: {
            if(synopsisContentBtn.activeFocus){
                if(!visible) resumeBtn.forceActiveFocus()
                else playBtn.forceActiveFocus()
            }
            else {
                if(!visible) resumeBtn.focus=true
                else playBtn.focus=true
            }
        }
        width: qsTranslate('','synopsis_play_w')
        height: qsTranslate('','synopsis_play_h')
        anchors.right: parent.right
        anchors.rightMargin:qsTranslate('','synopsis_play_rm')
        anchors.bottom: parent.bottom
        anchors.bottomMargin: qsTranslate('','synopsis_play_bm')
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
        normalBorder: [qsTranslate('','synopsis_play_margin')]
        highlightBorder: [qsTranslate('','synopsis_play_margin')]
        nav:[playBtn,!slider.downArrow.isDisable?slider.downArrow:((!slider.upArrow.isDisable)?slider.upArrow:synopsisCloseBtn),playBtn,""]
        onPadReleased:  if(dir=="right" || dir=="left"){extremeEnds(dir)}
        onEnteredReleased:{
            viewHelper.playingCategory="tv";
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory})
            loadVod("play","movies")
        }
        SimpleButton{
            anchors.left: parent.left
            anchors.leftMargin:qsTranslate('','synopsis_play_icon_lm')
            anchors.verticalCenter: parent.verticalCenter
            isHighlight: playBtn.activeFocus
            normImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_n
            highlightimg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_h
            pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.play_icon_p
        }
        ViewText{
            anchors.left: playBtn.left
            anchors.leftMargin:qsTranslate('','synopsis_play_text_lm')
            anchors.verticalCenter: parent.verticalCenter
            width:qsTranslate('','synopsis_play_textw')
            varText: [text,playBtn.textColor,qsTranslate('','synopsis_play_fontsize')]
            text:configTool.language.movies.play
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    //Resume and Restart swapped for customer request//
    SimpleNavBrdrBtn{
        property string textColor: restartBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(restartBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
        id:restartBtn
        visible:  !playBtn.visible
        width: qsTranslate('','synopsis_resume_w')
        height: qsTranslate('','synopsis_resume_h')
        btnTextWidth:parseInt(qsTranslate('',"synopsis_resume_w"),10);
        buttonText: [configTool.language.movies.restart,restartBtn.textColor,qsTranslate('','synopsis_resume_fontsize')]
        anchors.right: resumeBtn.left
        anchors.rightMargin :qsTranslate('','synopsis_resume_gap')
        anchors.bottom: parent.bottom
        anchors.bottomMargin: qsTranslate('','synopsis_play_bm')
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
        normalBorder: [qsTranslate('','synopsis_resume_margin')]
        highlightBorder: [qsTranslate('','synopsis_resume_margin')]
        nav:[restartBtn,!slider.downArrow.isDisable?slider.downArrow:((!slider.upArrow.isDisable)?slider.upArrow:synopsisCloseBtn),resumeBtn,resumeBtn]
        onEnteredReleased:  {
            viewHelper.playingCategory="tv";
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory})
            loadVod("restart","movies")
        }
        onPadReleased:  if(dir=="left"){extremeEnds(dir)}
    }
    SimpleNavBrdrBtn{
        id:resumeBtn
        property string textColor: resumeBtn.activeFocus?configTool.config[currentTemplateNode].synopsis.btn_text_h:(resumeBtn.isPressed?configTool.config[currentTemplateNode].synopsis.btn_text_p:configTool.config[currentTemplateNode].synopsis.btn_text_n)
        visible: !playBtn.visible
        width: qsTranslate('','synopsis_resume_w')
        height: qsTranslate('','synopsis_resume_h')
        btnTextWidth:parseInt(qsTranslate('',"synopsis_resume_w"),10);
        buttonText: [configTool.language.movies.resume,resumeBtn.textColor,qsTranslate('','synopsis_resume_fontsize')]
        anchors.right: parent.right
        anchors.rightMargin:qsTranslate('','synopsis_play_rm')
        anchors.bottom: parent.bottom
        anchors.bottomMargin: qsTranslate('','synopsis_play_bm')
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_n
        highlightImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_h
        pressedImg: viewHelper.configToolImagePath+configTool.config[currentTemplateNode].synopsis.btn_p
        normalBorder: [qsTranslate('','synopsis_resume_margin')]
        highlightBorder: [qsTranslate('','synopsis_resume_margin')]
        nav:[restartBtn,!slider.downArrow.visible?slider.downArrow:(!slider.upArrow.visible)?slider.upArrow:synopsisCloseBtn,resumeBtn,""]
        onPadReleased: if(dir=="right"){extremeEnds("right")}
        onEnteredReleased:  {
            viewHelper.playingCategory="tv";
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory})
            loadVod("resume","movies")

        }
    }
}
