// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"


FocusScope {
    //    width: 1000
    //    height: 500
    property variant configTag:configTool.config.seatchat.terms_condition;
    //    color: "yellow"
    Connections{
        target:(visible)?viewHelper:null
        onSeatChatServiceBlock:{
            console.log("terms n conditions.qml | onSeatChatServiceBlock  = ")
            viewHelper.homeAction();
            viewController.updateSystemPopupQ(7,true);
        }
        onSeatchatKarmaActionPerform:{
            init()
        }
    }


    function init(){
        console.log(">>>seatchat terms ")
        viewHelper.setHelpTemplate("seatchat_tc");
        if(viewHelper.fromInvitePopup==true){
            if(viewHelper.getSeatChatTerms()){
                //                synopsisLoader.sourceComponent=blankComp
                synopsisLoader.sourceComponent=nickNamePage
            }else{
                //                synopsisLoader.sourceComponent=blankComp
                synopsisLoader.sourceComponent=seatChatTermsPage
            }
        }else{
        }

        acceptBtn.forceActiveFocus()
    }
    function backBtnPressed(){
        viewHelper.fromInvitePopup=false
        closeLoader()
    }
    function clearOnExit(){
        //        visible=false;
        //        synopsisLoader.sourceComponent=blankComp;
    }



    Item{
        id:posterobj
        anchors.fill: parent
        property alias staticBG: staticBG
        Image{
            id:listingBG
            source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
        }
        Image{
            id:staticBG
            anchors.bottom:parent.bottom
            source:viewHelper.configToolImagePath+configTool.config.seatchat.screen.seatcat_background
            MouseArea{
                anchors.fill:parent
                onClicked: {

                }
            }
        }
    }

    Image{
        id:panelBG
        anchors.horizontalCenter: posterobj.horizontalCenter
        anchors.top:posterobj.top;
        anchors.topMargin: qsTranslate('','seatchat_tc_tm')
        source:viewHelper.configToolImagePath+configTool.config.usb.screen.tc_panel
        MouseArea{
            anchors.fill:parent
            onClicked: {

            }
        }
    }

    ViewText{
        id:termsTitleObj
        anchors.horizontalCenter: panelBG.horizontalCenter
        anchors.top: panelBG.top;
        horizontalAlignment: Text.AlignHCenter
        anchors.topMargin:qsTranslate('',"usb_terms_title_tm");
        width:qsTranslate('',"usb_terms_title_w");
        height:qsTranslate('',"usb_terms_title_h");
        varText:[configTool.language.seatchat.terms_title,configTool.config.seatchat.terms_condition.title_color,qsTranslate('',"usb_terms_title_fontsize")]


    }
    Flickable{
        id : termsDescObjFlickable
        anchors.horizontalCenter: panelBG.horizontalCenter
        anchors.top: panelBG.top;
        anchors.topMargin:qsTranslate('',"usb_terms_desc_tm");
        width:qsTranslate('',"usb_terms_desc_w");
        height:qsTranslate('',"usb_terms_desc_h");
        contentWidth: width
        contentHeight: termsDescObj.paintedHeight
        clip:true
        ViewText{
            id:termsDescObj
            width:parent.width
            height:parent.height
            varText:[configTool.language.seatchat.terms_desc,configTag["description1_color"],qsTranslate('',"usb_terms_desc_fontsize")]
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
        }
    }


    FocusScope{
        anchors.fill: panelBG
        function getAcceptBtnRef(){return acceptBtn}
        function getRejectBtnRef(){return rejectBtn}
        function getScrollBarRef(){return seat_terms_Scroll}
        SimpleNavBrdrBtn{
            id:acceptBtn
            normalImg: viewHelper.configToolImagePath+configTool.config.seatchat.terms_condition.btn_n;
            highlightImg:viewHelper.configToolImagePath+configTool.config.seatchat.terms_condition.btn_h
            pressedImg:viewHelper.configToolImagePath+configTool.config.seatchat.terms_condition.btn_p
            height:qsTranslate('',"usb_terms_btn_h");
            width:qsTranslate('',"usb_terms_btn_w");
            btnTextWidth:parseInt(qsTranslate('',"usb_terms_btn_textw"),10);
            buttonText: [configTool.language.seatchat.seatchat_accept,(isPressed?configTool.config.seatchat.terms_condition.btn_text_p:isHighlight?configTool.config.seatchat.terms_condition.btn_text_h:configTool.config.seatchat.terms_condition.btn_text_n),qsTranslate('','usb_terms_btn_fontsize')]
            anchors.bottom:parent.bottom;
            anchors.bottomMargin:qsTranslate('',"usb_terms_btn_bm")
            anchors.left:parent.left;
            anchors.leftMargin:qsTranslate('',"usb_terms_btn_lm")
            normalBorder:[qsTranslate('',"usb_terms_btn_margin")]
            highlightBorder:[qsTranslate('',"usb_terms_btn_margin")]
            elide:Text.ElideRight
            nav: ["","","",""]
            onNoItemFound: {
                if(dir=="up"){if(seat_terms_Scroll.visible)seat_terms_Scroll.forceActiveFocus()
                else widgetList.focustTomainMenu()}
                else if(dir=="right"){rejectBtn.forceActiveFocus()}
            }
            onEnteredReleased:{
                viewHelper.toacceptInvitationPopup()
                viewHelper.setSeatChatTerms(true)
                synopsisLoader.sourceComponent=nickNamePage
            }
        }
        SimpleNavBrdrBtn{
            id:rejectBtn
            normalImg: viewHelper.configToolImagePath+configTool.config.seatchat.terms_condition.btn_n;
            highlightImg:viewHelper.configToolImagePath+configTool.config.seatchat.terms_condition.btn_h
            pressedImg:viewHelper.configToolImagePath+configTool.config.seatchat.terms_condition.btn_p
            height:qsTranslate('',"usb_terms_btn_h");
            width:qsTranslate('',"usb_terms_btn_w");
            btnTextWidth:parseInt(qsTranslate('',"usb_terms_btn_textw"),10);
            buttonText: [configTool.language.seatchat.seatchat_reject,(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','usb_terms_btn_fontsize')]
            anchors.bottom:parent.bottom;
            anchors.bottomMargin:qsTranslate('',"usb_terms_btn_bm")
            anchors.left:acceptBtn.right;
            anchors.leftMargin:qsTranslate('',"usb_terms_btn_gap")
            normalBorder:[qsTranslate('',"usb_terms_btn_margin")]
            highlightBorder:[qsTranslate('',"usb_terms_btn_margin")]
            elide:Text.ElideRight
            nav: ["","","",""]
            onNoItemFound: {
                if(dir=="up"){if(seat_terms_Scroll.visible)seat_terms_Scroll.forceActiveFocus()
                    else widgetList.focustTomainMenu()}
                else if(dir=="left"){acceptBtn.forceActiveFocus()}
            }
            onEnteredReleased:{
                if(viewHelper.fromInvitePopup){
                    core.debug( "In SeatChatPopups | SeatChat Invitation Pop up Decline")
                    core.debug("_______viewHelper.seatDataSession________: "+viewHelper.cSessionID)
                    core.debug("_______viewHelper.blockSeatArr1________: "+viewHelper.tSeatNo);
                    pif.seatChat.declineInvitation(viewHelper.cSessionID,viewHelper.tSeatNo);
                }
              seatchatScreen.backBtnPressed()
            }
        }
        Scroller{
            id:seat_terms_Scroll
            targetData:termsDescObjFlickable
            sliderbg:[qsTranslate('','usb_terms_scrollcont_base_margin'),qsTranslate('','usb_terms_scrollcont_w'),qsTranslate('','usb_terms_scrollcont_base_h'),configTag["scroll_base"]]
            scrollerDot:configTag["scroll_button"]
            upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
            downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
            anchors.right:parent.right;
            anchors.rightMargin:qsTranslate('','usb_terms_scrollcont_rm');
            anchors.verticalCenter: parent.verticalCenter
            height:qsTranslate('','usb_terms_scrollcont_h');
            width: qsTranslate('','usb_terms_scrollcont_w');
            onSliderNavigation:{
                if(direction=="down"){acceptBtn.forceActiveFocus()}
                else if(direction=="up") {widgetList.focustTomainMenu()}
            }
            onArrowPressed: {

            }
        }
    }

}
