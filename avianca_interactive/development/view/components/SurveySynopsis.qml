// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import ImageMask 1.0
FocusScope{
    anchors.fill: parent
    anchors.top:parent.top
    anchors.topMargin:qsTranslate('','synopsis_survey_panel_tm')
    property alias synopsisCloseBtn: synopsisCloseBtn
    property variant model:listingobj.compVpathListing.model
    property variant currentIndex: listingobj.compVpathListing.currentIndex
    property variant synopsisDetails: []
    property bool isSliderNavChanged: false
    property bool isSurveyBTN: false
    property bool isSurveyCategory: false
    property int currentMid: -1
    property bool skipOnlyOneSurvey: false
    signal extremeEnds(string direction)
    /**************************************************************************************************************************/
    onExtremeEnds:{
        if(direction=="left" && listingobj.compVpathListing.count>1){
            decrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="right" && listingobj.compVpathListing.count>1){
            incrementSelindex();synopsisAnim.restart()
        }
        else if(direction=="up"){
            widgetList.focustTomainMenu();
        }
    }
    Connections{
        target:(visible)?viewHelper:null
        onSurveyFromKarma: launchSurveyCategory(true)
    }
    /**************************************************************************************************************************/
    function init(){
         if(viewHelper.shortcutCalled){
             launchSurveyCategory(true)
             viewHelper.shortcutCalled=false
         }else{
             surveyInnerLoader.sourceComponent=blankCompSurvey
             surveyInnerLoader.sourceComponent=synopsisPage
         }
         synopsisCloseBtn.forceActiveFocus();
    }
    function clearOnExit(){
        isSurveyBTN=false
        isSurveyCategory=false
    }
    function forceFocus(){
        surveyInnerLoader.item.forceActiveFocus()
    }
    function launchSurveyCategory(param){
        console.log("launchSurveyCategory >>>>>>>>>>>>>>>>>>>>> = "+param +"getMediaInfo(category_attr_template_id) = "+getMediaInfo("category_attr_template_id"))
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
           pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":getMediaInfo("category_attr_template_id")/*,"showContOnKarma":viewHelper.showContOnKarma*/});
        }
        if(dataController.mediaServices.getCidBlockStatus(model.getValue(currentIndex,"cid"))){
            viewController.updateSystemPopupQ(7,true)
            return;
        }
        skipOnlyOneSurvey=param
        dataController.survey.getAllSurvey(surveyCategoryList);
    }
    function surveyCategoryList(){
        surveyInnerLoader.sourceComponent=surveyCategory
        surveyInnerLoader.item.synopsisContentFlickable.model=viewHelper.blankModel
        surveyInnerLoader.item.synopsisContentFlickable.model=dataController.survey.getSurveyModel()
        isSurveyCategory=true
        if(dataController.survey.getSurveyModel().count==1 && skipOnlyOneSurvey==true){
            viewHelper.surveyListingIndex=0;
            loadSurveyMCQ(dataController.survey.getSurveyModel().getValue(0,"surveyID"))
        }
        else if(dataController.survey.getSurveyModel().count==1 ){
            init()
        }

    }
    function getSynopsisDetails(){
    }
    function loadSurveyMCQ(surveyId){
        if(dataController.mediaServices.getCidBlockStatus(model.getValue(currentIndex,"cid"))){
            viewController.updateSystemPopupQ(7,true)
            return;
        }
        viewHelper.queNumber=viewHelper.lastSurveyVisited[surveyId]!=undefined?viewHelper.lastSurveyVisited[surveyId]:-1
        viewHelper.surveyId=surveyId
        viewHelper.surveyCategoryIndex=surveyInnerLoader.item.synopsisContentFlickable.currentIndex
        viewHelper.mediaListing=currentIndex
        viewController.loadNext(model,currentIndex)
    }
    function checkSurveyId(surveyId){
        return viewHelper.getSurveystatus(surveyId)
    }
    function backBtnPressed(){
        if(isSurveyCategory){
            surveyInnerLoader.sourceComponent=synopsisPage
            isSurveyCategory=false
            isSurveyBTN=false
        }
        else   closeLoader()
    }
    function handleExternally(){
        surveyCategoryList()
        surveyInnerLoader.item.synopsisContentFlickable.currentIndex= viewHelper.surveyCategoryIndex
        surveyInnerLoader.item.synopsisContentFlickable.forceActiveFocus()
        isSurveyBTN=true
    }
    function showError(){
        if( dataController.survey.getSurveyModel().count>0){
            if(surveyInnerLoader.sourceComponent==synopsisPage){
                surveyInnerLoader.item.continueBtn.visible=true
                errorMessage.visible=false
            }
            forceFocus()
        }
        else{
            if(surveyInnerLoader.sourceComponent==synopsisPage){
                surveyInnerLoader.item.continueBtn.visible=false
                errorMessage.visible=true
                viewHelper.setHelpTemplate("survey_synopsis_no_button")
            }
            synopsisCloseBtn.forceActiveFocus()
        }
    }
    /**************************************************************************************************************************/
    Image{
        id:synopsisIMG
        source:viewHelper.configToolImagePath+configTool.config.discover.synopsis.panel
        anchors.horizontalCenter: parent.horizontalCenter
    }
    SimpleNavButton{
        id:synopsisCloseBtn
        anchors.right: synopsisIMG.right
        anchors.rightMargin: qsTranslate('','synopsis_close_rm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_close_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTool.config.discover.synopsis.close_btn_n;
        highlightimg:  viewHelper.configToolImagePath+configTool.config.discover.synopsis.close_btn_h;
        pressedImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.close_btn_p;
        onEnteredReleased:{closeLoader();}
        onNoItemFound: {
            if(dir=="left" || dir=="right" || dir=="up")extremeEnds(dir)
            else if(dir=="down"){
                if(surveyInnerLoader.sourceComponent==synopsisPage){
                    if(!surveyInnerLoader.item.continueBtn.visible)return;
                }

                if(slider.visible){
                    if(slider.upArrow.isDisable)slider.downArrow.forceActiveFocus()
                    else slider.forceActiveFocus()
                }
                else forceFocus()
            }
        }
    }
    MaskedImage{
        id:posterIMGBtn
        visible:!isSurveyCategory
        width: qsTranslate('','synopsis_survey_poster_w')
        height: qsTranslate('','synopsis_survey_poster_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin: qsTranslate('','synopsis_survey_poster_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_survey_poster_tm')
        source: model.count>0?viewHelper.cMediaImagePath+model.getValue(currentIndex,"poster"):""
        maskSource:viewController.settings.assetImagePath+qsTranslate('','listing_games_list_source');

    }
    ViewText{
        id:synopsisTitleBtn
        width:isSurveyCategory?qsTranslate('','synopsis_survey_title_w1'):qsTranslate('','synopsis_survey_title_w')
        height:qsTranslate('','synopsis_survey_title_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin:isSurveyCategory?qsTranslate('','synopsis_survey_title_lm1'):qsTranslate('','synopsis_survey_title_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_survey_title_tm')
        varText: [text,configTool.config.discover.synopsis.title_color,qsTranslate('','synopsis_survey_title_fontsize')]
        text:model?(model.count>0?model.getValue(currentIndex,"title"):""):""
        elide: Text.ElideRight
        maximumLineCount: 2
        wrapMode: Text.Wrap

    }
    Scroller{
        id:slider
        anchors.top:synopsisIMG.top; anchors.topMargin: isSurveyCategory?qsTranslate('','synopsis_surveylist_slider_tm'):qsTranslate('','synopsis_surveydesc_slider_tm')
        anchors.right: synopsisIMG.right ;anchors.rightMargin: isSurveyCategory?qsTranslate('','synopsis_surveylist_slider_rm'):qsTranslate('','synopsis_surveydesc_slider_rm')
        width: isSurveyCategory?qsTranslate('','synopsis_surveylist_slider_w'):qsTranslate('','synopsis_surveydesc_slider_w')
        height: isSurveyCategory?qsTranslate('','synopsis_surveylist_slider_h'):qsTranslate('','synopsis_surveydesc_slider_h')
        targetData:surveyInnerLoader.sourceComponent!=blankCompSurvey?surveyInnerLoader.item.synopsisContentFlickable:null
        sliderbg:isSurveyCategory?[qsTranslate('','synopsis_surveylist_slider_base_margin'),qsTranslate('','synopsis_surveylist_slider_w'),qsTranslate('','synopsis_surveylist_slider_base_h'),configTool.config.discover.synopsis.scroll_base,]:
                                   [qsTranslate('','synopsis_surveydesc_slider_base_margin'),qsTranslate('','synopsis_surveydesc_slider_w'),qsTranslate('','synopsis_surveydesc_slider_base_h'),configTool.config.discover.synopsis.scroll_base,]
        scrollerDot:configTool.config.games.synopsis.scroll_button
        downArrowDetails:  [configTool.config.discover.synopsis.dn_arrow_n,configTool.config.discover.synopsis.dn_arrow_h,configTool.config.discover.synopsis.dn_arrow_p]
        upArrowDetails: [configTool.config.discover.synopsis.up_arrow_n,configTool.config.discover.synopsis.up_arrow_h,configTool.config.discover.synopsis.up_arrow_p]
        visible:surveyInnerLoader.sourceComponent!=blankCompSurvey? surveyInnerLoader.item.synopsisContentFlickable.contentHeight>surveyInnerLoader.item.synopsisContentFlickable.height:false
        onSliderNavigation: {
            if(direction=="down" ){
                if(surveyInnerLoader.item.continueBtn.visible)
                    surveyInnerLoader.item.continueBtn.forceActiveFocus()
            }
            else if(direction=="up")
                synopsisCloseBtn.forceActiveFocus()
            else if(direction=="right" || direction=="left"){
                isSliderNavChanged=true
                extremeEnds(direction)
            }
        }
    }
    Component{id:blankCompSurvey;Item{}}
    Loader{
        id:surveyInnerLoader
        anchors.fill: synopsisIMG
        sourceComponent:blankCompSurvey
        onStatusChanged: {
            if(surveyInnerLoader.sourceComponent==blankCompSurvey)return;
            if (surveyInnerLoader.status == Loader.Ready)
                dataController.survey.getAllSurvey(showError)
        }
    }
    Component{
        id:synopsisPage
        FocusScope{
            anchors.fill: parent
            property alias continueBtn: continueBtn
            property alias synopsisContentFlickable: synopsisContentFlickable
            Flickable{
                id : synopsisContentFlickable
                width:qsTranslate('','synopsis_survey_desc1_w')
                height:qsTranslate('','synopsis_survey_desc1_h')
                contentWidth: width
                contentHeight: synopsisSubDescIMG.paintedHeight
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','synopsis_survey_desc1_tm')
                anchors.left:parent.left
                anchors.leftMargin: qsTranslate('','synopsis_survey_title_lm')
                clip:true
                ViewText{
                    id:synopsisSubDescIMG
                    width:parent.width
                    height:parent.height
                    varText:[text,configTool.config.discover.synopsis.description1_color, qsTranslate('','synopsis_survey_desc1_fontsize')]
                    wrapMode: Text.WordWrap
                    text:model?(model.count>0?model.getValue(currentIndex,"description"):""):""
                }
            }
            SimpleNavBrdrBtn{
                id:continueBtn
                focus: true
                property string textColor: continueBtn.activeFocus?configTool.config.discover.synopsis.btn_text_h:(continueBtn.isPressed?configTool.config.discover.synopsis.btn_text_p:configTool.config.discover.synopsis.btn_text_n)
                visible: false
                anchors.bottom:parent.bottom
                anchors.bottomMargin: qsTranslate('','synopsis_survey_play_bm')
                anchors.right: parent.right
                anchors.rightMargin: qsTranslate('','synopsis_survey_play_rm')
                width: qsTranslate('','synopsis_survey_play_w')
                height: qsTranslate('','synopsis_survey_play_h')
                btnTextWidth:parseInt(qsTranslate('',"synopsis_survey_play_w"),10);
                buttonText: [configTool.language.global_elements.cont,continueBtn.textColor,qsTranslate('','synopsis_survey_play_fontsize')]
                isHighlight: activeFocus
                normalImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_n
                highlightImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_h
                pressedImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_p
                normalBorder: [qsTranslate('','synopsis_survey_play_margin')]
                highlightBorder: [qsTranslate('','synopsis_survey_play_margin')]
                onEnteredReleased: {launchSurveyCategory(true)}
                onNoItemFound: {
                    if(dir=="up"){
                        if(slider.visible){
                            if(slider.upArrow.isDisable)
                                slider.downArrow.forceActiveFocus()
                            else
                                slider.forceActiveFocus()
                        }
                        else synopsisCloseBtn.forceActiveFocus()
                    } else if(dir=="left" || dir=="right") extremeEnds(dir)
                }
            }
        }
    }
    Component{
        id:surveyCategory
        FocusScope{
            anchors.fill: parent
            property alias synopsisContentFlickable: synopsisContentFlickable
            Rectangle{
                id:divline
                anchors.bottom: synopsisContentFlickable.top
                anchors.bottomMargin:     qsTranslate("",'episode_episode_high_bm')
                anchors.left: synopsisContentFlickable.left
                width: synopsisContentFlickable.width
                height: qsTranslate("",'common_line_h')
                color: configTool.config.tv.synopsis.description1_color
            }
            VerticalView{
                id:synopsisContentFlickable
                focus: true
                width: qsTranslate('','synopsis_surveylistview_w')
                height: qsTranslate('','synopsis_surveylistview_h')
                delegate: surveyCategoryDelegate
                anchors.left: parent.left
                anchors.leftMargin: qsTranslate('','synopsis_survey_title_lm1')
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','synopsis_surveylistview_tm')
                Keys.onRightPressed: {
                    if(slider.visible){
                        if(slider.downArrow.isDisable)slider.upArrow.focus=true
                        if(slider.upArrow.isDisable)  slider.downArrow.forceActiveFocus()
                        else slider.forceActiveFocus()
                    }
                    else synopsisCloseBtn.forceActiveFocus()
                }
                Keys.onUpPressed: {
                    if( synopsisContentFlickable.currentIndex!=0) {
                        if(isSurveyBTN) isSurveyBTN=false
                        decrementCurrentIndex()
                        synopsisContentFlickable.forceActiveFocus()
                    }
                    else widgetList.focustTomainMenu()
                }
                Keys.onDownPressed:  {
                    if(synopsisContentFlickable.currentIndex!=(count-1)) {
                        if(isSurveyBTN )isSurveyBTN=false
                        incrementCurrentIndex()
                    }
                }
            }
        }
    }
    Component{
        id:surveyCategoryDelegate
        Item{
            width: qsTranslate('','synopsis_surveylist_w')
            height: surveyDescription.visible?(highlightIMg.height+divline.height+parseInt(qsTranslate("",'synopsis_surveylist_high_bm'),10)): qsTranslate('','synopsis_surveylist_h')
            onActiveFocusChanged: if(activeFocus)highlightIMg.forceActiveFocus()
            SimpleBorderBtn{
                id:highlightIMg
                anchors.bottom: divline.top
                width: qsTranslate('','synopsis_surveylist_high_w')
                height: surveyDescription.visible?(surveyDescription.height+surveyBTN.height+surveyTitle.height+parseInt(qsTranslate('','synopsis_surveylisttitle_tm'),10)+parseInt(qsTranslate('','synopsis_surveylist_desc1_tm'),10)+parseInt(qsTranslate("",'synopsis_surveylist_btn_tm'),10)):parseInt(qsTranslate('','synopsis_surveylist_high_h'),10)
                isHighlight: activeFocus
                normalImg:   activeFocus && !isSurveyBTN?viewHelper.configToolImagePath+configTool.config.tv.synopsis.listing_highlight:""
                onEnteredReleased: {
                    surveyInnerLoader.item.synopsisContentFlickable.currentIndex=index
                    highlightIMg.forceActiveFocus()
                    isSurveyBTN=!isSurveyBTN
                }
                ViewText{
                    id:surveyTitle
                    width:  qsTranslate('','synopsis_surveylisttitle_w')
                    height: qsTranslate('','synopsis_surveylisttitle_h')
                    anchors.top:parent.top
                    anchors.left: parent.left
                    anchors.topMargin: qsTranslate('','synopsis_surveylisttitle_tm')
                    anchors.leftMargin:  qsTranslate('','synopsis_surveylisttitle_lm')
                    text:dataController.survey.getSurveyModel().count>0?dataController.survey.getSurveyModel().getValue(index,"surveyTitle"):""
                    varText: [text,configTool.config.discover.synopsis.description1_color,qsTranslate('','synopsis_surveylisttitle_fontsize')]
                }
                ViewText{
                    id:surveyDescription
                    visible: surveyInnerLoader.item.synopsisContentFlickable.currentIndex==index && isSurveyBTN
                    anchors.top:surveyTitle.bottom
                    anchors.topMargin: qsTranslate('','synopsis_surveylist_desc1_tm')
                    anchors.left: surveyTitle.left
                    width: qsTranslate('','synopsis_surveylist_desc1_w')
                    height: paintedHeight
                    text:dataController.survey.getSurveyModel().count>0?dataController.survey.getSurveyModel().getValue(index,"longDescription"):""
                    varText: [text,configTool.config.discover.synopsis.description1_color,qsTranslate('','episode_episode_desc1_fontsize')]
                    wrapMode: Text.Wrap
                    onVisibleChanged:  if(visible)surveyBTN.forceActiveFocus()
                }
                SimpleNavBrdrBtn{
                    id:surveyBTN
                    property string textColor: surveyBTN.activeFocus?configTool.config.discover.synopsis.btn_text_h:(surveyBTN.isPressed?configTool.config.discover.synopsis.btn_text_p:configTool.config.discover.synopsis.btn_text_n)
                    visible: surveyDescription.visible
                    width: qsTranslate('','synopsis_surveylist_btn_w')
                    height: qsTranslate('','synopsis_surveylist_btn_h')
                    anchors.top:surveyDescription.bottom
                    anchors.topMargin: qsTranslate('','synopsis_surveylist_btn_tm')
                    anchors.left: parent.left
                    anchors.leftMargin: qsTranslate('','synopsis_surveylist_btn_lm')
                    btnTextWidth:parseInt(qsTranslate('',"synopsis_surveylist_btn_textw"),10);
                    buttonText: [configTool.language.survey.take_survey,surveyBTN.textColor,qsTranslate('','synopsis_surveylist_btn_fontsize')]
                    isHighlight: activeFocus
                    normalImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_n
                    highlightImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_h
                    pressedImg: viewHelper.configToolImagePath+configTool.config.discover.synopsis.btn_p
                    normalBorder: [qsTranslate('','synopsis_surveylist_btn_margin')]
                    highlightBorder: [qsTranslate('','synopsis_surveylist_btn_margin')]
                    onEnteredReleased:  {
                        viewHelper.surveyListingIndex=surveyInnerLoader.item.synopsisContentFlickable.currentIndex
                        loadSurveyMCQ(dataController.survey.getSurveyModel().getValue(surveyInnerLoader.item.synopsisContentFlickable.currentIndex,"surveyID"))
                    }
                }
            }
            Rectangle{
                id:divline
                anchors.bottom: parent.bottom
                anchors.bottomMargin:   qsTranslate("",'synopsis_surveylist_high_bm')
                width: parent.width
                height: qsTranslate("",'common_line_h')
                color: configTool.config.tv.synopsis.description1_color
                opacity: qsTranslate('','common_disabled_opacity')
            }
        }
    }
    ViewText{
        id:errorMessage
        width:qsTranslate('','synopsis_note_w')
        height:qsTranslate('','synopsis_note_h')
        anchors.left:synopsisIMG.left
        anchors.leftMargin:qsTranslate('','synopsis_note_lm')
        anchors.top:synopsisIMG.top
        anchors.topMargin:qsTranslate('','synopsis_note_tm')
        varText: [text,configTool.config.discover.synopsis.cg_info_text_color,qsTranslate('','synopsis_note_fontsize')]
        wrapMode: Text.Wrap
        elide: Text.ElideRight
        text:configTool.language.connecting_gate.cg_info_notavailable
    }
}
