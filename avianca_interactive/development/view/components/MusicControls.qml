import QtQuick 1.1
import "../../framework/components"

FocusScope{
    property string controlBg:controlBg.source;
    property string tmpId:current.template_id
    property variant configVal:core.configTool.config.music.tracklist;
    property variant configTag:configTool.config.usb.screen;//for usb
    property variant upFocus:[];
    property variant section:(viewHelper.usbTrackPlayed)?pif.usbMp3:pif.aod
    property alias nowPlayW: nowplaying.width
    property alias playNpause: playNpause
    property alias progressbar: progressbar
    property bool noButton:false;
    signal extremeEnds(string direction)

    /**************************************************************************************************************************/
    function specialFocus(dir){
        if(dir=="left"){
            playNpause.forceActiveFocus();
        }
    }
    /**************************************************************************************************************************/
    Image{
        id:controlBg;
        anchors.fill:parent;
    }
    ViewText{
        id:nowplaying
        varText:[labelTag["now_playing"]+" : "+viewHelper.audioPlayer.midTitle,configVal["description1_color"],(tmpId=="playlist")?qsTranslate('','playlist_nowplay_text_fontsize'):qsTranslate('','tracklist_nowplay_text_fontsize')]
        x:(tmpId=="usb_music")?qsTranslate('','usb_nowplay_text_lm'):(tmpId=="playlist" && noButton)?qsTranslate('','playlist_nowplay_text_lm'):qsTranslate('','tracklist_nowplay_text_lm');
        height:(tmpId=="usb_music")?qsTranslate('','usb_nowplay_text_h'):(tmpId=="playlist" && noButton)?qsTranslate('','playlist_nowplay_text_h'):qsTranslate('','tracklist_nowplay_text_h');
        width:(appearance=="tracklist_")?(tmpId=="usb_music")?qsTranslate('','usb_nowplay_text_w'):qsTranslate('','tracklist_nowplay_text_w'):(getCidTidForSubCat()[1]=="genre_listing"?qsTranslate('','tracklist_nowplay_text_w'):qsTranslate('','tracklist_nowplay_text_w1'));
        visible:(getCidTidForSubCat()[1]!="playlist"  && !(viewHelper.trackStop))
        elide:Text.ElideRight;
        maximumLineCount:1;
        wrapMode: "WordWrap"


    }
    Row{
        spacing:(tmpId=="usb_music")?qsTranslate('','usb_Controller_gap'):qsTranslate('','tracklist_controls_hgap')
        anchors.top:nowplaying.bottom;
        x:(tmpId=="usb_music")?qsTranslate('','usb_Controller_lm'):0
        Item{
            width:(tmpId=="usb_music")?qsTranslate('','usb_Controller_w'):qsTranslate('','tracklist_controls_w');
            height:(tmpId=="usb_music")?qsTranslate('','usb_Controller_h'):qsTranslate('','tracklist_controls_h');
            ToggleNavButton{
                id:playNpause
                normImg1:viewHelper.configToolImagePath+configVal["pause_button_n"];
                normImg2:viewHelper.configToolImagePath+configVal["play_button_n"];
                highImg1:viewHelper.configToolImagePath+configVal["pause_button_h"];
                highImg2:viewHelper.configToolImagePath+configVal["play_button_h"];
                pressImg1: viewHelper.configToolImagePath+configVal["pause_button_p"];
                pressImg2: viewHelper.configToolImagePath+configVal["play_button_p"];
                anchors.centerIn:parent;
                isHighlight: activeFocus && !isPressed?true:false
                isDisable:(viewHelper.trackStop)
                increaseTouchArea:true;
                increaseValue:parent.width/4;
                focus:true;
                nav:["",upFocus[0],prev,""];
                onNoItemFound:{
                    if(dir=="left"){
                        if(getCidTidForSubCat()[1]=="artist_listing"){
                            customLeft();
                        }else if(getCidTidForSubCat()[1]=="music_listing"){
                            extremeEnds(dir)
                        }
                    }
                }
                cstate:(!viewHelper.trackStop)?(section.getMediaState()==section.cMEDIA_PLAY):false;
                onEnteredReleased:{
                    if(cstate){
                        section.pause()
                    }else{
                        section.resume()
                    }
                }
            }


        }
        Item{
            width:(tmpId=="usb_music")?qsTranslate('','usb_Controller_w'):qsTranslate('','tracklist_controls_w')
            height:(tmpId=="usb_music")?qsTranslate('','usb_Controller_h'):qsTranslate('','tracklist_controls_h')
            SimpleNavButton{
                id:prev

                normImg:viewHelper.configToolImagePath+configVal["prev_button_n"]
                highlightimg:viewHelper.configToolImagePath+configVal["prev_button_h"]
                pressedImg:viewHelper.configToolImagePath+configVal["prev_button_p"]
                anchors.centerIn:parent;
                isDisable:(viewHelper.trackStop)
                increaseTouchArea:true;
                increaseValue:parent.width/4;
                nav:[playNpause,upFocus[0],next,""];
                onEnteredReleased:{
                    if(viewHelper.genreSongIndex>=0 && section.getMediaElapseTime()<=3){
                        viewHelper.playedFromGenre((viewHelper.genreSongIndex-1))
                    }else{
                        section.playPrevious()

                    }
                }
            }
        }
        Item{
            width:(tmpId=="usb_music")?qsTranslate('','usb_Controller_w'):qsTranslate('','tracklist_controls_w')
            height:(tmpId=="usb_music")?qsTranslate('','usb_Controller_h'):qsTranslate('','tracklist_controls_h')
            SimpleNavButton{
                id:next
                increaseTouchArea: true
                normImg:viewHelper.configToolImagePath+configVal["next_button_n"];
                highlightimg:viewHelper.configToolImagePath+configVal["next_button_h"];
                pressedImg:viewHelper.configToolImagePath+configVal["next_button_p"];
                isDisable:(viewHelper.trackStop)
                anchors.centerIn:parent;
                increaseValue:parent.width/4;
                nav:[prev,upFocus[0],repeatOnOff,""];
                onEnteredReleased:{
                    if(viewHelper.genreSongIndex>=0){
                        viewHelper.playedFromGenre((viewHelper.genreSongIndex+1))
                    }else{
                        section.playNext()
                    }
                }
            }
        }
    }
    Slider{
        id:progressbar
        enableDragging:!(viewHelper.trackStop);
        sliderbg:(tmpId=="usb_music")?configTag["base_image"]:configVal["base_image"]
        sliderFillingtItems:(tmpId=="usb_music")?[configTag["fill_image"],configTag["indicator"]]:[configVal["fill_image"],configVal["indicator"]]
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.bottom: parent.bottom
        anchors.bottomMargin: qsTranslate('','tracklist_progressbar_bm')
        height:(tmpId=="usb_music")?qsTranslate('','usb_progressbar_h'):qsTranslate('','tracklist_progressbar_h')
        width:(tmpId=="usb_music")?qsTranslate('','usb_progressbar_w'):qsTranslate('','tracklist_progressbar_w')
        stepsValue:1
        isFill: true
        maxLevel:section.getMediaEndTime();
        onValueChanged:{
            var count=Math.round(sliderDotImg.anchors.leftMargin/progressbar.shiftingPerClick)
            if(progressbar.stepsValue*count>=(maxLevel-3)){
                section.setMediaPosition((maxLevel-3))
            }else{
                section.setMediaPosition(progressbar.stepsValue*count)
            }
        }
        onDragingOn: {
            var elapseTime=section.getMediaEndTime()-shiftingValueForDragging
            elapsedTime.text=core.coreHelper.getTimeFormatFromSecs(elapseTime, 6)
        }
    }
    ViewText{
        id:elapsedTime;
        anchors.left:progressbar.left;
        anchors.top:progressbar.bottom;
        anchors.topMargin:(tmpId=="usb_music")?qsTranslate('','usb_progressbar_time_tm'):qsTranslate('','tracklist_time_tm');
        height:(tmpId=="usb_music")?qsTranslate('','usb_progressbar_time_h'):qsTranslate('','tracklist_time_h');
        width:(tmpId=="usb_music")?qsTranslate('','usb_progressbar_time_w'):qsTranslate('','tracklist_time_w');
        varText:[section.__mediaElapseText,configVal["description1_color"],qsTranslate('','tracklist_time_fontsize')]
        visible:!(viewHelper.trackStop)
    }
    ViewText{
        id:remainTime;
        anchors.right:progressbar.right;
        anchors.top:progressbar.bottom;
        anchors.topMargin:qsTranslate('','tracklist_time_tm');
        height:(tmpId=="usb_music")?qsTranslate('','usb_progressbar_time_h'):qsTranslate('','tracklist_time_h');
        width:(tmpId=="usb_music")?qsTranslate('','usb_progressbar_time_w'):qsTranslate('','tracklist_time_w');
        horizontalAlignment:Text.AlignRight
        varText:["- "+(progressbar.isDragged? core.coreHelper.convertSecToMSLeadZero(progressbar.remainingValue):section.__mediaRemainText),configVal["description1_color"],qsTranslate('','tracklist_time_fontsize')]
        visible:!(viewHelper.trackStop)
    }
    Row{
        anchors.right:parent.right
        anchors.rightMargin: (tmpId=="usb_music")?qsTranslate('','usb_progressbar_gap'):0
        anchors.top:nowplaying.bottom;
        spacing:(tmpId=="usb_music")?qsTranslate('','usb_Controller_gap'):qsTranslate('','tracklist_controls_hgap')
        Item{
            width:(tmpId=="usb_music")?qsTranslate('','usb_Controller_w'):qsTranslate('','tracklist_controls_w')
            height:(tmpId=="usb_music")?qsTranslate('','usb_Controller_h'):qsTranslate('','tracklist_controls_h')
            ToggleNavButton{
                id:repeatOnOff
                normImg1:viewHelper.configToolImagePath+configVal["repeat_on_n"];
                normImg2:viewHelper.configToolImagePath+configVal["repeat_off_n"];
                highImg1:viewHelper.configToolImagePath+configVal["repeat_on_h"];
                highImg2:viewHelper.configToolImagePath+configVal["repeat_off_h"];
                pressImg1: viewHelper.configToolImagePath+configVal["repeat_on_p"];
                pressImg2: viewHelper.configToolImagePath+configVal["repeat_off_p"];
                isHighlight: activeFocus && !isPressed?true:false
                anchors.centerIn:parent;
                increaseTouchArea:true;
                increaseValue:parent.width/4;
                isDisable:(viewHelper.trackStop)
                nav:[next,upFocus[1],shuffleOnOff,""];
                cstate:viewHelper.userRepeatOn?(section.getMediaRepeat()==1):false;
                selectedImg1:pressImg1;
                selectedImg2:pressImg2;
                isSelected:cstate
                onEnteredReleased:{
                    if(!viewHelper.userRepeatOn){
                        section.setMediaRepeat(0)
                        section.setMediaRepeat(1)
                    }
                    else{
                        if(cstate){
                            section.setMediaRepeat(0);

                        }else{
                            section.setMediaRepeat(1);
                        }
                    }
                    if(section.getMediaRepeat()==0)
                        viewHelper.userRepeatOn=false
                    else
                        viewHelper.userRepeatOn=true

                }
            }
        }
        Item{
            width:(tmpId=="usb_music")?qsTranslate('','usb_Controller_w'):qsTranslate('','tracklist_controls_w')
            height:(tmpId=="usb_music")?qsTranslate('','usb_Controller_h'):qsTranslate('','tracklist_controls_h')
            ToggleNavButton{
                id:shuffleOnOff
                normImg1:viewHelper.configToolImagePath+configVal["shuffle_on_n"];
                normImg2:viewHelper.configToolImagePath+configVal["shuffle_off_n"];

                highImg1:viewHelper.configToolImagePath+configVal["shuffle_on_h"];
                highImg2:viewHelper.configToolImagePath+configVal["shuffle_off_h"];

                pressImg1: viewHelper.configToolImagePath+configVal["shuffle_on_p"];
                pressImg2: viewHelper.configToolImagePath+configVal["shuffle_off_p"];

                anchors.centerIn:parent;
                increaseTouchArea:true;
                isHighlight: activeFocus && !isPressed?true:false
                increaseValue:parent.width/4;
                isDisable:(viewHelper.trackStop)
                nav:[repeatOnOff,upFocus[1],volume,""];
                cstate:(section.getMediaState()!=section.cMEDIA_STOP)?(!section.getMediaShuffle()):false;
                selectedImg1:pressImg1;
                selectedImg2:pressImg2;
                isSelected:cstate
                onEnteredReleased:{
                    if(cstate){
                        if(viewHelper.genreSongIndex>=0){
                            viewHelper.shuffleGenreModel(false);
                        }
                        section.setMediaShuffle(false);

                    }else{
                        if(viewHelper.genreSongIndex>=0){
                            //its a shuffle from genreInit
                            viewHelper.shuffleGenreModel(true);
                        }
                        section.setMediaShuffle(true);

                    }
                }
            }
        }
        Item{
            width:(tmpId=="usb_music")?qsTranslate('','usb_Controller_w'):qsTranslate('','tracklist_controls_w')
            height:(tmpId=="usb_music")?qsTranslate('','usb_Controller_h'):qsTranslate('','tracklist_controls_h')
            x:(tmpId=="usb_music")?qsTranslate('','usb_Controller_rm'):0
            SimpleNavButton{
                id:volume
                normImg:viewHelper.configToolImagePath+configVal["volume_button_n"]
                highlightimg:viewHelper.configToolImagePath+configVal["volume_button_h"]
                pressedImg:viewHelper.configToolImagePath+configVal["volume_button_p"]
                anchors.centerIn:parent;
                increaseTouchArea:true;
                increaseValue:parent.width/4;
                isDisable:(viewHelper.trackStop)
                nav:[shuffleOnOff,upFocus[1],"",""];
                onEnteredReleased:{
                    widgetList.showHandsetBar(false)
                    widgetList.showVolumeBar(true)
                }
                onNoItemFound:{
                    if(dir=="right"){
                        extremeEnds(dir)
                    }
                }
            }
            Connections{
                target:visible?(widgetList.getVolumebarRef().visible?widgetList.getVolumebarRef():null):null
                onNavigation:{
                    if(dir!="down")
                        widgetList.showVolumeBar(false)
                    if(dir=="left"){
                        shuffleOnOff.forceActiveFocus();
                    }else if(dir=="right"){
                        volume.forceActiveFocus();
                    }else if(dir=="up"){
                        upFocus[1].forceActiveFocus()
                    }
                }
            }
        }
        Connections{
            target:(visible)?viewHelper:null;
            onCustomHandsetSignal:{
                if(hstStr=="fwd"){
                    if(viewHelper.trackStop)return;
                    if(visible)
                        next.forceActiveFocus();

                }else if(hstStr=="rwd"){
                    if(viewHelper.trackStop)return;
                    if(visible)
                        prev.forceActiveFocus();
                }
            }
        }

        Connections{
            target:section;
            onSigMediaElapsed:{
                if(progressbar.isDragged)return;
                elapsedTime.text  = elapseText;
                remainTime.text = remainText;
                progressbar.externalCalc(elapseTime,endTime)
            }
            onSigMidPlayBegan:{
                if(viewHelper.trackStop)return;
                if(viewHelper.aodHandsetControls){
                    viewHelper.aodHandsetControls=false
                    if(parent.visible)playNpause.forceActiveFocus();
                }
                if(section.getMediaState()==section.cMEDIA_PLAY){
                    playNpause.cstate=true;
                }
            }
            onSigMediaShuffleState:{
                if(section.getMediaShuffle() ){
                    section.setMediaRepeat(1)
                }
                shuffleOnOff.cstate=shuffle;
            }
            onSigMediaPaused:{
                if(viewHelper.trackStop)return;
                playNpause.cstate=false;
                if(viewHelper.aodHandsetControls){
                    viewHelper.aodHandsetControls=false
                    if(parent.visible)playNpause.forceActiveFocus();
                }
            }
            onSigMediaForward:{
            }
            onSigMediaRewind:{
            }
            onSigMediaRepeatState:{
                // repeatOnOff.cstate=repeatAll;
            }
            onSigAggPlayStopped:{
                upFocus[3].forceActiveFocus();
                progressbar.resetSlider()
            }

        }
    }
}
