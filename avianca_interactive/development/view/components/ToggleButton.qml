import QtQuick 1.0

SimpleButton {
    property bool cstate: false;
    property bool externalCstate: false;
    property string normImg1:"";
    property string normImg2:"";
    property string highImg1:"";
    property string highImg2:"";
    property string pressImg1:"";
    property string pressImg2:"";
    normImg:(cstate)?normImg1:normImg2;
    highlightimg:(cstate)?highImg1:highImg2;
    pressedImg:(cstate)?pressImg1:pressImg2;
    onActionReleased:{
        if(externalCstate)return;
        enteredReleased();
        if(cstate){
            cstate=false;
        }else{
            cstate=true;
        }

    }
}
