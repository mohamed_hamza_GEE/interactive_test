// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

BorderImage {
    id: base
    horizontalTileMode :isHorizantalTileReq?BorderImage.Repeat:BorderImage.Null
    source: normalImg;
    opacity:(isDisable)?0.4:1;
    border.left: parseInt(normalBorder[0],10);border.right: parseInt(normalBorder[0],10);
    border.top: parseInt(normalBorder[0],10);border.bottom:  parseInt(normalBorder[0],10);
    property string normalImg:""
    property string highlightImg:""
    property string pressedImg:""
    property bool isHighlight: false
    property bool isPressed: false
    property bool increaseTouchArea: false
    property int increaseValue: -40
    property bool isExternalWidth: false
    property bool isHorizantalTileReq: false
    property bool isDisable: false
    property int textMargin:0
    property int btnTextWidth: 0
    property double buttonTextopacity: 1
    property variant buttonText: []
    property variant normalBorder:[0];
    property variant highlightBorder:[0];
    property alias btnText: btnText
    property alias wrapMode:btnText.wrapMode;
    property alias elide:btnText.elide;
    property alias lineHeight:btnText.lineHeight
    property alias maximumLineCount:btnText.maximumLineCount;
    signal actionPressed
    signal actionReleased
    signal enteredReleased
  /**************************************************************************************************************************/
    BorderImage {
        id: highlight
        source:(isPressed)?pressedImg:(isHighlight)?highlightImg:""
        width:isExternalWidth?btnTextWidth:parent.width;
        height:parent.height
        anchors.centerIn: parent
        horizontalTileMode :isHorizantalTileReq?BorderImage.Repeat:BorderImage.Null
        border.left: parseInt(highlightBorder[0],10);border.right: parseInt(highlightBorder[0],10);
        border.top: parseInt(normalBorder[0],10);border.bottom:  parseInt(normalBorder[0],10);
        opacity:(isHighlight || isPressed)?1:0;
        Behavior on opacity {
            NumberAnimation{duration:300;}
        }
    }
    ViewText{
        id:btnText
        width:btnTextWidth?btnTextWidth:paintedWidth
        varText:buttonText
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter:  parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        opacity: buttonTextopacity
    }
    onActionReleased:  {
        base.forceActiveFocus()
        enteredReleased()
    }
    Keys.onPressed: {
        if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
            if(isDisable)return;
            actionPressed();
        }
    }
    Keys.onReleased: {
        if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
            if(isDisable)return;
            actionReleased()
        }
    }
    MouseArea{
        anchors.fill:parent;
        anchors.margins: increaseTouchArea ? increaseValue : 0
        onPositionChanged: isPressed=false
        onPressed: {
            if(isDisable)return;
            if(pressedImg!="")
                isPressed=true
            actionPressed();
        }
        onReleased:{
            if(isDisable)return;
            isPressed=false
            actionReleased()
        }
    }
}
