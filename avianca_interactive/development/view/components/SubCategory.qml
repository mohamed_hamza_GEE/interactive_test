import QtQuick 1.1
FocusScope{
    anchors.fill: parent
    property alias compVpathSubCategory: compVpathSubCategory
    property bool isUsb:(current.template_id=="usb_images"||current.template_id=="usb_doc"||current.template_id=="usb_music")?true:false
    Image{
        id:listingLayersBG
        anchors.top: parent.top
        anchors.topMargin: qsTranslate('','mainmenu_submenu_y')
        source:  viewHelper.configToolImagePath+configTool.config.menu.submenu.band
    }
    ListView{
        id:compVpathSubCategory
        function setModel(dModel){compVpathSubCategory.model=dModel;}
        visible: (count>0)
        anchors.top:parent.top
        anchors.topMargin: qsTranslate('','mainmenu_submenu_y')
        anchors.horizontalCenter:  parent.horizontalCenter
        width:(contentWidth<parseInt(qsTranslate('','mainmenu_submenu_w')))?contentWidth: qsTranslate('','mainmenu_submenu_w');
        height: qsTranslate('','mainmenu_submenu_h')
        clip: true
        focus:true;
        boundsBehavior: ListView.StopAtBounds
        snapMode: ListView.SnapOneItem
        highlightMoveDuration: 200
        orientation: ListView.Horizontal
        delegate: iconTextCenter
        onMovementEnded:  {
            currentIndex=indexAt(contentX+5,contentY+2)
            compVpathSubCategory.forceActiveFocus()
        }
        Keys.onRightPressed: {
            incrementCurrentIndex()
        }
        Keys.onLeftPressed: {
            decrementCurrentIndex()
        }
        Keys.onDownPressed: {
            if(isUsb){if(buttonPanel.backBtnRef().isDisable!=true){buttonPanel.backBtnRef().forceActiveFocus()} else {focusFromSubCat()}}
            else{
                if(isSynopsis){
                    if(synopsisobj.item.specialFocus){
                        synopsisobj.item.specialFocus("down");
                    }else{
                        synopsisobj.item.forceActiveFocus();
                    }
                }else if(listingobj.compVpathListing.count>0 && listingobj.compVpathListing.visible)
                    listingobj.compVpathListing.forceActiveFocus()
            }

        }
        Keys.onUpPressed: {
            widgetList.focustTomainMenu()
        }
        Keys.onReleased: {
            if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter)
                enterReleased(currentIndex)
        }
    }
    SimpleButton{
        id:subCategoryRightArrow
        visible:((compVpathSubCategory.count>5 ||  compVpathSubCategory.contentWidth>compVpathSubCategory.width) && (compVpathSubCategory.contentX<(compVpathSubCategory.contentWidth-compVpathSubCategory.width)))?true:false
        anchors.right: parent.right
        anchors.rightMargin: qsTranslate('','mainmenu_arrow_gap')
        anchors.top:parent.top
        anchors.topMargin:  qsTranslate('','mainmenu_submenu_y')
        normImg: viewHelper.configToolImagePath+configTool.config.menu.submenu.rightarrow
        onEnteredReleased: {
            compVpathSubCategory.forceActiveFocus()
            compVpathSubCategory.incrementCurrentIndex()
        }
    }
    SimpleButton{
        id:subCategoryLeftArrow
        visible: ((compVpathSubCategory.count>5 ||  compVpathSubCategory.contentWidth>compVpathSubCategory.width) && (compVpathSubCategory.contentX>0))?true:false
        anchors.left: parent.left
        anchors.leftMargin:  qsTranslate('','mainmenu_arrow_gap')
        anchors.top:parent.top
        anchors.topMargin:  qsTranslate('','mainmenu_submenu_y')
        source:  viewHelper.configToolImagePath+configTool.config.menu.submenu.leftarrow
        onEnteredReleased: {
            compVpathSubCategory.forceActiveFocus()
            compVpathSubCategory.decrementCurrentIndex()
        }
    }
}

