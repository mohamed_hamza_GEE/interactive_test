// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../framework"

Rectangle{
    id:mainRect
    x:105;
    Row{
        y: 0; x:10;
        spacing: 10;
        Rectangle{
            id:link1
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Launch VKB"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(viewController.widgetList.getVkbRef().visible) viewController.widgetList.showVkb(false)
                    else viewController.widgetList.showVkb(true)
                }
            }
        }
        Rectangle{
            id:link16
            radius: 10
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link16_txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Survey"}
            MouseArea{
                anchors.fill: parent;
                onClicked: { pif.sigSurveyReceived(19,'Test'); }
            }
        }
        Rectangle{
            id:link2
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1Txt1;anchors.centerIn: parent; color:'#FFFFFF'; text:"Launch Settings"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(3)
                }
            }
        }
        Rectangle{
            id:link3
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1Txt2;anchors.centerIn: parent; color:'#FFFFFF'; text:"Launch Synopsis"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(4)
                }
            }
        }
        Rectangle{
            id:link4
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1Txt3;anchors.centerIn: parent; color:'#FFFFFF'; text:"ResetVkarma"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    pif.sharedModelServerInterface.sendApiData("init")

                }
            }
        }
        Rectangle {
            color: "#5C5C5C"; width:120; height: 30
            Text { id:chatInvite;anchors.centerIn: parent; text: " ChatInvite " }
            MouseArea{ anchors.fill: parent; onClicked:{
                    pif.seatChat.sigInvitationReceived("2A","CR13","i6989");
                    pif.seatChat.sigInvitationReceived("3A","CR15","i7989");
                    pif.seatChat.sigInvitationReceived("4A","CR17","i8989");
                }
            }
        }
        Rectangle {
            color: "#5C5C5C"; width: 120; height: 30
            Text { id:chatRecieve;anchors.centerIn: parent; text: " ChatRecieve " }
            MouseArea{ anchors.fill: parent; onClicked:{
                    pif.seatChat.chatHistoryModel.clear();
                    pif.seatChat.chatHistoryModel.append({"time":new Date(),"seat":"1A", "seatName":"", "msg":"Hey dude what have you ordered for lunch?"})
                    pif.seatChat.chatHistoryModel.append({"time":new Date(),"seat":"1A", "seatName":"", "msg":"I have ordered Chicken Cheddar Deluxe"})
                    pif.seatChat.chatHistoryModel.append({"time":new Date(),"seat":"1B", "seatName":"", "msg":"Not very sure what to order. I was also thinking abt the same dish. And what did you order for drinks?"})
                    pif.seatChat.chatHistoryModel.append({"time":new Date(),"seat":"1A", "seatName":"", "msg":"I have heard this airline serves tasty food"})
                    pif.seatChat.chatHistoryModel.append({"time":new Date(),"seat":"1B", "seatName":"", "msg":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu nisl quam, tristique bibendum eros. Sed commodo suscipit turpis, a ullamcorper erat porttitor ac. Nullam imperdiet rutrum risus, in blandit sapien vulputate a. Nulla facilisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum quis commodo neque. Curabitur a libero ut magna aliquam commodo. Suspendisse in lectus a velit consequat condimentum. Maecenas pulvinar magna eget neque posuere sit amet porta nulla tempus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ut urna a felis imperdiet dictum eget et ipsum. Vivamus augue neque, mattis quis molestie sit amet, eleifend at eros."})
                    pif.seatChat.chatHistoryModel.append({"time":new Date(),"seat":"1A", "seatName":"", "msg":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu nisl quam, tristique bibendum eros. Sed commodo suscipit turpis, a ullamcorper erat porttitor ac. Nullam imperdiet rutrum risus, in blandit sapien vulputate a. Nulla facilisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum quis commodo neque. Curabitur a libero ut magna aliquam commodo. Suspendisse in lectus a velit consequat condimentum. Maecenas pulvinar magna eget neque posuere sit amet porta nulla tempus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ut urna a felis imperdiet dictum eget et ipsum. Vivamus augue neque, mattis quis molestie sit amet, eleifend at eros."})
                }
            }
        }
        Rectangle {
            color: "#5C5C5C"; width: 120; height: 30
            Text { id:chatNotify1;anchors.centerIn: parent; text: " NotifyDecline " }
            MouseArea{ anchors.fill: parent; onClicked:{pif.seatChat.sigSentInvitationStatus("69HG","i6989",0)}
            }
        }
        //        Rectangle {
        //            color: "#5C5C5C"; width: 120; height: 30
        //            Text { id:chatNotify2;anchors.centerIn: parent; text: " ChatNotify 2" }
        //            MouseArea{ anchors.fill: parent; onClicked:{
        //                    pif.seatChat.buddyListModel.append( {"seat":"CR-1023","state":"avail","seatname":"Dummy"} )
        //                    pif.seatChat.buddyListModel.append( {"seat": "CR13","state":"avail","seatname":"Dummy"} )
        //                    pif.seatChat.buddyListModel.append( {"seat": "CR-3","state":"avail","seatname":"Dummy"} )
        //                    pif.seatChat.buddyListModel.append( {"seat": "CR-13","state":"avail","seatname":"Dummy"} )
        //                    pif.seatChat.sigJoinPublicSessionStatus(2,true)
        //                    //mcVariables.update_UserNotification = 1;
        //                }
        //            }
        //        }
        Rectangle {
            color: "#5C5C5C"; width:120; height: 30
            Text { id:chatNotify3;anchors.centerIn: parent; text: " NotifyAccept" }
            MouseArea{ anchors.fill: parent; onClicked:{pif.seatChat.sigSentInvitationStatus("69CR","i6989",1)}
            }
        }

    }

    Row{
        y:40;x:10;spacing: 10
        Rectangle {
            color: "#5C5C5C"; width: 120; height: 30
            Text { id:chatNotify4;anchors.centerIn: parent; text: "ChatBlockReady" }
            MouseArea{ anchors.fill: parent; onClicked:{
                    pif.seatChat.publicsessionListModel.append( {"pSessionId": "C1023"} )
                    pif.seatChat.publicsessionListModel.append( {"pSessionId": "C13"} )
                    pif.seatChat.publicsessionListModel.append( {"pSessionId": "C3"} )
                    pif.seatChat.publicsessionListModel.append( {"pSessionId": "C13"} )
                    pif.seatChat.sigJoinPublicSessionStatus(21,true)
                }
            }
        }
        Rectangle {
            color: "#5C5C5C"; width:120; height: 30
            Text { id:chatNotify5;anchors.centerIn: parent; text: " NotifyExit" }
            MouseArea{ anchors.fill: parent; onClicked:{pif.seatChat.sigBuddyExit("69CR","i6989");}
            }
        }
        Rectangle {
            color: "#5C5C5C"; width:120; height: 30
            Text { id:chatExit;anchors.centerIn: parent; text: " ChatExit" }
            MouseArea{ anchors.fill: parent; onClicked:{pif.seatChat.exitChat();}
            }
        }
        Rectangle {
            color: "#5C5C5C"; width:120; height: 30
            Text {anchors.centerIn: parent; text:"populate Buddy";color: "#FFFFFF";}
            MouseArea{
                anchors.fill: parent; onPressed:{
                    pif.seatChat.buddyListModel.clear();
                    pif.seatChat.buddyListModel.append({'seatnum':"1A",'seatname':"Test",'seatstate':1,'status':"avail",'blockstatus':false})
                    pif.seatChat.buddyListModel.append({'seatnum':"2A",'seatname':"Case",'seatstate':1,'status':"avail",'blockstatus':true})
                    pif.seatChat.buddyListModel.append({'seatnum':"3A",'seatname':"",'seatstate':1,'status':"avail",'blockstatus':false})
                    pif.seatChat.buddyListModel.append({'seatnum':"4A",'seatname':"",'seatstate':1,'status':"avail",'blockstatus':true})
                    pif.seatChat.buddyListModel.append({'seatnum':"5A",'seatname':"",'seatstate':1,'status':"avail",'blockstatus':true})
                    pif.seatChat.buddyListModel.append({'seatnum':"6A",'seatname':"",'seatstate':1,'status':"avail",'blockstatus':false})
                    pif.seatChat.buddyListModel.append({'seatnum':"7A",'seatname':"",'seatstate':1,'status':"avail",'blockstatus':true})
                    pif.seatChat.buddyListModel.append({'seatnum':"8A",'seatname':"",'seatstate':1,'status':"avail",'blockstatus':true})
                    //                    pif.seatChat.sigJoinPublicSessionStatus(2,true)
                    //                    viewHelper.update_UserNotification = 1;
                }
            }
        }

        Rectangle {
            //                id:rect4
            //                anchors.left:rect3.right;anchors.top:rect3.top;anchors.leftMargin: 50;
            color: "#5C5C5C"; width:120; height: 30
            Text { anchors.centerIn: parent; text:"populate Session";color: "#FFFFFF";}
            MouseArea{
                anchors.fill: parent; onPressed:{
                    pif.seatChat.sessionListModel.clear();
                    pif.seatChat.sessionListModel.append({'session':"i10"})
                    pif.seatChat.sessionListModel.append({'session':"i20"})
                    pif.seatChat.sessionListModel.append({'session':"i30"})
                    pif.seatChat.sessionListModel.append({'session':"i40"})
                    pif.seatChat.sessionListModel.append({'session':"i50"})
                    pif.seatChat.sessionListModel.append({'session':"i60"})
                    pif.seatChat.sessionListModel.append({'session':"i70"})
                    pif.seatChat.sessionListModel.append({'session':"i80"})
                    pif.seatChat.sessionListModel.append({'session':"i90"})
                    pif.seatChat.sessionListModel.append({'session':"i100"})
                }
            }
        }
        Rectangle {
            color: "#5C5C5C"; width:120; height: 30
            Text { id:blockModel;anchors.centerIn: parent; text: " Block Model" }
            MouseArea{ anchors.fill: parent; onClicked:{
                    //                    pif.seatChat.blockListModel.clear();
                    pif.seatChat.blockListModel.append( {'seat':"1A"} )
                    pif.seatChat.blockListModel.append( {'seat':"2A"} )
                    pif.seatChat.blockListModel.append( {'seat':"3A"} )
                    pif.seatChat.blockListModel.append( {'seat':"4A"} )
                }
            }
        }
    }
    Row{
        y: 80; x:10;
        spacing: 10;
        Rectangle{
            id:link5
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link5Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock Ipod"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link5Txt.text=="ServiceBlock Ipod"){
                        pif.serviceAccessChanged('blocked',6176)
                        link5Txt.text="ServiceUnblock Ipod"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',6176)
                        link5Txt.text="ServiceBlock Ipod"
                    }
                }
            }
        }
        Rectangle{
            id:link6
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link6Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock AOD"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link6Txt.text=="ServiceBlock AOD"){
                        pif.serviceAccessChanged('blocked',6193)
                        link6Txt.text="ServiceUnblock AOD"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',6193)
                        link6Txt.text="ServiceBlock AOD"
                    }
                }
            }
        }
        Rectangle{
            id:link7
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link7Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock VOD"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link6Txt.text=="ServiceBlock VOD"){
                        pif.serviceAccessChanged('blocked',6191)
                        link6Txt.text="ServiceUnblock VOD"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',6191)
                        link6Txt.text="ServiceBlock VOD"
                    }
                }
            }
        }
        Rectangle{
            id:link8
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link8Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock TV"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link8Txt.text=="ServiceBlock TV"){
                        pif.serviceAccessChanged('blocked',6195)
                        link8Txt.text="ServiceUnblock TV"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',6195)
                        link8Txt.text="ServiceBlock TV"
                    }
                }
            }
        }
        Rectangle{
            id:link9
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link9Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock Usb"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link9Txt.text=="ServiceBlock Usb"){
                        pif.serviceAccessChanged('blocked',6177)
                        link9Txt.text="ServiceUnblock Usb"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',6177)
                        link9Txt.text="ServiceBlock Usb"
                    }
                }
            }
        }
        Rectangle{
            id:link10
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link10Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock Shopping"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link9Txt.text=="ServiceBlock Shopping"){
                        pif.serviceAccessChanged('blocked',27210)
                        link9Txt.text="ServiceUnblock Shopping"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',27210)
                        link9Txt.text="ServiceBlock Shopping"
                    }
                }
            }
        }
        Rectangle{
            id:link11
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link11Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Invite popup"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.updateSystemPopupQ(13,true)
                }
            }
        }
        Rectangle{
            id:link12
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link12Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Invite popup"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(15)
                }
            }
        }
        Rectangle{
            id:link13
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link13Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Invite popup"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
//                    viewController.updateSystemPopupQ(15,true)
                    viewController.showScreenPopup(16)
                }
            }
        }

    }

    Row{
        y: 120; x:10;
        spacing: 10;
        Rectangle{
            id:link101
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link101Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock Hospitality"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link101Txt.text=="ServiceBlock Shopping"){
                        pif.serviceAccessChanged('blocked',27216)
                        link101Txt.text="ServiceUnblock Shopping"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',27216)
                        link101Txt.text="ServiceBlock Shopping"
                    }
                }
            }
        }
        Rectangle{
            id:link102
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link102Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock SeatChat"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link102Txt.text=="ServiceBlock SeatChat"){
                        pif.serviceAccessChanged('blocked',27215)
                        link102Txt.text="ServiceUnblock SeatChat"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',27215)
                        link102Txt.text="ServiceBlock SeatChat"
                    }
                }
            }
        }
        Rectangle{
            id:link103
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link103Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Power Down "}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    pif.powerDownEventReceived()
                }
            }
        }
    }
}
