import QtQuick 1.1 

Item {
    Connections{
        target: viewController;
        onSystemPopupClosed:{
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedCtMessageinProgress",{"status":false});
        }
    }
    Connections{
        target: core;
        onSigLanguageChanged:{
            if(viewController.getActiveScreenRef().languageUpdate){
                viewController.getActiveScreenRef().languageUpdate();
            }
            if(pif.vod.getMediaState()!=pif.vod.cMEDIA_STOP)
                widgetList.soundTrackSubtitle.getSubTitlesQuery()

            if(viewController.getActiveScreen()!="Welcome")
                viewHelper.setLocale(languageISO)
            //pif.android.setLocale(viewHelper.localeLanguages[languageISO]);
        }
    }

    Connections{
        target: core.dataController.mediaServices;
        onSigCidBlockChanged:{
            core.debug("PifEvents.qml | Cid Block Changed | cidList : " + cidList + ", status : " + status + ", smid : " + smid + ", serviceCode : " + serviceCode);
            viewHelper.actionOnServiceBlock(status,serviceCode,cidList);
        }
        onSigMidBlockChanged: {
            core.debug("PifEvents.qml | Mid Block Changed | mid : " + mid + ", status : " + status);
            viewHelper.actionOnMidBlock(mid,status);
            /** Following code is work around till core does not fix karma getting the mid block event **/
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendApiData("userDefinedMidBlock",{statusValue:status,midValue:mid})

            }
        }
        onBacklightStateChanged:{
            //pif.android.forceUserActivity();
        }
    }


    Connections{target:pif.seatChat

        onSigSentInvitationStatus:{
            viewHelper.invitationSent--;
        }

        onSigInvitationReceived:{
            core.info("Pifevents.qml | onSigInvitationReceived | session = "+session + " | seatNum = "+seatNum + " | seatName = "+seatName);
            if(viewHelper.getBlockInvitationStatus()){
                pif.seatChat.declineInvitation(session,seatNum)
                return;
            }
            if((viewHelper.actualSessionCount+viewHelper.invitationSent)<=viewHelper.maxSessionCount){
                pif.seatChat.addToQueue(seatNum,seatName,"",session)
                if(viewController.getActiveSystemPopupID()!=13 && (viewController.getActiveScreen()!="Welcome")){
                    viewHelper.cSessionID=session
                    viewHelper.tSeatNo=seatNum
                    viewHelper.popupTSeatNo=seatNum
                    viewController.updateSystemPopupQ(13,true)
                }
            }
            else {
                core.info("Pifevents.qml | onSigInvitationReceived | viewHelper.actualSessionCount >>>>>>>>>>>>>>>>>>>>>>>>>"+viewHelper.actualSessionCount)
                viewController.showScreenPopup(21)}
        }

        onSigChatMessageReceived:{
            core.info("Pifevents.qml | onSigChatMessageReceived | session =  "+message);
        }


    }

    Connections{
        target: pif
        onSeatRatingChanged:{
            viewHelper.actionOnMidBlock(viewHelper.playingVODmid,"",pif.vod.getMediaRating());
            viewHelper.actionOnMidBlock(pif.aod.getPlayingMid(),"",pif.aod.getMediaRating());
        }
    }

    /*Connections{
        target:pif.usb;
        onSigUsbConnected:{
            viewHelper.filePathName = rootPath;
            if(pif.getEntOn()==0 || pif.getOpenFlight()==0 ) return;
            pif.paxus.startContentLog('Usb Connected')
            if(viewController.getActiveScreen()=="Welcome" || viewController.getActiveScreen()=="EntOff" || viewController.getActiveScreen()=="USB"|| viewController.getActiveScreen()=="UsbThumb"||  viewController.getActiveScreen()=="UsbListing")return;
            else if(widgetList.isUsbSlideShowOn())return;
            viewController.updateSystemPopupQ(4,true);

        }
        onSigUsbDisconnected:{
            if(pif.getEntOn()==0 || pif.getOpenFlight()==0 ) return;
            pif.paxus.stopContentLog()
            viewHelper.usbIndex="usb_images";
            if(viewController.getActiveScreen()=="USB"|| viewController.getActiveScreen()=="UsbThumb"||  viewController.getActiveScreen()=="UsbListing"){viewHelper.homeAction();viewController.updateSystemPopupQ(5,true);return;}
            if(viewController.getActiveScreen()=="Welcome" || viewController.getActiveScreen()=="EntOff" ) return;
            else if(widgetList.isUsbSlideShowOn())return;
            else viewController.updateSystemPopupQ(5,true);
        }
    }*/

    Connections{
        target: pif
        onPaStateChanged:{
            core.info("PifEvents.qml | onPaStateChanged | paState :::::"+paState)
            viewHelper.isPAonVoyager = false;
            if(paState==1){
                if(pif.getOpenFlight()){
                    viewHelper.paLaunch()
                }else core.debug("Flight is closed")
            }
            else if(paState==2){
                viewHelper.updatePremiumHandset(3,true);
                viewHelper.isPAonVoyager = true;
            }
            else if(paState==0){
                viewHelper.updatePremiumHandset(3,false);
                viewController.updateSystemPopupQ(1,false)
            }
            else {
                core.debug("invalid paState: "+paState)
            }

            if(pif.vod.getMediaState()!=pif.vod.cMEDIA_STOP && viewHelper.getVodType()==1)return;

            if(paState){
                pShow.stop();
                pHide.restart();
            }else{
                if(pif.getEntOn()){
                    pHide.stop();
                    pShow.restart();
                }
            }

        }
        onCapSensorStateChanged:{
            if(lockcap.running)return;
            lockcap.restart();
            if(pif.getBacklightState()) pif.setBacklightOff();
            else pif.setBacklightOn();
        }
        onWowStateChanged:{
        }
        onSigSurveyReceived:{
            core.debug("Trigger message with Survey received | surveyId: " + surveyId + " | surveyTitle |" + surveyTitle );
            if(current.template_id=="WELC")return
            viewHelper.surveyId = surveyId;
            viewController.updateSystemPopupQ(11,true)
        }
        onPowerDownEventReceived:{
            core.debug("Pifevents.qml | onPowerDownEventReceived ");
            viewController.updateSystemPopupQ(14,true);
            viewController.sendMsgToExtPopup(14,qsTranslate('',configTool.language.global_elements.power_down_txt+"|"+configTool.config.popup.generic.bg_color+"|"+configTool.config.popup.generic.text_color+"|"+viewHelper.isKids(current.template_id)+"|"+pif.ctMessage.ctTimeout));
            if(pif.getExternalAppActive()){pif.setBacklightOn();}
            pif.executePowerDownEvent()
        }
    }

    Timer{
        id:pHide;
        interval:2500;onTriggered:{
            core.log(" Hide Android Bar Start >>>>>>>>>>>>>>>>>")
            viewController.getExternalPopupRef().setPopUpBarVisibilityType(viewController.getExternalPopupRef().cSYSTEM_BAR_HIDE);
            viewHelper.hideAndroidBar()
            core.log(" Hide Android Bar END >>>>>>>>>>>>>>>>>")
        }
    }

    Timer{id:pShow;interval:1000;onTriggered:{
            core.log(" Show Android Bar Start>>>>>>>>>>>>>>>>>")
            if(widgetList.isVideoVisible()){
                if(widgetList.getVideoRef().showHeaderFooter)viewHelper.showAndroidBar();
                return
            }
            viewController.getExternalPopupRef().setPopUpBarVisibilityType(getExternalPopupRef().cSYSTEM_BAR_VISIBLE);
            viewHelper.showAndroidBar()
            core.log(" Show Android Bar End>>>>>>>>>>>>>>>>>")
        }
    }

    Timer{id:lockcap;interval:250;}

    Connections{     // to trigger survey
        target:dataController.survey
        onSigtTriggerSurveyReady:{
            core.debug("Trigger message with Survey received | surveyId : " + surveyId + " | Event |" + event );
            if(current.template_id=="WELC")return
            viewHelper.surveyId=surveyId
            viewController.updateSystemPopupQ(11,true)
        }
    }

    Connections{
        target: pif.ctMessage;
        onCtToSeatMsgReceived: {
            viewHelper.ctToSeatMsgLaunch()
        }
    }

    Connections{
        target: widgetList?(widgetList.isFooterVisible()?widgetList.getFooterRef():null):null
        onBackPressed:{
            if(current.template_id=="WELC" || current.template_id=="kids" || current.template_id=="adult")return;
            viewHelper.backaction()
        }
        onHomePressed:{
            if(current.template_id=="WELC" || current.template_id=="kids" || current.template_id=="adult")return;
            viewHelper.homeAction()
        }
    }
    Connections{
        target:core.pif;
        onCgDataAvailableChanged:{
            if(viewController.getActiveScreen() != "ConnectingGate" && (dataController.connectingGate)){
                viewController.updateSystemPopupQ(10,true)
            }
        }
    }
    Connections{
        target:(dataController.connectingGate)?dataController.connectingGate:null;
        onSigCgStatusChanged:{
            if(dataController.connectingGate)viewController.updateSystemPopupQ(9,true)
        }
    }
    Connections{
        target:pif.launchApp;
        onSigAppLaunched:{
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                if(viewHelper.gameMid > 0)pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedGameLaunch",{midValue:viewHelper.gameMid})
            }
        }
        onSigAppExit:{
            pTimer.restart()
        }
    }

    Timer{id:pTimer;interval:1000;onTriggered:{viewHelper.checkGameQueue();/*viewHelper.checkVideoQueue()*/}}

    Connections{
        target:(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)?pif.sharedModelServerInterface:null;
        onClientConnected :{
            core.debug("Vkarma Connected to seat")
            pif.sharedModelServerInterface.sendApiData("userDefinedKarmaStatus",{value:"true"})
            pif.sharedModelServerInterface.sendApiData("userDefinedCatPlaying",{playingCategory:viewHelper.playingCategory})
        }
        onSigDataReceived:{
            core.debug("In Pif Events  Seat-Vkarma | sharedModelServerInterface| api "+api+ " params "+params)
            switch(api){
            case  "userDefinedStoreTrailerParentMid":{
                viewHelper.storeTrailerParentMid(params.parentMid)
            }
            break;
            case "userDefinedgetSeatStatus":{
                core.debug("In Pif Events  Seat-Vkarma | viewHelper.bootUp: "+viewHelper.bootUp)
                if(viewHelper.bootUp){
                    pif.sharedModelServerInterface.sendApiData("userDefinedsetSeatStatus",{value:"ready",languageIso:core.settings.languageISO})
                }
            }
            break;
            case "userDefinedsendInvite":{
                core.debug("In Pif Events  Seat-Vkarma | userDefinedsendInvite | parmas.seatNo: "+params.seatNo)
                viewHelper.tSeatNo=params.seatNo
                pif.seatChat.sendInvitation(viewHelper.cSessionID,viewHelper.tSeatNo)
                viewController.showScreenPopup(17)
            }
            break;
            case "userDefinedsendMutipleInvite":{
                core.debug("In Pif Events  Seat-Vkarma | userDefinedsendMutipleInvite | parmas.seatNo: "+params.seatNo)
                viewHelper.tSeatNo=params.seatNo
                pif.seatChat.sendInvitation(viewHelper.cSessionID,params.seatNo)
                //                viewController.updateSystemPopupQ(17,true)  //changed to screen popup
                viewController.showScreenPopup(17)
            }
            break;
            case "userDefinedRepeatOn":{
                if(params.repeatStatus!=viewHelper.userRepeatOn)
                    viewHelper.userRepeatOn=params.repeatStatus
            }
            break;

            case "userDefinedForceCloseGame":{
                console.log("userDefinedForceCloseGame >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  | viewHelper.gamePath = "+viewHelper.gamePath)

                if(pif.launchApp.launchId==viewHelper.voyager3dAppName){
                    if(pif.launchApp.forceStopPackage)pif.launchApp.forceStopPackage(viewHelper.voyager3dAppName)
                }else{
                    if(pif.launchApp.forceStopPackage)pif.launchApp.forceStopPackage(viewHelper.gamePath)
                }

                if(!pif.launchApp.closeApp()){
                    pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedGameExit",{})
                }
            }
            break;


            case "userDefinedUpdateLang":{
                viewHelper.languageIndex=params.langIndex
            }
            break;

            case "changeLanguage":{
                var lid = dataController.getLidByLanguageISO(params['languageIso']);
                core.debug("Pif Events |  lid "+lid+ " iso "+params['languageIso'])
                core.setInteractiveLanguage(lid,params['languageIso'] );
                viewHelper.setHelpLanguage(params['languageIso'])
                if(viewController.getActiveScreenRef().languageUpdate)
                    viewController.getActiveScreenRef().languageUpdate()
            }
            break;

            case "userDefinedJump":{
                console.log("viewHelper.sectionSelected | userDefinedJump "+viewHelper.sectionSelected)
                console.log("params.section | userDefinedJump "+params.section)
                viewController.hideScreenPopup();
                if(viewHelper.sectionSelected==params.section)return;
                viewHelper.sectionSelected=params.section
                if(widgetList.isVideoVisible())widgetList.getHeaderRef().stopVODTimer(true);
                widgetList.closeAllwidgets()
                viewHelper.jumpToWelcome(params.section,params.language)
                if(pif.screenSaver.__screenSaverState!=0)
                    pif.screenSaver.restartScreenSaver();
            }
            break;
            case "userDefinedPlayEpisode":{
                viewHelper.playEpisode(params.index)
            }
            break;

            case "userDefinedPlayFromGenre":{
                viewHelper.trackStop=false;
                viewHelper.customHandsetSignal(params.control)
            }
            break;

            case "launchCategory":{
                if(widgetList.getshowAppLoading()==true && params.launchAppId.tid != 'maps')return;
                if(widgetList.getMealStatus()){widgetList.showMealMenu(false)}
                if(widgetList.getHelpStatus()==1)widgetList.setHelpStatus(false)
                if(viewController.getActiveSystemPopupID())return;
                if(viewController.getActiveScreenPopupID())viewController.hideScreenPopup();

                /* core.log("params.launchAppId")
                core.log("params.launchAppId.pcid ===  "+params.launchAppId.pcid)
                core.log("params.launchAppId.tid ===  "+params.launchAppId.tid)
                for(var a in params){
                    core.log(a+ " ------  "+params[a]);
                    for(var b in params[a]){
                        core.log(a+" "+b+ " ------  "+params[a][b]);
                    }
                }*/

                core.info("PifEvent | Connection | onSigDataReceived | widgetList.isFlightMapOn() "+widgetList.isFlightMapOn());

                if(widgetList.isFlightMapOn()){
                    if(params.launchAppId.tid == 'maps'){
                        core.info("PifEvent | Connection | onSigDataReceived | Map Already Launched");
                        return;
                    }
                    widgetList.showFlightMap(false)
                }
                core.log("PifEvent | Connection | onSigDataReceived | launchCategory");
                if(widgetList.getisFlightInfoOpen())widgetList.setisFlightInfoOpen(false)
                if(widgetList.isVideoVisible()){
                    widgetList.stopVod()
                    if(viewController.getActiveScreenPopupID()==1)
                        viewController.hideScreenPopup()

                }


                if(params.launchAppId.tid=="seatchat"){
                    console.log("launchCategorylaunchCategorylaunchCategorylaunchCategory >>>>>>>>>>>> seatchat pressed")
                    viewHelper.seatchatKarmaActionPerform()
                    if(viewController.getActiveScreen()=="SeatChat"){
                        console.log("launchCategory*******************************************")
                        viewHelper.seatchaNickNametKarmaActionPerform()
                        return;
                    }
                    console.log("launchCategory***********&&&&&&&&&&&&&&&&&&&&&&&&&&&********************************")
                    viewHelper.loadShortcutByCid(params.launchAppId)
                }else{
                    console.log("launchCategorylaunchCategorylaunchCategorylaunchCategory >>>>>>>>>>>> not seatchat pressed")
                    viewHelper.loadShortcutByCid(params.launchAppId)
                }
                //                  viewHelper.loadShortcutByCid(params.launchAppId)
                if(pif.screenSaver.__screenSaverState!=0)
                    pif.screenSaver.restartScreenSaver();


            }
            break;

            case "showKeyboard":{
                widgetList.showVkb(params.showKeyboard)
            }
            break;

            case "userDefinedToggleKeyBoard":{
                if(pif.screenSaver.__screenSaverState!=0)
                    pif.screenSaver.restartScreenSaver();

                widgetList.getVkbRef().toggleKeys(params.toggleKeyBoard)
            }
            break;

            case "userDefinedPlayGenre":{
                viewHelper.trackStop=false;
                core.debug(" params.genreValue : "+params.genreValue)
                var dataParama=new Object();
                dataParama['cid']           = params.genreValue[0];
                dataParama['template_id']   = params.genreValue[1]
                dataParama['inputSearch']   = params.genreValue[2];
                dataParama['searchType']    = "full"  ;
                dataParama['fieldList']     = 'title,artist,mid,rating,aggregate_parentmid,duration';
                dataParama['searchBy']      = 'genre'
                dataParama['orderBy']       = 'title';
                dataParama['groupBy']       = 'title'
                dataParama['isParentAggSearch'] = true;
                //                if(viewHelper.genreSongIndex>=0){
                //                    viewHelper.genreSongIndex=params.genreIndex;
                //                    viewHelper.playGenreFromKarma(viewHelper.getGenreSongOrgModel())
                //                }else{
                viewHelper.genreSongIndex=params.genreIndex;
                dataController.keywordSearch.getData(dataParama,function (dmodel){viewHelper.playGenreFromKarma(dmodel)});
                //}

            }
            break;

            case "videoSoundtrack":{
                viewHelper.setSoundTracklid(params.soundtrackLid)
            }
            break;

            case "videoSubtitle":{
                viewHelper.setSubtitlelid(params.subtitleLid)

            }
            break;

            case "userDefinedSendPlayCat":{
                viewHelper.playingCategory=params.playingCat
                //	viewHelper.setVodType(params.playingCat)
            }
            break;

            /**************************************************USB***************************/

            case "userDefinedContinueUsb":{
                if(widgetList.isFlightMapOn()){widgetList.showFlightMap(false)}
                viewHelper.showUsb("continue")
                if(pif.screenSaver.__screenSaverState!=0)
                    pif.screenSaver.restartScreenSaver();
            }
            break;

            case "userDefinedCancelUsb":{
                viewHelper.showUsb("cancel")
                if(pif.screenSaver.__screenSaverState!=0)
                    pif.screenSaver.restartScreenSaver();
            }
            break;

            case "userDefinedUsbDisconnect":{
                viewController.updateSystemPopupQ(viewController.getActiveSystemPopupID(),false);
                if(pif.screenSaver.__screenSaverState!=0)
                    pif.screenSaver.restartScreenSaver();
            }
            break;

            case "userDefinedVodType":{
                core.log("vodType"+params.vodType)
                viewHelper.setVodType(params.vodType)

                if(params.current_id=="movies_listing"){widgetList.setSndtemplateId("movies")}
                else if(params.current_id=="tv_listing"){widgetList.setSndtemplateId("tv")}
                else {widgetList.setSndtemplateId(params.current_id);}
            }
            break;

            case "userDefinedTvIndex":{
                viewHelper.setTvSeriesIndex(params.index)
            }
            break;

            case "userDefinedCid":{
                viewHelper.setCidFromKarma(params.cid)
            }
            break;

            case "closeApp":{
                core.debug("pif.launchApp.launchId | in Pif Events "+pif.launchApp.launchId)
                core.debug("pif.launchApp.launchId | in Pif Events | viewHelper.gamePath "+viewHelper.gamePath)
                if(pif.launchApp.launchId!=""){
                    if(pif.launchApp.launchId==viewHelper.voyager3dAppName){
                        if(pif.launchApp.forceStopPackage)pif.launchApp.forceStopPackage(viewHelper.voyager3dAppName)
                    }else{
                        if(pif.launchApp.forceStopPackage)pif.launchApp.forceStopPackage(viewHelper.gamePath)
                    }
                }
                // pif.launchApp.forceStopPackage(viewHelper.gamePath)
                // pif.launchApp.closeApp();
            }
            break;

            case "userDefinedgetPlayingCategory":{
                var playParams=new Object()
                playParams.category=viewHelper.playingCategory//widgetList.getCidTidMainMenu()[1]
                core.debug("viewHelper.playingCategory >>>>>>>>>>>>>>>>>>  "+viewHelper.playingCategory)
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedsetPlayingCategory",{category: viewHelper.playingCategory,/*vodType:viewHelper.getVodType()*/})
            }
            break;

            case "playVod":{
                core.log("playVod "+viewHelper.getVodType())
                if(widgetList.getisFlightInfoOpen()){widgetList.setisFlightInfoOpen(false)}

                if(pif.getExternalAppActive() == true  || pif.launchApp.launchInProgress){                   
                    if(pif.launchApp.launchId==viewHelper.voyager3dAppName){
                        if(pif.launchApp.forceStopPackage)pif.launchApp.forceStopPackage(viewHelper.voyager3dAppName)
                    }else{
                        if(pif.launchApp.forceStopPackage)pif.launchApp.forceStopPackage(viewHelper.gamePath)
                    }
                   /*pif.launchApp.closeApp();*/

                }else if(widgetList.getMealStatus()){
                    widgetList.showMealMenu(false)
                }
                if(viewController.getActiveSystemPopupID()==6)viewController.updateSystemPopupQ(6,false);


                for(var a in params){
                    core.log(a+ " ----- ------  "+params[a]);
                   /* for(var b in params[a]){
                        core.log(a+" "+b+ "--- ------  "+params[a][b]);
                    }*/
                }


                var vodParams=new Object()
                vodParams["mid"]=params.mid
                vodParams["aggregateMid"]=params.aggregateMid
                vodParams["sndtrkLid"]=params.soundtrackLid
                vodParams["soundtrackType"]=params.soundtrackType
                vodParams["subtitleLid"]=params.subtitleLid
                vodParams["subtitleType"]=params.subtitleType
                vodParams["elapsedTime"]=params.elapsedTime
                viewHelper.setSubtitlelid(params.subtitleLid)
                viewHelper.setSoundTracklid(params.soundtrackLid)
                var action="restart"
                if(params.elapsedTime>0)
                    action="resume"
                var fromTrailer=false
                var vodPlayType
                core.debug("viewHelper.getVodType "+viewHelper.getVodType())
                vodPlayType=(viewHelper.getVodType()==1)?"trailer":(viewHelper.getVodType()==0)?"tvseries":(viewHelper.getVodType()==2)?"episode":"movies"
                viewHelper.initializeVOD(action,vodPlayType,vodParams,true,fromTrailer)

            }
            break;

            case "playTrailer":{
                console.log("playTrailer "+viewHelper.getVodType())
                /*if(pif.getExternalAppActive()){
                     viewHelper.videoAfterGameParams=[params,'trailer']
                     pif.launchApp.closeApp();
                     return;
                }
                else if(widgetList.getshowAppLoading()==true){
                    viewHelper.videoAfterGameParams=[params,'trailer']
                    pif.launchApp.closeApp();
                    return;
                }*/
                if(pif.getExternalAppActive() == true  || pif.launchApp.launchInProgress){
                    pif.launchApp.closeApp();
                }
                viewHelper.isTrailerPressed=true
                var trailerParams=new Object()
                trailerParams["mid"]=params.mid
                trailerParams["aggregateMid"]=params.aggregateMid
                trailerParams["sndtrkLid"]=params.soundtrackLid
                trailerParams["soundtrackType"]=params.soundtrackType
                trailerParams["subtitleLid"]=params.subtitleLid
                trailerParams["subtitleType"]=params.subtitleType
                trailerParams["elapsedTime"]=params.elapsedTime
                var traileraction="restart"
                if(params.elapsedTime>0)
                    traileraction="resume"
                var vodPlayType
                vodPlayType=(viewHelper.getVodType()==1)?"trailer":(viewHelper.getVodType()==0)?"tvseries":(viewHelper.getVodType()==2)?"episode":"movies"
                viewHelper.initializeVOD(traileraction,vodPlayType,trailerParams,true,true)
            }
            break;

            case "mediaPause":{
                if(viewController.getActiveScreenPopupID()>0||viewController.getActiveSystemPopupID()>0)return;
                if(widgetList.isSoundTrackSubtitleOpen()||widgetList.getisFlightInfoOpen())return;
                pif.vod.pause()
            }
            break;

            case "mediaResume":{
                if(viewController.getActiveScreenPopupID()>0||viewController.getActiveSystemPopupID()>0)return;
                if(widgetList.isSoundTrackSubtitleOpen()||widgetList.getisFlightInfoOpen())return;
                pif.vod.resume()
            }
            break;

            case "mediaFastForward":{
                if(viewController.getActiveSystemPopupID()>0)return;
                if(widgetList.isSoundTrackSubtitleOpen()||widgetList.getisFlightInfoOpen())return;
                pif.vod.forward();
            }
            break;

            case "mediaRewind":{
                if(viewController.getActiveSystemPopupID()>0)return;
                if(widgetList.isSoundTrackSubtitleOpen()||widgetList.getisFlightInfoOpen())return;
                pif.vod.rewind();
            }
            break;

            case "videoStop":{
                if(viewController.getActiveSystemPopupID()>0)return;
                if(widgetList.isSoundTrackSubtitleOpen()||widgetList.getisFlightInfoOpen())return;
                widgetList.actionOnVOD(true)

            }
            break;


            /****************AOD*****************/
            case "mediaShuffle":{
                pif.lastSelectedAudio.setMediaShuffle(params.mediaShuffle);
                console.log("mediaShuffle "+params.mediaShuffle)

            }
            break;

            case "playAod":{
                core.debug("playAod "+params.mid)
                viewHelper.karmaCurrentPlayinMid=params.mid
                if(params.aggregateMid==-1 || params.aggregateMid==-2 || params.aggregateMid==-3)
                    karmaAodCallBack(pif.aodPlaylist.getPlaylistModel())
                else  core.dataController.getMediaByMid([params.aggregateMid],getAlbum)

            }
            break;

            case "userDefinedAudioBroadcastData":{
                viewHelper.aodBroadcastMid=params.mid
                viewHelper.radioAlbumSelected=params.playingIndex
                core.dataController.getMediaByMid([params.mid],karmaAodBroadCastCallBk)
            }
            break;

            case "launchApp":{
                if(widgetList.isFlightMapOn()){widgetList.showFlightMap(false)}
                viewHelper.isLaunchingAppFromKarma=true;
                viewHelper.isLaunchingAppIdFromKarma=params.launchAppId
                if(widgetList.getVideoRef().visible){
                    pif.vod.stop()
                }else{
                    viewHelper.gameMid=params.launchAppId;
                    core.dataController.getMediaByMid([params.launchAppId],viewHelper.karmaGameCallbk)
                }
            }
            break;
            case "userDefinedSetAudioGamesMusic":{
                viewHelper.isMusicGameAudioDetails=params.musicGameAudio
            }
            break;
            case "userDefinedRadioPlay":{
                viewHelper.trackStop=params.flag;
            }
            break;
            case "userDefinedSetDim":{
                viewHelper.setDimLevel(params['chVal']);
            }
            break;
            case "userDefinedBacklightOff":{
                if(pif.getBacklightState()) pif.setBacklightOff();
            }
            break;
            default :
                core.debug("Api does not exists "+api)
            }
        }
    }
    function getAlbum(dModel){
        dataController.getAudioTrackList([dModel.getValue(0,"mid"),,dModel.getValue(0,"rating"),99],karmaAodCallBack)
    }
    function karmaAodCallBack(dModel){
        var playingIndex=core.coreHelper.searchEntryInModel(dModel,"mid",viewHelper.karmaCurrentPlayinMid)
        viewHelper.usbTrackPlayed=false
        viewHelper.trackStop=false;
        if(pif.aod.getPlayingAlbumId() != dModel.getValue(0,"aggregate_parentmid") || pif.lastSelectedAudio!=pif.aod){
            var params=new Object()
            params={
                "amid" : dModel.getValue(playingIndex,"mid"),
                "duration" : dModel.getValue(playingIndex,"duration"),
                "elapseTimeFormat" : 6,
                "cid" :viewData.mainMenuModel.getValue((core.coreHelper.searchEntryInModel(viewData.mainMenuModel,"category_attr_template_id","music")),"cid"),
                "playIndex" : playingIndex,
                "elapseTime" : 0,
                "mediaType": "audioAggregate",
                "repeatType":pif.aod.getMediaRepeat(),
                "playType":2,
                "shuffle":pif.aod.getMediaShuffle()
            }
            pif.aod.playByModel(dModel,params)
        }
        else
            pif.lastSelectedAudio.setPlayingIndex(core.coreHelper.searchEntryInModel(dModel,"mid",viewHelper.karmaCurrentPlayinMid));
    }
    function karmaAodBroadCastCallBk(dModel){
        viewHelper.karmaAodBrdcastModel=dModel
        viewHelper.playAudioBroadcast()
    }

}
