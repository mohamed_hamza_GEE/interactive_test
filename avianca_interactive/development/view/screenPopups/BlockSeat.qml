import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: thisPopup
    property variant configTag:configTool.config.seatchat.blockseat_popup;
    property variant blocklistObj:[];
    property int itemsPerRow: 2

    function init(){
        core.log("BlockSeat.qml | init called = "+activeFocus)
        blocklist_grid.setInitialBlock()
        blockBtn.forceActiveFocus()
    }

    function clearOnExit(){
        core.log("BlockSeat.qml | clearOnExit called");
    }


    function blockSeat(){
        core.log("BlockSeat.qml | blockSeat ")
        var blockArr=new Array()
        var  unblockArr=new Array();
        for(var i=0;i<blocklistObj.length;i++){
            core.debug("blocklistObj[i] = "+blocklistObj[i])
            var seat = blocklist_grid.model.get(i).seatnum
            if(blocklistObj[i]==1){
                blockArr.push(seat)
            }else{
                unblockArr.push(seat)
            }
        }
        core.debug("unblockArr = "+unblockArr.length)
        core.debug("blockArr >>>>>>>>>>>>"+blockArr.length)
        if(unblockArr.length>0)pif.seatChat.unblockSeat(unblockArr)
        if(blockArr.length>0)pif.seatChat.blockSeat(blockArr)
        viewController.hideScreenPopup();
    }

    /**************************************************************************************************************************/

    Rectangle{
        id:black_bg
        color:"#000000"
        opacity:0.4;
        width:core.width
        height:core.height

        MouseArea{
            anchors.fill: parent
            onClicked:{

            }
        }
    }
    Rectangle{
        id:blockpop_bg
        width:qsTranslate('','seatchat_blockpop_bg_w')
        height:qsTranslate('','seatchat_blockpop_bg_h')
        anchors.horizontalCenter: black_bg.horizontalCenter
        radius:parseInt(qsTranslate('','seatchat_blockpop_bg_radius'),10)
        color:configTag["bg_color"];
        anchors.top:black_bg.top
        anchors.topMargin:qsTranslate('','seatchat_blockpop_bg_tm')
        MouseArea{
            anchors.fill: parent
            onClicked:{

            }
        }
    }
    SimpleNavButton{
        id:close_button
        anchors.right: blockpop_bg.right
        anchors.rightMargin: qsTranslate('','seatchat_close_btn_rm')
        anchors.top:blockpop_bg.top
        anchors.topMargin:qsTranslate('','seatchat_close_btn_tm')
        isHighlight: activeFocus
        normImg:   viewHelper.configToolImagePath+configTag["close_btn_n"];
        highlightimg:  viewHelper.configToolImagePath+configTag["close_btn_h"];
        pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
        nav:["","","",blocklist_grid]
        onEnteredReleased:{viewController.hideScreenPopup();}

    }
    ViewText{
        id:blockpop_Title
        anchors.top: blockpop_bg.top;
        anchors.left:blockpop_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_blocktitle_txt_lm')
        anchors.topMargin:qsTranslate('','seatchat_blocktitle_txt_tm')
        width:qsTranslate('','seatchat_blocktitle_txt_w')
        height:qsTranslate('','seatchat_blocktitle_txt_h')
        varText:[configTool.language.seatchat.block_list_title,configTag["title_color"],qsTranslate('',"seatchat_blocktitle_txt_fontsize")]
    }

    ViewText{
        id:block_SubTitle
        anchors.top: blockpop_Title.bottom;
        anchors.left:blockpop_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_blocktitle_txt_lm')
        anchors.topMargin:qsTranslate('','seatchat_blockmsg_txt_tm')
        width:qsTranslate('','seatchat_blockmsg_txt_w')
        height:qsTranslate('','seatchat_blockmsg_txt_h')
        varText:[configTool.language.seatchat.block_list_desc,configTag["title_color"],qsTranslate('',"seatchat_blockmsg_txt_fontsize")]
        elide:Text.ElideRight
    }


    FocusScope{
        id:blocklist
        width: qsTranslate('','seatchat_blockseat_cont_w')
        height: qsTranslate('','seatchat_blockseat_cont_h')
        //        color:"green "
        //        opacity:.5
        anchors{left:blockpop_bg.left;top:blockpop_bg.top;topMargin:qsTranslate('','seatchat_blockseat_cont_tm');leftMargin:qsTranslate('','seatchat_blockseat_cont_lm');}


        GridView{
            id:blocklist_grid
            focus:true;
            height: 4*cellHeight;width:2*cellWidth
            cellHeight: qsTranslate('','seatchat_blockseat_h');
            cellWidth: qsTranslate('','seatchat_blockseat_w');
            clip:true;
            //            flow :GridView.TopToBottom
            interactive: (contentHeight>height);
            model:pif.seatChat.buddyListModel

            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    blocklist_grid.setBlock(currentIndex)
                }

            }

            Keys.onPressed: {
                switch(event.key){
                case(Qt.Key_Left):{
                    if((blocklist_grid.currentIndex%itemsPerRow) == 0){
                        blocklist_grid.focus=false;
                    }  else blocklist_grid.moveCurrentIndexLeft();
                    event.accepted = true;
                }
                break;

                case(Qt.Key_Right):  {
                    if(((blocklist_grid.currentIndex%itemsPerRow+1) == itemsPerRow)){
                        if(slider.visible){
                         slider.forceActiveFocus()
                        }else{
                        blocklist_grid.focus=true;
                        }
                    } else if((blocklist_grid.currentIndex==blocklist_grid.count-1)){
                        blocklist_grid.moveCurrentIndexUp();
                        blocklist_grid.moveCurrentIndexRight();
                    }else blocklist_grid.moveCurrentIndexRight();
                    event.accepted = true;
                }
                break;

                case(Qt.Key_Up):{
                    switch(blocklist_grid.currentIndex)
                    {
                    case 0:
                    case 1: {
                        close_button.focus=true;
                    }
                    break;

                    default:{
                        blocklist_grid.moveCurrentIndexUp();
                    }break;
                    }
                    event.accepted = true;
                }
                break;

                case(Qt.Key_Down):{
                    if(((blocklist_grid.currentIndex+itemsPerRow) == blocklist_grid.count ||(blocklist_grid.currentIndex+itemsPerRow-1) == blocklist_grid.count  )){
                        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                        blockBtn.forceActiveFocus()
                    }else {
                        blocklist_grid.moveCurrentIndexDown();
                        console.log("8888888888888888888888888888888888888888888888888888888888888888888888888 ")
                    }
                    event.accepted = true;
                }
                break;
                }
            }

            function setBlock(index){
                core.info("BlockSeat |setBlock")
                //                if(blockstatus==undefined || blockstatus == -1){
                //                    blockstatus=pif.seatChat.buddyListModel.get(index).blockstatus;
                //                }
                var temp=blocklistObj;
                core.info("BlockSeat | b4 | setBlock | temp["+index+"] = "+temp[index])
                if(temp[index]==0){ temp[index]=1}
                else{ temp[index]=0}
                core.info("BlockSeat | setBlock | temp["+index+"] = "+temp[index])
                blocklistObj=[]
                blocklistObj=temp;
                core.info("BlockSeat | setBlock | blocklistObj["+index+"] = "+blocklistObj[index])
            }
            function setInitialBlock(){
                core.info("BlockSeat |setInitialBlock")
                var temp=[]
                core.info("BlockSeat | setInitialBlock | temp =>>>>>>>> "+temp.length)
                for(var i=0;i<pif.seatChat.buddyListModel.count;i++){
                    console.log("BlockSeat | setInitialBlock | index "+ i +" pif.seatChat.blockListModel.get(i).blockedstatus "+pif.seatChat.buddyListModel.get(i).blockstatus)
                    temp.push(pif.seatChat.buddyListModel.get(i).blockstatus)
                }
                blocklistObj=temp
                core.info("BlockSeat | setInitialBlock | blocklistObj = "+blocklistObj.length)
                blocklist_grid.delegate=blockdelegate;
            }

            Component{
                id:blockdelegate
                Rectangle{
                    id:block_list
                    height:qsTranslate('','seatchat_blockseat_h')
                    width: qsTranslate('','seatchat_blockseat_w')
                    color:"transparent"
                    onActiveFocusChanged: if(activeFocus)ccbox.forceActiveFocus()
//                                        opacity:.5

                    ToggleButton{
                        id:ccbox
                        externalCstate:true
                        cstate:blocklistObj[index]==1?true:false
                        normImg1:viewHelper.configToolImagePath+configTag["tick_box_n"]
                        normImg2:viewHelper.configToolImagePath+configTag["check_box_n"]
                        highImg1:viewHelper.configToolImagePath+configTag["tick_box_h"]
                        highImg2:viewHelper.configToolImagePath+configTag["check_box_h"]
                        pressImg1:viewHelper.configToolImagePath+configTag["check_box_p"]
                        pressImg2:viewHelper.configToolImagePath+configTag["tick_box_p"]
                        anchors.left:block_list.left
                        isHighlight:(blocklist_grid.currentIndex==index&&blocklist_grid.activeFocus)
                        onCstateChanged: {
                            console.log(">>>>>>>>>>>>>>>>>>>>>> cstate = "+cstate +"index = "+index)
                        }





                    }

                    ViewText{
                        id:seatno_txt
                        width:qsTranslate('','seatchat_seatno_txt_w');
                        //                        height:qsTranslate('','seatchat_seatno_txt_h');
                        anchors.left:block_list.left
                        anchors.leftMargin: qsTranslate('','seatchat_seatno_txt_lm');
                        //                        anchors.verticalCenter: parent.verticalCenter
                        varText: [seatnum+(seatname.length>0?(" | "+seatname):""),"white",qsTranslate('','seatchat_seatno_txt_fontsize')];

                    }
                    MouseArea{
                        anchors.fill:parent;
                        onClicked:{
                            blocklist_grid.currentIndex=index;
                            blocklist_grid.setBlock(index)

                        }
                    }

                }
            }

            Component.onCompleted: {
                console.log("Component.onCompleted ")
                for (var i=0 ;i<pif.seatChat.buddyListModel.count; i++){
                    console.log("Component.onCompleted |seatnum = "+pif.seatChat.buddyListModel.get(i).seatnum)
                    console.log("Component.onCompleted |seatname = "+pif.seatChat.buddyListModel.get(i).seatname)
                }
            }
        }
    }
    Scroller{
        id:slider
        anchors.top:blockpop_bg.top;
        anchors.topMargin: qsTranslate('','seatchat_blockslider_tm')
//        anchors.verticalCenter: blockpop_bg.verticalCenter
        anchors.right: blockpop_bg.right ;anchors.rightMargin: qsTranslate('','seatchat_blockslider_rm')
        width: qsTranslate('','seatchat_blockslider_w')
        height: qsTranslate('','seatchat_blockslider_h')
        targetData:blocklist_grid//visible?synopsisContentFlickable:null
        sliderbg:[qsTranslate('','seatchat_blockslider_base_margin'),qsTranslate('','seatchat_blockslider_w'),qsTranslate('','seatchat_blockslider_base_h'),configTag["scroll_base"]]
        scrollerDot:configTag["scroll_button"]
        downArrowDetails:  [configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
        upArrowDetails: [configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
        visible:blocklist_grid.contentHeight>blocklist_grid.height
        onSliderNavigation: {
            if(direction=="down" ){
                blockBtn.forceActiveFocus()
            }else if(direction=="up")
                close_button.forceActiveFocus()
            else if(direction=="right" || direction=="left"){
                 blocklist_grid.forceActiveFocus()
            }
        }
    }


    SimpleNavBrdrBtn{    // exit chat session
        id:blockBtn
        property string textColor: blockBtn.activeFocus?configTool.config.seatchat.chatroom.btn_text_h:(blockBtn.isPressed?configTool.config.seatchat.chatroom.btn_text_p:configTool.config.seatchat.chatroom.btn_text_n)
        anchors.top:blockpop_bg.top
        anchors.topMargin: qsTranslate('','seatchat_blockbtn_tm')
        anchors.right: blockpop_bg.right
        anchors.rightMargin: qsTranslate('','seatchat_blockbtn_rm')
        width: parseInt(qsTranslate('','seatchat_blockbtn_w'),10)
        btnTextWidth:parseInt(qsTranslate('',"seatchat_blockbtn_w"),10);
        buttonText: [configTool.language.seatchat.block,blockBtn.textColor,qsTranslate('','seatchat_blockbtn_fontsize')]
        isHighlight: activeFocus
        normalImg: viewHelper.configToolImagePath+configTag["btn_n"]
        highlightImg: viewHelper.configToolImagePath+configTag["btn_h"]
        pressedImg: viewHelper.configToolImagePath+configTag["btn_p"]
        normalBorder: [qsTranslate('','seatchat_blockbtn_margin')]
        highlightBorder: [qsTranslate('','seatchat_blockbtn_margin')]
        onEnteredReleased: {
            blockSeat()
        }
        nav:["",blocklist_grid,"",""]
        onNoItemFound: {

        }
    }
}
