// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import Qt 4.7
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:exitVod
    property variant configPopupVod:  configTool.config.popup.generic
    /**************************************************************************************************************************/
    function load(){
        push(compAudioGameLaunch,'objAudioGames')
    }
    function init(){
        objAudioGames.firstBtn.forceActiveFocus()
    }
    /**************************************************************************************************************************/
    Rectangle{
        anchors.fill: parent
        color:"black"
        opacity: 0.4
        MouseArea{
            anchors.fill: parent
            onPressed: {}
            onReleased: {}
        }
    }
    property QtObject objAudioGames
    Component{
        id:compAudioGameLaunch
        Rectangle {
            id:audioGameFS
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top:parent.top
            anchors.topMargin: qsTranslate('','popups_resume_bg_tm')
            width: qsTranslate('','popups_resume_bg_w')
            height: qsTranslate('','popups_resume_bg_h')
            color:configPopupVod.bg_color
            radius: qsTranslate('','popups_resume_bg_radius')
            property alias firstBtn: firstBtn
            ViewText{
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','popups_resume_title_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                width:  qsTranslate('','popups_resume_title_w')
                height:  qsTranslate('','popups_resume_title_h')
                varText: [configTool.language.games.gameaudiomsg,configPopupVod.text_color,qsTranslate('','popups_resume_title_fontsize')]
                wrapMode: Text.Wrap
            }

            //resume and restart swapped because of customer request

            SimpleNavBrdrBtn{
                id:firstBtn
                property string textColor: firstBtn.activeFocus?configPopupVod.btn_text_h:(firstBtn.isPressed?configPopupVod.btn_text_p:configPopupVod.btn_text_n)
                width: qsTranslate('','popups_resume_btn_w')
                height: qsTranslate('','popups_resume_btn_h')
                anchors.bottom:parent.bottom
                anchors.bottomMargin: qsTranslate('','popups_resume_btn_bm')
                anchors.left:parent.left
                anchors.leftMargin:  qsTranslate('','popups_resume_btn_lm')
                isHighlight: activeFocus
                btnTextWidth:parseInt(qsTranslate('',"popups_resume_btntxt_w"),10);
                buttonText: [configTool.language.games.gamebtntxt,firstBtn.textColor,qsTranslate('','popups_resume_btntxt_fontsize')]
                normalImg: viewHelper.configToolImagePath+configPopupVod.btn_n
                highlightImg: viewHelper.configToolImagePath+configPopupVod.btn_h
                pressedImg: viewHelper.configToolImagePath+configPopupVod.btn_p
                normalBorder:[qsTranslate('','common_border_margin')]
                highlightBorder: [qsTranslate('','common_border_margin')]
                nav:["","",secondBtn,""]
                onEnteredReleased: {
					widgetList.showAppLoading(true);
					viewHelper.launchGamesonSelection(false);
					viewController.hideScreenPopup();
				}
            }
            SimpleNavBrdrBtn{
                id:secondBtn
                property string textColor: secondBtn.activeFocus?configPopupVod.btn_text_h:(secondBtn.isPressed?configPopupVod.btn_text_p:configPopupVod.btn_text_n)
                width: qsTranslate('','popups_resume_btn_w')
                height: qsTranslate('','popups_resume_btn_h')
                anchors.bottom:parent.bottom
                anchors.bottomMargin: qsTranslate('','popups_resume_btn_bm')
                anchors.left:firstBtn.right
                anchors.leftMargin:  qsTranslate('','popups_resume_btn_hgap')
                isHighlight: activeFocus
                btnTextWidth:parseInt(qsTranslate('',"popups_resume_btntxt_w"),10);
                buttonText: [configTool.language.games.musicbtntxt,secondBtn.textColor,qsTranslate('','popups_resume_btntxt_fontsize')]
                normalImg: viewHelper.configToolImagePath+configPopupVod.btn_n
                highlightImg: viewHelper.configToolImagePath+configPopupVod.btn_h
                pressedImg: viewHelper.configToolImagePath+configPopupVod.btn_p
                normalBorder: [qsTranslate('','common_border_margin')]
                highlightBorder: [qsTranslate('','common_border_margin')]
                nav:[firstBtn]
                onEnteredReleased: {
					widgetList.showAppLoading(true);
					viewHelper.launchGamesonSelection(true);
					viewController.hideScreenPopup();
				}
            }
        }
    }
}
