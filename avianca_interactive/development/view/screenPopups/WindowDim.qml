import QtQuick 1.0
import "../../framework/viewcontroller/"
import "../components/"

SmartLoader{
    id:windowDimPopup;

    property variant configTag:configTool.config.popup.generic;
    property variant wdConfigTag:configTool.config.popup.dim;
    property variant wdLabelTag:core.configTool.language.window_dimming;
    property int minLevel:viewHelper.windowMinLevel;
    property int maxLevel:viewHelper.windowMaxLevel;
    property int currLevel:viewHelper.windowCurLevel;

    function load(){}
    function init(){
        close.forceActiveFocus();
        console.log('windowDimPopup.qml | dim levels | min level : '+minLevel+'| max level : '+maxLevel +'| current Level : '+currLevel);
    }
    function clearOnExit(){
        viewController.getActiveScreenRef().forceActiveFocus();
    }
    function reload(){}

    function setDimLevel(val){
        var dimLvl = currLevel + (val)*minLevel;
        console.log('WindowDim.qml|DimLevel Changed||dim Level: '+dimLvl);
        pif.windowDimmable.setDimLevel(dimLvl);
    }

    Rectangle{
        color:"#000000"
        opacity:0.4;
        anchors.fill:parent;
        MouseArea{
            anchors.fill: parent;
            onClicked:{}
        }
    }

    Rectangle{
        id:popUpBg
        width:qsTranslate('','popups_dim_bg_w');
        height:qsTranslate('','popups_dim_bg_h');
        radius:qsTranslate('','popups_dim_bg_radius');
        color:configTag["bg_color"];
        anchors.horizontalCenter:parent.horizontalCenter;
        anchors.top: parent.top;
        anchors.topMargin: qsTranslate('','popups_dim_bg_tm');

        SimpleNavButton{
            id:close;
            normImg:viewHelper.configToolImagePath+wdConfigTag["close_btn_n"];
            highlightimg:viewHelper.configToolImagePath+wdConfigTag["close_btn_h"];
            pressedImg: viewHelper.configToolImagePath+wdConfigTag["close_btn_p"];
            anchors.right:parent.right;
            anchors.rightMargin: qsTranslate('',"popups_dim_close_rm");
            anchors.top:parent.top;
            anchors.topMargin: qsTranslate('',"popups_dim_close_tm");
            nav:["","","",dimPlus]
            onEnteredReleased:{
                viewController.hideScreenPopup();
                if(widgetList.isVideoVisible()){
                    widgetList.actionOnVOD(false);
                }
            }
        }
        ViewText{
            id:titleTxt
            width:qsTranslate('','popups_dim_title_w');
            height:qsTranslate('','popups_dim_title_h');
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','popups_dim_title_tm');
            anchors.left: parent.left
            anchors.leftMargin: qsTranslate('','popups_dim_title_lm');
            lineHeight: qsTranslate('','popups_dim_title_lh');
            maximumLineCount: 2;
            wrapMode: Text.WordWrap;
            varText:[wdLabelTag["windowdimming_title"],configTag["text_color"],qsTranslate('',"popups_dim_title_fontsize")]

        }
        ViewText{
            id:descTxt
            width:qsTranslate('','popups_dim_desc_w');
            height:qsTranslate('','popups_dim_desc_h');
            anchors.top:titleTxt.bottom;
            anchors.topMargin: qsTranslate('','popups_dim_desc_tm');
            anchors.left: titleTxt.left;
            lineHeight: qsTranslate('','popups_dim_desc_lh');
            lineHeightMode: Text.FixedHeight;
            maximumLineCount: 5;
            wrapMode: Text.Wrap;
            varText:[wdLabelTag["windowdimming_description"] ,configTag["text_color"],qsTranslate('',"popups_dim_desc_fontsize")]
        }

        Item{
            id:controlls
            width: controllBG.width;
            height:controllBG.height;
            anchors.horizontalCenter: parent.horizontalCenter;
            anchors.bottom: parent.bottom;
            anchors.bottomMargin: qsTranslate('','popups_dim_btnbg_bm');

            Image{
                id:controllBG
                source:viewHelper.configToolImagePath+wdConfigTag["btn_bg"];
            }

            SimpleNavButton{
                id:dimMinus;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left;
                anchors.leftMargin: qsTranslate('','popups_dim_btnbg_btn_hgap');
                normImg:viewHelper.configToolImagePath+wdConfigTag["minus_n"];
                highlightimg:viewHelper.configToolImagePath+wdConfigTag["minus_h"];
                pressedImg: viewHelper.configToolImagePath+wdConfigTag["minus_p"];
                nav:["",close,dimPlus,""]
                onEnteredReleased:{
                    if(currLevel > minLevel) {
                        setDimLevel(-1);
                    }
                }
            }

            SimpleNavButton{
                id:dimPlus;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.right: parent.right;
                anchors.rightMargin: qsTranslate('','popups_dim_btnbg_btn_hgap');
                normImg:viewHelper.configToolImagePath+wdConfigTag["plus_n"];
                highlightimg:viewHelper.configToolImagePath+wdConfigTag["plus_h"];
                pressedImg: viewHelper.configToolImagePath+wdConfigTag["plus_p"];
                nav:[dimMinus,close,"",""]
                onEnteredReleased:{
                    if(currLevel <maxLevel) {
                        setDimLevel(1);
                    }
                }
            }
        }
    }
}
