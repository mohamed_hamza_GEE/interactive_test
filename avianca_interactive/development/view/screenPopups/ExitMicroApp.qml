import QtQuick 1.0
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:exitVod
    property variant configPopupVod:  configTool.config.popup.generic
     /**************************************************************************************************************************/
    function load(){
        push(compExitVod,'objexitvod')
    }
    function init(){
        objexitvod.firstBtn.forceActiveFocus()
    }
    onActiveFocusChanged: {
        console.log("onActiveFocusChanged  | activeFocus = "+activeFocus)
        console.log("onActiveFocusChanged  | visible = "+visible)
        if(!activeFocus && visible){
            console.log("onActiveFocusChanged  | systemPopups = "+exitVod.activeFocus)
           delay_activeFocus.restart();
            console.log("onActiveFocusChanged  | systemPopups >>>>>>>>>>= "+exitVod.activeFocus)
        }
    }
    Timer{
        id:delay_activeFocus
        interval: 300
        onTriggered: {
            exitVod.forceActiveFocus();
            console.log("onActiveFocusChanged  | systemPopups >>>>>>>>>>= "+exitVod.activeFocus)
        }
    }

     /**************************************************************************************************************************/
    Rectangle{
        color:"#000000"
        opacity:0.4;
        anchors.fill:parent;
        MouseArea{
            anchors.fill: parent
            onClicked:{

            }
        }
    }

    property QtObject objexitvod
    Component{
        id:compExitVod
        Rectangle {
            id:exitVodFS
            property alias firstBtn: firstBtn
            anchors.centerIn: parent
            width: qsTranslate('','popups_vodexit_bg_w')
            height: qsTranslate('','popups_vodexit_bg_h')
            color:configPopupVod.bg_color
            radius: qsTranslate('','popups_vodexit_bg_radius')
            ViewText{
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','popups_vodexit_title_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                width:  qsTranslate('','popups_vodexit_title_w')
                height:  qsTranslate('','popups_vodexit_title_h')
                varText: [configTool.language.movies.do_you_want_to_exit,configPopupVod.text_color,qsTranslate('','popups_vodexit_title_fontsize')]
            }
            SimpleNavBrdrBtn{
                id:firstBtn
                property string textColor: firstBtn.activeFocus?configPopupVod.btn_text_h:(firstBtn.isPressed?configPopupVod.btn_text_p:configPopupVod.btn_text_n)
                width: qsTranslate('','popups_vodexit_btn_w')
                height: qsTranslate('','popups_vodexit_btn_h')
                anchors.bottom:parent.bottom
                anchors.bottomMargin: qsTranslate('','popups_vodexit_btn_bm')
                anchors.left:parent.left
                anchors.leftMargin:  qsTranslate('','popups_vodexit_btn_lm')
                isHighlight: activeFocus
                btnTextWidth:parseInt(qsTranslate('',"popups_vodexit_btntxt_w"),10);
                buttonText: [configTool.language.global_elements.yes,firstBtn.textColor,qsTranslate('','popups_vodexit_btntxt_fontsize')]
                normalImg: viewHelper.configToolImagePath+configPopupVod.btn_n
                highlightImg: viewHelper.configToolImagePath+configPopupVod.btn_h
                pressedImg: viewHelper.configToolImagePath+configPopupVod.btn_p
                normalBorder: [qsTranslate('','common_border_margin')]
                highlightBorder: [qsTranslate('','common_border_margin')]
                nav:["","",secondBtn,""]
                onEnteredReleased: viewController.getActiveScreenRef().actionOnExit(true);
            }
            SimpleNavBrdrBtn{
                id:secondBtn
                property string textColor: secondBtn.activeFocus?configPopupVod.btn_text_h:(secondBtn.isPressed?configPopupVod.btn_text_p:configPopupVod.btn_text_n)
                width: qsTranslate('','popups_vodexit_btn_w')
                height: qsTranslate('','popups_vodexit_btn_h')
                anchors.bottom:parent.bottom
                anchors.bottomMargin: qsTranslate('','popups_vodexit_btn_bm')
                anchors.left:firstBtn.right
                anchors.leftMargin:  qsTranslate('','popups_vodexit_btn_hgap')
                isHighlight: activeFocus
                btnTextWidth:parseInt(qsTranslate('',"popups_vodexit_btntxt_w"),10);
                buttonText: [configTool.language.global_elements.no,secondBtn.textColor,qsTranslate('','popups_vodexit_btntxt_fontsize')]
                normalImg: viewHelper.configToolImagePath+configPopupVod.btn_n
                highlightImg: viewHelper.configToolImagePath+configPopupVod.btn_h
                pressedImg: viewHelper.configToolImagePath+configPopupVod.btn_p
                normalBorder:[qsTranslate('','common_border_margin')]
                highlightBorder: [qsTranslate('','common_border_margin')]
                nav:[firstBtn]
                onEnteredReleased: viewController.getActiveScreenRef().actionOnExit(false);
            }
        }
    }
}
