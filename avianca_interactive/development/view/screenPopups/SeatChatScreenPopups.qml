import QtQuick 1.0
import "../components/"
SeatChatPopup{
    property string title;
    property bool titleupdate:true;
    property int screenpopupId: viewController.getActiveScreenPopupID()
    //    noOfButtons:3;
    // visible: false;
    Connections{
        target: core;
        onSigLanguageChanged:{
            updateTitle()
        }
    }

    bgDetails:[(screenpopupId !=17 || screenpopupId !=22)?qsTranslate('','popups_chatpop_bg_tm1'):qsTranslate('','popups_chatpop_bg_tm'),
                                                                            (screenpopupId !=17 || screenpopupId !=22)? qsTranslate('','popups_chatpop_bg_w1'): qsTranslate('','popups_chatpop_bg_w'),
                                                                                                                                         (screenpopupId ==15||screenpopupId ==18 ||screenpopupId ==16)?qsTranslate('','popups_chatpop_bg_h1'):qsTranslate('','popups_chatpop_bg_h'),
                                                                                                                                                                                                    configTag["bg_color"],
                                                                                                                                                                                                    qsTranslate('','popups_chatpop_bg_radius')
    ]
    titleDetails:[
        qsTranslate('','popups_chatpop_title_tm'),
        qsTranslate('','popups_chatpop_title_w'),
        (screenpopupId ==17)?qsTranslate('','popups_chatpop_title_h'):qsTranslate('','popups_chatpop_title_h1'),
                            title,
                                                configTool.config.music.popup["title_color"],
                                                qsTranslate('','popups_chatpop_title_fontsize'),
                                                "AlignHCenter",
                                                qsTranslate('','popups_chatpop_title_lh')
    ]

    descDetails:[0,0,0,"","transparent",0,0]

    buttonDetails:[
        qsTranslate('','popups_chatpop_btn_bm'),
        parseInt(qsTranslate('','popups_chatpop_btn_w'),10),
        qsTranslate('','popups_chatpop_btn_h'),
        ((screenpopupId ==15||screenpopupId ==16 ||screenpopupId ==22)?parseInt(qsTranslate('','popups_chatpop_btn_hgap1'),10):parseInt(qsTranslate('','popups_chatpop_btn_hgap'),10)),
                             qsTranslate('','popups_chatpop_btn_margin'),
                             "btn",
                             (screenpopupId ==16   || screenpopupId ==22 || screenpopupId ==15)?parseInt(qsTranslate('','popups_chatpop_btn_lm1'),10):(parseInt(qsTranslate('','popups_chatpop_bg_w'),10)/2)
    ]

    buttonTextDetails:[
        qsTranslate('','popups_chatpop_btntxt_w'),
        "btn_text",
        qsTranslate('','popups_chatpop_btntxt_fontsize')
        //        configTool.language.global_elements.ok
        /*,w,text,color,fs*/
    ]

    onButtonSelect:{
      if(screenpopupId ==16){
          closePopup()
            if(btnId==0){
                viewHelper.exitseatChat()
            }

        }else if(screenpopupId ==15){
            if(btnId==0){
                closePopup()
//                viewController.updateSystemPopupQ(18,true)  // changed to screen popup
                viewController.showScreenPopup(18);
            }else{closePopup()}

        }else if(screenpopupId ==18){
            if(btnId==0){
                //                viewController.updateSystemPopupQ(14,true)
                var blockArr = new Array();
                blockArr[0]=viewHelper.singleBlockSeat;
                pif.seatChat.blockSeat(blockArr);
//                pif.seatChat.declineInvitation(viewHelper.cSessionID,viewHelper.tSeatNo)
                core.debug( "In SeatChatPopups | SeatChat single user in session Pop up Blocked "+blockArr)
                //                pif.seatChat.exitChatSession(viewHelper.cSessionID)
                viewHelper.toblockExitSingleBudddy()
            }
            closePopup()
        }else if(screenpopupId ==21){
            closePopup()
//            viewController.updateSystemPopupQ(22,true) //changed to sreen popup
//            viewController.showScreenPopup(22);
        }else if(screenpopupId ==22) {
            if(btnId==0){
                viewHelper.toblockExitSingleBudddy()
                closePopup()
            }else{
                closePopup()
            }
        }else{
//            viewController.updateSystemPopupQ(screenpopupId ,false);
             viewController.hideScreenPopup();
        }
    }
    function closePopup(){
//        viewController.updateSystemPopupQ(screenpopupId ,false);
         viewController.hideScreenPopup();
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendApiData("userDefinedUsbHide",{device:"usb"})
    }

    function updateTitle(){
        if(screenpopupId ==15){
              title=configTool.language.seatchat.block_confirmation;
              noOfButtons=2;
              buttonTextDetails=[
                          qsTranslate('','popups_resume_btntxt_w'),
                          "btn_text",
                          qsTranslate('','popups_resume_btntxt_fontsize'),
                          configTool.language.global_elements.yes,
                          configTool.language.global_elements.no

                      ]
          }else if(screenpopupId ==16){
              title=configTool.language.seatchat.leave_chat_confirmation;
              noOfButtons=2;
              buttonTextDetails=[
                          qsTranslate('','popups_resume_btntxt_w'),
                          "btn_text",
                          qsTranslate('','popups_resume_btntxt_fontsize'),
                          configTool.language.global_elements.yes,
                          configTool.language.global_elements.cancel

                      ]
          }else if(screenpopupId ==16){
              title=configTool.language.seatchat.leave_chat_confirmation;
              noOfButtons=2;
              buttonTextDetails=[
                          qsTranslate('','popups_resume_btntxt_w'),
                          "btn_text",
                          qsTranslate('','popups_resume_btntxt_fontsize'),
                          configTool.language.global_elements.yes,
                          configTool.language.global_elements.cancel

                      ]
          }else if(screenpopupId ==22){
              title=configTool.language.seatchat.chatsession_exit_text
              noOfButtons=2;
              buttonTextDetails=[
                          qsTranslate('','popups_resume_btntxt_w'),
                          "btn_text",
                          qsTranslate('','popups_resume_btntxt_fontsize'),
                          configTool.language.global_elements.yes,
                          configTool.language.global_elements.no

                      ]

          }else{
              if(screenpopupId ==21){
                  title=configTool.language.seatchat.session_limit_message
              }else if(screenpopupId ==17){
                  var chatSent=configTool.language.seatchat.invitation_sent+" ";
                  title=chatSent.replace("XX"," "+viewHelper.tSeatNo+" ")
              }else if(screenpopupId ==18){
                  title = configTool.language.seatchat.block_completed;
              }else if(screenpopupId ==19){
                  title=configTool.language.seatchat.multiple_invitation_sent+viewHelper.tSeatNo.join(", ");

              }else if(screenpopupId ==20){
                  title = configTool.language.seatchat.session_limit_message;
              }
              noOfButtons=1;
              buttonTextDetails=[
                          qsTranslate('','popups_resume_btntxt_w'),
                          "btn_text",
                          qsTranslate('','popups_resume_btntxt_fontsize'),
                          configTool.language.global_elements.ok

                      ]
          }
    }

    function init(){

        console.log("SEATCHAT  POPUP viewController.getActivescreenpopupId () = "+screenpopupId )
        buttonView.currentIndex=0
        screenpopupId =viewController.getActiveScreenPopupID()
        updateTitle()

        visible=true;
        forceActiveFocus();
    }

    function clearOnExit(){
//         core.debug("SeatChatPopups.qml| clearOnExit | pif.seatChat.pendingInvitationList before  = "+pif.seatChat.pendingInvitationList.count)
//        viewHelper.cSessionID=pif.seatChat.pendingInvitationList.getValue(0,"session")
//        viewHelper.tSeatNo=pif.seatChat.pendingInvitationList.getValue(0,"seat")
//        console.log(">>>>>>>> viewHelper.cSessionID "+viewHelper.cSessionID)
//        pif.seatChat.removeFromQueue(pif.seatChat.pendingInvitationList.getValue(0,"session"))
//        if(pif.seatChat.pendingInvitationList.count>0){
//            viewHelper.popupTSeatNo=pif.seatChat.pendingInvitationList.getValue(0,"seat")
//            console.log("pif.seatChat.pendingInvitationList > 0 after = "+pif.seatChat.pendingInvitationList.count)
//            console.log("viewHelper.cSessionID  = "+viewHelper.cSessionID)
//            console.log("viewHelper.tSeatNo  = "+viewHelper.tSeatNo)
//            //                    viewController.updateSystemPopupQ(13,false)
//            delay_Invite_queue.restart()
//        }else delay_Invite_queue.stop()
//         core.debug("SeatChatPopups.qml| clearOnExit pif.seatChat.pendingInvitationList = "+pif.seatChat.pendingInvitationList.count)
    }

//    Timer{
//        id:delay_Invite_queue;
//        running: false
//        interval:3000;
//        onTriggered:{
//            viewController.updateSystemPopupQ(13,true)
//        }
//    }


}


