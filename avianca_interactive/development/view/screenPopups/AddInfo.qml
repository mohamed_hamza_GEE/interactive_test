import QtQuick 1.0
import "../../framework/viewcontroller"
import "../components"
import ImageMask 1.0


SmartLoader{
    id:addInfo
    property variant configPopupVod:  configTool.config.popup.generic
    property string langCode : core.settings.languageISO;

    /**************************************************************************************************************************/
    Connections{
        target: configTool
        onMicroAppXMLChangedFromThemeMgr:{
            reload();
        }
    }
    function load(){
        push(compAddInfo,'objaddInfo')
    }
    function reload(){
        addTextInfo.source = viewHelper.addInfoXMLSource;
        addListInfoModel.source = viewHelper.addInfoXMLSource;
        addTextInfo.reload();
        addListInfoModel.reload();
    }
    function setHeadingText(obj,dmodel,ind,modelName){
        var modelName_cdata = modelName+"_cdata";
        obj.text = (eval("dmodel.get("+ind+")."+modelName_cdata) !== "") ? eval("dmodel.get("+ind+")."+modelName_cdata) : eval("dmodel.get("+ind+")."+modelName);
        obj.color = configTool.config.lifemiles.screen.text_title;
        obj.font.pixelSize = qsTranslate('','lifemiles_popup_fontsize');
    }
    function setHeadingInfoText(obj,dmodel,ind,modelName){
        var modelName_cdata = modelName+"_cdata";
        obj.text = (eval("dmodel.get("+ind+")."+modelName_cdata) !== "") ? eval("dmodel.get("+ind+")."+modelName_cdata) : eval("dmodel.get("+ind+")."+modelName);
        obj.color = configTool.config.lifemiles.screen.text_title_info;
        obj.font.pixelSize = qsTranslate('','lifemiles_popup_fontsize1')
    }
    function init(){
        console.log("AddInfo.qml | init ");
        //        objaddInfo.scroller.targetData = objaddInfo.allianceGridIcon;
        addTextInfo.source = viewHelper.addInfoXMLSource;
        addListInfoModel.source = viewHelper.addInfoXMLSource;

        objaddInfo.allianceGridIcon.model = addListInfoModel;

        if(objaddInfo.scroller.visible){
            objaddInfo.scroller.forceActiveFocus();
        }else{
            objaddInfo.appCloseBtn.forceActiveFocus();
        }
        objaddInfo.allianceGridIcon.delegate=gridDelegate
    }
    function clearOnExit(){
        console.log("AddInfo.qml | clearOnExit ");
        //        objaddInfo.scroller.visible = false
        objaddInfo.headingText.text="";
        objaddInfo.headingInfoText.text="";
        objaddInfo.allianceGridIcon.model = viewHelper.blankModel;
        objaddInfo.allianceGridIcon.delegate=blankDelegate
        addTextInfo.source = "";
        addListInfoModel.source = "";
    }
    Component{
        id:blankDelegate
        Item {}
    }
    Rectangle{
        color:"#000000"
        opacity:0.4;
        anchors.fill:parent;
        MouseArea{
            anchors.fill: parent
            onClicked:{

            }
        }
    }
    XmlListModel{
        id:addTextInfo;
        //        source: viewHelper.addInfoXMLSource;
        query: "/application/screen[@_id='"+viewHelper.addInfoscreenID+"']";

        XmlRole { name : "addTitleText"; query: "text6/"+langCode+"/@_value/string()" }
        XmlRole { name : "addTitleText_cdata"; query: "text6/"+langCode+"/string()" }
        XmlRole { name : "addTitleInfoText"; query: "text7/"+langCode+"/@_value/string()" }
        XmlRole { name : "addTitleInfoText_cdata"; query: "text7/"+langCode+"/string()" }
        onCountChanged: {
            setHeadingText(objaddInfo.headingText,addTextInfo,0,"addTitleText");
            setHeadingInfoText(objaddInfo.headingInfoText,addTextInfo,0,"addTitleInfoText");
        }
    }

    XmlListModel{
        id:addListInfoModel;
        //        source: viewHelper.addInfoXMLSource;
        query: "/application/screen[@_id='"+viewHelper.addInfoscreenID+"']/list3/listItem";

        XmlRole { name : "addImage"; query: "image3/@_value/string()" }
    }


    property QtObject objaddInfo
    Component{
        id:compAddInfo
        Image {
            id:addInfoContainer;
            property alias headingText: headingText;
            property alias headingInfoText: headingInfoText;
            property alias appCloseBtn: appCloseBtn;
            property alias scroller : scroller;
            property alias allianceGridIcon : allianceGridIcon;
            anchors.horizontalCenter: parent.horizontalCenter;
            anchors.top: parent.top;
            anchors.topMargin: qsTranslate('','lifemiles_popup_y');
            source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.popup;

            SimpleButton {
                id: appCloseBtn;
                anchors.right: addInfoContainer.right;
                anchors.top: addInfoContainer.top;
                anchors.rightMargin: qsTranslate('','lifemiles_popup_close_rm');
                anchors.topMargin: qsTranslate('','lifemiles_popup_close_tm');
                isHighlight: activeFocus
                normImg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_n;
                highlightimg: viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;
                pressedImg:viewHelper.configToolImagePath+ configTool.config.lifemiles.screen.close_h;
                onEnteredReleased: {
                    viewController.hideScreenPopup();
                }
                Keys.onDownPressed: {
                    if(scroller.visible){
                        scroller.forceActiveFocus();
                    }
                }
            }

            ViewText{
                id:headingText;
                anchors.top: addInfoContainer.top;
                anchors.topMargin: qsTranslate('','lifemiles_popup_title_tm');
                anchors.left: addInfoContainer.left;
                anchors.leftMargin: qsTranslate('','lifemiles_popup_title_lm');
                width: qsTranslate('','lifemiles_popup_title_w');
                height: qsTranslate('','lifemiles_popup_title_h');
                wrapMode: Text.Wrap;
                elide: Text.ElideRight;
                maximumLineCount:1;
                //                varText: [ (addTextInfo.get(0).addTitleText_cdata !== "") ? addTextInfo.get(0).addTitleText_cdata : addTextInfo.get(0).addTitleText,
                //                    configTool.config.lifemiles.screen.text_title,
                //                    qsTranslate('','lifemiles_popup_fontsize')
                //                ]
            }

            ViewText{
                id:headingInfoText;
                anchors.top: headingText.bottom;
                anchors.topMargin: qsTranslate('','lifemiles_popup_vgap');
                anchors.left: headingText.left;
                width: qsTranslate('','lifemiles_popup_title_w');
                height: qsTranslate('','lifemiles_popup_desc_h');
                wrapMode: Text.Wrap;
                lineHeight:qsTranslate('','lifemiles_popup_lh');
                lineHeightMode: Text.FixedHeight
                elide: Text.ElideRight;
                maximumLineCount:3;
                //                varText: [ (addTextInfo.get(0).addTitleInfoText_cdata !== "") ? addTextInfo.get(0).addTitleInfoText_cdata : addTextInfo.get(0).addTitleInfoText,
                //                    configTool.config.lifemiles.screen.text_title_info,
                //                    qsTranslate('','lifemiles_popup_fontsize1')
                //                ]
            }

            GridView{
                id: allianceGridIcon;
                anchors.top: headingInfoText.bottom;
                //                anchors.left: headingInfoText.left;
                anchors.left: parent.left;
                anchors.topMargin: qsTranslate('','lifemiles_popup_list_tm');
                anchors.leftMargin: qsTranslate('','lifemiles_popup_list_lm');
                height: qsTranslate('','lifemiles_popup_list_h');
                width: qsTranslate('','lifemiles_popup_list_w');
                cellHeight: qsTranslate('','lifemiles_popup_cell_h');
                cellWidth: qsTranslate('','lifemiles_popup_cell_w');
                //                model: addListInfoModel;
                delegate: gridDelegate;
                clip: true;
                onCountChanged:{
                    allianceGridIcon.contentY = 0;
                    if(scroller.visible){
                        scroller.forceActiveFocus();
                    }else{
                        appCloseBtn.forceActiveFocus();
                    }
                }
                Behavior on contentY {
                    NumberAnimation{duration: 300}
                }
            }
            Scroller{
                id:scroller
                height: qsTranslate('','lifemiles_popup_scroll_h');
                width: qsTranslate('','lifemiles_popup_scroll_w');
                opacity: (visible)?1:0;
                anchors.top: allianceGridIcon.top;
                anchors.right: addInfoContainer.right;
                anchors.rightMargin: qsTranslate('','lifemiles_popup_scroll_rm');
                sliderbg:[qsTranslate('','lifemiles_popup_scroll_base_margin'),qsTranslate('','lifemiles_popup_scroll_w'),qsTranslate('','lifemiles_popup_scroll_base_h'),configTool.config.lifemiles.screen.scrollbg]
                scrollerDot: configTool.config.lifemiles.screen.slider
                upArrowDetails:[configTool.config.lifemiles.screen.app_arwup_n, configTool.config.lifemiles.screen.app_arwup_h, configTool.config.lifemiles.screen.app_arwup_h ]
                downArrowDetails:[configTool.config.lifemiles.screen.app_arwdwn_n, configTool.config.lifemiles.screen.app_arwdwn_h, configTool.config.lifemiles.screen.app_arwdwn_h]
                targetData:allianceGridIcon;
                onSliderNavigation: {
                    if(direction == "up"){
                        appCloseBtn.forceActiveFocus()
                    }
                }
            }
        }
    }

    Component{
        id:gridDelegate;

        Item{
            id: cellContainer;
            height: qsTranslate('','lifemiles_popup_cell_h');
            width: qsTranslate('','lifemiles_popup_cell_w');
            Image{
                id: outline;
                anchors.centerIn: parent;
                source: viewHelper.configToolImagePath+configTool.config.lifemiles.screen.outline;

                MaskedImage{
                    id:posterIMG;
                    anchors.centerIn: parent;
                    source: viewHelper.addImagesPath + addImage;
                    //            maskSource:viewHelper.configToolImagePath+configTool.config.lifemiles.screen.outline_mask;
                    maskSource:viewHelper.configToolImagePath+ "outline_mask.png";
                }
            }
        }
    }
}
