import QtQuick 1.0
import "../components/"
QMLPopup{
    property string title;
    noOfButtons:2;
    configTag: configTool.config.music.popup
    bgDetails:[
        qsTranslate('','popups_resume_bg_tm'),
        qsTranslate('','popups_resume_bg_w'),
        qsTranslate('','popups_resume_bg_h'),
        configTool.config.popup.generic["bg_color"],
        qsTranslate('','popups_resume_bg_radius')
    ]
    titleDetails:[
        qsTranslate('','popups_resume_title_tm'),
        qsTranslate('','popups_resume_title_w'),
        qsTranslate('','popups_resume_title_h'),
        configTool.language.music.delete_track_title,
        configTag["title_color"],
        qsTranslate('','popups_resume_title_fontsize'),
        "AlignHCenter"


    ]
    descDetails:[0,0,0,"","transparent",0,0]

    buttonDetails:[
        qsTranslate('','popups_resume_btn_bm'),
        parseInt(qsTranslate('','popups_resume_btn_w'),10),
        qsTranslate('','popups_resume_btn_h'),
        qsTranslate('','popups_resume_btn_hgap'),
        qsTranslate('','popups_resume_btn_margin'),
        "empty_button"
        /*tm,w,h,hgap,margin*/
    ]
    buttonTextDetails:[
        qsTranslate('','popups_resume_btntxt_w'),
        "empty_button_text",
        qsTranslate('','popups_resume_btntxt_fontsize'),
        configTool.language.global_elements.yes,
        configTool.language.global_elements.no
        /*,w,text,color,fs*/
    ]

    onButtonSelect:{
        var retArray = [btnId]
        viewController.hideScreenPopup(retArray);
    }

    function init(){
        buttonView.currentIndex=0;
        forceActiveFocus();
    }

}

