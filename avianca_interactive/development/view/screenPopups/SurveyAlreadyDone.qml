import QtQuick 1.0
import "../components/"
QMLPopup{
    property string title;
    noOfButtons:0;
    visible: false;
    bgDetails:[
        qsTranslate('','popups_bg_tm'),
        qsTranslate('','popups_bg_w'),
        qsTranslate('','popups_bg_h'),
        configTag["bg_color"],
        qsTranslate('','popups_bg_radius')
    ]
    titleDetails:[
        0,
        qsTranslate('','popups_text_w'),
        qsTranslate('','popups_text_h'),
        "Survey Already done HardCoded",
        configTool.config.popup.generic.text_color,
        qsTranslate('','popups_text_fontsize'),
        "AlignHCenter",
        qsTranslate('','popups_text_lh'),
    ]
    descDetails:[0,0,0,"","transparent",0,0]
    function init(){
        forceActiveFocus();
    }
    function clearOnExit(){

    }

    Keys.onReleased: {
        viewController.hideScreenPopup();
    }

    MouseArea{
        anchors.fill:parent;
        onClicked:{
            viewController.hideScreenPopup();
            if(widgetList.isVideoVisible())widgetList.actionOnVOD(false)
        }
    }
}

