import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: dialpad
     property int systempopupId: viewController.getActiveScreenPopupID()

    function load(){
        push(compDialpadPopup, "dialpadPopup")
    }
    function init(){
        core.debug("Dialpad | init called | "+viewController.getActiveScreenPopupID())
//        dialpadPopup.visible=true
        systempopupId=viewController.getActiveScreenPopupID()
        dialpadPopup.init();
        dialpadPopup.setFocus();
    }
    function clearOnExit(){
        core.debug("Dialpad | clearOnExit called")
        dialpadPopup.clearAll();
    }
    /**************************************************************************************************************************/

//    CompPopup{}
    property QtObject dialpadPopup;
    Component{
        id: compDialpadPopup
        CompDialpad{
            width:core.width
            height:core.height
        }
    }
}
