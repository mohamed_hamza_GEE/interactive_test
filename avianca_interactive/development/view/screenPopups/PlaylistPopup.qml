import QtQuick 1.0
import "../../framework/viewcontroller/"
import "../components/"
SmartLoader{
    id:playlistPopup;
    property variant labelTag:core.configTool.language.music;
    property variant configTag:core.configTool.config.music.popup;
    /**************************************************************************************************************************/
    function init(){
        playlistView.currentIndex=0;
        playlistView.forceActiveFocus();
        if(viewController.getActiveScreenPopupID()==3)
            titleText.text=labelTag["add_tracks_to"]
        else titleText.text=labelTag["add_all_tracks_to"]
    }

    function clearOnExit(){
    }
    /**************************************************************************************************************************/
    Rectangle{
        color:"#000000"
        opacity:0.4;
        anchors.fill:parent;
        MouseArea{
            anchors.fill: parent
            onClicked:{

            }
        }
    }
    Rectangle{
        y:qsTranslate('','popups_addtrack_bg_tm');
        width:qsTranslate('','popups_addtrack_bg_w');
        height:qsTranslate('','popups_addtrack_bg_h');
        radius:qsTranslate('','popups_addtrack_bg_radius');
        anchors.horizontalCenter:parent.horizontalCenter;
        color:configTool.config.popup.generic.bg_color;

        ViewText{
            id:titleText
            x: qsTranslate('',"popups_addtrack_title_lm");
            y:qsTranslate('',"popups_addtrack_title_tm");
            width:qsTranslate('',"popups_addtrack_title_w");
            height:qsTranslate('',"popups_addtrack_title_h");
            varText:["",configTag["title_color"],qsTranslate('',"popups_addtrack_title_fontsize")]
             elide: Text.ElideRight
        }

        SimpleNavButton{
            id:close;
            normImg:viewHelper.configToolImagePath+configTag["close_btn_n"];
            highlightimg:viewHelper.configToolImagePath+configTag["close_btn_h"];
            pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
            anchors.right:parent.right;
            anchors.rightMargin: qsTranslate('',"popups_addtrack_close_rm")
            anchors.top:parent.top;
            anchors.topMargin: qsTranslate('',"popups_addtrack_close_tm")
            nav:["","","",playlistView]
            onEnteredReleased:{
                var retArray=[0]
                viewController.hideScreenPopup(retArray)
            }


        }

        Column{
            id:playlistView
            y:qsTranslate('','popups_addtrack_btn_tm');
            spacing:qsTranslate('','popups_addtrack_btn_vgap');
            anchors.horizontalCenter:parent.horizontalCenter;
            property int currentIndex;
            Keys.onUpPressed:{
                if(currentIndex==0){
                    close.forceActiveFocus();
                }else{
                    currentIndex--;
                }
            }
            Keys.onDownPressed:{
                if(currentIndex!=3){
                    currentIndex++;
                }
            }

            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    select()
                }
            }

            function select(){
                var retArray=[(currentIndex+1)*-1]
                viewController.hideScreenPopup(retArray)
                pif.paxus.playlistCreatedLog()
            }

            Repeater{
                model:3;
                SimpleBorderBtn{
                    id:btn;
                    normalImg:viewHelper.configToolImagePath+configTag["playlist_selection_n"];
                    width:qsTranslate('','popups_addtrack_btn_w');
                    height:qsTranslate('','popups_addtrack_btn_h');
                    normalBorder:[qsTranslate('','popups_addtrack_btn_margin')]
                    highlightBorder:normalBorder
                    highlightImg:viewHelper.configToolImagePath+configTag["playlist_selection_h"];
                    pressedImg: viewHelper.configToolImagePath+configTag["playlist_selection_p"];
                    isHighlight:(playlistView.activeFocus && index==playlistView.currentIndex)
                    btnTextWidth:qsTranslate('','popups_addtrack_btntxt_w')
                    buttonText:[labelTag["playlist"+(index+1)]+" ("+pif.aodPlaylist.getTotalTracks(((index+1)*-1))+"/99)",(isPressed?configTag["playlist_text_p"]:isHighlight?configTag["playlist_text_h"]:configTag["playlist_text_n"]),qsTranslate('','popups_addtrack_btntxt_fontsize')]
                    onEnteredReleased:{
                        playlistView.currentIndex=index;
                        playlistView.select();
                    }
                }
            }
        }
    }
}




