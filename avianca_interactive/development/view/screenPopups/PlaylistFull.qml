import QtQuick 1.0
import "../components/"
QMLPopup{
    property string title;

    noOfButtons:1;
    property variant name: ["","playlist1_is_full","playlist2_is_full","playlist3_is_full"]


    bgDetails:[
        qsTranslate('','popups_playfull_bg_tm'),
        qsTranslate('','popups_playfull_bg_w'),
        qsTranslate('','popups_playfull_bg_h'),
        configTag["bg_color"],
        qsTranslate('','popups_playfull_bg_radius')
    ]
    titleDetails:[
        qsTranslate('','popups_playfull_title_tm'),
        qsTranslate('','popups_playfull_title_w'),
        qsTranslate('','popups_playfull_title_h'),
        configTool.language.music[name[viewHelper.fullPlayId]],
        configTool.config.music.popup["title_color"],
        qsTranslate('','popups_playfull_title_fontsize')

    ]
    descDetails:[
        qsTranslate('','popups_playfull_desc_tm'),
        qsTranslate('','popups_playfull_desc_w'),
        qsTranslate('','popups_playfull_desc_h'),
        configTool.language.music.playlist_full_desc.replace("[image]","✔"),
        configTool.config.music.popup["description_color"],
        qsTranslate('','popups_playfull_desc_fontsize'),
        qsTranslate('','popups_playfull_desc_lh')

    ]
    buttonDetails:[
        qsTranslate('','popups_playfull_btn_bm'),
        parseInt(qsTranslate('','popups_playfull_btn_w'),10),
        qsTranslate('','popups_playfull_btn_h'),
        0,
        qsTranslate('','popups_playfull_btn_margin'),
        "btn"
        /*tm,w,h,hgap,margin*/
    ]
    buttonTextDetails:[
        qsTranslate('','popups_playfull_btntxt_w'),
        "btn_text",
        qsTranslate('','popups_playfull_btntxt_fontsize'),
        configTool.language.global_elements.ok
        /*,w,text,color,fs*/
    ]
    onButtonSelect:{
        viewController.hideScreenPopup();
    }

    function init(){
        forceActiveFocus();
    }
    function clearOnExit(){
        viewHelper.fullPlayId=0;
    }
}
