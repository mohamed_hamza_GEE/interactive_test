import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
Rectangle {
    id : handsetDefault;
    x:parseInt(qsTranslate('',"HandsetDisplay_hs_bg1_x"));
    y:parseInt(qsTranslate('',"HandsetDisplay_hs_bg1_y"));
    width:parseInt(qsTranslate('',"HandsetDisplay_hs_bg1_w"));
    height:parseInt(qsTranslate('',"HandsetDisplay_hs_bg1_h"));
    function init(){ core.debug("HandsetDefault | init called") }
    function clearOnExit(){ core.debug("HandsetDefault | clearOnExit called")}
    Image {id:handsetBg;source:viewController.settings.phsImagePath+"background.png"}
}
