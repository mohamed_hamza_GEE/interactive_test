import QtQuick 1.0
import Panasonic.Pif 1.0

Item {
    id: themeMgrItem

    property string dir : Qt.resolvedUrl("../content/configtool/")
    property alias config : config_object
    property alias language : language_object
    property alias base : myTheme.base
    property alias path : myTheme.path
    property alias name : myTheme.name
    property alias languageName : myTheme.language
    property alias status : myTheme.status

    Theme {
        id: myTheme
        base : dir
        name : "business"
        language : "eng"
        onConfigChanged: {
            themeMgrItem.config.config_value = myTheme.config
        }
        onI18nChanged: {
            themeMgrItem.language.language_value = myTheme.i18n
        }
    }
    QtObject {
        id: config_object
        property variant config_value
        property QtObject aviancaapp : QtObject {
            id: aviancaappId1
            property variant aviancaapp_object : config_object.config_value.aviancaapp
            property QtObject screen : QtObject {
                id: screenId1
                property variant screen_object : aviancaappId1.aviancaapp_object.screen
                property variant aircraft_icon : screenId1.screen_object.aircraft_icon.value
                property variant app_arwdwn_h : screenId1.screen_object.app_arwdwn_h.value
                property variant app_arwdwn_n : screenId1.screen_object.app_arwdwn_n.value
                property variant app_arwup_h : screenId1.screen_object.app_arwup_h.value
                property variant app_arwup_n : screenId1.screen_object.app_arwup_n.value
                property variant app_bg : screenId1.screen_object.app_bg.value
                property variant app_header : screenId1.screen_object.app_header.value
                property variant app_historybg : screenId1.screen_object.app_historybg.value
                property variant app_logo : screenId1.screen_object.app_logo.value
                property variant app_menu_bg : screenId1.screen_object.app_menu_bg.value
                property variant app_menu_divider : screenId1.screen_object.app_menu_divider.value
                property variant app_menu_h : screenId1.screen_object.app_menu_h.value
                property variant app_menubtn_h : screenId1.screen_object.app_menubtn_h.value
                property variant app_menubtn_n : screenId1.screen_object.app_menubtn_n.value
                property variant app_menubtn_p : screenId1.screen_object.app_menubtn_p.value
                property variant app_progress_empty : screenId1.screen_object.app_progress_empty.value
                property variant app_progress_fill : screenId1.screen_object.app_progress_fill.value
                property variant arrow_dn_h : screenId1.screen_object.arrow_dn_h.value
                property variant arrow_dn_n : screenId1.screen_object.arrow_dn_n.value
                property variant arrow_left_h : screenId1.screen_object.arrow_left_h.value
                property variant arrow_left_n : screenId1.screen_object.arrow_left_n.value
                property variant arrow_right_h : screenId1.screen_object.arrow_right_h.value
                property variant arrow_right_n : screenId1.screen_object.arrow_right_n.value
                property variant arrow_up_h : screenId1.screen_object.arrow_up_h.value
                property variant arrow_up_n : screenId1.screen_object.arrow_up_n.value
                property variant avianca_routes : screenId1.screen_object.avianca_routes.value
                property variant business_class_icon : screenId1.screen_object.business_class_icon.value
                property variant close_app_n : screenId1.screen_object.close_app_n.value
                property variant close_h : screenId1.screen_object.close_h.value
                property variant close_n : screenId1.screen_object.close_n.value
                property variant connection_center : screenId1.screen_object.connection_center.value
                property variant economy_class : screenId1.screen_object.economy_class.value
                property variant fleet_info : screenId1.screen_object.fleet_info.value
                property variant history_h : screenId1.screen_object.history_h.value
                property variant history_n : screenId1.screen_object.history_n.value
                property variant history_play_h : screenId1.screen_object.history_play_h.value
                property variant history_play_n : screenId1.screen_object.history_play_n.value
                property variant map_dn_h : screenId1.screen_object.map_dn_h.value
                property variant map_dn_n : screenId1.screen_object.map_dn_n.value
                property variant map_fadeimage : screenId1.screen_object.map_fadeimage.value
                property variant map_left_h : screenId1.screen_object.map_left_h.value
                property variant map_left_n : screenId1.screen_object.map_left_n.value
                property variant map_right_h : screenId1.screen_object.map_right_h.value
                property variant map_right_n : screenId1.screen_object.map_right_n.value
                property variant map_up_h : screenId1.screen_object.map_up_h.value
                property variant map_up_n : screenId1.screen_object.map_up_n.value
                property variant path_n : screenId1.screen_object.path_n.value
                property variant path_s : screenId1.screen_object.path_s.value
                property variant play_map_h : screenId1.screen_object.play_map_h.value
                property variant play_map_n : screenId1.screen_object.play_map_n.value
                property variant scrollbg : screenId1.screen_object.scrollbg.value
                property variant sidemenu_h : screenId1.screen_object.sidemenu_h.value
                property variant sidemenu_n : screenId1.screen_object.sidemenu_n.value
                property variant sidemenu_panel : screenId1.screen_object.sidemenu_panel.value
                property variant sidemenu_s : screenId1.screen_object.sidemenu_s.value
                property variant slider : screenId1.screen_object.slider.value
                property variant taca_routes : screenId1.screen_object.taca_routes.value
                property variant text_airport_title : screenId1.screen_object.text_airport_title.value
                property variant text_airport_txt : screenId1.screen_object.text_airport_txt.value
                property variant text_fleet_icons : screenId1.screen_object.text_fleet_icons.value
                property variant text_fleetinfo_text : screenId1.screen_object.text_fleetinfo_text.value
                property variant text_fleetinfo_type : screenId1.screen_object.text_fleetinfo_type.value
                property variant text_header_menu : screenId1.screen_object.text_header_menu.value
                property variant text_header_title : screenId1.screen_object.text_header_title.value
                property variant text_map_indicator : screenId1.screen_object.text_map_indicator.value
                property variant text_map_play : screenId1.screen_object.text_map_play.value
                property variant text_path_indicator : screenId1.screen_object.text_path_indicator.value
                property variant text_path_info : screenId1.screen_object.text_path_info.value
                property variant text_sidemenu_n : screenId1.screen_object.text_sidemenu_n.value
                property variant text_sidemenu_s : screenId1.screen_object.text_sidemenu_s.value
                property variant text_welcome_menu : screenId1.screen_object.text_welcome_menu.value
                property variant text_welcome_title : screenId1.screen_object.text_welcome_title.value
                property variant text_zoom : screenId1.screen_object.text_zoom.value
                property variant zoom_in_h : screenId1.screen_object.zoom_in_h.value
                property variant zoom_in_n : screenId1.screen_object.zoom_in_n.value
                property variant zoom_out_h : screenId1.screen_object.zoom_out_h.value
                property variant zoom_out_n : screenId1.screen_object.zoom_out_n.value
            }
        }
        property QtObject aviancatours : QtObject {
            id: aviancatoursId1
            property variant aviancatours_object : config_object.config_value.aviancatours
            property QtObject screen : QtObject {
                id: screenId2
                property variant screen_object : aviancatoursId1.aviancatours_object.screen
                property variant app_arwdwn_h : screenId2.screen_object.app_arwdwn_h.value
                property variant app_arwdwn_n : screenId2.screen_object.app_arwdwn_n.value
                property variant app_arwup_h : screenId2.screen_object.app_arwup_h.value
                property variant app_arwup_n : screenId2.screen_object.app_arwup_n.value
                property variant close_app_n : screenId2.screen_object.close_app_n.value
                property variant close_h : screenId2.screen_object.close_h.value
                property variant close_n : screenId2.screen_object.close_n.value
                property variant conttours_bg : screenId2.screen_object.conttours_bg.value
                property variant down_btn_h : screenId2.screen_object.down_btn_h.value
                property variant down_btn_n : screenId2.screen_object.down_btn_n.value
                property variant list_bg : screenId2.screen_object.list_bg.value
                property variant menu_btn_h : screenId2.screen_object.menu_btn_h.value
                property variant menu_btn_n : screenId2.screen_object.menu_btn_n.value
                property variant overlay : screenId2.screen_object.overlay.value
                property variant place_bg_h : screenId2.screen_object.place_bg_h.value
                property variant place_bg_n : screenId2.screen_object.place_bg_n.value
                property variant placelist_logo : screenId2.screen_object.placelist_logo.value
                property variant scrollbg : screenId2.screen_object.scrollbg.value
                property variant slider : screenId2.screen_object.slider.value
                property variant synposis_bg : screenId2.screen_object.synposis_bg.value
                property variant text_conttours_h : screenId2.screen_object.text_conttours_h.value
                property variant text_conttours_n : screenId2.screen_object.text_conttours_n.value
                property variant text_conttours_s : screenId2.screen_object.text_conttours_s.value
                property variant text_info : screenId2.screen_object.text_info.value
                property variant text_listview_h : screenId2.screen_object.text_listview_h.value
                property variant text_listview_info : screenId2.screen_object.text_listview_info.value
                property variant text_listview_n : screenId2.screen_object.text_listview_n.value
                property variant text_place : screenId2.screen_object.text_place.value
                property variant text_place_amount : screenId2.screen_object.text_place_amount.value
                property variant text_title : screenId2.screen_object.text_title.value
                property variant text_txt : screenId2.screen_object.text_txt.value
                property variant text_txt_info : screenId2.screen_object.text_txt_info.value
                property variant tours_logo : screenId2.screen_object.tours_logo.value
                property variant up_btn_h : screenId2.screen_object.up_btn_h.value
                property variant up_btn_n : screenId2.screen_object.up_btn_n.value
                property variant welcome_logo : screenId2.screen_object.welcome_logo.value
            }
        }
        property QtObject discover : QtObject {
            id: discoverId1
            property variant discover_object : config_object.config_value.discover
            property QtObject cg : QtObject {
                id: cgId1
                property variant cg_object : discoverId1.discover_object.cg
                property variant btn_h : cgId1.cg_object.btn_h.value
                property variant btn_n : cgId1.cg_object.btn_n.value
                property variant btn_p : cgId1.cg_object.btn_p.value
                property variant btn_text_h : cgId1.cg_object.btn_text_h.value
                property variant btn_text_n : cgId1.cg_object.btn_text_n.value
                property variant btn_text_p : cgId1.cg_object.btn_text_p.value
                property variant close_btn_h : cgId1.cg_object.close_btn_h.value
                property variant close_btn_n : cgId1.cg_object.close_btn_n.value
                property variant close_btn_p : cgId1.cg_object.close_btn_p.value
                property variant code_share_icon : cgId1.cg_object.code_share_icon.value
                property variant description1_color : cgId1.cg_object.description1_color.value
                property variant dn_arrow_h : cgId1.cg_object.dn_arrow_h.value
                property variant dn_arrow_n : cgId1.cg_object.dn_arrow_n.value
                property variant dn_arrow_p : cgId1.cg_object.dn_arrow_p.value
                property variant fading_image : cgId1.cg_object.fading_image.value
                property variant list_text_color : cgId1.cg_object.list_text_color.value
                property variant listing_highlight : cgId1.cg_object.listing_highlight.value
                property variant panel : cgId1.cg_object.panel.value
                property variant scroll_base : cgId1.cg_object.scroll_base.value
                property variant scroll_button : cgId1.cg_object.scroll_button.value
                property variant short_title_color : cgId1.cg_object.short_title_color.value
                property variant title_color : cgId1.cg_object.title_color.value
                property variant up_arrow_h : cgId1.cg_object.up_arrow_h.value
                property variant up_arrow_n : cgId1.cg_object.up_arrow_n.value
                property variant up_arrow_p : cgId1.cg_object.up_arrow_p.value
            }
            property QtObject listing : QtObject {
                id: listingId1
                property variant listing_object : discoverId1.discover_object.listing
                property variant background : listingId1.listing_object.background.value
                property variant dark_patch : listingId1.listing_object.dark_patch.value
                property variant poster_holder_h : listingId1.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId1.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId1.listing_object.poster_holder_p.value
                property variant text_color_h : listingId1.listing_object.text_color_h.value
                property variant text_color_n : listingId1.listing_object.text_color_n.value
                property variant text_color_p : listingId1.listing_object.text_color_p.value
            }
            property QtObject survey : QtObject {
                id: surveyId1
                property variant survey_object : discoverId1.discover_object.survey
                property variant btn_h : surveyId1.survey_object.btn_h.value
                property variant btn_n : surveyId1.survey_object.btn_n.value
                property variant btn_p : surveyId1.survey_object.btn_p.value
                property variant btn_text_h : surveyId1.survey_object.btn_text_h.value
                property variant btn_text_n : surveyId1.survey_object.btn_text_n.value
                property variant btn_text_p : surveyId1.survey_object.btn_text_p.value
                property variant check_h : surveyId1.survey_object.check_h.value
                property variant check_n : surveyId1.survey_object.check_n.value
                property variant check_p : surveyId1.survey_object.check_p.value
                property variant check_sh : surveyId1.survey_object.check_sh.value
                property variant check_sn : surveyId1.survey_object.check_sn.value
                property variant check_sp : surveyId1.survey_object.check_sp.value
                property variant close_btn_h : surveyId1.survey_object.close_btn_h.value
                property variant close_btn_n : surveyId1.survey_object.close_btn_n.value
                property variant close_btn_p : surveyId1.survey_object.close_btn_p.value
                property variant description1_color : surveyId1.survey_object.description1_color.value
                property variant dn_arrow_h : surveyId1.survey_object.dn_arrow_h.value
                property variant dn_arrow_n : surveyId1.survey_object.dn_arrow_n.value
                property variant dn_arrow_p : surveyId1.survey_object.dn_arrow_p.value
                property variant fading_image : surveyId1.survey_object.fading_image.value
                property variant indicator_h : surveyId1.survey_object.indicator_h.value
                property variant indicator_n : surveyId1.survey_object.indicator_n.value
                property variant indicator_p : surveyId1.survey_object.indicator_p.value
                property variant left_arrow_h : surveyId1.survey_object.left_arrow_h.value
                property variant left_arrow_n : surveyId1.survey_object.left_arrow_n.value
                property variant left_arrow_p : surveyId1.survey_object.left_arrow_p.value
                property variant likert_scroll_base : surveyId1.survey_object.likert_scroll_base.value
                property variant likert_scroll_button : surveyId1.survey_object.likert_scroll_button.value
                property variant listing_highlight : surveyId1.survey_object.listing_highlight.value
                property variant multitext_panel : surveyId1.survey_object.multitext_panel.value
                property variant multitext_panel_h : surveyId1.survey_object.multitext_panel_h.value
                property variant radio_h : surveyId1.survey_object.radio_h.value
                property variant radio_n : surveyId1.survey_object.radio_n.value
                property variant radio_p : surveyId1.survey_object.radio_p.value
                property variant radio_sh : surveyId1.survey_object.radio_sh.value
                property variant radio_sn : surveyId1.survey_object.radio_sn.value
                property variant radio_sp : surveyId1.survey_object.radio_sp.value
                property variant right_arrow_h : surveyId1.survey_object.right_arrow_h.value
                property variant right_arrow_n : surveyId1.survey_object.right_arrow_n.value
                property variant right_arrow_p : surveyId1.survey_object.right_arrow_p.value
                property variant scroll_base : surveyId1.survey_object.scroll_base.value
                property variant scroll_button : surveyId1.survey_object.scroll_button.value
                property variant semantic_base_end : surveyId1.survey_object.semantic_base_end.value
                property variant semantic_base_middle : surveyId1.survey_object.semantic_base_middle.value
                property variant semantic_base_start : surveyId1.survey_object.semantic_base_start.value
                property variant semantic_divider_color : surveyId1.survey_object.semantic_divider_color.value
                property variant semantic_title_color : surveyId1.survey_object.semantic_title_color.value
                property variant singletext_panel : surveyId1.survey_object.singletext_panel.value
                property variant singletext_panel_h : surveyId1.survey_object.singletext_panel_h.value
                property variant surveylist_panel : surveyId1.survey_object.surveylist_panel.value
                property variant surveyquestions_panel : surveyId1.survey_object.surveyquestions_panel.value
                property variant title_color : surveyId1.survey_object.title_color.value
                property variant up_arrow_h : surveyId1.survey_object.up_arrow_h.value
                property variant up_arrow_n : surveyId1.survey_object.up_arrow_n.value
                property variant up_arrow_p : surveyId1.survey_object.up_arrow_p.value
                property variant vkb_btn_h : surveyId1.survey_object.vkb_btn_h.value
                property variant vkb_btn_n : surveyId1.survey_object.vkb_btn_n.value
                property variant vkb_btn_p : surveyId1.survey_object.vkb_btn_p.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId1
                property variant synopsis_object : discoverId1.discover_object.synopsis
                property variant btn_h : synopsisId1.synopsis_object.btn_h.value
                property variant btn_n : synopsisId1.synopsis_object.btn_n.value
                property variant btn_p : synopsisId1.synopsis_object.btn_p.value
                property variant btn_text_h : synopsisId1.synopsis_object.btn_text_h.value
                property variant btn_text_n : synopsisId1.synopsis_object.btn_text_n.value
                property variant btn_text_p : synopsisId1.synopsis_object.btn_text_p.value
                property variant cg_info_text_color : synopsisId1.synopsis_object.cg_info_text_color.value
                property variant close_btn_h : synopsisId1.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId1.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId1.synopsis_object.close_btn_p.value
                property variant description1_color : synopsisId1.synopsis_object.description1_color.value
                property variant dn_arrow_h : synopsisId1.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId1.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId1.synopsis_object.dn_arrow_p.value
                property variant fading_image : synopsisId1.synopsis_object.fading_image.value
                property variant panel : synopsisId1.synopsis_object.panel.value
                property variant scroll_base : synopsisId1.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId1.synopsis_object.scroll_button.value
                property variant short_title_color : synopsisId1.synopsis_object.short_title_color.value
                property variant title_color : synopsisId1.synopsis_object.title_color.value
                property variant up_arrow_h : synopsisId1.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId1.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId1.synopsis_object.up_arrow_p.value
            }
        }
        property QtObject games : QtObject {
            id: gamesId1
            property variant games_object : config_object.config_value.games
            property QtObject listing : QtObject {
                id: listingId2
                property variant listing_object : gamesId1.games_object.listing
                property variant background : listingId2.listing_object.background.value
                property variant dark_patch : listingId2.listing_object.dark_patch.value
                property variant poster_holder_h : listingId2.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId2.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId2.listing_object.poster_holder_p.value
                property variant text_color_h : listingId2.listing_object.text_color_h.value
                property variant text_color_n : listingId2.listing_object.text_color_n.value
                property variant text_color_p : listingId2.listing_object.text_color_p.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId2
                property variant synopsis_object : gamesId1.games_object.synopsis
                property variant btn_h : synopsisId2.synopsis_object.btn_h.value
                property variant btn_n : synopsisId2.synopsis_object.btn_n.value
                property variant btn_p : synopsisId2.synopsis_object.btn_p.value
                property variant btn_text_h : synopsisId2.synopsis_object.btn_text_h.value
                property variant btn_text_n : synopsisId2.synopsis_object.btn_text_n.value
                property variant btn_text_p : synopsisId2.synopsis_object.btn_text_p.value
                property variant close_btn_h : synopsisId2.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId2.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId2.synopsis_object.close_btn_p.value
                property variant description1_color : synopsisId2.synopsis_object.description1_color.value
                property variant dn_arrow_h : synopsisId2.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId2.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId2.synopsis_object.dn_arrow_p.value
                property variant fading_image : synopsisId2.synopsis_object.fading_image.value
                property variant panel : synopsisId2.synopsis_object.panel.value
                property variant scroll_base : synopsisId2.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId2.synopsis_object.scroll_button.value
                property variant short_title_color : synopsisId2.synopsis_object.short_title_color.value
                property variant title_color : synopsisId2.synopsis_object.title_color.value
                property variant up_arrow_h : synopsisId2.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId2.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId2.synopsis_object.up_arrow_p.value
            }
        }
        property QtObject global : QtObject {
            id: globalId1
            property variant global_object : config_object.config_value.global
            property QtObject brightness : QtObject {
                id: brightnessId1
                property variant brightness_object : globalId1.global_object.brightness
                property variant base : brightnessId1.brightness_object.base.value
                property variant bg : brightnessId1.brightness_object.bg.value
                property variant bri_dcr_h : brightnessId1.brightness_object.bri_dcr_h.value
                property variant bri_dcr_n : brightnessId1.brightness_object.bri_dcr_n.value
                property variant bri_dcr_p : brightnessId1.brightness_object.bri_dcr_p.value
                property variant bri_incr_h : brightnessId1.brightness_object.bri_incr_h.value
                property variant bri_incr_n : brightnessId1.brightness_object.bri_incr_n.value
                property variant bri_incr_p : brightnessId1.brightness_object.bri_incr_p.value
                property variant fill_color : brightnessId1.brightness_object.fill_color.value
                property variant indicator : brightnessId1.brightness_object.indicator.value
            }
            property QtObject entertainment_off : QtObject {
                id: entertainment_offId1
                property variant entertainment_off_object : globalId1.global_object.entertainment_off
                property variant entoff_bg : entertainment_offId1.entertainment_off_object.entoff_bg.value
                property variant entoff_logo : entertainment_offId1.entertainment_off_object.entoff_logo.value
                property variant entoff_oceanair_logo : entertainment_offId1.entertainment_off_object.entoff_oceanair_logo.value
                property variant recent_app : entertainment_offId1.entertainment_off_object.recent_app.value
            }
            property QtObject header : QtObject {
                id: headerId1
                property variant header_object : globalId1.global_object.header
                property variant airplane_icon : headerId1.header_object.airplane_icon.value
                property variant airport_map_btn_h : headerId1.header_object.airport_map_btn_h.value
                property variant airport_map_btn_n : headerId1.header_object.airport_map_btn_n.value
                property variant airport_map_btn_p : headerId1.header_object.airport_map_btn_p.value
                property variant chat_notify_h : headerId1.header_object.chat_notify_h.value
                property variant chat_notify_n : headerId1.header_object.chat_notify_n.value
                property variant chat_notify_p : headerId1.header_object.chat_notify_p.value
                property variant chat_notify_text_color : headerId1.header_object.chat_notify_text_color.value
                property variant flight_connect_btn_h : headerId1.header_object.flight_connect_btn_h.value
                property variant flight_connect_btn_n : headerId1.header_object.flight_connect_btn_n.value
                property variant flight_connect_btn_p : headerId1.header_object.flight_connect_btn_p.value
                property variant flight_info_btn_h : headerId1.header_object.flight_info_btn_h.value
                property variant flight_info_btn_n : headerId1.header_object.flight_info_btn_n.value
                property variant flight_info_btn_p : headerId1.header_object.flight_info_btn_p.value
                property variant flight_info_close_icon : headerId1.header_object.flight_info_close_icon.value
                property variant flight_info_open_icon : headerId1.header_object.flight_info_open_icon.value
                property variant flight_path_base_image : headerId1.header_object.flight_path_base_image.value
                property variant flight_path_circle_image_h : headerId1.header_object.flight_path_circle_image_h.value
                property variant flight_path_circle_image_n : headerId1.header_object.flight_path_circle_image_n.value
                property variant flight_path_fill_color : headerId1.header_object.flight_path_fill_color.value
                property variant flight_text_color : headerId1.header_object.flight_text_color.value
                property variant header_button_h : headerId1.header_object.header_button_h.value
                property variant header_button_n : headerId1.header_object.header_button_n.value
                property variant header_button_p : headerId1.header_object.header_button_p.value
                property variant help_button_h : headerId1.header_object.help_button_h.value
                property variant help_button_n : headerId1.header_object.help_button_n.value
                property variant help_button_p : headerId1.header_object.help_button_p.value
                property variant overlay_icon1 : headerId1.header_object.overlay_icon1.value
                property variant overlay_icon2 : headerId1.header_object.overlay_icon2.value
                property variant overlay_icon3 : headerId1.header_object.overlay_icon3.value
                property variant text_color_h : headerId1.header_object.text_color_h.value
                property variant text_color_n : headerId1.header_object.text_color_n.value
                property variant text_color_p : headerId1.header_object.text_color_p.value
                property variant view_map_btn_h : headerId1.header_object.view_map_btn_h.value
                property variant view_map_btn_n : headerId1.header_object.view_map_btn_n.value
                property variant view_map_btn_p : headerId1.header_object.view_map_btn_p.value
                property variant vod_logo : headerId1.header_object.vod_logo.value
            }
            property QtObject settings : QtObject {
                id: settingsId1
                property variant settings_object : globalId1.global_object.settings
                property variant accept_btn_h : settingsId1.settings_object.accept_btn_h.value
                property variant accept_btn_n : settingsId1.settings_object.accept_btn_n.value
                property variant accept_btn_p : settingsId1.settings_object.accept_btn_p.value
                property variant accept_text_h : settingsId1.settings_object.accept_text_h.value
                property variant accept_text_n : settingsId1.settings_object.accept_text_n.value
                property variant accept_text_p : settingsId1.settings_object.accept_text_p.value
                property variant base_fill : settingsId1.settings_object.base_fill.value
                property variant base_image : settingsId1.settings_object.base_image.value
                property variant brightness_dcr_h : settingsId1.settings_object.brightness_dcr_h.value
                property variant brightness_dcr_n : settingsId1.settings_object.brightness_dcr_n.value
                property variant brightness_dcr_p : settingsId1.settings_object.brightness_dcr_p.value
                property variant brightness_incr_h : settingsId1.settings_object.brightness_incr_h.value
                property variant brightness_incr_n : settingsId1.settings_object.brightness_incr_n.value
                property variant brightness_incr_p : settingsId1.settings_object.brightness_incr_p.value
                property variant call_crew1_h : settingsId1.settings_object.call_crew1_h.value
                property variant call_crew1_n : settingsId1.settings_object.call_crew1_n.value
                property variant call_crew1_p : settingsId1.settings_object.call_crew1_p.value
                property variant call_crew_h : settingsId1.settings_object.call_crew_h.value
                property variant call_crew_n : settingsId1.settings_object.call_crew_n.value
                property variant call_crew_p : settingsId1.settings_object.call_crew_p.value
                property variant call_crewcancel1_h : settingsId1.settings_object.call_crewcancel1_h.value
                property variant call_crewcancel1_n : settingsId1.settings_object.call_crewcancel1_n.value
                property variant call_crewcancel1_p : settingsId1.settings_object.call_crewcancel1_p.value
                property variant call_crewcancel_h : settingsId1.settings_object.call_crewcancel_h.value
                property variant call_crewcancel_n : settingsId1.settings_object.call_crewcancel_n.value
                property variant call_crewcancel_p : settingsId1.settings_object.call_crewcancel_p.value
                property variant crew_vdivider : settingsId1.settings_object.crew_vdivider.value
                property variant disable_btn_h : settingsId1.settings_object.disable_btn_h.value
                property variant disable_btn_n : settingsId1.settings_object.disable_btn_n.value
                property variant disable_btn_p : settingsId1.settings_object.disable_btn_p.value
                property variant disable_text_h : settingsId1.settings_object.disable_text_h.value
                property variant disable_text_n : settingsId1.settings_object.disable_text_n.value
                property variant disable_text_p : settingsId1.settings_object.disable_text_p.value
                property variant indicator : settingsId1.settings_object.indicator.value
                property variant language_text_h : settingsId1.settings_object.language_text_h.value
                property variant language_text_n : settingsId1.settings_object.language_text_n.value
                property variant language_text_s : settingsId1.settings_object.language_text_s.value
                property variant left_arrow_h : settingsId1.settings_object.left_arrow_h.value
                property variant left_arrow_n : settingsId1.settings_object.left_arrow_n.value
                property variant left_arrow_p : settingsId1.settings_object.left_arrow_p.value
                property variant reading_light1_h : settingsId1.settings_object.reading_light1_h.value
                property variant reading_light1_n : settingsId1.settings_object.reading_light1_n.value
                property variant reading_light1_p : settingsId1.settings_object.reading_light1_p.value
                property variant reading_light_h : settingsId1.settings_object.reading_light_h.value
                property variant reading_light_n : settingsId1.settings_object.reading_light_n.value
                property variant reading_light_p : settingsId1.settings_object.reading_light_p.value
                property variant reading_lightoff1_h : settingsId1.settings_object.reading_lightoff1_h.value
                property variant reading_lightoff1_n : settingsId1.settings_object.reading_lightoff1_n.value
                property variant reading_lightoff1_p : settingsId1.settings_object.reading_lightoff1_p.value
                property variant reading_lightoff_h : settingsId1.settings_object.reading_lightoff_h.value
                property variant reading_lightoff_n : settingsId1.settings_object.reading_lightoff_n.value
                property variant reading_lightoff_p : settingsId1.settings_object.reading_lightoff_p.value
                property variant right_arrow_h : settingsId1.settings_object.right_arrow_h.value
                property variant right_arrow_n : settingsId1.settings_object.right_arrow_n.value
                property variant right_arrow_p : settingsId1.settings_object.right_arrow_p.value
                property variant screen_off1_h : settingsId1.settings_object.screen_off1_h.value
                property variant screen_off1_n : settingsId1.settings_object.screen_off1_n.value
                property variant screen_off1_p : settingsId1.settings_object.screen_off1_p.value
                property variant screen_off_h : settingsId1.settings_object.screen_off_h.value
                property variant screen_off_n : settingsId1.settings_object.screen_off_n.value
                property variant screen_off_p : settingsId1.settings_object.screen_off_p.value
                property variant text_color : settingsId1.settings_object.text_color.value
                property variant volume_dcr_h : settingsId1.settings_object.volume_dcr_h.value
                property variant volume_dcr_n : settingsId1.settings_object.volume_dcr_n.value
                property variant volume_dcr_p : settingsId1.settings_object.volume_dcr_p.value
                property variant volume_incr_h : settingsId1.settings_object.volume_incr_h.value
                property variant volume_incr_n : settingsId1.settings_object.volume_incr_n.value
                property variant volume_incr_p : settingsId1.settings_object.volume_incr_p.value
                property variant window_dim1_h : settingsId1.settings_object.window_dim1_h.value
                property variant window_dim1_n : settingsId1.settings_object.window_dim1_n.value
                property variant window_dim1_p : settingsId1.settings_object.window_dim1_p.value
            }
            property QtObject video : QtObject {
                id: videoId1
                property variant video_object : globalId1.global_object.video
                property variant  vol_incr_p : videoId1.video_object. vol_incr_p.value
                property variant aspect1_h : videoId1.video_object.aspect1_h.value
                property variant aspect1_n : videoId1.video_object.aspect1_n.value
                property variant aspect1_p : videoId1.video_object.aspect1_p.value
                property variant aspect2_h : videoId1.video_object.aspect2_h.value
                property variant aspect2_n : videoId1.video_object.aspect2_n.value
                property variant aspect2_p : videoId1.video_object.aspect2_p.value
                property variant base_image : videoId1.video_object.base_image.value
                property variant base_image_trailer : videoId1.video_object.base_image_trailer.value
                property variant bright_dcr_h : videoId1.video_object.bright_dcr_h.value
                property variant bright_dcr_n : videoId1.video_object.bright_dcr_n.value
                property variant bright_dcr_p : videoId1.video_object.bright_dcr_p.value
                property variant bright_incr_h : videoId1.video_object.bright_incr_h.value
                property variant bright_incr_n : videoId1.video_object.bright_incr_n.value
                property variant bright_incr_p : videoId1.video_object.bright_incr_p.value
                property variant brightness_h : videoId1.video_object.brightness_h.value
                property variant brightness_n : videoId1.video_object.brightness_n.value
                property variant brightness_p : videoId1.video_object.brightness_p.value
                property variant controller_bg : videoId1.video_object.controller_bg.value
                property variant fill_color : videoId1.video_object.fill_color.value
                property variant fill_color_trailer : videoId1.video_object.fill_color_trailer.value
                property variant forward_h : videoId1.video_object.forward_h.value
                property variant forward_n : videoId1.video_object.forward_n.value
                property variant forward_p : videoId1.video_object.forward_p.value
                property variant indicator : videoId1.video_object.indicator.value
                property variant pause_h : videoId1.video_object.pause_h.value
                property variant pause_n : videoId1.video_object.pause_n.value
                property variant pause_p : videoId1.video_object.pause_p.value
                property variant play_h : videoId1.video_object.play_h.value
                property variant play_n : videoId1.video_object.play_n.value
                property variant play_p : videoId1.video_object.play_p.value
                property variant popup_base_image : videoId1.video_object.popup_base_image.value
                property variant popup_fill_color : videoId1.video_object.popup_fill_color.value
                property variant replay_h : videoId1.video_object.replay_h.value
                property variant replay_n : videoId1.video_object.replay_n.value
                property variant replay_p : videoId1.video_object.replay_p.value
                property variant rewfor_popup : videoId1.video_object.rewfor_popup.value
                property variant rewind_h : videoId1.video_object.rewind_h.value
                property variant rewind_n : videoId1.video_object.rewind_n.value
                property variant rewind_p : videoId1.video_object.rewind_p.value
                property variant soundtrackcc_h : videoId1.video_object.soundtrackcc_h.value
                property variant soundtrackcc_n : videoId1.video_object.soundtrackcc_n.value
                property variant soundtrackcc_p : videoId1.video_object.soundtrackcc_p.value
                property variant speed_text : videoId1.video_object.speed_text.value
                property variant stop_h : videoId1.video_object.stop_h.value
                property variant stop_n : videoId1.video_object.stop_n.value
                property variant stop_p : videoId1.video_object.stop_p.value
                property variant text_color : videoId1.video_object.text_color.value
                property variant vod_btn_h : videoId1.video_object.vod_btn_h.value
                property variant vod_btn_n : videoId1.video_object.vod_btn_n.value
                property variant vod_btn_p : videoId1.video_object.vod_btn_p.value
                property variant vol_dcr_h : videoId1.video_object.vol_dcr_h.value
                property variant vol_dcr_n : videoId1.video_object.vol_dcr_n.value
                property variant vol_dcr_p : videoId1.video_object.vol_dcr_p.value
                property variant vol_incr_h : videoId1.video_object.vol_incr_h.value
                property variant vol_incr_n : videoId1.video_object.vol_incr_n.value
                property variant volume_h : videoId1.video_object.volume_h.value
                property variant volume_n : videoId1.video_object.volume_n.value
                property variant volume_p : videoId1.video_object.volume_p.value
                property variant volume_popup : videoId1.video_object.volume_popup.value
            }
            property QtObject volume : QtObject {
                id: volumeId1
                property variant volume_object : globalId1.global_object.volume
                property variant base : volumeId1.volume_object.base.value
                property variant bg : volumeId1.volume_object.bg.value
                property variant fill_color : volumeId1.volume_object.fill_color.value
                property variant handset_fill_color : volumeId1.volume_object.handset_fill_color.value
                property variant handset_indicator : volumeId1.volume_object.handset_indicator.value
                property variant indicator : volumeId1.volume_object.indicator.value
                property variant vol_dcr_h : volumeId1.volume_object.vol_dcr_h.value
                property variant vol_dcr_n : volumeId1.volume_object.vol_dcr_n.value
                property variant vol_dcr_p : volumeId1.volume_object.vol_dcr_p.value
                property variant vol_incr_h : volumeId1.volume_object.vol_incr_h.value
                property variant vol_incr_n : volumeId1.volume_object.vol_incr_n.value
                property variant vol_incr_p : volumeId1.volume_object.vol_incr_p.value
            }
            property QtObject welcome : QtObject {
                id: welcomeId1
                property variant welcome_object : globalId1.global_object.welcome
                property variant adult_btn_h : welcomeId1.welcome_object.adult_btn_h.value
                property variant adult_btn_n : welcomeId1.welcome_object.adult_btn_n.value
                property variant adult_btn_p : welcomeId1.welcome_object.adult_btn_p.value
                property variant adult_btn_txt_h : welcomeId1.welcome_object.adult_btn_txt_h.value
                property variant adult_btn_txt_n : welcomeId1.welcome_object.adult_btn_txt_n.value
                property variant adult_btn_txt_p : welcomeId1.welcome_object.adult_btn_txt_p.value
                property variant kids_btn_h : welcomeId1.welcome_object.kids_btn_h.value
                property variant kids_btn_n : welcomeId1.welcome_object.kids_btn_n.value
                property variant kids_btn_p : welcomeId1.welcome_object.kids_btn_p.value
                property variant kids_btn_txt_h : welcomeId1.welcome_object.kids_btn_txt_h.value
                property variant kids_btn_txt_n : welcomeId1.welcome_object.kids_btn_txt_n.value
                property variant kids_btn_txt_p : welcomeId1.welcome_object.kids_btn_txt_p.value
                property variant language_text_h : welcomeId1.welcome_object.language_text_h.value
                property variant language_text_n : welcomeId1.welcome_object.language_text_n.value
                property variant language_text_s : welcomeId1.welcome_object.language_text_s.value
                property variant left_arrow : welcomeId1.welcome_object.left_arrow.value
                property variant right_arrow : welcomeId1.welcome_object.right_arrow.value
                property variant welc_bg : welcomeId1.welcome_object.welc_bg.value
                property variant welc_logo : welcomeId1.welcome_object.welc_logo.value
                property variant welc_oceanair_logo : welcomeId1.welcome_object.welc_oceanair_logo.value
            }
        }
        property QtObject help : QtObject {
            id: helpId1
            property variant help_object : config_object.config_value.help
            property QtObject english : QtObject {
                id: englishId1
                property variant english_object : helpId1.help_object.english
                property variant airlineinfo_synop : englishId1.english_object.airlineinfo_synop.value
                property variant artist_listing : englishId1.english_object.artist_listing.value
                property variant artist_traklist : englishId1.english_object.artist_traklist.value
                property variant connectgate_details : englishId1.english_object.connectgate_details.value
                property variant connectgate_listing : englishId1.english_object.connectgate_listing.value
                property variant discover_listing : englishId1.english_object.discover_listing.value
                property variant discover_synop : englishId1.english_object.discover_synop.value
                property variant discover_synop_no_button : englishId1.english_object.discover_synop_no_button.value
                property variant episode_listing : englishId1.english_object.episode_listing.value
                property variant games_listing : englishId1.english_object.games_listing.value
                property variant games_synop : englishId1.english_object.games_synop.value
                property variant genre : englishId1.english_object.genre.value
                property variant main_menu : englishId1.english_object.main_menu.value
                property variant movie_listing : englishId1.english_object.movie_listing.value
                property variant movie_synopsis : englishId1.english_object.movie_synopsis.value
                property variant movie_trailer : englishId1.english_object.movie_trailer.value
                property variant movie_vod : englishId1.english_object.movie_vod.value
                property variant music_listing : englishId1.english_object.music_listing.value
                property variant playlist : englishId1.english_object.playlist.value
                property variant playlist_empty : englishId1.english_object.playlist_empty.value
                property variant radio_listing : englishId1.english_object.radio_listing.value
                property variant seatchat_chatscreen : englishId1.english_object.seatchat_chatscreen.value
                property variant seatchat_chatscreen_noblock : englishId1.english_object.seatchat_chatscreen_noblock.value
                property variant seatchat_name : englishId1.english_object.seatchat_name.value
                property variant seatchat_tc : englishId1.english_object.seatchat_tc.value
                property variant shopping_listing : englishId1.english_object.shopping_listing.value
                property variant shopping_synop : englishId1.english_object.shopping_synop.value
                property variant survey : englishId1.english_object.survey.value
                property variant survey_synopsis : englishId1.english_object.survey_synopsis.value
                property variant survey_synopsis_no_button : englishId1.english_object.survey_synopsis_no_button.value
                property variant tracklist : englishId1.english_object.tracklist.value
                property variant tv_listing : englishId1.english_object.tv_listing.value
                property variant tv_synop : englishId1.english_object.tv_synop.value
                property variant tv_vod : englishId1.english_object.tv_vod.value
                property variant usb_doc_listing : englishId1.english_object.usb_doc_listing.value
                property variant usb_mus_listing : englishId1.english_object.usb_mus_listing.value
                property variant usb_pic_listing : englishId1.english_object.usb_pic_listing.value
                property variant usb_terms_cndtions : englishId1.english_object.usb_terms_cndtions.value
            }
            property QtObject germany : QtObject {
                id: germanyId1
                property variant germany_object : helpId1.help_object.germany
                property variant airlineinfo_synop : germanyId1.germany_object.airlineinfo_synop.value
                property variant artist_listing : germanyId1.germany_object.artist_listing.value
                property variant artist_traklist : germanyId1.germany_object.artist_traklist.value
                property variant connectgate_details : germanyId1.germany_object.connectgate_details.value
                property variant connectgate_listing : germanyId1.germany_object.connectgate_listing.value
                property variant discover_listing : germanyId1.germany_object.discover_listing.value
                property variant discover_synop : germanyId1.germany_object.discover_synop.value
                property variant discover_synop_no_button : germanyId1.germany_object.discover_synop_no_button.value
                property variant episode_listing : germanyId1.germany_object.episode_listing.value
                property variant games_listing : germanyId1.germany_object.games_listing.value
                property variant games_synop : germanyId1.germany_object.games_synop.value
                property variant genre : germanyId1.germany_object.genre.value
                property variant main_menu : germanyId1.germany_object.main_menu.value
                property variant movie_listing : germanyId1.germany_object.movie_listing.value
                property variant movie_synopsis : germanyId1.germany_object.movie_synopsis.value
                property variant movie_trailer : germanyId1.germany_object.movie_trailer.value
                property variant movie_vod : germanyId1.germany_object.movie_vod.value
                property variant music_listing : germanyId1.germany_object.music_listing.value
                property variant playlist : germanyId1.germany_object.playlist.value
                property variant playlist_empty : germanyId1.germany_object.playlist_empty.value
                property variant radio_listing : germanyId1.germany_object.radio_listing.value
                property variant seatchat_chatscreen : germanyId1.germany_object.seatchat_chatscreen.value
                property variant seatchat_chatscreen_noblock : germanyId1.germany_object.seatchat_chatscreen_noblock.value
                property variant seatchat_name : germanyId1.germany_object.seatchat_name.value
                property variant seatchat_tc : germanyId1.germany_object.seatchat_tc.value
                property variant shopping_listing : germanyId1.germany_object.shopping_listing.value
                property variant shopping_synop : germanyId1.germany_object.shopping_synop.value
                property variant survey : germanyId1.germany_object.survey.value
                property variant survey_synopsis : germanyId1.germany_object.survey_synopsis.value
                property variant survey_synopsis_no_button : germanyId1.germany_object.survey_synopsis_no_button.value
                property variant tracklist : germanyId1.germany_object.tracklist.value
                property variant tv_listing : germanyId1.germany_object.tv_listing.value
                property variant tv_synop : germanyId1.germany_object.tv_synop.value
                property variant tv_vod : germanyId1.germany_object.tv_vod.value
                property variant usb_doc_listing : germanyId1.germany_object.usb_doc_listing.value
                property variant usb_mus_listing : germanyId1.germany_object.usb_mus_listing.value
                property variant usb_pic_listing : germanyId1.germany_object.usb_pic_listing.value
                property variant usb_terms_cndtions : germanyId1.germany_object.usb_terms_cndtions.value
            }
            property QtObject portuguese : QtObject {
                id: portugueseId1
                property variant portuguese_object : helpId1.help_object.portuguese
                property variant airlineinfo_synop : portugueseId1.portuguese_object.airlineinfo_synop.value
                property variant artist_listing : portugueseId1.portuguese_object.artist_listing.value
                property variant artist_traklist : portugueseId1.portuguese_object.artist_traklist.value
                property variant connectgate_details : portugueseId1.portuguese_object.connectgate_details.value
                property variant connectgate_listing : portugueseId1.portuguese_object.connectgate_listing.value
                property variant discover_listing : portugueseId1.portuguese_object.discover_listing.value
                property variant discover_synop : portugueseId1.portuguese_object.discover_synop.value
                property variant discover_synop_no_button : portugueseId1.portuguese_object.discover_synop_no_button.value
                property variant episode_listing : portugueseId1.portuguese_object.episode_listing.value
                property variant games_listing : portugueseId1.portuguese_object.games_listing.value
                property variant games_synop : portugueseId1.portuguese_object.games_synop.value
                property variant genre : portugueseId1.portuguese_object.genre.value
                property variant main_menu : portugueseId1.portuguese_object.main_menu.value
                property variant movie_listing : portugueseId1.portuguese_object.movie_listing.value
                property variant movie_synopsis : portugueseId1.portuguese_object.movie_synopsis.value
                property variant movie_trailer : portugueseId1.portuguese_object.movie_trailer.value
                property variant movie_vod : portugueseId1.portuguese_object.movie_vod.value
                property variant music_listing : portugueseId1.portuguese_object.music_listing.value
                property variant playlist : portugueseId1.portuguese_object.playlist.value
                property variant playlist_empty : portugueseId1.portuguese_object.playlist_empty.value
                property variant radio_listing : portugueseId1.portuguese_object.radio_listing.value
                property variant seatchat_chatscreen : portugueseId1.portuguese_object.seatchat_chatscreen.value
                property variant seatchat_chatscreen_noblock : portugueseId1.portuguese_object.seatchat_chatscreen_noblock.value
                property variant seatchat_name : portugueseId1.portuguese_object.seatchat_name.value
                property variant seatchat_tc : portugueseId1.portuguese_object.seatchat_tc.value
                property variant shopping_listing : portugueseId1.portuguese_object.shopping_listing.value
                property variant shopping_synop : portugueseId1.portuguese_object.shopping_synop.value
                property variant survey : portugueseId1.portuguese_object.survey.value
                property variant survey_synopsis : portugueseId1.portuguese_object.survey_synopsis.value
                property variant survey_synopsis_no_button : portugueseId1.portuguese_object.survey_synopsis_no_button.value
                property variant tracklist : portugueseId1.portuguese_object.tracklist.value
                property variant tv_listing : portugueseId1.portuguese_object.tv_listing.value
                property variant tv_synop : portugueseId1.portuguese_object.tv_synop.value
                property variant tv_vod : portugueseId1.portuguese_object.tv_vod.value
                property variant usb_doc_listing : portugueseId1.portuguese_object.usb_doc_listing.value
                property variant usb_mus_listing : portugueseId1.portuguese_object.usb_mus_listing.value
                property variant usb_pic_listing : portugueseId1.portuguese_object.usb_pic_listing.value
                property variant usb_terms_cndtions : portugueseId1.portuguese_object.usb_terms_cndtions.value
            }
            property QtObject spanish : QtObject {
                id: spanishId1
                property variant spanish_object : helpId1.help_object.spanish
                property variant airlineinfo_synop : spanishId1.spanish_object.airlineinfo_synop.value
                property variant artist_listing : spanishId1.spanish_object.artist_listing.value
                property variant artist_traklist : spanishId1.spanish_object.artist_traklist.value
                property variant connectgate_details : spanishId1.spanish_object.connectgate_details.value
                property variant connectgate_listing : spanishId1.spanish_object.connectgate_listing.value
                property variant discover_listing : spanishId1.spanish_object.discover_listing.value
                property variant discover_synop : spanishId1.spanish_object.discover_synop.value
                property variant discover_synop_no_button : spanishId1.spanish_object.discover_synop_no_button.value
                property variant episode_listing : spanishId1.spanish_object.episode_listing.value
                property variant games_listing : spanishId1.spanish_object.games_listing.value
                property variant games_synop : spanishId1.spanish_object.games_synop.value
                property variant genre : spanishId1.spanish_object.genre.value
                property variant main_menu : spanishId1.spanish_object.main_menu.value
                property variant movie_listing : spanishId1.spanish_object.movie_listing.value
                property variant movie_synopsis : spanishId1.spanish_object.movie_synopsis.value
                property variant movie_trailer : spanishId1.spanish_object.movie_trailer.value
                property variant movie_vod : spanishId1.spanish_object.movie_vod.value
                property variant music_listing : spanishId1.spanish_object.music_listing.value
                property variant playlist : spanishId1.spanish_object.playlist.value
                property variant playlist_empty : spanishId1.spanish_object.playlist_empty.value
                property variant radio_listing : spanishId1.spanish_object.radio_listing.value
                property variant seatchat_chatscreen : spanishId1.spanish_object.seatchat_chatscreen.value
                property variant seatchat_chatscreen_noblock : spanishId1.spanish_object.seatchat_chatscreen_noblock.value
                property variant seatchat_name : spanishId1.spanish_object.seatchat_name.value
                property variant seatchat_tc : spanishId1.spanish_object.seatchat_tc.value
                property variant shopping_listing : spanishId1.spanish_object.shopping_listing.value
                property variant shopping_synop : spanishId1.spanish_object.shopping_synop.value
                property variant survey : spanishId1.spanish_object.survey.value
                property variant survey_synopsis : spanishId1.spanish_object.survey_synopsis.value
                property variant survey_synopsis_no_button : spanishId1.spanish_object.survey_synopsis_no_button.value
                property variant tracklist : spanishId1.spanish_object.tracklist.value
                property variant tv_listing : spanishId1.spanish_object.tv_listing.value
                property variant tv_synop : spanishId1.spanish_object.tv_synop.value
                property variant tv_vod : spanishId1.spanish_object.tv_vod.value
                property variant usb_doc_listing : spanishId1.spanish_object.usb_doc_listing.value
                property variant usb_mus_listing : spanishId1.spanish_object.usb_mus_listing.value
                property variant usb_pic_listing : spanishId1.spanish_object.usb_pic_listing.value
                property variant usb_terms_cndtions : spanishId1.spanish_object.usb_terms_cndtions.value
            }
        }
        property QtObject kids_games_listing : QtObject {
            id: kids_games_listingId1
            property variant kids_games_listing_object : config_object.config_value.kids_games_listing
            property QtObject listing : QtObject {
                id: listingId3
                property variant listing_object : kids_games_listingId1.kids_games_listing_object.listing
                property variant background : listingId3.listing_object.background.value
                property variant dark_patch : listingId3.listing_object.dark_patch.value
                property variant poster_holder_h : listingId3.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId3.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId3.listing_object.poster_holder_p.value
                property variant text_color_h : listingId3.listing_object.text_color_h.value
                property variant text_color_n : listingId3.listing_object.text_color_n.value
                property variant text_color_p : listingId3.listing_object.text_color_p.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId3
                property variant synopsis_object : kids_games_listingId1.kids_games_listing_object.synopsis
                property variant btn_h : synopsisId3.synopsis_object.btn_h.value
                property variant btn_n : synopsisId3.synopsis_object.btn_n.value
                property variant btn_p : synopsisId3.synopsis_object.btn_p.value
                property variant btn_text_h : synopsisId3.synopsis_object.btn_text_h.value
                property variant btn_text_n : synopsisId3.synopsis_object.btn_text_n.value
                property variant btn_text_p : synopsisId3.synopsis_object.btn_text_p.value
                property variant close_btn_h : synopsisId3.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId3.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId3.synopsis_object.close_btn_p.value
                property variant description1_color : synopsisId3.synopsis_object.description1_color.value
                property variant dn_arrow_h : synopsisId3.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId3.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId3.synopsis_object.dn_arrow_p.value
                property variant fading_image : synopsisId3.synopsis_object.fading_image.value
                property variant panel : synopsisId3.synopsis_object.panel.value
                property variant scroll_base : synopsisId3.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId3.synopsis_object.scroll_button.value
                property variant short_title_color : synopsisId3.synopsis_object.short_title_color.value
                property variant title_color : synopsisId3.synopsis_object.title_color.value
                property variant up_arrow_h : synopsisId3.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId3.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId3.synopsis_object.up_arrow_p.value
            }
        }
        property QtObject kids_global : QtObject {
            id: kids_globalId1
            property variant kids_global_object : config_object.config_value.kids_global
            property QtObject brightness : QtObject {
                id: brightnessId2
                property variant brightness_object : kids_globalId1.kids_global_object.brightness
                property variant base : brightnessId2.brightness_object.base.value
                property variant bg : brightnessId2.brightness_object.bg.value
                property variant bri_dcr_h : brightnessId2.brightness_object.bri_dcr_h.value
                property variant bri_dcr_n : brightnessId2.brightness_object.bri_dcr_n.value
                property variant bri_dcr_p : brightnessId2.brightness_object.bri_dcr_p.value
                property variant bri_incr_h : brightnessId2.brightness_object.bri_incr_h.value
                property variant bri_incr_n : brightnessId2.brightness_object.bri_incr_n.value
                property variant bri_incr_p : brightnessId2.brightness_object.bri_incr_p.value
                property variant fill_color : brightnessId2.brightness_object.fill_color.value
                property variant indicator : brightnessId2.brightness_object.indicator.value
            }
            property QtObject header : QtObject {
                id: headerId2
                property variant header_object : kids_globalId1.kids_global_object.header
                property variant airplane_icon : headerId2.header_object.airplane_icon.value
                property variant airport_map_btn_h : headerId2.header_object.airport_map_btn_h.value
                property variant airport_map_btn_n : headerId2.header_object.airport_map_btn_n.value
                property variant airport_map_btn_p : headerId2.header_object.airport_map_btn_p.value
                property variant chat_notify_h : headerId2.header_object.chat_notify_h.value
                property variant chat_notify_n : headerId2.header_object.chat_notify_n.value
                property variant chat_notify_p : headerId2.header_object.chat_notify_p.value
                property variant chat_notify_text_color : headerId2.header_object.chat_notify_text_color.value
                property variant flight_connect_btn_h : headerId2.header_object.flight_connect_btn_h.value
                property variant flight_connect_btn_n : headerId2.header_object.flight_connect_btn_n.value
                property variant flight_connect_btn_p : headerId2.header_object.flight_connect_btn_p.value
                property variant flight_info_btn_h : headerId2.header_object.flight_info_btn_h.value
                property variant flight_info_btn_n : headerId2.header_object.flight_info_btn_n.value
                property variant flight_info_btn_p : headerId2.header_object.flight_info_btn_p.value
                property variant flight_info_close_icon : headerId2.header_object.flight_info_close_icon.value
                property variant flight_info_open_icon : headerId2.header_object.flight_info_open_icon.value
                property variant flight_path_base_image : headerId2.header_object.flight_path_base_image.value
                property variant flight_path_circle_image_h : headerId2.header_object.flight_path_circle_image_h.value
                property variant flight_path_circle_image_n : headerId2.header_object.flight_path_circle_image_n.value
                property variant flight_path_fill_color : headerId2.header_object.flight_path_fill_color.value
                property variant flight_text_color : headerId2.header_object.flight_text_color.value
                property variant header_button_h : headerId2.header_object.header_button_h.value
                property variant header_button_n : headerId2.header_object.header_button_n.value
                property variant header_button_p : headerId2.header_object.header_button_p.value
                property variant help_button_h : headerId2.header_object.help_button_h.value
                property variant help_button_n : headerId2.header_object.help_button_n.value
                property variant help_button_p : headerId2.header_object.help_button_p.value
                property variant overlay_icon1 : headerId2.header_object.overlay_icon1.value
                property variant overlay_icon2 : headerId2.header_object.overlay_icon2.value
                property variant overlay_icon3 : headerId2.header_object.overlay_icon3.value
                property variant text_color_h : headerId2.header_object.text_color_h.value
                property variant text_color_n : headerId2.header_object.text_color_n.value
                property variant text_color_p : headerId2.header_object.text_color_p.value
                property variant view_map_btn_h : headerId2.header_object.view_map_btn_h.value
                property variant view_map_btn_n : headerId2.header_object.view_map_btn_n.value
                property variant view_map_btn_p : headerId2.header_object.view_map_btn_p.value
                property variant vod_logo : headerId2.header_object.vod_logo.value
            }
            property QtObject settings : QtObject {
                id: settingsId2
                property variant settings_object : kids_globalId1.kids_global_object.settings
                property variant accept_btn_h : settingsId2.settings_object.accept_btn_h.value
                property variant accept_btn_n : settingsId2.settings_object.accept_btn_n.value
                property variant accept_btn_p : settingsId2.settings_object.accept_btn_p.value
                property variant accept_text_h : settingsId2.settings_object.accept_text_h.value
                property variant accept_text_n : settingsId2.settings_object.accept_text_n.value
                property variant accept_text_p : settingsId2.settings_object.accept_text_p.value
                property variant base_fill : settingsId2.settings_object.base_fill.value
                property variant base_image : settingsId2.settings_object.base_image.value
                property variant brightness_dcr_h : settingsId2.settings_object.brightness_dcr_h.value
                property variant brightness_dcr_n : settingsId2.settings_object.brightness_dcr_n.value
                property variant brightness_dcr_p : settingsId2.settings_object.brightness_dcr_p.value
                property variant brightness_incr_h : settingsId2.settings_object.brightness_incr_h.value
                property variant brightness_incr_n : settingsId2.settings_object.brightness_incr_n.value
                property variant brightness_incr_p : settingsId2.settings_object.brightness_incr_p.value
                property variant call_crew1_h : settingsId2.settings_object.call_crew1_h.value
                property variant call_crew1_n : settingsId2.settings_object.call_crew1_n.value
                property variant call_crew1_p : settingsId2.settings_object.call_crew1_p.value
                property variant call_crew_h : settingsId2.settings_object.call_crew_h.value
                property variant call_crew_n : settingsId2.settings_object.call_crew_n.value
                property variant call_crew_p : settingsId2.settings_object.call_crew_p.value
                property variant call_crewcancel1_h : settingsId2.settings_object.call_crewcancel1_h.value
                property variant call_crewcancel1_n : settingsId2.settings_object.call_crewcancel1_n.value
                property variant call_crewcancel1_p : settingsId2.settings_object.call_crewcancel1_p.value
                property variant call_crewcancel_h : settingsId2.settings_object.call_crewcancel_h.value
                property variant call_crewcancel_n : settingsId2.settings_object.call_crewcancel_n.value
                property variant call_crewcancel_p : settingsId2.settings_object.call_crewcancel_p.value
                property variant crew_vdivider : settingsId2.settings_object.crew_vdivider.value
                property variant disable_btn_h : settingsId2.settings_object.disable_btn_h.value
                property variant disable_btn_n : settingsId2.settings_object.disable_btn_n.value
                property variant disable_btn_p : settingsId2.settings_object.disable_btn_p.value
                property variant disable_text_h : settingsId2.settings_object.disable_text_h.value
                property variant disable_text_n : settingsId2.settings_object.disable_text_n.value
                property variant disable_text_p : settingsId2.settings_object.disable_text_p.value
                property variant indicator : settingsId2.settings_object.indicator.value
                property variant language_text_h : settingsId2.settings_object.language_text_h.value
                property variant language_text_n : settingsId2.settings_object.language_text_n.value
                property variant language_text_s : settingsId2.settings_object.language_text_s.value
                property variant left_arrow_h : settingsId2.settings_object.left_arrow_h.value
                property variant left_arrow_n : settingsId2.settings_object.left_arrow_n.value
                property variant left_arrow_p : settingsId2.settings_object.left_arrow_p.value
                property variant reading_light1_h : settingsId2.settings_object.reading_light1_h.value
                property variant reading_light1_n : settingsId2.settings_object.reading_light1_n.value
                property variant reading_light1_p : settingsId2.settings_object.reading_light1_p.value
                property variant reading_light_h : settingsId2.settings_object.reading_light_h.value
                property variant reading_light_n : settingsId2.settings_object.reading_light_n.value
                property variant reading_light_p : settingsId2.settings_object.reading_light_p.value
                property variant reading_lightoff1_h : settingsId2.settings_object.reading_lightoff1_h.value
                property variant reading_lightoff1_n : settingsId2.settings_object.reading_lightoff1_n.value
                property variant reading_lightoff1_p : settingsId2.settings_object.reading_lightoff1_p.value
                property variant reading_lightoff_h : settingsId2.settings_object.reading_lightoff_h.value
                property variant reading_lightoff_n : settingsId2.settings_object.reading_lightoff_n.value
                property variant reading_lightoff_p : settingsId2.settings_object.reading_lightoff_p.value
                property variant right_arrow_h : settingsId2.settings_object.right_arrow_h.value
                property variant right_arrow_n : settingsId2.settings_object.right_arrow_n.value
                property variant right_arrow_p : settingsId2.settings_object.right_arrow_p.value
                property variant screen_off1_h : settingsId2.settings_object.screen_off1_h.value
                property variant screen_off1_n : settingsId2.settings_object.screen_off1_n.value
                property variant screen_off1_p : settingsId2.settings_object.screen_off1_p.value
                property variant screen_off_h : settingsId2.settings_object.screen_off_h.value
                property variant screen_off_n : settingsId2.settings_object.screen_off_n.value
                property variant screen_off_p : settingsId2.settings_object.screen_off_p.value
                property variant text_color : settingsId2.settings_object.text_color.value
                property variant volume_dcr_h : settingsId2.settings_object.volume_dcr_h.value
                property variant volume_dcr_n : settingsId2.settings_object.volume_dcr_n.value
                property variant volume_dcr_p : settingsId2.settings_object.volume_dcr_p.value
                property variant volume_incr_h : settingsId2.settings_object.volume_incr_h.value
                property variant volume_incr_n : settingsId2.settings_object.volume_incr_n.value
                property variant volume_incr_p : settingsId2.settings_object.volume_incr_p.value
                property variant window_dim1_h : settingsId2.settings_object.window_dim1_h.value
                property variant window_dim1_n : settingsId2.settings_object.window_dim1_n.value
                property variant window_dim1_p : settingsId2.settings_object.window_dim1_p.value
            }
            property QtObject video : QtObject {
                id: videoId2
                property variant video_object : kids_globalId1.kids_global_object.video
                property variant  vol_dcr_p : videoId2.video_object. vol_dcr_p.value
                property variant aspect1_h : videoId2.video_object.aspect1_h.value
                property variant aspect1_n : videoId2.video_object.aspect1_n.value
                property variant aspect1_p : videoId2.video_object.aspect1_p.value
                property variant aspect2_h : videoId2.video_object.aspect2_h.value
                property variant aspect2_n : videoId2.video_object.aspect2_n.value
                property variant aspect2_p : videoId2.video_object.aspect2_p.value
                property variant base_image : videoId2.video_object.base_image.value
                property variant base_image_trailer : videoId2.video_object.base_image_trailer.value
                property variant bright_dcr_h : videoId2.video_object.bright_dcr_h.value
                property variant bright_dcr_n : videoId2.video_object.bright_dcr_n.value
                property variant bright_dcr_p : videoId2.video_object.bright_dcr_p.value
                property variant bright_incr_h : videoId2.video_object.bright_incr_h.value
                property variant bright_incr_n : videoId2.video_object.bright_incr_n.value
                property variant bright_incr_p : videoId2.video_object.bright_incr_p.value
                property variant brightness_h : videoId2.video_object.brightness_h.value
                property variant brightness_n : videoId2.video_object.brightness_n.value
                property variant brightness_p : videoId2.video_object.brightness_p.value
                property variant controller_bg : videoId2.video_object.controller_bg.value
                property variant fill_color : videoId2.video_object.fill_color.value
                property variant fill_color_trailer : videoId2.video_object.fill_color_trailer.value
                property variant forward_h : videoId2.video_object.forward_h.value
                property variant forward_n : videoId2.video_object.forward_n.value
                property variant forward_p : videoId2.video_object.forward_p.value
                property variant indicator : videoId2.video_object.indicator.value
                property variant pause_h : videoId2.video_object.pause_h.value
                property variant pause_n : videoId2.video_object.pause_n.value
                property variant pause_p : videoId2.video_object.pause_p.value
                property variant play_h : videoId2.video_object.play_h.value
                property variant play_n : videoId2.video_object.play_n.value
                property variant play_p : videoId2.video_object.play_p.value
                property variant popup_base_image : videoId2.video_object.popup_base_image.value
                property variant popup_fill_color : videoId2.video_object.popup_fill_color.value
                property variant replay_h : videoId2.video_object.replay_h.value
                property variant replay_n : videoId2.video_object.replay_n.value
                property variant replay_p : videoId2.video_object.replay_p.value
                property variant rewfor_popup : videoId2.video_object.rewfor_popup.value
                property variant rewind_h : videoId2.video_object.rewind_h.value
                property variant rewind_n : videoId2.video_object.rewind_n.value
                property variant rewind_p : videoId2.video_object.rewind_p.value
                property variant soundtrackcc_h : videoId2.video_object.soundtrackcc_h.value
                property variant soundtrackcc_n : videoId2.video_object.soundtrackcc_n.value
                property variant soundtrackcc_p : videoId2.video_object.soundtrackcc_p.value
                property variant speed_text : videoId2.video_object.speed_text.value
                property variant stop_h : videoId2.video_object.stop_h.value
                property variant stop_n : videoId2.video_object.stop_n.value
                property variant stop_p : videoId2.video_object.stop_p.value
                property variant text_color : videoId2.video_object.text_color.value
                property variant vod_btn_h : videoId2.video_object.vod_btn_h.value
                property variant vod_btn_n : videoId2.video_object.vod_btn_n.value
                property variant vod_btn_p : videoId2.video_object.vod_btn_p.value
                property variant vol_dcr_h : videoId2.video_object.vol_dcr_h.value
                property variant vol_dcr_n : videoId2.video_object.vol_dcr_n.value
                property variant vol_incr_h : videoId2.video_object.vol_incr_h.value
                property variant vol_incr_n : videoId2.video_object.vol_incr_n.value
                property variant vol_incr_p : videoId2.video_object.vol_incr_p.value
                property variant volume_h : videoId2.video_object.volume_h.value
                property variant volume_n : videoId2.video_object.volume_n.value
                property variant volume_p : videoId2.video_object.volume_p.value
                property variant volume_popup : videoId2.video_object.volume_popup.value
            }
            property QtObject volume : QtObject {
                id: volumeId2
                property variant volume_object : kids_globalId1.kids_global_object.volume
                property variant base : volumeId2.volume_object.base.value
                property variant bg : volumeId2.volume_object.bg.value
                property variant fill_color : volumeId2.volume_object.fill_color.value
                property variant handset_fill_color : volumeId2.volume_object.handset_fill_color.value
                property variant handset_indicator : volumeId2.volume_object.handset_indicator.value
                property variant indicator : volumeId2.volume_object.indicator.value
                property variant vol_dcr_h : volumeId2.volume_object.vol_dcr_h.value
                property variant vol_dcr_n : volumeId2.volume_object.vol_dcr_n.value
                property variant vol_dcr_p : volumeId2.volume_object.vol_dcr_p.value
                property variant vol_incr_h : volumeId2.volume_object.vol_incr_h.value
                property variant vol_incr_n : volumeId2.volume_object.vol_incr_n.value
                property variant vol_incr_p : volumeId2.volume_object.vol_incr_p.value
            }
        }
        property QtObject kids_help : QtObject {
            id: kids_helpId1
            property variant kids_help_object : config_object.config_value.kids_help
            property QtObject english : QtObject {
                id: englishId2
                property variant english_object : kids_helpId1.kids_help_object.english
                property variant episode_listing : englishId2.english_object.episode_listing.value
                property variant games_listing : englishId2.english_object.games_listing.value
                property variant games_synop : englishId2.english_object.games_synop.value
                property variant main_menu : englishId2.english_object.main_menu.value
                property variant movie_listing : englishId2.english_object.movie_listing.value
                property variant movie_synopsis : englishId2.english_object.movie_synopsis.value
                property variant movie_trailer : englishId2.english_object.movie_trailer.value
                property variant movie_vod : englishId2.english_object.movie_vod.value
                property variant music_listing : englishId2.english_object.music_listing.value
                property variant tracklist : englishId2.english_object.tracklist.value
                property variant tv_listing : englishId2.english_object.tv_listing.value
                property variant tv_synop : englishId2.english_object.tv_synop.value
                property variant tv_vod : englishId2.english_object.tv_vod.value
            }
            property QtObject germany : QtObject {
                id: germanyId2
                property variant germany_object : kids_helpId1.kids_help_object.germany
                property variant episode_listing : germanyId2.germany_object.episode_listing.value
                property variant games_listing : germanyId2.germany_object.games_listing.value
                property variant games_synop : germanyId2.germany_object.games_synop.value
                property variant main_menu : germanyId2.germany_object.main_menu.value
                property variant movie_listing : germanyId2.germany_object.movie_listing.value
                property variant movie_synopsis : germanyId2.germany_object.movie_synopsis.value
                property variant movie_trailer : germanyId2.germany_object.movie_trailer.value
                property variant movie_vod : germanyId2.germany_object.movie_vod.value
                property variant music_listing : germanyId2.germany_object.music_listing.value
                property variant tracklist : germanyId2.germany_object.tracklist.value
                property variant tv_listing : germanyId2.germany_object.tv_listing.value
                property variant tv_synop : germanyId2.germany_object.tv_synop.value
                property variant tv_vod : germanyId2.germany_object.tv_vod.value
            }
            property QtObject portuguese : QtObject {
                id: portugueseId2
                property variant portuguese_object : kids_helpId1.kids_help_object.portuguese
                property variant episode_listing : portugueseId2.portuguese_object.episode_listing.value
                property variant games_listing : portugueseId2.portuguese_object.games_listing.value
                property variant games_synop : portugueseId2.portuguese_object.games_synop.value
                property variant main_menu : portugueseId2.portuguese_object.main_menu.value
                property variant movie_listing : portugueseId2.portuguese_object.movie_listing.value
                property variant movie_synopsis : portugueseId2.portuguese_object.movie_synopsis.value
                property variant movie_trailer : portugueseId2.portuguese_object.movie_trailer.value
                property variant movie_vod : portugueseId2.portuguese_object.movie_vod.value
                property variant music_listing : portugueseId2.portuguese_object.music_listing.value
                property variant tracklist : portugueseId2.portuguese_object.tracklist.value
                property variant tv_listing : portugueseId2.portuguese_object.tv_listing.value
                property variant tv_synop : portugueseId2.portuguese_object.tv_synop.value
                property variant tv_vod : portugueseId2.portuguese_object.tv_vod.value
            }
            property QtObject spanish : QtObject {
                id: spanishId2
                property variant spanish_object : kids_helpId1.kids_help_object.spanish
                property variant episode_listing : spanishId2.spanish_object.episode_listing.value
                property variant games_listing : spanishId2.spanish_object.games_listing.value
                property variant games_synop : spanishId2.spanish_object.games_synop.value
                property variant main_menu : spanishId2.spanish_object.main_menu.value
                property variant movie_listing : spanishId2.spanish_object.movie_listing.value
                property variant movie_synopsis : spanishId2.spanish_object.movie_synopsis.value
                property variant movie_trailer : spanishId2.spanish_object.movie_trailer.value
                property variant movie_vod : spanishId2.spanish_object.movie_vod.value
                property variant music_listing : spanishId2.spanish_object.music_listing.value
                property variant tracklist : spanishId2.spanish_object.tracklist.value
                property variant tv_listing : spanishId2.spanish_object.tv_listing.value
                property variant tv_synop : spanishId2.spanish_object.tv_synop.value
                property variant tv_vod : spanishId2.spanish_object.tv_vod.value
            }
        }
        property QtObject kids_menu : QtObject {
            id: kids_menuId1
            property variant kids_menu_object : config_object.config_value.kids_menu
            property QtObject main : QtObject {
                id: mainId1
                property variant main_object : kids_menuId1.kids_menu_object.main
                property variant bg : mainId1.main_object.bg.value
                property variant btn_h : mainId1.main_object.btn_h.value
                property variant btn_n : mainId1.main_object.btn_n.value
                property variant btn_p : mainId1.main_object.btn_p.value
                property variant btn_s : mainId1.main_object.btn_s.value
                property variant desc_color : mainId1.main_object.desc_color.value
                property variant text_color_h : mainId1.main_object.text_color_h.value
                property variant text_color_n : mainId1.main_object.text_color_n.value
                property variant text_color_p : mainId1.main_object.text_color_p.value
                property variant wel_color : mainId1.main_object.wel_color.value
            }
        }
        property QtObject kids_movies_listing : QtObject {
            id: kids_movies_listingId1
            property variant kids_movies_listing_object : config_object.config_value.kids_movies_listing
            property QtObject listing : QtObject {
                id: listingId4
                property variant listing_object : kids_movies_listingId1.kids_movies_listing_object.listing
                property variant background : listingId4.listing_object.background.value
                property variant dark_patch : listingId4.listing_object.dark_patch.value
                property variant poster_holder_h : listingId4.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId4.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId4.listing_object.poster_holder_p.value
                property variant text_color_h : listingId4.listing_object.text_color_h.value
                property variant text_color_n : listingId4.listing_object.text_color_n.value
                property variant text_color_p : listingId4.listing_object.text_color_p.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId1
                property variant sndtrk_cc_object : kids_movies_listingId1.kids_movies_listing_object.sndtrk_cc
                property variant button_h : sndtrk_ccId1.sndtrk_cc_object.button_h.value
                property variant button_n : sndtrk_ccId1.sndtrk_cc_object.button_n.value
                property variant button_p : sndtrk_ccId1.sndtrk_cc_object.button_p.value
                property variant button_text_h : sndtrk_ccId1.sndtrk_cc_object.button_text_h.value
                property variant button_text_n : sndtrk_ccId1.sndtrk_cc_object.button_text_n.value
                property variant button_text_p : sndtrk_ccId1.sndtrk_cc_object.button_text_p.value
                property variant language_text_color_n : sndtrk_ccId1.sndtrk_cc_object.language_text_color_n.value
                property variant language_text_color_s : sndtrk_ccId1.sndtrk_cc_object.language_text_color_s.value
                property variant left_arrow_h : sndtrk_ccId1.sndtrk_cc_object.left_arrow_h.value
                property variant left_arrow_n : sndtrk_ccId1.sndtrk_cc_object.left_arrow_n.value
                property variant left_arrow_p : sndtrk_ccId1.sndtrk_cc_object.left_arrow_p.value
                property variant popup_text_color : sndtrk_ccId1.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow_h : sndtrk_ccId1.sndtrk_cc_object.right_arrow_h.value
                property variant right_arrow_n : sndtrk_ccId1.sndtrk_cc_object.right_arrow_n.value
                property variant right_arrow_p : sndtrk_ccId1.sndtrk_cc_object.right_arrow_p.value
                property variant synop_popup_bg1 : sndtrk_ccId1.sndtrk_cc_object.synop_popup_bg1.value
                property variant synop_popup_bg2 : sndtrk_ccId1.sndtrk_cc_object.synop_popup_bg2.value
                property variant synop_popup_bg3 : sndtrk_ccId1.sndtrk_cc_object.synop_popup_bg3.value
                property variant vod_popup_bg1 : sndtrk_ccId1.sndtrk_cc_object.vod_popup_bg1.value
                property variant vod_popup_bg2 : sndtrk_ccId1.sndtrk_cc_object.vod_popup_bg2.value
                property variant vod_popup_bg3 : sndtrk_ccId1.sndtrk_cc_object.vod_popup_bg3.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId4
                property variant synopsis_object : kids_movies_listingId1.kids_movies_listing_object.synopsis
                property variant btn_h : synopsisId4.synopsis_object.btn_h.value
                property variant btn_n : synopsisId4.synopsis_object.btn_n.value
                property variant btn_p : synopsisId4.synopsis_object.btn_p.value
                property variant btn_text_h : synopsisId4.synopsis_object.btn_text_h.value
                property variant btn_text_n : synopsisId4.synopsis_object.btn_text_n.value
                property variant btn_text_p : synopsisId4.synopsis_object.btn_text_p.value
                property variant close_btn_h : synopsisId4.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId4.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId4.synopsis_object.close_btn_p.value
                property variant description1_color : synopsisId4.synopsis_object.description1_color.value
                property variant description2_color : synopsisId4.synopsis_object.description2_color.value
                property variant description3_color : synopsisId4.synopsis_object.description3_color.value
                property variant dn_arrow_h : synopsisId4.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId4.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId4.synopsis_object.dn_arrow_p.value
                property variant duration_icon : synopsisId4.synopsis_object.duration_icon.value
                property variant fading_image : synopsisId4.synopsis_object.fading_image.value
                property variant halfstar_icon_s : synopsisId4.synopsis_object.halfstar_icon_s.value
                property variant panel : synopsisId4.synopsis_object.panel.value
                property variant play_icon_h : synopsisId4.synopsis_object.play_icon_h.value
                property variant play_icon_n : synopsisId4.synopsis_object.play_icon_n.value
                property variant play_icon_p : synopsisId4.synopsis_object.play_icon_p.value
                property variant scroll_base : synopsisId4.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId4.synopsis_object.scroll_button.value
                property variant star_icon_n : synopsisId4.synopsis_object.star_icon_n.value
                property variant star_icon_s : synopsisId4.synopsis_object.star_icon_s.value
                property variant subtitle_color : synopsisId4.synopsis_object.subtitle_color.value
                property variant title_color : synopsisId4.synopsis_object.title_color.value
                property variant trailor_btn_h : synopsisId4.synopsis_object.trailor_btn_h.value
                property variant trailor_btn_n : synopsisId4.synopsis_object.trailor_btn_n.value
                property variant trailor_btn_p : synopsisId4.synopsis_object.trailor_btn_p.value
                property variant up_arrow_h : synopsisId4.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId4.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId4.synopsis_object.up_arrow_p.value
                property variant watch_trailer_color : synopsisId4.synopsis_object.watch_trailer_color.value
            }
        }
        property QtObject kids_music_listing : QtObject {
            id: kids_music_listingId1
            property variant kids_music_listing_object : config_object.config_value.kids_music_listing
            property QtObject listing : QtObject {
                id: listingId5
                property variant listing_object : kids_music_listingId1.kids_music_listing_object.listing
                property variant background : listingId5.listing_object.background.value
                property variant dark_patch : listingId5.listing_object.dark_patch.value
                property variant poster_holder_h : listingId5.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId5.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId5.listing_object.poster_holder_p.value
                property variant text_color_h : listingId5.listing_object.text_color_h.value
                property variant text_color_n : listingId5.listing_object.text_color_n.value
                property variant text_color_p : listingId5.listing_object.text_color_p.value
            }
            property QtObject tracklist : QtObject {
                id: tracklistId1
                property variant tracklist_object : kids_music_listingId1.kids_music_listing_object.tracklist
                property variant base_image : tracklistId1.tracklist_object.base_image.value
                property variant close_btn_h : tracklistId1.tracklist_object.close_btn_h.value
                property variant close_btn_n : tracklistId1.tracklist_object.close_btn_n.value
                property variant close_btn_p : tracklistId1.tracklist_object.close_btn_p.value
                property variant description1_color : tracklistId1.tracklist_object.description1_color.value
                property variant dn_arrow_h : tracklistId1.tracklist_object.dn_arrow_h.value
                property variant dn_arrow_n : tracklistId1.tracklist_object.dn_arrow_n.value
                property variant dn_arrow_p : tracklistId1.tracklist_object.dn_arrow_p.value
                property variant fading_image : tracklistId1.tracklist_object.fading_image.value
                property variant fill_image : tracklistId1.tracklist_object.fill_image.value
                property variant indicator : tracklistId1.tracklist_object.indicator.value
                property variant listing_highlight : tracklistId1.tracklist_object.listing_highlight.value
                property variant next_button_h : tracklistId1.tracklist_object.next_button_h.value
                property variant next_button_n : tracklistId1.tracklist_object.next_button_n.value
                property variant next_button_p : tracklistId1.tracklist_object.next_button_p.value
                property variant now_playing_icon : tracklistId1.tracklist_object.now_playing_icon.value
                property variant panel : tracklistId1.tracklist_object.panel.value
                property variant pause_button_h : tracklistId1.tracklist_object.pause_button_h.value
                property variant pause_button_n : tracklistId1.tracklist_object.pause_button_n.value
                property variant pause_button_p : tracklistId1.tracklist_object.pause_button_p.value
                property variant play_button_h : tracklistId1.tracklist_object.play_button_h.value
                property variant play_button_n : tracklistId1.tracklist_object.play_button_n.value
                property variant play_button_p : tracklistId1.tracklist_object.play_button_p.value
                property variant prev_button_h : tracklistId1.tracklist_object.prev_button_h.value
                property variant prev_button_n : tracklistId1.tracklist_object.prev_button_n.value
                property variant prev_button_p : tracklistId1.tracklist_object.prev_button_p.value
                property variant repeat_off_h : tracklistId1.tracklist_object.repeat_off_h.value
                property variant repeat_off_n : tracklistId1.tracklist_object.repeat_off_n.value
                property variant repeat_off_p : tracklistId1.tracklist_object.repeat_off_p.value
                property variant repeat_on_h : tracklistId1.tracklist_object.repeat_on_h.value
                property variant repeat_on_n : tracklistId1.tracklist_object.repeat_on_n.value
                property variant repeat_on_p : tracklistId1.tracklist_object.repeat_on_p.value
                property variant scroll_base : tracklistId1.tracklist_object.scroll_base.value
                property variant scroll_button : tracklistId1.tracklist_object.scroll_button.value
                property variant short_title_color : tracklistId1.tracklist_object.short_title_color.value
                property variant shuffle_off_h : tracklistId1.tracklist_object.shuffle_off_h.value
                property variant shuffle_off_n : tracklistId1.tracklist_object.shuffle_off_n.value
                property variant shuffle_off_p : tracklistId1.tracklist_object.shuffle_off_p.value
                property variant shuffle_on_h : tracklistId1.tracklist_object.shuffle_on_h.value
                property variant shuffle_on_n : tracklistId1.tracklist_object.shuffle_on_n.value
                property variant shuffle_on_p : tracklistId1.tracklist_object.shuffle_on_p.value
                property variant title_color : tracklistId1.tracklist_object.title_color.value
                property variant up_arrow_h : tracklistId1.tracklist_object.up_arrow_h.value
                property variant up_arrow_n : tracklistId1.tracklist_object.up_arrow_n.value
                property variant up_arrow_p : tracklistId1.tracklist_object.up_arrow_p.value
                property variant volume_button_h : tracklistId1.tracklist_object.volume_button_h.value
                property variant volume_button_n : tracklistId1.tracklist_object.volume_button_n.value
                property variant volume_button_p : tracklistId1.tracklist_object.volume_button_p.value
            }
        }
        property QtObject kids_popup : QtObject {
            id: kids_popupId1
            property variant kids_popup_object : config_object.config_value.kids_popup
            property QtObject gamelaunch : QtObject {
                id: gamelaunchId1
                property variant gamelaunch_object : kids_popupId1.kids_popup_object.gamelaunch
                property variant loading : gamelaunchId1.gamelaunch_object.loading.value
            }
            property QtObject generic : QtObject {
                id: genericId1
                property variant generic_object : kids_popupId1.kids_popup_object.generic
                property variant bg_color : genericId1.generic_object.bg_color.value
                property variant btn_h : genericId1.generic_object.btn_h.value
                property variant btn_n : genericId1.generic_object.btn_n.value
                property variant btn_p : genericId1.generic_object.btn_p.value
                property variant btn_text_h : genericId1.generic_object.btn_text_h.value
                property variant btn_text_n : genericId1.generic_object.btn_text_n.value
                property variant btn_text_p : genericId1.generic_object.btn_text_p.value
                property variant ct_icon : genericId1.generic_object.ct_icon.value
                property variant text_color : genericId1.generic_object.text_color.value
            }
        }
        property QtObject kids_tv_listing : QtObject {
            id: kids_tv_listingId1
            property variant kids_tv_listing_object : config_object.config_value.kids_tv_listing
            property QtObject listing : QtObject {
                id: listingId6
                property variant listing_object : kids_tv_listingId1.kids_tv_listing_object.listing
                property variant background : listingId6.listing_object.background.value
                property variant dark_patch : listingId6.listing_object.dark_patch.value
                property variant poster_holder_h : listingId6.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId6.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId6.listing_object.poster_holder_p.value
                property variant text_color_h : listingId6.listing_object.text_color_h.value
                property variant text_color_n : listingId6.listing_object.text_color_n.value
                property variant text_color_p : listingId6.listing_object.text_color_p.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId2
                property variant sndtrk_cc_object : kids_tv_listingId1.kids_tv_listing_object.sndtrk_cc
                property variant button_h : sndtrk_ccId2.sndtrk_cc_object.button_h.value
                property variant button_n : sndtrk_ccId2.sndtrk_cc_object.button_n.value
                property variant button_p : sndtrk_ccId2.sndtrk_cc_object.button_p.value
                property variant button_text_h : sndtrk_ccId2.sndtrk_cc_object.button_text_h.value
                property variant button_text_n : sndtrk_ccId2.sndtrk_cc_object.button_text_n.value
                property variant button_text_p : sndtrk_ccId2.sndtrk_cc_object.button_text_p.value
                property variant language_text_color_n : sndtrk_ccId2.sndtrk_cc_object.language_text_color_n.value
                property variant language_text_color_s : sndtrk_ccId2.sndtrk_cc_object.language_text_color_s.value
                property variant left_arrow_h : sndtrk_ccId2.sndtrk_cc_object.left_arrow_h.value
                property variant left_arrow_n : sndtrk_ccId2.sndtrk_cc_object.left_arrow_n.value
                property variant left_arrow_p : sndtrk_ccId2.sndtrk_cc_object.left_arrow_p.value
                property variant popup_text_color : sndtrk_ccId2.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow_h : sndtrk_ccId2.sndtrk_cc_object.right_arrow_h.value
                property variant right_arrow_n : sndtrk_ccId2.sndtrk_cc_object.right_arrow_n.value
                property variant right_arrow_p : sndtrk_ccId2.sndtrk_cc_object.right_arrow_p.value
                property variant synop_popup_bg1 : sndtrk_ccId2.sndtrk_cc_object.synop_popup_bg1.value
                property variant synop_popup_bg2 : sndtrk_ccId2.sndtrk_cc_object.synop_popup_bg2.value
                property variant vod_popup_bg1 : sndtrk_ccId2.sndtrk_cc_object.vod_popup_bg1.value
                property variant vod_popup_bg2 : sndtrk_ccId2.sndtrk_cc_object.vod_popup_bg2.value
                property variant vod_popup_bg3 : sndtrk_ccId2.sndtrk_cc_object.vod_popup_bg3.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId5
                property variant synopsis_object : kids_tv_listingId1.kids_tv_listing_object.synopsis
                property variant btn_h : synopsisId5.synopsis_object.btn_h.value
                property variant btn_n : synopsisId5.synopsis_object.btn_n.value
                property variant btn_p : synopsisId5.synopsis_object.btn_p.value
                property variant btn_text_h : synopsisId5.synopsis_object.btn_text_h.value
                property variant btn_text_n : synopsisId5.synopsis_object.btn_text_n.value
                property variant btn_text_p : synopsisId5.synopsis_object.btn_text_p.value
                property variant close_btn_h : synopsisId5.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId5.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId5.synopsis_object.close_btn_p.value
                property variant description1_color : synopsisId5.synopsis_object.description1_color.value
                property variant description2_color : synopsisId5.synopsis_object.description2_color.value
                property variant dn_arrow_h : synopsisId5.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId5.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId5.synopsis_object.dn_arrow_p.value
                property variant fading_image : synopsisId5.synopsis_object.fading_image.value
                property variant listing_highlight : synopsisId5.synopsis_object.listing_highlight.value
                property variant panel : synopsisId5.synopsis_object.panel.value
                property variant play_icon_h : synopsisId5.synopsis_object.play_icon_h.value
                property variant play_icon_n : synopsisId5.synopsis_object.play_icon_n.value
                property variant play_icon_p : synopsisId5.synopsis_object.play_icon_p.value
                property variant scroll_base : synopsisId5.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId5.synopsis_object.scroll_button.value
                property variant subtitle_color : synopsisId5.synopsis_object.subtitle_color.value
                property variant title_color : synopsisId5.synopsis_object.title_color.value
                property variant up_arrow_h : synopsisId5.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId5.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId5.synopsis_object.up_arrow_p.value
            }
        }
        property QtObject lifemiles : QtObject {
            id: lifemilesId1
            property variant lifemiles_object : config_object.config_value.lifemiles
            property QtObject screen : QtObject {
                id: screenId3
                property variant screen_object : lifemilesId1.lifemiles_object.screen
                property variant app_arwdwn_h : screenId3.screen_object.app_arwdwn_h.value
                property variant app_arwdwn_n : screenId3.screen_object.app_arwdwn_n.value
                property variant app_arwup_h : screenId3.screen_object.app_arwup_h.value
                property variant app_arwup_n : screenId3.screen_object.app_arwup_n.value
                property variant close_h : screenId3.screen_object.close_h.value
                property variant close_n : screenId3.screen_object.close_n.value
                property variant close_p : screenId3.screen_object.close_p.value
                property variant common_menubtn_h : screenId3.screen_object.common_menubtn_h.value
                property variant common_menubtn_n : screenId3.screen_object.common_menubtn_n.value
                property variant common_menubtn_p : screenId3.screen_object.common_menubtn_p.value
                property variant discover_bg : screenId3.screen_object.discover_bg.value
                property variant discoverbtn_h : screenId3.screen_object.discoverbtn_h.value
                property variant discoverbtn_n : screenId3.screen_object.discoverbtn_n.value
                property variant discoverbtn_p : screenId3.screen_object.discoverbtn_p.value
                property variant earn_plusbtn_h : screenId3.screen_object.earn_plusbtn_h.value
                property variant earn_plusbtn_n : screenId3.screen_object.earn_plusbtn_n.value
                property variant lflogo : screenId3.screen_object.lflogo.value
                property variant logobtn_h : screenId3.screen_object.logobtn_h.value
                property variant logobtn_n : screenId3.screen_object.logobtn_n.value
                property variant logobtn_p : screenId3.screen_object.logobtn_p.value
                property variant menulist_bgd : screenId3.screen_object.menulist_bgd.value
                property variant outline : screenId3.screen_object.outline.value
                property variant popup : screenId3.screen_object.popup.value
                property variant scrollbg : screenId3.screen_object.scrollbg.value
                property variant slider : screenId3.screen_object.slider.value
                property variant text_discover_info : screenId3.screen_object.text_discover_info.value
                property variant text_discover_title : screenId3.screen_object.text_discover_title.value
                property variant text_elite_list : screenId3.screen_object.text_elite_list.value
                property variant text_list : screenId3.screen_object.text_list.value
                property variant text_title : screenId3.screen_object.text_title.value
                property variant text_title_info : screenId3.screen_object.text_title_info.value
                property variant text_txt_basline : screenId3.screen_object.text_txt_basline.value
            }
        }
        property QtObject meal : QtObject {
            id: mealId1
            property variant meal_object : config_object.config_value.meal
            property QtObject listing : QtObject {
                id: listingId7
                property variant listing_object : mealId1.meal_object.listing
                property variant background : listingId7.listing_object.background.value
                property variant close_btn_h : listingId7.listing_object.close_btn_h.value
                property variant close_btn_n : listingId7.listing_object.close_btn_n.value
                property variant close_btn_p : listingId7.listing_object.close_btn_p.value
                property variant curve_image : listingId7.listing_object.curve_image.value
                property variant curve_image_small : listingId7.listing_object.curve_image_small.value
                property variant logo : listingId7.listing_object.logo.value
                property variant menu_beverages_h : listingId7.listing_object.menu_beverages_h.value
                property variant menu_beverages_n : listingId7.listing_object.menu_beverages_n.value
                property variant menu_beverages_p : listingId7.listing_object.menu_beverages_p.value
                property variant menu_beverages_s : listingId7.listing_object.menu_beverages_s.value
                property variant menu_breakfast_h : listingId7.listing_object.menu_breakfast_h.value
                property variant menu_breakfast_n : listingId7.listing_object.menu_breakfast_n.value
                property variant menu_breakfast_p : listingId7.listing_object.menu_breakfast_p.value
                property variant menu_breakfast_s : listingId7.listing_object.menu_breakfast_s.value
                property variant menu_btn_text_color_h : listingId7.listing_object.menu_btn_text_color_h.value
                property variant menu_btn_text_color_n : listingId7.listing_object.menu_btn_text_color_n.value
                property variant menu_btn_text_color_p : listingId7.listing_object.menu_btn_text_color_p.value
                property variant menu_btn_text_color_s : listingId7.listing_object.menu_btn_text_color_s.value
                property variant menu_liquors_h : listingId7.listing_object.menu_liquors_h.value
                property variant menu_liquors_n : listingId7.listing_object.menu_liquors_n.value
                property variant menu_liquors_p : listingId7.listing_object.menu_liquors_p.value
                property variant menu_liquors_s : listingId7.listing_object.menu_liquors_s.value
                property variant menu_lunch_h : listingId7.listing_object.menu_lunch_h.value
                property variant menu_lunch_n : listingId7.listing_object.menu_lunch_n.value
                property variant menu_lunch_p : listingId7.listing_object.menu_lunch_p.value
                property variant menu_lunch_s : listingId7.listing_object.menu_lunch_s.value
                property variant menubtn_big_h : listingId7.listing_object.menubtn_big_h.value
                property variant menubtn_big_n : listingId7.listing_object.menubtn_big_n.value
                property variant menubtn_big_p : listingId7.listing_object.menubtn_big_p.value
                property variant menubtn_big_s : listingId7.listing_object.menubtn_big_s.value
                property variant menubtn_small_h : listingId7.listing_object.menubtn_small_h.value
                property variant menubtn_small_n : listingId7.listing_object.menubtn_small_n.value
                property variant menubtn_small_p : listingId7.listing_object.menubtn_small_p.value
                property variant menubtn_small_s : listingId7.listing_object.menubtn_small_s.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId6
                property variant synopsis_object : mealId1.meal_object.synopsis
                property variant dn_arrow_h : synopsisId6.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId6.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId6.synopsis_object.dn_arrow_p.value
                property variant menu_cal_color : synopsisId6.synopsis_object.menu_cal_color.value
                property variant menu_desc_color : synopsisId6.synopsis_object.menu_desc_color.value
                property variant menu_title_color : synopsisId6.synopsis_object.menu_title_color.value
                property variant optionbtn_h : synopsisId6.synopsis_object.optionbtn_h.value
                property variant optionbtn_n : synopsisId6.synopsis_object.optionbtn_n.value
                property variant optionbtn_p : synopsisId6.synopsis_object.optionbtn_p.value
                property variant optionbtn_s : synopsisId6.synopsis_object.optionbtn_s.value
                property variant optionbtn_text_h : synopsisId6.synopsis_object.optionbtn_text_h.value
                property variant optionbtn_text_n : synopsisId6.synopsis_object.optionbtn_text_n.value
                property variant optionbtn_text_p : synopsisId6.synopsis_object.optionbtn_text_p.value
                property variant optionbtn_text_s : synopsisId6.synopsis_object.optionbtn_text_s.value
                property variant scroll_base : synopsisId6.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId6.synopsis_object.scroll_button.value
                property variant scroll_fadeimage : synopsisId6.synopsis_object.scroll_fadeimage.value
                property variant synop_bg : synopsisId6.synopsis_object.synop_bg.value
                property variant title_color : synopsisId6.synopsis_object.title_color.value
                property variant up_arrow_h : synopsisId6.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId6.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId6.synopsis_object.up_arrow_p.value
            }
        }
        property QtObject menu : QtObject {
            id: menuId1
            property variant menu_object : config_object.config_value.menu
            property QtObject main : QtObject {
                id: mainId2
                property variant main_object : menuId1.menu_object.main
                property variant bg : mainId2.main_object.bg.value
                property variant btn_h : mainId2.main_object.btn_h.value
                property variant btn_n : mainId2.main_object.btn_n.value
                property variant btn_p : mainId2.main_object.btn_p.value
                property variant btn_s : mainId2.main_object.btn_s.value
                property variant desc_color : mainId2.main_object.desc_color.value
                property variant text_color_h : mainId2.main_object.text_color_h.value
                property variant text_color_n : mainId2.main_object.text_color_n.value
                property variant text_color_p : mainId2.main_object.text_color_p.value
                property variant wel_color : mainId2.main_object.wel_color.value
            }
            property QtObject submenu : QtObject {
                id: submenuId1
                property variant submenu_object : menuId1.menu_object.submenu
                property variant band : submenuId1.submenu_object.band.value
                property variant btn_h : submenuId1.submenu_object.btn_h.value
                property variant btn_n : submenuId1.submenu_object.btn_n.value
                property variant btn_p : submenuId1.submenu_object.btn_p.value
                property variant leftarrow : submenuId1.submenu_object.leftarrow.value
                property variant rightarrow : submenuId1.submenu_object.rightarrow.value
                property variant select_icon : submenuId1.submenu_object.select_icon.value
                property variant text_color_h : submenuId1.submenu_object.text_color_h.value
                property variant text_color_n : submenuId1.submenu_object.text_color_n.value
                property variant text_color_p : submenuId1.submenu_object.text_color_p.value
            }
        }
        property QtObject movies : QtObject {
            id: moviesId1
            property variant movies_object : config_object.config_value.movies
            property QtObject listing : QtObject {
                id: listingId8
                property variant listing_object : moviesId1.movies_object.listing
                property variant background : listingId8.listing_object.background.value
                property variant dark_patch : listingId8.listing_object.dark_patch.value
                property variant poster_holder_h : listingId8.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId8.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId8.listing_object.poster_holder_p.value
                property variant text_color_h : listingId8.listing_object.text_color_h.value
                property variant text_color_n : listingId8.listing_object.text_color_n.value
                property variant text_color_p : listingId8.listing_object.text_color_p.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId3
                property variant sndtrk_cc_object : moviesId1.movies_object.sndtrk_cc
                property variant button_h : sndtrk_ccId3.sndtrk_cc_object.button_h.value
                property variant button_n : sndtrk_ccId3.sndtrk_cc_object.button_n.value
                property variant button_p : sndtrk_ccId3.sndtrk_cc_object.button_p.value
                property variant button_text_h : sndtrk_ccId3.sndtrk_cc_object.button_text_h.value
                property variant button_text_n : sndtrk_ccId3.sndtrk_cc_object.button_text_n.value
                property variant button_text_p : sndtrk_ccId3.sndtrk_cc_object.button_text_p.value
                property variant language_text_color_n : sndtrk_ccId3.sndtrk_cc_object.language_text_color_n.value
                property variant language_text_color_s : sndtrk_ccId3.sndtrk_cc_object.language_text_color_s.value
                property variant left_arrow_h : sndtrk_ccId3.sndtrk_cc_object.left_arrow_h.value
                property variant left_arrow_n : sndtrk_ccId3.sndtrk_cc_object.left_arrow_n.value
                property variant left_arrow_p : sndtrk_ccId3.sndtrk_cc_object.left_arrow_p.value
                property variant popup_text_color : sndtrk_ccId3.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow_h : sndtrk_ccId3.sndtrk_cc_object.right_arrow_h.value
                property variant right_arrow_n : sndtrk_ccId3.sndtrk_cc_object.right_arrow_n.value
                property variant right_arrow_p : sndtrk_ccId3.sndtrk_cc_object.right_arrow_p.value
                property variant synop_popup_bg1 : sndtrk_ccId3.sndtrk_cc_object.synop_popup_bg1.value
                property variant synop_popup_bg2 : sndtrk_ccId3.sndtrk_cc_object.synop_popup_bg2.value
                property variant synop_popup_bg3 : sndtrk_ccId3.sndtrk_cc_object.synop_popup_bg3.value
                property variant vod_popup_bg1 : sndtrk_ccId3.sndtrk_cc_object.vod_popup_bg1.value
                property variant vod_popup_bg2 : sndtrk_ccId3.sndtrk_cc_object.vod_popup_bg2.value
                property variant vod_popup_bg3 : sndtrk_ccId3.sndtrk_cc_object.vod_popup_bg3.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId7
                property variant synopsis_object : moviesId1.movies_object.synopsis
                property variant btn_h : synopsisId7.synopsis_object.btn_h.value
                property variant btn_n : synopsisId7.synopsis_object.btn_n.value
                property variant btn_p : synopsisId7.synopsis_object.btn_p.value
                property variant btn_text_h : synopsisId7.synopsis_object.btn_text_h.value
                property variant btn_text_n : synopsisId7.synopsis_object.btn_text_n.value
                property variant btn_text_p : synopsisId7.synopsis_object.btn_text_p.value
                property variant close_btn_h : synopsisId7.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId7.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId7.synopsis_object.close_btn_p.value
                property variant description1_color : synopsisId7.synopsis_object.description1_color.value
                property variant description2_color : synopsisId7.synopsis_object.description2_color.value
                property variant description3_color : synopsisId7.synopsis_object.description3_color.value
                property variant dn_arrow_h : synopsisId7.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId7.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId7.synopsis_object.dn_arrow_p.value
                property variant duration_icon : synopsisId7.synopsis_object.duration_icon.value
                property variant fading_image : synopsisId7.synopsis_object.fading_image.value
                property variant halfstar_icon_s : synopsisId7.synopsis_object.halfstar_icon_s.value
                property variant panel : synopsisId7.synopsis_object.panel.value
                property variant play_icon_h : synopsisId7.synopsis_object.play_icon_h.value
                property variant play_icon_n : synopsisId7.synopsis_object.play_icon_n.value
                property variant play_icon_p : synopsisId7.synopsis_object.play_icon_p.value
                property variant scroll_base : synopsisId7.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId7.synopsis_object.scroll_button.value
                property variant star_icon_n : synopsisId7.synopsis_object.star_icon_n.value
                property variant star_icon_s : synopsisId7.synopsis_object.star_icon_s.value
                property variant subtitle_color : synopsisId7.synopsis_object.subtitle_color.value
                property variant title_color : synopsisId7.synopsis_object.title_color.value
                property variant trailor_btn_h : synopsisId7.synopsis_object.trailor_btn_h.value
                property variant trailor_btn_n : synopsisId7.synopsis_object.trailor_btn_n.value
                property variant trailor_btn_p : synopsisId7.synopsis_object.trailor_btn_p.value
                property variant up_arrow_h : synopsisId7.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId7.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId7.synopsis_object.up_arrow_p.value
                property variant watch_trailer_color : synopsisId7.synopsis_object.watch_trailer_color.value
            }
        }
        property QtObject music : QtObject {
            id: musicId1
            property variant music_object : config_object.config_value.music
            property QtObject artist : QtObject {
                id: artistId1
                property variant artist_object : musicId1.music_object.artist
                property variant albumlist_highlight : artistId1.artist_object.albumlist_highlight.value
                property variant albumlist_panel : artistId1.artist_object.albumlist_panel.value
                property variant alpha_bg_color : artistId1.artist_object.alpha_bg_color.value
                property variant alpha_text_color : artistId1.artist_object.alpha_text_color.value
                property variant list_highlight : artistId1.artist_object.list_highlight.value
                property variant list_panel : artistId1.artist_object.list_panel.value
                property variant tracklist_panel : artistId1.artist_object.tracklist_panel.value
            }
            property QtObject listing : QtObject {
                id: listingId9
                property variant listing_object : musicId1.music_object.listing
                property variant background : listingId9.listing_object.background.value
                property variant dark_patch : listingId9.listing_object.dark_patch.value
                property variant poster_holder_h : listingId9.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId9.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId9.listing_object.poster_holder_p.value
                property variant text_color_h : listingId9.listing_object.text_color_h.value
                property variant text_color_n : listingId9.listing_object.text_color_n.value
                property variant text_color_p : listingId9.listing_object.text_color_p.value
            }
            property QtObject playlist : QtObject {
                id: playlistId1
                property variant playlist_object : musicId1.music_object.playlist
                property variant dn_arrow_h : playlistId1.playlist_object.dn_arrow_h.value
                property variant dn_arrow_n : playlistId1.playlist_object.dn_arrow_n.value
                property variant dn_arrow_p : playlistId1.playlist_object.dn_arrow_p.value
                property variant genre_panel : playlistId1.playlist_object.genre_panel.value
                property variant now_play_text : playlistId1.playlist_object.now_play_text.value
                property variant playlist_empty_panel : playlistId1.playlist_object.playlist_empty_panel.value
                property variant playlist_list_highlight : playlistId1.playlist_object.playlist_list_highlight.value
                property variant playlist_panel : playlistId1.playlist_object.playlist_panel.value
                property variant remove_button_d : playlistId1.playlist_object.remove_button_d.value
                property variant remove_button_h : playlistId1.playlist_object.remove_button_h.value
                property variant remove_button_n : playlistId1.playlist_object.remove_button_n.value
                property variant remove_button_p : playlistId1.playlist_object.remove_button_p.value
                property variant scroll_base : playlistId1.playlist_object.scroll_base.value
                property variant scroll_button : playlistId1.playlist_object.scroll_button.value
                property variant up_arrow_h : playlistId1.playlist_object.up_arrow_h.value
                property variant up_arrow_n : playlistId1.playlist_object.up_arrow_n.value
                property variant up_arrow_p : playlistId1.playlist_object.up_arrow_p.value
            }
            property QtObject popup : QtObject {
                id: popupId1
                property variant popup_object : musicId1.music_object.popup
                property variant close_btn_h : popupId1.popup_object.close_btn_h.value
                property variant close_btn_n : popupId1.popup_object.close_btn_n.value
                property variant close_btn_p : popupId1.popup_object.close_btn_p.value
                property variant description_color : popupId1.popup_object.description_color.value
                property variant empty_button_h : popupId1.popup_object.empty_button_h.value
                property variant empty_button_n : popupId1.popup_object.empty_button_n.value
                property variant empty_button_p : popupId1.popup_object.empty_button_p.value
                property variant empty_button_text_h : popupId1.popup_object.empty_button_text_h.value
                property variant empty_button_text_n : popupId1.popup_object.empty_button_text_n.value
                property variant empty_button_text_p : popupId1.popup_object.empty_button_text_p.value
                property variant playlist_selection_h : popupId1.popup_object.playlist_selection_h.value
                property variant playlist_selection_n : popupId1.popup_object.playlist_selection_n.value
                property variant playlist_selection_p : popupId1.popup_object.playlist_selection_p.value
                property variant playlist_text_h : popupId1.popup_object.playlist_text_h.value
                property variant playlist_text_n : popupId1.popup_object.playlist_text_n.value
                property variant playlist_text_p : popupId1.popup_object.playlist_text_p.value
                property variant title_color : popupId1.popup_object.title_color.value
            }
            property QtObject radio : QtObject {
                id: radioId1
                property variant radio_object : musicId1.music_object.radio
                property variant mute_off_h : radioId1.radio_object.mute_off_h.value
                property variant mute_off_n : radioId1.radio_object.mute_off_n.value
                property variant mute_off_p : radioId1.radio_object.mute_off_p.value
                property variant mute_on_h : radioId1.radio_object.mute_on_h.value
                property variant mute_on_n : radioId1.radio_object.mute_on_n.value
                property variant mute_on_p : radioId1.radio_object.mute_on_p.value
                property variant panel_bg : radioId1.radio_object.panel_bg.value
                property variant play_h : radioId1.radio_object.play_h.value
                property variant play_n : radioId1.radio_object.play_n.value
                property variant play_p : radioId1.radio_object.play_p.value
                property variant radio_highlight : radioId1.radio_object.radio_highlight.value
                property variant radio_vol_h : radioId1.radio_object.radio_vol_h.value
                property variant radio_vol_n : radioId1.radio_object.radio_vol_n.value
                property variant stop_h : radioId1.radio_object.stop_h.value
                property variant stop_n : radioId1.radio_object.stop_n.value
                property variant stop_p : radioId1.radio_object.stop_p.value
            }
            property QtObject tracklist : QtObject {
                id: tracklistId2
                property variant tracklist_object : musicId1.music_object.tracklist
                property variant add_all_btn_h : tracklistId2.tracklist_object.add_all_btn_h.value
                property variant add_all_btn_n : tracklistId2.tracklist_object.add_all_btn_n.value
                property variant add_all_btn_p : tracklistId2.tracklist_object.add_all_btn_p.value
                property variant base_image : tracklistId2.tracklist_object.base_image.value
                property variant btn_h : tracklistId2.tracklist_object.btn_h.value
                property variant btn_n : tracklistId2.tracklist_object.btn_n.value
                property variant btn_p : tracklistId2.tracklist_object.btn_p.value
                property variant btn_text_h : tracklistId2.tracklist_object.btn_text_h.value
                property variant btn_text_n : tracklistId2.tracklist_object.btn_text_n.value
                property variant btn_text_p : tracklistId2.tracklist_object.btn_text_p.value
                property variant check_box_all_h : tracklistId2.tracklist_object.check_box_all_h.value
                property variant check_box_all_n : tracklistId2.tracklist_object.check_box_all_n.value
                property variant check_box_all_p : tracklistId2.tracklist_object.check_box_all_p.value
                property variant check_box_h : tracklistId2.tracklist_object.check_box_h.value
                property variant check_box_n : tracklistId2.tracklist_object.check_box_n.value
                property variant check_box_p : tracklistId2.tracklist_object.check_box_p.value
                property variant close_btn_h : tracklistId2.tracklist_object.close_btn_h.value
                property variant close_btn_n : tracklistId2.tracklist_object.close_btn_n.value
                property variant close_btn_p : tracklistId2.tracklist_object.close_btn_p.value
                property variant description1_color : tracklistId2.tracklist_object.description1_color.value
                property variant dn_arrow_h : tracklistId2.tracklist_object.dn_arrow_h.value
                property variant dn_arrow_n : tracklistId2.tracklist_object.dn_arrow_n.value
                property variant dn_arrow_p : tracklistId2.tracklist_object.dn_arrow_p.value
                property variant fading_image : tracklistId2.tracklist_object.fading_image.value
                property variant fill_image : tracklistId2.tracklist_object.fill_image.value
                property variant indicator : tracklistId2.tracklist_object.indicator.value
                property variant listing_highlight : tracklistId2.tracklist_object.listing_highlight.value
                property variant next_button_h : tracklistId2.tracklist_object.next_button_h.value
                property variant next_button_n : tracklistId2.tracklist_object.next_button_n.value
                property variant next_button_p : tracklistId2.tracklist_object.next_button_p.value
                property variant now_playing_icon : tracklistId2.tracklist_object.now_playing_icon.value
                property variant panel : tracklistId2.tracklist_object.panel.value
                property variant pause_button_h : tracklistId2.tracklist_object.pause_button_h.value
                property variant pause_button_n : tracklistId2.tracklist_object.pause_button_n.value
                property variant pause_button_p : tracklistId2.tracklist_object.pause_button_p.value
                property variant play_button_h : tracklistId2.tracklist_object.play_button_h.value
                property variant play_button_n : tracklistId2.tracklist_object.play_button_n.value
                property variant play_button_p : tracklistId2.tracklist_object.play_button_p.value
                property variant prev_button_h : tracklistId2.tracklist_object.prev_button_h.value
                property variant prev_button_n : tracklistId2.tracklist_object.prev_button_n.value
                property variant prev_button_p : tracklistId2.tracklist_object.prev_button_p.value
                property variant repeat_off_h : tracklistId2.tracklist_object.repeat_off_h.value
                property variant repeat_off_n : tracklistId2.tracklist_object.repeat_off_n.value
                property variant repeat_off_p : tracklistId2.tracklist_object.repeat_off_p.value
                property variant repeat_on_h : tracklistId2.tracklist_object.repeat_on_h.value
                property variant repeat_on_n : tracklistId2.tracklist_object.repeat_on_n.value
                property variant repeat_on_p : tracklistId2.tracklist_object.repeat_on_p.value
                property variant scroll_base : tracklistId2.tracklist_object.scroll_base.value
                property variant scroll_button : tracklistId2.tracklist_object.scroll_button.value
                property variant short_title_color : tracklistId2.tracklist_object.short_title_color.value
                property variant shuffle_off_h : tracklistId2.tracklist_object.shuffle_off_h.value
                property variant shuffle_off_n : tracklistId2.tracklist_object.shuffle_off_n.value
                property variant shuffle_off_p : tracklistId2.tracklist_object.shuffle_off_p.value
                property variant shuffle_on_h : tracklistId2.tracklist_object.shuffle_on_h.value
                property variant shuffle_on_n : tracklistId2.tracklist_object.shuffle_on_n.value
                property variant shuffle_on_p : tracklistId2.tracklist_object.shuffle_on_p.value
                property variant tick_box_h : tracklistId2.tracklist_object.tick_box_h.value
                property variant tick_box_n : tracklistId2.tracklist_object.tick_box_n.value
                property variant tick_box_p : tracklistId2.tracklist_object.tick_box_p.value
                property variant tickbox_all_h : tracklistId2.tracklist_object.tickbox_all_h.value
                property variant tickbox_all_n : tracklistId2.tracklist_object.tickbox_all_n.value
                property variant tickbox_all_p : tracklistId2.tracklist_object.tickbox_all_p.value
                property variant title_color : tracklistId2.tracklist_object.title_color.value
                property variant up_arrow_h : tracklistId2.tracklist_object.up_arrow_h.value
                property variant up_arrow_n : tracklistId2.tracklist_object.up_arrow_n.value
                property variant up_arrow_p : tracklistId2.tracklist_object.up_arrow_p.value
                property variant volume_button_h : tracklistId2.tracklist_object.volume_button_h.value
                property variant volume_button_n : tracklistId2.tracklist_object.volume_button_n.value
                property variant volume_button_p : tracklistId2.tracklist_object.volume_button_p.value
            }
        }
        property QtObject popup : QtObject {
            id: popupId2
            property variant popup_object : config_object.config_value.popup
            property QtObject dim : QtObject {
                id: dimId1
                property variant dim_object : popupId2.popup_object.dim
                property variant btn_bg : dimId1.dim_object.btn_bg.value
                property variant close_btn_h : dimId1.dim_object.close_btn_h.value
                property variant close_btn_n : dimId1.dim_object.close_btn_n.value
                property variant close_btn_p : dimId1.dim_object.close_btn_p.value
                property variant minus_h : dimId1.dim_object.minus_h.value
                property variant minus_n : dimId1.dim_object.minus_n.value
                property variant minus_p : dimId1.dim_object.minus_p.value
                property variant plus_h : dimId1.dim_object.plus_h.value
                property variant plus_n : dimId1.dim_object.plus_n.value
                property variant plus_p : dimId1.dim_object.plus_p.value
            }
            property QtObject gamelaunch : QtObject {
                id: gamelaunchId2
                property variant gamelaunch_object : popupId2.popup_object.gamelaunch
                property variant loading : gamelaunchId2.gamelaunch_object.loading.value
            }
            property QtObject generic : QtObject {
                id: genericId2
                property variant generic_object : popupId2.popup_object.generic
                property variant bg_color : genericId2.generic_object.bg_color.value
                property variant btn_h : genericId2.generic_object.btn_h.value
                property variant btn_n : genericId2.generic_object.btn_n.value
                property variant btn_p : genericId2.generic_object.btn_p.value
                property variant btn_text_h : genericId2.generic_object.btn_text_h.value
                property variant btn_text_n : genericId2.generic_object.btn_text_n.value
                property variant btn_text_p : genericId2.generic_object.btn_text_p.value
                property variant close_btn_h : genericId2.generic_object.close_btn_h.value
                property variant close_btn_n : genericId2.generic_object.close_btn_n.value
                property variant close_btn_p : genericId2.generic_object.close_btn_p.value
                property variant ct_icon : genericId2.generic_object.ct_icon.value
                property variant text_color : genericId2.generic_object.text_color.value
            }
            property QtObject seatchatlaunch : QtObject {
                id: seatchatlaunchId1
                property variant seatchatlaunch_object : popupId2.popup_object.seatchatlaunch
                property variant loading : seatchatlaunchId1.seatchatlaunch_object.loading.value
                property variant loading_text : seatchatlaunchId1.seatchatlaunch_object.loading_text.value
            }
        }
        property QtObject seatchat : QtObject {
            id: seatchatId1
            property variant seatchat_object : config_object.config_value.seatchat
            property QtObject blockseat_popup : QtObject {
                id: blockseat_popupId1
                property variant blockseat_popup_object : seatchatId1.seatchat_object.blockseat_popup
                property variant bg_color : blockseat_popupId1.blockseat_popup_object.bg_color.value
                property variant btn_h : blockseat_popupId1.blockseat_popup_object.btn_h.value
                property variant btn_n : blockseat_popupId1.blockseat_popup_object.btn_n.value
                property variant btn_p : blockseat_popupId1.blockseat_popup_object.btn_p.value
                property variant btn_text_h : blockseat_popupId1.blockseat_popup_object.btn_text_h.value
                property variant btn_text_n : blockseat_popupId1.blockseat_popup_object.btn_text_n.value
                property variant btn_text_p : blockseat_popupId1.blockseat_popup_object.btn_text_p.value
                property variant check_box_h : blockseat_popupId1.blockseat_popup_object.check_box_h.value
                property variant check_box_n : blockseat_popupId1.blockseat_popup_object.check_box_n.value
                property variant check_box_p : blockseat_popupId1.blockseat_popup_object.check_box_p.value
                property variant close_btn_h : blockseat_popupId1.blockseat_popup_object.close_btn_h.value
                property variant close_btn_n : blockseat_popupId1.blockseat_popup_object.close_btn_n.value
                property variant close_btn_p : blockseat_popupId1.blockseat_popup_object.close_btn_p.value
                property variant dn_arrow_h : blockseat_popupId1.blockseat_popup_object.dn_arrow_h.value
                property variant dn_arrow_n : blockseat_popupId1.blockseat_popup_object.dn_arrow_n.value
                property variant dn_arrow_p : blockseat_popupId1.blockseat_popup_object.dn_arrow_p.value
                property variant scroll_base : blockseat_popupId1.blockseat_popup_object.scroll_base.value
                property variant scroll_button : blockseat_popupId1.blockseat_popup_object.scroll_button.value
                property variant subtitle_color : blockseat_popupId1.blockseat_popup_object.subtitle_color.value
                property variant tick_box_h : blockseat_popupId1.blockseat_popup_object.tick_box_h.value
                property variant tick_box_n : blockseat_popupId1.blockseat_popup_object.tick_box_n.value
                property variant tick_box_p : blockseat_popupId1.blockseat_popup_object.tick_box_p.value
                property variant title_color : blockseat_popupId1.blockseat_popup_object.title_color.value
                property variant up_arrow_h : blockseat_popupId1.blockseat_popup_object.up_arrow_h.value
                property variant up_arrow_n : blockseat_popupId1.blockseat_popup_object.up_arrow_n.value
                property variant up_arrow_p : blockseat_popupId1.blockseat_popup_object.up_arrow_p.value
            }
            property QtObject chatroom : QtObject {
                id: chatroomId1
                property variant chatroom_object : seatchatId1.seatchat_object.chatroom
                property variant block_btn_h : chatroomId1.chatroom_object.block_btn_h.value
                property variant block_btn_n : chatroomId1.chatroom_object.block_btn_n.value
                property variant block_btn_p : chatroomId1.chatroom_object.block_btn_p.value
                property variant bottom_fade_bg : chatroomId1.chatroom_object.bottom_fade_bg.value
                property variant btn_h : chatroomId1.chatroom_object.btn_h.value
                property variant btn_n : chatroomId1.chatroom_object.btn_n.value
                property variant btn_p : chatroomId1.chatroom_object.btn_p.value
                property variant btn_text_h : chatroomId1.chatroom_object.btn_text_h.value
                property variant btn_text_n : chatroomId1.chatroom_object.btn_text_n.value
                property variant btn_text_p : chatroomId1.chatroom_object.btn_text_p.value
                property variant chat_joined_txt : chatroomId1.chatroom_object.chat_joined_txt.value
                property variant chat_txt : chatroomId1.chatroom_object.chat_txt.value
                property variant chatconf_icon_h : chatroomId1.chatroom_object.chatconf_icon_h.value
                property variant chatconf_icon_n : chatroomId1.chatroom_object.chatconf_icon_n.value
                property variant chatfrnd_icon_h : chatroomId1.chatroom_object.chatfrnd_icon_h.value
                property variant chatfrnd_icon_n : chatroomId1.chatroom_object.chatfrnd_icon_n.value
                property variant chatfrnd_name_h : chatroomId1.chatroom_object.chatfrnd_name_h.value
                property variant chatfrnd_name_n : chatroomId1.chatroom_object.chatfrnd_name_n.value
                property variant chatfrnd_seatno_h : chatroomId1.chatroom_object.chatfrnd_seatno_h.value
                property variant chatfrnd_seatno_n : chatroomId1.chatroom_object.chatfrnd_seatno_n.value
                property variant close_btn_h : chatroomId1.chatroom_object.close_btn_h.value
                property variant close_btn_n : chatroomId1.chatroom_object.close_btn_n.value
                property variant close_btn_p : chatroomId1.chatroom_object.close_btn_p.value
                property variant conference_btn_h : chatroomId1.chatroom_object.conference_btn_h.value
                property variant conference_btn_n : chatroomId1.chatroom_object.conference_btn_n.value
                property variant conference_btn_p : chatroomId1.chatroom_object.conference_btn_p.value
                property variant dn_arrow_h : chatroomId1.chatroom_object.dn_arrow_h.value
                property variant dn_arrow_n : chatroomId1.chatroom_object.dn_arrow_n.value
                property variant dn_arrow_p : chatroomId1.chatroom_object.dn_arrow_p.value
                property variant friendlist_highlight : chatroomId1.chatroom_object.friendlist_highlight.value
                property variant input_d : chatroomId1.chatroom_object.input_d.value
                property variant input_h : chatroomId1.chatroom_object.input_h.value
                property variant input_n : chatroomId1.chatroom_object.input_n.value
                property variant input_p : chatroomId1.chatroom_object.input_p.value
                property variant input_txt : chatroomId1.chatroom_object.input_txt.value
                property variant instructions_txt : chatroomId1.chatroom_object.instructions_txt.value
                property variant me_divider : chatroomId1.chatroom_object.me_divider.value
                property variant me_txt : chatroomId1.chatroom_object.me_txt.value
                property variant newmsg_icon_h : chatroomId1.chatroom_object.newmsg_icon_h.value
                property variant newmsg_icon_n : chatroomId1.chatroom_object.newmsg_icon_n.value
                property variant notification_txt_n : chatroomId1.chatroom_object.notification_txt_n.value
                property variant panel : chatroomId1.chatroom_object.panel.value
                property variant said_divider : chatroomId1.chatroom_object.said_divider.value
                property variant said_txt : chatroomId1.chatroom_object.said_txt.value
                property variant scroll_base : chatroomId1.chatroom_object.scroll_base.value
                property variant scroll_button : chatroomId1.chatroom_object.scroll_button.value
                property variant title_color : chatroomId1.chatroom_object.title_color.value
                property variant top_fade_bg : chatroomId1.chatroom_object.top_fade_bg.value
                property variant up_arrow_h : chatroomId1.chatroom_object.up_arrow_h.value
                property variant up_arrow_n : chatroomId1.chatroom_object.up_arrow_n.value
                property variant up_arrow_p : chatroomId1.chatroom_object.up_arrow_p.value
                property variant usercount : chatroomId1.chatroom_object.usercount.value
                property variant vkb_d : chatroomId1.chatroom_object.vkb_d.value
                property variant vkb_h : chatroomId1.chatroom_object.vkb_h.value
                property variant vkb_n : chatroomId1.chatroom_object.vkb_n.value
                property variant vkb_p : chatroomId1.chatroom_object.vkb_p.value
                property variant welcome_txt : chatroomId1.chatroom_object.welcome_txt.value
            }
            property QtObject invite_popup : QtObject {
                id: invite_popupId1
                property variant invite_popup_object : seatchatId1.seatchat_object.invite_popup
                property variant add_tochat_h : invite_popupId1.invite_popup_object.add_tochat_h.value
                property variant add_tochat_n : invite_popupId1.invite_popup_object.add_tochat_n.value
                property variant add_tochat_p : invite_popupId1.invite_popup_object.add_tochat_p.value
                property variant addtochat_h : invite_popupId1.invite_popup_object.addtochat_h.value
                property variant addtochat_n : invite_popupId1.invite_popup_object.addtochat_n.value
                property variant addtochat_p : invite_popupId1.invite_popup_object.addtochat_p.value
                property variant backspace_icon_h : invite_popupId1.invite_popup_object.backspace_icon_h.value
                property variant backspace_icon_n : invite_popupId1.invite_popup_object.backspace_icon_n.value
                property variant backspace_icon_p : invite_popupId1.invite_popup_object.backspace_icon_p.value
                property variant bg_color : invite_popupId1.invite_popup_object.bg_color.value
                property variant btn_h : invite_popupId1.invite_popup_object.btn_h.value
                property variant btn_n : invite_popupId1.invite_popup_object.btn_n.value
                property variant btn_p : invite_popupId1.invite_popup_object.btn_p.value
                property variant btn_text_h : invite_popupId1.invite_popup_object.btn_text_h.value
                property variant btn_text_n : invite_popupId1.invite_popup_object.btn_text_n.value
                property variant btn_text_p : invite_popupId1.invite_popup_object.btn_text_p.value
                property variant close_btn_h : invite_popupId1.invite_popup_object.close_btn_h.value
                property variant close_btn_n : invite_popupId1.invite_popup_object.close_btn_n.value
                property variant close_btn_p : invite_popupId1.invite_popup_object.close_btn_p.value
                property variant dail_btn_text_h : invite_popupId1.invite_popup_object.dail_btn_text_h.value
                property variant dail_btn_text_n : invite_popupId1.invite_popup_object.dail_btn_text_n.value
                property variant dail_btn_text_p : invite_popupId1.invite_popup_object.dail_btn_text_p.value
                property variant dailnum_h : invite_popupId1.invite_popup_object.dailnum_h.value
                property variant dailnum_n : invite_popupId1.invite_popup_object.dailnum_n.value
                property variant dailnum_p : invite_popupId1.invite_popup_object.dailnum_p.value
                property variant input_h : invite_popupId1.invite_popup_object.input_h.value
                property variant input_n : invite_popupId1.invite_popup_object.input_n.value
                property variant input_p : invite_popupId1.invite_popup_object.input_p.value
                property variant invalidseat_msg : invite_popupId1.invite_popup_object.invalidseat_msg.value
                property variant left_arrow_h : invite_popupId1.invite_popup_object.left_arrow_h.value
                property variant left_arrow_n : invite_popupId1.invite_popup_object.left_arrow_n.value
                property variant left_arrow_p : invite_popupId1.invite_popup_object.left_arrow_p.value
                property variant right_arrow_h : invite_popupId1.invite_popup_object.right_arrow_h.value
                property variant right_arrow_n : invite_popupId1.invite_popup_object.right_arrow_n.value
                property variant right_arrow_p : invite_popupId1.invite_popup_object.right_arrow_p.value
                property variant seatlist_btn_h : invite_popupId1.invite_popup_object.seatlist_btn_h.value
                property variant seatlist_btn_n : invite_popupId1.invite_popup_object.seatlist_btn_n.value
                property variant seatlist_btn_p : invite_popupId1.invite_popup_object.seatlist_btn_p.value
                property variant seatlist_title : invite_popupId1.invite_popup_object.seatlist_title.value
                property variant subtitle_color : invite_popupId1.invite_popup_object.subtitle_color.value
                property variant title_color : invite_popupId1.invite_popup_object.title_color.value
            }
            property QtObject listing : QtObject {
                id: listingId10
                property variant listing_object : seatchatId1.seatchat_object.listing
                property variant background : listingId10.listing_object.background.value
                property variant dark_patch : listingId10.listing_object.dark_patch.value
                property variant poster_holder_h : listingId10.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId10.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId10.listing_object.poster_holder_p.value
                property variant text_color_h : listingId10.listing_object.text_color_h.value
                property variant text_color_n : listingId10.listing_object.text_color_n.value
                property variant text_color_p : listingId10.listing_object.text_color_p.value
            }
            property QtObject nickname : QtObject {
                id: nicknameId1
                property variant nickname_object : seatchatId1.seatchat_object.nickname
                property variant btn_h : nicknameId1.nickname_object.btn_h.value
                property variant btn_n : nicknameId1.nickname_object.btn_n.value
                property variant btn_p : nicknameId1.nickname_object.btn_p.value
                property variant btn_text_h : nicknameId1.nickname_object.btn_text_h.value
                property variant btn_text_n : nicknameId1.nickname_object.btn_text_n.value
                property variant btn_text_p : nicknameId1.nickname_object.btn_text_p.value
                property variant close_btn_h : nicknameId1.nickname_object.close_btn_h.value
                property variant close_btn_n : nicknameId1.nickname_object.close_btn_n.value
                property variant close_btn_p : nicknameId1.nickname_object.close_btn_p.value
                property variant input_h : nicknameId1.nickname_object.input_h.value
                property variant input_n : nicknameId1.nickname_object.input_n.value
                property variant input_p : nicknameId1.nickname_object.input_p.value
                property variant message_color : nicknameId1.nickname_object.message_color.value
                property variant panel : nicknameId1.nickname_object.panel.value
                property variant seatname_color : nicknameId1.nickname_object.seatname_color.value
                property variant seatnum_color : nicknameId1.nickname_object.seatnum_color.value
                property variant title_color : nicknameId1.nickname_object.title_color.value
                property variant vkb_d : nicknameId1.nickname_object.vkb_d.value
                property variant vkb_h : nicknameId1.nickname_object.vkb_h.value
                property variant vkb_n : nicknameId1.nickname_object.vkb_n.value
                property variant vkb_p : nicknameId1.nickname_object.vkb_p.value
            }
            property QtObject screen : QtObject {
                id: screenId4
                property variant screen_object : seatchatId1.seatchat_object.screen
                property variant sceen : screenId4.screen_object.sceen.value
                property variant seatcat_background : screenId4.screen_object.seatcat_background.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId8
                property variant synopsis_object : seatchatId1.seatchat_object.synopsis
                property variant btn_h : synopsisId8.synopsis_object.btn_h.value
                property variant btn_n : synopsisId8.synopsis_object.btn_n.value
                property variant btn_p : synopsisId8.synopsis_object.btn_p.value
                property variant btn_text_h : synopsisId8.synopsis_object.btn_text_h.value
                property variant btn_text_n : synopsisId8.synopsis_object.btn_text_n.value
                property variant btn_text_p : synopsisId8.synopsis_object.btn_text_p.value
                property variant close_btn_h : synopsisId8.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId8.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId8.synopsis_object.close_btn_p.value
                property variant description_color : synopsisId8.synopsis_object.description_color.value
                property variant panel : synopsisId8.synopsis_object.panel.value
                property variant title_color : synopsisId8.synopsis_object.title_color.value
            }
            property QtObject terms_condition : QtObject {
                id: terms_conditionId1
                property variant terms_condition_object : seatchatId1.seatchat_object.terms_condition
                property variant btn_h : terms_conditionId1.terms_condition_object.btn_h.value
                property variant btn_n : terms_conditionId1.terms_condition_object.btn_n.value
                property variant btn_p : terms_conditionId1.terms_condition_object.btn_p.value
                property variant btn_text_h : terms_conditionId1.terms_condition_object.btn_text_h.value
                property variant btn_text_n : terms_conditionId1.terms_condition_object.btn_text_n.value
                property variant btn_text_p : terms_conditionId1.terms_condition_object.btn_text_p.value
                property variant description1_color : terms_conditionId1.terms_condition_object.description1_color.value
                property variant dn_arrow_h : terms_conditionId1.terms_condition_object.dn_arrow_h.value
                property variant dn_arrow_n : terms_conditionId1.terms_condition_object.dn_arrow_n.value
                property variant dn_arrow_p : terms_conditionId1.terms_condition_object.dn_arrow_p.value
                property variant panel : terms_conditionId1.terms_condition_object.panel.value
                property variant scroll_base : terms_conditionId1.terms_condition_object.scroll_base.value
                property variant scroll_button : terms_conditionId1.terms_condition_object.scroll_button.value
                property variant title_color : terms_conditionId1.terms_condition_object.title_color.value
                property variant up_arrow_h : terms_conditionId1.terms_condition_object.up_arrow_h.value
                property variant up_arrow_n : terms_conditionId1.terms_condition_object.up_arrow_n.value
                property variant up_arrow_p : terms_conditionId1.terms_condition_object.up_arrow_p.value
            }
        }
        property QtObject shop : QtObject {
            id: shopId1
            property variant shop_object : config_object.config_value.shop
            property QtObject listing : QtObject {
                id: listingId11
                property variant listing_object : shopId1.shop_object.listing
                property variant background : listingId11.listing_object.background.value
                property variant dark_patch : listingId11.listing_object.dark_patch.value
                property variant poster_holder_h : listingId11.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId11.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId11.listing_object.poster_holder_p.value
                property variant text_color_h : listingId11.listing_object.text_color_h.value
                property variant text_color_n : listingId11.listing_object.text_color_n.value
                property variant text_color_p : listingId11.listing_object.text_color_p.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId9
                property variant synopsis_object : shopId1.shop_object.synopsis
                property variant close_btn_h : synopsisId9.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId9.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId9.synopsis_object.close_btn_p.value
                property variant currency_color : synopsisId9.synopsis_object.currency_color.value
                property variant description1_color : synopsisId9.synopsis_object.description1_color.value
                property variant details_color : synopsisId9.synopsis_object.details_color.value
                property variant dn_arrow_h : synopsisId9.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId9.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId9.synopsis_object.dn_arrow_p.value
                property variant fading_image : synopsisId9.synopsis_object.fading_image.value
                property variant panel : synopsisId9.synopsis_object.panel.value
                property variant poster_overlay : synopsisId9.synopsis_object.poster_overlay.value
                property variant price_color : synopsisId9.synopsis_object.price_color.value
                property variant scroll_base : synopsisId9.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId9.synopsis_object.scroll_button.value
                property variant short_title_color : synopsisId9.synopsis_object.short_title_color.value
                property variant title_color : synopsisId9.synopsis_object.title_color.value
                property variant up_arrow_h : synopsisId9.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId9.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId9.synopsis_object.up_arrow_p.value
            }
        }
        property QtObject tv : QtObject {
            id: tvId1
            property variant tv_object : config_object.config_value.tv
            property QtObject listing : QtObject {
                id: listingId12
                property variant listing_object : tvId1.tv_object.listing
                property variant background : listingId12.listing_object.background.value
                property variant dark_patch : listingId12.listing_object.dark_patch.value
                property variant poster_holder_h : listingId12.listing_object.poster_holder_h.value
                property variant poster_holder_n : listingId12.listing_object.poster_holder_n.value
                property variant poster_holder_p : listingId12.listing_object.poster_holder_p.value
                property variant text_color_h : listingId12.listing_object.text_color_h.value
                property variant text_color_n : listingId12.listing_object.text_color_n.value
                property variant text_color_p : listingId12.listing_object.text_color_p.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId4
                property variant sndtrk_cc_object : tvId1.tv_object.sndtrk_cc
                property variant button_h : sndtrk_ccId4.sndtrk_cc_object.button_h.value
                property variant button_n : sndtrk_ccId4.sndtrk_cc_object.button_n.value
                property variant button_p : sndtrk_ccId4.sndtrk_cc_object.button_p.value
                property variant button_text_h : sndtrk_ccId4.sndtrk_cc_object.button_text_h.value
                property variant button_text_n : sndtrk_ccId4.sndtrk_cc_object.button_text_n.value
                property variant button_text_p : sndtrk_ccId4.sndtrk_cc_object.button_text_p.value
                property variant language_text_color_n : sndtrk_ccId4.sndtrk_cc_object.language_text_color_n.value
                property variant language_text_color_s : sndtrk_ccId4.sndtrk_cc_object.language_text_color_s.value
                property variant left_arrow_h : sndtrk_ccId4.sndtrk_cc_object.left_arrow_h.value
                property variant left_arrow_n : sndtrk_ccId4.sndtrk_cc_object.left_arrow_n.value
                property variant left_arrow_p : sndtrk_ccId4.sndtrk_cc_object.left_arrow_p.value
                property variant popup_text_color : sndtrk_ccId4.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow_h : sndtrk_ccId4.sndtrk_cc_object.right_arrow_h.value
                property variant right_arrow_n : sndtrk_ccId4.sndtrk_cc_object.right_arrow_n.value
                property variant right_arrow_p : sndtrk_ccId4.sndtrk_cc_object.right_arrow_p.value
                property variant synop_popup_bg1 : sndtrk_ccId4.sndtrk_cc_object.synop_popup_bg1.value
                property variant synop_popup_bg2 : sndtrk_ccId4.sndtrk_cc_object.synop_popup_bg2.value
                property variant vod_popup_bg1 : sndtrk_ccId4.sndtrk_cc_object.vod_popup_bg1.value
                property variant vod_popup_bg2 : sndtrk_ccId4.sndtrk_cc_object.vod_popup_bg2.value
                property variant vod_popup_bg3 : sndtrk_ccId4.sndtrk_cc_object.vod_popup_bg3.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId10
                property variant synopsis_object : tvId1.tv_object.synopsis
                property variant btn_h : synopsisId10.synopsis_object.btn_h.value
                property variant btn_n : synopsisId10.synopsis_object.btn_n.value
                property variant btn_p : synopsisId10.synopsis_object.btn_p.value
                property variant btn_text_h : synopsisId10.synopsis_object.btn_text_h.value
                property variant btn_text_n : synopsisId10.synopsis_object.btn_text_n.value
                property variant btn_text_p : synopsisId10.synopsis_object.btn_text_p.value
                property variant close_btn_h : synopsisId10.synopsis_object.close_btn_h.value
                property variant close_btn_n : synopsisId10.synopsis_object.close_btn_n.value
                property variant close_btn_p : synopsisId10.synopsis_object.close_btn_p.value
                property variant description1_color : synopsisId10.synopsis_object.description1_color.value
                property variant description2_color : synopsisId10.synopsis_object.description2_color.value
                property variant dn_arrow_h : synopsisId10.synopsis_object.dn_arrow_h.value
                property variant dn_arrow_n : synopsisId10.synopsis_object.dn_arrow_n.value
                property variant dn_arrow_p : synopsisId10.synopsis_object.dn_arrow_p.value
                property variant fading_image : synopsisId10.synopsis_object.fading_image.value
                property variant listing_highlight : synopsisId10.synopsis_object.listing_highlight.value
                property variant panel : synopsisId10.synopsis_object.panel.value
                property variant play_icon_h : synopsisId10.synopsis_object.play_icon_h.value
                property variant play_icon_n : synopsisId10.synopsis_object.play_icon_n.value
                property variant play_icon_p : synopsisId10.synopsis_object.play_icon_p.value
                property variant scroll_base : synopsisId10.synopsis_object.scroll_base.value
                property variant scroll_button : synopsisId10.synopsis_object.scroll_button.value
                property variant subtitle_color : synopsisId10.synopsis_object.subtitle_color.value
                property variant title_color : synopsisId10.synopsis_object.title_color.value
                property variant up_arrow_h : synopsisId10.synopsis_object.up_arrow_h.value
                property variant up_arrow_n : synopsisId10.synopsis_object.up_arrow_n.value
                property variant up_arrow_p : synopsisId10.synopsis_object.up_arrow_p.value
            }
        }
        property QtObject usb : QtObject {
            id: usbId1
            property variant usb_object : config_object.config_value.usb
            property QtObject screen : QtObject {
                id: screenId5
                property variant screen_object : usbId1.usb_object.screen
                property variant back_h : screenId5.screen_object.back_h.value
                property variant back_n : screenId5.screen_object.back_n.value
                property variant back_p : screenId5.screen_object.back_p.value
                property variant base_image : screenId5.screen_object.base_image.value
                property variant btn_h : screenId5.screen_object.btn_h.value
                property variant btn_n : screenId5.screen_object.btn_n.value
                property variant btn_p : screenId5.screen_object.btn_p.value
                property variant btn_text_h : screenId5.screen_object.btn_text_h.value
                property variant btn_text_n : screenId5.screen_object.btn_text_n.value
                property variant btn_text_p : screenId5.screen_object.btn_text_p.value
                property variant close_btn_h : screenId5.screen_object.close_btn_h.value
                property variant close_btn_n : screenId5.screen_object.close_btn_n.value
                property variant close_btn_p : screenId5.screen_object.close_btn_p.value
                property variant description1_color : screenId5.screen_object.description1_color.value
                property variant dn_arrow_h : screenId5.screen_object.dn_arrow_h.value
                property variant dn_arrow_n : screenId5.screen_object.dn_arrow_n.value
                property variant dn_arrow_p : screenId5.screen_object.dn_arrow_p.value
                property variant fading_image : screenId5.screen_object.fading_image.value
                property variant fill_image : screenId5.screen_object.fill_image.value
                property variant folder_icon : screenId5.screen_object.folder_icon.value
                property variant indicator : screenId5.screen_object.indicator.value
                property variant list_title_color : screenId5.screen_object.list_title_color.value
                property variant listing_highlight : screenId5.screen_object.listing_highlight.value
                property variant next_button_h : screenId5.screen_object.next_button_h.value
                property variant next_button_n : screenId5.screen_object.next_button_n.value
                property variant next_button_p : screenId5.screen_object.next_button_p.value
                property variant now_playing_icon : screenId5.screen_object.now_playing_icon.value
                property variant path_text_color : screenId5.screen_object.path_text_color.value
                property variant pause_button_h : screenId5.screen_object.pause_button_h.value
                property variant pause_button_n : screenId5.screen_object.pause_button_n.value
                property variant pause_button_p : screenId5.screen_object.pause_button_p.value
                property variant play_button_h : screenId5.screen_object.play_button_h.value
                property variant play_button_n : screenId5.screen_object.play_button_n.value
                property variant play_button_p : screenId5.screen_object.play_button_p.value
                property variant prev_button_h : screenId5.screen_object.prev_button_h.value
                property variant prev_button_n : screenId5.screen_object.prev_button_n.value
                property variant prev_button_p : screenId5.screen_object.prev_button_p.value
                property variant repeat_off_h : screenId5.screen_object.repeat_off_h.value
                property variant repeat_off_n : screenId5.screen_object.repeat_off_n.value
                property variant repeat_off_p : screenId5.screen_object.repeat_off_p.value
                property variant repeat_on_h : screenId5.screen_object.repeat_on_h.value
                property variant repeat_on_n : screenId5.screen_object.repeat_on_n.value
                property variant repeat_on_p : screenId5.screen_object.repeat_on_p.value
                property variant rotate1_h : screenId5.screen_object.rotate1_h.value
                property variant rotate1_n : screenId5.screen_object.rotate1_n.value
                property variant rotate1_p : screenId5.screen_object.rotate1_p.value
                property variant rotate2_h : screenId5.screen_object.rotate2_h.value
                property variant rotate2_n : screenId5.screen_object.rotate2_n.value
                property variant rotate2_p : screenId5.screen_object.rotate2_p.value
                property variant scroll_base : screenId5.screen_object.scroll_base.value
                property variant scroll_button : screenId5.screen_object.scroll_button.value
                property variant shuffle_off_h : screenId5.screen_object.shuffle_off_h.value
                property variant shuffle_off_n : screenId5.screen_object.shuffle_off_n.value
                property variant shuffle_off_p : screenId5.screen_object.shuffle_off_p.value
                property variant shuffle_on_h : screenId5.screen_object.shuffle_on_h.value
                property variant shuffle_on_n : screenId5.screen_object.shuffle_on_n.value
                property variant shuffle_on_p : screenId5.screen_object.shuffle_on_p.value
                property variant slide_show_control_bg : screenId5.screen_object.slide_show_control_bg.value
                property variant slide_show_panel_fill : screenId5.screen_object.slide_show_panel_fill.value
                property variant slide_show_text_color : screenId5.screen_object.slide_show_text_color.value
                property variant tc_panel : screenId5.screen_object.tc_panel.value
                property variant title_color : screenId5.screen_object.title_color.value
                property variant up_arrow_h : screenId5.screen_object.up_arrow_h.value
                property variant up_arrow_n : screenId5.screen_object.up_arrow_n.value
                property variant up_arrow_p : screenId5.screen_object.up_arrow_p.value
                property variant usb_background : screenId5.screen_object.usb_background.value
                property variant usb_music_panel : screenId5.screen_object.usb_music_panel.value
                property variant usb_panel : screenId5.screen_object.usb_panel.value
                property variant usb_thumb_highlight : screenId5.screen_object.usb_thumb_highlight.value
                property variant volume_button_h : screenId5.screen_object.volume_button_h.value
                property variant volume_button_n : screenId5.screen_object.volume_button_n.value
                property variant volume_button_p : screenId5.screen_object.volume_button_p.value
            }
        }
        property QtObject vkb : QtObject {
            id: vkbId1
            property variant vkb_object : config_object.config_value.vkb
            property QtObject screen : QtObject {
                id: screenId6
                property variant screen_object : vkbId1.vkb_object.screen
                property variant backspace_icon_h : screenId6.screen_object.backspace_icon_h.value
                property variant backspace_icon_n : screenId6.screen_object.backspace_icon_n.value
                property variant backspace_icon_p : screenId6.screen_object.backspace_icon_p.value
                property variant btn_popup_bg : screenId6.screen_object.btn_popup_bg.value
                property variant btn_popup_h : screenId6.screen_object.btn_popup_h.value
                property variant btn_popup_indicator : screenId6.screen_object.btn_popup_indicator.value
                property variant caps_h : screenId6.screen_object.caps_h.value
                property variant caps_n : screenId6.screen_object.caps_n.value
                property variant caps_p : screenId6.screen_object.caps_p.value
                property variant caps_s : screenId6.screen_object.caps_s.value
                property variant close_btn_h : screenId6.screen_object.close_btn_h.value
                property variant close_btn_n : screenId6.screen_object.close_btn_n.value
                property variant close_btn_p : screenId6.screen_object.close_btn_p.value
                property variant common_h : screenId6.screen_object.common_h.value
                property variant common_n : screenId6.screen_object.common_n.value
                property variant common_p : screenId6.screen_object.common_p.value
                property variant common_s : screenId6.screen_object.common_s.value
                property variant confirm_h : screenId6.screen_object.confirm_h.value
                property variant confirm_n : screenId6.screen_object.confirm_n.value
                property variant confirm_p : screenId6.screen_object.confirm_p.value
                property variant dn_arrow_h : screenId6.screen_object.dn_arrow_h.value
                property variant dn_arrow_n : screenId6.screen_object.dn_arrow_n.value
                property variant dn_arrow_p : screenId6.screen_object.dn_arrow_p.value
                property variant enter_h : screenId6.screen_object.enter_h.value
                property variant enter_n : screenId6.screen_object.enter_n.value
                property variant enter_p : screenId6.screen_object.enter_p.value
                property variant left_arrow_h : screenId6.screen_object.left_arrow_h.value
                property variant left_arrow_n : screenId6.screen_object.left_arrow_n.value
                property variant left_arrow_p : screenId6.screen_object.left_arrow_p.value
                property variant limit_text : screenId6.screen_object.limit_text.value
                property variant num_h : screenId6.screen_object.num_h.value
                property variant num_n : screenId6.screen_object.num_n.value
                property variant num_p : screenId6.screen_object.num_p.value
                property variant num_s : screenId6.screen_object.num_s.value
                property variant right_arrow_h : screenId6.screen_object.right_arrow_h.value
                property variant right_arrow_n : screenId6.screen_object.right_arrow_n.value
                property variant right_arrow_p : screenId6.screen_object.right_arrow_p.value
                property variant shift_icon_h : screenId6.screen_object.shift_icon_h.value
                property variant shift_icon_n : screenId6.screen_object.shift_icon_n.value
                property variant shift_icon_p : screenId6.screen_object.shift_icon_p.value
                property variant space_h : screenId6.screen_object.space_h.value
                property variant space_n : screenId6.screen_object.space_n.value
                property variant space_p : screenId6.screen_object.space_p.value
                property variant space_s : screenId6.screen_object.space_s.value
                property variant text_bg : screenId6.screen_object.text_bg.value
                property variant text_color : screenId6.screen_object.text_color.value
                property variant up_arrow_h : screenId6.screen_object.up_arrow_h.value
                property variant up_arrow_n : screenId6.screen_object.up_arrow_n.value
                property variant up_arrow_p : screenId6.screen_object.up_arrow_p.value
                property variant vkb_btn_enlarge : screenId6.screen_object.vkb_btn_enlarge.value
                property variant vkb_btn_text_enlarge : screenId6.screen_object.vkb_btn_text_enlarge.value
                property variant vkb_btn_text_h : screenId6.screen_object.vkb_btn_text_h.value
                property variant vkb_btn_text_n : screenId6.screen_object.vkb_btn_text_n.value
                property variant vkb_btn_text_p : screenId6.screen_object.vkb_btn_text_p.value
                property variant vkb_btn_text_s : screenId6.screen_object.vkb_btn_text_s.value
                property variant vkb_close_h : screenId6.screen_object.vkb_close_h.value
                property variant vkb_close_n : screenId6.screen_object.vkb_close_n.value
                property variant vkb_close_p : screenId6.screen_object.vkb_close_p.value
                property variant vkb_panel : screenId6.screen_object.vkb_panel.value
            }
        }
    }
    QtObject {
        id: language_object
        property variant language_value
        property QtObject connecting_gate : QtObject {
            id: connecting_gateId1
            property variant connecting_gate_object : language_object.language_value.connecting_gate
            property variant arrival_information : connecting_gateId1.connecting_gate_object.arrival_information.value
            property variant arrival_terminal : connecting_gateId1.connecting_gate_object.arrival_terminal.value
            property variant arrival_time : connecting_gateId1.connecting_gate_object.arrival_time.value
            property variant back : connecting_gateId1.connecting_gate_object.back.value
            property variant baggage_carousel : connecting_gateId1.connecting_gate_object.baggage_carousel.value
            property variant baggage_info : connecting_gateId1.connecting_gate_object.baggage_info.value
            property variant cg_info_available : connecting_gateId1.connecting_gate_object.cg_info_available.value
            property variant cg_info_notavailable : connecting_gateId1.connecting_gate_object.cg_info_notavailable.value
            property variant cg_info_update : connecting_gateId1.connecting_gate_object.cg_info_update.value
            property variant codeshare_flight : connecting_gateId1.connecting_gate_object.codeshare_flight.value
            property variant connecting_flights : connecting_gateId1.connecting_gate_object.connecting_flights.value
            property variant current_flight_txt : connecting_gateId1.connecting_gate_object.current_flight_txt.value
            property variant departure_gate : connecting_gateId1.connecting_gate_object.departure_gate.value
            property variant departure_time : connecting_gateId1.connecting_gate_object.departure_time.value
            property variant destination : connecting_gateId1.connecting_gate_object.destination.value
            property variant flight : connecting_gateId1.connecting_gate_object.flight.value
            property variant flight_number : connecting_gateId1.connecting_gate_object.flight_number.value
            property variant header_destination : connecting_gateId1.connecting_gate_object.header_destination.value
            property variant header_gate : connecting_gateId1.connecting_gate_object.header_gate.value
            property variant schedule_dept_date : connecting_gateId1.connecting_gate_object.schedule_dept_date.value
            property variant schedule_dept_time : connecting_gateId1.connecting_gate_object.schedule_dept_time.value
            property variant status : connecting_gateId1.connecting_gate_object.status.value
        }
        property QtObject games : QtObject {
            id: gamesId2
            property variant games_object : language_object.language_value.games
            property variant gameaudiomsg : gamesId2.games_object.gameaudiomsg.value
            property variant gamebtntxt : gamesId2.games_object.gamebtntxt.value
            property variant loading : gamesId2.games_object.loading.value
            property variant musicbtntxt : gamesId2.games_object.musicbtntxt.value
            property variant play : gamesId2.games_object.play.value
        }
        property QtObject global_elements : QtObject {
            id: global_elementsId1
            property variant global_elements_object : language_object.language_value.global_elements
            property variant airport_map : global_elementsId1.global_elements_object.airport_map.value
            property variant altitude : global_elementsId1.global_elements_object.altitude.value
            property variant button_kids_text : global_elementsId1.global_elements_object.button_kids_text.value
            property variant button_mainmenu_text : global_elementsId1.global_elements_object.button_mainmenu_text.value
            property variant button_settings_text : global_elementsId1.global_elements_object.button_settings_text.value
            property variant cancel : global_elementsId1.global_elements_object.cancel.value
            property variant comingsoon : global_elementsId1.global_elements_object.comingsoon.value
            property variant cont : global_elementsId1.global_elements_object.cont.value
            property variant flight_connection : global_elementsId1.global_elements_object.flight_connection.value
            property variant flight_duration : global_elementsId1.global_elements_object.flight_duration.value
            property variant flight_elapse_time : global_elementsId1.global_elements_object.flight_elapse_time.value
            property variant flight_estimate_time : global_elementsId1.global_elements_object.flight_estimate_time.value
            property variant flight_local_time_dest : global_elementsId1.global_elements_object.flight_local_time_dest.value
            property variant flight_local_time_org : global_elementsId1.global_elements_object.flight_local_time_org.value
            property variant flight_remain_time : global_elementsId1.global_elements_object.flight_remain_time.value
            property variant ground_speed : global_elementsId1.global_elements_object.ground_speed.value
            property variant in_seat_monitor : global_elementsId1.global_elements_object.in_seat_monitor.value
            property variant no : global_elementsId1.global_elements_object.no.value
            property variant ok : global_elementsId1.global_elements_object.ok.value
            property variant outside_temp : global_elementsId1.global_elements_object.outside_temp.value
            property variant pa : global_elementsId1.global_elements_object.pa.value
            property variant power_down_txt : global_elementsId1.global_elements_object.power_down_txt.value
            property variant selectionUnavilable : global_elementsId1.global_elements_object.selectionUnavilable.value
            property variant view_map : global_elementsId1.global_elements_object.view_map.value
            property variant yes : global_elementsId1.global_elements_object.yes.value
        }
        property QtObject karma : QtObject {
            id: karmaId1
            property variant karma_object : language_object.language_value.karma
            property variant change_language : karmaId1.karma_object.change_language.value
            property variant handset_settings : karmaId1.karma_object.handset_settings.value
            property variant help_cat : karmaId1.karma_object.help_cat.value
            property variant help_desc : karmaId1.karma_object.help_desc.value
            property variant language_settings : karmaId1.karma_object.language_settings.value
            property variant launch_app : karmaId1.karma_object.launch_app.value
            property variant launch_handset : karmaId1.karma_object.launch_handset.value
            property variant launch_map : karmaId1.karma_object.launch_map.value
            property variant launch_seat : karmaId1.karma_object.launch_seat.value
            property variant monitor_settings : karmaId1.karma_object.monitor_settings.value
            property variant save : karmaId1.karma_object.save.value
            property variant settings_bright : karmaId1.karma_object.settings_bright.value
            property variant settings_screen_off : karmaId1.karma_object.settings_screen_off.value
            property variant settings_title : karmaId1.karma_object.settings_title.value
            property variant settings_vol : karmaId1.karma_object.settings_vol.value
            property variant stop_btn : karmaId1.karma_object.stop_btn.value
        }
        property QtObject main_menu : QtObject {
            id: main_menuId1
            property variant main_menu_object : language_object.language_value.main_menu
            property variant mainmenu_description : main_menuId1.main_menu_object.mainmenu_description.value
            property variant mainmenu_kids_description : main_menuId1.main_menu_object.mainmenu_kids_description.value
            property variant mainmenu_kids_title : main_menuId1.main_menu_object.mainmenu_kids_title.value
            property variant mainmenu_title : main_menuId1.main_menu_object.mainmenu_title.value
        }
        property QtObject movies : QtObject {
            id: moviesId2
            property variant movies_object : language_object.language_value.movies
            property variant accept : moviesId2.movies_object.accept.value
            property variant cancel : moviesId2.movies_object.cancel.value
            property variant directed_by : moviesId2.movies_object.directed_by.value
            property variant do_you_want_to_exit : moviesId2.movies_object.do_you_want_to_exit.value
            property variant fullscreen : moviesId2.movies_object.fullscreen.value
            property variant none : moviesId2.movies_object.none.value
            property variant play : moviesId2.movies_object.play.value
            property variant play_movie_in : moviesId2.movies_object.play_movie_in.value
            property variant produced_by : moviesId2.movies_object.produced_by.value
            property variant restart : moviesId2.movies_object.restart.value
            property variant resume : moviesId2.movies_object.resume.value
            property variant soundtrack : moviesId2.movies_object.soundtrack.value
            property variant starring : moviesId2.movies_object.starring.value
            property variant subtitle_cc : moviesId2.movies_object.subtitle_cc.value
            property variant trailer : moviesId2.movies_object.trailer.value
            property variant vod_change_screensize : moviesId2.movies_object.vod_change_screensize.value
            property variant vod_change_snd : moviesId2.movies_object.vod_change_snd.value
            property variant vod_change_subcc : moviesId2.movies_object.vod_change_subcc.value
        }
        property QtObject music : QtObject {
            id: musicId2
            property variant music_object : language_object.language_value.music
            property variant add_all_tracks_to : musicId2.music_object.add_all_tracks_to.value
            property variant add_tracks_to : musicId2.music_object.add_tracks_to.value
            property variant button_add_to_playlist : musicId2.music_object.button_add_to_playlist.value
            property variant clear_all_playlist : musicId2.music_object.clear_all_playlist.value
            property variant delete_track_title : musicId2.music_object.delete_track_title.value
            property variant go_to_albums : musicId2.music_object.go_to_albums.value
            property variant now_playing : musicId2.music_object.now_playing.value
            property variant play_all_songs : musicId2.music_object.play_all_songs.value
            property variant playlist1 : musicId2.music_object.playlist1.value
            property variant playlist1_empty_title : musicId2.music_object.playlist1_empty_title.value
            property variant playlist1_is_full : musicId2.music_object.playlist1_is_full.value
            property variant playlist2 : musicId2.music_object.playlist2.value
            property variant playlist2_empty_title : musicId2.music_object.playlist2_empty_title.value
            property variant playlist2_is_full : musicId2.music_object.playlist2_is_full.value
            property variant playlist3 : musicId2.music_object.playlist3.value
            property variant playlist3_empty_title : musicId2.music_object.playlist3_empty_title.value
            property variant playlist3_is_full : musicId2.music_object.playlist3_is_full.value
            property variant playlist_empty_desc : musicId2.music_object.playlist_empty_desc.value
            property variant playlist_full_desc : musicId2.music_object.playlist_full_desc.value
            property variant remove_from_playlist : musicId2.music_object.remove_from_playlist.value
            property variant remove_songs : musicId2.music_object.remove_songs.value
            property variant text_album : musicId2.music_object.text_album.value
            property variant text_albums : musicId2.music_object.text_albums.value
        }
        property QtObject seatchat : QtObject {
            id: seatchatId2
            property variant seatchat_object : language_object.language_value.seatchat
            property variant block : seatchatId2.seatchat_object.block.value
            property variant block_completed : seatchatId2.seatchat_object.block_completed.value
            property variant block_confirmation : seatchatId2.seatchat_object.block_confirmation.value
            property variant block_list_desc : seatchatId2.seatchat_object.block_list_desc.value
            property variant block_list_title : seatchatId2.seatchat_object.block_list_title.value
            property variant cancel : seatchatId2.seatchat_object.cancel.value
            property variant chatsession_exit_text : seatchatId2.seatchat_object.chatsession_exit_text.value
            property variant chatting_with : seatchatId2.seatchat_object.chatting_with.value
            property variant continue_text : seatchatId2.seatchat_object.continue_text.value
            property variant decline : seatchatId2.seatchat_object.decline.value
            property variant disable : seatchatId2.seatchat_object.disable.value
            property variant empty_message : seatchatId2.seatchat_object.empty_message.value
            property variant enter_seat_subtitle : seatchatId2.seatchat_object.enter_seat_subtitle.value
            property variant enter_seat_title : seatchatId2.seatchat_object.enter_seat_title.value
            property variant invalid_seat_message : seatchatId2.seatchat_object.invalid_seat_message.value
            property variant invalid_session : seatchatId2.seatchat_object.invalid_session.value
            property variant invitation_message : seatchatId2.seatchat_object.invitation_message.value
            property variant invitation_sent : seatchatId2.seatchat_object.invitation_sent.value
            property variant leave_chat_confirmation : seatchatId2.seatchat_object.leave_chat_confirmation.value
            property variant leave_chat_room : seatchatId2.seatchat_object.leave_chat_room.value
            property variant me : seatchatId2.seatchat_object.me.value
            property variant multiple_invitation_sent : seatchatId2.seatchat_object.multiple_invitation_sent.value
            property variant multiple_seat_invitation : seatchatId2.seatchat_object.multiple_seat_invitation.value
            property variant new_chat : seatchatId2.seatchat_object.new_chat.value
            property variant nickname_message : seatchatId2.seatchat_object.nickname_message.value
            property variant nickname_title : seatchatId2.seatchat_object.nickname_title.value
            property variant notification_joined_txt : seatchatId2.seatchat_object.notification_joined_txt.value
            property variant notification_left_txt : seatchatId2.seatchat_object.notification_left_txt.value
            property variant privacy_desc : seatchatId2.seatchat_object.privacy_desc.value
            property variant privacy_title : seatchatId2.seatchat_object.privacy_title.value
            property variant said : seatchatId2.seatchat_object.said.value
            property variant seatchat_accept : seatchatId2.seatchat_object.seatchat_accept.value
            property variant seatchat_loading : seatchatId2.seatchat_object.seatchat_loading.value
            property variant seatchat_reject : seatchatId2.seatchat_object.seatchat_reject.value
            property variant send : seatchatId2.seatchat_object.send.value
            property variant send_invitation : seatchatId2.seatchat_object.send_invitation.value
            property variant session_instructions : seatchatId2.seatchat_object.session_instructions.value
            property variant session_limit_message : seatchatId2.seatchat_object.session_limit_message.value
            property variant session_welcome : seatchatId2.seatchat_object.session_welcome.value
            property variant sessions_title : seatchatId2.seatchat_object.sessions_title.value
            property variant terms_desc : seatchatId2.seatchat_object.terms_desc.value
            property variant terms_title : seatchatId2.seatchat_object.terms_title.value
            property variant unblock : seatchatId2.seatchat_object.unblock.value
            property variant unblock_list_desc : seatchatId2.seatchat_object.unblock_list_desc.value
            property variant unblock_list_title : seatchatId2.seatchat_object.unblock_list_title.value
            property variant unblock_seats : seatchatId2.seatchat_object.unblock_seats.value
        }
        property QtObject settings : QtObject {
            id: settingsId3
            property variant settings_object : language_object.language_value.settings
            property variant call_crew : settingsId3.settings_object.call_crew.value
            property variant change_language : settingsId3.settings_object.change_language.value
            property variant chat_disable_txt : settingsId3.settings_object.chat_disable_txt.value
            property variant chat_disablemsg_txt : settingsId3.settings_object.chat_disablemsg_txt.value
            property variant chat_enable_txt : settingsId3.settings_object.chat_enable_txt.value
            property variant chat_enablemsg_txt : settingsId3.settings_object.chat_enablemsg_txt.value
            property variant chattitle_txt : settingsId3.settings_object.chattitle_txt.value
            property variant reading_light : settingsId3.settings_object.reading_light.value
            property variant screen_off : settingsId3.settings_object.screen_off.value
            property variant windowdimmable_button : settingsId3.settings_object.windowdimmable_button.value
        }
        property QtObject survey : QtObject {
            id: surveyId2
            property variant survey_object : language_object.language_value.survey
            property variant done : surveyId2.survey_object.done.value
            property variant enter_your_comments : surveyId2.survey_object.enter_your_comments.value
            property variant next : surveyId2.survey_object.next.value
            property variant previous : surveyId2.survey_object.previous.value
            property variant remaining_text : surveyId2.survey_object.remaining_text.value
            property variant survey_triggered : surveyId2.survey_object.survey_triggered.value
            property variant take_another_survey : surveyId2.survey_object.take_another_survey.value
            property variant take_survey : surveyId2.survey_object.take_survey.value
            property variant thank_you : surveyId2.survey_object.thank_you.value
            property variant thank_you_desc : surveyId2.survey_object.thank_you_desc.value
        }
        property QtObject tv : QtObject {
            id: tvId2
            property variant tv_object : language_object.language_value.tv
            property variant min : tvId2.tv_object.min.value
            property variant next_episode : tvId2.tv_object.next_episode.value
            property variant play_video_in : tvId2.tv_object.play_video_in.value
            property variant previous_episode : tvId2.tv_object.previous_episode.value
            property variant tv_episode : tvId2.tv_object.tv_episode.value
            property variant tv_episodes : tvId2.tv_object.tv_episodes.value
            property variant tv_play : tvId2.tv_object.tv_play.value
            property variant tv_play_all : tvId2.tv_object.tv_play_all.value
            property variant what_do_you_wish : tvId2.tv_object.what_do_you_wish.value
        }
        property QtObject window_dimming : QtObject {
            id: window_dimmingId1
            property variant window_dimming_object : language_object.language_value.window_dimming
            property variant windowdimming_description : window_dimmingId1.window_dimming_object.windowdimming_description.value
            property variant windowdimming_title : window_dimmingId1.window_dimming_object.windowdimming_title.value
        }
    }
}
