// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import ImageMask 1.0
SmartLoader{
    id:listingScreen
    property bool isAnimFirstTime: true
    property bool hideCenter:false;
    property bool isSynopsis: false
    property bool isModelUpdated: false
    property bool isMousePressed: false
    //property bool showChatMainScreen: (synopsisLoader.synopsisItem.sourceComponent == mainSeatChatWindowPage||synopsisLoader.synopsisItem.sourceComponent == seatChatLoaderItem||synopsisLoader.synopsisItem.sourceComponent == seatChatTermsPage||synopsisLoader.synopsisItem.sourceComponent == nickNamePage)
    property int selIndex:-1;
    property int selprevIndex:0;
    property int selnextIndex:0;
    property int listingsingleimgwidth:qsTranslate('','listing_games_list_itemw')
    property variant mediaList:listingobj;
    property variant subCatobj:widgetList.getMainMenuRef();
    property variant currentTemplateNode:"games"
    property string compOffset: qsTranslate('','listing_games_list_hgap')/2
    property string comptext_h: configTool.config[currentTemplateNode].listing.text_color_h
    property string comptext_n: configTool.config[currentTemplateNode].listing.text_color_n
    property string comptext_p: configTool.config[currentTemplateNode].listing.text_color_p
    property string compScale_n:qsTranslate('','listing_games_list_scale')
    property string compScale_h:qsTranslate('','listing_games_list_scale_h')
    property string compImage_h:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1])].listing.poster_holder_h
    property string compImage_n:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1])].listing.poster_holder_n
    property string compImage_p:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1])].listing.poster_holder_p
    property string compImageMask:viewController.settings.assetImagePath+qsTranslate('','listing_games_list_source')
    property string compText_w:qsTranslate('','listing_games_list_text_w')
    property string compText_w2:qsTranslate('','listing_games_list_text_w2')
    property string compText_h:qsTranslate('','listing_games_list_text_h1')
    property string compTextHighlight_h:qsTranslate('','listing_games_list_text_h')
    property string compImageTM_h:qsTranslate('','listing_games_list_text_tm_h')
    property string compImageTM_n:qsTranslate('','listing_games_list_text_tm_n')
    property string compTextFonsize_h:qsTranslate('','listing_games_list_text_fontsize_h')
    property string compTextFonsize_n:qsTranslate('','listing_games_list_text_fontsize_n')
    property string compTextOpacity:qsTranslate('','listing_games_list_text_opacity')
    signal makeSynopsisOpenForceFully
    /**************************************************************************************************************************/
    Connections{
        target:visible?viewHelper:null
        onMainMenuIndexChanged:{
            isSynopsis=false
            //getSubCategoryMenu()
            revsynopsisAnim.start()
        }
    }
    /**************************************************************************************************************************/
    function load(){
        push(compPostersBG,"posterobj")
        push(compListing,"listingobj")
        push(compSynopsisLoader,"synopsisLoader")
    }
    function init(){
        core.debug("GamesListing.qml | Init ")
        if(current.template_id=="games"){
            viewHelper.setHelpTemplate("games_listing");
        }else{
            viewHelper.setHelpTemplate("discover_listing");
        }
        widgetList.showHeader(true);
        widgetList.showMainMenu(true);
        getSubCategoryMenu()
    }

    function getSubCategoryMenu(){
        console.log("getSubCategoryMenu >>>>>>>>>>>>>>>>> | viewHelper.shortcutCalled = "+viewHelper.shortcutCalled)
        if(viewHelper.shortcutCalled)
            dataController.getNextMenu([viewHelper.pcid,viewHelper.ptid ],subCategoryReady);
        else
            dataController.getNextMenu([widgetList.getCidTidMainMenu()[0] ,widgetList.getCidTidMainMenu()[1] ],subCategoryReady);
    }
    function reload(){
        if(!isModelUpdated){
            closeLoader();
        }
        init();
    }
    function clearOnExit(){
        core.debug("GameListing.qml | clearOnExit ")
        listingobj.compVpathListing.model=viewHelper.blankModel;
        listingobj.compVpathListing.currentIndex=0;
        isSynopsis=false;
        //        viewHelper.fromInvitePopup=false
        if(synopsisLoader.synopsisItem.sourceComponent!=blankComp){
            synopsisLoader.synopsisItem.item.clearOnExit();
        }
        synopsisLoader.synopsisItem.sourceComponent=blankComp
        widgetList.showMealMenu(false);
    }
    function subCategoryReady(dmodel){
        listingobj.compVpathListing.setModel(viewHelper.blankModel)
        listingReady(dmodel)
    }
    function enterReleasedforListing(index){
        listingobj.compVpathListing.currentIndex=index
        selIndex=index;
        console.log("GameListing.qml | enterReleasedforListing(index) | viewHelper.shortcutCalled = "+viewHelper.shortcutCalled)
        loadSynopsis()
    }
    function loadSynopsis(){
        if(current.template_id=="games"){
            viewHelper.setHelpTemplate("games_synop");
        }else{
            viewHelper.setHelpTemplate("discover_synop");
        }
        if(!isMousePressed) isSynopsis=true;
        else if(isMousePressed)isMousePressed=false
        if(getMediaInfo("category_attr_template_id")=="seatchat")synopsisLoader.synopsisItem.sourceComponent=seatChatLoaderItem
        else if(getMediaInfo("category_attr_template_id")=="survey")synopsisLoader.synopsisItem.sourceComponent=surveySynopsisItem
        else  if(current.template_id=="discover")synopsisLoader.synopsisItem.sourceComponent=discoverSynopsisItem
        else synopsisLoader.synopsisItem.sourceComponent=gameSynopsisItem
        synopsisLoader.synopsisItem.item.forceActiveFocus();
        if(viewHelper.shortcutCalled){
            if(getMediaInfo("category_attr_template_id")=="survey"){
//                synopsisLoader.synopsisItem.item.launchSurveyCategory(true)
            }else{
                synopsisLoader.synopsisItem.item.continueAction()
            }
            viewHelper.shortcutCalled=false
        }
    }
    function incrementSelindex(){
        if(selIndex==listingobj.compVpathListing.count-1)selIndex=0
        else  selIndex++
    }
    function decrementSelindex(){
        if(selIndex==0)selIndex=listingobj.compVpathListing.count-1
        else  selIndex--
    }
    function getListingMenu(){
    }
    function listingReady(dModel){
        currentTemplateNode=current.template_id;
        listingobj.compVpathListing.setModel(viewHelper.blankModel);
        if(dModel.count==0){
            if(viewHelper.isCurtainFirstTime){
                widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
                viewHelper.isCurtainFirstTime=false
            }
            viewController.showScreenPopup(7);
            return;
        }
        listingobj.compVpathListing.anchors.topMargin=qsTranslate('','listing_games_list_tm')
        core.debug(" dModel.count : "+dModel.count)
        listingobj.compVpathListing.delegate = maskingIconText;
        listingobj.compVpathListing.setModel(dModel)
        core.debug("listingReady >>>>>>>>>>>>>>>>>>>>>>> isSynopsis| viewHelper.keepSynopsisOpen | else = "+viewHelper.keepSynopsisOpen)
        if((viewHelper.keepSynopsisOpen && synopsisLoader.synopsisItem!=blankComp)){
            core.debug("listingReady >>>>>>>>>>>>>>>>>>>>>>> isSynopsis = "+isSynopsis)
            listingobj.compVpathListing.synopIndex=viewHelper.mediaListing
            selIndex=viewHelper.mediaListing
            isSynopsis=true
            enterReleasedforListing(viewHelper.mediaListing)
            //            if(!viewHelper.shortcutJumpSeatChat){
            //                if(synopsisLoader.synopsisItem.item.handleExternally)  synopsisLoader.synopsisItem.item.handleExternally()
            //            }
            //            viewHelper.fromInvitePopup=true
            viewHelper.shortcutJumpSeatChat=false
            if(viewHelper.shortcutJumpSeatChat)return;
        }
        else{
            isSynopsis=false
            core.debug("listingReady >>>>>>>>>>>>>>>>>>>>>>> isSynopsis | else = "+isSynopsis)
            listingobj.compVpathListing.currentIndex=0
        }
        viewHelper.setMediaListingModel(dModel)
        if(viewHelper.isCurtainFirstTime){
            widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
            viewHelper.isCurtainFirstTime=false
        }
        if(viewHelper.keepSynopsisOpen)viewHelper.keepSynopsisOpen=false
        else   if(!isModelUpdated && dModel.count>0) listingobj.compVpathListing.forceActiveFocus()
        if(viewHelper.shortcutCalled){
            isSynopsis=true;
            var index=core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id",viewHelper.tid)
            core.debug("listingReady >>>>>>>>>>>>>>>>>>>>>>> | viewHelper.fromInvitePopup | index "+index)
            selIndex=index
            synopsisAnim.restart()
            //            viewHelper.shortcutCalled=false
        }
        isModelUpdated=false
    }
    function focusFromMainMenu(){
        if(isSynopsis){
            synopsisLoader.synopsisItem.item.forceActiveFocus();
        }else{
            listingobj.forceActiveFocus();
        }
    }
    function focusFromFooter(){
        if(listingobj.compVpathListing.count>0)
            listingobj.compVpathListing.forceActiveFocus()
        else
            widgetList.focustTomainMenu()
    }
    function getMediaInfo(tag){
        core.debug("Get media info to be called when album is open "+current.template_id)
        return mediaList.getMediaInfo(tag)
    }
    function getCidTidForListing(){
        var param=[]
        param[0]=listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"cid")
        param[1]=listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"media_attr_template_id")
        return param;
    }
    function getCidTidForListingKarma(){
        var param=[]
        param[0]=listingobj.compVpathListing.model.getValue(listingobj.compVpathListing.currentIndex,"category_attr_template_id")
        console.log("getCidTidForListingKarma >>>>>>>>>>>>> param = "+param[0])
        return param;
    }

    function closeLoader(){
        if(current.template_id=="games"){
            viewHelper.setHelpTemplate("games_listing");
        }else{
            viewHelper.setHelpTemplate("discover_listing");
        }
        if(!isModelUpdated) isSynopsis=false;
        if(synopsisLoader.sourceComponent!=blankComp){
            synopsisLoader.item.clearOnExit();
        }
        synopsisLoader.sourceComponent = blankComp;
        if(!isModelUpdated && listingobj.compVpathListing.count>0)listingobj.compVpathListing.forceActiveFocus();
    }
    function backBtnPressed(){
        if(isSynopsis)synopsisLoader.synopsisItem.item.backBtnPressed();
        else {
            viewHelper.homeAction()
            synopsisLoader.synopsisItem.sourceComponent=blankComp
            viewHelper.keepSynopsisOpen=false
        }
    }
    function languageUpdate(){
        isModelUpdated=false;
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel],[listingUpdate]);
    }
    function listingUpdate(){
        if(listingobj.visible){
            dataController.refreshModelsByIntLanguage([listingobj.compVpathListing.model],[updateSynopsis]);
        }else{
            updateSynopsis();
        }
    }
    function updateSynopsis(){
        if(isSynopsis){
            if(synopsisobj.item.langUpdate){
                synopsisobj.item.langUpdate();
            }
        }
    }
    /**************************************************************************************************************************/
    property QtObject posterobj
    Component{
        id:compPostersBG
        Item{
            anchors.fill: parent
            property alias staticBG: staticBG
            Image{
                id:listingBG
                source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
            }
            Image{
                id:staticBG
                anchors.bottom:parent.bottom
                source:(current.template_id=="discover")?viewHelper.configToolImagePath+configTool.config.discover.listing.background:viewHelper.configToolImagePath+configTool.config.games.listing.background
            }
        }
    }

    property QtObject listingobj
    Component{
        id:compListing
        MediaList{
            upFocus:widgetList.getMainMenuRef();
        }
    }


    property QtObject synopsisLoader
    Component{
        id:compSynopsisLoader
        Loader{
            id:synopsisItem
            anchors.fill: parent
            property alias synopsisItem: synopsisItem
            sourceComponent: blankComp
            onStatusChanged: {
                if(synopsisItem.sourceComponent==blankComp)return;
                if (synopsisItem.status == Loader.Ready){
                    synopsisItem.item.init()
                }
            }
        }
    }
    Component{
        id:gameSynopsisItem
        GameSynopsis{
        }
    }
    Component{
        id:surveySynopsisItem
        SurveySynopsis{
        }
    }
    Component{
        id:seatChatLoaderItem
        SeatChatLoaderItem{
        }
    }
    Component{
        id:discoverSynopsisItem
        DiscoverSynopsis{
        }
    }


    Component{
        id:maskingIconText
        Item{
            id:thisDelRect
            property int offset:(thisDelRect.x<PathView.view.children[0].x)?-compOffset:compOffset
            property string textColor: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?comptext_h:(thisDel.isPressed?comptext_p:comptext_n))
            width:listingsingleimgwidth
            height:listingobj.compVpathListing.height
            visible: listingobj.compVpathListing.currentIndex==index  && isSynopsis?false:true
            MaskedImage{
                id:maskingImage
                anchors.centerIn:   thisDel
                maskSource:compImageMask
                source:viewHelper.cMediaImagePath+poster
                scale: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compScale_h:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?compScale_h:compScale_n
            }
            SimpleButton{
                id:thisDel
                isHighlight:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?true:false
                highlightimg:compImage_h
                normImg: compImage_n
                pressedImg: compImage_p
                isFocusRequired:!isSynopsis
                anchors.centerIn: thisDelRect
                anchors.horizontalCenterOffset: (listingobj.compVpathListing.currentIndex!=index &&  listingobj.compVpathListing.activeFocus)?offset:0
                scale: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compScale_h:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?compScale_h:compScale_n
                Behavior on opacity{
                    NumberAnimation{duration:500}
                }
                onEnteredReleased: {
                    //    listingobj.compVpathListing.currentIndex=index
                    listingobj,compVpathListing.select(index);
                }
                onIsPressedChanged: if(isPressed)isMousePressed=true
                onAdjustfocus:synopsisLoader.synopsisItem.item.isSliderNavChanged=true
            }
            ViewText {
                width: listingobj.compVpathListing.currentIndex==index?compText_w2:compText_w
                height:listingobj.compVpathListing.currentIndex==index?compTextHighlight_h:compText_h
                anchors.bottom: parent.bottom
                //anchors.topMargin:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compImageTM_h:compImageTM_n
                anchors.horizontalCenter: maskingImage.horizontalCenter
                varText: [title,textColor,listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compTextFonsize_h:compTextFonsize_n]
                opacity: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus)?1:compTextOpacity
                wrapMode: Text.Wrap
                maximumLineCount: 2;
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        thisDel.isPressed=true
                    }
                    onReleased: {
                        thisDel.isPressed=false
                        isMousePressed=true
                        thisDel.enteredReleased()
                    }
                }
            }
        }
    }
    Component{id:blankComp;Item{}}
    SequentialAnimation{
        id:revsynopsisAnim
        NumberAnimation { targets: [listingobj.compVpathListing]; properties: "opacity";from:1;to:0; duration: 10 }
        PauseAnimation { duration: 800 }
        NumberAnimation { targets: [listingobj.compVpathListing]; properties: "opacity";from:0;to:1; duration: 250 }
    }
    SequentialAnimation{
        id:synopsisAnim
        ScriptAction{
            script:{
                if(listingobj.compVpathListing.model.getValue(selIndex,"category_attr_template_id")=="airline_exp"){
                    listingobj.compVpathListing.currentIndex=selIndex;
                    synopsisAnim.stop();closeLoader()
                    return;
                }

                if(selIndex%2==1){
                    selprevIndex=selIndex;
                }else{
                    selnextIndex=selIndex;
                }
            }
        }
        NumberAnimation{targets: [!isSynopsis?listingobj.compVpathListing:synopsisLoader,synopsisLoader];properties: "opacity";from:1;to:0;duration:250}
        ScriptAction{
            script:{
                listingobj.compVpathListing.synopIndex=selIndex;
                listingobj.compVpathListing.currentIndex=selIndex
                enterReleasedforListing(selIndex);
                if(viewHelper.shortcutCalled)viewHelper.surveyFromKarma()
            }
        }
        PauseAnimation { duration: 100 }
        ScriptAction{
            script:{
                isSynopsis=true
            }
        }
        NumberAnimation{targets: [!isSynopsis?listingobj.compVpathListing:synopsisLoader,synopsisLoader];properties: "opacity";from:0;to:1;duration: 250}
        ScriptAction{
            script:  isMousePressed=false
        }
    }
}
