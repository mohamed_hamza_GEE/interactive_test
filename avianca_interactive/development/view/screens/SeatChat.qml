// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import ImageMask 1.0
SmartLoader{
    id:seatchatScreen
    property bool isSynopsis: false
    property bool isModelUpdated: false
    //    property bool showChatMainScreen: (synopsisLoader.sourceComponent == mainSeatChatWindowPage||synopsisLoader.sourceComponent == seatChatTermsPage||synopsisLoader.sourceComponent == nickNamePage)
    signal makeSynopsisOpenForceFully
    /**************************************************************************************************************************/

    function load(){
        push(compSynopsisLoader,"synopsisLoader")
    }
    Connections{
        target:viewHelper
        onAcceptInviataionfromPopup:{
            //init()
        }
    }

    function init(){
        core.info(" View/SeatChat.qml | init() ")
        if(viewHelper.seatchat_enabled){
            core.info(" View/SeatChat.qml | init() | viewHelper.seatchat_enabled = "+viewHelper.seatchat_enabled)
            if(!pif.seatChat.getAppActiveState())pif.seatChat.setAppActive();
            if(viewHelper.getSeatChatTerms()==false){  // first time
                core.debug(" terms and condition to be launched | nickNamePage= "+viewHelper.getSeatChatTerms())
                synopsisLoader.sourceComponent=seatChatTermsPage
            }else{
                synopsisLoader.sourceComponent=nickNamePage
                core.debug(" terms and condition to be launched | seatChatTermsPage= "+viewHelper.getSeatChatTerms())
            }
        }else{
            core.debug("viewHelper.seatchat_enabled = "+viewHelper.seatchat_enabled)

        }
    }
    function focusFromMainMenu(){
        core.debug("focusFromMainMenu = "+forceActiveFocus)
        synopsisLoader.synopsisItem.item.forceActiveFocus();
    }

    function clearOnExit(){
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendApiData("showKeyboard",{showKeyboard:false});
            pif.sharedModelServerInterface.sendApiData("userDefinedhideChatFromSeat",{});
        }
        isSynopsis=false;
        viewHelper.fromInvitePopup=false
        if(synopsisLoader.synopsisItem.sourceComponent!=blankComp){
            synopsisLoader.synopsisItem.item.clearOnExit();
        }
        synopsisLoader.synopsisItem.sourceComponent=blankComp
        viewHelper.specialCaseForSeatchatJump=false
    }

    function closeLoader(){
        if(!isModelUpdated) isSynopsis=false;
        if(synopsisLoader.sourceComponent!=blankComp){
            synopsisLoader.item.clearOnExit();
        }
        synopsisLoader.sourceComponent = blankComp;

    }
    function backBtnPressed(){
        viewHelper.keepSynopsisOpen=true;
        //        if(!viewHelper.fromInvitePopup){viewController.loadPrev();}
        /*  else*/
        console.log("backBtnPressed >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>from seatchat screen ")
        if(viewHelper.specialCaseForSeatchatJump){
            viewHelper.isHomeFromAndroid=true
            viewHelper.homeAction()
        }
        else viewController.loadPrev()
        viewHelper.specialCaseForSeatchatJump=false
        //         viewController.loadNext(viewData.mainMenuModel,widgetList.getMainMenuRef().mainmenuPathView.currentIndex);
    }
    function languageUpdate(){
        isModelUpdated=false;
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel]);
    }
    //    function listingUpdate(){
    //        if(listingobj.visible){
    //            dataController.refreshModelsByIntLanguage([listingobj.compVpathListing.model],[updateSynopsis]);
    //        }else{
    //            updateSynopsis();
    //        }
    //    }

    function updateSynopsis(){
        if(isSynopsis){
            if(synopsisobj.item.langUpdate){
                synopsisobj.item.langUpdate();
            }
        }
    }
    /**************************************************************************************************************************/

    //    function returnSourceComponent(){
    //        console.log("returnSourceComponent   ***********************")
    //        if(synopsisLoader.synopsisItem.sourceComponent!=blankComp){
    //            console.log("returnSourceComponent   ")
    //            return   showChatMainScreen
    //        }
    //    }

    //    function acceptFromPopup(){
    //        console.log("acceptFromPopup == ")
    //        return   synopsisLoader.synopsisItem.item.init()
    //    }

    property QtObject synopsisLoader
    Component{
        id:compSynopsisLoader
        Loader{
            id:synopsisItem
            anchors.fill: parent
            property alias synopsisItem: synopsisItem
            sourceComponent: blankComp
            onStatusChanged: {
                if(synopsisItem.sourceComponent==blankComp)return;
                if (synopsisItem.status == Loader.Ready){
                    synopsisItem.item.init()
                }
            }
        }
    }

    Component{
        id:seatChatTermsPage
        SeatChatTerms{
        }
    }
    Component{
        id:nickNamePage
        NickNamePage{
        }
    }
    Component{
        id:mainSeatChatWindowPage
        MainSeatChatWindow{
        }
    }
    Component{id:blankComp;Item{}}
}
