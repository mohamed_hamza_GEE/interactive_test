// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

SmartLoader{
    id:surveyScreen
    property int surveyId:  viewHelper.surveyId
    property bool isMultiCheckAllowed: false
    property bool isVkbPressed: false
    property bool isMCQFreetxt: false
    property bool isSemanticScaleMoved: false
    property bool isPrevious: false
    property string questionType: "mcqmany";
    property string ansString: ""
    property string freeText: ""
    property variant surquestionModel;
    property variant suranswerModel;
    property variant suroptionModel;
    property variant surveyResponseModel
    property variant columnMCQ:[]
    property variant columnRowMCQ:[]
    property variant columnRowMCQOrdinal: []
    /***************************************************************************************************************************/
    Connections{
        target:(visible)?viewHelper:null
        onVkbData:{
            core.debug("vkbData "+vkbData)
            choosenTemplateObj.surveyChoosenLoader.item.updateText(vkbData)
        }
        onVkbClosed:{
            choosenTemplateObj.surveyChoosenLoader.item.focusOnVkbBtn()
        }
    }

    /**************************************************************************************************************************/
    function load(){
        push(compCommon,"commonObj")
        push(compChoosenTemplate,"choosenTemplateObj")
    }
    function init(){
        viewHelper.setHelpTemplate("survey");
        core.debug("Survey.qml |  Init ")
        var params=new Object()
        params['surveyId'] =surveyId
        params['qId'] =""
        params['aId']=""
        params['answer']=""
        params['questionType']=""
        dataController.survey.advSurvey(params,advSurveyCallBack)
        pif.paxus.startContentLog('Survey')
    }
    function reload(){

    }
    function clearOnExit(){
        isPrevious=false
        dataController.survey.resumeLater()
        var  temp=viewHelper.lastSurveyVisited
        temp[surveyId]=viewHelper.queNumber
        viewHelper.lastSurveyVisited=temp
        viewHelper.isTriggeredSurvey=false
        choosenTemplateObj.sourceComponent=blankCompSurvey;
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":""/*,"showContOnKarma":viewHelper.showContOnKarma*/});
        }
        pif.paxus.stopContentLog()
    }
    function focusFromMainMenu(){commonObj.synopsisCloseBtn.forceActiveFocus()}
    function advSurveyCallBack(status,questionModel,answerModel,optionModel){
        if(viewHelper.queNumber ==dataController.survey.getSurveyModel().getValue(viewHelper.surveyListingIndex,"questionCount")){
            viewHelper.setSurveystatus(surveyId)
        }
        surquestionModel=questionModel
        suranswerModel=answerModel
        suroptionModel=optionModel
        surveyResponseModel = dataController.survey.getResponseModel();
        if(!isPrevious) viewHelper.queNumber =surquestionModel.count>0?(viewHelper.queNumber==-1?viewHelper.queNumber=1:(viewHelper.lastSurveyVisited[surveyId]!=undefined?viewHelper.lastSurveyVisited[surveyId]:1)):-1
        else viewHelper.queNumber-=1
        commonObj.updateSurveyQues(viewHelper.queNumber,surquestionModel.getValue(0,'question'))
        viewHelper.qID = surquestionModel.getValue(0,'QID');
        questionType =surquestionModel.count>0?surquestionModel.getValue(0,'questionType'):"surveyDone"
        if(questionType =="likertScale" && suroptionModel.count<=0)
            questionType="mcq"
        pickUpTemplate(questionType,answerModel);
        viewHelper.aID = ""
        ansString = ""
        var  temp=viewHelper.lastSurveyVisited
        temp[surveyId]=viewHelper.queNumber+1
        viewHelper.lastSurveyVisited=temp
    }
    function performResponseAction(){
        if(questionType=="freeTextResponse"){
            freeText=surveyResponseModel.getValue(0,"answer");
        }else if(questionType=="semanticDifferentialScale"){
            freeText=parseInt(surveyResponseModel.getValue(0,"answer"),10);
        }else if(questionType=="likertScale"){
            for(var i=0;i<surveyResponseModel.count;i++){
                var aidIndex=core.coreHelper.searchEntryInModel(suranswerModel,"AID",surveyResponseModel.getValue(i,"AID"))
                var qidIndex=core.coreHelper.searchEntryInModel(suroptionModel,"QID",surveyResponseModel.getValue(i,"QID"))
                if(aidIndex!=-1 && qidIndex!=-1)  initializeLikertScalerray(qidIndex,aidIndex)
            }
        }
        else   if(questionType=="ordinalResponse"){
            for(var i=0;i<surveyResponseModel.count;i++){
                var aidIndex=core.coreHelper.searchEntryInModel(surveyResponseModel,"AID",surveyResponseModel.getValue(i,"AID"))
                var columnIndex=parseInt(surveyResponseModel.getValue(i,"answer"),10)-1
                if(aidIndex!=-1)  initializeOrdinalScalerray(aidIndex,columnIndex)
            }
        }

        else {
            for(var i=0;i<surveyResponseModel.count;i++){
                var temp=core.coreHelper.searchEntryInModel(suranswerModel,"AID",surveyResponseModel.getValue(i,"AID"))
                if(temp!=-1)
                    initializeModifyColumnArray(temp,"",true)
                if(i==(surveyResponseModel.count-1))freeText=surveyResponseModel.getValue(i,"answer")
            }
        }
    }
    function pickUpTemplate(qType){
        choosenTemplateObj.surveyChoosenLoader.sourceComponent=blankCompSurvey
        isMultiCheckAllowed=false
        isVkbPressed=false
        isMCQFreetxt=false
        columnRowMCQ=[]
        compLikerScale=[]
        columnRowMCQOrdinal=[]
        switch(qType){
        case "mcqMany":
        case "mcqOtherMany":
            isMultiCheckAllowed=true
            initializeModifyColumnArray(0,"start")
            if(qType=="mcqOtherMany")isMCQFreetxt=true
            choosenTemplateObj.surveyChoosenLoader.sourceComponent=mcq
            break;
        case "mcq":
            isMultiCheckAllowed=false
            initializeModifyColumnArray(0,"start")
            if(suroptionModel.count==2) choosenTemplateObj.surveyChoosenLoader.sourceComponent=yesNoType
            else choosenTemplateObj.surveyChoosenLoader.sourceComponent=mcq
            break;
        case "mcqOther":
            isMultiCheckAllowed=false
            initializeModifyColumnArray(0,"start")
            isMCQFreetxt=true
            choosenTemplateObj.surveyChoosenLoader.sourceComponent=mcq
            break
        case "ordinalResponse":
        case "likertScale":
            if(qType=="likertScale")initializeLikertScalerray(-1,-1,"start")
            else initializeOrdinalScalerray(-1,-1,"start")
            choosenTemplateObj.surveyChoosenLoader.sourceComponent=likertScale
            break
        case "freeTextResponse":
            choosenTemplateObj.surveyChoosenLoader.sourceComponent=freetext
            break
        case "semanticDifferentialScale":
            freeText=1
            isSemanticScaleMoved=false

            choosenTemplateObj.surveyChoosenLoader.sourceComponent=semantic

            break
        case  "surveyDone":
            choosenTemplateObj.surveyChoosenLoader.sourceComponent=surveyDone
            break
        }
        if(surveyResponseModel.count >0 )performResponseAction()
    }
    function  initializeModifyColumnArray(index,params,status){
        var tempArray=[]
        for(var i=0;i<=suranswerModel.count;i++){
            if(isMultiCheckAllowed){
                if(params=="start"){
                    tempArray[i]=false
                }else if(index==i){
                    tempArray[index]=status
                }else{
                    tempArray[i]=columnMCQ[i]
                }
            }
            else{
                if(i==index) tempArray[i]=true
                else tempArray[i]=false
            }
        }
        columnMCQ=tempArray
    }

    function initializeLikertScalerray(row,column,params){
        var tempArray=[]
        for(var i=0;i<=suroptionModel.count;i++){
            for(var j=0;j<suranswerModel.count;j++){
                if(params=="start"){
                    if(tempArray[i]==undefined)tempArray[i]=new Array()
                    tempArray[i][j]=false
                }
                else{
                    if(tempArray[i]==undefined)tempArray[i]=new Array()
                    if(i==row && j==column)  tempArray[i][j]=true
                    else  if(i==row)tempArray[i][j]=false
                    else tempArray[i][j]=columnRowMCQ[i][j]
                }
            }
        }
        columnRowMCQ=tempArray
    }
    function initializeOrdinalScalerray(row,column,params){
        var tempArray=[]
        for(var i=0;i<=suranswerModel.count;i++){
            for(var j=0;j<suranswerModel.count;j++){
                if(params=="start"){
                    if(tempArray[i]==undefined)tempArray[i]=new Array()
                    tempArray[i][j]=false
                }
                else{
                    if(tempArray[i]==undefined)tempArray[i]=new Array()
                    if(i==row && j==column)  tempArray[i][j]=true
                    else  if(i==row)tempArray[i][j]=false
                    else if(j==column && columnRowMCQOrdinal[i][j] && i!=row)tempArray[i][j]=false
                    else tempArray[i][j]=columnRowMCQOrdinal[i][j]
                }
            }
        }
        columnRowMCQOrdinal=tempArray
    }
    function prevQuestion(){
        var params = new Object()
        params['surveyId'] =surveyId
        params['qId'] =viewHelper.qID
        isPrevious=true
        dataController.survey.getPreviousQuestion(params,advSurveyCallBack)
    }
    function checkForOtherOption(index){
        return  (index==(suranswerModel.count-1))
    }
    function nextQuestion(){
        var aid = new Array();
        var ans = new Array();
        var queId = new Array();
        isPrevious=false
        freeText= choosenTemplateObj.surveyChoosenLoader.item.textBox? choosenTemplateObj.surveyChoosenLoader.item.textBox.text:freeText
        if(questionType=="likertScale"){
            for(var i=0;i<suroptionModel.count;i++){
                for(var j=0;j<suranswerModel.count;j++){
                    if(columnRowMCQ[i][j]){
                        queId.push(suroptionModel.getValue(i,"QID"))
                        aid.push(suranswerModel.getValue(j,'AID'));
                        ans.push(suranswerModel.getValue(j,'answer'));
                    }
                }
            }
        }
        else   if(questionType=="ordinalResponse"){
            for(var i=0;i<suranswerModel.count;i++){
                for(var j=0;j<suranswerModel.count;j++){
                    if(columnRowMCQOrdinal[i][j]){
                        queId.push(suranswerModel.getValue(i,"QID"))
                        aid.push(suranswerModel.getValue(j,'AID'));
                        ans.push(j+1);
                    }
                }
            }
        }

        else{
            for(var i=0;i<suranswerModel.count;i++){
                if(questionType=="freeTextResponse" || questionType=="semanticDifferentialScale"){
                    aid.push(suranswerModel.getValue(0 ,'AID'));
                    ans.push(escape(freeText))
                    if(!isSemanticScaleMoved && questionType=="semanticDifferentialScale"){
                        aid= new Array();
                        ans= new Array();
                    }
                    if(questionType=="semanticDifferentialScale")break;
                }
                else if(columnMCQ[i]){
                    aid.push(suranswerModel.getValue(i ,'AID'));
                    if(checkForOtherOption(i))ans.push(escape(freeText))
                    else ans.push("")
                }
            }
        }
        viewHelper.aID = aid.join(",")
        ansString = ans.join(",")
        var params=new Object()
        var queID = queId.join(",")
        params['surveyId'] =surveyId
        params['qId'] =viewHelper.qID
        params['aId']=viewHelper.aID
        params['answer']=ansString
        params['questionType']=questionType
        if(questionType == "likertScale")  params['subQuestionId']=queID
        dataController.survey.advSurvey(params,advSurveyCallBack);
        freeText=""
    }
    function backBtnPressed(){
        widgetList.showVkb(false)
        if(dataController.survey.getSurveyModel().count!=1){
            viewHelper.keepSynopsisOpen=true;
        }
        if(!viewHelper.isTriggeredSurvey){ viewHelper.keepSynopsisOpen=true;viewController.loadPrev();}
        else   viewController.loadNext(viewData.mainMenuModel,widgetList.getMainMenuRef().mainmenuPathView.currentIndex);
    }

    function languageUpdate(){
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel]);
    }

    /**************************************************************************************************************************/
    property QtObject commonObj
    Component{
        id:compCommon
        FocusScope{
            anchors.fill: parent
            property alias surveyPanel: surveyPanel
            property alias synopsisCloseBtn: synopsisCloseBtn
            property alias nxtPrevFS: nxtPrevFS
            function updateSurveyQues(index,desc){
                if(index!=-1) questionTitle.text= '<font style="font-size:'+qsTranslate('','survey_question_fontsize_index')+'px"> ' +index+'</font> . '+desc
                else  questionTitle.text=""
            }
            Image{
                id:mainmenuScreenBG
                source:  viewHelper.configToolImagePath+configTool.config.menu.main.bg
            }
            Image{
                id:staticBG
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTool.config.discover.listing.background
            }
            Image{
                id:surveyPanel
                anchors.top: parent.top
                anchors.topMargin:qsTranslate('','survey_panel_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                source:viewHelper.configToolImagePath+configTool.config.discover.survey.surveyquestions_panel
            }

            ViewText{
                id:questionTitle
                anchors.left:surveyPanel.left
                anchors.top: surveyPanel.top
                anchors.leftMargin: qsTranslate('','survey_question_lm')
                anchors.topMargin: qsTranslate('','survey_question_tm')
                width: qsTranslate('','survey_question_w')
                height: qsTranslate('','survey_question_h')
                varText: [text,configTool.config.discover.survey.description1_color, qsTranslate('','survey_question_fontsize')]
                wrapMode: Text.WordWrap
                elide: Text.ElideRight
                textFormat:Text.RichText

            }
            SimpleNavButton{
                id:synopsisCloseBtn
                anchors.right: surveyPanel.right
                anchors.rightMargin: qsTranslate('','survey_close_rm')
                anchors.top:surveyPanel.top
                anchors.topMargin:qsTranslate('','survey_close_tm')
                isHighlight: activeFocus
                normImg:   viewHelper.configToolImagePath+configTool.config.discover.survey.close_btn_n;
                highlightimg:  viewHelper.configToolImagePath+configTool.config.discover.survey.close_btn_h;
                pressedImg: viewHelper.configToolImagePath+configTool.config.discover.survey.close_btn_p;
                nav:["","","",dataController.survey.getSurveyModel().count>1?choosenTemplateObj.surveyChoosenLoader.item:((choosenTemplateObj.surveyChoosenLoader.sourceComponent==surveyDone)?synopsisCloseBtn:choosenTemplateObj.surveyChoosenLoader.item)]
                onNoItemFound: if(dir=="up")widgetList.focustTomainMenu()
                onEnteredReleased:{backBtnPressed()}
            }
            FocusScope{
                anchors.fill: surveyPanel
                id:nxtPrevFS
                property alias prevBtn:prevBtn
                property alias nextBtn:nextBtn
                opacity:(choosenTemplateObj.surveyChoosenLoader.sourceComponent==surveyDone)?0:1;
                SimpleNavBrdrBtn{
                    id:prevBtn
                    property string textColor: prevBtn.activeFocus?configTool.config.discover.survey.btn_text_h:(prevBtn.isPressed?configTool.config.discover.survey.btn_text_p:configTool.config.discover.survey.btn_text_n)
                    visible:  viewHelper.queNumber==1 || viewHelper.queNumber ==-1?false:true
                    onVisibleChanged:if(!visible) nextBtn.focus=true
                    width: qsTranslate('','survey_btns_w')
                    height: qsTranslate('','survey_btns_h')
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: qsTranslate('','survey_btns_gap')
                    anchors.bottomMargin: qsTranslate('','survey_btns_bm')
                    btnTextWidth:parseInt(qsTranslate('',"survey_btns_textw"),10);
                    buttonText: [configTool.language.survey.previous,prevBtn.textColor,qsTranslate('','survey_btns_fontsize')]
                    isHighlight: activeFocus
                    normalImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_n
                    highlightImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_h
                    pressedImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_p
                    normalBorder: [qsTranslate('','survey_btns_margin')]
                    highlightBorder: [qsTranslate('','survey_btns_margin')]
                    nav:["",choosenTemplateObj.surveyChoosenLoader.item,nextBtn]
                    onEnteredReleased:  prevQuestion()
                }
                SimpleNavBrdrBtn{
                    id:nextBtn
                    property string textColor: nextBtn.activeFocus?configTool.config.discover.survey.btn_text_h:(nextBtn.isPressed?configTool.config.discover.survey.btn_text_p:configTool.config.discover.survey.btn_text_n)
                    focus: true
                    visible: surquestionModel.count>0?true:false
                    onVisibleChanged:if(!visible) prevBtn.focus=true
                    width: qsTranslate('','survey_btns_w')
                    height: qsTranslate('','survey_btns_h')
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.rightMargin:  qsTranslate('','survey_btns_gap')
                    anchors.bottomMargin: qsTranslate('','survey_btns_bm')
                    btnTextWidth:parseInt(qsTranslate('',"survey_btns_textw"),10);
                    buttonText: [viewHelper.queNumber ==dataController.survey.getSurveyModel().getValue(viewHelper.surveyListingIndex,"questionCount")?configTool.language.survey.done:configTool.language.survey.next,nextBtn.textColor,qsTranslate('','survey_btns_fontsize')]
                    isHighlight: activeFocus
                    normalImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_n
                    highlightImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_h
                    pressedImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_p
                    normalBorder: [qsTranslate('','survey_btns_margin')]
                    highlightBorder: [qsTranslate('','survey_btns_margin')]
                    nav:[prevBtn.visible?prevBtn:"",choosenTemplateObj.surveyChoosenLoader.item]
                    onEnteredReleased: nextQuestion()
                }
            }
        }
    }
    property QtObject choosenTemplateObj
    Component{
        id:surveyDone
        FocusScope{
            anchors.fill:parent
            ViewText{
                id:thankuTxt
                anchors.top: parent.top
                anchors.topMargin: qsTranslate('','survey_thanku_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                width: qsTranslate('','survey_thanku_w')
                height: qsTranslate('','survey_thanku_h')
                varText: [configTool.language.survey.thank_you,configTool.config.discover.survey.title_color,qsTranslate('','survey_thanku_fontsize')]
                horizontalAlignment: Text.AlignHCenter
            }
            ViewText{
                id:thankuDesc
                anchors.top: thankuTxt.bottom
                anchors.topMargin: qsTranslate('','survey_thanku_desc_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                width: qsTranslate('','survey_thanku_desc_w')
                height: qsTranslate('','survey_thanku_desc_h')
                varText: [configTool.language.survey.thank_you_desc,configTool.config.discover.survey.description1_color,qsTranslate('','survey_thanku_desc_fontsize')]
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
            SimpleNavBrdrBtn{
                id:nxtTmlpate
                focus:true
                visible: dataController.survey.getSurveyModel().count>1?true:false
                property string textColor: nxtTmlpate.activeFocus?configTool.config.discover.survey.btn_text_h:(nxtTmlpate.isPressed?configTool.config.discover.survey.btn_text_p:configTool.config.discover.survey.btn_text_n)
                width: qsTranslate('','survey_thanku_btn_w')
                height: qsTranslate('','survey_thanku_btn_h')
                anchors.horizontalCenter:  parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: qsTranslate('','survey_thanku_btn_bm')
                btnTextWidth:parseInt(qsTranslate('',"survey_thanku_btn_textw"),10);
                buttonText: [configTool.language.survey.take_another_survey,nxtTmlpate.textColor,qsTranslate('','survey_thanku_btn_fontsize')]
                isHighlight: activeFocus
                normalImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_n
                highlightImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_h
                pressedImg: viewHelper.configToolImagePath+configTool.config.discover.survey.btn_p
                normalBorder: [qsTranslate('','survey_thanku_btn_margin')]
                highlightBorder: [qsTranslate('','survey_thanku_btn_margin')]
                onEnteredReleased:  backBtnPressed()
                onNoItemFound: if(dir=="up")commonObj.synopsisCloseBtn.forceActiveFocus()
                Component.onCompleted: {
                    if(nxtTmlpate.visible==false)commonObj.synopsisCloseBtn.forceActiveFocus()
                    else nxtTmlpate.forceActiveFocus()
                }
            }
        }
    }
    Component{id:blankCompSurvey;Item{}}
    Component{
        id:compChoosenTemplate
        Loader{
            id:surveyChoosenLoader
            anchors.top: parent.top
            anchors.topMargin:qsTranslate('','survey_panel_tm')
            anchors.horizontalCenter: parent.horizontalCenter
            width: commonObj.surveyPanel.width
            height: commonObj.surveyPanel.height
            sourceComponent:blankCompSurvey
            property alias surveyChoosenLoader: surveyChoosenLoader
            onStatusChanged:  {
                if(surveyChoosenLoader.sourceComponent==blankCompSurvey)return;
                if (surveyChoosenLoader.status == Loader.Ready){
                    if(sourceComponent!=surveyDone) surveyChoosenLoader.item.forceActiveFocus()
                }
            }
        }
    }

    Component{
        id:freetext
        FocusScope{
            id:freeTextFS
            anchors.fill: parent
            property alias textBox: textBox
            property alias vkbBtn: vkbBtn
            function updateText(intext){textBox.text=intext}
            function focusOnVkbBtn(){vkbBtn.forceActiveFocus()}
            Image{
                anchors.centerIn: multiTxtPanel
                visible: textBox.activeFocus
                source: viewHelper.configToolImagePath+configTool.config.discover.survey.multitext_panel_h
            }
            Image{
                id:multiTxtPanel
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin:  qsTranslate('','survey_freetext_lm')
                anchors.topMargin:  qsTranslate('','survey_freetext_tm')
                source:viewHelper.configToolImagePath+configTool.config.discover.survey.multitext_panel
            }
            Item{
                id:rectTxtEdit
                width: qsTranslate('','survey_freetext_w')
                height:  qsTranslate('','survey_freetext_h')
                anchors.left: multiTxtPanel.left
                anchors.top: multiTxtPanel.top
                anchors.leftMargin:  qsTranslate('','survey_freetext_text_lm')
                anchors.topMargin:  qsTranslate('','survey_freetext_text_tm')
                //clip: true
                TextEdit{
                    id:textBox
                    property int lineLimit :9
                    color:configTool.config.discover.survey.description1_color
                    font.pixelSize:  qsTranslate('','survey_freetext_fontsize')
                    font.family:viewHelper.adultFont;
                    cursorVisible:activeFocus
                    activeFocusOnPress:false
                    width:rectTxtEdit.width;
                    height:rectTxtEdit.height;
                    wrapMode:Text.Wrap;
                    inputMethodHints:Qt.ImhNoPredictiveText
                    text:unescape(freeText)
                    focus:true
                    onTextChanged: {
                        if(textBox.lineCount > lineLimit)
                            textBox.text = textBox.text.substring(0,textBox.cursorPosition-1) + textBox.text.substring(textBox.cursorPosition,textBox.text.length)
                    }
                    Keys.onUpPressed:{
                        if(cursorRectangle.y==0)commonObj.synopsisCloseBtn.forceActiveFocus()
                        else event.accepted=false
                    }
                    onActiveFocusChanged: {
                        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){  // CR changes
                            viewHelper.vkbInputData=textBox.text
                            core.debug("onActionReleased | viewHelper.vkbInputData "+viewHelper.vkbInputData)
                            widgetList.showVkb(true)
                        }
                    }

                    Keys.onDownPressed: {

                        if(cursorRectangle.y>=(textBox.paintedHeight-cursorRectangle.height)){
                            textBox.focus=false;
                            vkbBtn.forceActiveFocus();
                            event.accepted=true;
                        }
                        else event.accepted=false
                    }
                }
                MouseArea{
                    anchors.fill:parent;
                    onClicked:{
                        parent.forceActiveFocus();
                    }
                }
            }
            SimpleNavButton{
                id:vkbBtn
                visible: true;
                //onVisibleChanged: if(visible)vkbBtn.forceActiveFocus()
                anchors.bottom: multiTxtPanel.bottom
                anchors.right: multiTxtPanel.right
                anchors.rightMargin:   qsTranslate('','survey_freetext_btn_rm')
                anchors.bottomMargin:   qsTranslate('','survey_freetext_btn_bm')
                isHighlight: activeFocus
                normImg: viewHelper.configToolImagePath+configTool.config.discover.survey.vkb_btn_n
                pressedImg: viewHelper.configToolImagePath+configTool.config.discover.survey.vkb_btn_p
                highlightimg: viewHelper.configToolImagePath+configTool.config.discover.survey.vkb_btn_h
                onEnteredReleased: {
                    viewHelper.vkbInputData=textBox.text
                    core.debug("onActionReleased | viewHelper.vkbInputData "+viewHelper.vkbInputData)
                    widgetList.showVkb(true)
                }
                onNoItemFound:{
                    if(dir=="down"){
                        if(!commonObj.nxtPrevFS.prevBtn.visible)commonObj.nxtPrevFS.nextBtn.forceActiveFocus()
                    }else if(dir=="up"){
                        textBox.forceActiveFocus();
                    }
                }

            }
        }
    }

    Component{
        id:mcq
        FocusScope{
            id:mcqFS
            anchors.fill: parent
            property alias textBox: textBox
            property alias vkbBtn: vkbBtn
            function updateText(intext){textBox.text=intext}
            Scroller{
                id:slider
                anchors.top:parent.top; anchors.topMargin: qsTranslate('','survey_slider_tm')
                anchors.right: parent.right ;anchors.rightMargin: qsTranslate('','survey_slider_rm')
                width:qsTranslate('','survey_slider_w');height:qsTranslate('','survey_slider_h')
                targetData:mcqLV
                sliderbg:[qsTranslate('','survey_slider_base_margin'),qsTranslate('','survey_slider_w'),qsTranslate('','survey_slider_base_h'),configTool.config.discover.survey.scroll_base]
                scrollerDot:configTool.config.discover.survey.scroll_button
                downArrowDetails:  [configTool.config.discover.survey.dn_arrow_n,configTool.config.discover.survey.dn_arrow_h,configTool.config.discover.survey.dn_arrow_p]
                upArrowDetails: [configTool.config.discover.survey.up_arrow_n,configTool.config.discover.survey.up_arrow_h,configTool.config.discover.survey.up_arrow_p]
                visible: mcqLV.contentHeight>mcqLV.height
                onSliderNavigation: {
                    if(direction=="up")commonObj.synopsisCloseBtn.forceActiveFocus()
                    else if(direction=="down")commonObj.nxtPrevFS.forceActiveFocus()
                    else if(direction=="left") {
                        if(txtPanel.visible){
                            columnMCQ[mcqLV.count-1]?txtPanel.forceActiveFocus():mcqLV.forceActiveFocus()
                        }
                        else  mcqLV.forceActiveFocus()
                    }
                }
                onArrowPressed: mcqLV.currentIndex=mcqLV.indexAt(5,mcqLV.contentY+2)
            }
            ListView{
                id:mcqLV
                clip: true
                focus: true
                delegate: compOption
                model:suranswerModel
                width: qsTranslate('','survey_mcq_w')
                height: qsTranslate('','survey_mcq_h')
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin:  qsTranslate('','survey_mcq_lm')
                anchors.topMargin:  qsTranslate('','survey_mcq_tm')
                Keys.onRightPressed:{
                    if(txtPanel.visible){
                        if(currentIndex==count-1) {
                            initializeModifyColumnArray(count-1)
                            txtPanel.forceActiveFocus()
                        }
                    }
                    else slider.forceActiveFocus()
                }
                Keys.onUpPressed: {
                    if(currentItem.y==contentY || currentIndex==0)commonObj.synopsisCloseBtn.forceActiveFocus()
                    else decrementCurrentIndex()
                }
                Keys.onDownPressed: {
                    if((currentItem.y+currentItem.height)>=(contentY+height)|| (currentIndex==count-1))
                        commonObj.nxtPrevFS.forceActiveFocus()
                    else incrementCurrentIndex()
                }
                function select(){
                    forceActiveFocus();
                    if(isMultiCheckAllowed)
                        initializeModifyColumnArray(currentIndex,"",!currentItem.getbtnState())
                    else  initializeModifyColumnArray(currentIndex,"",true)
                }
                onMovementEnded: {
                    currentIndex=indexAt(contentX+5,contentY+5)
                    mcqLV.forceActiveFocus()
                }
            }
            FocusScope{
                id:txtPanel
                width: singleTxtPanel.width
                height: singleTxtPanel.height
                anchors.top: mcqLV.top
                anchors.topMargin:(qsTranslate('','survey_mcq_cellh') * (mcqLV.count-1)) - mcqLV.contentY
                anchors.right: slider.left
                anchors.rightMargin:   qsTranslate('','survey_mcq_text_rm')
                visible: isMCQFreetxt?((mcqLV.contentY>=(mcqLV.contentHeight-mcqLV.height))?true:false):false
                Image{
                    anchors.centerIn: singleTxtPanel
                    visible: textBox.activeFocus
                    source: viewHelper.configToolImagePath+configTool.config.discover.survey.singletext_panel_h
                }
                Image{
                    id:singleTxtPanel
                    source:viewHelper.configToolImagePath+configTool.config.discover.survey.singletext_panel
                }
                TextInput{
                    id:textBox
                    anchors.verticalCenter: singleTxtPanel.verticalCenter
                    anchors.left: singleTxtPanel.left
                    anchors.leftMargin: qsTranslate('','survey_mcq_text_lm')
                    width: qsTranslate('','survey_mcq_text_w')
                    color:configTool.config.discover.survey.description1_color
                    font.pixelSize:  qsTranslate('','survey_mcq_text_fontsize')
                    font.family: "FS Elliot";
                    cursorVisible:activeFocus
                    activeFocusOnPress:false
                    text:unescape(freeText)
                    focus:true
                    Keys.onUpPressed: commonObj.synopsisCloseBtn.forceActiveFocus()
                    Keys.onDownPressed:commonObj.nxtPrevFS.forceActiveFocus()
                    Keys.onRightPressed:{

                        if(cursorPosition==text.length){vkbBtn.forceActiveFocus()}
                        else cursorPosition +=1
                    }
                    Keys.onLeftPressed: {
                        if(cursorPosition==0)mcqLV.forceActiveFocus()
                        else cursorPosition -=1
                    }
                    onActiveFocusChanged: {
                        if((pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)){   // CR changes
                            isVkbPressed=true
                            initializeModifyColumnArray(suranswerModel.count-1)
                            vkbBtn.forceActiveFocus()
                            viewHelper.vkbInputData=textBox.text
                            widgetList.showVkb(true)
                        }

                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            isVkbPressed=true
                            initializeModifyColumnArray(suranswerModel.count-1)
                            vkbBtn.forceActiveFocus()
                        }
                    }
                }
                SimpleNavButton{
                    id:vkbBtn
                    //                    visible: isVkbPressed
                    //                    onVisibleChanged: {
                    //                        if(visible){
                    //                            if(mcqLV.contentY>=(mcqLV.contentHeight-mcqLV.height)){
                    //                                if(textBox.activeFocus)vkbBtn.forceActiveFocus()
                    //                                return
                    //                            }
                    //                            vkbBtn.forceActiveFocus()
                    //                        }
                    //                        else textBox.focus=true
                    //                    }
                    anchors.verticalCenter:  singleTxtPanel.verticalCenter
                    anchors.right: singleTxtPanel.right
                    anchors.rightMargin:    qsTranslate('','survey_mcq_text_btn_rm')
                    isHighlight: activeFocus
                    normImg: viewHelper.configToolImagePath+configTool.config.discover.survey.vkb_btn_n
                    pressedImg: viewHelper.configToolImagePath+configTool.config.discover.survey.vkb_btn_p
                    highlightimg: viewHelper.configToolImagePath+configTool.config.discover.survey.vkb_btn_h
                    onEnteredReleased:{
                        viewHelper.vkbInputData=textBox.text
                        widgetList.showVkb(true)
                    }
                    onNoItemFound:{
                        if(dir=="right"){
                            if(slider.visible){slider.forceActiveFocus()}
                        }else if(dir=="down"){
                            commonObj.nxtPrevFS.forceActiveFocus();
                        }else if(dir=="left"){
                            textBox.forceActiveFocus();
                        }
                    }
                }
            }
        }
    }
    Component{
        id:compOption
        FocusScope{
            width: qsTranslate('','survey_mcq_cellw')
            height: qsTranslate('','survey_mcq_cellh')
            function getbtnState(){
                return btn.isSelected;
            }
            SimpleButton{
                id: btn
                focus: true
                isSelected:(columnMCQ[index]==true)?((isPressed)?false:true):false
                isHighlight:(mcqLV.currentIndex==index && mcqLV.activeFocus)
                forcelySelected:isSelected
                normImg: viewHelper.configToolImagePath+(isMultiCheckAllowed?configTool.config.discover.survey.check_n:configTool.config.discover.survey.radio_n)
                pressedImg:isMultiCheckAllowed? (viewHelper.configToolImagePath+(isSelected?configTool.config.discover.survey.check_sp:configTool.config.discover.survey.check_p)):(viewHelper.configToolImagePath+(isSelected?configTool.config.discover.survey.radio_sp:configTool.config.discover.survey.radio_p))
                highlightimg: viewHelper.configToolImagePath+(isMultiCheckAllowed?configTool.config.discover.survey.check_h:configTool.config.discover.survey.radio_h)
                selectedImg:isMultiCheckAllowed? (viewHelper.configToolImagePath+((isHighlight && isSelected)?configTool.config.discover.survey.check_sh:(configTool.config.discover.survey.check_sn))): (viewHelper.configToolImagePath+((isHighlight && isSelected)?configTool.config.discover.survey.radio_sh:(configTool.config.discover.survey.radio_sn)))
                onEnteredReleased:{
                    mcqLV.currentIndex=index;
                    mcqLV.select();
                }
            }
            ViewText{
                anchors.left: btn.right
                anchors.leftMargin: qsTranslate('','survey_mcq_gap')
                anchors.verticalCenter: btn.verticalCenter
                width: qsTranslate('','survey_mcq_textw')
                varText: [text,configTool.config.discover.survey.description1_color,qsTranslate('','survey_mcq_fontsize')]
                text:suranswerModel.getValue(index,"answer")
                MouseArea{
                    anchors.fill:parent;
                    onClicked:{
                        btn.enteredReleased();
                    }
                }
            }
        }
    }
    Component{
        id:semantic
        FocusScope{
            id:semanticFS
            width: qsTranslate('','survey_semantic_w')
            height: parseInt(qsTranslate('','survey_semantic_h'),10)+notLiked.height+parseInt( qsTranslate('','survey_semantic_vgap'),10)
            property int reduceSpacing: virtualIndicator.width/2
            anchors.top:parent.top
            anchors.left: parent.left
            anchors.leftMargin: qsTranslate('','survey_semantic_lm')
            anchors.topMargin: qsTranslate('','survey_semantic_tm')
            ViewText{
                id:notLiked
                anchors.left:parent.left
                anchors.leftMargin: qsTranslate('','survey_semantic_gap')
                width:  qsTranslate('','survey_semantic_textw')
                varText: [suranswerModel.getValue(0 ,'answer'),configTool.config.discover.survey.semantic_title_color, qsTranslate('','survey_semantic_fontsize')]
                horizontalAlignment: Text.AlignLeft
            }
            ViewText{
                id:liked
                anchors.right:parent.right
                anchors.rightMargin:  qsTranslate('','survey_semantic_gap')
                width:  qsTranslate('','survey_semantic_textw')
                varText: [suranswerModel.getValue(1 ,'answer'),configTool.config.discover.survey.semantic_title_color, qsTranslate('','survey_semantic_fontsize')]
                horizontalAlignment:  Text.AlignRight
            }
            Rectangle{
                id:semanticScale
                anchors.left:parent.left
                anchors.leftMargin: -qsTranslate('','survey_semantic_h')
                anchors.top: notLiked.bottom
                anchors.topMargin: qsTranslate('','survey_semantic_vgap')
                width: qsTranslate('','survey_semantic_h')
                height:qsTranslate('','survey_semantic_w')
                radius:  qsTranslate('','survey_semantic_radius')
                smooth: true
                transformOrigin: Item.TopRight
                rotation:270
                gradient: Gradient {
                    GradientStop { position: 0.0; color: configTool.config.discover.survey.semantic_base_start }
                    GradientStop { position: 0.5; color: configTool.config.discover.survey.semantic_base_middle }
                    GradientStop { position: 1.0; color:  configTool.config.discover.survey.semantic_base_end }
                }
                Column{
                    id:divLine
                    spacing:(semanticScale.height/9)
                    Repeater{
                        model:9
                        delegate: Rectangle{
                            width: qsTranslate('','survey_semantic_h')
                            height: 1
                            color: index==0?"transparent":configTool.config.discover.survey.semantic_divider_color
                        }
                    }
                }
            }
            Rectangle{
                id:virtualSemanticScale
                color: "transparent"
                anchors.left:parent.left
                anchors.top: notLiked.bottom
                anchors.topMargin: qsTranslate('','survey_semantic_vgap')
                width: qsTranslate('','survey_semantic_w')
                height:qsTranslate('','survey_semantic_h')
            }
            Rectangle{
                id:virtualIndicator
                width: qsTranslate('','survey_semantic_circle_w')
                height:qsTranslate('','survey_semantic_circle_h')
                color: "transparent"
                anchors.verticalCenter: virtualSemanticScale.verticalCenter
                x:(freeText==1?0:((freeText-1)* divLine.spacing)-semanticFS.reduceSpacing)
            }
            SimpleNavButton{
                id:indicator
                focus: true
                anchors.centerIn: virtualIndicator
                // x:(freeText* divLine.spacing)
                isHighlight: activeFocus
                normImg: viewHelper.configToolImagePath+configTool.config.discover.survey.indicator_n
                pressedImg: viewHelper.configToolImagePath+configTool.config.discover.survey.indicator_p
                highlightimg: viewHelper.configToolImagePath+configTool.config.discover.survey.indicator_h
                nav:["",commonObj.synopsisCloseBtn,"",commonObj.nxtPrevFS]
                onNoItemFound: {
                    var page=Math.floor((Math.floor(((virtualIndicator.x+semanticFS.reduceSpacing)/virtualSemanticScale.width) *100))/10)
                    if(dir=="left"){
                        if((page-1)==0)virtualIndicator.x=0
                        else if(virtualIndicator.x>0)virtualIndicator.x = ((page-1) *  divLine.spacing)-semanticFS.reduceSpacing
                    }
                    else if(dir=="right"){
                        if(virtualIndicator.x<indicatorMouse.drag.maximumX)virtualIndicator.x =((page+1) *  divLine.spacing)-semanticFS.reduceSpacing
                        if((page+1)==9)virtualIndicator.x=(virtualSemanticScale.width-virtualIndicator.width)
                    }
                    var freetextPage=Math.floor((Math.floor((virtualIndicator.x/virtualSemanticScale.width) *100))/10)
                    freeText=(freetextPage+1)
                    isSemanticScaleMoved=true
                }
                MouseArea{
                    id:indicatorMouse
                    anchors.fill: parent
                    drag.target: virtualIndicator
                    drag.axis: Drag.XAxis
                    drag.minimumX: 0
                    drag.maximumX: (virtualSemanticScale.width-virtualIndicator.width)
                    onReleased: {
                        var page=Math.floor((Math.floor((virtualIndicator.x/virtualSemanticScale.width) *100))/10)
                        if(page==9)virtualIndicator.x=(virtualSemanticScale.width-virtualIndicator.width)
                        else if(page==0)virtualIndicator.x=0
                        else virtualIndicator.x=(page * divLine.spacing)-semanticFS.reduceSpacing
                        freeText=(page+1)
                        isSemanticScaleMoved=true
                    }
                }
            }

        }
    }
    Component{
        id:yesNoType
        FocusScope{
            anchors.fill: parent
            SimpleNavButton{
                id:yes
                focus: true
                anchors.left: parent.left
                anchors.leftMargin:  qsTranslate('','survey_single_lm')
                anchors.top: parent.top
                anchors.topMargin:  qsTranslate('','survey_single_lm')
                isSelected: true
                isHighlight: activeFocus
                forcelySelected:isSelected
                normImg: viewHelper.configToolImagePath+configTool.config.discover.survey.radio_n
                pressedImg: viewHelper.configToolImagePath+(isSelected?configTool.config.discover.survey.radio_sp:configTool.config.discover.survey.radio_p)
                highlightimg: viewHelper.configToolImagePath+configTool.config.discover.survey.radio_h
                selectedImg: viewHelper.configToolImagePath+((isHighlight && isSelected)?configTool.config.discover.survey.radio_sh:(configTool.config.discover.survey.radio_sn))
                nav:["",commonObj.synopsisCloseBtn,no,commonObj.nxtPrevFS]
                onEnteredReleased: {
                    yes.isSelected=true
                    no.isSelected=false
                }
            }
            ViewText{
                id:yesTxt
                width: qsTranslate('','survey_single_w')
                anchors.verticalCenter: yes.verticalCenter
                anchors.left: yes.right
                anchors.leftMargin:  qsTranslate('','survey_single_text_lm')
                varText: [suroptionModel.getValue(0,"answer"),configTool.config.discover.survey.description1_color, qsTranslate('','survey_single_fontsize')]
            }
            SimpleNavButton{
                id:no
                anchors.left: yesTxt.right
                anchors.leftMargin:  qsTranslate('','survey_single_gap')
                anchors.top: parent.top
                anchors.topMargin:  qsTranslate('','survey_single_lm')
                isSelected: false
                isHighlight: activeFocus
                forcelySelected:isSelected
                normImg: viewHelper.configToolImagePath+configTool.config.discover.survey.radio_n
                pressedImg: viewHelper.configToolImagePath+(isSelected?configTool.config.discover.survey.radio_sp:configTool.config.discover.survey.radio_p)
                highlightimg: viewHelper.configToolImagePath+configTool.config.discover.survey.radio_h
                selectedImg: viewHelper.configToolImagePath+(isHighlight?configTool.config.discover.survey.radio_sh:configTool.config.discover.survey.radio_sn)
                nav:[yes,commonObj.synopsisCloseBtn,"",commonObj.nxtPrevFS]
                onEnteredReleased: {
                    no.isSelected=true
                    yes.isSelected=false
                }
            }
            ViewText{
                id:noTxt
                width: qsTranslate('','survey_single_w')
                anchors.verticalCenter: no.verticalCenter
                anchors.left: no.right
                anchors.leftMargin:  qsTranslate('','survey_single_text_lm')
                varText: [suroptionModel.getValue(1,"answer"),configTool.config.discover.survey.description1_color, qsTranslate('','survey_single_fontsize')]
            }
        }
    }
    Component{
        id:likertScale
        FocusScope{
            id:likertScaleFS
            anchors.fill: parent
            property alias likertScaleLV: likertScaleLV
            property alias likertScaleGV: likertScaleGV
            property alias likerScaleTitle: likerScaleTitle
            property bool directionFromHScroller: false
            function setView(){
                if(likertScaleOption.contentY+likertScaleOption.height>likertScaleOption.contentHeight)
                    likertScaleOption.contentY=likertScaleOption.contentHeight-likertScaleOption.height;
                else if(likertScaleOption.contentY < 0)likertScaleOption.contentY=0
                if(likertScaleOption.contentX+likertScaleOption.width>likertScaleOption.contentWidth)
                    likertScaleOption.contentX=likertScaleOption.contentWidth-likertScaleOption.width;
                else  if(likertScaleOption.contentX<0)likertScaleOption.contentX=0
                likertScaleLV.contentY=likertScaleGV.contentY
                likerScaleTitle.contentX=likertScaleOption.contentX
                likertScaleLV.currentIndex=Math.floor(likertScaleGV.currentIndex/likerScaleTitle.count)
            }
            function setViewFromTitle(){
                if(likerScaleTitle.contentX+likerScaleTitle.width>likerScaleTitle.contentWidth)
                    likerScaleTitle.contentX=likerScaleTitle.contentWidth-likerScaleTitle.width;
                else if(likerScaleTitle.contentX < 0)likerScaleTitle.contentX=0
                likertScaleOption.contentX=likerScaleTitle.contentX
                likertScaleGV.currentIndex=likertScaleGV.indexAt(likerScaleTitle.contentX+5,likerScaleTitle.contentY)
                likertScaleLV.currentIndex=Math.floor(likertScaleGV.currentIndex/likerScaleTitle.count)
            }
            function setViewForQuestion(){
                if(likertScaleLV.contentY+likertScaleLV.height>likertScaleLV.contentHeight)
                    likertScaleLV.contentY=likertScaleLV.contentHeight-likertScaleLV.height;
                else if(likertScaleLV.contentY < 0)likertScaleLV.contentY=0
                likertScaleGV.contentY=likertScaleLV.contentY
                likertScaleLV.currentIndex=likertScaleLV.indexAt(likertScaleLV.contentX,likertScaleLV.contentY+2)
                likertScaleGV.currentIndex=likertScaleGV.indexAt(likertScaleGV.contentX+2,likertScaleGV.contentY)
            }
            ListView{
                id:likertScaleLV
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin:  qsTranslate('','survey_likertscale_lm')
                anchors.topMargin:  qsTranslate('','survey_likertscale_tm')
                width: qsTranslate('','survey_likertscale_textw')
                height:qsTranslate('','survey_likertscale_h')
                clip: true
                interactive: count>3?true:false
                boundsBehavior: ListView.StopAtBounds;
                snapMode:ListView.SnapToItem;
                preferredHighlightBegin:0;
                preferredHighlightEnd:height;
                highlightRangeMode: "StrictlyEnforceRange"
                delegate:    ViewText{
                    id:optionTxt
                    width: qsTranslate('','survey_likertscale_textw')
                    height: qsTranslate('','survey_likertscale_cellh')
                    varText: [text,configTool.config.discover.survey.description1_color,qsTranslate('','survey_likertscale_fontsize')]
                    text:questionType=="likertScale"?suroptionModel.getValue(index,"question"):suranswerModel.getValue(index,"answer")
                    verticalAlignment: Text.AlignVCenter
                }
                model:questionType=="likertScale"?suroptionModel:suranswerModel
                onMovementEnded: setViewForQuestion()
            }
            Flickable{
                id:likertScaleOption
                width: parseInt(qsTranslate('','survey_likertscale_title_w'),10) * (likerScaleTitle.count>=3?3:likerScaleTitle.count)
                height:likertScaleLV.count>=3? qsTranslate('','survey_likertscale_h'):likertScaleLV.count * parseInt(qsTranslate('','survey_likertscale_cellh'),10)
                clip:true
                focus: true
                anchors.left:likerScaleTitle.left
                anchors.top:likertScaleLV.top
                contentWidth:parseInt(qsTranslate('','survey_likertscale_title_w'),10) * likerScaleTitle.count
                contentHeight: likertScaleLV.contentHeight
                flickableDirection: Flickable.HorizontalAndVerticalFlick
                boundsBehavior: GridView.StopAtBounds;
                onMovementEnded: {
                    var temp= parseInt(qsTranslate('','survey_likertscale_title_w'),10)
                    if((contentX% temp)!=0){
                        if((contentX+temp)<(contentWidth-contentX))
                            contentX=(contentX-(contentX%temp))+temp
                        else
                            contentX=(contentX-(contentX%temp))
                    }
                    likertScaleFS.setView()
                    likertScaleGV.currentIndex=likertScaleGV.indexAt(contentX+5,contentY+5);
                }
                GridView{
                    id:likertScaleGV
                    focus: true
                    cellWidth:  parseInt(qsTranslate('','survey_likertscale_title_w'),10)
                    cellHeight:qsTranslate('','survey_likertscale_cellh')
                    width: parseInt(qsTranslate('','survey_likertscale_title_w'),10) * likerScaleTitle.count
                    height:  qsTranslate('','survey_likertscale_h')
                    model:questionType=="likertScale"? (likertScaleLV.count * likerScaleTitle.count):(suranswerModel.count * suranswerModel.count)
                    clip: true
                    interactive: likertScaleLV.count>3?true:false
                    delegate:questionType=="likertScale"? compLikerScale:(questionType=="ordinalResponse"?compOrdinalResponse:"")
                    layoutDirection: GridView.LeftToRight
                    onMovementEnded:{   currentIndex=indexAt(contentX+5,contentY+5);likertScaleFS.setView()}
                    Keys.onReleased:  likertScaleFS.directionFromHScroller=false
                    Keys.onRightPressed: {
                        if(currentIndex+1<(likerScaleTitle.count * (likertScaleLV.currentIndex+1)))currentIndex++
                        else {
                            if(sliderV.visible)   sliderV.forceActiveFocus()
                            else commonObj.synopsisCloseBtn.forceActiveFocus()
                        }
                        if(currentItem.x >= (likertScaleOption.contentX + likertScaleOption.width))likertScaleOption.contentX += parseInt(qsTranslate('','survey_likertscale_title_w'),10)
                        likertScaleFS.setView()
                        likertScaleFS.directionFromHScroller=false
                    }
                    Keys.onLeftPressed:  {
                        if((currentIndex)!= ((likerScaleTitle.count * (likertScaleLV.currentIndex+1))-likerScaleTitle.count))currentIndex--
                        else return
                        if(currentItem.x < likertScaleOption.contentX)likertScaleOption.contentX -= parseInt(qsTranslate('','survey_likertscale_title_w'),10)
                        likertScaleFS.setView()
                    }
                    Keys.onDownPressed: {
                        if((likertScaleLV.currentIndex+1)<= (likertScaleLV.count-1))currentIndex +=likerScaleTitle.count
                        else {
                            if(sliderH.visible) sliderH.forceActiveFocus()
                            else commonObj.nxtPrevFS.forceActiveFocus()
                        }
                        likertScaleFS.setView()

                    }
                    Keys.onUpPressed: {
                        if((likertScaleLV.currentIndex-1)>=0)currentIndex -=likerScaleTitle.count
                        likertScaleFS.setView()
                    }
                }

            }
            ListView{
                id:likerScaleTitle
                width: (qsTranslate('','survey_likertscale_title_w') * 3)
                height: parseInt(qsTranslate('','survey_likertscale_title_fontsize'),10)+3
                orientation: ListView.Horizontal
                //     interactive: false
                boundsBehavior: ListView.StopAtBounds;
                snapMode:ListView.SnapToItem;
                clip: true
                anchors.left:likertScaleLV.left
                anchors.leftMargin: qsTranslate('','survey_likertscale_title_lm')
                anchors.bottom: likertScaleLV.top
                anchors.bottomMargin: qsTranslate('','survey_likertscale_title_bm')
                model:suranswerModel
                onMovementEnded: setViewFromTitle()
                delegate: Rectangle{
                    width: qsTranslate('','survey_likertscale_title_w')
                    height: rating.paintedHeight
                    color:"transparent"
                    ViewText{
                        id:rating
                        width:  qsTranslate('','survey_likertscale_title_text_w')
                        anchors.centerIn: parent
                        varText: [text,configTool.config.discover.survey.title_color,qsTranslate('','survey_likertscale_title_fontsize')]
                        text:questionType=="likertScale"?likerScaleTitle.model.getValue(index,"answer"):(index +1 )
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
            }
            ScrollerHoriz{
                id:sliderH
                anchors.top:likertScaleLV.bottom; anchors.topMargin: qsTranslate('','survey_likertscale_hslider_tm')
                anchors.right: likertScaleOption.right ;anchors.rightMargin: qsTranslate('','survey_likertscale_hslider_rm')
                width:qsTranslate('','survey_likertscale_hslider_w');height:qsTranslate('','survey_likertscale_hslider_h')
                targetData:likertScaleOption
                sliderbg:[qsTranslate('','survey_likertscale_hslider_base_margin'),qsTranslate('','survey_likertscale_hslider_base_w'),qsTranslate('','survey_likertscale_hslider_h'),configTool.config.discover.survey.likert_scroll_base]
                scrollerDot:configTool.config.discover.survey.likert_scroll_button
                downArrowDetails:  [configTool.config.discover.survey.right_arrow_n,configTool.config.discover.survey.right_arrow_h,configTool.config.discover.survey.right_arrow_p]
                upArrowDetails: [configTool.config.discover.survey.left_arrow_n,configTool.config.discover.survey.left_arrow_h,configTool.config.discover.survey.left_arrow_p]
                visible: likertScaleOption.contentWidth>likertScaleOption.width
                onSliderNavigation: {
                    if(direction=="up")likertScaleGV.forceActiveFocus()
                    else if(direction=="down")commonObj.nxtPrevFS.forceActiveFocus()
                    else if(direction=="right"){
                        likertScaleFS.directionFromHScroller=true
                        if(sliderV.visible)
                            sliderV.forceActiveFocus()
                    }
                }
                onArrowPressed: {
                    likertScaleFS.directionFromHScroller=true
                    likertScaleFS.setView()
                    likertScaleGV.currentIndex=likertScaleGV.indexAt(likertScaleOption.contentX+2,likertScaleOption.contentY)
                    likertScaleLV.currentIndex=Math.floor(likertScaleGV.currentIndex/likerScaleTitle.count)
                }
            }
            Scroller{
                id:sliderV
                anchors.top:parent.top; anchors.topMargin: qsTranslate('','survey_likertscale_vslider_tm')
                anchors.right: parent.right ;anchors.rightMargin: qsTranslate('','survey_likertscale_vslider_rm')
                width:qsTranslate('','survey_likertscale_vslider_w');height:qsTranslate('','survey_likertscale_vslider_h')
                targetData:likertScaleGV
                sliderbg:[qsTranslate('','survey_likertscale_vslider_base_margin'),qsTranslate('','survey_likertscale_vslider_w'),qsTranslate('','survey_likertscale_vslider_base_h'),configTool.config.discover.survey.scroll_base]
                scrollerDot:configTool.config.discover.survey.scroll_button
                downArrowDetails:  [configTool.config.discover.survey.dn_arrow_n,configTool.config.discover.survey.dn_arrow_h,configTool.config.discover.survey.dn_arrow_p]
                upArrowDetails: [configTool.config.discover.survey.up_arrow_n,configTool.config.discover.survey.up_arrow_h,configTool.config.discover.survey.up_arrow_p]
                visible: likertScaleGV.contentHeight>likertScaleGV.height
                onSliderNavigation: {
                    if(direction=="up")commonObj.synopsisCloseBtn.forceActiveFocus()
                    else if(direction=="down")commonObj.nxtPrevFS.forceActiveFocus()
                    else if(direction=="left"){
                        if(likertScaleFS.directionFromHScroller && sliderH.visible)sliderH.forceActiveFocus()
                        else   likertScaleGV.forceActiveFocus()
                    }
                }
                onArrowPressed: {
                    likertScaleGV.currentIndex=likertScaleGV.indexAt(likertScaleOption.contentX+5,likertScaleGV.contentY+5)
                    likertScaleFS.setView()
                }
            }

        }
    }
    Component{
        id:compOrdinalResponse
        Rectangle{
            color:"transparent"
            width: parseInt(qsTranslate('','survey_likertscale_title_w'),10)
            height: qsTranslate('','survey_likertscale_cellh')
            property int rowIndex: choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle==undefined?0:Math.floor(index/choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle.count)
            property int colIndex:choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle==undefined?0: (index>(choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle.count-1)?(index -((choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle.count) * (rowIndex))):index)
            onActiveFocusChanged: if(activeFocus)btn.forceActiveFocus()
            SimpleNavButton{
                id: btn
                anchors.centerIn: parent
                isSelected: columnRowMCQOrdinal[rowIndex]==undefined?false:columnRowMCQOrdinal[rowIndex][colIndex]
                isHighlight: activeFocus
                forcelySelected:isSelected
                normImg: viewHelper.configToolImagePath+configTool.config.discover.survey.radio_n
                pressedImg:viewHelper.configToolImagePath+(isSelected?configTool.config.discover.survey.radio_sp:configTool.config.discover.survey.radio_p)
                highlightimg: viewHelper.configToolImagePath+configTool.config.discover.survey.radio_h
                selectedImg:viewHelper.configToolImagePath+((isHighlight && isSelected)?configTool.config.discover.survey.radio_sh:(configTool.config.discover.survey.radio_sn))
                onEnteredReleased:{
                    choosenTemplateObj.surveyChoosenLoader.item.likertScaleGV.currentIndex=index
                    initializeOrdinalScalerray(rowIndex,colIndex)
                }
            }
        }
    }
    Component{
        id:compLikerScale
        Rectangle{
            color:"transparent"
            width: parseInt(qsTranslate('','survey_likertscale_title_w'),10)
            height: qsTranslate('','survey_likertscale_cellh')
            property int rowIndex: choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle==undefined?0:Math.floor(index/choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle.count)
            property int colIndex:choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle==undefined?0:( index>(choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle.count-1)?(index -((choosenTemplateObj.surveyChoosenLoader.item.likerScaleTitle.count) * (rowIndex))):index)
            onActiveFocusChanged: if(activeFocus)btn.forceActiveFocus()
            SimpleNavButton{
                id: btn
                anchors.centerIn: parent
                isSelected: columnRowMCQ[rowIndex]==undefined?false:columnRowMCQ[rowIndex][colIndex]
                isHighlight: activeFocus
                forcelySelected:isSelected
                normImg: viewHelper.configToolImagePath+configTool.config.discover.survey.radio_n
                pressedImg:viewHelper.configToolImagePath+(isSelected?configTool.config.discover.survey.radio_sp:configTool.config.discover.survey.radio_p)
                highlightimg: viewHelper.configToolImagePath+configTool.config.discover.survey.radio_h
                selectedImg:viewHelper.configToolImagePath+((isHighlight && isSelected)?configTool.config.discover.survey.radio_sh:(configTool.config.discover.survey.radio_sn))
                onEnteredReleased:{
                    choosenTemplateObj.surveyChoosenLoader.item.likertScaleGV.currentIndex=index
                    initializeLikertScalerray(rowIndex,colIndex)
                }
            }
        }
    }
    UserEventFilter{
        id: vkbBtnFilter;
        onKeyPressed: {  }
        onKeyReleased: {
            if(choosenTemplateObj.surveyChoosenLoader.sourceComponent==freetext || choosenTemplateObj.surveyChoosenLoader.sourceComponent==mcq ){


            }
            isVkbPressed=false
        }

    }
}
