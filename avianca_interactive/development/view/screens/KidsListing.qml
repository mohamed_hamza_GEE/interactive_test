// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import ImageMask 1.0

SmartLoader{
    id:listingScreen
    property bool isAnimFirstTime: true
    property bool hideCenter:false;
    property bool isSynopsis: false
    property bool isModelUpdated: false
    property bool isMousePressed: false
    property int selIndex:-1;
    property int selprevIndex:0;
    property int selnextIndex:0;
    property int listingsingleimgwidth:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_itemw')
    property variant mediaList:listingobj;
    property variant subCatobj:widgetList.getMainMenuRef();
    property variant radioData;
    property string currentTemplateNode:"movies"
    property string compOffset: qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_hgap')/2
    property string comptext_h: configTool.config[currentTemplateNode].listing.text_color_h
    property string comptext_n: configTool.config[currentTemplateNode].listing.text_color_n
    property string comptext_p: configTool.config[currentTemplateNode].listing.text_color_p
    property string compScale_n:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_scale')
    property string compScale_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_scale_h')
    property string compImage_h:viewHelper.configToolImagePath+configTool.config[currentTemplateNode].listing.poster_holder_h
    property string compImage_n:viewHelper.configToolImagePath+configTool.config[currentTemplateNode].listing.poster_holder_n
    property string compImage_p:viewHelper.configToolImagePath+configTool.config[currentTemplateNode].listing.poster_holder_p
    property string compImageMask:viewController.settings.assetImagePath+qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_source')
    property string compText_w:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_w')
    property string compText_w2:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_w2')
    property string compText_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_h1')
    property string compTextHighlight_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_h')
    property string compImageTM_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_tm_h')
    property string compImageTM_n:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_tm_n')
    property string compTextFonsize_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_fontsize_h')
    property string compTextFonsize_n:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_fontsize_n')
    property string compTextOpacity:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_opacity')


    property string compsubText_w:qsTranslate('','listing_music_list_subtext_w')
    property string compsubText_w2:qsTranslate('','listing_music_list_subtext_w2')
    property string compsubText_tm_h:qsTranslate('','listing_music_list_subtext_tm_h')
    property string compsubText_tm_n:qsTranslate('','listing_music_list_subtext_tm_n')
    property string compsubText_fontsize_n:qsTranslate('','listing_music_list_subtext_fontsize_n')
    property string compsubText_fontsize_h:qsTranslate('','listing_music_list_subtext_fontsize_h')
    /**************************************************************************************************************************/
    onIsSynopsisChanged: {
        if(isSynopsis){
            if(getCidTidForListing()[1]=="tvseries")
                getTvSeries()
        }
    }
    /**************************************************************************************************************************/
    function load(){
        push(compPostersBG,"posterobj")
        push(compListing,"listingobj")
        push(compSynopsis,"synopsisobj")
    }
    function init(){
        core.debug("KidsListing.qml | Init ")
        widgetList.showHeader(true);
        widgetList.showMainMenu(true);
        getListingMenu();

    }
    function reload(){
        if(!isModelUpdated){
            closeLoader();
        }
        getListingMenu();
    }
    function clearOnExit(){
        closeLoader();
        listingobj.compVpathListing.model=viewHelper.blankModel;
        listingobj.compVpathListing.currentIndex=0;
        core.debug("KidsListing.qml | clearOnExit ")
    }
    function backBtnPressed(){
        if(isSynopsis)synopsisobj.item.backBtnPressed();
        else viewHelper.homeAction()
    }
    function enterReleasedforListing(index){
        selIndex=index;
        listingobj.compVpathListing.currentIndex=index
        viewHelper.setMediaListingIndex(index)
        loadSynopsis()
    }
    function loadSynopsis(){
        if(currentTemplateNode=="kids_music_listing"){
            if(synopsisobj.sourceComponent==musicAlbumSynopsis){
                synopsisobj.item.init();
            }else{
                synopsisobj.sourceComponent = musicAlbumSynopsis;
            }
            viewHelper.setHelpTemplate("tracklist");
        }else if(currentTemplateNode=="kids_games_listing"){
            viewHelper.setHelpTemplate("games_synop");
            synopsisobj.synopsisItem.sourceComponent=gameSynopsisItem
        }
        else{
            synopsisobj.sourceComponent = baseSynopsis;

        }
        if(!isMousePressed) isSynopsis=true;
        else if(isMousePressed)isMousePressed=false
    }
    function incrementSelindex(){
        if(selIndex==listingobj.compVpathListing.count-1)selIndex=0
        else  selIndex++
    }
    function decrementSelindex(){
        if(selIndex==0)selIndex=listingobj.compVpathListing.count-1
        else  selIndex--
    }
    function getListingMenu(){
        mediaList=listingobj;
        listingobj.visible=true;
        listingobj.compVpathListing.setModel(viewHelper.blankModel)
        dataController.getNextMenu([getCidTidForSubCat()[0]  , getCidTidForSubCat()[1] ],listingReady)
    }
    function listingReady(dModel){
        listingobj.compVpathListing.setModel(viewHelper.blankModel)
        currentTemplateNode=widgetList.getCidTidMainMenu()[1];
        if(dModel.count==0){
            if(viewHelper.isCurtainFirstTime){
                widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
                viewHelper.isCurtainFirstTime=false
            }

            viewController.showScreenPopup(7);
            return;
        }
        listingobj.compVpathListing.forceActiveFocus()
        viewHelper.setHelpTemplateForListing(currentTemplateNode);
        if(current.template_id=="kids_games_listing"){
            listingobj.compVpathListing.delegate =maskingIconText;
        }else{
            listingobj.compVpathListing.delegate =iconText
        }
        listingobj.compVpathListing.setModel(dModel)
        if(!isModelUpdated) {listingobj.compVpathListing.currentIndex=0;}
        else  listingobj.compVpathListing.currentIndex=selIndex
        viewHelper.setMediaListingModel(dModel)
        if(viewHelper.isCurtainFirstTime){
            widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
            viewHelper.isCurtainFirstTime=false
        }
        if(!isModelUpdated) closeLoader()
        else synopsisobj.synopsisItem.item.init();
        if(!isModelUpdated && dModel.count>0)listingobj.compVpathListing.forceActiveFocus()
        if(widgetList.isVideoVisible())widgetList.videolanguageUpdate()
        isModelUpdated=false
    }
    function getCidTidForSubCat(){
        var params=[current.node_id,current.template_id]
        return params;
    }
    function getCidTidForListing(){
        var param=[]
        param[0]=listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"cid")
        param[1]=listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"media_attr_template_id")
        return param;
    }
    function focusFromMainMenu(){
        if(isSynopsis){

            synopsisobj.item.forceActiveFocus();
        }else{
            if(listingobj.compVpathListing.model.count>0)
                listingobj.compVpathListing.forceActiveFocus()
        }
    }
    function getTvSeries(){
        dataController.getNextMenu([listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"mid"),'tvseries'],episodeModelReady);
    }
    function episodeModelReady(dModel){
        synopsisobj.synopsisItem.item.tvModel=dModel//tempTvModel
        viewHelper.setTvSeriesModel(dModel)
        if(!isModelUpdated){
            synopsisobj.synopsisItem.item.forceFocus();
        }
    }
    function screenPopupClosed(id,param){
        if(id==2){
            viewHelper.isResume=param
            synopsisobj.synopsisItem.item.loadVod("play","tvseries")
        } else if(id==7){
            widgetList.getMainMenuRef().forceActiveFocus()
        }
    }
    function closeLoader(){
        if(!isModelUpdated) isSynopsis=false;
        if(synopsisobj.sourceComponent!=blankComp){
            synopsisobj.item.clearOnExit();
        }
        synopsisobj.sourceComponent = blankComp;
        if(!isModelUpdated && listingobj.compVpathListing.count>0)listingobj.compVpathListing.forceActiveFocus();
    }
    function getMediaInfo(tag){
        return mediaList.getMediaInfo(tag)
    }
    function getAudioCid(){
        return current.node_id;
    }
    function getAudioTid(){
        return current.template_id;
    }
    function languageUpdate(){
        isModelUpdated=false;
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel],[listingUpdate]);
    }
    function listingUpdate(){
        if(listingobj.visible){
            dataController.refreshModelsByIntLanguage([listingobj.compVpathListing.model],[updateSynopsis]);
        }else{
            updateSynopsis();
        }

    }
    function updateSynopsis(){
        if(isSynopsis){
            if(synopsisobj.item.langUpdate){
                synopsisobj.item.langUpdate();
            }
        }
    }
    /**************************************************************************************************************************/
    property QtObject posterobj
    Component{
        id:compPostersBG
        Item{
            anchors.fill: parent
            property alias staticBG: staticBG
            Image{
                id:listingBG
                source:viewHelper.configToolImagePath+configTool.config.kids_menu.main.bg
            }
            Image{
                id:staticBG
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1],true)].listing.background
            }
            Image{
                id:movieBG1
                opacity:0//(isSynopsis && ((selIndex%2)==1))?1:0;
                Behavior on opacity{
                    NumberAnimation{duration:opacity==0?1000:0}
                }
                anchors.bottom:parent.bottom
                sourceSize.width:core.width;
                width:core.width;
                height:(core.settings.resolutionIndex==1)?452:585;
                sourceSize.height:(settings.resolutionIndex==1)?452:585;
                source:(isSynopsis)?(listingobj.compVpathListing.count>0?viewHelper.cMediaImagePath+listingobj.compVpathListing.model.getValue(selprevIndex,'synopsisposter'):""):""
            }
            Image{
                id:movieBG2
                opacity:0//(isSynopsis && selIndex%2==0)?1:0;
                Behavior on opacity{
                    NumberAnimation{duration:1000}
                }
                sourceSize.width:core.width;
                width:core.width;
                height:(core.settings.resolutionIndex==1)?452:585;
                sourceSize.height:(settings.resolutionIndex==1)?452:585;
                anchors.bottom:parent.bottom
                source:(isSynopsis)?(listingobj.compVpathListing.visible?viewHelper.cMediaImagePath+listingobj.compVpathListing.model.getValue(selnextIndex,'synopsisposter'):''): ""
            }
            Rectangle{
                width:core.width;
                height:movieBG1.opacity==1?movieBG1.height:((movieBG2.opacity==1)?movieBG2.height:0)
                opacity:0.6;
                color:"black";
                anchors.bottom:parent.bottom
                visible:(movieBG1.opacity>0 || movieBG2.opacity>0)
            }

        }
    }
    property QtObject listingobj
    Component{
        id:compListing
        MediaList{
            upFocus:widgetList.getMainMenuRef();
        }
    }
    property QtObject synopsisobj
    Component{
        id:compSynopsis
        Loader{
            id:synopsisItem
            width: parent.width
            property alias synopsisItem: synopsisItem
            sourceComponent: blankComp
            onStatusChanged: {
                if(synopsisItem.sourceComponent==blankComp)return;
                if (synopsisItem.status == Loader.Ready){
                    synopsisItem.item.init()
                }
            }
        }
    }
    Component{
        id:baseSynopsis
        BaseSynopsis{
        }
    }
    Component{
        id:iconText
        Item{
            id:thisDelRect
            property int offset:(thisDelRect.x<PathView.view.children[0].x)?-compOffset:compOffset
            property string textColor: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?comptext_h:(thisDel.isPressed?comptext_p:comptext_n))
            width:listingsingleimgwidth
            height:listingobj.compVpathListing.height
            visible: listingobj.compVpathListing.currentIndex==index  && isSynopsis?false:true
            Image{
                id:posterBG
                anchors.centerIn: thisDel
                source: viewHelper.cMediaImagePath+poster
                asynchronous: true
                scale: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus  && !listingobj.compVpathListing.moving? 1:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?1:compScale_n
            }
            SimpleButton{
                id:thisDel
                isHighlight:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus && !listingobj.compVpathListing.moving?true:false
                highlightimg:compImage_h
                normImg: compImage_n
                pressedImg: compImage_p
                anchors.horizontalCenter:  parent.horizontalCenter
                anchors.horizontalCenterOffset: (listingobj.compVpathListing.currentIndex!=index &&  listingobj.compVpathListing.activeFocus)?(listingobj.compVpathListing.count<=2?0:offset):0
                isFocusRequired:!isSynopsis
                Behavior on opacity{
                    NumberAnimation{duration:500}
                }
                onEnteredReleased: {
                    //    listingobj.compVpathListing.currentIndex=index
                    listingobj.compVpathListing.select(index);
                }
                onIsPressedChanged: if(isPressed)isMousePressed=true
                onAdjustfocus:synopsisobj.synopsisItem.item.isSliderNavChanged=true
                scale: posterBG.scale;//listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus? 1:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?1:compScale
            }
            ViewText {
                id:titleTxt
                width: listingobj.compVpathListing.currentIndex==index?compText_w2:compText_w
                height:listingobj.compVpathListing.currentIndex==index?(lineCount>1?compTextHighlight_h:paintedHeight):(lineCount>1?compText_h:paintedHeight)
                anchors.top: parent.top
                anchors.topMargin:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compImageTM_h:compImageTM_n
                anchors.horizontalCenter: posterBG.horizontalCenter
                varText: [title,textColor,listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compTextFonsize_h:compTextFonsize_n]
                opacity: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus)?1:compTextOpacity
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                //verticalAlignment: Text.AlignVCenter
                maximumLineCount: 2
                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        thisDel.isPressed=true
                    }
                    onReleased: {
                        thisDel.isPressed=false
                        isMousePressed=true
                        thisDel.enteredReleased()
                    }
                }
            }
            ViewText{
                visible:widgetList.getCidTidMainMenu()[1]=="kids_music_listing"?true:false
                anchors.top: titleTxt.bottom
                anchors.topMargin: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compsubText_tm_h:compsubText_tm_n
                width: listingobj.compVpathListing.currentIndex==index?compsubText_w2:compsubText_w
                anchors.horizontalCenter: posterBG.horizontalCenter
                varText: [artist,textColor,listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compTextFonsize_h:compTextFonsize_n]
                opacity: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus)?1:compTextOpacity
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        thisDel.isPressed=true
                    }
                    onReleased: {
                        thisDel.isPressed=false
                        isMousePressed=true
                        thisDel.enteredReleased()
                    }
                }
            }

        }
    }
    Component{id:blankComp;Item{}}
    Component{
        id:musicAlbumSynopsis;
        AlbumSynopsis{
            width:listingScreen.width;
            configTag:core.configTool.config.kids_music_listing.tracklist
        }
    }
    Component{
        id:gameSynopsisItem
        GameSynopsis{
        }
    }
    Component{
        id:maskingIconText
        Item{
            id:thisDelRect
            property int offset:(thisDelRect.x<PathView.view.children[0].x)?-compOffset:compOffset
            property string textColor: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?comptext_h:(thisDel.isPressed?comptext_p:comptext_n))
            width:listingsingleimgwidth
            height:listingobj.compVpathListing.height
            visible: listingobj.compVpathListing.currentIndex==index  && isSynopsis?false:true
            MaskedImage{
                id:maskingImage
                anchors.centerIn:   thisDel
                maskSource:compImageMask
                source:viewHelper.cMediaImagePath+poster
                scale: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compScale_h:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?compScale_h:compScale_n
            }
            SimpleButton{
                id:thisDel
                isHighlight:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?true:false
                highlightimg:compImage_h
                normImg: compImage_n
                pressedImg: compImage_p
                isFocusRequired:!isSynopsis
                anchors.centerIn: thisDelRect
                anchors.horizontalCenterOffset: (listingobj.compVpathListing.currentIndex!=index &&  listingobj.compVpathListing.activeFocus)?offset:0
                scale: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compScale_h:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?compScale_h:compScale_n
                Behavior on opacity{
                    NumberAnimation{duration:500}
                }
                onEnteredReleased: {
                    if(listingobj.compVpathListing.flicking || listingobj.compVpathListing.moving)return
                    //   listingobj.compVpathListing.currentIndex=index
                    listingobj.compVpathListing.select(index);
                }
                onIsPressedChanged: if(isPressed)isMousePressed=true
                onAdjustfocus:synopsisobj.synopsisItem.item.isSliderNavChanged=true
            }
            ViewText {
                width: listingobj.compVpathListing.currentIndex==index?compText_w2:compText_w
                height:listingobj.compVpathListing.currentIndex==index?compTextHighlight_h:compText_h
                anchors.top: parent.top
                anchors.topMargin:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compImageTM_h:compImageTM_n
                anchors.horizontalCenter: maskingImage.horizontalCenter
                varText: [title,textColor,listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compTextFonsize_h:compTextFonsize_n]
                opacity: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus)?1:compTextOpacity
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        thisDel.isPressed=true
                    }
                    onReleased: {
                        thisDel.isPressed=false
                        isMousePressed=true
                        thisDel.enteredReleased()
                    }
                }
            }
            ViewText{
                visible:widgetList.getCidTidMainMenu()[1]=="kids_music_listing"?true:false
                anchors.top: titleTxt.bottom
                anchors.topMargin: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compsubText_tm_h:compsubText_tm_n
                width: listingobj.compVpathListing.currentIndex==index?compsubText_w2:compsubText_w
                anchors.horizontalCenter: posterBG.horizontalCenter
                varText: [artist,textColor,listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compsubText_fontsize_h:compsubText_fontsize_n]
                opacity: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus)?1:compTextOpacity
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        thisDel.isPressed=true
                    }
                    onReleased: {
                        thisDel.isPressed=false
                        isMousePressed=true
                        thisDel.enteredReleased()
                    }
                }
            }
        }
    }
    Component{
        id:compBlank
        Item{
        }
    }
    SequentialAnimation{
        id:revsynopsisAnim
        NumberAnimation { targets: [listingobj.compVpathListing]; properties: "opacity";from:1;to:0; duration: 10 }
        PauseAnimation { duration: 800 }
        NumberAnimation { targets: [listingobj.compVpathListing]; properties: "opacity";from:0;to:1; duration: 250 }
    }
    SequentialAnimation{
        id:synopsisAnim
        ScriptAction{
            script:{
                if(selIndex%2==1){
                    selprevIndex=selIndex;
                }else{
                    selnextIndex=selIndex;
                }
            }
        }
        NumberAnimation{targets: [!isSynopsis?listingobj.compVpathListing:synopsisobj,synopsisobj];properties: "opacity";from:1;to:0;duration:250}

        ScriptAction{
            script:{
                listingobj.compVpathListing.synopIndex=selIndex;
                listingobj.compVpathListing.currentIndex=selIndex
                enterReleasedforListing(selIndex);
            }
        }
        PauseAnimation { duration: 100 }
        ScriptAction{
            script:{
                isSynopsis=true
            }
        }
        NumberAnimation{targets: [!isSynopsis?listingobj.compVpathListing:synopsisobj,synopsisobj];properties: "opacity";from:0;to:1;duration: 250}
        ScriptAction{
            script:  isMousePressed=false
        }
    }
}
