import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

SmartLoader{
    id: aviancaLifeMiles;

    property int appWidth: width;
    property int appHeight: height;

    property string microAppName:(pif.getCabinClassName() === "Economy" ) ? "aviancaLifeMile_economy" : pif.getCabinClassName() === "Business" ?"aviancaLifeMile_business":(pif.getCabinClassName() === "FCRC" && pif.getLruData("XResolution")=="1024x600")?"aviancaLifeMile_economy":"aviancaLifeMile_business"
    property string currAppName:"lifemiles";
    property string currAppDataName:currAppName+"_data.xml";
    property string origBaseMicroAppDir: core.configTool.base+"microapps/";
    property string tmpCurrAppDataFileName:currAppDataName
    property string microAppDir: Qt.resolvedUrl(origBaseMicroAppDir+microAppName+"/"+currAppName+"/");
    property string tmpBaseMicroAppDir:"";
    property string baseMicroAppDir: Qt.resolvedUrl(origBaseMicroAppDir+microAppName+"/")
    property string microAppData:microAppName;
    property string childAppPath:origBaseMicroAppDir
    property string langCode : core.settings.languageISO;
    property int currIndex:0;
    property string childAppName: currAppName;
    property string selScreenID;

    Connections{
        target: configTool
        onMicroAppXMLChangedFromThemeMgr:{
            if(microappMain.childLoader.sourceComponent != "background"){
                microappMain.childLoader.item.reloadData();
            }
        }
    }

    onMicroAppDirChanged: {
        if (microAppDir.indexOf("file:")=== -1){
            microAppDir = "file:///"+microAppDir;
        }
    }

    function load (){}

    function init (){
        core.log("AviancaLifeMiles | init ");
        widgetList.showHeader(false);
        widgetList.showMainMenu(false);
        microappMain.selectChild();
        microappMain.childLoader.item.setInitialFocus();
    }
    function reload (){}
    function clearOnExit (){
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
           pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":""/*,"showContOnKarma":viewHelper.showContOnKarma*/});
        }
        microappMain.menuContainer.model = null
        microappMain.childLoader.sourceComponent= null;
    }
    function actionOnExit(param) {
        if(param){
            viewController.hideScreenPopup();
            viewHelper.keepSynopsisOpen=true;
            viewController.loadPrev();
        }else{
            viewController.hideScreenPopup();
        }
    }
    function backBtnPressed(){
        viewHelper.keepSynopsisOpen=true;
        viewController.loadPrev();
    }
    function languageUpdate(){
        microappMain.appcategoryData.reload()
        microappMain.childLoader.item.reloadData();
        microappMain.childLoader.item.setInitialFocus();
        viewController.hideScreenPopup()
    }

    MicroApp_LifeMiles{
        id:microappMain
    }
    Component{
        id:appDiscover;
        LifeMilesDiscover{ }
    }
    Component{
        id:appEarn;
        LifeMilesEarn { }
    }
    Component{
        id:appElite;
        LifeMilesElite{ }
    }
    Component {
        id:appEnjoy;
        LifeMilesEnjoy { }
    }
}
