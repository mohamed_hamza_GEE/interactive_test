// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:usbListScreen
    /******************************************************************************************************************************/
    property variant configTag:configTool.config.usb.screen;
    property variant labelTag:core.configTool.language.music;
    property variant userModel
    property variant  filterArray: []
    property string filterType:''
    property string firstMp3IndexPath:''
    property int firstMp3Index:0
    property int selectedEpi: -1
    property bool trackPlaying:false
    property bool trackInitialize: false;
    property bool isModelUpdated: false
    /*****************************************************************************************************************************/
    Connections{
        target:pif.usbMp3
        onSigMidPlayBegan:{
            firstMp3Index=playIndex;viewHelper.trackStop=false;viewHelper.usbTrackPlayed=true
            if(pif.usb.isFolderInUsb(viewHelper.usbPathFileName)==false){trackPlaying=true;}
//            core.debug("_isPlayingMP3 pif.usbMp3.getMediaState() : "+pif.usbMp3.getMediaState());
//            core.debug("_isPlayingMP3 pif.usbMp3.cMEDIA_STOP : "+pif.usbMp3.cMEDIA_STOP);
//            core.debug("_isPlayingMP3 pif.getLSAmediaSource() : "+pif.getLSAmediaSource());
//            core.debug("_isPlayingMP3 pif.usbMp3.mp3Info.source : "+pif.usbMp3.mp3Info.source);
//            core.debug("_isPlayingMP3 filePath : "+filePath);


        }
        onSigMidPlayStopped:{
            viewHelper.usbTrackPlayed=false;/*pif.usbMp3.stop()*/
            if(pif.usb.isFolderInUsb(viewHelper.usbPathFileName)==false){trackPlaying=false;}
        }
        onSigMediaPaused:{
            if(pif.usb.isFolderInUsb(viewHelper.usbPathFileName)==false){trackPlaying=false;}
        }
    }
    /*****************************************************************************************************************************/
    function load(){
        push(compSubCategory,"subCatobj")
        push(compPanelObj,"panel");
        push(compButtonPanel,"buttonPanel")
        push(compUsbScroll,"usbScroll");
        push(compUsbListView,"usbListView");
        push(compMusicControl,"musicControl");
    }
    function init(){
        core.debug("USBListing.qml | Init ")
        viewHelper.usbIndex=current.template_id;
        if(current.template_id=="usb_doc"){viewHelper.setHelpTemplate("usb_doc_listing");}
        else if(current.template_id=="usb_music") {viewHelper.setHelpTemplate("usb_mus_listing");}
        widgetList.showHeader(true);widgetList.showMainMenu(true);
        getSubCategoryMenu()
    }
    function reload(path){
        getListing(path)
    }

    function checkFileType(fileType,template_id){
        return (((filterArray[template_id].replace(/,/g,")") + ")").replace(/\./g,"(").replace(/\*/g,"")).indexOf("("+fileType+")") != -1);
    }

    function isMusic(fileType){return checkFileType(fileType.toLowerCase(),"usb_music"); }

    function _isPlayingMP3(filePath){

//        core.debug("_isPlayingMP3 pif.usbMp3.getMediaState() : "+pif.usbMp3.getMediaState());
//        core.debug("_isPlayingMP3 pif.usbMp3.cMEDIA_STOP : "+pif.usbMp3.cMEDIA_STOP);
//        core.debug("_isPlayingMP3 pif.getLSAmediaSource() : "+pif.getLSAmediaSource());
//        core.debug("_isPlayingMP3 pif.usbMp3.mp3Info.source : "+pif.usbMp3.mp3Info.source);
//        core.debug("_isPlayingMP3 filePath : "+filePath);
        return (pif.usbMp3.getMediaState() != pif.usbMp3.cMEDIA_STOP && pif.getLSAmediaSource()=="usb" && pif.usbMp3.mp3Info.source == filePath);
    }

    function getListing(path){
        core.debug("USBListing.qml | reload ");
        viewHelper.usbIndex=current.template_id
        if(path){
            viewHelper.filePathName = path
        }
        if(current.template_id=="usb_music"){

            filterType='*.mp3'
            viewHelper.setHelpTemplate("usb_mus_listing")
            assignUserModel(viewHelper.filePathName,'*.mp3')
        }else{

            filterType='*.pdf'
            viewHelper.setHelpTemplate("usb_doc_listing")
            assignUserModel(viewHelper.filePathName,'*.pdf')
        }
    }
    function assignUserModel(path,filter){
        usbListScreen.userModel  = viewHelper.blankModel
        core.log("assignUserModel | pif.usb.getMountPath() : "+pif.usb.getMountPath()+"   filter : "+filter)
        pif.usb.readFolder(path,filter);
        usbListScreen.userModel = pif.usb.browserModel
        usbListView.setModel(viewHelper.blankModel)
        usbListView.setModel(pif.usb.browserModel)
        usbListView.getListViewRef().forceActiveFocus()

    }
    function usbSelect(strType,path,name,index){
        if(pif.usb.isFolderInUsb(path)){
            viewHelper.filePathName = path;
            usbListScreen.assignUserModel(viewHelper.filePathName,'*.'+strType);
        } else if (strType == 'png' || strType == 'jpg' || strType == 'jpeg'){
            viewHelper.imageSequencer.populateSlideShowModel(pif.usb.browserModel);
        } else if (strType == 'mp3'){
            usbListScreen.playMp3(path,index);

           // pif.launchApp.launchPdf(pif.usb.browserModel.getValue(listing.currentIndex,'pathfilename'),"",true);


        } else if (strType == 'pdf'){
            if(pif.android){
                pif.launchApp.launchPdf(path,"",true);
            }else {
                pif.launchApp.launchApp("",2,"USBPDF",core.coreHelper.escapeSpaceInString(path),true);
            }
        }
        else core.log("USBListing.qml || Unknown File Format ");
    }
    function playMp3(curPath,curIndex){
        viewHelper.usbTrackPlayed = true;viewHelper.trackStop=false
        if(pif.usbMp3.playedDirectory != pif.usb.getFolderPath()){
            var struct = {"mediaType":"mp3","cid":current.node_id,"shuffle": false,"repeatType":0,"elapseTimeFormat": 6,"playIndex": pif.usb.browserModel.getValue(curIndex,"pathfilename"),"playType": 2};
            pif.usbMp3.playByModel(viewHelper.filePathName,struct);
        } else pif.usbMp3.setPlayingFileName(curPath);
    }
    function clearOnExit(){
        core.debug("USBListing.qml | clearOnExit ")
    }
    function subCategoryReady(dModel){
        subCatobj.compVpathSubCategory.model=viewHelper.blankModel
        subCatobj.compVpathSubCategory.model=dModel;viewHelper.subCategoryMenuIndex=core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id",viewHelper.usbIndex)
        subCatobj.compVpathSubCategory.currentIndex=viewHelper.subCategoryMenuIndex
        if(viewHelper.filePathName==""){
            getListing(pif.usb.getMountPath()[0]);
        }else{
            getListing(viewHelper.filePathName);
        }
        if(usbListView.getListViewRef().model.count==0){usbListView.visible=false;buttonPanel.backBtnRef().forceActiveFocus()}
        else{usbListView.visible=true;usbListView.getListViewRef().forceActiveFocus()}
    }
    function languageUpdate(){
        isModelUpdated=false
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel,subCatobj.compVpathSubCategory.model],[donothing]);
    }
    function donothing(){}
    function getSubCategoryMenu(){
        var temp=core.dataController.mediaServices.getCategoryDataByTagName("usb");
        var cid=temp.cid
        var tid=temp.tid
        dataController.getNextMenu([cid,tid],subCategoryReady)
    }
    function enterReleased(index){
        if( viewHelper.subCategoryMenuIndex==index)return;
        subCatobj.compVpathSubCategory.currentIndex=index
        viewHelper.subCategoryMenuIndex=index
        viewHelper.jumToUsbScreen(subCatobj.compVpathSubCategory.model.getValue(index,"category_attr_template_id"))
    }
    function pathManipulation(str){return str.substr(0);}
    function focusFromMainMenu(){
        subCatobj.compVpathSubCategory.forceActiveFocus()
    }
    function focusFromSubCat(){

        buttonPanel.ejectBtnRef().forceActiveFocus()

    }
    function backBtnPressed(){
        viewHelper.homeAction()
        viewHelper.usbBackPressed();
    }
    function usbBackBtnPressed(){
//        buttonPanel.noFilesTxtRef().visible=false;
        if(viewHelper.filePathName!=pif.usb.getMountPath()[0]){
            var temp = viewHelper.filePathName.lastIndexOf("/");
            viewHelper.filePathName=viewHelper.filePathName.substring(0,temp);
            if(temp==0){
                viewHelper.filePathName=pif.usb.getMountPath()[0];
            }
            pif.usb.readFolder(viewHelper.filePathName,usbListScreen.filterType);
            usbListView.visible=true;usbListView.getListViewRef().currentIndex=0;usbListView.getListViewRef().forceActiveFocus()
        }
    }

    /**************************************************************************************/
    Item{
        id:posterobj
        anchors.fill: parent
        property alias staticBG: staticBG
        Image{
            id:listingBG
            source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
        }
        Image{
            id:staticBG
            anchors.bottom:parent.bottom
            source:viewHelper.configToolImagePath+configTool.config.usb.screen.usb_background
        }
    }


    property QtObject subCatobj
    Component{
        id:compSubCategory
        SubCategory{}
    }
    property QtObject buttonPanel
    Component{
        id:compButtonPanel
        Item{
            anchors.fill: panel
            function backBtnRef(){return backBtn}
            function ejectBtnRef(){return ejectBtn}
            function noFilesTxtRef(){return noFilesText}
            SimpleNavBrdrBtn{
                id:backBtn
                normalImg: viewHelper.configToolImagePath+configTag["back_n"];
                highlightImg:viewHelper.configToolImagePath+configTag["back_h"];
                pressedImg:viewHelper.configToolImagePath+configTag["back_p"];
                anchors.bottom:parent.top;
                anchors.bottomMargin:qsTranslate('',"usb_back_bm")
                anchors.left:parent.left;
                anchors.leftMargin:qsTranslate('',"usb_back_lm")
                isDisable:viewHelper.filePathName==pif.usb.getMountPath()[0]
                onIsDisableChanged: {if(isDisable){usbListView.getListViewRef().forceActiveFocus()}}
                nav: ["","","",""]
                onNoItemFound: {
                    if(dir=="up"){subCatobj.compVpathSubCategory.forceActiveFocus()}
                    else if(dir=="down"){if(!noFilesText.visible){usbListView.getListViewRef().forceActiveFocus()}}
                    else if(dir=="right"){ejectBtn.forceActiveFocus()}
                }
                onEnteredReleased:{
                    usbBackBtnPressed()
                }
            }
            ViewText{
                id:termsDescObj
                anchors.verticalCenter: backBtn.verticalCenter
                anchors.left:backBtn.right
                width:qsTranslate('',"usb_back_w");
                varText:[pathManipulation(pif.usb.getFolderPath()),configTag["path_text_color"],qsTranslate('',"usb_eject_fontsize")]
                wrapMode: Text.Wrap
                elide:Text.ElideRight
            }
            ViewText{
                id:noFilesText
                anchors.centerIn: parent
                z:5
                width:qsTranslate('',"usb_thumbview_w");
                horizontalAlignment: Text.AlignHCenter
                varText:[configTool.language.usb.no_files,configTag["description1_color"],qsTranslate('',"usb_thumbview_fontsize")]
                visible: usbListScreen.userModel.count <= 0
            }
            SimpleNavBrdrBtn{
                id:ejectBtn
                normalImg: viewHelper.configToolImagePath+configTag["btn_n"];
                highlightImg:viewHelper.configToolImagePath+configTag["btn_h"];
                pressedImg:viewHelper.configToolImagePath+configTag["btn_p"];
                height:qsTranslate('',"usb_eject_h");
                width:qsTranslate('',"usb_eject_w");
                btnTextWidth:parseInt(qsTranslate('',"usb_eject_textw"),10);
                buttonText: [configTool.language.usb.eject,(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','usb_eject_fontsize')]
                anchors.bottom:parent.top;
                anchors.bottomMargin:qsTranslate('',"usb_eject_bm")
                anchors.right:parent.right;
                anchors.rightMargin:qsTranslate('',"usb_eject_rm")
                normalBorder:[qsTranslate('',"usb_eject_margin")]
                highlightBorder:[qsTranslate('',"usb_eject_margin")]
                elide:Text.ElideRight
                nav: ["","","",""]
                onNoItemFound: {
                    if(dir=="up"){subCatobj.compVpathSubCategory.forceActiveFocus()}
                    if(dir=="down"){if(!noFilesText.visible && usbScroll.focus){usbScroll.forceActiveFocus()}else if(!noFilesText.visible ){usbListView.forceActiveFocus()}else{ return}}
                    if(dir=="left"){if(backBtn.isDisable==false)backBtn.forceActiveFocus()}

                }
                onEnteredReleased:{viewHelper.homeAction();pif.usb.usbChangeDetected(pif.usb.cUSB_OUT,pif.usb.getMountPath()[0]);}
            }
        }
    }
    property QtObject panel
    Component{
        id:compPanelObj
        Image{
            id:panelBG
            anchors.horizontalCenter: posterobj.horizontalCenter
            anchors.top:posterobj.top;
            anchors.topMargin: qsTranslate('','usb_panel_tm')
            source:(current.template_id=="usb_doc")?viewHelper.configToolImagePath+configTool.config.usb.screen.usb_panel:viewHelper.configToolImagePath+configTool.config.usb.screen.usb_music_panel
        }
    }
    property QtObject usbListView
    Component{
        id:compUsbListView;
        FocusScope{
            id:usbListItem
            anchors.fill:panel
            function setModel(dmodel){
                usbList.model=dmodel;
            }
            function getListViewRef(){return usbList}
            function getScrollerRef(){return usbScroll}
            function formatSize(bytes){
                bytes=bytes*1;
                var GB=1073741824;var MB=1048576;var KB=1024;
                var suffix="";var div;var size;
                if (bytes >= GB){
                    div=GB; suffix=" GB";
                }else if(bytes >= MB){
                    div=MB;suffix=" MB";
                }else{
                    div=KB;suffix=" KB";
                }
                size=(Math.round(bytes/div*100000)/100000);
                size=(size.toFixed(2)) + suffix;
                return size;
            }
            function formatDateString(unixTime){
                var d1 = new Date(unixTime*1000);
                var day = (d1.getDate()>9)?d1.getDate():"0" + d1.getDate();
                var mth = ((d1.getMonth()+1)>9)?(d1.getMonth()+1):"0" +(d1.getMonth()+1);
                var year = d1.getFullYear();
                return day+"/"+mth+"/"+year;
            }
            function performAction(){
                viewHelper.folderPathLocally = pif.usb.getFolderPath();
                if(pif.usb.isFolderInUsb(usbList.model.getValue(usbList.currentIndex,"pathfilename"))){
                    viewHelper.filePathName =usbList.model.getValue(usbList.currentIndex,"pathfilename");
                    usbListScreen.assignUserModel(viewHelper.filePathName,usbListScreen.filterType);
                    if(usbList.model.count==0){
//                        buttonPanel.noFilesTxtRef().visible=true;
                        usbListView.visible=false;
                        buttonPanel.backBtnRef().forceActiveFocus()
                    }
                    else{usbListView.visible=true;getListViewRef().forceActiveFocus()}
                }
                else{
                    selectedEpi=usbList.currentIndex
                    usbListScreen.usbSelect(usbList.model.getValue(usbList.currentIndex,"filetype"),usbList.model.getValue(usbList.currentIndex,"pathfilename"),usbList.model.getValue(usbList.currentIndex,"filenameonly"),usbList.currentIndex);
                }
            }
            function setUsbListDelegate(){
                if(current.template_id=="usb_doc"){
                    usbList.visible=true
                    return usbDocDelegate
                }else if(current.template_id=="usb_music"){
                    usbList.visible=true
                    return usbMusicDelegate
                }else
                    usbList.visible=false
            }
            ViewText{
                id:fileNameTxt
                anchors.top: parent.top;
                anchors.left:parent.left
                anchors.leftMargin:qsTranslate('',"usb_file_lm")
                anchors.topMargin:qsTranslate('',"usb_file_tm")
                width:qsTranslate('',"usb_file_w");
                varText:[configTool.language.usb.filename,configTag["list_title_color"],qsTranslate('',"usb_file_fontsize")]
            }
            ViewText{
                id:dateOrArtistTxt
                anchors.verticalCenter: fileNameTxt.verticalCenter
                anchors.left:fileNameTxt.right
                anchors.leftMargin:qsTranslate('',"usb_date_gap")
                width:qsTranslate('',"usb_date_w");
                varText:[(current.template_id=="usb_doc")?configTool.language.usb.date:configTool.language.usb.artist,configTag["list_title_color"],qsTranslate('',"usb_file_fontsize")]
            }
            ViewText{
                id:sizeOrTimeTxt
                anchors.verticalCenter: fileNameTxt.verticalCenter
                anchors.left:dateOrArtistTxt.right
                anchors.leftMargin:qsTranslate('',"usb_size_gap")
                width:qsTranslate('',"usb_size_w");
                varText:[(current.template_id=="usb_doc")?configTool.language.usb.size:configTool.language.usb.time,configTag["list_title_color"],qsTranslate('',"usb_file_fontsize")]
            }
            VerticalView{
                id:usbList
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('',"usb_docview_tm");
                anchors.left:parent.left
                anchors.leftMargin:qsTranslate('',"usb_docview_lm")
                width:qsTranslate('',"usb_docview_w");
                height:(current.template_id=="usb_doc")?qsTranslate('',"usb_docview_h"):qsTranslate('','usb_docview_h1');
                focus:true;
                Rectangle{
                    width:parent.width;
                    height:qsTranslate('',"usb_docview_high_bm")
                    color:configTag["description1_color"]
                }
                Keys.onRightPressed: {
                    if(usbScroll.visible)
                        usbScroll.forceActiveFocus()
                }
                Keys.onUpPressed: {
                    if(usbList.currentIndex==0) {
                        if(buttonPanel.backBtnRef().isDisable!=true){
                            buttonPanel.backBtnRef().forceActiveFocus()
                        } else{buttonPanel.ejectBtnRef().forceActiveFocus()}}
                    else{decrementCurrentIndex()}
                }
                Keys.onDownPressed: {
                    if(current.template_id=="usb_music"){
                        if(usbList.currentIndex<usbList.model.count-1){
                            incrementCurrentIndex()
                        }
                        else if(!viewHelper.trackStop){musicControl.forceActiveFocus()}
                    }
                    else incrementCurrentIndex()
                }
                Keys.onReleased: {
                    if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                        usbListItem.performAction()
                    }
                }
                delegate:usbListItem.setUsbListDelegate()
                onCountChanged:  {
                    if(count<=0)
                        buttonPanel.noFilesTxtRef().visible=true;
                    else buttonPanel.noFilesTxtRef().visible=false;
                }
            }
            BorderImage{
                source:viewHelper.configToolImagePath+configTag["fading_image"];
                anchors.bottom:usbList.bottom;
                anchors.horizontalCenter: usbList.horizontalCenter;
                border.left:qsTranslate('','common_scrollfade_margin')
                border.top:qsTranslate('','common_scrollfade_margin')
                border.right:qsTranslate('','common_scrollfade_margin')
                border.bottom:qsTranslate('','common_scrollfade_margin')
                width:(usbList.width+parseInt(qsTranslate('','common_scrollfade_extra_w'),10));
                visible:usbScroll.visible?(usbScroll.downArrow.isDisable?false:true):false
            }
        }
    }
    property QtObject musicControl
    Component{
        id:compMusicControl
        MusicControls{
            id:musicControl
            height:qsTranslate('','usb_Controller_cont_h');
            width:qsTranslate('','usb_Controller_cont_w');
            anchors.bottom:panel.bottom;
            anchors.horizontalCenter:panel.horizontalCenter;
            upFocus:[usbListView,usbScroll]
            visible:(current.template_id=="usb_music" && !buttonPanel.noFilesTxtRef().visible)
            configVal:core.configTool.config.usb.screen;
        }
    }
    property QtObject usbScroll
    Component{
        id:compUsbScroll
        Scroller{
            id:usbScroll
            targetData:usbListView.getListViewRef()
            sliderbg:[qsTranslate('','usb_music_slider_base_margin'),qsTranslate('','usb_music_slider_w'),(current.template_id=="usb_doc")?qsTranslate('','usb_doc_slider_base_h'):qsTranslate('','usb_music_slider_base_h'),configTag["scroll_base"]]
            scrollerDot:configTag["scroll_button"]
            upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
            downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
            anchors.right:panel.right;
            anchors.rightMargin:qsTranslate('','usb_doc_slider_rm');
            anchors.top:panel.top
            anchors.topMargin:qsTranslate('','usb_doc_slider_tm')
            height: (current.template_id=="usb_doc")?qsTranslate('','usb_doc_slider_h'):qsTranslate('','usb_music_slider_h');
            width: qsTranslate('','usb_music_slider_w');
            visible:(buttonPanel.noFilesTxtRef().visible)?false:((usbListView.getListViewRef().contentHeight>usbListView.getListViewRef().height)?true:false)
            onSliderNavigation:{
                if(direction=="down"){if(current.template_id=="usb_music"){musicControl.forceActiveFocus()}else return;}
                else if(direction=="up") {buttonPanel.ejectBtnRef().forceActiveFocus()}
                else if(direction=="right"){return;}
                else if(direction=="left"){usbListView.getListViewRef().forceActiveFocus()}
            }
            onArrowPressed: {
                usbListView.getListViewRef().currentIndex=usbListView.getListViewRef().indexAt(usbListView.getListViewRef().contentX,usbListView.getListViewRef().contentY+20)
            }
        }
    }
    Component{
        id:iconTextCenter
        Rectangle{
            id:thisDel
            property int offset:qsTranslate('','mainmenu_submenu_margin')
            property string textColor: configTool.config.menu.submenu.text_color_n
            width: (imageHig.btnText.paintedWidth+offset)<qsTranslate('','mainmenu_submenu_w')/5?qsTranslate('','mainmenu_submenu_w')/5:(imageHig.btnText.paintedWidth+offset)
            height:qsTranslate('','mainmenu_submenu_h')
            color: "transparent"
            BorderImage{
                id:imageSel
                width: imageHig.btnText.paintedWidth+offset
                height:parent.height
                visible: !thisDel.activeFocus
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: thisDel.bottom
                source:viewHelper.subCategoryMenuIndex==index? viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_p:""
                horizontalTileMode:  BorderImage.Stretch
                border.left: sourceSize.width/2-1
                border.right:sourceSize.width/2-1
                border.top: sourceSize.width/2-1
                border.bottom: sourceSize.width/2-1
                Image{
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    source: viewHelper.subCategoryMenuIndex==index? viewHelper.configToolImagePath+configTool.config.menu.submenu.select_icon:""
                }
            }
            SimpleNavBrdrBtn{
                id:imageHig
                width: imageHig.btnText.paintedWidth+offset
                height: parent.height
                normalImg: viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_n
                anchors.horizontalCenter: parent.horizontalCenter
                highlightImg:subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_h:""
                pressedImg: viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_p
                buttonText: [title,"white",qsTranslate('','mainmenu_submenu_fontsize')]
                btnText.horizontalAlignment: Text.AlignHCenter
                btnText.verticalAlignment: Text.AlignVCenter
                isHighlight: subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?true:false
                buttonTextopacity:viewHelper.subCategoryMenuIndex==index || (subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus)?1:0.6
                onEnteredReleased:  enterReleased(index);
            }
        }
    }
    Component{
        id:usbDocDelegate
        Item{
            id:thisDel
            width:qsTranslate('',"usb_docview_cell_w");
            height:qsTranslate('',"usb_docview_cell_h");
            property bool isFolder:(pif.usb.isFolderInUsb(pathfilename))?true:false;
            Image{
                id:highalbum
                source:viewHelper.configToolImagePath+configTag["listing_highlight"];
                anchors.bottom: parent.bottom;
                anchors.bottomMargin:qsTranslate('','usb_docview_high_bm')
                height:qsTranslate('','usb_docview_high_h');
                width:qsTranslate('','usb_docview_high_w');
                opacity:(usbList.currentIndex==index && usbList.activeFocus)?1:0
            }
            Rectangle{
                width:parent.width;
                height:qsTranslate('',"connectgate_divider_h")
                color:configTag["description1_color"]
                anchors.bottom:parent.bottom;
            }
            Image{
                id:folderIcon
                source:viewHelper.configToolImagePath+configTag["folder_icon"]
                scale: qsTranslate('','usb_docview_title_scale')
                visible: thisDel.isFolder
                anchors.left:parent.left
                anchors.leftMargin: -qsTranslate('',"usb_docview_title_text_lm");
                anchors.verticalCenter: highalbum.verticalCenter
                anchors.verticalCenterOffset:- qsTranslate('','usb_docview_title_text_tm')
            }
            ViewText{
                id:list_name
                anchors.left:folderIcon.right
                anchors.verticalCenter: folderIcon.verticalCenter
                anchors.leftMargin: -qsTranslate('',"usb_docview_title_text_lm");
                width:qsTranslate('',"usb_docview_title_w");
                height:qsTranslate('',"usb_docview_title_h");
                varText:[filenameonly,configTag["description1_color"],qsTranslate('',"usb_docview_title_fontsize")]
                elide: Text.ElideRight
            }
            ViewText{
                id:list_Date
                anchors.verticalCenter: folderIcon.verticalCenter
                anchors.left:list_name.right
                anchors.leftMargin: qsTranslate('',"usb_docview_date_gap");
                width:qsTranslate('',"usb_docview_date_w");
                height:qsTranslate('',"usb_docview_date_h");
                varText:[(thisDel.isFolder)?'':usbListItem.formatDateString(lastmodified),configTag["description1_color"],qsTranslate('',"usb_docview_date_fontsize")]
            }
            ViewText{
                id:list_Size
                anchors.verticalCenter: folderIcon.verticalCenter
                anchors.left:list_Date.right
                anchors.leftMargin: qsTranslate('',"usb_docview_size_gap");
                width:qsTranslate('',"usb_docview_size_w");
                height:qsTranslate('',"usb_docview_size_h");
                varText:[(thisDel.isFolder)?'':usbListItem.formatSize(filesize),configTag["description1_color"],qsTranslate('',"usb_docview_size_fontsize")]
            }
            MouseArea{
                anchors.fill:parent;
                onClicked:{
                    usbList.currentIndex=index;
                    usbListItem.performAction()
                }
            }
        }
    }
    Component{
        id:usbMusicDelegate
        Item{
            id:thisDel
            width:qsTranslate('',"usb_docview_cell_w");
            height:qsTranslate('',"usb_docview_cell_h");
            property bool isFolder:(pif.usb.isFolderInUsb(pathfilename))?true:false;
            property bool isPlayingMP3:(isMusic(filetype) && _isPlayingMP3(pathfilename));



            Image{
                id:highalbum
                source:viewHelper.configToolImagePath+configTag["listing_highlight"];
                anchors.bottom: parent.bottom;
                anchors.bottomMargin:qsTranslate('','usb_docview_high_bm')
                height:qsTranslate('','usb_docview_high_h');
                width:qsTranslate('','usb_docview_high_w');
                opacity:(usbList.currentIndex==index && usbList.activeFocus)?1:0
            }
            Rectangle{
                width:parent.width;
                height:qsTranslate('',"connectgate_divider_h")
                color:configTag["description1_color"]
                anchors.bottom:parent.bottom;
            }
            Image{
                id:folderIcon
                source:viewHelper.configToolImagePath+configTag["folder_icon"]
                scale: qsTranslate('','usb_docview_title_scale')
                visible: thisDel.isFolder
                anchors.left:parent.left
                anchors.leftMargin: -qsTranslate('',"usb_docview_title_text_lm");
                anchors.verticalCenter: highalbum.verticalCenter
                anchors.verticalCenterOffset:- qsTranslate('','usb_docview_title_text_tm')
            }
            Image{
                id:playIcon
                source:viewHelper.configToolImagePath+configTag["now_playing_icon"]
                //visible:(thisDel.isPlayingMP3)
                visible:(pif.usbMp3.getMediaState() != pif.usbMp3.cMEDIA_STOP
                         && pif.getLSAmediaSource()=="usb"
                         && pif.usbMp3.mp3Info.source == pathfilename)
                anchors.right:list_Time.left
                anchors.rightMargin:qsTranslate('','usb_nowplay_rm')
                anchors.verticalCenter: highalbum.verticalCenter
                anchors.verticalCenterOffset:- qsTranslate('','usb_docview_title_text_tm')
            }
            ViewText{
                id:list_name
                anchors.left:folderIcon.right
                anchors.verticalCenter: folderIcon.verticalCenter
                anchors.leftMargin: -qsTranslate('',"usb_docview_title_text_lm");
                width:qsTranslate('',"usb_docview_title_w");
                height:qsTranslate('',"usb_docview_title_h");
                varText:[filenameonly,configTag["description1_color"],qsTranslate('',"usb_docview_title_fontsize")]
                elide: Text.ElideRight
            }
            ViewText{
                id:list_Artist
                anchors.verticalCenter: folderIcon.verticalCenter
                anchors.left:list_name.right
                anchors.leftMargin: qsTranslate('',"usb_docview_date_gap");
                width:qsTranslate('',"usb_docview_date_w");
                height:qsTranslate('',"usb_docview_date_h");
                varText:[(thisDel.isFolder)?'':mp3artist,configTag["description1_color"],qsTranslate('',"usb_docview_date_fontsize")]
                elide: Text.ElideRight
            }
            ViewText{
                id:list_Time
                anchors.verticalCenter: folderIcon.verticalCenter
                anchors.left:list_Artist.right
                anchors.leftMargin: qsTranslate('',"usb_docview_size_gap");
                width:qsTranslate('',"usb_docview_size_w");
                height:qsTranslate('',"usb_docview_size_h");
                varText:[(thisDel.isFolder)?'':text,configTag["description1_color"],qsTranslate('',"usb_docview_size_fontsize")]
                Connections {
                    target:pif.usbMp3.mp3Info
                    onDurationCompleted:{
                        if((filetype.toLowerCase()=="mp3") && (pathfilename == source)) list_Time.text = core.coreHelper.convertSecToMSLeadZero(duration)
                    }
                }
            }
            MouseArea{
                anchors.fill:parent;
                onClicked:{
                    usbList.currentIndex=index;
                    selectedEpi=index
                    usbListItem.performAction()


                }
            }
            Component.onCompleted:{
                if(filetype.toLowerCase()=="mp3"){
                    pif.usbMp3.mp3Info.requestDuration(pathfilename);
                }
            }
        }
    }
    /**************************************************************************************************************************/
}
