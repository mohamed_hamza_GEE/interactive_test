import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader{
    id:mainmenuScreen
    property bool animReq: false
    property bool isModelUpdated: false
    property variant cats:[];
    property bool isReloaded:false
    /**************************************************************************************************************************/
    onActiveFocusChanged: if(activeFocus){
                              core.debug("activeFocus : "+activeFocus)
                              delayFocus.restart();
                              //widgetList.getMainMenuRef().mainmenuPathView.forceActiveFocus()
                          }
    
    Timer{
        id:delayFocus;
        interval:500;
        onTriggered:{
            if(activeFocus)
                widgetList.getMainMenuRef().mainmenuPathView.forceActiveFocus();
        }
    }
    
    Connections{
        target: mainmenuScreen.visible && viewHelper.directJumpFromKids==""?viewData:null
        onMainMenuModelReady:{
            setParameters()
        }
    }
    /**************************************************************************************************************************/
    function load(){

    }
    function init(){
        core.info("MainMenu | init")
        if(viewHelper.seatchat_enabled){
            if(!pif.seatChat.getAppActiveState())
                pif.seatChat.setAppActive();
        }else{}
        viewHelper.setHelpTemplate("main_menu");
        viewHelper.isCurtainFirstTime=true
        widgetList.showHeader(true);
        widgetList.showMainMenu(true);
        animReq=viewHelper.isFromWelComeScreen?true:false
        if(viewHelper.isFromWelComeScreen && !isReloaded)widgetList.getMainMenuRef().mainmenuPathView.model=viewHelper.blankModel
        core.debug(" current.template_id : "+current.template_id)
        if(current.template_id=="kids"){
            cats=["kids_games_listing,kids_music_listing,kids_movies_listing,kids_tv_listing"]
           // viewHelper.mainMenuIndex=2;
        }else{
            cats=["shopping,games,music,movies,tv,discover,usb"];
           // viewHelper.mainMenuIndex=2;
        }
        core.debug("viewHelper.mainMenuIndex "+viewHelper.mainMenuIndex)
        getMenu()

    }
    function clearOnExit(){
        viewHelper.isFromWelComeScreen=false
        isReloaded=false
        core.debug("MainMenu | clearOnExit")
    }
    function reload(){
        // widgetList.getMainMenuRef().mainmenuPathView.model=viewHelper.blankModel
        isReloaded=true
        init()
    }
    function getMenu(){
        if(animReq) dataController.getCategoryByTemplateId(cats,viewData.mainMenuReady)
        else setParameters()
    }
    function setParameters(){
        if(((animReq || viewHelper.isDirectMainMenu) && !isReloaded) || isReloaded)
            widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm'),0,viewHelper.isDirectMainMenu?false:animReq,viewHelper.mainMenuIndex,isModelUpdated)
        else if(!isReloaded)
            widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,animReq,viewHelper.mainMenuIndex,isModelUpdated)
        viewHelper.isDirectMainMenu=false
        if(!isModelUpdated)widgetList.focustTomainMenu()
        isModelUpdated=false
        if(isReloaded){
            widgetList.setMainMenuCurrentIndex(viewHelper.mainMenuIndex)
            isReloaded=false
            widgetList.getMainMenuRef().mainmenuPathView.model=viewData.mainMenuModel
        }
        if(viewData.mainMenuModel.count<=0)widgetList.focusToHeader()
    }
    function getSubCategoryMenu(model,index,param){
        viewController.loadNext(model,index);
    }
    function languageUpdate(){
        animReq=true
        isModelUpdated=true
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel],[viewData.mainMenuReady]);
    }
    function backBtnPressed(){
        viewHelper.isBackFromMainMenu=true
        viewController.jumpTo(0,"WELC");
    }


}
