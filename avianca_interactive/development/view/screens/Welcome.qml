// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import Qt 4.7 
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:welcomeScreen
    property int fontSize1: qsTranslate('','welcome_language_fontsize_h')
    property int fontSize2: qsTranslate('','welcome_language_fontsize_n')
    property int timeControl:300
    property double pathOpacity:0.7//parseInt(qsTranslate('','welcome_language_opacity'),10) //customer requested 0.7 to make it better
    property string rootCid: dataController.getRootNode()[0]
    property string rootTid: dataController.getRootNode()[1]
    property variant configTag:configTool.config.global;
    property int adultIndex;
    property int kidsIndex;
    property bool  isContinuouslyPressed:false
    /**************************************************************************************************************************/
    Connections{
        target:welcomeScreen.visible?viewData:null
        onHomeModelReady:{
            languageChanged()
        }
    }
    Connections{
        target:viewHelper
        onJumpToWelcome:{

            core.debug("Welcome.qml | onJumpToWelcome called")
            var index;
            for(var i=0;i<viewData.homeModel.count;i++){
                if(viewData.homeModel.getValue(i,"category_attr_template_id")==section)
                    index=i;
            }
            var langIndex;
            for(var j=0;j<dataController.getIntLanguageModel().count;j++){
                //if(dataController.getIntLanguageModel().get(j).ISO639==lang)
                if(dataController.getIntLanguageModel().get(j).ISO639.toUpperCase()==lang.toUpperCase())
                    langIndex=j;
            }
            var lid=dataController.getLidByLanguageISO(lang)
            langobj.vmenu.currentIndex=langIndex
            viewHelper.isDirectMainMenu=true
            viewHelper.isFromWelComeScreen=true;
            displayNextScreen(index)
            var locale=langobj.vmenu.model.get(langobj.vmenu.currentIndex).language_locale;
            core.setInteractiveLanguage(lid,lang,false,locale);
        }
    }
    /**************************************************************************************************************************/
    function load(){
        push(compLogo,"logoobj")
        push(compEntOn,"entONobj")
        push(compLanguage,"langobj")
        push(compSection,"sectionobj")
    }
    function init(){
        pif.paxus.interactiveScreenLog("WELC");
        core.debug("Welcome.qml | init")
        widgetList.showHeader(false);
        widgetList.showMainMenu(false);
        if(viewHelper.isBackFromMainMenu) langobj.vmenu.currentIndex=viewHelper.welComeIndex
        else  viewHelper.welComeIndex=langobj.vmenu.currentIndex
        setLanguage();
        if(viewHelper.isBackFromMainMenu){
            widgetList.getMainMenuRef().revwelcomeAnim.restart()
            sectionobj.welcomeAnimFromMM.restart()
        }
        else
            sectionobj.welcomeAnim.restart()

        var iso=langobj.vmenu.model.get(langobj.vmenu.currentIndex).ISO639;
        langobj.vmenu.forceActiveFocus()
        viewHelper.setLocale(iso)
    }
    function clearOnExit(){
        core.debug("Welcome.qml | Clear on Exit")
        langobj.revwelcomeAnim.restart()
    }
    function getMenu(){
        dataController.getNextMenu([rootCid,rootTid],viewData.homeReady)
    }
    function setLanguage(){
        timer.restart()
    }
    Timer{
        id:timer;
        interval: 500
        onTriggered: {
            var lid=langobj.vmenu.model.get(langobj.vmenu.currentIndex).LID
            var iso=langobj.vmenu.model.get(langobj.vmenu.currentIndex).ISO639;
            var locale=langobj.vmenu.model.get(langobj.vmenu.currentIndex).language_locale;
            viewHelper.setHelpLanguage(iso)
            core.setInteractiveLanguage(lid,iso,false,locale);
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedLangIndex",{langIndex:langobj.vmenu.currentIndex})
                pif.sharedModelServerInterface.sendApiData("changeLanguage",{languageIso:iso})
            }
            viewHelper.languageIndex=langobj.vmenu.currentIndex
            getMenu()
        }
    }
    function languageChanged(){
        core.debug("language changed code")
        if(viewHelper.isBackFromMainMenu) langobj.vmenu.currentIndex=viewHelper.welComeIndex
        else  viewHelper.welComeIndex=langobj.vmenu.currentIndex
        viewHelper.isBackFromMainMenu=false
        for(var i=0;i<viewData.homeModel.count;i++){
            if(viewData.homeModel.getValue(i,"category_attr_template_id")=="adult"){
                sectionobj.adultSectionTxt.text=viewData.homeModel.getValue(i,"title")
                adultIndex=i
            }
            else  if(viewData.homeModel.getValue(i,"category_attr_template_id")=="kids"){
                sectionobj.kidsSectionTxt.text=viewData.homeModel.getValue(i,"title")
                kidsIndex=i
            }

        }
        //langobj.vmenu.forceActiveFocus()
    }

    function displayNextScreen(cIndex){
        if(viewHelper.seatchat_enabled){
            if(!pif.seatChat.getAppActiveState())
                pif.seatChat.setAppActive();
        }else{}
        viewHelper.maxSessionTimer()
        viewHelper.adultIndex=cIndex;
        viewHelper.languageIndex=langobj.vmenu.currentIndex
        viewHelper.isFromWelComeScreen=true
        viewController.loadNext(viewData.homeModel,cIndex);
        if(viewHelper.isKids(current.template_id))viewHelper.sectionSelected="kids"
        else viewHelper.sectionSelected="adult"
        var iso=dataController.getIntLanguageModel().get(langobj.vmenu.currentIndex).ISO639
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedMenuJump",{sectionIndex:cIndex,iso:iso})
        }
        viewHelper.setLocale(iso)

    }
    /**************************************************************************************************************************/
    property QtObject entONobj
    Component{
        id:compEntOn
        Image {
            property alias entON: entON
            id: entON
            source:  viewHelper.configToolImagePath+configTag.entertainment_off.entoff_bg;
            Image{
                anchors.top: parent.top
                anchors.topMargin: qsTranslate('','welcome_ent_logo_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                source:viewHelper.configToolImagePath+configTag.entertainment_off.entoff_logo
                    //viewHelper.configToolImagePath+(viewHelper.isAviancaBrasilAir?configTag.entertainment_off.entoff_oceanair_logo:configTag.entertainment_off.entoff_logo);
            }
        }
    }
    property QtObject logoobj
    Component{
        id:compLogo
        Item{
            anchors.fill: parent
            property alias logo: logo
            Image{
                id:welcomeBG
                source:viewHelper.configToolImagePath+configTag.welcome.welc_bg;

            }


            SimpleButton{
                id:logo
                opacity: 0
                anchors.bottom:parent.bottom
                anchors.bottomMargin: qsTranslate('','welcome_logo_bm')
                anchors.horizontalCenter: parent.horizontalCenter
                source:viewHelper.configToolImagePath+(configTag.welcome.welc_logo);
                    //viewHelper.configToolImagePath+(viewHelper.isAviancaBrasilAir?configTag.welcome.welc_oceanair_logo:configTag.welcome.welc_logo);
            }
        }
    }
    property QtObject langobj
    Component{
        id:compLanguage
        Item{
            anchors.fill: parent
            property alias vmenu: vmenu
            property alias languageArrowAnim: languageArrowAnim
            property alias revwelcomeAnim: revwelcomeAnim
            Path{
                id:menupath
                //first point
                startX: vmenu.width/2
                startY: 0
                PathAttribute{name: 'pathOpacity'; value:pathOpacity}
                PathAttribute{name: 'fontSize'; value:fontSize2}
                //second point
                PathLine{x:vmenu.width/2; y:vmenu.height*0.5}
                PathAttribute{name: 'pathOpacity'; value:1 }
                PathAttribute{name: 'fontSize'; value: fontSize1}
                //third point
                PathLine{x:vmenu.width/2; y:vmenu.height}
                PathAttribute{name: 'pathOpacity'; value: pathOpacity}
                PathAttribute{name: 'fontSize'; value: fontSize2}
            }
            PathView{
                id:vmenu
                focus: true
                opacity: 0
                anchors.top:parent.top
                anchors.topMargin:qsTranslate('','welcome_language_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                width:  qsTranslate('','welcome_language_w')
                height:  qsTranslate('','welcome_language_h')
                path:menupath;
                preferredHighlightBegin: 0.5
                preferredHighlightEnd: 0.5
                highlightMoveDuration: 200
                pathItemCount:(model.count<5)?3:5;
                model:viewHelper.languageModel
                delegate:     Text {
                    text: name
                    color:(vmenu.currentIndex==index && vmenu.activeFocus?configTag.welcome.language_text_h:(vmenu.currentIndex==index?configTag.welcome.language_text_s:configTag.welcome.language_text_n))
                    opacity:(vmenu.currentIndex==index && !vmenu.activeFocus)?0.6:PathView.pathOpacity;
					
					// hack - customer approved change to prevent font corruption in menu screen (on linux)
                    font.pixelSize: Math.min(PathView.fontSize, 64);
                    font.family: viewHelper.thinFont

                    MouseArea{
                        anchors.fill: parent
                        onReleased:  {
                            if(sectionobj.welcomeAnim.running)return;
                            vmenu.currentIndex=index;
                            languageArrowAnim.restart()
                            setLanguage()
                        }
                    }
                }
                Keys.onReleased: {
                    isContinuouslyPressed=false
                }
                Keys.onLeftPressed:{
                    if(isContinuouslyPressed)return
                    isContinuouslyPressed=true
                    if(!sectionobj.welcomeAnim.running)sectionobj.kidsSection.forceActiveFocus()
                }
                Keys.onRightPressed: {
                    if(isContinuouslyPressed)return
                    isContinuouslyPressed=true
                    if(!sectionobj.welcomeAnim.running)sectionobj.adultSection.forceActiveFocus()
                }
                Keys.onUpPressed: {
                    if(isContinuouslyPressed)return
                    if(languageArrowAnim.running)languageArrowAnim.complete()
                    vmenu.decrementCurrentIndex()
                    setLanguage()
                    isContinuouslyPressed=true
                    languageArrowAnim.restart()
                }
                Keys.onDownPressed: {
                    if(isContinuouslyPressed)return
                    if(languageArrowAnim.running)languageArrowAnim.complete()
                    vmenu.incrementCurrentIndex()
                    isContinuouslyPressed=true
                    setLanguage()
                    languageArrowAnim.restart()
                }
                onMovementEnded: {
                    languageArrowAnim.restart()
                    setLanguage()
                }
            }
            Image{
                id:language_left_arrow1
                opacity: 0
                source:  viewHelper.configToolImagePath+configTag.welcome.left_arrow;
                anchors.left:parent.left
                anchors.leftMargin:  qsTranslate('','welcome_arrow_gap')
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','welcome_arrow_tm')
            }
            Image{
                id:language_left_arrow2
                source:  viewHelper.configToolImagePath+configTag.welcome.left_arrow;
                opacity: 0
                anchors.left:language_left_arrow1.left
                anchors.leftMargin: qsTranslate('','welcome_arrow_gap1')
                anchors.top:parent.top
                anchors.topMargin:qsTranslate('','welcome_arrow_tm')
            }
            Image{
                id:language_left_arrow3
                opacity:0
                source:  viewHelper.configToolImagePath+configTag.welcome.left_arrow;
                anchors.left:language_left_arrow2.left
                anchors.leftMargin:  qsTranslate('','welcome_arrow_gap1')
                anchors.top:parent.top
                anchors.topMargin:qsTranslate('','welcome_arrow_tm')
            }
            Image{
                id:language_right_arrow1
                opacity: 0
                source:  viewHelper.configToolImagePath+configTag.welcome.right_arrow;
                anchors.right:parent.right
                anchors.rightMargin: qsTranslate('','welcome_arrow_gap')
                anchors.top:parent.top
                anchors.topMargin:qsTranslate('','welcome_arrow_tm')
            }
            Image{
                id:language_right_arrow2
                opacity: 0
                source:  viewHelper.configToolImagePath+configTag.welcome.right_arrow;
                anchors.right:language_right_arrow1.right
                anchors.rightMargin:   qsTranslate('','welcome_arrow_gap1')
                anchors.top:parent.top
                anchors.topMargin:qsTranslate('','welcome_arrow_tm')
            }
            Image{
                id:language_right_arrow3
                opacity: 0
                source:  viewHelper.configToolImagePath+configTag.welcome.right_arrow;
                anchors.right:language_right_arrow2.right
                anchors.rightMargin:   qsTranslate('','welcome_arrow_gap1')
                anchors.top:parent.top
                anchors.topMargin:qsTranslate('','welcome_arrow_tm')
            }
            ParallelAnimation{
                id:languageArrowAnim
                SequentialAnimation{
                    NumberAnimation { targets: language_left_arrow1;properties: "opacity";from:1;to:0;duration: 0 }
                    NumberAnimation { targets: language_left_arrow3;properties: "opacity";from:0;to:0.2;duration: timeControl}
                    ParallelAnimation{
                        NumberAnimation { targets: language_left_arrow3;properties: "opacity";from:0.2;to:0;duration: timeControl }
                        NumberAnimation { targets: language_left_arrow2;properties: "opacity";from:0.2;to:0.5;duration:timeControl }
                    }
                    ParallelAnimation{
                        NumberAnimation { targets: language_left_arrow2;properties: "opacity";from:0.5;to:0;duration: timeControl}
                        NumberAnimation { targets: language_left_arrow1;properties: "opacity";from:0.5;to:0.7;duration: timeControl }
                    }
                }
                SequentialAnimation{
                    NumberAnimation { targets: language_right_arrow1;properties: "opacity";from:1;to:0;duration: 0 }
                    NumberAnimation { targets: language_right_arrow3;properties: "opacity";from:0;to:0.2;duration: timeControl }
                    ParallelAnimation{
                        NumberAnimation { targets: language_right_arrow3;properties: "opacity";from:0.2;to:0;duration: timeControl }
                        NumberAnimation { targets: language_right_arrow2;properties: "opacity";from:0.2;to:0.5;duration: timeControl}
                    }
                    ParallelAnimation{
                        NumberAnimation { targets: language_right_arrow2;properties: "opacity";from:0.5;to:0;duration: timeControl}
                        NumberAnimation { targets: language_right_arrow1;properties: "opacity";from:0.5;to:0.7;duration: timeControl }
                    }
                }
                NumberAnimation { targets: sectionobj?sectionobj.adultSectionTxt:null;properties: "opacity";from:0;to:1;duration: 2000 }
                NumberAnimation { targets: sectionobj?sectionobj.kidsSectionTxt:null;properties: "opacity";from:0;to:1;duration: 2000 }
            }
            ParallelAnimation{
                id:revwelcomeAnim
                NumberAnimation{target: entONobj?entONobj.entON:null;properties: "opacity";from:0;to:1;duration:10}
                NumberAnimation{target: welcomeScreen;properties: "opacity";from:0;to:1;duration:10;}
                NumberAnimation{target: logoobj?logoobj.logo:null;properties: "opacity";from:1;to:0;duration:10;}
                NumberAnimation{target: langobj?langobj.vmenu:null;properties: "opacity";from:1;to:0;duration:10;}
                NumberAnimation{target:sectionobj?sectionobj.adultSection:null;properties: "opacity";from:1;to:0;duration:10;}
                NumberAnimation{target: sectionobj?sectionobj.kidsSection:null;properties: "opacity";from:1;to:0;duration:10;}
                NumberAnimation{target: sectionobj?sectionobj.adultSection:null;properties: "anchors.rightMargin";from:0;to:-70;duration:10;}
                NumberAnimation{target: sectionobj?sectionobj.kidsSection:null;properties: "anchors.leftMargin";from:0;to:-70;duration:10;}
                NumberAnimation { targets: language_left_arrow1;properties: "opacity";from:1;to:0;duration: 0 }
                NumberAnimation { targets: language_left_arrow2;properties: "opacity";from:1;to:0;duration: timeControl}
                NumberAnimation { targets: language_left_arrow3;properties: "opacity";from:1;to:0;duration: timeControl}
                NumberAnimation { targets: language_right_arrow1;properties: "opacity";from:1;to:0;duration: 0 }
                NumberAnimation { targets: language_right_arrow2;properties: "opacity";from:1;to:0;duration: 0 }
                NumberAnimation { targets: language_right_arrow3;properties: "opacity";from:1;to:0;duration: 0 }
            }
        }
    }
    property QtObject sectionobj
    Component{
        id:compSection
        Item{
            anchors.fill: parent
            property alias adultSection: adultSection
            property alias adultSectionTxt: adultSectionTxt
            property alias kidsSection: kidsSection
            property alias kidsSectionTxt: kidsSectionTxt
            property alias welcomeAnim: welcomeAnim
            property alias welcomeAnimFromMM: welcomeAnimFromMM
            SimpleNavButton{
                id:adultSection
                opacity: 0
                anchors.right: parent.right
                anchors.rightMargin: -70
                anchors.top:parent.top
                anchors.topMargin:  qsTranslate('','welcome_selection_tm')
                normImg: viewHelper.configToolImagePath+configTag.welcome.adult_btn_n;
                highlightimg: viewHelper.configToolImagePath+configTag.welcome.adult_btn_h;
                pressedImg: viewHelper.configToolImagePath+configTag.welcome.adult_btn_p;
                isHighlight: activeFocus
                ViewText{
                    id:adultSectionTxt
                    opacity: 0
                    anchors.right: parent.right
                    anchors.rightMargin: qsTranslate('','welcome_selection_text_gap')
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    width:  qsTranslate('','welcome_selection_text_w')
                    height:  qsTranslate('','welcome_selection_text_h')
                    varText: ['',(adultSection.isPressed?configTag.welcome.adult_btn_txt_p:(adultSection.activeFocus?configTag.welcome.adult_btn_txt_h:configTag.welcome.adult_btn_txt_n)),qsTranslate('','welcome_selection_text_fontsize')]
                    wrapMode: Text.WordWrap
                }
                onEnteredReleased: {
                    if(!welcomeAnim.running)
                        displayNextScreen(adultIndex)
                }
                Keys.onLeftPressed: langobj.vmenu.forceActiveFocus()
            }
            SimpleNavButton{
                id:kidsSection
                opacity: 0
                anchors.left: parent.left
                anchors.leftMargin: -70
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','welcome_selection_tm')
                normImg:  viewHelper.configToolImagePath+configTag.welcome.kids_btn_n
                highlightimg: viewHelper.configToolImagePath+configTag.welcome.kids_btn_h
                pressedImg: viewHelper.configToolImagePath+configTag.welcome.kids_btn_p
                isHighlight: activeFocus
                ViewText{
                    id:kidsSectionTxt
                    opacity: 0
                    anchors.left: parent.left
                    anchors.leftMargin:  qsTranslate('','welcome_selection_text_gap')
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    width:  qsTranslate('','welcome_selection_text_w')
                    height:  qsTranslate('','welcome_selection_text_h')
                    varText: ['',(kidsSection.isPressed?configTag.welcome.kids_btn_txt_p:(kidsSection.activeFocus?configTag.welcome.kids_btn_txt_h:configTag.welcome.kids_btn_txt_n)),qsTranslate('','welcome_selection_text_fontsize')]
                    wrapMode: Text.WordWrap
                }
                onEnteredReleased: {
                    if(!welcomeAnim.running){
                        //                        viewController.showScreenPopup(7)
                        //                        return;
                        displayNextScreen(kidsIndex)
                    }
                }
                Keys.onRightPressed:  langobj.vmenu.forceActiveFocus()
            }
            ParallelAnimation{
                id:welcomeAnim
                NumberAnimation{target: entONobj.entON;properties: "opacity";from:1;to:0;duration:1500}
                SequentialAnimation{
                    PauseAnimation { duration: 1000 }
                    ParallelAnimation{
                        // NumberAnimation{target: welcomeScreen;properties: "opacity";from:0;to:1;duration:1000;}
                        NumberAnimation{target: logoobj.logo;properties: "opacity";from:0;to:1;duration:1000;}
                    }
                    ParallelAnimation{
                        SequentialAnimation{
                            NumberAnimation{target: langobj.vmenu;properties: "opacity";from:0;to:1;duration:900;}
                        }
                        //                NumberAnimation{target: language_left_arrow3;properties: "opacity";from:0;to:0.7;duration:1000;}
                        //                NumberAnimation{target: language_right_arrow3;properties: "opacity";from:0;to:0.7;duration:1000;}
                    }
                    ParallelAnimation{
                        NumberAnimation{target: adultSection;properties: "opacity";from:0;to:1;duration:200;}
                        NumberAnimation{target: kidsSection;properties: "opacity";from:0;to:1;duration:200;}
                        NumberAnimation{target: adultSection;properties: "anchors.rightMargin";from:-70;to:0;duration:450;}
                        NumberAnimation{target: kidsSection;properties: "anchors.leftMargin";from:-70;to:0;duration:450;}
                    }
                    ScriptAction{
                        script:{
                            langobj.languageArrowAnim.restart()
                            viewHelper.isBackFromMainMenu=false
                        }
                    }
                }
            }



            ParallelAnimation{
                id:welcomeAnimFromMM
                NumberAnimation{target: entONobj.entON;properties: "opacity";from:1;to:0;duration: 0}
                /* since anyways did not work as it could not access the targets */
                //                NumberAnimation { targets: langobj.language_left_arrow1;properties: "opacity";from:1;to:0;duration: 0 }
                //                NumberAnimation { targets: langobj.language_left_arrow2;properties: "opacity";from:1;to:0;duration: 0}
                //                NumberAnimation { targets: language_left_arrow3;properties: "opacity";from:1;to:0;duration: 0}
                //                NumberAnimation { targets: language_right_arrow1;properties: "opacity";from:1;to:0;duration: 0 }
                //                NumberAnimation { targets: language_right_arrow2;properties: "opacity";from:1;to:0;duration: 0 }
                //                NumberAnimation { targets: language_right_arrow3;properties: "opacity";from:1;to:0;duration: 0 }
                SequentialAnimation{
                    ParallelAnimation{
                        NumberAnimation{target: logoobj.logo;properties: "opacity";from:0;to:1;duration:1000;}
                    }
                    ParallelAnimation{
                        SequentialAnimation{
                            NumberAnimation{target: langobj.vmenu;properties: "opacity";from:0;to:1;duration:900;}
                        }
                    }
                    ParallelAnimation{
                        NumberAnimation{target: adultSection;properties: "opacity";from:0;to:1;duration:200;}
                        NumberAnimation{target: kidsSection;properties: "opacity";from:0;to:1;duration:200;}
                        NumberAnimation{target: adultSection;properties: "anchors.rightMargin";from:-70;to:0;duration:450;}
                        NumberAnimation{target: kidsSection;properties: "anchors.leftMargin";from:-70;to:0;duration:450;}
                    }
                    ScriptAction{
                        script:{
                            langobj.languageArrowAnim.restart()
                        }
                    }
                }
            }
        }
    }
}
