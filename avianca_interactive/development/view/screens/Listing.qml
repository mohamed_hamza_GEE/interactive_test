// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:listingScreen
    property bool isAnimFirstTime: true
    property bool hideCenter:false;
    property bool isSynopsis: false
    property bool isModelUpdated: false
    property bool isMousePressed: false
    property int selIndex:-1;
    property int selprevIndex:0;
    property int selnextIndex:0;
    property int listingsingleimgwidth:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_itemw')
    property int compOffset: qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_hgap')/2
    property variant mediaList:listingobj;
    property variant radioData;
    property variant currentTemplateNode:"movies"
    property string comptext_h: configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1],true)].listing.text_color_h
    property string comptext_n: configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1],true)].listing.text_color_n
    property string comptext_p: configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1],true)].listing.text_color_p
    property string compScale:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_scale')
    property string compImage_h:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].listing.poster_holder_h
    property string compImage_n:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].listing.poster_holder_n
    property string compImage_p:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(currentTemplateNode)].listing.poster_holder_p
    property string compText_w:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_w')
    property string compText_w2:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_w2')
    property string compText_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_h1')
    property string compTextHighlight_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_h')
    property string compImageTM_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_tm_h')
    property string compImageTM_n:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_tm_n')
    property string compTextFonsize_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_fontsize_h')
    property string compTextFonsize_n:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_fontsize_n')
    property string compTextOpacity:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_opacity')

    property string compsubText_w:qsTranslate('','listing_music_list_subtext_w')
    property string compsubText_w2:qsTranslate('','listing_music_list_subtext_w2')
    property string compsubText_tm_h:qsTranslate('','listing_music_list_subtext_tm_h')
    property string compsubText_tm_n:qsTranslate('','listing_music_list_subtext_tm_n')
    property string compsubText_fontsize_n:qsTranslate('','listing_music_list_subtext_fontsize_n')
    property string compsubText_fontsize_h:qsTranslate('','listing_music_list_subtext_fontsize_h')
    /**************************************************************************************************************************/
    onIsSynopsisChanged: {
        if(isSynopsis){
            if(getCidTidForListing()[1]=="tvseries")
                getTvSeries()
        }
    }
    Connections{
        target:visible?viewHelper:null
        onMainMenuIndexChanged:{
            isSynopsis=false
            getSubCategoryMenu()
            if(!revsynopsisAnim.running)
                revsynopsisAnim.restart()
        }
    }
    onCurrentTemplateNodeChanged: core.debug("onCurrentTemplateNodeChanged "+currentTemplateNode)
    /**************************************************************************************************************************/
    function load(){
        push(compPostersBG,"posterobj")
        push(compListing,"listingobj")
        push(compSubCategory,"subCatobj")
        push(compSynopsis,"synopsisobj")
    }
    function init(){
		core.debug("Listing.qml | Init ")
        widgetList.showHeader(true);
        widgetList.showMainMenu(true);
        subCatobj.compVpathSubCategory.opacity=1;
        getSubCategoryMenu()
    }
    function reload(){
        if(!isModelUpdated){
            closeLoader();
        }
        subCatobj.compVpathSubCategory.opacity=0;       
    }
    function clearOnExit(){
        closeLoader();
        subCatobj.compVpathSubCategory.currentIndex=0;
        subCatobj.compVpathSubCategory.model=viewHelper.blankModel;
        listingobj.compVpathListing.model=viewHelper.blankModel;
        listingobj.compVpathListing.currentIndex=0;
        viewHelper.subCategoryMenuIndex=0;
        core.debug("Listing.qml | clearOnExit ")
    }
    function backBtnPressed(){
        if(isSynopsis){
            if(widgetList.isSoundTrackSubtitleOpen()){widgetList.getSoundTrackSubtitleRef().visible=false}
            synopsisobj.item.backBtnPressed();
        }
        else viewHelper.homeAction()
    }
    function languageUpdate(){
        isModelUpdated=false;
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel,subCatobj.compVpathSubCategory.model],[listingUpdate]);
    }
    function listingUpdate(){
        if(listingobj.visible){
            dataController.refreshModelsByIntLanguage([listingobj.compVpathListing.model],[updateSynopsis]);
        }else{
            updateSynopsis();
        }
    }
    function updateSynopsis(){
        if(isSynopsis){
            if(synopsisobj.item.langUpdate){
                synopsisobj.item.langUpdate();
            }
        }
    }
    function enterReleased(index){        
        if(viewHelper.subCategoryMenuIndex==index)return;
        subCatobj.compVpathSubCategory.currentIndex=index
        viewHelper.subCategoryMenuIndex=index
        isSynopsis=false
        subCatobj.compVpathSubCategory.forceActiveFocus();

        subCategoryPaxus();
        getListingMenu()
    }

    function subCategoryPaxus(){
        var paxusScreen = subCatobj.compVpathSubCategory.model.getValue(viewHelper.subCategoryMenuIndex,'title');
        pif.paxus.interactiveScreenLog(paxusScreen);
    }


    function enterReleasedforListing(index){
        selIndex=index;
        listingobj.compVpathListing.currentIndex=index
        viewHelper.setMediaListingIndex(index)
        loadSynopsis()
    }
    function loadSynopsis(){
        //core.debug("pravin @@@@@@@@@@  getCidTidForSubCat()[0] : "+getCidTidForSubCat()[0]+" |  getCidTidForSubCat()[1] "+getCidTidForSubCat()[1])
        viewHelper.setSubCatCid(getCidTidForSubCat()[0]);
        if(getCidTidForSubCat()[1]=="music_listing"){
            viewHelper.setHelpTemplate("tracklist");
            if(synopsisobj.sourceComponent==musicAlbumSynopsis){
                synopsisobj.item.init();
            }else{
                synopsisobj.sourceComponent = musicAlbumSynopsis;
            }
        }else if(getCidTidForSubCat()[1]=="artist_listing"){
            viewHelper.setHelpTemplate("artist_listing");
            synopsisobj.sourceComponent = musicArtistSynopsis;
        }else if(getCidTidForSubCat()[1]=="genre_listing"){
            synopsisobj.sourceComponent =  musicGenreSynopsis
        }else if(getCidTidForSubCat()[1]=="playlist"){
            synopsisobj.sourceComponent =  playlistSynopsis;
        }else if(getCidTidForSubCat()[1]=="media_listing"){
            synopsisobj.sourceComponent =  radioSynopsis;
        }else{
            synopsisobj.sourceComponent = baseSynopsis;
        }
        if(!isMousePressed) isSynopsis=true;
        else if(isMousePressed)isMousePressed=false
    }
    function incrementSelindex(){
        if(selIndex==listingobj.compVpathListing.count-1)selIndex=0
        else  selIndex++
    }
    function decrementSelindex(){
        if(selIndex==0)selIndex=listingobj.compVpathListing.count-1
        else  selIndex--
    }
    function getSubCategoryMenu(){
        core.log("Pravin  inside | getSubCategoryMenu | widgetList.getCidTidMainMenu()[0] - "+widgetList.getCidTidMainMenu()[0]+" | widgetList.getCidTidMainMenu()[1] - "+widgetList.getCidTidMainMenu()[1])
        subCatobj.compVpathSubCategory.model=viewHelper.blankModel
        dataController.getNextMenu([widgetList.getCidTidMainMenu()[0] ,widgetList.getCidTidMainMenu()[1] ],subCategoryReady)
    }
    function getListingMenu(){
        if(getCidTidForSubCat()[1]=="artist_listing" || getCidTidForSubCat()[1]=="genre_listing" || getCidTidForSubCat()[1]=="playlist"){
            if(viewHelper.isCurtainFirstTime){
                widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
                viewHelper.isCurtainFirstTime=false
            }
            loadSynopsis()
            listingobj.visible=false;
            isModelUpdated=false;
        }else if(getCidTidForSubCat()[1]=="media_listing"){
            listingobj.visible=false;
            dataController.getNextMenu([getCidTidForSubCat()[0]  , getCidTidForSubCat()[1] ],radioListReady)
        }else if(current.template_id!="discover"){
            mediaList=listingobj;
            listingobj.visible=true;
            listingobj.compVpathListing.setModel(viewHelper.blankModel)
            dataController.getNextMenu([getCidTidForSubCat()[0]  , getCidTidForSubCat()[1] ],listingReady)
        }
    }
    function subCategoryReady(dModel){
        if(dModel.count==0){
            if(viewHelper.isCurtainFirstTime){
                widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
                viewHelper.isCurtainFirstTime=false
            }
            viewController.showScreenPopup(7);
            return;
        }

        subCatobj.compVpathSubCategory.model=dModel
        if(!isModelUpdated){

            subCatobj.compVpathSubCategory.currentIndex=0;viewHelper.subCategoryMenuIndex=0;
        }
        else subCatobj.compVpathSubCategory.currentIndex=viewHelper.subCategoryMenuIndex

        subCategoryPaxus();
        if(getCidTidForSubCat()[1]=="artist_listing"){
            loadSynopsis()
            listingobj.visible=false;
            isModelUpdated=false;
        }else{
            mediaList=listingobj;
            listingobj.visible=true;
            getListingMenu()
        }
    }




    function radioListReady(dmodel){
        if(dmodel.count==0){
            synopsisobj.sourceComponent = blankComp;
            viewController.showScreenPopup(7);
            return;
        }
        radioData = dmodel;
        loadSynopsis();
        isModelUpdated=false;
    }
    function listingReady(dModel){
        currentTemplateNode=widgetList.getCidTidMainMenu()[1];
        listingobj.compVpathListing.setModel(viewHelper.blankModel);
        if(dModel.count==0){
            if(viewHelper.isCurtainFirstTime){
                widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
                viewHelper.isCurtainFirstTime=false
            }
            viewController.showScreenPopup(7);
            return;
        }
        viewHelper.setHelpTemplateForListing(currentTemplateNode);
        listingobj.compVpathListing.delegate =iconText
        listingobj.compVpathListing.setModel(dModel)
        if(!isModelUpdated) {listingobj.compVpathListing.currentIndex=0;}
        else  listingobj.compVpathListing.currentIndex=selIndex
        viewHelper.setMediaListingModel(dModel)
        if(viewHelper.isCurtainFirstTime){
            widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
            viewHelper.isCurtainFirstTime=false
        }
        if(!isModelUpdated) closeLoader()
        else synopsisobj.synopsisItem.item.init();
        if(!isModelUpdated && dModel.count>0) listingobj.compVpathListing.forceActiveFocus()
        if(widgetList.isVideoVisible())widgetList.videolanguageUpdate()
        isModelUpdated=false
    }
    function getCidTidForSubCat(){
        var param=[]
        param[0]=subCatobj.compVpathSubCategory.model.getValue(viewHelper.subCategoryMenuIndex,"cid")
        param[1]=subCatobj.compVpathSubCategory.model.getValue(viewHelper.subCategoryMenuIndex,"category_attr_template_id")
        return param;
    }
    function findAlbumAndJump(){
        for(var i=0;i<subCatobj.compVpathSubCategory.model.count;i++){
            var tid=subCatobj.compVpathSubCategory.model.getValue(i,"category_attr_template_id");
            if(tid=="music_listing"){
                enterReleased(i)
                return;
            }
        }
    }
    function getCidTidForListing(){
        var param=[]
        param[0]=listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"cid")
        param[1]=listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"media_attr_template_id")
        return param;
    }
    function focusFromMainMenu(){if(subCatobj.compVpathSubCategory.model.count>0)subCatobj.compVpathSubCategory.forceActiveFocus()}
    function focusFromFooter(){
        if(listingobj.compVpathListing.count>0)
            listingobj.compVpathListing.forceActiveFocus()
        else if(subCatobj.compVpathSubCategory.count>0)
            subCatobj.compVpathSubCategory.forceActiveFocus()
        else
            widgetList.focustTomainMenu()
    }
    function getTvSeries(){
        dataController.getNextMenu([listingobj.compVpathListing.model.getValue( listingobj.compVpathListing.currentIndex,"mid"),'tvseries'],episodeModelReady);
    }
    function episodeModelReady(dModel){
        synopsisobj.synopsisItem.item.tvModel=dModel//tempTvModel
        viewHelper.setTvSeriesModel(dModel)
        if(!isModelUpdated){
            synopsisobj.synopsisItem.item.forceFocus();
        }
    }
    function screenPopupClosed(id,param){
        if(id==2){
            viewHelper.isResume=param
            synopsisobj.synopsisItem.item.loadVod("play","tvseries")
        }
        else if(id==7){
            if(subCatobj.compVpathSubCategory.model.count>0)
                subCatobj.compVpathSubCategory.forceActiveFocus()
            else widgetList.getMainMenuRef().forceActiveFocus()
        }
    }
    function closeLoader(){
        viewHelper.setHelpTemplateForListing(currentTemplateNode);
        if(!isModelUpdated) isSynopsis=false;
        if(synopsisobj.sourceComponent!=blankComp){
            synopsisobj.synopsisItem.item.clearOnExit();
        }
        synopsisobj.sourceComponent = blankComp;
        if(!isModelUpdated && listingobj.compVpathListing.count>0 && listingobj.compVpathListing.visible)listingobj.compVpathListing.forceActiveFocus();
        else subCatobj.compVpathSubCategory.forceActiveFocus()


    }
    function getMediaInfo(tag){
        return mediaList.getMediaInfo(tag)
    }
    function getAudioCid(){
        return subCatobj.compVpathSubCategory.model.getValue(0,"cid");
    }
    function getAudioTid(){
        return subCatobj.compVpathSubCategory.model.getValue(0,"category_attr_template_id");
    }
    /**************************************************************************************************************************/
    property QtObject posterobj
    Component{
        id:compPostersBG
        Item{
            anchors.fill: parent
            property alias staticBG: staticBG
            Image{
                id:listingBG
                source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
            }
            Image{
                id:staticBG
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTool.config[viewHelper.mapMenuConfigTid(widgetList.getCidTidMainMenu()[1],true)].listing.background
            }
            Image{
                id:movieBG1
                opacity:(isSynopsis && ((selIndex%2)==1) && listingobj.compVpathListing.visible)?1:0;
                Behavior on opacity{
                    NumberAnimation{duration:opacity==0?1000:0}
                }
                anchors.bottom:parent.bottom
                sourceSize.width:core.width;
                width:core.width;
                height:(core.settings.resolutionIndex==1)?452:585;
                sourceSize.height:(settings.resolutionIndex==1)?452:585;
                source:(isSynopsis && widgetList.getCidTidMainMenu()[1]!="music")?(listingobj.compVpathListing.count>0?viewHelper.cMediaImagePath+listingobj.compVpathListing.model.getValue(selprevIndex,'synopsisposter'):""):""
            }
            Image{
                id:movieBG2
                opacity:(isSynopsis && selIndex%2==0 && listingobj.compVpathListing.visible)?1:0;
                Behavior on opacity{
                    NumberAnimation{duration:1000}
                }
                sourceSize.width:core.width;
                width:core.width;
                height:(core.settings.resolutionIndex==1)?452:585;
                sourceSize.height:(settings.resolutionIndex==1)?452:585;
                anchors.bottom:parent.bottom
                source:(isSynopsis && widgetList.getCidTidMainMenu()[1]!="music")?(listingobj.compVpathListing.visible?viewHelper.cMediaImagePath+listingobj.compVpathListing.model.getValue(selnextIndex,'synopsisposter'):''): ""
            }
            Rectangle{
                width:core.width;
                height:movieBG1.opacity==1?movieBG1.height:((movieBG2.opacity==1)?movieBG2.height:0)
                opacity:0.6;
                color:"black";
                anchors.bottom:parent.bottom
                visible:(movieBG1.opacity>0 || movieBG2.opacity>0)
            }
        }
    }

    property QtObject listingobj
    Component{
        id:compListing
        MediaList{
        }
    }
    property QtObject subCatobj
    Component{
        id:compSubCategory
        SubCategory{

        }
    }


    property QtObject synopsisobj
    Component{
        id:compSynopsis
        Loader{
            id:synopsisItem
            width: parent.width
            y:17;
            property alias synopsisItem: synopsisItem
            sourceComponent: blankComp
            onStatusChanged: {
                if(synopsisItem.sourceComponent==blankComp)return;
                if (synopsisItem.status == Loader.Ready){
                    synopsisItem.item.init()
                }
            }
        }
    }
    Component{
        id:baseSynopsis
        BaseSynopsis{
        }
    }
    Component{
        id:iconTextCenter
        Rectangle{
            id:thisDel
            property int offset:qsTranslate('','mainmenu_submenu_margin')
            property string textColor: (subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?configTool.config.menu.submenu.text_color_h:(thisDel.isPressed?configTool.config.menu.submenu.text_color_p:configTool.config.menu.submenu.text_color_n))
            width: (imageHig.btnText.paintedWidth+offset)<qsTranslate('','mainmenu_submenu_w')/5?qsTranslate('','mainmenu_submenu_w')/5:(imageHig.btnText.paintedWidth+offset)
            height:qsTranslate('','mainmenu_submenu_h')
            color: "transparent"
            BorderImage{
                id:imageSel
                width: parent.width
                height:parent.height
                visible: !thisDel.activeFocus
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: thisDel.bottom
                source:viewHelper.subCategoryMenuIndex==index? viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_p:""
                horizontalTileMode:  BorderImage.Stretch
                border.left: sourceSize.width/2-1
                border.right:sourceSize.width/2-1
                border.top: sourceSize.width/2-1
                border.bottom: sourceSize.width/2-1
                Image{
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    source: viewHelper.subCategoryMenuIndex==index? viewHelper.configToolImagePath+configTool.config.menu.submenu.select_icon:""
                }
            }
            SimpleNavBrdrBtn{
                id:imageHig
                width: parent.width
                height: parent.height
                normalImg: viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_n
                anchors.horizontalCenter: parent.horizontalCenter
                highlightImg:subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_h:""
                pressedImg: viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_p
                buttonText: [title,textColor,qsTranslate('','mainmenu_submenu_fontsize')]
                btnText.horizontalAlignment: Text.AlignHCenter
                btnText.verticalAlignment: Text.AlignVCenter
                isHighlight: subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?true:false
                buttonTextopacity:viewHelper.subCategoryMenuIndex==index || (subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus)?1:0.6
                onEnteredReleased:  enterReleased(index);

            }
        }
    }
    Component{
        id:iconText
        Item{
            id:thisDelRect
            property int offset:(thisDelRect.x<PathView.view.children[0].x)?-compOffset:compOffset
            property string textColor: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?comptext_h:(thisDel.isPressed?comptext_p:comptext_n))
            width:listingsingleimgwidth
            height:listingobj.compVpathListing.height
            visible: listingobj.compVpathListing.currentIndex==index  && isSynopsis?false:true
            Image{
                id:posterBG
                anchors.centerIn: thisDel
                source: viewHelper.cMediaImagePath+poster
                asynchronous: true
                scale: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus  && !listingobj.compVpathListing.moving? 1:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?1:compScale
            }
            SimpleButton{
                id:thisDel
                isHighlight:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus && !listingobj.compVpathListing.moving?true:false
                highlightimg:compImage_h
                normImg: compImage_n
                pressedImg: compImage_p
                anchors.horizontalCenter:  parent.horizontalCenter
                anchors.horizontalCenterOffset: (listingobj.compVpathListing.currentIndex!=index &&  listingobj.compVpathListing.activeFocus)?(listingobj.compVpathListing.count<=2?0:offset):0
                isFocusRequired:!isSynopsis
                Behavior on opacity{
                    NumberAnimation{duration:500}
                }
                onEnteredReleased: {
                    //   listingobj.compVpathListing.currentIndex=index;
                    listingobj.compVpathListing.select(index);

                }
                onIsPressedChanged: if(isPressed)isMousePressed=true
                onAdjustfocus:synopsisobj.synopsisItem.item.isSliderNavChanged=true
                scale: posterBG.scale;//listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus? 1:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?1:compScale
            }
            ViewText {
                id:titleTxt
                width: listingobj.compVpathListing.currentIndex==index?compText_w2:compText_w
                height:paintedHeight//listingobj.compVpathListing.currentIndex==index?(lineCount>1?compTextHighlight_h:paintedHeight):(lineCount>1?compText_h:paintedHeight)
                anchors.top: parent.top
                anchors.topMargin:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compImageTM_h:compImageTM_n
                anchors.horizontalCenter: posterBG.horizontalCenter
                varText: [title,textColor,listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compTextFonsize_h:compTextFonsize_n]
                opacity: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus)?1:compTextOpacity
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                //verticalAlignment: Text.AlignVCenter
                maximumLineCount: 2

                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        thisDel.isPressed=true
                    }
                    onReleased: {
                        thisDel.isPressed=false
                        isMousePressed=true
                        thisDel.enteredReleased()
                    }
                }
            }
            ViewText{
                visible:widgetList.getCidTidMainMenu()[1]=="music"?true:false
                anchors.top: titleTxt.bottom
                anchors.topMargin: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compsubText_tm_h:compsubText_tm_n
                width: listingobj.compVpathListing.currentIndex==index?compsubText_w2:compsubText_w
                anchors.horizontalCenter: posterBG.horizontalCenter
                varText: [artist,textColor,listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compsubText_fontsize_h:compsubText_fontsize_n]
                opacity: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus)?1:compTextOpacity
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                height: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compsubText_fontsize_h:compsubText_fontsize_n
                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        thisDel.isPressed=true
                    }
                    onReleased: {
                        thisDel.isPressed=false
                        isMousePressed=true
                        thisDel.enteredReleased()
                    }
                }
            }

        }
    }
    Component{
        id:musicAlbumSynopsis;
        AlbumSynopsis{
            width:listingScreen.width;
        }
    }
    Component{
        id:musicArtistSynopsis
        ArtistSynopsis{
            width:listingScreen.width;
        }
    }
    Component{
        id:musicGenreSynopsis
        GenreSynopsis{
            width:listingScreen.width;
        }
    }
    Component{
        id:playlistSynopsis
        PlaylistSynopsis{
            width:listingScreen.width;
        }
    }
    Component{
        id:radioSynopsis
        RadioSynopsis{
            width:listingScreen.width;
        }
    }
    Component{id:blankComp;Item{}}
    SequentialAnimation{
        id:revsynopsisAnim
        NumberAnimation { targets: [listingobj.compVpathListing,subCatobj.compVpathSubCategory]; properties: "opacity";from:1;to:0; duration: 10 }
        PauseAnimation { duration: 800 }
        NumberAnimation { targets: [listingobj.compVpathListing,subCatobj.compVpathSubCategory]; properties: "opacity";from:0;to:1; duration: 250 }
    }
    SequentialAnimation{
        id:synopsisAnim
        ScriptAction{
            script:{
                if(selIndex%2==1){
                    selprevIndex=selIndex;
                }else{
                    selnextIndex=selIndex;
                }
            }
        }
        NumberAnimation{targets: [!isSynopsis?listingobj.compVpathListing:synopsisobj,synopsisobj];properties: "opacity";from:1;to:0;duration:250}
        ScriptAction{
            script:{
                listingobj.compVpathListing.synopIndex=selIndex;
                listingobj.compVpathListing.currentIndex=selIndex
                enterReleasedforListing(selIndex);
            }
        }
        PauseAnimation { duration: 100 }
        ScriptAction{
            script:{
                isSynopsis=true
            }
        }
        NumberAnimation{targets: [!isSynopsis?listingobj.compVpathListing:synopsisobj,synopsisobj];properties: "opacity";from:0;to:1;duration: 250}
        ScriptAction{
            script:  isMousePressed=false
        }
    }
}
