// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:usbThumbScreen
    focus: true
    /*****************************************************************************************************************************/
    property bool isModelUpdated: false
    property string filter:"*.jpg,*.gif,*.bmp,*.png,*.jpeg";
    property variant configTag:configTool.config.usb.screen;
    /*****************************************************************************************************************************/
    Connections {
        target: (visible)?viewHelper:null;
        onUsbSlideShowCLosed:{
            thumbGrid.forceActiveFocus()
        }
    }
    /*****************************************************************************************************************************/
    function load(){

        push(compSubCategory,"subCatobj")
        push(compPanelObj,"panel");
        push(compButtonPanel,"buttonPanel")
        push(compThumbGrid,"thumbGrid");
        push(compScrollBar,"scrollBar");
    }
    function init(){
        core.debug("USBThumb.qml | Init ")
        viewHelper.setHelpTemplate("usb_pic_listing");
        widgetList.showHeader(true);widgetList.showMainMenu(true);
        viewHelper.usbIndex=current.template_id
        if(viewHelper.filePathName==""){
            reload(pif.usb.getMountPath()[0]);
        }else{
            reload(viewHelper.filePathName);
        }
        //        pif.usb.readFolder(viewHelper.filePathName,filter);
        //        getSubCategoryMenu()
    }
    function reload(path){
        core.debug("USBthumb.qml |reload called  : ");
        if(path){
            viewHelper.filePathName = path
        }
        core.debug("USBTHUMB.qml |  viewHelper.filePathName  : "+viewHelper.filePathName);
        pif.usb.readFolder(viewHelper.filePathName,filter);
        getSubCategoryMenu()
    }
    function clearOnExit(){
        core.debug("USBThumb.qml | clearOnExit")
    }
    function subCategoryReady(dModel){
        subCatobj.compVpathSubCategory.model=viewHelper.blankModel
        subCatobj.compVpathSubCategory.model=dModel;
        viewHelper.subCategoryMenuIndex=core.coreHelper.searchEntryInModel(dModel,"category_attr_template_id",viewHelper.usbIndex)
        subCatobj.compVpathSubCategory.currentIndex=viewHelper.subCategoryMenuIndex
        thumbGrid.setModel(viewHelper.blankModel);
        thumbGrid.setModel(pif.usb.browserModel);
        if(thumbGrid.getGridViewRef().model.count==0){buttonPanel.backBtnRef().forceActiveFocus()}
        else{thumbGrid.getGridViewRef().forceActiveFocus()}
    }
    function languageUpdate(){
        isModelUpdated=false
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel,subCatobj.compVpathSubCategory.model],[donothing]);
    }
    function donothing(){}
    function getSubCategoryMenu(){
        var temp=core.dataController.mediaServices.getCategoryDataByTagName("usb");
        var cid=temp.cid
        var tid=temp.tid
        dataController.getNextMenu([cid,tid],subCategoryReady)
    }
    function enterReleased(index){
        if( viewHelper.subCategoryMenuIndex==index)return;
        subCatobj.compVpathSubCategory.currentIndex=index
        viewHelper.subCategoryMenuIndex=index
        viewHelper.jumToUsbScreen(subCatobj.compVpathSubCategory.model.getValue(index,"category_attr_template_id"))

    }
    function pathManipulation(str){return str.substr(0);}
    function focusFromMainMenu(){
        subCatobj.compVpathSubCategory.forceActiveFocus()
    }
    function focusFromSubCat(){
        buttonPanel.ejectBtnRef().forceActiveFocus()
    }
    function backBtnPressed(){
        viewHelper.homeAction()
        viewHelper.usbBackPressed();
    }
    function usbBackBtnPressed(){
        //        buttonPanel.noFilesTxtRef().visible=false;
        if(viewHelper.filePathName!=pif.usb.getMountPath()[0]){
            var temp = viewHelper.filePathName.lastIndexOf("/");
            viewHelper.filePathName=viewHelper.filePathName.substring(0,temp);
            if(temp==0){
                viewHelper.filePathName=pif.usb.getMountPath()[0];
            }
            pif.usb.readFolder(viewHelper.filePathName,filter);
        }
        thumbGrid.getGridViewRef().currentIndex=0
        thumbGrid.getGridViewRef().forceActiveFocus()
    }
    /**************************************************************************************************************************/
    Item{
        id:posterobj
        anchors.fill: parent
        property alias staticBG: staticBG
        Image{
            id:listingBG
            source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
        }
        Image{
            id:staticBG
            anchors.bottom:parent.bottom
            source:viewHelper.configToolImagePath+configTool.config.usb.screen.usb_background
        }
    }

    property QtObject subCatobj
    Component{
        id:compSubCategory
        SubCategory{}
    }
    property QtObject buttonPanel
    Component{
        id:compButtonPanel
        Item{
            anchors.fill: panel
            function backBtnRef(){return backBtn}
            function ejectBtnRef(){return ejectBtn}
            function noFilesTxtRef(){return noFilesText}
            property alias noFilesText:noFilesText
            SimpleNavBrdrBtn{
                id:backBtn
                normalImg: viewHelper.configToolImagePath+configTag["back_n"];
                highlightImg:viewHelper.configToolImagePath+configTag["back_h"];
                pressedImg:viewHelper.configToolImagePath+configTag["back_p"];
                anchors.bottom:parent.top;
                isDisable:viewHelper.filePathName==pif.usb.getMountPath()[0]
                onIsDisableChanged: {if(isDisable)thumbGrid.getGridViewRef().forceActiveFocus()}
                anchors.bottomMargin:qsTranslate('',"usb_back_bm")
                anchors.left:parent.left;
                anchors.leftMargin:qsTranslate('',"usb_back_lm")
                nav: ["","","",""]
                onNoItemFound: {
                    if(dir=="up"){subCatobj.compVpathSubCategory.forceActiveFocus()}
                    else if(dir=="down"){if(buttonPanel.noFilesTxtRef().visible==false)thumbGrid.getGridViewRef().forceActiveFocus()}
                    else if(dir=="right"){ejectBtn.forceActiveFocus()}
                }
                onEnteredReleased:{
                    usbBackBtnPressed()
                }
            }
            ViewText{
                id:termsDescObj
                anchors.verticalCenter: backBtn.verticalCenter
                anchors.left:backBtn.right
                width:qsTranslate('',"usb_back_w");
                varText:[pathManipulation(pif.usb.getFolderPath()),configTag["path_text_color"],qsTranslate('',"usb_eject_fontsize")]
                wrapMode: Text.Wrap
                elide:Text.ElideRight
            }
            ViewText{
                id:noFilesText
                anchors.centerIn: parent
                z:5
                width:qsTranslate('',"usb_thumbview_w");
                horizontalAlignment: Text.AlignHCenter
                varText:[configTool.language.usb.no_files,configTag["description1_color"],qsTranslate('',"usb_thumbview_fontsize")]
                visible:thumbGrid.getGridViewRef().model.count <=0
            }
            SimpleNavBrdrBtn{
                id:ejectBtn
                normalImg: viewHelper.configToolImagePath+configTag["btn_n"];
                highlightImg:viewHelper.configToolImagePath+configTag["btn_h"];
                pressedImg:viewHelper.configToolImagePath+configTag["btn_p"];
                height:qsTranslate('',"usb_eject_h");
                width:qsTranslate('',"usb_eject_w");
                btnTextWidth:parseInt(qsTranslate('',"usb_eject_textw"),10);
                buttonText: [configTool.language.usb.eject,(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','usb_eject_fontsize')]
                anchors.bottom:parent.top;
                anchors.bottomMargin:qsTranslate('',"usb_eject_bm")
                anchors.right:parent.right;
                anchors.rightMargin:qsTranslate('',"usb_eject_rm")
                normalBorder:[qsTranslate('',"usb_eject_margin")]
                highlightBorder:[qsTranslate('',"usb_eject_margin")]
                elide:Text.ElideRight
                nav: ["","","",""]
                onNoItemFound: {
                    if(dir=="up"){subCatobj.compVpathSubCategory.forceActiveFocus()}
                    if(dir=="down"){
                        if(buttonPanel.noFilesTxtRef().visible==false && scrollBar.visible)
                            scrollBar.forceActiveFocus()
                        else if(buttonPanel.noFilesTxtRef().visible==false)
                            thumbGrid.forceActiveFocus()
                        else return
                    }
                    if(dir=="left"){if(backBtn.isDisable==false)backBtn.forceActiveFocus()}
                }
                onEnteredReleased:{viewHelper.homeAction();pif.usb.usbChangeDetected(pif.usb.cUSB_OUT,pif.usb.getMountPath()[0]);}
            }
        }
    }
    property QtObject panel
    Component{
        id:compPanelObj
        Image{
            id:panelBG
            anchors.horizontalCenter: posterobj.horizontalCenter
            anchors.top:posterobj.top;
            anchors.topMargin: qsTranslate('','usb_panel_tm')
            source:viewHelper.configToolImagePath+configTool.config.usb.screen.usb_panel
        }
    }
    property QtObject thumbGrid
    Component{
        id:compThumbGrid
        GridView{
            id:gridView
            property string folderImg:viewHelper.configToolImagePath+configTag["folder_icon"]
            function setModel(model){gridView.model=model}
            function getGridViewRef(){return gridView}
            function performAction(){
                var path = gridView.model.getValue(gridView.currentIndex,"pathfilename")
                var isFolder = pif.usb.isFolderInUsb(path)
                if(isFolder){
                    gridView.currentIndex=0
                    pif.usb.readFolder(path,filter)
                    viewHelper.filePathName=path;
                    if(gridView.model.count==0){
                        //                        buttonPanel.noFilesTxtRef().visible=true;
                        buttonPanel.backBtnRef().forceActiveFocus()
                    }
                    return;
                }else{
                    viewHelper.usbSlideshow = true
                    viewHelper.usbFileName=gridView.model.getValue(gridView.currentIndex,"filenameonly")
                    widgetList.showUsbSlideshow(true)
                    return;
                }
            }
            function goUpGridView(){
                if(gridView.currentIndex<=4){
                    focus=false;
                    if(buttonPanel.backBtnRef().isDisable!=true){buttonPanel.backBtnRef().forceActiveFocus()}
                    else{buttonPanel.ejectBtnRef().forceActiveFocus()}
                }
                else{
                    var newIndex;
                    newIndex = gridView.currentIndex -5;
                    if(newIndex<0)
                        newIndex = 0
                    gridView.currentIndex = newIndex
                }
            }
            function goLeft(){
                if(gridView.currentIndex==0 || gridView.currentIndex%5==0){return}
                else gridView.currentIndex--
            }

            function goRightGridView(){
                var tmpIndex = gridView.indexAt(gridView.contentX+gridView.width-20,gridView.contentY+20)
                var tmpIndex1 = gridView.indexAt(gridView.contentX+gridView.width-20,gridView.contentY+(20+gridView.cellHeight))
                if(gridView.currentIndex==tmpIndex || gridView.currentIndex==tmpIndex1 || gridView.currentIndex==gridView.model.count-1){
                    if(scrollBar.visible)scrollBar.forceActiveFocus()
                }else{
                    gridView.currentIndex++
                }
            }
            cellHeight: qsTranslate('','usb_thumbview_cellh')
            cellWidth: qsTranslate('','usb_thumbview_cellw')
            width:qsTranslate('','usb_thumbview_w')
            height:qsTranslate('','usb_thumbview_h')
            clip:true
            snapMode:GridView.SnapToRow
            boundsBehavior:GridView.StopAtBounds
            anchors.left: panel.left
            anchors.leftMargin: qsTranslate('','usb_thumbview_lm')
            anchors.top:panel.top;
            anchors.topMargin: qsTranslate('','usb_thumbview_tm')
            onModelChanged: {
                if(model.count<=0)
                    buttonPanel.noFilesTxtRef().visible=true;
                else buttonPanel.noFilesTxtRef().visible=false;
            }
            Keys.onLeftPressed: goLeft()
            Keys.onUpPressed: {goUpGridView()}
            Keys.onRightPressed: {goRightGridView()}
            Keys.onReleased: {
                if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                    viewHelper.usbIndexSelected=gridView.currentIndex
                    gridView.performAction()
                }
            }
            delegate:Item{
                id:thisDel
                width: gridView.cellWidth
                height: gridView.cellHeight
                property bool isFData:thisDel.isFolder;
                property string imgSrc:(thisDel.isFolder)?gridView.folderImg:pathfilename
                property string fileData: filenameonly
                property bool isFolder:(pif.usb.isFolderInUsb(pathfilename))?true:false;
                Rectangle{
                    anchors.top:parent.top
                    anchors.topMargin:qsTranslate('','usb_thumbview_image_tm')
                    anchors.horizontalCenter: parent.horizontalCenter
                    width:qsTranslate('','usb_thumbview_imagew')
                    height:qsTranslate('','usb_thumbview_imageh')
                    color:qsTranslate('','usb_thumbview_color')
                    Image {
                        id:dImg
                        anchors.centerIn: parent
                        sourceSize.width: parent.width;
                        sourceSize.height: parent.height;
                        source:(thisDel.isFData)?imgSrc:"image://thumbnailimage/"+imgSrc;
                        asynchronous: true;
                        smooth: false;
                        onStatusChanged:{
                            if(status == Image.Error || status == Image.Null){
                                source = imgSrc;
                            }
                        }
                    }
                    ViewText{
                        id:imgName
                        anchors.top: parent.bottom
                        anchors.topMargin: qsTranslate('',"usb_thumbview_text_tm");
                        width:qsTranslate('',"usb_thumbview_textw");
                        varText:[thisDel.fileData,configTag["description1_color"],qsTranslate('',"usb_thumbview_fontsize")]
                        lineHeight:qsTranslate('','usb_thumbview_lheight')
                        wrapMode: Text.Wrap
                        elide:Text.ElideRight
                        horizontalAlignment: Text.AlignHCenter
                        maximumLineCount:1
                    }
                    Image{
                        id:posterImg
                        anchors.centerIn: parent
                        source: viewHelper.configToolImagePath+"usb_thumb_h.png"//configTag["usb_thumb_highlight"]
                        visible: (index==gridView.currentIndex && gridView.activeFocus)
                    }
                    MouseArea{
                        anchors.fill: parent
                        z:1;
                        onClicked:{
                            gridView.currentIndex = index;
                            viewHelper.usbIndexSelected=index
                            gridView.performAction();
                        }
                    }
                }
            }
            onCountChanged:  {
                if(count<=9)  scrollBar.opacity=0
                else scrollBar.opacity=1
                if(count<=0)
                    buttonPanel.noFilesTxtRef().visible=true;
                else buttonPanel.noFilesTxtRef().visible=false;

            }
        }
    }
    property QtObject scrollBar
    Component{
        id:compScrollBar
        Scroller{
            id:usbScroll
            targetData:thumbGrid.getGridViewRef()
            sliderbg:[qsTranslate('','usb_thumbview_slider_base_margin'),qsTranslate('','usb_thumbview_slider_w'),qsTranslate('','usb_thumbview_slider_base_h'),configTag["scroll_base"]]
            scrollerDot:configTag["scroll_button"]
            upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
            downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
            anchors.right:panel.right;
            anchors.rightMargin:qsTranslate('','usb_thumbview_slider_rm');
            anchors.top:panel.top
            anchors.topMargin:qsTranslate('','usb_thumbview_slider_tm')
            height: qsTranslate('','usb_thumbview_slider_h');
            width: qsTranslate('','usb_thumbview_slider_w');
            visible:(buttonPanel.noFilesTxtRef().visible)?false:((thumbGrid.getGridViewRef().contentHeight>thumbGrid.getGridViewRef().height)?true:false)
            onSliderNavigation:{
                if(direction=="down"){return;}
                else if(direction=="up") {buttonPanel.ejectBtnRef().forceActiveFocus()}
                else if(direction=="right"){return;}
                else if(direction=="left"){thumbGrid.getGridViewRef().forceActiveFocus()}
            }
            onArrowPressed: {
                thumbGrid.getGridViewRef().currentIndex=thumbGrid.getGridViewRef().indexAt(thumbGrid.getGridViewRef().contentX,thumbGrid.getGridViewRef().contentY+20)
            }
        }
    }
    Component{
        id:iconTextCenter
        Rectangle{
            id:thisDel
            property int offset:qsTranslate('','mainmenu_submenu_margin')
            property string textColor: configTool.config.menu.submenu.text_color_n
            width: (imageHig.btnText.paintedWidth+offset)<qsTranslate('','mainmenu_submenu_w')/5?qsTranslate('','mainmenu_submenu_w')/5:(imageHig.btnText.paintedWidth+offset)
            height:qsTranslate('','mainmenu_submenu_h')
            color: "transparent"
            BorderImage{
                id:imageSel
                width: imageHig.btnText.paintedWidth+offset
                height:parent.height
                visible: !thisDel.activeFocus
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: thisDel.bottom
                source:viewHelper.subCategoryMenuIndex==index? viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_p:""
                horizontalTileMode:  BorderImage.Stretch
                border.left: sourceSize.width/2-1
                border.right:sourceSize.width/2-1
                border.top: sourceSize.width/2-1
                border.bottom: sourceSize.width/2-1
                Image{
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    source: viewHelper.subCategoryMenuIndex==index? viewHelper.configToolImagePath+configTool.config.menu.submenu.select_icon:""
                }
            }
            SimpleNavBrdrBtn{
                id:imageHig
                width: imageHig.btnText.paintedWidth+offset
                height: parent.height
                normalImg: viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_n
                anchors.horizontalCenter: parent.horizontalCenter
                highlightImg:subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_h:""
                pressedImg: viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_p
                buttonText: [title,"white",qsTranslate('','mainmenu_submenu_fontsize')]
                btnText.horizontalAlignment: Text.AlignHCenter
                btnText.verticalAlignment: Text.AlignVCenter
                isHighlight: subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?true:false
                buttonTextopacity:viewHelper.subCategoryMenuIndex==index || (subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus)?1:0.6
                onEnteredReleased:  enterReleased(index);
            }
        }
    }
    /**************************************************************************************************************************/
}
