// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:usbScreen
    property variant configTag:configTool.config.usb.screen;
    /*****************************************************************************************************************************/
    Connections{
        target:(visible)?viewData:null
        onUsbSubMenuModelReady:{
            viewHelper.jumToUsbScreen(viewHelper.usbIndex)
        }
    }
    Connections{
        target:(visible)?viewHelper:null
        onUsbBackPressed:{
            viewHelper.homeAction()
        }
    }
    /*****************************************************************************************************************************/
    function load(){
     //   push(compPostersBG,"posterobj");
        push(compPanelObj,"panel");
        push(compTermsTitle,"termsTitle");
        push(compTermsDesc,"termsDesc");
        push(compTermsButtons,"termsButtons");
    }
    function init(){
        core.debug("USB.qml | Init")
        pif.paxus.startContentLog('Usb')
        widgetList.showHeader(true);
        widgetList.showMainMenu(true);
        if(viewHelper.getUsbTerms()==true && pif.usb.getUsbConnected()==true){
            core.debug("USB.qml | getUsbTerms "+viewHelper.getUsbTerms())
            termsButtons.getAcceptBtnRef().enteredReleased()
        }
        else{
            core.debug("USB.qml  2| getUsbTerms "+viewHelper.getUsbTerms())
            panel.visible=true;termsTitle.visible=true;viewHelper.setHelpTemplate("usb_terms_cndtions")
            termsDesc.visible=true;termsButtons.visible=true;
            termsButtons.getAcceptBtnRef().forceActiveFocus()
        }
    }
    function reload(){init()}
    function clearOnExit(){
        core.debug("USB.qml | clearOnExit ")

    }
    function backBtnPressed(){
        viewHelper.homeAction()
    }
    function focusFromMainMenu(){
        termsButtons.getScrollBarRef().forceActiveFocus()
    }
    /**************************************************************************************************************************/
    Item{
        id:posterobj
        anchors.fill: parent
        property alias staticBG: staticBG
        Image{
            id:listingBG
            source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
        }
        Image{
            id:staticBG
            anchors.bottom:parent.bottom
            source:viewHelper.configToolImagePath+configTool.config.usb.screen.usb_background
        }
    }
    property QtObject panel
    Component{
        id:compPanelObj
        Image{
            id:panelBG
            anchors.horizontalCenter: posterobj.horizontalCenter
            anchors.top:posterobj.top;
            anchors.topMargin: qsTranslate('','usb_panel_tm1')
            source:viewHelper.configToolImagePath+configTool.config.usb.screen.tc_panel
            visible: false
        }
    }
    property QtObject termsTitle
    Component{
        id:compTermsTitle;
        ViewText{
            id:termsTitleObj
            anchors.horizontalCenter: panel.horizontalCenter
            anchors.top: panel.top;
            horizontalAlignment: Text.AlignHCenter
            anchors.topMargin:qsTranslate('',"usb_terms_title_tm");
            width:qsTranslate('',"usb_terms_title_w");
            height:qsTranslate('',"usb_terms_title_h");
            varText:[configTool.language.usb.terms_title,configTag["title_color"],qsTranslate('',"usb_terms_title_fontsize")]
            visible: false
        }
    }
    property QtObject termsDesc
    Component{
        id:compTermsDesc;
        Flickable{
            id : termsDescObjFlickable
            anchors.horizontalCenter: panel.horizontalCenter
            anchors.top: panel.top;
            anchors.topMargin:qsTranslate('',"usb_terms_desc_tm");
            width:qsTranslate('',"usb_terms_desc_w");
            height:qsTranslate('',"usb_terms_desc_h");
            contentWidth: width
            contentHeight: termsDescObj.paintedHeight
            clip:true
            visible: false
            ViewText{
                id:termsDescObj
                width:parent.width
                height:parent.height
                varText:[configTool.language.usb.terms_desc,configTag["description1_color"],qsTranslate('',"usb_terms_desc_fontsize")]
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }
    property QtObject termsButtons
    Component{
        id:compTermsButtons;
        Item{
            visible: false
            anchors.fill: panel
            function getAcceptBtnRef(){return acceptBtn}
            function getRejectBtnRef(){return rejectBtn}
            function getScrollBarRef(){return usbScroll}
            SimpleNavBrdrBtn{
                id:acceptBtn
                normalImg: viewHelper.configToolImagePath+configTag["btn_n"];
                highlightImg:viewHelper.configToolImagePath+configTag["btn_h"];
                pressedImg:viewHelper.configToolImagePath+configTag["btn_p"];
                height:qsTranslate('',"usb_terms_btn_h");
                width:qsTranslate('',"usb_terms_btn_w");
                btnTextWidth:parseInt(qsTranslate('',"usb_terms_btn_textw"),10);
                buttonText: [configTool.language.usb.accept,(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','usb_terms_btn_fontsize')]
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('',"usb_terms_btn_bm")
                anchors.left:parent.left;
                anchors.leftMargin:qsTranslate('',"usb_terms_btn_lm")
                normalBorder:[qsTranslate('',"usb_terms_btn_margin")]
                highlightBorder:[qsTranslate('',"usb_terms_btn_margin")]
                elide:Text.ElideRight
                nav: ["","","",""]
                onNoItemFound: {
                    if(dir=="up"){usbScroll.forceActiveFocus()}
                    else if(dir=="right"){rejectBtn.forceActiveFocus()}
                }
                onEnteredReleased:{
                    viewHelper.setUsbTerms(true)
                    viewHelper.jumToUsbScreen(viewHelper.usbIndex)
                }
            }
            SimpleNavBrdrBtn{
                id:rejectBtn
                normalImg: viewHelper.configToolImagePath+configTag["btn_n"];
                highlightImg:viewHelper.configToolImagePath+configTag["btn_h"];
                pressedImg:viewHelper.configToolImagePath+configTag["btn_p"];
                height:qsTranslate('',"usb_terms_btn_h");
                width:qsTranslate('',"usb_terms_btn_w");
                btnTextWidth:parseInt(qsTranslate('',"usb_terms_btn_textw"),10);
                buttonText: [configTool.language.usb.reject,(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','usb_terms_btn_fontsize')]
                anchors.bottom:parent.bottom;
                anchors.bottomMargin:qsTranslate('',"usb_terms_btn_bm")
                anchors.left:acceptBtn.right;
                anchors.leftMargin:qsTranslate('',"usb_terms_btn_gap")
                normalBorder:[qsTranslate('',"usb_terms_btn_margin")]
                highlightBorder:[qsTranslate('',"usb_terms_btn_margin")]
                elide:Text.ElideRight
                nav: ["","","",""]
                onNoItemFound: {
                    if(dir=="up"){usbScroll.forceActiveFocus()}
                    else if(dir=="left"){acceptBtn.forceActiveFocus()}
                }
                onEnteredReleased:{viewHelper.homeAction()}
            }
            Scroller{
                id:usbScroll
                targetData:termsDesc
                sliderbg:[qsTranslate('','usb_terms_scrollcont_base_margin'),qsTranslate('','usb_terms_scrollcont_w'),qsTranslate('','usb_terms_scrollcont_base_h'),configTag["scroll_base"]]
                scrollerDot:configTag["scroll_button"]
                upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
                downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
                anchors.right:parent.right;
                anchors.rightMargin:qsTranslate('','usb_terms_scrollcont_rm');
                anchors.verticalCenter: parent.verticalCenter
                height:qsTranslate('','usb_terms_scrollcont_h');
                width: qsTranslate('','usb_terms_scrollcont_w');
                onSliderNavigation:{
                    if(direction=="down"){acceptBtn.forceActiveFocus()}
                    else if(direction=="up") {widgetList.focustTomainMenu()}
                }
                onArrowPressed: {

                }
            }
        }
    }
}


