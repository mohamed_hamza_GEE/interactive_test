import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:shopping
    property bool isAnimFirstTime: true
    property bool hideCenter:false;
    property bool isSynopsis: false
    property bool isModelUpdated: false
    property bool isMousePressed: false
    property int selIndex:-1;
    property int selprevIndex:0;
    property int selnextIndex:0;
    property variant mediaList:listingobj;
    property variant refModel
    property variant iscurrentItem
    property variant catalogModel
    property variant categoryModel
    property variant itemModel
    property variant currentTemplateNode:"shopping"
    property int compOffset: qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_hgap')/2
    property int listingsingleimgwidth:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_itemw')
    property string comptext_h: configTool.config.shop.listing.text_color_h
    property string comptext_n: configTool.config.shop.listing.text_color_n
    property string comptext_p: configTool.config.shop.listing.text_color_p
    property string compScale:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_scale')
    property string compImage_h:viewHelper.configToolImagePath+configTool.config.shop.listing.poster_holder_h
    property string compImage_n:viewHelper.configToolImagePath+configTool.config.shop.listing.poster_holder_n
    property string compImage_p:viewHelper.configToolImagePath+configTool.config.shop.listing.poster_holder_p
    property string compText_w:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_w')
    property string compText_w2:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_w2')
    property string compText_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_h1')
    property string compTextHighlight_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_h')
    property string compImageTM_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_tm_h')
    property string compImageTM_n:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_tm_n')
    property string compTextFonsize_h:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_fontsize_h')
    property string compTextFonsize_n:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_fontsize_n')
    property string compTextOpacity:qsTranslate('','listing_'+viewHelper.mapMenuTid(currentTemplateNode)+'_list_text_opacity')

    function load(){
        push(compPostersBG,"posterobj")
        push(compListing,"listingobj")
        push(compSubCategory,"subCatobj")
        push(compSynopsis,"synopsisobj")
    }

    function getPoster(){
        return viewHelper.shopValue.item_thumb ;
    }

    function init(){
        posterobj.visible=true
        listingobj.forceActiveFocus();
        widgetList.showHeader(true);
        widgetList.showMainMenu(true);
        console.log("Shopping.qml | init() ")
        getCatalogMenu();
        pif.paxus.startContentLog('Shopping')

    }

    function languageUpdate(){
        isModelUpdated=false;
        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel]);
        var curLangISO=core.settings.languageISO.toUpperCase()
        var params=new Object()
        params['catalog']=catalogModel//viewHelper.shop_catalog_id
        params['category']=categoryModel//viewHelper.shop_category_id
        params['item']=itemModel//viewHelper.shop_item_id
        core.debug(" params['catalog'] = "+ params['catalog'])
        core.debug(" params['category'] = "+ params['category'])
        core.debug(" params['item'] = "+ params['item'])
        core.debug("Shopping.qml | languageUpdate core.dataController.shopping.getLanguageModel().count = "+core.dataController.shopping.getLanguageModel().count)
        console.log("core.dataController.shopping.getLanguageModel() = "+core.dataController.shopping.getLanguageModel())
        //        var ln =core.dataController.shopping.getLanguageModel()
        //        for(var i=0 ; i <ln.count ;i++){
        //            console.log("languageUpdate |  curLangISO = "+ln.getValue(i,"language_code")+" curLangISO = "+curLangISO)
        //        }

        var langIndex =core.coreHelper.searchEntryInModel(core.dataController.shopping.getLanguageModel(),"language_code",curLangISO)
        console.log("languageUpdate |  viewHelper.defaultLang = "+viewHelper.defaultLang+" langIndex = "+langIndex+ " curLangISO = "+curLangISO)
        if(langIndex!=-1){
            console.log("languageUpdate |  curLangISO = "+curLangISO)
            core.dataController.shopping.changeLang(curLangISO,params,langModelReady);
        }else{
            console.log("defaultLang "+viewHelper.defaultLang)
            core.dataController.shopping.changeLang(viewHelper.defaultLang,params,langModelReady);
        }

        //                if(core.dataController.shopping.getLanguageModel().count<=1){
        //                    core.dataController.shopping.changeLang(viewHelper.defaultLang,params,langModelReady);
        //                }else{
        //                    core.dataController.shopping.changeLang(curLangISO,params,langModelReady);
        //                }
    }

    function langModelReady(dModel){
        if(dModel.count>0){
            subCatobj.compVpathSubCategory.setModel(core.dataController.shopping.getCategoryModel())
            listingobj.compVpathListing.setModel(core.dataController.shopping.getItemModel())
            if(synopsisobj.sourceComponent!=blankComp){
                core.dataController.shopping.getItemDetailById(viewHelper.shop_item_id ,synopsisModelReady)
            }
        }else{
            viewController.showScreenPopup(7)
        }
    }
    function backBtnPressed(){
        if(isSynopsis)synopsisobj.synopsisItem.item.backBtnPressed();
        else {
            viewHelper.homeAction()
            synopsisobj.synopsisItem.sourceComponent=blankComp
        }
    }

    function getCatalogMenu(){
        core.debug("Shopping.qml | getCatalogMenu() | core.dataController.shopping.getLanguageModel().count = "+core.dataController.shopping.getLanguageModel().count)
        var curLangISO=core.settings.languageISO.toUpperCase()
        var langIndex =core.coreHelper.searchEntryInModel(core.dataController.shopping.getLanguageModel(),"language_code",curLangISO)
        core.debug("Shopping.qml | languageUpdate |  viewHelper.defaultLang = "+viewHelper.defaultLang+" langIndex = "+langIndex+ " curLangISO = "+curLangISO)
        if(langIndex!=-1){
            core.debug("Shopping.qml | languageUpdate |  curLangISO = "+curLangISO)
            core.dataController.shopping.fetchData(curLangISO,catalogMenuReady)
        }else{
            core.debug("Shopping.qml | defaultLang "+viewHelper.defaultLang)
            core.dataController.shopping.fetchData(viewHelper.defaultLang,catalogMenuReady)
        }
    }

    function focusFromMainMenu(){
        subCatobj.forceActiveFocus();
    }

    function focusFromFooter(){
        if(listingobj.compVpathListing.count>0)
            listingobj.compVpathListing.forceActiveFocus()
        else if(subCatobj.compVpathSubCategory.count>0)
            subCatobj.compVpathSubCategory.forceActiveFocus()
        else
            widgetList.focustTomainMenu()
    }
    Connections{
        target:visible?dataController.shopping:null
        onSigCatalogStatusChanged:{
            console.log("Hospitality.qml | catalog_id "+catalog_id+ " status= "+status)
            dataController.shopping.fetchData(viewHelper.defaultLang,catalogMenuReady)
        }
    }

    function catalogMenuReady(dModel,status){
        core.debug("Shopping.qml | catalogMenuReady  | dModel.count ="+dModel.count)
        if(dModel.count<=0){viewHelper.homeAction();viewController.showScreenPopup(7); return;}
        catalogModel=dModel
        viewHelper.shopValue=dModel.at(0)
        viewHelper.shop_catalog_id=viewHelper.shopValue.catalog_id
        getCategoryMenu()

    }

    function getCategoryMenu(){
        core.debug(" Shopping.qml | getCategoryMenu  | viewHelper.shopValue = "+viewHelper.shopValue)
        core.debug(" Shopping.qml | getCategoryMenu | viewHelper.shop_catalog_id = "+viewHelper.shop_catalog_id)
        core.dataController.shopping.getNextList(viewHelper.shopValue,viewHelper.shop_catalog_id,"","",nextListCategoryModelReady)
    }

    function nextListCategoryModelReady(dModel,status,type){
        core.debug("ShopList | nextListCategoryModelReady called | status : " + status + " | dModel.count : " + dModel.count+" | type: "+type);
        if(dModel.count==0){viewController.showScreenPopup(7);return;}
        for (var i=0; i<dModel.count;i++){
            var title = dModel.getValue(i,"title")
            core.debug(">>>>>>>>.> title = "+title)
            core.debug(">>>>>>>>.> category_node_id = "+dModel.getValue(i,"category_node_id"))
        }
        viewHelper.shopValue=dModel.at(0)
        categoryModel=dModel
        viewHelper.shop_category_id=viewHelper.shopValue.category_node_id
        if(status){
            if(type == "category"){
                subCatobj.compVpathSubCategory.delegate=iconTextCenter
                subCatobj.compVpathSubCategory.setModel(viewHelper.blankModel)
                subCatobj.compVpathSubCategory.setModel(dModel)
            }
        }

        if(!isModelUpdated){subCatobj.compVpathSubCategory.currentIndex=0;viewHelper.subCategoryMenuIndex=0}
        else subCatobj.compVpathSubCategory.currentIndex=viewHelper.subCategoryMenuIndex
        getListingMenu()
    }

    function getListingMenu(){
        core.debug("Shopping.qml | getListingMenu | viewHelper.shopValue = "+viewHelper.shopValue)
        core.debug("Shopping.qml | getListingMenu | viewHelper.shop_category_id = "+viewHelper.shop_category_id)
        core.dataController.shopping.getNextList(viewHelper.shopValue,viewHelper.shop_category_id,"","",nextListModelReady)
    }

    function nextListModelReady(dModel,status,type){
        core.debug("Shopping.qml | nextListModelReady dModel.count = "+dModel.count)
        if(dModel.count==0){
            if(viewHelper.isCurtainFirstTime){
                widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
                viewHelper.isCurtainFirstTime=false
            }
            viewController.showScreenPopup(7);
            return;
        }
        viewHelper.setHelpTemplate("shopping_listing");
        itemModel=dModel
        for (var i=0; i<dModel.count;i++){
            var sub_title = dModel.getValue(i,"sub_title")
            var title = dModel.getValue(i,"title")
            var item_id = dModel.getValue(i,"item_id")
            core.debug(">>>>>>>>.> sub_title = "+sub_title)
            core.debug(">>>>>>>>.> item_id = "+item_id)
            core.debug(">>>>>>>>.> title = "+title)
        }
        if(status){
            if(type == "item"){
                listingobj.compVpathListing.delegate=iconText
                listingobj.compVpathListing.setModel(viewHelper.blankModel)
                listingobj.compVpathListing.setModel(dModel)
            }
        }
        if(!isModelUpdated) {listingobj.compVpathListing.currentIndex=0;}
        else  listingobj.compVpathListing.currentIndex=selIndex
        if(viewHelper.isCurtainFirstTime){
            widgetList.setMainMenuParameters(qsTranslate('','mainmenu_menu_tm1'),0,false,viewHelper.mainMenuIndex)
            viewHelper.isCurtainFirstTime=false
        }
        if(!isModelUpdated) closeLoader()
        else synopsisobj.synopsisItem.item.init();
        if(!isModelUpdated && dModel.count>0) listingobj.compVpathListing.forceActiveFocus()
        if(widgetList.isVideoVisible())widgetList.videolanguageUpdate()
        isModelUpdated=false

    }

    function reload(){
        init()
    }

    function clearOnExit(){
        subCatobj.compVpathSubCategory.currentIndex=0;
        subCatobj.compVpathSubCategory.model=viewHelper.blankModel;
        listingobj.compVpathListing.model=viewHelper.blankModel;
        listingobj.compVpathListing.currentIndex=0;
        viewHelper.subCategoryMenuIndex=0;
        pif.paxus.stopContentLog()
    }

    function enterReleased(index){
        core.debug(" viewHelper.shop_category_id before = "+viewHelper.shop_category_id + "index : "+index)
        if(viewHelper.subCategoryMenuIndex==index)return;
        subCatobj.compVpathSubCategory.currentIndex=index
        viewHelper.subCategoryMenuIndex=index
        isSynopsis=false
        subCatobj.compVpathSubCategory.forceActiveFocus()
        viewHelper.shop_category_id=index
        core.debug(" viewHelper.shop_category_id after = "+viewHelper.shop_category_id)
        viewHelper.shopValue=subCatobj.compVpathSubCategory.model.at(subCatobj.compVpathSubCategory.currentIndex)
        core.dataController.shopping.getNextList(viewHelper.shopValue,viewHelper.shop_category_id,"","",nextListModelReady)
    }

    function enterReleasedforListing(index){
        core.debug("Shopping.qml | enterReleasedforListing")
        core.debug("Shopping.qml | enterReleasedforListing | listingobj.compVpathListing.currentIndex = "+listingobj.compVpathListing.currentIndex)
        selIndex=index;
        listingobj.compVpathListing.currentIndex=index
        viewHelper.setMediaListingIndex(index)
        viewHelper.shop_item_id =  listingobj.compVpathListing.model.getValue(index,"item_id");
        core.debug("Shopping.qml | enterReleasedforListing | viewHelper.shop_item_id = "+viewHelper.shop_item_id)
        viewHelper.shopValue=listingobj.compVpathListing.model.at(listingobj.compVpathListing.currentIndex)
        core.dataController.shopping.getItemDetailById(viewHelper.shop_item_id ,synopsisModelReady)
    }

    function synopsisModelReady(dModel,status,type){
        if(status){
            core.debug("ShopList.qml | synopsisModelReady | status : "+status)
            refModel = dModel;
            for(var i=0; i<dModel.count;i++){
                core.debug("ShopList.qml | synopsisModelReady | title : "+dModel.getValue(i,"title"))
                core.debug("ShopList.qml | synopsisModelReady | sub_title : "+dModel.getValue(i,"sub_title"))
            }
            loadSynopsis()

        }else{
            core.debug("ShopList.qml | synopsisModelReady | status : | else "+status)
        }
    }


    function loadSynopsis(){
        viewHelper.setHelpTemplate("shopping_synop");
        if(!isMousePressed) isSynopsis=true;
        else if(isMousePressed)isMousePressed=false
        synopsisobj.sourceComponent=shoppingSynopsisItem
        synopsisobj.synopsisItem.item.init();
    }

    function incrementSelindex(){
        if(selIndex==listingobj.compVpathListing.count-1)selIndex=0
        else  selIndex++
    }
    function decrementSelindex(){
        if(selIndex==0){
            selIndex=listingobj.compVpathListing.count-1
        }else{
            selIndex--
        }
    }
    function closeLoader(){
        viewHelper.setHelpTemplate("shopping_listing");
        if(!isModelUpdated) isSynopsis=false;
        if(synopsisobj.sourceComponent!=blankComp){
            synopsisobj.item.clearOnExit();
        }
        synopsisobj.sourceComponent = blankComp;
        if(!isModelUpdated && listingobj.compVpathListing.count>0)listingobj.compVpathListing.forceActiveFocus();
    }


    property QtObject posterobj
    Component{
        id:compPostersBG
        Item{
            anchors.fill: parent
            property alias staticBG: staticBG
            Image{
                id:shoppingBG
                source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
            }
            Image{
                id:staticBG
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTool.config.shop.listing.background
            }
        }
    }

    property QtObject listingobj
    Component{
        id:compListing
        MediaList{
        }
    }

    property QtObject subCatobj
    Component{
        id:compSubCategory
        SubCategory{
        }
    }


    Component{
        id:iconTextCenter
        Rectangle{
            id:thisDel
            property int offset:qsTranslate('','mainmenu_submenu_margin')
            property string textColor: (subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?configTool.config.menu.submenu.text_color_h:(thisDel.isPressed?configTool.config.menu.submenu.text_color_p:configTool.config.menu.submenu.text_color_n))
            width: (imageHig.btnText.paintedWidth+offset)<qsTranslate('','mainmenu_submenu_w')/5?qsTranslate('','mainmenu_submenu_w')/5:(imageHig.btnText.paintedWidth+offset)
            height:qsTranslate('','mainmenu_submenu_h')
            color: "transparent"
            BorderImage{
                id:imageSel
                width: parent.width
                height:parent.height
                visible: !thisDel.activeFocus
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: thisDel.bottom
                source:viewHelper.subCategoryMenuIndex==index? viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_p:""
                horizontalTileMode:  BorderImage.Stretch
                border.left: sourceSize.width/2-1
                border.right:sourceSize.width/2-1
                border.top: sourceSize.width/2-1
                border.bottom: sourceSize.width/2-1
                Image{
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    source: viewHelper.subCategoryMenuIndex==index? viewHelper.configToolImagePath+configTool.config.menu.submenu.select_icon:""
                }
            }
            SimpleNavBrdrBtn{
                id:imageHig
                width: parent.width
                height: parent.height
                normalImg: ""
                anchors.horizontalCenter: parent.horizontalCenter
                highlightImg:subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_h:""
                pressedImg: viewHelper.configToolImagePath+configTool.config.menu.submenu.btn_p
                buttonText: [value.title,textColor,qsTranslate('','mainmenu_submenu_fontsize')]
                btnText.horizontalAlignment: Text.AlignHCenter
                btnText.verticalAlignment: Text.AlignVCenter
                isHighlight: subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus?true:false
                buttonTextopacity:viewHelper.subCategoryMenuIndex==index || (subCatobj.compVpathSubCategory.currentIndex==index && subCatobj.compVpathSubCategory.activeFocus)?1:0.6
                onEnteredReleased: {
                    enterReleased(index)
                }
            }

            //            function getValue(){
            //                return value;
            //            }
        }
    }

    Component{
        id:iconText
        Item{
            id:thisDelRect
            property int offset:(thisDelRect.x<PathView.view.children[0].x)?-compOffset:compOffset
            property string textColor: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?comptext_h:(thisDel.isPressed?comptext_p:comptext_n))
            width:listingsingleimgwidth
            height:listingobj.compVpathListing.height
            visible: listingobj.compVpathListing.currentIndex==index  && isSynopsis?false:true
            Image{
                id:posterBG
                anchors.centerIn:thisDel
                source: value.item_thumb
                asynchronous: true
                width: qsTranslate('','synopsis_shop_poster_w')
                height: qsTranslate('','synopsis_shop_poster_h')
                scale: listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus  && !listingobj.compVpathListing.moving? 1:(synopsisAnim.running && listingobj.compVpathListing.currentIndex==index)?1:compScale
            }
            SimpleButton{
                id:thisDel
                isHighlight:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus && !listingobj.compVpathListing.moving?true:false
                highlightimg:compImage_h
                normImg: compImage_n
                pressedImg: compImage_p
                anchors.horizontalCenter:  parent.horizontalCenter
                anchors.horizontalCenterOffset: (listingobj.compVpathListing.currentIndex!=index &&  listingobj.compVpathListing.activeFocus)?(listingobj.compVpathListing.count<=2?0:offset):0
                isFocusRequired:!isSynopsis
                Behavior on opacity{
                    NumberAnimation{duration:500}
                }
                onEnteredReleased: {
                    viewHelper.shopValue=value
                    listingobj.compVpathListing.currentIndex=index;
                    listingobj.compVpathListing.select(index);

                }
                onIsPressedChanged: if(isPressed)isMousePressed=true
                onAdjustfocus:synopsisobj.synopsisItem.item.isSliderNavChanged=true
                scale:posterBG.scale;
            }
            ViewText {
                id:titleTxt
                width: listingobj.compVpathListing.currentIndex==index?compText_w2:compText_w
                height:paintedHeight
                anchors.top: parent.top
                anchors.topMargin:listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compImageTM_h:compImageTM_n
                anchors.horizontalCenter: posterBG.horizontalCenter
                varText: [value.title,textColor,listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus?compTextFonsize_h:compTextFonsize_n]
                opacity: (listingobj.compVpathListing.currentIndex==index && listingobj.compVpathListing.activeFocus)?1:compTextOpacity
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                maximumLineCount: 3
                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        thisDel.isPressed=true
                    }
                    onReleased: {
                        thisDel.isPressed=false
                        isMousePressed=true
                        thisDel.enteredReleased()
                    }
                }
            }

        }
    }


    property QtObject synopsisobj
    Component{
        id:compSynopsis
        Loader{
            id:synopsisItem
            width: parent.width
            y:17;
            property alias synopsisItem: synopsisItem
            sourceComponent: blankComp
            onStatusChanged: {
                if(synopsisItem.sourceComponent==blankComp)return;
                if (synopsisItem.status == Loader.Ready){
                    synopsisItem.item.init()
                }
            }
        }
    }

    Component{
        id:shoppingSynopsisItem
        ShoppingSynopsis{
        }
    }

    Component{id:blankComp;Item{}}

    SequentialAnimation{
        id:revsynopsisAnim
        NumberAnimation { targets: [listingobj.compVpathListing,subCatobj.compVpathSubCategory]; properties: "opacity";from:1;to:0; duration: 10 }
        PauseAnimation { duration: 800 }
        NumberAnimation { targets: [listingobj.compVpathListing,subCatobj.compVpathSubCategory]; properties: "opacity";from:0;to:1; duration: 250 }
    }

    SequentialAnimation{
        id:synopsisAnim
        ScriptAction{
            script:{
                if(selIndex%2==1){
                    selprevIndex=selIndex;
                }else{
                    selnextIndex=selIndex;
                }
            }
        }
        NumberAnimation{targets: [!isSynopsis?listingobj.compVpathListing:synopsisobj,synopsisobj];properties: "opacity";from:1;to:0;duration:250}
        ScriptAction{
            script:{
                listingobj.compVpathListing.synopIndex=selIndex;
                listingobj.compVpathListing.currentIndex=selIndex
                enterReleasedforListing(selIndex);
            }
        }
        PauseAnimation { duration: 100 }
        ScriptAction{
            script:{
                isSynopsis=true
            }
        }
        NumberAnimation{targets: [!isSynopsis?listingobj.compVpathListing:synopsisobj,synopsisobj];properties: "opacity";from:0;to:1;duration: 250}
        ScriptAction{
            script:  isMousePressed=false
        }
    }
}
