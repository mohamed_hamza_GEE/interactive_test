import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

SmartLoader{
    id:aviancaApp;

    property string microAppName:(pif.getCabinClassName() === "Economy" ) ? "aviancaApp_economy" : pif.getCabinClassName() === "Business" ?"aviancaApp_business":(pif.getCabinClassName() === "FCRC" && pif.getLruData("XResolution")=="1024x600")?"aviancaApp_economy":"aviancaApp_business"
    property string baseDir: core.configTool.configToolBase+"microapps/";
    property string currAppName:"mainMenu";
    property string microAppDir: Qt.resolvedUrl(core.configTool.base+"microapps/" +microAppName+"/")
    property string langCode : core.settings.languageISO;
    property int gameListingIndex: 0;
    property int lastSelInd: -1;

    // VARIABLES FOR CHILD APP
    property string selectedChildApp:"";
    property bool isChildAppSelected: false;

    Connections{
        target: configTool
        onMicroAppXMLChangedFromThemeMgr:{
            core.log("AviancaApp | XML Changed | currAppName : "+currAppName)
            core.log("AviancaApp | XML Changed | microAppDir : "+microAppDir)
            chlidAppLoader.item.reload();
        }
    }
    function load() {
    }

    function init() {
        core.log("AviancaApp | init ")
        widgetList.showHeader(false);
        widgetList.showMainMenu(false);
        lastSelInd = 0;
        gameListingIndex = viewHelper.mediaListing;
        currAppName="mainMenu";
        chlidAppLoader.sourceComponent = apMainMenu;
        chlidAppLoader.item.setFocus();
    }
    function reload() {}
    function clearOnExit() {
        viewHelper.mediaListing = gameListingIndex;
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
           pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":""/*,"showContOnKarma":viewHelper.showContOnKarma*/});
        }
        isChildAppSelected = false;
        selectedChildApp="";
        chlidAppLoader.sourceComponent = null;
    }
    function actionOnExit(param) {
        if(param){
            viewController.hideScreenPopup();
            if(isChildAppSelected){
                currAppName="mainMenu";
                chlidAppLoader.sourceComponent = apMainMenu;
            }else{
                viewHelper.keepSynopsisOpen=true;
                viewController.loadPrev();
            }
        }else{
            viewController.hideScreenPopup();
        }
    }
    function backBtnPressed(){
        viewHelper.keepSynopsisOpen=true
        switch(chlidAppLoader.sourceComponent){
        case apHistory:
        case apAirport:
        case apNews:
        case apOurFleet:
        case apRouteMap:
            currAppName="mainMenu";
            chlidAppLoader.sourceComponent = apMainMenu;
            break;
        case apMainMenu :
//            currAppName="mainMenu";
            viewHelper.keepSynopsisOpen=true;
            viewController.loadPrev();
            break;
        default:
            viewController.loadPrev();
            break;
        }
    }
    function childAppSwitcher(childApp) {
        switch(childApp){
        case "history" :
            currAppName=childApp;
            chlidAppLoader.sourceComponent = apHistory;
            break;
        case "ourFleet" :
            currAppName=childApp;
            chlidAppLoader.sourceComponent = apOurFleet;
            break;
        case "routesMap" :
            currAppName=childApp;
            chlidAppLoader.sourceComponent = apRouteMap;
            break;
        case "airports" :
            currAppName=childApp;
            chlidAppLoader.sourceComponent = apAirport;
            break;
        case "news" :
            currAppName=childApp;
            chlidAppLoader.sourceComponent = apNews;
            break;
        default :
            currAppName="mainMenu";
            chlidAppLoader.sourceComponent = apMainMenu;
            break
        }
    }
    function languageUpdate(){
        chlidAppLoader.item.languageUpdate()
    }
    Loader{
        height: core.height; width: core.width;
        id:chlidAppLoader;
        onStatusChanged: {
            if(chlidAppLoader.status==Loader.Ready)
                chlidAppLoader.item.init();
        }
    }
    Component{
        id: apMainMenu;
        AppMainMenu{
            onSwitchApp: {
                childAppSwitcher(chlidAppName);
            }
        }
    }
    Component{
        id:apAirport;
        AppAirports{ }
    }
    Component{
        id:apHistory;
        AppHistory { }
    }
    Component{
        id:apNews;
        AppNews{ }
    }
    Component {
        id:apOurFleet;
        AppOurFleet { }
    }
    Component {
        id:apRouteMap;
        AppRoutesMap { }
    }
}

