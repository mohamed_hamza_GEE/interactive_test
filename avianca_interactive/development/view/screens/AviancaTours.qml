import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

SmartLoader{
    id: aviancaLifeMiles;

    property int appWidth: width;
    property int appHeight: height;

    property string microAppName:(pif.getCabinClassName() === "Economy" ) ? "aviancaTour_economy" : pif.getCabinClassName() === "Business" ?"aviancaTour_business":(pif.getCabinClassName() === "FCRC" && pif.getLruData("XResolution")=="1024x600")?"aviancaTour_economy":"aviancaTour_business"
    property string currAppName:"tour";
    property string currAppDataName:currAppName+"_data.xml";
    property string origBaseMicroAppDir: core.configTool.base+"microapps/";
    property string tmpCurrAppDataFileName:currAppDataName
    property string microAppDir: Qt.resolvedUrl(origBaseMicroAppDir+microAppName+"/"+currAppName+"/");
    property string tmpBaseMicroAppDir:"";
    property string baseMicroAppDir: Qt.resolvedUrl(origBaseMicroAppDir+microAppName+"/")
    property string microAppData:microAppName;
    property string childAppPath:origBaseMicroAppDir
    property string langCode : core.settings.languageISO;
    property int currIndex:0;

    property string xmlModelSourceOrg: microAppDir+tmpCurrAppDataFileName;

    property string selCountryScreenID;
    property int selCountryScreenIndex : 0;
    property string detailScreenId;
    property int detailScreenIndex : 0;

    onMicroAppDirChanged: {
        console.log("Main --> microAppDir : " + microAppDir);
        if (microAppDir.indexOf("file:")=== -1){
            microAppDir = "file:///"+microAppDir;
        }
    }

    function load (){}
    function init (){
        core.log("AviancaTours | init ");
        widgetList.showHeader(false);
        widgetList.showMainMenu(false);
        currAppName="tour";
        microappMain.childLoader.sourceComponent = null;
        microappMain.childLoader.forceActiveFocus();
        microappMain.refreshData();
    }
    function backBtnPressed(){
        core.info("AviancaTours | backBtnPressed");
        if(microappMain.childLoader.sourceComponent == appListing){
            detailScreenIndex = 0;
            microappMain.childLoader.sourceComponent = appSelection;
        }else if(microappMain.childLoader.sourceComponent == appDetails){
            microappMain.childLoader.sourceComponent = appListing;
        }else{
            viewHelper.keepSynopsisOpen=true;
            viewController.loadPrev();
        }
    }

    function reload (){}
    function clearOnExit (){
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
            pif.sharedModelServerInterface.sendApiData("userDefinedCheckForTemplate",{"checkTemplate":""/*,"showContOnKarma":viewHelper.showContOnKarma*/});
        }
    }
    function actionOnExit(param) {
        if(param){
            viewHelper.keepSynopsisOpen=true;
            viewController.hideScreenPopup();
            if(microappMain.childLoader.sourceComponent == appListing){
                detailScreenIndex = 0;
                microappMain.childLoader.sourceComponent = appSelection;
            }else if(microappMain.childLoader.sourceComponent == appDetails){
                microappMain.childLoader.sourceComponent = appListing;
            }else{
                microappMain.childLoader.item.setIndex();
                viewController.loadPrev();
            }
        }else{
            viewController.hideScreenPopup();
        }
    }
    function languageUpdate(){
        if(microappMain.childLoader.sourceComponent==appSelection)
            microappMain.refreshModels();
        microappMain.childLoader.item.modelUpdate()
    }


    MicroApp_Tours{
        id:microappMain
    }
    Component{
        id:appSelection;
        ToursSelection{ }
    }
    Component{
        id:appListing;
        ToursListing { }
    }
    Component{
        id:appDetails;
        ToursDetails { }
    }
}
