// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader{
    /**************************************************************************************************************************/
    property variant arrivalInfoModel;
    property bool arrivalInfoReceived:false;
    property bool cgInfoReceived:false;
    property variant configTag:configTool.config.discover.cg;
    /*****************************************************************************************************************************/
    Connections{
        target:(visible)?dataController.connectingGate:null;
        onSigCgStatusChanged:{
            if(status==false){
                viewController.loadPrev();
                return;
            }
            viewController.updateSystemPopupQ(9,true)
            dataController.connectingGate.fetchData(cgModelReady)
            if(cgDetailsView.visible==true){cgDetailsView.visible=false;cgView.visible=true}
        }
    }
    Connections{
        target:(visible)?core.pif:null;
        onCgDataAvailableChanged:{
            if(cgInfoReceived && dataController.connectingGate.getCgModel()==0){
                viewController.updateSystemPopupQ(7,true)
                viewController.loadPrev()
            }
        }
    }
    /*****************************************************************************************************************************/
    function load(){
        push(compPostersBG,"posterobj");
        push(compPanelObj,"panel");
        push(closeComp,"close");
        push(leftPanelComp,"leftPanel");
        push(rightTitleComp,"rightTitle");
        push(cgViewComp,"cgView");
        push(cgDetailsViewComp,"cgDetailsView");
    }
    function init(){
        core.info("ConnectingGate.qml | Init  ")
        viewHelper.setHelpTemplate("connectgate_listing");
        widgetList.showHeader(true);
        widgetList.showMainMenu(true);
        dataController.connectingGate.fetchData(cgModelReady)
        pif.paxus.startContentLog('Connecting Gate')
        cgView.focusToCgList();cgView.refreshIndex(0)
    }
    function reload(){init()}
    function clearOnExit(){
        core.info("ConnectingGate.qml | clearOnExit ")
        pif.paxus.stopContentLog()
        viewHelper.cgFromHeader=false
        cgDetailsView.visible=false
        cgView.visible=true
    }
    function cgModelReady(dModel,status,modelId,errNum){
        core.info("Connecting Gate.qml || getModelDetails() || dModel: "+dModel.count+" || status: "+status+" || modelId: "+modelId+" || errNum: "+errNum);
        if(modelId != "arrivalInfoModel" && dModel.count==0){viewController.updateSystemPopupQ(7,true);backBtnPressed();}
        if(modelId == "arrivalInfoModel"){
            //setArrivalModel(dModel,status,errNum)
            arrivalInfoReceived=true;
            if(dataController.connectingGate.getArrivalInfoModel().count > 0){
                leftPanel.visible=true;
                leftPanel.updateArrDetails(dataController.connectingGate.getArrivalInfoModel().get(0))
            }
        }else{
            setCgModel(dModel,status,errNum);
        }
    }
    function setCgModel(dModel,status,errNum){
        cgInfoReceived=true;
        if(errNum==0 && dModel.count > 0 && status == true){
            cgView.visible=true;
            cgView.setModel(dModel);
            cgView.focusToCgList();
            //leftPanel.updateCGDetails(dModel.get(0))
            core.info(" Connecting Gate | setCgModel | dataController.connectingGate.getArrivalInfoModel().count "+dataController.connectingGate.getArrivalInfoModel().count)
            if(dataController.connectingGate.getArrivalInfoModel().count > 0){
                leftPanel.visible=true;
                leftPanel.updateArrDetails(dataController.connectingGate.getArrivalInfoModel().get(0))
            }
        }else if(arrivalInfoReceived && dataController.connectingGate.getArrivalInfoModel().count==0){
            viewController.updateSystemPopupQ(7,true)
            viewHelper.homeAction()
        }else{
            //either arrivall info is not recived or there is some thing in arrival info, both cases no actions
        }
    }

    function setArrivalModel(dModel,status,errNum){
        arrivalInfoReceived=true;
        if(errNum==0 && dModel.count > 0 && status == true){
            leftPanel.visible=true;
            leftPanel.updateArrDetails(dModel.get(0))
            //don't care if arrival cg info there or not go ahead and least show arrival info
        }else if(cgInfoReceived && dataController.connectingGate.getCgModel()==0){
            //nothing is ready, cg infor is received but model is empty also arrival info model is empty
            viewController.updateSystemPopupQ(7,true)
            viewHelper.homeAction()
        }else{
            //either cg info is not recived or there is some thing in cg info, both cases no actions
        }

    }
    function focusFromMainMenu(){
        if(cgDetailsView.visible){cgDetailsView.focusToBackBtn()}
        else cgView.focusToCgList()
    }
    function backBtnPressed(){
        viewHelper.keepSynopsisOpen=true;
        if(!viewHelper.cgFromHeader){viewController.loadPrev();}
        else {viewController.loadNext(viewData.mainMenuModel,widgetList.getMainMenuRef().mainmenuPathView.currentIndex);}
    }
    property QtObject posterobj
    Component{
        id:compPostersBG
        Item{
            anchors.fill: parent
            property alias staticBG: staticBG
            Image{
                id:listingBG
                source:viewHelper.configToolImagePath+configTool.config.menu.main.bg
            }
            Image{
                id:staticBG
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTool.config.discover.listing.background
            }
        }
    }
    property QtObject panel
    Component{
        id:compPanelObj
        Image{
            id:panelBG
            anchors.horizontalCenter: posterobj.horizontalCenter
            anchors.top:posterobj.top;
            anchors.topMargin: qsTranslate('','connectgate_panel_tm')
            source:viewHelper.configToolImagePath+configTag["panel"]
        }
    }
    property QtObject close
    Component{
        id:closeComp
        SimpleNavButton{
            id:closeObj;
            normImg:viewHelper.configToolImagePath+configTag["close_btn_n"];
            highlightimg:viewHelper.configToolImagePath+configTag["close_btn_h"];
            pressedImg: viewHelper.configToolImagePath+configTag["close_btn_p"];
            anchors.right:panel.right;
            anchors.rightMargin: qsTranslate('',"connectgate_close_rm")
            anchors.top:panel.top;
            anchors.topMargin: qsTranslate('',"connectgate_close_tm")
            nav:["","","",""]
            visible:(cgView.visible)
            onEnteredReleased:backBtnPressed()
            onNoItemFound:{
                if(dir=="down"){
                    if(cgView.getScrollerRef().visible){cgView.getScrollerRef().forceActiveFocus()}
                    else cgView.focusToCgList()
                }
            }
        }
    }
    property QtObject leftPanel
    Component{
        id:leftPanelComp;
        Item{
            id:leftPanelItem
            anchors.left:panel.left;
            anchors.top:panel.top;
            visible:false
            property variant cgArrivalData:["","","","","",""];
            property variant cgArrivalInfo:["","","","","",""];
            property variant dataObj;
            function updateArrDetails(data){
                dataObj=data;
                var d = data["sector_scheduled_arrival"];
                var currTime = "<b>"+d.split(" ")[1]+"<b>";

                cgArrivalData=[
                            "",
                            currTime,
                            getData("gate_designator"),
                            getData("baggage_area_designator"),
                            getData("carrier_code"),
                            getData("flight_number")
                        ];
            }
            function updateCGDetails(data){
                dataObj=data;
                var d = data["sector_scheduled_departure"];
                cgArrivalInfo=[
                            getData("departure_airport_name"),
                            getData("departure_airport_iata_code"),
                            getData("arrival_airport_name"),
                            getData("arrival_airport_iata_code"),
                            getData("flight_number"),
                            getData("carrier_code")
                        ];
            }

            function getData(tag){
                if(dataObj[tag]){
                    return "<b>"+dataObj[tag]+"<b>";
                }else{
                    return "";
                }
            }

            ViewText{
                id:leftTitle
                anchors.left:parent.left
                anchors.top: parent.top;
                anchors.leftMargin:qsTranslate('',"connectgate_left_title_lm");
                anchors.topMargin:qsTranslate('',"connectgate_left_title_tm");
                width:qsTranslate('',"connectgate_left_title_w");
                height:qsTranslate('',"connectgate_left_title_h");
                varText:[configTool.language.connecting_gate.arrival_information,configTag["title_color"],qsTranslate('',"connectgate_left_title_fontsize")]
            }
            ViewText{
                id:subtitle
                anchors.left:leftTitle.left;
                anchors.top: parent.top;
                anchors.topMargin:qsTranslate('',"connectgate_left_subtitle_tm");
                width:qsTranslate('',"connectgate_left_subtitle_w");
                height:qsTranslate('',"connectgate_left_subtitle_h");
                varText:[pif.getSourceIATA()+" - "+pif.getDestinationIATA()+" "+"Avianca "+leftPanelItem.cgArrivalData[4]+" "+leftPanelItem.cgArrivalData[5],configTag["short_title_color"],qsTranslate('',"connectgate_left_subtitle_fontsize")]
                maximumLineCount:4;
                wrapMode: "WordWrap";
                font.bold: false
                elide: Text.elideRight;
            }
            Column{
                id:desc
                property variant descLabel:[configTool.language.connecting_gate.arrival_terminal+": ",configTool.language.connecting_gate.arrival_time+": ",configTool.language.connecting_gate.header_gate+": ",configTool.language.connecting_gate.baggage_carousel+": "]
                anchors.top: subtitle.bottom;
                anchors.left:leftTitle.left;
                anchors.topMargin:qsTranslate('',"connectgate_desc_tm");
                spacing:qsTranslate('',"connectgate_desc_vgap")
                Repeater{
                    model:4;
                    ViewText{
                        width:qsTranslate('',"connectgate_desc_w");
                        height:qsTranslate('',"connectgate_desc_h");
                        varText:[desc.descLabel[index]+leftPanelItem.cgArrivalData[index],configTag["description1_color"],qsTranslate('',"connectgate_desc_fontsize")]
                        visible:(leftPanelItem.cgArrivalData[index]!="")
                    }
                }
            }
            ViewText{
                id:note
                anchors.left:leftTitle.left;
                anchors.top: parent.top;
                anchors.topMargin:qsTranslate('',"connectgate_note_tm");
                width:qsTranslate('',"connectgate_note_w");
                height:qsTranslate('',"connectgate_note_h");
                varText:[configTool.language.connecting_gate.baggage_info,configTag["description1_color"],qsTranslate('',"connectgate_note_fontsize")]
                maximumLineCount:5;
                wrapMode: "WordWrap";
                elide: Text.elideRight;
            }
            Rectangle{
                color:configTag["description1_color"];
                anchors.left:leftTitle.left;
                anchors.top: parent.top;
                anchors.topMargin:qsTranslate('',"connectgate_divider_tm");
                width:qsTranslate('',"connectgate_divider_w");
                height:qsTranslate('',"connectgate_divider_h");
            }
        }
    }
    property QtObject rightTitle
    Component{
        id:rightTitleComp;
        ViewText{
            id:rightTitleObj
            anchors.left:panel.left
            anchors.top: panel.top;
            anchors.leftMargin:qsTranslate('',"connectgate_right_title_lm");
            anchors.topMargin:qsTranslate('',"connectgate_right_title_tm");
            width:qsTranslate('',"connectgate_right_title_w");
            height:qsTranslate('',"connectgate_right_title_h");
            varText:[configTool.language.connecting_gate.connecting_flights,configTag["title_color"],qsTranslate('',"connectgate_right_title_fontsize")]
        }
    }
    property QtObject cgView
    Component{
        id:cgViewComp;
        FocusScope{
            anchors.top:panel.top;
            anchors.left:rightTitle.left;
            function focusToCgList(){cgList.forceActiveFocus()}
            function setModel(dmodel){
                cgList.model=dmodel;
            }
            function getScrollerRef(){return cgScroll}
            function refreshIndex(val){cgList.currentIndex=val}
            ViewText{
                id:flight
                anchors.top: parent.top;
                anchors.left:cgList.left
                anchors.leftMargin:qsTranslate('',"connectgate_list_flight_lm")
                anchors.topMargin:qsTranslate('',"connectgate_flight_tm")
                width:qsTranslate('',"connectgate_list_flight_w");
                height:qsTranslate('',"connectgate_list_flight_h");
                varText:[configTool.language.connecting_gate.flight,configTag["short_title_color"],qsTranslate('',"connectgate_flight_fontsize")]
            }
            ViewText{
                id:destination
                anchors.top: flight.top;
                anchors.left:cgList.left
                anchors.leftMargin:qsTranslate('',"connectgate_list_dest_lm")
                width:qsTranslate('',"connectgate_list_dest_w");
                height:qsTranslate('',"connectgate_list_dest_h");
                varText:[configTool.language.connecting_gate.destination,configTag["short_title_color"],qsTranslate('',"connectgate_destination_fontsize")]
            }
            ViewText{
                id:time
                anchors.top: flight.top;
                anchors.left:cgList.left
                anchors.leftMargin:qsTranslate('',"connectgate_list_time_lm")
                width:qsTranslate('',"connectgate_list_time_w");
                height:qsTranslate('',"connectgate_list_time_h");
                varText:[configTool.language.connecting_gate.departure_time,configTag["short_title_color"],qsTranslate('',"connectgate_time_fontsize")]
            }
            ViewText{
                id:gate
                anchors.top: flight.top;
                anchors.left:cgList.left
                anchors.leftMargin:qsTranslate('',"connectgate_list_gate_lm")
                width:qsTranslate('',"connectgate_list_gate_w");
                height:qsTranslate('',"connectgate_list_gate_h");
                varText:[configTool.language.connecting_gate.header_gate,configTag["short_title_color"],qsTranslate('',"connectgate_gate_fontsize")]
            }
            VerticalView{
                id:cgList
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('',"connectgate_listview_tm");
                width:qsTranslate('',"connectgate_listview_w");
                height:qsTranslate('',"connectgate_listview_h");
                focus:true;
                Rectangle{
                    width:parent.width;
                    height:qsTranslate('',"connectgate_divider_h")
                    color:configTag["description1_color"]
                }
                function select(){
                    viewHelper.setHelpTemplate("connectgate_details");
                    cgView.visible=false;
                    cgDetailsView.visible=true;
                    cgDetailsView.focusToBackBtn()
                    cgDetailsView.updateCgDetails(model.get(currentIndex))
                    cgDetailsView.forceActiveFocus();
                }
                Keys.onRightPressed: {
                    if(cgScroll.visible){
                        cgScroll.forceActiveFocus()
                    }else{
                        close.forceActiveFocus();
                    }
                }
                Keys.onUpPressed: {
                    if(cgList.currentIndex==0) {widgetList.focustTomainMenu()}
                    else{decrementCurrentIndex()}
                }
                Keys.onReleased: {
                    if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
                        select();
                    }
                }
                delegate:Item{
                    width:qsTranslate('',"connectgate_list_item_w");
                    height:qsTranslate('',"connectgate_list_item_h");
                    Image{
                        id:highalbum
                        source:viewHelper.configToolImagePath+configTag["listing_highlight"];
                        anchors.bottom: parent.bottom;
                        anchors.bottomMargin:qsTranslate('','connectgate_list_high_bm')
                        height:qsTranslate('','connectgate_list_high_h');
                        width:parent.width;
                        opacity:(cgList.currentIndex==index && cgList.activeFocus)?1:0
                    }
                    Rectangle{
                        width:parent.width;
                        height:qsTranslate('',"connectgate_divider_h")
                        color:configTag["list_text_color"]
                        anchors.bottom:parent.bottom;
                    }
                    ViewText{
                        id:list_flight
                        x:qsTranslate('',"connectgate_list_flight_lm")
                        width:qsTranslate('',"connectgate_list_flight_w");
                        height:qsTranslate('',"connectgate_list_flight_h");
                        anchors.verticalCenter: parent.verticalCenter;
                        varText:[carrier_code+" "+flight_number,configTag["description1_color"],qsTranslate('',"connectgate_list_flight_fontsize")]
                    }
                    ViewText{
                        id:list_destination
                        x:qsTranslate('',"connectgate_list_dest_lm")
                        width:qsTranslate('',"connectgate_list_dest_w");
                        height:qsTranslate('',"connectgate_list_dest_h");
                        anchors.verticalCenter: parent.verticalCenter;
                        varText:[arrival_city,configTag["description1_color"],qsTranslate('',"connectgate_list_dest_fontsize")]
                    }
                    ViewText{
                        id:list_time
                        x:qsTranslate('',"connectgate_list_time_lm")
                        width:qsTranslate('',"connectgate_list_time_w");
                        height:qsTranslate('',"connectgate_list_time_h");
                        anchors.verticalCenter: parent.verticalCenter;
                        property variant dateObj:new Date(sector_scheduled_departure*1000);
                        property string currTime:dateObj.getHours()+":"+dateObj.getMinutes();
                        varText:[currTime,configTag["description1_color"],qsTranslate('',"connectgate_list_time_fontsize")]
                    }
                    ViewText{
                        id:list_gate
                        x:qsTranslate('',"connectgate_list_gate_lm")
                        width:qsTranslate('',"connectgate_list_gate_w");
                        height:qsTranslate('',"connectgate_list_gate_h");
                        anchors.verticalCenter: parent.verticalCenter;
                        varText:[departure_gate_designator,configTag["description1_color"],qsTranslate('',"connectgate_list_gate_fontsize")]
                    }
                    MouseArea{
                        anchors.fill:parent;
                        onClicked:{
                            cgList.currentIndex=index;
                            cgList.select();
                        }
                    }
                }
            }


            Scroller{
                id:cgScroll
                targetData:cgList;
                sliderbg:[qsTranslate('','connectgate_slider_base_margin'),qsTranslate('','connectgate_slider_w'),qsTranslate('','connectgate_slider_base_h'),configTag["scroll_base"]]
                scrollerDot:configTag["scroll_button"]
                upArrowDetails:[configTag["up_arrow_n"],configTag["up_arrow_h"],configTag["up_arrow_p"]]
                downArrowDetails:[configTag["dn_arrow_n"],configTag["dn_arrow_h"],configTag["dn_arrow_p"]]
                anchors.left:cgList.right;
                anchors.leftMargin:qsTranslate('','connectgate_slider_rm');
                height: qsTranslate('','connectgate_slider_h');
                width: qsTranslate('','connectgate_slider_w');
                anchors.verticalCenter: cgList.verticalCenter;
                onSliderNavigation:{
                    if(direction=="down")
                        return;
                    else if(direction=="up")
                        close.forceActiveFocus()
                    else if(direction=="right"){
                        return;
                    }
                    else if(direction=="left"){
                        cgList.forceActiveFocus()
                    }
                }

            }
            BorderImage{
                source:viewHelper.configToolImagePath+configTag["fading_image"];
                anchors.bottom:cgList.bottom;
                anchors.horizontalCenter: cgList.horizontalCenter;
                border.left:qsTranslate('','common_scrollfade_margin')
                border.top:qsTranslate('','common_scrollfade_margin')
                border.right:qsTranslate('','common_scrollfade_margin')
                border.bottom:qsTranslate('','common_scrollfade_margin')
                width:(parseInt(qsTranslate('','common_scrollfade_extra_w'),10)+cgList.width);
                visible: cgScroll.visible?(cgScroll.downArrow.isDisable?false:true):false

            }

        }

    }
    property QtObject cgDetailsView
    Component{
        id:cgDetailsViewComp
        FocusScope{
            anchors.left:rightTitle.left;
            anchors.top:panel.top;
            visible:false;
            function focusToBackBtn(){backBtn.forceActiveFocus()}
            property variant cgDetailData:["","","","","","",""];
            property variant dataObj;
            function updateCgDetails(data){
                var d = new Date(data["sector_scheduled_departure"]*1000)
                var currTime = "<b>"+d.getHours()+":"+d.getMinutes()+"<b>";
                var day = "<b>"+d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+"<b>";
                dataObj=data;

                cgDetailData=[
                            getData("arrival_city"),
                            getData("operator_name"),
                            getData("carrier_code")+" "+getData("flight_number"),
                            currTime,
                            getData("departure_gate_designator"),
                            day,
                            getData("sector_status")];
            }
            function getData(tag){
                if(dataObj[tag]){
                    return "<b>"+dataObj[tag]+"<b>";
                }else{
                    return "";
                }

            }



            ViewText{
                anchors.top:parent.top;
                anchors.left:parent.left;
                anchors.topMargin:qsTranslate('','connectgate_right_subtitle_tm');
                width:qsTranslate('','connectgate_right_subtitle_w');
                height:qsTranslate('','connectgate_right_subtitle_h');
                varText:[configTool.language.connecting_gate.destination+": "+cgDetailData[0],configTag["description1_color"],qsTranslate('','connectgate_right_subtitle_fontsize')]
                visible:(cgDetailData[0]!="");

            }

            ViewText{
                anchors.top:parent.top;
                anchors.left:parent.left;
                anchors.topMargin:qsTranslate('','connectgate_flight_name_tm');
                width:qsTranslate('','connectgate_flight_name_w');
                height:qsTranslate('','connectgate_flight_name_h');
                varText:[cgDetailData[1],configTag["description1_color"],qsTranslate('','connectgate_flight_name_fontsize')]
                visible:(cgDetailData[1]!="");
            }

            ViewText{
                anchors.top:parent.top;
                anchors.left:parent.left;
                anchors.topMargin:qsTranslate('','connectgate_flight_number_tm');
                width:qsTranslate('','connectgate_flight_number_w');
                height:qsTranslate('','connectgate_flight_number_h');
                varText:[configTool.language.connecting_gate.flight_number+": "+cgDetailData[2],configTag["description1_color"],qsTranslate('','connectgate_flight_number_fontsize')]
                visible:(cgDetailData[2]!=" ");
            }

            ViewText{
                anchors.top:parent.top;
                anchors.left:parent.left;
                anchors.topMargin:qsTranslate('','connectgate_flight_time_tm');
                width:qsTranslate('','connectgate_flight_time_w');
                height:qsTranslate('','connectgate_flight_time_h');
                varText:[configTool.language.connecting_gate.schedule_dept_time+": "+cgDetailData[3],configTag["description1_color"],qsTranslate('','connectgate_flight_time_fontsize')]
                visible:(cgDetailData[3]!="");
            }

            ViewText{
                anchors.top:parent.top;
                anchors.left:parent.left;
                anchors.topMargin:qsTranslate('','connectgate_flight_gate_tm');
                width:qsTranslate('','connectgate_flight_gate_w');
                height:qsTranslate('','connectgate_flight_gate_h');
                varText:[configTool.language.connecting_gate.departure_gate+": "+cgDetailData[4],configTag["description1_color"],qsTranslate('','connectgate_flight_gate_fontsize')]
                visible:(cgDetailData[4]!="");
            }

            ViewText{
                id:flight_date
                anchors.top:parent.top;
                anchors.left:parent.left;
                anchors.topMargin:qsTranslate('','connectgate_flight_date_tm');
                width:(paintedWidth>width)?qsTranslate('','connectgate_flight_date_w'):paintedWidth;
                height:qsTranslate('','connectgate_flight_date_h');
                varText:[configTool.language.connecting_gate.schedule_dept_date+": "+cgDetailData[5],configTag["description1_color"],qsTranslate('','connectgate_flight_date_fontsize')]
                visible:(cgDetailData[5]!="");
            }





            ViewText{
                anchors.top:parent.top;
                anchors.left:parent.left;
                anchors.topMargin:qsTranslate('','connectgate_flight_status_tm');
                width:qsTranslate('','connectgate_flight_status_w');
                height:qsTranslate('','connectgate_flight_status_h');
                varText:[configTool.language.connecting_gate.status+": "+cgDetailData[6],configTag["description1_color"],qsTranslate('','connectgate_flight_status_fontsize')]
                visible:(cgDetailData[6]!="");
            }

            SimpleNavBrdrBtn{
                id:backBtn
                normalImg: viewHelper.configToolImagePath+configTag["btn_n"];
                highlightImg:viewHelper.configToolImagePath+configTag["btn_h"];
                pressedImg:viewHelper.configToolImagePath+configTag["btn_p"];
                height:qsTranslate('',"connectgate_back_h");
                width:qsTranslate('',"connectgate_back_w");
                btnTextWidth:parseInt(qsTranslate('',"connectgate_back_textw"),10);
                buttonText: [configTool.language.connecting_gate.back,(isPressed?configTag["btn_text_p"]:isHighlight?configTag["btn_text_h"]:configTag["btn_text_n"]),qsTranslate('','connectgate_back_fontsize')]
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('',"connectgate_back_tm")
                normalBorder:[qsTranslate('',"connectgate_back_margin")]
                highlightBorder:[qsTranslate('',"connectgate_back_margin")]
                elide:Text.ElideRight
                nav: ["","","",""]
                onNoItemFound: {
                    if(dir=="up"){widgetList.focustTomainMenu()}
                }

                onEnteredReleased:{
                    cgDetailsView.visible=false;
                    cgView.visible=true;
                    cgView.forceActiveFocus();
                    viewHelper.setHelpTemplate("connectgate_listing");
                }


            }


        }
    }
}


