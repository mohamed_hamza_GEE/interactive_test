// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import Qt 4.7
import "../../framework/viewcontroller"
import "../components"
SmartLoader{
    id:entOffScreen
    property variant configTag:configTool.config.global;
    function init(){
        pif.paxus.interactiveScreenLog("EntOff");
        viewHelper.hideAndroidBar();
        if(pif.getOpenFlight()==1){
            widgetList.showHeader(false);
            widgetList.showMainMenu(false);
        }
       var ind = viewHelper.welComeIndex;
       core.log("EntOnOff.qml | init | language_locale "+dataController.getIntLanguageModel().get(ind).language_locale)
       core.setInteractiveLanguage(dataController.getIntLanguageModel().get(ind).LID,dataController.getIntLanguageModel().get(ind).ISO639,false,dataController.getIntLanguageModel().get(ind).language_locale);
    }
    function clearOnExit(){
        core.log("EntOnOff.qml | init")
    }
    function reload(){init();}

    Image {
        id: entOff
        source:  viewHelper.configToolImagePath+configTag.entertainment_off.entoff_bg;
        Image{
            anchors.top: parent.top
            anchors.topMargin: qsTranslate('','welcome_ent_logo_tm')
            anchors.horizontalCenter: parent.horizontalCenter
            source: viewHelper.configToolImagePath+configTag.entertainment_off.entoff_logo

            //viewHelper.configToolImagePath+(viewHelper.isAviancaBrasilAir?configTag.entertainment_off.entoff_oceanair_logo:configTag.entertainment_off.entoff_logo);
        }
    }
}
