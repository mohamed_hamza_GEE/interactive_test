import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

ExtPopup{

    id:externalPop;
    anchors.fill:parent;

    property string title;
    property string textColor;
    property variant message:["","","",10];
    property int offset:0;
    property alias bg: bg;
    property alias popupText:popupText;
    property alias viewHelper:viewHelper;

    Rectangle{
        id:bg
        x:(extAppWidth/2-(parseInt(qsTranslate('','popups_bg_w'),10)/2));
        y:(extAppHeight/2-(parseInt(qsTranslate('','popups_bg_h'),10)/2));
        width:parseInt(qsTranslate('','popups_bg_w'),10);
        height:parseInt(qsTranslate('','popups_bg_h'),10);

        ViewText{
            id:popupText
            anchors.centerIn:parent;
            varText:[title,textColor,qsTranslate('','popups_text_fontsize')]
            width: qsTranslate('','popups_text_w')
            height:qsTranslate('','popups_text_h')
            horizontalAlignment:Text.AlignHCenter;
            verticalAlignment: Text.AlignVCenter;
            maximumLineCount: 2;
            lineHeight: qsTranslate('','popups_text_lh')
            wrapMode:"WordWrap";
            lineHeightMode: "FixedHeight"
            elide:Text.ElideRight
            font.family:viewHelper.isKids(current.template_id)?viewHelper.kidsFont: viewHelper.adultFont;
        }
    }



    Item{
        id:viewHelper;
        property string adultFont:"FS Elliot";
        property string kidsFont:"FS Elliot"
        property bool isKidsStr:false
        function isKids(tnode){
            return isKidsStr
        }
    }
    Item{
        id:current;
        property string template_id:""
    }

    function init(str){

        message = str.split("|")

        if(message[5]){
            cticon.source=message[5];
            bg.width = parseInt(qsTranslate('','popups_bg_w'),10)+cticon.width/2;
            bg.height = parseInt(qsTranslate('','popups_bg_h'),10)+cticon.height;
        }
        title=message[0];
        bg.color=message[1];
        textColor=message[2];
        viewHelper.isKidsStr=message[3]
        visible=true;
        forceActiveFocus();
        setPopupDimension(0,0,1024,600);
    }

    function clearOnExit(){
        visible=false;
    }
}
