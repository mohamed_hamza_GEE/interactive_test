import QtQuick 1.0

PA {
    id:cttoSeatMessage
    offset:(cticon.width/2)
    property variant extFonts:["fs_elliotbolditalic.ttf","fs_elliotlightitalic.ttf","GosmickSansBoldOblique.ttf","fs_elliotbold.ttf","fs_elliotlight.ttf","GosmickSansBold.ttf","fs_elliotheavyitalic.ttf","fs_elliotregular.ttf","GosmickSansOblique.ttf","fs_elliotheavy.ttf","fs_elliotthinitalic.ttf","GosmickSans.ttf","fs_elliotitalic.ttf","fs_elliotthin.ttf"]
    property string intPath:"/tmp/interactive/"
    function init(str){
        message = str.split("|")
        __loadFonts();
        console.log("Ct to seat message ::::::::::::::::::::::::::::::::::::::::::::::::"+message[0]+", mesaage 3 :"+message[3])

        if(message[5]){
            cticon.source=message[5];
            bg.width = parseInt(qsTranslate('','popups_bg_w'),10)+cticon.width/2;
            bg.height = parseInt(qsTranslate('','popups_bg_h'),10)+cticon.height;
        }
        title=message[0];
        bg.color=message[1];
        textColor=message[2];
        viewHelper.isKidsStr=message[3]
        visible=true;
        forceActiveFocus();
        setPopupDimension(0,0,1024,600);
//        setPopupDimension(0,0,1368,768);
        popupText.maximumLineCount= 6
        popupText.height= popupText.paintedHeight
        //bg.height=  cticon.height+popupText.height+cticon.height/2
        popupText.anchors.centerIn=null
        popupText.anchors.top=bg.top
        popupText.anchors.topMargin=cticon.height
        hideTimer.restart();
    }
    Image{
        id:cticon
        anchors.top:bg.top;
        anchors.horizontalCenter:bg.horizontalCenter;
    }

    Timer{
        id:hideTimer
        interval: (message[4]*1000);
        onTriggered:{
            closePopup();
           // viewController.updateSystemPopupQ(2,false);
        }
    }
//    onVisibleChanged:{
//        if(visible){
//            hideTimer.restart();
//        }
//    }

    /*
    FontLoader{
        id: defaultFont1;
         source : "/tmp/interactive/fonts/fs_elliotbolditalic.ttf";
    }
    FontLoader{
        id: defaultFont2;
         source : "/tmp/interactive/fonts/fs_elliotlightitalic.ttf";
    }
    FontLoader{
        id: defaultFont3;
         source : "/tmp/interactive/fonts/GosmickSansBoldOblique.ttf";
    }
    FontLoader{
        id: defaultFont4;
         source : "/tmp/interactive/fonts/fs_elliotbold.ttf";
    }
    FontLoader{
        id: defaultFont5;
         source : "/tmp/interactive/fonts/fs_elliotlight.ttf";
    }
    FontLoader{
        id: defaultFont6;
         source : "/tmp/interactive/fonts/GosmickSansBold.ttf";
    }
    FontLoader{
        id: defaultFont7;
         source : "/tmp/interactive/fonts/fs_elliotheavyitalic.ttf";
    }
    FontLoader{
        id: defaultFont8;
         source : "/tmp/interactive/fonts/fs_elliotregular.ttf";
    }
    FontLoader{
        id: defaultFont9;
         source : "/tmp/interactive/fonts/GosmickSansOblique.ttf";
    }
    FontLoader{
        id: defaultFont10;
         source : "/tmp/interactive/fonts/fs_elliotheavy.ttf";
    }
    FontLoader{
        id: defaultFont11;
         source : "/tmp/interactive/fonts/fs_elliotthinitalic.ttf";
    }
    FontLoader{
        id: defaultFont12;
         source : "/tmp/interactive/fonts/GosmickSans.ttf";
    }
    FontLoader{
        id: defaultFont13;
         source : "/tmp/interactive/fonts/fs_elliotitalic.ttf";
    }
    FontLoader{
        id: defaultFont14;
         source : "/tmp/interactive/fonts/fs_elliotthin.ttf";
    }*/
    function __loadFonts(){
        console.log("CttoSeatMessage.qml | Loading Fonts...");
        var fontList = extFonts;
        for (var j=0;j<fontList.length;j++){
            Qt.createQmlObject('import QtQuick 1.1;  FontLoader{id:fon; source:  "'+intPath+'fonts/'+fontList[j]+'"}',parent);
        }
    }

}
