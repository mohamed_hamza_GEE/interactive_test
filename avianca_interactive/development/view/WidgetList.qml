import QtQuick 1.1
import "widgets"

// Contains interactive specific screen holders

Item {
    function getWidgetList () {
        var cList=new Object();
        // cList[0] = {"compId":compHelp, "id": "help", "z-index":15};

        cList[0] = {"compId":compHeader, "id": "header", "z-index":15};
        cList[1]={"compId":compMainMenu,"id":"mainMenu","z-index":13};
        cList[2]={"compId":compVideo,"id":"video","z-index":14};
        cList[3]={"compId":compsoundTrackSubtitle,"id":"soundTrackSubtitle","z-index":16};
        cList[4]={"compId":compHandsetBar,"id":"handsetbar","z-index":17};
        cList[5]={"compId":compAppLoading,"id":"appLoading","z-index":19};
        cList[6]={"compId":compFooter,"id":"footer","z-index":17};
        cList[7] = {"compId":compUsbSlideshow,"id":"slideShow","z-index":16};
        cList[8] = {"compId":compFlightMap,"id":"flightMap","z-index":16};
        cList[9] = {"compId":compHelp,"id":"help","z-index":19};
        cList[10]={"compId":compVolumeBar,"id":"volumebar","z-index":17};
        cList[11]={"compId":compVkb,"id":"vkb","z-index":17};
        cList[12]={"compId":compMeal,"id":"meal","z-index":16};
        cList[13] = {"compId":compBackground, "id": "background", "z-index":-1};
        return cList;
    }
    /*********************************Common******************************************/
    function closeAllwidgets(){
        showMealMenu(false);
        showHeader(false)
        showMainMenu(false)
        showVideo(false)
        showsoundTrackSubtitle(false)
        showHandsetBar(false)
        showUsbSlideshow(false)
        showFlightMap(false)

    }
    function resetAllWidgets(){
        resetHeader()
        resetMainMenu()
        resetVideo()
        resetSoundTrack()
        resetHandsetBar()
        showVkb(false)

    }
    /*******************************FOOTER**********************************************/
    function getFooterRef(){return footer?footer:false}
    function isFooterVisible(){if(getFooterRef()) return  getFooterRef()?getFooterRef().visible:tempLV}

    /*******************************HEADER********************************************/
    function getHeaderRef(){return header;}
    function showHeader(flag){getHeaderRef().showHeader(flag);}
    function resetHeader(){getHeaderRef().reset();}
    function focusToHeader(){getHeaderRef().focusToHeader()}
    function getHeaderBtnFocus(){return getHeaderRef().getHeaderBtnFocus()}
    function getisFlightInfoOpen(){return getHeaderRef().isFlightInfoOpen}
    function setisFlightInfoOpen(param){ getHeaderRef().isFlightInfoOpen=param}
    function showHelp(){ getHeaderRef().showHelp()}
    /*******************************MainMenu********************************************/
    function getMainMenuRef(){return mainMenu?mainMenu:tempLV}
    function showMainMenu(flag){getMainMenuRef().showMainMenu(flag);}
    function isCurtainUp(){return getMainMenuRef().isCurtainUp;}
    function mainMenuPosition(){getMainMenuRef().mainMenuPosition();}
    function setCurrentIndexForUsb(){getMainMenuRef().setCurrentIndexForUsb();}
    function setCurrentIndexForDiscover(){getMainMenuRef().setCurrentIndexForDiscover()}
    function getMainMenuCurrentIndex(){getMainMenuRef().getMainMenuCurrentIndex()}
    function setMainMenuCurrentIndex(cind){getMainMenuRef().setMainMenuCurrentIndex(cind)}
    function resetMainMenu(){getMainMenuRef().reset();}
    function setMainMenuParameters(topMargin,leftMargin,animReq,cIndex,isModelUpdated){getMainMenuRef().setMainMenuParameters(topMargin,leftMargin,animReq,cIndex,isModelUpdated);}
    function getCidTidMainMenu(){return getMainMenuRef().getCidTid()!=undefined?getMainMenuRef().getCidTid():["","movies"];}
    function focustTomainMenu(){getMainMenuRef().focustTomainMenu()}
    function setBlankModel(){getMainMenuRef().setBlankModel()}
    function mainMenuEnterReleased(index){getMainMenuRef().enterReleased(index)}
    /*******************************Video********************************************/
    function getVideoRef(){return video?video:tempLV.get(0);}
    function showVideo(flag){getVideoRef().showVideo(flag);}
    function resetVideo(){if(getVideoRef())getVideoRef().reset();}
    function isVideoVisible(){if(getVideoRef()) return getVideoRef().visible;}
    function focusToVideo(param){getVideoRef().focusToVideo(param)}
    function actionOnVOD(param){getVideoRef().actionOnVOD(param)}
    function videolanguageUpdate(){getVideoRef().languageUpdate()}
    function videobackBtnPressed(param){getVideoRef().backBtnPressed(param)}
    function stopVod(){getVideoRef().stopVod();}
    /*******************************USB slideShow*************************************************/
    function getUsbSlideshowRef(){return slideShow}
    function showUsbSlideshow(flag){getUsbSlideshowRef().showUsbSlideshow(flag)}
    function closeSlideShow(){getUsbSlideshowRef().closeSlideShow()}
    function isUsbSlideShowOn(){return getUsbSlideshowRef().visible}
    /********************************flightMap*****************************************************/
    function getFlightMapRef(){return flightMap}
    function showFlightMap(flag){getFlightMapRef().showFlightMap(flag)}
    function isFlightMapOn(){return getFlightMapRef().visible}
    /*******************************soundTrackSubtitle********************************************/
    function getSoundTrackSubtitleRef(){return soundTrackSubtitle;}
    function isSoundTrackSubtitleOpen(){return getSoundTrackSubtitleRef().visible;}
    function showsoundTrackSubtitle(flag){getSoundTrackSubtitleRef().showsoundTrackSubtitle(flag);}
    function resetSoundTrack(){getSoundTrackSubtitleRef().reset();}
    function focusTosoundTrackSubtitle(){getSoundTrackSubtitleRef().focusTosoundTrackSubtitle()}
    function closeSoundTrack(){getSoundTrackSubtitleRef().close()}
    function soundTrackinit(){getSoundTrackSubtitleRef().init()}
    function setSndtemplateId(tid){soundTrackSubtitle.setSndtemplateId(tid)}
    /*******************************HandsetBar********************************************/
    function getHandsetbarRef(){return handsetbar;}
    function showHandsetBar(flag){getHandsetbarRef().isVisble(flag);}
    function resetHandsetBar(){getHandsetbarRef().reset();}
    /******************************Volumebar***********************************************/
    function getVolumebarRef(){return volumebar;}
    function showVolumeBar(flag){getVolumebarRef().showVolumeBar(flag);}
    function isVolumeVisible(){return getVolumebarRef().isVisble();}
    /*******************************Loading********************************************/
    function showAppLoading(flag){appLoading.visible=flag;}
     function getshowAppLoading(){return appLoading.visible;}
    /*******************************Help********************************************/

    function getHelpStatus(){
        return help?help.opacity:0
    }
    function setHelpStatus(flag){
        if(help){
            help.visible=flag
            header.stopVODTimer(flag)
        }
    }
    /*******************************VKB********************************************/
    function getVkbRef(){return vkb}
    function showVkb(val){getVkbRef().showVkb(val)}

    function getMealStatus(){
        return meal.visible
    }

    function mealBack(){
        meal.mealBack()
    }

    function showMealMenu(flag){
        meal.showMealMenu(flag)

    }

    ListModel{
        id:tempLV
        ListElement{visible: false}
    }


    property QtObject background
    Component{
        id:compBackground
        Image{
            id:bg
            property variant configTag: configTool.config
            property variant configMenu: (viewHelper.isKids(current.template_id))?"kids_menu":"menu"
            source:viewHelper.configToolImagePath+configTag[configMenu].main.bg
        }
    }

    property QtObject header
    Component{
        id: compHeader
        Header{
            id:header
            visible:false;
        }
    }
    property QtObject video
    Component{
        id: compVideo
        Video{
            id:video
            visible:false;
            property alias video: video
        }
    }
    property QtObject soundTrackSubtitle
    Component{
        id: compsoundTrackSubtitle
        SoundTrackSubtitleSelection{
            id:soundTrackSubTitle
            visible:false;
        }
    }
    property QtObject mainMenu
    Component{
        id: compMainMenu
        Mainmenu{
            id:mainMenu
            visible:false;
        }
    }
    property QtObject handsetbar
    Component{
        id: compHandsetBar
        HandsetBars{
            id:handsetbar
            visible:false;
        }
    }
    property QtObject volumebar
    Component{
        id: compVolumeBar
        VolumeBar{
            id:volumebar
            visible:false;
        }
    }

    property QtObject appLoading
    Component{
        id: compAppLoading
        AppLoading{
            id:appLoad
            visible:false;
        }
    }

    property QtObject footer
    Component{
        id: compFooter
        Footer{
            id:footer
            visible:core.pc
        }
    }
    property QtObject slideShow
    Component{
        id:compUsbSlideshow
        UsbSlideShow{
            id:slideShow
            visible: false
        }

    }
    property QtObject flightMap
    Component{
        id:compFlightMap
        Voyager{
            id:flightMap
            visible: false
        }
    }

    property QtObject help
    Component{
        id:compHelp
        Image{
            visible:false;
            source:(visible)?viewHelper.configToolImagePath+viewHelper.getHelpImage():"";
            opacity:(status==Image.Ready)?1:0;
            anchors.fill:parent;
            Behavior on opacity{
                NumberAnimation{duration:10;}
            }


            Text{
                anchors.centerIn:parent;
                text:viewHelper.showHelpElements();
                color:"green"
                font.pixelSize:20;
                visible:(core.testMode)
                wrapMode:Text.WordWrap;
            }
        }
    }

    property QtObject vkb
    Component{
        id:compVkb
        VirtualKeyboard{
            id:vkb
            //visible:false
            function launchVKB(identifier,defaultString){
                if(defaultString == undefined)defaultString = "";
                if(identifier == undefined)identifier = "";
                keyBoard.inputText.text = defaultString;
                viewHelper.identifier= identifier;
                console.log("viewHelper.vkbInputData | qml:"+viewHelper.vkbInputData)
                console.log("vkb.identifier | qml:"+viewHelper.identifier)
                console.log("vkb.identifier | defaultString:"+defaultString)
                console.log("vkb.identifier | identifier:"+identifier)
                showVkb(true,identifier,defaultString)
            }

        }
    }

    property QtObject meal
    Component{
        id:compMeal
        Meal{
            id:meal;
            visible:false;
        }
    }
}
