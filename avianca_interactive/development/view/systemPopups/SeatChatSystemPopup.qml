import QtQuick 1.0
import "../components/"
SeatChatPopup{
    id:seatChatpopups
    property string title;
    property int systempopupId: viewController.getActiveSystemPopupID()
    //    noOfButtons:3;
    // visible: false;

    bgDetails:[(systempopupId!=13||systempopupId!=17 || systempopupId!=22)?qsTranslate('','popups_chatpop_bg_tm1'):qsTranslate('','popups_chatpop_bg_tm'),
                                                                            (systempopupId!=13||systempopupId!=17 || systempopupId!=22)? qsTranslate('','popups_chatpop_bg_w1'): qsTranslate('','popups_chatpop_bg_w'),
                                                                                                                                         (systempopupId==14||systempopupId==18||systempopupId==15)?qsTranslate('','popups_chatpop_bg_h1'):qsTranslate('','popups_chatpop_bg_h'),
                                                                                                                                                                                                    configTag["bg_color"],
                                                                                                                                                                                                    qsTranslate('','popups_chatpop_bg_radius')
    ]
    titleDetails:[
        qsTranslate('','popups_chatpop_title_tm'),
        qsTranslate('','popups_chatpop_title_w'),
        (systempopupId==13||systempopupId==17)?qsTranslate('','popups_chatpop_title_h'):qsTranslate('','popups_chatpop_title_h1'),
                                                title,
                                                configTool.config.music.popup["title_color"],
                                                qsTranslate('','popups_chatpop_title_fontsize'),
                                                "AlignHCenter",
                                                qsTranslate('','popups_chatpop_title_lh')
    ]

    descDetails:[0,0,0,"","transparent",0,0]

    buttonDetails:[
        qsTranslate('','popups_chatpop_btn_bm'),
        parseInt(qsTranslate('','popups_chatpop_btn_w'),10),
        qsTranslate('','popups_chatpop_btn_h'),
        (systempopupId==13)?parseInt(qsTranslate('','popups_chatpop_btn_hgap2'),10):((systempopupId==14||systempopupId==15||systempopupId==16 ||systempopupId==22)?parseInt(qsTranslate('','popups_chatpop_btn_hgap1'),10):parseInt(qsTranslate('','popups_chatpop_btn_hgap'),10)),
                             qsTranslate('','popups_chatpop_btn_margin'),
                             "btn",
                             (systempopupId==15 || systempopupId==16 || systempopupId==22 || systempopupId==14)?parseInt(qsTranslate('','popups_chatpop_btn_lm1'),10):(systempopupId==13 )?qsTranslate('','popups_chatpop_btn_lm2'):/*qsTranslate('','popups_chatpop_btn_lm')*/(parseInt(qsTranslate('','popups_chatpop_bg_w'),10)/2)/*-(parseInt(qsTranslate('','popups_chatpop_btn_w'),10)/2)*/
    ]

    buttonTextDetails:[
        qsTranslate('','popups_chatpop_btntxt_w'),
        "btn_text",
        qsTranslate('','popups_chatpop_btntxt_fontsize')
        //        configTool.language.global_elements.ok
        /*,w,text,color,fs*/
    ]

    onButtonSelect:{
        if(systempopupId==13){
            if(btnId==0){
                core.debug("SeatChatPopups.qml| Accept | pif.seatChat.pendingInvitationList accept= "+pif.seatChat.pendingInvitationList.count)
                //                pif.seatChat.removeFromQueue(viewHelper.cSessionID)
                core.debug( "In SeatChatPopups | SeatChat Invitation Pop up accept")
                core.debug("_______viewHelper.seatDataSession________: "+viewHelper.cSessionID)
                core.debug("_______viewHelper.blockSeatArr1________: "+viewHelper.tSeatNo);
                core.debug("viewHelper.actualSessionCount: "+viewHelper.actualSessionCount)
                core.debug("viewHelper.maxSessionCount: "+viewHelper.maxSessionCount);
                if((viewHelper.actualSessionCount+viewHelper.invitationSent)==viewHelper.maxSessionCount){
                    closePopup(1)
                    viewController.showScreenPopup(21)
                    return;
                }
                closePopup(0)
                viewHelper.fromInvitePopup = true
                console.log("viewController.getActiveScreen() = "+viewController.getActiveScreen())
                if(viewController.getActiveScreen()!="SeatChat"){
                    if(viewHelper.getSeatChatTerms()){
                        viewHelper.toacceptInvitationPopup()
                        //viewHelper.acceptInviataionfromPopup()
                        console.log("if terms n conditoin is accepted  | getSeatChatTerms= "+viewHelper.getSeatChatTerms())

                    }else{
                        console.log("if terms n conditoin is not  accepted  |getSeatChatTerms= "+viewHelper.getSeatChatTerms())

                    }
                    console.log("Not In seatChat >>>>>>>>>>>>>>>>>>>>>>>>>>>>= "+viewController.getActiveScreen())
                }else{
                    viewHelper.toacceptInvitationPopup()
                    //viewHelper.acceptInviataionfromPopup()
                    console.log("Inside seatChat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>= "+viewController.getActiveScreen())
                }
                //                closePopup()

                if(viewController.getActiveScreen()!="SeatChat"){
                    if(widgetList.isVideoVisible()){
                        viewController.hideScreenPopup();
                        viewController.showScreenPopup(1)
                    }

                    if(widgetList.getMealStatus())widgetList.showMealMenu(false)
                    if(viewHelper.checkForExternalBlock("SeatChat")) return;
                    var tmpID=viewHelper.isKids(current.template_id)
                    if(tmpID){
                        viewHelper.directJumpFromKids="SeatChat"
                        viewHelper.refreshToMainSection();
                    }
                    if(widgetList.isVideoVisible()){
                        viewHelper.forceFullyCloseWidgets()
                        widgetList.stopVod()
                        viewHelper.isExternalOnVOD="SeatChat"
                        return;
                    }
                    else  if(!tmpID)viewHelper.externalEvents("SeatChat")
                }
            }else if(btnId==1){
                core.debug("SeatChatPopups.qml| decline | pif.seatChat.pendingInvitationList  = "+pif.seatChat.pendingInvitationList.count)
                //                pif.seatChat.removeFromQueue(viewHelper.cSessionID)
                core.debug( "In SeatChatPopups | SeatChat Invitation Pop up Decline")
                core.debug("_______viewHelper.seatDataSession________: "+viewHelper.cSessionID)
                core.debug("_______viewHelper.blockSeatArr1________: "+viewHelper.tSeatNo);
                closePopup(0);
                pif.seatChat.declineInvitation(viewHelper.cSessionID,viewHelper.tSeatNo);

            }else {
                core.debug("SeatChatPopups.qml| Block | pif.seatChat.pendingInvitationList block = "+pif.seatChat.pendingInvitationList.count)
                //                pif.seatChat.removeFromQueue(viewHelper.cSessionID)
                var tempArr = new Array();
                tempArr[0]=pif.seatChat.pendingInvitationList.getValue(0,"seat")//viewHelper.tSeatNo
                closePopup(0)
                pif.seatChat.declineInvitation(pif.seatChat.pendingInvitationList.getValue(0,"session"),pif.seatChat.pendingInvitationList.getValue(0,"seat"))
                pif.seatChat.blockSeat(tempArr);
                core.debug( "In SeatChatPopups | SeatChat Invitation Pop up Blocked "+pif.seatChat.blockSeat(tempArr))
                core.debug("_______viewHelper.tempArr: "+viewHelper.tSeatNo);
                //                pif.seatChat.declineInvitation(viewHelper.cSessionID,viewHelper.tSeatNo);

                //                pif.seatChat.getallBuddiesNBlockList();
            }
        }else{
            viewController.updateSystemPopupQ(systempopupId,false);
        }
    }
    function closePopup(node){
        if(!node)node=0;
        viewHelper.cSessionID=pif.seatChat.pendingInvitationList.getValue(0,"session")
        viewHelper.tSeatNo=pif.seatChat.pendingInvitationList.getValue(0,"seat")
        if(node==0){
            pif.seatChat.removeFromQueue(pif.seatChat.pendingInvitationList.getValue(0,"session"))
            if((viewHelper.actualSessionCount+viewHelper.invitationSent)<viewHelper.maxSessionCount){
                if(pif.seatChat.pendingInvitationList.count>0){
                    viewHelper.popupTSeatNo=pif.seatChat.pendingInvitationList.getValue(0,"seat")
                    console.log("pif.seatChat.pendingInvitationList > 0 after = "+pif.seatChat.pendingInvitationList.count)
                    console.log("viewHelper.cSessionID  = "+viewHelper.cSessionID)
                    console.log("viewHelper.tSeatNo  = "+viewHelper.tSeatNo)
                    //                    viewController.updateSystemPopupQ(13,false)
                    delay_Invite_queue.restart()
                }else {delay_Invite_queue.stop()}
            }
        }
        viewController.updateSystemPopupQ(systempopupId,false);
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendApiData("userDefinedUsbHide",{device:"usb"})
    }
    function init(){
        console.log("SEATCHAT  POPUP viewController.getActiveSystemPopupID() = "+systempopupId)
        buttonView.currentIndex=0
        systempopupId=viewController.getActiveSystemPopupID()
        if(widgetList.isVideoVisible()){
            if(widgetList.getHelpStatus()==1)widgetList.setHelpStatus(false)
        }
        if(systempopupId==13){
            var temp= " "+configTool.language.seatchat.invitation_message;
            title=temp.replace("XX"," "+viewHelper.popupTSeatNo)
            noOfButtons=3;
            buttonTextDetails=[
                        qsTranslate('','popups_resume_btntxt_w'),
                        "btn_text",
                        qsTranslate('','popups_resume_btntxt_fontsize'),
                        configTool.language.seatchat.seatchat_accept,
                        configTool.language.seatchat.decline,
                        configTool.language.seatchat.block
                    ]
        }
        visible=true;
        forceActiveFocus();
    }

    function clearOnExit(){
        if(pif.getPAState())return;
        core.debug("SeatChatPopups.qml| clearOnExit | pif.seatChat.pendingInvitationList before  = "+pif.seatChat.pendingInvitationList.count)
        console.log(">>>>>>>> viewHelper.cSessionID "+viewHelper.cSessionID)

        core.debug("SeatChatPopups.qml| clearOnExit pif.seatChat.pendingInvitationList = "+pif.seatChat.pendingInvitationList.count)
    }

    Timer{
        id:delay_Invite_queue;
        running: false
        interval:3000;
        onTriggered:{

            viewController.updateSystemPopupQ(13,true)
        }
    }


}

