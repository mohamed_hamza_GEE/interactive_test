import QtQuick 1.0
import "../components/"
QMLPopup{
    property string title;
    noOfButtons:1;
    visible: false;
    Image{
        source:viewHelper.configToolImagePath+configTag["ct_icon"]//"announcement.png"//configTool.config.popup.ct.Icon;
        id:cticon
        anchors.top:parent.top;
        anchors.topMargin:(bgDetails[0]!=0)?bgDetails[0]:(parent.height/2-height/2);
        anchors.horizontalCenter:parent.horizontalCenter;
        visible:(viewController.getActiveSystemPopupID()==2)
    }

    bgDetails:[
        parseInt(qsTranslate('','popups_bg_tm'),10)-(cticon.width/2),
        parseInt(qsTranslate('','popups_bg_w'),10)+(cticon.width/2),
        parseInt(qsTranslate('','popups_bg_h'),10)+(cticon.height),
        configTag["bg_color"],
        qsTranslate('','popups_bg_radius')
    ]
    titleDetails:[
        cticon.height,
        qsTranslate('','popups_text_w'),
        qsTranslate('','popups_text_h'),
        title,
        configTool.config.popup.generic.text_color,
        qsTranslate('','popups_text_fontsize'),
        "AlignHCenter",
        qsTranslate('','popups_text_lh'),
    ]
    descDetails:[0,0,0,"","transparent",0,0]
    function init(){
        if(viewController.getActiveSystemPopupID()==2){
            title=pif.ctMessage.ctMessage
            buttonTitle.maximumLineCount=6
            noOfButtons=0;

            seatMsgTimer.interval=pif.ctMessage.ctTimeout*1000;
            seatMsgTimer.restart();

        }
        else if(viewController.getActiveSystemPopupID()==7){
            title=configTool.language.global_elements.selectionUnavilable
            noOfButtons=0;
            close_button.forceActiveFocus();
        }
        else if(viewController.getActiveSystemPopupID()==8){
            title=configTool.language.global_elements.comingsoon
            noOfButtons=0;
        }else if(viewController.getActiveSystemPopupID()== 14){
            title="System will power down in 5 secs. Please save and exit."
            noOfButtons=0;
            close_button.forceActiveFocus();

        }
        visible=true;
        forceActiveFocus();
    }

    function clearOnExit(){
        visible=false;
        if(viewController.getActiveSystemPopupID()==2){
            if(viewHelper.multipleSamePopupInstance.length==0){
                if(viewHelper.storePreviousStateofBacklight)pif.setBacklightOn()
                else pif.setBacklightOff()
            }
            else
                viewHelper.multipleSamePopupInstance.pop()
        }
    }

    Keys.onReleased: {
        if(event.key==Qt.Key_Return || event.key==Qt.Key_Enter){
            if(viewController.getActiveSystemPopupID()==7||viewController.getActiveSystemPopupID()==8||viewController.getActiveSystemPopupID()==14)
                viewController.updateSystemPopupQ(viewController.getActiveSystemPopupID(),false)
//            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
//                event.accepted=true;
//            }
        }
    }

    Timer{id:seatMsgTimer;onTriggered:{
       if(viewController.getActiveSystemPopupID()==2)viewController.updateSystemPopupQ(2,false);}
    }

    MouseArea{
        anchors.fill:parent;
        onClicked:{
            if(viewController.getActiveSystemPopupID()==7||viewController.getActiveSystemPopupID()==8||viewController.getActiveSystemPopupID()==14)
                viewController.updateSystemPopupQ(viewController.getActiveSystemPopupID(),false)


        }
    }


}

