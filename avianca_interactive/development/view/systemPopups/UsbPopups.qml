import QtQuick 1.0
import "../components/"
QMLPopup{
    property string title;
    noOfButtons:1;
    visible: false;
    bgDetails:[
        qsTranslate('','popups_resume_bg_tm'),
        qsTranslate('','popups_resume_bg_w'),
        qsTranslate('','popups_resume_bg_h'),
        configTag["bg_color"],
        qsTranslate('','popups_resume_bg_radius')
    ]
    titleDetails:[
        qsTranslate('','popups_resume_title_tm'),
        qsTranslate('','popups_resume_title_w'),
        qsTranslate('','popups_resume_title_h'),
        title,
        configTool.config.music.popup["title_color"],
        qsTranslate('','popups_resume_title_fontsize'),
        "AlignHCenter"
    ]
    descDetails:[0,0,0,"","transparent",0,0]

    buttonDetails:[
        qsTranslate('','popups_resume_btn_bm'),
        parseInt(qsTranslate('','popups_resume_btn_w'),10),
        qsTranslate('','popups_resume_btn_h'),
        qsTranslate('','popups_resume_btn_hgap'),
        qsTranslate('','popups_resume_btn_margin'),
        "btn"
        /*tm,w,h,hgap,margin*/
    ]
    buttonTextDetails:[
        qsTranslate('','popups_resume_btntxt_w'),
        "btn_text",
        qsTranslate('','popups_resume_btntxt_fontsize'),
        configTool.language.global_elements.ok
        /*,w,text,color,fs*/
    ]

    onButtonSelect:{
        if(viewController.getActiveSystemPopupID()==4){
            if(btnId==0){
                var tmpID=viewHelper.isKids(current.template_id)
                if(tmpID){
                    viewHelper.directJumpFromKids="USB"
                    viewHelper.refreshToMainSection();
                }
                if(widgetList.isVideoVisible()){
                    viewHelper.forceFullyCloseWidgets()
                    closePopup()
                    widgetList.stopVod()
                    viewHelper.isExternalOnVOD="USB"
                    return;
                }
                else if(!tmpID) viewHelper.externalEvents("USB")
            }
            closePopup()
        }
        else{
            if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET){
                pif.sharedModelServerInterface.sendUserDefinedApi("userDefinedDisconnectUsb",{usbDisconnect:true})

            }
            viewController.updateSystemPopupQ(viewController.getActiveSystemPopupID(),false);
        }
    }
    Connections{
        target:viewHelper
        onShowUsb:{
            if(usbStatus=='continue')buttonSelect(0);
            else buttonSelect(1);
        }
    }
    function closePopup(){
        viewController.updateSystemPopupQ(viewController.getActiveSystemPopupID(),false);
        if(pif.getHandsetType()==pif.cVIDEO_KARMA_HANDSET)
            pif.sharedModelServerInterface.sendApiData("userDefinedUsbHide",{device:"usb"})
    }
    function init(){
        buttonView.currentIndex=0
        if(viewController.getActiveSystemPopupID()==4){
            title=configTool.language.usb.connected;
            noOfButtons=2;
            buttonTextDetails=[
                        qsTranslate('','popups_resume_btntxt_w'),
                        "btn_text",
                        qsTranslate('','popups_resume_btntxt_fontsize'),
                        configTool.language.global_elements.cont,
                        configTool.language.global_elements.cancel

                    ]
        }else {
            if(viewController.getActiveSystemPopupID()==5){
                title = configTool.language.usb.disconnected;
            }else if(viewController.getActiveSystemPopupID()==6){
                title = configTool.language.usb.connect;
            }else if(viewController.getActiveSystemPopupID()==12){
                title = configTool.language.usb.notsupported;
            }
            noOfButtons=1;
            buttonTextDetails=[
                        qsTranslate('','popups_resume_btntxt_w'),
                        "btn_text",
                        qsTranslate('','popups_resume_btntxt_fontsize'),
                        configTool.language.global_elements.ok

                    ]
        }
        visible=true;
        forceActiveFocus();
    }

    function clearOnExit(){
        visible=false;
    }

}

