import QtQuick 1.0
import "../components/"
QMLPopup{
    property string title;
    noOfButtons:0;
    visible: false;
    bgDetails:[
        qsTranslate('','popups_resume_bg_tm'),
        qsTranslate('','popups_resume_bg_w'),
        qsTranslate('','popups_resume_bg_h'),
        configTag["bg_color"],
        qsTranslate('','popups_resume_bg_radius')
    ]
    titleDetails:[
        qsTranslate('','popups_resume_title_tm'),
        qsTranslate('','popups_resume_title_w'),
        qsTranslate('','popups_resume_title_h'),
        title,
        configTool.config.popup.generic.text_color,
        qsTranslate('','popups_resume_title_fontsize'),
        "AlignHCenter"
    ]
    descDetails:[0,0,0,"","transparent",0,0]
    buttonDetails:[
        qsTranslate('','popups_resume_btn_bm'),
        parseInt(qsTranslate('','popups_resume_btn_w'),10),
        qsTranslate('','popups_resume_btn_h'),
        qsTranslate('','popups_resume_btn_hgap'),
        qsTranslate('','popups_resume_btn_margin'),
        "btn"
        /*tm,w,h,hgap,margin*/
    ]
    buttonTextDetails:[
        qsTranslate('','popups_resume_btntxt_w'),
        "btn_text",
        qsTranslate('','popups_resume_btntxt_fontsize'),
        configTool.language.global_elements.yes,
        configTool.language.global_elements.no
        /*,w,text,color,fs*/
    ]
    function init(){
        title=  configTool.language.survey.survey_triggered
        noOfButtons=2;
        forceActiveFocus();
    }
    function clearOnExit(){

    }
    onButtonSelect: {
        if(btnId==0){
            var tmpID=viewHelper.isKids(current.template_id)
            if(tmpID){
                viewHelper.directJumpFromKids="Survey"
                viewHelper.refreshToMainSection();
            }
            if(widgetList.isFlightMapOn()){widgetList.showFlightMap(false)}
            if(widgetList.isVideoVisible()){
                viewHelper.forceFullyCloseWidgets()
                viewController.updateSystemPopupQ(viewController.getActiveSystemPopupID(),false);
                widgetList.stopVod()
                viewHelper.isExternalOnVOD="Survey"
                return;
            }
            else  if(!tmpID)viewHelper.externalEvents("Survey")
        }
        viewController.updateSystemPopupQ(viewController.getActiveSystemPopupID(),false);
    }


}

