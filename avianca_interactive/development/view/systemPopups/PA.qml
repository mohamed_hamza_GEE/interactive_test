import QtQuick 1.0
import "../components/"
QMLPopup{
    property string title;
    noOfButtons:0;
    visible: false;
    bgDetails:[
        qsTranslate('','popups_bg_tm'),
        qsTranslate('','popups_bg_w'),
        qsTranslate('','popups_bg_h'),
        configTag["bg_color"],
        qsTranslate('','popups_bg_radius')
    ]
    titleDetails:[
        0,
        qsTranslate('','popups_text_w'),
        qsTranslate('','popups_text_h'),
        configTool.language.global_elements.pa,
        configTool.config.popup.generic.text_color,
        qsTranslate('','popups_text_fontsize'),
        "AlignHCenter",
        qsTranslate('','popups_text_lh'),
    ]
    descDetails:[0,0,0,"","transparent",0,0]
    function init(){

        if(viewController.getActiveSystemPopupID()==1){
            title=configTool.language.global_elements.pa
            noOfButtons=0;
        }
        visible=true;
        forceActiveFocus();
    }
    function clearOnExit(){
        visible=false;
    }
}

