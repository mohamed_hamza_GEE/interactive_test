import QtQuick 1.0
import "../components/"
QMLPopup{
    property string title;
    noOfButtons:0;
    visible: false;
    bgDetails:[
        qsTranslate('','popups_cg_bg_tm'),
        qsTranslate('','popups_cg_bg_w'),
        qsTranslate('','popups_cg_bg_h'),
        configTag["bg_color"],
        qsTranslate('','popups_cg_bg_radius'),
        qsTranslate('','popups_cg_bg_rm')
    ]
    titleDetails:[
        0,
        qsTranslate('','popups_cg_text_w'),
        qsTranslate('','popups_cg_text_h'),
        title,
        configTool.config.music.popup["title_color"],
        qsTranslate('','popups_cg_text_fontsize'),
        "AlignHCenter",
        qsTranslate('','popups_cg_text_lh'),


    ]
    descDetails:[0,0,0,"","transparent",0,0]

    function init(){

        if(viewController.getActiveSystemPopupID()==9){
            title=configTool.language.connecting_gate.cg_info_update
            noOfButtons=0;
        }else {
            if(viewController.getActiveSystemPopupID()==10){
                title = configTool.language.connecting_gate.cg_info_available
            }
            noOfButtons=0;

        }
        visible=true;
        forceActiveFocus();
    }

    function clearOnExit(){
        visible=false;
    }

}

