import QtQuick 1.1
import "framework"
Rectangle{
    width:432
    height: 432
    color:"#000000"
    Core {
        pc:true;
        logLevel:"log";
        simulation:true;
        anchors.centerIn:parent;
        rotation:pif.getOrientationType()!="p"?-90:0;
        testMode: 1
    }
}
