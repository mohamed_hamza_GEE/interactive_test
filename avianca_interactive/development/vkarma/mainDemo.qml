import QtQuick 1.1
import "framework"

Core {
    pc:true;
    logLevel:"log";
    simulation:true;
    testMode: 1
    width:1024;
    height:600;
    property bool demo: false;
}
