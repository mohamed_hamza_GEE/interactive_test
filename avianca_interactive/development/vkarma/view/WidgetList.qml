import QtQuick 1.1
import "widgets"
import "components"

// Contains interactive specific screen holders

Item {
    Component.onCompleted: { }
    function getWidgetList () {
        var cList=new Object();
        cList[0] = {"compId":compHeader, "id": "header", "z-index":18};
        cList[1] = {"compId":compFooter, "id": "footer", "z-index":18};
        cList[2] = {"compId":compVideo, "id": "video", "z-index":14};
        cList[3] = {"compId":compGamesNowPlaying, "id": "gamesNowPlaying", "z-index":15};
        cList[4] = {"compId":compSettings, "id": "settings", "z-index":17};
        cList[5] = {"compId":compAudio, "id": "audio", "z-index":14};
        cList[6] = {"compId":compVKB, "id": "vkb", "z-index":22};
        cList[7] = {"compId":compMaps, "id": "maps", "z-index":18};
        return cList;
    }
    /***********************************************HEADER*************************************************************************/
    function getHeaderRef(){return header}
    function setSectionBtnText(val){getHeaderRef().setSectionBtnText(val)}
    function showHeader(val){getHeaderRef().showHeader(val)}
    function changeSection(index){getHeaderRef().changeSection(index)}
    /**************************************************FOOTER**********************************************************************/
    function getFooterRef(){return footer}
    function showFooter(val){getFooterRef().showFooter(val)}
    function showFooterHomeBtn(val){getFooterRef().showFooterHomeBtn(val)}
    function showNowPlayinBtn(val){getFooterRef().showNowPlayinBtn(val)}
    /************************************************VIDEO************************************************************************/
    function getVideoRef(){return video}
    function showVodNowPlaying(){getVideoRef().showVodNowPlaying()}
    function hideVodNowPlaying(){getVideoRef().hideVodNowPlaying()}
    /*******************************************GAMES*****************************************************************************/
    function getGamesRef(){return gamesNowPlaying}
    function showGamesNowPlaying(val){getGamesRef().showGamesNowPlaying(val)}
    function showGamesNowPlayingClose(){gamesNowPlaying.showGamesNowPlayingClose();}
    /*******************************************SETTINGS*********************************************************/
    function getSettingsRef(){return settings}
    function showSettingScreen(val){getSettingsRef().showSettingScreen(val)}
    /****************************************AUDIO************************************************************/
    function getAudioRef(){return audio}
    function showAodControls(){getAudioRef().showAodControls()}
    function hideAodControls(){getAudioRef().hideAodControls()}
    //function setPlayingCategory(cat){getFooterRef().setPlayingCategory(cat)}
    function getNowPlayingSource(){getFooterRef().getNowPlayingSource()}
    /*****************************************vkb***********************************************************/
    function getVkbRef(){return vkb}
    function showHideVKBScreen(show){
        if(show){
            if(/*viewController.getActiveScreen()!="Welcome" &&*/viewController.getActiveScreen()!="EntOff" )

            vkb.visible = true;
        }
        else{
            pif.vkarmaToSeat.hideVkb();
            vkb.visible = false;
        }
    }
    /********************************************MAPS******************************************************/
    function setMapStatus(val){
        maps.visible=val;
    }
    function getMapStatus(){
        return maps.visible;
    }

    /********************************************MAPS******************************************************/

    property QtObject header
    Component{
        id: compHeader
        Header{
            id:header
        }
    }
    property QtObject footer
    Component{
        id: compFooter
        Footer{
            id:footer
        }
    }
    property QtObject video
    Component{
        id:compVideo
        Video{
            id:video
        }
    }
    property QtObject gamesNowPlaying
    Component{
        id:compGamesNowPlaying
        GamesNowPlaying{
            id:gamesNowPlaying
        }
    }
    property QtObject settings
    Component{
        id:compSettings
        SettingsScreen{
            id:settings
        }
    }
    property QtObject audio
    Component{
        id:compAudio
        Audio{
            id:audio
        }
    }
    /*******************************************************************************************************************************/
    property QtObject vkb
    Component{
        id: compVKB
        Vkb{
            visible: false
        }
    }
    /*******************************************************************************************************************************/
    property QtObject maps
    Component{
        id:compMaps
        Voyager{}
    }
    /*******************************************************************************************************************************/

}
