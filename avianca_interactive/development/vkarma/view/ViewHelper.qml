import QtQuick 1.1
import Panasonic.Pif 1.0
import "../framework/blank.js" as Code
import "../framework/components"
import "../framework"

// Contains Interactive specific functions and variables.

Item {
    /************************************strings******************************/
    property string configToolImagePath:core.configTool.path + "images/";
    property string mediaImagePath : core.settings.contentPath +"/media/images/";
    property string assetPath: core.settings.viewAssetPath + "0/images/"
    property string nowPlayingCat:'movies'
    property string catSelected:'movies'
    property string catIcon: ''
    property string subCatSelected:''
    property string tempId:''
    property string tvSeriesTitle:''
    property string mediaPoster:''
    property string vodOperation: ''
    property string categoryPlaying: ''
    property string ixplorImg:(core.pc)?"/tmp/map.bmp":"";
    property string selectedIso: ''
    property variant genreValue:""
     property string tempCat: ""
    property string checkTemplate:""
    onCheckTemplateChanged: {
        console.log("onCheckTemplateChanged >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ="+checkTemplate)
    }

    property string defaultLang:core.settings.defaultISO
    /*********************************variants********************************************/
    property variant synopsisModel:blankModel
    property variant currentPlayingDetails;
    property variant tempPlayingDetails;
    property variant __playDetails;
    property variant soundtrackModel:blankModel
    property variant subtitleModel:blankModel
    property variant vodParams:[]
    property variant cgArrivalData:["","","","","",""];
    property variant settingsMainMenuModel: isAircraft787()?[configTool.language.karma.language_settings,configTool.language.karma.monitor_settings,configTool.language.karma.handset_settings,configTool.language.settings.windowdimmable_button]:[configTool.language.karma.language_settings,configTool.language.karma.monitor_settings,configTool.language.karma.handset_settings]
    property variant monitorSettingsModel: [configTool.language.karma.settings_vol,configTool.language.karma.settings_bright,configTool.language.karma.settings_screen_off,configTool.language.settings.call_crew,configTool.language.settings.reading_light]
    property variant tvModel: blankModel
    property variant episodeModel:blankModel
    property variant musicData;
    property variant currentAlbumModel: blankModel
    property variant shopValue;
    property variant shop_catalog_id
    property variant shop_category_id
    property variant shop_item_id
    property variant shoppingMediaModel
    //    property variant refModel
    property variant shoppingCategoryMenuModel
    property variant catalogModel
    property variant hospValue;
    property variant hosp_catalog_id
    property variant hosp_category_id
    property variant hosp_item_id
    property string hosp_catalog_type:""
    property int categoryIndex:-1
    property variant hospitalityMediaModel
    //    property variant refModel
    property variant hospitalitCategoryMenuModel
    property variant hospCatalogModel



    /**********************************integers*******************************************/
    property int selectedIndex:0//for mainMenu
    property int subMenuSelIndex:0 //For SubMenu
    property int mediaSelected:0 //for MediaListing
    property int episodeIndex: 0
    property int playingEpisodeIndex: 0
    property int playingMid:0
    property int vodType:0
    property int vodMidToBePlayed:0
    property int soundtrackId:0;
    property int subtitleId:0
    property int soundTracklid:-1;
    property int aspectlid: 0
    property int subtitlelid:-1;
    property int resumeSecs:10;
    property int sectionIndexSel: 0
    property int langId:0
    property int tvIndex: 0

    property int exitCase: 0
    property int  fullPlayId: 0
    property int  playListIndex: 0
    property int playingAudioMid: 0
    property int  aodMidToBePlayed: 0
    property int radioAlbumSelected: 0
    property int gameMidToBePlayed: 0
    property int vkbLinesLimit:1;
    property int vkbCharLimit:1;
    property string vkb_identifier:""

    /*************************************boolean****************************************/
    property bool firstTimeCloseVkb: false
    property bool firstMainLoad: false
    property bool firstKidsLoad: false
    property bool playMovieFromKarma:false
    property bool isSingleSoundtrack: false
    property bool isSingleSubtitles: false
    property bool isResume:false
    property bool isSettings: false
    property bool handsetSelected: false
    property bool isCgSelected: false
    property bool sectionSelected: false
    property bool isMonitor: false
    property bool isTvSeries: false
    property bool isFromGenre: false
    property bool isPlayList: false
    property bool sectionBtnPressed: false
    property bool comingFromTrackList: false
    property bool isFromKarma: false
    property bool isFromExitPopup: false
    property bool isFromResumePopup: false
    property bool disabledStateForAspectRatio: false
    property bool fromthreeDMaps: false
    property bool isTrailerPressed: false
    property bool isPlayBtnClicked:false
    property bool isFromSeat: false
    property bool isTrackPlayedFromKids: false
    property bool isPlaylistFull:false
    property bool _sndccPopFromMovie:false;
    property bool isStillVideoPlayingOnSeat:false;
    property bool fromBack:false;
    property bool isAviancaBrasilAir: false
    property bool userRepeatOn: false
    property bool isVoyagerBroadcast: false
    property string currMediaType:pif.vkarmaToSeat.getMediaType();
    property string prevMediaType:"";

    /*****************************Aliases*****************************/
    property alias emptyDel:emptyDel
    property alias blankModel: blankModel
    property alias audioPlayerVkarma: audioPlayerVkarma
    property alias imagegrabber:imagegrabber;
    /*****************************Signals*****************************/
    signal homeCalled;
    signal switchToDiscover(bool swtich,string category)
    signal vkarmaReady(bool vkarmaReady)
    signal launchApp(string screenName)
    signal updateTvSeriesIndex(int playIndex)
    signal stopVod(bool status)
    signal changeSection(bool sectionClicked)
    signal callVodPlay
    signal seatUp
    signal shoppingLangUpdate(variant dmodel)
    signal hospitalityLangUpdate(variant dmodel)
    property variant cSessionID:0
    property variant tSeatNo:"";
    /********************************************************************************************/
    property string paxusTIDtagging:viewController.getActiveScreen()
    onPaxusTIDtaggingChanged: {
        if(paxusTIDtagging=="HospSubMenu" || paxusTIDtagging=="HospSynopsis" || paxusTIDtagging=="HospListing")
            if(pif.paxus.currentContent!='on board menu')
                pif.paxus.startContentLog('on board menu')
            else  if(catSelected!="shopping" || paxusTIDtagging=="MainMenu")
                pif.paxus.stopContentLog()
    }
    onCatSelectedChanged: {
        if(catSelected=="shopping"){
            pif.paxus.startContentLog('Shopping')
        }
    }
    onUserRepeatOnChanged:{
        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedRepeatOn",{repeatStatus:userRepeatOn});
    }
    onHomeCalled: {
        hideWidgets()
        var cid=viewData.optionMenuModel.getValue(sectionIndexSel,"cid")
        var tid=viewData.optionMenuModel.getValue(sectionIndexSel,"category_attr_template_id")
        viewController.jumpTo(cid,tid)
    }
    onCurrMediaTypeChanged:{
        if(prevMediaType=="audio"||prevMediaType=="audioAggregate"){
            if(currMediaType=="broadcastVideo"){
                pif.vkarmaToSeat._mediaType=prevMediaType;return;
            }
        }
        prevMediaType=currMediaType;

    }

    Connections{
        target:viewHelper
        onVkarmaReady:{
            if(vkarmaReady){
                viewController.jumpTo(0,"WELC");
                seatUp()
            }
        }
    }
    /*****************************Functions******************************/
    function isAircraftType787(){
        return ((core.dataController.getAircraftType().indexOf("787") != -1))?true:false;
    }

    function isAircraftType330(){
        return ((core.dataController.getAircraftType().indexOf("330") != -1))?true:false;
    }

    /****************************************Lang chnage for shopping***************************************************************/

    function languageUpdate(){
        core.info("ViewHelper.qml | languageUpdate if not shopping end here ")
        if(viewController.getActiveScreenPopupID()==1) viewController.getActiveScreenPopupRef().getSubTitlesQuery()
        if(viewHelper.catSelected!="shopping" || current.template_id!="shopping"|| current.template_id!="shopping_listing")return;
        core.info("ViewHelper.qml | languageUpdate if shopping go inside ")
        var curLangISO=core.settings.languageISO.toUpperCase()
        var params=new Object()
        params['catalog']=catalogModel//viewHelper.shop_catalog_id
        params['category']=shoppingCategoryMenuModel//viewHelper.shop_category_id
        params['item']=shoppingMediaModel//viewHelper.shop_item_id
        core.debug(" params['catalog'] = "+ params['catalog'])
        core.debug(" params['category'] = "+ params['category'])
        core.debug(" params['item'] = "+ params['item'])
        var langIndex =core.coreHelper.searchEntryInModel(core.dataController.shopping.getLanguageModel(),"language_code",curLangISO)
        console.log("viewHelper.qml| languageUpdate |  viewHelper.defaultLang = "+viewHelper.defaultLang+" langIndex = "+langIndex+ " curLangISO = "+curLangISO)
        if(langIndex!=-1){
            console.log("viewHelper.qml | languageUpdate |  curLangISO = "+curLangISO)
            core.dataController.shopping.changeLang(curLangISO,params,langModelReady);
        }else{
            console.log("viewHelper.qml | defaultLang "+viewHelper.defaultLang)
            core.dataController.shopping.changeLang(viewHelper.defaultLang,params,langModelReady);
        }

    }

    function langModelReady(dModel){
        if(dModel.count>0){
            core.info("langModelReady >>>>>> if count greater than zero")
            shoppingLangUpdate(dModel)
        }else{
            core.info("langModelReady >>>>>> if count is zero")
            viewController.showScreenPopup(27)
        }
    }
    /****************************************Lang chnage for Hospitality***************************************************************/

    function hosLanguageUpdate(){
        core.info("ViewHelper.qml | hosLanguageUpdate if not shopping end here ")
        if(current.template_id!="hospitality"|| current.template_id!="hospitality_listing"||current.template_id!="hospitality_Synopsis")return;
        core.info("ViewHelper.qml | hosLanguageUpdate if shopping go inside ")
        var curLangISO=core.settings.languageISO.toUpperCase()
        var params=new Object()
        params['catalog']=hospCatalogModel//viewHelper.shop_catalog_id
        params['category']=hospitalitCategoryMenuModel//viewHelper.shop_category_id
        params['item']=hospitalityMediaModel//viewHelper.shop_item_id
        core.debug(" params['catalog'] = "+ params['catalog'])
        core.debug(" params['category'] = "+ params['category'])
        core.debug(" params['item'] = "+ params['item'])
        var langIndex =core.coreHelper.searchEntryInModel(core.dataController.hospitality.getLanguageModel(),"language_code",curLangISO)
        console.log("viewHelper.qml| languageUpdate |  viewHelper.defaultLang = "+viewHelper.defaultLang+" langIndex = "+langIndex+ " curLangISO = "+curLangISO)
        if(langIndex!=-1){
            console.log("viewHelper.qml | languageUpdate |  curLangISO = "+curLangISO)
            core.dataController.hospitality.changeLang(curLangISO,params,langModelReady);
        }else{
            console.log("viewHelper.qml | defaultLang "+viewHelper.defaultLang)
            core.dataController.hospitality.changeLang(viewHelper.defaultLang,params,hosLangModelReady);
        }
    }

    function hosLangModelReady(dModel){
        if(dModel.count>0){
            core.info("ViewHelper.qml | hosLangModelReady >>>>>> if count greater than zero")
            hospitalityLangUpdate(dModel)
        }else{
            core.info("ViewHelper.qml | hosLangModelReady >>>>>> if count is zero")
            viewController.showScreenPopup(27)
        }
    }

    /*******************************************************************************************************/

    function launchGame(){
        if(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play" && (pif.vkarmaToSeat.getMediaType()=="audio" || pif.vkarmaToSeat.getMediaType()=="audioAggregate"))
            viewController.showScreenPopup(28)
        else
            launchGamesonSelection(false)
    }
    function launchGamesonSelection(param){

        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedSetAudioGamesMusic",{"musicGameAudio":param})
        pif.vkarmaToSeat.launchApp({launchAppId:gameMidToBePlayed});
    }
    function getsndccPopFromMovie(){
        return _sndccPopFromMovie;
    }

    function setsndccPopFromMovie(flag){
        _sndccPopFromMovie=flag;
    }

    function loadEntOnScreen(){
        setScreenSaverData(5,60,30)
        isAviancaBrasilAir = false;//pif.getLruData("Airline_ICAO")=="ONE"?true:false;
        viewController.jumpTo(0,"WELC");
        viewController.hideScreenPopup()
        createTracklistStack()
        createPlaylistStack()
        checkTemplate=""
        if(pif.getPAState()){viewController.updateSystemPopupQ(1,true);}
        dataController.survey.getAllSurvey(); //added so data controller has the information about survey
    }

    //    ListModel{id:seat_alphabets}
    //    property alias seat_alphabets: seat_alphabets
    //    property variant seatLetters: []
    //    function getSeatLetters(){
    //         seatLetters = pif.getLru1DArrayData("Seat_letters")
    //        if(seatLetters == undefined)return;
    //        for(var i=0;i<seatLetters.length;i++){
    //            seat_alphabets.append({"letter": seatLetters[i]})
    //        }
    //    }

    function loadEntOffScreen(){
        viewController.hideScreenPopup()
        hideWidgets()
        isAviancaBrasilAir = false;//pif.getLruData("Airline_ICAO")=="ONE"?true:false;
        viewController.jumpTo(0,"ENTOFF");
        setScreenSaverData(10,60,30)
        if(pif.getPAState()){
            viewController.updateSystemPopupQ(1,true);
        }
    }
    function loadCloseFlightScreen(){
        isAviancaBrasilAir = false;//pif.getLruData("Airline_ICAO")=="ONE"?true:false;
        console.log("loadCloseFlightScreen called ")
        pif.setBacklightOn();
        setScreenSaverData(10,60,30)
        //seat_alphabets.clear();
        viewController.jumpTo(0,"ENTOFF");
    }
    function resetViewVariables(){
        //Variables on ENT On to OFF
    }
    function setScreenSaverData(x,y,z){
        pif.screenSaver.setScreenSaverType(1);
        pif.screenSaver.setScreenSaverTimeout(x)
        pif.screenSaver.setDimTimeout(y);
        pif.screenSaver.setDimBrightness(z);
        pif.screenSaver.restartScreenSaver();

    }
    function isAircraft787(){
        return ((core.dataController.getAircraftType().indexOf("787") != -1)  && (pif.vkarmaToSeat.getNoOfDimmableWindows()>0))?true:false;
    }

    /*************************************************************************************************************/
    function setCurrentPlayingDetails(params){ currentPlayingDetails=params}
    function getCurrentPlayingDetails(){return currentPlayingDetails}
    function settempPlayingDetails(params){ tempPlayingDetails=params}
    function gettempPlayingDetails(){return tempPlayingDetails}
    function setVodType(type){vodType = (type=="tvseries")?0:type=="trailer"?1:type=="episode"?2:3}
    function getVodType(){ return vodType  }
    function getTempVodType(type){  return  (type=="tvseries")?0:type=="trailer"?1:type=="episode"?2:3    }
    function getConvertedVodType(number){  return  (number=="0")?"tvseries":number=="1"?"trailer":number=="2"?"episode":"movies"    }
    function setPlayDetails(val){ __playDetails=val;}
    function getPlayDetails(){ return __playDetails;}
    function setSoundtrackModel(dModel){soundtrackModel=dModel}
    function getSoundtrackModel(){return soundtrackModel }
    function setSubtitleModel(dModel){subtitleModel=dModel}
    function getSubtitleModel(){return subtitleModel }
    function setSoundTracklid(val){ soundTracklid=parseInt(val,10);}
    function getSoundTracklid(){ return soundTracklid;}
    function setSubtitlelid(val){ subtitlelid=parseInt(val,10);}
    function getSubtitlelid(){ return subtitlelid;}
    function setAspectlid(val){ aspectlid=val;}
    function getAspectlid(){ return aspectlid;}
    function setLanguageId(val){langId=val}
    function getLanguageId(){return langId}
    function setTvModel(dModel){tvModel=dModel}
    function getTvModel(){return tvModel}
    function setTvIndex(index){tvIndex=index }
    function getTvIndex(){return tvIndex}
    function setMusicData(params){musicData=params }
    function getMusicDataToPlay(mid){
        if(musicData["mid"]==mid)
            return musicData
        else
            console.log("Mid does not exist "+mid)
    }
    function setCategory(title){
        if(title=="kids_games_listing")catSelected="kids_games"
        else if(title=="kids_music_listing")catSelected="kids_music"
        else if(title=="kids_movies_listing")catSelected="kids_movies"
        else if(title=="kids_tv_listing")catSelected="kids_tv"
        else if(title=="shopping")catSelected="shopping"
        else if(title=="games")catSelected="games"
        else if(title=="music")catSelected="music"
        else if(title=="movies")catSelected="movies"
        else if(title=="tv")catSelected="tv"
        else if(title=="discover")catSelected="discover"
        else if(title=="usb")catSelected="usb"
    }
    function hideWidgets(){
        if(widgetList.getVideoRef().visible)widgetList.getVideoRef().visible=false
        else if (widgetList.getAudioRef().visible)widgetList.getAudioRef().visible=false
        else if (widgetList.getGamesRef().visible)widgetList.getGamesRef().visible=false
        else if (widgetList.getSettingsRef().visible)widgetList.getSettingsRef().visible=false
        else if (widgetList.getVkbRef().visible)widgetList.getVkbRef().visible=false
    }
    function setPlayingCategory(cat){categoryPlaying=cat}
    function getPlayingCategory(){return categoryPlaying }
    /********************************************Video functions*****************************************************************/
    function setVodParams(){
        var tempVodType=getTempVodType(gettempPlayingDetails()["vodType"])
        var temCurrentPlayingDetails=gettempPlayingDetails()
        var params=new Object();
        params["mid"]=(tempVodType==0)?tvModel.getValue(tvIndex,"mid"):temCurrentPlayingDetails["mid"]
        params["soundtrackLid"]=getSoundTracklid()
        params["soundtrackType"]=2
        params["subtitleLid"]=getSubtitlelid()
        params["subtitleType"]=2
        params["aggregateMid"]=(tempVodType==0)?temCurrentPlayingDetails["aMid"]:temCurrentPlayingDetails["mid"]
        params["elapsedTime"]=(isResume)?((tempVodType==0)?pif.vkarmaToSeat.getMidElapsedTime(tvModel.getValue(tvIndex,"mid")):pif.vkarmaToSeat.getMidElapsedTime(temCurrentPlayingDetails["mid"])):0
        params["ignoreSystemAspectRatio"]=true
        params["rating"]=viewHelper.gettempPlayingDetails().rating

        if(tempVodType==3 ) params["playIndex"]=getTvIndex()
        else   if(tempVodType==0) {params["playIndex"]=isResume?getTvIndex():0;isTvSeries=true;}
        vodParams=params
        playVideo()
    }
    function playVideo(){
        blockKeysForSeconds(3.5)
        var tempVodType=getTempVodType(gettempPlayingDetails()["vodType"])
        var params=getVodParams()
        var playValue=(isKids(current.template_id))?viewData.kidsMenuModel.getValue(selectedIndex,"category_attr_template_id"):viewData.mainMenuModel.getValue(selectedIndex,"category_attr_template_id")
        if(isFromKarma) pif.vkarmaToSeat.sendUserDefinedApi("userDefinedSendPlayCat",{playingCat:getPlayingCategory()})
        if(isFromKarma) setPlayingCategory(playValue)
        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedVodType",{vodType:gettempPlayingDetails()["vodType"],current_id:current.template_id})
        setVodType(gettempPlayingDetails()["vodType"])
        if(tempVodType==1) {
            pif.vkarmaToSeat.playTrailer(params)
            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedStoreTrailerParentMid",{parentMid:gettempPlayingDetails()["parentMid"]})
        }
        else pif.vkarmaToSeat.playVodByMid(params)
    }
    function getVodParams(){return vodParams }
    function getSoundtracks(mid){
        var sndParams=[];
        sndParams[0]=mid;
        sndParams[2]=dataController.getLidByLanguageISO(getDefaultSndLang());
        dataController.getSoundTracks(sndParams,setSoundTrackQuery);
    }
    function setSoundTrackQuery(dModel){
        setSoundtrackModel(dModel);
        getSubTitlesQuery();
    }

    function getDefaultSndLang(){
        if(isAviancaBrasilAir){
            return "por";
        }else{
            return "spa";
        }
    }

    function getSubTitlesQuery(){ dataController.getSubtitlesWithLabel([tempPlayingDetails["mid"]],setSubtitleQuery,configTool.language.movies.none);}

    function setSubtitleQuery(dModel){
        setSubtitleModel(dModel);
        var subtitle_lid=isResume?pif.getMidInfo(tempPlayingDetails["mid"],'subtitleLid'):((dModel.count>1 && getTempVodType(gettempPlayingDetails()["vodType"])!=1 && subtitleId>=0)?dModel.get(subtitleId).subtitle_lid:-1)
        var soundtrack_lid=isResume?pif.getMidInfo(tempPlayingDetails["mid"],'sndtrkLid'):((getSoundtrackModel().count>0 && getTempVodType(gettempPlayingDetails()["vodType"])!=1 && soundtrackId>0)?getSoundtrackModel().getValue(soundtrackId,"soundtrack_lid"):dataController.getLidByLanguageISO(getDefaultSndLang()));
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> soundtrackinfo = "+soundtrack_lid);
        setSubtitlelid(subtitle_lid)
        setSoundTracklid(soundtrack_lid)
        if(!isFromKarma)return;
        pif.vkarmaToSeat.setPlayingSubtitle(subtitle_lid)
        pif.vkarmaToSeat.setPlayingSoundtrack(soundtrack_lid)
        if(soundtrackModel.count<=1){setVodParams()}
        else viewController.showScreenPopup(1)
    }

    function loadVideoScreen(mid){
        playMovieFromKarma = true;
        var currBacklightStatus = pif.vkarmaToSeat.getBacklightStatus();
        var tempVodType=getTempVodType(gettempPlayingDetails()["vodType"])
        var tempMid=tempVodType==0?gettempPlayingDetails()["aMid"]:gettempPlayingDetails()["mid"]
        if(!currBacklightStatus){
            console.log("Video is about to play but backlight is turned off so turning backlight ON");
            pif.vkarmaToSeat.toggleBacklight();
        }
        if(tempVodType==2 || tempVodType==3){
            if(isResume)setVodParams()
            else getSoundtracks(tempMid)
        }
        else if(tempVodType==1) //trailer
            getSoundtracks(tempMid)
        else if(tempVodType==0){ //tvSeries
            if(isResume)setVodParams()
            else getSoundtracks(tempMid)
        }
    }

    property string  launchAppVkarma:""
    function loadAppScreen(screenName){
        console.log(">>>>>>>>>>>>>>>>>> screenName "+screenName)
        if(screenName=="connecting_gate"||screenName=="airport_map"||screenName=="maps"){
            catSelected="discover"

            delayadded.restart()
            launchAppVkarma=screenName
        }

    }
    Timer{
        id:delayadded
        interval: 600
        onTriggered: {
            selectedIndex=core.coreHelper.searchEntryInModel(viewData.mainMenuModel,"category_attr_template_id","discover")
            viewController.loadScreen("discover")
        }
    }

    /*******************************************************AOD***********************************************/
    /***********tracklist***********/
    function createTracklistStack(){
        Code.tracklistAddStack=new Array()
    }
    function addToTracklistStack(mid){
        Code.tracklistAddStack[mid]=1;
    }
    function removeFromTracklistStack(mid){
        Code.tracklistAddStack[mid]=undefined;
    }
    function clearTracklistStack(){
        Code.tracklistAddStack=new Array()
    }
    function inTracklistStack(mid){
        if(Code.tracklistAddStack[mid]==1){
            return true
        }
        return false;
    }
    /***********playlist***********/
    function createPlaylistStack(){
        Code.playlistAddStack=new Array()
    }
    function addToPlaylistStack(mid){
        Code.playlistAddStack[mid]=1;
    }
    function removeFromPlaylistStack(mid){
        Code.playlistAddStack[mid]=undefined;
    }
    function clearPlaylistStack(){
        Code.playlistAddStack=new Array()
    }
    function inPlaylistStack(mid){
        if(Code.playlistAddStack[mid]==1){
            return true
        }
        return false;
    }

    function getPlaylistStackLen(){
        return    Code.playlistAddStack.length;
    }
    function checkForExternalBlock(params){
        var customName=""
        if(params=="USB")customName="svcSeatUSBMediaPlayer"
        else if(params=="Survey")customName="svcSeatAdvSurveys"
        else if(params=="CG")customName="svcSeatGateConnect"
        else if(params=="Voyager")customName="svcSeatBDVChannelMap"
        else if(params=="Hospitality")customName="svcSeatHospitality"  //its a dummy name
        else if(params=="SeatChat")customName="svcSeatChat"
        if(customName && dataController.mediaServices.getServiceBlockStatus(customName)){
            viewController.showScreenPopup(27)
            return true;
        }
        else return false;
    }
    /*************Function to Block Keys **************************/
    Timer{
        id:blockKeyTimer
        interval:2000
        onTriggered: unblockKeys()
    }
    function blockKeys(){
        core.debug("ViewHelper | blockKeys  | Keys Blocked.....");
        blockKeysFilter.enabled = true;

    }
    function unblockKeys(){
        core.debug("In ViewHelper | unblockKeys | Keys Unblocked.....");
        blockKeysFilter.enabled =false;
    }
    function blockKeysForSeconds(sec){
        blockKeys()
        if(!sec)
            blockKeyTimer.interval = 2000;
        else
            blockKeyTimer.interval = sec * 1000;
        blockKeyTimer.running = true;
    }
    UserEventFilter{
        id: blockKeysFilter;
        enabled:false
        onMousePressed: {core.debug("HelperScreen onMousePressed " );event.accepted=true;}
        onMouseReleased: {core.debug("HelperScreen onMouseReleased") ;event.accepted=true;}
    }
    /*********************************************************************/
    function actionOnServiceBlock(status,serviceCode,cidList){
        if(status=='unblocked')return
        switch(serviceCode){
        case "svcSeatAODMenus":{
            pif.vkarmaToSeat.removeAllFromPlaylist({"playlistIndex":-1,"mediaType":"aod"})
            pif.vkarmaToSeat.removeAllFromPlaylist({"playlistIndex":-2,"mediaType":"aod"})
            pif.vkarmaToSeat.removeAllFromPlaylist({"playlistIndex":-3,"mediaType":"aod"})
            if(pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" && (pif.vkarmaToSeat.getMediaType()=="audioAggregate" || pif.vkarmaToSeat.getMediaType()=="audio")) pif.vkarmaToSeat.stopAudio()
            console.log("viewController.getActiveScreen() "+viewController.getActiveScreen())
            if(current.template_id=='music'|| current.template_id=='music_listing'|| current.template_id=='tracklist'||current.template_id=='artist_listing'|| current.template_id=='genre_listing'|| current.template_id=='playlist'|| current.template_id=='radio_synopsis'|| current.template_id=='kids_music_listing'){
                homeCalled()
                viewController.showScreenPopup(27)
            }
            break;
        }
        case "svcSeatTV":
        case "svcSeatVODMovies":{
            if(pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" && (pif.vkarmaToSeat.getMediaType()=="videoAggregate" || pif.vkarmaToSeat.getMediaType()=="video"))
                pif.vkarmaToSeat.stopVideo();
            if(viewController.getActiveScreenPopupID())viewController.hideScreenPopup();
            if(current.template_id=='movies_listing'|| current.template_id=='tv_listing'|| current.template_id=='movies'|| current.template_id=='tv'|| current.template_id=='synopsis'|| current.template_id=='kids_tv_listing'|| current.template_id=='kids_movies_listing'){
                homeCalled()
                viewController.showScreenPopupFget(27)
            }

            break;
        }
        case "svcSeatGames":{
            if(pif.launchApp.launchId=="games")pif.launchApp.closeApp();
            homeCalled()
            if(status=="blocked" && (current.template_id=="games" ||current.template_id=="kids_games_listing" )){
                homeCalled()
                viewController.showScreenPopup(27)
            }
            break;
        }
        case "svcSeatCatalogShopping":{
            if(viewHelper.catSelected=="shopping"||viewHelper.catSelected=="shopping_listing" || viewHelper.catSelected=="synopsis") {
                console.log(">>>>>>>>>>>>>>>>>>>>> service  blocking ")
                homeCalled();
                viewController.showScreenPopup(27);
            }
            break;
        }
        case "svcSeatGateConnect":{
            if(launchAppVkarma=='connecting_gate'){  homeCalled();viewController.showScreenPopup(27)}
            break;
        }
        case "svcSeatHospitality":{
            if(viewController.getActiveScreen()=="HospSubMenu"||viewController.getActiveScreen()=="HospListing"||viewController.getActiveScreen()=="HospSynopsis"){
                homeCalled();viewController.showScreenPopup(27)};
            break;
        }
        case "svcSeatChat":{
            if(launchAppVkarma=='seatchat'){  homeCalled();viewController.showScreenPopup(27)}
            break;
        }
        case "svcSeatBDVChannelMap":{
            if(widgetList.getMapStatus()){widgetList.setMapStatus(false);viewController.showScreenPopup(27)}
            break;
        }
        }

    }

    function actionOnMidBlock(mid,status,rating){
        //        var mid_status = dataController.mediaServices.getMidBlockStatus(mid,rating);
        //        if(!mid_status) status = "unblocked";
        //        else status = "blocked"
        //        var amid = -1;

        /* Need not to do any calls to stop media because the seat also receives the same event and would act on it */
        /* Karma shall only be a reactor.*/
        //        var curr_playing_audio_mid = pif.vkarmaToSeat.getPlayingMidInfo().mid
        //        if(curr_playing_audio_mid==mid && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" && status=="blocked"&& (pif.vkarmaToSeat.getMediaType()=="audioAggregate" || pif.vkarmaToSeat.getMediaType()=="audio")) pif.vkarmaToSeat.stopAudio();
        //        var curr_playing_video_mid = pif.vkarmaToSeat.getPlayingMidInfo().mid
        //        if(curr_playing_video_mid==mid && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" && status=="blocked"&& (pif.vkarmaToSeat.getMediaType()=="videoAggregate" || pif.vkarmaToSeat.getMediaType()=="video")){
        //            pif.vkarmaToSeat.stopVideo()
        //        }
        //if(status=="blocked")viewController.showScreenPopup(27)
    }

    /**********************************************GAMES*******************************/
    function checkVODGameLaunched(){
        if(((pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate") && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"))
            return true;
        else
            return false;
    }
    /**********************************************SEATCHAT*******************************/
    function isValidSearchString(str){
        core.debug("str "+str)
        if(str == undefined) str = "";
        if(str.trim().length == 0){
            core.debug("qml | isValidSearchString() | Not valid string for Search "+str)
            return false;
        }
        return true;
    }
    /**************************************************KIDS************************************************/
    function isKids(tnode){
        if(tnode=="kids"||
                tnode=="kids_movies_listing"||
                tnode=="kids_music_listing"||
                tnode=="kids_tv_listing"||
                tnode=="kids_games_listing"){
            return true;}
        return false;
    }
    function isVideoPlaying(){
        var currentDetails=gettempPlayingDetails()
        if(((pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="")&& currentDetails["mid"]!=pif.vkarmaToSeat.getPlayingMidInfo().mid  &&(pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate")) || (pif.launchApp.launchId!=""))
            return true;
        else
            return false;
    }
    function isVideoPlayingOnSeat(){
        if((pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="")&&(pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate"))
            return true;
        else
            return false;
    }
    /***********************Objects****************************************/
    SimpleModel{id:blankModel}
    Component{
        id:emptyDel;
        Item{}
    }
    AudioPlayerdataSMClient{id:audioPlayerVkarma}
    ViewKeyListener{id:keyListener}

    ImageStream {
        id: imagegrabber
        ipaddress:pif.getLruData("Map_Streamer_Address");
        port:pif.getLruData("Map_Streamer_Port");
        onNewImageAvailable: {
            core.debug("ViewHelper | Imagegrabber | onNewImageAvailable , url = "+url);
            ixplorImg = url;
        }
    }

    Component.onCompleted: {
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOnScreen,[loadEntOnScreen]);
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOffScreen,[loadEntOffScreen]);
        interactiveStartup.registerToStage(interactiveStartup.cStageCloseFlightScreen,[loadCloseFlightScreen]);
        interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[resetViewVariables]);
    }


}
