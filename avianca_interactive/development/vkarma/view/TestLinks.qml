// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../framework"

Rectangle{
    id:mainRect
    x:105;
    Row{
        y: 0; x:10;
        spacing: 10;
        Rectangle{
            id:link1
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Launch Screen Off"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(5)
                }
            }
        }
        Rectangle{
            id:link2
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1Txt1;anchors.centerIn: parent; color:'#FFFFFF'; text:"Launch Call Crew"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(8)
                }
            }
        }
        Rectangle{
            id:link3
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1Txt2;anchors.centerIn: parent; color:'#FFFFFF'; text:"Launch Reading light"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(9)
                }
            }
        }
        Rectangle{
            id:link4
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1Txt3;anchors.centerIn: parent; color:'#FFFFFF'; text:"ResetVkarma"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    pif.sharedModelServerInterface.sendApiData("init")
                }
            }
        }


    }

    Row{
        y:40;x:10;spacing: 10
        Rectangle{
            id:link11
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link11Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"CG unavailable"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(10)
                }
            }
        }
        Rectangle{
            id:link22
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link11Txt1;anchors.centerIn: parent; color:'#FFFFFF'; text:" Launch Vkb"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(viewController.widgetList.getVkbRef().visible) viewController.widgetList.showHideVKBScreen(false)
                    else viewController.widgetList.showHideVKBScreen(true)
                }
            }
        }
        Rectangle{
            id:link33
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link11Txt2;anchors.centerIn: parent; color:'#FFFFFF'; text:"cg status false"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    core.dataController.connectingGate.sigCgStatusChanged(false)
                }
            }
        }
    }

    Row{
        y: 80; x:10;
        spacing: 10;
        Rectangle{
            id:link13
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link13Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Coming Soon"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(6)
                }
            }
        }
        Rectangle{
            id:link23
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link13Txt1;anchors.centerIn: parent; color:'#FFFFFF'; text:"Loading"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(7)
                }
            }
        }



    }
    Row{
        y: 120; x:10;
        spacing: 10;
        Rectangle{
            id:link53
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link53Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"playlist full"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(14)
                }
            }
        }
        Rectangle{
            id:link253
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link153Txt1;anchors.centerIn: parent; color:'#FFFFFF'; text:"playlist empty"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(15)
                }
            }
        }
        Rectangle{
            id:link2535
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link1535Txt1;anchors.centerIn: parent; color:'#FFFFFF'; text:"block service cid"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    pif.__setBlockServiceMid(27212);
                }
            }
        }



    }
    Row{
        y: 160; x:10;
        spacing: 10;
        Rectangle{
            id:link5d3
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link5d3Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"unblock service cid"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    pif.__setUnBlockServiceMid(27212);
                }
            }
        }
        Rectangle{
            id:link25d3
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link15d3Txt1;anchors.centerIn: parent; color:'#FFFFFF'; text:"cg avaialable"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    core.pif.cgDataAvailableChanged(1)
                    core.dataController.connectingGate.sigCgStatusChanged(true)
                }
            }
        }



    }
    Row{
        y: 200; x:10;
        spacing: 10;
        Rectangle{
            id:link6d3
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link6d3Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"block Mid"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    pif.__setBlockMid(15354);
                }
            }
        }
        Rectangle{
            id:link65d3
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link65d3Txt1;anchors.centerIn: parent; color:'#FFFFFF'; text:"unblock mid"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    pif.__setUnBlockMid(15354);
                }
            }
        }
        Rectangle{
            id:link10link65d5
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link10link65d5Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock Shopping"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link10link65d5Txt.text=="ServiceBlock Shopping"){
                        pif.serviceAccessChanged('blocked',27210)
                        link10link65d5Txt.text="ServiceUnblock Shopping"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',27210)
                        link10link65d5Txt.text="ServiceBlock Shopping"
                    }
                }
            }
        }
    }


    Row{
        y: 240; x:10;
        spacing: 10;
        Rectangle{
            id:link12link65d5
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link10link623d5Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"dailpad"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    viewController.showScreenPopup(29);

                }
            }
        }
        Rectangle{
            id:link13link65d5
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link103ink623d5Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock Hospitality"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link103ink623d5Txt.text=="ServiceBlock Hospitality"){
                        pif.serviceAccessChanged('blocked',27216)
                        link103ink623d5Txt.text="ServiceUnblock Hospitality"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',27216)
                        link103ink623d5Txt.text="ServiceBlock Hospitality"
                    }

                }
            }
        }
        Rectangle{
            id:link14link65d5
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link104ink623d5Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"ServiceBlock SeatChat"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    if(link104ink623d5Txt.text=="ServiceBlock SeatChat"){
                        pif.serviceAccessChanged('blocked',27215)
                        link104ink623d5Txt.text="ServiceUnblock SeatChat"
                    }
                    else{
                        pif.serviceAccessChanged('unblocked',27215)
                        link104ink623d5Txt.text="ServiceBlock SeatChat"
                    }

                }
            }
        }
    }


    Row{
        y: 280; x:10;
        spacing: 10;
        Rectangle{
            id:link14link65d6
            radius: 10;
            color: "#5C5C5C"; width:120; height: 30;
            Text { id:link104ink623d6Txt;anchors.centerIn: parent; color:'#FFFFFF'; text:"Power Down"}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    pif.powerDownEventReceived()
                }
            }
        }
    }
}
