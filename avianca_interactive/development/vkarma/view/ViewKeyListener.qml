import QtQuick 1.1

Item {
    property int fwdKey;
    property int rewKey;
    property int playKey;
    property int stopKey;
    property int orientation;
    function assignKeyMap(){
        if(core.pc){
            fwdKey=41;      // Key F
            rewKey=27;      // Key R;
            playKey=33;     // Key P;
            stopKey=39;     // Key S;
            orientation=32;     // Key O;
        }else{
            fwdKey=173;
            rewKey=168;
            playKey=169;
            stopKey=176;
        }
    }
    function keyOnPressed(event){
        switch(event.nativeScanCode){
        case orientation:
            if (pif.getOrientation()==0) pif.__setOrientation(1);
            else if (pif.getOrientation()==1) pif.__setOrientation(3);
            else if (pif.getOrientation()==3) pif.__setOrientation(0);
            break;

        }
    }
    function keyOnReleased(event){}
    Component.onCompleted: {
        assignKeyMap();
        viewController.setNativeKeyPressed(keyOnPressed);
    }
}
