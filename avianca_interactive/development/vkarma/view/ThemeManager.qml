import QtQuick 1.0
import Panasonic.Pif 1.0

Item {
    id: themeMgrItem

    property string dir : Qt.resolvedUrl("../content/configtool/")
    property alias config : config_object
    property alias language : language_object
    property alias base : myTheme.base
    property alias path : myTheme.path
    property alias name : myTheme.name
    property alias languageName : myTheme.language
    property alias status : myTheme.status

    Theme {
        id: myTheme
        base : dir
        name : "vkarma"
        language : "eng"
        onConfigChanged: {
            themeMgrItem.config.config_value = myTheme.config
        }
        onI18nChanged: {
            themeMgrItem.language.language_value = myTheme.i18n
        }
    }
    QtObject {
        id: config_object
        property variant config_value
        property QtObject discover_l : QtObject {
            id: discover_lId1
            property variant discover_l_object : config_object.config_value.discover_l
            property QtObject listing : QtObject {
                id: listingId1
                property variant listing_object : discover_lId1.discover_l_object.listing
                property variant background : listingId1.listing_object.background.value
                property variant left_arrow : listingId1.listing_object.left_arrow.value
                property variant poster_holder : listingId1.listing_object.poster_holder.value
                property variant right_arrow : listingId1.listing_object.right_arrow.value
                property variant text_color : listingId1.listing_object.text_color.value
                property variant text_color_press : listingId1.listing_object.text_color_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId1
                property variant sndtrk_cc_object : discover_lId1.discover_l_object.sndtrk_cc
                property variant button : sndtrk_ccId1.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId1.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId1.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId1.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId1.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId1.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId1.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId1.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId1.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId1.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId1.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId1.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId1.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId1
                property variant synopsis_object : discover_lId1.discover_l_object.synopsis
                property variant btn : synopsisId1.synopsis_object.btn.value
                property variant btn_press : synopsisId1.synopsis_object.btn_press.value
                property variant btn_text : synopsisId1.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId1.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId1.synopsis_object.category_txt_color.value
                property variant cg_info_text_color : synopsisId1.synopsis_object.cg_info_text_color.value
                property variant description_color : synopsisId1.synopsis_object.description_color.value
                property variant fading_image : synopsisId1.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId1.synopsis_object.left_arrow.value
                property variant listing_highlight : synopsisId1.synopsis_object.listing_highlight.value
                property variant panel : synopsisId1.synopsis_object.panel.value
                property variant right_arrow : synopsisId1.synopsis_object.right_arrow.value
                property variant title_color : synopsisId1.synopsis_object.title_color.value
            }
        }
        property QtObject discover_p : QtObject {
            id: discover_pId1
            property variant discover_p_object : config_object.config_value.discover_p
            property QtObject listing : QtObject {
                id: listingId2
                property variant listing_object : discover_pId1.discover_p_object.listing
                property variant background : listingId2.listing_object.background.value
                property variant divline : listingId2.listing_object.divline.value
                property variant down_arrow : listingId2.listing_object.down_arrow.value
                property variant poster_holder : listingId2.listing_object.poster_holder.value
                property variant text_color : listingId2.listing_object.text_color.value
                property variant text_color_press : listingId2.listing_object.text_color_press.value
                property variant up_arrow : listingId2.listing_object.up_arrow.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId2
                property variant sndtrk_cc_object : discover_pId1.discover_p_object.sndtrk_cc
                property variant button : sndtrk_ccId2.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId2.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId2.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId2.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId2.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId2.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId2.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId2.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId2.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId2.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId2.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId2.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId2.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId2
                property variant synopsis_object : discover_pId1.discover_p_object.synopsis
                property variant btn : synopsisId2.synopsis_object.btn.value
                property variant btn_press : synopsisId2.synopsis_object.btn_press.value
                property variant btn_text : synopsisId2.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId2.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId2.synopsis_object.category_txt_color.value
                property variant cg_info_text_color : synopsisId2.synopsis_object.cg_info_text_color.value
                property variant description_color : synopsisId2.synopsis_object.description_color.value
                property variant fading_image : synopsisId2.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId2.synopsis_object.left_arrow.value
                property variant listing_highlight : synopsisId2.synopsis_object.listing_highlight.value
                property variant panel : synopsisId2.synopsis_object.panel.value
                property variant right_arrow : synopsisId2.synopsis_object.right_arrow.value
                property variant title_color : synopsisId2.synopsis_object.title_color.value
            }
        }
        property QtObject games_l : QtObject {
            id: games_lId1
            property variant games_l_object : config_object.config_value.games_l
            property QtObject listing : QtObject {
                id: listingId3
                property variant listing_object : games_lId1.games_l_object.listing
                property variant background : listingId3.listing_object.background.value
                property variant left_arrow : listingId3.listing_object.left_arrow.value
                property variant poster_holder : listingId3.listing_object.poster_holder.value
                property variant right_arrow : listingId3.listing_object.right_arrow.value
                property variant text_color : listingId3.listing_object.text_color.value
                property variant text_color_press : listingId3.listing_object.text_color_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId3
                property variant sndtrk_cc_object : games_lId1.games_l_object.sndtrk_cc
                property variant button : sndtrk_ccId3.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId3.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId3.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId3.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId3.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId3.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId3.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId3.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId3.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId3.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId3.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId3.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId3.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId3
                property variant synopsis_object : games_lId1.games_l_object.synopsis
                property variant btn : synopsisId3.synopsis_object.btn.value
                property variant btn_press : synopsisId3.synopsis_object.btn_press.value
                property variant btn_text : synopsisId3.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId3.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId3.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId3.synopsis_object.description_color.value
                property variant fading_image : synopsisId3.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId3.synopsis_object.left_arrow.value
                property variant panel : synopsisId3.synopsis_object.panel.value
                property variant right_arrow : synopsisId3.synopsis_object.right_arrow.value
                property variant title_color : synopsisId3.synopsis_object.title_color.value
            }
        }
        property QtObject games_p : QtObject {
            id: games_pId1
            property variant games_p_object : config_object.config_value.games_p
            property QtObject listing : QtObject {
                id: listingId4
                property variant listing_object : games_pId1.games_p_object.listing
                property variant background : listingId4.listing_object.background.value
                property variant divline : listingId4.listing_object.divline.value
                property variant down_arrow : listingId4.listing_object.down_arrow.value
                property variant poster_holder : listingId4.listing_object.poster_holder.value
                property variant text_color : listingId4.listing_object.text_color.value
                property variant text_color_press : listingId4.listing_object.text_color_press.value
                property variant up_arrow : listingId4.listing_object.up_arrow.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId4
                property variant sndtrk_cc_object : games_pId1.games_p_object.sndtrk_cc
                property variant button : sndtrk_ccId4.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId4.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId4.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId4.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId4.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId4.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId4.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId4.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId4.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId4.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId4.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId4.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId4.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId4
                property variant synopsis_object : games_pId1.games_p_object.synopsis
                property variant btn : synopsisId4.synopsis_object.btn.value
                property variant btn_press : synopsisId4.synopsis_object.btn_press.value
                property variant btn_text : synopsisId4.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId4.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId4.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId4.synopsis_object.description_color.value
                property variant fading_image : synopsisId4.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId4.synopsis_object.left_arrow.value
                property variant panel : synopsisId4.synopsis_object.panel.value
                property variant right_arrow : synopsisId4.synopsis_object.right_arrow.value
                property variant title_color : synopsisId4.synopsis_object.title_color.value
            }
        }
        property QtObject global_l : QtObject {
            id: global_lId1
            property variant global_l_object : config_object.config_value.global_l
            property QtObject entertainment_off : QtObject {
                id: entertainment_offId1
                property variant entertainment_off_object : global_lId1.global_l_object.entertainment_off
                property variant entoff_bg : entertainment_offId1.entertainment_off_object.entoff_bg.value
                property variant entoff_logo : entertainment_offId1.entertainment_off_object.entoff_logo.value
                property variant entoff_oceanair_logo : entertainment_offId1.entertainment_off_object.entoff_oceanair_logo.value
            }
            property QtObject flight_info : QtObject {
                id: flight_infoId1
                property variant flight_info_object : global_lId1.global_l_object.flight_info
                property variant airport_map_btn : flight_infoId1.flight_info_object.airport_map_btn.value
                property variant airport_map_btn_press : flight_infoId1.flight_info_object.airport_map_btn_press.value
                property variant button_text : flight_infoId1.flight_info_object.button_text.value
                property variant button_text_press : flight_infoId1.flight_info_object.button_text_press.value
                property variant close_btn : flight_infoId1.flight_info_object.close_btn.value
                property variant close_btn_pressed : flight_infoId1.flight_info_object.close_btn_pressed.value
                property variant flight_connect_btn : flight_infoId1.flight_info_object.flight_connect_btn.value
                property variant flight_connect_btn_press : flight_infoId1.flight_info_object.flight_connect_btn_press.value
                property variant flight_icon : flight_infoId1.flight_info_object.flight_icon.value
                property variant flight_path_base_image : flight_infoId1.flight_info_object.flight_path_base_image.value
                property variant flight_path_circle_image_fill : flight_infoId1.flight_info_object.flight_path_circle_image_fill.value
                property variant flight_path_circle_image_n : flight_infoId1.flight_info_object.flight_path_circle_image_n.value
                property variant flight_path_fill_color : flight_infoId1.flight_info_object.flight_path_fill_color.value
                property variant overlay_icon1 : flight_infoId1.flight_info_object.overlay_icon1.value
                property variant overlay_icon2 : flight_infoId1.flight_info_object.overlay_icon2.value
                property variant overlay_icon3 : flight_infoId1.flight_info_object.overlay_icon3.value
                property variant text_color : flight_infoId1.flight_info_object.text_color.value
                property variant view_map_btn : flight_infoId1.flight_info_object.view_map_btn.value
                property variant view_map_btn_press : flight_infoId1.flight_info_object.view_map_btn_press.value
            }
            property QtObject footer : QtObject {
                id: footerId1
                property variant footer_object : global_lId1.global_l_object.footer
                property variant back_btn : footerId1.footer_object.back_btn.value
                property variant back_btn_press : footerId1.footer_object.back_btn_press.value
                property variant bg_color : footerId1.footer_object.bg_color.value
                property variant home_btn : footerId1.footer_object.home_btn.value
                property variant home_btn_press : footerId1.footer_object.home_btn_press.value
                property variant nowplay_icon : footerId1.footer_object.nowplay_icon.value
                property variant nowplay_icon_press : footerId1.footer_object.nowplay_icon_press.value
                property variant settings_btn : footerId1.footer_object.settings_btn.value
                property variant settings_btn_press : footerId1.footer_object.settings_btn_press.value
            }
            property QtObject header : QtObject {
                id: headerId1
                property variant header_object : global_lId1.global_l_object.header
                property variant flightinfo_button : headerId1.header_object.flightinfo_button.value
                property variant flightinfo_button_press : headerId1.header_object.flightinfo_button_press.value
                property variant header_divline : headerId1.header_object.header_divline.value
                property variant header_text_press : headerId1.header_object.header_text_press.value
                property variant help_button : headerId1.header_object.help_button.value
                property variant help_button_press : headerId1.header_object.help_button_press.value
                property variant text_color : headerId1.header_object.text_color.value
                property variant text_color_press : headerId1.header_object.text_color_press.value
            }
            property QtObject help : QtObject {
                id: helpId1
                property variant help_object : global_lId1.global_l_object.help
                property variant bg_color : helpId1.help_object.bg_color.value
                property variant close_btn : helpId1.help_object.close_btn.value
                property variant close_btn_press : helpId1.help_object.close_btn_press.value
                property variant desc_text : helpId1.help_object.desc_text.value
                property variant fadeimage : helpId1.help_object.fadeimage.value
                property variant help_image : helpId1.help_object.help_image.value
            }
            property QtObject language_sel : QtObject {
                id: language_selId1
                property variant language_sel_object : global_lId1.global_l_object.language_sel
                property variant down_arrow : language_selId1.language_sel_object.down_arrow.value
                property variant lang_bg : language_selId1.language_sel_object.lang_bg.value
                property variant lang_logo : language_selId1.language_sel_object.lang_logo.value
                property variant lang_oceanair_logo : language_selId1.language_sel_object.lang_oceanair_logo.value
                property variant language_text : language_selId1.language_sel_object.language_text.value
                property variant language_text_press : language_selId1.language_sel_object.language_text_press.value
                property variant up_arrow : language_selId1.language_sel_object.up_arrow.value
            }
            property QtObject option_sel : QtObject {
                id: option_selId1
                property variant option_sel_object : global_lId1.global_l_object.option_sel
                property variant adult_btn : option_selId1.option_sel_object.adult_btn.value
                property variant adult_btn_press : option_selId1.option_sel_object.adult_btn_press.value
                property variant adult_btn_txt : option_selId1.option_sel_object.adult_btn_txt.value
                property variant adult_btn_txt_press : option_selId1.option_sel_object.adult_btn_txt_press.value
                property variant kids_btn : option_selId1.option_sel_object.kids_btn.value
                property variant kids_btn_press : option_selId1.option_sel_object.kids_btn_press.value
                property variant kids_btn_txt : option_selId1.option_sel_object.kids_btn_txt.value
                property variant kids_btn_txt_press : option_selId1.option_sel_object.kids_btn_txt_press.value
            }
            property QtObject settings : QtObject {
                id: settingsId1
                property variant settings_object : global_lId1.global_l_object.settings
                property variant brightness_decrease : settingsId1.settings_object.brightness_decrease.value
                property variant brightness_decrease_press : settingsId1.settings_object.brightness_decrease_press.value
                property variant brightness_increase : settingsId1.settings_object.brightness_increase.value
                property variant brightness_increase_press : settingsId1.settings_object.brightness_increase_press.value
                property variant call_crew : settingsId1.settings_object.call_crew.value
                property variant call_crew_cancel : settingsId1.settings_object.call_crew_cancel.value
                property variant call_crew_cancel_press : settingsId1.settings_object.call_crew_cancel_press.value
                property variant call_crew_press : settingsId1.settings_object.call_crew_press.value
                property variant category_title_text_color : settingsId1.settings_object.category_title_text_color.value
                property variant close_btn : settingsId1.settings_object.close_btn.value
                property variant close_btn_press : settingsId1.settings_object.close_btn_press.value
                property variant divline : settingsId1.settings_object.divline.value
                property variant icon : settingsId1.settings_object.icon.value
                property variant indicator : settingsId1.settings_object.indicator.value
                property variant language_text_n : settingsId1.settings_object.language_text_n.value
                property variant language_text_s : settingsId1.settings_object.language_text_s.value
                property variant listing_highlight : settingsId1.settings_object.listing_highlight.value
                property variant reading_light : settingsId1.settings_object.reading_light.value
                property variant reading_light_off : settingsId1.settings_object.reading_light_off.value
                property variant reading_light_off_press : settingsId1.settings_object.reading_light_off_press.value
                property variant reading_light_press : settingsId1.settings_object.reading_light_press.value
                property variant screen_off : settingsId1.settings_object.screen_off.value
                property variant screen_off_press : settingsId1.settings_object.screen_off_press.value
                property variant slider_base : settingsId1.settings_object.slider_base.value
                property variant slider_fill : settingsId1.settings_object.slider_fill.value
                property variant subcategory_title_color : settingsId1.settings_object.subcategory_title_color.value
                property variant text_color : settingsId1.settings_object.text_color.value
                property variant volume_decrease : settingsId1.settings_object.volume_decrease.value
                property variant volume_decrease_press : settingsId1.settings_object.volume_decrease_press.value
                property variant volume_increase : settingsId1.settings_object.volume_increase.value
                property variant volume_increase_press : settingsId1.settings_object.volume_increase_press.value
            }
            property QtObject video_now_playing : QtObject {
                id: video_now_playingId1
                property variant video_now_playing_object : global_lId1.global_l_object.video_now_playing
                property variant  vol_dcr_press : video_now_playingId1.video_now_playing_object. vol_dcr_press.value
                property variant  vol_incr_press : video_now_playingId1.video_now_playing_object. vol_incr_press.value
                property variant aspect1 : video_now_playingId1.video_now_playing_object.aspect1.value
                property variant aspect1_press : video_now_playingId1.video_now_playing_object.aspect1_press.value
                property variant aspect2 : video_now_playingId1.video_now_playing_object.aspect2.value
                property variant aspect2_press : video_now_playingId1.video_now_playing_object.aspect2_press.value
                property variant base_image : video_now_playingId1.video_now_playing_object.base_image.value
                property variant base_image_trailer : video_now_playingId1.video_now_playing_object.base_image_trailer.value
                property variant bright_dcr : video_now_playingId1.video_now_playing_object.bright_dcr.value
                property variant bright_dcr_press : video_now_playingId1.video_now_playing_object.bright_dcr_press.value
                property variant bright_incr : video_now_playingId1.video_now_playing_object.bright_incr.value
                property variant bright_incr_press : video_now_playingId1.video_now_playing_object.bright_incr_press.value
                property variant brightness : video_now_playingId1.video_now_playing_object.brightness.value
                property variant brightness_press : video_now_playingId1.video_now_playing_object.brightness_press.value
                property variant btn : video_now_playingId1.video_now_playing_object.btn.value
                property variant btn_press : video_now_playingId1.video_now_playing_object.btn_press.value
                property variant btn_text : video_now_playingId1.video_now_playing_object.btn_text.value
                property variant fill_color : video_now_playingId1.video_now_playing_object.fill_color.value
                property variant fill_color_trailer : video_now_playingId1.video_now_playing_object.fill_color_trailer.value
                property variant forward : video_now_playingId1.video_now_playing_object.forward.value
                property variant forward_press : video_now_playingId1.video_now_playing_object.forward_press.value
                property variant indicator : video_now_playingId1.video_now_playing_object.indicator.value
                property variant pause : video_now_playingId1.video_now_playing_object.pause.value
                property variant pause_press : video_now_playingId1.video_now_playing_object.pause_press.value
                property variant play : video_now_playingId1.video_now_playing_object.play.value
                property variant play_press : video_now_playingId1.video_now_playing_object.play_press.value
                property variant popup_base_image : video_now_playingId1.video_now_playing_object.popup_base_image.value
                property variant popup_fill_color : video_now_playingId1.video_now_playing_object.popup_fill_color.value
                property variant replay : video_now_playingId1.video_now_playing_object.replay.value
                property variant replay_press : video_now_playingId1.video_now_playing_object.replay_press.value
                property variant rewind : video_now_playingId1.video_now_playing_object.rewind.value
                property variant rewind_press : video_now_playingId1.video_now_playing_object.rewind_press.value
                property variant soundtrackcc : video_now_playingId1.video_now_playing_object.soundtrackcc.value
                property variant soundtrackcc_press : video_now_playingId1.video_now_playing_object.soundtrackcc_press.value
                property variant speed_text : video_now_playingId1.video_now_playing_object.speed_text.value
                property variant stop : video_now_playingId1.video_now_playing_object.stop.value
                property variant stop_press : video_now_playingId1.video_now_playing_object.stop_press.value
                property variant text_color : video_now_playingId1.video_now_playing_object.text_color.value
                property variant vod_controller_bg : video_now_playingId1.video_now_playing_object.vod_controller_bg.value
                property variant vol_dcr : video_now_playingId1.video_now_playing_object.vol_dcr.value
                property variant vol_incr : video_now_playingId1.video_now_playing_object.vol_incr.value
                property variant volume : video_now_playingId1.video_now_playing_object.volume.value
                property variant volume_press : video_now_playingId1.video_now_playing_object.volume_press.value
            }
        }
        property QtObject global_p : QtObject {
            id: global_pId1
            property variant global_p_object : config_object.config_value.global_p
            property QtObject entertainment_off : QtObject {
                id: entertainment_offId2
                property variant entertainment_off_object : global_pId1.global_p_object.entertainment_off
                property variant entoff_bg : entertainment_offId2.entertainment_off_object.entoff_bg.value
                property variant entoff_logo : entertainment_offId2.entertainment_off_object.entoff_logo.value
                property variant entoff_oceanair_logo : entertainment_offId2.entertainment_off_object.entoff_oceanair_logo.value
            }
            property QtObject flight_info : QtObject {
                id: flight_infoId2
                property variant flight_info_object : global_pId1.global_p_object.flight_info
                property variant airport_map_btn : flight_infoId2.flight_info_object.airport_map_btn.value
                property variant airport_map_btn_press : flight_infoId2.flight_info_object.airport_map_btn_press.value
                property variant button_text : flight_infoId2.flight_info_object.button_text.value
                property variant button_text_press : flight_infoId2.flight_info_object.button_text_press.value
                property variant close_btn : flight_infoId2.flight_info_object.close_btn.value
                property variant close_btn_pressed : flight_infoId2.flight_info_object.close_btn_pressed.value
                property variant flight_connect_btn : flight_infoId2.flight_info_object.flight_connect_btn.value
                property variant flight_connect_btn_press : flight_infoId2.flight_info_object.flight_connect_btn_press.value
                property variant flight_icon : flight_infoId2.flight_info_object.flight_icon.value
                property variant flight_path_base_image : flight_infoId2.flight_info_object.flight_path_base_image.value
                property variant flight_path_circle_image_fill : flight_infoId2.flight_info_object.flight_path_circle_image_fill.value
                property variant flight_path_circle_image_n : flight_infoId2.flight_info_object.flight_path_circle_image_n.value
                property variant flight_path_fill_color : flight_infoId2.flight_info_object.flight_path_fill_color.value
                property variant overlay_icon1 : flight_infoId2.flight_info_object.overlay_icon1.value
                property variant overlay_icon2 : flight_infoId2.flight_info_object.overlay_icon2.value
                property variant overlay_icon3 : flight_infoId2.flight_info_object.overlay_icon3.value
                property variant text_color : flight_infoId2.flight_info_object.text_color.value
                property variant view_map_btn : flight_infoId2.flight_info_object.view_map_btn.value
                property variant view_map_btn_press : flight_infoId2.flight_info_object.view_map_btn_press.value
            }
            property QtObject footer : QtObject {
                id: footerId2
                property variant footer_object : global_pId1.global_p_object.footer
                property variant back_btn : footerId2.footer_object.back_btn.value
                property variant back_btn_press : footerId2.footer_object.back_btn_press.value
                property variant bg_color : footerId2.footer_object.bg_color.value
                property variant home_btn : footerId2.footer_object.home_btn.value
                property variant home_btn_press : footerId2.footer_object.home_btn_press.value
                property variant nowplay_icon : footerId2.footer_object.nowplay_icon.value
                property variant nowplay_icon_press : footerId2.footer_object.nowplay_icon_press.value
                property variant settings_btn : footerId2.footer_object.settings_btn.value
                property variant settings_btn_press : footerId2.footer_object.settings_btn_press.value
            }
            property QtObject header : QtObject {
                id: headerId2
                property variant header_object : global_pId1.global_p_object.header
                property variant flightinfo_button : headerId2.header_object.flightinfo_button.value
                property variant flightinfo_button_press : headerId2.header_object.flightinfo_button_press.value
                property variant header_divline : headerId2.header_object.header_divline.value
                property variant header_text_press : headerId2.header_object.header_text_press.value
                property variant help_button : headerId2.header_object.help_button.value
                property variant help_button_press : headerId2.header_object.help_button_press.value
                property variant text_color : headerId2.header_object.text_color.value
                property variant text_color_press : headerId2.header_object.text_color_press.value
            }
            property QtObject help : QtObject {
                id: helpId2
                property variant help_object : global_pId1.global_p_object.help
                property variant bg_color : helpId2.help_object.bg_color.value
                property variant close_btn : helpId2.help_object.close_btn.value
                property variant close_btn_press : helpId2.help_object.close_btn_press.value
                property variant desc_text : helpId2.help_object.desc_text.value
                property variant fadeimage : helpId2.help_object.fadeimage.value
                property variant help_image : helpId2.help_object.help_image.value
            }
            property QtObject language_sel : QtObject {
                id: language_selId2
                property variant language_sel_object : global_pId1.global_p_object.language_sel
                property variant down_arrow : language_selId2.language_sel_object.down_arrow.value
                property variant lang_bg : language_selId2.language_sel_object.lang_bg.value
                property variant lang_logo : language_selId2.language_sel_object.lang_logo.value
                property variant lang_oceanair_logo : language_selId2.language_sel_object.lang_oceanair_logo.value
                property variant language_text : language_selId2.language_sel_object.language_text.value
                property variant language_text_press : language_selId2.language_sel_object.language_text_press.value
                property variant up_arrow : language_selId2.language_sel_object.up_arrow.value
            }
            property QtObject option_sel : QtObject {
                id: option_selId2
                property variant option_sel_object : global_pId1.global_p_object.option_sel
                property variant adult_btn : option_selId2.option_sel_object.adult_btn.value
                property variant adult_btn_press : option_selId2.option_sel_object.adult_btn_press.value
                property variant adult_btn_txt : option_selId2.option_sel_object.adult_btn_txt.value
                property variant adult_btn_txt_press : option_selId2.option_sel_object.adult_btn_txt_press.value
                property variant kids_btn : option_selId2.option_sel_object.kids_btn.value
                property variant kids_btn_press : option_selId2.option_sel_object.kids_btn_press.value
                property variant kids_btn_txt : option_selId2.option_sel_object.kids_btn_txt.value
                property variant kids_btn_txt_press : option_selId2.option_sel_object.kids_btn_txt_press.value
            }
            property QtObject settings : QtObject {
                id: settingsId2
                property variant settings_object : global_pId1.global_p_object.settings
                property variant brightness_decrease : settingsId2.settings_object.brightness_decrease.value
                property variant brightness_decrease_press : settingsId2.settings_object.brightness_decrease_press.value
                property variant brightness_increase : settingsId2.settings_object.brightness_increase.value
                property variant brightness_increase_press : settingsId2.settings_object.brightness_increase_press.value
                property variant call_crew : settingsId2.settings_object.call_crew.value
                property variant call_crew_cancel : settingsId2.settings_object.call_crew_cancel.value
                property variant call_crew_cancel_press : settingsId2.settings_object.call_crew_cancel_press.value
                property variant call_crew_press : settingsId2.settings_object.call_crew_press.value
                property variant category_title_text_color : settingsId2.settings_object.category_title_text_color.value
                property variant close_btn : settingsId2.settings_object.close_btn.value
                property variant close_btn_press : settingsId2.settings_object.close_btn_press.value
                property variant divline : settingsId2.settings_object.divline.value
                property variant fadeimage : settingsId2.settings_object.fadeimage.value
                property variant icon : settingsId2.settings_object.icon.value
                property variant indicator : settingsId2.settings_object.indicator.value
                property variant language_text_n : settingsId2.settings_object.language_text_n.value
                property variant language_text_s : settingsId2.settings_object.language_text_s.value
                property variant listing_highlight : settingsId2.settings_object.listing_highlight.value
                property variant reading_light : settingsId2.settings_object.reading_light.value
                property variant reading_light_off : settingsId2.settings_object.reading_light_off.value
                property variant reading_light_off_press : settingsId2.settings_object.reading_light_off_press.value
                property variant reading_light_press : settingsId2.settings_object.reading_light_press.value
                property variant screen_off : settingsId2.settings_object.screen_off.value
                property variant screen_off_press : settingsId2.settings_object.screen_off_press.value
                property variant slider_base : settingsId2.settings_object.slider_base.value
                property variant slider_fill : settingsId2.settings_object.slider_fill.value
                property variant subcategory_title_color : settingsId2.settings_object.subcategory_title_color.value
                property variant text_color : settingsId2.settings_object.text_color.value
                property variant volume_decrease : settingsId2.settings_object.volume_decrease.value
                property variant volume_decrease_press : settingsId2.settings_object.volume_decrease_press.value
                property variant volume_increase : settingsId2.settings_object.volume_increase.value
                property variant volume_increase_press : settingsId2.settings_object.volume_increase_press.value
            }
            property QtObject video_now_playing : QtObject {
                id: video_now_playingId2
                property variant video_now_playing_object : global_pId1.global_p_object.video_now_playing
                property variant  vol_dcr_press : video_now_playingId2.video_now_playing_object. vol_dcr_press.value
                property variant  vol_incr_press : video_now_playingId2.video_now_playing_object. vol_incr_press.value
                property variant aspect1 : video_now_playingId2.video_now_playing_object.aspect1.value
                property variant aspect1_press : video_now_playingId2.video_now_playing_object.aspect1_press.value
                property variant aspect2 : video_now_playingId2.video_now_playing_object.aspect2.value
                property variant aspect2_press : video_now_playingId2.video_now_playing_object.aspect2_press.value
                property variant base_image : video_now_playingId2.video_now_playing_object.base_image.value
                property variant base_image_trailer : video_now_playingId2.video_now_playing_object.base_image_trailer.value
                property variant bright_dcr : video_now_playingId2.video_now_playing_object.bright_dcr.value
                property variant bright_dcr_press : video_now_playingId2.video_now_playing_object.bright_dcr_press.value
                property variant bright_incr : video_now_playingId2.video_now_playing_object.bright_incr.value
                property variant bright_incr_press : video_now_playingId2.video_now_playing_object.bright_incr_press.value
                property variant brightness : video_now_playingId2.video_now_playing_object.brightness.value
                property variant brightness_press : video_now_playingId2.video_now_playing_object.brightness_press.value
                property variant btn : video_now_playingId2.video_now_playing_object.btn.value
                property variant btn_press : video_now_playingId2.video_now_playing_object.btn_press.value
                property variant btn_text : video_now_playingId2.video_now_playing_object.btn_text.value
                property variant controller_bg : video_now_playingId2.video_now_playing_object.controller_bg.value
                property variant fill_color : video_now_playingId2.video_now_playing_object.fill_color.value
                property variant fill_color_trailer : video_now_playingId2.video_now_playing_object.fill_color_trailer.value
                property variant forward : video_now_playingId2.video_now_playing_object.forward.value
                property variant forward_press : video_now_playingId2.video_now_playing_object.forward_press.value
                property variant indicator : video_now_playingId2.video_now_playing_object.indicator.value
                property variant pause : video_now_playingId2.video_now_playing_object.pause.value
                property variant pause_press : video_now_playingId2.video_now_playing_object.pause_press.value
                property variant play : video_now_playingId2.video_now_playing_object.play.value
                property variant play_press : video_now_playingId2.video_now_playing_object.play_press.value
                property variant popup_base_image : video_now_playingId2.video_now_playing_object.popup_base_image.value
                property variant popup_fill_color : video_now_playingId2.video_now_playing_object.popup_fill_color.value
                property variant replay : video_now_playingId2.video_now_playing_object.replay.value
                property variant replay_press : video_now_playingId2.video_now_playing_object.replay_press.value
                property variant rewind : video_now_playingId2.video_now_playing_object.rewind.value
                property variant rewind_press : video_now_playingId2.video_now_playing_object.rewind_press.value
                property variant soundtrackcc : video_now_playingId2.video_now_playing_object.soundtrackcc.value
                property variant soundtrackcc_press : video_now_playingId2.video_now_playing_object.soundtrackcc_press.value
                property variant speed_text : video_now_playingId2.video_now_playing_object.speed_text.value
                property variant stop : video_now_playingId2.video_now_playing_object.stop.value
                property variant stop_press : video_now_playingId2.video_now_playing_object.stop_press.value
                property variant text_color : video_now_playingId2.video_now_playing_object.text_color.value
                property variant vol_dcr : video_now_playingId2.video_now_playing_object.vol_dcr.value
                property variant vol_incr : video_now_playingId2.video_now_playing_object.vol_incr.value
                property variant volume : video_now_playingId2.video_now_playing_object.volume.value
                property variant volume_press : video_now_playingId2.video_now_playing_object.volume_press.value
            }
        }
        property QtObject kids_games_l : QtObject {
            id: kids_games_lId1
            property variant kids_games_l_object : config_object.config_value.kids_games_l
            property QtObject listing : QtObject {
                id: listingId5
                property variant listing_object : kids_games_lId1.kids_games_l_object.listing
                property variant background : listingId5.listing_object.background.value
                property variant left_arrow : listingId5.listing_object.left_arrow.value
                property variant poster_holder : listingId5.listing_object.poster_holder.value
                property variant right_arrow : listingId5.listing_object.right_arrow.value
                property variant text_color : listingId5.listing_object.text_color.value
                property variant text_color_press : listingId5.listing_object.text_color_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId5
                property variant sndtrk_cc_object : kids_games_lId1.kids_games_l_object.sndtrk_cc
                property variant button : sndtrk_ccId5.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId5.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId5.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId5.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId5.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId5.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId5.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId5.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId5.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId5.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId5.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId5.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId5.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId5
                property variant synopsis_object : kids_games_lId1.kids_games_l_object.synopsis
                property variant btn : synopsisId5.synopsis_object.btn.value
                property variant btn_press : synopsisId5.synopsis_object.btn_press.value
                property variant btn_text : synopsisId5.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId5.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId5.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId5.synopsis_object.description_color.value
                property variant fading_image : synopsisId5.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId5.synopsis_object.left_arrow.value
                property variant panel : synopsisId5.synopsis_object.panel.value
                property variant right_arrow : synopsisId5.synopsis_object.right_arrow.value
                property variant title_color : synopsisId5.synopsis_object.title_color.value
            }
        }
        property QtObject kids_games_p : QtObject {
            id: kids_games_pId1
            property variant kids_games_p_object : config_object.config_value.kids_games_p
            property QtObject listing : QtObject {
                id: listingId6
                property variant listing_object : kids_games_pId1.kids_games_p_object.listing
                property variant background : listingId6.listing_object.background.value
                property variant divline : listingId6.listing_object.divline.value
                property variant down_arrow : listingId6.listing_object.down_arrow.value
                property variant poster_holder : listingId6.listing_object.poster_holder.value
                property variant text_color : listingId6.listing_object.text_color.value
                property variant text_color_press : listingId6.listing_object.text_color_press.value
                property variant up_arrow : listingId6.listing_object.up_arrow.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId6
                property variant sndtrk_cc_object : kids_games_pId1.kids_games_p_object.sndtrk_cc
                property variant button : sndtrk_ccId6.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId6.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId6.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId6.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId6.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId6.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId6.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId6.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId6.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId6.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId6.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId6.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId6.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId6
                property variant synopsis_object : kids_games_pId1.kids_games_p_object.synopsis
                property variant btn : synopsisId6.synopsis_object.btn.value
                property variant btn_press : synopsisId6.synopsis_object.btn_press.value
                property variant btn_text : synopsisId6.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId6.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId6.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId6.synopsis_object.description_color.value
                property variant fading_image : synopsisId6.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId6.synopsis_object.left_arrow.value
                property variant panel : synopsisId6.synopsis_object.panel.value
                property variant right_arrow : synopsisId6.synopsis_object.right_arrow.value
                property variant title_color : synopsisId6.synopsis_object.title_color.value
            }
        }
        property QtObject kids_global_l : QtObject {
            id: kids_global_lId1
            property variant kids_global_l_object : config_object.config_value.kids_global_l
            property QtObject flight_info : QtObject {
                id: flight_infoId3
                property variant flight_info_object : kids_global_lId1.kids_global_l_object.flight_info
                property variant airport_map_btn : flight_infoId3.flight_info_object.airport_map_btn.value
                property variant airport_map_btn_press : flight_infoId3.flight_info_object.airport_map_btn_press.value
                property variant button_text : flight_infoId3.flight_info_object.button_text.value
                property variant button_text_press : flight_infoId3.flight_info_object.button_text_press.value
                property variant close_btn : flight_infoId3.flight_info_object.close_btn.value
                property variant close_btn_pressed : flight_infoId3.flight_info_object.close_btn_pressed.value
                property variant flight_connect_btn : flight_infoId3.flight_info_object.flight_connect_btn.value
                property variant flight_connect_btn_press : flight_infoId3.flight_info_object.flight_connect_btn_press.value
                property variant flight_icon : flight_infoId3.flight_info_object.flight_icon.value
                property variant flight_path_base_image : flight_infoId3.flight_info_object.flight_path_base_image.value
                property variant flight_path_circle_image_fill : flight_infoId3.flight_info_object.flight_path_circle_image_fill.value
                property variant flight_path_circle_image_n : flight_infoId3.flight_info_object.flight_path_circle_image_n.value
                property variant flight_path_fill_color : flight_infoId3.flight_info_object.flight_path_fill_color.value
                property variant overlay_icon1 : flight_infoId3.flight_info_object.overlay_icon1.value
                property variant overlay_icon2 : flight_infoId3.flight_info_object.overlay_icon2.value
                property variant overlay_icon3 : flight_infoId3.flight_info_object.overlay_icon3.value
                property variant text_color : flight_infoId3.flight_info_object.text_color.value
                property variant view_map_btn : flight_infoId3.flight_info_object.view_map_btn.value
                property variant view_map_btn_press : flight_infoId3.flight_info_object.view_map_btn_press.value
            }
            property QtObject footer : QtObject {
                id: footerId3
                property variant footer_object : kids_global_lId1.kids_global_l_object.footer
                property variant back_btn : footerId3.footer_object.back_btn.value
                property variant back_btn_press : footerId3.footer_object.back_btn_press.value
                property variant bg_color : footerId3.footer_object.bg_color.value
                property variant home_btn : footerId3.footer_object.home_btn.value
                property variant home_btn_press : footerId3.footer_object.home_btn_press.value
                property variant nowplay_icon : footerId3.footer_object.nowplay_icon.value
                property variant nowplay_icon_press : footerId3.footer_object.nowplay_icon_press.value
                property variant settings_btn : footerId3.footer_object.settings_btn.value
                property variant settings_btn_press : footerId3.footer_object.settings_btn_press.value
            }
            property QtObject header : QtObject {
                id: headerId3
                property variant header_object : kids_global_lId1.kids_global_l_object.header
                property variant flightinfo_button : headerId3.header_object.flightinfo_button.value
                property variant flightinfo_button_press : headerId3.header_object.flightinfo_button_press.value
                property variant header_divline : headerId3.header_object.header_divline.value
                property variant header_text_press : headerId3.header_object.header_text_press.value
                property variant help_button : headerId3.header_object.help_button.value
                property variant help_button_press : headerId3.header_object.help_button_press.value
                property variant text_color : headerId3.header_object.text_color.value
                property variant text_color_press : headerId3.header_object.text_color_press.value
            }
            property QtObject help : QtObject {
                id: helpId3
                property variant help_object : kids_global_lId1.kids_global_l_object.help
                property variant bg_color : helpId3.help_object.bg_color.value
                property variant close_btn : helpId3.help_object.close_btn.value
                property variant close_btn_press : helpId3.help_object.close_btn_press.value
                property variant desc_text : helpId3.help_object.desc_text.value
                property variant fadeimage : helpId3.help_object.fadeimage.value
                property variant help_image : helpId3.help_object.help_image.value
            }
            property QtObject settings : QtObject {
                id: settingsId3
                property variant settings_object : kids_global_lId1.kids_global_l_object.settings
                property variant brightness_decrease : settingsId3.settings_object.brightness_decrease.value
                property variant brightness_decrease_press : settingsId3.settings_object.brightness_decrease_press.value
                property variant brightness_increase : settingsId3.settings_object.brightness_increase.value
                property variant brightness_increase_press : settingsId3.settings_object.brightness_increase_press.value
                property variant call_crew : settingsId3.settings_object.call_crew.value
                property variant call_crew_cancel : settingsId3.settings_object.call_crew_cancel.value
                property variant call_crew_cancel_press : settingsId3.settings_object.call_crew_cancel_press.value
                property variant call_crew_press : settingsId3.settings_object.call_crew_press.value
                property variant category_title_text_color : settingsId3.settings_object.category_title_text_color.value
                property variant close_btn : settingsId3.settings_object.close_btn.value
                property variant close_btn_press : settingsId3.settings_object.close_btn_press.value
                property variant divline : settingsId3.settings_object.divline.value
                property variant icon : settingsId3.settings_object.icon.value
                property variant indicator : settingsId3.settings_object.indicator.value
                property variant language_text_n : settingsId3.settings_object.language_text_n.value
                property variant language_text_s : settingsId3.settings_object.language_text_s.value
                property variant listing_highlight : settingsId3.settings_object.listing_highlight.value
                property variant reading_light : settingsId3.settings_object.reading_light.value
                property variant reading_light_off : settingsId3.settings_object.reading_light_off.value
                property variant reading_light_off_press : settingsId3.settings_object.reading_light_off_press.value
                property variant reading_light_press : settingsId3.settings_object.reading_light_press.value
                property variant screen_off : settingsId3.settings_object.screen_off.value
                property variant screen_off_press : settingsId3.settings_object.screen_off_press.value
                property variant slider_base : settingsId3.settings_object.slider_base.value
                property variant slider_fill : settingsId3.settings_object.slider_fill.value
                property variant subcategory_title_color : settingsId3.settings_object.subcategory_title_color.value
                property variant text_color : settingsId3.settings_object.text_color.value
                property variant volume_decrease : settingsId3.settings_object.volume_decrease.value
                property variant volume_decrease_press : settingsId3.settings_object.volume_decrease_press.value
                property variant volume_increase : settingsId3.settings_object.volume_increase.value
                property variant volume_increase_press : settingsId3.settings_object.volume_increase_press.value
            }
            property QtObject video_now_playing : QtObject {
                id: video_now_playingId3
                property variant video_now_playing_object : kids_global_lId1.kids_global_l_object.video_now_playing
                property variant  vol_dcr_press : video_now_playingId3.video_now_playing_object. vol_dcr_press.value
                property variant  vol_incr_press : video_now_playingId3.video_now_playing_object. vol_incr_press.value
                property variant aspect1 : video_now_playingId3.video_now_playing_object.aspect1.value
                property variant aspect1_press : video_now_playingId3.video_now_playing_object.aspect1_press.value
                property variant aspect2 : video_now_playingId3.video_now_playing_object.aspect2.value
                property variant aspect2_press : video_now_playingId3.video_now_playing_object.aspect2_press.value
                property variant base_image : video_now_playingId3.video_now_playing_object.base_image.value
                property variant base_image_trailer : video_now_playingId3.video_now_playing_object.base_image_trailer.value
                property variant bright_dcr : video_now_playingId3.video_now_playing_object.bright_dcr.value
                property variant bright_dcr_press : video_now_playingId3.video_now_playing_object.bright_dcr_press.value
                property variant bright_incr : video_now_playingId3.video_now_playing_object.bright_incr.value
                property variant bright_incr_press : video_now_playingId3.video_now_playing_object.bright_incr_press.value
                property variant brightness : video_now_playingId3.video_now_playing_object.brightness.value
                property variant brightness_press : video_now_playingId3.video_now_playing_object.brightness_press.value
                property variant btn : video_now_playingId3.video_now_playing_object.btn.value
                property variant btn_press : video_now_playingId3.video_now_playing_object.btn_press.value
                property variant btn_text : video_now_playingId3.video_now_playing_object.btn_text.value
                property variant fill_color : video_now_playingId3.video_now_playing_object.fill_color.value
                property variant fill_color_trailer : video_now_playingId3.video_now_playing_object.fill_color_trailer.value
                property variant forward : video_now_playingId3.video_now_playing_object.forward.value
                property variant forward_press : video_now_playingId3.video_now_playing_object.forward_press.value
                property variant indicator : video_now_playingId3.video_now_playing_object.indicator.value
                property variant pause : video_now_playingId3.video_now_playing_object.pause.value
                property variant pause_press : video_now_playingId3.video_now_playing_object.pause_press.value
                property variant play : video_now_playingId3.video_now_playing_object.play.value
                property variant play_press : video_now_playingId3.video_now_playing_object.play_press.value
                property variant popup_base_image : video_now_playingId3.video_now_playing_object.popup_base_image.value
                property variant popup_fill_color : video_now_playingId3.video_now_playing_object.popup_fill_color.value
                property variant replay : video_now_playingId3.video_now_playing_object.replay.value
                property variant replay_press : video_now_playingId3.video_now_playing_object.replay_press.value
                property variant rewind : video_now_playingId3.video_now_playing_object.rewind.value
                property variant rewind_press : video_now_playingId3.video_now_playing_object.rewind_press.value
                property variant soundtrackcc : video_now_playingId3.video_now_playing_object.soundtrackcc.value
                property variant soundtrackcc_press : video_now_playingId3.video_now_playing_object.soundtrackcc_press.value
                property variant speed_text : video_now_playingId3.video_now_playing_object.speed_text.value
                property variant stop : video_now_playingId3.video_now_playing_object.stop.value
                property variant stop_press : video_now_playingId3.video_now_playing_object.stop_press.value
                property variant text_color : video_now_playingId3.video_now_playing_object.text_color.value
                property variant vod_controller_bg : video_now_playingId3.video_now_playing_object.vod_controller_bg.value
                property variant vol_dcr : video_now_playingId3.video_now_playing_object.vol_dcr.value
                property variant vol_incr : video_now_playingId3.video_now_playing_object.vol_incr.value
                property variant volume : video_now_playingId3.video_now_playing_object.volume.value
                property variant volume_press : video_now_playingId3.video_now_playing_object.volume_press.value
            }
        }
        property QtObject kids_global_p : QtObject {
            id: kids_global_pId1
            property variant kids_global_p_object : config_object.config_value.kids_global_p
            property QtObject flight_info : QtObject {
                id: flight_infoId4
                property variant flight_info_object : kids_global_pId1.kids_global_p_object.flight_info
                property variant airport_map_btn : flight_infoId4.flight_info_object.airport_map_btn.value
                property variant airport_map_btn_press : flight_infoId4.flight_info_object.airport_map_btn_press.value
                property variant button_text : flight_infoId4.flight_info_object.button_text.value
                property variant button_text_press : flight_infoId4.flight_info_object.button_text_press.value
                property variant close_btn : flight_infoId4.flight_info_object.close_btn.value
                property variant close_btn_pressed : flight_infoId4.flight_info_object.close_btn_pressed.value
                property variant flight_connect_btn : flight_infoId4.flight_info_object.flight_connect_btn.value
                property variant flight_connect_btn_press : flight_infoId4.flight_info_object.flight_connect_btn_press.value
                property variant flight_icon : flight_infoId4.flight_info_object.flight_icon.value
                property variant flight_path_base_image : flight_infoId4.flight_info_object.flight_path_base_image.value
                property variant flight_path_circle_image_fill : flight_infoId4.flight_info_object.flight_path_circle_image_fill.value
                property variant flight_path_circle_image_n : flight_infoId4.flight_info_object.flight_path_circle_image_n.value
                property variant flight_path_fill_color : flight_infoId4.flight_info_object.flight_path_fill_color.value
                property variant overlay_icon1 : flight_infoId4.flight_info_object.overlay_icon1.value
                property variant overlay_icon2 : flight_infoId4.flight_info_object.overlay_icon2.value
                property variant overlay_icon3 : flight_infoId4.flight_info_object.overlay_icon3.value
                property variant text_color : flight_infoId4.flight_info_object.text_color.value
                property variant view_map_btn : flight_infoId4.flight_info_object.view_map_btn.value
                property variant view_map_btn_press : flight_infoId4.flight_info_object.view_map_btn_press.value
            }
            property QtObject footer : QtObject {
                id: footerId4
                property variant footer_object : kids_global_pId1.kids_global_p_object.footer
                property variant back_btn : footerId4.footer_object.back_btn.value
                property variant back_btn_press : footerId4.footer_object.back_btn_press.value
                property variant bg_color : footerId4.footer_object.bg_color.value
                property variant home_btn : footerId4.footer_object.home_btn.value
                property variant home_btn_press : footerId4.footer_object.home_btn_press.value
                property variant nowplay_icon : footerId4.footer_object.nowplay_icon.value
                property variant nowplay_icon_press : footerId4.footer_object.nowplay_icon_press.value
                property variant settings_btn : footerId4.footer_object.settings_btn.value
                property variant settings_btn_press : footerId4.footer_object.settings_btn_press.value
            }
            property QtObject header : QtObject {
                id: headerId4
                property variant header_object : kids_global_pId1.kids_global_p_object.header
                property variant flightinfo_button : headerId4.header_object.flightinfo_button.value
                property variant flightinfo_button_press : headerId4.header_object.flightinfo_button_press.value
                property variant header_divline : headerId4.header_object.header_divline.value
                property variant header_text_press : headerId4.header_object.header_text_press.value
                property variant help_button : headerId4.header_object.help_button.value
                property variant help_button_press : headerId4.header_object.help_button_press.value
                property variant text_color : headerId4.header_object.text_color.value
                property variant text_color_press : headerId4.header_object.text_color_press.value
            }
            property QtObject help : QtObject {
                id: helpId4
                property variant help_object : kids_global_pId1.kids_global_p_object.help
                property variant bg_color : helpId4.help_object.bg_color.value
                property variant close_btn : helpId4.help_object.close_btn.value
                property variant close_btn_press : helpId4.help_object.close_btn_press.value
                property variant desc_text : helpId4.help_object.desc_text.value
                property variant fadeimage : helpId4.help_object.fadeimage.value
                property variant help_image : helpId4.help_object.help_image.value
            }
            property QtObject settings : QtObject {
                id: settingsId4
                property variant settings_object : kids_global_pId1.kids_global_p_object.settings
                property variant brightness_decrease : settingsId4.settings_object.brightness_decrease.value
                property variant brightness_decrease_press : settingsId4.settings_object.brightness_decrease_press.value
                property variant brightness_increase : settingsId4.settings_object.brightness_increase.value
                property variant brightness_increase_press : settingsId4.settings_object.brightness_increase_press.value
                property variant call_crew : settingsId4.settings_object.call_crew.value
                property variant call_crew_cancel : settingsId4.settings_object.call_crew_cancel.value
                property variant call_crew_cancel_press : settingsId4.settings_object.call_crew_cancel_press.value
                property variant call_crew_press : settingsId4.settings_object.call_crew_press.value
                property variant category_title_text_color : settingsId4.settings_object.category_title_text_color.value
                property variant close_btn : settingsId4.settings_object.close_btn.value
                property variant close_btn_press : settingsId4.settings_object.close_btn_press.value
                property variant divline : settingsId4.settings_object.divline.value
                property variant fadeimage : settingsId4.settings_object.fadeimage.value
                property variant icon : settingsId4.settings_object.icon.value
                property variant indicator : settingsId4.settings_object.indicator.value
                property variant language_text_n : settingsId4.settings_object.language_text_n.value
                property variant language_text_s : settingsId4.settings_object.language_text_s.value
                property variant listing_highlight : settingsId4.settings_object.listing_highlight.value
                property variant reading_light : settingsId4.settings_object.reading_light.value
                property variant reading_light_off : settingsId4.settings_object.reading_light_off.value
                property variant reading_light_off_press : settingsId4.settings_object.reading_light_off_press.value
                property variant reading_light_press : settingsId4.settings_object.reading_light_press.value
                property variant screen_off : settingsId4.settings_object.screen_off.value
                property variant screen_off_press : settingsId4.settings_object.screen_off_press.value
                property variant slider_base : settingsId4.settings_object.slider_base.value
                property variant slider_fill : settingsId4.settings_object.slider_fill.value
                property variant subcategory_title_color : settingsId4.settings_object.subcategory_title_color.value
                property variant text_color : settingsId4.settings_object.text_color.value
                property variant volume_decrease : settingsId4.settings_object.volume_decrease.value
                property variant volume_decrease_press : settingsId4.settings_object.volume_decrease_press.value
                property variant volume_increase : settingsId4.settings_object.volume_increase.value
                property variant volume_increase_press : settingsId4.settings_object.volume_increase_press.value
            }
            property QtObject video_now_playing : QtObject {
                id: video_now_playingId4
                property variant video_now_playing_object : kids_global_pId1.kids_global_p_object.video_now_playing
                property variant  vol_dcr_press : video_now_playingId4.video_now_playing_object. vol_dcr_press.value
                property variant  vol_incr_press : video_now_playingId4.video_now_playing_object. vol_incr_press.value
                property variant aspect1 : video_now_playingId4.video_now_playing_object.aspect1.value
                property variant aspect1_press : video_now_playingId4.video_now_playing_object.aspect1_press.value
                property variant aspect2 : video_now_playingId4.video_now_playing_object.aspect2.value
                property variant aspect2_press : video_now_playingId4.video_now_playing_object.aspect2_press.value
                property variant base_image : video_now_playingId4.video_now_playing_object.base_image.value
                property variant base_image_trailer : video_now_playingId4.video_now_playing_object.base_image_trailer.value
                property variant bright_dcr : video_now_playingId4.video_now_playing_object.bright_dcr.value
                property variant bright_dcr_press : video_now_playingId4.video_now_playing_object.bright_dcr_press.value
                property variant bright_incr : video_now_playingId4.video_now_playing_object.bright_incr.value
                property variant bright_incr_press : video_now_playingId4.video_now_playing_object.bright_incr_press.value
                property variant brightness : video_now_playingId4.video_now_playing_object.brightness.value
                property variant brightness_press : video_now_playingId4.video_now_playing_object.brightness_press.value
                property variant btn : video_now_playingId4.video_now_playing_object.btn.value
                property variant btn_press : video_now_playingId4.video_now_playing_object.btn_press.value
                property variant btn_text : video_now_playingId4.video_now_playing_object.btn_text.value
                property variant controller_bg : video_now_playingId4.video_now_playing_object.controller_bg.value
                property variant fill_color : video_now_playingId4.video_now_playing_object.fill_color.value
                property variant fill_color_trailer : video_now_playingId4.video_now_playing_object.fill_color_trailer.value
                property variant forward : video_now_playingId4.video_now_playing_object.forward.value
                property variant forward_press : video_now_playingId4.video_now_playing_object.forward_press.value
                property variant indicator : video_now_playingId4.video_now_playing_object.indicator.value
                property variant pause : video_now_playingId4.video_now_playing_object.pause.value
                property variant pause_press : video_now_playingId4.video_now_playing_object.pause_press.value
                property variant play : video_now_playingId4.video_now_playing_object.play.value
                property variant play_press : video_now_playingId4.video_now_playing_object.play_press.value
                property variant popup_base_image : video_now_playingId4.video_now_playing_object.popup_base_image.value
                property variant popup_fill_color : video_now_playingId4.video_now_playing_object.popup_fill_color.value
                property variant replay : video_now_playingId4.video_now_playing_object.replay.value
                property variant replay_press : video_now_playingId4.video_now_playing_object.replay_press.value
                property variant rewind : video_now_playingId4.video_now_playing_object.rewind.value
                property variant rewind_press : video_now_playingId4.video_now_playing_object.rewind_press.value
                property variant soundtrackcc : video_now_playingId4.video_now_playing_object.soundtrackcc.value
                property variant soundtrackcc_press : video_now_playingId4.video_now_playing_object.soundtrackcc_press.value
                property variant speed_text : video_now_playingId4.video_now_playing_object.speed_text.value
                property variant stop : video_now_playingId4.video_now_playing_object.stop.value
                property variant stop_press : video_now_playingId4.video_now_playing_object.stop_press.value
                property variant text_color : video_now_playingId4.video_now_playing_object.text_color.value
                property variant vol_dcr : video_now_playingId4.video_now_playing_object.vol_dcr.value
                property variant vol_incr : video_now_playingId4.video_now_playing_object.vol_incr.value
                property variant volume : video_now_playingId4.video_now_playing_object.volume.value
                property variant volume_press : video_now_playingId4.video_now_playing_object.volume_press.value
            }
        }
        property QtObject kids_menu_l : QtObject {
            id: kids_menu_lId1
            property variant kids_menu_l_object : config_object.config_value.kids_menu_l
            property QtObject main : QtObject {
                id: mainId1
                property variant main_object : kids_menu_lId1.kids_menu_l_object.main
                property variant bg : mainId1.main_object.bg.value
                property variant btn_press : mainId1.main_object.btn_press.value
                property variant left_arrow : mainId1.main_object.left_arrow.value
                property variant right_arrow : mainId1.main_object.right_arrow.value
                property variant text_color : mainId1.main_object.text_color.value
                property variant text_color_p : mainId1.main_object.text_color_p.value
            }
        }
        property QtObject kids_menu_p : QtObject {
            id: kids_menu_pId1
            property variant kids_menu_p_object : config_object.config_value.kids_menu_p
            property QtObject main : QtObject {
                id: mainId2
                property variant main_object : kids_menu_pId1.kids_menu_p_object.main
                property variant bg : mainId2.main_object.bg.value
                property variant btn_press : mainId2.main_object.btn_press.value
                property variant divline : mainId2.main_object.divline.value
                property variant down_arrow : mainId2.main_object.down_arrow.value
                property variant text_color : mainId2.main_object.text_color.value
                property variant text_color_p : mainId2.main_object.text_color_p.value
                property variant up_arrow : mainId2.main_object.up_arrow.value
            }
        }
        property QtObject kids_movies_l : QtObject {
            id: kids_movies_lId1
            property variant kids_movies_l_object : config_object.config_value.kids_movies_l
            property QtObject listing : QtObject {
                id: listingId7
                property variant listing_object : kids_movies_lId1.kids_movies_l_object.listing
                property variant background : listingId7.listing_object.background.value
                property variant category_txt_color : listingId7.listing_object.category_txt_color.value
                property variant left_arrow : listingId7.listing_object.left_arrow.value
                property variant poster_holder : listingId7.listing_object.poster_holder.value
                property variant right_arrow : listingId7.listing_object.right_arrow.value
                property variant text_color : listingId7.listing_object.text_color.value
                property variant text_color_press : listingId7.listing_object.text_color_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId7
                property variant sndtrk_cc_object : kids_movies_lId1.kids_movies_l_object.sndtrk_cc
                property variant button : sndtrk_ccId7.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId7.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId7.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId7.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId7.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId7.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId7.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId7.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId7.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId7.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId7.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId7.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId7.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId7
                property variant synopsis_object : kids_movies_lId1.kids_movies_l_object.synopsis
                property variant btn : synopsisId7.synopsis_object.btn.value
                property variant btn_press : synopsisId7.synopsis_object.btn_press.value
                property variant btn_text : synopsisId7.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId7.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId7.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId7.synopsis_object.description_color.value
                property variant fading_image : synopsisId7.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId7.synopsis_object.left_arrow.value
                property variant panel : synopsisId7.synopsis_object.panel.value
                property variant right_arrow : synopsisId7.synopsis_object.right_arrow.value
                property variant title_color : synopsisId7.synopsis_object.title_color.value
            }
        }
        property QtObject kids_movies_p : QtObject {
            id: kids_movies_pId1
            property variant kids_movies_p_object : config_object.config_value.kids_movies_p
            property QtObject listing : QtObject {
                id: listingId8
                property variant listing_object : kids_movies_pId1.kids_movies_p_object.listing
                property variant background : listingId8.listing_object.background.value
                property variant category_txt_color : listingId8.listing_object.category_txt_color.value
                property variant divline : listingId8.listing_object.divline.value
                property variant down_arrow : listingId8.listing_object.down_arrow.value
                property variant poster_holder : listingId8.listing_object.poster_holder.value
                property variant text_color : listingId8.listing_object.text_color.value
                property variant text_color_press : listingId8.listing_object.text_color_press.value
                property variant up_arrow : listingId8.listing_object.up_arrow.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId8
                property variant sndtrk_cc_object : kids_movies_pId1.kids_movies_p_object.sndtrk_cc
                property variant button : sndtrk_ccId8.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId8.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId8.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId8.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId8.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId8.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId8.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId8.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId8.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId8.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId8.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId8.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId8.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId8
                property variant synopsis_object : kids_movies_pId1.kids_movies_p_object.synopsis
                property variant btn : synopsisId8.synopsis_object.btn.value
                property variant btn_press : synopsisId8.synopsis_object.btn_press.value
                property variant btn_text : synopsisId8.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId8.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId8.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId8.synopsis_object.description_color.value
                property variant fading_image : synopsisId8.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId8.synopsis_object.left_arrow.value
                property variant panel : synopsisId8.synopsis_object.panel.value
                property variant right_arrow : synopsisId8.synopsis_object.right_arrow.value
                property variant title_color : synopsisId8.synopsis_object.title_color.value
            }
        }
        property QtObject kids_music_l : QtObject {
            id: kids_music_lId1
            property variant kids_music_l_object : config_object.config_value.kids_music_l
            property QtObject listing : QtObject {
                id: listingId9
                property variant listing_object : kids_music_lId1.kids_music_l_object.listing
                property variant background : listingId9.listing_object.background.value
                property variant left_arrow : listingId9.listing_object.left_arrow.value
                property variant poster_holder : listingId9.listing_object.poster_holder.value
                property variant right_arrow : listingId9.listing_object.right_arrow.value
                property variant text_color : listingId9.listing_object.text_color.value
                property variant text_color_press : listingId9.listing_object.text_color_press.value
            }
            property QtObject now_playing : QtObject {
                id: now_playingId1
                property variant now_playing_object : kids_music_lId1.kids_music_l_object.now_playing
                property variant aod_controller_bg : now_playingId1.now_playing_object.aod_controller_bg.value
                property variant base_image : now_playingId1.now_playing_object.base_image.value
                property variant btn : now_playingId1.now_playing_object.btn.value
                property variant btn_press : now_playingId1.now_playing_object.btn_press.value
                property variant btn_text : now_playingId1.now_playing_object.btn_text.value
                property variant btn_text_press : now_playingId1.now_playing_object.btn_text_press.value
                property variant duration_color : now_playingId1.now_playing_object.duration_color.value
                property variant fill_image : now_playingId1.now_playing_object.fill_image.value
                property variant indicator : now_playingId1.now_playing_object.indicator.value
                property variant media_time_color : now_playingId1.now_playing_object.media_time_color.value
                property variant next_button : now_playingId1.now_playing_object.next_button.value
                property variant next_button_press : now_playingId1.now_playing_object.next_button_press.value
                property variant pause_button : now_playingId1.now_playing_object.pause_button.value
                property variant pause_button_press : now_playingId1.now_playing_object.pause_button_press.value
                property variant play_button : now_playingId1.now_playing_object.play_button.value
                property variant play_button_press : now_playingId1.now_playing_object.play_button_press.value
                property variant prev_button : now_playingId1.now_playing_object.prev_button.value
                property variant prev_button_press : now_playingId1.now_playing_object.prev_button_press.value
                property variant repeat_off : now_playingId1.now_playing_object.repeat_off.value
                property variant repeat_off_press : now_playingId1.now_playing_object.repeat_off_press.value
                property variant repeat_on : now_playingId1.now_playing_object.repeat_on.value
                property variant repeat_on_press : now_playingId1.now_playing_object.repeat_on_press.value
                property variant shuffle_off : now_playingId1.now_playing_object.shuffle_off.value
                property variant shuffle_off_press : now_playingId1.now_playing_object.shuffle_off_press.value
                property variant shuffle_on : now_playingId1.now_playing_object.shuffle_on.value
                property variant shuffle_on_press : now_playingId1.now_playing_object.shuffle_on_press.value
                property variant stop : now_playingId1.now_playing_object.stop.value
                property variant stop_press : now_playingId1.now_playing_object.stop_press.value
                property variant title_color : now_playingId1.now_playing_object.title_color.value
                property variant track_name_color : now_playingId1.now_playing_object.track_name_color.value
                property variant volume_button : now_playingId1.now_playing_object.volume_button.value
                property variant volume_button_press : now_playingId1.now_playing_object.volume_button_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId9
                property variant sndtrk_cc_object : kids_music_lId1.kids_music_l_object.sndtrk_cc
                property variant button : sndtrk_ccId9.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId9.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId9.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId9.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId9.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId9.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId9.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId9.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId9.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId9.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId9.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId9.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId9.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject tracklist : QtObject {
                id: tracklistId1
                property variant tracklist_object : kids_music_lId1.kids_music_l_object.tracklist
                property variant btn : tracklistId1.tracklist_object.btn.value
                property variant btn_press : tracklistId1.tracklist_object.btn_press.value
                property variant btn_text : tracklistId1.tracklist_object.btn_text.value
                property variant btn_text_press : tracklistId1.tracklist_object.btn_text_press.value
                property variant check_box : tracklistId1.tracklist_object.check_box.value
                property variant check_box_all : tracklistId1.tracklist_object.check_box_all.value
                property variant check_box_all_press : tracklistId1.tracklist_object.check_box_all_press.value
                property variant check_box_press : tracklistId1.tracklist_object.check_box_press.value
                property variant fading_image : tracklistId1.tracklist_object.fading_image.value
                property variant listing_highlight : tracklistId1.tracklist_object.listing_highlight.value
                property variant nowplay_icon : tracklistId1.tracklist_object.nowplay_icon.value
                property variant panel : tracklistId1.tracklist_object.panel.value
                property variant tick_box : tracklistId1.tracklist_object.tick_box.value
                property variant tick_box_press : tracklistId1.tracklist_object.tick_box_press.value
                property variant tickbox_all : tracklistId1.tracklist_object.tickbox_all.value
                property variant tickbox_all_press : tracklistId1.tracklist_object.tickbox_all_press.value
                property variant title_color : tracklistId1.tracklist_object.title_color.value
                property variant track_title_color : tracklistId1.tracklist_object.track_title_color.value
            }
        }
        property QtObject kids_music_p : QtObject {
            id: kids_music_pId1
            property variant kids_music_p_object : config_object.config_value.kids_music_p
            property QtObject listing : QtObject {
                id: listingId10
                property variant listing_object : kids_music_pId1.kids_music_p_object.listing
                property variant background : listingId10.listing_object.background.value
                property variant divline : listingId10.listing_object.divline.value
                property variant down_arrow : listingId10.listing_object.down_arrow.value
                property variant poster_holder : listingId10.listing_object.poster_holder.value
                property variant text_color : listingId10.listing_object.text_color.value
                property variant text_color_press : listingId10.listing_object.text_color_press.value
                property variant up_arrow : listingId10.listing_object.up_arrow.value
            }
            property QtObject now_playing : QtObject {
                id: now_playingId2
                property variant now_playing_object : kids_music_pId1.kids_music_p_object.now_playing
                property variant aod_controller_bg : now_playingId2.now_playing_object.aod_controller_bg.value
                property variant base_image : now_playingId2.now_playing_object.base_image.value
                property variant btn : now_playingId2.now_playing_object.btn.value
                property variant btn_press : now_playingId2.now_playing_object.btn_press.value
                property variant btn_text : now_playingId2.now_playing_object.btn_text.value
                property variant btn_text_press : now_playingId2.now_playing_object.btn_text_press.value
                property variant duration_color : now_playingId2.now_playing_object.duration_color.value
                property variant fill_image : now_playingId2.now_playing_object.fill_image.value
                property variant indicator : now_playingId2.now_playing_object.indicator.value
                property variant media_time_color : now_playingId2.now_playing_object.media_time_color.value
                property variant next_button : now_playingId2.now_playing_object.next_button.value
                property variant next_button_press : now_playingId2.now_playing_object.next_button_press.value
                property variant pause_button : now_playingId2.now_playing_object.pause_button.value
                property variant pause_button_press : now_playingId2.now_playing_object.pause_button_press.value
                property variant play_button : now_playingId2.now_playing_object.play_button.value
                property variant play_button_press : now_playingId2.now_playing_object.play_button_press.value
                property variant prev_button : now_playingId2.now_playing_object.prev_button.value
                property variant prev_button_press : now_playingId2.now_playing_object.prev_button_press.value
                property variant repeat_off : now_playingId2.now_playing_object.repeat_off.value
                property variant repeat_off_press : now_playingId2.now_playing_object.repeat_off_press.value
                property variant repeat_on : now_playingId2.now_playing_object.repeat_on.value
                property variant repeat_on_press : now_playingId2.now_playing_object.repeat_on_press.value
                property variant shuffle_off : now_playingId2.now_playing_object.shuffle_off.value
                property variant shuffle_off_press : now_playingId2.now_playing_object.shuffle_off_press.value
                property variant shuffle_on : now_playingId2.now_playing_object.shuffle_on.value
                property variant shuffle_on_press : now_playingId2.now_playing_object.shuffle_on_press.value
                property variant stop : now_playingId2.now_playing_object.stop.value
                property variant stop_press : now_playingId2.now_playing_object.stop_press.value
                property variant title_color : now_playingId2.now_playing_object.title_color.value
                property variant track_name_color : now_playingId2.now_playing_object.track_name_color.value
                property variant volume_button : now_playingId2.now_playing_object.volume_button.value
                property variant volume_button_press : now_playingId2.now_playing_object.volume_button_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId10
                property variant sndtrk_cc_object : kids_music_pId1.kids_music_p_object.sndtrk_cc
                property variant button : sndtrk_ccId10.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId10.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId10.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId10.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId10.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId10.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId10.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId10.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId10.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId10.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId10.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId10.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId10.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject tracklist : QtObject {
                id: tracklistId2
                property variant tracklist_object : kids_music_pId1.kids_music_p_object.tracklist
                property variant btn : tracklistId2.tracklist_object.btn.value
                property variant btn_press : tracklistId2.tracklist_object.btn_press.value
                property variant btn_text : tracklistId2.tracklist_object.btn_text.value
                property variant btn_text_press : tracklistId2.tracklist_object.btn_text_press.value
                property variant check_box : tracklistId2.tracklist_object.check_box.value
                property variant check_box_all : tracklistId2.tracklist_object.check_box_all.value
                property variant check_box_all_press : tracklistId2.tracklist_object.check_box_all_press.value
                property variant check_box_press : tracklistId2.tracklist_object.check_box_press.value
                property variant fading_image : tracklistId2.tracklist_object.fading_image.value
                property variant listing_highlight : tracklistId2.tracklist_object.listing_highlight.value
                property variant nowplay_icon : tracklistId2.tracklist_object.nowplay_icon.value
                property variant panel : tracklistId2.tracklist_object.panel.value
                property variant tick_box : tracklistId2.tracklist_object.tick_box.value
                property variant tick_box_press : tracklistId2.tracklist_object.tick_box_press.value
                property variant tickbox_all : tracklistId2.tracklist_object.tickbox_all.value
                property variant tickbox_all_press : tracklistId2.tracklist_object.tickbox_all_press.value
                property variant title_color : tracklistId2.tracklist_object.title_color.value
                property variant track_title_color : tracklistId2.tracklist_object.track_title_color.value
            }
        }
        property QtObject kids_popup_l : QtObject {
            id: kids_popup_lId1
            property variant kids_popup_l_object : config_object.config_value.kids_popup_l
            property QtObject generic : QtObject {
                id: genericId1
                property variant generic_object : kids_popup_lId1.kids_popup_l_object.generic
                property variant bg_color : genericId1.generic_object.bg_color.value
                property variant btn : genericId1.generic_object.btn.value
                property variant btn_press : genericId1.generic_object.btn_press.value
                property variant btn_text : genericId1.generic_object.btn_text.value
                property variant btn_text_press : genericId1.generic_object.btn_text_press.value
                property variant close_btn : genericId1.generic_object.close_btn.value
                property variant close_btn_press : genericId1.generic_object.close_btn_press.value
                property variant text_color : genericId1.generic_object.text_color.value
            }
            property QtObject windowdim_l : QtObject {
                id: windowdim_lId1
                property variant windowdim_l_object : kids_popup_lId1.kids_popup_l_object.windowdim_l
                property variant bg_color : windowdim_lId1.windowdim_l_object.bg_color.value
                property variant btn_bg : windowdim_lId1.windowdim_l_object.btn_bg.value
                property variant close_btn : windowdim_lId1.windowdim_l_object.close_btn.value
                property variant close_btn_press : windowdim_lId1.windowdim_l_object.close_btn_press.value
                property variant desc_text_color : windowdim_lId1.windowdim_l_object.desc_text_color.value
                property variant minus : windowdim_lId1.windowdim_l_object.minus.value
                property variant minus_press : windowdim_lId1.windowdim_l_object.minus_press.value
                property variant plus : windowdim_lId1.windowdim_l_object.plus.value
                property variant plus_press : windowdim_lId1.windowdim_l_object.plus_press.value
                property variant title_text_color : windowdim_lId1.windowdim_l_object.title_text_color.value
            }
        }
        property QtObject kids_popup_p : QtObject {
            id: kids_popup_pId1
            property variant kids_popup_p_object : config_object.config_value.kids_popup_p
            property QtObject generic : QtObject {
                id: genericId2
                property variant generic_object : kids_popup_pId1.kids_popup_p_object.generic
                property variant bg_color : genericId2.generic_object.bg_color.value
                property variant btn : genericId2.generic_object.btn.value
                property variant btn_press : genericId2.generic_object.btn_press.value
                property variant btn_text : genericId2.generic_object.btn_text.value
                property variant btn_text_press : genericId2.generic_object.btn_text_press.value
                property variant close_btn : genericId2.generic_object.close_btn.value
                property variant close_btn_press : genericId2.generic_object.close_btn_press.value
                property variant text_color : genericId2.generic_object.text_color.value
            }
            property QtObject windowdim_p : QtObject {
                id: windowdim_pId1
                property variant windowdim_p_object : kids_popup_pId1.kids_popup_p_object.windowdim_p
                property variant bg_color : windowdim_pId1.windowdim_p_object.bg_color.value
                property variant btn_bg : windowdim_pId1.windowdim_p_object.btn_bg.value
                property variant close_btn : windowdim_pId1.windowdim_p_object.close_btn.value
                property variant close_btn_press : windowdim_pId1.windowdim_p_object.close_btn_press.value
                property variant desc_text_color : windowdim_pId1.windowdim_p_object.desc_text_color.value
                property variant minus : windowdim_pId1.windowdim_p_object.minus.value
                property variant minus_press : windowdim_pId1.windowdim_p_object.minus_press.value
                property variant plus : windowdim_pId1.windowdim_p_object.plus.value
                property variant plus_press : windowdim_pId1.windowdim_p_object.plus_press.value
                property variant title_text_color : windowdim_pId1.windowdim_p_object.title_text_color.value
            }
        }
        property QtObject kids_tv_l : QtObject {
            id: kids_tv_lId1
            property variant kids_tv_l_object : config_object.config_value.kids_tv_l
            property QtObject episode : QtObject {
                id: episodeId1
                property variant episode_object : kids_tv_lId1.kids_tv_l_object.episode
                property variant divline : episodeId1.episode_object.divline.value
                property variant fading_image : episodeId1.episode_object.fading_image.value
                property variant listing_highlight : episodeId1.episode_object.listing_highlight.value
                property variant play_all : episodeId1.episode_object.play_all.value
                property variant play_all_press : episodeId1.episode_object.play_all_press.value
                property variant title_color : episodeId1.episode_object.title_color.value
            }
            property QtObject listing : QtObject {
                id: listingId11
                property variant listing_object : kids_tv_lId1.kids_tv_l_object.listing
                property variant background : listingId11.listing_object.background.value
                property variant left_arrow : listingId11.listing_object.left_arrow.value
                property variant poster_holder : listingId11.listing_object.poster_holder.value
                property variant right_arrow : listingId11.listing_object.right_arrow.value
                property variant text_color : listingId11.listing_object.text_color.value
                property variant text_color_press : listingId11.listing_object.text_color_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId11
                property variant sndtrk_cc_object : kids_tv_lId1.kids_tv_l_object.sndtrk_cc
                property variant button : sndtrk_ccId11.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId11.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId11.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId11.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId11.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId11.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId11.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId11.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId11.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId11.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId11.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId11.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId11.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId9
                property variant synopsis_object : kids_tv_lId1.kids_tv_l_object.synopsis
                property variant btn : synopsisId9.synopsis_object.btn.value
                property variant btn_press : synopsisId9.synopsis_object.btn_press.value
                property variant btn_text : synopsisId9.synopsis_object.btn_text.value
                property variant btn_text_p : synopsisId9.synopsis_object.btn_text_p.value
                property variant category_txt_color : synopsisId9.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId9.synopsis_object.description_color.value
                property variant fading_image : synopsisId9.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId9.synopsis_object.left_arrow.value
                property variant panel : synopsisId9.synopsis_object.panel.value
                property variant right_arrow : synopsisId9.synopsis_object.right_arrow.value
                property variant title_color : synopsisId9.synopsis_object.title_color.value
            }
        }
        property QtObject kids_tv_p : QtObject {
            id: kids_tv_pId1
            property variant kids_tv_p_object : config_object.config_value.kids_tv_p
            property QtObject episode : QtObject {
                id: episodeId2
                property variant episode_object : kids_tv_pId1.kids_tv_p_object.episode
                property variant divline : episodeId2.episode_object.divline.value
                property variant fading_image : episodeId2.episode_object.fading_image.value
                property variant listing_highlight : episodeId2.episode_object.listing_highlight.value
                property variant play_all : episodeId2.episode_object.play_all.value
                property variant play_all_press : episodeId2.episode_object.play_all_press.value
                property variant title_color : episodeId2.episode_object.title_color.value
            }
            property QtObject listing : QtObject {
                id: listingId12
                property variant listing_object : kids_tv_pId1.kids_tv_p_object.listing
                property variant background : listingId12.listing_object.background.value
                property variant divline : listingId12.listing_object.divline.value
                property variant down_arrow : listingId12.listing_object.down_arrow.value
                property variant poster_holder : listingId12.listing_object.poster_holder.value
                property variant text_color : listingId12.listing_object.text_color.value
                property variant text_color_press : listingId12.listing_object.text_color_press.value
                property variant up_arrow : listingId12.listing_object.up_arrow.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId12
                property variant sndtrk_cc_object : kids_tv_pId1.kids_tv_p_object.sndtrk_cc
                property variant button : sndtrk_ccId12.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId12.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId12.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId12.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId12.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId12.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId12.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId12.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId12.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId12.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId12.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId12.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId12.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId10
                property variant synopsis_object : kids_tv_pId1.kids_tv_p_object.synopsis
                property variant btn : synopsisId10.synopsis_object.btn.value
                property variant btn_press : synopsisId10.synopsis_object.btn_press.value
                property variant btn_text : synopsisId10.synopsis_object.btn_text.value
                property variant btn_text_p : synopsisId10.synopsis_object.btn_text_p.value
                property variant category_txt_color : synopsisId10.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId10.synopsis_object.description_color.value
                property variant fading_image : synopsisId10.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId10.synopsis_object.left_arrow.value
                property variant panel : synopsisId10.synopsis_object.panel.value
                property variant right_arrow : synopsisId10.synopsis_object.right_arrow.value
                property variant title_color : synopsisId10.synopsis_object.title_color.value
            }
        }
        property QtObject meal_l : QtObject {
            id: meal_lId1
            property variant meal_l_object : config_object.config_value.meal_l
            property QtObject listing : QtObject {
                id: listingId13
                property variant listing_object : meal_lId1.meal_l_object.listing
                property variant background : listingId13.listing_object.background.value
                property variant btn_press : listingId13.listing_object.btn_press.value
                property variant category_icon : listingId13.listing_object.category_icon.value
                property variant menu_beverages : listingId13.listing_object.menu_beverages.value
                property variant menu_breakfast : listingId13.listing_object.menu_breakfast.value
                property variant menu_liquors : listingId13.listing_object.menu_liquors.value
                property variant menu_lunch : listingId13.listing_object.menu_lunch.value
                property variant text_color : listingId13.listing_object.text_color.value
                property variant text_color_p : listingId13.listing_object.text_color_p.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId13
                property variant sndtrk_cc_object : meal_lId1.meal_l_object.sndtrk_cc
                property variant button : sndtrk_ccId13.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId13.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId13.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId13.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId13.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId13.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId13.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId13.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId13.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId13.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId13.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId13.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId13.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId11
                property variant synopsis_object : meal_lId1.meal_l_object.synopsis
                property variant left_arrow : synopsisId11.synopsis_object.left_arrow.value
                property variant menu_cal_color : synopsisId11.synopsis_object.menu_cal_color.value
                property variant menu_desc_color : synopsisId11.synopsis_object.menu_desc_color.value
                property variant menu_title_color : synopsisId11.synopsis_object.menu_title_color.value
                property variant optn_title_color : synopsisId11.synopsis_object.optn_title_color.value
                property variant optn_title_icon_beverages : synopsisId11.synopsis_object.optn_title_icon_beverages.value
                property variant optn_title_icon_breakfast : synopsisId11.synopsis_object.optn_title_icon_breakfast.value
                property variant optn_title_icon_liquors : synopsisId11.synopsis_object.optn_title_icon_liquors.value
                property variant optn_title_icon_lunch : synopsisId11.synopsis_object.optn_title_icon_lunch.value
                property variant panel : synopsisId11.synopsis_object.panel.value
                property variant right_arrow : synopsisId11.synopsis_object.right_arrow.value
                property variant scroll_fadeimage : synopsisId11.synopsis_object.scroll_fadeimage.value
                property variant title_color : synopsisId11.synopsis_object.title_color.value
            }
        }
        property QtObject meal_p : QtObject {
            id: meal_pId1
            property variant meal_p_object : config_object.config_value.meal_p
            property QtObject listing : QtObject {
                id: listingId14
                property variant listing_object : meal_pId1.meal_p_object.listing
                property variant background : listingId14.listing_object.background.value
                property variant btn_press : listingId14.listing_object.btn_press.value
                property variant category_icon : listingId14.listing_object.category_icon.value
                property variant menu_beverages : listingId14.listing_object.menu_beverages.value
                property variant menu_breakfast : listingId14.listing_object.menu_breakfast.value
                property variant menu_liquors : listingId14.listing_object.menu_liquors.value
                property variant menu_lunch : listingId14.listing_object.menu_lunch.value
                property variant text_color : listingId14.listing_object.text_color.value
                property variant text_color_p : listingId14.listing_object.text_color_p.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId14
                property variant sndtrk_cc_object : meal_pId1.meal_p_object.sndtrk_cc
                property variant button : sndtrk_ccId14.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId14.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId14.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId14.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId14.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId14.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId14.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId14.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId14.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId14.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId14.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId14.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId14.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId12
                property variant synopsis_object : meal_pId1.meal_p_object.synopsis
                property variant left_arrow : synopsisId12.synopsis_object.left_arrow.value
                property variant menu_cal_color : synopsisId12.synopsis_object.menu_cal_color.value
                property variant menu_desc_color : synopsisId12.synopsis_object.menu_desc_color.value
                property variant menu_title_color : synopsisId12.synopsis_object.menu_title_color.value
                property variant optn_title_color : synopsisId12.synopsis_object.optn_title_color.value
                property variant optn_title_icon_beverages : synopsisId12.synopsis_object.optn_title_icon_beverages.value
                property variant optn_title_icon_breakfast : synopsisId12.synopsis_object.optn_title_icon_breakfast.value
                property variant optn_title_icon_liquors : synopsisId12.synopsis_object.optn_title_icon_liquors.value
                property variant optn_title_icon_lunch : synopsisId12.synopsis_object.optn_title_icon_lunch.value
                property variant panel : synopsisId12.synopsis_object.panel.value
                property variant right_arrow : synopsisId12.synopsis_object.right_arrow.value
                property variant scroll_fadeimage : synopsisId12.synopsis_object.scroll_fadeimage.value
                property variant title_color : synopsisId12.synopsis_object.title_color.value
            }
        }
        property QtObject menu_l : QtObject {
            id: menu_lId1
            property variant menu_l_object : config_object.config_value.menu_l
            property QtObject main : QtObject {
                id: mainId3
                property variant main_object : menu_lId1.menu_l_object.main
                property variant bg : mainId3.main_object.bg.value
                property variant btn_press : mainId3.main_object.btn_press.value
                property variant left_arrow : mainId3.main_object.left_arrow.value
                property variant right_arrow : mainId3.main_object.right_arrow.value
                property variant text_color : mainId3.main_object.text_color.value
                property variant text_color_p : mainId3.main_object.text_color_p.value
            }
            property QtObject submenu : QtObject {
                id: submenuId1
                property variant submenu_object : menu_lId1.menu_l_object.submenu
                property variant bg_discover : submenuId1.submenu_object.bg_discover.value
                property variant bg_games : submenuId1.submenu_object.bg_games.value
                property variant bg_movies : submenuId1.submenu_object.bg_movies.value
                property variant bg_music : submenuId1.submenu_object.bg_music.value
                property variant bg_tv : submenuId1.submenu_object.bg_tv.value
                property variant category_txt_color : submenuId1.submenu_object.category_txt_color.value
                property variant left_arrow : submenuId1.submenu_object.left_arrow.value
                property variant right_arrow : submenuId1.submenu_object.right_arrow.value
                property variant text_color : submenuId1.submenu_object.text_color.value
                property variant text_color_press : submenuId1.submenu_object.text_color_press.value
            }
        }
        property QtObject menu_p : QtObject {
            id: menu_pId1
            property variant menu_p_object : config_object.config_value.menu_p
            property QtObject main : QtObject {
                id: mainId4
                property variant main_object : menu_pId1.menu_p_object.main
                property variant bg : mainId4.main_object.bg.value
                property variant btn_press : mainId4.main_object.btn_press.value
                property variant divline : mainId4.main_object.divline.value
                property variant down_arrow : mainId4.main_object.down_arrow.value
                property variant text_color : mainId4.main_object.text_color.value
                property variant text_color_p : mainId4.main_object.text_color_p.value
                property variant up_arrow : mainId4.main_object.up_arrow.value
            }
            property QtObject submenu : QtObject {
                id: submenuId2
                property variant submenu_object : menu_pId1.menu_p_object.submenu
                property variant bg_discover : submenuId2.submenu_object.bg_discover.value
                property variant bg_games : submenuId2.submenu_object.bg_games.value
                property variant bg_movies : submenuId2.submenu_object.bg_movies.value
                property variant bg_music : submenuId2.submenu_object.bg_music.value
                property variant bg_tv : submenuId2.submenu_object.bg_tv.value
                property variant category_txt_color : submenuId2.submenu_object.category_txt_color.value
                property variant divline : submenuId2.submenu_object.divline.value
                property variant down_arrow : submenuId2.submenu_object.down_arrow.value
                property variant text_color : submenuId2.submenu_object.text_color.value
                property variant text_color_press : submenuId2.submenu_object.text_color_press.value
                property variant up_arrow : submenuId2.submenu_object.up_arrow.value
            }
        }
        property QtObject movies_l : QtObject {
            id: movies_lId1
            property variant movies_l_object : config_object.config_value.movies_l
            property QtObject listing : QtObject {
                id: listingId15
                property variant listing_object : movies_lId1.movies_l_object.listing
                property variant background : listingId15.listing_object.background.value
                property variant category_txt_color : listingId15.listing_object.category_txt_color.value
                property variant left_arrow : listingId15.listing_object.left_arrow.value
                property variant poster_holder : listingId15.listing_object.poster_holder.value
                property variant right_arrow : listingId15.listing_object.right_arrow.value
                property variant text_color : listingId15.listing_object.text_color.value
                property variant text_color_press : listingId15.listing_object.text_color_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId15
                property variant sndtrk_cc_object : movies_lId1.movies_l_object.sndtrk_cc
                property variant button : sndtrk_ccId15.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId15.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId15.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId15.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId15.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId15.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId15.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId15.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId15.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId15.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId15.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId15.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId15.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId13
                property variant synopsis_object : movies_lId1.movies_l_object.synopsis
                property variant btn : synopsisId13.synopsis_object.btn.value
                property variant btn_press : synopsisId13.synopsis_object.btn_press.value
                property variant btn_text : synopsisId13.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId13.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId13.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId13.synopsis_object.description_color.value
                property variant fading_image : synopsisId13.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId13.synopsis_object.left_arrow.value
                property variant panel : synopsisId13.synopsis_object.panel.value
                property variant right_arrow : synopsisId13.synopsis_object.right_arrow.value
                property variant title_color : synopsisId13.synopsis_object.title_color.value
            }
        }
        property QtObject movies_p : QtObject {
            id: movies_pId1
            property variant movies_p_object : config_object.config_value.movies_p
            property QtObject listing : QtObject {
                id: listingId16
                property variant listing_object : movies_pId1.movies_p_object.listing
                property variant background : listingId16.listing_object.background.value
                property variant category_txt_color : listingId16.listing_object.category_txt_color.value
                property variant divline : listingId16.listing_object.divline.value
                property variant down_arrow : listingId16.listing_object.down_arrow.value
                property variant poster_holder : listingId16.listing_object.poster_holder.value
                property variant text_color : listingId16.listing_object.text_color.value
                property variant text_color_press : listingId16.listing_object.text_color_press.value
                property variant up_arrow : listingId16.listing_object.up_arrow.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId16
                property variant sndtrk_cc_object : movies_pId1.movies_p_object.sndtrk_cc
                property variant button : sndtrk_ccId16.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId16.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId16.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId16.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId16.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId16.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId16.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId16.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId16.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId16.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId16.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId16.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId16.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId14
                property variant synopsis_object : movies_pId1.movies_p_object.synopsis
                property variant btn : synopsisId14.synopsis_object.btn.value
                property variant btn_press : synopsisId14.synopsis_object.btn_press.value
                property variant btn_text : synopsisId14.synopsis_object.btn_text.value
                property variant btn_text_press : synopsisId14.synopsis_object.btn_text_press.value
                property variant category_txt_color : synopsisId14.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId14.synopsis_object.description_color.value
                property variant fading_image : synopsisId14.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId14.synopsis_object.left_arrow.value
                property variant panel : synopsisId14.synopsis_object.panel.value
                property variant right_arrow : synopsisId14.synopsis_object.right_arrow.value
                property variant title_color : synopsisId14.synopsis_object.title_color.value
            }
        }
        property QtObject music_l : QtObject {
            id: music_lId1
            property variant music_l_object : config_object.config_value.music_l
            property QtObject addtracks : QtObject {
                id: addtracksId1
                property variant addtracks_object : music_lId1.music_l_object.addtracks
                property variant button : addtracksId1.addtracks_object.button.value
                property variant button_press : addtracksId1.addtracks_object.button_press.value
                property variant text_colo_pressr : addtracksId1.addtracks_object.text_colo_pressr.value
                property variant text_color : addtracksId1.addtracks_object.text_color.value
            }
            property QtObject listing : QtObject {
                id: listingId17
                property variant listing_object : music_lId1.music_l_object.listing
                property variant background : listingId17.listing_object.background.value
                property variant left_arrow : listingId17.listing_object.left_arrow.value
                property variant poster_holder : listingId17.listing_object.poster_holder.value
                property variant right_arrow : listingId17.listing_object.right_arrow.value
                property variant text_color : listingId17.listing_object.text_color.value
                property variant text_color_press : listingId17.listing_object.text_color_press.value
            }
            property QtObject now_playing : QtObject {
                id: now_playingId3
                property variant now_playing_object : music_lId1.music_l_object.now_playing
                property variant aod_controller_bg : now_playingId3.now_playing_object.aod_controller_bg.value
                property variant base_image : now_playingId3.now_playing_object.base_image.value
                property variant btn : now_playingId3.now_playing_object.btn.value
                property variant btn_press : now_playingId3.now_playing_object.btn_press.value
                property variant btn_text : now_playingId3.now_playing_object.btn_text.value
                property variant btn_text_press : now_playingId3.now_playing_object.btn_text_press.value
                property variant duration_color : now_playingId3.now_playing_object.duration_color.value
                property variant fill_image : now_playingId3.now_playing_object.fill_image.value
                property variant indicator : now_playingId3.now_playing_object.indicator.value
                property variant media_time_color : now_playingId3.now_playing_object.media_time_color.value
                property variant next_button : now_playingId3.now_playing_object.next_button.value
                property variant next_button_press : now_playingId3.now_playing_object.next_button_press.value
                property variant pause_button : now_playingId3.now_playing_object.pause_button.value
                property variant pause_button_press : now_playingId3.now_playing_object.pause_button_press.value
                property variant play_button : now_playingId3.now_playing_object.play_button.value
                property variant play_button_press : now_playingId3.now_playing_object.play_button_press.value
                property variant prev_button : now_playingId3.now_playing_object.prev_button.value
                property variant prev_button_press : now_playingId3.now_playing_object.prev_button_press.value
                property variant repeat_off : now_playingId3.now_playing_object.repeat_off.value
                property variant repeat_off_press : now_playingId3.now_playing_object.repeat_off_press.value
                property variant repeat_on : now_playingId3.now_playing_object.repeat_on.value
                property variant repeat_on_press : now_playingId3.now_playing_object.repeat_on_press.value
                property variant shuffle_off : now_playingId3.now_playing_object.shuffle_off.value
                property variant shuffle_off_press : now_playingId3.now_playing_object.shuffle_off_press.value
                property variant shuffle_on : now_playingId3.now_playing_object.shuffle_on.value
                property variant shuffle_on_press : now_playingId3.now_playing_object.shuffle_on_press.value
                property variant stop : now_playingId3.now_playing_object.stop.value
                property variant stop_press : now_playingId3.now_playing_object.stop_press.value
                property variant title_color : now_playingId3.now_playing_object.title_color.value
                property variant track_name_color : now_playingId3.now_playing_object.track_name_color.value
                property variant volume_button : now_playingId3.now_playing_object.volume_button.value
                property variant volume_button_press : now_playingId3.now_playing_object.volume_button_press.value
            }
            property QtObject playlist : QtObject {
                id: playlistId1
                property variant playlist_object : music_lId1.music_l_object.playlist
                property variant btn : playlistId1.playlist_object.btn.value
                property variant btn_press : playlistId1.playlist_object.btn_press.value
                property variant btn_text : playlistId1.playlist_object.btn_text.value
                property variant btn_text_press : playlistId1.playlist_object.btn_text_press.value
                property variant category_txt_color : playlistId1.playlist_object.category_txt_color.value
                property variant divline : playlistId1.playlist_object.divline.value
                property variant playlist_list_highlight : playlistId1.playlist_object.playlist_list_highlight.value
                property variant playlist_panel : playlistId1.playlist_object.playlist_panel.value
            }
            property QtObject radio : QtObject {
                id: radioId1
                property variant radio_object : music_lId1.music_l_object.radio
                property variant btn : radioId1.radio_object.btn.value
                property variant btn_press : radioId1.radio_object.btn_press.value
                property variant btn_text : radioId1.radio_object.btn_text.value
                property variant btn_text_press : radioId1.radio_object.btn_text_press.value
                property variant description_color : radioId1.radio_object.description_color.value
                property variant title_color : radioId1.radio_object.title_color.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId17
                property variant sndtrk_cc_object : music_lId1.music_l_object.sndtrk_cc
                property variant button : sndtrk_ccId17.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId17.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId17.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId17.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId17.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId17.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId17.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId17.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId17.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId17.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId17.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId17.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId17.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject tracklist : QtObject {
                id: tracklistId3
                property variant tracklist_object : music_lId1.music_l_object.tracklist
                property variant btn : tracklistId3.tracklist_object.btn.value
                property variant btn_press : tracklistId3.tracklist_object.btn_press.value
                property variant btn_text : tracklistId3.tracklist_object.btn_text.value
                property variant btn_text_press : tracklistId3.tracklist_object.btn_text_press.value
                property variant check_box : tracklistId3.tracklist_object.check_box.value
                property variant check_box_all : tracklistId3.tracklist_object.check_box_all.value
                property variant check_box_all_press : tracklistId3.tracklist_object.check_box_all_press.value
                property variant check_box_press : tracklistId3.tracklist_object.check_box_press.value
                property variant fading_image : tracklistId3.tracklist_object.fading_image.value
                property variant listing_highlight : tracklistId3.tracklist_object.listing_highlight.value
                property variant nowplay_icon : tracklistId3.tracklist_object.nowplay_icon.value
                property variant panel : tracklistId3.tracklist_object.panel.value
                property variant tick_box : tracklistId3.tracklist_object.tick_box.value
                property variant tick_box_press : tracklistId3.tracklist_object.tick_box_press.value
                property variant tickbox_all : tracklistId3.tracklist_object.tickbox_all.value
                property variant tickbox_all_press : tracklistId3.tracklist_object.tickbox_all_press.value
                property variant title_color : tracklistId3.tracklist_object.title_color.value
                property variant track_title_color : tracklistId3.tracklist_object.track_title_color.value
            }
        }
        property QtObject music_p : QtObject {
            id: music_pId1
            property variant music_p_object : config_object.config_value.music_p
            property QtObject addtracks : QtObject {
                id: addtracksId2
                property variant addtracks_object : music_pId1.music_p_object.addtracks
                property variant button : addtracksId2.addtracks_object.button.value
                property variant button_press : addtracksId2.addtracks_object.button_press.value
                property variant text_colo_pressr : addtracksId2.addtracks_object.text_colo_pressr.value
                property variant text_color : addtracksId2.addtracks_object.text_color.value
            }
            property QtObject listing : QtObject {
                id: listingId18
                property variant listing_object : music_pId1.music_p_object.listing
                property variant background : listingId18.listing_object.background.value
                property variant divline : listingId18.listing_object.divline.value
                property variant down_arrow : listingId18.listing_object.down_arrow.value
                property variant poster_holder : listingId18.listing_object.poster_holder.value
                property variant text_color : listingId18.listing_object.text_color.value
                property variant text_color_press : listingId18.listing_object.text_color_press.value
                property variant up_arrow : listingId18.listing_object.up_arrow.value
            }
            property QtObject now_playing : QtObject {
                id: now_playingId4
                property variant now_playing_object : music_pId1.music_p_object.now_playing
                property variant aod_controller_bg : now_playingId4.now_playing_object.aod_controller_bg.value
                property variant base_image : now_playingId4.now_playing_object.base_image.value
                property variant btn : now_playingId4.now_playing_object.btn.value
                property variant btn_press : now_playingId4.now_playing_object.btn_press.value
                property variant btn_text : now_playingId4.now_playing_object.btn_text.value
                property variant btn_text_press : now_playingId4.now_playing_object.btn_text_press.value
                property variant duration_color : now_playingId4.now_playing_object.duration_color.value
                property variant fill_image : now_playingId4.now_playing_object.fill_image.value
                property variant indicator : now_playingId4.now_playing_object.indicator.value
                property variant media_time_color : now_playingId4.now_playing_object.media_time_color.value
                property variant next_button : now_playingId4.now_playing_object.next_button.value
                property variant next_button_press : now_playingId4.now_playing_object.next_button_press.value
                property variant pause_button : now_playingId4.now_playing_object.pause_button.value
                property variant pause_button_press : now_playingId4.now_playing_object.pause_button_press.value
                property variant play_button : now_playingId4.now_playing_object.play_button.value
                property variant play_button_press : now_playingId4.now_playing_object.play_button_press.value
                property variant prev_button : now_playingId4.now_playing_object.prev_button.value
                property variant prev_button_press : now_playingId4.now_playing_object.prev_button_press.value
                property variant repeat_off : now_playingId4.now_playing_object.repeat_off.value
                property variant repeat_off_press : now_playingId4.now_playing_object.repeat_off_press.value
                property variant repeat_on : now_playingId4.now_playing_object.repeat_on.value
                property variant repeat_on_press : now_playingId4.now_playing_object.repeat_on_press.value
                property variant shuffle_off : now_playingId4.now_playing_object.shuffle_off.value
                property variant shuffle_off_press : now_playingId4.now_playing_object.shuffle_off_press.value
                property variant shuffle_on : now_playingId4.now_playing_object.shuffle_on.value
                property variant shuffle_on_press : now_playingId4.now_playing_object.shuffle_on_press.value
                property variant stop : now_playingId4.now_playing_object.stop.value
                property variant stop_press : now_playingId4.now_playing_object.stop_press.value
                property variant title_color : now_playingId4.now_playing_object.title_color.value
                property variant track_name_color : now_playingId4.now_playing_object.track_name_color.value
                property variant volume_button : now_playingId4.now_playing_object.volume_button.value
                property variant volume_button_press : now_playingId4.now_playing_object.volume_button_press.value
            }
            property QtObject playlist : QtObject {
                id: playlistId2
                property variant playlist_object : music_pId1.music_p_object.playlist
                property variant btn : playlistId2.playlist_object.btn.value
                property variant btn_press : playlistId2.playlist_object.btn_press.value
                property variant btn_text : playlistId2.playlist_object.btn_text.value
                property variant btn_text_press : playlistId2.playlist_object.btn_text_press.value
                property variant category_txt_color : playlistId2.playlist_object.category_txt_color.value
                property variant divline : playlistId2.playlist_object.divline.value
                property variant playlist_panel : playlistId2.playlist_object.playlist_panel.value
                property variant playlist_pist_highlight : playlistId2.playlist_object.playlist_pist_highlight.value
            }
            property QtObject radio : QtObject {
                id: radioId2
                property variant radio_object : music_pId1.music_p_object.radio
                property variant btn : radioId2.radio_object.btn.value
                property variant btn_press : radioId2.radio_object.btn_press.value
                property variant btn_text : radioId2.radio_object.btn_text.value
                property variant btn_text_press : radioId2.radio_object.btn_text_press.value
                property variant description_color : radioId2.radio_object.description_color.value
                property variant title_color : radioId2.radio_object.title_color.value
                property variant track_title_color : radioId2.radio_object.track_title_color.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId18
                property variant sndtrk_cc_object : music_pId1.music_p_object.sndtrk_cc
                property variant button : sndtrk_ccId18.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId18.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId18.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId18.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId18.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId18.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId18.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId18.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId18.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId18.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId18.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId18.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId18.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject tracklist : QtObject {
                id: tracklistId4
                property variant tracklist_object : music_pId1.music_p_object.tracklist
                property variant btn : tracklistId4.tracklist_object.btn.value
                property variant btn_press : tracklistId4.tracklist_object.btn_press.value
                property variant btn_text : tracklistId4.tracklist_object.btn_text.value
                property variant btn_text_press : tracklistId4.tracklist_object.btn_text_press.value
                property variant check_box : tracklistId4.tracklist_object.check_box.value
                property variant check_box_all : tracklistId4.tracklist_object.check_box_all.value
                property variant check_box_all_press : tracklistId4.tracklist_object.check_box_all_press.value
                property variant check_box_press : tracklistId4.tracklist_object.check_box_press.value
                property variant fading_image : tracklistId4.tracklist_object.fading_image.value
                property variant listing_highlight : tracklistId4.tracklist_object.listing_highlight.value
                property variant nowplay_icon : tracklistId4.tracklist_object.nowplay_icon.value
                property variant panel : tracklistId4.tracklist_object.panel.value
                property variant tick_box : tracklistId4.tracklist_object.tick_box.value
                property variant tick_box_press : tracklistId4.tracklist_object.tick_box_press.value
                property variant tickbox_all : tracklistId4.tracklist_object.tickbox_all.value
                property variant tickbox_all_press : tracklistId4.tracklist_object.tickbox_all_press.value
                property variant title_color : tracklistId4.tracklist_object.title_color.value
                property variant track_title_color : tracklistId4.tracklist_object.track_title_color.value
            }
        }
        property QtObject popup_l : QtObject {
            id: popup_lId1
            property variant popup_l_object : config_object.config_value.popup_l
            property QtObject generic : QtObject {
                id: genericId3
                property variant generic_object : popup_lId1.popup_l_object.generic
                property variant bg_color : genericId3.generic_object.bg_color.value
                property variant btn : genericId3.generic_object.btn.value
                property variant btn_press : genericId3.generic_object.btn_press.value
                property variant btn_text : genericId3.generic_object.btn_text.value
                property variant btn_text_press : genericId3.generic_object.btn_text_press.value
                property variant close_btn : genericId3.generic_object.close_btn.value
                property variant close_btn_press : genericId3.generic_object.close_btn_press.value
                property variant text_color : genericId3.generic_object.text_color.value
            }
            property QtObject windowdim_l : QtObject {
                id: windowdim_lId2
                property variant windowdim_l_object : popup_lId1.popup_l_object.windowdim_l
                property variant bg_color : windowdim_lId2.windowdim_l_object.bg_color.value
                property variant btn_bg : windowdim_lId2.windowdim_l_object.btn_bg.value
                property variant close_btn : windowdim_lId2.windowdim_l_object.close_btn.value
                property variant close_btn_press : windowdim_lId2.windowdim_l_object.close_btn_press.value
                property variant desc_text_color : windowdim_lId2.windowdim_l_object.desc_text_color.value
                property variant minus : windowdim_lId2.windowdim_l_object.minus.value
                property variant minus_press : windowdim_lId2.windowdim_l_object.minus_press.value
                property variant plus : windowdim_lId2.windowdim_l_object.plus.value
                property variant plus_press : windowdim_lId2.windowdim_l_object.plus_press.value
                property variant title_text_color : windowdim_lId2.windowdim_l_object.title_text_color.value
            }
        }
        property QtObject popup_p : QtObject {
            id: popup_pId1
            property variant popup_p_object : config_object.config_value.popup_p
            property QtObject generic : QtObject {
                id: genericId4
                property variant generic_object : popup_pId1.popup_p_object.generic
                property variant bg_color : genericId4.generic_object.bg_color.value
                property variant btn : genericId4.generic_object.btn.value
                property variant btn_press : genericId4.generic_object.btn_press.value
                property variant btn_text : genericId4.generic_object.btn_text.value
                property variant btn_text_press : genericId4.generic_object.btn_text_press.value
                property variant close_btn : genericId4.generic_object.close_btn.value
                property variant close_btn_press : genericId4.generic_object.close_btn_press.value
                property variant text_color : genericId4.generic_object.text_color.value
            }
            property QtObject windowdim_p : QtObject {
                id: windowdim_pId2
                property variant windowdim_p_object : popup_pId1.popup_p_object.windowdim_p
                property variant bg_color : windowdim_pId2.windowdim_p_object.bg_color.value
                property variant btn_bg : windowdim_pId2.windowdim_p_object.btn_bg.value
                property variant close_btn : windowdim_pId2.windowdim_p_object.close_btn.value
                property variant close_btn_press : windowdim_pId2.windowdim_p_object.close_btn_press.value
                property variant desc_text_color : windowdim_pId2.windowdim_p_object.desc_text_color.value
                property variant minus : windowdim_pId2.windowdim_p_object.minus.value
                property variant minus_press : windowdim_pId2.windowdim_p_object.minus_press.value
                property variant plus : windowdim_pId2.windowdim_p_object.plus.value
                property variant plus_press : windowdim_pId2.windowdim_p_object.plus_press.value
                property variant title_text_color : windowdim_pId2.windowdim_p_object.title_text_color.value
            }
        }
        property QtObject seatchat_l : QtObject {
            id: seatchat_lId1
            property variant seatchat_l_object : config_object.config_value.seatchat_l
            property QtObject screen : QtObject {
                id: screenId1
                property variant screen_object : seatchat_lId1.seatchat_l_object.screen
                property variant backspace_icon : screenId1.screen_object.backspace_icon.value
                property variant bg_panel : screenId1.screen_object.bg_panel.value
                property variant close_btn : screenId1.screen_object.close_btn.value
                property variant close_btn_pressed : screenId1.screen_object.close_btn_pressed.value
                property variant conference_btn : screenId1.screen_object.conference_btn.value
                property variant conference_btn_press : screenId1.screen_object.conference_btn_press.value
                property variant conference_btn_txt : screenId1.screen_object.conference_btn_txt.value
                property variant conference_btn_txt_press : screenId1.screen_object.conference_btn_txt_press.value
                property variant dial_btn : screenId1.screen_object.dial_btn.value
                property variant dial_btn_press : screenId1.screen_object.dial_btn_press.value
                property variant dial_btn_press_text : screenId1.screen_object.dial_btn_press_text.value
                property variant dial_btn_text : screenId1.screen_object.dial_btn_text.value
                property variant input_add_btn : screenId1.screen_object.input_add_btn.value
                property variant input_add_btn_press : screenId1.screen_object.input_add_btn_press.value
                property variant input_error_text_color : screenId1.screen_object.input_error_text_color.value
                property variant input_text_color : screenId1.screen_object.input_text_color.value
                property variant left_arrow : screenId1.screen_object.left_arrow.value
                property variant left_arrow_press : screenId1.screen_object.left_arrow_press.value
                property variant right_arrow : screenId1.screen_object.right_arrow.value
                property variant right_arrow_press : screenId1.screen_object.right_arrow_press.value
                property variant send_btn : screenId1.screen_object.send_btn.value
                property variant send_btn_press : screenId1.screen_object.send_btn_press.value
                property variant send_btn_text : screenId1.screen_object.send_btn_text.value
                property variant title_text_color : screenId1.screen_object.title_text_color.value
            }
        }
        property QtObject shop_l : QtObject {
            id: shop_lId1
            property variant shop_l_object : config_object.config_value.shop_l
            property QtObject listing : QtObject {
                id: listingId19
                property variant listing_object : shop_lId1.shop_l_object.listing
                property variant background : listingId19.listing_object.background.value
                property variant left_arrow : listingId19.listing_object.left_arrow.value
                property variant poster_holder : listingId19.listing_object.poster_holder.value
                property variant right_arrow : listingId19.listing_object.right_arrow.value
                property variant text_color : listingId19.listing_object.text_color.value
                property variant text_color_press : listingId19.listing_object.text_color_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId19
                property variant sndtrk_cc_object : shop_lId1.shop_l_object.sndtrk_cc
                property variant button : sndtrk_ccId19.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId19.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId19.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId19.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId19.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId19.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId19.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId19.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId19.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId19.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId19.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId19.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId19.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId15
                property variant synopsis_object : shop_lId1.shop_l_object.synopsis
                property variant category_txt_color : synopsisId15.synopsis_object.category_txt_color.value
                property variant currency_color : synopsisId15.synopsis_object.currency_color.value
                property variant description_color : synopsisId15.synopsis_object.description_color.value
                property variant fading_image : synopsisId15.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId15.synopsis_object.left_arrow.value
                property variant panel : synopsisId15.synopsis_object.panel.value
                property variant price_color : synopsisId15.synopsis_object.price_color.value
                property variant right_arrow : synopsisId15.synopsis_object.right_arrow.value
                property variant shortitle_color : synopsisId15.synopsis_object.shortitle_color.value
                property variant title_color : synopsisId15.synopsis_object.title_color.value
            }
        }
        property QtObject shop_p : QtObject {
            id: shop_pId1
            property variant shop_p_object : config_object.config_value.shop_p
            property QtObject listing : QtObject {
                id: listingId20
                property variant listing_object : shop_pId1.shop_p_object.listing
                property variant background : listingId20.listing_object.background.value
                property variant divline : listingId20.listing_object.divline.value
                property variant down_arrow : listingId20.listing_object.down_arrow.value
                property variant poster_holder : listingId20.listing_object.poster_holder.value
                property variant text_color : listingId20.listing_object.text_color.value
                property variant text_color_press : listingId20.listing_object.text_color_press.value
                property variant up_arrow : listingId20.listing_object.up_arrow.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId20
                property variant sndtrk_cc_object : shop_pId1.shop_p_object.sndtrk_cc
                property variant button : sndtrk_ccId20.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId20.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId20.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId20.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId20.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId20.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId20.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId20.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId20.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId20.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId20.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId20.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId20.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId16
                property variant synopsis_object : shop_pId1.shop_p_object.synopsis
                property variant category_txt_color : synopsisId16.synopsis_object.category_txt_color.value
                property variant currency_color : synopsisId16.synopsis_object.currency_color.value
                property variant description_color : synopsisId16.synopsis_object.description_color.value
                property variant fading_image : synopsisId16.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId16.synopsis_object.left_arrow.value
                property variant panel : synopsisId16.synopsis_object.panel.value
                property variant price_color : synopsisId16.synopsis_object.price_color.value
                property variant right_arrow : synopsisId16.synopsis_object.right_arrow.value
                property variant shortitle_color : synopsisId16.synopsis_object.shortitle_color.value
                property variant title_color : synopsisId16.synopsis_object.title_color.value
            }
        }
        property QtObject tv_l : QtObject {
            id: tv_lId1
            property variant tv_l_object : config_object.config_value.tv_l
            property QtObject episode : QtObject {
                id: episodeId3
                property variant episode_object : tv_lId1.tv_l_object.episode
                property variant divline : episodeId3.episode_object.divline.value
                property variant fading_image : episodeId3.episode_object.fading_image.value
                property variant listing_highlight : episodeId3.episode_object.listing_highlight.value
                property variant play_all : episodeId3.episode_object.play_all.value
                property variant play_all_press : episodeId3.episode_object.play_all_press.value
                property variant title_color : episodeId3.episode_object.title_color.value
            }
            property QtObject listing : QtObject {
                id: listingId21
                property variant listing_object : tv_lId1.tv_l_object.listing
                property variant background : listingId21.listing_object.background.value
                property variant left_arrow : listingId21.listing_object.left_arrow.value
                property variant poster_holder : listingId21.listing_object.poster_holder.value
                property variant right_arrow : listingId21.listing_object.right_arrow.value
                property variant text_color : listingId21.listing_object.text_color.value
                property variant text_color_press : listingId21.listing_object.text_color_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId21
                property variant sndtrk_cc_object : tv_lId1.tv_l_object.sndtrk_cc
                property variant button : sndtrk_ccId21.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId21.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId21.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId21.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId21.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId21.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId21.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId21.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId21.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId21.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId21.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId21.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId21.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId17
                property variant synopsis_object : tv_lId1.tv_l_object.synopsis
                property variant btn : synopsisId17.synopsis_object.btn.value
                property variant btn_press : synopsisId17.synopsis_object.btn_press.value
                property variant btn_text : synopsisId17.synopsis_object.btn_text.value
                property variant btn_text_p : synopsisId17.synopsis_object.btn_text_p.value
                property variant category_txt_color : synopsisId17.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId17.synopsis_object.description_color.value
                property variant fading_image : synopsisId17.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId17.synopsis_object.left_arrow.value
                property variant panel : synopsisId17.synopsis_object.panel.value
                property variant right_arrow : synopsisId17.synopsis_object.right_arrow.value
                property variant title_color : synopsisId17.synopsis_object.title_color.value
            }
        }
        property QtObject tv_p : QtObject {
            id: tv_pId1
            property variant tv_p_object : config_object.config_value.tv_p
            property QtObject episode : QtObject {
                id: episodeId4
                property variant episode_object : tv_pId1.tv_p_object.episode
                property variant divline : episodeId4.episode_object.divline.value
                property variant fading_image : episodeId4.episode_object.fading_image.value
                property variant listing_highlight : episodeId4.episode_object.listing_highlight.value
                property variant play_all : episodeId4.episode_object.play_all.value
                property variant play_all_press : episodeId4.episode_object.play_all_press.value
                property variant title_color : episodeId4.episode_object.title_color.value
            }
            property QtObject listing : QtObject {
                id: listingId22
                property variant listing_object : tv_pId1.tv_p_object.listing
                property variant background : listingId22.listing_object.background.value
                property variant divline : listingId22.listing_object.divline.value
                property variant down_arrow : listingId22.listing_object.down_arrow.value
                property variant poster_holder : listingId22.listing_object.poster_holder.value
                property variant text_color : listingId22.listing_object.text_color.value
                property variant text_color_press : listingId22.listing_object.text_color_press.value
                property variant up_arrow : listingId22.listing_object.up_arrow.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId22
                property variant sndtrk_cc_object : tv_pId1.tv_p_object.sndtrk_cc
                property variant button : sndtrk_ccId22.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId22.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId22.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId22.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId22.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId22.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId22.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId22.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId22.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId22.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId22.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId22.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId22.sndtrk_cc_object.sndtrck_popup_bg.value
            }
            property QtObject synopsis : QtObject {
                id: synopsisId18
                property variant synopsis_object : tv_pId1.tv_p_object.synopsis
                property variant btn : synopsisId18.synopsis_object.btn.value
                property variant btn_press : synopsisId18.synopsis_object.btn_press.value
                property variant btn_text : synopsisId18.synopsis_object.btn_text.value
                property variant btn_text_p : synopsisId18.synopsis_object.btn_text_p.value
                property variant category_txt_color : synopsisId18.synopsis_object.category_txt_color.value
                property variant description_color : synopsisId18.synopsis_object.description_color.value
                property variant fading_image : synopsisId18.synopsis_object.fading_image.value
                property variant left_arrow : synopsisId18.synopsis_object.left_arrow.value
                property variant panel : synopsisId18.synopsis_object.panel.value
                property variant right_arrow : synopsisId18.synopsis_object.right_arrow.value
                property variant title_color : synopsisId18.synopsis_object.title_color.value
            }
        }
        property QtObject usb_l : QtObject {
            id: usb_lId1
            property variant usb_l_object : config_object.config_value.usb_l
            property QtObject now_playing : QtObject {
                id: now_playingId5
                property variant now_playing_object : usb_lId1.usb_l_object.now_playing
                property variant aod_controller_bg : now_playingId5.now_playing_object.aod_controller_bg.value
                property variant base_image : now_playingId5.now_playing_object.base_image.value
                property variant btn : now_playingId5.now_playing_object.btn.value
                property variant btn_press : now_playingId5.now_playing_object.btn_press.value
                property variant btn_text : now_playingId5.now_playing_object.btn_text.value
                property variant btn_text_press : now_playingId5.now_playing_object.btn_text_press.value
                property variant duration_color : now_playingId5.now_playing_object.duration_color.value
                property variant fill_image : now_playingId5.now_playing_object.fill_image.value
                property variant indicator : now_playingId5.now_playing_object.indicator.value
                property variant media_time_color : now_playingId5.now_playing_object.media_time_color.value
                property variant next_button : now_playingId5.now_playing_object.next_button.value
                property variant next_button_press : now_playingId5.now_playing_object.next_button_press.value
                property variant pause_button : now_playingId5.now_playing_object.pause_button.value
                property variant pause_button_press : now_playingId5.now_playing_object.pause_button_press.value
                property variant play_button : now_playingId5.now_playing_object.play_button.value
                property variant play_button_press : now_playingId5.now_playing_object.play_button_press.value
                property variant prev_button : now_playingId5.now_playing_object.prev_button.value
                property variant prev_button_press : now_playingId5.now_playing_object.prev_button_press.value
                property variant repeat_off : now_playingId5.now_playing_object.repeat_off.value
                property variant repeat_off_press : now_playingId5.now_playing_object.repeat_off_press.value
                property variant repeat_on : now_playingId5.now_playing_object.repeat_on.value
                property variant repeat_on_press : now_playingId5.now_playing_object.repeat_on_press.value
                property variant shuffle_off : now_playingId5.now_playing_object.shuffle_off.value
                property variant shuffle_off_press : now_playingId5.now_playing_object.shuffle_off_press.value
                property variant shuffle_on : now_playingId5.now_playing_object.shuffle_on.value
                property variant shuffle_on_press : now_playingId5.now_playing_object.shuffle_on_press.value
                property variant stop : now_playingId5.now_playing_object.stop.value
                property variant stop_press : now_playingId5.now_playing_object.stop_press.value
                property variant title_color : now_playingId5.now_playing_object.title_color.value
                property variant track_name_color : now_playingId5.now_playing_object.track_name_color.value
                property variant volume_button : now_playingId5.now_playing_object.volume_button.value
                property variant volume_button_press : now_playingId5.now_playing_object.volume_button_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId23
                property variant sndtrk_cc_object : usb_lId1.usb_l_object.sndtrk_cc
                property variant button : sndtrk_ccId23.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId23.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId23.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId23.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId23.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId23.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId23.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId23.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId23.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId23.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId23.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId23.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId23.sndtrk_cc_object.sndtrck_popup_bg.value
            }
        }
        property QtObject usb_p : QtObject {
            id: usb_pId1
            property variant usb_p_object : config_object.config_value.usb_p
            property QtObject now_playing : QtObject {
                id: now_playingId6
                property variant now_playing_object : usb_pId1.usb_p_object.now_playing
                property variant aod_controller_bg : now_playingId6.now_playing_object.aod_controller_bg.value
                property variant base_image : now_playingId6.now_playing_object.base_image.value
                property variant btn : now_playingId6.now_playing_object.btn.value
                property variant btn_press : now_playingId6.now_playing_object.btn_press.value
                property variant btn_text : now_playingId6.now_playing_object.btn_text.value
                property variant btn_text_press : now_playingId6.now_playing_object.btn_text_press.value
                property variant duration_color : now_playingId6.now_playing_object.duration_color.value
                property variant fill_image : now_playingId6.now_playing_object.fill_image.value
                property variant indicator : now_playingId6.now_playing_object.indicator.value
                property variant media_time_color : now_playingId6.now_playing_object.media_time_color.value
                property variant next_button : now_playingId6.now_playing_object.next_button.value
                property variant next_button_press : now_playingId6.now_playing_object.next_button_press.value
                property variant pause_button : now_playingId6.now_playing_object.pause_button.value
                property variant pause_button_press : now_playingId6.now_playing_object.pause_button_press.value
                property variant play_button : now_playingId6.now_playing_object.play_button.value
                property variant play_button_press : now_playingId6.now_playing_object.play_button_press.value
                property variant prev_button : now_playingId6.now_playing_object.prev_button.value
                property variant prev_button_press : now_playingId6.now_playing_object.prev_button_press.value
                property variant repeat_off : now_playingId6.now_playing_object.repeat_off.value
                property variant repeat_off_press : now_playingId6.now_playing_object.repeat_off_press.value
                property variant repeat_on : now_playingId6.now_playing_object.repeat_on.value
                property variant repeat_on_press : now_playingId6.now_playing_object.repeat_on_press.value
                property variant shuffle_off : now_playingId6.now_playing_object.shuffle_off.value
                property variant shuffle_off_press : now_playingId6.now_playing_object.shuffle_off_press.value
                property variant shuffle_on : now_playingId6.now_playing_object.shuffle_on.value
                property variant shuffle_on_press : now_playingId6.now_playing_object.shuffle_on_press.value
                property variant stop : now_playingId6.now_playing_object.stop.value
                property variant stop_press : now_playingId6.now_playing_object.stop_press.value
                property variant title_color : now_playingId6.now_playing_object.title_color.value
                property variant track_name_color : now_playingId6.now_playing_object.track_name_color.value
                property variant volume_button : now_playingId6.now_playing_object.volume_button.value
                property variant volume_button_press : now_playingId6.now_playing_object.volume_button_press.value
            }
            property QtObject sndtrk_cc : QtObject {
                id: sndtrk_ccId24
                property variant sndtrk_cc_object : usb_pId1.usb_p_object.sndtrk_cc
                property variant button : sndtrk_ccId24.sndtrk_cc_object.button.value
                property variant button_press : sndtrk_ccId24.sndtrk_cc_object.button_press.value
                property variant button_text : sndtrk_ccId24.sndtrk_cc_object.button_text.value
                property variant button_text_press : sndtrk_ccId24.sndtrk_cc_object.button_text_press.value
                property variant language_background : sndtrk_ccId24.sndtrk_cc_object.language_background.value
                property variant language_text_color : sndtrk_ccId24.sndtrk_cc_object.language_text_color.value
                property variant language_text_color_sel : sndtrk_ccId24.sndtrk_cc_object.language_text_color_sel.value
                property variant left_arrow : sndtrk_ccId24.sndtrk_cc_object.left_arrow.value
                property variant left_arrow_press : sndtrk_ccId24.sndtrk_cc_object.left_arrow_press.value
                property variant popup_text_color : sndtrk_ccId24.sndtrk_cc_object.popup_text_color.value
                property variant right_arrow : sndtrk_ccId24.sndtrk_cc_object.right_arrow.value
                property variant right_arrow_press : sndtrk_ccId24.sndtrk_cc_object.right_arrow_press.value
                property variant sndtrck_popup_bg : sndtrk_ccId24.sndtrk_cc_object.sndtrck_popup_bg.value
            }
        }
        property QtObject vkb_l : QtObject {
            id: vkb_lId1
            property variant vkb_l_object : config_object.config_value.vkb_l
            property QtObject screen : QtObject {
                id: screenId2
                property variant screen_object : vkb_lId1.vkb_l_object.screen
                property variant backspace_icon : screenId2.screen_object.backspace_icon.value
                property variant bg_panel : screenId2.screen_object.bg_panel.value
                property variant confirm_btn : screenId2.screen_object.confirm_btn.value
                property variant confirm_btn_press : screenId2.screen_object.confirm_btn_press.value
                property variant discard_btn : screenId2.screen_object.discard_btn.value
                property variant discard_btn_press : screenId2.screen_object.discard_btn_press.value
                property variant enlarge_btn : screenId2.screen_object.enlarge_btn.value
                property variant enlarge_btn_text : screenId2.screen_object.enlarge_btn_text.value
                property variant extend_arrow : screenId2.screen_object.extend_arrow.value
                property variant extend_chara_text : screenId2.screen_object.extend_chara_text.value
                property variant extended_popup : screenId2.screen_object.extended_popup.value
                property variant extended_popup_line_color : screenId2.screen_object.extended_popup_line_color.value
                property variant key_caps : screenId2.screen_object.key_caps.value
                property variant key_caps_press : screenId2.screen_object.key_caps_press.value
                property variant key_colapse : screenId2.screen_object.key_colapse.value
                property variant key_common_key : screenId2.screen_object.key_common_key.value
                property variant key_common_key_press : screenId2.screen_object.key_common_key_press.value
                property variant key_common_key_sel : screenId2.screen_object.key_common_key_sel.value
                property variant key_down_arrow : screenId2.screen_object.key_down_arrow.value
                property variant key_enter : screenId2.screen_object.key_enter.value
                property variant key_enter_press : screenId2.screen_object.key_enter_press.value
                property variant key_left_arrow : screenId2.screen_object.key_left_arrow.value
                property variant key_num : screenId2.screen_object.key_num.value
                property variant key_num_press : screenId2.screen_object.key_num_press.value
                property variant key_right_arrow : screenId2.screen_object.key_right_arrow.value
                property variant key_spacebar : screenId2.screen_object.key_spacebar.value
                property variant key_spacebar_press : screenId2.screen_object.key_spacebar_press.value
                property variant key_up_arrow : screenId2.screen_object.key_up_arrow.value
                property variant remaining_text : screenId2.screen_object.remaining_text.value
                property variant shift_icon : screenId2.screen_object.shift_icon.value
                property variant text_box_text : screenId2.screen_object.text_box_text.value
            }
        }
    }
    QtObject {
        id: language_object
        property variant language_value
        property QtObject connecting_gate : QtObject {
            id: connecting_gateId1
            property variant connecting_gate_object : language_object.language_value.connecting_gate
            property variant arrival_information : connecting_gateId1.connecting_gate_object.arrival_information.value
            property variant arrival_terminal : connecting_gateId1.connecting_gate_object.arrival_terminal.value
            property variant arrival_time : connecting_gateId1.connecting_gate_object.arrival_time.value
            property variant back : connecting_gateId1.connecting_gate_object.back.value
            property variant baggage_carousel : connecting_gateId1.connecting_gate_object.baggage_carousel.value
            property variant baggage_info : connecting_gateId1.connecting_gate_object.baggage_info.value
            property variant cg_info_available : connecting_gateId1.connecting_gate_object.cg_info_available.value
            property variant cg_info_notavailable : connecting_gateId1.connecting_gate_object.cg_info_notavailable.value
            property variant cg_info_update : connecting_gateId1.connecting_gate_object.cg_info_update.value
            property variant codeshare_flight : connecting_gateId1.connecting_gate_object.codeshare_flight.value
            property variant connecting_flights : connecting_gateId1.connecting_gate_object.connecting_flights.value
            property variant current_flight_txt : connecting_gateId1.connecting_gate_object.current_flight_txt.value
            property variant departure_gate : connecting_gateId1.connecting_gate_object.departure_gate.value
            property variant departure_time : connecting_gateId1.connecting_gate_object.departure_time.value
            property variant destination : connecting_gateId1.connecting_gate_object.destination.value
            property variant flight : connecting_gateId1.connecting_gate_object.flight.value
            property variant flight_number : connecting_gateId1.connecting_gate_object.flight_number.value
            property variant header_destination : connecting_gateId1.connecting_gate_object.header_destination.value
            property variant header_gate : connecting_gateId1.connecting_gate_object.header_gate.value
            property variant schedule_dept_date : connecting_gateId1.connecting_gate_object.schedule_dept_date.value
            property variant schedule_dept_time : connecting_gateId1.connecting_gate_object.schedule_dept_time.value
            property variant status : connecting_gateId1.connecting_gate_object.status.value
        }
        property QtObject games : QtObject {
            id: gamesId1
            property variant games_object : language_object.language_value.games
            property variant gameaudiomsg : gamesId1.games_object.gameaudiomsg.value
            property variant gamebtntxt : gamesId1.games_object.gamebtntxt.value
            property variant loading : gamesId1.games_object.loading.value
            property variant musicbtntxt : gamesId1.games_object.musicbtntxt.value
            property variant play : gamesId1.games_object.play.value
        }
        property QtObject global_elements : QtObject {
            id: global_elementsId1
            property variant global_elements_object : language_object.language_value.global_elements
            property variant airport_map : global_elementsId1.global_elements_object.airport_map.value
            property variant altitude : global_elementsId1.global_elements_object.altitude.value
            property variant button_kids_text : global_elementsId1.global_elements_object.button_kids_text.value
            property variant button_mainmenu_text : global_elementsId1.global_elements_object.button_mainmenu_text.value
            property variant button_settings_text : global_elementsId1.global_elements_object.button_settings_text.value
            property variant cancel : global_elementsId1.global_elements_object.cancel.value
            property variant comingsoon : global_elementsId1.global_elements_object.comingsoon.value
            property variant cont : global_elementsId1.global_elements_object.cont.value
            property variant flight_connection : global_elementsId1.global_elements_object.flight_connection.value
            property variant flight_duration : global_elementsId1.global_elements_object.flight_duration.value
            property variant flight_elapse_time : global_elementsId1.global_elements_object.flight_elapse_time.value
            property variant flight_estimate_time : global_elementsId1.global_elements_object.flight_estimate_time.value
            property variant flight_local_time_dest : global_elementsId1.global_elements_object.flight_local_time_dest.value
            property variant flight_local_time_org : global_elementsId1.global_elements_object.flight_local_time_org.value
            property variant flight_remain_time : global_elementsId1.global_elements_object.flight_remain_time.value
            property variant ground_speed : global_elementsId1.global_elements_object.ground_speed.value
            property variant in_seat_monitor : global_elementsId1.global_elements_object.in_seat_monitor.value
            property variant no : global_elementsId1.global_elements_object.no.value
            property variant ok : global_elementsId1.global_elements_object.ok.value
            property variant outside_temp : global_elementsId1.global_elements_object.outside_temp.value
            property variant pa : global_elementsId1.global_elements_object.pa.value
            property variant power_down_txt : global_elementsId1.global_elements_object.power_down_txt.value
            property variant selectionUnavilable : global_elementsId1.global_elements_object.selectionUnavilable.value
            property variant view_map : global_elementsId1.global_elements_object.view_map.value
            property variant yes : global_elementsId1.global_elements_object.yes.value
        }
        property QtObject karma : QtObject {
            id: karmaId1
            property variant karma_object : language_object.language_value.karma
            property variant change_language : karmaId1.karma_object.change_language.value
            property variant handset_settings : karmaId1.karma_object.handset_settings.value
            property variant help_cat : karmaId1.karma_object.help_cat.value
            property variant help_desc : karmaId1.karma_object.help_desc.value
            property variant language_settings : karmaId1.karma_object.language_settings.value
            property variant launch_app : karmaId1.karma_object.launch_app.value
            property variant launch_handset : karmaId1.karma_object.launch_handset.value
            property variant launch_map : karmaId1.karma_object.launch_map.value
            property variant launch_seat : karmaId1.karma_object.launch_seat.value
            property variant monitor_settings : karmaId1.karma_object.monitor_settings.value
            property variant save : karmaId1.karma_object.save.value
            property variant settings_bright : karmaId1.karma_object.settings_bright.value
            property variant settings_screen_off : karmaId1.karma_object.settings_screen_off.value
            property variant settings_title : karmaId1.karma_object.settings_title.value
            property variant settings_vol : karmaId1.karma_object.settings_vol.value
            property variant stop_btn : karmaId1.karma_object.stop_btn.value
        }
        property QtObject main_menu : QtObject {
            id: main_menuId1
            property variant main_menu_object : language_object.language_value.main_menu
            property variant mainmenu_description : main_menuId1.main_menu_object.mainmenu_description.value
            property variant mainmenu_kids_description : main_menuId1.main_menu_object.mainmenu_kids_description.value
            property variant mainmenu_kids_title : main_menuId1.main_menu_object.mainmenu_kids_title.value
            property variant mainmenu_title : main_menuId1.main_menu_object.mainmenu_title.value
        }
        property QtObject movies : QtObject {
            id: moviesId1
            property variant movies_object : language_object.language_value.movies
            property variant accept : moviesId1.movies_object.accept.value
            property variant cancel : moviesId1.movies_object.cancel.value
            property variant directed_by : moviesId1.movies_object.directed_by.value
            property variant do_you_want_to_exit : moviesId1.movies_object.do_you_want_to_exit.value
            property variant fullscreen : moviesId1.movies_object.fullscreen.value
            property variant none : moviesId1.movies_object.none.value
            property variant play : moviesId1.movies_object.play.value
            property variant play_movie_in : moviesId1.movies_object.play_movie_in.value
            property variant produced_by : moviesId1.movies_object.produced_by.value
            property variant restart : moviesId1.movies_object.restart.value
            property variant resume : moviesId1.movies_object.resume.value
            property variant soundtrack : moviesId1.movies_object.soundtrack.value
            property variant starring : moviesId1.movies_object.starring.value
            property variant subtitle_cc : moviesId1.movies_object.subtitle_cc.value
            property variant trailer : moviesId1.movies_object.trailer.value
            property variant vod_change_screensize : moviesId1.movies_object.vod_change_screensize.value
            property variant vod_change_snd : moviesId1.movies_object.vod_change_snd.value
            property variant vod_change_subcc : moviesId1.movies_object.vod_change_subcc.value
        }
        property QtObject music : QtObject {
            id: musicId1
            property variant music_object : language_object.language_value.music
            property variant add_all_tracks_to : musicId1.music_object.add_all_tracks_to.value
            property variant add_tracks_to : musicId1.music_object.add_tracks_to.value
            property variant button_add_to_playlist : musicId1.music_object.button_add_to_playlist.value
            property variant clear_all_playlist : musicId1.music_object.clear_all_playlist.value
            property variant delete_track_title : musicId1.music_object.delete_track_title.value
            property variant go_to_albums : musicId1.music_object.go_to_albums.value
            property variant now_playing : musicId1.music_object.now_playing.value
            property variant playlist1 : musicId1.music_object.playlist1.value
            property variant playlist1_empty_title : musicId1.music_object.playlist1_empty_title.value
            property variant playlist1_is_full : musicId1.music_object.playlist1_is_full.value
            property variant playlist2 : musicId1.music_object.playlist2.value
            property variant playlist2_empty_title : musicId1.music_object.playlist2_empty_title.value
            property variant playlist2_is_full : musicId1.music_object.playlist2_is_full.value
            property variant playlist3 : musicId1.music_object.playlist3.value
            property variant playlist3_empty_title : musicId1.music_object.playlist3_empty_title.value
            property variant playlist3_is_full : musicId1.music_object.playlist3_is_full.value
            property variant playlist_empty_desc : musicId1.music_object.playlist_empty_desc.value
            property variant playlist_full_desc : musicId1.music_object.playlist_full_desc.value
            property variant remove_from_playlist : musicId1.music_object.remove_from_playlist.value
            property variant remove_songs : musicId1.music_object.remove_songs.value
            property variant text_album : musicId1.music_object.text_album.value
            property variant text_albums : musicId1.music_object.text_albums.value
        }
        property QtObject seatchat : QtObject {
            id: seatchatId1
            property variant seatchat_object : language_object.language_value.seatchat
            property variant block : seatchatId1.seatchat_object.block.value
            property variant block_completed : seatchatId1.seatchat_object.block_completed.value
            property variant block_confirmation : seatchatId1.seatchat_object.block_confirmation.value
            property variant block_list_desc : seatchatId1.seatchat_object.block_list_desc.value
            property variant block_list_title : seatchatId1.seatchat_object.block_list_title.value
            property variant cancel : seatchatId1.seatchat_object.cancel.value
            property variant chatsession_exit_text : seatchatId1.seatchat_object.chatsession_exit_text.value
            property variant chatting_with : seatchatId1.seatchat_object.chatting_with.value
            property variant continue_text : seatchatId1.seatchat_object.continue_text.value
            property variant decline : seatchatId1.seatchat_object.decline.value
            property variant disable : seatchatId1.seatchat_object.disable.value
            property variant empty_message : seatchatId1.seatchat_object.empty_message.value
            property variant enter_seat_subtitle : seatchatId1.seatchat_object.enter_seat_subtitle.value
            property variant enter_seat_title : seatchatId1.seatchat_object.enter_seat_title.value
            property variant invalid_seat_message : seatchatId1.seatchat_object.invalid_seat_message.value
            property variant invalid_session : seatchatId1.seatchat_object.invalid_session.value
            property variant invitation_message : seatchatId1.seatchat_object.invitation_message.value
            property variant invitation_sent : seatchatId1.seatchat_object.invitation_sent.value
            property variant leave_chat_confirmation : seatchatId1.seatchat_object.leave_chat_confirmation.value
            property variant leave_chat_room : seatchatId1.seatchat_object.leave_chat_room.value
            property variant me : seatchatId1.seatchat_object.me.value
            property variant multiple_invitation_sent : seatchatId1.seatchat_object.multiple_invitation_sent.value
            property variant multiple_seat_invitation : seatchatId1.seatchat_object.multiple_seat_invitation.value
            property variant new_chat : seatchatId1.seatchat_object.new_chat.value
            property variant nickname_message : seatchatId1.seatchat_object.nickname_message.value
            property variant nickname_title : seatchatId1.seatchat_object.nickname_title.value
            property variant notification_joined_txt : seatchatId1.seatchat_object.notification_joined_txt.value
            property variant notification_left_txt : seatchatId1.seatchat_object.notification_left_txt.value
            property variant privacy_desc : seatchatId1.seatchat_object.privacy_desc.value
            property variant privacy_title : seatchatId1.seatchat_object.privacy_title.value
            property variant said : seatchatId1.seatchat_object.said.value
            property variant seatchat_accept : seatchatId1.seatchat_object.seatchat_accept.value
            property variant seatchat_loading : seatchatId1.seatchat_object.seatchat_loading.value
            property variant seatchat_reject : seatchatId1.seatchat_object.seatchat_reject.value
            property variant send : seatchatId1.seatchat_object.send.value
            property variant send_invitation : seatchatId1.seatchat_object.send_invitation.value
            property variant session_instructions : seatchatId1.seatchat_object.session_instructions.value
            property variant session_limit_message : seatchatId1.seatchat_object.session_limit_message.value
            property variant session_welcome : seatchatId1.seatchat_object.session_welcome.value
            property variant sessions_title : seatchatId1.seatchat_object.sessions_title.value
            property variant terms_desc : seatchatId1.seatchat_object.terms_desc.value
            property variant terms_title : seatchatId1.seatchat_object.terms_title.value
            property variant unblock : seatchatId1.seatchat_object.unblock.value
            property variant unblock_list_desc : seatchatId1.seatchat_object.unblock_list_desc.value
            property variant unblock_list_title : seatchatId1.seatchat_object.unblock_list_title.value
            property variant unblock_seats : seatchatId1.seatchat_object.unblock_seats.value
        }
        property QtObject settings : QtObject {
            id: settingsId5
            property variant settings_object : language_object.language_value.settings
            property variant call_crew : settingsId5.settings_object.call_crew.value
            property variant change_language : settingsId5.settings_object.change_language.value
            property variant chat_disable_txt : settingsId5.settings_object.chat_disable_txt.value
            property variant chat_disablemsg_txt : settingsId5.settings_object.chat_disablemsg_txt.value
            property variant chat_enable_txt : settingsId5.settings_object.chat_enable_txt.value
            property variant chat_enablemsg_txt : settingsId5.settings_object.chat_enablemsg_txt.value
            property variant chattitle_txt : settingsId5.settings_object.chattitle_txt.value
            property variant reading_light : settingsId5.settings_object.reading_light.value
            property variant screen_off : settingsId5.settings_object.screen_off.value
            property variant windowdimmable_button : settingsId5.settings_object.windowdimmable_button.value
        }
        property QtObject survey : QtObject {
            id: surveyId1
            property variant survey_object : language_object.language_value.survey
            property variant done : surveyId1.survey_object.done.value
            property variant enter_your_comments : surveyId1.survey_object.enter_your_comments.value
            property variant next : surveyId1.survey_object.next.value
            property variant previous : surveyId1.survey_object.previous.value
            property variant remaining_text : surveyId1.survey_object.remaining_text.value
            property variant survey_triggered : surveyId1.survey_object.survey_triggered.value
            property variant take_another_survey : surveyId1.survey_object.take_another_survey.value
            property variant take_survey : surveyId1.survey_object.take_survey.value
            property variant thank_you : surveyId1.survey_object.thank_you.value
            property variant thank_you_desc : surveyId1.survey_object.thank_you_desc.value
        }
        property QtObject tv : QtObject {
            id: tvId1
            property variant tv_object : language_object.language_value.tv
            property variant min : tvId1.tv_object.min.value
            property variant next_episode : tvId1.tv_object.next_episode.value
            property variant play_video_in : tvId1.tv_object.play_video_in.value
            property variant previous_episode : tvId1.tv_object.previous_episode.value
            property variant tv_episode : tvId1.tv_object.tv_episode.value
            property variant tv_episodes : tvId1.tv_object.tv_episodes.value
            property variant tv_play : tvId1.tv_object.tv_play.value
            property variant tv_play_all : tvId1.tv_object.tv_play_all.value
            property variant what_do_you_wish : tvId1.tv_object.what_do_you_wish.value
        }
        property QtObject usb : QtObject {
            id: usbId1
            property variant usb_object : language_object.language_value.usb
            property variant accept : usbId1.usb_object.accept.value
            property variant artist : usbId1.usb_object.artist.value
            property variant connect : usbId1.usb_object.connect.value
            property variant connected : usbId1.usb_object.connected.value
            property variant date : usbId1.usb_object.date.value
            property variant disconnected : usbId1.usb_object.disconnected.value
            property variant eject : usbId1.usb_object.eject.value
            property variant filename : usbId1.usb_object.filename.value
            property variant no_files : usbId1.usb_object.no_files.value
            property variant notsupported : usbId1.usb_object.notsupported.value
            property variant reject : usbId1.usb_object.reject.value
            property variant size : usbId1.usb_object.size.value
            property variant terms_desc : usbId1.usb_object.terms_desc.value
            property variant terms_title : usbId1.usb_object.terms_title.value
            property variant time : usbId1.usb_object.time.value
        }
        property QtObject window_dimming : QtObject {
            id: window_dimmingId1
            property variant window_dimming_object : language_object.language_value.window_dimming
            property variant windowdimming_description : window_dimmingId1.window_dimming_object.windowdimming_description.value
            property variant windowdimming_title : window_dimmingId1.window_dimming_object.windowdimming_title.value
        }
    }
}
