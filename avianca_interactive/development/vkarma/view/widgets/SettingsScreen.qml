import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: settingsCont
    visible:false
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string subCatSelected:''
    property bool isSubMenuOpen: false
    property variant configMenu: (viewHelper.isKids(current.template_id))?"kids_menu_"+orientationType:"menu_"+orientationType
    property variant arrowMenu: (viewHelper.isKids(current.template_id))?"kids_menu_p":"menu_p"
    property string  globalSetting: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:"global_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSettingsMenu,"settingsMenu")
        push(compSettingsSubMenu,"settingsSubMenu")
    }
    function init(){
        console.log("Settings | init")
        settingsCont.visible=true
        viewHelper.isSettings=true
    }
    function reload(){}
    function clearOnExit(){
        console.log("Settings | clearonexit")
        viewHelper.isSettings=false
        visible=false
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    function showSettingScreen(val){
        if(val)init()
        else clearOnExit()
    }
    function loadPreviousScreen(){
        if(isSubMenuOpen){
            settingsMenu.setMainMenu()
        }
        else{
            showSettingScreen(false)
        }
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Image{
            id:entOnbg
            source:viewHelper.configToolImagePath+configTag[configMenu].main.bg
            MouseArea{
                anchors.fill:parent;
                onClicked: {}
            }

            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.configToolImagePath+configTag[globalSetting].settings.icon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText: [configTool.language.global_elements.button_settings_text,configTag["menu_"+orientationType].submenu.text_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }
    property QtObject settingsMenu
    Component{
        id:compSettingsMenu
        Item{
            anchors.top:parent.top
            anchors.fill:parent
            property alias lView: settingsMainMenu
            function performAction(index){
                if(index==0)viewController.showScreenPopup(19)
                else if(index==1 || index==2){
                    if(index==1)viewHelper.isMonitor=true
                    else viewHelper.isMonitor=false
                    subCatSelected=viewHelper.settingsMainMenuModel[index]
                    showSubCat(true)
                    settingsMainMenu.model=viewHelper.blankModel
                    settingsMainMenu.delegate=viewHelper.emptyDel
                    divLine.visible=false
                    isSubMenuOpen=true
                    settingsSubMenu.showSubMenu(true)
                }
                else if(index==3){
                    viewController.showScreenPopup(30);
                }
            }
            function setMainMenu(){
                subCatSelected=""
                showSubCat(false)
                settingsMainMenu.model=viewHelper.settingsMainMenuModel
                settingsMainMenu.delegate=compSettingsMainDel
                divLine.visible=true
                isSubMenuOpen=false
                settingsSubMenu.showSubMenu(false)
            }
            function showSubCat(val){
                subtitleText.visible=val
            }
            ViewText{
                id:subtitleText
                width:qsTranslate('','Settings_subcat_text_'+orientationType+'_w')
                height:qsTranslate('','Settings_subcat_text_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','Settings_subcat_text_'+orientationType+'_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                visible:false
                horizontalAlignment: Text.AlignLeft
                varText: [subCatSelected,configTag[globalSetting].settings.subcategory_title_color,qsTranslate('','Settings_subcat_text_'+orientationType+'_fontsize')]
                lineHeight:qsTranslate('','Settings_subcat_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }
            Rectangle{
                id:mainCont
                color:"transparent"
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','Settings_container_'+orientationType+'_tm')
                anchors.horizontalCenter: background.horizontalCenter
                height:qsTranslate('','Settings_container_'+orientationType+'_h')
                width:qsTranslate('','Settings_container_'+orientationType+'_w')
                Image{
                    id:divLine
                    anchors.bottom: settingsMainMenu.top
                    source:viewHelper.configToolImagePath+configTag[globalSetting].settings.divline
                }
                Image{
                    id:upArrow
                    visible:(settingsMainMenu.contentY>0)
                    source:viewHelper.configToolImagePath+configTag[arrowMenu].main.up_arrow
                    anchors.bottom: parent.top;
                    anchors.bottomMargin:5;
                    anchors.horizontalCenter: parent.horizontalCenter
                }


                Image{
                    id:downArrow
                    visible:((settingsMainMenu.contentY+settingsMainMenu.height)<settingsMainMenu.contentHeight)
                    source:viewHelper.configToolImagePath+configTag[arrowMenu].main.down_arrow
                    anchors.top:parent.bottom;
                    anchors.topMargin:5;
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                ListView{
                    id:settingsMainMenu
                    width:parent.width;height:parent.height
                    model:viewHelper.settingsMainMenuModel
                    delegate:compSettingsMainDel
                    snapMode: ListView.SnapOneItem
                    onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                    boundsBehavior: Flickable.StopAtBounds
                    clip: true;

                }
            }
        }
    }
    property QtObject settingsSubMenu
    Component{
        id:compSettingsSubMenu
        Item{
            id:mainCont
            visible: false
            property alias lView: settingsSubMenulistView
            width:qsTranslate('','Settings_monitor_cont_'+orientationType+'_w');height:qsTranslate('','Settings_monitor_cont_'+orientationType+'_h')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Settings_monitor_cont_'+orientationType+'_tm')
            anchors.horizontalCenter: parent.horizontalCenter
            function showSubMenu(val){visible=val}
            function performAction(index){
                if(index==0)viewController.showScreenPopup(3)
                else if(index==1)viewController.showScreenPopup(4)
                else if(index==2)viewController.showScreenPopup(5)
                else if(index==3)viewController.showScreenPopup(8)
                else if(index==4)viewController.showScreenPopup(9)
            }
            Image{
                id:divLine
                anchors.bottom: settingsSubMenu.top
                source:viewHelper.configToolImagePath+configTag[globalSetting].settings.divline
            }
            ListView{
                id:settingsSubMenulistView
                width:parent.width;height:parent.height
                model:viewHelper.monitorSettingsModel
                delegate:compSettingsSubDel
                snapMode: ListView.SnapOneItem
                boundsBehavior: Flickable.StopAtBounds
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                clip:true
            }
        }
    }
    Component{
        id:compSettingsSubDel
        Item{
            width:qsTranslate('','Settings_monitor_cells_'+orientationType+'_w')
            height:qsTranslate('','Settings_monitor_cells_'+orientationType+'_h')
            property bool isPressed: false
            ViewText{
                id:monitoSettingsText
                height:qsTranslate('','Settings_text_'+orientationType+'_h');width:qsTranslate('','Settings_text_'+orientationType+'_w')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignLeft
                varText: [viewHelper.monitorSettingsModel[index],configTag[globalSetting].settings.text_color,qsTranslate('','Settings_text_'+orientationType+'_fontsize')]
                elide: Text.ElideRight;
            }
            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:viewHelper.configToolImagePath+configTag[globalSetting].settings.divline
            }
            BorderImage{
                id:monitorsettingsHighlight
                width:qsTranslate('','Settings_press_'+orientationType+'_w')
                anchors.bottom: parent.bottom;anchors.bottomMargin: qsTranslate('','Settings_press_'+orientationType+'_bm')
                anchors.horizontalCenter: parent.horizontalCenter
                source:(isPressed)?viewHelper.configToolImagePath+configTag[globalSetting].settings.listing_highlight:""
            }
            MouseArea{
                anchors.fill: parent
                enabled: !settingsSubMenu.lView.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }
                onReleased: {
                    isPressed=false
                    settingsSubMenu.performAction(index)
                }
            }
        }
    }
    Component{
        id:compSettingsMainDel
        Item{
            width:qsTranslate('','Settings_cells_'+orientationType+'_w');height:qsTranslate('','Settings_cells_'+orientationType+'_h')
            property bool isPressed: false
            ViewText{
                id:settingsText
                height:qsTranslate('','Settings_text_'+orientationType+'_h');width:qsTranslate('','Settings_text_'+orientationType+'_w')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignLeft
                varText: [viewHelper.settingsMainMenuModel[index],configTag[globalSetting].settings.text_color,qsTranslate('','Settings_text_'+orientationType+'_fontsize')]
                elide: Text.ElideRight;
            }
            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:viewHelper.configToolImagePath+configTag[globalSetting].settings.divline
            }
            BorderImage{
                id:settingsHighlight
                width:qsTranslate('','Settings_press_'+orientationType+'_w')
                anchors.bottom: parent.bottom;anchors.bottomMargin: qsTranslate('','Settings_press_'+orientationType+'_bm')
                anchors.horizontalCenter: parent.horizontalCenter
                source:(isPressed)?viewHelper.configToolImagePath+configTag[globalSetting].settings.listing_highlight:""
            }
            MouseArea{
                anchors.fill: parent
                enabled: !settingsMenu.lView.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }
                onReleased: {
                    isPressed=false
                    settingsMenu.performAction(index)
                }
            }
        }
    }
}

