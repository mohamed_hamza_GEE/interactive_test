// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../components"
import "../../framework/viewcontroller"
SmartLoader{
    id:background;
    visible:false;
    property variant configTag: configTool.config;
    property string orientationType: (visible)?pif.getOrientationType():'p'
    onVisibleChanged:{
        closeBtn.opacity=0;
        if(visible){
            pif.paxus.startContentLog("Moving Map")
            pif.screenSaver.stopScreenSaver()
            hideClose.restart();
        }else{
            viewHelper.setScreenSaverData(10,60,30);
            pif.paxus.stopContentLog();
            hideClose.stop();
        }
    }

    Timer{
        id:hideClose;
        interval:5000;
        onTriggered:{
            closeBtn.opacity=1;
        }
    }

    Rectangle{
        anchors.fill:parent;
        color:"black"
    }

    Image{
        anchors.centerIn:parent;
        rotation:(pif.getOrientationType()=="p")?-90:0;
        source:(background.visible)?viewHelper.ixplorImg:""

    }

    MouseArea{
        anchors.fill:parent;
        onClicked:{
            if(closeBtn.opacity==1){
                closeBtn.opacity=0;
                hideClose.stop();
            }else{
                closeBtn.opacity=1;
                hideClose.restart();
            }
        }
    }

    Rectangle{
        anchors.fill:closeBtn;
        color:"black";
        opacity:0.5;
        visible:(closeBtn.opacity==1);
    }

    SimpleButton{
        id:closeBtn
        anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
        anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
        normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].help.close_btn_press;
        pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].help.close_btn_press;
        onActionReleased: {
            widgetList.setMapStatus(false);
        }
    }
}
