import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

FocusScope {
    id: vkb
    width: viewController.width
    height: viewController.height
    property int characterLimit:viewHelper.vkbCharLimit
    property string  orientationType;
    //    property int lineLimit:(identifier=="chat_nickname" ||identifier=="chat_nickname")?1:3
    property int curItemX:0;
    property int curItemWidth:0;
    property variant suggestion:extKeyModel.model;
    property bool sendSignal : false
    property int  valueForGap:0
    property variant configVkbTag: configTool.config.vkb_l.screen
    signal closeAllSuggetions()
    onVisibleChanged: {
        if(visible){ init() }
        else{clearOnExit()}
    }
onOrientationTypeChanged:(pif.getOrientationType()=="p")?pif.lockOrientation([pif.cPORTRAIT]):pif.lockOrientation([pif.cLANDSCAPE])

    /**************************************************************************************************************************/
    Connections{
        target:pif.vkarmaToSeat;
        onSigDataReceivedFromSeat:{
            switch(api){
            case "keyboardData":{
                sendSignal = false;
                if(virtualKeyBoard.sendInputTextRef().text== params.keyboardData)return;
                else{
                    virtualKeyBoard.sendInputTextRef().text = params.keyboardData;
                    virtualKeyBoard.sendInputTextRef().cursorPosition=virtualKeyBoard.sendInputTextRef().text.length;
                }
            }
            break;
            }
        }
    }
    /**************************************************************************************************************************/
    function init(){
        console.log("Vkb.qml | init")
        //pif.lockOrientation([pif.cLANDSCAPE])
        if(virtualKeyBoard.callKeyBoard().vkbGenerated == false)
            virtualKeyBoard.createKeyboard();
        virtualKeyBoard.forceActiveFocus();
        //        virtualKeyBoard.sendInputTextRef().text=""
        virtualKeyBoard.sendInputTextRef().cursorPosition = virtualKeyBoard.sendInputTextRef().text.length;
        virtualKeyBoard.sendInputTextRef().focus = true;
        virtualKeyBoard.checkConfirmBtnState();
    }

    function reload(){}
    function clearOnExit(){
        console.log("Vkb.qml | clearOnExit")
        if(viewController.getActiveScreenPopupID()==29||viewController.getActiveScreenPopupID()==31){
          //  pif.lockOrientation([pif.cLANDSCAPE])
        }else{
            pif.unlockOrientation()
        }
        virtualKeyBoard.callKeyBoard(). toggle=false
        virtualKeyBoard.callKeyBoard().isShiftON=false
        virtualKeyBoard.callKeyBoard().isCapsON=false
    }
    /*******************************************alphabet models **********************************************************************/
    VKBExtendedKeyModel{id:extKeyModel;}
    VKBEnglishModel{id:engRegular;}
    VKBEnglishShiftModel{id:engShift}
    /********************************************************************************************************/
    function callPostKeyFn(keyCode,character,modifier){virtualKeyBoard.callPostKeyFunction(keyCode,character,modifier)}
    function toggleKeys(status){virtualKeyBoard.callKeyBoard().toggleKeys(status)}
    /**************************************************************************************************************************/
    FocusScope{
        id:virtualKeyBoard;
        rotation:(pif.getOrientationType()=="p")?-90:0
        anchors.fill:vkb
        /*****************************************************************************************************************/
        function createKeyboard(){myKeyboard.createKeyboard();}
        function sendInputTextRef(){return inputText}
        function getSuggestionList(keyCode){
            var modVal;
            modVal = (myKeyboard.isCapsON)?1:0;
            if(suggestion[keyCode] != undefined){
                if(suggestion[keyCode][modVal] != undefined)
                    return (suggestion[keyCode][modVal]);
                else
                    return (suggestion[keyCode]);
            }
            else
                return false;
        }
        function moveUpDown(dir){ //reference AMX // dir : 0 -> up, 1 -> down
            if(dir == 0)
                inputText.cursorPosition = inputText.positionAt(inputText.cursorRectangle.x,inputText.cursorRectangle.y)
            else
                inputText.cursorPosition = inputText.positionAt(inputText.cursorRectangle.x,inputText.cursorRectangle.y+2*inputText.cursorRectangle.height)
        }
        function showSuggPopup(model){
            calculateX();
            suggestion_popup.y = (parseInt(brdrBG.y + container.y + parseInt(myKeyboard.y) - ( suggestion_popup.height +parseInt( qsTranslate('','VKB_extend_popup_tm'))) +parseInt(myKeyboard.getListViewReference().y)));
            suggList.model = model;
            suggestion_popup.visible = true;
        }
        function calculateX(){
            var actualX = (parseInt(brdrBG.x + (curItemX),10));
            var diff = ((actualX - Math.floor(suggestion_popup.width/2)) + Math.ceil(Math.abs(curItemWidth/2)))+valueForGap;
            if(diff < 0){suggestion_popup.x = 0;}
            else if((diff + suggestion_popup.width) > 432){
                diff = (diff - (432 - (diff + suggestion_popup.width)))
                suggestion_popup.x = diff;
            }
            else{
                suggestion_popup.x = diff;
            }
        }
        function isValidSearchString(str){
            if(str == undefined) str = "";
            if(str.trim().length == 0){
                core.log(" isValidSearchString() | Not valid string for Search ")
                return false;
            }
            return true;
        }
        function checkConfirmBtnState(){
            confirm_btn.isDisable=false
            confirm_btn.opacity=1;
            return;

        }
        function callPostKeyFunction(keyCode,character,modifier){inputText.postQKeyEvent(keyCode,character,modifier)}
        function callKeyBoard(){return myKeyboard}
        /***************************************************************************************************************/
        Rectangle{
            height: vkb.height;
            width: vkb.width;
            color:"transparent";
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                }
            }
        }
        Image {
            id:brdrBG;
            anchors.centerIn: parent
            source: viewHelper.configToolImagePath+configVkbTag.bg_panel
            Rectangle{
                id:container;
                width: parent.width
                height: parent.height
                anchors.centerIn:parent;
                color:"transparent";
                TextEdit{
                    id:inputText
                    width: qsTranslate('','VKB_nametext_area_w');
                    height: qsTranslate('','VKB_nametext_area_h');
                    anchors.left:parent.left;anchors.leftMargin: qsTranslate('','VKB_nametext_area_lm')
                    activeFocusOnPress:false
                    cursorVisible: true;
                    anchors.top:parent.top;anchors.topMargin:qsTranslate('','VKB_nametext_area_tm')
                    font.family: (viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot";
                    font.pixelSize:qsTranslate('','VKB_nametext_area_fontsize')
                    color:configVkbTag.text_box_text
                    clip: true;
                    text: ""
                    onTextChanged: {
                        if(inputText.lineCount > viewHelper.vkbLinesLimit ){
                            inputText.text = inputText.text.substring(0,inputText.cursorPosition-1) + inputText.text.substring(inputText.cursorPosition,inputText.text.length)
                            cursorPosition=inputText.text.length
                        }
                        else if(sendSignal)
                            pif.vkarmaToSeat.sendKeyboardDataToSeat(inputText.text);
                        sendSignal =false
                    }
                    function postQKeyEvent(keyCode,character,modifierVal){
                        var pos=parseInt(inputText.cursorPosition,10);
                        if(keyCode=="0x01000003"){
                            var a=inputText.text.substring(0,(inputText.cursorPosition-1));
                            var b=inputText.text.substring(inputText.cursorPosition,inputText.length);
                            inputText.text = a+b;
                            inputText.cursorPosition=pos-1;
                            return;
                        }else if(keyCode=="0x01000004" && inputText.cursorRectangle.y<=(inputText.cursorRectangle.height)){
                            inputText.text+="\n";
                            inputText.cursorPosition=inputText.text.length
                            return;
                        }
                        if(inputText.text.length >= characterLimit)return;
                        if(pos!=inputText.length){
                            var temp=inputText.text
                            var d= temp.substring(0,pos);
                            inputText.text =d+character+temp.substring(pos,temp.length);
                        }else
                            inputText.text+=character;
                        inputText.cursorPosition=pos+1;
                    }
                    wrapMode: TextEdit.Wrap;
                    Keys.onPressed: {
                        sendSignal = true;
                        switch(event.key){
                        case Qt.Key_Left:
                        case Qt.Key_Up:
                        case Qt.Key_Delete:
                        case Qt.Key_Backspace:
                        case Qt.Key_Shift:
                        case Qt.Key_CapsLock:
                            break;
                        case Qt.Key_Enter:
                        case Qt.Key_Return:{
                            if(inputText.lineCount == viewHelper.vkbLinesLimit || inputText.text.length >= characterLimit)
                                event.accepted = true
                        }
                        break;
                        default:
                            if(characterLimit >= 0){
                                if(inputText.text.length == characterLimit){
                                    event.accepted = true
                                }
                            }
                            break;
                        }
                    }

                }
                Rectangle{
                    color:"transparent";
                    width: qsTranslate('','VKB_remain_text_w');
                    height: qsTranslate('','VKB_remain_text_h');
                    anchors{
                        right: parent.right;
                        rightMargin: qsTranslate('','VKB_remain_text_x');
                    }
                    y:qsTranslate('','VKB_remain_text_y');
                    Text {
                        id:rem_Txt
                        text: configTool.language.survey.remaining_text//"Remaining Text "
                        font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot";
                        font.pixelSize:qsTranslate('','VKB_remain_text_fontsize')
                        color:configVkbTag.remaining_text//"#c0c0c0"
                        width: paintedWidth;
                        height: paintedHeight;
                        verticalAlignment:Text.AlignBottom;
                        anchors{
                            right: rem_count.left;
                            leftMargin: qsTranslate('','VKB_remain_text_textGap');
                            verticalCenter: parent.verticalCenter;
                        }
                    }
                    Text {
                        id:rem_count;
                        text:(characterLimit - inputText.text.length);  //
                        font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot";
                        font.pixelSize:qsTranslate('','VKB_remain_text_fontsize')
                        color:configVkbTag.remaining_text//"#c0c0c0";
                        width: paintedWidth;
                        height: paintedHeight;
                        verticalAlignment:Text.AlignBottom;
                        anchors{
                            right: parent.right;
                            verticalCenter: rem_Txt.verticalCenter;
                        }
                    }
                }
                Component{
                    id:btnDel;
                    Item {
                        id: thisItem
                        property variant curJsnRef:'VKB_'+jsnRef
                        height: qsTranslate('',curJsnRef+'_h')
                        width: qsTranslate('',curJsnRef+'_w')
                        property string btnLabel: (character != undefined)?character:"";
                        property string iconSource :(icon != undefined)?icon:"";
                        property bool changeCharCase:true
                        property string buttonType;

                        property variant parentView:ListView.view
                        property bool isFocused:(myKeyboard.isFocused == true && myKeyboard.isSuggPopupOpen == false && myKeyboard.curIndex == index && myKeyboard.selectedRowNumber == parentView.listRowNum);
                        property bool isCurXItem:(myKeyboard.selectedColNumber == index && myKeyboard.selectedRowNumber == parentView.listRowNum)
                        property variant viewCurrentItem: ListView.view;
                        onIsCurXItemChanged: {
                            if(isCurXItem == true){
                                curItemX = thisItem.x + ListView.view.x;
                                curItemWidth = thisItem.width;
                            }
                        }
                        Image{
                            id:tmpImg;
                            height: sourceSize.height;
                            width: sourceSize.width;
                            anchors{
                                horizontalCenter: parent.horizontalCenter;
                                verticalCenter: parent.verticalCenter;
                                verticalCenterOffset:(curJsnRef)?(qsTranslate('',curJsnRef+'_tm') != undefined)?qsTranslate('',curJsnRef+'_tm'):0:0;
                                horizontalCenterOffset:(curJsnRef)?(qsTranslate('',curJsnRef+'_lm') != undefined)?qsTranslate('',curJsnRef+'_lm'):0:0;
                            }
                            source: (curJsnRef)?(msArea.pressed)?(viewHelper.configToolImagePath+configVkbTag["key_"+jsnRef+"_press"]):(viewHelper.configToolImagePath+configVkbTag["key_"+jsnRef]):""
                            Text {
                                id:label_txt
                                text:myKeyboard.getCapsCharacter(btnLabel);
                                anchors.centerIn: parent;
                                visible: (text != "")?true:false;
                                font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot";
                                font.pixelSize:14;
                                color:configVkbTag.extend_chara_text
                                textFormat: Text.RichText;
                            }
                            Image {
                                id: iconImage
                                source:  (iconSource != "")?( viewHelper.configToolImagePath+configVkbTag[iconSource]):"";
                                anchors.centerIn: parent;
                                visible: (source != "")?true:false;
                            }
                            Image{
                                id:keyPopupImg;
                                visible: false;
                                property bool popupToDisplay: ((popup_txt.text.length == 1 || popup_txt.text.substring(0,1) == "&") &&(popup_txt.text!=" ")) ?true:false;
                                source:viewHelper.configToolImagePath+configVkbTag.enlarge_btn
                                Text {
                                    id:popup_txt
                                    anchors.horizontalCenter: parent.horizontalCenter;
                                    horizontalAlignment: Text.AlignHCenter;
                                    text: label_txt.text;
                                    wrapMode: Text.Wrap;
                                    width:qsTranslate('','VKB_enlarge_btn_text_w');
                                    height: qsTranslate('','VKB_enlarge_btn_text_h');
                                    y:qsTranslate('','VKB_enlarge_btn_text_tm');
                                    font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot";
                                    font.pixelSize:qsTranslate('','VKB_enlarge_btn_text_fontsize')
                                    color:configVkbTag.extend_chara_text
                                    textFormat: Text.RichText;
                                }
                                anchors.horizontalCenter: parent.horizontalCenter;
                                anchors.bottom: parent.bottom;
                                anchors.bottomMargin:qsTranslate('','VKB_enlarge_btn_bm');
                            }

                            Connections{
                                target: vkb;
                                onCloseAllSuggetions:{
                                    if(keyPopupImg.visible == true){
                                        keyPopupImg.visible = false
                                    }
                                }
                            }

                            MouseArea{
                                id:msArea
                                anchors.fill: parent;
                                onPressed: {
                                    curItemX = thisItem.x;
                                    curItemWidth = thisItem.width;
                                    valueForGap=parentView.style[3]
                                    sendSignal = true;
                                    closeAllSuggetions();
                                    myKeyboard.setFocus();
                                    myKeyboard.setSelected(parentView.listRowNum,index)
                                    if(keyPopupImg.popupToDisplay){
                                        keyPopupImg.visible = true;
                                    }
                                }
                                onPressAndHold: {
                                    closeAllSuggetions();
                                    var isList = (virtualKeyBoard.getSuggestionList(key_code));
                                    if(isList != false)
                                        virtualKeyBoard.showSuggPopup(isList);
                                }
                                onReleased: {
                                    if(keyPopupImg.popupToDisplay && keyPopupImg.visible == true){
                                        keyPopupImg.visible = false;
                                    }
                                    if(!suggestion_popup.visible)
                                        myKeyboard.click();
                                }
                            }
                        }
                    }
                }
                SimpleButton{
                    id:confirm_btn
                    anchors.left:parent.left;anchors.leftMargin: qsTranslate('','VKB_confirm_arrow_lm')
                    anchors.top:parent.top;anchors.topMargin:qsTranslate('','VKB_confirm_arrow_tm')
                    normImg: viewHelper.configToolImagePath+configVkbTag.confirm_btn
                    pressedImg: viewHelper.configToolImagePath+configVkbTag.confirm_btn_press
                    isDisable: (isValidSearchString(inputText.text) && inputText.text.trim().length >= 1)?false:true
                    opacity: (isDisabled)?0.2:1;
                    onActionReleased: {
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedVKBData",{vkbData:inputText.text});
                        widgetList.showHideVKBScreen(false);
                        pif.vkarmaToSeat.hideVkb();
                    }
                }
                SimpleButton{
                    id:discard_btn
                    anchors.left:parent.left;anchors.leftMargin: qsTranslate('','VKB_confirm_arrow_lm')
                    anchors.top:parent.top;anchors.topMargin:qsTranslate('','VKB_confirm_arrow_tm1')
                    normImg: viewHelper.configToolImagePath+configVkbTag.discard_btn
                    pressedImg: viewHelper.configToolImagePath+configVkbTag.discard_btn_press
                    onActionReleased: {
                        widgetList.showHideVKBScreen(false);
                        pif.vkarmaToSeat.hideVkb();
                    }
                }
                KeyBoard{
                    id:myKeyboard
                    textControl:inputText
                    buttonDelegate:btnDel;
                    modelArray:{"eng":[engRegular,engShift]}
                    nav:[-1,-1,-1,inputText]
                    navigableKeyboard: false;
                    onPerFormAction:{
                        confirm_btn.actionReleased();
                    }
                    property bool isSuggPopupOpen: false
                    function setFocus(){
                        textControl.focus = true;
                        myKeyboard.isFocused = true;
                    }
                    function preSendKey(key_code){
                        var isList = (getSuggestionList(key_code));
                        if(isList != false){
                            virtualKeyBoard.showSuggPopup(isList);
                            return true;
                        }
                        return false;
                    }
                    jsnRef1: [
                        [
                            [qsTranslate('','VKB_alpha_row_lm'),qsTranslate('','VKB_alpha_row_tm'),qsTranslate('','VKB_alpha_row_w'),qsTranslate('','VKB_btn_gap_row1_hgap')],
                            [qsTranslate('','VKB_num_row_lm'),qsTranslate('','VKB_num_row_tm'),qsTranslate('','VKB_num_row_w'),qsTranslate('','VKB_btn_gap_row1_hgap')]
                        ],
                        [
                            [qsTranslate('','VKB_alpha_row_lm1'),qsTranslate('','VKB_alpha_row_tm1'),qsTranslate('','VKB_alpha_row_w1'),qsTranslate('','VKB_btn_gap_row2_hgap')],
                            [qsTranslate('','VKB_num_row_lm1'),qsTranslate('','VKB_num_row_tm1'),qsTranslate('','VKB_num_row_w1'),qsTranslate('','VKB_btn_gap_row2_hgap')]
                        ],
                        [
                            [qsTranslate('','VKB_alpha_row_lm2'),qsTranslate('','VKB_alpha_row_tm2'),qsTranslate('','VKB_alpha_row_w2'),qsTranslate('','VKB_btn_gap_row3_hgap')],
                            [qsTranslate('','VKB_num_row_lm2'),qsTranslate('','VKB_num_row_tm2'),qsTranslate('','VKB_num_row_w2'),qsTranslate('','VKB_btn_gap_row3_hgap')]
                        ],
                        [
                            [qsTranslate('','VKB_alpha_row_lm3'),qsTranslate('','VKB_alpha_row_tm3'),qsTranslate('','VKB_alpha_row_w3'),qsTranslate('','VKB_btn_gap_row4_hgap')],
                            [qsTranslate('','VKB_num_row_lm3'),qsTranslate('','VKB_num_row_tm3'),qsTranslate('','VKB_num_row_w3'),qsTranslate('','VKB_btn_gap_row4_hgap')]
                        ]
                    ];
                    height: qsTranslate('','VKB_key_btn_area_h');
                    width: qsTranslate('','VKB_key_btn_area_w');
                    anchors.left:parent.left;anchors.leftMargin: qsTranslate('','VKB_key_btn_area_lm');
                    anchors.top: parent.top;anchors.topMargin: qsTranslate('','VKB_key_btn_area_tm');
                    onSpecialKeyPressed: {
                        switch(keyCode){
                        case "upperCase":{
                            myKeyboard.capsPressed();
                            break;
                        }
                        case "hide_key":{
                            widgetList.showHideVKBScreen(false);
                            pif.vkarmaToSeat.hideVkb();
                            break;
                        }
                        case "TOGGLE":{
                            myKeyboard.toggleKeys();
                            break;
                        }
                        case "SHIFT":{
                            myKeyboard.shiftPressed();
                            break;
                        }
                        case "dArrow":{
                            virtualKeyBoard.moveUpDown(1)
                            break;
                        }
                        case "uArrow":{
                            virtualKeyBoard.moveUpDown(0)
                            break;
                        }
                        case "lArrow":{
                            if(inputText.cursorPosition>0)
                                inputText.cursorPosition--;
                            break;
                        }
                        case "rArrow":{
                            if(inputText.cursorPosition<inputText.text.length)
                                inputText.cursorPosition++;
                            break;
                        }
                        }
                    }
                }
            }
        }
        Rectangle{
            height: vkb.height;
            width: vkb.width;
            color: "transparent";
            visible: suggestion_popup.visible;
            MouseArea{
                anchors.fill: parent;
                onClicked: {
                    suggestion_popup.closeSuggPopup();
                }
            }
        }
        Image{
            id:e_arrow
            source:viewHelper.configToolImagePath+configVkbTag.extend_arrow
            visible: suggestion_popup.visible;
            anchors.horizontalCenter: suggestion_popup.horizontalCenter
            anchors.top: suggestion_popup.bottom;anchors.topMargin: qsTranslate('','VKB_arrow_tm');
        }
        BorderImage {
            id: suggestion_popup
            visible: false;
            source : viewHelper.configToolImagePath+configVkbTag.extended_popup
            width: ((Math.ceil(suggList.count/2)*(qsTranslate('','VKB_extend_chara_text_w1'))) + (2 * qsTranslate('','VKB_extend_chara_text_lm')))
            onWidthChanged: virtualKeyBoard.calculateX();
            border{
                left:qsTranslate('','VKB_extend_popup_preserveLeftGap');
                right: qsTranslate('','VKB_extend_popup_preserveRightGap');
            }
            function sendSuggKey(index){
                var modelData;
                if(index == undefined)
                    index = suggList.currentIndex;
                modelData = suggList.model[index];
                myKeyboard.sendKey(modelData["key_code"],modelData["character"],modelData["modifier"])
                closeSuggPopup()
            }
            onVisibleChanged: {
                if(visible == true){
                    suggestion_popup.forceActiveFocus();
                    suggList.focus = true;
                    suggList.currentIndex = 0;
                }
            }
            function closeSuggPopup(){
                myKeyboard.setFocus();
                suggestion_popup.visible = false;
            }

            GridView{
                id:suggList
                x:qsTranslate('','VKB_extend_chara_text_lm');
                y:qsTranslate('','VKB_extend_chara_text_tm');
                width:(qsTranslate('','VKB_extend_chara_text_w1') * Math.ceil(count/2));
                height:(qsTranslate('','VKB_extend_chara_text_h1') * 2);
                cellHeight: qsTranslate('','VKB_extend_chara_text_h1');
                cellWidth:qsTranslate('','VKB_extend_chara_text_w1');
                focus: suggestion_popup.visible;
                interactive:false;
                property bool pressedHere: false
                Repeater{
                    model:(Math.ceil(suggList.count/2)-1)
                    Rectangle{
                        width:2;height: suggList.height;
                        x:(suggList.cellWidth * (index+1));
                        color: configVkbTag.extended_popup_line_color
                        opacity:qsTranslate('','VKB_line_opacity')}
                }
                Rectangle{
                    width: suggList.width;height: 2;
                    anchors.centerIn: parent;
                    color: configVkbTag.extended_popup_line_color
                    opacity:qsTranslate('','VKB_line_opacity')}

                delegate: Rectangle{
                    id:thisDel
                    width: qsTranslate('','VKB_extend_chara_text_w1');
                    height: qsTranslate('','VKB_extend_chara_text_h1');
                    property bool isFocused:(GridView.isCurrentItem);
                    color: "transparent"
                    Text {
                        id:sugg_txt
                        text:modelData["character"];
                        width: paintedWidth;
                        height: paintedHeight;
                        anchors.centerIn: parent;
                        visible: (text != "")?true:false;
                        font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot";
                        font.pixelSize:qsTranslate('','VKB_extend_chara_text_fontsize')
                        color:configVkbTag.extend_chara_text
                        textFormat: Text.RichText;
                    }

                    MouseArea{
                        anchors.fill: parent;
                        onPressed: {suggList.currentIndex = index;}
                        onReleased: {suggestion_popup.sendSuggKey(index);}
                    }
                }
            }
        }
    }
    Component.onCompleted:{
        try{init();}catch(e){}
        try{clearOnExit();}catch(e){}
    }
}
