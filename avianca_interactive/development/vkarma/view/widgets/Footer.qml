import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: footer
    visible:false
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'l'
    property bool  nowPlayPressed:false
    property string playingCategory: ''
    property string  globalFooter: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:"global_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compFooterBtns,"footerBtns")
    }
    function init(){
        console.log("Footer.qml | init")
    }
    function reload(){}
    function clearOnExit(){
        console.log("Footer.qml | clearOnExit")
    }
    onLoaded: {}
    Connections{
        target:pif.vkarmaToSeat
        onSigDataReceivedFromSeat:{
            switch(api){
            case "userDefinedsetPlayingCategory":{
                console.log("userDefinedsetPlayingCategory | playing Category "+params.category)
                if(viewHelper.isFromKarma)return
                else{
                    viewHelper.setPlayingCategory(params.category)
                    footerBtns.getNowPlayingSource()
                }
            }
            break;
            }
        }
    }
    /**************************************************************************************************************************/
    function showFooter(val){footer.visible=val}
    function showFooterHomeBtn(val){footerBtns.showFooterHomeBtn(val)}
    function showNowPlayinBtn(val){footerBtns.showNowPlayinBtn(val)}
    function backPressed(){
        if(widgetList.getVideoRef().visible)
            widgetList.getVideoRef().loadPreviousScreen()
        else if(widgetList.getAudioRef().visible)
            widgetList.getAudioRef().loadPreviousScreen()
        else if(widgetList.getSettingsRef().visible)
            widgetList.getSettingsRef().loadPreviousScreen()
        else if(widgetList.getGamesRef().visible)
            widgetList.getGamesRef().loadPreviousScreen()
        else if(viewController.getActiveScreenRef().loadPreviousScreen)
            viewController.getActiveScreenRef().loadPreviousScreen()
    }
    function showController(){
        if(widgetList.getSettingsRef().visible)widgetList.showSettingScreen(false)
        if(pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate"){
            if(widgetList.getVideoRef().visible)widgetList.hideVodNowPlaying()
            else widgetList.showVodNowPlaying()
        }
        else if(pif.vkarmaToSeat.getMediaType()=="audio" ||  pif.vkarmaToSeat.getMediaType()=="audioAggregate"||pif.vkarmaToSeat.getMediaType()=="mp3"){
            if(widgetList.getAudioRef().visible)widgetList.hideAodControls()
            else widgetList.showAodControls()
        }else{

        }
    }
    //function setPlayingCategory(cat){playingCategory=cat}
    function getNowPlayingSource(){footerBtns.getNowPlayingSource()}
    /**************************************************************************************************************************/
    property QtObject footerBtns
    Component{
        id:compFooterBtns
        Rectangle{
            id:footerBg
            function showFooterHomeBtn(val){homeBtn.visible=val}
            function showNowPlayinBtn(val){nowPlayingBtn.visible=val}
            function getNowPlayingSource(){
                var index;
                var model=( viewHelper.isTrackPlayedFromKids)?viewData.kidsMenuModel:viewData.mainMenuModel
                if(pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" || pif.vkarmaToSeat.getCurrentMediaPlayerState()==""){
                    if(pif.vkarmaToSeat.getMediaType()=="mp3" ||pif.vkarmaToSeat.getMediaType()=="audioAggregate"){
                        viewHelper.setPlayingCategory((viewHelper.isTrackPlayedFromKids)?"kids_music_listing":"music")
                    }
                    console.log("playingCategory "+viewHelper.getPlayingCategory())
                    index=core.coreHelper.searchEntryInModel(model,"category_attr_template_id",viewHelper.getPlayingCategory())
                    console.log("viewData.mainMenuModel.count  2 "+model.count)
                    console.log("index "+index)
                    nowPlayingBtn.source=viewHelper.mediaImagePath+model.getValue(index,(orientationType=='p')?"poster_p":"poster_l")
                    nowPlayRect.opacity=1
                }
            }
            Timer{
                id:nowPlayingTimer
                interval: 100
                onTriggered: {
                    nowPlayRect.opacity=0
                    if(viewHelper.isFromKarma){getNowPlayingSource()}
                    else
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedgetPlayingCategory")
                }
            }
            width:parent.width;height:qsTranslate('','Common_foot_h')
            anchors.bottom: parent.bottom
            color:configTag[globalFooter].footer.bg_color
            SimpleCellBtn{
                id:backBtn
                normImg: viewHelper.configToolImagePath+configTag[globalFooter].footer.back_btn
                pressedImg: viewHelper.configToolImagePath+configTag[globalFooter].footer.back_btn_press
                anchors.left: parent.left;
                width:qsTranslate('','Common_foot_iconcell_'+orientationType+'_w')
                height:qsTranslate('','Common_foot_iconcell_'+orientationType+'_h')
                onBtnReleased: {backPressed()}
            }
            SimpleCellBtn{
                id:homeBtn
                normImg: viewHelper.configToolImagePath+configTag[globalFooter].footer.home_btn
                pressedImg: viewHelper.configToolImagePath+configTag[globalFooter].footer.home_btn_press
                anchors.left: backBtn.right;anchors.leftMargin: qsTranslate('','Common_foot_iconcell_'+orientationType+'_hgap')
                width:qsTranslate('','Common_foot_iconcell_'+orientationType+'_w')
                height:qsTranslate('','Common_foot_iconcell_'+orientationType+'_h')
                onBtnReleased: {
                    viewHelper.launchAppVkarma=""
                    viewHelper.firstKidsLoad=false
                    viewHelper.firstMainLoad=false
                    viewHelper.homeCalled()
                }
            }
            SimpleCellBtn{
                id:settingsBtn
                normImg:viewHelper.configToolImagePath+configTag[globalFooter].footer.settings_btn
                pressedImg: viewHelper.configToolImagePath+configTag[globalFooter].footer.settings_btn_press
                anchors.left: (homeBtn.visible)?homeBtn.right:backBtn.right;anchors.leftMargin:qsTranslate('','Common_foot_iconcell_'+orientationType+'_hgap')
                width:qsTranslate('','Common_foot_iconcell_'+orientationType+'_w')
                height:qsTranslate('','Common_foot_iconcell_'+orientationType+'_h')
                onBtnReleased: {
                    if(widgetList.getSettingsRef().visible)widgetList.showSettingScreen(false)
                    else{
                        if(widgetList.getVideoRef().visible)widgetList.hideVodNowPlaying()
                        widgetList.showSettingScreen(true)
                    }
                }
            }
            Rectangle{
                id:nowPlayRect
                width:qsTranslate('','Common_foot_iconcell_'+orientationType+'_w')
                height:qsTranslate('','Common_foot_iconcell_'+orientationType+'_h')
                anchors.right:parent.right;anchors.bottom:parent.bottom;
                color:"transparent"
                visible: {
                    if((pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate" ||pif.vkarmaToSeat.getMediaType()=="audio" ||  pif.vkarmaToSeat.getMediaType()=="audioAggregate"||pif.vkarmaToSeat.getMediaType()=="mp3")&&(pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"))
                        return true
                    else
                        return false;
                }
                Image{
                    id:nowPlayingHighlight
                    source:(nowPlayPressed)?viewHelper.configToolImagePath+configTag[globalFooter].footer.nowplay_icon_press:""
                    anchors.centerIn: parent;
                }
                Image{
                    id:nowPlayingBtn

                    width:qsTranslate('','Common_foot_nowplayicon_'+orientationType+'_w')
                    height:qsTranslate('','Common_foot_nowplayicon_'+orientationType+'_h')
                    anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Common_foot_nowplayicon_'+orientationType+'_lm')
                    anchors.top:parent.top;
                    smooth: true;
                }
                Image{
                    id:playingIcon
                    source:viewHelper.configToolImagePath+configTag[globalFooter].footer.nowplay_icon
                    anchors.centerIn: parent;
                }
                MouseArea{
                    anchors.fill: parent
                    onPressed: {nowPlayPressed=true}
                    onReleased: {
                        nowPlayPressed=false;
                        showController()
                    }
                }
                onVisibleChanged: {
                    nowPlayingTimer.restart()
                }
                onOpacityChanged:{
                    core.debug("nowPlayingBtn.opacity : "+nowPlayingBtn.opacity)
                }
            }
        }
    }
}

