import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: audioControls
    visible:false
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property bool isPlaying:false
    property variant configMenu: (viewHelper.isKids(current.template_id))?"kids_menu_"+orientationType:"menu_"+orientationType
    property bool isUsb:(pif.vkarmaToSeat.getMediaType()=="mp3")
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compNowPlayingDetails,"nowPlayingDetails")
        push(compAodPanelPortrait,"aodPanelPortrait")
        push(compAodPanelLandScape,"aodPanelLandScape")
    }
    function init(){
        console.log("Audio.qml | init")
        visible=true
        if(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play"){
            isPlaying=true
        }
        setInitialSliderVal()
    }
    function reload(){console.log("Audio.qml | reload")}
    function clearOnExit(){
        console.log("Audio.qml | clear on exit")
        visible=false
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    Connections{
        target:pif.vkarmaToSeat
        onSigDataReceivedFromSeat:{
            switch(api){
            case "frameworkUpdateAudioPlayerdata":{
                setInitialSliderVal()
            }
            break;
            case "playAodAck":{
                isPlaying=true
                setInitialSliderVal()
            }
            break;
            case "mediaPauseAck":{
                isPlaying=false
            }
            break;
            case "mediaShuffleAck":{
            }
            break;
            case "mediaResumeAck":{
                isPlaying=true
            }
            break;
            case "userDefinedRepeatOn":{
                if(params.repeatStatus!=viewHelper.userRepeatOn){
                    viewHelper.userRepeatOn=params.repeatStatus
                }
            }
            break;
            case "audioStopAck":{
                if(pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"&& (pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate")){
                    widgetList.getFooterRef().getNowPlayingSource()
                }
                hideAodControls()
            }
            break;
            case "setMediaPositionAck":{
                var durationInSec = viewHelper.audioPlayerVkarma.midDuration
                nowPlayingDetails.calcTime(params.elapsedTime,durationInSec)
                nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getElapsedTime(),6))
                nowPlayingDetails.setRemainingTime("-"+core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getRemainingTime(durationInSec),6))
            }
            break;
            case "mediaRepeatAck":{
            }
            break;
            default:
                break;
            }
        }
        onMediaRepeatChanged:{
        }
    }
    /**************************************************************************************************************************/
    function showAodControls(){init()}
    function hideAodControls(){clearOnExit()}
    function setInitialSliderVal(){
        nowPlayingDetails.setSliderMaxValue( viewHelper.audioPlayerVkarma.midDuration)
        nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getElapsedTime(),6))
        nowPlayingDetails.setRemainingTime("-"+core.coreHelper.getTimeFormatFromSecs(viewHelper.audioPlayerVkarma.midDuration,6))
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        hideAodControls()
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag[configMenu].main.bg
            }
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.background
                MouseArea{
                    anchors.fill:parent
                    onClicked:{}
                }
            }
        }
    }
    property QtObject nowPlayingDetails
    Component{
        id:compNowPlayingDetails
        Item{
            id:nowPlayCont
            width:qsTranslate('','Listing_container_'+orientationType+'_w');height:qsTranslate('','Listing_container_'+orientationType+'_h')
            anchors.top:background.top;
            function getSliderRef(){return progressBar}
            function calcTime(elapsedTime,durationSec){progressBar.externalCalc(elapsedTime,durationSec)}
            function setSliderMaxValue(endTime){progressBar.maxLevel=endTime}
            function setElapseTime(elapsedTime){elapsedText.text=elapsedTime}
            function setRemainingTime(remainingTime){remainingText.text=remainingTime}
            Image{
                id:poster
                anchors.left: parent.left;anchors.leftMargin: qsTranslate('','NowPlay_aod_poster_'+orientationType+'_lm')
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_aod_poster_'+orientationType+'_tm')
                source:(orientationType=='p')?viewHelper.mediaImagePath+viewHelper.currentAlbumModel.getValue(0,"poster_p"):viewHelper.mediaImagePath+viewHelper.currentAlbumModel.getValue(0,"poster_l")
                width:qsTranslate('','Listing_poster_mus_'+orientationType+'_w')
                height:qsTranslate('','Listing_poster_mus_'+orientationType+'_h')
                visible:(pif.vkarmaToSeat.getMediaType()!="mp3")?true:false
                Rectangle{anchors.fill:parent;color:"transparent"}
            }
            ViewText{
                id:albumTitle
                anchors.left:(!isUsb)?poster.right:parent.left;anchors.leftMargin:(!isUsb)?qsTranslate('','NowPlay_aod_albmtitle_'+orientationType+'_lm'):qsTranslate('','NowPlay_usb_albmtitle_'+orientationType+'_lm')
                anchors.top:(!isUsb)?poster.top:parent.top;anchors.topMargin:(!isUsb)? qsTranslate('','NowPlay_aod_albmtitle_'+orientationType+'_tm'):qsTranslate('','NowPlay_usb_albmtitle_'+orientationType+'_tm')
                height:(!isUsb)?qsTranslate('','NowPlay_aod_albmtitle_'+orientationType+'_h'):qsTranslate('','NowPlay_usb_albmtitle_'+orientationType+'_h')
                width:(!isUsb)?qsTranslate('','NowPlay_aod_albmtitle_'+orientationType+'_w'):qsTranslate('','NowPlay_usb_albmtitle_'+orientationType+'_w')
                lineHeight:(!isUsb)?qsTranslate('','NowPlay_aod_albmtitle_'+orientationType+'_lh'):qsTranslate('','NowPlay_usb_albmtitle_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                wrapMode:Text.WordWrap
                elide: Text.ElideRight
                varText: [viewHelper.audioPlayerVkarma.aMidTitle+" - "+viewHelper.audioPlayerVkarma.aMidArtist,configTag["music_"+orientationType].now_playing.title_color,qsTranslate('','NowPlay_aod_albmtitle_'+orientationType+'_fontsize')]
            }
            ViewText{
                id:trackTitle
                anchors.left:albumTitle.left;anchors.leftMargin: (poster.visible)?qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_lm'):0
                anchors.top:albumTitle.bottom;anchors.topMargin: qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_tm')
                height:qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_h')
                lineHeight:qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                elide:Text.ElideRight
                wrapMode:Text.Wrap
                varText: [viewHelper.audioPlayerVkarma.midTitle,configTag["music_"+orientationType].now_playing.track_name_color,qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_fontsize')]
                onPaintedWidthChanged: {
                    if(paintedWidth<parseInt(qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_w'),10))return
                    else width=parseInt(qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_w'),10)-parseInt(qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_w_time'),10)
                }
                ViewText{
                    id:duration
                    anchors.left: trackTitle.right
                    anchors.top:trackTitle.top
                    height:qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_h')
                    width:qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_w_time')
                    lineHeight:qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    varText: [" - "+core.coreHelper.getTimeFormatFromSecs(viewHelper.audioPlayerVkarma.midDuration,6),configTag["music_"+orientationType].now_playing.track_name_color,qsTranslate('','NowPlay_aod_songtitle_'+orientationType+'_fontsize')]
                }
            }
            Rectangle{
                id:progressbarCont
                width:(!isUsb)?qsTranslate('','NowPlay_progcont_'+orientationType+'_w'):qsTranslate('','NowPlay_progcont_'+orientationType+'_w_usb')
                height:qsTranslate('','NowPlay_progcont_'+orientationType+'_h')
                anchors.left:parent.left;anchors.leftMargin:qsTranslate('','NowPlay_progcont_'+orientationType+'_lm_aod')//parseInt( qsTranslate('','NowPlay_progcont_'+orientationType+'_lm_vod'),10)+parseInt(qsTranslate('','NowPlay_vod_poster_'+orientationType+'_lm'),10)+parseInt(qsTranslate('','Listing_poster_'+orientationType+'_w'),10)
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','NowPlay_progcont_'+orientationType+'_tm_aod')// parseInt(qsTranslate('','NowPlay_progcont_'+orientationType+'_tm_vod'),10)+parseInt( qsTranslate('','NowPlay_vod_poster_'+orientationType+'_tm'),10)
                color:"transparent"
                ViewText{
                    id:elapsedText
                    height:qsTranslate('','NowPlay_prog_time_'+orientationType+'_h')
                    width:qsTranslate('','NowPlay_prog_time_'+orientationType+'_w')
                    font.pixelSize: qsTranslate('','NowPlay_prog_time_'+orientationType+'_fontsize')
                    color:configTag["music_"+orientationType].now_playing.media_time_color
                    anchors.left: progressbarCont.left
                    anchors.bottom: progressbarCont.bottom
                    horizontalAlignment: Text.AlignLeft
                }
                ViewText{
                    id:remainingText
                    height:qsTranslate('','NowPlay_prog_time_'+orientationType+'_h')
                    width:qsTranslate('','NowPlay_prog_time_'+orientationType+'_w')
                    font.pixelSize: qsTranslate('','NowPlay_prog_time_'+orientationType+'_fontsize')
                    color:configTag["music_"+orientationType].now_playing.media_time_color
                    anchors.right: progressbarCont.right
                    anchors.bottom: progressbarCont.bottom
                    horizontalAlignment: Text.AlignRight
                }
                Slider{
                    id: progressBar
                    anchors.horizontalCenter: progressbarCont.horizontalCenter
                    anchors.top: progressbarCont.top
                    height:qsTranslate('','NowPlay_prog_'+orientationType+'_h')
                    width:qsTranslate('','NowPlay_prog_'+orientationType+'_w_aod')
                    sliderbg:(!isUsb)?configTag["music_"+orientationType].now_playing.base_image:configTag["usb_"+orientationType].now_playing.base_image
                    sliderFillingtItems:[(!isUsb)?configTag["music_"+orientationType].now_playing.fill_image:configTag["usb_"+orientationType].now_playing.fill_image,(!isUsb)?configTag["music_"+orientationType].now_playing.indicator:configTag["usb_"+orientationType].now_playing.indicator]
                    stepsValue:1
                    isFill:true
                    onValueChanged:{
                        var count=Math.round(sliderDotImg.anchors.leftMargin/progressBar.shiftingPerClick)
                        pif.vkarmaToSeat.setPlayingMediaPosition({elapsedTime:progressBar.stepsValue * count})
                    }
                }
            }
        }
    }
    property QtObject aodPanelPortrait
    Component{
        id:compAodPanelPortrait
        Item{
            id:mainCont
            width:background.width
            height: background.height
            visible:(orientationType=='p')
            Image{
                id:controllerBg1
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm')
                source: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.aod_controller_bg:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.aod_controller_bg
            }
            Image{
                id:controllerBg2
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm1')
                source: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.aod_controller_bg:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.aod_controller_bg
            }
            SimpleCellBtn{
                id:previousTrack
                anchors.top:controllerBg1.top;anchors.left:controllerBg1.left
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.prev_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.prev_button
                pressedImg:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.prev_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.prev_button_press
                onBtnReleased:{
                    if(viewHelper.isFromGenre){
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"rwd"})
                    }else{
                        pif.vkarmaToSeat.playPreviousMedia();
                    }
                }
            }
            SimpleCellBtn{
                id:playPauseBtn
                anchors.top:controllerBg1.top
                anchors.left: previousTrack.right
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg:(isPlaying)?(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.pause_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.pause_button: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.play_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.play_button
                pressedImg:(isPlaying)?(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.pause_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.pause_button: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.play_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.play_button_press
                onBtnReleased:{
                    if(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play"){
                        pif.vkarmaToSeat.pausePlayingMedia();
                    }
                    else{
                        pif.vkarmaToSeat.resumePlayingMedia();
                    }
                }
            }
            SimpleCellBtn{
                id:nextTrack
                anchors.top:controllerBg1.top
                anchors.left: playPauseBtn.right
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.next_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.next_button
                pressedImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.next_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.next_button
                onBtnReleased: {
                    if(viewHelper.isFromGenre){
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"fwd"})
                    }else{
                        pif.vkarmaToSeat.playNextMedia();
                    }
                }
            }
            Rectangle{
                id:repeatRect
                width:qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                height:qsTranslate('','NowPlay_control_'+orientationType+'_h')
                anchors.top:controllerBg2.top;
                anchors.left:controllerBg2.left
                color:"transparent"
            }
            ToggleButton{
                id:repeatBtn
                anchors.centerIn: repeatRect
                normImg1:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.repeat_on:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.repeat_on
                normImg2:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.repeat_off:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.repeat_off
                pressImg1:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.repeat_on_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.repeat_on_press
                pressImg2:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.repeat_off_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.repeat_off_press
                cstate: viewHelper.userRepeatOn?(pif.vkarmaToSeat.getMediaRepeatValue()=="all"):false
                onActionReleased: {
                    if(!viewHelper.userRepeatOn){
                        if(viewHelper.isFromGenre){
                            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"repeat",type:"none"})
                            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"repeat",type:"all"})
                        }
                        else {
                            pif.vkarmaToSeat.repeatTrack({mediaRepeat:"none"})
                            pif.vkarmaToSeat.repeatTrack({mediaRepeat:"all"})
                        }
                    }
                    else{
                        if(cstate){
                            if(viewHelper.isFromGenre){
                                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"repeat",type:"none"})
                            }else{
                                pif.vkarmaToSeat.repeatTrack({mediaRepeat:"none"})
                            }
                        }else{
                            if(viewHelper.isFromGenre){
                                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"repeat",type:"all"})
                            }else{
                                pif.vkarmaToSeat.repeatTrack({mediaRepeat:"all"})
                            }
                        }
                    }
                    if(pif.vkarmaToSeat.getMediaRepeatValue()=="none")
                        viewHelper.userRepeatOn=false
                    else
                        viewHelper.userRepeatOn=true
                }
            }
            Rectangle{
                id:shuffleOnOffBtnRect
                width:qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                height:qsTranslate('','NowPlay_control_'+orientationType+'_h')
                anchors.top:controllerBg2.top;
                anchors.left:repeatRect.right
                color:"transparent"
            }
            ToggleButton{
                id:shuffleOnOffBtn
                anchors.centerIn: shuffleOnOffBtnRect
                normImg1: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.shuffle_on:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.shuffle_on
                normImg2:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.shuffle_off:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.shuffle_off
                pressImg1:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.shuffle_on_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.shuffle_on_press
                pressImg2:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.shuffle_off_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.shuffle_off_press
                cstate: (pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")?(pif.vkarmaToSeat.getMediaShuffle()):false
                onActionReleased: {
                    if(viewHelper.isFromGenre){
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"shuffle"});
                    }else{
                        pif.vkarmaToSeat.shuffleTrack()
                    }
                }
            }
            SimpleCellBtn{
                id:volumeBtn
                anchors.left:shuffleOnOffBtnRect.right
                anchors.top:controllerBg2.top;
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.volume_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.volume_button
                pressedImg: viewHelper.configToolImagePath+(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.volume_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.volume_button_press
                onBtnReleased: {
                    viewController.showScreenPopup(3)
                }
            }

        }
    }
    property QtObject aodPanelLandScape
    Component{
        id:compAodPanelLandScape
        Item{
            id:mainContL
            width:background.width
            height: background.height
            visible: (orientationType=='l' )
            Image{
                id:controlBg
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm_aod')
                source: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.aod_controller_bg:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.aod_controller_bg
            }
            SimpleCellBtn{
                id:previousTrack
                anchors.top:controlBg.top;anchors.left:controlBg.left
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.prev_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.prev_button
                pressedImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.prev_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.prev_button_press
                onBtnReleased:{
                    if(viewHelper.isFromGenre){
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"rwd"})
                    }else{
                        pif.vkarmaToSeat.playPreviousMedia();
                    }
                }
            }
            SimpleCellBtn{
                id:playPauseBtn
                anchors.top:controlBg.top
                anchors.left: previousTrack.right
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg:(isPlaying)?(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.pause_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.pause_button:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.play_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.play_button
                pressedImg:(isPlaying)?(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.pause_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.pause_button_press:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.play_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.play_button_press
                onBtnReleased:{
                    if(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play"){
                        pif.vkarmaToSeat.pausePlayingMedia();
                    }
                    else{
                        pif.vkarmaToSeat.resumePlayingMedia();
                    }
                }
            }
            SimpleCellBtn{
                id:nextTrack
                anchors.top:controlBg.top
                anchors.left: playPauseBtn.right
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.next_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.next_button
                pressedImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.next_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.next_button_press
                onBtnReleased: {
                    if(viewHelper.isFromGenre){
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"fwd"})
                    }else{
                        pif.vkarmaToSeat.playNextMedia();
                    }
                }
            }
            Rectangle{
                id:repeatRect
                width:qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                height:qsTranslate('','NowPlay_control_'+orientationType+'_h')
                anchors.top:controlBg.top;
                anchors.left:nextTrack.right
                color:"transparent"
            }
            ToggleButton{
                id:repeatBtn
                anchors.centerIn: repeatRect
                normImg1:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.repeat_on:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.repeat_on
                normImg2:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.repeat_off:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.repeat_off
                pressImg1:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.repeat_on_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.repeat_on_press
                pressImg2:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.repeat_off_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.repeat_off_press
                cstate: viewHelper.userRepeatOn?(pif.vkarmaToSeat.getMediaRepeatValue()=="all"):false
                onActionReleased: {
                    if(!viewHelper.userRepeatOn){
                        if(viewHelper.isFromGenre){
                            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"repeat",type:"none"})
                            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"repeat",type:"all"})
                        }
                        else {
                            pif.vkarmaToSeat.repeatTrack({mediaRepeat:"none"})
                            pif.vkarmaToSeat.repeatTrack({mediaRepeat:"all"})
                        }
                    }
                    else{
                        if(cstate){
                            if(viewHelper.isFromGenre){
                                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"repeat",type:"none"})
                            }else{
                                pif.vkarmaToSeat.repeatTrack({mediaRepeat:"none"})
                            }
                        }else{
                            if(viewHelper.isFromGenre){
                                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"repeat",type:"all"})
                            }else{
                                pif.vkarmaToSeat.repeatTrack({mediaRepeat:"all"})
                            }
                        }
                    }
                    if(pif.vkarmaToSeat.getMediaRepeatValue()=="none")
                        viewHelper.userRepeatOn=false
                    else
                        viewHelper.userRepeatOn=true
                }
            }
            Rectangle{
                id:shuffleOnOffBtnRect
                width:qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                height:qsTranslate('','NowPlay_control_'+orientationType+'_h')
                anchors.top:controlBg.top;
                anchors.left:repeatRect.right
                color:"transparent"
            }
            ToggleButton{
                id:shuffleOnOffBtn
                anchors.centerIn: shuffleOnOffBtnRect
                normImg1: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.shuffle_on:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.shuffle_on
                normImg2:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.shuffle_off:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.shuffle_off
                pressImg1:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.shuffle_on_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.shuffle_on_press
                pressImg2:(!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.shuffle_off_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.shuffle_off_press
                cstate: (pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")?(pif.vkarmaToSeat.getMediaShuffle()):false
                onActionReleased: {
                    if(viewHelper.isFromGenre){
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayFromGenre",{control:"shuffle"});
                    }else{
                        pif.vkarmaToSeat.shuffleTrack()
                    }
                }
            }
            SimpleCellBtn{
                id:volumeBtn
                anchors.left:shuffleOnOffBtnRect.right
                anchors.top:controlBg.top;
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_aod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.volume_button:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.volume_button
                pressedImg: (!isUsb)?viewHelper.configToolImagePath+configTag["music_"+orientationType].now_playing.volume_button_press:viewHelper.configToolImagePath+configTag["usb_"+orientationType].now_playing.volume_button_press
                onBtnReleased: {
                    viewController.showScreenPopup(3)
                }
            }
        }
    }
}
