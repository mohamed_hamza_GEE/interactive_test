import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: video
    visible:false
    /**************************************************************************************************************************/
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property bool isPlaying:(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play")?true:false
    property int currentMid:0
    property variant playDetails;
    property variant vodData
    property variant episodeModel;
    //  property bool playingFromSeat:false
    property bool isSeries:false
    property bool isAspect4isto3:false
    property string aspectRatio: ""
    property variant configMenu: (viewHelper.isKids(current.template_id))?"kids_menu_"+orientationType:"menu_"+orientationType
    /**************************************************************************************************************************/
    Timer{
        id:delayforVodPlay
        interval: 300
        onTriggered: viewHelper.callVodPlay()
    }
    Timer{
        id:delayforVodClose
        interval: 200
        onTriggered: executeNextVod()
    }

    Timer{
        id:pTimer
        interval: 1000
        onTriggered:{
            if(viewHelper.fromthreeDMaps){
                viewHelper.fromthreeDMaps=false
                var catnode = new Object();
                catnode["pcid"] = viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"cid")
                catnode["ptid"] =viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                catnode["cid"] = viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"cid")
                catnode["tid"] = "maps"
                pif.vkarmaToSeat.launchCategory(catnode);
            }
        }
    }

    Connections{
        target:pif.vkarmaToSeat
        onSigDataReceivedFromSeat:{
            switch(api){
            case "playTrailerAck":
            case "playVodAck":{
                viewHelper.setCurrentPlayingDetails(pif.vkarmaToSeat.getPlayingMidInfo())
                viewHelper.settempPlayingDetails(pif.vkarmaToSeat.getPlayingMidInfo())
                if(!viewHelper.isFromKarma) viewHelper.getSoundtracks(pif.vkarmaToSeat.getPlayingMidInfo()["mid"])
                currentMid=viewHelper.getVodType()==0?pif.vkarmaToSeat.getPlayingMidInfo()["aggregateMid"]:pif.vkarmaToSeat.getPlayingMidInfo()["mid"]
                core.dataController.getMediaByMid([currentMid],vodDataReady)
                viewHelper.isFromKarma=false
                if(current.template_id=="WELC" || current.template_id=="OPTSEL")return;
                init()
            }
            break;
            case "mediaPauseAck":{
            }
            break;
            case "mediaResumeAck":{
                nowPlayingDetails.showSpeed(false)
            }
            break;
            case "mediaRewindAck":{
                nowPlayingDetails.showSpeed(true)
                var speed=params.mediaPlayRate/100
                nowPlayingDetails.setSpeed(speed +"X")
            }
            break;
            case "mediaFastForwardAck":{
                nowPlayingDetails.showSpeed(true)
                var speed=params.mediaPlayRate/100
                nowPlayingDetails.setSpeed(speed +"X")
            }
            break;
            case "aspectRatioAck":{
                setAspectRatio()
            }
            break;
            case "videoStopAck":{
                core.debug(" videoStopAck is called : ")
                if(viewController.getActiveScreenPopupID()!=27)viewController.hideScreenPopup()
                nowPlayingDetails.showSpeed(false)
                if(viewHelper.fromthreeDMaps){pTimer.restart();}
                hideVodNowPlaying()
                //                if(viewHelper.fromthreeDMaps){
                //                    viewHelper.fromthreeDMaps=false
                //                    var catnode = new Object();
                //                    catnode["pcid"] = viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"cid")
                //                    catnode["ptid"] =viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                //                    catnode["cid"] = viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"cid")
                //                    catnode["tid"] = "maps"
                //                    pif.vkarmaToSeat.launchCategory(catnode);
                //                }
                delayforVodClose.restart();


            }
            break;
            case "setMediaPositionAck":{
                if(nowPlayingDetails.getSliderRef().isDragged)return
                var durationInSec = core.coreHelper.convertHMSToSec((isSeries)?episodeModel.getValue(nowPlayingDetails.getCurrentIndex(),"duration"):vodData.getValue(0,"duration"))
                nowPlayingDetails.calcTime(params.elapsedTime,durationInSec)
                nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getElapsedTime(),2))
                nowPlayingDetails.setRemainingTime("-"+(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getRemainingTime(durationInSec),2)))
            }
            break;
            default:
                break;
            }
        }
    }
    Connections{
        target:viewHelper
        onUpdateTvSeriesIndex:{
            nowPlayingDetails.setCurrentIndex(playIndex)
            nowPlayingDetails.setRemainingTime("-"+(parseInt(episodeModel.getValue(playIndex,"duration"),10)))
            nowPlayingDetails.setSliderMaxValue(core.coreHelper.convertHMSToSec(episodeModel.getValue(playIndex,"duration")))
            nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getElapsedTime(),2))
        }
        onStopVod:{
            if(status){
                pif.vkarmaToSeat.pausePlayingMedia()
                viewController.showScreenPopup(2)
            }
            else console.log("Status is false")
        }
    }

    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compNowPlayingDetails,"nowPlayingDetails")
        push(compVodPanelLandscape,"vodPanelLandscape")
        push(compVodPanelPortrait,"vodPanelPortrait")
        push(compTrailerLandscape,"trailerLandscape")
        push(compTrailerPortrait,"trailerPortrait")
    }
    function init(){
        console.log("Video Now Playing | init")
        if(viewHelper.isVoyagerBroadcast){viewHelper.isVoyagerBroadcast=false;return;}
        video.visible=true
    }
    function reload(){console.log("Video Now Playing | reload")}
    function clearOnExit(){
        core.debug("Video Now Playing | clearOnExit")
        video.visible=false
        //        if(viewHelper.isFromResumePopup){
        //            viewHelper.isFromResumePopup=false
        //            if(viewHelper.getVodType()==0)
        //                viewHelper.resumeTv()
        //            else
        //                viewHelper.resumeMovie()
        //        }


    }
    onLoaded: {}
    /**************************************************************************************************************************/
    function showVodNowPlaying(){init()}
    function hideVodNowPlaying(){clearOnExit()}
    function setAspectRatio(param){
        aspectRatio=pif.vkarmaToSeat.getAspectRatio()
        if(aspectRatio=="4x3Stretched"){
            isAspect4isto3=true
        }else if(aspectRatio=="16x9Stretched"){
            isAspect4isto3=false
        }else if(aspectRatio=="4x3"){
            isAspect4isto3=false
        }else if(aspectRatio=="16x9Fixed"){
            isAspect4isto3=false;
        }else {
            if(vodData.getValue(0,"aspect")=="16x9Fixed")
                isAspect4isto3=false
            else
                isAspect4isto3=true
        }
    }
    function setSoundtrackBtn(){
        if(viewHelper.soundtrackModel.count<=1){
            vodPanelPortrait.soundtrack.isDisable=true
            vodPanelLandscape.soundtrack.isDisable=true
        }else {
            vodPanelPortrait.soundtrack.isDisable=false
            vodPanelLandscape.soundtrack.isDisable=false
        }
    }
    function executeNextVod(){
        if(viewHelper.isTrailerPressed){
            viewHelper.isFromExitPopup=false
            viewHelper.loadVideoScreen()
        }
        if(viewHelper.isPlayBtnClicked && !viewHelper.isTrailerPressed){
            viewHelper.isPlayBtnClicked=false
            if((pif.vkarmaToSeat.getMidElapsedTime(viewHelper.gettempPlayingDetails()["mid"])>viewHelper.resumeSecs))
                viewController.showScreenPopup(20)
            else viewHelper.loadVideoScreen()
        }
        viewHelper.isFromExitPopup=false
        viewHelper.isTrailerPressed=false
        viewHelper.isPlayBtnClicked=false
        nowPlayingDetails.calcTime(0,1)
        viewHelper.changeSection(viewHelper.sectionBtnPressed)
        viewHelper.sectionBtnPressed=false;
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function playMovieFromTrailer(){
        var   tempcurrentMid=-1
        for(var i=0;i<viewHelper.synopsisModel.count;i++){
            if(viewHelper.synopsisModel.getValue(i,"trailer_mid")==currentMid){
                tempcurrentMid=viewHelper.synopsisModel.getValue(i,"mid")
            }
            else console.log("Invalid Mid")
        }
        core.dataController.getMediaByMid([tempcurrentMid,"","","","",5252],trailerToMovieData)
    }
    function trailerToMovieData(dModel){
        var playValue=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id"):viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
        var params=new Object()
        params["title"]=dModel.getValue(0,"title")
        params["mid"]=dModel.getValue(0,"mid")
        params["duration"]=dModel.getValue(0,"duration")
        params["vodType"]="movies"
        params["playingCategory"]=playValue
        viewHelper.settempPlayingDetails(params)
        viewHelper.isFromKarma=true
        viewHelper.isFromExitPopup=false
        viewHelper.isResume=false

        if( params["mid"]==pif.vkarmaToSeat.getPlayingMidInfo().mid  && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")return;
        else  if(viewHelper.isVideoPlaying()){
            //  viewHelper.exitCase=2
            viewHelper.isFromExitPopup=true
            viewController.showScreenPopup(2)
        }
        else  if((pif.vkarmaToSeat.getMidElapsedTime(viewHelper.synopsisModel.getValue(index,"mid"))>viewHelper.resumeSecs)){
            viewController.showScreenPopup(20)
        }else{
            viewHelper.loadVideoScreen(params["mid"])
        }
    }
    function vodDataReady(dModel){
        vodData=dModel
        var duration
        if (viewHelper.getVodType()==1)
            duration=core.coreHelper.convertHMSToSec(dModel.getValue(0,"duration"))
        else
            duration=core.coreHelper.convertHMSToSec(dModel.getValue(0,"duration"))
        if(dModel.getValue(0,"media_attr_template_id")=="tvseries"){
            nowPlayingDetails.showArrows(true)
            dataController.getNextMenu([dModel.getValue(0,"mid"),"tvseries"],tvEpisodeModelReady)
        }
        else{
            isSeries=false
            nowPlayingDetails.showArrows(false)
            nowPlayingDetails.setModel(dModel)
            nowPlayingDetails.setSliderMaxValue(duration)

            if(viewHelper.isResume){
                nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getElapsedTime(),2))
            }else {
                nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(0,2))
            }
            nowPlayingDetails.setRemainingTime("-"+(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getRemainingTime(duration),2)))
        }
        setAspectRatio()
        setSoundtrackBtn()
    }
    function tvEpisodeModelReady(dModel){
        episodeModel=dModel
        isSeries=true
        nowPlayingDetails.setModel(dModel)
        nowPlayingDetails.setSliderMaxValue(core.coreHelper.convertHMSToSec(dModel.getValue(0,"duration")))
        nowPlayingDetails.setRemainingTime("-"+(parseInt(dModel.getValue(0,"duration"),10)))
        nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getElapsedTime(),2))
        viewHelper.episodeModel=dModel;
        viewHelper.setTvModel(dModel)
    }
    function loadPreviousScreen(){
        widgetList.showFooter(false)
        widgetList.showFooter(true)
        hideVodNowPlaying()
    }
    function showPopup(val){
        if(val=="volume")viewController.showScreenPopup(3)
        else if(val=="brightness"){viewHelper.isMonitor=true;viewController.showScreenPopup(4)}
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag[configMenu].main.bg
            }
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.background//.bg_music
            }

        }
    }
    property QtObject nowPlayingDetails
    Component{
        id:compNowPlayingDetails
        Item{
            anchors.fill:background
            function getSliderRef(){return progressBar}
            function calcTime(elapsedTime,durationSec){progressBar.externalCalc(elapsedTime,durationSec)}
            function showSpeed(visible){speedText.visible=visible}
            function setSpeed(value){speedText.text=value}
            function setSliderMaxValue(endTime){progressBar.maxLevel=endTime}
            function setElapseTime(elapsedTime){elapsedText.text=elapsedTime}
            function setRemainingTime(remainingTime){

                remainingText.text=remainingTime}
            function showArrows(val){leftArrow.visible=val;rightArrow.visible=val }
            function setModel(dModel){vodFlickView.model=dModel}
            function getCurrentIndex(){return vodFlickView.currentIndex}
            function setCurrentIndex(val){vodFlickView.currentIndex=val}
            ListView{
                id:vodFlickView
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_vod_cont_'+orientationType+'_tm')
                anchors.left: parent.left;anchors.leftMargin: qsTranslate('','NowPlay_vod_cont_'+orientationType+'_lm')
                width:qsTranslate('','NowPlay_vod_cont_'+orientationType+'_w')
                height:qsTranslate('','NowPlay_vod_cont_'+orientationType+'_h')
                boundsBehavior:ListView.StopAtBounds;
                preferredHighlightBegin:0;
                preferredHighlightEnd:width;
                highlightRangeMode:ListView.StrictlyEnforceRange;
                orientation:ListView.Horizontal
                clip:true;
                delegate:compVodDetails
                snapMode: ListView.SnapOneItem
                flickDeceleration: 50
                onModelChanged: console.log(">>>>>>>>>>>>>>>>>>>> Model Changed")
                onMovementEnded: {
                    if(episodeModel.count>0){
                        viewHelper.playingEpisodeIndex=currentIndex
                        nowPlayingDetails.setRemainingTime("-"+(parseInt(episodeModel.getValue(currentIndex,"duration"),10)))

                        nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getElapsedTime(),2))
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayEpisode",{index:currentIndex})
                    }
                }
            }
            Image{
                id:leftArrow
                source:viewHelper.configToolImagePath+configTag["menu_l"].main.left_arrow
                anchors.left:parent.left
                visible: false
                anchors.verticalCenter: vodFlickView.verticalCenter
            }
            Image{
                id:rightArrow
                source:viewHelper.configToolImagePath+configTag["menu_l"].main.right_arrow
                anchors.right:parent.right
                visible: false
                anchors.verticalCenter: vodFlickView.verticalCenter
            }
            ViewText{
                id:speedText
                visible:false
                color:configTag["global_"+orientationType].video_now_playing.speed_text
                font.pixelSize: qsTranslate('','NowPlay_vod_rewfor_'+orientationType+'_fontsize')
                width:qsTranslate('','NowPlay_vod_rewfor_'+orientationType+'_w')
                height:qsTranslate('','NowPlay_vod_rewfor_'+orientationType+'_h')
                anchors.left:parent.left;anchors.leftMargin:qsTranslate('','NowPlay_vod_rewfor_'+orientationType+'_lm')//parseInt(qsTranslate('','NowPlay_vod_rewfor_'+orientationType+'_lm'),10)+parseInt(qsTranslate('','NowPlay_vod_poster_'+orientationType+'_lm'),10)+parseInt(qsTranslate('','Listing_poster_'+orientationType+'_w'),10)
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','NowPlay_vod_rewfor_'+orientationType+'_tm')//parseInt(qsTranslate('','NowPlay_vod_rewfor_'+orientationType+'_tm'),10)+parseInt( qsTranslate('','NowPlay_vod_poster_'+orientationType+'_tm'),10)
                horizontalAlignment: Text.AlignRight
            }
            Rectangle{
                id:progressbarCont
                width:qsTranslate('','NowPlay_progcont_'+orientationType+'_w')
                height:qsTranslate('','NowPlay_progcont_'+orientationType+'_h')
                anchors.left:parent.left;anchors.leftMargin:qsTranslate('','NowPlay_progcont_'+orientationType+'_lm_vod')//parseInt( qsTranslate('','NowPlay_progcont_'+orientationType+'_lm_vod'),10)+parseInt(qsTranslate('','NowPlay_vod_poster_'+orientationType+'_lm'),10)+parseInt(qsTranslate('','Listing_poster_'+orientationType+'_w'),10)
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','NowPlay_progcont_'+orientationType+'_tm_vod')// parseInt(qsTranslate('','NowPlay_progcont_'+orientationType+'_tm_vod'),10)+parseInt( qsTranslate('','NowPlay_vod_poster_'+orientationType+'_tm'),10)
                color:"transparent"
                ViewText{
                    id:elapsedText
                    height:qsTranslate('','NowPlay_prog_time_'+orientationType+'_h')
                    width:qsTranslate('','NowPlay_prog_time_'+orientationType+'_w')
                    font.pixelSize: qsTranslate('','NowPlay_prog_time_'+orientationType+'_fontsize')
                    color:configTag["global_"+orientationType].video_now_playing.text_color
                    anchors.left: progressbarCont.left
                    anchors.bottom: progressbarCont.bottom
                    horizontalAlignment: Text.AlignLeft
                }
                ViewText{
                    id:remainingText
                    height:qsTranslate('','NowPlay_prog_time_'+orientationType+'_h')
                    width:qsTranslate('','NowPlay_prog_time_'+orientationType+'_w')
                    font.pixelSize: qsTranslate('','NowPlay_prog_time_'+orientationType+'_fontsize')
                    color:configTag["global_"+orientationType].video_now_playing.text_color
                    anchors.right: progressbarCont.right
                    anchors.bottom: progressbarCont.bottom
                    horizontalAlignment: Text.AlignRight
                }
                Slider{
                    id: progressBar
                    anchors.horizontalCenter: progressbarCont.horizontalCenter
                    anchors.top: progressbarCont.top
                    height:qsTranslate('','NowPlay_prog_'+orientationType+'_h')
                    width:qsTranslate('','NowPlay_prog_'+orientationType+'_w')
                    sliderbg:configTag["global_"+orientationType].video_now_playing.base_image
                    sliderFillingtItems:[configTag["global_"+orientationType].video_now_playing.fill_color,configTag["global_"+orientationType].video_now_playing.indicator]
                    stepsValue:1
                    isFill:true
                    enableDragging: speedText.visible?false:true
                    onValueChanged:{
                        var count=Math.round(sliderDotImg.anchors.leftMargin/progressBar.shiftingPerClick)
                        pif.vkarmaToSeat.setPlayingMediaPosition({elapsedTime:progressBar.stepsValue * count})
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedDraggingOn")

                    }
                    onDragingOn: {

                        var durationInSec = core.coreHelper.convertHMSToSec((isSeries)?episodeModel.getValue(nowPlayingDetails.getCurrentIndex(),"duration"):vodData.getValue(0,"duration"))
                        var elapseTime=durationInSec-shiftingValueForDragging
                        nowPlayingDetails.setElapseTime(core.coreHelper.getTimeFormatFromSecs(elapseTime,2))
                        //nowPlayingDetails.setRemainingTime("-"+core.coreHelper.getTimeFormatFromSecs(pif.vkarmaToSeat.getRemainingTime(durationInSec),2))
                        //pif.vkarmaToSeat.setPlayingMediaPosition({elapsedTime:elapseTime})

                    }
                }
            }
        }
    }
    property QtObject vodPanelLandscape
    Component{
        id:compVodPanelLandscape
        Item{
            id:mainContLandscape
            width:background.width
            height: background.height
            visible: (orientationType=='l' && viewHelper.getVodType()!=1)
            property alias soundtrack: soundtrack
            Image{
                id:controlBg
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm_vod')
                source: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.vod_controller_bg
            }
            Rectangle{
                id:btnContainer
                width:qsTranslate('','NowPlay_control_'+orientationType+'_w_total_trail')
                height:qsTranslate('','NowPlay_control_'+orientationType+'_h')
                anchors.centerIn: controlBg
                color:"transparent"
                SimpleCellBtn{
                    id:rewind
                    anchors.left: controlBg.left
                    anchors.top:controlBg.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.rewind
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.rewind_press
                    onBtnReleased: {
                        pif.vkarmaToSeat.rewindPlayingMedia();
                    }
                }
                SimpleCellBtn{
                    id:playPause
                    anchors.left: rewind.right
                    anchors.top:controlBg.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg:(isPlaying)?viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.pause: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.play
                    pressedImg:(isPlaying)?viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.pause_press:viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.play_press
                    onBtnReleased: {
                        if(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play"){
                            pif.vkarmaToSeat.pausePlayingMedia();
                        }
                        else{
                            pif.vkarmaToSeat.resumePlayingMedia();
                        }
                    }
                }
                SimpleCellBtn{
                    id:stop
                    anchors.left: playPause.right
                    anchors.top:controlBg.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.stop
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.stop_press
                    onBtnReleased: {
                        if(pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")pif.vkarmaToSeat.pausePlayingMedia()
                        viewHelper.sectionBtnPressed=false
                        viewController.showScreenPopup(2)
                    }
                }

                SimpleCellBtn{
                    id:forward
                    anchors.left: stop.right
                    anchors.top:controlBg.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.forward
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.forward_press
                    onBtnReleased: {
                        pif.vkarmaToSeat.forwardPlayingMedia();
                    }
                }
                SimpleCellBtn{
                    id:soundtrack
                    anchors.left:forward.right
                    anchors.top:controlBg.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.soundtrackcc
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.soundtrackcc_press
                    onBtnReleased: {
                        viewHelper.setsndccPopFromMovie(true);
                        viewController.showScreenPopup(1)
                    }
                }
                SimpleCellBtn{
                    id:brightnessBtn
                    anchors.left:soundtrack.right
                    anchors.top:controlBg.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.brightness
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.brightness_press
                    onBtnReleased: {
                        showPopup("brightness")
                    }
                }
                SimpleCellBtn{
                    id:volumeBtn
                    anchors.left:brightnessBtn.right
                    anchors.top:controlBg.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.volume
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.volume_press
                    onBtnReleased: {
                        showPopup("volume")
                    }
                }
            }
        }
    }
    property QtObject vodPanelPortrait
    Component{
        id:compVodPanelPortrait
        Item{
            id:mainContPortrait
            width:background.width
            height: background.height
            visible:(orientationType=='p'&& viewHelper.getVodType()!=1)
            property alias soundtrack: soundtrack
            Image{
                id:controllerBg1
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm')
                source: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.controller_bg
            }
            Image{
                id:controllerBg2
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm1')
                source: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.controller_bg
            }
            SimpleCellBtn{
                id:rewind
                anchors.left: controllerBg1.left
                anchors.top:controllerBg1.top;
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.rewind
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.rewind_press
                onBtnReleased: {
                    pif.vkarmaToSeat.rewindPlayingMedia();
                }
            }
            SimpleCellBtn{
                id:playPause
                anchors.left: rewind.right
                anchors.top:controllerBg1.top;
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg:(isPlaying)?viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.pause: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.play
                pressedImg:(isPlaying)?viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.pause_press:viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.play_press
                onBtnReleased: {
                    if(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play"){
                        pif.vkarmaToSeat.pausePlayingMedia();
                    }
                    else{
                        pif.vkarmaToSeat.resumePlayingMedia();
                    }
                }
            }
            SimpleCellBtn{
                id:stop
                anchors.left: playPause.right
                anchors.top:controllerBg1.top;
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.stop
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.stop_press
                onBtnReleased: {
                    pif.vkarmaToSeat.pausePlayingMedia()
                    viewHelper.sectionBtnPressed=false
                    viewController.showScreenPopup(2)
                }
            }
            SimpleCellBtn{
                id:forward
                anchors.left: stop.right
                anchors.top:controllerBg1.top;
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.forward
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.forward_press
                onBtnReleased: {
                    pif.vkarmaToSeat.forwardPlayingMedia();
                }
            }
            Rectangle{
                id:btnContainer
                width:qsTranslate('','NowPlay_control_'+orientationType+'_w_total_trail')
                height:qsTranslate('','NowPlay_control_'+orientationType+'_h')
                anchors.centerIn: controllerBg2
                color:"transparent"


                SimpleCellBtn{
                    id:soundtrack
                    anchors.left:controllerBg2.left
                    anchors.top:controllerBg2.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.soundtrackcc
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.soundtrackcc_press
                    onBtnReleased: {
                        viewHelper.setsndccPopFromMovie(true);
                        viewController.showScreenPopup(1)
                    }
                }
                SimpleCellBtn{
                    id:brightnessBtn
                    anchors.left:soundtrack.right
                    anchors.top:controllerBg2.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.brightness
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.brightness_press
                    onBtnReleased: {
                        showPopup("brightness")
                    }
                }
                SimpleCellBtn{
                    id:volumeBtn
                    anchors.left:brightnessBtn.right
                    anchors.top:controllerBg2.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.volume
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.volume_press
                    onBtnReleased: {
                        showPopup("volume")
                    }
                }
            }
        }
    }
    property QtObject trailerLandscape
    Component{
        id:compTrailerLandscape
        Item{
            id:mainContLandscape
            width:background.width
            height: background.height
            visible: (orientationType=='l' && viewHelper.getVodType()==1)
            Image{
                id:controlBg
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm_vod')
                source: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.vod_controller_bg
            }
            SimpleCellBtn{
                id:replayBtn
                anchors.left: controlBg.left
                anchors.top:controlBg.top;
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.replay
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.replay_press
                onBtnReleased: {
                    pif.vkarmaToSeat.setPlayingMediaPosition(0)
                }
            }
            SimpleCellBtn{
                id:stopBtn
                anchors.left:replayBtn.right
                anchors.top:controlBg.top
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.stop
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.stop_press
                onBtnReleased: {
                    viewController.showScreenPopup(2)
                }
            }
            SimpleCellBtn{
                id:volumebtn
                anchors.left:stopBtn.right
                anchors.top:controlBg.top
                cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.volume
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.volume_press
                onBtnReleased: {
                    showPopup("volume")
                }
            }
            BorderImage{
                id:playMovieBtn
                property bool isPressed: false
                anchors.right:controlBg.right;anchors.rightMargin:qsTranslate('','NowPlay_play_button_'+orientationType+'_rm')
                anchors.top:controlBg.top;anchors.topMargin:qsTranslate('','NowPlay_play_button_'+orientationType+'_tm')
                width:qsTranslate('','NowPlay_play_button_'+orientationType+'_w')
                border{
                    left:qsTranslate('','NowPlay_play_button_'+orientationType+'_margin')
                    right:qsTranslate('','NowPlay_play_button_'+orientationType+'_margin')
                }
                source:(isPressed)?viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.btn_press:viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.btn
                ViewText{
                    id:playMovieBtnTxt
                    varText: [configTool.language.movies.play,(playMovieBtn.isPressed)?configTag["global_"+orientationType].video_now_playing.btn_text:configTag["global_"+orientationType].video_now_playing.btn_text,qsTranslate('','NowPlay_play_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','NowPlay_play_buttontxt_'+orientationType+'_w')
                    height:qsTranslate('','NowPlay_play_buttontxt_'+orientationType+'_h')
                    anchors.centerIn:parent
                    horizontalAlignment: Text.AlignHCenter
                }
                MouseArea{
                    anchors.fill:parent
                    onPressed: playMovieBtn.isPressed=true
                    onReleased: {
                        playMovieBtn.isPressed=false
                        playMovieFromTrailer()
                    }
                }
            }
        }
    }
    property QtObject trailerPortrait
    Component{
        id:compTrailerPortrait
        Item{
            id:mainContPortrait
            width:background.width
            height: background.height
            visible: (orientationType=='p' && viewHelper.getVodType()==1)
            Image{
                id:controllerBg1
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm')
                source: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.controller_bg
            }
            Image{
                id:controllerBg2
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_controlbg_'+orientationType+'_tm1')
                source: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.controller_bg
            }
            BorderImage{
                id:playMovieBtn
                property bool isPressed: false
                anchors.right:controllerBg2.right;anchors.rightMargin:qsTranslate('','NowPlay_play_button_'+orientationType+'_rm')
                anchors.top:controllerBg2.top;anchors.topMargin:qsTranslate('','NowPlay_play_button_'+orientationType+'_tm')
                width:qsTranslate('','NowPlay_play_button_'+orientationType+'_w')
                border{
                    left:qsTranslate('','NowPlay_play_button_'+orientationType+'_margin')
                    right:qsTranslate('','NowPlay_play_button_'+orientationType+'_margin')
                }
                source:(isPressed)?viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.btn_press:viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.btn
                ViewText{
                    id:playMovieBtnTxt
                    varText: [configTool.language.movies.play,(playMovieBtn.isPressed)?configTag["global_"+orientationType].video_now_playing.btn_text:configTag["global_"+orientationType].video_now_playing.btn_text,qsTranslate('','NowPlay_play_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','NowPlay_play_buttontxt_'+orientationType+'_w')
                    height:qsTranslate('','NowPlay_play_buttontxt_'+orientationType+'_h')
                    anchors.centerIn:parent
                    horizontalAlignment: Text.AlignHCenter
                }
                MouseArea{
                    anchors.fill:parent;
                    onPressed: playMovieBtn.isPressed=true
                    onReleased: {
                        playMovieBtn.isPressed=false
                        playMovieFromTrailer()
                    }
                }
            }
            Rectangle{
                id:btnContainer
                width:qsTranslate('','NowPlay_control_'+orientationType+'_w_total_trail')
                height:qsTranslate('','NowPlay_control_'+orientationType+'_h')
                anchors.centerIn: controllerBg1
                color:"transparent"
                SimpleCellBtn{
                    id:replayBtn
                    anchors.left: btnContainer.left
                    anchors.top:btnContainer.top;
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.replay
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.replay_press
                    onBtnReleased: {
                        pif.vkarmaToSeat.setPlayingMediaPosition(0)
                    }
                }
                SimpleCellBtn{
                    id:stopBtn
                    anchors.left:replayBtn.right
                    anchors.top:btnContainer.top
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.stop
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.stop_press
                    onBtnReleased: {
                        viewController.showScreenPopup(2)
                    }
                }
                SimpleCellBtn{
                    id:volumebtn
                    anchors.left:stopBtn.right
                    anchors.top:btnContainer.top
                    cellWidth: qsTranslate('','NowPlay_control_'+orientationType+'_w_single_vod')
                    cellHeight: qsTranslate('','NowPlay_control_'+orientationType+'_h')
                    normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.volume
                    pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].video_now_playing.volume_press
                    onBtnReleased: {
                        showPopup("volume")
                    }
                }
            }
        }
    }
    Component{
        id:compVodDetails
        Item{
            width:qsTranslate('','NowPlay_vod_cont_'+orientationType+'_w')
            height:qsTranslate('','NowPlay_vod_cont_'+orientationType+'_h')
            Image{
                id:poster
                anchors.bottom: parent.bottom;anchors.bottomMargin: qsTranslate('','NowPlay_poster_'+orientationType+'_bm')
                anchors.left:parent.left;anchors.leftMargin: qsTranslate('','NowPlay_poster_'+orientationType+'_lm')

                width:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_w'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_w'):qsTranslate('','Listing_poster_disc_'+orientationType+'_w')
                height:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_h'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_h'):qsTranslate('','Listing_poster_disc_'+orientationType+'_h')
                source:(orientationType=='l')?viewHelper.mediaImagePath+poster_l:viewHelper.mediaImagePath+poster_p
            }
            ViewText{
                id:titleText
                varText:[title,configTag["global_"+orientationType].video_now_playing.text_color,qsTranslate('','NowPlay_vod_title_'+orientationType+'_fontsize')]
                width:qsTranslate('','NowPlay_vod_title_'+orientationType+'_w');
                //Rectangle{anchors.fill: parent;color:"green";opacity:0.5}
                height:qsTranslate('','NowPlay_vod_title_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','NowPlay_vod_title_'+orientationType+'_tm')
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: (orientationType=='l')?Text.AlignLeft:Text.AlignHCenter
                anchors.right: parent.right;//anchors.rightMargin: qsTranslate('','NowPlay_vod_title_'+orientationType+'_rm')
                lineHeight:qsTranslate('','NowPlay_vod_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                wrapMode:Text.WordWrap
            }
        }
    }
}
