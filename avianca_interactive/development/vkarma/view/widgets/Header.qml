import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: header
    visible:false
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'l'
    property bool isPressed: false
    property string  globalHeader: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:"global_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compHeaderBtns,"headerBtns")
    }
    function init(){
        console.log("Header.qml | init")
    }
    function reload(){console.log("Header.qml | reload")}
    function clearOnExit(){
        console.log("Header.qml | clearOnExit")
    }
    onLoaded: {}
    Connections{
        target:viewHelper
        onChangeSection:{
            if(sectionClicked){
                sectionBtnAction()
            }
        }
    }
    /**************************************************************************************************************************/
    function setSectionBtnText(val){headerBtns.setSectionBtnText(val)}
    function showHeader(val){header.visible=val}
    function changeSection(selIndex){
        if(viewData.optionMenuModel.getValue(selIndex,"category_attr_template_id")=="adult")
            widgetList.setSectionBtnText(configTool.language.global_elements.button_kids_text)
        else
            widgetList.setSectionBtnText(configTool.language.global_elements.button_mainmenu_text)
        if(viewHelper.isFromSeat==true){viewHelper.isFromSeat=false}
        else{
            core.info(">>>>>>>>>>>>>>>>>>>>>>>>>>SECTION"+viewData.optionMenuModel.getValue(viewHelper.sectionIndexSel,"category_attr_template_id"))
          senduserDefindTimer.restart()
        }
        viewController.loadNext(viewData.optionMenuModel,selIndex);
    }
    Timer{id:senduserDefindTimer;interval: 200;onTriggered:pif.vkarmaToSeat.sendUserDefinedApi("userDefinedJump",{section:viewData.optionMenuModel.getValue(viewHelper.sectionIndexSel,"category_attr_template_id"),language:core.settings.languageISO})}
    function sectionBtnAction(){
        viewHelper.firstKidsLoad=false
        viewHelper.firstMainLoad=false
        if(viewData.optionMenuModel.getValue(viewHelper.sectionIndexSel,"category_attr_template_id")=="adult")
            viewHelper.sectionIndexSel=core.coreHelper.searchEntryInModel(viewData.optionMenuModel,"category_attr_template_id","kids")
        else
            viewHelper.sectionIndexSel=core.coreHelper.searchEntryInModel(viewData.optionMenuModel,"category_attr_template_id","adult")
        viewHelper.hideWidgets()
        changeSection(viewHelper.sectionIndexSel)

    }
    /**************************************************************************************************************************/
    property QtObject headerBtns
    Component{
        id:compHeaderBtns
        Item{
            function setSectionBtnText(val){sectionBtnText.text=val}
            height:qsTranslate('','Common_head_h');width:parent.width
            SimpleButton{
                id:flightInfo
                normImg:viewHelper.configToolImagePath+configTag[globalHeader].header.flightinfo_button
                pressedImg: viewHelper.configToolImagePath+configTag[globalHeader].header.flightinfo_button_press
                anchors.top:parent.top;anchors.left:parent.left
                onActionReleased: {
                    viewController.showScreenPopup(18)
                }
            }
            SimpleButton{
                id:help
                normImg:viewHelper.configToolImagePath+configTag[globalHeader].header.help_button
                pressedImg: viewHelper.configToolImagePath+configTag[globalHeader].header.help_button_press
                anchors.top:parent.top;
                anchors.right:sectionBtn.left;anchors.rightMargin: qsTranslate('','Common_headhelp_rm')
                onActionReleased: {
                    viewController.showScreenPopup(13)
                }
            }
            Rectangle{
                id:sectionBtn
                width:qsTranslate('','Common_headtext_w');height:qsTranslate('','Common_headtext_w')
                color:"transparent"
                anchors.top:parent.top;
                anchors.right:parent.right;anchors.rightMargin: qsTranslate('','Common_headtext_rm')
                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        isPressed=true
                    }
                    onReleased: {
                        isPressed=false
                        if((pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate") && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"){
                            viewHelper.stopVod(true)
                            viewHelper.sectionBtnPressed=true
                        }
                        else {
                            viewHelper.isFromSeat=false
                            sectionBtnAction()
                        }
                    }
                }
                ViewText{
                    id:sectionBtnText
                    width:parent.width
                    varText: ['',isPressed?configTag[globalHeader].header.text_color_press:configTag[globalHeader].header.text_color,qsTranslate('','Common_headtext_fontsize')]
                    horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter
                    anchors.centerIn:parent;
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight;
                }
            }
            Image{
                id:btnPressedState
                anchors.centerIn: sectionBtn
                visible:isPressed
                source:viewHelper.configToolImagePath+configTag[globalHeader].header.header_text_press
            }
            Image{
                id:divLine
                source:viewHelper.configToolImagePath+configTag[globalHeader].header.header_divline
                anchors.bottom: parent.bottom
            }
        }
    }
}
