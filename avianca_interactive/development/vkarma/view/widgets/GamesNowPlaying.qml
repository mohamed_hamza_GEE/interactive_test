import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: gamesNowPlaying
    visible:false
    property variant configTag: configTool.config
    property string orientationType:"l"// (visible)?pif.getOrientationType():'p'
    property bool isBriSlider:false;
    property variant gameModel;
    MouseArea{
        anchors.fill:parent;
        onClicked:{

        }
    }

    Image{
        source:viewHelper.configToolImagePath+configTag["global_l"].entertainment_off.entoff_bg
        anchors.centerIn:parent;
        rotation:(pif.getOrientationType()=="p")?-90:0;
        Image{
            anchors.centerIn: parent
            source:viewHelper.configToolImagePath+configTag["global_l"].entertainment_off.entoff_logo
            smooth:true
        }
        Image{
            id:gameImg
            anchors.fill:parent;
            source:viewHelper.mediaImagePath+gameModel.getValue(0,"poster_"+orientationType)
        }
    }

    Rectangle{
        anchors.fill:closeBtn;
        color:"black";
        opacity:0.5;
        visible:false//(closeBtn.opacity==1);
    }
    SimpleButton{
        id:closeBtn
//        opacity:0;
        anchors.top:gamesNowPlaying.top;anchors.topMargin: parseInt(qsTranslate('','Popups_close_'+orientationType+'_game_tm'),10)
        anchors.right: gamesNowPlaying.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
        normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].help.close_btn_press;
        pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].help.close_btn_press;
        onActionReleased: {
            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedForceCloseGame")
        }
    }


    Item{
        id:volBrightCont
        //visible: false
        width:qsTranslate('','Popups_volbri_cont_'+orientationType+'_w')
        height:qsTranslate('','Popups_volbri_cont_'+orientationType+'_h')


        anchors.verticalCenter:parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        rotation:(pif.getOrientationType()=="p")?0:90;
        SimpleButton{
            id:volumeMinus
            anchors.bottom: volBrightCont.bottom
            anchors.horizontalCenter: volBrightCont.horizontalCenter
            normImg:  viewHelper.configToolImagePath+(isBriSlider?configTag["global_"+orientationType].settings.brightness_decrease:configTag["global_"+orientationType].settings.volume_decrease)
            pressedImg: viewHelper.configToolImagePath+(isBriSlider?configTag["global_"+orientationType].settings.brightness_decrease_press:configTag["global_"+orientationType].settings.volume_decrease_press)
            onActionReleased: {
                volumeSliderComp.prevPage()
            }
            focus:true;
            rotation:(orientationType=="p")?0:-90
        }
        Rectangle{
            anchors.centerIn: parent;
            color:"black"
            opacity:0.6;
            height:volBrightCont.height-(volumeMinus.height);
            width:volBrightCont.width/4

        }
        Slider{
            id:volumeSliderComp
            anchors.centerIn: parent
            height: volBrightCont.height-(volumeMinus.height+volumePlus.height)
            width:  volBrightCont.width
            orientation: Qt.Vertical
            isReverseSliding:true
            sliderbg:configTag["global_"+orientationType].settings.slider_base
            sliderFillingtItems: [configTag["global_"+orientationType].settings.slider_fill,configTag["global_"+orientationType].settings.indicator]
            stepsValue:(isBriSlider)?pif.getBrightnessStep(): pif.getVolumeStep()
            isFill: true
            maxLevel:100

            onValueChanged: {
                var dragValue=shiftedValue
                pif.vkarmaToSeat.setSeatbackVolume(dragValue)

            }

        }
        SimpleButton{
            id:volumePlus
            anchors.top: volBrightCont.top
            anchors.horizontalCenter: volBrightCont.horizontalCenter
            normImg:  viewHelper.configToolImagePath+(isBriSlider?configTag["global_"+orientationType].settings.brightness_increase:configTag["global_"+orientationType].settings.volume_increase)
            pressedImg: viewHelper.configToolImagePath+(isBriSlider?configTag["global_"+orientationType].settings.brightness_increase_press:configTag["global_"+orientationType].settings.volume_increase_press)
            onActionPressed: {volumeSliderComp.nextPage()}
            rotation:(orientationType=="p")?0:-90
        }
        Component.onCompleted: {
            volumeSliderComp.setInitailValue=isBriSlider?pif.vkarmaToSeat.getSeatbackBrightness():pif.vkarmaToSeat.getSeatbackVolume()
        }
    }


    /**************************************************************************************************************************/
    function load(){
    //    push(compNowPlayingArea,"nowPlayingArea")
    }
    function init(){
        console.log("gamesNowPlaying.qml | init")
        visible=true;
        var params=[];
        params["mid"]=viewHelper.gameMidToBePlayed;
        core.debug(params["mid"])
        dataController.getMediaByMid([params["mid"]],gotData)
    }

    function gotData(model){
        gameModel=model;

    }

    function showGamesNowPlayingClose(){
      core.debug("show the close button")
//        closeBtn.opacity=1;
    }

    function reload(){}
    function clearOnExit(){
        gameModel="";
        console.log("gamesNowPlaying.qml | clearOnExit")
//        closeBtn.opacity=0;
        visible=false
    }
    onLoaded: {}
    /********************************************************************************************/
    function showGamesNowPlaying(val){
        if(val)init()
        else clearOnExit()
    }
    function loadPreviousScreen(){visible=false}
    /**************************************************************************************************************************/

}
