import QtQuick 1.1
import "../framework"
Item {
    property bool vkb_available:true
    property bool _vKarmaReady:false
    //Timer added since Framework variable not getting update pif.vkarmaToSeat.getMediaType() instantly
    Timer{
        id:delayTimer
        interval: 300
        onTriggered:viewHelper.launchGame()
    }
    Connections{
        target:core.pif;
        onPaStateChanged:{
            if(pif.getPAState())
                viewController.updateSystemPopupQ(1,true);
            else
                viewController.updateSystemPopupQ(1,false);
        }
        onWowStateChanged:{ /*viewHelper.checkAndStartSS();*/}
        onOpenFlightChanged:{
            viewController.hideScreenPopup(false);
            viewController.updateSystemPopupQ(1,false);
        }
        onEntOnChanged:{
            /*viewHelper.checkAndStartSS();*/
        }
        onPowerDownEventReceived:{
            core.debug("PifEvents.qml | onPowerDownEventReceived")
            viewController.updateSystemPopupQ(3,true);
            pif.executePowerDownEvent()
        }
    }
    Connections{
        target:dataController.connectingGate;
        onSigCgStatusChanged:{
            if(status==true)
                viewController.showScreenPopup(11)
        }
    }
    Connections{
        target:core.pif;
        onCgDataAvailableChanged:{
            if(cgDataAvailable==1){
                if(current.template_id=="discover")
                    return
                else
                    viewController.showScreenPopup(12)
            }
            else{
                viewController.showScreenPopup(10)
                viewHelper.homeCalled()
            }
        }
    }

    Connections{
        target: core.dataController.mediaServices;
        onSigCidBlockChanged:{
            console.log("PifEvents.qml | Cid Block Changed | cidList : " + cidList + ", status : " + status + ", smid : " + smid + ", serviceCode : " + serviceCode);
            viewHelper.actionOnServiceBlock(status,serviceCode,cidList);
        }
        onSigMidBlockChanged: {
            console.log("PifEvents.qml | Mid Block Changed | mid : " + mid + ", status : " + status);
            viewHelper.actionOnMidBlock(mid,status);
        }
    }
    Connections{
        target: pif
        onSeatRatingChanged:{
            if(pif.vkarmaToSeat){
                viewHelper.actionOnMidBlock(pif.vkarmaToSeat.getPlayingMid(),"",pif.vod.getMediaRating());
                viewHelper.actionOnMidBlock(pif.vkarmaToSeat.getPlayingMid(),"",pif.aod.getMediaRating());
            }
        }
    }
    Connections{
        target:pif.sharedModelServerInterface;
        onSigDataReceived:{}
    }

    Connections{
        target:pif.vkarmaToSeat;
        onDisconnected:{
            _vKarmaReady=false;
            viewController.hideScreenPopup()
            viewHelper.loadEntOffScreen()
        }
        onMediaRepeatChanged:{}
        onSigDataReceivedFromSeat:{
            switch(api){
            case "playVodAck":{

                if(viewController.getActiveScreenPopupID()==1){
                    if(params.mid==viewData.mediaModel.getValue(viewHelper.mediaSelected,"mid"))
                        viewController.hideScreenPopup()
                }
            }
            break;
            case "userDefinedsetSeatStatus":{
                if(params.value=="ready"){
                    _vKarmaReady = true
                    var mediaPlayerState=pif.vkarmaToSeat.getCurrentMediaPlayerState();
                    var iso = params.languageIso
                    core.setInteractiveLanguage(dataController.getLidByLanguageISO(iso),iso);
                    if(mediaPlayerState!="stop") {
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedgetPlayingCategory")
                    }
                    viewHelper.vkarmaReady(_vKarmaReady)
                }
            }
            break;
            case "userDefinedBroadCastWithNoAudio":{
                pif.vkarmaToSeat.mediaType="audio"
                if(params.mediaState==5)
                    pif.vkarmaToSeat.mediaPlayerState="stop";
                else if(params.mediaState==1)
                    pif.vkarmaToSeat.mediaPlayerState="play";
                else if(params.mediaState==2)
                    pif.vkarmaToSeat.mediaPlayerState="pause";

            }
            break;
            case "userDefinedCtMessageinProgress":{
                if(params.status)
                    viewController.updateSystemPopupQ(2,true);
                else {
                    if(viewController.getActiveSystemPopupID()==2)
                        viewController.updateSystemPopupQ(2,false);
                }
            }
            break;
            case "userDefinedCheckForTemplate":{
                viewHelper.checkTemplate=params.checkTemplate
                if(widgetList.getVkbRef.visible)widgetList.showHideVKBScreen(false)
            }
            break;
            case "userDefinedBradcastVoyager":{
                viewHelper.isVoyagerBroadcast=params.voyagerBroadcast
            }
            break;
            case "userDefinedMidBlock":{
                core.pif.midAccessChanged(params.statusValue,params.midValue)
            }
            break;
            case "userDefinedplayFromSeat":{
                if(params.isFromSeat){
                    viewHelper.setVodType(viewHelper.getConvertedVodType(params.vodType))
                    //  viewHelper.getSoundtracks(pif.vkarmaToSeat.getPlayingMidInfo()["mid"])
                    //  viewHelper.isFromKarma=false
                }
            }
            break;
            case "userDefinedLangIndex":{
                viewHelper.setLanguageId(params.langIndex)
            }
            break;
            case "userDefinedSoundtrackUpdate":{
                viewHelper.soundtrackId=params.soundtrackIndex
                viewHelper.subtitleId=params.subtitleIndex
            }
            break;
            case "changeLanguage":{
                var iso = params.languageIso
                core.setInteractiveLanguage(dataController.getLidByLanguageISO(iso),iso);
                dataController.refreshModelsByIntLanguage([viewData.mainMenuModel,viewData.subMenuModel,viewData.mediaModel,viewData.tracklistModel,viewData.currentTrackData,viewData.playListModel],['']);
                console.log("change langiuge >>>>>>>>>>>>>>>>>>>>>>>> iso  = "+iso)
                viewHelper.languageUpdate()
                if(viewData.optionMenuModel.getValue(viewHelper.sectionIndexSel,"category_attr_template_id")=="adult")
                    widgetList.setSectionBtnText(configTool.language.global_elements.button_kids_text)
                else
                    widgetList.setSectionBtnText(configTool.language.global_elements.button_mainmenu_text)
                if(viewController.getActiveScreen()=="Welcome"){
                    //viewController.jumpTo(2,"OPTSEL")
                }
            }
            break;
            case "videoStopAck":{
                if(viewHelper.exitCase==2){
                    viewHelper.exitCase = 0;
                    // showVideoScreenTimer.running = true;
                }
                else if(viewHelper.exitCase==3){
                    viewHelper.exitCase = 0;
                    launchAudio()
                }else if(viewHelper.exitCase==4){
                    viewHelper.exitCase = 0;
                    delayTimer.restart()
                }
            }
            break;
            case "userDefinedMenuJump":{
                if(params.iso!="")
                    core.setInteractiveLanguage(dataController.getLidByLanguageISO(params.iso),params.iso)
                viewHelper.sectionSelected=true
                viewHelper.selectedIso=params.iso
                viewHelper.sectionIndexSel=params.sectionIndex
                if(viewController.getActiveScreen()=="Welcome")
                    viewController.jumpTo(2,"OPTSEL")
                else {viewHelper.isFromSeat=true;widgetList.changeSection(params.sectionIndex)}
            }
            break;
            case "userDefinedPlayEpisode":{
                viewHelper.updateTvSeriesIndex(params.playIndex)
            }
            break;
            case "userDefinedKarmaStatus":{
                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedgetSeatStatus")
                var mediaPlayerState=pif.vkarmaToSeat.getCurrentMediaPlayerState();
                if(mediaPlayerState!="stop"){
                    var mediaType = pif.vkarmaToSeat.getMediaType();
                    var a = pif.vkarmaToSeat.getPlayingMidInfo();
                    pif.vkarmaToSeat.sendUserDefinedApi("userDefinedgetPlayingCategory")
                    if(mediaType=="audioAggregate" || mediaType=="audio"){
                        pif.vkarmaToSeat.getAudioPlayerdata()
                    }
                    else if(mediaType=="broadcastAudio"){
                        pif.vkarmaToSeat.getAudioPlayerdata()
                    }
                    else if(mediaType=="mp3"){
                        pif.vkarmaToSeat.getAudioPlayerdata()
                    }

                }
            }
            break;
            case "userDefinedCatPlaying":{
                viewHelper.setPlayingCategory(params.playingCategory)
                if(params.playingCategory=="music"){
                    viewHelper.isFromGenre=params.genre;
                }
            }
            break;


            case "closeAppAck":{//On Game Exit
                if(widgetList.getGamesRef().visible && (pif.launchApp.launchId=="games"))widgetList.showGamesNowPlaying(false)
            }
            break;
            case "userDefinedToggleKeyBoard":{
                widgetList.getVkbRef().toggleKeys(params.toggleKeyBoard)
            }
            break;
            case "userDefinedAspectRatio":{
                viewHelper.disabledStateForAspectRatio=params.aspectRatio
            }
            break;
            /*********************************MUSIC****************************/

            case "playAodAck":{
                viewHelper.playingAudioMid=params.mid
                dataController.getMediaByMid([params.mid],viewData.getCurrentPlayingTrack)
            }
            break;
            /********************************************************************************************************/
            /*
            case "usbConnected":{
                 viewController.hideScreenPopup()
                 if(params.usbConnected==true)
                     viewController.showScreenPopup(16)
                 else
                     viewController.showScreenPopup(24)

            }
            break;
            case "userDefinedDisconnectUsb":{
                viewController.hideScreenPopup()
            }
            break;
            */

            case "userDefinedhideChatFromSeat":{

                if(viewController.getActiveScreenPopupID()==29||viewController.getActiveScreenPopupID()==31)
                    viewController.hideScreenPopup();

            }
            break;

            case "userDefinednewChatFromSeat":{
                viewController.hideScreenPopup();
                if(viewController.getActiveScreen()=="EntOff" ) return;
                if(params.isFromSeat)
                    viewController.showScreenPopup(29);

            }
            break;
            case "userDefinedInviteMultipleInviteSeat":{
                viewController.hideScreenPopup();
                if(viewController.getActiveScreen()=="EntOff" ) return;
                if(params.isFromSeat)
                    viewController.showScreenPopup(31);
            }
            break;
            case "userDefinedUsbHide":{
                if(viewController.getActiveScreenPopupID()==16)
                    viewController.hideScreenPopup()
            }
            break;
            case "userDefinedVKBLimit":{
                viewHelper.vkbCharLimit = params.vkbCharLimit;
                viewHelper.vkbLinesLimit = params.vkbLineLimit;
                viewHelper.vkb_identifier = params.vkb_identifier
            }
            break;
            case "showKeyboard":{
                core.info("PifEvent | showKeyboard Event | params.showKeyboard | "+params.showKeyboard)
                if(params.showKeyboard){
                    widgetList.showHideVKBScreen(true);
                }else{
                    widgetList.showHideVKBScreen(false);
                }
            }
            break;
            case "userDefinedGameExit":{
                widgetList.showGamesNowPlaying(false)
            }
            break;
            case "userDefinedGameLaunch":{
                viewHelper.gameMidToBePlayed=params.midValue;
                widgetList.showGamesNowPlaying(true);
            }
            break;
            case "launchAppAck":{
                console.log("PifEvents | launchAppAck ");
                //                widgetList.showGamesNowPlayingClose()
            }
            break;

            default:
                break;
            }
        }
    }
    /********************************************************************************/
    function launchAudio(){
        viewHelper.getMusicDataToPlay(viewHelper.aodMidToBePlayed);
        var a = viewHelper.musicData;
        var musicData = {mid:a.mid,aggregateMid:a.aggregateMid,mediaRepeat:a.mediaRepeat};
        pif.vkarmaToSeat.playAodByMid(musicData);
    }

    function isVkarmaReady(){return _vKarmaReady}
    /********************************************************************************/
    Timer{
        id:showVideoScreenTimer
        interval: 200
        onTriggered: {
            if((pif.vkarmaToSeat.getMidElapsedTime(viewHelper.synopsisModel.getValue(viewHelper.mediaSelected,"mid"))>viewHelper.resumeSecs))
            {
                return;
            }
            var currentDetails=viewHelper.getCurrentPlayingDetails()
            viewHelper.loadVideoScreen(currentDetails["mid"]);
        }
    }
}
