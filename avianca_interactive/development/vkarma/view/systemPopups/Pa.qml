import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"
import "../screenPopups"

SmartLoader {
    id: paPopupCont
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property bool isBriSlider:false;
    property int popupID:viewController.getActiveSystemPopupID();
    property string configGlobalPopup: (viewHelper.isKids(current.template_id))?"kids_popup_"+orientationType:"popup_"+orientationType
     property string configGlobal: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:"global_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compPAPopup,"paPopup")
    }
    function init(){
        console.log("PA popup.qml | init")
        popupID=viewController.getActiveSystemPopupID()
    }
    function reload(){console.log("PA.qml | reload")}
    function clearOnExit(){
        console.log("PA.qml | clearOn exit")
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    property QtObject paPopup
    Component{
        id:compPAPopup
        Item{
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobalPopup].generic.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            SimpleButton{
                id:closeBtn
                visible:(popupID==3)?true:false
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].settings.close_btn
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].settings.close_btn_press
                onActionReleased: {
                    viewController.updateSystemPopupQ(viewController.getActiveSystemPopupID(),false)
                }
            }
            ViewText{
                id:contentText
                varText: [(popupID==3)?configTool.language.global_elements.power_down_txt:configTool.language.global_elements.pa,configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_exit_title_'+orientationType+'_fontsize')]
                width:qsTranslate('','Popups_pa_text_'+orientationType+'_w')
                height:qsTranslate('','Popups_pa_text_'+orientationType+'_h')
                anchors.centerIn: parent
                anchors.verticalCenterOffset:(popupID==3)?0:(orientationType=="l")?-20:-120;

                lineHeight:qsTranslate('','Popups_pa_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment:Text.AlignVCenter
                wrapMode: Text.WordWrap
            }
            Item{
                id:volBrightCont
                visible: (popupID==3)?false:true
                width:qsTranslate('','Popups_volbri_cont_'+orientationType+'_w')
                height:qsTranslate('','Popups_volbri_cont_'+orientationType+'_h')
                anchors.top:parent.top;
                anchors.topMargin: parseInt(qsTranslate('','Popups_volbri_cont_'+orientationType+'_tm'),10)+20;
                anchors.horizontalCenter: parent.horizontalCenter
                rotation:(orientationType=="p")?0:90
                SimpleButton{
                    id:volumeMinus
                    anchors.bottom: volBrightCont.bottom
                    anchors.horizontalCenter: volBrightCont.horizontalCenter
                    normImg:  viewHelper.configToolImagePath+(isBriSlider?configTag[configGlobal].settings.brightness_decrease:configTag[configGlobal].settings.volume_decrease)
                    pressedImg: viewHelper.configToolImagePath+(isBriSlider?configTag[configGlobal].settings.brightness_decrease_press:configTag[configGlobal].settings.volume_decrease_press)
                    onActionReleased: {
                        core.debug(pif.vkarmaToSeat.getSeatbackVolume())
                        core.debug((core.pif.getMinPAVolume()+core.pif.getVolumeStep()))
                        if(pif.vkarmaToSeat.getSeatbackVolume()>=(core.pif.getMinPAVolume()+core.pif.getVolumeStep()))
                        volumeSliderComp.prevPage()
                    }
                    focus:true;
                    rotation:(orientationType=="p")?0:-90
                }
                Slider{
                    id:volumeSliderComp
                    anchors.centerIn: parent
                    height: volBrightCont.height-(volumeMinus.height+volumePlus.height)
                    width:  volBrightCont.width
                    orientation: Qt.Vertical
                    isReverseSliding:true
                    sliderbg:configTag[configGlobal].settings.slider_base
                    sliderFillingtItems: [configTag[configGlobal].settings.slider_fill,configTag[configGlobal].settings.indicator]
                    stepsValue:(isBriSlider)?pif.getBrightnessStep(): pif.getVolumeStep()
                    isFill: true
                    maxLevel:100
                    offset:core.pif.getMinPAVolume();
                    onValueChanged: {
                        var dragValue=shiftedValue
                        core.debug(" core.pif.getMinPAVolume() : "+core.pif.getMinPAVolume())
                        core.debug(" dragValue : "+dragValue)
                        pif.vkarmaToSeat.setSeatbackVolume(dragValue)

                    }

                }
                SimpleButton{
                    id:volumePlus
                    anchors.top: volBrightCont.top
                    anchors.horizontalCenter: volBrightCont.horizontalCenter
                    normImg:  viewHelper.configToolImagePath+(isBriSlider?configTag[configGlobal].settings.brightness_increase:configTag[configGlobal].settings.volume_increase)
                    pressedImg: viewHelper.configToolImagePath+(isBriSlider?configTag[configGlobal].settings.brightness_increase_press:configTag[configGlobal].settings.volume_increase_press)
                    onActionPressed: {volumeSliderComp.nextPage()}
                    rotation:(orientationType=="p")?0:-90
                }
                Component.onCompleted: {
                    volumeSliderComp.setInitailValue=isBriSlider?pif.vkarmaToSeat.getSeatbackBrightness():pif.vkarmaToSeat.getSeatbackVolume()
                }
            }
        }

    }


}

