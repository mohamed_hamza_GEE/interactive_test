import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: blankPopup
    /**************************************************************************************************************************/
    function load(){
        push(compBlankPopup,'objBlankPopup')
    }
    function init(){
        console.log("BlankPopup popup.qml | init")
    }
    function reload(){console.log("BlankPopup popup.qml | reload")}
    function clearOnExit(){
        console.log("BlankPopup popup.qml | clear on Exit")
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    property QtObject objBlankPopup
    Component{
        id:compBlankPopup
        Rectangle{
            width: core.width
            height: core.height
            color:"transparent"
            MouseArea{
                anchors.fill: parent
                onClicked: {}
            }
        }
    }
}
