// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
Item{
    id:synopisisPanel
    Image{
        /**************************************************************************************************************************/
        width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');height:qsTranslate('','Common_whitepanel_'+orientationType+'_h')
        source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.panel
        anchors.horizontalCenter:parent.horizontalCenter
        anchors.top:parent.top;
        /**************************************************************************************************************************/
        Image{
            id:poster
            anchors.left:parent.left;anchors.leftMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_lm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_tm')
            width:qsTranslate('','Listing_poster_'+orientationType+'_w')
            height:qsTranslate('','Listing_poster_'+orientationType+'_h')
            source:(orientationType=='p')?viewHelper.mediaImagePath+viewHelper.synopsisModel.getValue(index,"synopsisposter_p"):viewHelper.mediaImagePath+viewHelper.synopsisModel.getValue(index,"synopsisposter_l")
            Rectangle{anchors.fill:parent;color:"transparent"}
        }
        ViewText{
            id:titleText
            width:qsTranslate('','Synopsis_title_'+orientationType+'_w')
            color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.title_color
            font.pixelSize: qsTranslate('','Synopsis_title_'+orientationType+'_fontsize')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_title_'+orientationType+'_tm')
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            text:viewHelper.synopsisModel.getValue(index,"title")
            wrapMode: Text.WordWrap
            maximumLineCount:1;


        }
        Item{
            id:descriptionPanel
            width:qsTranslate('','Synopsis_desc_'+orientationType+'_w')
            height:qsTranslate('','Synopsis_desc_'+orientationType+'_h')
            anchors.top:titleText.bottom;
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            clip:true
            Flickable{
                id:movieSyn
                anchors.fill: parent
                interactive: true
                contentHeight:directorTxt.paintedHeight+castTxt.paintedHeight+durationTxt.paintedHeight+descTxt.paintedHeight
                boundsBehavior :Flickable.StopAtBounds
                ViewText{
                    id:directorTxt
                    anchors.left: parent.left;anchors.top: parent.top
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    text:configTool.language.movies.directed_by+viewHelper.synopsisModel.getValue(index,"director")
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                }/*
                ViewText{
                    id:producerTxt
                    anchors.left: parent.left;anchors.top: directorTxt.bottom
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    text:viewHelper.synopsisModel.getValue(index,"short_description")
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                }*/
                ViewText{
                    id:castTxt
                    anchors.left: parent.left;anchors.top: directorTxt.bottom
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    text:configTool.language.movies.starring+viewHelper.synopsisModel.getValue(index,"cast")
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                }
                ViewText{
                    id:durationTxt
                    anchors.left: parent.left;anchors.top: (castTxt.paintedHeight==0)?producerTxt.bottom:castTxt.bottom
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    text:viewHelper.synopsisModel.getValue(index,"duration")+" | "+viewHelper.synopsisModel.getValue(index,"rating_name")+" | "+viewHelper.synopsisModel.getValue(index,"genre")+" | "+viewHelper.synopsisModel.getValue(index,"year")
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                }
                ViewText{
                    id:descTxt
                    anchors.left: parent.left;anchors.top: durationTxt.bottom
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    text:viewHelper.synopsisModel.getValue(index,"description")
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                }
            }
        }
        Image{
            id:fadingImage
            width:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_w')
            anchors.bottom: parent.bottom;anchors.bottomMargin:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_bm')
            anchors.right:parent.right
            visible:(movieSyn.height+movieSyn.contentY<movieSyn.contentHeight)
            source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.fading_image
        }
        BorderImage{
            id:playBtn
            width:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_w'),10)
            height:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_h'),10)
            property bool isPressed: false
            border{
                left: qsTranslate('','Synopsis_button_'+orientationType+'_margin')
                right:qsTranslate('','Synopsis_button_'+orientationType+'_margin')
            }
            anchors.right: parent.right;anchors.rightMargin: qsTranslate('','Synopsis_button_'+orientationType+'_rm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_button_'+orientationType+'_tm')
            source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn
            Text{
                id:playTxt
                color:(playBtn.isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text_press:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text
                height:qsTranslate('','Synopsis_buttontxt_'+orientationType+'_h');width:qsTranslate('','Synopsis_buttontxt_'+orientationType+'_w')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: qsTranslate('','Synopsis_buttontxt_'+orientationType+'_fontsize')
                text:configTool.language.movies.play
                wrapMode: Text.WordWrap
                font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot";
            }
            MouseArea{
                anchors.fill:parent
                onPressed:{
                    playBtn.isPressed=true
                }
                onReleased: {
                    playBtn.isPressed=false
                    var ret = dataController.mediaServices.getMidBlockStatus(viewHelper.synopsisModel.getValue(index,"mid"),viewHelper.synopsisModel.getValue(index,"rating"));
                    if(!ret){
                        var playValue=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id"):viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                        var params=new Object()
                        params["title"]=viewHelper.synopsisModel.getValue(index,"title")
                        params["mid"]=viewHelper.synopsisModel.getValue(index,"mid")
                        params["duration"]=viewHelper.synopsisModel.getValue(index,"duration")
                        params["vodType"]="movies"
                        params["playingCategory"]=playValue
                        params["rating"]=viewHelper.synopsisModel.getValue(index,"rating")
                        viewHelper.settempPlayingDetails(params)
                        viewHelper.isFromKarma=true
                        viewHelper.isFromExitPopup=false
                        viewHelper.isResume=false
                        if(params["mid"]==pif.vkarmaToSeat.getPlayingMidInfo().mid  && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")return;
                        else  if(viewHelper.isVideoPlaying()){
                            //   viewHelper.exitCase=2
                            viewHelper.isFromExitPopup=true
                            viewController.showScreenPopup(2)
                        }else  if((pif.vkarmaToSeat.getMidElapsedTime(viewHelper.synopsisModel.getValue(index,"mid"))>viewHelper.resumeSecs)){
                            viewController.showScreenPopup(20)
                        }else{
                            viewHelper.loadVideoScreen(params["mid"])
                        }
                    }else{
                        console.log("MID is blocked")
                        viewController.showScreenPopup(27)
                    }
                }
            }
        }
        BorderImage{
            id:trailerBtn
            width:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_w'),10)
            height:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_h'),10)
            property bool isPressed: false
            visible: {
                if(viewHelper.synopsisModel.getValue(index,"trailer_mid")==0|| viewHelper.synopsisModel.getValue(index,"trailer_mid")=="")
                    return false
                else return true
            }
            border{
                left: qsTranslate('','Synopsis_button_'+orientationType+'_margin')
                right:qsTranslate('','Synopsis_button_'+orientationType+'_margin')
            }
            anchors.left:(orientationType=='l') ?parent.left:playBtn.left;anchors.leftMargin: (orientationType=='l') ?qsTranslate('','Synopsis_button_'+orientationType+'_lm'):0
            anchors.top:(orientationType=='p') ?playBtn.bottom:playBtn.top;anchors.topMargin: (orientationType=='p') ?qsTranslate('','Synopsis_button_'+orientationType+'_vgap'):0
            source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn
            Text{
                id:trailerTxt
                color:(trailerBtn.isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text_press:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: qsTranslate('','Synopsis_buttontxt_'+orientationType+'_fontsize')
                text:configTool.language.movies.trailer
                font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot";
            }
            MouseArea{
                anchors.fill:parent
                onPressed:{
                    trailerBtn.isPressed=true
                }
                onReleased: {
                    trailerBtn.isPressed=false
                    var ret = dataController.mediaServices.getMidBlockStatus(viewHelper.synopsisModel.getValue(index,"trailer_mid"),viewHelper.synopsisModel.getValue(index,"rating"));
                    if(!ret){

                        var playValue=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id"):viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                        var params=new Object()
                        params["title"]=viewHelper.synopsisModel.getValue(index,"title")
                        params["mid"]=viewHelper.synopsisModel.getValue(index,"trailer_mid")
                        params["parentMid"]=viewHelper.synopsisModel.getValue(index,"mid")
                        params["duration"]=viewHelper.synopsisModel.getValue(index,"trailer_duration")
                        params["vodType"]="trailer"
                        params["playingCategory"]=playValue
                        viewHelper.settempPlayingDetails(params)
                        viewHelper.isFromKarma=true
                        viewHelper.isResume=false
                        viewHelper.isFromExitPopup=false
                        var currentDetails=viewHelper.getCurrentPlayingDetails()
                        viewHelper.isTrackPlayedFromKids=viewHelper.isKids(current.template_id)
                        if(params["mid"]==pif.vkarmaToSeat.getPlayingMidInfo().mid  && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")return;
                        else  if(viewHelper.isVideoPlaying()){
                            viewHelper.isTrailerPressed=true
                            //    viewHelper.exitCase=2
                            viewHelper.isFromExitPopup=true
                            viewController.showScreenPopup(2)
                        }
                        else   viewHelper.loadVideoScreen(params["mid"])
                    }else conosle.log("MID Blocked")
                }
            }
        }
    }
}
