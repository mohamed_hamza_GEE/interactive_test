import Qt 4.7
Image {
    id:iconBtn
    property bool increaseTouchArea: false
    property bool isPressed: false
    property bool isDisable: false
    property string normImg:"";
    property string pressedImg:"";
    property string disableImg:"";
    property alias iconBtn: iconBtn
    property bool isFocusRequired: true
    property double disableOpacity: 0.4
    signal actionPressed
    signal actionReleased
    fillMode: Image.PreserveAspectFit
    source:((isPressed)?pressedImg:normImg)
    width: sourceSize.width<=0?innerImg.sourceSize.width:sourceSize.width
    height: sourceSize.height<=0?innerImg.sourceSize.height:sourceSize.height
    opacity:(isDisable)?disableOpacity:1;
    Image{
        id:innerImg
        anchors.fill: parent
        source:(isPressed)?pressedImg:""
    }
    MouseArea{
        anchors.fill:parent;
        anchors.margins: increaseTouchArea ? -20 : 0
        onPressed: {
            if(isDisable)return;
            if(pressedImg!="")
                isPressed=true
            actionPressed();
        }
        onReleased:{
            if(isDisable)return;
            isPressed=false
            actionReleased()
        }
    }
}
