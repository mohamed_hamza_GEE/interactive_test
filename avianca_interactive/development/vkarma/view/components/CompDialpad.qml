import QtQuick 1.1 
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id:popupImg
    property variant configTag: configTool.config.seatchat_l.screen
    property int itemsPerRow: 3;
    property int maxSeatNoLimit: 4
    property bool isError: false
    property bool isPopup: false
    signal upPressed();
    property string inputTextInvite: ""
    property variant buttonText: []

    function init(){
        dummyInviteModel.clear()
        if(viewController.getActiveScreenPopupID()==31){
            add_button.visible=true;
        }else{
            add_button.visible=false;
        }
        sendInviteIsdisable()
    }

    function clearOnExit(){
        clearAll()
    }
    function sendInviteIsdisable (){
        if(systempopupId==31){
            if(dummyInviteModel.count>0){
                return false
            }else{
                return true
            }
        }else {
            if(textBox.text.length>0)
                return false
            else return true
        }

    }
    function clearAll(){
        isError=false;
        textBox.text="";
    }
    function addToSeatNum(val){
        if(isError)isError=false;
        console.log("textBox.text = "+textBox.text)
        var txt = textBox.text;
        if(txt.length == maxSeatNoLimit) return
        textBox.text += val;

    }
    function deleteNumber(){
        if(isError)isError=false;
        var n = textBox.text
        textBox.text = n.substring(0,n.length-1)
        textBox.cursorPosition = n.length-1
    }
    function sendInvite(){
        core.info("Dialpad | sendInvite | viewHelper.cSessionID = "+viewHelper.cSessionID)
        var a = textBox.text.toUpperCase();
        core.info("Dialpad | sendInvite | core.coreHelper.validateSeatNoForApps(a) = "+core.coreHelper.validateSeatNoForApps(a))
        if(textBox.text!=""  && a != pif.getSeatNumber().toUpperCase() && core.coreHelper.validateSeatNoForApps(a)){
            var seatNo=new Array();
            seatNo[0]=a
            viewHelper.tSeatNo=a
            core.info("Dialpad | before sendInvite | viewHelper.tSeatNo = "+viewHelper.tSeatNo)
            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedsendInvite",{seatNo:seatNo});
            viewController.hideScreenPopup();
        }else{
            isError=true;
        }
    }

    function addInvites(){
        var tempseatno=textBox.text.toUpperCase()
        if(tempseatno== pif.getSeatNumber())return;
        else {
            dummyInviteModel.append({"seatnum":tempseatno});
            console.log("<<<<<<<<<<<< seatnum = "+tempseatno)
            textBox.text=""
        }
    }
    function forMutipleInvite(){
        var tempArr = new Array();
        for(var i = 0; i < dummyInviteModel.count; i++){
            console.debug("____________dummyInviteModel.get(i).seat_________________: "+dummyInviteModel.get(i).seatnum)
            tempArr[i] =(dummyInviteModel.get(i).seatnum).toUpperCase();
            viewHelper.tSeatNo=tempArr
            console.debug("____________tempArr[i]_________________: "+tempArr[i])
        }
        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedsendMutipleInvite",{seatNo:tempArr});
        viewController.hideScreenPopup();
        //        pif.seatChat.sendInvitation(viewHelper.cSessionID,tempArr)
    }


    function checkLocalInviteModel(seatNo){
        core.log(">>>>>>>>>>>>>>>checkLocalInviteModel seatNo = "+seatNo)
        for(var indx = 0 ;indx<dummyInviteModel.count;indx++){
            core.log(">>>>>>>>>>>>>>>checkLocalInviteModel chatSetting.dummyInvite.get(indx).seat = "+dummyInviteModel.get(indx).seatnum)
            if(dummyInviteModel.get(indx).seatnum == seatNo) return true;
        }
        core.log(">>>>>>>>>>>>>>>checkLocalInviteModel seatNo = "+seatNo+" does not exist")
        return false;
    }

    Image{
        id:invite_bg
        width:qsTranslate('','Popups_bg_l_w')
        height:qsTranslate('','Popups_bg_l_h')
        source:viewHelper.configToolImagePath+configTag["bg_panel"]
        MouseArea{
            anchors.fill: parent
            onClicked:{

            }
        }
    }
    SimpleButton{
        id:close_button
        anchors.right: invite_bg.right
        anchors.rightMargin: qsTranslate('','seatchat_closebtn_l_rm')
        anchors.top:invite_bg.top
        anchors.topMargin:qsTranslate('','seatchat_closebtn_l_tm')
        normImg: viewHelper.configToolImagePath+configTag["close_btn"]
        pressedImg: viewHelper.configToolImagePath+configTag["close_btn_pressed" ]
        onActionReleased: {
            viewController.hideScreenPopup()
        }
    }

    ViewText{
        id:invite_Title
        anchors.top: invite_bg.top;
        anchors.left:invite_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_title_l_lm')
        anchors.topMargin:qsTranslate('','seatchat_title_l_tm')
        width:qsTranslate('','seatchat_title_l_w')
        height:qsTranslate('','seatchat_title_l_h')
        //        wrapMode: Text.WordWrap
        elide:Text.ElideRight
        varText:[configTool.language.seatchat.enter_seat_title,configTag["title_text_color"],qsTranslate('',"seatchat_title_l_fontsize")]
    }

    FocusScope{
        id:dialpadContainer
        anchors.left:invite_bg.left;
        anchors.top:invite_bg.top;
        anchors.leftMargin:qsTranslate('','seatchat_dialgrid_l_lm')
        anchors.topMargin:qsTranslate('','seatchat_dialgrid_l_tm');
        width:qsTranslate('','seatchat_dialgrid_l_w');
        height: qsTranslate('','seatchat_dialgrid_l_h');
    }
    //Model to form dialpad
    ListModel{ id:lettePadModel319_321
        ListElement{
            letter:"A"
        }
        ListElement{
            letter:"B"
        }
        ListElement{
            letter:"C"
        }
        ListElement{
            letter:"D"
        }
        ListElement{
            letter:"E"
        }
        ListElement{
            letter:"K"
        }

    }
    ListModel{ id:lettePadModel787;
        ListElement{
            letter:"A"
        }
        ListElement{
            letter:"B"
        }
        ListElement{
            letter:"C"
        }
        ListElement{
            letter:"D"
        }
        ListElement{
            letter:"E"
        }
        ListElement{
            letter:"F"
        }
        ListElement{
            letter:"G"
        }
        ListElement{
            letter:"J"
        }
        ListElement{
            letter:"K"
        }
    }

    ListModel{ id:lettePadModel330;
        ListElement{
            letter:"A"
        }
        ListElement{
            letter:"C"
        }
        ListElement{
            letter:"D"
        }
        ListElement{
            letter:"E"
        }
        ListElement{
            letter:"F"
        }
        ListElement{
            letter:"G"
        }
        ListElement{
            letter:"J"
        }
        ListElement{
            letter:"K"
        }
    }

    //Numpad grid
    GridView{
        id:numberPad;
        focus:true;
        anchors.top:dialpadContainer.top
        anchors.left: dialpadContainer.left
        height: 4*cellHeight;width:3*cellWidth
        cellHeight: qsTranslate('','seatchat_dialgrid_l_cellh');
        cellWidth: qsTranslate('','seatchat_dialgrid_l_cellw');
        clip:true;
        interactive: false;

        function addNumber(index,text){
            if(index==11) { deleteNumber() }
            else{ addToSeatNum(text); }
        }

        Component{
            id:gridDelegate;
            SimpleButton{
                id:padBut
                normImg: viewHelper.configToolImagePath+configTag["dial_btn"]
                pressedImg: viewHelper.configToolImagePath+configTag["dial_btn_press"]
                opacity: (index==9)?0:1;
                onActionReleased: {if(padBut.opacity!=1)return;
                    console.log("index = "+index)
                    numberPad.addNumber(index,number_txt.text)
                }
                Image{
                    id:icon
                    anchors.centerIn: padBut
                    source: (index==11)?viewHelper.configToolImagePath+configTag["backspace_icon"]:""
                }
                Text{
                    id:number_txt
                    //                    anchors.verticalCenter: padBut.verticalCenter
                    anchors.centerIn:padBut
                    //                    width:qsTranslate('','seatchat_dialgrid_l_text_w')
                    //                    font.family:viewHelper.adultFont;
                    font.pixelSize: 10
                    text:(index==11)?"":((index==10)?"0":(index+1))
                    color:"white"
                }
            }

        }

        onCurrentIndexChanged:{
            if(currentIndex == 9) currentIndex = 10;
        }

        Component.onCompleted: {
            numberPad.delegate=gridDelegate;
            numberPad.model=12;
        }
    }

    //letter pad
    GridView{
        id:letterPad;
        focus:true;
        anchors.top:dialpadContainer.top
        anchors.left: numberPad.right
        height: 4*cellHeight;width:3*cellWidth
        cellHeight: qsTranslate('','seatchat_dialgrid_l_cellh');
        cellWidth: qsTranslate('','seatchat_dialgrid_l_cellw');
        clip:true;
        interactive: false;
        function addLetter(index){
//            if(viewHelper.seatLetters!= [] ||viewHelper.seatLetters!= undefined){
//                core.log("addLetter | viewHelper.seatLetters | if exists")
//                addToSeatNum(viewHelper.seat_alphabets.get(index).letter)
//            }else{
                if(viewHelper.isAircraftType787()){
                    addToSeatNum(lettePadModel787.get(index).letter)
                }else  if(viewHelper.isAircraftType330()){
                    addToSeatNum(lettePadModel330.get(index).letter)
                }else {
                    addToSeatNum(lettePadModel319_321.get(index).letter)
                }
//            }
        }

        Component{
            id:gridDelegate1;
            SimpleButton{
                id:padBut
                normImg: viewHelper.configToolImagePath+configTag["dial_btn"]
                pressedImg: viewHelper.configToolImagePath+configTag["dial_btn_press"]
                opacity: (index==9)?0:1;
                onActionReleased: {if(padBut.opacity!=1)return;
                    numberPad.addNumber(index,letter_txt.text)
                }
                Text{
                    id:letter_txt
                    //                    anchors.verticalCenter: padBut.verticalCenter
                    anchors.centerIn:padBut
                    text:letter
                    font.pixelSize: 10
                    color:"white"
                }
            }
        }

        Component.onCompleted: {
//            if(viewHelper.seatLetters!= [] ||viewHelper.seatLetters!= undefined){
//                core.log("Component.onCompleted | viewHelper.seatLetters | if exists")
//                letterPad.model=viewHelper.blankModel
//                letterPad.model=viewHelper.seat_alphabets;
//            }else{
                if(viewHelper.isAircraftType787()){
                    letterPad.model=lettePadModel787;
                }else  if(viewHelper.isAircraftType330()){
                    letterPad.model=lettePadModel330;
                }else{
                    letterPad.model=lettePadModel319_321;
                }
//            }
            letterPad.delegate=gridDelegate1;
        }
    }
    TextInput{
        id:textBox
        width: qsTranslate('','seatchat_input_txt_l_w');
        height:  qsTranslate('','seatchat_input_txt_l_h');
        anchors.topMargin:   qsTranslate('','seatchat_input_txt_l_tm');
        anchors.top:invite_bg.top
        anchors.leftMargin:    qsTranslate('','seatchat_input_txt_l_lm');
        anchors.left: invite_bg.left
        maximumLength: 4
        text:""
        font.pixelSize:qsTranslate('','seatchat_input_txt_l_fontsize')
        color:configTag["input_text_color"]
        onTextChanged: {
            if(textBox.length == maxSeatNoLimit)deleteNumber();
        }
    }



    ViewText{
        id:errorMsg
        anchors.top:invite_bg.top;
        anchors.topMargin: qsTranslate('','seatchat_input_error_l_tm')
        anchors.left:invite_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_input_error_l_lm')
        width:qsTranslate('','seatchat_input_error_l_w')
        height: qsTranslate('','seatchat_input_error_l_h')
        varText: [configTool.language.seatchat.invalid_seat_message,configTag["input_error_text_color"],qsTranslate('',"seatchat_input_error_l_fontsize")]
        font.bold: true
        wrapMode: Text.WordWrap
        maximumLineCount: 2
        visible:isError
    }

    SimpleButton{
        id:add_button
        anchors.topMargin: qsTranslate('','seatchat_input_add_l_tm')
        anchors.top:invite_bg.top
        anchors.left: invite_bg.left
        anchors.leftMargin: qsTranslate('','seatchat_input_add_l_lm')
        normImg:   viewHelper.configToolImagePath+configTag["input_add_btn"];
        pressedImg: viewHelper.configToolImagePath+configTag["input_add_btn_press"];
        visible:(viewController.getActiveScreenPopupID()==31)?true:false
        onVisibleChanged: console.log("0000000000000000000000000000000000000000:"+visible+":"+viewController.getActiveScreenPopupID())

        onActionReleased:{
            var a=textBox.text.toUpperCase()
            console.log(">>>>>>>>>>>>>>>>>>>>>>>> core.coreHelper.validateSeatNoForApps(a) = "+core.coreHelper.validateSeatNoForApps(a)+":"+viewController.getActiveScreenPopupID()+":"+visible)
             if(textBox.text!=""  && a != pif.getSeatNumber().toUpperCase() && core.coreHelper.validateSeatNoForApps(a)){
                console.log(">>>>>>>>>>>>>>>>>>>>>>>>  a = "+a)
               /* if(!checkLocalInviteModel)*/addInvites()          // two check if the same seat exist or not if exists then dont send it again
            }else{
                console.log(">>>>>>>>>>>>>>>>>>>>>>>>  a >> = "+a)
                isError=true
            }
        }
    }

    ListModel{id:dummyInviteModel}

    Item{
        id:inviteee
        width:qsTranslate('','seatchat_conf_cont_l_w')
        height:qsTranslate('','seatchat_conf_cont_l_h')
        anchors.top:invite_bg.top
        anchors.topMargin: qsTranslate('','seatchat_conf_cont_l_tm')
        anchors.left: invite_bg.left
        anchors.leftMargin:qsTranslate('','seatchat_conf_cont_l_lm')

        SimpleButton{
            id:left_arrow
            visible:((dummyInviteModel.count>2 ||  invitelist.contentWidth>invitelist.width) && (invitelist.contentX<(invitelist.contentWidth-invitelist.width)))?true:false
            anchors.right:invitelist.left
            anchors.verticalCenter: invitelist.verticalCenter
            normImg: viewHelper.configToolImagePath+configTag["left_arrow"]
            pressedImg:viewHelper.configToolImagePath+configTag["left_arrow_press"]
            onActionReleased: {
                invitelist.incrementCurrentIndex()
            }

        }
        SimpleButton{
            id:right_arrow
            visible:((invitelist.count>2 ||  invitelist.contentWidth>invitelist.width) && (invitelist.contentX>0))?true:false
            anchors.left: invitelist.right
            anchors.verticalCenter: invitelist.verticalCenter
            normImg: viewHelper.configToolImagePath+configTag["right_arrow"]
            pressedImg:viewHelper.configToolImagePath+configTag["right_arrow_press"]
            onActionReleased: {
                invitelist.decrementCurrentIndex()
            }
        }

        ListView{
            id:invitelist
            width:qsTranslate('','seatchat_conf_cont_l_w')
            height:qsTranslate('','seatchat_conf_cont_l_h')
            boundsBehavior: Flickable.StopAtBounds
            snapMode: ListView.SnapToItem
            interactive: true
            delegate: invite_list_delegate
            model:dummyInviteModel
            orientation :ListView.Horizontal
            visible: (dummyInviteModel.count>0)?true:false
            clip:true
        }
        function removeIndex(curIndex){
            dummyInviteModel.remove(curIndex);
        }

        Component{
            id:invite_list_delegate

            SimpleButton{
                id:invite_button_list
                normImg:  viewHelper.configToolImagePath+configTag["conference_btn"]
                pressedImg: viewHelper.configToolImagePath+configTag["conference_btn_press"]
                onActionReleased: {
                    invitelist.currentIndex=index;
                    inviteee.removeIndex(invitelist.currentIndex)
                }

                ViewText{
                    id:invite_button_text
                    anchors.top:invite_button_list.top
                    anchors.topMargin: qsTranslate('','seatchat_conf_cont_l_text_tm')
                    anchors.horizontalCenter:invite_button_list.horizontalCenter
                    horizontalAlignment:Text.AlignHCenter
                    width:qsTranslate('','seatchat_conf_cont_l_text_w')
                    height: qsTranslate('','seatchat_conf_cont_l_text_h')
                    varText: [seatnum.toUpperCase(), (invite_button_list.pressedImg)?configTag["conference_btn_txt_press"]:configTag["conference_btn_txt"],qsTranslate('',"seatchat_conf_cont_l_fontsize")]
                }
            }
        }
    }


    SimpleButton{    // newchat button
        id:sendInvite_button
        focus: true
        anchors.top: invite_bg.top
        anchors.topMargin: qsTranslate('','seatchat_sendbtn_l_tm')
        anchors.left: invite_bg.left
        anchors.leftMargin:qsTranslate('','seatchat_sendbtn_l_lm')
        normImg: viewHelper.configToolImagePath+configTag["send_btn"]
        pressedImg: viewHelper.configToolImagePath+configTag["send_btn_press"]
        isDisable:sendInviteIsdisable()
        onActionReleased:{
            if(systempopupId==31){
                console.log(" for mutiple invite")
                forMutipleInvite()
            }else{
                console.log(" for single invite")
                sendInvite()
            }
        }
        ViewText {
            id: send_txt
            anchors.centerIn: sendInvite_button
            width:qsTranslate('','seatchat_sendbtn_l_text_w')
            height:qsTranslate('','seatchat_sendbtn_l_text_h')
//            font.pixelSize: qsTranslate('','seatchat_sendbtn_l_fontsize')
//            color:configTag["send_btn_text"]
            elide: Text.ElideRight
            varText: [configTool.language.seatchat.send_invitation,configTag["send_btn_text"],qsTranslate('','seatchat_sendbtn_l_fontsize')]
        }

    }

    Component.onCompleted:{
//        pif.seatChat.getAllusers();
    }

}
