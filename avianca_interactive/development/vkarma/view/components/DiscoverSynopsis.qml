import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"
import "../../framework"
import ImageMask 1.0
Item{
    id:mainCont
    property bool showAppMsg:false

    Connections{
        target:viewData;
        onHospitalitModelReady:{
            if(viewData.hospitalityModel.count==0){
                viewController.showScreenPopup(27);
            }else{
                viewController.loadScreen("hospitality")
            }
        }
    }

    Image{
        width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');height:qsTranslate('','Common_whitepanel_'+orientationType+'_h')
        source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.panel
        anchors.horizontalCenter:parent.horizontalCenter
        anchors.top:parent.top;
        MaskedImage{
            id:maskingImage
            anchors.left:parent.left;anchors.leftMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_lm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_tm')
            width:qsTranslate('','Listing_poster_'+orientationType+'_w')
            height:qsTranslate('','Listing_poster_'+orientationType+'_h')
            source:(orientationType=='p')?viewHelper.assetPath+"post_movtv3_p.png":viewHelper.assetPath+"post_movtv3_l.png"//viewHelper.mediaImagePath+poster_p:viewHelper.mediaImagePath+poster_l
        }
        ViewText{
            id:titleText
            color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.title_color
            font.pixelSize: qsTranslate('','Synopsis_title_'+orientationType+'_fontsize')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_title_'+orientationType+'_tm')
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            text:viewHelper.synopsisModel.getValue(index,"title")
            wrapMode: Text.WordWrap
        }
        Item{
            id:descriptionPanel
            width:qsTranslate('','Synopsis_desc_'+orientationType+'_w')
            height:(errorMsg.visible)?qsTranslate('','Synopsis_desc_'+orientationType+'_h_connect'):qsTranslate('','Synopsis_desc_'+orientationType+'_h')
            anchors.top:titleText.bottom;
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            clip:true
            Flickable{
                id:flickTextArea
                anchors.fill: parent
                interactive: true
                contentHeight: descriptionTxt.paintedHeight
                boundsBehavior :Flickable.StopAtBounds
                visible:(continueBtn.visible || errorMsg.visible)?true:false; //(!viewHelper.showContOnKarma && (viewHelper.checkTemplate!=viewHelper.launchAppVkarma||viewHelper.checkTemplate==""))
                ViewText{
                    id:descriptionTxt
                    anchors.left: parent.left;anchors.top: parent.top
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    text:viewHelper.synopsisModel.getValue(index,"description")
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                }
            }
        }
        Item{
            id:description_seatTxt
            width:qsTranslate('','Synopsis_desc_'+orientationType+'_w')
            height:(errorMsg.visible)?qsTranslate('','Synopsis_desc_'+orientationType+'_h_connect'):qsTranslate('','Synopsis_desc_'+orientationType+'_h')
            anchors.top:titleText.bottom;
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            clip:true
            Flickable{
                id:flickTextArea_seatTxt
                anchors.fill: parent
                interactive: true
                visible:showAppMsg;//(!continueBtn.visible && !errorMsg.visible)
                contentHeight: descriptionTxt.paintedHeight
                boundsBehavior :Flickable.StopAtBounds
                ViewText{
                    id:descriptionTxt_seatTxt
                    anchors.left: parent.left;anchors.top: parent.top
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    text:configTool.language.global_elements.in_seat_monitor
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                }
            }
        }
        Image{
            id:fadingImage
            width:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_w')
            anchors.bottom: descriptionPanel.bottom;
            //anchors.bottomMargin:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_bm')
            anchors.right:parent.right
            visible:(descriptionTxt.paintedHeight>descriptionPanel.height && (flickTextArea.contentY+flickTextArea.height)<flickTextArea.contentHeight)
            source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.fading_image
        }
        BorderImage{
            id:continueBtn
            visible:{
               // if((viewHelper.launchAppVkarma=="airline_tour" && viewHelper.tempCat=="airline_tour") ||( viewHelper.launchAppVkarma=="airline_info" && viewHelper.tempCat=="airline_info")|| (viewHelper.launchAppVkarma=="milesapp"  && viewHelper.tempCat=="milesapp"))return false
                if(showAppMsg)return false;
                if(!errorMsg.visible)return true
                return false
            }
            width:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_w'),10)
            height:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_h'),10)
            property bool isPressed: false
            border{
                left: qsTranslate('','Synopsis_button_'+orientationType+'_margin')
                right:qsTranslate('','Synopsis_button_'+orientationType+'_margin')
            }
            //anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_button_'+orientationType+'_lm')
            anchors.right: parent.right;anchors.leftMargin: qsTranslate('','Synopsis_button_'+orientationType+'_rm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_button_'+orientationType+'_tm')
            source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn
            Text{
                id:continueTxt
                color:(continueBtn.isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text_press:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text
                height:qsTranslate('','Synopsis_buttontxt_'+orientationType+'_h');width:qsTranslate('','Synopsis_buttontxt_'+orientationType+'_w')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: qsTranslate('','Synopsis_buttontxt_'+orientationType+'_fontsize')
                text:configTool.language.global_elements.cont
                wrapMode: Text.WordWrap
            }
            MouseArea{
                anchors.fill:parent
                onPressed:{
                    continueBtn.isPressed=true
                }
                onReleased: {
                    continueBtn.isPressed=false
                    if(dataController.mediaServices.getServiceBlockStatus("svcSeatGateConnect")){
                        viewController.showScreenPopup(27)
                        return;
                    }else if(dataController.mediaServices.getServiceBlockStatus("svcSeatChat")){
                        viewController.showScreenPopup(27)
                        return;
                    }else if(dataController.mediaServices.getServiceBlockStatus("svcSeatHospitality")){
                        viewController.showScreenPopup(27)
                        return;
                    }else if(dataController.mediaServices.getServiceBlockStatus("svcSeatGateConnect")){
                        viewController.showScreenPopup(27)
                        return;
                    }
                    viewHelper.tempCat=viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")
                    if(viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")=='connecting_gate')
                        loadCgModel()
                    else  if(viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")=='hospitality'){
                        var curLangISO=core.settings.languageISO
                        core.debug("Discover.qml submenu curLangISO | core.dataController.shopping.getLanguageModel().count "+core.dataController.hospitality.getLanguageModel().count)

                        if(core.dataController.hospitality.getLanguageModel().count<=1){
                            core.debug("Discover.qml submenu curLangISO | eng >>>>>>>>>>>>"+curLangISO+ "viewHelper.defaultLang = "+viewHelper.defaultLang )
                            core.dataController.hospitality.fetchData(viewHelper.defaultLang,viewData.setHospitalitData)
                        }else{
                            core.debug("Discover.qml submenu curLangISO |%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+curLangISO)
                            core.dataController.hospitality.fetchData(curLangISO,viewData.setHospitalitData)
                        }
                    }else{
                        showAppMsg = false;
                        var catnode = new Object();
                        catnode["pcid"] = viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"cid")
                        catnode["ptid"] =viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                        catnode["cid"] = cid;
                        catnode["tid"] = category_attr_template_id;


                        if(catnode["tid"]=="maps")viewController.showScreenPopup(26)
                        else{
                            pif.vkarmaToSeat.launchCategory(catnode);
                            viewHelper.checkTemplate=category_attr_template_id
                            showAppMsg = true;
                            console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>viewHelper.checkTemplate = "+viewHelper.checkTemplate+"viewHelper.launchAppVkarma = "+viewHelper.launchAppVkarma)
                        }

                    }
                }
            }
        }
        ViewText{
            id:errorMsg
            visible:((!dataController.connectingGate.getCgStatus() && viewHelper.isCgSelected==true && viewHelper.launchAppVkarma=="connecting_gate"))?true:(viewHelper.launchAppVkarma=="survey" && dataController.survey.getSurveyModel().count<=0)?true:false
            anchors.bottom: parent.bottom;anchors.bottomMargin: qsTranslate('','Synopsis_connect_info_'+orientationType+'_bm')
            anchors.horizontalCenter: parent.horizontalCenter
            height:qsTranslate('','Synopsis_connect_info_'+orientationType+'_h')
            width:qsTranslate('','Synopsis_connect_info_'+orientationType+'_w')
            lineHeight:qsTranslate('','Synopsis_connect_info_'+orientationType+'_lh')
            lineHeightMode:Text.FixedHeight
            wrapMode: Text.WordWrap
            varText:[configTool.language.connecting_gate.cg_info_notavailable,configTag["discover_"+orientationType].synopsis.cg_info_text_color,qsTranslate('','Synopsis_connect_info_'+orientationType+'_fontsize')]
        }
    }
}
