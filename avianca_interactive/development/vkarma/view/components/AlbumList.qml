// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
Item{
    id:mediaMenuDel
    property bool isPressed: false
    Image{
        id:posterHolder
        smooth:true
        anchors.centerIn: posterImg
        source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.poster_holder:""
    }
    Image{
        id:posterImg
        width:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_w'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_w'):qsTranslate('','Listing_poster_disc_'+orientationType+'_w')
        height:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_h'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_h'):qsTranslate('','Listing_poster_disc_'+orientationType+'_h')
        anchors.left: parent.left;anchors.leftMargin:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_lm'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_lm'):qsTranslate('','Listing_poster_disc_'+orientationType+'_lm')
        anchors.top:parent.top;anchors.topMargin:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_tm'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_tm'):qsTranslate('','Listing_poster_disc_'+orientationType+'_tm')
        source:(orientationType=='p')?viewHelper.mediaImagePath+musicMediaMenu.lView.model.getValue(index,"poster_p"):viewHelper.mediaImagePath+musicMediaMenu.lView.model.getValue(index,"poster_l")
        Rectangle{anchors.fill:parent;color:"transparent"}
    }
    ViewText{
        id:mediaName
        varText:[musicMediaMenu.lView.model.getValue(index,"title"),(isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].listing.text_color_press:configTag["menu_"+orientationType].main.text_color,qsTranslate('','Listing_text_'+orientationType+'_fontsize')]
        anchors.left:(orientationType=='p')? posterImg.right:undefined;anchors.leftMargin: (orientationType=='p')?qsTranslate('','Listing_text_'+orientationType+'_lm'):0
        anchors.top:(orientationType=='l')?posterImg.bottom:undefined;anchors.topMargin:(orientationType=='l')?qsTranslate('','Listing_text_l_tm'):0
        height:qsTranslate('','Listing_text_'+orientationType+'_h')
        width:qsTranslate('','Listing_text_'+orientationType+'_w')
        anchors.verticalCenter:(orientationType=='p')? posterImg.verticalCenter:undefined
        anchors.horizontalCenter: (orientationType=='l')?posterImg.horizontalCenter:undefined
        verticalAlignment:(orientationType=='p')?Text.AlignVCenter:undefined
        horizontalAlignment: (orientationType=='l')?Text.AlignHCenter:undefined
        elide: Text.ElideRight
        maximumLineCount:2
        wrapMode: Text.WordWrap
        lineHeight:qsTranslate('','Listing_text_'+orientationType+'_lh')
        lineHeightMode:Text.FixedHeight
    }
    Image{
        id:divLine
        anchors.bottom: parent.bottom
        source:(orientationType=='p')?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline:""
    }
    MouseArea{
        anchors.fill:parent
        enabled: !musicMediaMenu.lView.moving
        onEnabledChanged: {
            if(!enabled)isPressed=false
        }
        onPressed: {
            if(enabled)isPressed=true;
        }
        onReleased: {
            isPressed=false
            musicMediaMenu.lView.mediaSelected(index)
        }
    }
    Component.onCompleted: {
        if(musicMediaMenu.lView.currentIndex!=0)musicMediaMenu.lView.currentIndex=0
    }

}

