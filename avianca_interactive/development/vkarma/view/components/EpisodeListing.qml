import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
Item{
    id:tvSeries
    property alias episodeList: episodeList
    property bool listMoving:episodeList.moving
    Image{
        source:viewHelper.configToolImagePath+configTag["tv_"+orientationType].synopsis.panel
        anchors.horizontalCenter:parent.horizontalCenter
        anchors.top:parent.top;
        ViewText{
            id:titleText
            width:qsTranslate('','EpisodeCg_title_'+orientationType+'_w')
            height:qsTranslate('','EpisodeCg_title_'+orientationType+'_h')
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','EpisodeCg_title_'+orientationType+'_lm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','EpisodeCg_title_'+orientationType+'_tm')
            font.pixelSize: qsTranslate('','EpisodeCg_title_'+orientationType+'_fontsize')
            font.weight: Font.Bold
            maximumLineCount:1;
            wrapMode: Text.WordWrap;
            color:configTag["tv_"+orientationType].episode.title_color
            text:(viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")=='connecting_gate')?viewData.mediaModel.getValue(viewHelper.mediaSelected,"title"):viewHelper.tvSeriesTitle
        }
        Image{
            id:divLine
            source:viewHelper.configToolImagePath+configTag["tv_"+orientationType].episode.divline
            anchors.bottom:episodeList.top
            anchors.horizontalCenter: parent.horizontalCenter
        }
        ListView{
            id:episodeList
            width:qsTranslate('','EpisodeCg_container_'+orientationType+'_w')
            height:qsTranslate('','EpisodeCg_container_'+orientationType+'_h')
            anchors.bottom: parent.bottom;anchors.bottomMargin: qsTranslate('','EpisodeCg_container_'+orientationType+'_bm')
            clip:true
            model:(viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")=='connecting_gate')?cgModel:viewHelper.episodeModel
            delegate:(viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")=='connecting_gate')?connectingGateDel:episodeDel
            boundsBehavior: Flickable.StopAtBounds
            orientation:ListView.Vertical
            snapMode: ListView.SnapToItem
            onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
            onMovingChanged: {
                episodeListMoving=true
            }
            onMovementEnded: episodeListMoving=false
        }
        Image{
            id:fadingImage
            width:qsTranslate('','EpisodeCg_fadeimage_'+orientationType+'_w')
            anchors.bottom: episodeList.bottom;anchors.bottomMargin:qsTranslate('','EpisodeCg_fadeimage_'+orientationType+'_bm')
            anchors.right:parent.right
            visible:episodeList.contentY+episodeList.height<episodeList.contentHeight
            source:viewHelper.configToolImagePath+configTag["tv_"+orientationType].synopsis.fading_image
        }
        BorderImage{
            id:playAllBtn
            visible:!currentFlightBtn.visible
            property bool isPressed: false
            width:qsTranslate('','EpisodeCg_button_'+orientationType+'_w')
            anchors.top: parent.top;anchors.topMargin: qsTranslate('','EpisodeCg_button_'+orientationType+'_tm')
            anchors.right: parent.right;anchors.rightMargin: qsTranslate('','EpisodeCg_button_'+orientationType+'_rm')
            source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].episode.play_all_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].episode.play_all

            border{
                left: qsTranslate('','EpisodeCg_button_'+orientationType+'_margin')
                right:  qsTranslate('','EpisodeCg_button_'+orientationType+'_margin')
            }
            Text{
                id:playAllText
                width:qsTranslate('','EpisodeCg_buttontxt_'+orientationType+'_w')
                height:qsTranslate('','EpisodeCg_buttontxt_'+orientationType+'_h')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: qsTranslate('','EpisodeCg_buttontxt_'+orientationType+'_fontsize')
                maximumLineCount:1;
                wrapMode: Text.WordWrap;
                color:(playAllBtn.isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text_press:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text
                text:configTool.language.tv.tv_play_all
            }
            MouseArea{
                anchors.fill:parent
                onPressed:{
                    playAllBtn.isPressed=true
                }
                onReleased:{
                    playAllBtn.isPressed=false
                    var ret = dataController.mediaServices.getMidBlockStatus(viewHelper.synopsisModel.getValue(index,"mid"),viewHelper.synopsisModel.getValue(index,"rating"));
                    if(!ret){
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedTvIndex",{index:0})
                        var playValue=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id"):viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                        var params=new Object()
                        params["aMid"]=viewHelper.synopsisModel.getValue(viewHelper.mediaSelected,"mid")
                        params["title"]=viewHelper.episodeModel.getValue(0,"title")
                        params["mid"]=viewHelper.episodeModel.getValue(0,"mid")
                        params["duration"]=viewHelper.episodeModel.getValue(0,"duration")
                        params["vodType"]="tvseries"
                        params["playingCategory"]=playValue
                        viewHelper.settempPlayingDetails(params)
                        viewHelper.isFromKarma=true
                        viewHelper.isFromExitPopup=false
                        viewHelper.isResume=false
                        if(params["mid"]==pif.vkarmaToSeat.getPlayingMidInfo().mid  && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")return;
                        else   if( viewHelper.isVideoPlaying()){
                            //viewHelper.exitCase=2
                            viewHelper.isFromExitPopup=true
                            viewController.showScreenPopup(2)
                        }else if((pif.vkarmaToSeat.getMidElapsedTime(viewHelper.synopsisModel.getValue(viewHelper.mediaSelected,"mid"))>viewHelper.resumeSecs)){
                            viewController.showScreenPopup(20)
                        }else{
                            viewHelper.loadVideoScreen(params["mid"])
                        }
                    }
                    else{
                        console.log("MID is blocked")
                        viewController.showScreenPopup(27)
                    }
                }
            }
        }
        BorderImage{
            id:currentFlightBtn
            visible:(viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")=='connecting_gate')
            property bool isPressed: false
            width:qsTranslate('','EpisodeCg_button_'+orientationType+'_w')
            anchors.top: parent.top;anchors.topMargin: qsTranslate('','EpisodeCg_button_'+orientationType+'_tm')
            anchors.right: parent.right;anchors.rightMargin: qsTranslate('','EpisodeCg_button_'+orientationType+'_rm')
            source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn
            border{
                left: qsTranslate('','EpisodeCg_button_'+orientationType+'_margin')
                right:  qsTranslate('','EpisodeCg_button_'+orientationType+'_margin')
            }
            Text{
                id:currentFlightText
                width:parent.width
                height:qsTranslate('','EpisodeCg_buttontxt_'+orientationType+'_h')
                anchors.centerIn: parent
                color:(currentFlightBtn.isPressed)?configTag["popup_"+orientationType].generic.btn_text_press:configTag["popup_"+orientationType].generic.btn_text
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: qsTranslate('','EpisodeCg_buttontxt_'+orientationType+'_fontsize')
                wrapMode: Text.WordWrap
                text:configTool.language.connecting_gate.current_flight_txt
            }
            MouseArea{
                anchors.fill: parent
                onPressed: { currentFlightBtn.isPressed=true}
                onReleased: {
                    currentFlightBtn.isPressed=false
                    viewController.showScreenPopup(17)
                }
            }
        }
    }
}
