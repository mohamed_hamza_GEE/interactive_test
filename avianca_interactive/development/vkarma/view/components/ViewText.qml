// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Text {
    property variant varText:[]
    id: viewText
    text: varText[0]?varText[0]:''
    color:varText[1]?""+varText[1]:"transparent"
    font.pixelSize: parseInt(varText[2]?varText[2]:0)
    font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot"
}
