import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
Item{
    id:panel
    Image{
        /**************************************************************************************************************************/
        source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.panel
        anchors.horizontalCenter:parent.horizontalCenter
        anchors.top:parent.top;
        Image{
            id:poster
            anchors.left:parent.left;anchors.leftMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_lm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_tm')
            width:qsTranslate('','Listing_poster_'+orientationType+'_w')
            height:qsTranslate('','Listing_poster_'+orientationType+'_h')
            source:(orientationType=='p')?viewHelper.mediaImagePath+viewHelper.synopsisModel.getValue(index,"synopsisposter_p"):viewHelper.mediaImagePath+viewHelper.synopsisModel.getValue(index,"synopsisposter_l")
            Rectangle{anchors.fill:parent;color:"transparent"}
        }
        ViewText{
            id:titleText
            color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.title_color
            font.pixelSize: qsTranslate('','Synopsis_title_'+orientationType+'_fontsize')
            width:qsTranslate('','Synopsis_title_'+orientationType+'_w')
            height:qsTranslate('','Synopsis_title_'+orientationType+'_h')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_title_'+orientationType+'_tm')
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            text:viewHelper.synopsisModel.getValue(index,"title")
            elide: Text.ElideRight
            maximumLineCount:1;
            wrapMode: Text.WordWrap;
        }
        Item{
            id:descriptionPanel
            width:qsTranslate('','Synopsis_desc_'+orientationType+'_w')
            height:qsTranslate('','Synopsis_desc_'+orientationType+'_h')
            anchors.top:titleText.bottom;
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            clip:true
            Flickable{
                id:tvSyn
                anchors.fill: parent
                interactive: true
                contentHeight: genreTxt.height+durationTxt.height+descTxt.height
                boundsBehavior :Flickable.StopAtBounds
                ViewText{
                    id:genreTxt
                    anchors.left: parent.left;anchors.top: parent.top
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    text:viewHelper.synopsisModel.getValue(index,"genre")
                }
                ViewText{
                    id:durationTxt
                    anchors.left: parent.left;anchors.top: genreTxt.bottom
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.Wrap
                    width:parent.width
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    text:viewHelper.synopsisModel.getValue(index,"rating_name")+" | "+core.coreHelper.convertHMSToMin(viewHelper.synopsisModel.getValue(index,"duration"))+" "+configTool.language.tv.min
                }
                ViewText{
                    id:descTxt
                    anchors.left: parent.left;anchors.top: durationTxt.bottom
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.Wrap
                    width:parent.width
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    text:viewHelper.synopsisModel.getValue(index,"description")
                }
            }
        }
        Image{
            id:fadingImage
            width:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_w')
            anchors.bottom: parent.bottom;anchors.bottomMargin:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_bm')
            anchors.right:parent.right
            visible:(tvSyn.height+tvSyn.contentY<tvSyn.contentHeight)
            source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.fading_image
        }
        BorderImage{
            id:playBtn
            width:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_w'),10)
            height:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_h'),10)
            property bool isPressed: false
            border{
                left: qsTranslate('','Synopsis_button_'+orientationType+'_margin')
                right:qsTranslate('','Synopsis_button_'+orientationType+'_margin')
            }
            //anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_button_'+orientationType+'_lm')
            anchors.right: parent.right;anchors.leftMargin: qsTranslate('','Synopsis_button_'+orientationType+'_rm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_button_'+orientationType+'_tm')
            source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn
            Text{
                id:playTxt
                color:(playBtn.isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text_p:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text
                height:qsTranslate('','Synopsis_buttontxt_'+orientationType+'_h');width:qsTranslate('','Synopsis_buttontxt_'+orientationType+'_w')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: qsTranslate('','Synopsis_buttontxt_'+orientationType+'_fontsize')
                text:configTool.language.movies.play
                wrapMode: Text.WordWrap;
                maximumLineCount:1;
            }
            MouseArea{
                anchors.fill:parent
                onPressed:{
                    playBtn.isPressed=true
                }
                onReleased: {
                    playBtn.isPressed=false
                    var ret = dataController.mediaServices.getMidBlockStatus(viewHelper.synopsisModel.getValue(index,"mid"),viewHelper.synopsisModel.getValue(index,"rating"));
                    if(!ret){
                        var playValue=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id"):viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                        var params=new Object()
                        params["title"]=viewHelper.synopsisModel.getValue(index,"title")
                        params["mid"]=viewHelper.synopsisModel.getValue(index,"mid")
                        params["duration"]=viewHelper.synopsisModel.getValue(index,"duration")
                        params["vodType"]="episode"
                        params["playingCategory"]=playValue
                        viewHelper.settempPlayingDetails(params)
                        viewHelper.isFromKarma=true
                        viewHelper.isFromExitPopup=false
                        viewHelper.isResume=false
                        if(params["mid"]==pif.vkarmaToSeat.getPlayingMidInfo().mid  && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")return;
                        else if(viewHelper.isVideoPlaying()){
                            //   viewHelper.exitCase=2
                            viewHelper.isFromExitPopup=true
                            viewController.showScreenPopup(2)
                        }else if((pif.vkarmaToSeat.getMidElapsedTime(viewHelper.synopsisModel.getValue(index,"mid"))>viewHelper.resumeSecs)){
                            viewController.showScreenPopup(20)
                        }else{
                            viewHelper.loadVideoScreen(params["mid"])
                        }
                    }else{
                        console.log("MID is blocked")
                        viewController.showScreenPopup(27)
                    }
                }
            }
        }
    }
}

