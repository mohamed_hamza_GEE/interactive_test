import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
Item{
    id:mainCont
    Image{
        source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.panel
        anchors.horizontalCenter:parent.horizontalCenter
        anchors.top:parent.top;
        ViewText{
            id:titleText
            anchors.top: parent.top;anchors.topMargin:qsTranslate('','EpisodeCg_synop_title_'+orientationType+'_tm')
            anchors.horizontalCenter: parent.horizontalCenter
            height:qsTranslate('','EpisodeCg_synop_title_'+orientationType+'_h')
            width:qsTranslate('','EpisodeCg_synop_title_'+orientationType+'_w')
            horizontalAlignment: Text.AlignLeft
            wrapMode: Text.WordWrap
            varText: [viewHelper.tvSeriesTitle,configTag[viewHelper.catSelected+"_"+orientationType].synopsis.title_color,qsTranslate('','EpisodeCg_synop_title_'+orientationType+'_fontsize')]
            font.weight: qsTranslate('','EpisodeCg_synop_title_'+orientationType+'_fontweight')
            maximumLineCount:1;

        }
        Rectangle{
            id:scrollText
            width:qsTranslate('','EpisodeCg_synop_desc_'+orientationType+'_w');
            height:qsTranslate('','EpisodeCg_synop_desc_'+orientationType+'_h')
            anchors.top:titleText.bottom;anchors.topMargin: qsTranslate('','EpisodeCg_synop_desc_'+orientationType+'_tm')
            anchors.left: titleText.left;
            color:"transparent"
            clip:true
            Flickable{
                width:parent.width;height:parent.height
                interactive: true
                boundsBehavior :Flickable.StopAtBounds
                anchors.top:scrollText.top
                contentHeight:descriptionText.paintedHeight+episodeTitle.paintedHeight
                ViewText{
                    id:episodeTitle
                    width:parent.width;height:paintedHeight
                    varText: [title,configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color,qsTranslate('','EpisodeCg_synop_desc_'+orientationType+'_fontsize')]
                    lineHeight:qsTranslate('','EpisodeCg_synop_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignLeft
                }
                ViewText{
                    id:descriptionText
                    width:parent.width;height:parent.height
                    anchors.top:episodeTitle.bottom
                    varText: [description,configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color,qsTranslate('','EpisodeCg_synop_desc_'+orientationType+'_fontsize')]
                    lineHeight:qsTranslate('','EpisodeCg_synop_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignLeft
                }
            }
        }
        Image{
            id:fadingImage
            width:parent.width
            anchors.bottom: scrollText.bottom;
            anchors.horizontalCenter:scrollText.horizontalCenter
            visible:(descriptionText.paintedHeight>scrollText.height)
            source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.fading_image
        }
        BorderImage{
            id:playBtn
            width:parseInt(qsTranslate('','EpisodeCg_synop_button_'+orientationType+'_w'),10)
            height:parseInt(qsTranslate('','EpisodeCg_synop_button_'+orientationType+'_h'),10)
            property bool isPressed: false
            border{
                left: qsTranslate('','EpisodeCg_synop_button_'+orientationType+'_margin')
                right:qsTranslate('','EpisodeCg_synop_button_'+orientationType+'_margin')
            }
            //anchors.left: parent.left;
            anchors.right: parent.right;//anchors.leftMargin: qsTranslate('','EpisodeCg_synop_button_'+orientationType+'_rm')
            anchors.bottom:parent.bottom;anchors.topMargin: qsTranslate('','EpisodeCg_synop_button_'+orientationType+'_bm')
            source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn
            Text{
                id:playTxt
                color:(playBtn.isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text_press:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text
                height:qsTranslate('','EpisodeCg_synop_buttontxt_'+orientationType+'_h');width:qsTranslate('','EpisodeCg_synop_buttontxt_'+orientationType+'_h')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: qsTranslate('','EpisodeCg_synop_buttontxt_'+orientationType+'_fontsize')
                text:configTool.language.tv.tv_play
                maximumLineCount:1;
                wrapMode: Text.WordWrap;

            }
            MouseArea{
                anchors.fill:parent
                onPressed:{
                    playBtn.isPressed=true
                }
                onReleased: {
                    playBtn.isPressed=false
                    var ret = dataController.mediaServices.getMidBlockStatus(viewHelper.episodeModel.getValue(index,"mid"),viewHelper.episodeModel.getValue(index,"rating"));
                    if(!ret){
                        var playValue=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id"):viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                        var params=new Object()
                        params["title"]=viewHelper.episodeModel.getValue(index,"title")
                        params["mid"]=viewHelper.episodeModel.getValue(index,"mid")
                        params["duration"]=viewHelper.episodeModel.getValue(index,"duration")
                        params["vodType"]="episode"
                        params["playingCategory"]=playValue
                        viewHelper.settempPlayingDetails(params)
                        viewHelper.isFromKarma=true
                        viewHelper.isFromExitPopup=false
                        viewHelper.isResume=false
                        if(params["mid"]==pif.vkarmaToSeat.getPlayingMidInfo().mid  && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop")return;
                        else   if(viewHelper.isVideoPlaying()){
                            //viewHelper.exitCase=2
                            viewHelper.isFromExitPopup=true
                            viewController.showScreenPopup(2)
                        }else  if((pif.vkarmaToSeat.getMidElapsedTime(viewHelper.episodeModel.getValue(index,"mid"))>viewHelper.resumeSecs)){
                            viewController.showScreenPopup(20)
                        }
                        else{
                            viewHelper.loadVideoScreen(params["mid"])
                        }
                    }else {
                        console.log("MID is blocked")
                        viewController.showScreenPopup(27)
                    }
                }
            }
        }
    }
}
