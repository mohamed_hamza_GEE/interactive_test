import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
Item{
    id:mainCont
    Image{
        source:viewHelper.configToolImagePath+configTag["discover_"+orientationType].synopsis.panel
        anchors.horizontalCenter:parent.horizontalCenter
        anchors.top:parent.top;
        Rectangle{
            id:scrollText
            width:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_w');
            height:parent.height
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_tm')
            anchors.horizontalCenter: parent.horizontalCenter
            color:"transparent"
            clip:true
            Flickable{
                width:parent.width;height:parent.height
                interactive: true
                boundsBehavior :Flickable.StopAtBounds
                anchors.top:scrollText.top
                contentHeight:destinationTxt.paintedHeight+flightName.paintedHeight+flightNumber.paintedHeight+departureDetails.paintedHeight+statusDetails.paintedHeight
                ViewText{
                    id:destinationTxt
                    width:parent.width;height:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h1')
                    varText: [configTool.language.connecting_gate.destination+": "+ cgDetailData[0],configTag["discover_"+orientationType].synopsis.description_color,qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_fontsize')]
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignLeft
                    visible:( cgDetailData[0]!="");
                    lineHeight:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h')
                    lineHeightMode:Text.FixedHeight
                }
                ViewText{
                    id:flightName
                    width:parent.width;height:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h')
                    anchors.top:destinationTxt.bottom;anchors.topMargin: qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_vgap')
                    varText: [ cgDetailData[1],configTag["discover_"+orientationType].synopsis.description_color,qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_fontsize')]
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignLeft
                    visible:( cgDetailData[1]!="" );
                    lineHeight:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h')
                    lineHeightMode:Text.FixedHeight
                }
                ViewText{
                    id:flightNumber
                    width:parent.width;height:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h')
                    anchors.top:flightName.bottom;
                    varText: [configTool.language.connecting_gate.flight_number+": "+ cgDetailData[2],configTag["discover_"+orientationType].synopsis.description_color,qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_fontsize')]
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignLeft
                    visible:(cgDetailData[2]!="");
                    lineHeight:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h')
                    lineHeightMode:Text.FixedHeight
                }
                ViewText{
                    id:departureDetails
                    width:parent.width;height:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h2')
                    anchors.top:flightNumber.bottom;anchors.topMargin: qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_vgap')
                    varText: [configTool.language.connecting_gate.schedule_dept_time+": "+ cgDetailData[3]+"<br>"+configTool.language.connecting_gate.departure_gate+": "+ cgDetailData[4]+"<br>"+configTool.language.connecting_gate.schedule_dept_date+": "+ cgDetailData[5],configTag["discover_"+orientationType].synopsis.description_color,qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_fontsize')]
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignLeft
                    visible:( viewHelper.cgArrivalData[3]!=""&& viewHelper.cgArrivalData[4]!="" && viewHelper.cgArrivalData[5]!="" );
                    lineHeight:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h')
                    lineHeightMode:Text.FixedHeight
                }
                ViewText{
                    id:statusDetails
                    width:parent.width;height:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h')
                    anchors.top:departureDetails.bottom;anchors.topMargin: qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_vgap')
                    varText: [configTool.language.connecting_gate.status+": "+ cgDetailData[6],configTag["discover_"+orientationType].synopsis.description_color,qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_fontsize')]
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignLeft
                    visible:( viewHelper.cgArrivalData[6]!="");
                    lineHeight:qsTranslate('','EpisodeCg_flightinfo_cont_'+orientationType+'_h')
                    lineHeightMode:Text.FixedHeight
                }
            }
        }
    }
}
