import QtQuick 1.0

Item  {

    property variant model:[englishModel_row1,englishModel_row2,englishModel_row3,englishModel_row4]

    ListModel {
        id: englishModel_row1

        ListElement { character: "q"; modifier: 0; key_code: 24; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "w"; modifier: 0; key_code: 25; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "e"; modifier: 0; key_code: 26; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "r"; modifier: 0; key_code: 27; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "t"; modifier: 0; key_code: 28; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "y"; modifier: 0; key_code: 29; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "u"; modifier: 0; key_code: 30; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "i"; modifier: 0; key_code: 31; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "o"; modifier: 0; key_code: 32; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "p"; modifier: 0; key_code: 33; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: ""; modifier: 0; key_code: "0x01000003"; isSpecialKey:0;jsnRef:"common_key";icon:"backspace_icon";}
    }
    ListModel {
        id: englishModel_row2

        ListElement { character: "CAPS"; modifier: 0; key_code: "upperCase"; isSpecialKey:1;jsnRef:"caps";icon:"";}
        ListElement { character: "a"; modifier: 0; key_code: 38; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "s"; modifier: 0; key_code: 39; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "d"; modifier: 0; key_code: 40; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "f"; modifier: 0; key_code: 41; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "g"; modifier: 0; key_code: 42; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "h"; modifier: 0; key_code: 43; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "j"; modifier: 0; key_code: 44; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "k"; modifier: 0; key_code: 45; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "l"; modifier: 0; key_code: 46; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: ""; modifier: 0; key_code: "0x01000004"; isSpecialKey:0;jsnRef:"enter";icon:"";}
    }
    ListModel {
        id: englishModel_row3

        ListElement { character: ""; modifier: "SHIFT"; key_code: "SHIFT"; isSpecialKey:1;jsnRef:"common_key";icon:"shift_icon";}
        ListElement { character: "z"; modifier: 0; key_code: 52; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "x"; modifier: 0; key_code: 53; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "c"; modifier: 0; key_code: 54; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "v"; modifier: 0; key_code: 55; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "b"; modifier: 0; key_code: 56; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "n"; modifier: 0; key_code: 57; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "m"; modifier: 0; key_code: 58; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: ";"; modifier: 1; key_code: 59; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "."; modifier: 0; key_code: 60; isSpecialKey:0;jsnRef:"common_key";icon:"";}
    }
    ListModel {
        id: englishModel_row4

        ListElement { character: "123 #!?"; modifier: "TOGGLE"; key_code:"TOGGLE";isSpecialKey:1;jsnRef:"num";icon:"";}
        ListElement { character: " "; modifier: 0; key_code: 65; isSpecialKey:0;jsnRef:"spacebar";icon:"";}
        ListElement { character: ""; modifier: "dArrow"; key_code: "dArrow"; isSpecialKey:1;jsnRef:"common_key";icon:"key_down_arrow";}
        ListElement { character: ""; modifier: "uArrow"; key_code: "uArrow"; isSpecialKey:1;jsnRef:"common_key";icon:"key_up_arrow";}
        ListElement { character: ""; modifier: "lArrow"; key_code: "lArrow"; isSpecialKey:1;jsnRef:"common_key";icon:"key_left_arrow";}
        ListElement { character: ""; modifier: "rArrow"; key_code: "rArrow"; isSpecialKey:1;jsnRef:"common_key";icon:"key_right_arrow";}
        ListElement { character: ""; modifier: 0; key_code: "hide_key"; isSpecialKey:1;jsnRef:"common_key";icon:"key_colapse";}
    }
}
