import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
Item{
    id:gamesSynopsis
    Image{
        id:gameSyn
        /**************************************************************************************************************************/
        source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.panel
        anchors.horizontalCenter:parent.horizontalCenter
        anchors.top:parent.top;
        Image{
            id:poster
            anchors.left:parent.left;anchors.leftMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_lm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_tm')
            width:qsTranslate('','Listing_poster_'+orientationType+'_w')
            height:qsTranslate('','Listing_poster_'+orientationType+'_h')
            source:(orientationType=='p')?viewHelper.mediaImagePath+viewHelper.synopsisModel.getValue(index,"synopsisposter_p"):viewHelper.mediaImagePath+viewHelper.synopsisModel.getValue(index,"synopsisposter_l")
            Rectangle{anchors.fill:parent;color:"transparent"}
        }
        ViewText{
            id:titleText
            color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.title_color
            font.pixelSize: qsTranslate('','Synopsis_title_'+orientationType+'_fontsize')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_title_'+orientationType+'_tm')
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            text:viewHelper.synopsisModel.getValue(index,"title")
            elide: Text.ElideRight
        }
        Item{
            id:descriptionPanel
            width:qsTranslate('','Synopsis_desc_'+orientationType+'_w')
            height:qsTranslate('','Synopsis_desc_'+orientationType+'_h')
            anchors.top:titleText.bottom;
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            clip:true
            Flickable{
                id:gamesSyn
                anchors.fill: parent
                interactive: true
                contentHeight:descTxt.height
                boundsBehavior :Flickable.StopAtBounds
                ViewText{
                    id:descTxt
                    anchors.left: parent.left;anchors.top: parent.top
                    color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    text:viewHelper.synopsisModel.getValue(index,"description")
                }
            }
        }
        Image{
            id:fadingImage
            width:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_w')
            anchors.bottom: parent.bottom;anchors.bottomMargin:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_bm')
            anchors.right:parent.right
            visible:(gamesSyn.height+gamesSyn.contentY<gamesSyn.contentHeight)
            source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.fading_image
        }
        BorderImage{
            id:playBtn
            width:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_w'),10)
            height:parseInt(qsTranslate('','Synopsis_button_'+orientationType+'_h'),10)
            property bool isPressed: false
            border{
                left: qsTranslate('','Synopsis_button_'+orientationType+'_margin')
                right:qsTranslate('','Synopsis_button_'+orientationType+'_margin')
            }
            //anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_button_'+orientationType+'_lm')
            anchors.right: parent.right;anchors.leftMargin: qsTranslate('','Synopsis_button_'+orientationType+'_rm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_button_'+orientationType+'_tm')
            source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn
            Text{
                id:playTxt
                color:(playBtn.isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text_press:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.btn_text
                height:qsTranslate('','Synopsis_buttontxt_'+orientationType+'_h');width:qsTranslate('','Synopsis_buttontxt_'+orientationType+'_w')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: qsTranslate('','Synopsis_buttontxt_'+orientationType+'_fontsize')
                text:configTool.language.games.play
                wrapMode: Text.WordWrap
            }
            MouseArea{
                anchors.fill:parent
                onPressed:{
                    playBtn.isPressed=true
                }
                onReleased: {
                    playBtn.isPressed=false
                    viewHelper.isFromKarma=true
                    viewHelper.isTrackPlayedFromKids=viewHelper.isKids(current.template_id)
                    var playValue=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id"):viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
                    //widgetList.setPlayingCategory(playValue)
                    viewHelper.setPlayingCategory(playValue)
                    var mid =viewHelper.synopsisModel.getValue(index,"mid")
                    var rating = viewHelper.synopsisModel.getValue(index,"rating");
                    if(rating==""){
                        rating=254
                    }

                    var ret = viewHelper.checkVODGameLaunched();
                    if(ret){
                        viewHelper.exitCase = 4;
                        viewHelper.gameMidToBePlayed = mid;
                        console.log("Game Mid to be played When hit YES :  "+mid);
                        viewController.showScreenPopup(2);
                        return;
                    }
                    else{
                        var ret = dataController.mediaServices.getMidBlockStatus(mid,rating);
                        if(!ret){
                            viewHelper.gameMidToBePlayed = mid;
                            viewHelper.launchGame()
                        }
                        else{
                            core.info("Listing | Sorry, This Game MID is Blocked.");
                            viewController.showScreenPopup(27)
                            return;
                        }
                    }
                   // viewController.showScreenPopup(7)
                   // tempTimer.restart()
                }
            }
        }
    }
    Timer{  // added for loading popup to go off
        id:tempTimer
        running:false
        interval: 1000
        onTriggered:{viewController.hideScreenPopup();widgetList.showGamesNowPlaying(true)}
    }
}
