// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
Item{
    property int lvHeight:0
    property int lvWidth:0
    property int cellWidth: 0
    property int cellHeight: 0
    property bool isSelected: (viewController.getActiveScreenPopupID()==19)?(sountrackccLv.currentIndex==viewHelper.getLanguageId()):(isSoundtrack && viewHelper.soundtrackId==currentIndex)?true:(isSoundtrack==false && viewHelper.subtitleId==currentIndex)?true:false
    width:lvWidth;height: parseInt(lvHeight,10)+2;
    property variant langModel;
    property bool isSoundtrack: false
    property alias currentIndex: sountrackccLv.currentIndex
    property string categoryString:(viewHelper.catSelected=='shopping')?'shop':viewHelper.catSelected
    clip:true
    function nextLang(){sountrackccLv.incrementCurrentIndex();console.log("sountrackccLv cI|nextLang"+sountrackccLv.currentIndex)}
    function prevLang(){sountrackccLv.decrementCurrentIndex();console.log("sountrackccLv cI|prevLang"+sountrackccLv.currentIndex)}
    ListView{
        id:sountrackccLv
        boundsBehavior: Flickable.StopAtBounds
        width:parent.width;height:parent.height
        model:langModel
        interactive: true
        orientation:ListView.Horizontal
        preferredHighlightBegin:0;
        preferredHighlightEnd:width;
        highlightMoveDuration: 10
        highlightRangeMode:ListView.StrictlyEnforceRange;
        delegate:Item{
            width:cellWidth;height:cellHeight
            ViewText{
                anchors.fill:parent
                font.pixelSize: qsTranslate('','SoundtrackLang_title_'+orientationType+'_fontsize')
                color:(isSelected)?configTag[categoryString+"_"+orientationType].sndtrk_cc.language_text_color_sel:configTag[categoryString+"_"+orientationType].sndtrk_cc.language_text_color
                text:(viewController.getActiveScreenPopupID()==19)?name:label_name
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }
}
