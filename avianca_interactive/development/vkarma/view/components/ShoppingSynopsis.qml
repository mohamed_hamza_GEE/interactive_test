// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
Item{
    id:synopisisPanel
    Image{
        /**************************************************************************************************************************/
        width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');height:qsTranslate('','Common_whitepanel_'+orientationType+'_h')
        source:viewHelper.configToolImagePath+configTag["shop_"+orientationType].synopsis.panel
        anchors.horizontalCenter:parent.horizontalCenter
        anchors.top:parent.top;

        Image{
            id:poster
            anchors.left:parent.left;anchors.leftMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_lm')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_poster_'+orientationType+'_tm')
            width:qsTranslate('','Listing_poster_'+orientationType+'_w')
            height:qsTranslate('','Listing_poster_'+orientationType+'_h')
            source:(orientationType=='p')?itemdetails.item_thumb:itemdetails.item_thumb
            Rectangle{anchors.fill:parent;color:"transparent"}
        }
        ViewText{
            id:dollarText
            width:qsTranslate('','Synopsis_shop_price_'+orientationType+'_w')
            height:qsTranslate('','Synopsis_shop_price_'+orientationType+'_h')
            color:configTag["shop_"+orientationType].synopsis.category_txt_color
            font.bold: true
            font.pixelSize: qsTranslate('','Synopsis_shop_price_'+orientationType+'_fontsize')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_shop_price_'+orientationType+'_tm')
            anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Synopsis_shop_price_'+orientationType+'_lm')
            text:currencyCode+"  "+actualPrice
            wrapMode: Text.WordWrap
            maximumLineCount:2;
            lineHeight:parseInt(qsTranslate('','Synopsis_shop_price_'+orientationType+'_lh'),10)
            lineHeightMode: Text.FixedHeight
        }
        ViewText{
            id:tilteText
            width:qsTranslate('','Synopsis_title_'+orientationType+'_w')
            height:qsTranslate('','Synopsis_title_'+orientationType+'_h')
            color:configTag["shop_"+orientationType].synopsis.title_color
            font.pixelSize: qsTranslate('','Synopsis_title_'+orientationType+'_fontsize')
            anchors.top:parent.top;anchors.topMargin: qsTranslate('','Synopsis_title_'+orientationType+'_tm')
            anchors.left: poster.left;anchors.leftMargin: qsTranslate('','Synopsis_title_'+orientationType+'_lm')
            text:itemdetails.title
            wrapMode: Text.WordWrap
            maximumLineCount:1;
            font.bold: true
            elide: Text.ElideRight

//            anchors.bottom: descriptionPanel.top

        }
        ViewText{
            id:subtitleText
            width:qsTranslate('','Synopsis_shop_shorttitle_'+orientationType+'_w')
            height:qsTranslate('','Synopsis_shop_shorttitle_'+orientationType+'_h')
            color:configTag["shop_"+orientationType].synopsis.title_color
            font.pixelSize: qsTranslate('','Synopsis_shop_shorttitle_'+orientationType+'_fontsize')
            anchors.top:tilteText.bottom;anchors.topMargin: qsTranslate('','Synopsis_shop_shorttitle_'+orientationType+'_tm')
            anchors.left: tilteText.left;anchors.leftMargin: qsTranslate('','Synopsis_shop_shorttitle_'+orientationType+'_lm')
            text:itemdetails.sub_title
            wrapMode: Text.Wrap
            maximumLineCount:2;
            lineHeight:parseInt(qsTranslate('','Synopsis_shop_shorttitle_'+orientationType+'_lh'),10)
            lineHeightMode: Text.FixedHeight

        }
        Item{
            id:descriptionPanel
            width:qsTranslate('','Synopsis_desc_'+orientationType+'_w')
            height:qsTranslate('','Synopsis_desc_'+orientationType+'_h_shop')
            anchors.top:subtitleText.bottom;
            anchors.left: tilteText.left;anchors.leftMargin: qsTranslate('','Synopsis_shop_shorttitle_'+orientationType+'_lm')
            clip:true
            Flickable{
                id:descriptionSyn
                anchors.fill: parent
                interactive: true
                contentHeight:description_Txt.paintedHeight
                boundsBehavior :Flickable.StopAtBounds
                ViewText{
                    id:description_Txt
                    color:configTag["shop_"+orientationType].synopsis.description_color
                    font.pixelSize:qsTranslate('','Synopsis_desc_'+orientationType+'_fontsize')
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    width:parent.width
                    text:""+itemdetails.description_long
                    lineHeight:qsTranslate('','Synopsis_desc_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                }
            }
        }
        Image{
            id:fadingImage
            width:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_w')
            anchors.bottom: parent.bottom;//anchors.bottomMargin:qsTranslate('','Synopsis_fadeimage_'+orientationType+'_bm')
            anchors.right:parent.right
            visible:(descriptionSyn.height+descriptionSyn.contentY<descriptionSyn.contentHeight)
            source:viewHelper.configToolImagePath+configTag["shop_"+orientationType].synopsis.fading_image
        }
    }
}
