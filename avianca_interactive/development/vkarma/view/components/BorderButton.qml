// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

BorderImage{
    id:mainImage
    property bool isPressed: false
    property bool isDisable: false
    property string normImg:"";
    property string pressedImg:"";
    property string pressedTxtColor: ""
    property string  normalTextColor: ""
    property int  txtWidth: 0
    property int  txtHeight: 0
    property int  txtSize: 0
    property string txtData: ""
    property double  disabledOpacity:0.4
    signal actionReleased()
    source:(isPressed)?pressedImg:normImg
    opacity:(isDisable)?disabledOpacity:1
    Text{
        id:playTxt
        color:(mainImage.isPressed)?pressedTxtColor:normalTextColor
        height:txtHeight
        width:txtWidth
        anchors.centerIn: parent
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize:txtSize
        font.family:(viewHelper.isKids(current.template_id))?"FS Elliot": "FS Elliot"
        text:txtData
        elide: Text.ElideRight
    }
    MouseArea{
        anchors.fill:parent
        onPressed:{
            if(isDisable)return;
            mainImage.isPressed=true
        }
        onReleased: {
            if(isDisable)return;
            mainImage.isPressed=false
            actionReleased()
        }
    }
}
