import QtQuick 1.0

Item  {

    property variant model:[englishModel_row1,englishModel_row2,englishModel_row3,englishModel_row4]

    ListModel {
        id: englishModel_row1

        ListElement { character: "1"; modifier: 0; key_code: 10; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "2"; modifier: 0; key_code: 11; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "3"; modifier: 0; key_code: 12; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "4"; modifier: 0; key_code: 13; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "5"; modifier: 0; key_code: 14; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "6"; modifier: 0; key_code: 15; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "7"; modifier: 0; key_code: 16; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "8"; modifier: 0; key_code: 17; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "9"; modifier: 0; key_code: 18; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "0"; modifier: 0; key_code: 19; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: ""; modifier: 0; key_code: "0x01000003"; isSpecialKey:0;jsnRef:"common_key";icon:"backspace_icon";}
    }
    ListModel {
        id: englishModel_row2

        ListElement { character: "`"; modifier: 0; key_code: 49; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "|"; modifier: 1; key_code: 51; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "("; modifier: 1; key_code: 18; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: ")"; modifier: 1; key_code: 19; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "%"; modifier: 1; key_code: 14; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "^"; modifier: 1; key_code: 15; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "*"; modifier: 1; key_code: 17; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "/"; modifier: 0; key_code: 61; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "'"; modifier: 0; key_code: 48; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "_"; modifier: 1; key_code: 20; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: ""; modifier: 0; key_code: 36; isSpecialKey:0;jsnRef:"enter";icon:"";}
    }
    ListModel {
        id: englishModel_row3

        ListElement { character: "$"; modifier: 1; key_code: 13; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "£"; modifier: 0; key_code: 130; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "€"; modifier: 0; key_code: 132; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "¥"; modifier: 1; key_code: 131; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "!"; modifier: 1; key_code: 10; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "#"; modifier: 1; key_code: 12; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "="; modifier: 0; key_code: 21; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "+"; modifier: 1; key_code: 21; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "@"; modifier: 0; key_code: 11; isSpecialKey:0;jsnRef:"common_key";icon:"";}
        ListElement { character: "?"; modifier: 1; key_code: 61; isSpecialKey:0;jsnRef:"common_key";icon:"";}
    }
    ListModel {
        id: englishModel_row4

        ListElement { character: "ABC"; modifier: "TOGGLE"; key_code: "TOGGLE";isSpecialKey:1;jsnRef:"num";icon:"";}
        ListElement { character: " "; modifier: 0; key_code: 65; isSpecialKey:0;jsnRef:"spacebar";icon:"";}
        ListElement { character: ""; modifier: "dArrow"; key_code: "dArrow"; isSpecialKey:1;jsnRef:"common_key";icon:"key_down_arrow";}
        ListElement { character: ""; modifier: "uArrow"; key_code: "uArrow"; isSpecialKey:1;jsnRef:"common_key";icon:"key_up_arrow";}
        ListElement { character: ""; modifier: "lArrow"; key_code: "lArrow"; isSpecialKey:1;jsnRef:"common_key";icon:"key_left_arrow";}
        ListElement { character: ""; modifier: "rArrow"; key_code: "rArrow"; isSpecialKey:1;jsnRef:"common_key";icon:"key_right_arrow";}
        ListElement { character: ""; modifier: 0; key_code: "hide_key"; isSpecialKey:1;jsnRef:"common_key";icon:"key_colapse";}
    }
}
