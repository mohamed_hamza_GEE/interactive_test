import QtQuick 1.1
import "../../framework/components"

FocusScope{
    id:thisVKB
    property TextEdit textControl;
    property Component buttonDelegate;
    property variant nav:[-1,-1,-1,-1];
    property variant modelArray:[];
    property variant jsnRef1;
    property bool vkbGenerated:false;
    property string alphabets: "abcdefghijklmnopqrstuvwxyz";
    property bool navigableKeyboard: false
    signal specialKeyPressed(variant keyCode)
    signal perFormAction();
    property bool isCapsON: false;  // used to hold status of CAPS for alphabets
    property int selectedColNumber:0;
    property int selectedRowNumber: 0;
    property variant vkbNormalModel;
    property variant vkbShiftModel;
    property variant currentModel:(toggle)?vkbShiftModel:vkbNormalModel;
    property variant listViewsRef;
    property bool isShiftON: false; // for 1 time toggle
    property bool toggle : false;
    property string langISO: "eng";
    property bool isFocused: activeFocus;
    property bool fromSendKey:false;
    property int pressHoldTimerFlag:0;
    property bool enabled:true;
    Timer{
        id:pressHoldTimer;
        interval: 1500;
        onTriggered: {
            if(pressHoldTimerFlag == 1){
                if(thisVKB.preSendKey(getSelectedValues().key_code))
                    pressHoldTimerFlag = 0;
            }
        }
    }
    function createKeyboard(){
        if(vkbGenerated == true){
            core.log("Notice : Virtual keyboard already created.......");
            return false;
        }
        if(modelArray == [] || buttonDelegate == undefined || jsnRef1.length == 0){
            core.log("Error : Delegate not set or Models not set for keyboard");
            return false;
        }
        vkbNormalModel = modelArray[langISO][0];
        vkbShiftModel = modelArray[langISO][1];
        var i,tmpListView_N,tmpListView_S;
        var tmplistViewsRef;
        var thisRowModel,thisRowHeight,thisRowWidth,thisRowX,thisRowY,thisSpacing;
        var thisStyle1,thisStyle2;
        tmplistViewsRef = new Array();
        for(i=0;i<jsnRef1.length;i++){
            thisStyle1 = jsnRef1[i][0];
            if(jsnRef1[i].length >= 1)
                thisStyle2 = jsnRef1[i][1];
            else
                thisStyle2 = thisStyle1;
            tmpListView_N = Qt.createQmlObject('import QtQuick 1.0; ListView{ id:thisList; objectName:"buttonListView' + (i+1) + '_1"; interactive:false; boundsBehavior:Flickable.StopAtBounds;property int listRowNum:' + i + ';model:vkbNormalModel.model[listRowNum];property variant style:[0,0,0,0];x:style[0];y:style[1];width:style[2];spacing:style[3];}',normalPanel, ("buttonListView" + (i+1) + "_1"));
            tmpListView_N.style = thisStyle1;
            tmpListView_N.orientation = ListView.Horizontal;
            tmpListView_N.delegate = buttonDelegate;
            tmpListView_S = Qt.createQmlObject('import QtQuick 1.0; ListView{ id:thisList; objectName:"buttonListView' + (i+1) + '_2"; interactive:false; boundsBehavior:Flickable.StopAtBounds;property int listRowNum:' + i + ';model:vkbShiftModel.model[listRowNum];property variant style:[0,0,0,0];x:style[0];y:style[1];width:style[2];spacing:style[3];}',shiftPanel, ("buttonListView" + (i+1) + "_2"));
            tmpListView_S.style = thisStyle2;
            tmpListView_S.orientation = ListView.Horizontal;
            tmpListView_S.delegate = buttonDelegate;
            tmplistViewsRef[i] = new Array();
            tmplistViewsRef[i] = [tmpListView_N,tmpListView_S];
        }
        listViewsRef = tmplistViewsRef;
        vkbGenerated = true;
        if(pif.vkb != undefined){
            pif.vkb.selectLanguageVKB(langISO,modelArray[langISO][0],modelArray[langISO][1]);
        }
        else{
            core.log("~~~~~~~~~~~~~~~~ pif.vkb.selectLanguageVKB UNDEFINED ~~~~~~~~~~~~~~~~~~~~~");
        }
        return true;
    }
    function getCapsStatus(){return isCapsON}
    function getShiftStatus(){return isShiftON}
    function getListViewReference(rowNumber){
        if(rowNumber == undefined)
            rowNumber = selectedRowNumber;
        if(toggle == false)
            return listViewsRef[rowNumber][0];
        else
            return listViewsRef[rowNumber][1];
    }
    function shiftPressed(status){
        if(status == undefined) isShiftON = (!isShiftON)
        else isShiftON = status;
        capsPressed(isShiftON);
    }
    function toggleKeys(status){
        if(status == undefined) toggle = (!toggle)
        else toggle = status;

        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedToggleKeyBoard",{toggleKeyBoard:toggle});
    }
    function capsPressed(status){
        if(status == undefined) isCapsON = (!isCapsON)
        else isCapsON = status;
    }
    function getCapsCharacter(character){
        if((alphabets.indexOf(character) != -1) && (isCapsON == true))
            return character.toUpperCase();
        else
            return character;
    }
    function setSelected(row,col){
        if(row != undefined && row != -1)
            selectedRowNumber = row;
        if(col != undefined && col != -1)
            selectedColNumber = col;
    }

    function sendKey(keyCode,character,modifier){
        if(keyCode=="0x01000004" ){
            callPostKeyFn(keyCode,character,modifier)
            return;
        }
        if(isShiftON == true)
            shiftPressed(false);

        if(modifier==1){
            character = character.toUpperCase();
        }
        var k = parseInt(keyCode,10)
            if(keyCode == "0x01000003" ){
            callPostKeyFn(keyCode,character,modifier)
        }else{
            callPostKeyFn(0,character,modifier)
        }

    }
    function getSelectedValues(rowNum,colNum){
        if(rowNum == undefined)
            rowNum = selectedRowNumber;
        if(colNum == undefined)
            colNum = selectedColNumber;
        return currentModel.model[rowNum].get(colNum);
    }

    function click(rowIndex,colIndex){
        if(rowIndex != undefined && rowIndex != -1)
            selectedRowNumber = rowIndex;
        if(colIndex != undefined && colIndex != -1)
            selectedColNumber = colIndex;
        var modifier,keyCode,isSpecial,character;
        isSpecial = getSelectedValues().isSpecialKey;
        modifier = getSelectedValues().modifier;
        keyCode = getSelectedValues().key_code;
        character = getSelectedValues().character;
        if(character!=="" && (!thisVKB.enabled))return;
        if(isSpecial == 1){
            if(fromSendKey)
                fromSendKey = false;
            thisVKB.specialKeyPressed(keyCode);
        }else{
            var pressedChar = currentModel.model[selectedRowNumber].get(selectedColNumber).character;
            if((pressedChar.length == 1) && (alphabets.indexOf(pressedChar) != -1) && (isCapsON == true))
                modifier = 1;
            sendKey(keyCode,character,modifier)
        }
    }
    FocusScope{
        id:normalPanel;
        anchors.fill: parent;
        visible: (!(toggle));
    }
    FocusScope{
        id:shiftPanel;
        anchors.fill: parent;
        visible: toggle;
    }
}
