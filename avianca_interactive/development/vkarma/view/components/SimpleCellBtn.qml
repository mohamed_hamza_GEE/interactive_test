// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
Rectangle {
    property int cellWidth;
    property int cellHeight;
    property string normImg:''
    property string pressedImg:''
    property bool isPressed: false
    property bool isDisable: false
    property double disableOpacity: 0.4
    signal btnReleased()
    color:"transparent"
    width:cellWidth
    height:cellHeight
    Image{
        anchors.verticalCenter: parent.verticalCenter;anchors.horizontalCenter: parent.horizontalCenter
        source:isPressed?pressedImg:normImg
        opacity:(isDisable)?disableOpacity:1;
    }
    MouseArea{
        anchors.fill:parent
        onPressed: {
            if(isDisable)return;
            isPressed=true
        }
        onReleased: {
            if(isDisable)return;
            isPressed=false
            btnReleased()
        }
    }
}
