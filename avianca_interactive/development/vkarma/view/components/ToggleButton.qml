// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

SimpleButton {
    property bool cstate: false;
    property string normImg1:"";
    property string normImg2:"";
    property string pressImg1:"";
    property string pressImg2:"";
    normImg:(cstate)?normImg1:normImg2;
    pressedImg:(cstate)?pressImg1:pressImg2;
}
