// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
Item{
    id:mainCont
    property bool isPressed: false
    ViewText{
        id:artistListViewTxt
        varText: [(viewHelper.tempId=="genre_listing")?genre:artist,(isPressed)?configTag["menu_"+orientationType].submenu.text_color_press:configTag["menu_"+orientationType].submenu.text_color,qsTranslate('','SubMenu_text_'+orientationType+'_fontsize')]
        width:qsTranslate('','SubMenu_text_'+orientationType+'_w')
        height:qsTranslate('','SubMenu_text_'+orientationType+'_h')
        wrapMode: Text.WordWrap
        elide: Text.ElideRight
        maximumLineCount:2
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
    Image{
        id:divLine
        anchors.bottom: parent.bottom
        source:(orientationType=='p')?viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu.divline:""
    }
    MouseArea{
        anchors.fill:parent
        enabled: !musicMediaMenu.lView.moving
        onEnabledChanged: {
            if(!enabled)isPressed=false
        }
        onPressed: {
            if(enabled)isPressed=true;
        }
        onReleased: {
            isPressed=false
            musicMediaMenu.lView.mediaSelected(index)
        }
    }
    Component.onCompleted: {
        if(musicMediaMenu.lView.currentIndex!=0)musicMediaMenu.lView.currentIndex=0
    }
}
