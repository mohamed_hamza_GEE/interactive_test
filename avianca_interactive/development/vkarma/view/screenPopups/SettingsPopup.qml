import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import Panasonic.Pif 1.0

SmartLoader {
    id: settPopup
    /**************************************************************************************************************************/
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property bool isBriSlider:false
    property int dragValue:0
    property variant setValue: 0
    property int initialVal:0;
    property string configGlobal: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:(viewController.getActiveScreen()=="OptionSelection")?"global_"+orientationType:"global_"+orientationType
    onDragValueChanged:{
        setVolBrightness(true);
    }
    onConfigGlobalChanged:{
        core.debug("orientationType : "+orientationType)
        core.debug("configGlobal : "+configGlobal)
        setToggleSource();

    }
    /**************************************************************************************************************************/
    function load(){
        push(compSettingsPopup,"settingsPopup")
    }
    function init(){
        console.log("settingsPopup | init called")
        setTitle()
        setToggleSource()
        settingsPopup.showSaveBtn()
        settingsPopup.showVolBrightContainer()
        settingsPopup.setValue(-1)
        if(viewController.getActiveScreenPopupID()==4){
            isBriSlider=true
            settingsPopup.setValue((viewHelper.isMonitor)?pif.vkarmaToSeat.getSeatbackBrightness():pif.getBrightnessLevel())
        }
        else if(viewController.getActiveScreenPopupID()==3){
            isBriSlider=false
            settingsPopup.setValue(pif.vkarmaToSeat.getSeatbackVolume())
        }
        initialVal=dragValue;
    }
    function clearOnExit(){ core.debug("settingsPopup | clearOnExit called") }
    onLoaded: { core.debug("settingsPopup | onLoaded called") }
    /*****************************************************************************************************************************/
    function setTitle(){
        if(viewController.getActiveScreenPopupID()==3){
            if(viewHelper.isSettings &&viewHelper.isMonitor)
                settingsPopup.setTitleData(configTool.language.karma.monitor_settings+" - "+configTool.language.karma.settings_vol)
            else if(viewHelper.isSettings && !viewHelper.isMonitor)
                settingsPopup.setTitleData(configTool.language.karma.handset_settings+" - "+configTool.language.karma.settings_vol)
            else  settingsPopup.setTitleData(configTool.language.karma.settings_vol)
        }
        else if(viewController.getActiveScreenPopupID()==4){
            if(viewHelper.isSettings &&viewHelper.isMonitor)
                settingsPopup.setTitleData(configTool.language.karma.monitor_settings+" - "+configTool.language.karma.settings_bright)
            else if(viewHelper.isSettings && !viewHelper.isMonitor)
                settingsPopup.setTitleData(configTool.language.karma.handset_settings+" - "+configTool.language.karma.settings_bright)
            else  settingsPopup.setTitleData(configTool.language.karma.settings_bright)
        }
        else if(viewController.getActiveScreenPopupID()==8){
            if(viewHelper.isMonitor)
                settingsPopup.setTitleData(configTool.language.karma.monitor_settings+" - "+configTool.language.settings.call_crew)
            else
                settingsPopup.setTitleData(configTool.language.karma.handset_settings+" - "+configTool.language.settings.call_crew)
        }
        else if(viewController.getActiveScreenPopupID()==9){
            if(viewHelper.isMonitor)
                settingsPopup.setTitleData(configTool.language.karma.monitor_settings+" - "+configTool.language.settings.reading_light)
            else
                settingsPopup.setTitleData(configTool.language.karma.handset_settings+" - "+configTool.language.settings.reading_light)
        }
        else if(viewController.getActiveScreenPopupID()==5){
            if(viewHelper.isMonitor)
                settingsPopup.setTitleData(configTool.language.karma.monitor_settings+" - "+configTool.language.settings.screen_off)
            else
                settingsPopup.setTitleData(configTool.language.karma.handset_settings+" - "+configTool.language.settings.screen_off)
        }
    }

    function performOperation(){
        switch(viewController.getActiveScreenPopupID()){
        case 5: {
            if(blockTimer.running)return;
            blockTimer.restart();
            if(viewHelper.isMonitor){
                viewController.hideScreenPopup(false);
                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedBacklightOff",{});
                //                pif.setBacklightOff();
            }
            else{
                //pif.setBacklightOff();
                //                pif.vkarmaToSeat.toggleBacklight();
                viewController.hideScreenPopup(false);
                pif.setBacklightOff();
            }
            break;
        }
        case 9: {pif.setReadingLight(!pif.getReadingLight());setToggleSource();}break;
        case 8:{pif.setAttendantCall(!pif.getAttendantCall());setToggleSource();}break;
        }
    }
    function setToggleSource(){
        switch(viewController.getActiveScreenPopupID()){
        case 5:{
            settingsPopup.setToggleBtnSource(configTag[configGlobal].settings.screen_off,configTag[configGlobal].settings.screen_off_press)
            break;
        }
        case 8:{
            if(pif.getAttendantCall()){
                settingsPopup.setToggleBtnSource(configTag[configGlobal].settings.call_crew_cancel,configTag[configGlobal].settings.call_crew_cancel_press)
            }else{
                settingsPopup.setToggleBtnSource(configTag[configGlobal].settings.call_crew,configTag[configGlobal].settings.call_crew_press)
            }

            break;
        }
        case 9:{
            if(pif.getReadingLight()){
                settingsPopup.setToggleBtnSource(configTag[configGlobal].settings.reading_light_off,configTag[configGlobal].settings.reading_light_off_press)
            }else{
                settingsPopup.setToggleBtnSource(configTag[configGlobal].settings.reading_light,configTag[configGlobal].settings.reading_light_press)
            }

            break;
        }
        }
    }
    function setVolBrightness(closePressed){
        core.info(">>>>>>>>>> closePressed "+closePressed)
        var volumeLevel=(isBriSlider )?((viewHelper.isMonitor && (!closePressed || closePressed==undefined))?
                                            (pif.vkarmaToSeat.getSeatbackBrightness()):
                                            ((!closePressed || closePressed==undefined)? pif.getBrightnessLevel():"")):
                                        pif.vkarmaToSeat.getSeatbackVolume()
        core.info(">>>>>  pif.vkarmaToSeat.getSeatbackVolume() "+ pif.vkarmaToSeat.getSeatbackVolume())
        core.info(" dragValue : "+dragValue);
        core.info(" volumeLevel : "+volumeLevel);
        /*if(dragValue>volumeLevel){
            if(isBriSlider) {
                if(viewHelper.isMonitor)
                    pif.vkarmaToSeat.setSeatbackBrightnessByStep(1)
                else pif.setBrightnessByStep(1)
            }
            else  pif.vkarmaToSeat.setSeatbackVolumeByStep(1)
        }
        else if(dragValue<volumeLevel){
            if(isBriSlider)  {
                if(viewHelper.isMonitor)
                    pif.vkarmaToSeat.setSeatbackBrightnessByStep(-1)
                else pif.setBrightnessByStep(-1)
            }
            else pif.vkarmaToSeat.setSeatbackVolumeByStep(-1)
        }*/
        if(isBriSlider) {
            if(viewHelper.isMonitor && (!closePressed || closePressed==undefined))
                pif.vkarmaToSeat.setSeatbackBrightness(dragValue)
            else if(!closePressed || closePressed==undefined)
                pif.setBrightnessByValue(dragValue);
        }
        else if(!closePressed || closePressed==undefined)
            pif.vkarmaToSeat.setSeatbackVolume(dragValue)
        core.info(" dragValue : "+dragValue);
    }
    /**************************************************************************************************************************/
    Timer{id:blockTimer;interval:200;}

    property QtObject settingsPopup
    Component{
        id:compSettingsPopup
        Item{
            id:mainCont
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            property alias volumeSliderComp: volumeSliderComp
            property bool changeToggleState: false
            function setValue(val){
                volumeSliderComp.setInitailValue=val
            }
            function setTitleData(val){
                titleText.text=val
            }
            function setToggleBtnSource(norm,pressd){
                changeToggleState=!changeToggleState
                //                toggleBtns.normImg=viewHelper.configToolImagePath+norm
                //                toggleBtns.pressedImg=viewHelper.configToolImagePath+pressd
            }
            function getNormalImg(){
                switch(viewController.getActiveScreenPopupID()){
                case 5:{
                    return configTag[configGlobal].settings.screen_off
                    break;
                }
                case 8:{
                    if(pif.getAttendantCall()){
                        return configTag[configGlobal].settings.call_crew_cancel
                    }else{
                        return configTag[configGlobal].settings.call_crew
                    }

                    break;
                }
                case 9:{
                    if(pif.getReadingLight()){
                        return configTag[configGlobal].settings.reading_light_off
                    }else{
                        return configTag[configGlobal].settings.reading_light
                    }

                    break;
                }
                }
            }
            function getPressedImg(){
                switch(viewController.getActiveScreenPopupID()){
                case 5:{
                    return configTag[configGlobal].settings.screen_off_press
                    break;
                }
                case 8:{
                    if(pif.getAttendantCall()){
                        return configTag[configGlobal].settings.call_crew_cancel_press
                    }else{
                        return configTag[configGlobal].settings.call_crew_press
                    }

                    break;
                }
                case 9:{
                    if(pif.getReadingLight()){
                        return configTag[configGlobal].settings.reading_light_off_press
                    }else{
                        return configTag[configGlobal].settings.reading_light_press
                    }

                    break;
                }
                }
            }
            function showSaveBtn(){
                if(viewController.getActiveScreenPopupID()==3 || viewController.getActiveScreenPopupID()==4)
                    saveBtn.visible=true
                else
                    saveBtn.visible=false
            }
            function showVolBrightContainer(){
                if(viewController.getActiveScreenPopupID()==3 || viewController.getActiveScreenPopupID()==4){
                    volBrightCont.visible=true
                    toggleBtns.visible=false
                }
                else{
                    volBrightCont.visible=false
                    toggleBtns.visible=true
                }
            }
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag["popup_"+orientationType].generic.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            ViewText{
                id:titleText
                varText: ["",configTag["popup_"+orientationType].generic.text_color,qsTranslate('','Popups_volbri_title_'+orientationType+'_fontsize')]
                height:qsTranslate('','Popups_volbri_title_'+orientationType+'_h')
                width:qsTranslate('','Popups_volbri_title_'+orientationType+'_w')
                anchors.left: background.left;anchors.leftMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_lm')
                anchors.top: background.top;anchors.topMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_volbri_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignLeft
                wrapMode:Text.WordWrap
            }
            Rectangle{
                id:volBrightCont
                visible: false
                width:qsTranslate('','Popups_volbri_cont_'+orientationType+'_w')
                height:qsTranslate('','Popups_volbri_cont_'+orientationType+'_h')
                anchors.top:background.top;
                anchors.topMargin: qsTranslate('','Popups_volbri_cont_'+orientationType+'_tm')
                anchors.horizontalCenter: background.horizontalCenter
                color:"transparent"
                rotation:(orientationType=="p")?0:90
                SimpleButton{
                    id:volumeMinus
                    anchors.bottom: volBrightCont.bottom
                    anchors.horizontalCenter: volBrightCont.horizontalCenter
                    normImg:  viewHelper.configToolImagePath+(isBriSlider?configTag[configGlobal].settings.brightness_decrease:configTag[configGlobal].settings.volume_decrease)
                    pressedImg: viewHelper.configToolImagePath+(isBriSlider?configTag[configGlobal].settings.brightness_decrease_press:configTag[configGlobal].settings.volume_decrease_press)
                    onActionReleased: {volumeSliderComp.prevPage()}
                    focus:true;
                    rotation:(orientationType=="p")?0:-90
                }

                SimpleButton{
                    id:volumePlus
                    anchors.top: volBrightCont.top
                    anchors.horizontalCenter: volBrightCont.horizontalCenter
                    normImg:  viewHelper.configToolImagePath+(isBriSlider?configTag[configGlobal].settings.brightness_increase:configTag[configGlobal].settings.volume_increase)
                    pressedImg: viewHelper.configToolImagePath+(isBriSlider?configTag[configGlobal].settings.brightness_increase_press:configTag[configGlobal].settings.volume_increase_press)
                    onActionPressed: {volumeSliderComp.nextPage()}
                    rotation:(orientationType=="p")?0:-90
                }
                Component.onCompleted: {
                    volumeSliderComp.setInitailValue=isBriSlider?pif.vkarmaToSeat.getSeatbackBrightness():pif.vkarmaToSeat.getSeatbackVolume()
                }
            }
            Slider{
                id:volumeSliderComp
                anchors.centerIn: volBrightCont
                height: volBrightCont.height-(volumeMinus.height+volumePlus.height)
                width:  volBrightCont.width
                orientation:Qt.Vertical
                isReverseSliding:true
                rotation:(orientationType=="p")?0:90
                sliderbg:configTag[configGlobal].settings.slider_base
                sliderFillingtItems: [configTag[configGlobal].settings.slider_fill,configTag[configGlobal].settings.indicator]
                stepsValue:(isBriSlider)?pif.getBrightnessStep(): pif.getVolumeStep()
                isFill: true
                visible: volBrightCont.visible
                maxLevel:100
                property bool resetCheck:false
                onValueChanged: {
                    if(!resetCheck)
                        dragValue=shiftedValue
                }
                onRotationChanged: {
                    var tmpValue=dragValue!=0?dragValue:(isBriSlider?pif.vkarmaToSeat.getSeatbackBrightness():pif.vkarmaToSeat.getSeatbackVolume())
                    resetCheck=true
                    setInitailValue=0
                    resetCheck=false
                    setInitailValue=tmpValue
                }
            }
            SimpleButton{
                id:closeBtn
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobal].settings.close_btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobal].settings.close_btn_press
                onActionReleased: {
                    dragValue=initialVal;
                    setVolBrightness(true)
                    viewController.hideScreenPopup()
                }
            }
            SimpleButton{
                id:saveBtn
                visible: false
                anchors.bottom:background.bottom;anchors.topMargin: qsTranslate('','Popups_volbri_btn_'+orientationType+'_bm')
                anchors.right: background.right;
                anchors.rightMargin:qsTranslate('','Popups_volbri_btn_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag["popup_"+orientationType].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag["popup_"+orientationType].generic.btn_press
                ViewText{
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: saveBtn
                    horizontalAlignment: Text.AlignHCenter
                    varText: [configTool.language.karma.save,(saveBtn.isPressed)?configTag["popup_"+orientationType].generic.btn_text_press:configTag["popup_"+orientationType].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                    initialVal=dragValue;
                    setVolBrightness()
                }
            }
            SimpleButton{
                id:toggleBtns
                visible:false
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_seticon_'+orientationType+'_tm')
                anchors.horizontalCenter: background.horizontalCenter
                normImg:mainCont.changeToggleState?viewHelper.configToolImagePath+ mainCont.getNormalImg():viewHelper.configToolImagePath+ mainCont.getNormalImg()
                pressedImg:mainCont.changeToggleState?viewHelper.configToolImagePath+ mainCont.getPressedImg():viewHelper.configToolImagePath+ mainCont.getPressedImg()
                onActionPressed: {
                    isPressed=true
                }
                onActionReleased: {
                    isPressed=false
                    performOperation()
                }
            }
        }
    }
}
