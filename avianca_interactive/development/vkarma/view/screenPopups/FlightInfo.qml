import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: flightInfoCont
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property variant airPortDetails: []
    property variant viewMapsDetails: []
    property variant flightConnectionDetails: []
    property double distanceCovered: 0
    property variant configMenu: (viewHelper.isKids(current.template_id))?"kids_menu_"+orientationType:"menu_"+orientationType
    property string  globalMenu: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:"global_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compFlighInfo,"flightInfo")
    }
    function init(){
        console.log("Flight Info.qml | init")
        viewMapsDetails=[getFlightData(4),getFlightData(5),getFlightData(6)]
        flightConnectionDetails=[getFlightData(7),getFlightData(8),getFlightData(9)]
        distanceCoveredByPlane()
    }
    function reload(){console.log("Flight Info.qml | reload")}
    function clearOnExit(){
        console.log("Flight Info.qml | clearOnExit")
    }
    onLoaded: {}

    Connections{
        target: pif
        onTimeToDestinationChanged:{distanceCoveredByPlane();}
        onTimeSinceTakeOffChanged:{distanceCoveredByPlane();}
        onArrivalTimeChanged: viewMapsDetails=[getFlightData(4),getFlightData(5),getFlightData(6)]
        onTimeAtOriginChanged: viewMapsDetails=[getFlightData(4),getFlightData(5),getFlightData(6)]
        onTimeAtDestinationChanged: viewMapsDetails=[getFlightData(4),getFlightData(5),getFlightData(6)]
        onAltitudeChanged:   flightConnectionDetails=[getFlightData(7),getFlightData(8),getFlightData(9)]
        onGroundSpeedChanged:   flightConnectionDetails=[getFlightData(7),getFlightData(8),getFlightData(9)]
        onOutsideAirTempChanged: flightConnectionDetails=[getFlightData(7),getFlightData(8),getFlightData(9)]
        onSourceIATAChanged: flightInfo.sourcetxt.text=sourceIATA
        onDestinationIATAChanged:   flightInfo.destText.text=destinationIATA
    }


    /**************************************************************************************************************************/
    function getFlightData(param){
        switch (param){
        case 1:{
            //var elapseTime=parseInt(core.pif.getTimeSinceTakeOff(),10);
            var remainingTime=parseInt(core.pif.getTimeToDestination(),10)
            return ''+getTime((remainingTime),2)
        }
        break;
        case 2:return ''+(core.pif.getTimeSinceTakeOff().substring(0,2))+":"+(core.pif.getTimeSinceTakeOff().substring(2,4));break;
        case 3:return ''+getDuration();  break;
        case 4:return getTime(getMinsFromHHMM(core.pif.getTimeAtOrigin()));  break;
        case 5:return getTime(getMinsFromHHMM(core.pif.getArrivalTime())); break;
        case 6:return getTime(getMinsFromHHMM(core.pif.getTimeAtDestination())); break;
        case 7:return core.pif.getAltitude() +" "+ "feet"; break;
        case 8:return core.coreHelper.convertDistance(core.pif.getGroundSpeed(),"mph","knot",2)+" "+"mph"; break;
        case 9:return coreHelper.convertCelsiusToFahrenheit(core.pif.getOutsideAirTemperature())+"ºF"; break;
        }
    }

    function getDuration(){
        var ttd = parseInt(core.pif.getTimeToDestination(),10);     //in MINS
        var timetakeoff = core.pif.getTimeSinceTakeOff();           // in HHMM
        var timesincetakeoffinmins = ((timetakeoff.substring(0,2)*60)*1) + ((timetakeoff.substring(2,4))*1);
        var totaltime = ttd + timesincetakeoffinmins;
        var duration = core.coreHelper.convertSecToHMLeadZero(totaltime*60)
        return duration;

    }
    function distanceCoveredByPlane(){
        airPortDetails=[getFlightData(1),getFlightData(2),getFlightData(3)]
        var travelingImgWidth=parseInt(flightInfo.travellingImage.width,10)
        var planeWidth=parseInt(flightInfo.planeImage.width,10)
        var elapseTime=parseInt(core.pif.getTimeSinceTakeOff(),10)
        var remainingTime=parseInt(core.pif.getTimeToDestination(),10)
        var totaltime=elapseTime+remainingTime
        var percentageForRemainingTime=(elapseTime/totaltime)
        distanceCovered=(percentageForRemainingTime  *  (travelingImgWidth-planeWidth))
    }
    function getTime(mins,format){
        var ret = getHHMMFromMins(mins);
        return padZero(ret[0])+":"+padZero(ret[1])
    }
    function padZero(val){
        if(val<10)
            val = "0"+val;
        return val;
    }
    function getHHMMFromMins(mins){
        var arr = new Array()
        arr[0] = Math.floor(mins/60);
        arr[1] = mins%60;
        return arr
    }
    function getMinsFromHHMM(hhmm){
        var hh = hhmm.substring(0,2);
        var mins = hhmm.substring(2,4);
        return parseInt((hh*60),10) + parseInt(mins,10);
    }
    function setdiscoverTag(){
        viewHelper.selectedIndex=core.coreHelper.searchEntryInModel(viewData.mainMenuModel,"category_attr_template_id","discover")
        viewHelper.catIcon=viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"poster_"+orientationType)
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Image{
            source:viewHelper.configToolImagePath+configTag[configMenu].main.bg
        }
    }
    property QtObject flightInfo
    Component{
        id:compFlighInfo
        Flickable{
            id:flightInfoFS
            interactive: true
            clip:true
            boundsBehavior :Flickable.StopAtBounds
            contentHeight: (parseInt(qsTranslate('','FlightInfo_overlay_'+orientationType+'_vgap'),10)*2)+parseInt(qsTranslate('','FlightInfo_overlay_'+orientationType+'_tm'),10)+parseInt(qsTranslate('','FlightInfo_button_'+orientationType+'_tm'),10)+(parseInt(qsTranslate('','FlightInfo_button_'+orientationType+'_vgap'),10)*2)+closeBtn.height+flightPath.height+airPortMap.height+threeDMap.height+flightConnect.height+timeOverlay.height+srcDestOverlay.height+cgOverlay.height//+parseInt(qsTranslate('','FlightInfo_flightpath_'+orientationType+'_tm'),10)
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            property alias planeImage: planeImage
            property alias travellingImage: travellingImage
            property alias sourcetxt: sourcetxt
            property alias destText: destText
            SimpleButton{
                id:closeBtn
                opacity:0.6
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','FlightInfo_close_'+orientationType+'_tm')
                anchors.right: parent.right;anchors.rightMargin:qsTranslate('','FlightInfo_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[globalMenu].flight_info.close_btn
                pressedImg:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.close_btn_pressed
                onActionReleased: viewController.hideScreenPopup()
            }
            Item{
                id:flightPath
                width:qsTranslate('','FlightInfo_flightpath_'+orientationType+'_w')
                height:qsTranslate('','FlightInfo_flightpath_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','FlightInfo_flightpath_'+orientationType+'_tm')
                anchors.left: parent.left;anchors.leftMargin: qsTranslate('','FlightInfo_flightpath_'+orientationType+'_lm')
                Image{
                    id:leftCircle
                    anchors.left: flightPath.left
                    anchors.verticalCenter: travellingImage.verticalCenter
                    source:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.flight_path_circle_image_fill
                }
                Image{
                    id:travellingImage
                    anchors.centerIn: parent
                    source:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.flight_path_base_image
                    Image{
                        id:fillColor
                        anchors.left: travellingImage.left
                        width: distanceCovered
                        height: parent.height
                        source:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.flight_path_fill_color
                    }
                    Image{
                        id:planeImage
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: fillColor.right
                        source:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.flight_icon
                    }
                }
                Image{
                    id:rightCircle
                    anchors.right: flightPath.right
                    anchors.verticalCenter: travellingImage.verticalCenter
                    source:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.flight_path_circle_image_n
                }
                ViewText{
                    id:sourcetxt
                    anchors.right: flightPath.left
                    anchors.verticalCenter: travellingImage.verticalCenter
                    width: qsTranslate('','FlightInfo_flightpath_txt_'+orientationType+'_w')
                    height:qsTranslate('','FlightInfo_flightpath_txt_'+orientationType+'_h')
                    horizontalAlignment: Text.AlignLeft
                    varText: [pif.getSourceIATA(),configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_flightpath_txt_'+orientationType+'_fontsize')]
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:destText
                    anchors.left: flightPath.right
                    anchors.verticalCenter: travellingImage.verticalCenter
                    horizontalAlignment: Text.AlignRight
                    width: qsTranslate('','FlightInfo_flightpath_txt_'+orientationType+'_w')
                    height:qsTranslate('','FlightInfo_flightpath_txt_'+orientationType+'_h')
                    varText: [pif.getDestinationIATA(),configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_flightpath_txt_'+orientationType+'_fontsize')]
                    elide: Text.ElideRight;
                }
            }
            SimpleButton{
                id:airPortMap
                normImg: viewHelper.configToolImagePath+configTag[globalMenu].flight_info.airport_map_btn
                pressedImg:  viewHelper.configToolImagePath+configTag[globalMenu].flight_info.airport_map_btn_press
                anchors.top:flightPath.bottom;anchors.topMargin: qsTranslate('','FlightInfo_button_'+orientationType+'_tm')
                anchors.left: flightPath.left;anchors.leftMargin: qsTranslate('','FlightInfo_button_'+orientationType+'_lm')
                ViewText{
                    id:airportMapText
                    varText: [configTool.language.global_elements.airport_map,(airPortMap.isPressed)?configTag[globalMenu].flight_info.button_text_press:configTag[globalMenu].flight_info.button_text,qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_w');height:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_h')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_lm')
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignLeft
                    elide: Text.ElideRight;
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                    viewController.showScreenPopup(27)
                    return;
                    if(viewHelper.isKids(current.template_id)){viewHelper.changeSection(true);viewHelper.switchToDiscover(true,"airport_map");return}
                    viewHelper.loadAppScreen("airport_map")
                }
            }
            SimpleButton{
                id:threeDMap
                normImg: viewHelper.configToolImagePath+configTag[globalMenu].flight_info.view_map_btn
                pressedImg:  viewHelper.configToolImagePath+configTag[globalMenu].flight_info.view_map_btn_press
                anchors.top:airPortMap.bottom;anchors.topMargin: qsTranslate('','FlightInfo_button_'+orientationType+'_vgap')
                anchors.left: flightPath.left;anchors.leftMargin: qsTranslate('','FlightInfo_button_'+orientationType+'_lm')
                ViewText{
                    id:threeDMapText
                    varText: [configTool.language.global_elements.view_map,(threeDMap.isPressed)?configTag[globalMenu].flight_info.button_text_press:configTag[globalMenu].flight_info.button_text,qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_w');height:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_h')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_lm')
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignLeft
                    elide: Text.ElideRight;
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                    if(viewHelper.checkForExternalBlock("Voyager"))return
                    //                    if(viewHelper.isKids(current.template_id))viewHelper.changeSection(true)
                    //setdiscoverTag()
                    //                    if(viewHelper.isKids(current.template_id)){/*viewHelper.changeSection(true);*/viewHelper.switchToDiscover(true,"maps");return}
                    viewController.showScreenPopup(26)
                }
            }
            SimpleButton{
                id:flightConnect
                normImg: viewHelper.configToolImagePath+configTag[globalMenu].flight_info.flight_connect_btn
                pressedImg:  viewHelper.configToolImagePath+configTag[globalMenu].flight_info.flight_connect_btn_press
                anchors.top:threeDMap.bottom;anchors.topMargin: qsTranslate('','FlightInfo_button_'+orientationType+'_vgap')
                anchors.left: flightPath.left;anchors.leftMargin: qsTranslate('','FlightInfo_button_'+orientationType+'_lm')
                ViewText{
                    id:flightConnectText
                    varText: [configTool.language.global_elements.flight_connection,(threeDMap.isPressed)?configTag[globalMenu].flight_info.button_text_press:configTag[globalMenu].flight_info.button_text,qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_w');height:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_h')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_buttontxt_'+orientationType+'_lm')
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignLeft
                    elide: Text.ElideRight;
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                    if(viewHelper.checkForExternalBlock("CG"))return
                    var cgStat=dataController.connectingGate.getCgStatus()
                    if(!cgStat){
                        viewController.showScreenPopup(27);
                        return;
                    }
                    /*                    if(dataController.mediaServices.getServiceBlockStatus("svcSeatGateConnect")){
                        viewController.showScreenPopup(27)
                        return;
                    }*/

                    if(viewHelper.isKids(current.template_id))viewHelper.changeSection(true)
                    //                    setdiscoverTag()
                    if(viewHelper.isKids(current.template_id)){viewHelper.changeSection(true);viewHelper.switchToDiscover(true,"connecting_gate");return}
                    viewHelper.loadAppScreen("connecting_gate")
                }
            }
            Image{
                id:timeOverlay
                source:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.overlay_icon1
                anchors.horizontalCenter: flightConnect.horizontalCenter
                anchors.top:flightConnect.bottom;anchors.topMargin: qsTranslate('','FlightInfo_overlay_'+orientationType+'_tm')
                ViewText{
                    id:flightTxt
                    varText: [configTool.language.global_elements.flight_duration,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:parseInt(qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm'),10)
                    anchors.top:parent.top;anchors.topMargin:parseInt(qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm'),10)
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:flightDuration
                    varText: [airPortDetails[2],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    anchors.left:(orientationType=='p')?flightTxt.left:flightTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?flightTxt.bottom:parent.top;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:elapsedTimeTxt
                    varText: [configTool.language.global_elements.flight_elapse_time,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm')
                    anchors.top:flightDuration.bottom;anchors.topMargin: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_vgap')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:elpasedTime
                    varText: [airPortDetails[1],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    anchors.left:(orientationType=='p')?elapsedTimeTxt.left:elapsedTimeTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?elapsedTimeTxt.bottom:flightDuration.bottom;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:remainingTimeTxt
                    varText: [configTool.language.global_elements.flight_remain_time,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm')
                    anchors.top:elpasedTime.bottom;anchors.topMargin: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_vgap')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:remainingTime
                    varText: [airPortDetails[0],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    anchors.left:(orientationType=='p')?remainingTimeTxt.left:remainingTimeTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?remainingTimeTxt.bottom:elpasedTime.bottom;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                    elide: Text.ElideRight;
                }
            }
            Image{
                id:srcDestOverlay
                source:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.overlay_icon2
                anchors.horizontalCenter: flightConnect.horizontalCenter
                anchors.top:timeOverlay.bottom;anchors.topMargin: qsTranslate('','FlightInfo_overlay_'+orientationType+'_vgap')
                ViewText{
                    id:localTimeAtOriginTxt
                    varText: [configTool.language.global_elements.flight_local_time_org,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm')
                    anchors.top:parent.top;anchors.topMargin: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:localTimeAtOrigin
                    varText: [viewMapsDetails[0],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    anchors.left:(orientationType=='p')?localTimeAtOriginTxt.left:localTimeAtOriginTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?localTimeAtOriginTxt.bottom:parent.top;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:estimatedTimeTxt
                    varText: [configTool.language.global_elements.flight_estimate_time,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm')
                    anchors.top:localTimeAtOrigin.bottom;anchors.topMargin: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_vgap')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:estimatedTime
                    varText: [viewMapsDetails[1],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    anchors.left:(orientationType=='p')?estimatedTimeTxt.left:estimatedTimeTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?estimatedTimeTxt.bottom:localTimeAtOrigin.bottom;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:localTimeAtDestTxt
                    varText: [configTool.language.global_elements.flight_local_time_dest,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm')
                    anchors.top:estimatedTime.bottom;anchors.topMargin: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_vgap')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:localTimeAtDest
                    varText: [viewMapsDetails[2],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    anchors.left:(orientationType=='p')?localTimeAtDestTxt.left:localTimeAtDestTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?localTimeAtDestTxt.bottom:estimatedTime.bottom;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                    elide: Text.ElideRight;
                }
            }
            Image{
                id:cgOverlay
                source:viewHelper.configToolImagePath+configTag[globalMenu].flight_info.overlay_icon3
                anchors.horizontalCenter: flightConnect.horizontalCenter
                anchors.top:srcDestOverlay.bottom;anchors.topMargin: qsTranslate('','FlightInfo_overlay_'+orientationType+'_vgap')
                ViewText{
                    id:altitudeTxt
                    varText: [configTool.language.global_elements.altitude,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm')
                    anchors.top:parent.top;anchors.topMargin: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:altitude
                    varText: [flightConnectionDetails[0],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    elide: Text.ElideRight
                    anchors.left:(orientationType=='p')?altitudeTxt.left:altitudeTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?altitudeTxt.bottom:parent.top;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')

                }
                ViewText{
                    id:groundSpeedTxt
                    varText: [configTool.language.global_elements.ground_speed,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm')
                    anchors.top:altitude.bottom;anchors.topMargin: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_vgap')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:groundSpeed
                    varText: [flightConnectionDetails[1],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    elide: Text.ElideRight
                    anchors.left:(orientationType=='p')?groundSpeedTxt.left:groundSpeedTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?groundSpeedTxt.bottom:altitude.bottom;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')

                }
                ViewText{
                    id:outsideTempTxt
                    varText: [configTool.language.global_elements.outside_temp,configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w')
                    anchors.left:parent.left;anchors.leftMargin:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_lm')
                    anchors.top:groundSpeed.bottom;anchors.topMargin: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_vgap')
                    elide: Text.ElideRight;
                }
                ViewText{
                    id:outsideTemp
                    varText: [flightConnectionDetails[2],configTag[globalMenu].flight_info.text_color,qsTranslate('','FlightInfo_infotxt_'+orientationType+'_fontsize')]
                    height: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_h')
                    width:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_w_time')
                    elide: Text.ElideRight
                    anchors.left:(orientationType=='p')?outsideTempTxt.left:outsideTempTxt.right;anchors.leftMargin:(orientationType=='p')?0:qsTranslate('','FlightInfo_infotxt_'+orientationType+'_hgap')
                    anchors.top:(orientationType=='p')?outsideTempTxt.bottom:groundSpeed.bottom;anchors.topMargin:(orientationType=='p')?0: qsTranslate('','FlightInfo_infotxt_'+orientationType+'_tm')
                }
            }
        }
    }
}
