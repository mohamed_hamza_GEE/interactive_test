import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: playlistPopupCont
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string configGlobal: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:"global_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compPlaylistPopup,"playlistPopup")
    }
    function init(){
        console.log("Playlist.qml | init")
        getPlayListText()
        playlistPopup.setScrollTextCont(viewHelper.isPlaylistFull)
    }
    function reload(){console.log("Playlist.qml | reload")}
    function clearOnExit(){
        console.log("Playlist.qml | clear on exit")
    }
    onLoaded: {}
    function getPlayListText(){
        if(viewHelper.playListIndex==-1){
            if(viewHelper.isPlaylistFull)
                playlistPopup.setTitleText(configTool.language.music.playlist1_is_full)
            else playlistPopup.setTitleText(configTool.language.music.playlist1_empty_title)
        }
        else if(viewHelper.playListIndex==-2){
            if(viewHelper.isPlaylistFull)
                playlistPopup.setTitleText(configTool.language.music.playlist2_is_full)
            else playlistPopup.setTitleText(configTool.language.music.playlist2_empty_title)
        }
        else{
            if(viewHelper.isPlaylistFull)
                playlistPopup.setTitleText(configTool.language.music.playlist3_is_full)
            else playlistPopup.setTitleText(configTool.language.music.playlist3_empty_title)
        }
    }
    /**************************************************************************************************************************/
    property QtObject playlistPopup
    Component{
        id:compPlaylistPopup
        Item{
            id:mainCont
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            function setTitleText(val){titleText.text=val}
            function setScrollTextCont(val){
                if(val){
                    scrollTextFull.visible=true
                    scrollTextEmpty.visible=false
                }
                else{
                    scrollTextFull.visible=false
                    scrollTextEmpty.visible=true
                }
            }
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobal].help.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            ViewText{
                id:titleText
                varText: ["",configTag[configGlobal].help.desc_text,qsTranslate('','Popups_volbri_title_'+orientationType+'_fontsize')]
                height:qsTranslate('','Popups_volbri_title_'+orientationType+'_h')
                width:qsTranslate('','Popups_volbri_title_'+orientationType+'_w')
                anchors.left: background.left;anchors.leftMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_lm')
                anchors.top: background.top;anchors.topMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_volbri_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignLeft
                wrapMode:Text.WordWrap
            }
            SimpleButton{
                id:closeBtn
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobal].help.close_btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobal].help.close_btn_press
                onActionReleased: {
                    var retArray=[0]
                    viewController.hideScreenPopup(retArray)
                }
            }
            Image{
                id:upArrow
                visible: (orientationType=='p')
                source:viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu.up_arrow
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_playlist_arrow_'+orientationType+'_tm')
                anchors.right:background.right;anchors.rightMargin: qsTranslate('','Popups_playlist_arrow_'+orientationType+'_rm')
            }
            Image{
                id:downArrow
                visible: (orientationType=='p')
                source:viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu.down_arrow
                anchors.right:background.right;anchors.rightMargin: qsTranslate('','Popups_playlist_arrow_'+orientationType+'_rm')
                anchors.bottom: background.bottom;anchors.bottomMargin:qsTranslate('','Popups_playlist_arrow_'+orientationType+'_bm')
            }
            Rectangle{
                id:scrollTextFull
                width:qsTranslate('','Popups_playlist_text_'+orientationType+'_w');
                height:qsTranslate('','Popups_playlist_text_'+orientationType+'_h')
                anchors.top:titleText.bottom;anchors.topMargin: qsTranslate('','Popups_playlist_text_'+orientationType+'_tm')
                anchors.left:titleText.left
                color:"transparent"
                clip:true
                visible: false
                Flickable{
                    width:parent.width;height:parent.height
                    interactive: true
                    boundsBehavior :Flickable.StopAtBounds
                    anchors.top:scrollTextFull.top
                    contentHeight:descriptionText.paintedHeight
                    ViewText{
                        id:descriptionText
                        width:parent.width;height:parent.height
                        varText: [configTool.language.music.playlist_full_desc,configTag[configGlobal].help.desc_text,qsTranslate('','Popups_playlist_text_'+orientationType+'_fontsize')]
                        lineHeight:qsTranslate('','Popups_playlist_text_'+orientationType+'_lh')
                        lineHeightMode:Text.FixedHeight
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignLeft
                    }
                }
            }
            Rectangle{
                id:scrollTextEmpty
                width:qsTranslate('','Popups_playlist_text_'+orientationType+'_w');
                height:qsTranslate('','Popups_playlist_text_'+orientationType+'_h')
                anchors.top:titleText.bottom;anchors.topMargin: qsTranslate('','Popups_playlist_text_'+orientationType+'_tm')
                anchors.left:titleText.left
                color:"transparent"
                clip:true
                visible: false
                Flickable{
                    width:parent.width;height:parent.height
                    interactive: true
                    boundsBehavior :Flickable.StopAtBounds
                    anchors.top:scrollTextEmpty.top
                    contentHeight:descriptionText.paintedHeight
                    ViewText{
                        id:descriptionTextempty
                        width:parent.width;height:parent.height
                        varText: [configTool.language.music.playlist_empty_desc,configTag[configGlobal].help.desc_text,qsTranslate('','Popups_playlist_text_'+orientationType+'_fontsize')]
                        lineHeight:qsTranslate('','Popups_playlist_text_'+orientationType+'_lh')
                        lineHeightMode:Text.FixedHeight
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignLeft
                    }
                }
            }
        }
    }
}
