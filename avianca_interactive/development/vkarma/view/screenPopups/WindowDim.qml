import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: windowDimPopup
    property variant configTag:(viewHelper.isKids(current.template_id))? configTool.config["kids_popup_"+orientationType]: configTool.config["popup_"+orientationType]
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property variant wdLabelTag:core.configTool.language.window_dimming

    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background");
        push(compControls,"controls");
    }
    function init(){}
    function reload(){}
    function clearOnExit(){}

    function setDimLevel(val){    
        console.log('WindowDim.qml | dim level changed |dim Level changed by : '+val);
       pif.vkarmaToSeat.sendUserDefinedApi("userDefinedSetDim",{chVal:val});

    }

    property QtObject background
    Component{
        id:compBackground
        Rectangle{
            id:background
            width:parent.width;
            height:parent.height
            color:configTag['windowdim_'+orientationType].bg_color
            MouseArea{
                anchors.fill: parent
                onClicked:{}
            }

            ViewText{
                id:titleTxt
                width:qsTranslate('','Popups_dim_title_'+orientationType+'_w')
                height:qsTranslate('','Popups_dim_title_'+orientationType+'_h')
                anchors.top: parent.top;
                anchors.topMargin: qsTranslate('','Popups_dim_title_'+orientationType+'_tm')
                anchors.left: parent.left
                anchors.leftMargin:qsTranslate('','Popups_dim_title_'+orientationType+'_lm')
                lineHeight: qsTranslate('','Popups_dim_title_'+orientationType+'_lh');
                lineHeightMode: Text.FixedHeight;
                maximumLineCount: 2;
                varText:[wdLabelTag["windowdimming_title"],configTag['windowdim_'+orientationType].title_text_color,qsTranslate('','Popups_dim_title_'+orientationType+'_fontsize')]
                wrapMode: Text.WordWrap;

            }
            ViewText{
                id:descTxt
                width:qsTranslate('','Popups_dim_desc_'+orientationType+'_w');
                height:qsTranslate('','Popups_dim_desc_'+orientationType+'_h');
                anchors.top:titleTxt.bottom;
                anchors.topMargin: qsTranslate('','Popups_dim_desc_'+orientationType+'_tm');
                anchors.left: titleTxt.left;
                lineHeight: qsTranslate('','Popups_dim_desc_'+orientationType+'_lh');
                lineHeightMode: Text.FixedHeight;
                maximumLineCount: 5;
                wrapMode: Text.Wrap;
                varText:[wdLabelTag["windowdimming_description"] ,configTag['windowdim_'+orientationType].desc_text_color,qsTranslate('','Popups_dim_desc_'+orientationType+'_fontsize')]
            }

            SimpleButton{
                id:closeBtn
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+ configTag['windowdim_'+orientationType].close_btn
                pressedImg: viewHelper.configToolImagePath+ configTag['windowdim_'+orientationType].close_btn_press
                onActionReleased: {
                    viewController.hideScreenPopup()
                }
            }

        }
    }

    property QtObject controls
    Component{
        id:compControls
        Item{
            id:controlls
            width: controllBG.width;
            height:controllBG.height;
            anchors.horizontalCenter: parent.horizontalCenter;
            anchors.bottom: parent.bottom;
            anchors.bottomMargin: qsTranslate('','Popups_dim_button_'+orientationType+'_bm')

            Image{
                id:controllBG
                source:viewHelper.configToolImagePath+configTag['windowdim_'+orientationType].btn_bg;
            }

            SimpleButton{
                id:dimMinus;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left;
                anchors.leftMargin:qsTranslate('','Popups_dim_button_'+orientationType+'_gap');
                normImg:viewHelper.configToolImagePath+configTag['windowdim_'+orientationType].minus;
                pressedImg: viewHelper.configToolImagePath+configTag['windowdim_'+orientationType].minus_press;
                onActionReleased: {
                        setDimLevel(-1);
                }
            }

            SimpleButton{
                id:dimPlus;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.right: parent.right;
                anchors.rightMargin:qsTranslate('','Popups_dim_button_'+orientationType+'_gap');
                normImg:viewHelper.configToolImagePath+configTag['windowdim_'+orientationType].plus;
                pressedImg: viewHelper.configToolImagePath+configTag['windowdim_'+orientationType].plus_press;
                onActionReleased: {                 
                        setDimLevel(1);               
                }
            }
        }
    }
}
