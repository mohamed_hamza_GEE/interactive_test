import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: usbConnectCont
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string configGlobalPopup: (viewHelper.isKids(current.template_id))?"kids_popup_"+orientationType:"popup_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compUsbConnect,"usbConnect")
    }
    function init(){
        console.log("Usb Connect.qml | init")
        getTitle()
        getButton()
    }
    function reload(){console.log("Usb Connect.qml | reload")}
    function clearOnExit(){
        console.log("Usb Connect.qml | clearOnExit")
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    function getTitle(){
        if(viewController.getActiveScreenPopupID()==23)
            usbConnect.setTitleText(configTool.language.usb.connect)
        else if(viewController.getActiveScreenPopupID()==24)
            usbConnect.setTitleText(configTool.language.usb.disconnected)
        else if(viewController.getActiveScreenPopupID()==26)
            usbConnect.setTitleText(configTool.language.karma.launch_map)
        else
            usbConnect.setTitleText(configTool.language.usb.connected)
    }
    function getButton(){
        if(viewController.getActiveScreenPopupID()==23 ||viewController.getActiveScreenPopupID()==24)
            usbConnect.showusbConnectBtn(false)
        else
            usbConnect.showusbConnectBtn(true)
    }
    /**************************************************************************************************************************/
    property QtObject usbConnect
    Component{
        id:compUsbConnect
        Item{
            id:mainCont
            function setTitleText(val){
                titleText.text=val
            }
            function showusbConnectBtn(val){
                cancelBtn.visible=val
                continueBtn.visible=val
                okBtn.visible=!val
            }
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobalPopup].generic.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            ViewText{
                id:titleText
                varText: ["",configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_exit_title_'+orientationType+'_fontsize')]
                width:qsTranslate('','Popups_exit_title_'+orientationType+'_w')
                height:qsTranslate('','Popups_exit_title_'+orientationType+'_h')
                anchors.horizontalCenter:background.horizontalCenter
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_exit_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_exit_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
            SimpleButton{
                id:cancelBtn
                visible:false
                anchors.bottom: background.bottom;anchors.bottomMargin: qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.right:background.right;anchors.rightMargin:qsTranslate('','Popups_exit_button_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn_press
                ViewText{
                    id:cancelBtnText
                    varText: [(viewController.getActiveScreenPopupID()==26)?configTool.language.karma.launch_handset:configTool.language.global_elements.cancel,(cancelBtn.isPressed)?configTag[configGlobalPopup].generic.btn_text_press:configTag[configGlobalPopup].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    if(viewController.getActiveScreenPopupID()==16){
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedCancelUsb")
                    }
                    else if(viewController.getActiveScreenPopupID()==26){
                        viewController.hideScreenPopup()
                        if(viewHelper.checkForExternalBlock("Voyager"))return
                        viewHelper.imagegrabber.start();
                        widgetList.setMapStatus(true);
                    }

                }
            }
            SimpleButton{
                id:continueBtn
                visible:false
                anchors.bottom:(orientationType=='p')?cancelBtn.top:background.bottom;anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Popups_exit_button_'+orientationType+'_vgap'):qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.right:(orientationType=='l')?cancelBtn.left:background.right;anchors.rightMargin:(orientationType=='l')?qsTranslate('','Popups_exit_button_'+orientationType+'_hgap'):qsTranslate('','Popups_exit_button_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn_press
                ViewText{
                    id:continueBtnTxt
                    varText: [(viewController.getActiveScreenPopupID()==26)?configTool.language.karma.launch_seat:configTool.language.global_elements.cont,(continueBtn.isPressed)?configTag[configGlobalPopup].generic.btn_text_press:configTag[configGlobalPopup].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    viewHelper.fromthreeDMaps=false
                    if(viewController.getActiveScreenPopupID()==26){
                        // && !viewHelper.isVoyagerBroadcast
                        viewController.hideScreenPopup()
                        if(viewHelper.checkForExternalBlock("Voyager"))return
                        if (pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"){
                            viewHelper.fromthreeDMaps=true
                            if(pif.vkarmaToSeat.getMediaType()=="videoAggregate" || pif.vkarmaToSeat.getMediaType()=="video"){
                                viewHelper.stopVod(true);
                                return;
                            }
                            /*pif.vkarmaToSeat.stopVideo()*/
                        }

                        dataController.getCategoryByTemplateId(["maps"],function(model){
                            var catnode = new Object();
                            catnode["pcid"] = model.getValue(0,"cid")
                            catnode["ptid"] = model.getValue(0,"category_attr_template_id")
                            catnode["cid"] = model.getValue(0,"cid")
                            catnode["tid"] = "maps"
                            pif.vkarmaToSeat.launchCategory(catnode);
                        } )




                    }
                    else{
                        if(viewHelper.checkForExternalBlock("USB"))return
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedContinueUsb")
                        if((pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate") && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"){
                            viewController.hideScreenPopup()
                            viewController.showScreenPopup(2)
                        }
                        else
                            viewController.hideScreenPopup()
                    }
                }
            }
            SimpleButton{
                id:okBtn
                visible:false
                anchors.bottom: background.bottom;anchors.bottomMargin: qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.horizontalCenter: parent.horizontalCenter
                normImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn_press
                ViewText{
                    id:okBtnText
                    varText: [configTool.language.global_elements.ok,(okBtn.isPressed)?configTag[configGlobalPopup].generic.btn_text_press:configTag[configGlobalPopup].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    if(viewController.getActiveScreenPopupID()==24)
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedUsbDisconnect",{usbDisconnect:true})
                    viewController.hideScreenPopup()
                }
            }

            SimpleButton{
                id:closeBtn
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].settings.close_btn
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].settings.close_btn_press
                visible:(viewController.getActiveScreenPopupID()==26)
                onActionReleased: {
                    viewController.hideScreenPopup();
                }
            }
        }
    }
}
