import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: clearPlayListPopup
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string configGlobalPopup: (viewHelper.isKids(current.template_id))?"kids_popup_"+orientationType:"popup_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compClearPlaylist,"clearPlaylist")
    }
    function init(){
        console.log("ClearPlayList.qml | init")
        clearPlaylist.setTitleText()
        clearPlaylist.setBtnText()
    }
    function reload(){console.log("ClearPlayList.qml | reload")}
    function clearOnExit(){
        console.log("ClearPlayList.qml | clearOnExit")
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    property QtObject clearPlaylist
    Component{
        id:compClearPlaylist
        Item{
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            function setTitleText(){
                titleText.text=configTool.language.music.delete_track_title
            }
            function setBtnText(){
                yesBtnTxt.text=configTool.language.global_elements.yes
                noBtnText.text=configTool.language.global_elements.no
            }
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobalPopup].generic.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            ViewText{
                id:titleText
                varText: ["",configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_exit_title_'+orientationType+'_fontsize')]
                width:qsTranslate('','Popups_exit_title_'+orientationType+'_w')
                height:qsTranslate('','Popups_exit_title_'+orientationType+'_h')
                anchors.horizontalCenter:background.horizontalCenter
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_exit_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_exit_title_'+orientationType+'_lh')
                wrapMode: Text.WordWrap
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignHCenter
            }
            SimpleButton{
                id:noBtn
                anchors.bottom: background.bottom;anchors.bottomMargin: qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.right:background.right;anchors.rightMargin:qsTranslate('','Popups_exit_button_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn_press
                ViewText{
                    id:noBtnText
                    varText: ["",(noBtn.isPressed)?configTag[configGlobalPopup].generic.btn_text_press:configTag[configGlobalPopup].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    var retArray=[0]
                    viewController.hideScreenPopup(retArray)
                }
            }
            SimpleButton{
                id:yesBtn
                anchors.bottom:(orientationType=='p')?noBtn.top:background.bottom;anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Popups_exit_button_'+orientationType+'_vgap'):qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.right:(orientationType=='l')?noBtn.left:background.right;anchors.rightMargin:(orientationType=='l')?qsTranslate('','Popups_exit_button_'+orientationType+'_hgap'):qsTranslate('','Popups_exit_button_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn_press
                ViewText{
                    id:yesBtnTxt
                    varText: ["",(yesBtn.isPressed)?configTag[configGlobalPopup].generic.btn_text_press:configTag[configGlobalPopup].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    var retArray=[1]
                    viewController.hideScreenPopup(retArray)
                }
            }
        }
    }
}
