import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: helpScreenCont
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string configGlobal: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:"global_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compHelp,"help")
    }
    function init(){
        console.log("Help.qml | init")
    }
    function reload(){console.log("Help.qml | reload")}
    function clearOnExit(){
        console.log("Help.qml | clear on Exit")
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    property QtObject help
    Component{
        id:compHelp
        Item{
            id:mainCont
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobal].help.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            Image{
                id:poster
                source:viewHelper.configToolImagePath+configTag[configGlobal].help.help_image
                anchors.left: background.left;anchors.leftMargin: qsTranslate('','Help_image_'+orientationType+'_lm')
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Help_image_'+orientationType+'_tm')
            }
            ViewText{
                id:titleText
                varText: [configTool.language.karma.help_desc,configTag[configGlobal].help.desc_text,parseInt(qsTranslate('','Help_text_'+orientationType+'_fontsize'),10)+4]
                height:qsTranslate('','Popups_volbri_title_'+orientationType+'_h')
                width:qsTranslate('','Popups_volbri_title_'+orientationType+'_w')
                anchors.left: background.left;anchors.leftMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_lm')
                anchors.top: background.top;anchors.topMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_volbri_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignLeft
                wrapMode:Text.WordWrap
            }
            Rectangle{
                id:scrollText
                width:qsTranslate('','Help_text_'+orientationType+'_w');
                height:qsTranslate('','Help_text_'+orientationType+'_h')
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Help_text_'+orientationType+'_tm')
                anchors.left: background.left;anchors.leftMargin: qsTranslate('','Help_text_'+orientationType+'_lm')
                color:"transparent"
                clip:true
                Flickable{
                    id:flick_txt
                    width:parent.width;height:parent.height
                    interactive: true
                    boundsBehavior :Flickable.StopAtBounds
                    anchors.top:scrollText.top
                    contentHeight:descriptionText.paintedHeight
                    ViewText{
                        id:descriptionText
                        width:parent.width;height:parent.height
                        varText: [configTool.language.karma.help_cat,configTag[configGlobal].help.desc_text,qsTranslate('','Help_text_'+orientationType+'_fontsize')]
                        lineHeight:qsTranslate('','Help_text_'+orientationType+'_lh')
                        lineHeightMode:Text.FixedHeight
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignLeft
                    }
                }
            }
            Image{
                id:fadingImagetop
                anchors.top:scrollText.top
                anchors.right:background.right
                rotation: 180
                visible: (flick_txt.contentY==0)?false:true
                source:viewHelper.configToolImagePath+configTag[configGlobal].help.fadeimage
            }
            Image{
                id:fadingImage
                anchors.bottom: background.bottom
                anchors.right:background.right
//                visible:(descriptionText.paintedHeight>scrollText.height)
                visible: (flick_txt.contentY+scrollText.height == descriptionText.paintedHeight)?false:true
                source:viewHelper.configToolImagePath+configTag[configGlobal].help.fadeimage
            }
            SimpleButton{
                id:closeBtn
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobal].help.close_btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobal].help.close_btn_press
                onActionReleased: {
                    viewController.hideScreenPopup()
                }
            }
        }
    }
}
