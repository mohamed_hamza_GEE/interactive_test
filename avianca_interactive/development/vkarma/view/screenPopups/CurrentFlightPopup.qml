import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: currentFlightCont
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string configGlobal: (viewHelper.isKids(current.template_id))?"kids_global_"+orientationType:"global_"+orientationType
    property string configGlobalPopup: (viewHelper.isKids(current.template_id))?"kids_popup_"+orientationType:"popup_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compCurrentFlight,"currentFlight")
    }
    function init(){
        console.log("Current Flight | init called")
    }
    function clearOnExit(){ console.log("Current Flight | clearOnExit called") }
    onLoaded: { console.log("Current Flight | onLoaded called") }
    /**************************************************************************************************************************/
    property QtObject currentFlight
    Component{
        id:compCurrentFlight
        Item{
            id:mainCont
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobal].help.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            ViewText{
                id:titleText
                varText: [configTool.language.connecting_gate.current_flight_txt,configTag[configGlobal].help.desc_text,qsTranslate('','Popups_volbri_title_'+orientationType+'_fontsize')]
                height:qsTranslate('','Popups_volbri_title_'+orientationType+'_h')
                width:qsTranslate('','Popups_volbri_title_'+orientationType+'_w')
                anchors.left: background.left;anchors.leftMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_lm')
                anchors.top: background.top;anchors.topMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_volbri_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignLeft
                wrapMode:Text.WordWrap
            }
            Rectangle{
                id:flightInfoCont
                width:qsTranslate('','Popups_flight_info_'+orientationType+'_w')
                height:qsTranslate('','Popups_flight_info_'+orientationType+'_h')
                anchors.top:titleText.bottom;anchors.topMargin: qsTranslate('','Popups_flight_info_'+orientationType+'_tm')
                color:"transparent"
                anchors.left: titleText.left
                ViewText{
                    id:flightSrcDest
                    varText: [pif.getSourceIATA()+" - "+pif.getDestinationIATA(),configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_flight_info_'+orientationType+'_fontsize')]
                    lineHeight:qsTranslate('','Popups_flight_info_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    horizontalAlignment: Text.AlignLeft
                    wrapMode:Text.WordWrap
                }
                ViewText{
                    id:flightNumber
                    anchors.top:flightSrcDest.bottom
                    varText: [viewHelper.cgArrivalData[4]+" "+viewHelper.cgArrivalData[5],configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_flight_info_'+orientationType+'_fontsize')]
                    lineHeight:qsTranslate('','Popups_flight_info_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    horizontalAlignment: Text.AlignLeft
                    wrapMode:Text.WordWrap
                }
            }
            Rectangle{
                id:arrivalInfoCont
                width:qsTranslate('','Popups_flight_arrv_'+orientationType+'_w')
                height:qsTranslate('','Popups_flight_arrv_'+orientationType+'_h')
                anchors.top:flightInfoCont.bottom;anchors.topMargin: qsTranslate('','Popups_flight_arrv_'+orientationType+'_tm')
                color:"transparent"
                anchors.left: titleText.left
                ViewText{
                    id:arrivalInfo
                    anchors.top:parent.top
                    lineHeight:qsTranslate('','Popups_flight_arrv_'+orientationType+'_lh')
                    lineHeightMode:Text.FixedHeight
                    horizontalAlignment: Text.AlignLeft
                    wrapMode:Text.WordWrap
                    varText: [configTool.language.connecting_gate.arrival_terminal+": "+viewHelper.cgArrivalData[0]+"<br>"+configTool.language.connecting_gate.arrival_time+": "+viewHelper.cgArrivalData[1]+"<br>"+configTool.language.connecting_gate.header_gate+": "+viewHelper.cgArrivalData[2]+"<br>"+configTool.language.connecting_gate.baggage_carousel+": "+viewHelper.cgArrivalData[3],configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_flight_info_'+orientationType+'_fontsize')]
                }
            }
            SimpleButton{
                id:closeBtn
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobal].help.close_btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobal].help.close_btn_press
                onActionReleased: {
                    viewController.hideScreenPopup()
                }
            }
        }
    }
}
