import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: soundtrackCcPopup
    property alias soundtrackCcPopup: soundtrackCcPopup
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string configCategoryString:(viewHelper.catSelected=='shopping')?'shop':viewHelper.catSelected
    property bool leftArrowPressedSound: false
    property bool rightArrowPressedSound: false
    property bool leftArrowPressedSub: false
    property bool rightArrowPressedSub: false

    /**************************************************************************************************************************/
    function load(){
        push(compSoundTrack,"soundtrack")
    }
    function init(){
        console.log("soundtrackCcPopup | init called")
        aspectModel.setProperty(1,"label_name",configTool.language.movies.fullscreen)
        if(viewController.getActiveScreenPopupID()==19){
            soundtrack.showSubtitle(false)
            soundtrack.setTitle(configTool.language.karma.language_settings)
            soundtrack.setSubTitle(configTool.language.karma.change_language)
            soundtrack.setLangModel(dataController.getIntLanguageModel())
            setLangIndex()
        }
        else{
            core.debug(" configTool.language.movies.vod_change_screensize : "+configTool.language.movies.vod_change_screensize)
            if(viewHelper.getsndccPopFromMovie()){
                soundtrack.setTitle(" ");
                soundtrack.setAspectRatio();
            }else{
                soundtrack.setTitle(configTool.language.movies.play_movie_in);
                soundtrack.clearAspectRatio();
            }

            setIndexes()
            soundtrack.setSubTitle(configTool.language.movies.soundtrack)
            soundtrack.setLangModel(viewHelper.soundtrackModel)
            if(viewHelper.soundtrackModel.count<=1){
                soundtrack.showSoundTrack(false)
                if(viewHelper.subtitleModel.count<=1){
                    viewHelper.setVodParams()
                    viewController.hideScreenPopup()
                }
            }else{
                soundtrack.showSoundTrack(true)
                if(viewHelper.subtitleModel.count<=1){
                    soundtrack.showSubtitle(false)
                }else{
                    soundtrack.showSubtitle(true)
                    soundtrack.setSubtitleModel(viewHelper.subtitleModel)
                }
            }
            viewHelper.setsndccPopFromMovie(false);
        }

    }
    function clearOnExit(){
        core.debug("soundtrackCcPopup | clearOnExit called");

    }
    onLoaded: { core.debug("soundtrackCcPopup | onLoaded called") }
    function setLangIndex(){
        soundtrack.soundtrackLv.currentIndex=viewHelper.getLanguageId()
    }
    function setIndexes(){
        if(viewHelper.soundtrackModel.count>1){
            soundtrack.soundtrackLv.currentIndex=viewHelper.soundtrackId
            if(viewHelper.subtitleModel.count>0)
                soundtrack.subtitleLv.currentIndex=viewHelper.subtitleId
        }
    }

    function getSubTitlesQuery(){ dataController.getSubtitlesWithLabel([viewHelper.tempPlayingDetails["mid"]],setSubtitleQuery,configTool.language.movies.none);}
    function setSubtitleQuery(dModel){viewHelper.setSubtitleModel(dModel);}
    /**************************************************************************************************************************/

    ListModel{
        id:aspectModel
        ListElement{label_name:"4:3";aspect_lid:"0"}
        ListElement{label_name:"";aspect_lid:"1"}
    }
    Connections{
        target:viewHelper//(soundtrackCcPopup.visible)?viewHelper:null
        onCallVodPlay: {
            soundtrack.mainCont.playVodNow()
        }
    }
    property QtObject soundtrack
    Component{
        id:compSoundTrack
        Rectangle{
            id:mainCont
            width:qsTranslate('','SoundtrackLang_bg_'+orientationType+'_w');height:qsTranslate('','SoundtrackLang_bg_'+orientationType+'_h')

            color:configTag[configCategoryString+"_"+orientationType].sndtrk_cc.sndtrck_popup_bg
            property alias mainCont: mainCont
            function showSoundTrack(val){soundtrackCont.visible=val}
            function showSubtitle(val){subtitleCont.visible=val}

            function setTitle(val){
                playMovie.text=val
            }
            function setSubTitle(val){
                soundtrackTitle.text=val
            }
            function setLangModel(dModel){
                soundtrackLv.langModel=dModel
            }
            function setSubtitleModel(dModel){
                subtitleLv.langModel=dModel
            }

            function setAspectRatio(){

                core.debug(" pif.vkarmaToSeat.getAspectRatio() : "+pif.vkarmaToSeat.getAspectRatio())
                if(pif.vkarmaToSeat.getAspectRatio()=="4x3Stretched"||pif.vkarmaToSeat.getAspectRatio()=="4x3"){
                    aspectCont.opacity=1;
                    if(pif.vkarmaToSeat.aspectRatio=="4x3"){
                        aspectTrackLv.currentIndex=0;
                    }else{
                        aspectTrackLv.currentIndex=1;
                    }
                }else{
                    aspectCont.opacity=0;
                    pif.vkarmaToSeat.aspectRatio="disabled";
                }

            }
            function playVodNow(){
                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedSpecialCall",{})
                viewHelper.soundtrackId=soundtrackLv.currentIndex
                viewHelper.subtitleId=subtitleLv.currentIndex
                console.log("pif.vkarmaToSeat.getMediaType() "+pif.vkarmaToSeat.getMediaType())
                console.log("pif.vkarmaToSeat.getCurrentMediaPlayerState() "+pif.vkarmaToSeat.getCurrentMediaPlayerState())
                var soundtrack_lid=(soundtrackLv.currentIndex>=0)?soundtrackLv.langModel.getValue(soundtrackLv.currentIndex,"soundtrack_lid"):-1
                var subtitle_lid=(subtitleCont.visible && subtitleLv.currentIndex>=0)?subtitleLv.langModel.get(subtitleLv.currentIndex).subtitle_lid:-1
                var params=new Object()
                params["soundtrackLid"]=soundtrack_lid
                var params1=new Object()
                params1["subtitleLid"]=subtitle_lid
                console.log("soundtrackLv.currentIndex "+soundtrackLv.currentIndex)
                console.log("subtitleLv.currentIndex "+subtitleLv.currentIndex)
                viewHelper.setSubtitlelid(subtitle_lid)
                viewHelper.setSoundTracklid(soundtrack_lid)
                if(aspectTrackLv==1 && pif.vod.getAspectRatio()=="4x3Adjustable")pif.vod.toggleStretchState()
                else if(aspectTrackLv==0 && pif.vod.getAspectRatio()!="4x3Adjustable")pif.vod.toggleStretchState()
                pif.vkarmaToSeat.setPlayingSoundtrack(params)
                pif.vkarmaToSeat.setPlayingSubtitle(params1)
                if(aspectCont.opacity==1){
                    pif.vkarmaToSeat.toggleVodStretchState();
                }
                if((pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate") && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"){
                    //Do nothing
                }else{
                    viewHelper.setVodParams()
                }
                viewController.hideScreenPopup()
            }

            function clearAspectRatio(){
                aspectCont.opacity=0;
                pif.vkarmaToSeat.aspectRatio="disabled";
            }

            property alias soundtrackLv:soundtrackLv
            property alias subtitleLv:subtitleLv
            MouseArea{
                anchors.fill:parent
                onClicked:{}
            }


            Column{
                id:holder
                anchors.fill:parent;
                anchors.top:parent.top;
                anchors.topMargin:(aspectCont.opacity==1 && orientationType=="l")?0:qsTranslate('','SoundtrackLang_play_title_'+orientationType+'_tm');
                spacing:qsTranslate('','SoundtrackLang_play_title_'+orientationType+'_h')




                ViewText{
                    id:playMovie
                    width:qsTranslate('','SoundtrackLang_play_title_'+orientationType+'_w')
                    height:qsTranslate('','SoundtrackLang_play_title_'+orientationType+'_h')
                    anchors.horizontalCenter: parent.horizontalCenter
                    varText: ["",configTag[configCategoryString+"_"+orientationType].sndtrk_cc.popup_text_color,qsTranslate('','SoundtrackLang_play_title_'+orientationType+'_fontsize')]
                    horizontalAlignment: Text.AlignLeft
                    visible:(text=="")?false:true;
                }

                Item{
                    id:aspectCont
                    width:qsTranslate('','SoundtrackLang_container_'+orientationType+'_w')
                    height:qsTranslate('','SoundtrackLang_container_'+orientationType+'_h')
                    anchors.left:parent.left;anchors.leftMargin:  qsTranslate('','SoundtrackLang_container_'+orientationType+'_lm')
                    opacity:0;

                    ViewText{
                        id:aspectTitle
                        width:qsTranslate('','SoundtrackLang_title_'+orientationType+'_w')
                        height:qsTranslate('','SoundtrackLang_title_'+orientationType+'_h')
                        varText: [configTool.language.movies.vod_change_screensize,configTag[configCategoryString+"_"+orientationType].sndtrk_cc.popup_text_color,qsTranslate('','SoundtrackLang_title_'+orientationType+'_fontsize')]
                        x:qsTranslate('','SoundtrackLang_title_'+orientationType+'_lm')
                        horizontalAlignment: Text.AlignLeft
                    }
                    Image{
                        id:langAspectBg
                        source:viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.language_background
                        anchors.bottom:parent.bottom
                        anchors.right: parent.right;anchors.rightMargin: qsTranslate('','SoundtrackLang_langbg_'+orientationType+'_rm')
                    }
                    SoundtrackccListView{
                        id:aspectTrackLv
                        anchors.centerIn: langAspectBg
                        lvWidth: qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_w')
                        lvHeight: qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_h')
                        cellWidth:qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_w')
                        isSoundtrack:true
                        langModel:aspectModel
                        isSelected:(currentIndex==0&&pif.vkarmaToSeat.getAspectRatio()=="4x3"||currentIndex==1)

                    }
                    SimpleButton{
                        id:leftArrowasp
                        normImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.left_arrow
                        pressedImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.left_arrow_press
                        anchors.left:langAspectBg.left;anchors.bottom:langAspectBg.bottom
                        isDisable: (aspectTrackLv.currentIndex==0)?true:false
                        onActionReleased:  aspectTrackLv.prevLang()
                    }
                    SimpleButton{
                        id:rightArrowasp
                        normImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.right_arrow
                        pressedImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.right_arrow_press
                        anchors.right:langAspectBg.right;anchors.bottom:langAspectBg.bottom
                        isDisable: (aspectTrackLv.currentIndex==(aspectTrackLv.langModel.count-1))?true:false
                        onActionReleased: aspectTrackLv.nextLang()
                    }
                }


                Item{
                    id:soundtrackCont
                    width:qsTranslate('','SoundtrackLang_container_'+orientationType+'_w')
                    height:qsTranslate('','SoundtrackLang_container_'+orientationType+'_h')
                    //anchors.top:parent.top;anchors.topMargin:(subtitleCont.visible)? qsTranslate('','SoundtrackLang_container_'+orientationType+'_tm'):qsTranslate('','SoundtrackLang_container_'+orientationType+'_set_tm')
                    anchors.left:parent.left;anchors.leftMargin:  qsTranslate('','SoundtrackLang_container_'+orientationType+'_lm')


                    ViewText{
                        id:soundtrackTitle
                        width:qsTranslate('','SoundtrackLang_title_'+orientationType+'_w')
                        height:qsTranslate('','SoundtrackLang_title_'+orientationType+'_h')
                        varText: ["",configTag[configCategoryString+"_"+orientationType].sndtrk_cc.popup_text_color,qsTranslate('','SoundtrackLang_title_'+orientationType+'_fontsize')]
                        anchors.left: parent.left;anchors.leftMargin: qsTranslate('','SoundtrackLang_title_'+orientationType+'_lm')
                        //anchors.top: playMovie.bottom;anchors.topMargin: qsTranslate('','SoundtrackLang_title_'+orientationType+'_tm')
                        horizontalAlignment: Text.AlignLeft
                    }
                    Image{
                        id:langSoundBg
                        source:viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.language_background
                        anchors.bottom:parent.bottom
                        anchors.right: soundtrackCont.right;anchors.rightMargin: qsTranslate('','SoundtrackLang_langbg_'+orientationType+'_rm')
                    }
                    SoundtrackccListView{
                        id:soundtrackLv
                        anchors.centerIn: langSoundBg
                        lvWidth: qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_w')
                        lvHeight: qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_h')
                        cellWidth:qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_w')
                        isSoundtrack:true
                    }
                    SimpleButton{
                        id:leftArrow
                        normImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.left_arrow
                        pressedImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.left_arrow_press
                        anchors.left:langSoundBg.left;anchors.bottom:langSoundBg.bottom
                        isDisable: (soundtrackLv.currentIndex==0)?true:false
                        onActionReleased:  soundtrackLv.prevLang()
                    }
                    SimpleButton{
                        id:rightArrow
                        normImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.right_arrow
                        pressedImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.right_arrow_press
                        anchors.right:langSoundBg.right;anchors.bottom:langSoundBg.bottom
                        isDisable: (soundtrackLv.currentIndex==(soundtrackLv.langModel.count-1))?true:false
                        onActionReleased: soundtrackLv.nextLang()
                    }
                }
                Item{
                    id:subtitleCont
                    width:qsTranslate('','SoundtrackLang_container_'+orientationType+'_w')
                    height:qsTranslate('','SoundtrackLang_container_'+orientationType+'_h')
                    //anchors.top:(soundtrackCont.visible)?soundtrackCont.bottom:parent.bottom;
                    //anchors.topMargin:(soundtrackCont.visible)?qsTranslate('','SoundtrackLang_container_'+orientationType+'_vgap'):qsTranslate('','SoundtrackLang_container_'+orientationType+'_set_tm')
                    anchors.left:parent.left;anchors.leftMargin:  qsTranslate('','SoundtrackLang_container_'+orientationType+'_lm')

                    ViewText{
                        id:subtitleTxt
                        width:qsTranslate('','SoundtrackLang_title_'+orientationType+'_w')
                        height:qsTranslate('','SoundtrackLang_title_'+orientationType+'_h')
                        varText: [configTool.language.movies.subtitle_cc,configTag[configCategoryString+"_"+orientationType].sndtrk_cc.popup_text_color,qsTranslate('','SoundtrackLang_title_'+orientationType+'_fontsize')]
                        anchors.left: parent.left;anchors.leftMargin: qsTranslate('','SoundtrackLang_title_'+orientationType+'_lm')
                        anchors.top: parent.top;anchors.topMargin: qsTranslate('','SoundtrackLang_title_'+orientationType+'_tm')
                        horizontalAlignment: Text.AlignLeft
                    }
                    Image{
                        id:langSubBg
                        source:viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.language_background
                        anchors.bottom:parent.bottom
                        anchors.right: subtitleCont.right;anchors.rightMargin: qsTranslate('','SoundtrackLang_langbg_'+orientationType+'_rm')
                    }
                    SoundtrackccListView{
                        id:subtitleLv
                        anchors.centerIn: langSubBg
                        lvWidth: qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_w')
                        lvHeight: qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_h')
                        //langModel: viewHelper.subtitleModel
                        cellWidth:qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_w')
                        cellHeight: qsTranslate('','SoundtrackLang_langtext_'+orientationType+'_h')
                        isSoundtrack:false
                    }
                    SimpleButton{
                        id:leftArrowSub
                        normImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.left_arrow
                        pressedImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.left_arrow_press
                        anchors.left:langSubBg.left;anchors.bottom:langSubBg.bottom
                        isDisable: (subtitleLv.currentIndex==0)?true:false
                        onActionReleased:  subtitleLv.prevLang()
                    }
                    SimpleButton{
                        id:rightArrowSub
                        normImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.right_arrow
                        pressedImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.right_arrow_press
                        anchors.right:langSubBg.right;anchors.bottom:langSubBg.bottom
                        isDisable: (subtitleLv.currentIndex==(subtitleLv.langModel.count-1))?true:false
                        onActionReleased: subtitleLv.nextLang()
                    }
                }
            }
            SimpleButton{
                id:acceptBtn
                normImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.button
                pressedImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.button_press
                anchors.right:(orientationType=='l')?cancelBtn.left:parent.right;anchors.rightMargin:(orientationType=='l')? qsTranslate('','SoundtrackLang_button_'+orientationType+'_hgap'): qsTranslate('','SoundtrackLang_button_'+orientationType+'_rm')
                anchors.bottom:(orientationType=='l')?parent.bottom:cancelBtn.top;anchors.bottomMargin:(orientationType=='l')?qsTranslate('','SoundtrackLang_button_'+orientationType+'_bm'): qsTranslate('','SoundtrackLang_button_'+orientationType+'_vgap')
                ViewText{
                    id:acceptText
                    anchors.centerIn: parent
                    width:qsTranslate('','SoundtrackLang_buttontxt_'+orientationType+'_w')
                    varText: [configTool.language.movies.accept,(acceptBtn.isPressed)?configTag[configCategoryString+"_"+orientationType].sndtrk_cc.button_text_press:configTag[configCategoryString+"_"+orientationType].sndtrk_cc.button_text,qsTranslate('','SoundtrackLang_buttontxt_'+orientationType+'_fontsize')]
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    if(viewController.getActiveScreenPopupID()==19){
                        viewHelper.setLanguageId(soundtrackLv.currentIndex)
                        var iso=dataController.getIntLanguageModel().get(soundtrackLv.currentIndex).ISO639
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedUpdateLang",{langIndex:soundtrackLv.currentIndex})
                        pif.vkarmaToSeat.languageChange({languageIso:iso})
                        core.setInteractiveLanguage(dataController.getLidByLanguageISO(iso),iso)
                        if(viewData.optionMenuModel.getValue(viewHelper.sectionIndexSel,"category_attr_template_id")=="adult")
                            widgetList.setSectionBtnText(configTool.language.global_elements.button_kids_text)
                        else
                            widgetList.setSectionBtnText(configTool.language.global_elements.button_mainmenu_text)
                        dataController.refreshModelsByIntLanguage([viewData.mediaModel,viewData.tracklistModel,viewData.currentTrackData,viewData.playListModel],[""]);
                        dataController.refreshModelsByIntLanguage([viewData.mainMenuModel,viewData.kidsMenuModel,viewData.subMenuModel],[viewData.checkData,viewData.checkData,viewData.checkData]);
                        if(viewHelper.catSelected=="shopping"){
                            core.debug("onActionReleased>>>>>>>>>>>>>>>>>>>>>>>>>>>> current.template_id = "+current.template_id)
                            core.debug("onActionReleased>>>>>>>>>>>>>>>>>>>>>>>>>>>> viewHelper.catSelected = "+viewHelper.catSelected)
                            viewHelper.languageUpdate()
                        }
                    }
                    else{
                        mainCont.playVodNow()

                    }
                    viewController.hideScreenPopup()
                }
            }
            SimpleButton{
                id:cancelBtn
                normImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.button
                pressedImg: viewHelper.configToolImagePath+configTag[configCategoryString+"_"+orientationType].sndtrk_cc.button_press
                anchors.right: parent.right;anchors.rightMargin: qsTranslate('','SoundtrackLang_button_'+orientationType+'_rm')
                anchors.bottom:parent.bottom;anchors.bottomMargin:qsTranslate('','SoundtrackLang_button_'+orientationType+'_bm')
                ViewText{
                    id:cancelText
                    anchors.centerIn: parent
                    width:qsTranslate('','SoundtrackLang_buttontxt_'+orientationType+'_w')
                    varText: [configTool.language.movies.cancel,(cancelBtn.isPressed)?configTag[configCategoryString+"_"+orientationType].sndtrk_cc.button_text_press:configTag[configCategoryString+"_"+orientationType].sndtrk_cc.button_text,qsTranslate('','SoundtrackLang_buttontxt_'+orientationType+'_fontsize')]
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                }
            }
        }
    }
}
