import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: playlistPopup
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property variant playlistNames: [configTool.language.music.playlist1,configTool.language.music.playlist2,configTool.language.music.playlist3]
    property string configGlobalPopup: (viewHelper.isKids(current.template_id))?"kids_popup_"+orientationType:"popup_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compPlaylist,"playList")
    }
    function init(){
        console.log("playlistPopup.qml | init")
    }
    function reload(){console.log("playlistPopup.qml | reload")}
    function clearOnExit(){
        console.log("playlistPopup.qml | clearOnExit")
    }
    onLoaded: {}
    function getTracks(index){
        var midList =pif.vkarmaToSeat.getAllPlaylistMids({"playlistIndex":(index+1)*-1,"mediaType":"aod"})
        return " ("+midList.length+"/99)"
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Rectangle{
            id:background
            width:parent.width;height:parent.height
            color:configTag[configGlobalPopup].generic.bg_color
            MouseArea{
                anchors.fill: parent
                onClicked:{}
            }
            SimpleButton{
                id:closeBtn
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].help.close_btn
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].help.close_btn_press
                onActionReleased: {
                    var retArray=[0]
                    viewController.hideScreenPopup(retArray)
                }
            }
        }
    }
    property QtObject playList
    Component{
        id:compPlaylist
        Item{
            width:background.width;height:background.height
            ViewText{
                id:textPlay
                varText: [configTool.language.music.add_tracks_to,configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_volbri_title_'+orientationType+'_fontsize')]
                height:qsTranslate('','Popups_volbri_title_'+orientationType+'_h')
                width:qsTranslate('','Popups_volbri_title_'+orientationType+'_w')
                anchors.left: parent.left;anchors.leftMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_lm')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Popups_volbri_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_volbri_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignLeft
                wrapMode:Text.WordWrap
            }
            Column{
                id:playlistView
                width:qsTranslate('','Popups_addtrack_cont_'+orientationType+'_w')
                height:qsTranslate('','Popups_addtrack_cont_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','Popups_addtrack_cont_'+orientationType+'_tm')
                spacing:qsTranslate('','Popups_addtrack_cont_'+orientationType+'_vgap')
                property int currentIndex;
                anchors.horizontalCenter: parent.horizontalCenter
                Repeater{
                    model:3
                    SimpleButton{
                        id:playlistBtn
                        height:qsTranslate('','Popups_addtrack_cont_'+orientationType+'_cellh')
                        normImg: viewHelper.configToolImagePath+configTag["music_"+orientationType].addtracks.button
                        pressedImg: viewHelper.configToolImagePath+configTag["music_"+orientationType].addtracks.button_press
                        ViewText{
                            id:playlistText
                            anchors.centerIn: parent
                            width:qsTranslate('','Popups_addtrack_text_'+orientationType+'_w')
                            varText: [playlistNames[index]+getTracks(index),(playlistBtn.isPressed)?configTag["music_"+orientationType].addtracks.text_color:configTag["music_"+orientationType].addtracks.text_colo_pressr,qsTranslate('','Popups_addtrack_text_'+orientationType+'_fontsize')]
                            horizontalAlignment: Text.AlignHCenter
                        }
                        onActionReleased: {
                            playlistView.currentIndex=index;
                            var retArray=[(index+1)*-1]
                            viewController.hideScreenPopup(retArray)
                        }
                    }
                }
            }
        }
    }
}
