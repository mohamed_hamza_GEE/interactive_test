import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: gamePopup
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string configGlobalPopup: (viewHelper.isKids(current.template_id))?"kids_popup_"+orientationType:"popup_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compGamesPopup,"gamesPopup")
    }
    function init(){
        console.log("Games popup.qml | init")
        setContent()
        if(/*viewController.getActiveScreenPopupID()==6||*/viewController.getActiveScreenPopupID()==25)hidePopup.restart()
        gamesPopup.showCloseBtn()
    }
    function reload(){console.log("Games popup.qml | reload")}
    function clearOnExit(){
        console.log("Games popup.qml | clearOn exit")
    }
    onLoaded: {}
    function setContent(){
        switch(viewController.getActiveScreenPopupID()){
        case 6:gamesPopup.setContent(configTool.language.global_elements.comingsoon);break;
        case 7:gamesPopup.setContent(configTool.language.games.loading);break;
        case 10:gamesPopup.setContent(configTool.language.connecting_gate.cg_info_notavailable);break;
        case 11:gamesPopup.setContent(configTool.language.connecting_gate.cg_info_update);break;
        case 12:gamesPopup.setContent(configTool.language.connecting_gate.cg_info_available);break;
        case 25:gamesPopup.setContent(configTool.language.karma.launch_app);break;
        case 27:gamesPopup.setContent(configTool.language.global_elements.selectionUnavilable);break;
        }
    }

    /**************************************************************************************************************************/
    property QtObject gamesPopup
    Component{
        id:compGamesPopup
        Item{
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            function setContent(val){contentText.text=val}
            function showCloseBtn(){
                if(viewController.getActiveScreenPopupID()==10 ||viewController.getActiveScreenPopupID()==11 ||viewController.getActiveScreenPopupID()==12 ||viewController.getActiveScreenPopupID()==27)
                    closeBtn.visible=true
                else closeBtn.visible=false
            }
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobalPopup].generic.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{if(viewController.getActiveScreenPopupID()==6||viewController.getActiveScreenPopupID()==27)viewController.hideScreenPopup()}
                }
            }
            SimpleButton{
                id:closeBtn
                visible: false
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_close_'+orientationType+'_tm')
                anchors.right: background.right;anchors.rightMargin: qsTranslate('','Popups_close_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].settings.close_btn
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].settings.close_btn_press
                onActionReleased: {
                    viewController.hideScreenPopup()
                }
            }
            ViewText{
                id:contentText
                varText: ["",configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_exit_title_'+orientationType+'_fontsize')]
                width:qsTranslate('','Popups_pa_text_'+orientationType+'_w')
                height:qsTranslate('','Popups_pa_text_'+orientationType+'_h')
                anchors.centerIn: parent
                lineHeight:qsTranslate('','Popups_pa_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment:Text.AlignVCenter
                wrapMode: Text.WordWrap
            }
        }
    }
    Timer{
        id:hidePopup
        interval:1000
        running:false
        onTriggered: {viewController.hideScreenPopup()}
    }
}

