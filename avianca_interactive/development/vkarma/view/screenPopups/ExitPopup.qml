import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: exitScreenPopup
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property bool isExitPopup:false
    property string configGlobalPopup: (viewHelper.isKids(current.template_id))?"kids_popup_"+orientationType:"popup_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compExitPopup,'exitPopup')
    }
    function init(){
        console.log("Exit popup.qml | init")
        if(viewController.getActiveScreenPopupID()==2)isExitPopup=true
        else isExitPopup=false

        exitPopup.setTitleText()
        exitPopup.setBtnText()
    }
    function reload(){console.log("Exit popup.qml | reload")}
    function clearOnExit(){
        console.log("Exit popup.qml | clear on Exit")
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    property QtObject exitPopup
    Component{
        id:compExitPopup
        Item{
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            function setTitleText(){
                if(isExitPopup)titleText.text=configTool.language.movies.do_you_want_to_exit
                else titleText.text=configTool.language.tv.what_do_you_wish
            }
            function setBtnText(){
                if(isExitPopup){
                    yesBtnTxt.text=configTool.language.global_elements.yes
                    noBtnText.text=configTool.language.global_elements.no
                }else{
                    yesBtnTxt.text=configTool.language.movies.resume
                    noBtnText.text=configTool.language.movies.restart
                }
            }
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobalPopup].generic.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            ViewText{
                id:titleText
                varText: ["",configTag[configGlobalPopup].generic.text_color,qsTranslate('','Popups_exit_title_'+orientationType+'_fontsize')]
                width:qsTranslate('','Popups_exit_title_'+orientationType+'_w')
                height:qsTranslate('','Popups_exit_title_'+orientationType+'_h')
                anchors.horizontalCenter:background.horizontalCenter
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_exit_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_exit_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignHCenter
            }
            SimpleButton{//restart
                id:noBtn
                anchors.bottom: background.bottom;anchors.bottomMargin: qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.right:background.right;anchors.rightMargin:qsTranslate('','Popups_exit_button_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn_press
                ViewText{
                    id:noBtnText
                    varText: ["",(noBtn.isPressed)?configTag[configGlobalPopup].generic.btn_text_press:configTag[configGlobalPopup].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                    if(isExitPopup){
                        viewHelper.fromthreeDMaps=false
                        viewHelper.isTrailerPressed=false
                        //   pif.vkarmaToSeat.resumePlayingMedia();
                    }else    viewHelper.loadVideoScreen()
                }
            }
            SimpleButton{//resume
                id:yesBtn
                anchors.bottom:(orientationType=='p')?noBtn.top:background.bottom;anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Popups_exit_button_'+orientationType+'_vgap'):qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.right:(orientationType=='l')?noBtn.left:background.right;anchors.rightMargin:(orientationType=='l')?qsTranslate('','Popups_exit_button_'+orientationType+'_hgap'):qsTranslate('','Popups_exit_button_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobalPopup].generic.btn_press
                ViewText{
                    id:yesBtnTxt
                    varText: ["",(yesBtn.isPressed)?configTag[configGlobalPopup].generic.btn_text_press:configTag[configGlobalPopup].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                    if(isExitPopup){
                        viewHelper.blockKeysForSeconds(3.5)
                        if(viewHelper.isFromExitPopup){
                            viewHelper.isPlayBtnClicked=true;
                            pif.vkarmaToSeat.stopVideo()
                        }
                        else  pif.vkarmaToSeat.stopVideo()
                    }else   {
                        viewHelper.isResume=true
                        viewHelper.setVodParams()
                    }
                }
            }
        }
    }
}
