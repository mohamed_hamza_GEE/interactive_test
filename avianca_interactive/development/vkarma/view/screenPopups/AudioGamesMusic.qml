import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: exitScreenPopup
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property string configGlobal: (viewHelper.isKids(current.template_id))?"kids_popup_"+orientationType:"popup_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compAudioGamesPopup,'objAudioGames')
    }
    function init(){
        console.log("AudioGamesMusic popup.qml | init")
    }
    function reload(){console.log("AudioGamesMusic popup.qml | reload")}
    function clearOnExit(){
        console.log("AudioGamesMusicxit popup.qml | clear on Exit")
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    property QtObject objAudioGames
    Component{
        id:compAudioGamesPopup
        Item{
            width:qsTranslate('','Popups_bg_'+orientationType+'_w')
            height:qsTranslate('','Popups_bg_'+orientationType+'_h')
            Rectangle{
                id:background
                width:parent.width;height:parent.height
                color:configTag[configGlobal].generic.bg_color
                MouseArea{
                    anchors.fill: parent
                    onClicked:{}
                }
            }
            ViewText{
                id:titleText
                varText: ["Would you like to continue listening to your music selection or to switch to the game audio instead?",configTag[configGlobal].generic.text_color,qsTranslate('','Popups_exit_title_'+orientationType+'_fontsize')]
                width:qsTranslate('','Popups_exit_title_'+orientationType+'_w')
                height:qsTranslate('','Popups_exit_title_'+orientationType+'_h')
                anchors.horizontalCenter:background.horizontalCenter
                anchors.top:background.top;anchors.topMargin: qsTranslate('','Popups_exit_title_'+orientationType+'_tm')
                lineHeight:qsTranslate('','Popups_exit_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
            }
            SimpleButton{
                id:gameAudio
                anchors.bottom: background.bottom;anchors.bottomMargin: qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.right:background.right;anchors.rightMargin:qsTranslate('','Popups_exit_button_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobal].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobal].generic.btn_press
                ViewText{
                    id:gameAudioText
                    varText: ["Game",(gameAudio.isPressed)?configTag[configGlobal].generic.btn_text_press:configTag[configGlobal].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                    viewHelper.launchGamesonSelection(false)
                }
            }
            SimpleButton{//resume
                id:musicAudio
                anchors.bottom:(orientationType=='p')?gameAudio.top:background.bottom;anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Popups_exit_button_'+orientationType+'_vgap'):qsTranslate('','Popups_exit_button_'+orientationType+'_bm')
                anchors.right:(orientationType=='l')?gameAudio.left:background.right;anchors.rightMargin:(orientationType=='l')?qsTranslate('','Popups_exit_button_'+orientationType+'_hgap'):qsTranslate('','Popups_exit_button_'+orientationType+'_rm')
                normImg: viewHelper.configToolImagePath+configTag[configGlobal].generic.btn
                pressedImg: viewHelper.configToolImagePath+configTag[configGlobal].generic.btn_press
                ViewText{
                    id:musicAudioTxt
                    varText: ["Music",(musicAudio.isPressed)?configTag[configGlobal].generic.btn_text_press:configTag[configGlobal].generic.btn_text,qsTranslate('','Popups_buttontxt_'+orientationType+'_fontsize')]
                    width:qsTranslate('','Popups_buttontxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                onActionReleased: {
                    viewController.hideScreenPopup()
                    viewHelper.launchGamesonSelection(true)
                }
            }
        }
    }
}
