import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: dialpad
     property int systempopupId: viewController.getActiveScreenPopupID()

    function load(){
        push(compDialpadPopup, "dialpadPopup")
    }
    function init(){
        core.debug("Dialpad | init called | "+viewController.getActiveScreenPopupID())
//        if(widgetList.getVkbRef().visible){
//            widgetList.showHideVKBScreen(false)
//        }
        systempopupId=viewController.getActiveScreenPopupID()
        pif.lockOrientation([pif.cLANDSCAPE])
        dialpadPopup.visible=true
        dialpadPopup.init();
    }
    function clearOnExit(){
        core.debug("Dialpad | clearOnExit called")

        if(widgetList.getVkbRef().visible){
            pif.lockOrientation([pif.cLANDSCAPE])
        }else{
            pif.unlockOrientation()
        }
        //         pif.unlockOrientation()
        dialpadPopup.clearAll();
    }
    /**************************************************************************************************************************/

//    CompPopup{}
    property QtObject dialpadPopup;
    Component{
        id: compDialpadPopup
        CompDialpad{
            width:core.width
            height:core.height
            visible:false
        }
    }
}
