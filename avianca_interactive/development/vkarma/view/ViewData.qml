import QtQuick 1.1
import Panasonic.Pif 1.0
import "../framework/datacontroller"
// Contains interactive specific data model instances of mediaDb... E.g tracklist, synopsis, sountracks, subtitles

Item {
    property variant optionMenuModel;


    property int iso;


    signal optionMenuModelReady()
    function optionMenuReady(dModel){
        optionMenuModel=dModel
        optionMenuModelReady()
    }
    property variant mainMenuModel;
    signal mainMenuModelReady()
    function mainMenuReady(dModel){
        mainMenuModel=dModel
        mainMenuModelReady()
    }
    property variant kidsMenuModel;
    signal kidsMenuModelReady()
    function kidsMenuReady(dModel){
        kidsMenuModel=dModel
        kidsMenuModelReady()
    }
    property variant subMenuModel;
    signal subMenuModelReady()
    function subMenuReady(dModel){
        subMenuModel=dModel
        subMenuModelReady()
    }
    property variant mediaModel
    signal mediaModelReady()
    function mediaMenuReady(dModel){
        mediaModel=dModel
        mediaModelReady()
    }
    property variant tracklistModel
    signal tracklistModelReady()
    function tracklistReady(dModel){
        tracklistModel=dModel
        tracklistModelReady()
    }
    property variant  currentTrackData
    signal currentTrackReady()
    function getCurrentPlayingTrack(dModel){
        currentTrackData=dModel
        currentTrackReady()
    }
    property variant playListModel
    signal playListModelReady()
    function playListReady(dModel){
        playListModel=dModel
        playListModelReady()
    }

    property variant shoppingModel: viewHelper.blankModel
    signal shoppingModelReady()
    function setShopppingData(dModel){
        core.log("ViewData  | vkarma | shoppingModelReady dataReady called"+dModel.count);
        shoppingModel=dModel
        shoppingModelReady()
    }

    property variant hospitalityModel: viewHelper.blankModel
    signal hospitalitModelReady()
    function setHospitalitData(dModel){
        core.log("ViewData  | vkarma | hospitalitModelReady dataReady called"+dModel.count);
        hospitalityModel=dModel
        hospitalitModelReady()

    }

    function checkData(){
        core.debug("############################### iso : "+iso)
        core.debug("############################### value : "+viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"title"))
       iso++
    }
}
