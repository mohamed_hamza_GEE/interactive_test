import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: hospSubMenuScreen
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'

    /***************************************************************************************************************************/
    Connections{
        target:(visible)?viewHelper:null
        onHospitalityLangUpdate:{
            console.log("HospSubMenu.qml |  onHospitalityLangUpdate ")
            subMenu.model=core.dataController.hospitality.getCategoryModel()
        }
    }

    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSubMenuBackground,"subMenuBackground")
        push(compSubMenu,"subMenu")
    }
    function init(){
        core.debug("HospSubMenu.qml | init")
        setHeaderFooter()
        getCatalogMenu()
    }

    function reload(){
        init()
    }

    function clearOnExit(){
        core.debug("HospSubMenu.qml | clearOnExit")
        removeHeaderFooter()
    }
    onLoaded: {}

    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        viewController.loadScreen(current.template_id)
    }

    function getCatalogMenu(){
        var curLangISO_hos=core.settings.languageISO.toUpperCase()
        var langIndex_hos=core.coreHelper.searchEntryInModel(core.dataController.hospitality.getLanguageModel(),"language_code",curLangISO_hos)
        core.log("languageUpdate |  viewHelper.defaultLang  hospitality = "+viewHelper.defaultLang+" langIndex = "+langIndex_hos+ " curLangISO = "+curLangISO_hos)
        if(langIndex_hos!=-1){
            console.log("languageUpdate |  curLangISO = "+curLangISO_hos)
            core.dataController.hospitality.fetchData(curLangISO_hos,hosCatalogMenuReady)
        }else{
            console.log("defaultLang "+viewHelper.defaultLang)
            core.dataController.hospitality.fetchData(viewHelper.defaultLang,hosCatalogMenuReady)
        }
    }

    function hosCatalogMenuReady(dModel,status){
        core.debug("hosCatalogMenuReady | dModel.count "+dModel.count)
        if(dModel.count<=0){ viewController.showScreenPopup(27); return;}
        for (var i=0; i<dModel.count;i++){
            console.log("Inside For catalog_id = "+dModel.getValue(i,"catalog_id"))
            console.log("Inside For catalog_type = "+dModel.getValue(i,"catalog_type"))
            console.log("Inside For catalog_type = "+dModel.getValue(i,"category_node_id"))
        }
        viewHelper.hospCatalogModel=dModel
    }

    function subCatSelected(index){
        console.log(" SubMenu.qml | subCatSelected ")
        subMenu.lView.currentIndex=index
        viewHelper.subMenuSelIndex=index
        viewHelper.hospValue=subMenu.lView.currentItem.getValue();
        viewHelper.hosp_catalog_type=subMenu.lView.currentItem.catlogValue()
        viewController.loadScreen("hospitality_listing")
    }


    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:hospSubMenuScreen.width;
            height:hospSubMenuScreen.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
            Image{
                id:catBgImage
//                width:432
//                height:200
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag["meal_"+orientationType].listing.background
            }
        }
    }
    property QtObject subMenuBackground
    Component{
        id:compSubMenuBackground
        Item{
            anchors.top:parent.top
            anchors.horizontalCenter:background.horizontalCenter
            anchors.fill:parent

            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.mediaImagePath+viewHelper.catIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText: [viewData.mediaModel.getValue(viewHelper.mediaSelected,"title"),configTag["menu_"+orientationType].submenu.text_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }

    property QtObject subMenu
    Component{
        id:compSubMenu
        Item{
            anchors.horizontalCenter: background.horizontalCenter
            property alias lView: subMenuLv
            height: parent.height
            width:parent.width


            Image{
                id:upArrow
                visible:(orientationType=='p' && subMenuLv.contentY>0)
                source:viewHelper.configToolImagePath+configTag["menu_p"].submenu.up_arrow
                anchors.top:(orientationType=='p')? parent.top:undefined;
                anchors.topMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_up'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Image{
                id:upperDivLine
                visible:(orientationType=='p')
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu.divline:""
                anchors.bottom:subMenuLv.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:subMenuLv
                width:qsTranslate('','MainMenu_container_'+orientationType+'_w')
                 height:qsTranslate('','Listing_container_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','Listing_container_'+orientationType+'_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                orientation:(pif.getOrientationType()=='p')?ListView.Vertical:ListView.Horizontal
                boundsBehavior: ListView.StopAtBounds
                snapMode: ListView.SnapToItem
                cacheBuffer: width
                model:viewHelper.hospCatalogModel
                delegate:subMenuDelegate
                clip:true
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                interactive:(count>3)


                onMovementEnded: {
                    if(indexAt(contentX,contentY)>=0){
                        var cIndex=indexAt(contentX+10,contentY+10);
                        if(pif.getOrientationType()!="p" && subMenuLv.model.count<=3){
                            cIndex=0;
                        }else {
                            var resetPosition=(subMenuLv.model.count-3);
                            if(resetPosition>0){
                                cIndex=(cIndex>resetPosition)?(resetPosition):cIndex;
                            }else{
                                cIndex=0;
                            }
                        }
                        currentIndex=cIndex;
                    }
                }

            }
            Image{
                id:leftArrow
                visible:(orientationType=='l'&& subMenuLv.contentX>0)
                source:viewHelper.configToolImagePath+configTag["menu_l"].submenu.left_arrow
                anchors.left:(orientationType=='l')? parent.left:undefined;
                anchors.leftMargin: (orientationType=='l')?qsTranslate('','MainMenu_arrow_l_hgap'):0
                anchors.verticalCenter: subMenuLv.verticalCenter
            }
            Image{
                id:rightArrow
                visible:(orientationType=='l'&& subMenuLv.currentIndex<subMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag["menu_l"].submenu.right_arrow
                anchors.right:(orientationType=='l')? parent.right:undefined;
                anchors.rightMargin:(orientationType=='l')?qsTranslate('','MainMenu_arrow_l_hgap'):0
                anchors.verticalCenter: subMenuLv.verticalCenter
            }
            Image{
                id:downArrow
                visible:(orientationType=='p'&& subMenuLv.currentIndex<subMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag["menu_p"].submenu.down_arrow
                anchors.bottom:(orientationType=='p')? parent.bottom:undefined;
                anchors.bottomMargin: (orientationType=='p')?qsTranslate('','MainMenu_arrow_p_vgap'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
    Component{
        id:subMenuDelegate
        Item{
            id:hosp_del
            width:qsTranslate('','Listing_cells_'+orientationType+'_w')
            height:qsTranslate('','Listing_cells_'+orientationType+'_h')
            property bool isPressed: false
            function getValue(){
                return value
            }

            function catlogValue(){
                return value.catalog_type
            }

            function getNormIcon(typ){
                if(typ=="lunch"){return configTag.meal_l.listing.menu_lunch}
                else if(typ=="breakfast"){return configTag.meal_l.listing.menu_breakfast}
                else if(typ=="wines"){return configTag.meal_l.listing.menu_liquors}
                else if(typ=="beverages"){return configTag.meal_l.listing.menu_beverages}
            }
            Image{
                id:highLightImg
                smooth:true
                anchors.centerIn: mainMenuIcon
                source:(isPressed)?viewHelper.configToolImagePath+configTag["meal_"+orientationType].listing.btn_press:""
            }
            Image{
                id:mainMenuIcon
                width:qsTranslate('','MainMenu_icon_'+orientationType+'_w');height:qsTranslate('','MainMenu_icon_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','MainMenu_icon_'+orientationType+'_tm')
                anchors.left: parent.left;anchors.leftMargin:qsTranslate('','MainMenu_icon_'+orientationType+'_lm')
                source:viewHelper.configToolImagePath+hosp_del.getNormIcon(value.catalog_type);
            }

            ViewText{
                id:catname
                varText:[value.title,(isPressed)?configTag["menu_"+orientationType].submenu.text_color_press:configTag["menu_"+orientationType].submenu.text_color,qsTranslate('','SubMenu_text_'+orientationType+'_fontsize')]
                anchors.left:(orientationType=='p')? mainMenuIcon.right:undefined;anchors.leftMargin: (orientationType=='p')?qsTranslate('','MainMenu_text_'+orientationType+'_lm'):0
                anchors.top:(orientationType=='l')?mainMenuIcon.bottom:undefined;anchors.topMargin:(orientationType=='l')?qsTranslate('','MainMenu_text_l_tm'):0
                height:qsTranslate('','MainMenu_text_'+orientationType+'_h')
                width:qsTranslate('','MainMenu_text_'+orientationType+'_w')
                anchors.verticalCenter:(orientationType=='p')? mainMenuIcon.verticalCenter:undefined
                anchors.horizontalCenter: (orientationType=='l')?mainMenuIcon.horizontalCenter:undefined
                verticalAlignment: (orientationType=='p')?Text.AlignVCenter:Text.AlignTop
                horizontalAlignment: (orientationType=='l')?Text.AlignHCenter:Text.AlignLeft
                elide: Text.ElideRight
                lineHeight:qsTranslate('','MainMenu_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }

            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu.divline:""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !subMenu.lView.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }
                onReleased: {
                    isPressed=false
                    viewHelper.subMenuSelIndex=index
                    subCatSelected(index)
                }
            }
        }
    }
}

