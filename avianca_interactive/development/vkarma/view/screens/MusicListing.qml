import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import ImageMask 1.0
SmartLoader {
    id: musicListingScreen
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property variant artistModel: viewHelper.blankModel
    property string  catSelected: ""
    property bool isAlbumFromArtist: false
    property bool atAlbumLevel: false
    /**************************************************************************************************************************/
    Connections{
        target:(visible)?viewData:null
        onTracklistModelReady:{
            viewHelper.isPlayList=false
            viewController.loadScreen("tracklist")
        }
        onMediaModelReady:{
            musicMediaMenu.setMusicModel(viewData.mediaModel)
            musicMediaMenu.setMusicDelegate(compAlbumList)
        }
    }
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSubMenuBackground,"subMenuBackground")
        push(compMusicMediaMenu,"musicMediaMenu")
    }
    function init(){
        console.log("Music Listing | init called")
        setHeaderFooter()
        getMusicMedia()
    }
    function clearOnExit(){
        console.log("Music Listing | clearOnExit called")
        removeHeaderFooter()
        atAlbumLevel=false
    }
    function reload(){console.log("Music Listing | init called");init()}
    onLoaded: { core.debug("Media Listing | onLoaded called") }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        if(atAlbumLevel)reload()
        else viewController.loadPrev()
    }
    function getMusicMedia(){
        atAlbumLevel=false
        var cid=viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"cid")
        var tid=viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"category_attr_template_id")
        viewHelper.tempId=tid
        catSelected=tid
        if(tid=="music_listing"){
            dataController.getNextMenu([cid,tid],viewData.mediaMenuReady)
        }else if(tid=="artist_listing"){
            var params=new Object();
            params['cid']           = viewData.subMenuModel.getValue(0,"cid")
            params['template_id']   = viewData.subMenuModel.getValue(0,"category_attr_template_id")
            params['inputSearch']   = ""
            params['searchType']    = "NA"  ;
            params['fieldList']     = 'artist';
            params['searchBy']      = 'artist'
            params['orderBy']       = 'artist';
            params['groupBy']       = 'artist'
            dataController.keywordSearch.getData(params,artistListReady,99);

        }else if(tid=="genre_listing"){
            console.log("viewHelper.subMenuSelIndex : "+viewHelper.subMenuSelIndex)
            var params=new Object();
            params['cid']           = viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"cid")
            params['template_id']   = viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"category_attr_template_id")
            params['inputSearch']   = ""
            params['searchType']    = "NA";
            params['fieldList']     = 'genre';
            params['searchBy']      = 'genre'
            params['orderBy']       = 'genre';
            params['groupBy']       = 'genre'
            dataController.keywordSearch.getData(params,genreListReady,1);
        }else if(tid=="media_listing"){
            dataController.getNextMenu([cid,tid],radioListReady)
        }
    }
    function radioListReady(dModel){
        isAlbumFromArtist=false
        musicMediaMenu.setMusicModel(dModel)
        viewHelper.synopsisModel=dModel
        musicMediaMenu.setMusicDelegate(compAlbumList)
    }
    function artistListReady(dModel){
        isAlbumFromArtist=false
        artistModel=dModel
        musicMediaMenu.setMusicModel(dModel)
        musicMediaMenu.setMusicDelegate(compArtistList)
    }
    function albumListReady(dModel){
        isAlbumFromArtist=true
        musicMediaMenu.setMusicModel(dModel)
        musicMediaMenu.setMusicDelegate(compAlbumList)
    }
    function genreListReady(dModel){
        musicMediaMenu.setMusicModel(dModel)
        musicMediaMenu.setMusicDelegate(compArtistList)
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
        }
    }
    property QtObject subMenuBackground
    Component{
        id:compSubMenuBackground
        Item{
            anchors.top:parent.top
            anchors.horizontalCenter:background.horizontalCenter
            anchors.fill:parent
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.background//.bg_music
            }
            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.mediaImagePath+viewHelper.catIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText: [(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"title"):viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"title"),configTag["menu_"+orientationType].submenu.text_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }
    property QtObject musicMediaMenu
    Component{
        id:compMusicMediaMenu
        Item{
            function showAlbumList(val){visible=val}
            anchors.horizontalCenter: background.horizontalCenter
            height: parent.height
            width:parent.width
            property alias lView: mediaMenuLv
            function setMusicModel(dModel){
                mediaMenuLv.model=dModel
                viewHelper.currentAlbumModel=dModel
            }
            function setMusicDelegate(del){
                mediaMenuLv.delegate=del
            }
            Image{
                id:upArrow
                visible:(orientationType=='p' && mediaMenuLv.contentY>0)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.up_arrow
                anchors.top:(orientationType=='p')? parent.top:undefined;
                anchors.topMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_up'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Image{
                id:upperDivLine
                visible:(orientationType=='p')
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline:""
                anchors.bottom:mediaMenuLv.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:mediaMenuLv
                width:(catSelected=="artist_listing"||catSelected=="genre_listing" )?qsTranslate('','SubMenu_container_'+orientationType+'_w'):qsTranslate('','Listing_container_'+orientationType+'_w')
                height:(catSelected=="artist_listing"||catSelected=="genre_listing" )?qsTranslate('','SubMenu_container_'+orientationType+'_h'):qsTranslate('','Listing_container_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin:(catSelected=="artist_listing"||catSelected=="genre_listing" )?qsTranslate('','SubMenu_container_'+orientationType+'_tm'):qsTranslate('','Listing_container_'+orientationType+'_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                orientation:(pif.getOrientationType()=='p')?ListView.Vertical:ListView.Horizontal
                boundsBehavior: ListView.StopAtBounds
                snapMode: ListView.SnapToItem
                cacheBuffer: width
                clip:true
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                function mediaSelected(index){
                    currentIndex=index
                    viewHelper.mediaSelected=index
                    var currentSelection=viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"category_attr_template_id")
                    if(currentSelection=="music_listing"){
                        isAlbumFromArtist=false
                        dataController.getAudioTrackList([model.getValue(index,"mid"),currentSelection,model.getValue(index,"rating")],viewData.tracklistReady)
                    }
                    else if(currentSelection=="artist_listing"){
                        atAlbumLevel=true
                        console.log("index | artist listing "+index)
                        if(isAlbumFromArtist==false){
                            var params=new Object();
                            params['cid']           = viewData.subMenuModel.getValue(0,"cid")
                            params['template_id']   = viewData.subMenuModel.getValue(0,"category_attr_template_id")
                            params['inputSearch']   = (artistModel.getValue(index,"artist")).toLowerCase();
                            params['searchType']    = "full"  ;
                            params['fieldList']     = 'title,mid,rating,media_poster_filename_41 as poster_p,media_poster_filename_41 as poster_l';
                            params['searchBy']      = 'artist'
                            params['orderBy']       = 'title';
                            params['groupBy']       = ''
                            dataController.keywordSearch.getData(params,albumListReady,99);
                        }
                        else {
                            dataController.getAudioTrackList([model.getValue(index,"mid"),currentSelection,model.getValue(index,"rating")],viewData.tracklistReady)
                        }
                    }else if(currentSelection=="genre_listing"){
                        atAlbumLevel=true
                        viewHelper.isFromGenre=true
                        viewHelper.genreValue=[viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"cid"),
                                               viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"category_attr_template_id"),
                                               mediaMenuLv.model.getValue(mediaMenuLv.currentIndex,"genre").toLowerCase()]
                        var params=new Object();
                        params['cid']           = viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"cid")
                        params['template_id']   = viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"category_attr_template_id")
                        params['inputSearch']   = mediaMenuLv.model.getValue(mediaMenuLv.currentIndex,"genre").toLowerCase();
                        params['searchType']    = "full"  ;
                        params['fieldList']     = 'title,mid,rating,aggregate_parentmid,duration';
                        params['searchBy']      = 'genre'
                        params['orderBy']       = 'title';
                        params['groupBy']       = 'title'
                        params['isParentAggSearch'] = true
                        dataController.keywordSearch.getData(params,viewData.tracklistReady,2);
                    }else if(currentSelection=='media_listing'){
                        viewHelper.radioAlbumSelected=index
                        viewController.loadScreen("radio_synopsis")
                    }


                }
                onMovementEnded: {
                    if(indexAt(contentX,contentY)>=0){
                        var cIndex=indexAt(contentX+10,contentY+10);
                        if(pif.getOrientationType()!="p" && mediaMenuLv.model.count<=3){
                            cIndex=0;
                        }else {
                            var resetPosition=(mediaMenuLv.model.count-3);
                            if(resetPosition>0){
                                cIndex=(cIndex>resetPosition)?(resetPosition):cIndex;
                            }else{
                                cIndex=0;
                            }
                        }
                        currentIndex=cIndex;
                    }
                }
            }
            Image{
                id:leftArrow
                visible:(orientationType=='l'&& mediaMenuLv.contentX>0)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.left_arrow
                anchors.right:(orientationType=='l')? mediaMenuLv.left:undefined;
                anchors.rightMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: mediaMenuLv.verticalCenter
            }
            Image{
                id:rightArrow
                visible:(orientationType=='l'&& mediaMenuLv.currentIndex<mediaMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.right_arrow
                anchors.left:(orientationType=='l')? mediaMenuLv.right:undefined;
                anchors.leftMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: mediaMenuLv.verticalCenter
            }
            Image{
                id:downArrow
                visible:(orientationType=='p'&& mediaMenuLv.currentIndex<(mediaMenuLv.model.count-3))
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.down_arrow
                anchors.bottom:(orientationType=='p')? parent.bottom:undefined;
                anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_down'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
    Component{
        id:compAlbumList
        AlbumList{
            id:albumList
            width:qsTranslate('','Listing_cells_'+orientationType+'_w')
            height:qsTranslate('','Listing_cells_'+orientationType+'_h')
        }
    }
    Component{
        id:compArtistList
        ArtistList{
            id:artistList
            width:qsTranslate('','SubMenu_cells_'+orientationType+'_w');
            height:qsTranslate('','SubMenu_cells_'+orientationType+'_h');
        }
    }
}
