import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import ImageMask 1.0
SmartLoader {
    id: hospMediaListing
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'

    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSubMenuBackground,"subMenuBackground")
        push(compMediaMenu,"mediaMenu")
    }

    function init(){
        core.log("HospListing | init called")
        setHeaderFooter()
        getHospCategoryMenu()
    }

    function reload(){
        init()
    }

    function getHospCategoryMenu(){
        viewHelper.hospValue = viewHelper.hospCatalogModel.at(viewHelper.subMenuSelIndex)
        core.dataController.hospitality.getNextList(viewHelper.hospValue,viewHelper.hospValue.category_node_id,"","",nextHosListCategoryModelReady)
    }

    function nextHosListCategoryModelReady(dModel,status,type){
        core.debug("HospListing | nextHosListCategoryModelReady called | status : " + status + " | dModel.count : " + dModel.count+" | type: "+type);
        if(dModel.count==0){viewController.showScreenPopup(27);return;}
        for (var i=0; i<dModel.count;i++){
            var title = dModel.getValue(i,"title")
            core.log("Inside for | HospListing title = "+title)
            core.log("Inside for | HospListing category_node_id = "+dModel.getValue(i,"category_node_id"))
        }
        viewHelper.hospitalitCategoryMenuModel=dModel
        if(status){
            if(type == "category"){
                mediaMenu.visible=true
            }
        }
    }

    function clearOnExit(){
        core.debug("HospListing Listing | clearOnExit called")
        removeHeaderFooter()
    }

    onLoaded: { core.debug("HospListing Listing | onLoaded called") }

    onOrientationTypeChanged: {
        mediaMenu.resetDelegate()
    }

    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        //viewHelper.mediaSelected=7
        viewController.loadScreen("hospitality")
    }

    function mediaSelected(index){
        viewHelper.categoryIndex=index
        viewHelper.launchAppVkarma=mediaMenu.lView.model.getValue(index,"category_attr_template_id")
        viewHelper.hosp_item_id =  mediaMenu.lView.model.getValue(viewHelper.mediaSelected,"item_id");
        viewController.loadScreen("hospitality_Synopsis")
    }

    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:hospMediaListing.width;
            height:hospMediaListing.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag["meal_"+orientationType].listing.background
            }
        }
    }
    property QtObject subMenuBackground
    Component{
        id:compSubMenuBackground
        Item{
            anchors.top:parent.top
            anchors.horizontalCenter:background.horizontalCenter
            anchors.fill:parent

            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.mediaImagePath+viewHelper.catIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText: [viewData.mediaModel.getValue(viewHelper.mediaSelected,"title"),configTag["menu_"+orientationType].submenu.text_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }

    property QtObject mediaMenu
    Component{
        id:compMediaMenu
        Item{
            anchors.horizontalCenter: background.horizontalCenter
            height: parent.height
            width:parent.width
            property alias lView: mediaMenuLv
            function resetDelegate(){
                mediaMenuLv.delegate=viewHelper.emptyDel
                mediaMenuLv.delegate=discoverListing
            }



            Image{
                id:upArrow
                visible:(orientationType=='p' && mediaMenuLv.contentY>0)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.up_arrow
                anchors.top:(orientationType=='p')? parent.top:undefined;
                anchors.topMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_up'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Image{
                id:upperDivLine
                visible:(orientationType=='p')
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline:""
                anchors.bottom:mediaMenuLv.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:mediaMenuLv
                width:qsTranslate('','Listing_container_'+orientationType+'_w')
                height:qsTranslate('','Listing_container_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','Listing_container_'+orientationType+'_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                orientation:(pif.getOrientationType()=='p')?ListView.Vertical:ListView.Horizontal
                boundsBehavior: ListView.StopAtBounds
                snapMode: ListView.SnapToItem
                cacheBuffer: width
                model:viewHelper.hospitalitCategoryMenuModel
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                delegate:discoverListing
                clip:true

                onMovementEnded: {
                    if(indexAt(contentX,contentY)>=0){
                        var cIndex=indexAt(contentX+10,contentY+10);
                        if(pif.getOrientationType()!="p" && mediaMenuLv.model.count<=3){
                            cIndex=0;
                        }else {
                            var resetPosition=(mediaMenuLv.model.count-3);
                            if(resetPosition>0){
                                cIndex=(cIndex>resetPosition)?(resetPosition):cIndex;
                            }else{
                                cIndex=0;
                            }
                        }
                        currentIndex=cIndex;
                    }
                }
            }
            Image{
                id:leftArrow
                visible:(orientationType=='l'&& mediaMenuLv.contentX>0)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.left_arrow
                anchors.right:(orientationType=='l')? mediaMenuLv.left:undefined;
                anchors.rightMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: mediaMenuLv.verticalCenter
            }
            Image{
                id:rightArrow
                visible:(orientationType=='l'&& mediaMenuLv.currentIndex<mediaMenuLv.model.count-3)
                source:(viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.right_arrow)
                anchors.left:(orientationType=='l')? mediaMenuLv.right:undefined;
                anchors.leftMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: mediaMenuLv.verticalCenter
            }
            Image{
                id:downArrow
                visible:(orientationType=='p'&& mediaMenuLv.currentIndex<mediaMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.down_arrow
                anchors.bottom:(orientationType=='p')? parent.bottom:undefined;
                anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_down'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
    Component{
        id:discoverListing
        Item{
            width:qsTranslate('','Listing_cells_'+orientationType+'_w')
            height:qsTranslate('','Listing_cells_'+orientationType+'_h')
            property bool isPressed: false

            ViewText{
                id:mediaName
                varText:[value.title,(isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].listing.text_color_press:configTag["menu_"+orientationType].main.text_color,qsTranslate('','Listing_text_'+orientationType+'_fontsize')]
                height:qsTranslate('','Listing_text_'+orientationType+'_h')
                width:qsTranslate('','Listing_text_'+orientationType+'_w')
                anchors.verticalCenter:parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                verticalAlignment:Text.AlignVCenter
                horizontalAlignment:Text.AlignHCenter
                elide: Text.ElideRight
//                maximumLineCount:2
                wrapMode: Text.WordWrap
                lineHeight:qsTranslate('','Listing_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }
            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline:""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !mediaMenu.lView.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }
                onReleased: {
                    isPressed=false
                    mediaSelected(index)
                }
            }
            Component.onCompleted: {
                if(mediaMenu.lView.currentIndex!=0)mediaMenu.lView.currentIndex=0
            }
        }
    }
}
