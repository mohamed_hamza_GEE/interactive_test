import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import ImageMask 1.0
SmartLoader {
    id: mediaListing
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'


    /**************************************************************************************************************************/
    Connections{
        target:viewData
        onMediaModelReady:{
            if(viewHelper.launchAppVkarma=="connecting_gate" ||viewHelper.launchAppVkarma=="airport_map"||viewHelper.launchAppVkarma=="maps"){
                var index;
                if(viewHelper.launchAppVkarma=="connecting_gate" )
                    index=core.coreHelper.searchEntryInModel(viewData.mediaModel,"category_attr_template_id","connecting_gate")
                else if(viewHelper.launchAppVkarma=="airport_map")
                    index=core.coreHelper.searchEntryInModel(viewData.mediaModel,"category_attr_template_id","airport_map")
                else
                    index=core.coreHelper.searchEntryInModel(viewData.mediaModel,"category_attr_template_id","maps")
                console.log("onLaunchApp "+index)
                mediaSelected(index)
            }
        }
    }
    Connections{
        target:(visible)?viewHelper:null
        onShoppingLangUpdate:{
            console.log(" onShopping ")
            mediaMenu.lView.model=core.dataController.shopping.getItemModel()
        }
    }

    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSubMenuBackground,"subMenuBackground")
        push(compMediaMenu,"mediaMenu")
    }
    function init(){
        console.log("Media Listing | init called")
        setHeaderFooter()
        getMediaTimer.restart()
    }
    function reload(){
        console.log("Media Listing | reload called")
        init()
    }
    function clearOnExit(){
        console.log("Media Listing | clearOnExit called")
        removeHeaderFooter()
    }
    onLoaded: { core.debug("Media Listing | onLoaded called") }
    onOrientationTypeChanged: {
        mediaMenu.resetDelegate()
    }
    Timer{
        id:getMediaTimer
        interval:100
        onTriggered: {
            showMedia()
        }
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        if(viewHelper.catSelected=="shopping"){
            viewController.loadScreen("shopping")
        }else{
            viewController.loadPrev()
        }
    }
    function showMedia(){
        if(viewHelper.catSelected=="discover"){
            viewHelper.selectedIndex=core.coreHelper.searchEntryInModel(viewData.mainMenuModel,"category_attr_template_id","discover")
            viewHelper.catIcon=viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"poster_"+orientationType)
            var cid=viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"cid")
            var tid=viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
            viewHelper.tempId=tid
            dataController.getNextMenu([cid,tid],viewData.mediaMenuReady)
        }else{
        }
    }





    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
        }
    }
    property QtObject subMenuBackground
    Component{
        id:compSubMenuBackground
        Item{
            anchors.top:parent.top
            anchors.horizontalCenter:background.horizontalCenter
            anchors.fill:parent
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:(viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.background:(viewHelper.catSelected=="discover" || viewHelper.catSelected=="games"?viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu["bg_"+viewHelper.catSelected]:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.background)//.bg_music
            }
            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.mediaImagePath+viewHelper.catIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText: [viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"title"),configTag["menu_"+orientationType].submenu.text_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }
    function initializeExpVod(dModel){
        var params=new Object();
        params["mid"]=dModel.getValue(0,"mid")
        params["soundtrackLid"]=(dModel.getValue(0,"soundtrack_list").split(",")[0]=="")?viewHelper.getDefaultSndLang():dModel.getValue(0,"soundtrack_list").split(",")[0]
        params["subtitleLid"]=1
        params["aggregateMid"]=dModel.getValue(0,"mid")
        params["elapsedTime"]=0
        params["ignoreSystemAspectRatio"]=true
        params["rating"]=dModel.getValue(0,"rating")
        pif.vkarmaToSeat.playVodByMid(params)

    }
    function mediaSelected(index){
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+viewHelper.launchAppVkarma)
        viewHelper.mediaSelected=index
        viewHelper.launchAppVkarma=mediaMenu.lView.model.getValue(index,"category_attr_template_id")
        //                    var ret = dataController.mediaServices.getMidBlockStatus(viewData.mediaModel.getValue(index,"mid"),viewData.mediaModel.getValue(index,"rating"));
        //                    if(!ret){
        if(viewHelper.launchAppVkarma=='airline_exp'){
            console.log("Going in mediaSelected function");
            dataController.getNextMenu([mediaMenu.lView.model.getValue(index,"cid"),mediaMenu.lView.model.getValue(index,"category_attr_template_id") ],initializeExpVod)
            return;
        }
        if(current.template_id=='tv_listing'){
            viewHelper.tvSeriesTitle=viewData.mediaModel.getValue(index,"title")
        }
        if(current.template_id=='media_listing')
            viewController.loadScreen("radio_synopsis")
        else{
            if(viewHelper.catSelected=="shopping"){
                viewHelper.shop_item_id =  mediaMenu.lView.model.getValue(viewHelper.mediaSelected,"item_id");
            }
            viewController.loadScreen("synopsis")
        }
    }




    property QtObject mediaMenu
    Component{
        id:compMediaMenu
        Item{
            anchors.horizontalCenter: background.horizontalCenter
            height: parent.height
            width:parent.width
            property alias lView: mediaMenuLv
            function resetDelegate(){
                mediaMenuLv.delegate=viewHelper.emptyDel
                mediaMenuLv.delegate=(viewHelper.catSelected=='discover')?discoverListing:mediaMenuDel
            }


            Image{
                id:upArrow
                visible:(orientationType=='p' && mediaMenuLv.contentY>0)
                source:(viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.up_arrow:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.up_arrow
                anchors.top:(orientationType=='p')? parent.top:undefined;
                anchors.topMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_up'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Image{
                id:upperDivLine
                visible:(orientationType=='p')
                source:(orientationType=='p')?(viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.divline:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline:""
                anchors.bottom:mediaMenuLv.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:mediaMenuLv
                width:qsTranslate('','Listing_container_'+orientationType+'_w')
                height:qsTranslate('','Listing_container_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','Listing_container_'+orientationType+'_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                orientation:(pif.getOrientationType()=='p')?ListView.Vertical:ListView.Horizontal
                boundsBehavior: ListView.StopAtBounds
                snapMode: ListView.SnapToItem
                cacheBuffer: width
                model:(viewHelper.catSelected=="shopping")?viewHelper.shoppingMediaModel:viewData.mediaModel
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                delegate:(viewHelper.catSelected=='discover')?discoverListing:mediaMenuDel
                clip:true


                onMovementEnded: {
                    if(indexAt(contentX,contentY)>=0){
                        var cIndex=indexAt(contentX+10,contentY+10);
                        if(pif.getOrientationType()!="p" && mediaMenuLv.model.count<=3){
                            cIndex=0;
                        }else {
                            var resetPosition=(mediaMenuLv.model.count-3);
                            if(resetPosition>0){
                                cIndex=(cIndex>resetPosition)?(resetPosition):cIndex;
                            }else{
                                cIndex=0;
                            }
                        }
                        currentIndex=cIndex;
                    }
                }
            }
            Image{
                id:leftArrow
                visible:(orientationType=='l'&& mediaMenuLv.contentX>0)
                source:(viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.left_arrow:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.left_arrow
                anchors.right:(orientationType=='l')? mediaMenuLv.left:undefined;
                anchors.rightMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: mediaMenuLv.verticalCenter
            }
            Image{
                id:rightArrow
                visible:(orientationType=='l'&& mediaMenuLv.currentIndex<mediaMenuLv.model.count-3)
                source:(viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.right_arrow:(viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.right_arrow)
                anchors.left:(orientationType=='l')? mediaMenuLv.right:undefined;
                anchors.leftMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: mediaMenuLv.verticalCenter
            }
            Image{
                id:downArrow
                visible:(orientationType=='p'&& mediaMenuLv.currentIndex<mediaMenuLv.model.count-3)
                source:(viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.down_arrow:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.down_arrow
                anchors.bottom:(orientationType=='p')? parent.bottom:undefined;
                anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_down'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
    Component{//Media Delegate
        id:mediaMenuDel
        Item{
            width:qsTranslate('','Listing_cells_'+orientationType+'_w')
            height:qsTranslate('','Listing_cells_'+orientationType+'_h')
            property bool isPressed: false
            Image{
                id:posterHolder
                smooth:true
                anchors.centerIn: posterImg
                source:(isPressed)?((viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.poster_holder:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.poster_holder):""
            }
            Image{
                id:posterImg
                width:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_w'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_w'):qsTranslate('','Listing_poster_disc_'+orientationType+'_w')
                height:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_h'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_h'):qsTranslate('','Listing_poster_disc_'+orientationType+'_h')
                anchors.left: parent.left;anchors.leftMargin:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_lm'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_lm'):qsTranslate('','Listing_poster_disc_'+orientationType+'_lm')
                anchors.top:parent.top;anchors.topMargin:(current.template_id=='movies_listing'||current.template_id=='tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_tm'):(current.template_id=='games_listing'||current.template_id=='music_listing'||current.template_id=='artist_listing'||current.template_id=='genre_listing'||current.template_id=='playlist'||current.template_id=='media_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_tm'):qsTranslate('','Listing_poster_disc_'+orientationType+'_tm')
                source:(orientationType=='p')?(viewHelper.catSelected=="shopping")?value.item_thumb:viewHelper.mediaImagePath+poster_p:(viewHelper.catSelected=="shopping")?value.item_thumb:viewHelper.mediaImagePath+poster_l
                Rectangle{anchors.fill:parent;color:"transparent"}
            }
            ViewText{
                id:mediaName
                varText:[(viewHelper.catSelected=="shopping")?value.title:title,(isPressed)?((viewHelper.catSelected=="shopping"))?configTag["shop_"+orientationType].listing.text_color_press:configTag[viewHelper.catSelected+"_"+orientationType].listing.text_color_press:configTag[((viewHelper.catSelected=="shopping")?"shop":viewHelper.catSelected)+"_"+orientationType].listing.text_color,qsTranslate('','Listing_text_'+orientationType+'_fontsize')]
                anchors.left:(orientationType=='p')? posterImg.right:undefined;anchors.leftMargin: (orientationType=='p')?qsTranslate('','Listing_text_'+orientationType+'_lm'):0
                anchors.top:(orientationType=='l')?posterImg.bottom:undefined;anchors.topMargin:(orientationType=='l')?qsTranslate('','Listing_text_l_tm'):0
                height:qsTranslate('','Listing_text_'+orientationType+'_h')
                width:qsTranslate('','Listing_text_'+orientationType+'_w')
                anchors.verticalCenter:(orientationType=='p')? posterImg.verticalCenter:undefined
                anchors.horizontalCenter: (orientationType=='l')?posterImg.horizontalCenter:undefined
                verticalAlignment:(orientationType=='p')?Text.AlignVCenter:undefined
                horizontalAlignment: (orientationType=='l')?Text.AlignHCenter:undefined
                elide: Text.ElideRight
                maximumLineCount:2
                wrapMode: Text.WordWrap
                lineHeight:qsTranslate('','Listing_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }
            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:(orientationType=='p')?((viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.divline:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline):""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !mediaMenu.lView.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }

                onReleased: {
                    isPressed=false
                    mediaSelected(index)
                }
            }
            Component.onCompleted: {
                if(mediaMenu.lView.currentIndex!=0)mediaMenu.lView.currentIndex=0
            }

        }
    }
    Component{
        id:discoverListing
        Item{
            width:qsTranslate('','Listing_cells_'+orientationType+'_w')
            height:qsTranslate('','Listing_cells_'+orientationType+'_h')
            property bool isPressed: false
            Image{
                id:posterHolder
                smooth:true
                anchors.centerIn: posterImg
                source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.poster_holder:""
            }
            MaskedImage{
                id:posterImg
                width:qsTranslate('','Listing_poster_disc_'+orientationType+'_w')
                height:qsTranslate('','Listing_poster_disc_'+orientationType+'_h')
                anchors.left: parent.left;
                anchors.leftMargin:qsTranslate('','Listing_poster_disc_'+orientationType+'_lm')
                anchors.top:parent.top;
                anchors.topMargin:qsTranslate('','Listing_poster_disc_'+orientationType+'_tm')
                source:(orientationType=="l")?viewHelper.mediaImagePath+poster_l:viewHelper.mediaImagePath+poster_p
                maskSource:viewController.settings.asset0ImagePath+qsTranslate('','Listing_poster_disc_'+orientationType+'_source')
            }

            ViewText{
                id:mediaName
                varText:[title,(isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].listing.text_color_press:configTag[viewHelper.catSelected+"_"+orientationType].listing.text_color,qsTranslate('','Listing_text_'+orientationType+'_fontsize')]
                anchors.left:(orientationType=='p')? posterImg.right:undefined;anchors.leftMargin: (orientationType=='p')?qsTranslate('','Listing_text_'+orientationType+'_lm'):0
                anchors.top:(orientationType=='l')?posterImg.bottom:undefined;anchors.topMargin:(orientationType=='l')?qsTranslate('','Listing_text_l_tm'):0
                height:qsTranslate('','Listing_text_'+orientationType+'_h')
                width:qsTranslate('','Listing_text_'+orientationType+'_w')
                anchors.verticalCenter:(orientationType=='p')? posterImg.verticalCenter:undefined
                anchors.horizontalCenter: (orientationType=='l')?posterImg.horizontalCenter:undefined
                verticalAlignment:(orientationType=='p')?Text.AlignVCenter:undefined
                horizontalAlignment: (orientationType=='l')?Text.AlignHCenter:undefined
                elide: Text.ElideRight
                maximumLineCount:2
                wrapMode: Text.Wrap
                lineHeight:qsTranslate('','Listing_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }
            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline:""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !mediaMenuLv.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }
                onReleased: {
                    isPressed=false
                    viewHelper.isCgSelected=(mediaMenuLv.model.getValue(index,"category_attr_template_id")=='connecting_gate')?true:false
                    mediaSelected(index)
                }
            }
            Component.onCompleted: {
                if(mediaMenuLv.currentIndex!=0)mediaMenuLv.currentIndex=0
            }
        }
    }
}

