// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"


SmartLoader {
    id: synopsis
    /**************************************************************************************************************************/
    property variant configTag: configTool.config
    property variant configMenu: (viewHelper.isKids(current.template_id))?"kids_menu_"+orientationType:"menu_"+orientationType
    property string orientationType: (visible)?pif.getOrientationType():'p'

    /**************************************************************************************************************************/

    Connections{
        target:(visible)?viewHelper:null
        onHospitalityLangUpdate:{
            console.log(" onShopping ")
            synopsisPanel.lView.model=core.dataController.hospitality.getItemModel()
        }
    }
    /**********************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSynopsisPanel,"synopsisPanel")
    }
    function init(){
        console.log("HospSynopsis | init called")
        setHeaderFooter()
        getListingMenu()
    }
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        viewController.loadScreen("hospitality_listing")
    }

    function clearOnExit(){
        core.debug("HospSynopsis | clearOnExit called")
        viewHelper.mediaSelected=7
        removeHeaderFooter()
        viewHelper.launchAppVkarma=""
        viewHelper.hospitalityMediaModel=viewHelper.blankModel
        synopsisPanel.setDelegate("empty")
    }

    onLoaded: { core.debug("HospSynopsis | onLoaded called") }

    function getListingMenu(){
        viewHelper.hospValue = viewHelper.hospitalitCategoryMenuModel.at(viewHelper.categoryIndex)
        core.dataController.hospitality.getNextList(viewHelper.hospValue,viewHelper.hospValue.category_node_id,"","",nextHospListModelReady)
    }

    function nextHospListModelReady(dModel,status,type){
        core.log("HospSynopsis.qml | nextListModelReady dModel.count = "+dModel.count)
        if(dModel.count==0){
            viewController.showScreenPopup(27);
            return;}
        for (var i=0; i<dModel.count;i++){
            var sub_title = dModel.getValue(i,"sub_title")
            var item_id = dModel.getValue(i,"item_id")
            var title = dModel.getValue(i,"title")
            core.log("Inside For | sub_title = "+sub_title)
            core.log("Inside For | sub_title = "+title)
            core.log("Inside For | item_id = "+item_id)
        }
        if(status){
            if(type == "item"){
                viewHelper.hospitalityMediaModel=dModel
                core.debug("HospSynopsis.qml | synopsisModelReady | status : "+status)
                core.debug("HospSynopsis.qml | synopsisModelReady |  viewHelper.hospitalityMediaModel : "+ viewHelper.hospitalityMediaModel.count)
                core.debug("HospSynopsis.qml | synopsisModelReady | viewHelper.hospValue.sub_title : "+viewHelper.hospValue.sub_title)
                synopsisPanel.setModel(viewHelper.blankModel)
                synopsisPanel.setModel(viewHelper.hospitalityMediaModel)
                synopsisPanel.changeUserInteraction(true)
                synopsisPanel.setCurrentIndex(viewHelper.mediaSelected)
                synopsisPanel.setDelegate("empty")
                synopsisPanel.setDelegate('hospitality_Synopsis')

            }
        }
    }

    property QtObject background
    Component{
        id:compBackground
        Item{
            width:synopsis.width;
            height:synopsis.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }

            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag["meal_"+orientationType].listing.background
            }
            Image{
                id:bg_syn
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag["meal_"+orientationType].synopsis.panel
            }

        }
    }
    property QtObject synopsisPanel
    Component{
        id:compSynopsisPanel
        Item{
            id:synopsis_panel
            anchors.fill:background;

            property alias lView:synopsisListView
            function changeUserInteraction(val){synopsisListView.interactive=val}
            function setCurrentIndex(index){
                synopsisListView.currentIndex=index
                core.debug("setCurrentIndex | index "+synopsisListView.currentIndex)
                synopsisListView.positionViewAtIndex(synopsisListView.currentIndex,ListView.Beginning)
            }
            function setDelegate(tempId){
                synopsisListView.delegate=hospDel
            }
            function setModel(dModel){
                synopsisListView.model=dModel
            }
            function getNormIcon(typ){
                if(typ=="lunch"){return configTag["meal_"+orientationType].synopsis.optn_title_icon_lunch}
                else if(typ=="breakfast"){return configTag["meal_"+orientationType].synopsis.optn_title_icon_breakfast}
                else if(typ=="wines"){return configTag["meal_"+orientationType].synopsis.optn_title_icon_liquors}
                else if(typ=="beverages"){return configTag["meal_"+orientationType].synopsis.optn_title_icon_beverages}
            }

            Image{
                id:fade_image
//                width:synopsis.width
                source:viewHelper.configToolImagePath+configTag["meal_"+orientationType].synopsis.scroll_fadeimage
                visible:(synopsisListView.height+synopsisListView.contentY<synopsisListView.contentHeight)?true:false
                anchors.bottom: synopsisListView.bottom
//                anchors.bottomMargin: 25
            }
            Image{
                id:leftArrow
                visible:  viewHelper.categoryIndex>0
                source:viewHelper.configToolImagePath+configTag["meal_"+orientationType].synopsis.left_arrow
                anchors.left:parent.left;anchors.leftMargin: qsTranslate('','Meal_arrow_'+orientationType+'_hgap')
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','Meal_arrow_'+orientationType+'_tm')
                MouseArea{
                    anchors.left: parent.left
                    anchors.leftMargin:  - parent.width
                    anchors.top: parent.top
                    anchors.topMargin: - parent.height
                    width: parent.width *4;
                    height: parent.height *3
                    onClicked: {
                        viewHelper.categoryIndex--
                        getListingMenu()
                    }

                }

            }
            Image{
                id:rightArrow
                visible:  viewHelper.categoryIndex<viewHelper.hospitalitCategoryMenuModel.count-1
                source:viewHelper.configToolImagePath+configTag["meal_"+orientationType].synopsis.right_arrow
                anchors.right:parent.right;anchors.rightMargin: qsTranslate('','Meal_arrow_'+orientationType+'_hgap')
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','Meal_arrow_'+orientationType+'_tm')
                MouseArea{
                    anchors.left: parent.left
                    anchors.leftMargin:  - parent.width
                    anchors.top: parent.top
                    anchors.topMargin: - parent.height
                    width: parent.width *4;
                    height: parent.height *3
                    onClicked: {
                        viewHelper.categoryIndex++
                        getListingMenu()
                    }
                }

            }
            ViewText{
                id:catsubTitle
                width:qsTranslate('','Meal_title_'+orientationType+'_w')
                height:paintedHeight//qsTranslate('','Meal_title_'+orientationType+'_h')
                anchors.top:parent.top
                anchors.topMargin: qsTranslate('','Meal_title_'+orientationType+'_tm')
                varText: [viewHelper.hospValue.sub_title,configTag['meal_'+orientationType].synopsis.title_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                anchors.horizontalCenter:parent.horizontalCenter
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                elide: Text.ElideRight
                maximumLineCount: 2
                lineHeight: qsTranslate('','Meal_title_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }

            Rectangle{
                id:categoryTile
                color:"transparent"
                //                opacity:.5
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
//                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
//                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.configToolImagePath+synopsis_panel.getNormIcon(viewHelper.hosp_catalog_type)
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;
                    anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }

                ViewText{
                    id:catTitle
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    varText: [viewHelper.hospValue.title,configTag['meal_'+orientationType].synopsis.optn_title_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
            ListView{
                id:synopsisListView
                snapMode:ListView.SnapToItem;
                width:qsTranslate('','Meal_cont_txt_'+orientationType+'_w')
                height:qsTranslate('','Meal_cont_txt_'+orientationType+'_h')//qsTranslate('','Meal_cont_txt_'+orientationType+'_h')
                anchors.horizontalCenter:parent.horizontalCenter
                anchors.top:catsubTitle.bottom;anchors.topMargin:qsTranslate('','Meal_cont_txt_'+orientationType+'_tm')
                boundsBehavior:ListView.StopAtBounds;
//                preferredHighlightBegin:0;
//                preferredHighlightEnd:height;
                highlightRangeMode:ListView.StrictlyEnforceRange;
                //                orientation:ListView.Vertical
                clip:true;
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                interactive: true
                onContentXChanged: console.log("onContentXChanged "+contentX)
                onCurrentIndexChanged: console.log("onCurrentIndexChanged "+currentIndex)
                onModelChanged: { console.log("onModelChanged "+contentX)}
            }

            Component{
                id:hospDel
                Item{
                    id:synopisisPanel
                    width:qsTranslate('','Meal_cont_txt_'+orientationType+'_w')
                    height:itemTilte.paintedHeight+description_Txt.paintedHeight+parseInt(qsTranslate('','Meal_desc_txt_'+orientationType+'_vgap'),10)
//                    color:"transparent"
                    anchors.horizontalCenter:parent.horizontalCenter
                    ViewText{
                        id:itemTilte
                        height:paintedHeight
                        maximumLineCount: 2
                        lineHeight:qsTranslate('','Meal_menu_title_'+orientationType+'_lh')
                        lineHeightMode: Text.FixedHeight
                        //                        anchors.topMargin: qsTranslate('','Meal_title_'+orientationType+'_tm')
                        varText:[value.title+"-",configTag['meal_'+orientationType].synopsis.menu_title_color, qsTranslate('','Meal_title_'+orientationType+'_fontsize')]
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }

                    ViewText{
                        id:itemCalorie
//                        height:qsTranslate('','Meal_cal_txt_'+orientationType+'_h')
                        height:qsTranslate('','Meal_menu_title_'+orientationType+'_h')
                        anchors.left:itemTilte.right;
                        maximumLineCount: 2
                        varText:[(value.calories)?value.calories+"cal.":"",configTag['meal_'+orientationType].synopsis.menu_cal_color,qsTranslate('','Meal_cal_txt_'+orientationType+'_fontsize')]
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                    ViewText{
                        id:description_Txt
                        anchors.top:itemTilte.bottom
                        anchors.topMargin: qsTranslate('','Meal_desc_txt_'+orientationType+'_tm')
                        width:qsTranslate('','Meal_cont_txt_'+orientationType+'_w')
                        varText: [value.description_short,configTag['meal_'+orientationType].synopsis.menu_desc_color,qsTranslate('','Meal_desc_txt_'+orientationType+'_fontsize')]
                        wrapMode: Text.WordWrap
                    }
                }

            }

        }
    }
}

