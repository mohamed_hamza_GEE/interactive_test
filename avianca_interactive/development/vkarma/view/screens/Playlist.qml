import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: playlistScreen
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property variant playlistNames: [configTool.language.music.playlist1,configTool.language.music.playlist2,configTool.language.music.playlist3]
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSubMenuBackground,"subMenuBackground")
        push(compPlaylisting,"playlisting")
    }
    function init(){
        console.log("playlistScreen.qml | init")
        viewController.hideScreenPopup()
        viewHelper.isFromGenre=false
        setHeaderFooter()
    }
    function reload(){
        console.log("playlistScreen.qml | reload")
        init()
    }
    function clearOnExit(){
        console.log("playlistScreen.qml | clearOnexit")
        removeHeaderFooter()
    }
    onLoaded: {}
    Connections{
        target:(visible)?viewData:null
        onPlayListModelReady:{
            viewController.loadScreen("tracklist")
        }
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        viewController.loadPrev()
    }
    function getTracks(index){
        var midList =pif.vkarmaToSeat.getAllPlaylistMids({"playlistIndex":(index+1)*-1,"mediaType":"aod"})
        return " ("+midList.length+" / 99 )"
    }

    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
        }
    }
    property QtObject subMenuBackground
    Component{
        id:compSubMenuBackground
        Item{
            anchors.top:parent.top
            anchors.horizontalCenter:background.horizontalCenter
            anchors.fill:parent
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag["music_"+orientationType].listing.background//.bg_music
            }
        }
    }
    property QtObject playlisting
    Component{
        id:compPlaylisting
        Item{
            id:mainCont
            anchors.horizontalCenter: background.horizontalCenter
            height: background.height
            width:background.width
            Image{
                id:trackPanel
                width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');height:qsTranslate('','Common_whitepanel_'+orientationType+'_h')
                source:viewHelper.configToolImagePath+configTag["music_"+orientationType].tracklist.panel
                anchors.horizontalCenter:parent.horizontalCenter
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','Common_whitepanel_'+orientationType+'_tm')
            }
            Image{
                id:upperDivLine
                source:viewHelper.configToolImagePath+configTag["music_"+orientationType].listing.divline
                anchors.bottom:playLV.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:playLV
                width:qsTranslate('','TrackPlayList_play_container_'+orientationType+'_w')
                height:qsTranslate('','TrackPlayList_play_container_'+orientationType+'_h')
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top:trackPanel.top;anchors.topMargin: qsTranslate('','TrackPlayList_play_container_'+orientationType+'_tm')
                model:playlistNames
                boundsBehavior: Flickable.StopAtBounds
                snapMode: ListView.SnapOneItem
                clip:true
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                function playlistSelected(index){
                    viewHelper.isPlayList=true
                    viewHelper.playListIndex=(index+1)*-1
                    if(pif.vkarmaToSeat.getPlaylistCount({"playlistIndex":viewHelper.playListIndex,"mediaType":"aod"})==0){
                        viewController.showScreenPopup(15)
                    }
                    else{
                        var params=[pif.vkarmaToSeat.getAllPlaylistMids({"playlistIndex":viewHelper.playListIndex,"mediaType":"aod"}),""];
                        dataController.getPlaylistData(params,viewData.playListReady)
                    }
                }

                delegate:Item{
                    height:qsTranslate('','TrackPlayList_play_cells_'+orientationType+'_h')
                    width:qsTranslate('','TrackPlayList_play_cells_'+orientationType+'_w')
                    property bool isPressed: false
                    ViewText{
                        anchors.centerIn: parent
                        width:qsTranslate('','TrackPlayList_play_text_'+orientationType+'_w')
                        height:qsTranslate('','TrackPlayList_play_text_'+orientationType+'_h')
                        varText:[playlistNames[index]+getTracks(index),"#414141"/*configTag["music_"+orientationType].playlist.category_txt_color*/,qsTranslate('','TrackPlayList_play_text_'+orientationType+'_fontsize')]
                        lineHeight:qsTranslate('','TrackPlayList_play_text_'+orientationType+'_lh')
                        lineHeightMode:Text.FixedHeight
                    }
                    Image{
                        id:divLine
                        source:viewHelper.configToolImagePath+configTag["music_p"].listing.divline//viewHelper.configToolImagePath+configTag["music_"+orientationType].listing.divline
                        anchors.bottom:parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    BorderImage{
                        id:trackHighlight
                        width:qsTranslate('','TrackPlayList_press_'+orientationType+'_w')
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom;anchors.bottomMargin: qsTranslate('','TrackPlayList_press_'+orientationType+'_bm')
                        source:viewHelper.configToolImagePath+configTag["music_"+orientationType].playlist.playlist_list_highlight
                        visible:isPressed
                    }
                    MouseArea{
                        anchors.fill: parent
                        enabled: !playLV.moving
                        onEnabledChanged: {
                            if(!enabled)isPressed=false
                        }
                        onPressed: {
                            if(enabled)isPressed=true
                        }
                        onReleased: {
                            isPressed=false
                            playLV.playlistSelected(index)
                        }
                    }
                }
            }
            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.mediaImagePath+viewHelper.catIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText: ["Playlists",configTag["music_"+orientationType].playlist.category_txt_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }
}
