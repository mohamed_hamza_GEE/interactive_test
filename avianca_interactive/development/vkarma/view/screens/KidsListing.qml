import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: kidsListing
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSubMenuBackground,"subMenuBackground")
        push(compMediaMenu,"mediaMenu")
    }
    function init(){
        console.log("Kids Listing | init")
        setHeaderFooter()
        getMedia()
    }
    function reload(){console.log("Kids Listing | reload")}
    function clearOnExit(){
        console.log("Kids Listing | clear on exit")
        removeHeaderFooter()
    }
    onLoaded: {}
    onOrientationTypeChanged: {
        mediaMenu.resetDelegate()
    }
    /**************************************************************************************************************************/
    Connections{
        target:(visible)?viewData:null
        onTracklistModelReady:{
            viewHelper.isPlayList=false
            viewController.loadScreen("tracklist")
        }
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        viewController.loadPrev()
    }
    function getMedia(){
        var cid=viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"cid")
        var tid=viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
        viewHelper.tempId=tid
        dataController.getNextMenu([cid,tid],viewData.mediaMenuReady)
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["kids_menu_"+orientationType].main.bg
            }
        }
    }
    property QtObject subMenuBackground
    Component{
        id:compSubMenuBackground
        Item{
            anchors.top:parent.top
            anchors.horizontalCenter:background.horizontalCenter
            anchors.fill:parent
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.background
            }
            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.mediaImagePath+viewHelper.catIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText: [viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"title"),configTag[viewHelper.catSelected+"_"+orientationType].listing.category_txt_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }
    property QtObject mediaMenu
    Component{
        id:compMediaMenu
        Item{
            anchors.horizontalCenter: background.horizontalCenter
            height: parent.height
            width:parent.width
            property alias lView: mediaMenuLv
            function resetDelegate(){
                mediaMenuLv.delegate=viewHelper.emptyDel
                mediaMenuLv.delegate=mediaMenuDel
            }
            Image{
                id:upArrow
                visible:(orientationType=='p' && mediaMenuLv.contentY>0)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.up_arrow
                anchors.top:(orientationType=='p')? parent.top:undefined;
                anchors.topMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_up'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Image{
                id:upperDivLine
                visible:(orientationType=='p')
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline:""
                anchors.bottom:mediaMenuLv.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:mediaMenuLv
                width:qsTranslate('','Listing_container_'+orientationType+'_w')
                height:qsTranslate('','Listing_container_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','Listing_container_'+orientationType+'_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                orientation:(pif.getOrientationType()=='p')?ListView.Vertical:ListView.Horizontal
                boundsBehavior: ListView.StopAtBounds
                snapMode: ListView.SnapToItem
                cacheBuffer: width
                model:viewData.mediaModel
                delegate:mediaMenuDel
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                clip:true
                function mediaSelected(index){
                    viewHelper.mediaSelected=index
                    if(current.template_id=="kids_music_listing"){
                        dataController.getAudioTrackList([model.getValue(index,"mid"),,model.getValue(index,"rating")],viewData.tracklistReady)
                    }
                    else
                        viewController.loadScreen("synopsis")
                }
                onMovementEnded: {
                    if(indexAt(contentX,contentY)>=0){
                        var cIndex=indexAt(contentX+10,contentY+10);
                        if(pif.getOrientationType()!="p" && mediaMenuLv.model.count<=3){
                            cIndex=0;
                        }else {
                            var resetPosition=(mediaMenuLv.model.count-3);
                            if(resetPosition>0){
                                cIndex=(cIndex>resetPosition)?(resetPosition):cIndex;
                            }else{
                                cIndex=0;
                            }
                        }
                        currentIndex=cIndex;
                    }
                }
            }
            Image{
                id:leftArrow
                visible:(orientationType=='l'&& mediaMenuLv.contentX>0)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.left_arrow
                anchors.right:(orientationType=='l')? mediaMenuLv.left:undefined;
                anchors.rightMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: mediaMenuLv.verticalCenter
            }
            Image{
                id:rightArrow
                visible:(orientationType=='l'&& mediaMenuLv.currentIndex<mediaMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.right_arrow
                anchors.left:(orientationType=='l')? mediaMenuLv.right:undefined;
                anchors.leftMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: mediaMenuLv.verticalCenter
            }
            Image{
                id:downArrow
                visible:(orientationType=='p'&& mediaMenuLv.currentIndex<mediaMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.down_arrow
                anchors.bottom:(orientationType=='p')? parent.bottom:undefined;
                anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_down'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
    Component{//Media Delegate
        id:mediaMenuDel
        Item{
            width:qsTranslate('','Listing_cells_'+orientationType+'_w')
            height:qsTranslate('','Listing_cells_'+orientationType+'_h')
            property bool isPressed: false
            Image{
                id:posterHolder
                smooth:true
                anchors.centerIn: posterImg
                source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.poster_holder:""
            }
            Image{
                id:posterImg
                width:(current.template_id=='kids_movies_listing'||current.template_id=='kids_tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_w'):(current.template_id=='kids_games_listing'||current.template_id=='kids_music_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_w'):qsTranslate('','Listing_poster_disc_'+orientationType+'_w')
                height:(current.template_id=='kids_movies_listing'||current.template_id=='kids_tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_h'):(current.template_id=='kids_games_listing'||current.template_id=='kids_music_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_h'):qsTranslate('','Listing_poster_disc_'+orientationType+'_h')
                anchors.left: parent.left;anchors.leftMargin:(current.template_id=='kids_movies_listing'||current.template_id=='kids_tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_lm'):(current.template_id=='kids_games_listing'||current.template_id=='kids_music_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_lm'):qsTranslate('','Listing_poster_disc_'+orientationType+'_lm')
                anchors.top:parent.top;anchors.topMargin:(current.template_id=='kids_movies_listing'||current.template_id=='kids_tv_listing')?qsTranslate('','Listing_poster_'+orientationType+'_tm'):(current.template_id=='kids_games_listing'||current.template_id=='kids_music_listing')?qsTranslate('','Listing_poster_mus_'+orientationType+'_tm'):qsTranslate('','Listing_poster_disc_'+orientationType+'_tm')
                source:(orientationType=='p')?viewHelper.mediaImagePath+poster_p:viewHelper.mediaImagePath+poster_l
                Rectangle{anchors.fill:parent;color:"transparent"}
            }
            ViewText{
                id:mediaName
                varText:[title,(isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].listing.text_color_press:(configTag[viewHelper.catSelected+"_"+orientationType].listing.text_color),qsTranslate('','Listing_text_'+orientationType+'_fontsize')]
                anchors.left:(orientationType=='p')? posterImg.right:undefined;anchors.leftMargin: (orientationType=='p')?qsTranslate('','Listing_text_'+orientationType+'_lm'):0
                anchors.top:(orientationType=='l')?posterImg.bottom:undefined;anchors.topMargin:(orientationType=='l')?qsTranslate('','Listing_text_l_tm'):0
                height:qsTranslate('','Listing_text_'+orientationType+'_h')
                width:qsTranslate('','Listing_text_'+orientationType+'_w')
                anchors.verticalCenter:(orientationType=='p')? posterImg.verticalCenter:undefined
                anchors.horizontalCenter: (orientationType=='l')?posterImg.horizontalCenter:undefined
                verticalAlignment:(orientationType=='p')?Text.AlignVCenter:undefined
                horizontalAlignment: (orientationType=='l')?Text.AlignHCenter:undefined
                elide: Text.ElideRight
                wrapMode: Text.WordWrap
                lineHeight:qsTranslate('','Listing_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }
            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.divline:""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !mediaMenu.lView.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }
                onReleased: {
                    isPressed=false
                    mediaMenu.lView.mediaSelected(index)
                }
            }
            Component.onCompleted: {
                if(mediaMenu.lView.currentIndex!=0)mediaMenu.lView.currentIndex=0
            }
        }
    }
}

