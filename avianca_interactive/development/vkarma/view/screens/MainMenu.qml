import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: mainMenuScreen
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property variant configMenu: (viewHelper.isKids(current.template_id))?"kids_menu_"+orientationType:"menu_"+orientationType
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compMainMenu,"mainMenu")
    }
    function init(){
        console.log("Main Menu.qml | init")
        setHeaderFooter()
        getMainMenu()
        /*        if(viewHelper.isFromKarma)widgetList.getFooterRef().getNowPlayingSource()
        else
            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedgetPlayingCategory")*/
    }
    function reload(){
        console.log("Main Menu.qml | reload called")
        init()
    }
    function clearOnExit(){
        console.log("Main Menu.qml | clearOnExit")
        viewHelper.sectionSelected=false
        removeHeaderFooter()
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    Connections{
        target:(visible)?viewData:null
        onMainMenuModelReady:{
            updateMainMenu()
        }
        onKidsMenuModelReady:{
            updateMainMenu()
        }
        onSubMenuModelReady:{
            if(viewData.subMenuModel.count<=0){
                if(viewHelper.catSelected=='shopping')
                    viewController.showScreenPopup(6)
                else viewController.showScreenPopup(10)
                return;
            }
            else
                viewController.loadNext((viewHelper.isKids(current.template_id))?viewData.kidsMenuModel:viewData.mainMenuModel,viewHelper.selectedIndex)
        }
        onMediaModelReady:{
            if(viewData.mediaModel.count<=0){
                viewController.showScreenPopup(10)
                return;
            }
            else{
                viewController.loadNext((viewHelper.isKids(current.template_id))?viewData.kidsMenuModel:viewData.mainMenuModel,viewHelper.selectedIndex)
            }
        }
    }
    onOrientationTypeChanged: {
        console.log("onOrientationTypeChanged | currentIndex"+mainMenu.lView.currentIndex)

        resetDelegate()
    }
    Connections{
        target:(visible)?viewHelper:null
        onSwitchToDiscover:{
            if(swtich){
                viewHelper.selectedIndex=core.coreHelper.searchEntryInModel(viewData.mainMenuModel,"category_attr_template_id","discover")
                viewHelper.loadAppScreen(category)
            }
        }
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(false)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function updateMainMenu(){
        if(viewHelper.isKids(current.template_id)){
            if(viewHelper.firstKidsLoad==false ){
                viewHelper.firstKidsLoad=true
                mainMenu.lView.currentIndex=core.coreHelper.searchEntryInModel(mainMenu.lView.model,"category_attr_template_id","kids_music_listing")
            }else {console.log("Kids is reloaded")}
        }else{
            if(viewHelper.firstMainLoad==false ){
                viewHelper.firstMainLoad=true
                mainMenu.lView.currentIndex=core.coreHelper.searchEntryInModel(mainMenu.lView.model,"category_attr_template_id","music")
            }else {console.log("Main Menu is reloaded")}
        }
        mainMenu.lView.positionViewAtIndex(mainMenu.lView.currentIndex,ListView.Beginning)
        if(viewHelper.isFromKarma)widgetList.getFooterRef().getNowPlayingSource()
        else
            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedgetPlayingCategory")
    }
    function getMainMenu(){
        if(current.template_id=="kids"){
            var params=new Object()
            params[0]="kids_games_listing,kids_music_listing,kids_movies_listing,kids_tv_listing"
            params[5]=100
            dataController.getCategoryByTemplateId(params,viewData.kidsMenuReady)
        }
        else{
            var params1=new Object()
            params1[0]="shopping,games,music,movies,tv,discover,usb"
            params1[5]=200
            dataController.getCategoryByTemplateId(params1,viewData.mainMenuReady)
        }
    }
    function loadPreviousScreen(){
        if(widgetList.getSettingsRef().visible)widgetList.getSettingsRef().loadPreviousScreen()
        else{
            viewController.loadScreen("OPTSEL")
        }
    }
    function resetDelegate(){
        mainMenu.resetDel()
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag[configMenu].main.bg
            }
        }
    }
    property QtObject mainMenu
    Component{
        id:compMainMenu
        Item{
            id:mainCont
            anchors.horizontalCenter: background.horizontalCenter
            height: parent.height
            width:parent.width
            property alias lView: mainMenuLv
            function resetDel(){
                mainMenuLv.delegate=viewHelper.emptyDel
                mainMenuLv.delegate=mainMenuDel
            }
            function resetCurrentIndex(val){mainMenuLv.currentIndex=val}
            function repositionView(index){mainMenuLv.positionViewAtIndex(index,ListView.Beginning)}
            Image{
                id:upArrow
                visible:(orientationType=='p' && mainMenuLv.contentY>0)
                source:viewHelper.configToolImagePath+configTag[configMenu].main.up_arrow
                anchors.top:(orientationType=='p')? parent.top:undefined;
                anchors.topMargin: (orientationType=='p')?qsTranslate('','MainMenu_arrow_p_vgap'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Image{
                id:upperDivLine
                visible:(orientationType=='p')
                source:viewHelper.configToolImagePath+configTag[configMenu].main.divline
                anchors.bottom:mainMenuLv.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Timer{
                id:lViewTimer
                interval:100
                onTriggered: {
                    console.log("onOrientationChanged| Lview | currentIndex"+mainMenuLv.currentIndex);mainMenuLv.positionViewAtIndex(mainMenuLv.currentIndex,ListView.Beginning)
                }
            }

            ListView{
                id:mainMenuLv
                width:qsTranslate('','MainMenu_container_'+orientationType+'_w')
                height:qsTranslate('','MainMenu_container_'+orientationType+'_h')
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','MainMenu_container_'+orientationType+'_tm')
                orientation:(pif.getOrientationType()=='p')?ListView.Vertical:ListView.Horizontal
                boundsBehavior: ListView.StopAtBounds
                clip:true
                cacheBuffer:(orientationType=='l')?width:height
                snapMode: ListView.SnapToItem
                model:(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel:viewData.mainMenuModel
                delegate:mainMenuDel
                onOrientationChanged: {lViewTimer.restart()}

                function catalogMenuReady(dModel,status){
                    core.debug(">>>>>>>>>>>>>>dmodel count<<<<<<<<<<<<<<<<<<<<<<<<<<< "+dModel.count)
                    if(dModel.count<=0){ viewController.showScreenPopup(27); return;}
                        viewController.loadScreen("shopping")
                }
                function catSelected(index){
                    if(dataController.mediaServices.getCidBlockStatus(mainMenuLv.model.getValue(index,"cid"))){
                        viewController.showScreenPopup(27);return
                    }

                    pif.vkarmaToSeat.sendUserDefinedApi("userDefinedCid",{cid:(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel.getValue(index,"cid"):viewData.mainMenuModel.getValue(index,"cid")})
                    var model=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel:viewData.mainMenuModel
                    viewHelper.setCategory(model.getValue(index,"category_attr_template_id"))
                    viewHelper.catIcon=model.getValue(index,"poster_"+orientationType)
                    if(viewHelper.catSelected=="discover"|| viewHelper.catSelected=="games"){
                        var cid=viewData.mainMenuModel.getValue(index,"cid")
                        var tid=viewData.mainMenuModel.getValue(index,"category_attr_template_id")
                        viewHelper.tempId=tid
                        dataController.getNextMenu([cid,tid],viewData.mediaMenuReady)
                    }
                    else if(viewHelper.catSelected=="usb" ){
                        if( pif.vkarmaToSeat.getUsbConnectedStatus()==false)
                            viewController.showScreenPopup(23)
                        else{
                            var catnode = new Object();
                            catnode["pcid"] = viewData.optionMenuModel.getValue(viewHelper.sectionIndexSel,"cid")
                            catnode["ptid"] =viewData.optionMenuModel.getValue(viewHelper.sectionIndexSel,"category_attr_template_id")
                            catnode["cid"] = model.getValue(index,"cid")
                            catnode["tid"] = model.getValue(index,"category_attr_template_id")
                            pif.vkarmaToSeat.launchCategory(catnode);
                            viewController.showScreenPopup(25)
                        }
                    }
                    else if(viewHelper.catSelected=="shopping") {/*viewController.showScreenPopup(6)*/
                        var curLangISO=core.settings.languageISO
                        core.debug("Mainmenu.qml submenu curLangISO | core.dataController.shopping.getLanguageModel().count "+core.dataController.shopping.getLanguageModel().count)

                        if(core.dataController.shopping.getLanguageModel().count<=1){
                            core.debug("Mainmenu.qml submenu curLangISO | eng >>>>>>>>>>>>"+curLangISO+ "viewHelper.defaultLang = "+viewHelper.defaultLang )
                            core.dataController.shopping.fetchData(viewHelper.defaultLang,catalogMenuReady)
                        }else{
                            core.debug("Mainmenu.qml submenu curLangISO |%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+curLangISO)
                            core.dataController.shopping.fetchData(curLangISO,catalogMenuReady)
                        }


                    }else{
                        var cid=model.getValue(index,"cid")
                        var tid=model.getValue(index,"category_attr_template_id")
                        dataController.getNextMenu([cid,tid],viewData.subMenuReady)
                    }
                }
                onMovementEnded: {
                    if(indexAt(contentX,contentY)>=0){
                        var cIndex=indexAt(contentX,contentY);
                        if(pif.getOrientationType()!="p" && mainMenuLv.model.count<=3){
                            cIndex=0;
                        }else {
                            var resetPosition=(mainMenuLv.model.count-3);
                            if(resetPosition>0){
                                cIndex=(cIndex>resetPosition)?(resetPosition):cIndex;
                            }else{
                                cIndex=0;
                            }
                        }
                        currentIndex=cIndex;
                    }
                }
            }
            Image{
                id:leftArrow
                visible:(orientationType=='l'&& mainMenuLv.contentX>0)
                source:viewHelper.configToolImagePath+configTag[configMenu].main.left_arrow
                anchors.left:(orientationType=='l')? parent.left:undefined;
                anchors.leftMargin: (orientationType=='l')?qsTranslate('','MainMenu_arrow_l_hgap'):0
                anchors.verticalCenter: mainMenuLv.verticalCenter
            }
            Image{
                id:rightArrow
                visible:(orientationType=='l'&& mainMenuLv.currentIndex<mainMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag[configMenu].main.right_arrow
                anchors.right:(orientationType=='l')? parent.right:undefined;
                anchors.rightMargin: (orientationType=='l')?qsTranslate('','MainMenu_arrow_l_hgap'):0
                anchors.verticalCenter: mainMenuLv.verticalCenter
            }
            Image{
                id:downArrow
                visible:(orientationType=='p'&& mainMenuLv.currentIndex<mainMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag[configMenu].main.down_arrow
                anchors.bottom:(orientationType=='p')? parent.bottom:undefined;
                anchors.bottomMargin: (orientationType=='p')?qsTranslate('','MainMenu_arrow_p_vgap'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
    Component{//Main Menu Delegate
        id:mainMenuDel
        Item{
            width:qsTranslate('','MainMenu_cells_'+orientationType+'_w')
            height:qsTranslate('','MainMenu_cells_'+orientationType+'_h')
            property bool isPressed: false
            Image{
                id:highLightImg
                smooth:true
                anchors.centerIn: mainMenuIcon
                source:(isPressed)?viewHelper.configToolImagePath+configTag[configMenu].main.btn_press:""
            }
            Image{
                id:mainMenuIcon
                width:qsTranslate('','MainMenu_icon_'+orientationType+'_w');height:qsTranslate('','MainMenu_icon_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','MainMenu_icon_'+orientationType+'_tm')
                anchors.left: parent.left;anchors.leftMargin:qsTranslate('','MainMenu_icon_'+orientationType+'_lm')
                source:(orientationType=='p')?viewHelper.mediaImagePath+poster_p:viewHelper.mediaImagePath+poster_l
            }
            ViewText{
                id:catname
                varText:[title,(isPressed)?configTag[configMenu].main.text_color_p:configTag[configMenu].main.text_color,qsTranslate('','MainMenu_text_'+orientationType+'_fontsize')]
                anchors.left:(orientationType=='p')? mainMenuIcon.right:undefined;anchors.leftMargin: (orientationType=='p')?qsTranslate('','MainMenu_text_'+orientationType+'_lm'):0
                anchors.top:(orientationType=='l')?mainMenuIcon.bottom:undefined;anchors.topMargin:(orientationType=='l')?qsTranslate('','MainMenu_text_l_tm'):0
                height:qsTranslate('','MainMenu_text_'+orientationType+'_h')
                width:qsTranslate('','MainMenu_text_'+orientationType+'_w')
                anchors.verticalCenter:(orientationType=='p')? mainMenuIcon.verticalCenter:undefined
                anchors.horizontalCenter: (orientationType=='l')?mainMenuIcon.horizontalCenter:undefined
                verticalAlignment: (orientationType=='p')?Text.AlignVCenter:Text.AlignTop
                horizontalAlignment: (orientationType=='l')?Text.AlignHCenter:Text.AlignLeft
                elide: Text.ElideRight
                lineHeight:qsTranslate('','MainMenu_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }
            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag[configMenu].main.divline:""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !mainMenu.lView.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }
                onReleased: {
                    isPressed=false
                    viewHelper.selectedIndex=index
                    mainMenu.lView.currentIndex=index
                    mainMenu.lView.catSelected(index)
                }
            }
        }
    }
}
