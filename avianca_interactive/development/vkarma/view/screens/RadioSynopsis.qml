import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: radioNowPlaying
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSynopsisPanel,"synopsisPanel")
    }
    function init(){
        console.log("radioNowPlaying.qml | init")
        setHeaderFooter()
    }
    function reload(){console.log("radioNowPlaying.qml | reload")}
    function clearOnExit(){
        console.log("radioNowPlaying.qml | clear OnExit")
        removeHeaderFooter()
    }
    onLoaded: {}
    /***********************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        viewController.loadScreen("music_listing")
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].listing.background//.bg_music
            }
        }
    }
    property QtObject synopsisPanel
    Component{
        id:compSynopsisPanel
        Item{
            id:mainCont
            anchors.fill:background
            function playRadio(){
                var model=viewHelper.synopsisModel
                var mediaState=pif.vkarmaToSeat.getCurrentMediaPlayerState()
                if((mediaState=="play" || mediaState=="pause") && model.getValue(viewHelper.radioAlbumSelected,"mid")==pif.vkarmaToSeat.getPlayingMidInfo()["mid"]){//added because getCurrentMediaPlayerState returns blank initially,so cannot check against stop..need to ask dhiraj
                    viewHelper.isTrackPlayedFromKids=viewHelper.isKids(current.template_id)
                    if(mediaState=="play")
                        pif.vkarmaToSeat.pausePlayingMedia();
                    else
                        pif.vkarmaToSeat.resumePlayingMedia();
                }
                else{
                    pif.vkarmaToSeat.sendUserDefinedApi("userDefinedRadioPlay",{flag:true});
                    var params=new Object()
                    params["mid"]=model.getValue(viewHelper.radioAlbumSelected,"mid")
                    params["aggregateMid"]=model.getValue(viewHelper.radioAlbumSelected,"aggregate_parentmid")
                    params["mediaRepeat"]=pif.vkarmaToSeat.getMediaRepeatValue()
                    params["rating"]=model.getValue(viewHelper.radioAlbumSelected,"rating")
                    params["title"]=model.getValue(viewHelper.radioAlbumSelected,"title")
                    var ret = dataController.mediaServices.getMidBlockStatus(params["aggregateMid"],params["rating"]);
                    if(ret){
                        console.log("MID is blocked")
                        viewController.showScreenPopup(27)
                    }
                    viewHelper.setMusicData(params)
                    if(pif.vkarmaToSeat.getMediaType()=="videoAggregate" && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"){
                        viewHelper.exitCase = 3;
                        viewHelper.aodMidToBePlayed = params["mid"];
                        core.debug("Radio Mid to be played When hit YES :  "+params["mid"]);
                        viewController.showScreenPopup(2);
                    }else{
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedAudioBroadcastData",{mid:params["mid"],aggregateMid:params["aggregateMid"],mediaRepeat:params["mediaRepeat"],playingIndex:viewHelper.radioAlbumSelected});
                    }
                }
            }
            Image{
                id:poster
                width:qsTranslate('','Listing_poster_mus_'+orientationType+'_w')
                height:qsTranslate('','Listing_poster_mus_'+orientationType+'_h')
                source:(orientationType=='p')?viewHelper.mediaImagePath+viewHelper.synopsisModel.getValue(viewHelper.mediaSelected,"synopsisposter_p"):viewHelper.mediaImagePath+viewHelper.synopsisModel.getValue(viewHelper.mediaSelected,"synopsisposter_l")
                anchors.left:mainCont.left;anchors.leftMargin: qsTranslate('','NowPlay_radio_poster_'+orientationType+'_lm')
                anchors.top:mainCont.top;anchors.topMargin: qsTranslate('','NowPlay_radio_poster_'+orientationType+'_tm')
                Rectangle{anchors.fill:parent;color:"transparent"}
            }
            ViewText{
                id:titleText
                varText:[viewHelper.synopsisModel.getValue(viewHelper.radioAlbumSelected,"title"),configTag[viewHelper.catSelected+"_"+orientationType].radio.title_color,qsTranslate('','NowPlay_radio_title_'+orientationType+'_fontsize')]
                anchors.top:poster.top;anchors.topMargin: qsTranslate('','NowPlay_radio_title_'+orientationType+'_tm')
                anchors.left: poster.right;anchors.leftMargin: qsTranslate('','NowPlay_radio_title_'+orientationType+'_lm')
                width:qsTranslate('','NowPlay_radio_title_'+orientationType+'_w')
                height:qsTranslate('','NowPlay_radio_title_'+orientationType+'_h')
                wrapMode: Text.WordWrap
                font.weight: qsTranslate('','NowPlay_radio_title_'+orientationType+'_fontweight')
                horizontalAlignment: Text.AlignLeft
            }
            Rectangle{
                id:scrollText
                width:qsTranslate('','NowPlay_radio_title_w');
                height:qsTranslate('','NowPlay_radio_title_h')
                anchors.top:titleText.bottom;anchors.topMargin: qsTranslate('','NowPlay_radio_title_tm')
                anchors.left: titleText.left
                color:"transparent"
                clip:true
                Flickable{
                    width:parent.width;height:parent.height
                    interactive: true
                    boundsBehavior :Flickable.StopAtBounds
                    contentHeight:descriptionText.paintedHeight
                    ViewText{
                        id:descriptionText
                        width:parent.width;height:parent.height
                        varText: [viewHelper.synopsisModel.getValue(viewHelper.radioAlbumSelected,"description"),configTag[viewHelper.catSelected+"_"+orientationType].radio.description_color,qsTranslate('','NowPlay_radio_title_fontsize')]
                        lineHeight:qsTranslate('','NowPlay_radio_title_lh')
                        lineHeightMode:Text.FixedHeight
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignLeft
                    }
                }
            }
            BorderImage{
                id:playBtn
                width:parseInt(qsTranslate('','NowPlay_radio_btn_'+orientationType+'_w'),10)
                property bool isPressed: false
                border{
                    left: qsTranslate('','NowPlay_radio_btn_'+orientationType+'_margin')
                    right:qsTranslate('','NowPlay_radio_btn_'+orientationType+'_margin')
                }
                anchors.left:poster.right;anchors.leftMargin: qsTranslate('','NowPlay_radio_btn_'+orientationType+'_lm')
                anchors.top:poster.top;anchors.topMargin: qsTranslate('','NowPlay_radio_btn_'+orientationType+'_tm')
                source:(isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].radio.btn_press:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].radio.btn
                Text{
                    id:playTxt
                    color:(playBtn.isPressed)?configTag[viewHelper.catSelected+"_"+orientationType].radio.btn_text_press:configTag[viewHelper.catSelected+"_"+orientationType].radio.btn_text
                    height:qsTranslate('','NowPlay_radio_btntxt_'+orientationType+'_h');width:qsTranslate('','NowPlay_radio_btntxt_'+orientationType+'_w')
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: qsTranslate('','NowPlay_radio_btntxt_'+orientationType+'_fontsize')
                    text:(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play" && pif.vkarmaToSeat.getPlayingMidInfo().mid==viewHelper.synopsisModel.getValue(viewHelper.radioAlbumSelected,"mid"))?"Stop":configTool.language.movies.play //toggle on audio play to stop
                    wrapMode: Text.WordWrap
                }
                MouseArea{
                    anchors.fill:parent
                    onPressed:{
                        playBtn.isPressed=true
                    }
                    onReleased: {
                        playBtn.isPressed=false
                        mainCont.playRadio()
                    }
                }
            }
        }
    }
}

