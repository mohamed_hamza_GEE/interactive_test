import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: subMenuScreen
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'

    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSubMenuBackground,"subMenuBackground")
        push(compSubMenu,"subMenu")
    }
    function init(){
        console.log("SubMenu.qml | init")
        setHeaderFooter()
        if(viewHelper.catSelected=="shopping"){
            getCatalogMenu()
        }
    }
    function reload(){console.log("SubMenu.qml | reload") ; /*init()*/}
    function clearOnExit(){
        console.log("SubMenu.qml | clearOnExit")
        removeHeaderFooter()
    }
    onLoaded: {}
    Connections{
        target:(visible)?viewData:null
        onMediaModelReady:{
            if(viewData.mediaModel.count<=0)viewController.showScreenPopup(6);
            else
                viewController.loadNext(viewData.subMenuModel,viewHelper.subMenuSelIndex)
        }
    }
    Connections{
        target:(visible)?viewHelper:null
        onShoppingLangUpdate:{
            console.log(" onShopping ")
          subMenu.model=core.dataController.shopping.getCategoryModel()
        }
    }


    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        if(viewHelper.catSelected=="shopping"){
            console.log("load shopping main_menu >>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            viewController.loadScreen(current.template_id)
        }else{
            viewController.loadPrev()
        }
    }

    function getMedia(){
        if(viewHelper.catSelected=="discover"){
            var cid=viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"cid")
            var tid=viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"category_attr_template_id")
            viewHelper.tempId=tid
            dataController.getNextMenu([cid,tid],viewData.mediaMenuReady)
        }
        else if(viewHelper.catSelected=="music")viewController.loadNext(viewData.subMenuModel,viewHelper.subMenuSelIndex)
        else{
            var cid=viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"cid")
            var tid=viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"category_attr_template_id")
            viewHelper.tempId=tid
            dataController.getNextMenu([cid,tid],viewData.mediaMenuReady)
        }
    }

    function getCatalogMenu(){
        var curLangISO=core.settings.languageISO.toUpperCase()
        var langIndex =core.coreHelper.searchEntryInModel(core.dataController.shopping.getLanguageModel(),"language_code",curLangISO)
        core.log("languageUpdate |  viewHelper.defaultLang = "+viewHelper.defaultLang+" langIndex = "+langIndex+ " curLangISO = "+curLangISO)


        if(langIndex!=-1){
            console.log("languageUpdate |  curLangISO = "+curLangISO)
            core.dataController.shopping.fetchData(curLangISO,catalogMenuReady)
        }else{
            console.log("defaultLang "+viewHelper.defaultLang)
            core.dataController.shopping.fetchData(viewHelper.defaultLang,catalogMenuReady)
        }
    }
    function catalogMenuReady(dModel,status){
        core.debug(">>>>>>>>>>>>>>dmodel count<<<<<<<<<<<<<<<<<<<<<<<<<<< "+dModel.count)
        if(dModel.count<=0){ viewController.showScreenPopup(27); return;}
        for (var i=0; i<dModel.count;i++){
            console.log(">>>>>>>>.> catalog_id = "+dModel.getValue(i,"catalog_id"))
        }
        viewHelper.catalogModel=dModel
        viewHelper.shopValue=dModel.at(0)
        viewHelper.shop_catalog_id=viewHelper.shopValue.catalog_id
        getCategoryMenu()
    }

    function getCategoryMenu(){
        core.dataController.shopping.getNextList(viewHelper.shopValue,viewHelper.shop_catalog_id,"","",nextListCategoryModelReady)
    }

    function nextListCategoryModelReady(dModel,status,type){
        core.debug("ShopList | nextListCategoryModelReady called | status : " + status + " | dModel.count : " + dModel.count+" | type: "+type);
        if(dModel.count==0){viewController.showScreenPopup(27);return;}
        for (var i=0; i<dModel.count;i++){
            var title = dModel.getValue(i,"title")
            core.log(">>>>>>>>.> title = "+title)
            core.log(">>>>>>>>.> category_node_id = "+dModel.getValue(i,"category_node_id"))
        }
        viewHelper.shoppingCategoryMenuModel=dModel
        viewHelper.shopValue=dModel.at(0)
        viewHelper.shop_category_id=viewHelper.shopValue.category_node_id
        if(status){
            if(type == "category"){
                subMenu.visible=true
            }
        }
    }
    function getListingMenu(){
        core.dataController.shopping.getNextList(viewHelper.shopValue,viewHelper.shop_category_id,"","",nextListModelReady)
    }

    function nextListModelReady(dModel,status,type){
        core.log("Shopping.qml | nextListModelReady dModel.count = "+dModel.count)
        if(dModel.count==0){
            viewController.showScreenPopup(7);
            return;
        }
//        viewHelper.itemListingModel=dModel
        for (var i=0; i<dModel.count;i++){
            var sub_title = dModel.getValue(i,"sub_title")
            var item_id = dModel.getValue(i,"item_id")
            core.log(">>>>>>>>.> sub_title = "+sub_title)
            core.log(">>>>>>>>.> item_id = "+item_id)
        }
        if(status){
            if(type == "item"){
                viewHelper.shoppingMediaModel=dModel
            }
        }
    }
    function subCatSelected(index){
        console.log(" SubMenu.qml | subCatSelected = "+viewHelper.catSelected)
        if(viewHelper.catSelected=="shopping"){
            subMenu.lView.currentIndex=index
            viewHelper.subMenuSelIndex=index
            viewHelper.shop_category_id=index
            viewHelper.shopValue= subMenu.lView.currentItem.getValue();
            core.dataController.shopping.getNextList(viewHelper.shopValue,viewHelper.shop_category_id,"","",nextListModelReady)
            viewController.loadScreen("shopping_listing")
        }else{
            viewHelper.subCatSelected=viewData.subMenuModel.getValue(index,"title")
            var catId=viewData.subMenuModel.getValue(index,"category_attr_template_id")
            getMedia()
        }
    }


    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:subMenuScreen.width;
            height:subMenuScreen.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
        }
    }
    property QtObject subMenuBackground
    Component{
        id:compSubMenuBackground
        Item{
            anchors.top:parent.top
            anchors.horizontalCenter:background.horizontalCenter
            anchors.fill:parent
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:(viewHelper.catSelected=="shopping")?viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu.bg_discover:viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu["bg_"+viewHelper.catSelected]//.bg_music
            }
            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.mediaImagePath+viewHelper.catIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText: [viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"title"),configTag["menu_"+orientationType].submenu.category_txt_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }

    property QtObject subMenu
    Component{
        id:compSubMenu
        Item{
            anchors.horizontalCenter: background.horizontalCenter
            property alias lView: subMenuLv
            height: parent.height
            width:parent.width
            Image{
                id:upArrow
                visible:(orientationType=='p' && subMenuLv.contentY>0)
                source:viewHelper.configToolImagePath+configTag["menu_p"].submenu.up_arrow
                anchors.top:(orientationType=='p')? parent.top:undefined;
                anchors.topMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_up'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Image{
                id:upperDivLine
                visible:(orientationType=='p')
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu.divline:""
                anchors.bottom:subMenuLv.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:subMenuLv
                width:qsTranslate('','SubMenu_container_'+orientationType+'_w')
                height:qsTranslate('','SubMenu_container_'+orientationType+'_h')
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','SubMenu_container_'+orientationType+'_tm')
                anchors.horizontalCenter: parent.horizontalCenter
                orientation:(pif.getOrientationType()=='p')?ListView.Vertical:ListView.Horizontal
                boundsBehavior: ListView.StopAtBounds
                snapMode: ListView.SnapToItem
                cacheBuffer: width
                model:(viewHelper.catSelected=="shopping")?viewHelper.shoppingCategoryMenuModel:viewData.subMenuModel
                delegate:subMenuDelegate
                clip:true
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)

                onMovementEnded: {
                    if(indexAt(contentX,contentY)>=0){
                        var cIndex=indexAt(contentX+10,contentY+10);
                        if(pif.getOrientationType()!="p" && subMenuLv.model.count<=3){
                            cIndex=0;
                        }else {
                            var resetPosition=(subMenuLv.model.count-3);
                            if(resetPosition>0){
                                cIndex=(cIndex>resetPosition)?(resetPosition):cIndex;
                            }else{
                                cIndex=0;
                            }
                        }
                        currentIndex=cIndex;
                    }
                }
            }
            Image{
                id:leftArrow
                visible:(orientationType=='l'&& subMenuLv.contentX>0)
                source:viewHelper.configToolImagePath+configTag["menu_l"].submenu.left_arrow
                anchors.left:(orientationType=='l')? parent.left:undefined;
                anchors.leftMargin: (orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: subMenuLv.verticalCenter
            }
            Image{
                id:rightArrow
                visible:(orientationType=='l'&& subMenuLv.currentIndex<subMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag["menu_l"].submenu.right_arrow
                anchors.right:(orientationType=='l')? parent.right:undefined;
                anchors.rightMargin:(orientationType=='l')?qsTranslate('','Common_arrow_l_hgap'):0
                anchors.verticalCenter: subMenuLv.verticalCenter
            }
            Image{
                id:downArrow
                visible:(orientationType=='p'&& subMenuLv.currentIndex<subMenuLv.model.count-3)
                source:viewHelper.configToolImagePath+configTag["menu_p"].submenu.down_arrow
                anchors.bottom:(orientationType=='p')? parent.bottom:undefined;
                anchors.bottomMargin: (orientationType=='p')?qsTranslate('','Common_arrow_p_vgap_down'):0
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
    Component{
        id:subMenuDelegate
        Item{
            width:qsTranslate('','SubMenu_cells_'+orientationType+'_w');
            height:qsTranslate('','SubMenu_cells_'+orientationType+'_h');
            property bool isPressed: false
            function getValue(){
                return value
            }

            ViewText{
                id:subMenuTxt
                varText: [(viewHelper.catSelected=="shopping")?value.title:title,(isPressed)?configTag["menu_"+orientationType].submenu.text_color_press:configTag["menu_"+orientationType].submenu.text_color,qsTranslate('','SubMenu_text_'+orientationType+'_fontsize')]
                width:qsTranslate('','SubMenu_text_'+orientationType+'_w')
                height:qsTranslate('','SubMenu_text_'+orientationType+'_h')
                wrapMode: Text.WordWrap
                elide: Text.ElideRight
                maximumLineCount:2
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            Image{
                id:divLine
                anchors.bottom: parent.bottom
                source:(orientationType=='p')?viewHelper.configToolImagePath+configTag["menu_"+orientationType].submenu.divline:""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !subMenu.lView.moving
                onEnabledChanged: {
                    if(!enabled)isPressed=false
                }
                onPressed: {
                    if(enabled)isPressed=true;
                }
                onReleased: {
                    isPressed=false
                    viewHelper.subMenuSelIndex=index
                    subCatSelected(index)
                }
            }
        }
    }
}

