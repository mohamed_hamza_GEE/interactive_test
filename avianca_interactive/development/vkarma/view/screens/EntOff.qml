import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: entOff
    /**************************************************************************************************************************/
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'l'
    /**************************************************************************************************************************/
    function load(){
        push(compEntOff,"entOff")
    }
    function init(){
        console.log("EntOff | init called")
    }
    function reload(){console.log("EntOff | reload called")}
    function clearOnExit(){
        console.log("EntOff | clearOnExit called")
    }
    onLoaded: {}
    /**************************************************************************************************************************/
    property QtObject entOff
    Component{
        id:compEntOff
        Image{
            source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            Image{
                anchors.centerIn: parent
                source:viewHelper.configToolImagePath+(configTag["global_"+orientationType].entertainment_off.entoff_logo);
                 //viewHelper.configToolImagePath+(viewHelper.isAviancaBrasilAir?configTag["global_"+orientationType].entertainment_off.entoff_oceanair_logo:configTag["global_"+orientationType].entertainment_off.entoff_logo);
                smooth:true

            }
        }
    }
}
/**************************************************************************************************************************/
