import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: tracklistingScreen
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property variant tracksToBeAdded:[]
    property variant buttonClicked;
    property int playingIndex: 0
    property string  globalTrackList: (viewHelper.isKids(current.template_id))?"kids_music_"+orientationType:"music_"+orientationType
    /**************************************************************************************************************************/
    Connections{
        target:pif.vkarmaToSeat
        onSigDataReceivedFromSeat:{
            switch(api){
            case "playlists":{
                if(viewHelper.isPlayList){
                    var params=[pif.vkarmaToSeat.getAllPlaylistMids({"playlistIndex":viewHelper.playListIndex,"mediaType":"aod"}),""]
                    dataController.getPlaylistData(params,viewData.playListReady)
                }
                else
                    trackListing.checkDisability()
            }
            }
        }
        onSigPlaylistError:{
            console.log("onSigPlaylistError "+viewHelper.playListIndex)
            if(errorCode==4){

                viewHelper.isPlaylistFull=true
                viewController.showScreenPopup(14);
            }
        }
    }
    Connections{
        target:(visible)?viewData:null
        onPlayListModelReady:{
            trackListing.updateTrackModel(viewData.playListModel)
        }
        onCurrentTrackReady:{
            getPlayingIndex()
        }
    }
    Connections{
        id:catchScreenPop
        target:(visible)?viewController:null
        onScreenPopupClosed:{
            if(id==15){
                if(pif.vkarmaToSeat.getPlaylistCount({"playlistIndex":viewHelper.playListIndex,"mediaType":"aod"})==0){
                    viewHelper.isPlayList=false
                    viewController.hideScreenPopup()
                    viewController.loadPrev()
                }
            }
            if(viewHelper.isPlayList){
                if(retArray[0]==0)return;
                else {
                    if(buttonClicked=="removeTrackPlaylist")trackListing.removeTracks()
                    else trackListing.clearAllTracks()
                }
            }
            else{
                if(buttonClicked=="addToPlaylist")trackListing.addSingleTrack(retArray)
                else trackListing.addAllTracks(retArray)
            }
        }
    }
    Timer{
        id:checkForEmptyPlaylist
        interval:200
        running:false
        onTriggered: {
            var r=pif.vkarmaToSeat.getPlaylistCount({"playlistIndex":viewHelper.playListIndex,"mediaType":"aod"})
            core.debug("Tracklist.qml | checkForEmptyPlaylist Timer | r = "+r)
            if(r==0){viewHelper.clearPlaylistStack();viewController.showScreenPopup(15)}
        }
    }

    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSubMenuBackground,"subMenuBackground")
        push(compTrackListing,"trackListing")
    }
    function init(){
        console.log("Tracklisting.qml | init ")
        trackListing.checkBoxAll.cstate=false;
        setHeaderFooter()
        trackListing.playListBtnsvisibility()
        if(viewHelper.isPlayList){
            trackListing.isPlayList(true)
            trackListing.updateTrackModel(viewData.playListModel)
            viewHelper.clearPlaylistStack();
        }
        else{
            trackListing.isPlayList(false)
            trackListing.playListBtnsvisibility()
            trackListing.updateTrackModel(viewData.tracklistModel)
            viewHelper.clearTracklistStack();
        }
        showTracklist();

    }
    function reload(){console.log("Tracklisting.qml | reload")}
    function clearOnExit(){
        console.log("Tracklisting.qml | clearOnExit")
        removeHeaderFooter()
        trackListing.updateTrackModel(viewHelper.blankModel)
        trackStack.clear()
        viewHelper.clearTracklistStack();
    }
    onLoaded: {}
    /**************************************************************************/
    function showTracklist(){
        if(viewHelper.isPlayList)getTrackListTitle(viewHelper.blankModel)
        else
            dataController.getMediaByMid([viewData.tracklistModel.getValue(0,"aggregate_parentmid")],getTrackListTitle)

    }
    function getTrackListTitle(dModel){
        if(viewHelper.isFromGenre){
            trackListing.setTitle(dModel.getValue(0,"genre"))
        }
        else if(viewHelper.isPlayList){
            trackListing.setTitle((viewHelper.playListIndex==-1)?configTool.language.music.playlist1:(viewHelper.playListIndex==-2)?configTool.language.music.playlist2:configTool.language.music.playlist3)
        }
        else trackListing.setTitle(dModel.getValue(0,"title"))
    }
    function getPlayingIndex(){
        for(var i=0;i<trackListing.tracksList.model.count;i++){
            if(trackListing.tracksList.model.getValue(i,"mid")==viewHelper.playingAudioMid){
                playingIndex=i
                trackListing.tracksList.currentIndex=playingIndex
                return;
            }
        }
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        viewController.loadScreen(current.template_id)
    }
    SimpleModel{id:trackStack}
    SimpleModel{id:playListStack}
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
        }
    }
    property QtObject subMenuBackground
    Component{
        id:compSubMenuBackground
        Item{
            anchors.top:parent.top
            anchors.horizontalCenter:background.horizontalCenter
            anchors.fill:parent
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag[globalTrackList].listing.background//.bg_music
            }
        }
    }
    property QtObject trackListing
    Component{
        id:compTrackListing
        Item{
            anchors.horizontalCenter: background.horizontalCenter
            height: background.height
            width:background.width
            id:tracklIstingComp
            property alias isMoving: tracksList.moving
            property alias tracksList: tracksList
            property alias checkBoxAll: checkBoxAll
            function addSingleTrack(retArray){
                addToPlaylist.addTracksToPlayList(retArray)
            }
            function addAllTracks(retArray){
                addAllToPlaylist.addAlltracksToPlaylist(retArray)
            }
            function setTitle(title){titleText.text=title}
            function updateTrackModel(dModel){
                tracksList.model=dModel
                if(viewHelper.isPlayList){
                    checkBoxAll.isDisable=false;
                    checkBoxAll.cstate=false;
                }else checkDisability()

            }
            function checkDisability(){
                if (pif.vkarmaToSeat.isAllTracksAddedInPlaylist(tracksList.model,{mediaType:"aod"})){
                    checkBoxAll.isDisable=true;
                    checkBoxAll.cstate=true;
                }else{
                    checkBoxAll.isDisable=false;
                }
            }
            function isPlayList(val){
                addToPlaylist.visible=!val
                addAllToPlaylist.visible=!val
                removeTrackPlaylist.visible=val
                //                clearPlaylist.visible=val
            }
            function removeTracks(){
                removeTrackPlaylist.removeTracks()
            }
            function clearAllTracks(){
                clearPlaylist.clearAllTracks()
            }
            function playListBtnsvisibility(){
                if(current.template_id=="kids_music_listing"){
                    addAllToPlaylist.visible=false
                    addToPlaylist.visible=false
                }
                else{
                    addAllToPlaylist.visible=true
                    addToPlaylist.visible=true
                }
            }
            Image{
                id:trackPanel
                width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');height:qsTranslate('','Common_whitepanel_'+orientationType+'_h')
                source:viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.panel
                anchors.horizontalCenter:parent.horizontalCenter
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','Common_whitepanel_'+orientationType+'_tm')
            }
            ViewText{
                id:titleText
                anchors.top:trackPanel.top;anchors.topMargin:(viewHelper.isKids(current.template_id))? qsTranslate('','TrackPlayList_title_'+orientationType+'_tm_kids'): qsTranslate('','TrackPlayList_title_'+orientationType+'_tm')
                anchors.left: trackPanel.left;anchors.leftMargin: qsTranslate('','TrackPlayList_title_'+orientationType+'_lm')
                height:qsTranslate('','TrackPlayList_title_'+orientationType+'_h')
                width:qsTranslate('','TrackPlayList_title_'+orientationType+'_w')
                horizontalAlignment: Text.AlignLeft
                elide:"ElideRight";
                varText: ["",configTag[globalTrackList].tracklist.title_color,qsTranslate('','TrackPlayList_title_'+orientationType+'_fontsize')]
            }
            Image{
                id:divLine
                source:viewHelper.configToolImagePath+configTag["tv_"+orientationType].episode.divline
                anchors.bottom:tracksList.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:tracksList
                width:qsTranslate('','TrackPlayList_container_'+orientationType+'_w')
                height:qsTranslate('','TrackPlayList_container_'+orientationType+'_h')
                anchors.horizontalCenter: trackPanel.horizontalCenter
                anchors.top:trackPanel.top;anchors.topMargin:(viewHelper.isKids(current.template_id))?qsTranslate('','TrackPlayList_container_'+orientationType+'_tm_kids'):qsTranslate('','TrackPlayList_container_'+orientationType+'_tm')
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                clip:true
                boundsBehavior: Flickable.StopAtBounds
                snapMode: ListView.SnapToItem
                property int currCol:1;
                property int maxCol:2;
                property bool allRefresh:true;
                onCurrentIndexChanged: console.log("onCurrentIndexChanged | currentIndex "+currentIndex)
                function selectTrack(){
                    if(currCol==1){

                        var model=(viewHelper.isPlayList)?viewData.playListModel:viewData.tracklistModel
                        var params=new Object()
                        params["mid"]=model.getValue(currentIndex,"mid")
                        params["aggregateMid"]=(viewHelper.isPlayList)? viewHelper.playListIndex :model.getValue(currentIndex,"aggregate_parentmid")
                        params["mediaRepeat"]=pif.vkarmaToSeat.getMediaRepeatValue()
                        params["rating"]=model.getValue(currentIndex,"rating")
                        params["title"]=model.getValue(currentIndex,"title")
                        viewHelper.isFromKarma=true
                        var ret = dataController.mediaServices.getMidBlockStatus(params["aggregateMid"],params["rating"]);
                        if(ret){
                            console.log("MID is blocked")
                            viewController.showScreenPopup(27)
                        }

                        viewHelper.setMusicData(params)
                        console.log("pif.vkarmaToSeat.getMediaType() "+pif.vkarmaToSeat.getMediaType())
                        console.log("pif.vkarmaToSeat.getCurrentMediaPlayerState() "+pif.vkarmaToSeat.getCurrentMediaPlayerState())
                        console.log("pif.vkarmaToSeat.getPlayingMidInfo().mid "+pif.vkarmaToSeat.getPlayingMidInfo().mid)
                        if((pif.vkarmaToSeat.getMediaType()=="videoAggregate" || pif.vkarmaToSeat.getMediaType()=="video") && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop"){
                            viewHelper.exitCase = 3;
                            viewHelper.aodMidToBePlayed = params["mid"];
                            core.debug("Audio Mid to be played When hit YES :  "+params["mid"]);
                            viewController.showScreenPopup(2);
                        }else{
                            core.debug(" called api for genre")
                            if(viewHelper.isFromGenre){
                                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedPlayGenre",{genreValue:viewHelper.genreValue,genreIndex:currentIndex})
                            }else{
                                pif.vkarmaToSeat.playAodByMid({mid:params["mid"],aggregateMid:params["aggregateMid"],mediaRepeat:params["mediaRepeat"]});
                                pif.vkarmaToSeat.sendUserDefinedApi("userDefinedRadioPlay",{flag:false});
                            }
                            viewHelper.isTrackPlayedFromKids=viewHelper.isKids(current.template_id)
                        }
                        if(viewHelper.isFromKarma)widgetList.getFooterRef().getNowPlayingSource()
                        else
                            pif.vkarmaToSeat.sendUserDefinedApi("userDefinedgetPlayingCategory")
                        var model=(viewHelper.isKids(current.template_id))?viewData.kidsMenuModel:viewData.mainMenuModel
                        viewHelper.setPlayingCategory(model.getValue(viewHelper.selectedIndex,"category_attr_template_id"))
                    }
                    else{
                        if(viewHelper.isPlayList){
                            if(viewHelper.inPlaylistStack(tracksList.currentItem.getMid())){
                                viewHelper.removeFromPlaylistStack(tracksList.currentItem.getMid())
                                playListStack.removeByProperty("index",tracksList.currentIndex,1);
                            }else{
                                viewHelper.addToPlaylistStack(tracksList.currentItem.getMid());
                                playListStack.append({"index":tracksList.currentIndex,"trackmid":tracksList.currentItem.getMid()})
                            }
                        }
                        else{
                            if(viewHelper.inTracklistStack(tracksList.currentItem.getMid())){
                                viewHelper.removeFromTracklistStack(tracksList.currentItem.getMid())
                                trackStack.removeByProperty("index",tracksList.currentIndex,1);
                            }
                            else
                            {
                                viewHelper.addToTracklistStack(tracksList.currentItem.getMid())
                                trackStack.append({"index":tracksList.currentIndex,"trackmid":tracksList.currentItem.getMid()})
                            }

                        }
                        tracksList.currentItem.updateCheckBox()
                        checkPartOfStack()
                    }
                }
                function checkPartOfStack(){
                    console.log(">>>>>>>>>>>>>>>>>>> tracksList.model.count  "+tracksList.model.count)
                    for(var i=0;i<tracksList.model.count;i++){
                        var tmid=tracksList.model.getValue(i,"mid");
                        var params=new Object()
                        params["mid"]=tmid
                        params["mediaType"]="aod"
                        console.log(">>>>>>>>>>>>>>>>>>>>>> pif.aodPlaylist.isTrackInPlaylist(tmid) "+pif.vkarmaToSeat.isTrackAddedInPlaylist(params)+" viewHelper.inPlaylistStack(tmid) "+viewHelper.inPlaylistStack(tmid)+" i= "+i)
                        if(viewHelper.isPlayList){
                            if(pif.vkarmaToSeat.isTrackAddedInPlaylist(params) && viewHelper.inPlaylistStack(tmid)){
                                checkBoxAll.cstate=true;
                            }else{
                                checkBoxAll.cstate=false;
                                break;
                            }
                            console.log("checkBoxAll.cstate >>>>>>>>>>>>>>>>>>>>>>>> = "+checkBoxAll.cstate)
                        }else{
                            if(pif.vkarmaToSeat.isTrackAddedInPlaylist(params)||viewHelper.inTracklistStack(tmid)){
                                checkBoxAll.cstate=true;
                            }else{
                                checkBoxAll.cstate=false;
                                break;
                            }
                        }
                    }
                }
                delegate: Item{
                    id:mainCell
                    width:qsTranslate('','TrackPlayList_cells_'+orientationType+'_w')
                    height:qsTranslate('','TrackPlayList_cells_'+orientationType+'_h')
                    property bool currRefresh:true;
                    property bool isTrackPressed: false
                    property bool isCheckBoxPressed: false
                    function getMidState(){
                        var params=new Object()
                        params["mid"]=mid
                        params["mediaType"]="aod"
                        return pif.vkarmaToSeat.isTrackAddedInPlaylist(params);
                    }
                    function getMid(){return mid}
                    function updateCheckBox(){
                        currRefresh=false;
                        currRefresh=true;
                    }
                    function checkMid(mid){
                        var params=new Object()
                        params["mid"]=mid
                        params["mediaType"]="aod"
                        return pif.vkarmaToSeat.isTrackAddedInPlaylist(params);
                    }
                    BorderImage{
                        id:trackHighlight
                        width:(viewHelper.isKids(current.template_id))?qsTranslate('','TrackPlayList_press_'+orientationType+'_w_kids'):qsTranslate('','TrackPlayList_press_'+orientationType+'_w')
                        anchors.left: parent.left
                        anchors.bottom: parent.bottom;anchors.bottomMargin: qsTranslate('','TrackPlayList_press_'+orientationType+'_bm')
                        source:viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.listing_highlight
                        visible:mainCell.isTrackPressed && tracksList.currentIndex==index
                    }
                    ViewText{
                        id:trackTitle
                        width:(viewHelper.isKids(current.template_id))?qsTranslate('','TrackPlayList_text_'+orientationType+'_w_kids'):qsTranslate('','TrackPlayList_text_'+orientationType+'_w')
                        varText: [title,configTag[globalTrackList].tracklist.track_title_color,qsTranslate('','TrackPlayList_text_'+orientationType+'_fontsize')]
                        lineHeight:qsTranslate('','TrackPlayList_text_'+orientationType+'_lh')
                        lineHeightMode:Text.FixedHeight
                        height:qsTranslate('','TrackPlayList_text_'+orientationType+'_h')
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left;anchors.leftMargin: qsTranslate('','TrackPlayList_title_'+orientationType+'_lm')
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        elide:"ElideRight";
                        maximumLineCount:2;
                        wrapMode: Text.Wrap
                    }
                    ViewText{
                        id:trackDuration
                        width:qsTranslate('','TrackPlayList_text_'+orientationType+'_w_time')
                        anchors.left:trackTitle.right;anchors.leftMargin:qsTranslate('','TrackPlayList_text_'+orientationType+'_hgap')
                        height:parent.height
                        varText: [core.coreHelper.convertSecToMSLeadZero(core.coreHelper.convertHMSToSec(duration)),configTag[globalTrackList].tracklist.track_title_color,qsTranslate('','TrackPlayList_text_'+orientationType+'_fontsize')]
                        lineHeight:qsTranslate('','TrackPlayList_text_'+orientationType+'_lh')
                        lineHeightMode:Text.FixedHeight
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment:Text.AlignVCenter
                    }
                    Image{
                        id:nowPlayingIcon
                        visible:(pif.vkarmaToSeat.getCurrentMediaPlayerState()=="play" && pif.vkarmaToSeat.getPlayingMidInfo().mid==mid)
                        source:viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.nowplay_icon
                        anchors.left: trackDuration.right;anchors.leftMargin:qsTranslate('','TrackPlayList_playicon_'+orientationType+'_lm')
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    ToggleButton{
                        id:checkBox
                        visible:(viewHelper.isKids(current.template_id))?false:true
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right:parent.right;anchors.rightMargin: qsTranslate('','TrackPlayList_checkbox_'+orientationType+'_rm')
                        normImg1: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.tick_box
                        normImg2: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.check_box
                        pressImg1: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.tick_box_press
                        pressImg2: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.check_box_press
                        increaseTouchArea:true;
                        cstate:(viewHelper.isPlayList)?
                                   (currRefresh && tracksList.allRefresh)?
                                       (viewHelper.inPlaylistStack(mid)):false:
                                       (currRefresh && tracksList.allRefresh)?(viewHelper.inTracklistStack(mid)||pif.vkarmaToSeat.isTrackAddedInPlaylist({"mid":mid,"mediaType":"aod"})):false
                        isDisable:(viewHelper.isPlayList)?false:(tracksList.allRefresh)?checkMid(mid):false;

                        onActionReleased:{
                            tracksList.currCol=2;
                            tracksList.currentIndex=index;
                            tracksList.selectTrack()

                        }
                    }
                    Image{
                        id:divLineDel
                        source:viewHelper.configToolImagePath+configTag["tv_"+orientationType].episode.divline
                        anchors.bottom:parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    MouseArea{
                        anchors.fill:trackHighlight
                        enabled: !tracksList.moving
                        onEnabledChanged: {
                            if(!enabled)mainCell.isTrackPressed=false
                        }
                        onPressed: {
                            if(enabled)mainCell.isTrackPressed=true
                        }
                        onReleased: {
                            mainCell.isTrackPressed=false
                            tracksList.currCol=1
                            tracksList.currentIndex=index
                            playingIndex=index
                            tracksList.selectTrack()
                        }
                    }
                }
            }
            Image{
                id:fadingImage
                width:qsTranslate('','TrackPlayList_fadeimage_'+orientationType+'_w')
                anchors.bottom: tracksList.bottom;anchors.bottomMargin:qsTranslate('','TrackPlayList_fadeimage_'+orientationType+'_bm')
                anchors.horizontalCenter: tracksList.horizontalCenter
                visible:(tracksList.height+tracksList.contentY<tracksList.contentHeight)
                source:viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.fading_image
            }

            ToggleButton{
                id:checkBoxAll
                visible:(viewHelper.isKids(current.template_id) /*|| viewHelper.isPlayList*/)?false:true
                anchors.bottom:divLine.top;
                //anchors.topMargin: qsTranslate('','TrackPlayList_small_button_l_tm')
                anchors.right:trackPanel.right;
                anchors.rightMargin: qsTranslate('','TrackPlayList_checkbox_'+orientationType+'_rm')
                normImg1: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.tickbox_all
                normImg2: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.check_box_all
                pressImg1: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.tickbox_all_press
                pressImg2: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.tickbox_all_press
                //cstate:(viewHelper.isPlayList)?(currRefresh && tracksList.allRefresh)?(viewHelper.inPlaylistStack(mid)):false:(currRefresh && tracksList.allRefresh)?(viewHelper.inTracklistStack(mid)||pif.vkarmaToSeat.isTrackAddedInPlaylist({"mid":mid,"mediaType":"aod"})):false


                onActionReleased:{
                    core.debug(" Select function works : isDisable : "+isDisable)
                    core.debug(" Select function works : cstate : "+cstate)
                    core.debug(" Select function works : viewHelper.isPlayList : "+viewHelper.isPlayList)
                    if(isDisable)return;
                    if(viewHelper.isPlayList){
                        if(cstate==false){
                            cstate=true;
                            for(var i=0;i<tracksList.model.count;i++){
                                var tmid = tracksList.model.getValue(i,"mid");
                                core.debug(" Select function works : tmid : "+tmid)
                                if(!viewHelper.addToPlaylistStack(tmid)){
                                    viewHelper.addToPlaylistStack(tmid);
                                    playListStack.append({"index":i,"trackmid":tmid})
                                }
                            }
                        }else{
                            cstate=false;
                            playListStack.clear();
                            viewHelper.clearPlaylistStack()
                        }
                        tracksList.checkPartOfStack()
                    }
                    else {
                        if(cstate==false){
                            cstate=true;
                            for(var i=0;i<tracksList.model.count;i++){
                                var tmid = tracksList.model.getValue(i,"mid");
                                core.debug(" Select function works : tmid : "+tmid)
                                if(!viewHelper.addToTracklistStack(tmid)){
                                    viewHelper.addToTracklistStack(tmid);
                                    trackStack.append({"index":i,"trackmid":tmid})
                                }
                            }
                        }else{
                            cstate=false;
                            trackStack.clear();
                            viewHelper.clearTracklistStack()
                        }
                    }
                    tracksList.currentItem.updateCheckBox()
                    tracksList.allRefresh=false;
                    tracksList.allRefresh=true;
                }
            }


            BorderButton{
                id:addToPlaylist
                normImg: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.btn
                pressedImg:viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.btn_press
                normalTextColor: configTag[globalTrackList].tracklist.btn_text
                pressedTxtColor: configTag[globalTrackList].tracklist.btn_text_press
                txtWidth: qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_w')
                txtHeight: qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_h')
                txtSize:qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_fontsize')
                txtData: configTool.language.music.button_add_to_playlist
                width:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_w')

                anchors.top:trackPanel.top;
                anchors.topMargin:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_tm')
                anchors.right:checkBoxAll.left;
                anchors.rightMargin:(orientationType=='p')?0:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_hgap')

                isDisable: (trackStack.count==0)
                opacity:(isDisable)?0:1;
                border{
                    left:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_margin')
                    right:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_margin')
                }
                function addTracksToPlayList(retArray){
                    if(retArray[0]==0)return;
                    pif.vkarmaToSeat.getAllPlaylistMids({"playlistIndex":retArray[0],"mediaType":"aod"});
                    viewHelper.playListIndex = (retArray[0]);
                    addToPlaylistTimer.retArray=retArray;
                    addToPlaylistTimer.repeat=true;
                    addToPlaylistTimer.restart();
                    return;
                }
                onActionReleased: {
                    buttonClicked="addToPlaylist"
                    viewController.showScreenPopup(21)
                }
            }




            Timer{
                id:addToPlaylistTimer
                property int ctr:0;
                interval:200;
                property variant retArray;
                onTriggered:{
                    core.debug(" trackStack.count : "+trackStack.count)
                    core.debug(" ctr : "+ctr)
                    if(ctr>=trackStack.count){
                        ctr=0;
                        addToPlaylistTimer.stop();
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedRefreshPlayList",{playListRefresh:true,index:0});
                        trackStack.clear();
                        //                        checkPartOfStack();
                        trackListing.checkDisability();
                        tracksList.allRefresh=false;
                        tracksList.allRefresh=true;

                        return;
                    }
                    var tmid=trackStack.getValue(ctr,"trackmid")
                    if(viewHelper.inTracklistStack(tmid)){
                        pif.vkarmaToSeat.addMidToPlaylist(viewData.tracklistModel,{"index":ctr,"aggregateMid":viewData.tracklistModel.getValue(ctr,"aggregate_parentmid"),"rating":viewData.tracklistModel.getValue(ctr,"rating"),"playlistIndex":retArray[0]})
                    }
                    ctr++;
                }


            }


            BorderButton{
                id:addAllToPlaylist
                normImg: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.btn
                pressedImg:viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.btn_press
                normalTextColor: configTag[globalTrackList].tracklist.btn_text
                pressedTxtColor: configTag[globalTrackList].tracklist.btn_text_press
                txtWidth: qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_w')
                txtHeight: qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_h')
                txtSize:qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_fontsize')
                txtData: configTool.language.music.button_add_all_to_playlist
                width:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_w')
                anchors.top:(orientationType=='p')?addToPlaylist.bottom:trackPanel.top;anchors.topMargin:(orientationType=='p')?qsTranslate('','TrackPlayList_small_button_'+orientationType+'_vgap'): qsTranslate('','TrackPlayList_small_button_'+orientationType+'_tm')
                anchors.right:(orientationType=='p')?addToPlaylist.right:addToPlaylist.left;anchors.rightMargin:(orientationType=='p')?0:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_hgap')
                visible:false;
                opacity:0;
                border{
                    left:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_margin')
                    right:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_margin')
                }
                function addAlltracksToPlaylist(retArray){
                    if(retArray[0]==0)return;
                    pif.vkarmaToSeat.addAllMidsToPlaylist(viewData.tracklistModel,{"aggregateMid":viewData.tracklistModel.getValue(0,"aggregate_parentmid"),"rating":viewData.tracklistModel.getValue(0,"rating"),"playlistIndex":retArray[0]});
                    trackStack.clear();
                    viewHelper.clearTracklistStack()
                    tracksList.allRefresh=false;
                    tracksList.allRefresh=true;
                    tracklIstingComp.checkDisability()
                    pif.vkarmaToSeat.sendUserDefinedApi("userDefinedRefreshPlayList",{playListRefresh:true,index:retArray[0]*-1})
                    viewHelper.playListIndex = (retArray[0]);
                }
                onActionReleased: {
                    buttonClicked="addAllToPlaylist"
                    viewController.showScreenPopup(21)
                }
            }
            BorderButton{
                id:removeTrackPlaylist
                normImg: viewHelper.configToolImagePath+configTag[globalTrackList].playlist.btn
                pressedImg:viewHelper.configToolImagePath+configTag[globalTrackList].playlist.btn_press
                normalTextColor: configTag[globalTrackList].playlist.btn_text
                pressedTxtColor: configTag[globalTrackList].playlist.btn_text_press
                txtWidth: qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_w')
                txtHeight: qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_h')
                txtSize:qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_fontsize')
                txtData: configTool.language.music.remove_from_playlist
                width:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_w')
                anchors.top:trackPanel.top;anchors.topMargin: qsTranslate('','TrackPlayList_small_button_'+orientationType+'_tm')
                anchors.right:checkBoxAll.left;anchors.rightMargin: qsTranslate('','TrackPlayList_small_button_'+orientationType+'_rm')
                isDisable: (playListStack.count==0)
                opacity:(isDisable)?0:1;
                border{
                    left:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_margin')
                    right:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_margin')
                }
                function removeTracks(){
                    for (var i=0;i<viewData.playListModel.count;i++){
                        var mid=viewData.playListModel.getValue(i,"mid")
                        if(viewHelper.inPlaylistStack(mid)){
                            pif.vkarmaToSeat.removeMidFromPlaylist({"mids":[mid],"mediaType":"aod"})
                        }
                    }
                    pif.vkarmaToSeat.sendUserDefinedApi("userDefinedRefreshPlayList",{playListRefresh:true,index:viewHelper.playListIndex*-1})
                    console.log("Tracklist.qml | removeTracks ")
                    checkForEmptyPlaylist.restart()
                    playListStack.clear()
                }
                onActionReleased: {
                    buttonClicked="removeTrackPlaylist"
                    viewController.showScreenPopup(22)
                }
            }
            BorderButton{
                id:clearPlaylist
                normImg: viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.btn
                pressedImg:viewHelper.configToolImagePath+configTag[globalTrackList].tracklist.btn_press
                normalTextColor: configTag[globalTrackList].tracklist.btn_text
                pressedTxtColor: configTag[globalTrackList].tracklist.btn_text_press
                txtWidth: qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_w')
                txtHeight: qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_h')
                txtSize:qsTranslate('','TrackPlayList_small_buttontxt_'+orientationType+'_fontsize')
                txtData: configTool.language.music.clear_all_playlist
                width:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_w')
                anchors.top:(orientationType=='p')?removeTrackPlaylist.bottom:trackPanel.top;anchors.topMargin:(orientationType=='p')?qsTranslate('','TrackPlayList_small_button_'+orientationType+'_vgap'): qsTranslate('','TrackPlayList_small_button_'+orientationType+'_tm')
                anchors.right:(orientationType=='p')?trackPanel.right:removeTrackPlaylist.left;anchors.rightMargin:(orientationType=='p')?qsTranslate('','TrackPlayList_small_button_'+orientationType+'_rm'):qsTranslate('','TrackPlayList_small_button_'+orientationType+'_hgap')
                isDisable: (playListStack.count==0)
                opacity:0//(isDisable)?0:1;
                border{
                    left:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_margin')
                    right:qsTranslate('','TrackPlayList_small_button_'+orientationType+'_margin')
                }
                function clearAllTracks(){
                    var r=pif.vkarmaToSeat.removeAllFromPlaylist({"playlistIndex":viewHelper.playListIndex,"mediaType":"aod"})
                    pif.vkarmaToSeat.sendUserDefinedApi("userDefinedRefreshPlayList",{playListRefresh:true,index:viewHelper.playListIndex*-1})
                    if(r==0){viewHelper.clearPlaylistStack();viewController.showScreenPopup(15)}
                }
                onActionReleased: {
                    buttonClicked="clearPlaylist"
                    viewController.showScreenPopup(22)
                }
            }
            Timer{
                id:delayRefresh;
                interval:1000;
                onTriggered:{
                    tracksList.allRefresh=false;
                    tracksList.allRefresh=true;
                }
            }
        }



    }

}

