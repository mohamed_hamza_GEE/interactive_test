import QtQuick 1.1
import "../../framework/viewcontroller"
import "../components"
import "../../framework"

SmartLoader {
    id: synopsis
    /**************************************************************************************************************************/
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'p'
    property variant configMenu: (viewHelper.isKids(current.template_id))?"kids_menu_"+orientationType:"menu_"+orientationType
    property variant cgModel:viewHelper.blankModel
    property variant arrivalInfoModel: viewHelper.blankModel
    property bool isEpisodeSynopsisOpen: false
    property bool isCgSynopsisOpen: false
    property variant cgDetailData:["","","","","","",""];
    property variant dataObj;
    property bool episodeListMoving: false
    property variant refModel
    property variant itemdetails;
    property string actualPrice:" ";
    property string detail:" ";
    property string currencyCode:" ";
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compSynopsisPanel,"synopsisPanel")
    }
    function init(){
        console.log("Synopsis | init called")
        setHeaderFooter()
        showSynopsis()
    }

    function clearOnExit(){
        core.debug("Synopsis | clearOnExit called")
        removeHeaderFooter()
        viewHelper.launchAppVkarma=""
        viewHelper.synopsisModel=viewHelper.blankModel
        viewHelper.episodeModel=viewHelper.blankModel
        cgModel=viewHelper.blankModel
        refModel=viewHelper.blankModel
        synopsisPanel.setDelegate("empty")

    }
    onLoaded: { core.debug("Synopsis | onLoaded called") }
    /**************************************************************************************************************************/
    Connections{
        target:(visible)?dataController.connectingGate:null
        onSigCgStatusChanged:{
            if(status==true){
                viewController.showScreenPopup(11)
                init()
            }else{
                viewController.showScreenPopup(10)
                viewController.loadPrev()
            }
        }
    }
    Connections{
        target:(visible)?core.pif:null
        onCgDataAvailableChanged:{
            if(cgDataAvailable==1)return
            else{
                viewController.showScreenPopup(10)
                viewHelper.homeCalled()
            }
        }
    }
    Connections{
        target:(visible)?viewHelper:null
        onShoppingLangUpdate:{
            console.log(" Synopsis.qml | onShopping ")
            showSynopsis()
        }
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(true)
        widgetList.showFooter(true)
        widgetList.showFooterHomeBtn(true)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function loadPreviousScreen(){
        viewHelper.launchAppVkarma=""
        if(isEpisodeSynopsisOpen ){
            isEpisodeSynopsisOpen=false
            showSynopsis()
        }
        else if(isCgSynopsisOpen){
            isCgSynopsisOpen=false
            loadCgModel()
        }else if(viewHelper.catSelected=="shopping"){
            viewController.loadScreen("shopping_listing")
        }else if(current.template_id=='discover'){
            viewController.loadScreen("discover")
        }else if(viewHelper.isKids(current.template_id) || !viewHelper.isKids(current.template_id)){
            viewController.loadScreen(current.template_id)
        }else {
            viewController.loadPrev()
        }

    }
    function loadCgModel(){
        pif.paxus.startContentLog('Connecting Gate')
        if(dataController.connectingGate.getCgModel().count<=0 ||dataController.connectingGate.getArrivalInfoModel()<=0)
            dataController.connectingGate.fetchData(cgModelReady)
        else{
            cgModel=viewHelper.blankModel
            cgModel=dataController.connectingGate.getCgModel()
            synopsisPanel.setDelegate("empty")
            synopsisPanel.setDelegate('episodes')
            setArrivalModel(dataController.connectingGate.getCgModel())
        }
    }
    function cgModelReady(dModel,status,modelId,errNum){
        if(modelId == "arrivalInfoModel"){
            setArrivalModel(dModel,status,errNum)
        }else{
            setCgModel(dModel,status,errNum);
        }
    }
    function setArrivalModel(dModel,status,errNum){
        if(errNum==0 && dModel.count > 0 && status == true){
            arrivalInfoModel=viewHelper.blankModel
            arrivalInfoModel=dModel
            updateArrDetails(dModel.get(0))
        }
        else {
        }
    }
    function setCgModel(dModel,status,errNum){
        if(errNum==0 && dModel.count > 0 && status == true){
            cgModel=viewHelper.blankModel
            cgModel=dModel
            console.log("setCgModel "+cgModel.count)
            synopsisPanel.setDelegate("empty")
            synopsisPanel.setDelegate('episodes')
        }
        else{
             viewController.showScreenPopup(27)
        }
    }
    function synopsisModelReady(dModel,status,type){
        console.log("dModel.cpount = "+dModel.count)
        if(status){
            core.debug("MediaListing.qml | synopsisModelReady | status : "+status)
            refModel = viewHelper.blankModel;
            refModel = dModel;
            modelDisplay.model=refModel
            getPrices(itemdetails['price'])
            synopsisPanel.setModel(viewHelper.blankModel)
            synopsisPanel.setModel(viewHelper.shoppingMediaModel)
            synopsisPanel.changeUserInteraction(true)
            synopsisPanel.setCurrentIndex(viewHelper.mediaSelected)
            synopsisPanel.setDelegate("empty")
            synopsisPanel.setDelegate('shopping')

        }else{
            core.debug("MediaListing.qml | synopsisModelReady | status : | else "+status)
        }
    }

    function getPrices(detailsPrice){
        for(var i=0;i<detailsPrice.length;i++){
            if(detailsPrice[i].item_currency_code == "USD"){
                actualPrice = detailsPrice[i].item_currency_symbol_left +detailsPrice[i].item_price
                currencyCode = detailsPrice[i].item_currency_code
            }
        }
    }
    Repeater{
        id:modelDisplay
        Item {
            id:modelItem
            Component.onCompleted:{
                itemdetails = value
            }
        }
    }

    function showSynopsis(){
        if(viewHelper.catSelected=="shopping"){

            viewHelper.shop_item_id = viewHelper.shoppingMediaModel.getValue(viewHelper.mediaSelected,"item_id");
            console.log("media selected  | viewHelper.shop_item_id = "+viewHelper.shop_item_id)
            console.log("media selected  | viewHelper.mediaSelected = "+viewHelper.mediaSelected)
            core.dataController.shopping.getItemDetailById(viewHelper.shop_item_id ,synopsisModelReady)
        }else{
            var mid=viewData.mediaModel.getValue(viewHelper.mediaSelected,"mid")
            var mediaAttribute=viewData.mediaModel.getValue(viewHelper.mediaSelected,"media_attr_template_id")
            viewHelper.synopsisModel=viewData.mediaModel;
            synopsisPanel.setModel(viewHelper.synopsisModel)
            synopsisPanel.setCurrentIndex(viewHelper.mediaSelected)
            if(mediaAttribute=='tvseries'){
                dataController.getNextMenu([mid,'tvseries'],episodeModelReady);
            }
            else{
                if(viewHelper.launchAppVkarma=="connecting_gate"||viewHelper.launchAppVkarma=="airport_map"||viewHelper.launchAppVkarma=="maps" || viewHelper.launchAppVkarma=="survey"|| viewHelper.launchAppVkarma=="milesapp"|| viewHelper.launchAppVkarma=="airline_info")
                    synopsisPanel.setDelegate("discover")
                else
                    synopsisPanel.setDelegate(current.template_id);
                synopsisPanel.changeUserInteraction(true)
                if(current.template_id=='discover'&& viewHelper.launchAppVkarma=="connecting_gate"){
                    viewHelper.isCgSelected=(viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")=='connecting_gate')?true:false
                    if(viewData.mediaModel.getValue(viewHelper.mediaSelected,"category_attr_template_id")=='connecting_gate')
                        getCgData(viewHelper.synopsisModel.get(viewHelper.mediaSelected))
                }
            }
        }
    }
    function episodeModelReady(dModel){
        viewHelper.tvSeriesTitle=viewData.mediaModel.getValue(viewHelper.mediaSelected,"title")
        viewHelper.episodeModel=viewHelper.blankModel
        viewHelper.episodeModel=dModel
        viewHelper.setTvModel(dModel)
        synopsisPanel.setDelegate("empty")
        synopsisPanel.setDelegate('episodes')
        synopsisPanel.changeUserInteraction(true)
    }
    function showEpisodeSynopsis(index){
        isEpisodeSynopsisOpen=true
        synopsisPanel.setModel(viewHelper.episodeModel)
        synopsisPanel.setDelegate('episodeSynopsis')
        synopsisPanel.setCurrentIndex(index)
        synopsisPanel.changeUserInteraction(true)
    }
    function showCgSynopsis(index){
        isCgSynopsisOpen=true
        getCgData(cgModel.get(index));
        synopsisPanel.setModel(cgModel)
        synopsisPanel.setDelegate("empty")
        synopsisPanel.setDelegate('cgSynopsis')
        synopsisPanel.setCurrentIndex(index)
        synopsisPanel.changeUserInteraction(false)
    }
    function updateArrDetails(data){
        dataObj=data;
        var d = data["sector_scheduled_arrival"];
        var currTime = "<b>"+d.split(" ")[1]+"</b>";
        viewHelper.cgArrivalData=[
                    getData("terminal_name"),
                    currTime,
                    getData("gate_designator"),
                    getData("baggage_area_designator"),
                    getData("carrier_code"),
                    getData("flight_number")
                ];
    }
    function getCgData(data){
        var d = new Date(data["sector_scheduled_departure"]*1000)
        var currTime = "<b>"+d.getHours()+":"+d.getMinutes()+"</b>";
        var day = "<b>"+d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+"</b>";
        dataObj=data;
        cgDetailData=[
                    getData("arrival_city"),
                    getData("operator_name"),
                    getData("carrier_code")+" "+getData("flight_number"),
                    currTime,
                    getData("departure_gate_designator"),
                    day,
                    getData("sector_status")];
    }
    function getData(tag){
        if(dataObj[tag]){
            return "<b>"+dataObj[tag]+"</b>";
        }else{
            return "";
        }
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            width:bg.width;
            height:bg.height
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag[configMenu].main.bg
            }
            Image{
                id:catBgImage
                anchors.bottom:parent.bottom
                source:viewHelper.configToolImagePath+configTag["shop_"+orientationType].listing.background
            }
        }
    }
    property QtObject synopsisPanel
    Component{
        id:compSynopsisPanel
        Item{
            anchors.fill:background;
            property alias lView:synopsisListView
            function changeUserInteraction(val){synopsisListView.interactive=val}
            function setCurrentIndex(index){
                synopsisListView.currentIndex=index
                console.log("setCurrentIndex | index "+synopsisListView.currentIndex)
                synopsisListView.positionViewAtIndex(synopsisListView.currentIndex,ListView.Beginning)
            }
            function setDelegate(tempId){
                synopsisListView.delegate=viewHelper.emptyDel
                if(tempId=='movies_listing'|| tempId=='kids_movies_listing')
                    synopsisListView.delegate=compMoviesSynopsis
                else if(tempId=='tv_listing'|| tempId=='kids_tv_listing')
                    synopsisListView.delegate=compTvSynopsis
                else if(tempId=='games'|| tempId=='kids_games_listing')
                    synopsisListView.delegate=compGameSynopsis
                else if(tempId=='episodes')
                    synopsisListView.delegate=compEpisodeListing
                else if(tempId=='episodeSynopsis')
                    synopsisListView.delegate=compEpisodeSynopsis
                else if(tempId=='discover')
                {synopsisListView.delegate=compDiscoverSynopsis;}
                else if(tempId=='cgSynopsis')
                    synopsisListView.delegate=compCgSynopsis
                else if(tempId=='shopping')
                    synopsisListView.delegate=compShoppingSynopsisSynopsis
                else if(tempId=='empty')
                    synopsisListView.delegate=viewHelper.emptyDel
            }
            function setModel(dModel){
                synopsisListView.model=dModel
            }
            Image{
                id:leftArrow
                visible: synopsisListView.currentIndex>0
                source:(viewHelper.catSelected=='shopping')?viewHelper.configToolImagePath+configTag["shop_"+orientationType].synopsis.left_arrow:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.left_arrow
                anchors.left:parent.left;anchors.leftMargin: qsTranslate('','Common_arrow_l_hgap')
                anchors.verticalCenter: synopsisListView.verticalCenter
            }
            Image{
                id:rightArrow
                visible: synopsisListView.currentIndex<synopsisListView.model.count-1
                source:(viewHelper.catSelected=='shopping')?viewHelper.configToolImagePath+configTag["shop_"+orientationType].synopsis.right_arrow:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].synopsis.right_arrow
                anchors.right:parent.right;anchors.rightMargin: qsTranslate('','Common_arrow_l_hgap')
                anchors.verticalCenter: synopsisListView.verticalCenter
            }

            ListView{
                id:synopsisListView
                snapMode:"SnapOneItem";
                width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
                height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
                anchors.horizontalCenter:parent.horizontalCenter
                anchors.top:parent.top;anchors.topMargin: qsTranslate('','Common_whitepanel_'+orientationType+'_tm')
                boundsBehavior:ListView.StopAtBounds;
                preferredHighlightBegin:0;
                preferredHighlightEnd:width;
                highlightRangeMode:ListView.StrictlyEnforceRange;
                orientation:ListView.Horizontal
                clip:true;
                onOrientationChanged: positionViewAtIndex(currentIndex,ListView.Beginning)
                interactive: true
                onContentXChanged: console.log("onContentXChanged "+contentX)
                onCurrentIndexChanged: console.log("onCurrentIndexChanged "+currentIndex)
                onMovementEnded: {
                    if(indexAt(contentX,contentY)>=0){
                        currentIndex=indexAt(contentX,contentY);
                        viewHelper.launchAppVkarma=model.getValue(currentIndex,"category_attr_template_id")
                        if(!isEpisodeSynopsisOpen){
                            viewHelper.mediaSelected=currentIndex;

                            showSynopsis()
                        }
                        else{

                            viewHelper.episodeIndex=currentIndex
                        }

                    }
                }
            }
            Rectangle{
                id:categoryTile
                color:"transparent"
                width:qsTranslate('','Common_cattitle_cont_'+orientationType+'_w')
                height:qsTranslate('','Common_cattitle_cont_'+orientationType+'_h')
                anchors.top: parent.top;anchors.topMargin: qsTranslate('','Common_cattitle_cont_'+orientationType+'_tm1')
                anchors.horizontalCenter: parent.horizontalCenter
                clip:true
                Image{
                    id:catIcon
                    width:qsTranslate('','Common_caticon_'+orientationType+'_w')
                    height:qsTranslate('','Common_caticon_'+orientationType+'_h')
                    source:viewHelper.mediaImagePath+viewHelper.catIcon
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right:catTitle.left;anchors.rightMargin: qsTranslate('','Common_caticon_'+orientationType+'_hgap')
                }
                ViewText{
                    id:catTitle
                    varText:{core.debug(viewData.iso);
                        [(viewHelper.isKids(current.template_id))?
                             viewData.kidsMenuModel.getValue(viewHelper.selectedIndex,"title"):
                             (current.template_id=='discover'||current.template_id=='adult'||current.template_id=='games'||viewHelper.catSelected=='shopping')?
                                 viewData.mainMenuModel.getValue(viewHelper.selectedIndex,"title"):
                                 viewData.subMenuModel.getValue(viewHelper.subMenuSelIndex,"title"),
                                 (viewHelper.catSelected=='shopping')?configTag["shop_"+orientationType].synopsis.category_txt_color:configTag[viewHelper.catSelected+"_"+orientationType].synopsis.category_txt_color,qsTranslate('','Common_cattitle_'+orientationType+'_fontsize')]}
                    height:qsTranslate('','Common_cattitle_'+orientationType+'_h')
                    anchors.centerIn: parent
                    anchors.horizontalCenterOffset:catIcon.anchors.rightMargin+catIcon.width/2
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    elide: Text.ElideRight
                    onPaintedWidthChanged: {
                        if(paintedWidth<qsTranslate('','Common_cattitle_'+orientationType+'_w'))return
                        else width=qsTranslate('','Common_cattitle_'+orientationType+'_w')
                    }
                }
            }
        }
    }
    Component{
        id:connectingGateDel
        Item{
            width:qsTranslate('','EpisodeCg_cells_'+orientationType+'_w')
            height:qsTranslate('','EpisodeCg_cells_'+orientationType+'_h')
            property bool isPressed: false
            Rectangle{
                width:qsTranslate('','EpisodeCg_cg_text_'+orientationType+'_w_total')
                height:qsTranslate('','EpisodeCg_cg_text_'+orientationType+'_h')
                color:"transparent"
                anchors.centerIn: parent
                ViewText{
                    id:cgflightNumber
                    width:qsTranslate('','EpisodeCg_cg_text_'+orientationType+'_w_single')
                    anchors.left:parent.left;anchors.top:(orientationType=='p')?parent.top:parent.top
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    varText: [carrier_code+" "+flight_number,configTag[viewHelper.catSelected+"_"+orientationType].synopsis.category_txt_color,qsTranslate('','EpisodeCg_cg_text_'+orientationType+'_fontsize')]
                }
                ViewText{
                    id:deptTime
                    property variant dateObj:new Date(sector_scheduled_departure*1000);
                    property string currTime:dateObj.getHours()+":"+dateObj.getMinutes();
                    width:qsTranslate('','EpisodeCg_cg_text_'+orientationType+'_w_single')
                    height:qsTranslate('','EpisodeCg_text_'+orientationType+'_h')
                    anchors.right:parent.right;anchors.top:(orientationType=='l')?parent.top:cgflightNumber.bottom
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                    varText: [configTool.language.connecting_gate.departure_time+" - "+currTime,configTag[viewHelper.catSelected+"_"+orientationType].synopsis.category_txt_color,qsTranslate('','EpisodeCg_cg_text_'+orientationType+'_fontsize')]
                }
            }
            Image{
                id:divLine
                source:viewHelper.configToolImagePath+configTag["tv_"+orientationType].episode.divline
                anchors.bottom:parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }
            BorderImage{
                id:episodeHighlight
                width:parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                source:(parent.isPressed)?viewHelper.configToolImagePath+configTag["discover_"+orientationType].synopsis.listing_highlight:""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !episodeListMoving
                onEnabledChanged: {
                    if(!enabled)parent.isPressed=false
                }
                onPressed: {
                    if(enabled)parent.isPressed=true;
                }
                onReleased: {
                    parent.isPressed=false
                    showCgSynopsis(index)
                }
            }
        }
    }
    Component{
        id:episodeDel
        Item{
            width:qsTranslate('','EpisodeCg_cells_'+orientationType+'_w')
            height:qsTranslate('','EpisodeCg_cells_'+orientationType+'_h')
            property bool isPressed: false
            ViewText{
                id:episodeTitle
                width:qsTranslate('','EpisodeCg_text_'+orientationType+'_w')
                height:qsTranslate('','EpisodeCg_text_'+orientationType+'_h')
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
                varText: [(index+1)+" "+title,configTag[viewHelper.catSelected+"_"+orientationType].synopsis.category_txt_color,qsTranslate('','EpisodeCg_text_'+orientationType+'_fontsize')]
                lineHeight:qsTranslate('','EpisodeCg_text_'+orientationType+'_lh')
                lineHeightMode:Text.FixedHeight
            }
            Image{
                id:divLine
                source:viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].episode.divline
                anchors.bottom:parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }
            BorderImage{
                id:episodeHighlight
                width:parent.width
                anchors.centerIn: parent
                source:(parent.isPressed)?viewHelper.configToolImagePath+configTag[viewHelper.catSelected+"_"+orientationType].episode.listing_highlight:""
            }
            MouseArea{
                anchors.fill:parent
                enabled: !episodeListMoving
                onEnabledChanged: {
                    if(!enabled)parent.isPressed=false
                }
                onPressed: {
                    if(enabled)parent.isPressed=true;
                }
                onReleased: {
                    parent.isPressed=false
                    synopsisPanel.setCurrentIndex(index)
                    viewHelper.episodeIndex=index
                    showEpisodeSynopsis(index)
                }
            }
        }
    }
    Component{
        id:compMoviesSynopsis
        MovieSynopsis{
            id:synpanel
            width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
            height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
        }
    }
    Component{
        id:compTvSynopsis
        TvSynopsis{
            id:tvSynopsis
            width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
            height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
        }
    }
    Component{
        id:compGameSynopsis
        GameSynopsis{
            id:gamesSynopsis
            width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
            height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
        }
    }
    Component{
        id:compEpisodeListing
        EpisodeListing{
            id:episodeListing
            width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
            height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
        }
    }
    Component{
        id:compEpisodeSynopsis
        EpisodeSynospis{
            id:episodeSynopsis
            width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
            height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
        }
    }
    Component{
        id:compDiscoverSynopsis
        DiscoverSynopsis{
            id:discoverSynopsis
            width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
            height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
        }
    }
    Component{
        id:compCgSynopsis
        CgSynopsis{
            id:cgSynopsis
            width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
            height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
        }
    }
    Component{
        id:compShoppingSynopsisSynopsis
        ShoppingSynopsis{
            id:shoppingSynopsisSynopsis
            width:qsTranslate('','Common_whitepanel_'+orientationType+'_w');
            height:qsTranslate('','Common_whitepanel_'+orientationType+'_h');
        }

    }
}
