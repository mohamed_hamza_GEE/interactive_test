import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: optionSel
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'l'
    property int adultIndex:0;
    property int kidsIndex:0;
    /**************************************************************************************************************************/
    function load(){
        push(compBackground,"background")
        push(compOptionMenu,"optionMenu")
    }
    function init(){
        console.log("Option Selection Screen.qml | init")
        setHeaderFooter()
        getOptionMenu()

    }
    function reload(){console.log("Option Selection Screen.qml | reload");init()}
    function clearOnExit(){
        console.log("Option Selection Screen.qml | clearOnExit")
        viewHelper.sectionSelected=false
        removeHeaderFooter()
    }
    onLoaded: {}

    Connections{
        target: core;
        onSigLanguageChanged:{
            getOptionMenu()
        }
    }

    /**************************************************************************************************************************/
    Connections{
        target:optionSel.visible?viewData:null
        onOptionMenuModelReady:{
            for(var i=0;i<viewData.optionMenuModel.count;i++){
                if(viewData.optionMenuModel.getValue(i,"category_attr_template_id")=="adult"){
                    optionMenu.setAdultSectionText(viewData.optionMenuModel.getValue(i,"title"))
                    adultIndex=i
                }
                else  if(viewData.optionMenuModel.getValue(i,"category_attr_template_id")=="kids"){
                    optionMenu.setKidsSectionText(viewData.optionMenuModel.getValue(i,"title"))
                    kidsIndex=i
                }
            }
            if(viewHelper.sectionSelected){
                jumpToNextScreen(viewHelper.sectionIndexSel)
            }
        }
    }
    /**************************************************************************************************************************/
    function setHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(true)
//        widgetList.showFooterHomeBtn(false)
    }
    function removeHeaderFooter(){
        widgetList.showHeader(false)
        widgetList.showFooter(false)
    }
    function getOptionMenu(){dataController.getNextMenu([dataController.getRootNode()[0],dataController.getRootNode()[1]],viewData.optionMenuReady)}
    function jumpToNextScreen(selIndex){
        if(viewData.optionMenuModel.getValue(selIndex,"category_attr_template_id")=="adult")
            widgetList.setSectionBtnText(configTool.language.global_elements.button_kids_text)
        else
            widgetList.setSectionBtnText(configTool.language.global_elements.button_mainmenu_text)
        viewController.loadNext(viewData.optionMenuModel,selIndex);
    }

    function loadPreviousScreen(){
        if(widgetList.getSettingsRef().visible)widgetList.getSettingsRef().loadPreviousScreen()
        else{
            viewHelper.fromBack=true;
            viewController.jumpTo(0,"WELC");
        }
    }
    /**************************************************************************************************************************/
    property QtObject background
    Component{
        id:compBackground
        Item{
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            }
            Image{
                id:logo
                smooth: true
                source:viewHelper.configToolImagePath+(configTag["global_"+orientationType].language_sel.lang_logo);
                 //viewHelper.configToolImagePath+(viewHelper.isAviancaBrasilAir?configTag["global_"+orientationType].language_sel.lang_oceanair_logo:configTag["global_"+orientationType].language_sel.lang_logo);
                height: qsTranslate('','Language_logo_'+orientationType+'_h')
                width:qsTranslate('','Language_logo_'+pif.getOrientationType()+'_w')
                anchors.top:bg.top;anchors.topMargin: qsTranslate('','Language_logo_'+orientationType+'_tm')
                anchors.left: bg.left;anchors.leftMargin: qsTranslate('','Language_logo_'+orientationType+'_lm')

            }
        }
    }
    property QtObject optionMenu
    Component{
        id:compOptionMenu
        Item{
            anchors.fill:background
            function setAdultSectionText(value){adultSectionText.text=value}
            function setKidsSectionText(value){kidsSectionText.text=value}
            SimpleButton{
                id:kidsSection
                normImg:viewHelper.configToolImagePath+configTag["global_"+orientationType].option_sel.kids_btn
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].option_sel.kids_btn_press
                anchors.top:parent.top;anchors.topMargin:qsTranslate('','Option_button_'+orientationType+'_tm')
                anchors.left:parent.left;anchors.leftMargin:qsTranslate('','Option_button_'+orientationType+'_lm')
                ViewText{
                    id:kidsSectionText
                    anchors.bottom:kidsSection.bottom;anchors.bottomMargin: qsTranslate('','Option_buttontxt_bm')
                    anchors.horizontalCenter:kidsSection.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter
                    width:qsTranslate('','Option_buttontxt_w');height:qsTranslate('','Option_buttontxt_h')
                    varText: ['',kidsSection.pressedImg?configTag["global_"+orientationType].option_sel.adult_btn_txt_press:configTag["global_"+pif.getOrientationType()].option_sel.adult_btn_txt,qsTranslate('','Option_buttontxt_fontsize')]
                    wrapMode: Text.WordWrap
                    font.family:"FS Elliot"
                }
                onActionReleased: {

                    if((pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate") && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" && viewHelper.sectionIndexSel!=kidsIndex){
                        viewHelper.sectionIndexSel=adultIndex; //this looks wrong but its been set so that the swap works
                        viewHelper.stopVod(true)
                        viewHelper.sectionBtnPressed=true
                    }else{
                        jumpToNextScreen(kidsIndex)
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedJump",{section:"kids",language:viewHelper.selectedIso})
                    }
                }
            }
            SimpleButton{
                id:adultSection
                normImg:viewHelper.configToolImagePath+configTag["global_"+orientationType].option_sel.adult_btn
                pressedImg: viewHelper.configToolImagePath+configTag["global_"+orientationType].option_sel.adult_btn_press
                anchors.left: (pif.getOrientationType()=='p')?parent.left:kidsSection.right
                anchors.leftMargin:(pif.getOrientationType()=='p')?qsTranslate('','Option_button_p_lm'):qsTranslate('','Option_button_l_hgap')
                anchors.top: (pif.getOrientationType()=='p')?kidsSection.bottom:parent.top
                anchors.topMargin: (pif.getOrientationType()=='p')?qsTranslate('','Option_button_p_vgap'):qsTranslate('','Option_button_l_tm')
                ViewText{
                    id:adultSectionText
                    anchors.bottom:adultSection.bottom;anchors.bottomMargin: qsTranslate('','Option_buttontxt_bm')
                    horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter
                    width:qsTranslate('','Option_buttontxt_w');height:qsTranslate('','Option_buttontxt_h')
                    anchors.horizontalCenter:adultSection.horizontalCenter
                    varText: ['',adultSection.pressedImg?configTag["global_"+orientationType].option_sel.kids_btn_txt_press:configTag["global_"+orientationType].option_sel.kids_btn_txt,qsTranslate('','Option_buttontxt_fontsize')]
                    wrapMode: Text.WordWrap
                }
                onActionReleased: {

                    if((pif.vkarmaToSeat.getMediaType()=="video" || pif.vkarmaToSeat.getMediaType()=="videoAggregate") && pif.vkarmaToSeat.getCurrentMediaPlayerState()!="stop" && viewHelper.sectionIndexSel!=adultIndex){
                        viewHelper.sectionIndexSel=kidsIndex; //this looks wrong but its been set so that the swap works
                        viewHelper.stopVod(true)
                        viewHelper.sectionBtnPressed=true
                    }else{

                        jumpToNextScreen(adultIndex)
                        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedJump",{section:"adult",language:viewHelper.selectedIso})
                    }
                }
            }
        }
    }
}
