import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/viewcontroller"
import "../components"

SmartLoader {
    id: langSelect
    property variant configTag: configTool.config
    property string orientationType: (visible)?pif.getOrientationType():'l'

    /**************************************************************************************************************************/
    Connections{
        target:viewHelper
        onSeatUp:{
            languageMenu.welcomeTransition.restart()
        }
    }
    /**************************************************************************************************************************/
    function load(){
        push(compLanguageMenu,"languageMenu")
        push(compEntOn,"entOn")
    }
    function init(){
        console.log("Welcome | init")
        if(core.demo)languageMenu.welcomeTransition.restart();
        pif.vkarmaToSeat.sendUserDefinedApi("userDefinedgetSeatStatus")

    }
    function reload(){}
    function clearOnExit(){
        console.log("Welcome | clearOnExit")
        languageMenu.welcomerevTransition.restart()
    }
    onLoaded: {}
    /*****************************************************************************************************************/
    function selectLanguage(iso){
        core.setInteractiveLanguage(dataController.getLidByLanguageISO(iso),iso)
        pif.vkarmaToSeat.languageChange({languageIso:iso})
        viewHelper.sectionSelected=false
        viewHelper.selectedIso=iso
        viewController.jumpTo(2,"OPTSEL")
    }
    /*****************************************************************************************************************/
    property QtObject entOn
    Component{
        id:compEntOn
        Image{
            id:entOnbg
            source:viewHelper.configToolImagePath+configTag["global_"+orientationType].entertainment_off.entoff_bg
            visible:(!viewHelper.fromBack)
            Image{
                id:entOnLogo
                anchors.centerIn: parent
                source:viewHelper.configToolImagePath+(configTag["global_"+orientationType].entertainment_off.entoff_logo);
                 //viewHelper.configToolImagePath+(viewHelper.isAviancaBrasilAir?configTag["global_"+orientationType].entertainment_off.entoff_oceanair_logo:configTag["global_"+orientationType].entertainment_off.entoff_logo);
                smooth:true
            }
        }
    }
    property QtObject languageMenu
    Component{
        id:compLanguageMenu
        Item{
            property alias langmenu:langmenu
            property alias welcomeTransition: welcomeTransition
            property alias welcomerevTransition: welcomerevTransition
            Image{
                id:bg
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].language_sel.lang_bg
            }
            Image{
                id:logo
                smooth: true
                opacity: 0
                source:viewHelper.configToolImagePath+(configTag["global_"+orientationType].language_sel.lang_logo);
                  //viewHelper.configToolImagePath+(viewHelper.isAviancaBrasilAir?configTag["global_"+orientationType].language_sel.lang_oceanair_logo:configTag["global_"+orientationType].language_sel.lang_logo);
                height: qsTranslate('','Language_logo_'+pif.getOrientationType()+'_h')
                width:qsTranslate('','Language_logo_'+pif.getOrientationType()+'_w')
                anchors.top:bg.top;anchors.topMargin: qsTranslate('','Language_logo_'+orientationType+'_tm')
                anchors.left: bg.left;anchors.leftMargin: qsTranslate('','Language_logo_'+orientationType+'_lm')
            }
            Path{
                id:menupath
                //first point
                startX: langmenu.width/2
                startY: 0
                PathAttribute{name: 'pathOpacity'; value:0.7/*qsTranslate('','Language_container_opacity')*/}
                PathAttribute{name: 'fontSize'; value:qsTranslate('','Language_container_fontsize_n')}
                //second point
                PathLine{x:langmenu.width/2; y:langmenu.height*0.5}
                PathAttribute{name: 'pathOpacity'; value:1 }
                PathAttribute{name: 'fontSize'; value: qsTranslate('','Language_container_fontsize_h')}
                //third point
                PathLine{x:langmenu.width/2; y:langmenu.height}
                PathAttribute{name: 'pathOpacity'; value: 0.7/*qsTranslate('','Language_container_opacity')*/}
                PathAttribute{name: 'fontSize'; value: qsTranslate('','Language_container_fontsize_n')}
            }
            PathView{
                id:langmenu
                focus: true
                opacity: 0
                anchors.centerIn: bg
                height:qsTranslate('','Language_container_h')
                width:qsTranslate('','Language_container_w')
                path:menupath;
                preferredHighlightBegin: 0.5
                preferredHighlightEnd: 0.5
                highlightMoveDuration: 200
                pathItemCount:(model.count<5)?3:5;
                model:dataController.getIntLanguageModel()
                delegate:Text {
                    text: name
                    color:(langmenu.currentIndex==index)?configTag["global_"+orientationType].language_sel.language_text:configTag["global_"+pif.getOrientationType()].language_sel.language_text
                    opacity:PathView.pathOpacity;
                    font.pixelSize: PathView.fontSize
                    font.family:"FS Elliot Thin"
                    MouseArea{
                        anchors.fill: parent
                        onReleased:  {
                            langmenu.currentIndex=index;
                            viewHelper.setLanguageId(index)
                            selectLanguage(ISO639)
                        }
                    }
                }
            }
            Image{
                id:up_arrow
                opacity: 0
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].language_sel.up_arrow
                anchors.bottom:langmenu.top;anchors.bottomMargin: qsTranslate('','Language_arrow_gap')
                anchors.horizontalCenter: langmenu.horizontalCenter
            }
            Image{
                id:down_arrow
                opacity: 0
                source:viewHelper.configToolImagePath+configTag["global_"+orientationType].language_sel.down_arrow
                anchors.top: langmenu.bottom;anchors.topMargin:qsTranslate('','Language_arrow_gap')
                anchors.horizontalCenter: langmenu.horizontalCenter
            }
            ParallelAnimation{
                id:welcomeTransition;
                NumberAnimation{target:entOn;properties: "opacity";from:1;to:0;duration:1500}
                SequentialAnimation{
                    PauseAnimation { duration: 750}
                    ParallelAnimation{
                        NumberAnimation{target: logo;properties: "opacity";from:0;to:1;duration:1000;}
                    }
                    ParallelAnimation{
                        NumberAnimation{target: langmenu;properties: "opacity";from:0;to:1;duration:900;}
                        NumberAnimation{target: up_arrow;properties: "opacity";from:0;to:1;duration:900;}
                        NumberAnimation{target: down_arrow;properties: "opacity";from:0;to:1;duration:900;}
                    }
                }
                ScriptAction{
                    script:{
                        viewHelper.fromBack=false;
                    }
                }
            }
            ParallelAnimation{
                id:welcomerevTransition;
                NumberAnimation{target:entOn;properties: "opacity";from:0;to:1;duration:1}
                SequentialAnimation{
                    PauseAnimation { duration: 750}
                    ParallelAnimation{
                        NumberAnimation{target: logo;properties: "opacity";from:1;to:0;duration:1;}
                    }
                    ParallelAnimation{
                        NumberAnimation{target: langmenu;properties: "opacity";from:1;to:0;duration:1;}
                        NumberAnimation{target: up_arrow;properties: "opacity";from:1;to:0;duration:1;}
                        NumberAnimation{target: down_arrow;properties: "opacity";from:1;to:0;duration:1;}
                    }
                }
            }
        }
    }
}
