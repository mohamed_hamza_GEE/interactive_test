import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../view"
import "viewData.js" as ViewData

//Purpose: Controls the visual aspect of the interactive.
FocusScope {
    id: viewController;
    width: core.settings.screenWidth;
    height: core.settings.screenHeight;
    focus: true;
    // View shared variables
    property variant viewHelper;
    property variant viewData;
    property variant pifEvents;
    property variant widgetList;
    property variant viewStyle;
    property variant testPanel;
    property variant seatInfo;
    property variant forceAVScreen;
    property alias bg: bg;
    property alias screen: screen;
    property alias screenPopups: screenPopups;
    property alias systemPopups: systemPopups;
    property bool __disableLookAheadLoading: false;
    property int __unloadKickInTimout: 7000;

    property QtObject privateVar:QtObject{ property string __activeScreen;}
    property QtObject settings:QtObject{
        id: settings
        // Path
        property string contentImagePath:'common/images/';
        property string assetImagePath:'images/';
        property string asset0ImagePath:'/tmp/interactive/viewAssets/assets0/images/'
        property string phsImagePath:'/tmp/interactive/viewAssets/assets0/premiumHandset/images/';
    }
    signal loadCustomBg;
    signal nativeKeyPressed(variant event);
    signal nativeKeyReleased(variant event);
    signal systemPopupDisplayed(int activeSystemPopupID);
    signal systemPopupClosed(int id);
    signal screenPopupClosed(int id, variant retArray);
    Connections{
        target: pif
        onInfoModeChanged:{
            if (!width) return;
            if(infoMode){
                if(!pif.getBacklightState()) pif.setBacklightOn();
                __showSeatInfoScreen();
            } else __hideSeatInfoScreen();
        }
        onExternalAppActiveChanged:{
            core.info("ViewController.qml | onExternalAppActiveChanged| ViewData.ActiveSystemPopupID:"+ViewData.ActiveSystemPopupID)
            if(externalAppActive==false && ViewData.ActiveSystemPopupID){
                core.info("ViewController | onExternalAppActiveChanged | ActiveSystemPopupID: "+ViewData.ActiveSystemPopupID);
                externalPopup.children[0].sendMessage("SYSTEM#hide");
                externalPopup.children[0].visible=false;
                externalPopup.visible=false;
                var tmpId=ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["viewId"];
                if(ViewData.TemplateArr[tmpId]["objRef"]){
                    core.debug("ViewController | onExternalAppActiveChanged | Object ref Found")
                    ViewData.TemplateArr[tmpId]["objRef"].visible=true;
                    if(ViewData.TemplateArr[tmpId]["objRef"].init && (ViewData.TemplateArr[tmpId]["objRef"].isLoaded==undefined || ViewData.TemplateArr[tmpId]["objRef"].isLoaded)) ViewData.TemplateArr[tmpId]["objRef"].init();
                }else{
                    core.debug("ViewController | onExternalAppActiveChanged | loading object: "+ViewData.TemplateArr[tmpId]["viewName"])
                    if(!ViewData.TemplateArr[tmpId]["compRef"]){ViewData.TemplateArr[tmpId]["compRef"]=Qt.createComponent("../../view/systemPopups/"+ViewData.TemplateArr[tmpId]["viewName"]+".qml");}
                    ViewData.TemplateArr[tmpId]["objRef"] = ViewData.TemplateArr[tmpId]["compRef"].createObject(systemPopups, {visible: true});
                    if(!ViewData.TemplateArr[tmpId]["keepLoaded"] && ViewData.UnloadSystemPopArr.indexOf(tmpId)==-1) ViewData.UnloadSystemPopArr.push(tmpId)
                }
            }
        }
    }
    Connections{
        id: __navOutAnim; target: null
        onRunningChanged: {
            if(!ViewData.PrevScrnRef) return;
            if(ViewData.PrevScrnRef.navOutAnim.running) return;
            if(ViewData.PrevScrnRef.clearOnExit) ViewData.PrevScrnRef.clearOnExit();
            ViewData.PrevScrnRef.visible=false;
            ViewData.PrevScrnRef=null;
            if(!ViewData.AnimParallel) __loadScreenContinue()
        }
    }
    Connections{
        id: __navOutAnimReload; target: null
        onRunningChanged: {
            if(ViewData.PrevScrnRef.navOutAnim.running) return;
            if(ViewData.PrevScrnRef.reload) ViewData.PrevScrnRef.reload();
            if(ViewData.PrevScrnRef.navInAnim) ViewData.PrevScrnRef.navInAnim.running=true;
            ViewData.PrevScrnRef=null;
        }
    }
    function getStyleValue(element){return (ViewData.StyleConfigArr[element]) ? ViewData.StyleConfigArr[element]:"";}
    function getActiveScreen() {return privateVar.__activeScreen}
    function getActiveScreenRef() {return ViewData.ActiveScreenID?ViewData.TemplateArr[ViewData.ActiveScreenID]["objRef"]:""}
    function getActiveScreenPopupID() {return ViewData.ActiveScreenPopupID}
    function getActiveScreenPopupRef() {return (ViewData.ActiveScreenPopupID)?ViewData.TemplateArr[ViewData.ScreenPopupArr[ViewData.ActiveScreenPopupID]["viewId"]]["objRef"]:null;}
    function getActiveSystemPopupID() {return ViewData.ActiveSystemPopupID}
    function getActiveSystemPopupRef() {return (ViewData.ActiveSystemPopupID)?ViewData.TemplateArr[ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["viewId"]]["objRef"]:null;}
    function getPreloadStatus() {return ViewData.PreloadStatus}
    function setAnimParallel(val) { if(!val) val=false; ViewData.AnimParallel=val; }
    function setAnimOnReload(val) { if(!val) val=false; ViewData.AnimOnReload=val; }
    function getExternalPopupRef() { return externalPopup.children[0]; }
    function setNativeKeyPressed(callBackfn) { nativeKeyPressed.connect(callBackfn); }
    function setNativeKeyReleased(callBackfn) { nativeKeyReleased.connect(callBackfn); }
    function getCurrentScreenTemplateId(){return ViewData.CurTid; }
    function getViewToHideLoadingScreen(){return ViewData.ViewToHideLoadingScreen; }
    function getViewToSetTranslations(){return ViewData.ViewToSetTranslations; }
    function getDelayKarmaLoading(){return ViewData.DelayKarmaLoading;}
    function loadNext(refModel,index) {
        core.debug("ViewController | loadNext")
        if(refModel.getValue(index,"mid")){
            var nextNodeId = refModel.getValue(index,"mid");
            var nextTid = refModel.getValue(index,"media_attr_template_id");
        }else{
            var nextNodeId = refModel.getValue(index,"cid");
            var nextTid = refModel.getValue(index,"category_attr_template_id");
        }
        core.info("ViewController | loadNext | nextNodeId: "+nextNodeId+" nextTid: "+nextTid);
        if(!nextTid) {
            core.debug("ViewController | loadNext | No Template id found... End of category structure.")
            return false;
        }
        previous.formNode(current.node_id,current.template_id)
        current.formNode(nextNodeId,nextTid);
        __setParentInfo(current.id,previous.node_id,previous.template_id);
        loadScreen(current.template_id);
        __logScreenName();
        delete refModel;delete nextNodeId;delete nextTid;
        return true;
    }
    function loadPrev() {
        core.debug("ViewController | loadPrev")
        current.formNode(previous.node_id,previous.template_id);
        var a=__getParentInfo(current.id);
        core.info("ViewController | loadPrev | prev.nodeId: "+a["node_id"]+" prev.template_id: "+a["template_id"])
        if(a){
            previous.formNode(a["node_id"],a["template_id"]);
        }else{
            dataController.getParentMenu([current.node_id], __fetchParentInfoFromDb); // fetch parent information from db.
        }
        loadScreen (current.template_id);
        delete a;
    }
    function jumpTo(node_id,template_id) {
        core.info("ViewController | jumpTo | node_id: "+node_id+" template_id: "+template_id)
        current.formNode(node_id,template_id);
        var a = __getParentInfo(current.id);
        if(a){
            previous.formNode(a["node_id"],a["template_id"])
        }else{
            if(dataController.mediaServices.getPaxusDataByCid(current.node_id)){
                var tmpData=dataController.mediaServices.getPaxusDataByCid(current.node_id);
                __setParentInfo(current.id,tmpData["pcid"],tmpData['ptid'])
                previous.formNode(tmpData["pcid"],tmpData['ptid']);
                delete tmpData;
            }else {
            dataController.getParentMenu([current.node_id], __fetchParentInfoFromDb); // fetch parent information from db.        // Commented for performance reason. Need to find a different approach to make query.
            }
        }
        loadScreen (current.template_id);
        __logScreenName();
        delete a;
    }
    function loadScreen(template_id) {
        core.info("ViewController | loadScreen | template_id: "+template_id);
        unloadKickIn.restart(); loadScrnPopKickIn.stop(); screenUnload.stop(); screenPreLoad.stop(); loadScrnKickIn.stop();
        __navOutAnimReload.target=null; __navOutAnim.target=null;
        ViewData.CurTid=template_id;
        var tmpId=ViewData.ScreenArr[ViewData.CurTid]["viewId"];
        if(tmpId==ViewData.ActiveScreenID){
            core.info("ViewController | loadScreen | Reload "+tmpId);
            ViewData.PrevScrnRef=ViewData.TemplateArr[ViewData.ActiveScreenID]["objRef"];
            __loadBg(ViewData.ScreenArr[ViewData.CurTid]["bg"]);
            if(ViewData.PrevScrnRef.navOutAnim && ViewData.AnimOnReload){
                __navOutAnimReload.target=ViewData.PrevScrnRef.navOutAnim;
                ViewData.PrevScrnRef.navOutAnim.running=true;
            }else{
                if(ViewData.PrevScrnRef.reload) ViewData.PrevScrnRef.reload();
                if(ViewData.PrevScrnRef.navInAnim && ViewData.AnimOnReload) ViewData.PrevScrnRef.navInAnim.running=true;
                ViewData.PrevScrnRef=null;
                __startChildLoading(ViewData.CurTid)
            }
            return;
        }
        if(ViewData.ActiveScreenID){
            core.info("ViewController | loadScreen | Hide "+ViewData.ActiveScreenID);
            ViewData.PrevScrnRef=ViewData.TemplateArr[ViewData.ActiveScreenID]["objRef"];
            if(ViewData.PrevScrnRef.navOutAnim){
                __navOutAnim.target=ViewData.PrevScrnRef.navOutAnim;
                ViewData.PrevScrnRef.navOutAnim.running=true;
                if(!ViewData.AnimParallel) return;
            }else{
                if(ViewData.PrevScrnRef.clearOnExit) ViewData.PrevScrnRef.clearOnExit();
                ViewData.PrevScrnRef.visible=false;
                ViewData.PrevScrnRef=null;
            }
        }
        __loadScreenContinue();
        delete tmpId;
    }

    function clearUnloadScreens(){
        unloadKickIn.restart();screenUnload.restart();
    }

    function __loadScreenContinue(){
        var tmpId=ViewData.ScreenArr[ViewData.CurTid]["viewId"];
        ViewData.ActiveScreenID=tmpId;
        privateVar.__activeScreen=ViewData.TemplateArr[ViewData.ActiveScreenID]["viewName"]
        __loadBg(ViewData.ScreenArr[ViewData.CurTid]["bg"])
        if(ViewData.TemplateArr[tmpId]["objRef"]){
            core.info("ViewController | loadScreen | Object ref Found")
            ViewData.TemplateArr[tmpId]["objRef"].visible=true;
            if(ViewData.TemplateArr[tmpId]["objRef"].init && (ViewData.TemplateArr[tmpId]["objRef"].isLoaded==undefined || ViewData.TemplateArr[tmpId]["objRef"].isLoaded)) ViewData.TemplateArr[tmpId]["objRef"].init();
            if(ViewData.TemplateArr[tmpId]["objRef"].navInAnim) ViewData.TemplateArr[tmpId]["objRef"].navInAnim.running=true;
        }else{
            core.debug("ViewController | loadScreen | loading object: "+ViewData.TemplateArr[tmpId]["viewName"])
            if(!ViewData.TemplateArr[tmpId]["compRef"]){ViewData.TemplateArr[tmpId]["compRef"]=Qt.createComponent("../../view/screens/"+ViewData.TemplateArr[tmpId]["viewName"]+".qml");}
            ViewData.TemplateArr[tmpId]["objRef"] = ViewData.TemplateArr[tmpId]["compRef"].createObject(screen, {visible: true});
            if(ViewData.TemplateArr[tmpId]["objRef"].loaded) ViewData.TemplateArr[tmpId]["objRef"].loaded.connect(function(){if(ViewData.TemplateArr[tmpId]["objRef"].navInAnim) ViewData.TemplateArr[tmpId]["objRef"].navInAnim.running=true;});
            if(!ViewData.TemplateArr[tmpId]["keepLoaded"] && ViewData.UnloadScrnArr.indexOf(tmpId)==-1) ViewData.UnloadScrnArr.push(tmpId);
        }
        __startChildLoading(ViewData.CurTid);
        delete tmpId;
    }
    function __startChildLoading(template_id){
        if(__disableLookAheadLoading) return;
        if(ViewData.ScreenArr[template_id]["childViewId"]){
            core.debug("ViewController | startChildLoading | childViewId list found: "+ViewData.ScreenArr[template_id]["childViewId"])
            ViewData.ChildScreenArr = ViewData.ScreenArr[template_id]["childViewId"].split(",");
            screenPreLoad.count=0;
        }
        ViewData.ChildScrnPopArr=new Array();
        if(ViewData.ScreenArr[template_id]["childScrnPopViewId"]){
            core.debug("ViewController | startChildLoading | child int popup list found: "+ViewData.ScreenArr[template_id]["childScrnPopViewId"])
            ViewData.ChildScrnPopArr = ViewData.ScreenArr[template_id]["childScrnPopViewId"].split(",");
            loadScrnPopKickIn.count=0; loadScrnPopKickIn.start();
        }
        loadScrnKickIn.start();
    }
    function __logScreenName(){
        if(pif.getPaxusScreenLogStatus() && pif.paxus){
            var screenName;
            if(dataController.mediaServices.getPaxusDataByCid(current.node_id)){
                var cidArrayRef=dataController.mediaServices.getPaxusDataByCid(current.node_id);
                screenName=(cidArrayRef["paxus"])?cidArrayRef["paxus"]:cidArrayRef["title"];
            }
            if(screenName) pif.paxus.interactiveScreenLog(screenName);
            delete screenName;
        }
    }
    function showScreenPopup(id){
        core.info("ViewController | showScreenPopup | id: "+id)
        unloadKickIn.restart();
        if(ViewData.ActiveScreenPopupID) { core.info("ViewController | showScreenPopup | Already showing scrnPopup id: "+ViewData.ActiveScreenPopupID); return; }
        ViewData.ActiveScreenPopupID=id;
        var tmpId=ViewData.ScreenPopupArr[id]["viewId"]
        if(ViewData.ScreenPopupArr[id]["pauseVideo"]=='1' && ViewData.VideoStateOnAlert==0 && !pif.getPAState()){
            if(pif.vod.getMediaState()!=pif.vod.cMEDIA_STOP){ vodPauseTimer.restart();}
        }
        if(ViewData.ScreenPopupArr[id]["pauseAudio"]=='1' && ViewData.AudioStateOnAlert==0 && !pif.getPAState()){
            if(pif.aod.getMediaState()!=pif.aod.cMEDIA_STOP){ ViewData.AudioStateOnAlert=pif.aod.getMediaState(); pif.aod.pause(); }
        }
        if(ViewData.TemplateArr[tmpId]["objRef"]){
            core.info("ViewController | showScreenPopup | Object ref Found")
            ViewData.TemplateArr[tmpId]["objRef"].visible=true;
            if(ViewData.TemplateArr[tmpId]["objRef"].init && (ViewData.TemplateArr[tmpId]["objRef"].isLoaded==undefined || ViewData.TemplateArr[tmpId]["objRef"].isLoaded)) ViewData.TemplateArr[tmpId]["objRef"].init();
        }else{
            core.debug("ViewController | showScreenPopup | loading object: "+ViewData.TemplateArr[tmpId]["viewName"])
            if(!ViewData.TemplateArr[tmpId]["compRef"]){ViewData.TemplateArr[tmpId]["compRef"]=Qt.createComponent("../../view/screenPopups/"+ViewData.TemplateArr[tmpId]["viewName"]+".qml");}
            ViewData.TemplateArr[tmpId]["objRef"] = ViewData.TemplateArr[tmpId]["compRef"].createObject(screenPopups, {visible: true});
            if(!ViewData.TemplateArr[tmpId]["keepLoaded"] && ViewData.UnloadScrnPopArr.indexOf(tmpId)==-1) ViewData.UnloadScrnPopArr.push(tmpId)
        }
        if(parseInt(ViewData.ScreenPopupArr[id]["timeout"],10)>0){
            __screenPopupTimer.interval=parseInt(ViewData.ScreenPopupArr[id]["timeout"],10);
            __screenPopupTimer.start();
        }
    }
    function hideScreenPopup(retArray){
        core.info("ViewController | hideScreenPopup")
        unloadKickIn.restart();
        if(!ViewData.ActiveScreenPopupID) { core.info("ViewController | hideScreenPopup | No scrnPopup to hide"); return; }
        if(__screenPopupTimer.running)
            __screenPopupTimer.stop();
        if(ViewData.VideoStateOnAlert && !(ViewData.ActiveSystemPopupID && ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["pauseVideo"]=='1')){
            vodPauseTimer.stop();
            if(pif.getBacklightState()){
                if(ViewData.VideoStateOnAlert==pif.vod.cMEDIA_PLAY && (pif.getEntOn()|| pif.getFallbackMode())) pif.vod.resume();
                else if(ViewData.VideoStateOnAlert==pif.vod.cMEDIA_REWIND) pif.vod.rewind(true);
                else if(ViewData.VideoStateOnAlert==pif.vod.cMEDIA_FORWARD) pif.vod.forward(true);
            }
            ViewData.VideoStateOnAlert=0;
        }
        if(ViewData.AudioStateOnAlert && !(ViewData.ActiveSystemPopupID && ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["pauseAudio"]=='1')){
            pif.aod.resume(); ViewData.AudioStateOnAlert=0;
        }
        var tmpId=ViewData.ScreenPopupArr[ViewData.ActiveScreenPopupID]["viewId"];
        if(ViewData.TemplateArr[tmpId]["objRef"].clearOnExit) ViewData.TemplateArr[tmpId]["objRef"].clearOnExit();
        ViewData.TemplateArr[tmpId]["objRef"].visible=false;
        if(ViewData.ActiveSystemPopupID && ViewData.TemplateArr[ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["viewId"]]["objRef"]) systemPopups.forceActiveFocus();
        else screen.forceActiveFocus();
        var oaspId = ViewData.ActiveScreenPopupID;
        ViewData.ActiveScreenPopupID=0;
        if(ViewData.TemplateArr[ViewData.ActiveScreenID]["objRef"].screenPopupClosed) ViewData.TemplateArr[ViewData.ActiveScreenID]["objRef"].screenPopupClosed(oaspId,retArray);
        screenPopupClosed(oaspId,retArray);
        delete tmpId;delete oaspId;
    }
    function sendMsgToExtPopup(id,textMsg){
        if(!pif.getOpenFlight()) return;
        ViewData.ExtPopupMsgArray[id] = textMsg;
        if(id!=ViewData.ActiveSystemPopupID)  return;
        if(pif.getExternalAppActive() && externalPopup.visible) externalPopup.children[0].sendMessage('VIEW#'+textMsg)
    }
    function updateSystemPopupQ(id, status){
        core.info("ViewController | updateSystemPopupQ | id: "+id+" status: "+status);
        if(pif.getInteractiveOverride() && status) return;
        if(!pif.getOpenFlight() && !ViewData.ShowSystemPopupOnCloseFlight && status) return;
        var pIndex=ViewData.PopupQArr.indexOf(id);
        if(!status){
            if(pIndex!=-1){
                ViewData.PopupQArr.splice(pIndex,1);
                if(ViewData.ExtPopupMsgArray[id]) delete ViewData.ExtPopupMsgArray[id];
                __hideSystemPopup(id);
                if(ViewData.PopupQArr.length) __showSystemPopup(ViewData.PopupQArr[0])
                else ViewData.ActiveSystemPopupID=0;
            }else{
                core.debug("ViewController | updateSystemPopupQ | Popup not present in Q")
            }
        }else{
            if(pIndex!=-1){
                core.debug("ViewController | updateSystemPopupQ | Popup already exists in Q")
            }else{
                var pInserted=0;
                for(var i=0;i<ViewData.PopupQArr.length;i++){
                    if(ViewData.SystemPopupArr[id]["groupId"]!="NA" && ViewData.SystemPopupArr[id]["groupId"]==ViewData.SystemPopupArr[ViewData.PopupQArr[i]]["groupId"]){
                        ViewData.PopupQArr.splice(i,1);
                        if(ViewData.ExtPopupMsgArray[id]) delete ViewData.ExtPopupMsgArray[id];
                    }
                }
                for(var i=0;i<ViewData.PopupQArr.length;i++){
                    if(parseInt(ViewData.SystemPopupArr[id]["priority"],10) < parseInt(ViewData.SystemPopupArr[ViewData.PopupQArr[i]]["priority"],10)){
                        ViewData.PopupQArr.splice(i,0,id);
                        pInserted=1;
                        break;
                    }
                }
                if(!pInserted) ViewData.PopupQArr.push(id);
                if(ViewData.ActiveSystemPopupID!=0 && ViewData.ActiveSystemPopupID!=ViewData.PopupQArr[0]){
                    __hideSystemPopup(ViewData.ActiveSystemPopupID);
                }
                __showSystemPopup(ViewData.PopupQArr[0])
                delete i;
			}
        }
	delete pInserted;
    }

    function setExternalPopUpPriority(priority){
        core.info("Setting External Pop-up Priority "+priority);
        externalPopup.children[0].__setPopUpPriority(priority);
    }

    function __showSystemPopup(id){
        core.info("ViewController | showSystemPopup | id: "+id)
        unloadKickIn.restart();
        if(ViewData.ActiveSystemPopupID==id) { core.info("ViewController | showSystemPopup | Already showing scrnPopup id: "+ViewData.ActiveSystemPopupID); return; }
        ViewData.ActiveSystemPopupID=id;
        var tmpId=ViewData.SystemPopupArr[id]["viewId"];
        if(ViewData.SystemPopupArr[id]["screenOn"]=='1'){
            pif.screenSaver.restartScreenSaver();
        }
        if(ViewData.SystemPopupArr[id]["pauseVideo"]=='1' && ViewData.VideoStateOnAlert==0 && !pif.getPAState()){
            if(pif.vod.getMediaState()!=pif.vod.cMEDIA_STOP){
                vodPauseTimer.restart();
            }
        }
        if(ViewData.SystemPopupArr[id]["pauseAudio"]=='1' && ViewData.AudioStateOnAlert==0 && !pif.getPAState()){
            if(pif.aod.getMediaState()!=pif.aod.cMEDIA_STOP){
                ViewData.AudioStateOnAlert=pif.aod.getMediaState();
                pif.aod.pause();
            }
        }
        if(ViewData.SystemPopupArr[id]["extPriority"]!=undefined && ViewData.SystemPopupArr[id]["extPriority"]=='1'){
            core.info("ViewController | showSystemPopup | ViewData.SystemPopupArr[id] | Priority : "+ViewData.SystemPopupArr[id]["extPriority"])
            core.info("ViewController | showSystemPopup | Popup is criical | pif.getPAState"+pif.getPAState())
            if(pif.getPAState()>1){
                setExternalPopUpPriority(externalPopup.children[0].cPRIORITY_CRITICAL)
                if(!pif.getExternalAppActive()){
                    core.info("Settting External Popup Priority | Priority: "+externalPopup.children[0].cPRIORITY_CRITICAL)
                    pif.setExternalAppActive(true);
                    ViewData.externalAppActiveStatus=true;
                }
            }
        }
        if(pif.getExternalAppActive() && ViewData.SystemPopupArr[id]["extPopup"]=='1'){
            if(!pif.getPAState()) pif.launchApp.pauseApp();
            extPopupTimer.restart();
        }else{
            if(ViewData.TemplateArr[tmpId]["objRef"]){
                core.info("ViewController | showSystemPopup | Object ref Found")
                ViewData.TemplateArr[tmpId]["objRef"].visible=true;
                if(ViewData.TemplateArr[tmpId]["objRef"].init && (ViewData.TemplateArr[tmpId]["objRef"].isLoaded==undefined || ViewData.TemplateArr[tmpId]["objRef"].isLoaded)) ViewData.TemplateArr[tmpId]["objRef"].init();
            }else{
                core.debug("ViewController | showSystemPopup | loading object: "+ViewData.TemplateArr[tmpId]["viewName"])
                if(!ViewData.TemplateArr[tmpId]["compRef"]){ViewData.TemplateArr[tmpId]["compRef"]=Qt.createComponent("../../view/systemPopups/"+ViewData.TemplateArr[tmpId]["viewName"]+".qml");}
                ViewData.TemplateArr[tmpId]["objRef"] = ViewData.TemplateArr[tmpId]["compRef"].createObject(systemPopups, {visible: true});
                if(!ViewData.TemplateArr[tmpId]["keepLoaded"] && ViewData.UnloadSystemPopArr.indexOf(tmpId)==-1) ViewData.UnloadSystemPopArr.push(tmpId)
            }
        }
        if(parseInt(ViewData.SystemPopupArr[id]["timeout"],10)>0){
            __systemPopupTimer.interval=parseInt(ViewData.SystemPopupArr[id]["timeout"],10);
            __systemPopupTimer.start();
        }
        systemPopupDisplayed(ViewData.ActiveSystemPopupID);
        delete tmpId;
    }
    function __hideSystemPopup(id){
        core.info("ViewController | hideSystemPopup | id: "+id)
        unloadKickIn.restart();
        var tmpId=ViewData.SystemPopupArr[id]["viewId"];
        if(id==ViewData.ActiveSystemPopupID){
            __systemPopupTimer.stop();
            var nextPopupId=(ViewData.PopupQArr.length)?ViewData.PopupQArr[0]:0;
            if(ViewData.VideoStateOnAlert){
                if((nextPopupId && ViewData.SystemPopupArr[nextPopupId]["pauseVideo"]=='0') || !nextPopupId){
                    if(!(ViewData.ActiveScreenPopupID && ViewData.ScreenPopupArr[ViewData.ActiveScreenPopupID]["pauseVideo"]=='1')){
                        vodPauseTimer.stop();
                        if(pif.getBacklightState() && !pif.getPAState()){
                            if(ViewData.VideoStateOnAlert==pif.vod.cMEDIA_PLAY && (pif.getEntOn()||pif.getFallbackMode())) pif.vod.resume();
                            else if(ViewData.VideoStateOnAlert==pif.vod.cMEDIA_REWIND) pif.vod.rewind(true);
                            else if(ViewData.VideoStateOnAlert==pif.vod.cMEDIA_FORWARD) pif.vod.forward(true);
                        }
                        if(!pif.getPAState()) ViewData.VideoStateOnAlert=0;
                    }
                }
            }
            if(ViewData.AudioStateOnAlert){
                if((nextPopupId && ViewData.SystemPopupArr[nextPopupId]["pauseAudio"]=='0') || !nextPopupId){
                    if(!(ViewData.ActiveScreenPopupID && ViewData.ScreenPopupArr[ViewData.ActiveScreenPopupID]["pauseAudio"]=='1')){
                        pif.aod.resume(); ViewData.AudioStateOnAlert=0;
                    }
                }
            }
            delete nextPopupId;
        }
        if(externalPopup.visible){
            core.info("ViewController | __hideSystemPopup | ViewData.externalAppActiveCheck :"+ViewData.externalAppActiveStatus+", pif.getExternalAppActive:"+pif.getExternalAppActive())
            setExternalPopUpPriority(externalPopup.children[0].cPRIORITY_NORMAL);
            if(ViewData.externalAppActiveStatus){
                core.info("ViewController.qml | Setting External App Active false")
                pif.setExternalAppActive(false);
                ViewData.externalAppActiveStatus=false;
            } else {
                externalPopup.children[0].sendMessage("SYSTEM#hide");
                externalPopup.children[0].visible=false;
                externalPopup.visible=false;
            }
            if(!pif.getPAState()) pif.launchApp.resumeApp();
        }else{
            if(!ViewData.PopupQArr.length){
                if(ViewData.ActiveScreenPopupID) screenPopups.forceActiveFocus();
                else screen.forceActiveFocus();
            }
        }

        if(ViewData.TemplateArr[tmpId]["objRef"] && ViewData.TemplateArr[tmpId]["objRef"].clearOnExit) ViewData.TemplateArr[tmpId]["objRef"].clearOnExit();
        if(ViewData.TemplateArr[tmpId]["objRef"]) ViewData.TemplateArr[tmpId]["objRef"].visible=false;

        systemPopupClosed(id);
        delete tmpId;
    }

    function __setConfiguration (viewSection) { ViewData.setConfiguration(viewSection); return true;}
    function __setViewPaths () {
        settings.contentImagePath = coreHelper.cleanPath(Qt.resolvedUrl(core.settings.contentPath+"/common/images"+core.settings.resolutionIndex+"/"));
        settings.assetImagePath = coreHelper.cleanPath(Qt.resolvedUrl(core.settings.assetPath+"/images/"));
        settings.asset0ImagePath = coreHelper.cleanPath(Qt.resolvedUrl(core.settings.viewAssetPath+"/assets0/images/"));
        settings.phsImagePath = coreHelper.cleanPath(Qt.resolvedUrl(core.settings.viewAssetPath+"/assets0/premiumHandset/images/"));
        info("ViewController | settings.contentImagePath: "+settings.contentImagePath+" settings.assetImagePath: "+settings.assetImagePath);
        if (viewStyle) viewStyle.source=core.settings.assetPath+"/style.qm";
        if(ViewData.ScreenArr[ViewData.CurTid]) bgTimer.restart();
    }
    function __preloadScreenComponents() {
        if(coreHelper.fileExists("../viewAssets/assets"+core.settings.resolutionIndex+"/style.qm")) viewStyle=Qt.createQmlObject('import QtQuick 1.1;import Panasonic.Pif 1.0; TranslationFileLoader {}',viewController);
        __setViewPaths();
        Qt.createQmlObject('import QtQuick 1.1;import "../../view";ImgPreload{ id: imgPreload; visible: false}',viewController);
        if(coreHelper.fileExists("../viewAssets/assets0/style.qm")) Qt.createQmlObject('import QtQuick 1.1;import Panasonic.Pif 1.0;Item{TranslationFileLoader{source: core.settings.viewAssetPath+"/assets0/style.qm" }}',viewController);
        if(pif.getHandsetType()==pif.cPREMIUM_HANDSET && coreHelper.fileExists("../viewAssets/assets0/premiumHandset/style.qm")){
            Qt.createQmlObject('import QtQuick 1.1;import Panasonic.Pif 1.0;Item{TranslationFileLoader { id: premiumHandsetStyle; source: core.settings.viewAssetPath+"/assets0/premiumHandset/style.qm" }}',viewController);
        }
        for(var i in ViewData.SystemPopupArr){
            if(ViewData.SystemPopupArr[i]["extPopup"]=='1'){
                Qt.createQmlObject('import QtQuick 1.1;ExternalPopup{}',externalPopup);
                break;
            }
        }
        viewHelper = Qt.createQmlObject('import QtQuick 1.1;import "../../view";ViewHelper{}',viewController);
        viewData = Qt.createQmlObject('import QtQuick 1.1;import "../../view";ViewData{}',viewController);
        pifEvents = Qt.createQmlObject('import QtQuick 1.1;import "../../view";PifEvents{}',viewController);
        widgetList = Qt.createQmlObject('import QtQuick 1.1;import "../../view";WidgetList{}',viewController);
        var tmpName="";
        for(var i in ViewData.TemplateArr){
            tmpName=ViewData.TemplateArr[i]["viewName"]
            core.info("ViewController | preloadScreens | tmpName: "+tmpName);
            ViewData.TemplateArr[i]["keepLoaded"]=parseInt(ViewData.TemplateArr[i]["keepLoaded"],10);
//            if(ViewData.TemplateArr[i]["type"]=="screen")
//                ViewData.TemplateArr[i]["compRef"]=Qt.createComponent("../../view/screens/"+tmpName+".qml");
//            else if(ViewData.TemplateArr[i]["type"]=="screenPopup")
//                ViewData.TemplateArr[i]["compRef"]=Qt.createComponent("../../view/screenPopups/"+tmpName+".qml");
//            else
//                ViewData.TemplateArr[i]["compRef"]=Qt.createComponent("../../view/systemPopups/"+tmpName+".qml");

        }
        delete tmpName;delete i;
    }
    function __preloadScreenObjects(){
        core.info("ViewController | preloadScreenObjects");
        ViewData.PreloadStatus=false;
        var cList=widgetList.getWidgetList()
        for(var i in cList){
            core.info("ViewController | preloadScreenObjects | widgetList i: "+i);
            if(!cList[i]["z-index"]) cList[i]["z-index"]=0;
            if(!eval("widgetList."+cList[i]["id"])){
                core.log("Loading Widget "+cList[i]["id"]);
                if(cList[i]["z-index"]<20)
                    var objRef = cList[i]["compId"].createObject(screen, {z: cList[i]["z-index"]});
                else if(cList[i]["z-index"]<30)
                    var objRef = cList[i]["compId"].createObject(screenPopups, {z: cList[i]["z-index"]});
                else
                    var objRef = cList[i]["compId"].createObject(systemPopups, {z: cList[i]["z-index"]});
                eval("widgetList."+cList[i]["id"]+"=objRef");
                delete objRef;
            }
        }
        for(var i in ViewData.TemplateArr){
            if(ViewData.TemplateArr[i]["objRef"]) continue;
            ViewData.TemplateArr[i]["objRef"]=0;
            core.info("ViewController | preloadScreenObjects | keepLoaded: "+ViewData.TemplateArr[i]["viewName"]+" : "+ViewData.TemplateArr[i]["keepLoaded"]);
            if(ViewData.TemplateArr[i]["keepLoaded"]){
                if(ViewData.TemplateArr[i]["type"]=="screen"){
                    if(!ViewData.TemplateArr[i]["compRef"]){ViewData.TemplateArr[i]["compRef"]=Qt.createComponent("../../view/screens/"+ViewData.TemplateArr[i]["viewName"]+".qml");}
                    ViewData.TemplateArr[i]["objRef"]=ViewData.TemplateArr[i]["compRef"].createObject(screen, {visible: false});
                }else if(ViewData.TemplateArr[i]["type"]=="screenPopup"){
                    if(!ViewData.TemplateArr[i]["compRef"]){ViewData.TemplateArr[i]["compRef"]=Qt.createComponent("../../view/screenPopups/"+ViewData.TemplateArr[i]["viewName"]+".qml");}
                    ViewData.TemplateArr[i]["objRef"]=ViewData.TemplateArr[i]["compRef"].createObject(screenPopups, {visible: false});
                }else{
                    if(!ViewData.TemplateArr[i]["compRef"]){ViewData.TemplateArr[i]["compRef"]=Qt.createComponent("../../view/systemPopups/"+ViewData.TemplateArr[i]["viewName"]+".qml");}
                    ViewData.TemplateArr[i]["objRef"]=ViewData.TemplateArr[i]["compRef"].createObject(systemPopups, {visible: false});
                }
            }
        }
        ViewData.PreloadStatus=true;
        delete cList;delete i;
        return true;
    }
    function __clearScreenObjects(){
        core.info("ViewController | clearScreenObjects");
        for(var i in ViewData.TemplateArr){
            if((ViewData.ActiveSystemPopupID && i!=ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["viewId"]) || !ViewData.ActiveSystemPopupID){
                if(ViewData.TemplateArr[i]["objRef"]) ViewData.TemplateArr[i]["objRef"].destroy();
                ViewData.TemplateArr[i]["objRef"]=0;
            }
        }
        var cList=widgetList.getWidgetList()
        for(var i in cList){
            if(eval("widgetList."+cList[i]["id"])){
                eval("widgetList."+cList[i]["id"]+".destroy()");
                eval("widgetList."+cList[i]["id"]+"=null");
            }
        }
        ViewData.ActiveScreenID=undefined;
        delete cList;delete i;
    }
    function __setParentInfo(curId,pid,ptid){
        if(ViewData.ParentInfoArr[curId]) return;
        ViewData.ParentInfoArr[curId]=[];
        ViewData.ParentInfoArr[curId]["node_id"]=pid;
        ViewData.ParentInfoArr[curId]["template_id"]=ptid;
    }
    function __getParentInfo(curId){
        if(ViewData.ParentInfoArr[curId]) return ViewData.ParentInfoArr[curId];
        else return false;
    }
    function __fetchParentInfoFromDb(dModel){
        if(dModel.getValue(0,'cid')!=current.node_id) return;
        core.info("ViewController | fetchParentInfoFromDb | parentcid: "+dModel.getValue(0,'parentcid')+" | parent_template_id: "+dModel.getValue(0,'parent_template_id'))
        __setParentInfo(current.id,dModel.getValue(0,'parentcid'),dModel.getValue(0,'parent_template_id'))
        previous.formNode(dModel.getValue(0,'parentcid'),dModel.getValue(0,'parent_template_id'))
    }
    function __loadBg(fileName){
        core.info("ViewController | loadBg | fileName: "+fileName)
        if(fileName==undefined || fileName==""){
            core.debug("ViewController | loadBg | Calling loadCustomBg");
            loadCustomBg();
        }else bg.source = settings.contentImagePath+fileName;
    }
    function __unloadScreens(){
        core.info("ViewController | unloadScreens");
        for(var i=0; i<ViewData.UnloadScrnPopArr.length; i++){
            var tmpId=ViewData.UnloadScrnPopArr[i];
            core.info("ViewController | unloadScreens | unload int popup: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
            if(ViewData.ActiveScreenPopupID && tmpId==ViewData.ScreenPopupArr[ViewData.ActiveScreenPopupID]["viewId"]) continue;
            if(ViewData.TemplateArr[tmpId]["objRef"] && ViewData.ChildScrnPopArr.indexOf(tmpId)==-1 && !ViewData.TemplateArr[tmpId]["keepLoaded"]){
                core.debug("ViewController | unloadScreens | destroy int popup: "+tmpId);
                ViewData.TemplateArr[tmpId]["objRef"].destroy();
                ViewData.TemplateArr[tmpId]["objRef"]=0;
            }
        }
        ViewData.UnloadScrnPopArr=[];
        for(var i=0; i<ViewData.UnloadSystemPopArr.length; i++){
            var tmpId=ViewData.UnloadSystemPopArr[i];
            core.info("ViewController | unloadScreens | unload int popup: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
            if(ViewData.ActiveSystemPopupID && tmpId==ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["viewId"]) continue;
            if(ViewData.TemplateArr[tmpId]["objRef"] && !ViewData.TemplateArr[tmpId]["keepLoaded"]){
                core.debug("ViewController | unloadScreens | destroy system popup: "+tmpId);
                ViewData.TemplateArr[tmpId]["objRef"].destroy();
                ViewData.TemplateArr[tmpId]["objRef"]=0;
            }
        }
        ViewData.UnloadSystemPopArr=[];
        for(var i=0; i<ViewData.UnloadScrnArr.length; i++){
            var tmpId=ViewData.UnloadScrnArr[0];
            core.info("ViewController | unloadScreens | unload tmpId: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
            if(tmpId==ViewData.ActiveScreenID) continue;
            if(ViewData.TemplateArr[tmpId]["objRef"] && ViewData.ChildScreenArr.indexOf(tmpId)==-1 && !ViewData.TemplateArr[tmpId]["keepLoaded"]){
                core.debug("ViewController | unloadScreens | destroy screen: "+tmpId);
                ViewData.TemplateArr[tmpId]["objRef"].destroy();
                ViewData.TemplateArr[tmpId]["objRef"]=0;
            }
        }
        ViewData.UnloadScrnArr=[];
        delete tmpId;delete i;
    }
    function __seatInfoScreen () {if(pif.android) return; if (pif.getInfoMode()) __showSeatInfoScreen();}
    function __showSeatInfoScreen(){
        core.info("ViewController | showSeatInfoScreen");
        pif.screenSaver.restartScreenSaver();
        if(seatInfo) __hideSeatInfoScreen();
        seatInfo=core.coreHelper.loadDynamicComponent("components/SeatInfo.qml",viewController); 
        seatInfo.z=115;
        core.debug("ViewController | seatInfo: "+seatInfo);
        return true;
    }
    function __hideSeatInfoScreen(){
        core.info("ViewController | hideSeatInfoScreen");
        if(seatInfo) seatInfo.destroy();
        return true;
    }
    function __showForceAVScreen(){
        core.info("ViewController | showForceAVScreen");
        forceAVScreen=core.coreHelper.loadDynamicComponent("components/ForceAudioVideo.qml",viewController);
        //forceAVScreen = Qt.createQmlObject('import QtQuick 1.1;Rectangle{color:"black";width:viewController.width;height:viewController.height;}',viewController);
        forceAVScreen.z=110;
        core.debug("ViewController | forceAVScreen: "+forceAVScreen);
        return true;
    }
    function __hideForceAVScreen(){
        core.info("ViewController | HideForceAVScreen");
        if(!forceAVScreen) return false;
        forceAVScreen.destroy();
        return true;
    }
    function __showLoadingScreen(){
        core.info("ViewController | showLoadingScreen")
        if(loadingScrn.visible) return false;
        if(coreHelper.fileExists("../view/screens/LoadingScreen.qml")){
            var lscr=Qt.createComponent("../../view/screens/LoadingScreen.qml");
            lscr.createObject(loadingScrn);
        }else{
            Qt.createQmlObject('import QtQuick 1.1;Rectangle{color:"black";width:viewController.width;height:viewController.height;Text{text:"Loading...";anchors.centerIn:parent;font.pointSize: 24;color: "white";} MouseArea{anchors.fill: parent;onPressed:{} onReleased:{}}}',loadingScrn);
        }
        loadingScrn.visible=true;
        return true;
    }
    function __hideLoadingScreen(){
        core.info("ViewController | hideLoadingScreen");
        if(!loadingScrn.visible) return false;
        loadingScrn.visible=false;
        loadingScrn.children[0].destroy();
        return true;
    }
    function __loadTestPanel () {
        core.info("ViewController | loadTestPanel");
        testPanel=core.coreHelper.loadDynamicComponent("testpanel/SimulationPanel.qml",viewController);
        testPanel.z=120;
        core.debug("ViewController | testPanel: "+testPanel);
        if(core.testMode==2 && core.pc && core.simulation){
            core.info("ViewController.qml | __loadTestPanel | Loading SharedModelClientSim");
            pif.loadSharedModelSimClientInterface();
        }
    }
    function __resetApp(){
        hideScreenPopup();
        if(ViewData.ActiveSystemPopupID){
            if(!ViewData.ShowSystemPopupOnCloseFlight){
                __hideSystemPopup(ViewData.ActiveSystemPopupID);
                ViewData.ActiveSystemPopupID=0;
                ViewData.PopupQArr=[];
            }
        }else{
            ViewData.PopupQArr=[];
        }
        ViewData.VideoStateOnAlert=0;
        ViewData.AudioStateOnAlert=0;
        ViewData.ExtPopupMsgArray=[];
    }
    Component.onCompleted: {
        interactiveStartup.registerToStage(interactiveStartup.cStageLoadNonRouteData,[__preloadScreenComponents]);
        interactiveStartup.registerToStage(interactiveStartup.cStagePreloadData,[__preloadScreenObjects]);
        interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[__clearScreenObjects]);
        if (core.testMode) interactiveStartup.registerToStage(interactiveStartup.cStageLoadDynamicComponents,[__loadTestPanel]);
        interactiveStartup.registerToStage(interactiveStartup.cStageLoadDynamicComponents,[__showLoadingScreen]);
        interactiveStartup.registerToStage(interactiveStartup.cStageForceVideoAudioScreen,[__showForceAVScreen]);
        interactiveStartup.eventHandler(interactiveStartup.cViewControllerLoaded);
        interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[__resetApp]);
    }
    Image { id: bg }		// backgroundImgholder
    FocusScope { id: screen; z: 10; width: viewController.width; height: viewController.height }
    FocusScope { id: screenPopups; z: 20; width: viewController.width; height: viewController.height }
    FocusScope { id: systemPopups; z: 30; width: viewController.width; height: viewController.height }
    Item { id: externalPopup; z: 40; visible: false }
    Item { id: loadingScrn; z: 100; visible: false }
    // Menu Traversal (View Accessible)
    Node {id:current}
    Node {id:previous}
    Timer{ id: bgTimer; interval: 2; onTriggered: __loadBg(ViewData.ScreenArr[ViewData.CurTid]["bg"]);}
    Timer{ id: vodPauseTimer; interval: 20; onTriggered: {if(pif.vod.getMediaState()!=pif.vod.cMEDIA_PAUSE){ViewData.VideoStateOnAlert=pif.vod.getMediaState(); pif.vod.pause();}}}
    Timer{
        id: extPopupTimer; interval: 2;
        onTriggered: {
            var tmpId=ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["viewId"];
            externalPopup.visible=true;
            externalPopup.children[0].visible=true;
            externalPopup.children[0].sendMessage('SYSTEM#init#'+ViewData.TemplateArr[tmpId]["viewName"]+'#'+ViewData.ActiveSystemPopupID+'#'+core.settings.languageID+'#'+core.settings.languageISO);
            if(ViewData.ExtPopupMsgArray[ViewData.ActiveSystemPopupID]) externalPopup.children[0].sendMessage('VIEW#'+ViewData.ExtPopupMsgArray[ViewData.ActiveSystemPopupID])
        }
    }
    Timer{ id: loadScrnKickIn; interval: 3000; onTriggered: {screenPreLoad.start(); }}
    Timer{ id: unloadKickIn; interval: __unloadKickInTimout; onTriggered: {screenUnload.start(); }}
    Timer{ id: loadScrnPopKickIn; interval: 250; repeat: true; property int count: 0
        onTriggered: {
            if(count<ViewData.ChildScrnPopArr.length){
                var tmpId=ViewData.ChildScrnPopArr[count];
                core.info("ViewController | loadScrnPopKickIn | load scrnPopup: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
                if(!ViewData.TemplateArr[tmpId]["objRef"]){
                    if(!ViewData.TemplateArr[tmpId]["compRef"]){ViewData.TemplateArr[tmpId]["compRef"]=Qt.createComponent("../../view/screenPopups/"+ViewData.TemplateArr[tmpId]["viewName"]+".qml");}
                    ViewData.TemplateArr[tmpId]["objRef"] = ViewData.TemplateArr[tmpId]["compRef"].createObject(screenPopups, {visible: false});
                    if(!ViewData.TemplateArr[tmpId]["keepLoaded"] && ViewData.UnloadScrnPopArr.indexOf(tmpId)==-1) ViewData.UnloadScrnPopArr.push(tmpId);
                    core.debug("ViewController | loadScrnPopKickIn | objRef: "+ViewData.TemplateArr[tmpId]["objRef"]);
                }
                count++;
            }else loadScrnPopKickIn.stop();
        }
    }
    Timer{ id: screenPreLoad; interval: 250; repeat: true; property int count: 0
        onTriggered: {
            core.info("ViewController | screenPreLoad | count: "+count+" | ViewData.ChildScreenArr.length: "+ViewData.ChildScreenArr.length);
            if(count<ViewData.ChildScreenArr.length){
                var tmpId=ViewData.ChildScreenArr[count];
                core.info("ViewController | screenPreLoad | load tmpId: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
                if(!ViewData.TemplateArr[tmpId]["objRef"] && tmpId!=ViewData.ActiveScreenID){
                    if(!ViewData.TemplateArr[tmpId]["compRef"]){ViewData.TemplateArr[tmpId]["compRef"]=Qt.createComponent("../../view/screens/"+ViewData.TemplateArr[tmpId]["viewName"]+".qml");}
                    ViewData.TemplateArr[tmpId]["objRef"] = ViewData.TemplateArr[tmpId]["compRef"].createObject(screen, {visible: false});
                    if(!ViewData.TemplateArr[tmpId]["keepLoaded"] && ViewData.UnloadScrnArr.indexOf(tmpId)==-1) ViewData.UnloadScrnArr.push(tmpId);
                    core.debug("ViewController | screenPreLoad | objRef: "+ViewData.TemplateArr[tmpId]["objRef"]);
                }
                count++;
            }else screenPreLoad.stop();
        }
    }
    Timer{ id: screenUnload; interval: 100; repeat: true;
        onTriggered: {
            var continueUnload=false;
            if(ViewData.UnloadScrnPopArr.length){
                var tmpId=ViewData.UnloadScrnPopArr[0];
                core.info("ViewController | screenUnload | trying to unload int popup: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
                if(ViewData.ActiveScreenPopupID && tmpId==ViewData.ScreenPopupArr[ViewData.ActiveScreenPopupID]["viewId"]){
                    if(ViewData.UnloadScrnPopArr.length>1) {ViewData.UnloadScrnPopArr.push(ViewData.UnloadScrnPopArr.shift()); return;}
                }else if(ViewData.TemplateArr[tmpId]["objRef"] && ViewData.ChildScrnPopArr.indexOf(tmpId)==-1 && !ViewData.TemplateArr[tmpId]["keepLoaded"]){
                    core.debug("ViewController | screenUnload | destroy int popup: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
                    ViewData.TemplateArr[tmpId]["objRef"].destroy();
                    delete ViewData.TemplateArr[tmpId]["objRef"];
                    ViewData.TemplateArr[tmpId]["compRef"].destroy();
                    delete ViewData.TemplateArr[tmpId]["compRef"];
                    ViewData.UnloadScrnPopArr.shift();
                    continueUnload=true;
                }                
            }

            if(ViewData.UnloadSystemPopArr.length){
                var tmpId=ViewData.UnloadSystemPopArr[0];
                core.info("ViewController | screenUnload | trying to unload system popup: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
                if(ViewData.ActiveSystemPopupID && tmpId==ViewData.SystemPopupArr[ViewData.ActiveSystemPopupID]["viewId"]){
                    if(ViewData.UnloadSystemPopArr.length>1) {ViewData.UnloadSystemPopArr.push(ViewData.UnloadSystemPopArr.shift()); return;}
                }else if(ViewData.TemplateArr[tmpId]["objRef"] && !ViewData.TemplateArr[tmpId]["keepLoaded"]){
                    core.debug("ViewController | screenUnload | destroy system popup: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
                    ViewData.TemplateArr[tmpId]["objRef"].destroy();
                    delete ViewData.TemplateArr[tmpId]["objRef"];
                    ViewData.TemplateArr[tmpId]["compRef"].destroy();
                    delete ViewData.TemplateArr[tmpId]["compRef"];
                    ViewData.UnloadSystemPopArr.shift();
                    continueUnload=true;
                }                
            }

            if(ViewData.UnloadScrnArr.length){
                var tmpId=ViewData.UnloadScrnArr[0];
                core.info("ViewController | screenUnload | trying to unload tmpId: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
                if(tmpId==ViewData.ActiveScreenID){
                    if(ViewData.UnloadScrnArr.length>1){ViewData.UnloadScrnArr.push(ViewData.UnloadScrnArr.shift()); return; }
                }else if(ViewData.TemplateArr[tmpId]["objRef"] && ViewData.ChildScreenArr.indexOf(tmpId)==-1 && !ViewData.TemplateArr[tmpId]["keepLoaded"]){
                    core.debug("ViewController | screenUnload | destroy screen: "+tmpId+" viewName: "+ViewData.TemplateArr[tmpId]["viewName"]);
                    ViewData.TemplateArr[tmpId]["objRef"].destroy();
                    delete ViewData.TemplateArr[tmpId]["objRef"];
                    ViewData.TemplateArr[tmpId]["compRef"].destroy();
                    delete ViewData.TemplateArr[tmpId]["compRef"];
                    ViewData.UnloadScrnArr.shift();
                    continueUnload=true;
                }
            }

            if(!continueUnload)
                screenUnload.stop();
        }
    }
    Timer { id: __systemPopupTimer; onTriggered: updateSystemPopupQ(ViewData.ActiveSystemPopupID, false) }
    Timer { id: __screenPopupTimer; onTriggered: hideScreenPopup() }

    NativeScanCodeKeyListener{
        id: __nativeKeyListner
        onKeyPressed: {core.info("onKeyPressed | nativeScanCode: "+event.nativeScanCode); nativeKeyPressed(event);}
        onKeyReleased: {core.info("onKeyReleased | nativeScanCode: "+event.nativeScanCode); nativeKeyReleased(event);}
    }
    Keys.onLeftPressed: event.accepted=true;
    Keys.onDownPressed: event.accepted=true;
    Keys.onUpPressed:  event.accepted=true;
    Keys.onRightPressed: event.accepted=true;
}
