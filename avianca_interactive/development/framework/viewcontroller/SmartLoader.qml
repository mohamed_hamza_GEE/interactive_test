import QtQuick 1.1

FocusScope {
    id: smartLoader;
    width: viewController.width
    height: viewController.height
    property bool isLoaded: false;
    signal loaded()

    onLoaded: {isLoaded=true;}

    function load(){
        core.log("smartLoader | Inside default load")
    }
    function push(componentID,refId){
        loaderList.append({"componentID": componentID, "refId":refId})
    }
    ListModel{ id: loaderList}
    Timer{
        id: autoLoader
        interval: 100
        property int count: 0
        onTriggered: {
            //core.log("smartloader | count: "+count+" loaderList.count: "+loaderList.count);
            if(count<loaderList.count){
                var objRef = loaderList.get(count).componentID.createObject(smartLoader);
                if(loaderList.get(count).refId) eval(loaderList.get(count).refId+"=objRef");
                count++;
                //core.log("smartloader | objRef: "+objRef);
            }else{
                autoLoader.stop();
                autoLoader.repeat=false
                loaded();
                if(smartLoader.visible && init) init();
            }
        }
    }
    function loadAllComponents(){
        for(var i=0; i<loaderList.count; i++){
            var objRef = loaderList.get(i).componentID.createObject(smartLoader);
            if(loaderList.get(i).refId) eval(loaderList.get(i).refId+"=objRef");
        }
        loaded();
    }
    Component.onCompleted: {
        load();
        if(viewController.getPreloadStatus() || smartLoader.visible){
            if(loaderList.count){
                autoLoader.repeat=true;
                autoLoader.start();
            }else{
                loaded();
                if(smartLoader.visible && init) init();
            }
        }else{
            loadAllComponents();
        }
    }
    Component.onDestruction: {}
}
