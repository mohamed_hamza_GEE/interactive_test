import QtQuick 1.1
import Panasonic.Pif 1.0
import Panasonic.PopupManager 1.0

Popup {
    id: thisPopUp
    property int popupWindowX: 0;
    property int popupWindowY: 0;
    property int popupWindowWidth: viewController.width;
    property int popupWindowHeight: viewController.height;
    property int cPRIORITY_CRITICAL:2;
    property int cPRIORITY_HIGH:1;
    property int cPRIORITY_NORMAL:0;

    property int cSYSTEM_BAR_VISIBLE:3;
    property int cSYSTEM_BAR_DIM:4;
    property int cSYSTEM_BAR_HIDE:5;

    signal externalPopupMsg(int externalPopupId, string popupMsg);
    x: popupWindowX
    y: popupWindowY
    width: popupWindowWidth;
    height: popupWindowHeight;

    source: '/tmp/interactive/framework/viewcontroller/ExternalPopupContainer.qml'
    visible: false;

    onMessageReceived:{
        console.log("ExternalPopup | onMessageReceived | message: "+message);
        var mesgArr = message.split('#')
        if(mesgArr[0]=='SYSTEM'){
            if(mesgArr[1]=='SetDimension'){
                popupWindowX=mesgArr[2];
                popupWindowY=mesgArr[3];
                popupWindowWidth=mesgArr[4];
                popupWindowHeight=mesgArr[5];
            }else if(mesgArr[1]=='ClosePopup'){
                viewController.updateSystemPopupQ(parseInt(mesgArr[2],10),false);
            }
        }
        if(mesgArr[0]=='POPUP'){
            externalPopupMsg(mesgArr[1],mesgArr[2]);
        }
    }

     onPriorityChanged:{
         console.log("ExternalPopup.qml | onPriorityChanged | priority : "+priority)
         if(priority==Popup.Critical){thisPopUp.systemBarVisibilityType = Popup.SYSTEM_BAR_HIDE;}
         else {thisPopUp.systemBarVisibilityType = Popup.SYSTEM_BAR_VISIBLE;}
    }
  /*  onPopupLifecycleStopped:{
        console.log("ExternalPopup.qml | onPopupLifecycleStopped");
        pif.android.movePopupLauncherToForeground();
    }
*/
    Component.onCompleted:{
        sendMessage("SYSTEM#SetVariable#"+((pif.android)?true:false));
        if(pif.android) thisPopUp.clearQueueOnHide=true;
        thisPopUp.priority=Popup.Normal;
    }

    function setPopUpBarVisibilityType(visibilityType){
        if(!pif.android) {console.log("ExternalPop.qml | __setPopUpBarVisibilityType | Not an Android seat. Exiting..."); return false;}
        console.log("ExternalPop.qml | __setPopUpBarVisibilityType | visibilityType: "+visibilityType);
        if(visibilityType==cSYSTEM_BAR_DIM) thisPopUp.systemBarVisibilityType=Popup.SYSTEM_BAR_DIM;
        else if(visibilityType==cSYSTEM_BAR_HIDE) thisPopUp.systemBarVisibilityType=Popup.SYSTEM_BAR_HIDE;
        else thisPopUp.systemBarVisibilityType=Popup.SYSTEM_BAR_VISIBLE;
    }

    function __setPopUpPriority(priority){
        console.log("ExternalPop..qml | __setPopUpPriority | priority :"+priority)
        if(priority==cPRIORITY_CRITICAL)
            thisPopUp.priority=Popup.Critical;
        else if(priority==cPRIORITY_HIGH)
            thisPopUp.priority=Popup.High;
        else
            thisPopUp.priority=Popup.Normal;
    }
}
