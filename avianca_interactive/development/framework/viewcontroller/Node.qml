import QtQuick 1.1

QtObject{
    property int node_id;
    property string template_id;
    property string id;

    function formNode(a,b){
        node_id = parseInt(a,10);
        if(b) template_id = b;
        else template_id="NA"
        id=node_id+"_"+template_id
    }
}
