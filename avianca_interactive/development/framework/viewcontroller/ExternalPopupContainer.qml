import QtQuick 1.1
import Panasonic.Pif 1.0
import Panasonic.PopupManager 1.0

PopupContainer {
    id: externalPopupContainer
    property variant popupRef
    property bool android: false;
    property int extAppWidth: (android)?width:screenWidth;
    property int extAppHeight: (android)?height:screenHeight;
    property string assetImagePath: "/tmp/interactive/viewAssets/assets0/externalPopup/images/"
    property int languageID
    property string languageISO
    property int activeExternalPopupID
    winVisible: true;

    function setPopupDimension(x,y,w,h){
        console.log("ExternalPopupContainer | setPopupDimension | x:  "+x+" y: "+y+" width: "+w+" height: "+h);
        sendMessage("SYSTEM#SetDimension#"+x+"#"+y+"#"+w+"#"+h);
    }
    function closePopup(popupId){
        if(popupId==undefined) popupId=activeExternalPopupID;
        sendMessage("SYSTEM#ClosePopup#"+popupId);
    }
    function sendMsgToInt(msgString){
        sendMessage('POPUP#'+activeExternalPopupID+'#'+msgString);
    }
    function __hide(){
        externalPopupContainer.winVisible = false;
        return;
    }
    function __show(tmpName) {
        var  component = Qt.createComponent("../../view/externalPopups/"+tmpName+".qml");
        popupRef = component.createObject(externalPopupContainer);
        popupRef.focus=true;
        focusTimer.start();
    }
    onWinFocusChanged: { if (winFocus) { focusTimer.stop(); } }
    onMessageReceived: {
        console.log("ExternalPopupContainer | onMessageReceived | message: "+message);
        var mesgArr = message.split('#')
        if(mesgArr[0]=='SYSTEM' ){
            if(mesgArr[1]=='init'){
                if(popupRef) popupRef.destroy()
                //externalPopupContainer.winVisible = false;
                activeExternalPopupID=parseInt(mesgArr[3],10);
                languageID=mesgArr[4]
                languageISO=mesgArr[5]
                __show(mesgArr[2])
                externalPopupContainer.winVisible = true;
                externalPopupContainer.raiseWindow();
                externalPopupContainer.refreshScreenDimensions();
            }else if(mesgArr[1]=='hide'){
                __hide();
            }else if(mesgArr[1]=='SetVariable'){
                android=(mesgArr[2]=="true")?true:false;
                console.log("ExternalPopupContainer | android: "+android);
              //  if(android)fontLoaderComp.createObject(externalPopupContainer);
              //  if(android)defaultFontComp.createObject(externalPopupContainer);
            }
        }else if(mesgArr[0]=='VIEW'){
            if(mesgArr.length>1){
                mesgArr.shift();
                var intMsg = mesgArr.join("#");
            }else{
                var intMsg = ""
            }
            if(popupRef!='undefined') popupRef.init(intMsg);
        }
    }
    FontLoader{
        id: defaultFont1;
        Component.onCompleted: {
            source = "/tmp/interactive/fonts/DroidNaskhUI-Regular.ttf";
        }
    }

    FontLoader{
        id: defaultFont2;
        Component.onCompleted: {
            source = "/tmp/interactive/fonts/DroidSansFallbackFull.ttf";
        }
    }
    TranslationFileLoader { id: extPopupStyle; source: "/tmp/interactive/viewAssets/assets0/externalPopup/style.qm" }
    Timer {
        id: focusTimer; interval: 500;
        onRunningChanged: { if (running) { externalPopupContainer.activateFocus(); } }
        onTriggered: { externalPopupContainer.activateFocus(); }
    }
}
