// Template_id, ScreenList & Int Popups
var TemplateArr = [];
var ScreenArr = [];
var ScreenPopupArr = [];
var SystemPopupArr = [];

// Screen & Int popup layout
var StyleConfigArr = [];
var ParentInfoArr = [];
var ChildScreenArr = [];
var UnloadScrnArr = [];
var ChildScrnPopArr = [];
var UnloadScrnPopArr = [];
var UnloadSystemPopArr = [];
var PopupQArr = [];

// View shared variables
var ActiveScreenID;
var ActiveScreenPopupID=0;
var ActiveSystemPopupID=0;
var PreloadStatus;
var AnimParallel=true;
var AnimOnReload=false;
var ExtPopupMsgArray=[];
var VideoStateOnAlert=0;
var AudioStateOnAlert=0;

var ViewToHideLoadingScreen=false;
var ViewToSetTranslations=false;
var ShowSystemPopupOnCloseFlight=false;
var externalAppActiveStatus=false;

var DelayKarmaLoading=true;

function setConfiguration(viewSection) {
    ScreenArr = coreHelper.updateCfgArray (viewSection.ViewScreens, "template_id,viewId,bg,childViewId,childScrnPopViewId", "template_id");
    ScreenPopupArr = coreHelper.updateCfgArray (viewSection.ViewScreenPopups, "index,viewId,pauseAudio,pauseVideo,timeout", "index");
    TemplateArr = coreHelper.updateCfgArray (viewSection.ViewList, "id,viewName,type,keepLoaded", "id");
    SystemPopupArr = coreHelper.updateCfgArray (viewSection.ViewSystemPopups, "index,viewId,extPopup,groupId,priority,pauseAudio,pauseVideo,screenOn,timeout,extPriority", "index");
    ViewToHideLoadingScreen = viewSection.ViewToHideLoadingScreen?(viewSection.ViewToHideLoadingScreen=="true"?true:false):ViewToHideLoadingScreen;
    ViewToSetTranslations = viewSection.ViewToSetTranslations?(viewSection.ViewToSetTranslations=="true"&&pif.getMonitorType()==pif.cLRU_GEODE?true:false):ViewToSetTranslations;
    ShowSystemPopupOnCloseFlight = viewSection.ShowSystemPopupOnCloseFlight?(viewSection.ShowSystemPopupOnCloseFlight=="true"?true:false):ShowSystemPopupOnCloseFlight;
    DelayKarmaLoading = viewSection.DelayKarmaLoading?(viewSection.DelayKarmaLoading=="false"?false:true):DelayKarmaLoading;
    if(pif.getMonitorType()==pif.cLRU_VKARMA){__unloadKickInTimout=3000;__disableLookAheadLoading=true;}
}
