#/bin/sh


START=$(date +%s)

# PURPOSE: To create a shell script to parse data from ini files to ts file and convert it into qm file

# Set current directory
curdir=`pwd`
ArrayDir=@{}
echo "$1"
if [ ! -d $1 ]; then
  	echo "$1 folder does not exist"
	exit 1
	else
           echo "$1 folder exist"
	cd $1
ArrayDir[0]="`pwd`"
cd $curdir
fi

TSFILE=false
	which lrelease;
		if [[ $? == 0 ]] ; then
			echo "lrelease utility is installed"
				else
			echo "lrelease could not be found on your system, we wont be able to generate a .qm file"
            		read -p "Do you want to generate a ts file instead? (y/n) " RESP
			if [ "$RESP" = "y" ]; then
				TSFILE=true
				else
 					 echo "Please install lrelease and try again"
				exit 1
			fi

		 fi  

	which col;
		if [[ $? == 0 ]] ; then
				echo "col utility is installed"
				else
				echo "col could not be found on your system, please install col and try again"
				exit 1
		fi

ArrayLeft=@{}
ArrayRight=@{}
ini=""

cfg_parser ()
{
		ini=""
		ini="$(<$1)"               			 # read the file
    ini="${ini//[/\[}"         			 # escape [
    ini="${ini//]/\]}"         			 # escape ]
    IFS=$'\n' && ini=( ${ini} ) 		 # convert to line-array
    ini=( ${ini[*]//;*/} )      		 # remove comments with ;
    ini=( ${ini[*]/\    =/=} )  		 # remove tabs before =
    ini=( ${ini[*]/=\   /=} )   		 # remove tabs be =
    ini=( ${ini[*]/\ =\ /=} )       		 # remove anything with a space around =
    ini=( ${ini[*]/#\\[/\}$'\n'cfg.section.} )   # set section prefix
    ini=( ${ini[*]/%\\]/ \(} )     	         # convert text2function (1)
    ini=( ${ini[*]/=/=\( } )        		 # convert item to array
    ini=( ${ini[*]/%/ \)} )         		 # close array parenthesis
    ini=( ${ini[*]/%\\ \)/ \\} )     		 # the multiline trick
    ini=( ${ini[*]/%\( \)/\(\) \{} )		 # convert text2function (2)
    ini=( ${ini[*]/%\} \)/\}} )	     		 # remove extra parenthesis
    ini[0]=""  			    		 # remove first element
    ini[${#ini[*]} + 1]='}'          		 # add the last brace
    eval "$(echo "${ini[*]}")"       		 # eval the result
}


cfg_parser_replace ()
{
    ini=""
    ini="$(<$1)"              			 # read the file
    ini="${ini//[/\[}"         			 # escape [
    ini="${ini//]/\]}"        			 # escape ]
    IFS=$'\n' && ini=( ${ini} ) 		 # convert to line-array
    ini=( ${ini[*]//;*/} )      		 # remove comments with ;
    ini=( ${ini[*]/\    =/=} ) 		 	 # remove tabs before =
    ini=( ${ini[*]/=\   /=} )  			 # remove tabs be =
    ini=( ${ini[*]/\ =\ /=} )  			 # remove anything with a space around =
    ini=( ${ini[*]/#\\[/\}$'\n'cfg.rsection.} )  # set section prefix
    ini=( ${ini[*]/%\\]/ \(} )  		 # convert text2function (1)
    ini=( ${ini[*]/=/=\( } )   			 # convert item to array
    ini=( ${ini[*]/%/ \)} )    			 # close array parenthesis
    ini=( ${ini[*]/%\\ \)/ \\} ) 		 # the multiline trick
    ini=( ${ini[*]/%\( \)/\(\) \{} ) 		 # convert text2function (2)
    ini=( ${ini[*]/%\} \)/\}} ) 		 # remove extra parenthesis
    ini[0]="" 					 # remove first element
    ini[${#ini[*]} + 1]='}'   		 	 # add the last brace
    eval "$(echo "${ini[*]}")" 			 # eval the result
}

cfg_writer ()
{
IFS=' '$'\n'
fun="$(echo "${ini[*]}")"
#fun="$(declare -F)"
#fun="${fun//declare -f/}"
for f in $fun; do
[ "${f#cfg.section}" == "${f}" ] && continue
item="$(declare -f ${f})"
item="${item##*\{}"
item="${item%\}}"
vars="$item"
echo "[${f#cfg.section.}]"
for var in $item; do
var="${var%=*}"
echo $var=\"${!var}\"
temp=eval "${f}"
ArrayLeftTemp[$i]="$1_${f#cfg.section.}_$var"  
ArrayRightTemp[$i]=${!var}
i=`expr $i + 1`
done
i=`expr $i + 1`
echo
done
}

cfg_writer_replace ()
{
IFS=' '$'\n'
fun="$(declare -F)"
fun="${fun//declare -f/}"
i=0 
for f in $fun; do
[ "${f#cfg.rsection}" == "${f}" ] && continue
item="$(declare -f ${f})"
item="${item##*\{}"
item="${item%\}}"
vars="$item"
echo "[${f#cfg.rsection.}]"
for var in $item; do
var="${var%=*}"
echo $var=\"${!var}\"
temp=eval "${f}"
ArrayRLeft[$i]="$var"
ArrayRRight[$i]=${!var}
i=`expr $i + 1`
done
i=`expr $i + 1`
echo
done
}

curdir=`pwd`
rm -rf *.qs


	for entry1 in "${!ArrayDir[@]}"; do
	cd $curdir
	echo ${ArrayDir[$entry1]}
	cd ${ArrayDir[$entry1]}
	for INIFILES in *.{ini,txt}; do
        FILENAMES=(`echo "$INIFILES"`)
        echo ${FILENAMES[@]}

        cat ${FILENAMES[@]} | col -b > ${FILENAMES[@]}.bak
        mv ${FILENAMES[@]}.bak ${FILENAMES[@]}   


 	done

	for entry in *.ini; do
		FILES=(`echo "$entry"`)
		echo ${FILES[@]}
		FIRST=(`echo "$FILES" | sed 's/\(.*\)\..*/\1/'`)
		cfg_parser ${FILES[@]};
		cfg_writer ${FIRST};
		for tmpindex in "${!ArrayLeftTemp[@]}"; do
			ArrayLeft[$tmpindex]="${ArrayLeftTemp[$tmpindex]}"
			ArrayRight[$tmpindex]="${ArrayRightTemp[$tmpindex]}"
		done
	done

sed -i 's/[ \t]*$//' replace.txt

sed -e 's/\s\+/___SPACE___/g' replace.txt > replace.bak
mv replace.bak replace.txt
	cfg_parser_replace 'replace.txt';
	cfg_writer_replace;
sed -e 's/___SPACE___/ /g' replace.txt > replace.bak
mv replace.bak replace.txt

	for index in "${!ArrayRLeft[@]}";do
		for index1 in "${!ArrayRight[@]}";do
			if [ ${ArrayRLeft[$index]} = ${ArrayRight[$index1]} ]; then
				ArrayRight[$index1]="${ArrayRRight[$index]}"
			fi
		done
	done
cd ..;
	printf '<!DOCTYPE TS>\n<TS>\n' > style.ts
	for index in "${!ArrayLeft[@]}";do
		printf '<context><message><source>'${ArrayLeft[$index]}'</source><translation>'${ArrayRight[$index]}'</translation></message></context>\n'  >> style.ts
	done
	printf '</TS>' >> style.ts
sed -e 's/___SPACE___/ /g' style.ts > style.bak
mv style.bak style.ts

lrelease -removeidentical -silent style.ts

if $TSFILE ; then
    echo '.ts file has been generated successfully'
exit
fi
#	lrelease -removeidentical -silent style.ts
#rm -rf *.ts
echo "Completed : .qm file has been generated successfully"
    cd $curdir
    echo $curdir
   # rm -rf style.ts style.qm *.ini *.txt

done


END=$(date +%s)
DIFF=$(( $END - $START ))
echo "The script took $DIFF seconds to complete"
