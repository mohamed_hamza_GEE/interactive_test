#!/bin/sh

SOMEDIR=`dirname $0`
INTDIR=`cd $SOMEDIR/../..; pwd`
TMPDIR="/tmp"
ETCDIR="/etc"
SEATAPPBASE_LOG_FILE="${TMPDIR}/interactive.log"
SEATAPPBASE_PID_FILE="${TMPDIR}/seatappbase.pid"
SEATAPPBASE_QML_DIR="${TMPDIR}/interactive"
MY_NAME=`basename $0 .sh`
PLUGIN_CFG_DIR="/tmp"
PLUGIN_NAME="seatappbase"
MAIN_QML_FILE="${INTDIR}/main.qml" 
# get plugin profile vars
VERSION="00.00.0"

if [ -e ${ETCDIR}/sysconfig/plugin_info ]; then
	. ${ETCDIR}/sysconfig/plugin_info
fi
 
if [ -e $PLUGIN_CFG_DIR/$PLUGIN_NAME ]; then
	PLUGIN_CFG_FILE="$PLUGIN_CFG_DIR/$PLUGIN_NAME"
fi

if [ -e $PLUGIN_CFG_FILE ]; then
	. $PLUGIN_CFG_FILE
        VERSION=`cat ${SEATAPPBASE_MNTDIR}"/version/VERSION.TXT" | grep "version" | cut -d'=' -f2`
fi
export PIFCLIENTID=1 
export PIFCLIENTNAME="Interactive, ${VERSION}" 

 
exec >> $SEATAPPBASE_LOG_FILE 2>&1
 
date
 
if [ -e ${ETCDIR}/profile ]; then
	. ${ETCDIR}/profile
fi

 
# give time for the script that executed this script to write the pid to the file
sleep 1
 
PID_FROM_FILE=""
 
if [ -e $SEATAPPBASE_PID_FILE ]; then
        PID_FROM_FILE=`cat $SEATAPPBASE_PID_FILE`
else
        echo "$PLUGIN_NAME  $MY_NAME  pid file does not exists...  exiting."
        exit 1
fi
 
if [ "$PID_FROM_FILE" != "$$" ]; then
        echo "$PLUGIN_NAME  $MY_NAME  pid mismatch... exiting.  [$$]  [$PID_FROM_FILE]"
        exit 1
else
        echo "$PLUGIN_NAME  $MY_NAME  pid matched... executing"
fi
 
cd $INTDIR
while [ "$PID_FROM_FILE" = "$$" ]; do
        qtengine $MAIN_QML_FILE $engine_params
 
        sleep 1
 
        if [ -e $SEATAPPBASE_PID_FILE ]; then
                PID_FROM_FILE=`cat $SEATAPPBASE_PID_FILE`
        else
                PID_FROM_FILE=""
        fi
done
 
exit 0
