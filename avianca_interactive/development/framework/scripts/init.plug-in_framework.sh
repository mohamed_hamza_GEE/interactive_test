#!/bin/sh
echo "Executing init.plug-in_framework.sh file"

TMPDIR="/tmp"
SDADIR="/mnt/sda"
INTDIR=${TMPDIR}/interactive
ETCDIR="/etc"
QT_PLUGINDIR=${TMPDIR}/qtengine/plugin
QT_IMPORTDIR=${TMPDIR}/qtengine/imports
PLUGIN_CFG_DIR="/tmp"
ARM=0

#Default values
MONITOR_TYPE=unknown        # MONITOR_TYPE: geode, eco9, pseb, hdpseb, elite, vkarma, fe, via, lxgeode, sm17
MASK_IMAGE_SUPPORT=0
MNG_SUPPORT=0

PLUGIN_NAME="seatappbase"
# setup plugin profile
if [ -e ${ETCDIR}/sysconfig/plugin_info ]; then
	. ${ETCDIR}/sysconfig/plugin_info
fi

PLUGIN_CFG_FILE="$PLUGIN_CFG_DIR/$PLUGIN_NAME"

# get plugin profile vars 
if [ -e $PLUGIN_CFG_FILE ]; then
	. $PLUGIN_CFG_FILE
fi

exec >> ${TMPDIR}/init.d.seatAppBase.log 2>&1  

# Interactive Specific Configuration
curdir=`dirname $0`
if [ -e ${curdir}"/../../config/config.ini" ]; then
 	INTDIR=${curdir}/../../
elif [ ! -e "${INTDIR}/config/config.ini" ]; then
	echo "No Config found. Exiting..."
	exit 0
fi

INTDIR=`cd ${INTDIR}; pwd`
echo "INTDIR="${INTDIR}
configpath=${INTDIR}/config/config.ini
echo "configpath="${configpath}

if [ "X${configpath}" != "X" ]; then
	# Mask Image
    tmp=`grep '^[^//]' ${configpath} | grep 'MaskImageSupport' | tail -1 | grep -i "true"`
	if [ "X${tmp}" != "X" ]; then 
		MASK_IMAGE_SUPPORT=1; 
	fi
	# MNG
    tmp=`grep '^[^//]' ${configpath} | grep 'MngImageSupport' | tail -1 | grep -i "true"`
	if [ "X${tmp}" != "X" ]; then 
		MNG_SUPPORT=1; 
	fi	
fi

echo "MASK_IMAGE_SUPPORT: "${MASK_IMAGE_SUPPORT}
echo "MNG_SUPPORT: "${MNG_SUPPORT}
cd -

# Variables to export...
if [ -e  ${ETCDIR}/lru_type ] ; then
	LRU_TYPE_VALUE=`cat ${ETCDIR}/lru_type`
fi

if [ "X${LRU_TYPE_VALUE}" = "X" ] ; then
        LRU_TYPE_VALUE=0;
fi
echo "LRU_TYPE_VALUE: "${LRU_TYPE_VALUE}

if [ -e ${ETCDIR}/lru_subtype ] ; then
	LRU_SUB_TYPE_VALUE=`cat ${ETCDIR}/lru_subtype`
fi

if [ "X${LRU_SUB_TYPE_VALUE}" = "X" ] ; then
        LRU_SUB_TYPE_VALUE=0;
fi
echo "LRU_SUB_TYPE_VALUE: "${LRU_SUB_TYPE_VALUE}

# Monitor detection
if ([ "${LRU_TYPE_VALUE}" = 196 ] && ([ "${LRU_SUB_TYPE_VALUE}" = 1 ] || [ "${LRU_SUB_TYPE_VALUE}" = 5 ])) ; then      # SDU Geode 9inch & 7inch
    MONITOR_TYPE=geode
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && ([ "${LRU_SUB_TYPE_VALUE}" = 2 ] || [ "${LRU_SUB_TYPE_VALUE}" = 4 ])) ; then      # VIA - cn400
    MONITOR_TYPE=via
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && ([ "${LRU_SUB_TYPE_VALUE}" -ge 101 ] && [ "${LRU_SUB_TYPE_VALUE}" -le 112 ])) ; then      # FE - cx700
    MONITOR_TYPE=fe
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && ([ "${LRU_SUB_TYPE_VALUE}" = 6 ] || [ "${LRU_SUB_TYPE_VALUE}" = 7 ])) ; then      # LXGeode
    MONITOR_TYPE=lxgeode
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && ([ "${LRU_SUB_TYPE_VALUE}" -ge 120 ] && [ "${LRU_SUB_TYPE_VALUE}" -le 128 ])) ; then      # SM Elite
    MONITOR_TYPE=elite
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && [ "${LRU_SUB_TYPE_VALUE}" = 129 ]) ; then      # SM Elitev2
    MONITOR_TYPE=elitev2
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && [ "${LRU_SUB_TYPE_VALUE}" = 3 ]) ; then      # SM17
    MONITOR_TYPE=sm17
elif ([ "${LRU_TYPE_VALUE}" = 203 ] && [ "${LRU_SUB_TYPE_VALUE}" = 5 ]) ; then      # pseb
    MONITOR_TYPE=pseb
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && ([ "${LRU_SUB_TYPE_VALUE}" -ge 8 ] && [ "${LRU_SUB_TYPE_VALUE}" -le 12 ])) ; then      # Eco 9inch/a & 9inch
    MONITOR_TYPE=eco9
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && ([ "${LRU_SUB_TYPE_VALUE}" = 130 ] || [ "${LRU_SUB_TYPE_VALUE}" = 134 ])) ; then      # Eco9v2
        MONITOR_TYPE=eco9v2
elif ([ "${LRU_TYPE_VALUE}" = 196 ] && ([ "${LRU_SUB_TYPE_VALUE}" = 132 ] || [ "${LRU_SUB_TYPE_VALUE}" = 133 ])) ; then      # Altus
        MONITOR_TYPE=altus
elif ([ "${LRU_TYPE_VALUE}" = 203 ] && [ "${LRU_SUB_TYPE_VALUE}" -le 17 ]) ; then      # hdpseb
    MONITOR_TYPE=hdpseb
elif ([ "${LRU_TYPE_VALUE}" = 47 ] && ([ "${LRU_SUB_TYPE_VALUE}" = 50 ] || [ "${LRU_SUB_TYPE_VALUE}" = 54 ])) ; then      # vKarma
    MONITOR_TYPE=vkarma
else
    MONITOR_TYPE=unknown
fi

if ([ "X${MONITOR_TYPE}" = "Xeco9" ] || [ "X${MONITOR_TYPE}" = "Xeco9v2" ] ||[ "X${MONITOR_TYPE}" = "Xvkarma" ] || [ "X${MONITOR_TYPE}" = "Xhdpseb" ] || [ "X${MONITOR_TYPE}" = "Xaltus" ]) ; then
	ARM=1
fi

echo "MONITOR_TYPE: "${MONITOR_TYPE}
#export monitor_type=${MONITOR_TYPE}
echo "MONITOR_TYPE=${MONITOR_TYPE}" >> $PLUGIN_CFG_FILE

Run_Game=`which rungame`
echo "Run_Game: "$Run_Game
#export run_game=$Run_Game
echo "RUN_GAME=$Run_Game" >> $PLUGIN_CFG_FILE
	
# Added copy of database only for geode because of performance reasons.
if [ "X${MONITOR_TYPE}" = "Xgeode" ] ; then
	srcdb=${TMPDIR}/media/skeletal/database.sql3
	desdb=${SDADIR}/media.db
	if [ -e ${srcdb} ] ; then
		DB_CKSUM_B=`cksum ${srcdb} | cut -d" " -f1`        
		if [ -e ${desdb} ] ; then
			DB_CKSUM_A=`cksum ${desdb}  | cut -d" " -f1`
		fi

		if [ "X${DB_CKSUM_A}" != "X${DB_CKSUM_B}" ] ; then
			if [ -e ${desdb} ] ; then
				rm ${desdb}
			fi
			cp ${srcdb} ${desdb}
		fi
	fi
	if [ -e ${INTDIR}/content/media/media.db ] ; then
		rm ${INTDIR}/content/media/media.db
	fi
	ln -s ${SDADIR}/media.db ${INTDIR}/content/media/media.db
fi

# Add imports
# Add Image Mask 
if [ "X${MASK_IMAGE_SUPPORT}" = "X1" ] ; then
	image_mask_libpath=ImageMask/libqmlarsenal.so
	if [ ! -e "${QT_IMPORTDIR}/${image_mask_libpath}" ]; then       # Check if file does not exist.
		echo "COULDNT FIND IMAGE MASK LIB. Softlinking it..."
		mkdir -p ${QT_IMPORTDIR}/ImageMask
		echo "ANDROID: "${ANDROID}
		if ([ "X${ANDROID}" = "Xtrue" ]) ; then
			if [ -e "${INTDIR}/framework/imports/ImageMask/libqmlarsenal-android.so" ]; then
				ln -s ${INTDIR}/framework/imports/ImageMask/libqmlarsenal-android.so ${QT_IMPORTDIR}/${image_mask_libpath}
			fi
                elif ([ "X${MONITOR_TYPE}" = "Xelitev2" ] || [ "X${MONITOR_TYPE}" = "Xeco9v2" ]) ; then      # Monitors with Newer kernels Processors
                        if [ -e "${INTDIR}/framework/imports/ImageMask/libqmlarsenal-freescale.so" ]; then
                                ln -s ${INTDIR}/framework/imports/ImageMask/libqmlarsenal-freescale.so ${QT_IMPORTDIR}/${image_mask_libpath}
                        fi
		elif ([ "X${ARM}" = "X1" ]) ; then      # Monitors with ARM Processors
			if [ -e "${INTDIR}/framework/imports/ImageMask/libqmlarsenal-ism.so" ]; then
				ln -s ${INTDIR}/framework/imports/ImageMask/libqmlarsenal-ism.so ${QT_IMPORTDIR}/${image_mask_libpath}
			fi
		elif ([ "X${MONITOR_TYPE}" = "Xelite" ]) ; then      # Monitors with Newer kernels Processors
			if [ -e "${INTDIR}/framework/imports/ImageMask/libqmlarsenal-elite.so" ]; then
				ln -s ${INTDIR}/framework/imports/ImageMask/libqmlarsenal-elite.so ${QT_IMPORTDIR}/${image_mask_libpath}
			fi
		else
			if [ -e "${INTDIR}/framework/imports/${image_mask_libpath}" ]; then
				ln -s ${INTDIR}/framework/imports/${image_mask_libpath} ${QT_IMPORTDIR}/${image_mask_libpath}
			fi
		fi
		if [ -e "${INTDIR}/framework/imports/ImageMask/qmldir" ]; then
			ln -s ${INTDIR}/framework/imports/ImageMask/qmldir ${QT_IMPORTDIR}/ImageMask/
		fi
	fi
fi

# Add mng plugins
if [ "X${MNG_SUPPORT}" = "X1" ] ; then		
	mng_libpath=imageformats/libqmng.so
	if [ ! -e "${QT_PLUGINDIR}/${mng_libpath}" ]; then       # Check if file does not exist.
		echo "COULDNT FIND mng LIB. Softlinking it..."
		mkdir -p ${QT_PLUGINDIR}/imageformats
		if ([ "X${ARM}" = "X1" ]) ; then      # Monitors with ARM Processors
			if [ -e "${INTDIR}/framework/plugins/imageformats/libqmng-ism.so" ]; then
				ln -s ${INTDIR}/framework/plugins/imageformats/libqmng-ism.so ${QT_PLUGINDIR}/${mng_libpath}
			fi
		elif ([ "X${MONITOR_TYPE}" = "Xelite" ]) ; then      # Monitors with Newer kernels Processors
			if [ -e "${INTDIR}/framework/plugins/imageformats/libqmng-elite.so" ]; then
				ln -s ${INTDIR}/framework/plugins/imageformats/libqmng-elite.so ${QT_PLUGINDIR}/${mng_libpath}
			fi
		else
			if [ -e "${INTDIR}/framework/plugins/${mng_libpath}" ]; then
				ln -s ${INTDIR}/framework/plugins/${mng_libpath} ${QT_PLUGINDIR}/${mng_libpath}
			fi
		fi
	fi
fi	
	
# Config Tool Data
if [ -e "${INTDIR}/view/ThemeManager.qml" ]; then       # file exist.
	echo "Interactive supports Config Tool."
	if [ -d "/tmp/media/other/GuiConfigContent.squash/themes" ]; then       # folder exist.
		rm -rf ${INTDIR}/content/configtool
		mkdir -p ${INTDIR}/content/configtool
		ln -s /tmp/media/other/GuiConfigContent.squash/themes ${INTDIR}/content/configtool/
		if [ -d "/tmp/media/other/GuiConfigContent.squash/microapps" ]; then       # folder exist.
			ln -s /tmp/media/other/GuiConfigContent.squash/microapps ${INTDIR}/content/configtool/
		fi
	fi
else
	echo "Interactive does not support Config Tool."
fi
