#!/bin/sh

# For Android os don't execute the script.
if [ -e "/system/bin/logcat" ]; then	
	exit 1
fi	

TMPDIR="/tmp"
INTDIR=${TMPDIR}/interactive
INT_VOYAGERDIR=/home/madc/.mozilla/plugins/libASXiQtDtPlugin.so
PID_FILE=${TMPDIR}/seatappbase.pid
ENGINE_PARAMS=
ETCDIR="/etc"
PLUGIN_CFG_DIR=${TMPDIR}
SEATAPPBASE_PID_FILE=${TMPDIR}/seatappbase.pid
SEATAPPBASE_LOG_FILE="${TMPDIR}/interactive.log"
USRDIR="/usr/bin"
ARM=0

#Default values
INT_VOYAGER=0
WITH_OPENGL=0
WINDOW_MODE=0
MASK_IMAGE_SUPPORT=0
MNG_SUPPORT=0

PLUGIN_NAME="seatappbase"
 
# get plugin profile vars
if [ -e ${ETCDIR}/sysconfig/plugin_info ]; then
	. ${ETCDIR}/sysconfig/plugin_info	
fi

PLUGIN_CFG_FILE="$PLUGIN_CFG_DIR/$PLUGIN_NAME"

# 
if [ ! -e $PLUGIN_CFG_FILE ]; then	
	if ([ -e ${ETCDIR}/lru.cfg ] && [ -e ${ETCDIR}/lru_type ] && [ -e ${ETCDIR}/lru_subtype ]); then	
		echo "RACK=true" >> $PLUGIN_CFG_FILE
	else
		echo "RACK=false" >> $PLUGIN_CFG_FILE
	fi
	echo "ANDROID=false" >> $PLUGIN_CFG_FILE
	./init.plug-in_framework.sh
fi

# get plugin profile vars 
if [ -e $PLUGIN_CFG_FILE ]; then
	. $PLUGIN_CFG_FILE
fi

exec >> ${SEATAPPBASE_LOG_FILE} 2>&1

echo "RACK: "${RACK}  

# ps command detection
ps ax
if [ "X$?" != "X0" ]; then
    echo "ps ax command does not work. Using ps w"
    PROCESS_COMMAND="ps w"
else
    echo "ps ax command works."
    PROCESS_COMMAND="ps ax"
fi

INTDIR=`cd ${INTDIR}; pwd`
echo "INTDIR="${INTDIR}
configpath=${INTDIR}/config/config.ini
echo "configpath="${configpath}

if [ "X${configpath}" != "X" ]; then
	# OPENGL
    tmp=`grep '^[^//]' ${configpath} | grep 'OpenGL' | tail -1 | grep -i "true"`
	if [ "X${tmp}" != "X" ]; then 
		WITH_OPENGL=1; 
	fi
	# INT_VOYAGER
    tmp=`grep '^[^//]' ${configpath} | grep 'InteractiveVoyager' | tail -1 | grep -i "true"`
	if [ "X${tmp}" != "X" ]; then 
		INT_VOYAGER=1; 
	fi
	# WINDOW MODE
    tmp=`grep '^[^//]' ${configpath} | grep 'WindowMode' | tail -1 | grep -i "true"`
	if [ "X${tmp}" != "X" ]; then 
		WINDOW_MODE=1; 
	fi
fi

echo "INT_VOYAGER: "${INT_VOYAGER}
echo "WITH_OPENGL: "${WITH_OPENGL}
echo "WINDOW_MODE: "${WINDOW_MODE}
cd -

MONITOR_TYPE=`grep '^[^//]' ${PLUGIN_CFG_FILE} | grep 'MONITOR_TYPE' | tr -d "MONITOR_TYPE="`

if ([ "X${MONITOR_TYPE}" = "Xeco9" ] || [ "X${MONITOR_TYPE}" = "Xvkarma" ] || [ "X${MONITOR_TYPE}" = "Xhdpseb" ]) ; then
	ARM=1
	INT_VOYAGERDIR=/home/madc/.mozilla/plugins/libASXiQtArmPlugin.so
fi
echo "MONITOR_TYPE: "${MONITOR_TYPE}

if [ ! -e ${USRDIR}/interactive ]; then
	ln -s ${SEATAPPBASE_MNTDIR}/init.d/99_seatAppBase.sh ${USRDIR}/interactive
fi

export
date
. ${ETCDIR}/profile

start()
{
    if [ -e $SEATAPPBASE_PID_FILE ]; then
        echo "${0} is already started."
        exit 1
    fi
    echo "STARTING INTERACTIVE..."	
    cd ${INTDIR}
	
    if [ "X${MONITOR_TYPE}" = "Xgeode" ] ; then
		ENGINE_PARAMS="${ENGINE_PARAMS} -fast "
    fi
	
	# check if INT Voyager needed
	if [ "X${INT_VOYAGER}" = "X1" ] ; then
		ENGINE_PARAMS="${ENGINE_PARAMS} -asxi ${INT_VOYAGERDIR}"
	fi

	# check if OpenGL enabled (not available for Geode)	
	if ([ "X${WITH_OPENGL}" = "X1" ] && [ "X${MONITOR_TYPE}" != "Xgeode" ] && [ "X${MONITOR_TYPE}" != "Xlxgeode" ] && [ "X${RACK}" = "Xtrue" ]); then
		export __GL_SYNC_TO_VBLANK=1
		export OPENGL_VERSION_1_2=1
		export TDX_VIDLE=1
		export TDX_WM_DELAY=20000
		ENGINE_PARAMS="${ENGINE_PARAMS} -opengl"		
	fi
	
	if [ "X${WINDOW_MODE}" = "X1" ] ; then
		ENGINE_PARAMS="${ENGINE_PARAMS} -window"	
	fi
	
	if ([ "X${MONITOR_TYPE}" = "Xeco9" ]); then
		export QT_X11_NO_XRENDER=1
		export QT_NO_XRENDER=1
	fi
	
	# Config Tool Data
	if [ -e "${INTDIR}/view/ThemeManager.qml" ]; then       # file exist.
		echo "Interactive supports Config Tool."
		if [ -d "/tmp/media/other/GuiConfigContent.squash/themes" ]; then       # folder exist.
			rm -rf ${INTDIR}/content/configtool
			mkdir -p ${INTDIR}/content/configtool
			ln -s /tmp/media/other/GuiConfigContent.squash/themes ${INTDIR}/content/configtool/
			if [ -d "/tmp/media/other/GuiConfigContent.squash/microapps" ]; then       # folder exist.
				ln -s /tmp/media/other/GuiConfigContent.squash/microapps ${INTDIR}/content/configtool/
			fi
		fi
	else
		echo "Interactive does not support Config Tool."
	fi
	
	if ([ "X${ARM}" = "X1" ]) ; then
		xset r off
	fi

	echo ${WINDOW_MODE}
        export window_mode=${WINDOW_MODE}
        if ([ "X${MONITOR_TYPE}" = "Xfe" ] || ["X${MONITOR_TYPE}" = "Xvia"]); then
            export TDX_TEX_IDLE=1
        fi
 	
	if [ -e $SEATAPPBASE_PID_FILE ]; then
		echo "${0} is already started."
		exit 1
	fi
	echo ${ENGINE_PARAMS}
 	export engine_params="${ENGINE_PARAMS}"
 	cd ${INTDIR}
 	$INTDIR/framework/scripts/run.sh  &
	[ $? = 0 ] && echo $! > $SEATAPPBASE_PID_FILE

}

stop()
{
	[ -e $SEATAPPBASE_PID_FILE ] && rm $SEATAPPBASE_PID_FILE
	echo "STOPPING INTERACTIVE..."
	qtengine stop	
}

info()
{
	echo "Usage: ${0} {start|stop|restart}"
}

case "$1" in
	start)		stop	
			start
			;;
	stop)		stop
			;;
	restart)	stop
			start
			;;
	*)		info $0
     			exit 1
			;;
esac 
exit 0
