#!/bin/bash
# PURPOSE: To create a shell script to parse data from style.ts files to QML view files

START=$(date +%s)

# Set current directory
	cd `dirname $0`

# local variables
    #interactive=`pwd`
    #inputfile=${interactive}/viewAssets/assets${1}/style.ts
    inputfile=${1}
	#view_dir=${interactive}/view
    view_dir=${2}
	
# actual code 
	i=0
	while read line; do
        i=`expr $i + 1`

# Reads <source> data into array ArrayLeft from input file style.ts  
        ArrayLeft[$i]=`sed -n $i's/.*<source>\(.*\)<\/source>.*/\1/p' $inputfile`

# Read <translation> data into array ArrayRight from input file style.ts 
        ArrayRight[$i]=`sed -n $i's/.*<translation>\(.*\)<\/translation>.*/\1/p' $inputfile`

# Prints the Array of Source and Destination, 
        echo ${ArrayLeft[$i]}
        echo ${ArrayRight[$i]}
	done <  $inputfile

# Set the relative path of view folder
	cd $view_dir;
	for index in "${!ArrayLeft[@]}"
		do

# Replace qsTranslate('','<source>' with Translation value recursively
		echo "s/qsTranslate('','${ArrayLeft[$index]}')/${ArrayRight[$index]}/g"
		grep -lr "qsTranslate('','${ArrayLeft[$index]}')" . | xargs sed -i "s/qsTranslate('','${ArrayLeft[$index]}')/${ArrayRight[$index]}/g"
	done

# For Logging
	   echo "source translation script executed successfully"

END=$(date +%s)
DIFF=$(( $END - $START ))
echo "The script took $DIFF seconds to complete"
