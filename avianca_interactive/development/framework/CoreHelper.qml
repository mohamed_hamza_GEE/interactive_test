import QtQuick 1.1
import Panasonic.Pif 1.0
// Common utilities required across interactive

QtObject {
    property QtObject __resBrowser: ResourceBrowserModel{ showdotfiles:false }
    property QtObject __configParser: ConfigFileParser{}
    property QtObject xmlParser:Xml{}

    function getConfigFileData(filename, type){ __configParser.sourceType = type; __configParser.sourceFile = filename; return __configParser.variantData;}

    function cleanPath(path){
        path = ''+path; path = path.replace(/file\:\/\//i, '');
        return (path.indexOf(":")!=-1 && path.charAt(0)=="\/")?path.substr(1,path.length):path;
    }

    function removeDblQuotes(s){ s = ''+s; return s.replace(/"/g,'');}

    function replaceReservedChars(s){ s = ''+s; return s.replace(/"/g,'').replace(/\|/g,',').replace(/#/g,'=');}

    function escapeSpaceInString(strReplace){ return strReplace.replace(new RegExp(/ /g),"\\ ");}

    function fileExists(path){ return __resBrowser.fileExists(cleanPath(Qt.resolvedUrl(path)));}

    function isFolder(path){ return __resBrowser.isFolder(path)}

    function resolvePath(path, relativePath){
        var first = (path.length>0 ? path[0] : '');
        if (first=='/') return Qt.resolvedUrl(path);
        return Qt.resolvedUrl(relativePath+path.replace(/^\.\//, ''))
    }

    function cfgVariantObjToArray(obj,fieldlist){		// [fieldlist] - optional
        var s=[]; var v; var ctr1=0; var ctr2=0;
        if (!fieldlist) fieldlist=[]; else fieldlist = fieldlist.split(',');
        for (var i in obj) {
            ctr1=parseInt(i[1]+i[2]);
            v = eval("obj."+i);
            if (typeof(v)=="object") {
                for (var j in v) {
                    ctr2=parseInt(j[1]+j[2]);
                    if (!s[ctr1]) s[ctr1]=[];
                    s[ctr1][(fieldlist[ctr2])?fieldlist[ctr2]:ctr2]=replaceReservedChars(eval("obj."+i+"."+j));
                }
            } else s[ctr1]=replaceReservedChars(v);
        }
        return s;
    }

    function updateCfgArray (cfgRef, fieldlist, sfield, dfield) {
        var tmparr = cfgVariantObjToArray(cfgRef,fieldlist);
        if (sfield==undefined) return tmparr;
        var retArr = [];
        for (var i in tmparr) retArr[tmparr[i][sfield]]=(dfield)?tmparr[i][dfield]:tmparr[i];
        return retArr;
    }

    function randomizeArray(arr) {
        var i = arr.length; if (!i) return false; var j; var tmpi; var tmpj;
        while (--i) {
            j = Math.floor(Math.random()*(i+1));
            tmpi = arr[i]; tmpj = arr[j];
            arr[i] = tmpj; arr[j] = tmpi;
        }
        return arr;
    }

    function loadDynamicComponent(qmlfilepath, viewObjectRef) {
        if (!fileExists(qmlfilepath)) {core.debug("Core | File does not exist: "+qmlfilepath); return null;}
        var component = Qt.createComponent(qmlfilepath);
        if (component.status == Component.Ready) return component.createObject(viewObjectRef);
        else {core.debug("Core | File did not load: "+qmlfilepath); return null;}
    }

    function unloadDynamicComponent(ref) { ref.destroy();}

    /*  SUPPORTED TIMER FORMAT
        Time format	in secs                             => elapseTimeFormat = 0;
        Convert secs to HH:MM:SS                        => elapseTimeFormat = 1; convertSecToHMS();
        Convert secs to HH:MM:SS with leading zeros     => elapseTimeFormat = 2; convertSecToHMSLeadZero();
        Convert secs to HH:MM                           => elapseTimeFormat = 3; convertSecToHM();
        Convert secs to HH:MM with leading zeros        => elapseTimeFormat = 4; convertSecToHMLeadZero();
        Convert secs to MM:SS                           => elapseTimeFormat = 5; convertSecToMS();
        Convert secs to MM:SS with leading zeros        => elapseTimeFormat = 6; convertSecToMSLeadZero();
        Convert secs to HH:MM:SS,truncate HH if it is 0 => elapseTimeFormat = 7; seatapi.utils.convertSecToHMSTrunkH();
        Convert secs to MM:SS with leading zero for SS  => elapseTimeFormat = 8; convertSecToMSLeadZeroForSec();
        Convert secs to H:MM:SS, single zero added if HH is 0  => elapseTimeFormat = 9; seatapi.utils.convertSecToHMMSS();
    */

    function getTimeFormatFromSecs (secs, timeFormat) {
        if (timeFormat == 0) return secs;
        else if (timeFormat == 1) return convertSecToHMS(secs);
        else if (timeFormat == 2) return convertSecToHMSLeadZero(secs);
        else if (timeFormat == 3) return convertSecToHM(secs);
        else if (timeFormat == 4) return convertSecToHMLeadZero(secs);
        else if (timeFormat == 5) return convertSecToMS(secs);
        else if (timeFormat == 6) return convertSecToMSLeadZero(secs);
        else if (timeFormat == 7) return convertSecToHMSTrunkH(secs);
        else if (timeFormat == 8) return convertSecToMSLeadZeroForSec(secs);
        else if (timeFormat == 9) return convertSecToHMMSS(secs);
        else { core.error ("CoreHelper.qml | Unsupported Time Format"); return false;}
    }

    function convertSecToHMS(secs) {          // elapseTimeFormat = 1: Convert secs to HH:MM:SS time format.
        var hr; hr=Math.floor(secs/3600); secs=secs%3600;
        return hr+":"+Math.floor(secs/60)+":"+Math.floor(secs%60);
    }

    function convertSecToHMSLeadZero(secs) {        //elapseTimeFormat = 2: Convert secs to HH:MM:SS time format with leading zeros
        var hr; var mn; var ss;
        hr=Math.floor(secs/3600); secs=secs%3600; mn = Math.floor(secs/60); ss = Math.floor(secs%60);
        return (hr<10?"0"+hr:hr)+":"+(mn<10?"0"+mn:mn)+":"+(ss<10?"0"+ss:ss);
    }

    function convertSecToHM(secs) {                         //elapseTimeFormat = 3: Convert secs to HH:MM time format
        var hr; hr=Math.floor(secs/3600); secs=secs%3600;
        return hr+":"+Math.floor(secs/60);
    }

    function convertSecToHMLeadZero(secs) {                 //elapseTimeFormat = 4: Convert secs to HH:MM time format with leading zeros
        var hr; var mn;
        hr=Math.floor(secs/3600); secs=secs%3600; mn = Math.floor(secs/60);
        return (hr<10?"0"+hr:hr)+":"+(mn<10?"0"+mn:mn);
    }

    function convertSecToMS(secs) {                         // elapseTimeFormat = 5: Convert secs to MM:SS time format.
        return Math.floor(secs/60)+":"+secs%60;
    }

    function convertSecToMSLeadZero(secs) {                 // elapseTimeFormat = 6: Convert secs to MM:SS time format with leading zeros
        var mn; var ss;
        mn=Math.floor(secs/60); ss=Math.floor(secs%60);
        return (mn<10?"0"+mn:mn)+":"+(ss<10?"0"+ss:ss);
    }

    function convertSecToHMSTrunkH(secs) {                 // elapseTimeFormat = 7: Convert secs to HH:MM:SS time format and truncate HH field if it is 0.
        var hr; hr=Math.floor(secs/3600); secs=secs%3600;
        return (hr>0?(hr+":"):"")+Math.floor(secs/60)+":"+ Math.floor(secs%60);
    }

    function convertSecToMSLeadZeroForSec(secs) {           // elapseTimeFormat = 8: Convert secs to MM:SS time format with leading zero only for seconds
        var mn; var ss;
        mn=Math.floor(secs/60); ss=Math.floor(secs%60);
        return mn+":"+(ss<10?"0"+ss:ss);
    }

    function convertSecToHMMSS(secs) {                 // elapseTimeFormat = 9: Convert secs to H:MM:SS time format.
        var hr; hr=Math.floor(secs/3600); secs=secs%3600;
        return (hr>0?(hr+":"):"0:")+((Math.floor(secs/60)<10)?("0"+Math.floor(secs/60)):Math.floor(secs/60))+":"+ ((Math.floor(secs%60)<10)?("0"+Math.floor(secs%60)):Math.floor(secs%60));
    }

    function convertHMSToSec(duration) {
        var splits = duration.split(':');
        return parseInt(splits[0],10)*60*60 + parseInt(splits[1],10)*60 + parseInt(splits[2],10);
    }

    function convertHMSToMin(duration) {
        var splits = duration.split(':');
        return parseInt(splits[0],10)*60 + parseInt(splits[1],10);
    }
    /*
        Convert 24 Hours format to 12 hours format.
        PARAMS:
            Four digit number in the HHMM format.
        RETURN:
        The return value is an array with the below fields.
        ret[0]	0 if the input is valid and the conversion is successful.
                1 if the input is invalid like "1289", "128", "3546"
        ret[1]  Hours information will be present.
        ret[2]  Minutes information will be present.
        ret[3]  0 if it is AM
                1 if it is PM
    */
    function convert24To12Hr(hrs){
        var ampmInfo = 0;var hrs1 = hrs;var hrs2 = hrs;var hrs3 = hrs;
        var len = hrs2.length;var ret = new Array();

        if(len != 4) {ret[0] = 1;return ret;}
        else if(hrs1 > 2400){ret[0] = 1;return ret;}
        else if(hrs1 > 1259){
            hrs1 = hrs1 - 1200
            hrs2 = String(hrs1)
            var len = hrs2.length;
            if(len < 4){hrs2 = "0"+ hrs2}

            if(hrs2.substring(0, 1) == "0"){var hours = hrs2.substring(1, 2)}
            else{var hours = hrs2.substring(0, 2)}
            var mins = hrs2.substring(2, 4)
            ampmInfo = 1;							// for PM
            if(hours == "12") ampmInfo = 0;		// for AM
            if(mins > "59"){ret[0] = 1;return ret;}
            ret[0] = 0;ret[1] = hours;ret[2] = mins;ret[3] = ampmInfo;return ret;
        }
        else{
            if(hrs2.substring(0, 1) == "0"){var hours = hrs2.substring(1, 2)}
            else{var hours = hrs2.substring(0, 2)}
            var mins = hrs2.substring(2, 4)
            ampmInfo = 0;							// for AM
            if(hours == "12") ampmInfo = 1;		// for PM
            if(hrs3.substring(0, 2) == "00") hours = "12"
            if(mins > "59"){ret[0] = 1;return ret;}
            ret[0] = 0;	ret[1] = hours;	ret[2] = mins;ret[3] = ampmInfo;return ret;
        }
    }
    function convert24To12HrWithLeadingZeros(hrs){
        var ampmInfo = 0;var hrs1 = hrs;var hrs2 = hrs;var hrs3 = hrs;
        var len = hrs2.length;var ret = new Array();

        if(len != 4) {ret[0] = 1;return ret;}
        else if(hrs1 > 2400){ret[0] = 1;return ret;}
        else if(hrs1 > 1259){
            hrs1 = hrs1 - 1200
            hrs2 = String(hrs1)
            var len = hrs2.length;
            if(len < 4){hrs2 = "0"+ hrs2}

            // if(hrs2.substring(0, 1) == "0"){var hours = hrs2.substring(1, 2)}
            /*else{*/var hours = hrs2.substring(0, 2)/*}*/
            var mins = hrs2.substring(2, 4)
            ampmInfo = 1;							// for PM
            if(hours == "12") ampmInfo = 0;		// for AM
            if(mins > "59"){ret[0] = 1;return ret;}
            ret[0] = 0;ret[1] = hours;ret[2] = mins;ret[3] = ampmInfo;return ret;
        }
        else{
            //if(hrs2.substring(0, 1) == "0"){var hours = hrs2.substring(1, 2)}
            /*else{*/var hours = hrs2.substring(0, 2)/*}*/
            var mins = hrs2.substring(2, 4)
            ampmInfo = 0;							// for AM
            if(hours == "12") ampmInfo = 1;		// for PM
            if(hrs3.substring(0, 2) == "00") hours = "12"
            if(mins > "59"){ret[0] = 1;return ret;}
            ret[0] = 0;	ret[1] = hours;	ret[2] = mins;ret[3] = ampmInfo;return ret;
        }
    }

    function convertCelsiusToFahrenheit (celsius) { return Math.round(Math.round(celsius*1.8*100)/100 + 32);}
    function convertFahrenheitToCelsius (fahrenheit) {return Math.round(Math.round(((fahrenheit - 32) * 5*100)/9)/100);}

    /*
        trailingDecimals => No. of Trailing Digits needed after Decimal
        Convert the distance from one unit to other.
            nautical mile to km : example convertDistance(200,"km","nm",2);
            nautical mile to miles : example convertDistance(200,"miles","nm",2);
            feet to meters : example convertDistance(200,"meters","feet",2);
            meters to feet : example convertDistance(200,"feet","meters",2);

        Convert the speed from one unit to other.
            knot to kph : example convertDistance(200,"kph","knot",2);
            knot to mph : example convertDistance(200,"mph","knot",2);
    */
    function convertDistance(inputVal,expctedUnit,currentUnit,trailingDecimals){
        if (!trailingDecimals) trailingDecimals = 0;
        var outputVal; inputVal = parseInt(inputVal,10);
        switch(currentUnit){
        case 'nm': {return (expctedUnit=='km'?inputVal*1.852:expctedUnit=='miles'?inputVal*1.15077945:inputVal).toFixed(trailingDecimals); break;}
        case 'knot': {return (expctedUnit=='kph'?inputVal*1.852:expctedUnit=='mph'?inputVal*1.15077945:inputVal).toFixed(trailingDecimals); break;}
        case 'feet': {return (expctedUnit=='meters'?inputVal*0.3048:inputVal).toFixed(trailingDecimals); break;}
        case 'meters': {return (expctedUnit=='feet'?inputVal*3.2808399:inputVal).toFixed(trailingDecimals); break;}
        default: {return inputVal.toFixed(trailingDecimals); break;}
        }
    }

    /* searchEntryInModel(model,field,value)
       RETURN:
          The index of the model is returned if the field and the value exist.
          -1 is returned if the field or the value is not present.   */
    function searchEntryInModel(model,field,value){
        var record;var i;var j;var attribute;
        core.info("CoreHelper| SEARCH ENTRY IN MODEL CALLED MODEL COUNT IS " + model.count + " FIELD is " + field + " VALUE is " + value);
        if(model.count==0){core.error("CoreHelper| MODEL COUNT IS ZERO. EXITING..."); return -1;}
        attribute = model.at(0);
        for(j in attribute){if(j==field){for(i=0;i<model.count;i++){if(value==model.getValue(i,field)){return i;}}}}return -1;
    }

    function validateSeatNo(seatNo){
        if(seatNo.length>4 || seatNo.length<=1) return false;
        else {
            var alphabet = seatNo.substr(seatNo.length-1,1);
            if(alphabet.match(/[a-zA-z]/g)==null) return false;

            for(var i=0;i<seatNo.length-1;i++){ if (seatNo[i].match(/[0-9]/g)==null) return false;}
        }
        return true;
    }

    function validateSeatNoForApps(seatNo){
        if(seatNo.toLowerCase()==pif.getSeatNumber()) return false;
        return validateSeatNo(seatNo);
    }

    function setTimeout(callback,interval,params){
        Qt.createQmlObject("import QtQuick 1.1;Timer{id:stTimer;onTriggered:{eval("+callback+"(["+params+"]));stTimer.destroy();} interval:"+interval+";Component.onCompleted:stTimer.start();}",core);
    }

    function getDate(milliSeconds,yearFormat,leadingZero){
        if(milliSeconds==0 ||milliSeconds<0){core.error("CoreHelper| milliSeconds is invalid. EXITING..."); return -1;}
        if(yearFormat==undefined)yearFormat="yyyy";
        if(leadingZero==undefined)leadingZero=false;
        var d1= new Date(milliSeconds*1000);
        var date = d1.getDate();
        var month = d1.getMonth()+1;
        var hrs = d1.getHours();
        var mins = d1.getMinutes();
        var secs = d1.getSeconds();
        var year;
        switch(yearFormat){
        case "yyyy":
            year = d1.getFullYear();
            break;
        case "yy":
            year = d1.getFullYear().toString().substr(2,2);
            break;
        }
        if(leadingZero){
            if(date<10) date="0"+date;
            if(month<10) month="0"+month;
            if(hrs<10) hrs="0"+hrs;
            if(mins<10) mins="0"+mins;
            if(secs<10) secs="0"+secs;
        }
        return {"date":date,"month":month,"year":year,"hrs":hrs,"mins":mins,"secs":secs};
    }

    function convertSizeToMB(fsize,returnAbsValue){
        if(returnAbsValue == undefined) returnAbsValue =false;
        if(isNaN(fsize)) return 0;
        if(!returnAbsValue){fsize = (fsize/1048576).toFixed(2);}
        else {fsize = (fsize/1048576)}
        return fsize;
    }

    function printContent(data){
        for (var key in data) {
            if (typeof(data[key]) == "object" && data[key] != null) {
                console.log(key + " : " + data[key]);
                printContent(data[key]);
            }else{
                console.log(key + " : " + data[key]);
            }
        }
    }

    function parseXMLQuery(xml_str,query_str){
        if(xml_str=="" || query_str=="") return;
        xmlParser.text=xml_str;
        var str = xmlParser.query(query_str);
        str = str.replace(/\n$/, "")
        console.log("CoreHelper | Parsed string is "+str+"q");
        return str;
    }
}
