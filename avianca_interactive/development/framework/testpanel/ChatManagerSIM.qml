import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    id: chatManager;
    property string chatServerIp: "172.17.0.24";
    property string gatewayIp: "172.17.0.24";
    property string gatewayPort: "57777";
    property string listenPort: "57777";
    property int __errorCode:0;      // 1=> file not found
    property int __TotalPublicSessions:0;
    property int __TotalPrivateSessions: 0;
    property int __lastSessionId:0;
    property string __currentSessionId:"";
    property string __seatNo:"";
    property bool __joinChatError:false;
    property bool __skipDataPopulation:false;
    signal chatEvent(variant event, variant eventInfo);

    Connections {
        target: core.pif
        onOpenFlightChanged:{
            core.info("ChatManagerSIM.qml | onOpenFlightChanged called "+openFlight)
            if (openFlight) return;
            __initialiseArrData();
            __currentSessionId="";
            __TotalPublicSessions=0;
            __TotalPrivateSessions=0;
            __lastSessionId=__TotalPrivateSessions - 1;
            __seatNo="";
            __skipDataPopulation=false;
        }
    }

    Connections{
        target: core.viewController.testPanel?core.viewController.testPanel:null;
        onSigTestPanelMessage:{
            core.info("ChatManagerSIM.qml | onSigTestPanelMessage | reason: "+reason);
            if (reason!="SeatChat") return false;
            if (parameter1=="addbuddy") __addBuddy(parameter2,parameter3.toUpperCase());
            else if (parameter1=="removebuddy") __removeBuddy(parameter2,parameter3.toUpperCase());
            else if (parameter1=="chatmessage") __addSeatChatMessage(parameter3.toUpperCase(),parameter4);
            else if (parameter1=="setnickname") __updateSeatInfo("alias",parameter2.toUpperCase(),parameter3);
            else if (parameter1=="setstatus") __updateSeatInfo("status",parameter2.toUpperCase(),parameter3);
            else if (parameter1=="setstate") __updateSeatInfo("state",parameter2.toUpperCase(),parameter3);
            else if (parameter1=="inviteAccepted") __chatInvitationAccepted(parameter3.toUpperCase());
            else if (parameter1=="inviteDeclined") __chatInvitationDeclined(parameter3.toUpperCase());
            else if (parameter1=="allInviteAccepted") __allChatInvitationAccepted();
            else if (parameter1=="allInviteDeclined") __allChatInvitationDeclined();
            else if (parameter1=="sendInvite") __receivedInvitation(parameter3.toUpperCase());
            else core.error("ChatManagerSIM.qml | Unknown Request");
        }
    }

    function __removeBuddy(sessionid,seatno) {
        seatno = seatno.toUpperCase();
        if (!__skipDataPopulation || !seatno || Store.PublicSessionsArr[sessionid]==undefined) return false;
        var sptr = Store.AvailableSeatsArr[seatno];
        var ptr = Store.PublicSessionsArr[sessionid]["buddies"];
        for (var i=0;i<ptr.length;i++)
            if (ptr[i].seatnumber==seatno) {
                ptr.splice(i,1);
                core.info("ptr.length: "+ptr.length);
                core.info("buddies.length: "+Store.PublicSessionsArr[sessionid]["buddies"].length);
                __setEventInfo(4,{"sessionid":sessionid,"seatnumber":sptr.seatnumber,"seatname":sptr.seatname});
                return true;
            }
        return true;
    }

    function __addBuddy(sessionid,seatno) {
        seatno = seatno.toUpperCase();
        if (!__skipDataPopulation || !seatno || Store.PublicSessionsArr[sessionid]==undefined) return false;
        if (Store.AvailableSeatsArr[seatno]==undefined) Store.AvailableSeatsArr[seatno] = {"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
        var sptr = Store.AvailableSeatsArr[seatno];
        var ptr = Store.PublicSessionsArr[sessionid]["buddies"];
        for (var i=0;i<ptr.length;i++) if (ptr[i].seatnumber==seatno) return false;
        ptr.push(sptr);
        __setEventInfo(3,{"sessionid":sessionid,"seatnumber":sptr.seatnumber,"seatname":sptr.seatname});
        return true;
    }

    function getUsrSessions(to,from){
        return Store.PrivateSessionsArr;
    }

    function __chatInvitationAccepted (seatno){
        core.info("ChatManagerSIM.qml | __chatInvitationAccepted | seatno: "+seatno);
        seatno = seatno.toUpperCase();
        if (!Store.SendInviteArr[seatno]){
            core.info("ChatManagerSIM.qml | __chatInvitationAccepted | Invitations for Seatno :"+seatno+ " does not exist: ");
            return}
        var sendInvite = Store.SendInviteArr[seatno].split(",");
        for(var i=0;i<sendInvite.length;i++){
            if(!Store.PrivateSessionsArr[sendInvite[i]]){
                core.info("ChatManagerSIM.qml | __chatInvitationAccepted | Session does not exist: "+sendInvite[i]);
                continue;
            }
            if (!Store.AvailableSeatsArr[seatno])
                Store.AvailableSeatsArr[seatno]={"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
            Store.PrivateSessionsArr[sendInvite[i]]["buddies"].push(Store.AvailableSeatsArr[seatno]);
            core.info("ChatManagerSIM.qml | __chatInvitationAccepted | Sessions: "+sendInvite[i]+" Seatno: "+seatno);
            __setEventInfo(1,{"sessionid":sendInvite[i],"seatnumber":seatno});
            __setEventInfo(3,{"sessionid":sendInvite[i],"seatnumber":seatno,"seatname":""});
        }
        delete Store.SendInviteArr[seatno];
    }
    function __allChatInvitationAccepted (){
        core.info("ChatManagerSIM.qml | __allChatInvitationAccepted called ");
        var sendInvite;
        for (var i in Store.SendInviteArr){
            sendInvite = Store.SendInviteArr[i].split(",");
            core.info("ChatManagerSIM.qml | __allChatInvitationAccepted | Sessions:"+Store.SendInviteArr[i]+"Seatnumber"+i);
            for (var j=0; j<sendInvite.length;j++){
                if(!Store.PrivateSessionsArr[sendInvite[j]]){
                    core.info("ChatManagerSIM.qml | __allChatInvitationAccepted | Session does not exist: "+sendInvite[j]);
                    continue;
                }
                if (!Store.AvailableSeatsArr[i])
                    Store.AvailableSeatsArr[i]={"seatnumber":i,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
                Store.PrivateSessionsArr[sendInvite[j]]["buddies"].push(Store.AvailableSeatsArr[i]);
                core.info("ChatManagerSIM.qml | __allChatInvitationAccepted | Sessions: "+sendInvite[j]+" Seatno: "+i);
                __setEventInfo(1,{"sessionid":sendInvite[j],"seatnumber":i});
                __setEventInfo(3,{"sessionid":sendInvite[j],"seatnumber":i,"seatname":""});
            }
        }
        Store.SendInviteArr = [] ;
    }

    function __chatInvitationDeclined (seatno){
        core.info("ChatManagerSIM.qml | __chatInvitationDeclined called | Seatno: "+seatno);
        seatno = seatno.toUpperCase();
        if (!Store.SendInviteArr[seatno]){
            core.info("ChatManagerSIM.qml | __chatInvitationDeclined called | Invitation for Seatno: "+seatno+ "Does not exist: ");
            return;
        }
        var sendInvite = Store.SendInviteArr[seatno].split(",");
        for (var i=0;i<sendInvite.length;i++){
            if(!Store.PrivateSessionsArr[sendInvite[i]]){
                core.info("ChatManagerSIM.qml | sendInvitation called | Session does not exist: "+sendInvite[i]);continue;
            }
            if (!Store.AvailableSeatsArr[seatno])
                Store.AvailableSeatsArr[seatno]={"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
            __setEventInfo(19,{"sessionid":sendInvite[i],"seatnumber":seatno});
            core.info("ChatManagerSIM.qml | __chatInvitationDeclined | Sessions: "+sendInvite[i])
        }
        delete Store.SendInviteArr[seatno];
    }

    function __allChatInvitationDeclined (){
        core.info("ChatManagerSIM.qml | __allChatInvitationDeclined called ");
        var sendInvite;
        for (var i in Store.SendInviteArr){
            sendInvite = Store.SendInviteArr[i].split(",");
            for (var j=0;j<sendInvite.length;j++){
                if(!Store.PrivateSessionsArr[sendInvite[j]]){
                    core.info("ChatManagerSIM.qml | __allChatInvitationDeclined | Session does not exist: "+sendInvite[j]);continue;
                }
                if (!Store.AvailableSeatsArr[i])
                    Store.AvailableSeatsArr[i]={"seatnumber":i,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
                __setEventInfo(19,{"sessionid":sendInvite[j],"seatnumber":i});
            }
            core.info("ChatManagerSIM.qml | __chatInvitationDeclined | Sessions: "+sendInvite[j]+" Seatno: "+i)
        }
        Store.SendInviteArr = [] ;
    }

    function __receivedInvitation (seatno){
        core.info("ChatManagerSIM.qml | __receivedInvitation | Invitation form Seatno :"+seatno)
        if(Store.AvailableSeatsArr[__seatNo] && Store.AvailableSeatsArr[__seatNo].blockallinvites=="1"){
            core.info("ChatManagerSIM.qml | __receivedInvitation | All invitations blocked");
            return;
        }
        if(!Store.AvailableSeatsArr[seatno.toUpperCase()]){
            core.info("ChatManagerSIM.qml | __receivedInvitation | Seatno not present | Adding Seatno"+seatno)
            Store.AvailableSeatsArr[seatno.toUpperCase()]={"seatnumber":seatno.toUpperCase(),"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
        }
        if(Store.AvailableSeatsArr[seatno].blockstatus=="1"){
            core.info("ChatManagerSIM.qml | __receivedInvitation | Seat Blocked | Seatno :"+seatno);
            return;
        }
        createNewSession();
        var sessionid = Store.PrivateSessionsArr["j"+__lastSessionId].sessionid
        core.info("ChatManagerSIM.qml |__receivedInvitation | Sessionid :"+sessionid);
        if(!Store.ReceivedInviteArr[seatno]){
            Store.ReceivedInviteArr[seatno] = sessionid;
        }else{
            var receivedInvite = Store.ReceivedInviteArr[seatno].split(",");
            if (receivedInvite.indexOf(sessionid)==-1) receivedInvite.push(sessionid);
            Store.ReceivedInviteArr[seatno] = receivedInvite.join(",");
        }
        __setEventInfo(0,{"sessionid":sessionid,"seatnumber":seatno,"seatname":""})
    }

    function acceptInvitation (sessionid,seatno){
        core.info("ChatManagerSIM.qml | acceptInvitation called | Sessionid :"+sessionid+"Seatno :"+seatno);
        seatno = seatno.toUpperCase();
        if(!Store.ReceivedInviteArr[seatno])
            core.info("ChatManagerSIM.qml | __chatInvitationAccepted | Seat number does not exist: "+seatno)
        var receivedInvite = Store.ReceivedInviteArr[seatno].split(",")
        var index = receivedInvite.indexOf(sessionid)
        if (index == -1){
            core.info("ChatManagerSIM.qml | acceptInvitation | Sessionid Not present "+sessionid);
            return;
        }
        if (!Store.AvailableSeatsArr[seatno])
            Store.AvailableSeatsArr[seatno]={"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
        Store.PrivateSessionsArr[sessionid]["buddies"].push(Store.AvailableSeatsArr[seatno]);
        receivedInvite.splice(index,1);
        Store.ReceivedInviteArr[seatno] = receivedInvite.join(",");
        __setEventInfo(10,{"sessionid":sessionid})
    }

    function declineInvitation (sessionid,seatno){
        core.info("ChatManagerSIM.qml | declineInvitation called | Sessionid :"+sessionid+"Seatno :"+seatno);
        seatno = seatno.toUpperCase();
        if(!Store.ReceivedInviteArr[seatno])
            core.info("ChatManagerSIM.qml | declineInvitation | Seat number does not exist: "+seatno);
        var receivedInvite = Store.ReceivedInviteArr[seatno].split(",")
        var index = receivedInvite.indexOf(sessionid)
        if (index == -1){
            core.info("ChatManagerSIM.qml | declineInvitation | Sessionid Not present "+sessionid);
            return;
        }
        if(!Store.PrivateSessionsArr[sessionid]){
            core.info("ChatManagerSIM.qml | declineInvitation | Session does not exist: "+sessionid);
            continue;
        }
        if (!Store.AvailableSeatsArr[seatno])
            Store.AvailableSeatsArr[seatno]={"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
        core.info("ChatManagerSIM.qml | declineInvitation | Sessions: "+sessionid+" Seatno: "+seatno);
        receivedInvite.splice(index,1);
        delete Store.PrivateSessionsArr[sessionid];
        __TotalPrivateSessions = __TotalPrivateSessions - 1;
        Store.ReceivedInviteArr[seatno] = receivedInvite.join(",");
        __setEventInfo(7,{"sessionid":sessionid});
        __setEventInfo(10,{"sessionid":sessionid})
    }

    function sendInvitation (sessionid,seatno){
        core.info("ChatManagerSIM.qml | sendInvitation called | Sessionid :"+sessionid+" Seatno: "+seatno);
        if(!Store.PrivateSessionsArr[sessionid]){
            core.info("ChatManagerSIM.qml | sendInvitation | Session does not exist: "+sessionid);return;}
        if (!Store.AvailableSeatsArr[seatno.toUpperCase()])
            Store.AvailableSeatsArr[seatno.toUpperCase()]={"seatnumber":seatno.toUpperCase(),"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
        if(!Store.SendInviteArr[seatno]){
            Store.SendInviteArr[seatno] = sessionid;
        }else{
            var sendInvite = Store.SendInviteArr[seatno].split(",");
            if (sendInvite.indexOf(sessionid)==-1) sendInvite.push(sessionid);
            Store.SendInviteArr[seatno] = sendInvite.join(",");
        }
    }


    function getPublicSessions (to,from) {
        return Store.PublicSessionsArr;
    }

    function getBuddyList (sessionid,to,from) {
        return Store.PublicSessionsArr[sessionid]?Store.PublicSessionsArr[sessionid]["buddies"]:Store.PrivateSessionsArr[sessionid]?Store.PrivateSessionsArr[sessionid]["buddies"]:[];
    }

    function getSessionParticipantCount(serverSessionId,sessionid){return 1000;}

    function getSessionParticipantList(serverSessionid, sessionid, to,from){
        return Store.PublicSessionsArr[sessionid]?Store.PublicSessionsArr[sessionid]["buddies"]:Store.PrivateSessionsArr[sessionid]?Store.PrivateSessionsArr[sessionid]["buddies"]:[];
    }

    function getTotalBuddyCount (sessionid) {
        if (!Store.PrivateSessionsArr[sessionid]){
            core.info("ChatManagerSIM.qml | getTotalBuddyCount | Session does not exist: "+sessionid);
            return 0;
        }
        core.info("ChatManagerSIM.qml | getTotalBuddyCount | Sessionid :"+sessionid+" Number of buddies :"+Store.PrivateSessionsArr[sessionid]["buddies"].length)
        return Store.PrivateSessionsArr[sessionid]["buddies"].length;
    }

    function getHashValue(arrayPointer,arrayKey) {
        return arrayPointer[arrayKey];
    }

    function getBlockedUserList (to,from) {
        return Store.BlockedSeatsArr;
    }

    function getTotalMessageCount (sessionid) {
        return Store.ChatHistoryArr[sessionid].length;
    }

    function sendChatMsg (sessionid,message) {
        return __addChatMessageToSeat(sessionid,__seatNo,message);
    }

    function __addSeatChatMessage (seatno,message) {
        core.info("ChatManagerSIM.qml | __addSeatChatMessage | Seatno :"+seatno+" Message :"+message)
        var sptr
        var dptr
        for (var i in Store.PrivateSessionsArr){
            sptr = Store.PrivateSessionsArr[i]["buddies"]
            core.info("ChatManagerSIM.qml | __addSeatChatMessage called | Number of Buddies :"+sptr.length+" in Session : "+i)
            for (var j=0; j<sptr.length;j++){
                if(sptr[j].seatnumber == seatno){
                    core.info("ChatManagerSIM.qml | __addSeatChatMessage called | This is entered Seatno "+seatno+" Session"+i);
                    __addChatMessage(i,seatno.toUpperCase(),message)
                    break;
                }
            }
        }
        for (var k in Store.PublicSessionsArr){
            sptr = Store.PublicSessionsArr[k]["buddies"]
            core.info("ChatManagerSIM.qml | __addSeatChatMessage called | Number of Buddies :"+sptr.length+" in Session : "+k)
            for (var l=0; l<sptr.length;l++){
                if(sptr[l].seatnumber == seatno){
                    core.info("ChatManagerSIM.qml | __addSeatChatMessage called | This is entered Seatno "+seatno+" Session"+k);
                    __addChatMessage(k,seatno.toUpperCase(),message)
                    break;
                }
            }
        }
    }

    function __addChatMessage (sessionid,seatno,message) {
        var ptr = Store.BlockedSeatsArr;
        for (var i=0;i<ptr.length;i++) if (ptr[i].seatnumber==seatno) return false;
        Store.ChatHistoryArr[sessionid].push({"seatnumber":seatno,"time":new Date(),"seatname":Store.AvailableSeatsArr[seatno].seatname,"txtmessage":message});
        Store.CurrentChatArr.push(Store.ChatHistoryArr[sessionid].length-1);
        if(!Store.tmpArr) Store.tmpArr = []
        Store.tmpArr.push({"sessionid":sessionid,"seatnumber":seatno,"message":message})
        addChatMessageTimer.restart();
    }

       function __addChatMessageToSeat (sessionid,seatno,message) {
        var ptr = Store.BlockedSeatsArr;
        for (var i=0;i<ptr.length;i++) if (ptr[i].seatnumber==seatno) return false;
        Store.ChatHistoryArr[sessionid].push({"seatnumber":seatno,"time":new Date(),"seatname":Store.AvailableSeatsArr[seatno].seatname,"txtmessage":message});
        Store.CurrentChatArr.push(Store.ChatHistoryArr[sessionid].length-1);
    }

    function __addChatMessageTriggered () {
        if (!Store.tmpArr) return;
        var ptr = Store.tmpArr;
        var sptr
        for (var i=0;i<ptr.length;i++){
            core.info("__addChatMessageTriggered"+ptr[i].sessionid)
            sptr = Store.ChatHistoryArr[ptr[i].sessionid][Store.ChatHistoryArr[ptr[i].sessionid].length-1];
            __setEventInfo(2,{"sessionid":ptr[i].sessionid,"seatnumber":ptr[i].seatnumber,"seatname":sptr.seatname,"txtmessage":ptr[i].message,"time":sptr.time});     // Chat Message Received
        }
        Store.tmpArr = [];
    }

   function requestSessionHistory(serversessionid,sessionid){
        core.info("ChatManagerSIM.qml | requestSessionHistory | sessionid : "+sessionid)
        requestSessionHistoryTimer.restart();
        Store.tmpSessionArr = "";
        Store.tmpSessionArr = sessionid
    }
    function __requestSessionHistorytriggered(){
        core.info("ChatManagerSIM.qml | __requestSessionHistorytriggered ")
        if(Store.tmpSessionArr=="") return;
        console.log("Store.tmpSessionArr : "+Store.tmpSessionArr)
         __setEventInfo(32,{"sessionid":Store.tmpSessionArr});
        Store.tmpSessionArr="";

    }

    function getSessionHistory(serversessionid,sessionid){
        core.info("ChatManagerSIM.qml | getSessionHistory| sessionid "+sessionid);
        Store.CurrentChatArr = [];
        if (!Store.ChatHistoryArr[sessionid]) return Store.CurrentChatArr;
        var ptr = Store.ChatHistoryArr[sessionid];
        for (var i=0;i<ptr.length;i++) Store.CurrentChatArr.push(ptr[i]);
        return Store.CurrentChatArr;

    }

    function blockSeat(seatno) {
        seatno = seatno.toUpperCase()
        core.info("ChatManagerSIM.qml | blockSeat called: "+seatno);
        var ptr = Store.BlockedSeatsArr;
        for (var i=0;i<ptr.length;i++) if (ptr[i].seatnumber==seatno) return false;
        ptr.push({"seatnumber":seatno});
        Store.AvailableSeatsArr[seatno]["blockstatus"]="1";
        if (!Store.blocktmp) Store.blocktmp=[];
        Store.blocktmp.push(seatno);
        blockSeatTimer.restart();
    }

    function __blockSeatTriggered() {
        if (!Store.blocktmp) return;
        var ptr = Store.blocktmp;
        for (var i=0;i<ptr.length;i++) __setEventInfo(15,{"seatnumber":ptr[i]});
        Store.blocktmp = []
    }

    function unblockSeat(seatno) {
        seatno = seatno.toUpperCase();
        core.info("ChatManagerSIM.qml | unblockSeat called: "+seatno);
        var ptr = Store.BlockedSeatsArr;
        for (var i=0;i<ptr.length;i++)
            if (ptr[i].seatnumber==seatno) {
                ptr.splice(i,1);
                Store.AvailableSeatsArr[seatno]["blockstatus"]="0";
                if (!Store.unblocktmp) Store.unblocktmp=[];
                Store.unblocktmp.push(seatno);
                unblockSeatTimer.restart();
                return true;
            }
        return false;
    }

    function __unblockSeatTriggered(){
        if (!Store.unblocktmp) return;
        var ptr = Store.unblocktmp;
        for (var i=0;i<ptr.length;i++) __setEventInfo(16,{"seatnumber":ptr[i]});
        Store.unblocktmp = [];
    }

    function setAlias(aliasname){
        core.info("ChatManagerSIM.qml | setAlias called: "+aliasname);
        __updateSeatInfo("alias",__seatNo,aliasname);
    }

    function setUserState(seatState){
        core.info("ChatManagerSIM.qml | setUserState called: "+seatState);
        __updateSeatInfo("state",__seatNo,seatState);
    }

    function setUserStatus(status){
        core.info("ChatManagerSIM.qml | setUserStatus called: "+status);
        __updateSeatInfo("status",__seatNo,status);
    }

    function blockInvites (){
        core.info("ChatManagerSIM.qml | blockInvites called ");
        __updateSeatInfo("allinvites",__seatNo,"1");
    }

    function unblockInvites (){
        core.info("ChatManagerSIM.qml | unblockInvites called ");
        __updateSeatInfo("allinvites",__seatNo,"0");
    }

    function __updateSeatInfo(purpose,seatno,value){
        if (Store.AvailableSeatsArr[seatno]==undefined) Store.AvailableSeatsArr[seatno] = {"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
        if (purpose=="alias") Store.AvailableSeatsArr[seatno].seatname=value;
        else if (purpose=="state") Store.AvailableSeatsArr[seatno].state=value;
        else if (purpose=="status") Store.AvailableSeatsArr[seatno].status=value;
        else if (purpose=="allinvites")Store.AvailableSeatsArr[seatno].blockallinvites=value;
        if (!Store.tmpSeatInfo) Store.tmpSeatInfo=[];
        Store.tmpSeatInfo.push(seatno);
        updateSeatInfoTimer.restart();
    }

    function __updateSeatInfoTriggered(){
        if (!Store.tmpSeatInfo) return;
        var ptr = Store.tmpSeatInfo; var sptr;
        for (var i=0;i<ptr.length;i++) {
            sptr = Store.AvailableSeatsArr[ptr[i]];
            __setEventInfo(18,{"sessionid":__currentSessionId,"seatnumber":sptr.seatnumber,"seatname":sptr.seatname,"state":sptr.state,"status":sptr.status,"blockallinvites":sptr.blockallinvites});
        }
        Store.tmpSeatInfo = [];
    }

    function leavePublicChat (sessionid) {
        Store.ChatHistoryArr[sessionid]=[];
        if (sessionid==__currentSessionId) {
            Store.CurrentChatArr = [];
            __currentSessionId="";
        }
    }

    function exitChatSession (sessionid) { // for private chat
        core.info("ChatManagerSIM.qml | exitChatSession called: "+sessionid);
        if(!Store.PrivateSessionsArr[sessionid])
            core.info("ChatManagerSIM.qml | exitChatSession called | Session not present");
        else{
            delete Store.PrivateSessionsArr[sessionid];
            delete Store.ChatHistoryArr[sessionid];
            __TotalPrivateSessions = __TotalPrivateSessions - 1;
        }
        exitChatSessionTimer.sessionid = sessionid;
        exitChatSessionTimer.restart()
    }

    function __exitChatSessionTriggered (){
        if (exitChatSessionTimer.sessionid=="") return false;
        __setEventInfo(7,{"sessionid":exitChatSessionTimer.sessionid});
    }


    function exitChat() {
        core.info("ChatManagerSIM.qml | exitChat called");
        var ptr = Store.ChatHistoryArr;
        Store.CurrentChatArr = [];
        for (var i in ptr) ptr[i]=[];
        var sptr=Store.PrivateSessionsArr;
        for (var j in sptr){
            delete Store.ChatHistoryArr[j];
        }
        Store.PrivateSessionsArr = [];
        __currentSessionId="";
        __TotalPrivateSessions = 0;
        __lastSessionId = __TotalPrivateSessions - 1;
    }

    function setActive(seatno) {
        __seatNo = seatno.toUpperCase();
        appActiveTimer.restart();
        return true;
    }

    function __setActiveTriggered() {
        if (!__skipDataPopulation) {
            __initialiseArrData();
            __readSimData();
        } else __populateSimData();
    }

    function createNewSession(){
        core.info("ChatManagerSIM.qml | createNewSession | Last Session id: "+__lastSessionId);
        __lastSessionId = __lastSessionId +1;
        Store.PrivateSessionsArr["j"+__lastSessionId] ={
            "sessionid":"j"+__lastSessionId,
            "name":"",
            "buddies":[]
        }
        Store.ChatHistoryArr["j"+__lastSessionId] = [];
        __TotalPrivateSessions = __TotalPrivateSessions +1;
        createSessionTimer.restart();
        return "j"+__lastSessionId;
    }
    function __createNewSessionTriggered(){
        __setEventInfo(6,{"sessionid":Store.PrivateSessionsArr["j"+__lastSessionId].sessionid});
        __setEventInfo(20,{"sessionid":Store.PrivateSessionsArr["j"+__lastSessionId].sessionid})
    }

    function refreshAllChatUsers (){
        refreshAllChatUsersTimer.restart();

    }
    function __refreshAllChatUsersOnTrigerred (){
        __setEventInfo(25,{});
    }

    function getAllChatUsers (){
        return Store.AvailableSeatsArr;
    }


    function joinPublicChat(sessionid) {
        if (Store.PublicSessionsArr[sessionid]==undefined) return false;
        __currentSessionId = sessionid;
        joinPublicChatTimer.restart();
        return true;
    }

    function __joinPublicChatTriggered() {
        if (__joinChatError) __setEventInfo(22,{"sessionid":__currentSessionId});
        else __setEventInfo(21,{"sessionid":__currentSessionId,"sessionname":Store.PublicSessionsArr[__currentSessionId].name});
        __setEventInfo(10,{"sessionid":__currentSessionId});
    }

    function __setEventInfo(eventid,eventlist) {
        for (var i in Store.EventInfoArr) Store.EventInfoArr[i]=(eventlist[i]!=undefined)?eventlist[i]:"";
        var s=[];
        for (var i in Store.EventInfoArr) s.push(i+": "+Store.EventInfoArr[i]);
        core.info("ChatManagerSIM.qml | __setEventInfo | eventid: "+eventid+" EventInfo: "+s.join(','));
        chatEvent(eventid,Store.EventInfoArr);
    }

    function __initialiseArrData() {
        Store.PublicSessionsArr = [];
        Store.PrivateSessionsArr = [];
        Store.AvailableSeatsArr = [];
        Store.ChatHistoryArr = [];
        Store.CurrentChatArr = [];
        Store.EventInfoArr = {"sessionid":"","seatnumber":"","seatname":"","state":"","status":"","txtmessage":"","time":"","sessionname":"","blockallinvites":""};
        Store.BlockedSeatsArr = [];
        Store.SendInviteArr = [];
        Store.ReceivedInviteArr = [];
    }

    function __readSimData () {
        fileRequest.source = "";
        fileRequest.source = core.settings.intPath+"config/seatChatPC.json";
    }

    function __populateSimData () {
        core.info("ChatManagerSIM.qml | chatManagerSIM | populateSimData");
        if (!__skipDataPopulation) __populateSessions();
        // All sessions retrieved
        __setEventInfo(8,{});

        if (!__skipDataPopulation) __populateAvailableBuddies();
        // Current Seat's info
        var sptr = Store.AvailableSeatsArr[__seatNo];
        __setEventInfo(18,{"seatnumber":sptr.seatnumber,"seatname":sptr.seatname,"state":sptr.state,"status":sptr.status,"blockallinvites":sptr.blockallinvites,"blockstatus":sptr.blockstatus});

        //All Session retrieved:Just the event
        __setEventInfo(8,{});

        // All Buddies refreshed
        __setEventInfo(9,{});

        // Buddies per session
        if (!__skipDataPopulation) __populateBuddiesPerSession();
        for (var i=0; i<__TotalPublicSessions; i++) __setEventInfo(10,{"sessionid":"i"+i});
        for (var j=0; j<__TotalPrivateSessions;j++) __setEventInfo(10,{"sessionid":"j"+j})

        // blocked seats list
        __setEventInfo(17,{});
    }

    function __populateAvailableBuddies () {
        var ptr= fileRequest.value; var sptr;
        Store.BlockedSeatsArr=[];
        Store.AvailableSeatsArr = [];
        if (!ptr.AvailableSeats || !(ptr.AvailableSeats.length>0)) return false;
        ptr= fileRequest.value.AvailableSeats;
        for (var i=0; i<ptr.length; i++) {
            if (!ptr[i].seatnumber) continue;
            sptr = ptr[i];
            Store.AvailableSeatsArr[sptr.seatnumber.toUpperCase()] = {
                "seatnumber":sptr.seatnumber.toUpperCase(),
                "seatname":sptr.seatname?sptr.seatname:"",
                                          "state":sptr.state?sptr.state:"",
                                                              "status":sptr.status?sptr.status:"",
                                                                                    "blockallinvites":sptr.blockallinvites?sptr.blockallinvites:"",
                                                                                                                            "blockstatus":sptr.blockstatus?sptr.blockstatus:"0"
            }
            if (sptr.blockstatus) Store.BlockedSeatsArr.push({"seatnumber":sptr.seatnumber.toUpperCase()});
        }
        if (Store.AvailableSeatsArr[__seatNo]==undefined) Store.AvailableSeatsArr[__seatNo] = {"seatnumber":__seatNo,"seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"};
        core.info("ChatManagerSIM.qml | Available Seats Length: "+ptr.length);
        core.info("ChatManagerSIM.qml | Blocked Seats length: "+Store.BlockedSeatsArr.length);
    }

    function __populateBuddiesPerSession () {
        var ptr= fileRequest.value; var sptr; var dptr;
        for (var i=0; i<__TotalPublicSessions; i++) {
            if (ptr.PublicChatRooms && ptr.PublicChatRooms[i] && ptr.PublicChatRooms[i].seatnumbers) {
                sptr = ptr.PublicChatRooms[i].seatnumbers;
                for (var s=0; s<sptr.length; s++) {
                    if (Store.AvailableSeatsArr[sptr[s]]==undefined) Store.AvailableSeatsArr[sptr[s]] = {
                            "seatnumber":sptr[s].toUpperCase(),
                            "seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"}
                    Store.PublicSessionsArr["i"+i].buddies.push(Store.AvailableSeatsArr[sptr[s]]);

                }
            }
        }
        for (var j=0; j< __TotalPrivateSessions;j++){
            if (ptr.PrivateChatRooms && ptr.PrivateChatRooms[j] && ptr.PrivateChatRooms[j].seatnumbers){
                dptr = ptr.PrivateChatRooms[j].seatnumbers;
                for (var k=0; k<dptr.length;k++){
                    if (Store.AvailableSeatsArr[dptr[k]]==undefined) Store.AvailableSeatsArr[dptr[k]] = {
                            "seatnumber":dptr[k].toUpperCase(),
                            "seatname":"","state":"","status":"","blockallinvites":"","blockstatus":"0"}
                    Store.PrivateSessionsArr["j"+j].buddies.push(Store.AvailableSeatsArr[dptr[k]]);
                }
            }
        }
    }

    function __populateSessions () {
        var ptr= fileRequest.value; var sptr;
        Store.PublicSessionsArr = [];
        Store.PrivateSessionsArr = [];
        Store.ChatHistoryArr = [];
        if (ptr.PublicChatCount) __TotalPublicSessions=ptr.PublicChatCount;
        else if (ptr.PublicChatRooms && ptr.PublicChatRooms.length) __TotalPublicSessions=ptr.PublicChatRooms.length;
        for (var i=0; i<__TotalPublicSessions; i++) {
            Store.ChatHistoryArr["i"+i] = [];
            Store.PublicSessionsArr["i"+i] = {
                "sessionid":"i"+i,
                "name": ptr.PublicChatRooms && ptr.PublicChatRooms[i] && ptr.PublicChatRooms[i].name?ptr.PublicChatRooms[i].name:"Chat "+i,
                                                                                                      "buddies":[]
            }
        }
        core.info("ChatManagerSIM.qml | __populateSessions | __TotalPublicSessions: "+__TotalPublicSessions);
        if (ptr.PrivateChatCount) __TotalPrivateSessions=ptr.PrivateChatCount;
        else if (ptr.PrivateChatRooms && ptr.PrivateChatRooms.length) __TotalPrivateSessions=ptr.PrivateChatRooms.length;
        for (var j=0; j<__TotalPrivateSessions; j++){
            Store.ChatHistoryArr["j"+j] = [];
            Store.PrivateSessionsArr["j"+j] = {
                "sessionid":"j"+j,
                "name":ptr.PrivateChatRooms && ptr.PrivateChatRooms[j] && ptr.PrivateChatRooms[j].name?ptr.PrivateChatRooms[j].name:"",
                                                                                                        "buddies":[]
            }
        }
        __lastSessionId = __TotalPrivateSessions -1 ;
        core.info("ChatManagerSIM.qml | __populateSessions | __TotalPrivateSessions: "+__TotalPrivateSessions);
    }

    Component.onCompleted: {
        core.info("ChatManagerSIM.qml | chatManagerSIM | Component loaded: "+core.settings.intPath);
        __initialiseArrData();
    }
    Configuration{
        id: fileRequest;
        onStatusChanged:{
            __errorCode=0;
            if(status==Pif.Ready && fileRequest.source!=''){
                core.info("ChatManagerSIM.qml | chatManager | Configuration onStatusChanged | Data Ready");
                __populateSimData();
                __skipDataPopulation=true;
            }else if(status==Pif.Error && fileRequest.source!=''){
                core.info("ChatManagerSIM.qml | chatManager | Configuration onStatusChanged | ErrorStatus: "+status+" ERROR reading file: "+source);
                __errorCode=1;
            }
        }
    }
    Timer { id:appActiveTimer;interval:50; onTriggered: __setActiveTriggered();}
    Timer { id:joinPublicChatTimer;interval:100; onTriggered: __joinPublicChatTriggered();}
    Timer { id:addChatMessageTimer;interval:50;onTriggered: __addChatMessageTriggered();}
    Timer { id:blockSeatTimer;interval:100; onTriggered: __blockSeatTriggered();}
    Timer { id:unblockSeatTimer;interval:100; onTriggered: __unblockSeatTriggered();}
    Timer { id:updateSeatInfoTimer;interval:100; onTriggered: __updateSeatInfoTriggered();}
    Timer { id:createSessionTimer;interval:50;onTriggered: __createNewSessionTriggered();}
    Timer { id:getSessionTimer;interval:50;onTriggered: __getUsrSessionsTriggered();}
    Timer { id:refreshAllChatUsersTimer;interval:50;onTriggered: __refreshAllChatUsersOnTrigerred();}
    Timer { id:exitChatSessionTimer;interval:50; property string sessionid:""; onTriggered: __exitChatSessionTriggered();}
    Timer { id:requestSessionHistoryTimer;interval:50; onTriggered: __requestSessionHistorytriggered();}
}
