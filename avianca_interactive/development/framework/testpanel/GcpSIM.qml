import QtQuick 1.1
import Panasonic.Pif 1.0

Item{
    Connections {
        target: core.viewController.testPanel?core.viewController.testPanel:null;
        onSigTestPanelMessage:{
            if (reason!="gcpInterface") return false;
            core.info("GcpInterfaceSIM.qml | onSigTestPanelMessage | parameter1: "+parameter1);
            switch(parameter1){
            case "hsVolumeUp":
            case "hsVolumeDown":
            case "hsPlayPause":
            case "hsStop":
            case "hsFastForward":
            case "hsRewind":
            case "hsNext":
            case "hsPrevious":
            case "navUp":
            case "navDown":
            case "navLeft":
            case "navRight":
            case "navSelect":
            case "navBack":
            case "navHome":
            case "avodPlayPause":
            case "avodPlay":
            case "avodPause":
            case "avodResume":
            case "avodStop":
            case "avodFastForward":
            case "avodRewind":
            case "avodNext":
            case "avodPrevious":
            case "hsChannelUp":
            case "hsChannelDown":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"'+parameter1+'","args":null,"mirror":{"id":27}}');
                break;
            }
            case "mpSetPosition":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"mpSetPosition","args":{"position":"'+parameter2+'"},"mirror":{"id":27}}');
                break;
            }
            case "mpGetPosition":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"mpGetPosition","args":null,"mirror":{"id":28}}');
                break;
            }
            case "mpRepeatItem":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"mpRepeatItem","args":{"repeat":true},"mirror":{"id":26}}');
                break;
            }
            case "mpRepeatAll":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"mpRepeatAll","args":{"repeat":true},"mirror":{"id":26}}');
                break;
            }
            case "mpShuffle":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"mpShuffle","args":{"shuffle":true},"mirror":{"id":26}}');
                break;
            }
            case "mpSubtitle":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"mpSubtitle","args":{"lid":"'+parameter2+'","type":1,"enabled":true},"mirror":{"id":26}}');
                break;
            }
            case "mpSoundtrack":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"mpSoundtrack","args":{"lid":"'+parameter2+'","type":1,"enabled":true},"mirror":{"id":26}}');
                break;
            }
            case "avodPlayTrack":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"avodPlayTrack","args":{"mid":"'+parameter2+'","elapsedTime":0,"mediaType":"'+parameter3+'"},"mirror":{"id":2}}');
                break;
            }
            case "avodPlayAlbum":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"avodPlayAlbum","args":{"mid":"'+parameter2+'","trackIndex":0,"elapsedTime":0,"mediaType":"audioAggregate"},"mirror":{"id":3}}');
                break;
            }
            case "auxPlay":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"auxPlay","args":{"source":"'+parameter2+'"},"mirror":{"id":3}}');
                break;
            }
            case "appLaunch":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"appLaunch","args":{"app":"'+parameter2+'"},"mirror":{"id":3}}');
                break;
            }
            case "intLanguage":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"intLanguage","args":{"lid":"'+parameter2+'","iso":"'+parameter3+'"},"mirror":{"id":26}}');
                break;
            }
            case "dispBrightness":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"dispBrightness","args":{"level":"'+parameter2+'"},"mirror":{"id":26}}');
                break;
            }
            case "dispBacklight":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"dispBacklight","args":{"level":1},"mirror":{"id":26}}');
                break;
            }
            case "broadcastPlay":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"broadcastPlay","args":{"bid":"'+parameter2+'","mediaType":"broadcastVideo"},"mirror":{"id":2}}');
                break;
            }
            case "removeFromPlaylist":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"removeFromPlaylist","args":{"mid":[],"type":"'+parameter2+'"},"mirror":{"id":2}}');
                break;
            }
            case "addToPlaylist":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"addToPlaylist","args":{"startIndex":"-1","size":"1","type":"'+parameter2+'","mids":["'+parameter3+'"]},"mirror":{"id":2}}');
                break;
            }
            case "playlistPlayTrack":{
                gcpNetworkClient.sendMessage('json','{"type":"jsonwsp/request","version":"1.0","methodname":"playlistPlayTrack","args":{"index":0","elapsedTime":0,type":"'+parameter2+'"},"mirror":{"id":2}}');
                break;
            }
            }
        }
    }

    NetworkClient {
        id: gcpNetworkClient;
        serverHostname: "localhost"
        online: true
        onConnected: {core.log("GcpInterfaceSIM.qml | onConnected");}
        onReceived: {core.log("GcpInterfaceSIM.qml | onReceived | messageType: "+messageType+" | message: "+message);}
        onOnlineChanged: {core.log("GcpInterfaceSIM.qml | onOnlineChanged | online: "+gcpNetworkClient.online);}
        onServerHostnameChanged: {core.log("GcpInterfaceSIM.qml | onServerHostnameChanged");}
        onDisconnected: {core.log("GcpInterfaceSIM.qml | onDisconnected");}
        onError: {core.log("GcpInterfaceSIM.qml | onError | error: "+error);}
    }
}
