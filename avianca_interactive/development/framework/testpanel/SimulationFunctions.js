// sysSettings
var newObject;
var newObjectName=''
function brightnessUp_onClicked(ref){pif.setBrightnessByStep(1);}
function brightnessDown_onClicked(ref){pif.setBrightnessByStep(-1);}
function playPAS_onClicked(ref){
    var value = getUserInput('playPAS_onClicked','Enter Value',ref.hiddenText?ref.hiddenText:"TSH");
    if (value['value']) {
		pif.playPAS(value['value']);
        ref.hiddenText = value['value'];
    }
}
function stopPAS_onClicked(ref){pif.stopPAS();}
function volUp_onClicked(ref){pif.setVolumeByStep(1);}
function volDown_onClicked(ref){pif.setVolumeByStep(-1);}
function capSensorState_onClicked(ref){
    if (pif.getCapSensorState()){pif.__setCapSensorState(0); ref.text="Capsense Off";}
    else { pif.__setCapSensorState(1); ref.text="Capsense On";}
    return ref.text;
}
function stowState_onClicked(ref){
    if(pif.getStowedState()){pif.__setStowedState(0); ref.text="Stow Off";}
    else {pif.__setStowedState(1); ref.text="Stow On";}
    return ref.text;
}

// sysEvents
function flight_onLoad(){ return pif.getOpenFlight()?"Close Flight":"Open Flight";}
function flight_onClicked(ref)
{if (pif.getOpenFlight()){pif.__openFlightChangeAck(0);ref.text="Open Flight";}
    else {pif.__openFlightChangeAck(1);ref.text="Close Flight";}
    return ref.text;
}
function ent_onLoad(){ return pif.getEntOn()?"Ent Off":"Ent On";}
function ent_onClicked(ref){
    if (pif.getEntOn()){ pif.__entOnChangeAck(0);ref.text="Ent On"}
    else { pif.__entOnChangeAck(1);ref.text="Ent Off";}
    return ref.text;
}
function pa_onLoad(){ return pif.getPAState()?"PA Off":"PA On";}
function pa_onClicked(ref){
    if (pif.getPAState()){ pif.__setPAState(0); ref.text="PA On";}
    else { pif.__setPAState(1); ref.text="PA Off"}
    return ref.text;
}

function paxInfoDisable_onLoad(){ return pif.getPaxInfoDisable()?"DisablePaxInfo-false":"DisablePaxInfo-true";}

function paxInfoDisable_onClicked(ref){
    if (pif.getPaxInfoDisable()){
        var params=[];
        params[0]=0;
        pif.eventFlightData("PAX_INFO_DISABLE",params,false);ref.text="DisablePaxInfo-true"
    }
    else {
        params=[];
        params[0]=1;
        pif.eventFlightData("PAX_INFO_DISABLE",params,false);ref.text="DisablePaxInfo-false";}
    return ref.text;
}

function ctMsg_onClicked(ref){pif.__setCtMsg(1,'<?xml version="1.0" encoding="utf-8"?><msg timeout="5">The earliest fixed wing airline was the Aircraft Transport and Travel, formed by George Holt Thomas in 1916. Using a fleet of former military Airco biplanes that had been modified to carry two passengers in the fuselage.</msg>');}
function ctMsgPredefined_onClicked(){
    var modelCount = pif.transientDataSimulation.__modelCount();
    var mid=parseInt(pif.transientDataSimulation.__getCtMid(index),10);
        var messageTimeout=parseInt(pif.transientDataSimulation.__getCtMessageTimeout(index),10);
    core.log("Simulation.qml |ctMsgPredefined | pmid :"+mid+", messageTimeout :"+messageTimeout+", index :"+index+", modelCount :"+modelCount)
        var data;
        data = "<?xml version=\"1.0\" encoding=\"utf-8\"?><msg pmid=\"" +mid+ "\" timeout=\"" +messageTimeout+ "\"></msg>";
        pif.__setCtMsg(1,data);
    if(index!=modelCount-1)index = index + 1;
    else index =0;
}
function eXphone_onLoad(){ return pif.getExphoneServerState()?"eXphone Off":"eXphone On";}
function eXphone_onClicked(ref){
    if (pif.getExphoneServerState()){ pif.__setExphoneState(0); ref.text="eXphone On";}
    else { pif.__setExphoneState(1); ref.text="eXphone Off"}
    return ref.text;
}
function eXphoneNotification_onLoad(){ return pif.getExphoneNotificationState()?"eXphoneNotification Off":"eXphoneNotification On";}
function eXphoneNotification_onClicked(ref){
    if (pif.getExphoneNotificationState()){ pif.__setExphoneNotificationState(0); ref.text="eXphoneNotification On";}
    else { pif.__setExphoneNotificationState(1); ref.text="eXphoneNotification Off"}
    return ref.text;
}
function gmtOffsetOfDestination_onClicked(ref){
    var offset = String(pif.getGmtOffsetOfDestination()).split('.');
    if (parseInt(offset[1],10)>0) {offset[0]++; offset[1]=0} else offset[1]=30;
    pif.__setGmtOffsetOfDestination(parseFloat(offset.join('.')));
    ref.text="GMT offset of destination: "+pif.getGmtOffsetOfDestination();
    return ref.text;
}
function gmtOffsetOfSource_onClicked(ref){
    var offset = String(pif.getGmtOffsetOfSource()).split('.');
    if (parseInt(offset[1],10)>0) {offset[0]++; offset[1]=0} else offset[1]=30;
    pif.__setGmtOffsetOfSource(parseFloat(offset.join('.')));
    ref.text="GMT offset of source: "+pif.getGmtOffsetOfSource();
    return ref.text;
}
function infoMode_onLoad(){ return pif.getInfoMode()?"Info Mode Off":"Info Mode On";}
function infoMode_onClicked(ref){
    if (pif.getInfoMode()){ pif.__setInfoMode(false); ref.text="Info Mode On";}
    else {pif.__setInfoMode(true); ref.text="Info Mode Off"}
    return ref.text;
}
function readinglight_onLoad(){ return pif.getReadingLight()?"Reading Light Off":"Reading Light On";}
function readinglight_onClicked(ref){
    if(pif.getReadingLight()){pif.setReadingLight(0); ref.text="Reading Light On";}
    else {pif.setReadingLight(1);ref.text= "Reading Light Off";}
}
function extv_onLoad(){ return pif.getExtvState()?"EXTV Off":"EXTV On";}
function extv_onClicked(ref){
    if(pif.getExtvState()){pif.__setExtvState(0);ref.text="EXTV On";}
    else {pif.__setExtvState(1);ref.text="EXTV Off";}
}
function wow_onLoad(){ return pif.getWowState()?"W_OffWheels":"W_OnWheels";}
function wow_onClicked(ref){
    if(pif.getWowState()){pif.__setWowState(false);ref.text="W_OnWheels";}
    else {pif.__setWowState(true);ref.text="W_OffWheels";}
}
function forceaudiovideo_onLoad(){ return pif.getInteractiveOverride()?"ForceAV Off":"ForceAV On";}
function forceaudiovideo_onClicked(ref){
    if (pif.getInteractiveOverride()){ pif.__interactiveOverrideChangeAck(0);ref.text="ForceAV On";}
    else { pif.__interactiveOverrideChangeAck(1);ref.text="ForceAV Off";}
}
function satComState_onLoad(){ return pif.getSatcomState()?"SatCom Off":"SatCom On";}
function satComState_onClicked(ref){if (pif.getSatcomState()){ pif.__setSatcomState(0);ref.text="SatCom On";}
    else { pif.__setSatcomState(1);ref.text="SatCom Off";}
}

function groundspeed_onLoad(){ return "GROUND SPEED = "+pif.getGroundSpeed()+" km/hr";}
function groundspeed_onClicked(ref){
    var speed = pif.getGroundSpeed();
    if (speed < 280){speed =speed + 20;pif.__setGroundSpeed(speed);ref.text = "GROUND SPEED = "+pif.getGroundSpeed()+" km/hr";}
    else{pif.__setGroundSpeed(speed);ref.text = "GROUND SPEED = "+pif.getGroundSpeed()+" km/hr";}
}
function trueairspeed_onLoad(){ return "TRUE AIR SPEED = "+pif.getTrueAirSpeed()+" km/hr";}
function trueairspeed_onClicked(ref){
    var speed = pif.getTrueAirSpeed();
    if (speed < 900){speed =speed + 50;pif.__setTrueAirSpeed(speed);ref.text = "TRUE AIR SPEED = "+pif.getTrueAirSpeed()+" km/hr";}
    else{pif.__setTrueAirSpeed(speed);ref.text = "TRUE AIR SPEED = "+pif.getTrueAirSpeed()+" km/hr";}
}
function altitude_onLoad(){ return "ALTITUDE = "+pif.getAltitude()+" ft";}
function altitude_onClicked(ref){
    var distance = pif.getAltitude();
    if (distance < 45000){distance = distance + 1000;pif.__setAltitude(distance);ref.text = "ALTITUDE = "+pif.getAltitude()+" ft";}
    else{pif.__setAltitude(distance);ref.text = "ALTITUDE = "+pif.getAltitude()+" ft";}
}
function mediadate_onClicked(ref){
    var value = getUserInput('mediadate_onClicked','Enter Value',pif.getMediaDate());
    if (value['value']) pif.__setMediaDate(value['value']);
}
function seatrating_onClicked(ref){
    var value = getUserInput('seatrating_onClicked','Enter Value',pif.getSeatRating());
    if (value['value']) pif.__setInternalSeatRating(value['value']);
}
function route_onClicked(ref){
    var value = getUserInput('route_onClicked','Enter Value',pif.getRouteId());
    if (value['value']) pif.__setRouteId(value['value']);
}
function blockmid_onClicked(ref){
    var value = getUserInput('blockmid_onClicked','Enter Value',ref.hiddenText?ref.hiddenText:0);
    if (value['value']) {
        pif.__setBlockMid(value['value']);
        ref.hiddenText = value['value'];
    }
}
function unblockmid_onClicked(ref){
    var value = getUserInput('unblockmid_onClicked','Enter Value',ref.hiddenText?ref.hiddenText:0);
    if (value['value']) {
        pif.__setUnBlockMid(value['value']);
        ref.hiddenText = value['value'];
    }
}
function blockservicemid_onClicked(ref){
    var value = getUserInput('blockservicemid_onClicked','Enter Value',ref.hiddenText?ref.hiddenText:0);
    if (value['value']) {
        pif.__setBlockServiceMid(value['value']);
        ref.hiddenText = value['value'];
    }
}
function unblockservicemid_onClicked(ref){
    var value = getUserInput('unblockservicemid_onClicked','Enter Value',ref.hiddenText?ref.hiddenText:0);
    if (value['value']) {
        pif.__setUnBlockServiceMid(value['value']);
        ref.hiddenText = value['value'];
    }
}
function extvversion_onClicked(ref){
    var value = getUserInput('extvversion_onClicked','Enter Value',pif.getExtvVersion());
    if (value['value']) pif.__setExtvVersion(value['value']);
}

function fallbackMode_onLoad(){ return pif.getFallbackMode()?"Fallback Mode Off":"Fallback Mode On";}
function fallbackMode_onClicked(ref){
    if(pif.getFallbackMode()){pif.__setFallbackMode(false); ref.text="Fallback Mode On";}
    else {pif.__setFallbackMode(true);ref.text= "Fallback Mode Off";}
}
// apps
function usbConnect_onClicked(ref){pif.usb.usbChangeDetected(pif.usb.cUSB_IN,'/');pif.usb.getMountPath()[0]='/';}
function usbDisconnect_onClicked(ref){pif.usb.usbChangeDetected(pif.usb.cUSB_OUT,'/');}
function ipodConnect_onClicked(ref){
    if (pif.getIpodState()){pif.__setIpodState(false);ref.text="ConnectIpod";}
    else{pif.__setIpodState(true);ref.text="DisconnectIpod";}
}
function cardSwipeNeeded_onClicked(){pif.ppv.__onCardSwipeNeededAck()}
function purchaseMid_onClicked(){pif.ppv.purchaseMid(100,25,'USD');}
function cardSwipeCompleted_onClicked(){pif.ppv.__onCardSwipeCompletedAck()}
function cardSwipeError_onClicked(){pif.ppv.__onCardSwipeErrorAck()}
function paymentComplete_onClicked(){pif.ppv.__onPaymentCompleteAck()}
function paymentError_onClicked(){pif.ppv.__onPaymentErrorAck()}
function cancelPaymentProcess_onClicked(){pif.ppv.cancelPaymentProcess()}
function ppvcash_onClicked(ref){
    var value = getUserInput('ppvcash_onClicked','Enter Value',ref.hiddenText);
    if (value['value']) pif.__setppvMessage(value['value'],'paidorfree','PPV_CASH');
    ref.hiddenText = "PPV Cash";
}
function ppvcharge_onClicked(ref){
    var value = getUserInput('ppvcharge_onClicked','Enter Value',ref.hiddenText);
    if (value['value']) pif.__setppvMessage(value['value'],'paidorfree','PPV_CHARGE');
    ref.hiddenText = "PPV Charge";
}
function ppvcomp_onClicked(ref){
    var value = getUserInput('ppvcomp_onClicked','Enter Value',ref.hiddenText);
    if (value['value']) pif.__setppvMessage(value['value'],'paidorfree','PPV_COMP');
    ref.hiddenText = "PPV Comp"
}
function ppvrefund_onClicked(ref){
    var value = getUserInput('ppvrefund_onClicked','Enter Value',ref.hiddenText);
    if (value['value']) pif.__setppvMessage(value['value'],'pay','PPV_REFUND');
    ref.hiddenText = "PPV Refund"
}
function CallInProgress_onLoad(){
    return pif.telephony.__getCallInProgressState()?"CallIn Progress Off":"CallIn Progress On";}
function CallInProgress_onClicked(ref){
    if (pif.telephony.__getCallInProgressState()){pif.telephony.__setCallInProgressState(false);ref.text = "CallIn Progress On"
    }else{pif.telephony.__setCallInProgressState(true);ref.text = "CallIn Progress Off"}
}
function Air2grdService_onLoad(){return pif.telephony.__getAir2GroundService()?"Air2grdService down":"Air2grdService up";}
function Air2grdService_onClicked(ref){
    if (pif.telephony.__getAir2GroundService()){pif.telephony.__onPhoneServiceStatusAck(0,0);ref.text = "Air2grdService up"
    }else{pif.telephony.__onPhoneServiceStatusAck(0,1);ref.text = "Air2grdService down";}
}
function Seat2SeatService_onLoad(){return pif.telephony.__getSeat2SeatService()?"Seat2SeatService down":"Seat2SeatService up";}
function Seat2SeatService_onClicked(ref){
    if (pif.telephony.__getSeat2SeatService()){pif.telephony.__onPhoneServiceStatusAck(0,0);ref.text = "Seat2SeatService up "
    }else{pif.telephony.__onPhoneServiceStatusAck(1,0);ref.text = "Seat2SeatService down";}
}
function Swipecreditcard_onClicked(ref){pif.telephony.__onAuthenticationInputNeededAck();ref.text = "Swipe Credit Card";}
function Swipesuccessful_onClicked(ref){pif.telephony.__onAuthenticationCompleteAck();ref.text = "Swipe Successful";}
function Swipefailed_onClicked(ref){pif.telephony.__onAuthenticationErrorAck();ref.text = "Swipe failed";}

//orientation change
function orientation_onLoad(){return "Change Orientation";}
function orientation_onClicked(ref){
    core.info(">>> pif.getOrientation(): "+pif.getOrientation())
    if (pif.getOrientation()==0) pif.__setOrientation(1);
    else if (pif.getOrientation()==1) pif.__setOrientation(2);
    else if (pif.getOrientation()==2) pif.__setOrientation(3);
    else if (pif.getOrientation()==3) pif.__setOrientation(0);
    return ref.text;
}

//standard karma Engine4 communication
function init_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="init" /></remote_message>');}
function language_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="language" /><params><param value="eng" index="0" /></params> </remote_message>')}

function play_aod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="aod_play" /><params><param value="1001" index="0" /><param value="1000" index="1" /></params> </remote_message>')}
function shuffle_aod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="aod_shuffle" /><params><param value="on" index="0" /></params> </remote_message>');}
function aod_next_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="aod_next" /></remote_message>');}
function aod_prev_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="aod_prev" /></remote_message>');}
function pause_aod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="aod_pause" /></remote_message>');}
function resume_aod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="aod_resume" /></remote_message>');}
function stop_aod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="aod_stop" /></remote_message>');}

function play_movie_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="vod_play" /><params><param value="2000" index="0" /><param value="1" index="1" /><param value="2" index="2" /><param value="3" index="3" /><param value="4" index="4" /> </params> </remote_message>');}
function play_trailer_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="vodclip_play" /><params><param value="3000" index="0" /><param value="1" index="1" /><param value="2" index="2" /><param value="3" index="3" /><param value="4" index="4" /> </params> </remote_message>');}
function rewind_vod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="vod_rw" /></remote_message>');}
function ff_vod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="vod_ff" /></remote_message>');}
function pause_vod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="vod_ff" /></remote_message>');}
function resume_vod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="vod_ff" /></remote_message>');}
function stop_vod_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="vod_stop" /></remote_message>');}
function extv_play_onClicked(ref){pif.engine4Interface.__dataReceived('<remote_message><api value="extv_play" /><params><param value="1" index="0" /></params></remote_message>');}

// Seat Chat
function addBuddy_onClicked(ref){
    var value = getUserInput('addBuddy_onClicked','Enter session,seatno',ref.hiddenText?ref.hiddenText:"i0,3A");
    if (value['value']) {
        var arr = value['value'].split(",");
        if (arr[0] && arr[1]) sigTestPanelMessage("SeatChat","addbuddy",arr[0],arr[1],"","");
        ref.hiddenText = value['value'];
    }
}
function removeBuddy_onClicked(ref){
    var value = getUserInput('removeBuddy_onClicked','Enter session,seatno',ref.hiddenText?ref.hiddenText:"i0,3A");
    if (value['value']) {
        var arr = value['value'].split(",");
        if (arr[0] && arr[1]) sigTestPanelMessage("SeatChat","removebuddy",arr[0],arr[1],"","");
        ref.hiddenText = value['value'];
    }
}

function chatMessage_onClicked(ref){
    var value = getUserInput('chatMessage_onClicked','Enter seatno,msg',ref.hiddenText?ref.hiddenText:"2b,Chat Message from 2b");
    if (value['value']) {
        var arr = value['value'].split(",");
        if (arr[1]) sigTestPanelMessage("SeatChat","chatmessage","",arr[0],arr[1],"");
        ref.hiddenText = value['value'];
    }
}
function setNickname_onClicked(ref){
    var value = getUserInput('setNickname_onClicked','Enter seatno,nickname',ref.hiddenText?ref.hiddenText:"2b,New Nick Name");
    if (value['value']) {
        var arr = value['value'].split(",");
        if (arr[1]) sigTestPanelMessage("SeatChat","setnickname",arr[0],arr[1],"","");
        ref.hiddenText = value['value'];
    }
}

function setAvatar_onClicked(ref){
    var value = getUserInput('setAvatar_onClicked','Enter seatno,seat avatar',ref.hiddenText?ref.hiddenText:"2b,avtar_big_03");
    if (value['value']) {
        var arr = value['value'].split(",");
        if (arr[1]) sigTestPanelMessage("SeatChat","setavatar",arr[0],arr[1],"","");
        ref.hiddenText = value['value'];
    }
}

function setStatus_onClicked(ref){
    var value = getUserInput('setStatus_onClicked','Enter seatno,seat status',ref.hiddenText?ref.hiddenText:"2b,"+pif.seatChat.cDoNotDisturb);
    if (value['value']) {
        var arr = value['value'].split(",");
        if (arr[1]) sigTestPanelMessage("SeatChat","setstatus",arr[0],arr[1],"","");
        ref.hiddenText = value['value'];
    }
}
function inviteAccepted_onClicked(ref){
    var value = getUserInput('inviteAccepted_onClicked','Enter seatno',ref.hiddenText?ref.hiddenText:"2b");
    if (value['value']) {
        if (value['value']) sigTestPanelMessage("SeatChat","inviteAccepted","",value['value'],"","");
        ref.hiddenText = value['value'];
    }
}
function inviteDeclined_onClicked(ref){
    var value = getUserInput('inviteDeclined_onClicked','Enter seatno',ref.hiddenText?ref.hiddenText:"2b");
    if (value['value']) {
        if (value['value']) sigTestPanelMessage("SeatChat","inviteDeclined","",value['value'],"","");
        ref.hiddenText = value['value'];
    }
}


function allInviteAccepted_onClicked(ref){
    sigTestPanelMessage("SeatChat","allInviteAccepted","","","","");
}
function allInviteDeclined_onClicked(ref){
    sigTestPanelMessage("SeatChat","allInviteDeclined","","","","");
}
function sendInvite_onClicked(ref){
    var value = getUserInput('sendInvite_onClicked','Enter seatno',ref.hiddenText?ref.hiddenText:"2b");
    if (value['value']) {
        if (value['value']) sigTestPanelMessage("SeatChat","sendInvite","",value['value'],"","");
        ref.hiddenText = value['value'];
    }
}
function sendInviteCreator_onClicked(ref){
    var value = getUserInput('sendInviteCreator_onClicked','Enter seatno',ref.hiddenText?ref.hiddenText:"2b");
    if (value['value']) {
        if (value['value']) sigTestPanelMessage("SeatChat","sendInviteCreator","",value['value'],"","");
        ref.hiddenText = value['value'];
    }
}
function loginSeatChat_onClicked(ref){
    var value = getUserInput('loginSeatChat_onClicked','Enter seatno,name',ref.hiddenText?ref.hiddenText:"2b,dwayne");
    if (value['value']) {
        var arr = value['value'].split(",");
        sigTestPanelMessage("SeatChat","loginSeatChat","",arr[0],arr[1],pif.seatChat.cAvailable);
        ref.hiddenText = value['value'];
    }
}

function exitSeatChatByUser_onClicked(ref){
    var value = getUserInput('exitSeatChatByUser_onClicked','Enter seatno',ref.hiddenText?ref.hiddenText:"2b");
    if (value['value']) {
        var arr = value['value'].split(",");
        sigTestPanelMessage("SeatChat","exitSeatChatByUser","",arr[0],"","");
        ref.hiddenText = value['value'];
    }
}

function leavePrivateSession_onClicked(ref){
    var value = getUserInput('leavePrivateSession_onClicked','Enter session,seatno',ref.hiddenText?ref.hiddenText:"j0,2B");
    if (value['value']) {
        var arr = value['value'].split(",");
        if (arr[0] && arr[1]) sigTestPanelMessage("SeatChat","leavePrivateSession",arr[0],arr[1],"","");
        ref.hiddenText = value['value'];
    }
}


function incomingEmailMessage_onClicked(ref){

    var value = getUserInput('incomingEmailMessage_onClicked','from|sbj|body',ref.hiddenText?ref.hiddenText:"t1@gmail.com|Reminder|Welcome to smsEmail, to continue press continue");
    if (value['value']) {
        var tmp=value['value'].split("|");
        sigTestPanelMessage("SmsEmail","incomingEmailMessage",tmp[0],tmp[1],tmp[2],"");
        pif.smsEmail.sigNewMessageReceived();
        ref.hiddenText = value['value'];
    }
}
function incomingSmsMessage_onClicked(ref){
    var value = getUserInput('incomingSmsMessage_onClicked','from|body',ref.hiddenText?ref.hiddenText:"9833123456|Welcome to Smsemail, to continue press continue");
    if (value['value']) {
        var tmp=value['value'].split("|");
        sigTestPanelMessage("SmsEmail","incomingSmsMessage",tmp[0],tmp[1],"","");
        pif.smsEmail.sigNewMessageReceived();
        ref.hiddenText = value['value'];
    }
}
function initialise_onClicked(ref){
    core.pif.smsEmail.initialise();
}
function authenticate_onClicked(ref){
    pif.smsEmail.authenticate();
}

function swipeSuccess_onClicked(ref){
    sigTestPanelMessage("SmsEmail","SwipeCardSuccess","","","","");

}
function swipeFail_onClicked(ref){
    sigTestPanelMessage("SmsEmail","SwipeCardFail","","","","");
}

function setUserInfo_onClicked(ref){
    pif.smsEmail.setUserInfo("abc","abc@gmail.com");
}

function getMessages_onClicked(ref){
    pif.smsEmail.getMessages();
}

function logout_onClicked(ref){
    pif.smsEmail.requestLogout();
}

function numMsg5_onClicked(ref){
    sigTestPanelMessage("SmsEmail","numMsg","5","","","");
}

function numMsg10_onClicked(ref){
    sigTestPanelMessage("SmsEmail","numMsg","10","","","");
}

function numMsg20_onClicked(ref){
    sigTestPanelMessage("SmsEmail","numMsg","20","","","");
}

function userInfoFailed_onClicked(ref){
    pif.smsEmail.sigSetUserInfoFailed();
}

function msgUpdateFailed_onClicked(ref){
    pif.smsEmail.sigMessagesUpdatedFailed();
}

function msgBodyFailed_onClicked(ref){
    pif.smsEmail.sigMessageBodyFailed();
}

function msgSentFailed_onClicked(ref){
    pif.smsEmail.sigMessageSentFailed();
}

function logoutFailed_onClicked(ref){
    pif.smsEmail.sigLogoutFailed();
}

// GCP Interface
function hsVolumeUp_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsVolumeUp","","","","");}
function hsVolumeDown_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsVolumeDown","","","","");}
function hsPlayPause_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsPlayPause","","","","");}
function hsStop_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsStop","","","","");}
function hsFastForward_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsFastForward","","","","");}
function hsRewind_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsRewind","","","","");}
function hsNext_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsNext","","","","");}
function hsPrevious_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsPrevious","","","","");}
function navUp_onClicked(ref) {sigTestPanelMessage("gcpInterface","navUp","","","","");}
function navDown_onClicked(ref) {sigTestPanelMessage("gcpInterface","navDown","","","","");}
function navLeft_onClicked(ref) {sigTestPanelMessage("gcpInterface","navLeft","","","","");}
function navRight_onClicked(ref) {sigTestPanelMessage("gcpInterface","navRight","","","","");}
function navSelect_onClicked(ref) {sigTestPanelMessage("gcpInterface","navSelect","","","","");}
function navBack_onClicked(ref) {sigTestPanelMessage("gcpInterface","navBack","","","","");}
function navHome_onClicked(ref) {sigTestPanelMessage("gcpInterface","navHome","","","","");}
function avodPlayPause_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodPlayPause","","","","");}
function avodPlay_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodPlay","","","","");}
function avodPause_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodPause","","","","");}
function avodResume_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodResume","","","","");}
function avodStop_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodStop","","","","");}
function avodFastForward_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodFastForward","","","","");}
function avodRewind_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodRewind","","","","");}
function avodNext_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodNext","","","","");}
function avodPrevious_onClicked(ref) {sigTestPanelMessage("gcpInterface","avodPrevious","","","","");}
function mpSetPosition_onClicked(ref) {
    var value = getUserInput('mpSetPosition_onClicked','Enter Position',127);
    if (value['value']) sigTestPanelMessage("gcpInterface","mpSetPosition",value['value'],"","","");
}
function mpGetPosition_onClicked(ref) {sigTestPanelMessage("gcpInterface","mpGetPosition","","","","");}
function mpRepeatItem_onClicked(ref) {sigTestPanelMessage("gcpInterface","mpRepeatItem","","","","");}
function mpRepeatAll_onClicked(ref) {sigTestPanelMessage("gcpInterface","mpRepeatAll","","","","");}
function mpShuffle_onClicked(ref) {sigTestPanelMessage("gcpInterface","mpShuffle","","","","");}
function mpSubtitle_onClicked(ref) {
    var value = getUserInput('mpSubtitle_onClicked','Enter subtitle lid',2);
    if (value['value']) sigTestPanelMessage("gcpInterface","mpSubtitle",value['value'],"","","");
}
function mpSoundtrack_onClicked(ref) {
    var value = getUserInput('mpSoundtrack_onClicked','Enter soundtrack lid',2);
    if (value['value']) sigTestPanelMessage("gcpInterface","mpSoundtrack",value['value'],"","","");
}
function avodPlayTrack_onClicked(ref) {
    var value = getUserInput('avodPlayTrack_onClicked','Enter mid,type','123,audio');
    if (value['value']) {
        var arr=value['value'].split(",")
        sigTestPanelMessage("gcpInterface","avodPlayTrack",arr[0],arr[1],"","");
    }
}
function avodPlayAlbum_onClicked(ref) {
    var value = getUserInput('avodPlayAlbum_onClicked','Enter mid',123);
    if (value['value']) sigTestPanelMessage("gcpInterface","avodPlayAlbum",value['value'],"","","");
}
function auxPlay_onClicked(ref) {
    var value = getUserInput('auxPlay_onClicked','Enter source',"hdmi1");
    if (value['value']) sigTestPanelMessage("gcpInterface","auxPlay",value['value'],"","","");
}
function appLaunch_onClicked(ref) {
    var value = getUserInput('appLaunch_onClicked','Enter app',"ltn");
    if (value['value']) sigTestPanelMessage("gcpInterface","appLaunch",value['value'],"","","");
}
function intLanguage_onClicked(ref) {
    var value = getUserInput('intLanguage_onClicked','Enter lid',"1,eng");
    if (value['value']) {
        var arr=value['value'].split(",")
        sigTestPanelMessage("gcpInterface","intLanguage",arr[0],arr[1],"","");
    }
}
function dispBrightness_onClicked(ref) {
    var value = getUserInput('dispBrightness_onClicked','Enter brightness value',70);
    if (value['value']) sigTestPanelMessage("gcpInterface","dispBrightness",value['value'],"","","");
}
function dispBacklight_onClicked(ref) {sigTestPanelMessage("gcpInterface","dispBacklight","","","","");}
function broadcastPlay_onClicked(ref) {
    var value = getUserInput('broadcastPlay_onClicked','Enter channel id',2);
    if (value['value']) sigTestPanelMessage("gcpInterface","broadcastPlay",value['value'],"","","");
}
function hsChannelUp_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsChannelUp","","","","");}
function hsChannelDown_onClicked(ref) {sigTestPanelMessage("gcpInterface","hsChannelDown","","","","");}

function playlistPlayTrack_onClicked(ref) {
    var value = getUserInput('playlistPlayTrack_onClicked','Enter type(aod,vod)',"aod");
    if (value['value']) sigTestPanelMessage("gcpInterface","playlistPlayTrack",value['value'],"","","");
}

function addToPlaylist_onClicked(ref) {
    var value = getUserInput('addToPlaylist_onClicked','Enter type,mids','aod,123');
    if (value['value']) {
        var arr=value['value'].split(",")
        sigTestPanelMessage("gcpInterface","addToPlaylist",arr[0],arr[1],"","");
    }
}

function removeFromPlaylist_onClicked(ref) {
    var value = getUserInput('removeFromPlaylist_onClicked','Enter type(aod,vod)',"aod");
    if (value['value']) sigTestPanelMessage("gcpInterface","removeFromPlaylist",value['value'],"","","");
}

// EVID Interface
function blueRayPlay_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Control","PLAY","","","");
}

function blueRayPause_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Control","PAUSE","","","");
}

function blueRayFF_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Control","FASTFWD","","","");
}

function blueRayFR_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Control","FASTRWD","","","");
}

function blueRayMenuTitle_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Control","MENU_TITLE","","","");
}

function blueRayDiscTitle_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Control","MENU_DISC","","","");
}

function blueRayStop_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Control","STOP","","","");
}

function blueRayMediumNone_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Medium_Type","NONE","","","");
}

function blueRayMediumBD_onClicked(ref){
    sigTestPanelMessage("evidInterface","Blue_Ray_Medium_Type","BD","","","");
}

function lhtVolume_onClicked(ref){
    var value = getUserInput('lhtVolume_onClicked','Enter Volume value',15);
    if (value['value'])sigTestPanelMessage("evidInterface","lhtVolume",value['value'],"","","");
}

function lhtBrightness_onClicked(ref){
    var value = getUserInput('lhtBrightness_onClicked','Enter brightness value',15);
    if (value['value'])sigTestPanelMessage("evidInterface","lhtBrightness",value['value'],"","","");
}

function lhtContrast_onClicked(ref){
    var value = getUserInput('lhtContrast_onClicked','Enter Contrast value',15);
    if (value['value'])sigTestPanelMessage("evidInterface","lhtContrast",value['value'],"","","");
}

function lhtBacklightOn_onClicked(ref){
    sigTestPanelMessage("evidInterface","lhtBacklight","ON","","","");
}

function lhtBacklightOff_onClicked(ref){
    sigTestPanelMessage("evidInterface","lhtBacklight","OFF","","","");
}

function classVid_onClicked(ref){
    var value = getUserInput('custom_onClicked','vid,value ',15);
    if (value['value']){sigTestPanelMessage("evidInterface","custom",value['value'],"","","");}
}
function gamesAppExit_onClicked(ref){
    pif.launchApp.onLauncherExitSignal();
}

function aspectRatioSystem_onClicked(ref){
    if(aspectValue=="")aspectValue= "aspect-16:9-notfixed"
    if(aspectValue!="aspect-4:3-notfixed" && aspectValue!="aspect-16:9-fixed" && aspectValue!="aspect-16:9-notfixed")aspectValue="aspect-4:3-notfixed"
    else if(aspectValue!="aspect-4:3-fixed" && aspectValue!="aspect-16:9-fixed" && aspectValue!="aspect-16:9-notfixed")aspectValue="aspect-16:9-fixed"
    else if(aspectValue!="aspect-4:3-fixed" && aspectValue!="aspect-4:3-notfixed" && aspectValue!="aspect-16:9-notfixed")aspectValue="aspect-16:9-notfixed"
    else if(aspectValue!="aspect-4:3-fixed" && aspectValue!="aspect-4:3-notfixed" && aspectValue!="aspect-16:9-fixed")aspectValue="aspect-4:3-fixed"
    core.info("SimulationFunctions.js | Setting System Aspect Ratio :"+aspectValue)
    ref.text="Aspect Ratio :"+aspectValue;
    pif.vod.__aspectRatioChangeAck(aspectValue)
}

function cgDataAvaliable_onClicked(ref){
    var value = getUserInput('cgDataAvaliable_onClicked','Enter CG value','1');
    if (value['value']) {
        pif.__setCgDataAvaliable(parseInt(value['value'],10));
    }
}

function fallback_onLoad(){return pif.getFallbackMode()?"Fallback Off":"Fallback On";}
function fallback_onClicked(ref){
    if (pif.getFallbackMode()){ pif.__setFallbackMode(0);ref.text="Fallback On";}
    else { pif.__setFallbackMode(1);ref.text="Fallback Off";pif.__setGroupStates([1,1,1,1,1])}
    return ref.text;
}
function disablefirstgroupstate_onClicked(){
    pif.__setGroupStates([0,1,1,1,1])
}

function ShopCatalogDisable_onClicked(ref){
    var value = getUserInput('ShopCatalogDisable_onClicked','Enter Shopping catalog ID to be disabled','');
    if (value['value']) {
        pif.sigInflightMenuStatusChanged("shop","CATALOG",parseInt(value['value'],10),"blocked",core.pif.getCabinClassName().toUpperCase());
    }
}

function ShopCatalogEnable_onClicked(ref){
    var value = getUserInput('ShopCatalogEnable_onClicked','Enter Shopping catalog ID to be enabled','');
    if (value['value']) {
        pif.sigInflightMenuStatusChanged("shop","CATALOG",parseInt(value['value'],10),"unblocked",core.pif.getCabinClassName().toUpperCase());
    }
}

function HospCatalogDisable_onClicked(ref){
    var value = getUserInput('HospCatalogDisable_onClicked','Enter Hospitality catalog ID to be disabled','');
    if (value['value']) {
        pif.sigInflightMenuStatusChanged("hosp","CATALOG",parseInt(value['value'],10),"blocked",core.pif.getCabinClassName().toUpperCase());
    }
}

function HospCatalogEnable_onClicked(ref){
    var value = getUserInput('HospCatalogEnable_onClicked','Enter Hospitality catalog ID to be enabled','');
    if (value['value']) {
        pif.sigInflightMenuStatusChanged("hosp","CATALOG",parseInt(value['value'],10),"unblocked",core.pif.getCabinClassName().toUpperCase());
    }
}

function ShopCartDisable_onClicked(ref){
    var value = getUserInput('ShopCartDisable_onClicked','Enter Shopping cart ID to be disabled','');
    if (value['value']) {
        pif.sigInflightMenuStatusChanged("shop","CART",parseInt(value['value'],10),"blocked",core.pif.getCabinClassName().toUpperCase());
    }
}

function ShopCartEnable_onClicked(ref){
    var value = getUserInput('ShopCartEnable_onClicked','Enter Shopping cart ID to be enabled','');
    if (value['value']) {
        pif.sigInflightMenuStatusChanged("shop","CART",parseInt(value['value'],10),"unblocked");
    }
}

function HospCartDisable_onClicked(ref){
    var value = getUserInput('HospCartDisable_onClicked','Enter Hospitality cart ID to be disabled','');
    if (value['value']) {
        pif.sigInflightMenuStatusChanged("hosp","CART",parseInt(value['value'],10),"blocked");
    }
}

function HospCartEnable_onClicked(ref){
    var value = getUserInput('HospCartEnable_onClicked','Enter Hospitality cart ID to be enabled','');
    if (value['value']) {
        pif.sigInflightMenuStatusChanged("hosp","CART",parseInt(value['value'],10),"unblocked");
    }
}
