import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    id: chatManager;
    property string chatServerIp: "172.17.0.24";
    property string gatewayIp: "172.17.0.24";
    property string gatewayPort: "57777";
    property string listenPort: "57777";
    property int __errorCode:0;      // 1=> file not found
    property int __TotalPublicSessions:0;
    property int __TotalPrivateSessions: 0;
    property int __lastSessionId:0;
    property string __currentSessionId:"";
    property string __seatNo:"";
    property bool __joinChatError:false;
    property bool __leaveChatError:false;
    property bool __skipDataPopulation:false;
    property int __availableSeatsCount: 0;
    signal chatEvent(variant event, variant eventInfo);

    Connections {
        target: core.pif
        onOpenFlightChanged:{
            core.info("ChatManagerSIM.qml | onOpenFlightChanged called "+openFlight)
            if (openFlight) return;
            __initialiseArrData();
            __currentSessionId="";
            __TotalPublicSessions=0;
            __TotalPrivateSessions=0;
            __availableSeatsCount=0;
            __lastSessionId=__TotalPrivateSessions - 1;
            __seatNo="";
            __skipDataPopulation=false;
        }
    }

    Connections{
        target: core.viewController.testPanel?core.viewController.testPanel:null;
        onSigTestPanelMessage:{
            core.info("ChatManagerSIM.qml | onSigTestPanelMessage | reason: "+reason);
            if (reason!="SeatChat") return false;
            if (parameter1=="addbuddy") __addBuddy(parameter2,parameter3.toUpperCase());
            else if (parameter1=="removebuddy") __removeBuddy(parameter2,parameter3.toUpperCase());
            else if (parameter1=="chatmessage") __addSeatChatMessage(parameter3.toUpperCase(),parameter4);
            else if (parameter1=="setnickname") __updateSeatInfo("alias",parameter2.toUpperCase(),parameter3);
            else if (parameter1=="setstatus") __updateSeatInfo("status",parameter2.toUpperCase(),parameter3);
            else if (parameter1=="setstate") __updateSeatInfo("state",parameter2.toUpperCase(),parameter3);
            else if (parameter1=="setavatar") __updateSeatInfo("avatar",parameter2.toUpperCase(),parameter3);
            else if (parameter1=="inviteAccepted") __chatInvitationAccepted(parameter3.toUpperCase());
            else if (parameter1=="inviteDeclined") __chatInvitationDeclined(parameter3.toUpperCase());
            else if (parameter1=="allInviteAccepted") __allChatInvitationAccepted();
            else if (parameter1=="allInviteDeclined") __allChatInvitationDeclined();
            else if (parameter1=="sendInvite") __receivedInvitation(parameter3.toUpperCase());
            else if (parameter1=="sendInviteCreator") __receivedInvitationFromCreator(parameter3.toUpperCase());
            else if (parameter1=="loginSeatChat")__loginUserToSeatChat(parameter3.toUpperCase(),parameter4,parameter5)
            else if (parameter1=="exitSeatChatByUser")__exitSeatChatByUser(parameter3.toUpperCase());
            else if (parameter1=="leavePrivateSession")__leavePrivateSession(parameter2,parameter3.toUpperCase())
            else core.error("ChatManagerSIM.qml | Unknown Request");
        }
    }

    function __removeBuddy(serverSessionId,seatno) {
        seatno = seatno.toUpperCase();
        if (!__skipDataPopulation || !seatno || Store.PublicSessionsArr[serverSessionId]==undefined) return false;
        var sptr = Store.AvailableSeatsArr[seatno];
        var ptr = Store.PublicSessionsArr[serverSessionId]["buddies"];
        var dptr = Store.PublicSessionsArr[serverSessionId]["numofparticipants"];
        for (var i=0;i<ptr.length;i++)
            if (ptr[i].seatnumber==seatno) {
                ptr.splice(i,1);
                core.info("ptr.length: "+ptr.length);
                core.info("buddies.length: "+Store.PublicSessionsArr[serverSessionId]["buddies"].length);
                Store.PublicSessionsArr[serverSessionId].numofparticipants = Store.PublicSessionsArr[serverSessionId]["buddies"].length
                __setEventInfo(4,{"serversessionid":serverSessionId,"seatnumber":sptr.seatnumber,"seatname":sptr.seatname});
                return true;
            }
        return true;
    }

    function __addBuddy(serverSessionId,seatno) {
        seatno = seatno.toUpperCase();
        if (!__skipDataPopulation || !seatno || Store.PublicSessionsArr[serverSessionId]==undefined) return false;
        if (Store.AvailableSeatsArr[seatno]==undefined)
        {
            Store.AvailableSeatsArr[seatno] = {"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0"};
            __availableSeatsCount =__availableSeatsCount + 1;
        }
        var sptr = Store.AvailableSeatsArr[seatno];
        var ptr = Store.PublicSessionsArr[serverSessionId]["buddies"];
        for (var i=0;i<ptr.length;i++) if (ptr[i].seatnumber==seatno) return false;
        ptr.push(sptr);
        Store.PublicSessionsArr[serverSessionId].numofparticipants = Store.PublicSessionsArr[serverSessionId]["buddies"].length
        core.info("buddies.length: "+Store.PublicSessionsArr[serverSessionId]["buddies"].length);
        __setEventInfo(3,{"serversessionid":serverSessionId,"seatnumber":sptr.seatnumber,"seatname":sptr.seatname});
        return true;
    }

    function getPrivateSessionList(to,from){
        return Store.PrivateSessionsArr;
    }

    function __chatInvitationAccepted (seatno){
        core.info("ChatManagerSIM.qml | __chatInvitationAccepted | seatno: "+seatno);
        seatno = seatno.toUpperCase();
        if (!Store.SendInviteArr[seatno]){
            core.info("ChatManagerSIM.qml | __chatInvitationAccepted | Invitations for Seatno :"+seatno+ " does not exist: ");
            return}
        var sendInvite = Store.SendInviteArr[seatno].split(",");
        for(var i=0;i<sendInvite.length;i++){
            if(!Store.PrivateSessionsArr[sendInvite[i]]){
                core.info("ChatManagerSIM.qml | __chatInvitationAccepted | Session does not exist: "+sendInvite[i]);
                continue;
            }
            if (!Store.AvailableSeatsArr[seatno]){
                Store.AvailableSeatsArr[seatno]={"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0"};
            __availableSeatsCount =__availableSeatsCount + 1;
            }
            Store.PrivateSessionsArr[sendInvite[i]]["buddies"].push(Store.AvailableSeatsArr[seatno]);
            core.info("ChatManagerSIM.qml | __chatInvitationAccepted | Sessions: "+sendInvite[i]+" Seatno: "+seatno);
            __setEventInfo(1,{"serversessionid":sendInvite[i],"seatnumber":seatno});
            __setEventInfo(3,{"serversessionid":sendInvite[i],"seatnumber":seatno,"seatname":""});
        }
        delete Store.SendInviteArr[seatno];
    }
    function __allChatInvitationAccepted (){
        core.info("ChatManagerSIM.qml | __allChatInvitationAccepted called ");
        var sendInvite;
        for (var i in Store.SendInviteArr){
            sendInvite = Store.SendInviteArr[i].split(",");
            core.info("ChatManagerSIM.qml | __allChatInvitationAccepted | Sessions:"+Store.SendInviteArr[i]+"Seatnumber"+i);
            for (var j=0; j<sendInvite.length;j++){
                if(!Store.PrivateSessionsArr[sendInvite[j]]){
                    core.info("ChatManagerSIM.qml | __allChatInvitationAccepted | Session does not exist: "+sendInvite[j]);
                    continue;
                }
                if (!Store.AvailableSeatsArr[i]){
                    Store.AvailableSeatsArr[i]={"seatnumber":i,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
                __availableSeatsCount =__availableSeatsCount + 1;
                }
                Store.PrivateSessionsArr[sendInvite[j]]["buddies"].push(Store.AvailableSeatsArr[i]);
                core.info("ChatManagerSIM.qml | __allChatInvitationAccepted | Sessions: "+sendInvite[j]+" Seatno: "+i);
                __setEventInfo(1,{"serversessionid":sendInvite[j],"seatnumber":i});
                __setEventInfo(3,{"serversessionid":sendInvite[j],"seatnumber":i,"seatname":""});
            }
        }
        Store.SendInviteArr = [] ;
    }

    function __chatInvitationDeclined (seatno){
        core.info("ChatManagerSIM.qml | __chatInvitationDeclined called | Seatno: "+seatno);
        seatno = seatno.toUpperCase();
        if (!Store.SendInviteArr[seatno]){
            core.info("ChatManagerSIM.qml | __chatInvitationDeclined called | Invitation for Seatno: "+seatno+ "Does not exist: ");
            return;
        }
        var sendInvite = Store.SendInviteArr[seatno].split(",");
        for (var i=0;i<sendInvite.length;i++){
            if(!Store.PrivateSessionsArr[sendInvite[i]]){
                core.info("ChatManagerSIM.qml | sendInvitation called | Session does not exist: "+sendInvite[i]);continue;
            }
            if (!Store.AvailableSeatsArr[seatno]){
                Store.AvailableSeatsArr[seatno]={"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0"};
            __availableSeatsCount =__availableSeatsCount + 1;
            }
                __setEventInfo(19,{"serversessionid":sendInvite[i],"seatnumber":seatno});
            core.info("ChatManagerSIM.qml | __chatInvitationDeclined | Sessions: "+sendInvite[i])
        }
        delete Store.SendInviteArr[seatno];
    }

    function __allChatInvitationDeclined (){
        core.info("ChatManagerSIM.qml | __allChatInvitationDeclined called ");
        var sendInvite;
        for (var i in Store.SendInviteArr){
            sendInvite = Store.SendInviteArr[i].split(",");
            for (var j=0;j<sendInvite.length;j++){
                if(!Store.PrivateSessionsArr[sendInvite[j]]){
                    core.info("ChatManagerSIM.qml | __allChatInvitationDeclined | Session does not exist: "+sendInvite[j]);continue;
                }
                if (!Store.AvailableSeatsArr[i]){
                    Store.AvailableSeatsArr[i]={"seatnumber":i,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
                 __availableSeatsCount =__availableSeatsCount + 1;
                }
                core.info("ChatManagerSIM.qml | __chatInvitationDeclined | Sessions: "+sendInvite[j]+" Seatno: "+i)
                __setEventInfo(19,{"serversessionid":sendInvite[j],"seatnumber":i});
            }
        }
        Store.SendInviteArr = [] ;
    }

    function __receivedInvitation (seatno){
        core.info("ChatManagerSIM.qml | __receivedInvitation | Invitation form Seatno :"+seatno)
        if(Store.AvailableSeatsArr[__seatNo] && Store.AvailableSeatsArr[__seatNo].blockallinvites=="1"){
            core.info("ChatManagerSIM.qml | __receivedInvitation | All invitations blocked");
            return;
        }
        if(!Store.AvailableSeatsArr[seatno.toUpperCase()]){
            core.info("ChatManagerSIM.qml | __receivedInvitation | Seatno not present | Adding Seatno"+seatno)
            Store.AvailableSeatsArr[seatno.toUpperCase()]={"seatnumber":seatno.toUpperCase(),"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
            __availableSeatsCount =__availableSeatsCount + 1;
        }
        if(Store.AvailableSeatsArr[seatno].blockedstatus=="1"){
            core.info("ChatManagerSIM.qml | __receivedInvitation | Seat Blocked | Seatno :"+seatno);
            return;
        }
        createPrivateSessionForInvitation()
        var serverSessionId = Store.PrivateSessionsArr["j"+__lastSessionId].serversessionid
        core.info("ChatManagerSIM.qml |__receivedInvitation | serverSessionId :"+serverSessionId);
        if(!Store.ReceivedInviteArr[seatno]){
            Store.ReceivedInviteArr[seatno] = serverSessionId;
        }else{
            var receivedInvite = Store.ReceivedInviteArr[seatno].split(",");
            if (receivedInvite.indexOf(serverSessionId)==-1) receivedInvite.push(serverSessionId);
            Store.ReceivedInviteArr[seatno] = receivedInvite.join(",");
        }
        __setEventInfo(0,{"serversessionid":serverSessionId,"seatnumber":seatno,"seatname":(Store.AvailableSeatsArr[seatno.toUpperCase()].seatname!="")?Store.AvailableSeatsArr[seatno.toUpperCase()].seatname:""})
    }
    function __receivedInvitationFromCreator (seatno){
        core.info("ChatManagerSIM.qml | __receivedInvitation | Invitation form Seatno :"+seatno)
        if(Store.AvailableSeatsArr[__seatNo] && Store.AvailableSeatsArr[__seatNo].blockallinvites=="1"){
            core.info("ChatManagerSIM.qml | __receivedInvitation | All invitations blocked");
            return;
        }
        if(!Store.AvailableSeatsArr[seatno.toUpperCase()]){
            core.info("ChatManagerSIM.qml | __receivedInvitation | Seatno not present | Adding Seatno"+seatno)
            Store.AvailableSeatsArr[seatno.toUpperCase()]={"seatnumber":seatno.toUpperCase(),"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
            __availableSeatsCount =__availableSeatsCount + 1;
        }
        if(Store.AvailableSeatsArr[seatno].blockedstatus=="1"){
            core.info("ChatManagerSIM.qml | __receivedInvitation | Seat Blocked | Seatno :"+seatno);
            return;
        }
        createPrivateSessionForInvitation()
        var serverSessionId = Store.PrivateSessionsArr["j"+__lastSessionId].serversessionid
        core.info("ChatManagerSIM.qml |__receivedInvitation | serverSessionId :"+serverSessionId);
        if(!Store.ReceivedInviteArr[seatno]){
            Store.ReceivedInviteArr[seatno] = serverSessionId;
        }else{
            var receivedInvite = Store.ReceivedInviteArr[seatno].split(",");
            if (receivedInvite.indexOf(serverSessionId)==-1) receivedInvite.push(serverSessionId);
            Store.ReceivedInviteArr[seatno] = receivedInvite.join(",");
        }
        __setEventInfo(0,{"serversessionid":serverSessionId,"seatnumber":seatno,"seatname":""})
        Store.creatorSeatNumber[serverSessionId]=seatno
    }

    function __loginUserToSeatChat(seatNumber,seatName,status){
        var sessionCount = __TotalPublicSessions - 1;
        var serverSessionId = Store.PublicSessionsArr["i"+sessionCount]["serversessionid"]
        if(!Store.AvailableSeatsArr[seatNumber.toUpperCase()]){
            core.info("ChatManagerSIM.qml | __receivedInvitation | Seatno not present | Adding Seatno"+seatNumber)
            Store.AvailableSeatsArr[seatNumber.toUpperCase()]={"seatnumber":seatNumber.toUpperCase(),"seatname":seatName,"state":"","status":status,"blockallinvites":"","blockedstatus":"0","avatar":""};
            __availableSeatsCount =__availableSeatsCount + 1;
        }
        core.info("ChatManagerNewSIM.qml | __loginUserToSeatChat | seatNumber:"+seatNumber+",seatName:"+seatName+"serveSessionId:"+serverSessionId);
        __setEventInfo(3,{"serversessionid":serverSessionId,"seatnumber":seatNumber,"seatname":seatName,"status":status});
    }

    function __exitSeatChatByUser(seatNumber){
        var sptr
        var dptr
        var ptr = Store.AvailableSeatsArr[seatNumber];
        for (var i in Store.PrivateSessionsArr){
            sptr = Store.PrivateSessionsArr[i]["buddies"]
            core.info("ChatManagerSIM.qml | __addSeatChatMessage called | Number of Buddies :"+sptr.length+" in Session : "+i)
            for (var j=0; j<sptr.length;j++){
                if(sptr[j].seatnumber == seatNumber){
                    core.info("ChatManagerSIM.qml | __addSeatChatMessage called | This is entered Seatno "+seatNumber+" Session"+i);
                    sptr.splice(j,1);
                    __setEventInfo(4,{"serversessionid":i,"seatnumber":seatNumber,"seatname":ptr.seatname});
                    break;
                }
            }
        }
        for (i in Store.PublicSessionsArr){
            sptr = Store.PublicSessionsArr[i]["buddies"]
            core.info("ChatManagerSIM.qml | __addSeatChatMessage called | Number of Buddies :"+sptr.length+" in Session : "+i)
            for ( j=0; j<sptr.length;j++){
                if(sptr[j].seatnumber == seatNumber){
                    core.info("ChatManagerSIM.qml | __addSeatChatMessage called | This is entered Seatno "+seatNumber+" Session"+i);
                    sptr.splice(j,1);
                    __setEventInfo(4,{"serversessionid":i,"seatnumber":seatNumber,"seatname":ptr.seatname});
                    break;
                }
            }
        }
    }

    function __leavePrivateSession(serverSessionId,seatno) {
        seatno = seatno.toUpperCase();
        if (Store.PrivateSessionsArr[serverSessionId]==undefined) return false;
        var sptr = Store.AvailableSeatsArr[seatno];
        var ptr = Store.PrivateSessionsArr[serverSessionId]["buddies"];
        var dptr = Store.PrivateSessionsArr[serverSessionId]["numofparticipants"];
        for (var i=0;i<ptr.length;i++)
            if (ptr[i].seatnumber==seatno) {
                ptr.splice(i,1);
                core.info("ptr.length: "+ptr.length);
                core.info("buddies.length: "+Store.PrivateSessionsArr[serverSessionId]["buddies"].length);
                Store.PrivateSessionsArr[serverSessionId].numofparticipants = Store.PrivateSessionsArr[serverSessionId]["buddies"].length
                __setEventInfo(4,{"serversessionid":serverSessionId,"seatnumber":sptr.seatnumber,"seatname":sptr.seatname});
                return true;
            }
        return true;
    }

    function acceptPrivateInvitation (serverSessionId,sessionid,seatno){
        core.info("ChatManagerSIM.qml | acceptInvitation called | Seatno :"+seatno,+" serverSessionId :"+serverSessionId);
        seatno = seatno.toUpperCase();
        if(!Store.ReceivedInviteArr[seatno])
            core.info("ChatManagerSIM.qml | __chatInvitationAccepted | Seat number does not exist: "+seatno)
        var receivedInvite = Store.ReceivedInviteArr[seatno].split(",")
        var index = receivedInvite.indexOf(serverSessionId)
        if (index == -1){
            core.info("ChatManagerSIM.qml | acceptInvitation | serverSessionId Not present "+serverSessionId);
            return;
        }
        if (!Store.AvailableSeatsArr[seatno]){
            Store.AvailableSeatsArr[seatno]={"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
            __availableSeatsCount =__availableSeatsCount + 1;
        }
        Store.PrivateSessionsArr[serverSessionId]["buddies"].push(Store.AvailableSeatsArr[seatno]);
        receivedInvite.splice(index,1);
        Store.ReceivedInviteArr[seatno] = receivedInvite.join(",");
        if(Store.creatorSeatNumber[serverSessionId])__setEventInfo(10,{"serversessionid":serverSessionId,"creatorseatnumber":Store.creatorSeatNumber[serverSessionId]});
        else __setEventInfo(10,{"serversessionid":serverSessionId,"creatorseatnumber":""});
    }

    function declinePrivateInvitation (serverSessionId,sessionid,seatno){
        core.info("ChatManagerSIM.qml | declineInvitation called | Seatno :"+seatno+ " serverSessionId :"+serverSessionId);
        seatno = seatno.toUpperCase();
        if(!Store.ReceivedInviteArr[seatno])
            core.info("ChatManagerSIM.qml | declineInvitation | Seat number does not exist: "+seatno);
        var receivedInvite = Store.ReceivedInviteArr[seatno].split(",")
        var index = receivedInvite.indexOf(serverSessionId)
        if (index == -1){
            core.info("ChatManagerSIM.qml | declineInvitation | serverSessionId Not present "+serverSessionId);
            return;
        }
        if(!Store.PrivateSessionsArr[serverSessionId]){
            core.info("ChatManagerSIM.qml | declineInvitation | Session does not exist: "+serverSessionId);
            continue;
        }
        if (!Store.AvailableSeatsArr[seatno]){
            Store.AvailableSeatsArr[seatno]={"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
        __availableSeatsCount =__availableSeatsCount + 1;
        }
        core.info("ChatManagerSIM.qml | declineInvitation | Seatno: "+seatno+"Serversessionid :"+serverSessionId);
        receivedInvite.splice(index,1);
        delete Store.PrivateSessionsArr[serverSessionId];
        __TotalPrivateSessions = __TotalPrivateSessions - 1;
        Store.ReceivedInviteArr[seatno] = receivedInvite.join(",");
        __setEventInfo(7,{"serversessionid":serverSessionId});
        __setEventInfo(10,{"serversessionid":serverSessionId})
    }

    function sendPrivateInvitation (serverSessionId,sessionid,seatno,message){
        core.info("ChatManagerSIM.qml | sendInvitation called | serverSessionId :"+serverSessionId+" Seatno: "+seatno);
        if(!Store.PrivateSessionsArr[serverSessionId]){
            core.info("ChatManagerSIM.qml | sendInvitation | Session does not exist: "+serverSessionId);return;}
        if (!Store.AvailableSeatsArr[seatno.toUpperCase()]){
            Store.AvailableSeatsArr[seatno.toUpperCase()]={"seatnumber":seatno.toUpperCase(),"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
            __availableSeatsCount =__availableSeatsCount + 1;
        }
        if(!Store.SendInviteArr[seatno]){
            Store.SendInviteArr[seatno] = serverSessionId;
        }else{
            var sendInvite = Store.SendInviteArr[seatno].split(",");
            if (sendInvite.indexOf(serverSessionId)==-1) sendInvite.push(serverSessionId);
            Store.SendInviteArr[seatno] = sendInvite.join(",");
        }
    }

    function getTotalSessionCount(){
        var totalSessionCount = __TotalPublicSessions + __TotalPrivateSessions;
        return totalSessionCount
    }
    function getPrivateSessionCount(){
        return __TotalPrivateSessions;
    }

    function getPublicSessionList (from,to) {
        return Store.PublicSessionsArr;
    }

    function getSessionParticipantCount(serverSessionId,sessionid){
        return Store.PublicSessionsArr[serverSessionId]?Store.PublicSessionsArr[serverSessionId]["buddies"].length:Store.PrivateSessionsArr[serverSessionId]?Store.PrivateSessionsArr[serverSessionId]["buddies"].length:0;
    }

    function getSessionParticipantList (serverSessionId,sessionid,to,from) {
        return Store.PublicSessionsArr[serverSessionId]?Store.PublicSessionsArr[serverSessionId]["buddies"]:Store.PrivateSessionsArr[serverSessionId]?Store.PrivateSessionsArr[serverSessionId]["buddies"]:[];
    }

    /* Deprecated
        function getTotalBuddyCount (sessionid) {
        if (!Store.PrivateSessionsArr[sessionid]){
            core.info("ChatManagerSIM.qml | getTotalBuddyCount | Session does not exist: "+sessionid);
            return 0;
        }
        core.info("ChatManagerSIM.qml | getTotalBuddyCount | Sessionid :"+sessionid+" Number of buddies :"+Store.PrivateSessionsArr[sessionid]["buddies"].length)
        return Store.PrivateSessionsArr[sessionid]["buddies"].length;
    }
*/

    function getHashValue(arrayPointer,arrayKey) {
        return (arrayPointer[arrayKey]!=undefined)?arrayPointer[arrayKey]:"";
    }

    function getBlockedUserCount(){
        return Store.BlockedSeatsArr!=[]?Store.BlockedSeatsArr.length:0
    }


    function getBlockedUserList (to,from) {
        return Store.BlockedSeatsArr;
    }

    /* Deprecated
    function getTotalMessageCount (sessionid) {
        return Store.ChatHistoryArr[sessionid].length;
    }
*/
    function sendMessageToSession (serverSessionId,sessionid,message) {
        return __addChatMessageToSeat(serverSessionId,__seatNo,message);
    }

    function __addSeatChatMessage (seatno,message) {
        core.info("ChatManagerSIM.qml | __addSeatChatMessage | Seatno :"+seatno+" Message :"+message)
        var sptr
        var dptr
        for (var i in Store.PrivateSessionsArr){
            sptr = Store.PrivateSessionsArr[i]["buddies"]
            core.info("ChatManagerSIM.qml | __addSeatChatMessage called | Number of Buddies :"+sptr.length+" in Session : "+i)
            for (var j=0; j<sptr.length;j++){
                if(sptr[j].seatnumber == seatno){
                    core.info("ChatManagerSIM.qml | __addSeatChatMessage called | This is entered Seatno "+seatno+" Session"+i);
                    __addChatMessage(i,seatno.toUpperCase(),message)
                    break;
                }
            }
        }
        for (var k in Store.PublicSessionsArr){
            sptr = Store.PublicSessionsArr[k]["buddies"]
            core.info("ChatManagerSIM.qml | __addSeatChatMessage called | Number of Buddies :"+sptr.length+" in Session : "+k)
            for (var l=0; l<sptr.length;l++){
                if(sptr[l].seatnumber == seatno){
                    core.info("ChatManagerSIM.qml | __addSeatChatMessage called | This is entered Seatno "+seatno+" Session"+k);
                    __addChatMessage(k,seatno.toUpperCase(),message)
                    break;
                }
            }
        }
    }

    function __addChatMessage (serverSessionId,seatno,message) {
        var ptr = Store.BlockedSeatsArr;
        for (var i=0;i<ptr.length;i++) if (ptr[i].seatnumber==seatno) return false;
        if(!Store.tmpArr) Store.tmpArr = [];
        Store.ChatHistoryArr[serverSessionId].push({"seatnumber":seatno,"time":new Date(),"seatname":Store.AvailableSeatsArr[seatno].seatname,"txtmessage":message});
        if(Store.PrivateSessionsArr[serverSessionId]){
        Store.PrivateSessionsArr[serverSessionId].numofunreadmsgs = Store.PrivateSessionsArr[serverSessionId].numofunreadmsgs + 1;
        Store.tmpArr.push({"serversessionid":serverSessionId,"seatnumber":seatno,"message":message})
        } else if(Store.PublicSessionsArr[serverSessionId] && Store.joinedPublicSessions[serverSessionId]){
            Store.PublicSessionsArr[serverSessionId].numofunreadmsgs = Store.PublicSessionsArr[serverSessionId].numofunreadmsgs + 1;
            Store.tmpArr.push({"serversessionid":serverSessionId,"seatnumber":seatno,"message":message})
            }
        Store.CurrentChatArr.push(Store.ChatHistoryArr[serverSessionId].length-1);
        addChatMessageTimer.restart();
    }

       function __addChatMessageToSeat (serverSessionId,seatno,message) {
        var ptr = Store.BlockedSeatsArr;
        for (var i=0;i<ptr.length;i++) if (ptr[i].seatnumber==seatno) return false;
        Store.ChatHistoryArr[serverSessionId].push({"seatnumber":seatno,"time":new Date(),"seatname":Store.AvailableSeatsArr[seatno].seatname,"txtmessage":message});
        if(Store.PrivateSessionsArr[serverSessionId]){
        Store.PrivateSessionsArr[serverSessionId].numofunreadmsgs = Store.PrivateSessionsArr[serverSessionId].numofunreadmsgs + 1;}
        else if(Store.PublicSessionsArr[serverSessionId]){
            Store.PublicSessionsArr[serverSessionId].numofunreadmsgs = Store.PublicSessionsArr[serverSessionId].numofunreadmsgs + 1;
        }
        Store.CurrentChatArr.push(Store.ChatHistoryArr[serverSessionId].length-1);
    }
    function __addChatMessageTriggered () {
        if (!Store.tmpArr) return;
        var ptr = Store.tmpArr;
        var sptr
        for (var i=0;i<ptr.length;i++){
            core.info("__addChatMessageTriggered"+ptr[i].serversessionid)
            sptr = Store.ChatHistoryArr[ptr[i].serversessionid][Store.ChatHistoryArr[ptr[i].serversessionid].length-1];
            __setEventInfo(2,{"serversessionid":ptr[i].serversessionid,"seatnumber":ptr[i].seatnumber,"seatname":sptr.seatname,"txtmessage":ptr[i].message,"time":sptr.time});     // Chat Message Received
        }
        Store.tmpArr = [];
    }

    function getSessionHistory(serverSessionId,sessionid) {
        core.info("getChatHistory: "+serverSessionId);
        Store.CurrentChatArr = [];
        if (!Store.ChatHistoryArr[serverSessionId]) return Store.CurrentChatArr;
        var ptr = Store.ChatHistoryArr[serverSessionId];
        for (var i=0;i<ptr.length;i++) Store.CurrentChatArr.push(ptr[i]);
        return Store.CurrentChatArr;
    }

    function blockUser(seatno) {
        seatno = seatno.toUpperCase()
        core.info("ChatManagerSIM.qml | blockSeat called: "+seatno);
        var ptr = Store.BlockedSeatsArr;
        for (var i=0;i<ptr.length;i++) if (ptr[i].seatnumber==seatno) return false;
        Store.AvailableSeatsArr[seatno]["blockedstatus"]="1";
        ptr.push({"seatnumber":seatno,"seatname":(Store.AvailableSeatsArr[seatno].seatname)?Store.AvailableSeatsArr[seatno].seatname:"","status":(Store.AvailableSeatsArr[seatno].status)?Store.AvailableSeatsArr[seatno].status:"","avatar":(Store.AvailableSeatsArr[seatno].avatar)?Store.AvailableSeatsArr[seatno].avatar:"","blockedstatus":(Store.AvailableSeatsArr[seatno].blockedstatus)?Store.AvailableSeatsArr[seatno].blockedstatus:"0"});
        if (!Store.blocktmp) Store.blocktmp=[];
        Store.blocktmp.push(seatno);
        blockSeatTimer.restart();
    }

    function __blockSeatTriggered() {
        if (!Store.blocktmp) return;
        var ptr = Store.blocktmp;
        for (var i=0;i<ptr.length;i++) __setEventInfo(35,{"blockseatnumber":ptr[i]});
        Store.blocktmp = []
    }

    function unblockUser(seatno) {
        seatno = seatno.toUpperCase();
        core.info("ChatManagerSIM.qml | unblockSeat called: "+seatno);
        var ptr = Store.BlockedSeatsArr;
        for (var i=0;i<ptr.length;i++)
            if (ptr[i].seatnumber==seatno) {
                ptr.splice(i,1);
                Store.AvailableSeatsArr[seatno]["blockedstatus"]="0";
                if (!Store.unblocktmp) Store.unblocktmp=[];
                Store.unblocktmp.push(seatno);
                unblockSeatTimer.restart();
                return true;
            }
        return false;
    }

    function __unblockSeatTriggered(){
        if (!Store.unblocktmp) return;
        var ptr = Store.unblocktmp;
        for (var i=0;i<ptr.length;i++) __setEventInfo(36,{"unblockseatnumber":ptr[i]});
        Store.unblocktmp = [];
    }

    function setUserAlias(aliasname){
        core.info("ChatManagerSIM.qml | setAlias called: "+aliasname);
        __updateSeatInfo("alias",__seatNo,aliasname);
    }

    function setUserAvatar(avatar){
         core.info("ChatManagerSIM.qml | setUserAvatar called: "+avatar);
         __updateSeatInfo("avatar",__seatNo,avatar)
    }
/*Depracted
    function setUserState(seatState){
        core.info("ChatManagerSIM.qml | setUserState called: "+seatState);
        __updateSeatInfo("state",__seatNo,seatState);
    }
*/
    function setUserStatus(status){
        core.info("ChatManagerSIM.qml | setUserStatus called: "+status);
        __updateSeatInfo("status",__seatNo,status);
    }

    function blockInvites (){
        core.info("ChatManagerSIM.qml | blockInvites called ");
        __updateSeatInfo("allinvites",__seatNo,"1");
    }

    function unblockInvites (){
        core.info("ChatManagerSIM.qml | unblockInvites called ");
        __updateSeatInfo("allinvites",__seatNo,"0");
    }

    function __blockInvitesTriggered(blockInvitesStatus){
        core.info("ChatManagerSIM.qml | __blockInvitesTriggered called ");
        if(blockInvitesStatus=="1") __setEventInfo(37,{"responsestatus":"OK"});
        else if(blockInvitesStatus=="0") __setEventInfo(38,{"responsestatus":"OK"});
    }

    function requestSessionHistory(serverSessionId,sessionId){
        core.info("ChatManagerSIM.qml | requestSessionHistory called | serverSessionId :"+serverSessionId);
        __currentSessionId=serverSessionId //See if needed coz FE sets it
        requestSessionHistoryTimer.restart();
    }

    function __requestSessionHistoryOnTriggered(){
        core.info("ChatManagerSIM.qml | __requestSessionHistoryOnTriggered called")
     __setEventInfo(32,{"serversessionid":__currentSessionId});
    }

    function __updateSeatInfo(purpose,seatno,value){
        core.info("ChatManagerNewSIM.qml | __updateSeatInfo | purpose: "+purpose+", value:"+value)
        if (Store.AvailableSeatsArr[seatno]==undefined) Store.AvailableSeatsArr[seatno] = {"seatnumber":seatno,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
        if (purpose=="alias") Store.AvailableSeatsArr[seatno].seatname=value;
        else if (purpose=="state") Store.AvailableSeatsArr[seatno].state=value;
        else if (purpose=="status") Store.AvailableSeatsArr[seatno].status=value;
        else if (purpose=="allinvites"){Store.AvailableSeatsArr[seatno].blockallinvites=value;blockInvitesTimer.blockInvitesStatus=value;blockInvitesTimer.restart();return}
        else if (purpose=="avatar")Store.AvailableSeatsArr[seatno].avatar=value
        if(updateSeatInfoTimer.running){
            updateSeatInfoTimer2.reason=purpose;
            updateSeatInfoTimer2.restart();
        }else{
            updateSeatInfoTimer.reason=purpose;
            updateSeatInfoTimer.restart();
        }
    }

    function __updateSeatInfoTriggered(){
        core.info("ChatManagerNewSIM.qml | __updateSeatInfoTriggered :purpose :"+updateSeatInfoTimer.reason)
        if(updateSeatInfoTimer.reason=="alias")__setEventInfo(27,{"responsestatus":"OK"});
        else if(updateSeatInfoTimer.reason=="state")__setEventInfo(28,{"responsestatus":"OK"});
        else if(updateSeatInfoTimer.reason=="status")__setEventInfo(29,{"responsestatus":"OK"});
        else if(updateSeatInfoTimer.reason=="avatar")__setEventInfo(30,{"responsestatus":"OK"});
    }
    function __updateSeatInfoTriggered2(){
        core.info("ChatManagerNewSIM.qml | __updateSeatInfoTriggered2 :purpose :"+updateSeatInfoTimer2.reason)
        if(updateSeatInfoTimer.reason=="alias")__setEventInfo(27,{"responsestatus":"OK"});
        else if(updateSeatInfoTimer.reason=="state")__setEventInfo(28,{"responsestatus":"OK"});
        else if(updateSeatInfoTimer.reason=="status")__setEventInfo(29,{"responsestatus":"OK"});
        else if(updateSeatInfoTimer.reason=="avatar")__setEventInfo(30,{"responsestatus":"OK"});
    }


    function leavePublicSession(serverSessionId,sessionid) {
        Store.ChatHistoryArr[serverSessionId]=[];
        delete Store.joinedPublicSessions[serverSessionId]
        if (serverSessionId==__currentSessionId) {
            Store.CurrentChatArr = [];
            __currentSessionId="";
        }
        exitPublicChatSessionTimer.serverSessionId = serverSessionId;
        exitPublicChatSessionTimer.restart()
    }

    function __leavePublicSessionTriggered() {
        console.log("__leavePublicSessionTriggered: "+exitPublicChatSessionTimer.serverSessionId)
        if (exitPublicChatSessionTimer.serverSessionId=="") return false;
        if (__leaveChatError) __setEventInfo(24,{"serversessionid":exitPublicChatSessionTimer.serverSessionId});
        else __setEventInfo(23,{"serversessionid":exitPublicChatSessionTimer.serverSessionId});
    }

    function leavePrivateSession (serverSessionId,sessionid) { // for private chat
        core.info("ChatManagerSIM.qml | exitChatSession called: "+serverSessionId);
        if(!Store.PrivateSessionsArr[serverSessionId])
            core.info("ChatManagerSIM.qml | exitChatSession called | Session not present");
        else{
            delete Store.PrivateSessionsArr[serverSessionId];
            delete Store.ChatHistoryArr[serverSessionId];
            __TotalPrivateSessions = __TotalPrivateSessions - 1;
        }
        exitChatSessionTimer.serverSessionId = serverSessionId;
        exitChatSessionTimer.restart()
    }

    function __exitChatSessionTriggered (){
        if (exitChatSessionTimer.serverSessionId=="") return false;
        __setEventInfo(7,{"serversessionid":exitChatSessionTimer.serverSessionId});
    }


    function resetUser() {
        core.info("ChatManagerSIM.qml | exitChat called");
        var ptr = Store.ChatHistoryArr;
        Store.CurrentChatArr = [];
        for (var i in ptr) ptr[i]=[];
        var sptr=Store.PrivateSessionsArr;
        for (var j in sptr){
            delete Store.ChatHistoryArr[j];
        }
        Store.PrivateSessionsArr = [];
        Store.joinedPublicSessions = [];
        __currentSessionId="";
        __TotalPrivateSessions = 0;
        __lastSessionId = __TotalPrivateSessions - 1;
    }
    function resetUnreadMsgCount(serverSessionId){
        core.info("ChatManagerSIM.qml | resetUnreadMsgCount | serverSessionId :"+serverSessionId);
        (Store.PublicSessionsArr[serverSessionId])?(Store.PublicSessionsArr[serverSessionId]["numofunreadmsgs"]=0):(Store.PrivateSessionsArr[serverSessionId])?(Store.PrivateSessionsArr[serverSessionId]["numofunreadmsgs"]=0):(Store.PrivateSessionsArr[serverSessionId]["numofunreadmsgs"]=0)

    }

    function loginUserSeatNumber(seatno) {
        __seatNo = seatno.toUpperCase();
        appActiveTimer.restart();
        return true;
    }

    function sendConciergeMsg(message){
        core.info("ChatManagerSIM.qml | sendConciergeMsg called")
        return true;
    }

    function __setActiveTriggered() {
        if (!__skipDataPopulation) {
            __initialiseArrData();
            __readSimData();
        } else __populateSimData();
    }

    function createPrivateSession(){
        core.info("ChatManagerSIM.qml | createNewSession | Last Session id: "+__lastSessionId);
        __lastSessionId = __lastSessionId +1;
        Store.PrivateSessionsArr["j"+__lastSessionId] ={
            "serversessionid":"j"+__lastSessionId,
            "name":"",
            "buddies":[],
            "numofunreadmsgs":0
        }
        Store.ChatHistoryArr["j"+__lastSessionId] = [];
        __TotalPrivateSessions = __TotalPrivateSessions +1;
        createSessionTimer.restart();
        return "j"+__lastSessionId;
    }
    function createPrivateSessionForInvitation(){
        core.info("ChatManagerSIM.qml | createPrivateSessionForInvitation | Last Session id: "+__lastSessionId);
        __lastSessionId = __lastSessionId +1;
        Store.PrivateSessionsArr["j"+__lastSessionId] ={
            "serversessionid":"j"+__lastSessionId,
            "name":"",
            "buddies":[],
            "numofunreadmsgs":0
        }
        Store.ChatHistoryArr["j"+__lastSessionId] = [];
        __TotalPrivateSessions = __TotalPrivateSessions +1;
        createPrivateSessionForInvitationTimer.restart();
        return "j"+__lastSessionId;
    }

    function __createNewSessionForInvitationTriggered(){
     // This Event is deprecated
   __setEventInfo(6,{"serversessionid":Store.PrivateSessionsArr["j"+__lastSessionId].sessionid});
    }
    function __createNewSessionTriggered(){
     /* This Event is deprecated
   __setEventInfo(6,{"serversessionid":Store.PrivateSessionsArr["j"+__lastSessionId].sessionid});
   */
     __setEventInfo(20,{"serversessionid":Store.PrivateSessionsArr["j"+__lastSessionId].serversessionid})
    }

    function requestAllUserList (){
        refreshAllChatUsersTimer.restart();
    }

    function __refreshAllChatUsersOnTrigerred (){
        __setEventInfo(25,{});
    }

    function getAllChatUserCount(){
        console.log("*******************************__availableSeatsCount"+__availableSeatsCount)
        return __availableSeatsCount
    }

    function getAllChatUserListWithRange (to,from){
        return Store.AvailableSeatsArr;
    }

    function removeUserFromPrivateSession(serverSessionId,seatnumber){
        removeUserFromPrivateSessionTimer.serverSessionId=serverSessionId;
        removeUserFromPrivateSessionTimer.seatNumber=seatnumber;
        removeUserFromPrivateSessionTimer.restart();
    }

    function __removeUserFromPrivateSessionTriggered(serverSessionId,seatnumber){
        var sptr
        var dptr = Store.AvailableSeatsArr[seatnumber];
        for (var i in Store.PrivateSessionsArr){
            sptr = Store.PrivateSessionsArr[i]["buddies"]
            for (var j=0; j<sptr.length;j++){
                if(sptr[j].seatnumber == seatnumber){
                    core.info("ChatManagerSIM.qml | __addSeatChatMessage called | This is entered Seatno "+seatnumber+" Session"+i);
                    sptr.splice(j,1);
                    __setEventInfo(4,{"serversessionid":serverSessionId,"seatnumber":seatnumber,"seatname":dptr.seatname});
                    break;
                }
            }
        }
    }



    function joinPublicSession(serverSessionId,sessionid) {
        if (Store.PublicSessionsArr[serverSessionId]==undefined) return false;
        __currentSessionId = serverSessionId;
        Store.joinedPublicSessions[serverSessionId]=serverSessionId
        joinPublicChatTimer.restart();
        return true;
    }

    function __joinPublicChatTriggered() {
        if (__joinChatError) __setEventInfo(22,{"serversessionid":__currentSessionId});
        else __setEventInfo(21,{"serversessionid":__currentSessionId,"sessionname":Store.PublicSessionsArr[__currentSessionId].name});
        __setEventInfo(10,{"serversessionid":__currentSessionId});
    }

    function requestPublicSessions(){
        requestPublicSessionsTimer.restart();
    }

    function __requestPublicSessionsTriggered(){
        __setEventInfo(33,{"responsestatus":"OK"});
    }

    function requestUserProfile(){
        requestUserProfileTimer.restart();
    }

    function __requestUserProfileTriggered(){
        var sptr = Store.AvailableSeatsArr[__seatNo];
        __setEventInfo(18,{"seatnumber":sptr.seatnumber,"seatname":sptr.seatname,"state":sptr.state,"status":sptr.status,"blockallinvites":sptr.blockallinvites,"blockstatus":sptr.blockstatus,"avatar":sptr.avatar});
    }

    function requestUserBuddies(){
        requestUserBuddiesTimer.restart();
    }

    function __requestUserBuddiesTriggered(){
        __setEventInfo(9,{"responsestatus":"OK"});
    }

    function requestUserPrivateSessions(){
        requestUserPrivateSessionsTimer.restart();
    }

    function __requestUserPrivateSessionsTriggered(){
        __setEventInfo(34,{});
        __setEventInfo(17,{});
    }

    function __setEventInfo(eventid,eventlist) {
        for (var i in Store.EventInfoArr) Store.EventInfoArr[i]=(eventlist[i]!=undefined)?eventlist[i]:"";
        var s=[];
        for (var i in Store.EventInfoArr) s.push(i+": "+Store.EventInfoArr[i]);
        core.info("ChatManagerSIM.qml | __setEventInfo | eventid: "+eventid+" EventInfo: "+s.join(','));
        chatEvent(eventid,Store.EventInfoArr);
    }

    function __initialiseArrData() {
        Store.PublicSessionsArr = [];
        Store.PrivateSessionsArr = [];
        Store.AvailableSeatsArr = [];
        Store.ChatHistoryArr = [];
        Store.CurrentChatArr = [];
        Store.EventInfoArr = {"sessionid":"","seatnumber":"","seatname":"","state":"","status":"","txtmessage":"","time":"","sessionname":"","blockallinvites":"","serversessionid":"","avatar":"","responsestatus":"","blockseatnumber":"","unblockseatnumber":"","creatorseatnumber":""};
        Store.BlockedSeatsArr = [];
        Store.SendInviteArr = [];
        Store.ReceivedInviteArr = [];
        Store.joinedPublicSessions=[];
        Store.creatorSeatNumber=[];
    }

    function __readSimData () {
        fileRequest.source = "";
        fileRequest.source = core.settings.intPath+"config/seatChatPC.json";
    }

    function __populateSimData () {
        core.info("ChatManagerSIM.qml | chatManagerSIM | populateSimData");
        if (!__skipDataPopulation) __populateSessions();

        if (!__skipDataPopulation) __populateAvailableBuddies();
        var sptr = Store.AvailableSeatsArr[__seatNo];

        if (!__skipDataPopulation) __populateBuddiesPerSession();
        /* These Events do not come  using loginUserSeatNumber()
        for (var i=0; i<__TotalPublicSessions; i++) __setEventInfo(10,{"serversessionid":"i"+i});
        for (var j=0; j<__TotalPrivateSessions;j++) __setEventInfo(10,{"serversessionid":"j"+j})
        __setEventInfo(9,{});
        __setEventInfo(8,{});
        __setEventInfo(33,{});
        __setEventInfo(43,{});
        __setEventInfo(8,{});
        __setEventInfo(34,{});
        __setEventInfo(18,{"seatnumber":sptr.seatnumber,"seatname":sptr.seatname,"state":sptr.state,"status":sptr.status,"blockallinvites":sptr.blockallinvites,"blockstatus":sptr.blockstatus,"avatar":sptr.avatar});
        __setEventInfo(25,{});
        __setEventInfo(17,{});*/

    }

    function __populateAvailableBuddies () {
        var ptr= fileRequest.value; var sptr;
        Store.BlockedSeatsArr=[];
        Store.AvailableSeatsArr = {};
        if (!ptr.AvailableSeats || !(ptr.AvailableSeats.length>0)) return false;
        ptr= fileRequest.value.AvailableSeats;
        for (var i=0; i<ptr.length; i++) {
            if (!ptr[i].seatnumber) continue;
            sptr = ptr[i];
            Store.AvailableSeatsArr[sptr.seatnumber.toUpperCase()] = {
                "seatnumber":sptr.seatnumber.toUpperCase(),
                "seatname":sptr.seatname?sptr.seatname:"",
                                          "state":sptr.state?sptr.state:"",
                                                              "status":sptr.status?sptr.status:"",
                                                                                    "blockallinvites":sptr.blockallinvites?sptr.blockallinvites:"",
                                                                                                                            "blockedstatus":sptr.blockstatus?sptr.blockstatus:"0",
                "avatar":sptr.avatar?sptr.avatar:""
            }
            if (sptr.blockstatus) Store.BlockedSeatsArr.push({"seatnumber":sptr.seatnumber.toUpperCase(),"seatname":(sptr.seatname)?sptr.seatname:"","status":(sptr.status)?sptr.status:"","avatar":(sptr.avatar)?sptr.avatar:"","blockedstatus":(sptr.blockstatus)?sptr.blockstatus:"0"});
            __availableSeatsCount = __availableSeatsCount + 1;
        }
        if (Store.AvailableSeatsArr[__seatNo]==undefined) Store.AvailableSeatsArr[__seatNo] = {"seatnumber":__seatNo,"seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""};
        core.info("ChatManagerSIM.qml | Available Seats Length: "+ptr.length);
        core.info("ChatManagerSIM.qml | Blocked Seats length: "+Store.BlockedSeatsArr.length);
    }

    function __populateBuddiesPerSession () {
        var ptr= fileRequest.value; var sptr; var dptr;
        for (var i=0; i<__TotalPublicSessions; i++) {
            if (ptr.PublicChatRooms && ptr.PublicChatRooms[i] && ptr.PublicChatRooms[i].seatnumbers) {
                sptr = ptr.PublicChatRooms[i].seatnumbers;
                for (var s=0; s<sptr.length; s++) {
                    if (Store.AvailableSeatsArr[sptr[s]]==undefined) Store.AvailableSeatsArr[sptr[s]] = {
                            "seatnumber":sptr[s].toUpperCase(),
                            "seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""}
                    Store.PublicSessionsArr["i"+i].buddies.push(Store.AvailableSeatsArr[sptr[s]]);
                }
            }
            Store.PublicSessionsArr["i"+i].numofparticipants = Store.PublicSessionsArr["i"+i]["buddies"].length
        }
        for (var j=0; j< __TotalPrivateSessions;j++){
            if (ptr.PrivateChatRooms && ptr.PrivateChatRooms[j] && ptr.PrivateChatRooms[j].seatnumbers){
                dptr = ptr.PrivateChatRooms[j].seatnumbers;
                for (var k=0; k<dptr.length;k++){
                    if (Store.AvailableSeatsArr[dptr[k]]==undefined) Store.AvailableSeatsArr[dptr[k]] = {
                            "seatnumber":dptr[k].toUpperCase(),
                            "seatname":"","state":"","status":"","blockallinvites":"","blockedstatus":"0","avatar":""}
                    Store.PrivateSessionsArr["j"+j].buddies.push(Store.AvailableSeatsArr[dptr[k]]);
                }
            }
        }
    }

    function __populateSessions () {
        var ptr= fileRequest.value; var sptr;
        Store.PublicSessionsArr = {};
        Store.PrivateSessionsArr = {};
        Store.ChatHistoryArr = [];
        if (ptr.PublicChatCount) __TotalPublicSessions=ptr.PublicChatCount;
        else if (ptr.PublicChatRooms && ptr.PublicChatRooms.length) __TotalPublicSessions=ptr.PublicChatRooms.length;
        for (var i=0; i<__TotalPublicSessions; i++) {
            Store.ChatHistoryArr["i"+i] = [];
            Store.PublicSessionsArr["i"+i] = {
                "serversessionid":"i"+i,
                "name": ptr.PublicChatRooms && ptr.PublicChatRooms[i] && ptr.PublicChatRooms[i].name?ptr.PublicChatRooms[i].name:"Chat "+i,
                                                                                                      "buddies":[],
                                                                                                      "numofunreadmsgs":0,
                                                                                                      "numofparticipants":0

                }
        }
        core.info("ChatManagerSIM.qml | __populateSessions | __TotalPublicSessions: "+__TotalPublicSessions);
        if (ptr.PrivateChatCount) __TotalPrivateSessions=ptr.PrivateChatCount;
        else if (ptr.PrivateChatRooms && ptr.PrivateChatRooms.length) __TotalPrivateSessions=ptr.PrivateChatRooms.length;
        for (var j=0; j<__TotalPrivateSessions; j++){
            Store.ChatHistoryArr["j"+j] = [];
            Store.PrivateSessionsArr["j"+j] = {
                "serversessionid":"j"+j,
                "name":ptr.PrivateChatRooms && ptr.PrivateChatRooms[j] && ptr.PrivateChatRooms[j].name?ptr.PrivateChatRooms[j].name:"",
                                                                                                        "buddies":[],
                                                                                                        "numofunreadmsgs":0
            }
        }
        __lastSessionId = __TotalPrivateSessions -1 ;
        core.info("ChatManagerSIM.qml | __populateSessions | __TotalPrivateSessions: "+__TotalPrivateSessions);
    }

    Component.onCompleted: {
        core.info("ChatManagerSIM.qml | chatManagerSIM | Component loaded: "+core.settings.intPath);
        __initialiseArrData();
    }
    Configuration{
        id: fileRequest;
        onStatusChanged:{
            __errorCode=0;
            if(status==Pif.Ready && fileRequest.source!=''){
                core.info("ChatManagerSIM.qml | chatManager | Configuration onStatusChanged | Data Ready");
                __populateSimData();
                __skipDataPopulation=true;
            }else if(status==Pif.Error && fileRequest.source!=''){
                core.info("ChatManagerSIM.qml | chatManager | Configuration onStatusChanged | ErrorStatus: "+status+" ERROR reading file: "+source);
                __errorCode=1;
            }
        }
    }
    Timer { id:appActiveTimer;interval:50; onTriggered: __setActiveTriggered();}
    Timer { id:joinPublicChatTimer;interval:100; onTriggered: __joinPublicChatTriggered();}
    Timer { id:addChatMessageTimer;interval:50;onTriggered: __addChatMessageTriggered();}
    Timer { id:blockSeatTimer;interval:100; onTriggered: __blockSeatTriggered();}
    Timer { id:unblockSeatTimer;interval:100; onTriggered: __unblockSeatTriggered();}
    Timer { id:updateSeatInfoTimer;interval:100; property string reason:""; onTriggered: __updateSeatInfoTriggered();}
    Timer { id:updateSeatInfoTimer2;interval:100; property string reason:""; onTriggered: __updateSeatInfoTriggered2();}
    Timer { id:createSessionTimer;interval:50;onTriggered: __createNewSessionTriggered();}
    Timer { id:getSessionTimer;interval:50;onTriggered: __getUsrSessionsTriggered();}
    Timer { id:refreshAllChatUsersTimer;interval:50;onTriggered: __refreshAllChatUsersOnTrigerred();}
    Timer { id:exitChatSessionTimer;interval:50; property string serverSessionId:""; onTriggered: __exitChatSessionTriggered();}
    Timer { id:exitPublicChatSessionTimer;interval:50; property string serverSessionId:""; onTriggered: __leavePublicSessionTriggered();}
    Timer { id:requestSessionHistoryTimer;interval:50;onTriggered: __requestSessionHistoryOnTriggered();}
    Timer { id:createPrivateSessionForInvitationTimer;interval:50;onTriggered: __createNewSessionForInvitationTriggered();}
    Timer { id:requestPublicSessionsTimer;interval:50;onTriggered: __requestPublicSessionsTriggered();}
    Timer { id:requestUserProfileTimer;interval:50;onTriggered: __requestUserProfileTriggered();}
    Timer { id:requestUserBuddiesTimer;interval:50;onTriggered: __requestUserBuddiesTriggered();}
    Timer { id:requestUserPrivateSessionsTimer;interval:50;onTriggered: __requestUserPrivateSessionsTriggered();}
    Timer { id:blockInvitesTimer;interval:50;property string blockInvitesStatus:""; onTriggered: __blockInvitesTriggered(blockInvitesStatus);}
    Timer {id: removeUserFromPrivateSessionTimer;interval:50; property string serverSessionId:""; property string seatNumber:""; onTriggered:__removeUserFromPrivateSessionTriggered(serverSessionId,seatNumber)}
}
