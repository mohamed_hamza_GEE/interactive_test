import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    property int destinationCounter: 0;
    property int distanceCounter: 0;
    property int arrivalCounter: 0;

    function __updatingTimeToDestination(){
        core.debug("TransientDataSimulation.qml | __updatingTimeToDestination called | destinationCounter: "+destinationCounter);
        if(pif.getSimulationData().TimeToDestination && destinationCounter!=0 && pif.__getPifDataRef().TimeToDestination!=0){
            core.info("TransientDataSimulation.qml | __updatingTimeToDestination called ");
            pif.__getPifDataRef().TimeToDestination = pif.__getPifDataRef().TimeToDestination - 1;
            var timeToDestination = [pif.__getPifDataRef().TimeToDestination];
            pif.eventFlightData("FLTDATA_TIME_TO_DESTINATION",timeToDestination,false);
            core.info("TransientDataSimulation.qml | __updatingTimeToDestination | TimeToDestination :"+timeToDestination[0])
        }
        destinationCounter = destinationCounter + 1;
    }

    function __computingTimeAtDestination(){
        core.debug("TransientDataSimulation.qml | __computingTimeAtDestination called")
        if(pif.getSimulationData().GmtOffsetOfDestination){
            core.info("TransientDataSimulation.qml | __computingTimeAtDestination called")
            var date = new Date()
            var gmtTimeStamp = date.getTime() + date.getTimezoneOffset()*60*1000
            var timeStampAtDestination = gmtTimeStamp + (3600000*pif.__getPifDataRef().GmtOffsetOfDestination) + (3600000*pif.__getPifDataRef().DstOfDestination)
            var d = new Date(timeStampAtDestination);
            var formattedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
            var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
            var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
            pif.__getPifDataRef().TimeAtDestination = String(hours + "" + minutes);
            var timeAtDestination = [pif.__getPifDataRef().TimeAtDestination];
            core.info("TransientDataSimulation.qml | __computingTimeAtDestination | TimeAtDestination : "+timeAtDestination)
            pif.eventFlightData("FLTDATA_TIME_AT_DESTINATION",timeAtDestination,false)
        }
    }

    function __computingTimeAtOrigin(){
        core.debug("TransientDataSimulation.qml | __computingTimeAtOrigin called")
        if(pif.getSimulationData().GmtOffsetOfSource){
            core.info("TransientDataSimulation.qml | __computingTimeAtOrigin called")
            var date = new Date()
            var gmtTimeStamp = date.getTime() + date.getTimezoneOffset()*60*1000
            var timeStampAtOrigin = gmtTimeStamp + (3600000*pif.__getPifDataRef().GmtOffsetOfSource) + (3600000*pif.__getPifDataRef().DstOfSource)
            var d = new Date(timeStampAtOrigin);
            var formattedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
            var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
            var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
            pif.__getPifDataRef().TimeAtOrigin = String(hours + "" + minutes);
            var timeAtOrigin = [pif.__getPifDataRef().TimeAtOrigin];
            core.info("TransientDataSimulation.qml | __computingTimeAtOrigin | TimeAtOrigin : "+timeAtOrigin)
            pif.eventFlightData("FLTDATA_TIME_AT_ORIGIN",timeAtOrigin,false)
        }
    }

    function __computingTimeSinceTakeOff(){
        core.debug("TransientDataSimulation.qml | __computingTimeSinceTakeOff called ")
        if(pif.getSimulationData().TimeToDestination){
            core.info("TransientDataSimulation.qml | __computingTimeSinceTakeOff called ")
            var min = core.coreHelper.convertHMSToMin(pif.__getPifDataRef().TimeSinceTakeOff.substring(0,2)+":"+pif.__getPifDataRef().TimeSinceTakeOff.substring(2,4))
            min = min + 1;
            if(min <= pif.getSimulationData().TimeToDestination){
                var hm = core.coreHelper.getTimeFormatFromSecs(min*60,4)
                var timeSincetakeOff = hm.replace(":","");
                core.info("TransientDataSimulation.qml | __computingTimeSinceTakeOff | TimeSinceTakeOff :"+timeSincetakeOff);
                pif.eventFlightData("FLTDATA_TIME_SINCE_TAKEOFF",[timeSincetakeOff],false)
            }
        }
    }

    function __computingDistanceToDestination(){
        core.debug("TransientDataSimulation.qml | __computingDistanceToDestination called ")
        if(pif.getSimulationData().DistToDestination && distanceCounter!=0){
            core.info("PifData.qml | __computingDistanceToDestination called")
            pif.__getPifDataRef().DistToDestination = Math.floor((pif.__getPifDataRef().TrueAirSpeed*pif.__getPifDataRef().TimeToDestination)/60)
            if(pif.__getPifDataRef().DistToDestination >= 0){
                var distanceToDestination = [pif.__getPifDataRef().DistToDestination]
                core.info("PifData.qml | __computingDistanceToDestination | DistToDestination :"+distanceToDestination)
            } else{
                pif.__getPifDataRef().DistToDestination = 0;
                distanceToDestination = [pif.__getPifDataRef().DistToDestination]
                core.info("PifData.qml | __computingDistanceToDestination | DistToDestination :"+pif.__getPifDataRef().DistToDestination)
            }
            pif.eventFlightData("FLTDATA_DISTANCE_TO_DESTINATION",distanceToDestination,false)
        }
        distanceCounter = distanceCounter + 1;
    }

    function __computingArrivalTime(){
        core.info("TransientDataSimulation.qml | __computingArrivalTime called ");
        if(pif.getSimulationData().TimeToDestination && arrivalCounter==0){
            var date = new Date()
            var gmtTimeStamp = date.getTime() + date.getTimezoneOffset()*60*1000
            var g = new Date(gmtTimeStamp);
            var arrivalGMTTimeStamp = gmtTimeStamp + (pif.getSimulationData().TimeToDestination*60000)
            var timeStampAtDestination = arrivalGMTTimeStamp + (3600000*pif.__getPifDataRef().GmtOffsetOfDestination) + (3600000*pif.__getPifDataRef().DstOfDestination)
            var d = new Date(timeStampAtDestination);
            var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
            var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
            core.debug("Cuurent Time At Destination :"+hours+":"+minutes)
            pif.__getPifDataRef().ArrivalTime = hours+""+minutes
            var arrivalTime = [pif.__getPifDataRef().ArrivalTime]
            core.info("TransientDataSimulation.qml | __computingArrivalTime called | ArrivalTime :"+arrivalTime);
            pif.eventFlightData("FLTDATA_ESTIMATED_ARRIVAL_TIME",arrivalTime,false)
        }
        arrivalCounter = arrivalCounter + 1;
    }

    function __getCTMessages(){
        core.info("TransientDataSimulation.qml | __getCTMessages ")
        dataController.getCtToSeatMessage(["all"],__ctMsgDataReady);
    }

    function __ctMsgDataReady(dmodel){
        core.info("TransientDataSimulation.qml | __ctMsgDataReady | dmodel.count :"+dmodel.count)
        for(var i=0; i<dmodel.count;i++){
            Store.ctMid[i]=dmodel.getValue(i,"seat_message_id");
            Store.ctMessageTimeout[i]=dmodel.getValue(i,"seat_message_duration");
        }
        Store.modelCount=dmodel.count;
    }

    function __getCtMid(index){
        return Store.ctMid[index];
    }

    function __getCtMessageTimeout(index){
        return Store.ctMessageTimeout[index];
    }

    function __modelCount(){
        return Store.modelCount;
    }

    function __computeGameExitAppTimeout(){
        core.info("TransientDataSimulation.qml | __computeGameExitAppTimeout | GameTimeout :"+pif.getSimulationData().GameTimeout)
        if(pif.getSimulationData().GameTimeout){
            gameExitTimer.interval=pif.getSimulationData().GameTimeout*1000
            gameExitTimer.start()
        }
    }
    Component.onCompleted: {
        core.info("TransientDataSimulation.qml| Component loaded ");
        simulationTimer.restart();
        Store.ctMid = [];
        Store.ctMessageTimeout = [];
        Store.modelCount=0;
        __getCTMessages();
    }

    Connections{
        target:pif.launchApp;
        onSigAppExit:{
            if(gameExitTimer.running)gameExitTimer.stop();
        }
    }
    Timer {id:simulationTimer; interval:60000; repeat:true ;triggeredOnStart:true;
        onTriggered:{__updatingTimeToDestination();
            __computingTimeAtDestination();
            __computingTimeAtOrigin();
            __computingTimeSinceTakeOff();
            __computingDistanceToDestination();
            __computingArrivalTime();
        }
    }
    Timer {id:gameExitTimer ; repeat:false ;triggeredOnStart:false;onTriggered:pif.launchApp.onLauncherExitSignal();}
}
