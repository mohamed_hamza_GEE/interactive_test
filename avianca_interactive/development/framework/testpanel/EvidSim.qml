import QtQuick 1.1
import "../components"
import "../blank.js" as Store

Item{
    property bool connected: true;
    signal messageReceived(string command,string value);
    signal connectedChanged(bool connected);

    Connections {
        target: core.viewController.testPanel?core.viewController.testPanel:null;
        onSigTestPanelMessage:{
            if (reason!="evidInterface") return false;
            core.info("EvidInterface.qml | onSigTestPanelMessage | parameter1: "+parameter1+" parameter2: "+parameter2);
            switch(parameter1){
            case "Blue_Ray_Control":{
                send(pif.getLruData("Blue_Ray_Control"),parameter2)
                break;
            }
            case "lhtVolume":{
                send(pif.getLru1DArrayData("APA_VIDs"),parameter2)
                break;
            }
            case "lhtBrightness":{
                send(pif.getLruData("Monitor_Brightness_Control_Type"),parameter2)
                break;
            }
            case "lhtContrast":{
                send(pif.getLruData("Monitor_Contrast_VID"),parameter2)
                break;
            }

            case "lhtBacklight":{
                send(pif.getLruData("Monitor_Power_VID"),parameter2)
                break;
            }

            case "custom":{
                var tmp=parameter2.split(",");
                send(tmp[0],tmp[1])
                break;
            }

            }
        }
    }

    //Public Function
    function connectToServer(evid,value){
        core.info("EvidSim.qml | connectToServer");
        connectedChanged(true);
    }

    function requestValue(evid){
        for(var x in Store.arr){
            if(Store.arr[x]["vid"]==evid){
                core.info("EvidSim.qml | requestValue | FOUND - vid: "+evid+" -- "+x+" -- "+Store.arr[x]['value']);
                messageReceived(evid,Store.arr[x]['value']);
                return Store.arr[x]['value'];
            }
        }

        if(Store.arr[evid]){
            core.info("EvidSim.qml | requestValue | FOUND1 - vid: "+evid+" -- "+Store.arr[evid]);
            messageReceived(evid,Store.arr[evid]);
            return true;
        }else {core.info("EvidSim.qml | requestValue | NOT FOUND - vid: "+evid);}
    }

    function send(evid,value){
        core.info("EvidSim.qml | send | "+evid+" :: "+value);
        for(var x in Store.arr){
            if(Store.arr[x]["vid"]==evid){
                core.info("EvidSim.qml | send | FOUND - vid: "+evid+" -- "+x+" -- "+Store.arr[evid]);
                Store.arr[x]['value']=value;
                messageReceived(evid,value);
                return true;
            }
        }
        Store.arr[evid]=value;
        messageReceived(evid,value);
    }

    function setRowMapping(rowMap){
        //eg : Store.replaceSeatRow=[];Store.replaceSeatRow['User3']=[];Store.replaceSeatRow['User3']['1b']='s10'
        core.info("EvidSim.qml | setRowMapping()");
        Store.replaceSeatRow=rowMap;
        Store.arr = [];
        __loadVidJson();
    }

    //Private functions
    function __loadVidJson(){
        core.info("EvidSim.qml | __loadVidJson()");
        var getParams = '../../config/evid.json';
        jsonLoader.getLocalData(getParams);
    }

    function __readJsonObject(errCode,responseObj){
        core.info("EvidSim.qml | __readJsonObject() called errCode: " + errCode);
        var seatClass;
        if(errCode==0 && responseObj){
            seatClass = pif.getCabinClassName();
            core.info("EvidSim.qml | Reading vids of class: " + seatClass);
            for(var x in responseObj[seatClass]){

                Store.arr[responseObj[seatClass][x].name] = new Array();
                Store.arr[responseObj[seatClass][x].name]["name"] = responseObj[seatClass][x].name;

                var tmpVid=responseObj[seatClass][x].vid;
                if(tmpVid.indexOf("*SEAT_ROW*")>=0){
                    if(Store.replaceSeatRow && Store.replaceSeatRow[pif.getCabinClassName()] && Store.replaceSeatRow[pif.getCabinClassName()][pif.getSeatNumber()]){
                        tmpVid=tmpVid.replace("S*SEAT_ROW*",Store.replaceSeatRow[pif.getCabinClassName()][pif.getSeatNumber()]);
                    }else {
                        tmpVid=tmpVid.replace("*SEAT_ROW*",pif.getSeatRow());
                    }
                }

                Store.arr[responseObj[seatClass][x].name]["vid"] = tmpVid
                Store.arr[responseObj[seatClass][x].name]["type"] = responseObj[seatClass][x].type;
                Store.arr[responseObj[seatClass][x].name]["templateId"] = responseObj[seatClass][x].templateId;
                if(responseObj[seatClass][x].validInputs){
                    Store.arr[responseObj[seatClass][x].name]["validInputs"] = responseObj[seatClass][x].validInputs.toString();
                }
                else{Store.arr[responseObj[seatClass][x].name]["validInputs"] = -1;}
                if(responseObj[seatClass][x].min)Store.arr[responseObj[seatClass][x].name]["min"] = responseObj[seatClass][x].min;
                else Store.arr[responseObj[seatClass][x].name]["min"] = -1;
                if(responseObj[seatClass][x].max)Store.arr[responseObj[seatClass][x].name]["max"] = responseObj[seatClass][x].max;
                else Store.arr[responseObj[seatClass][x].name]["max"] = -1;
                if(responseObj[seatClass][x].step)Store.arr[responseObj[seatClass][x].name]["step"] = responseObj[seatClass][x].step;
                else Store.arr[responseObj[seatClass][x].name]["step"] = -1;
                if(responseObj[seatClass][x].defaultVal)Store.arr[responseObj[seatClass][x].name]["defaultVal"] = responseObj[seatClass][x].defaultVal;
                else Store.arr[responseObj[seatClass][x].name]["defaultVal"] = -1;

                Store.arr[responseObj[seatClass][x].name]['value']=Store.arr[responseObj[seatClass][x].name]["defaultVal"];
            }
        }

        var evidVolume = pif.getLru1DArrayData("APA_VIDs");
        if (evidVolume){
            for(var i = 0; i < evidVolume.length; i++)
                if (evidVolume[i]!=="") Store.arr[evidVolume[i]]=11; // LHT volume range is 0-33, hence the middle is 11
        }

        var evidBrightness = pif.getLruData("Monitor_Brightness_Control_Type");
        if (evidBrightness){ Store.arr[evidBrightness]=50;}

        var evidContrast = pif.getLruData("Monitor_Contrast_VID");
        if (evidContrast){ Store.arr[evidContrast]=50;}

        var evidScreenOn = pif.getLruData("Monitor_Power_VID");
        if (evidScreenOn) Store.arr[evidScreenOn]="ON";

        var monitorVid = pif.getLruData("Monitor_VID");
        if (monitorVid){Store.arr[monitorVid]="COMPONENT"}


        var blueRayControl = pif.getLruData("Blue_Ray_Control");
        Store.arr[blueRayControl]="STOP";
        var blueRayMediumType = pif.getLruData("Blue_Ray_Medium_Type");
        Store.arr[blueRayControl]="NONE";

        for(var index1 in Store.arr){
            if(Store.arr[index1]["name"]){
                for(var index2 in Store.arr[index1]){
                    core.info("EvidSim.qml | __readJsonObject() | "+index1+" : "+index2+" = "+Store.arr[index1][index2]);
                }
            }else {
                core.info("EvidSim.qml | __readJsonObject() | "+index1+" = "+Store.arr[index1]          );
            }
        }
    }

    function __vidMessageReceived(command, value){
        core.info("EvidSim.qml | __vidMessageReceived() command: " + command + " value:" + value);
        for(var x in Store.arr){
            if(Store.arr[x]['vid'] == command)Store.arr[x]['value'] = value;

            if(Store.arr[x]['type'] == "light_control")lightStateChanged(Store.arr[x]['name'],value);
            else if(Store.arr[x]['type'] == "window_shade")windowShadeStateChanged(Store.arr[x]['name'],value);
            else if(Store.arr[x]['type'] == "camera")cameraZoomChanged(Store.arr[x]['name'],value);
            else if(Store.arr[x]['type'] == "audio_control")audioStateChanged(Store.arr[x]['name'],value);

        }
    }


    Timer {
        id: t;
        repeat: false;
        interval: 1000;
        running: false;
        onTriggered: {}
    }


    DataLoader {
        id: jsonLoader;
        onSigLocalDataReady:{__readJsonObject(errCode,responseObj);}
    }

    Component.onCompleted: {
        __loadVidJson();Store.arr = [];Store.replaceSeatRow=[];
    }
}
