import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store
SharedModel{
    id:smClient
    isClient:true;
    serverHostname:"127.0.0.1"
    property variant clientMessage:{"requestToShare":false,"__metaData":{},"__messageIdentifier":"","__messageText":"","__senderSeatNo":"","reason":"","invitationStatus":false,"status":"","imgName":"","imgPath":"","seatNo":"","imgText":"","__imgName":"","__imgPath":"","__imgText":"","senderSeatUsbDisconnect":false,"getImage":false,"img":"","image":""};
    property variant messageToServer:{"messageType":"","sharingStatus":"","reason":""};
    property variant clientProperties: {"name":"","reason":"","serverHostname":"","imgName":"","imgPath":""}
    property string intendedSeat: "";
    property variant imgMessage;
    property bool usbClientConnected: false;
    property string imgReceiverSeat: "";

    signal sigMessageSharingDataReceived(variant client,variant msgObj);
    signal sigUsbSharingClientConnected();
    signal sigUsbSharingDataReceived (variant client,variant msgObj)

    onIntendedSeatChanged: {
        core.info("NetworkClientSim.qml | onIntendedSeatChanged | intendedSeat:"+intendedSeat)
    }

    onClientPropertiesChanged: {
        core.info("NetworkClientSim.qml | onClientPropertiesChanged | clientProperties:"+clientProperties)
        client.properties = clientProperties
    }

    onClientMessageChanged: {
        core.info("NetworkClientSim.qml | clientMessage: "+clientMessage+",intendedSeat :"+intendedSeat+"Own Seat:"+pif.getSeatNumber())
        if(clientMessage.reason==""){core.info("NetworkClientSim.qml | Message empty: | returning "); return }
        if(__getSeatNumber(intendedSeat).toUpperCase()==pif.getSeatNumber().toUpperCase()){
            messageToServer = {"messageType":"","sharingStatus":"","reason":""};
            if(clientMessage.reason=="USBSharing"){
                console.log("NetworkClientSim.qml | onClientMessageChanged | reason :"+clientMessage.reason)
                sigUsbSharingDataReceived(client,clientMessage);
            } else if  (clientMessage.reason=="MessageSharing"){
                core.info("NetworkClientSim.qml | onClientMessageChanged | reason :"+clientMessage.reason)
                sigMessageSharingDataReceived(client,clientMessage);
            }
        }
    }

    onImgMessageChanged: {
        core.info("NetworkClientSim.qml | onImgMessageChanged | imgReceiverSeat :"+__getSeatNumber(imgReceiverSeat)+"Own Seat:"+pif.getSeatNumber());
        if(__getSeatNumber(imgReceiverSeat).toUpperCase()==pif.getSeatNumber().toUpperCase()){
            pif.usbImageSharing.__client3ReceivedAck(imgMessage,"image");
        }
    }

    /* onUsbClientConnectedChanged: {
        console.log("NetworkClientSim.qml | onUsbClientConnectedChanged | usbClientConnected: "+usbClientConnected)
        if(usbClientConnected==true)sigUsbSharingClientConnected();
        usbClientConnected=false;
    }*/

    Item{
        id:client;
        property variant properties;
        function send(message){
            core.info("NetworkClientSim.qml | send |messageType: "+message.messageType+"sharingStatus: "+message.sharingStatus+", reason: "+message.reason)
            messageToServer = message
        }

        function sendMessage(type,name){
            console.log("NetworkClientSim.qml | sendMessage  | type :"+type+", intendedSeat: "+intendedSeat)
            imgMessage = name
        }
    }

    function __getSeatNumber(seatNo){
        return seatNo.replace("seat00","").replace("seat0","");
    }
    function __generateJsonMessage(result){
        var retJsonMsg = result;
        return JSON.stringify(retJsonMsg);
    }
}


