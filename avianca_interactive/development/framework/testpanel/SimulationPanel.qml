import QtQuick 1.1
import '../blank.js' as ButtonList
import 'SimulationFunctions.js' as FunctionList

Item{
    id:testlinksRect
    x:5;
    property int pid:0;
    property int baseY:0;
    property string backName;
    property int index:0;
    property string aspectValue: "";
    signal sigTestPanelMessage(string reason, string parameter1, string parameter2, string parameter3, string parameter4,string parameter5);

    function hideLinks(){for (var j in ButtonList.btnHolder) ButtonList.btnHolder[j]['obj'].destroy();}
    function loadCustLinks(){pid=999; loader.source="../../view/TestLinks.qml";}
    function isNodeAvailable(node){for(var i in ButtonList.menuHierarchy) if(ButtonList.menuHierarchy[i]['name']==node) return ButtonList.menuHierarchy[i]['availability']==1?true:false;}

    function showLinks(LinkId){
        baseY=0;
        if(!LinkId) pid=0;
        else{ hideLinks(); pid=LinkId;}

        var count = 0; ButtonList.btnHolder=[];
        var str=ButtonList.menuHierarchy[pid]['childlist'].toString(); var tmpArr=str.split(','); var parentName=ButtonList.menuHierarchy[pid]['name'];

        for (var j in tmpArr){
            if(isNodeAvailable(tmpArr[j])){
                var qmlObj1 = compBtn.createObject(btn); var len=ButtonList.btnHolder.length;
                ButtonList.btnHolder[len]=[]; ButtonList.btnHolder[len]["obj"]=qmlObj1;
                qmlObj1.index=j; qmlObj1.linkLabel=assignLabel(tmpArr[j]); qmlObj1.name=tmpArr[j];

                if(count==0) qmlObj1.x=backLink.width + backLink.x + 10;
                else{
                    var qmlObj2 = eval(ButtonList.btnHolder[count-1]["obj"]);
                    var p =  qmlObj2.width + qmlObj2.x + 10 +qmlObj1.width
                    if(p >=core.width){
                        qmlObj1.y=qmlObj2.y+35
                        baseY=qmlObj1.y
                        qmlObj1.x=backLink.width + backLink.x + 20
                    }else{
                        qmlObj1.x = qmlObj2.width + qmlObj2.x + 10
                        qmlObj1.y=baseY
                    }
                }
                count++; qmlObj1.opacity=1
            }
        }
    }

    function assignLabel(node){
        var loadfunction = "FunctionList."+node+"_onLoad";
        if (eval(loadfunction)) return eval(loadfunction)();
        for(var i in ButtonList.menuHierarchy){
            if(ButtonList.menuHierarchy[i]['name']==node) {
                return ButtonList.menuHierarchy[i]['aliasName']!=undefined?ButtonList.menuHierarchy[i]['aliasName']:ButtonList.menuHierarchy[i]['name'];
            }
        }
    }

    function showMenuHierarchy(){
        for(var k in ButtonList.menuHierarchy){
            core.debug('testpanel | ButtonList.menuHierarchy['+k+'][name]: '+ButtonList.menuHierarchy[k]['name']);
            core.debug('testpanel | ButtonList.menuHierarchy['+k+'][childlist]: '+ButtonList.menuHierarchy[k]['childlist']);
            core.debug('testpanel | ButtonList.menuHierarchy['+k+'][availability]: '+ButtonList.menuHierarchy[k]['availability']);
            core.debug('testpanel | ButtonList.menuHierarchy['+k+'][parentid]: '+ButtonList.menuHierarchy[k]['parentid']);
            if(ButtonList.menuHierarchy[k]['aliasName']) core.debug('testpanel | ButtonList.menuHierarchy['+k+'][aliasName]: '+ButtonList.menuHierarchy[k]['aliasName']);
        }
    }

    function modifyAvailability(){

        for(var i in ButtonList.dynamicPifComp){ // structure array is updated per dynamic loading of components
            for(var j in ButtonList.menuHierarchy){
                if((ButtonList.dynamicPifComp[i]).toLowerCase()==(ButtonList.menuHierarchy[j]['name']).toLowerCase()){
                    ButtonList.menuHierarchy[j]['availability']=1;
                    var parentId = ButtonList.menuHierarchy[j]['parentid'];
                    do{
                        ButtonList.menuHierarchy[parentId]['availability']=1;
                        parentId=ButtonList.menuHierarchy[parentId]['parentid'];
                    }while(parentId!=0)
                }
            }
        }

        for(var ic in ButtonList.menuHierarchy){ // if avialability of child count is zero, then make parent's availability to zero
            if(ButtonList.menuHierarchy[ic]['childlist']!=''){
                var cstr=ButtonList.menuHierarchy[ic]['childlist'].split(','); var availCount=0;
                for (var child in cstr){
                    for(var nic in ButtonList.menuHierarchy){
                        if(cstr[child]==ButtonList.menuHierarchy[nic]['name']){
                            if(ButtonList.menuHierarchy[nic]['availability']==0) availCount++;
                        }
                    }
                }
                if(availCount==cstr.length) ButtonList.menuHierarchy[ic]['availability']=0;
            }
        }
    }

    Component.onCompleted: {
        core.debug('test');
        var panelData = coreHelper.getConfigFileData("testpanel/buttonlist.ini","lru");
        ButtonList.ButtonArr = coreHelper.updateCfgArray (panelData.Buttons);

        var configData= coreHelper.getConfigFileData("../config/config.ini","lru");
        ButtonList.dynamicPifComp=coreHelper.cfgVariantObjToArray(configData.PifCompList);

        if(((ButtonList.ButtonArr[0]).toString()).split(',')[1]==0){showTestlinksBtn.opacity=0; return;}
        ButtonList.menuHierarchy = [];
        for (var i in ButtonList.ButtonArr) {
            var arr=ButtonList.ButtonArr[i]; var str=arr.toString(); var tmpArr=str.split(',');

            if (tmpArr.length>0){

                for(var nj in tmpArr) {
                    if(nj>=2){
                        var pname=tmpArr[0]; var name; var aliasPresent; var aliasArr;
                        if((tmpArr[nj].toString()).indexOf(':')!=-1){ aliasArr=(tmpArr[nj].toString()).split(':'); name=aliasArr[0]; aliasPresent=true;}
                        else {name=tmpArr[nj]; aliasPresent=false;}

                        var structLen=ButtonList.menuHierarchy.length;
                        ButtonList.menuHierarchy[structLen]=[];
                        ButtonList.menuHierarchy[structLen]["availability"]=1;
                        ButtonList.menuHierarchy[structLen]["name"]=name;
                        ButtonList.menuHierarchy[structLen]["childlist"]='';
                        if(aliasPresent) ButtonList.menuHierarchy[structLen]['aliasName']=aliasArr[1];
                        ButtonList.menuHierarchy[0]["parentid"]=-1;

                        for(var ni in ButtonList.ButtonArr){
                            var newArr=ButtonList.ButtonArr[ni]; var newStr=newArr.toString(); var newTmpArr=newStr.split(','); var childListArr=[];
                            if(newTmpArr.length>0 && name==newTmpArr[0]){
                                for(var cl=2; cl<newTmpArr.length; cl++){ if((newTmpArr[cl].toString()).indexOf(':')!=-1){ var cArr=(newTmpArr[cl].toString()).split(':'); childListArr.push(cArr[0]);} else childListArr.push(newTmpArr[cl]);}
                                ButtonList.menuHierarchy[structLen]["childlist"]=(ButtonList.menuHierarchy[structLen]["childlist"]!=''?ButtonList.menuHierarchy[structLen]["childlist"]+',':'')+childListArr.join(',');
                                ButtonList.menuHierarchy[structLen]["availability"]=newTmpArr[1]
                            }
                        }
                    }
                }
            }
        }

        for(var im in ButtonList.menuHierarchy){
            var arrName=ButtonList.menuHierarchy[im]["name"];
            for(var ij in ButtonList.menuHierarchy){
                var imChildStr=ButtonList.menuHierarchy[ij]["childlist"].toString(); var imChildArr=imChildStr.split(',');
                if(imChildArr.indexOf(arrName)!=-1) ButtonList.menuHierarchy[im]["parentid"]=ij;
            }
        }

        modifyAvailability();
    }

    function getUserInput(strName,displayStr,initialValue){
        var returnStatus=0; var returnValue='';
        initialValue = initialValue==undefined?"":initialValue;
        if(displayStr==undefined) displayStr="Enter Value"
        if(FunctionList.newObjectName=='' || FunctionList.newObjectName!=strName){
            if(FunctionList.newObject!=undefined) FunctionList.newObject.destroy();
            FunctionList.newObject = compTextEdit.createObject(testlinksRect);
            FunctionList.newObject.x=200
            FunctionList.newObject.y=200
            FunctionList.newObject.textArea1.text= displayStr + " = ";
            FunctionList.newObject.textArea2.text = initialValue;
            FunctionList.newObject.textArea2.cursorPosition=initialValue!=undefined?String(initialValue).length:0;
            FunctionList.newObjectName=strName
            returnStatus=0
        } else if(FunctionList.newObjectName==strName){
            core.debug('Value= '+FunctionList.newObject.textArea2.text);
            FunctionList.newObject.destroy();
            FunctionList.newObjectName=''
            returnStatus=1
            returnValue=FunctionList.newObject.textArea2.text
        }
        return {'status':returnStatus, 'value':returnValue}
    }

    function getPhsImage(source){
        phsImageContainer.visible=true;
        phsImage.source=source+"?id="+Math.random();
    }

    Loader { id: loader; focus: true}
    Rectangle{
        id: hidebutton
        x:5; radius: 10; opacity: 0;
        color: "#5C5C5C"; width: 30; height: 30;
        Text { id:backTxt2;anchors.centerIn: parent; color:'#FFFFFF'; text: '<b>[X]</b>';}
        MouseArea{
            anchors.fill: parent;
            drag.target: testlinksRect; drag.axis: Drag.XandYAxis; drag.minimumX: -(core.width - 50); drag.maximumX: core.width - 100; drag.minimumY: -(testlinksRect.height/2); drag.maximumY: core.height - (testlinksRect.height/2); drag.filterChildren: true;
            onClicked: { backName='';showTestlinksBtn.opacity=1; backLink.opacity=0; hidebutton.opacity=0; hideLinks(); loader.source='';}
        }
        Behavior on opacity{NumberAnimation{ duration : 600}}
    }
    Rectangle{
        id:showTestlinksBtn
        x: 5; radius: 10;
        color: "#5C5C5C"; width: showTestlinksBtnTxt.paintedWidth + 20; height: 30;
        Text { id:showTestlinksBtnTxt;anchors.centerIn: parent; color:'#FFFFFF'; text: "Show Testlinks" }
        MouseArea{
            anchors.fill: parent;
            drag.target: testlinksRect; drag.axis: Drag.XandYAxis; drag.minimumX: -(core.width - 50); drag.maximumX: core.width - 100; drag.minimumY: -(testlinksRect.height/2); drag.maximumY: core.height - (testlinksRect.height/2); drag.filterChildren: true;
            onClicked: {showTestlinksBtn.opacity=0; showLinks(0); backLink.opacity=1; hidebutton.opacity=1;}
        }
        Behavior on opacity{NumberAnimation{ duration : 600}}
    }
    Rectangle{
        id:backLink
        x:hidebutton.width+10
        radius: 10
        color: "#5C5C5C"; width: backTxt.paintedWidth + 20; height: 30;
        Text { id:backTxt;anchors.centerIn: parent; color:'#FFFFFF'; text: (pid==0)?"Hide Links":"Back" }
        opacity: 0
        MouseArea{
            anchors.fill: parent;
            drag.target: testlinksRect; drag.axis: Drag.XandYAxis; drag.minimumX: -(core.width - 50); drag.maximumX: core.width - 100; drag.minimumY: -(testlinksRect.height/2); drag.maximumY: core.height - (testlinksRect.height/2); drag.filterChildren: true;
            onClicked: {
                if(pid==0){backName=''; showTestlinksBtn.opacity=1;backLink.opacity=0;hidebutton.opacity=0;hideLinks()}
                else if(pid==999){ backName="custLinks"; loader.source=''; pid=0; showLinks();}
                else {hideLinks(); backName=ButtonList.menuHierarchy[pid]['name']; showLinks(ButtonList.menuHierarchy[pid]['parentid'])}
            }
        }
        Behavior on opacity{NumberAnimation{duration : 600}}
    }
    Rectangle{ id: btn;}

    Component{
        id:compBtn
        Rectangle {
            property string linkLabel: textId.text
            property alias hiddenText: textId.hiddenText;
            property string name; property int  index; property variant id;
            x:5; width: textId.paintedWidth + 30; height: 30; opacity: 0; radius: 5; color: "#5C5C5C";
            Text {id:textId ;anchors.centerIn: parent;text: linkLabel;color: "#FFFFFF"; property string hiddenText:"";}
            Behavior on opacity{NumberAnimation{duration : 600}}
            MouseArea{
                anchors.fill: parent;
                onClicked:{
                    for (var i in ButtonList.menuHierarchy)
                        if(ButtonList.menuHierarchy[i]['name']==name)
                            if(name=="custLinks"){ hideLinks(); loadCustLinks();}
                            else {
                                if(ButtonList.menuHierarchy[i]['childlist']!='') showLinks(i);
                                    else eval("var ret=FunctionList."+name+"_onClicked(textId); if(ret!=undefined && ButtonList.menuHierarchy[i]['aliasName']) ButtonList.menuHierarchy[i]['aliasName']=ret;");
                            }
                }
            }
        }
   }
    Component{
        id:compTextEdit
        Rectangle{
            id:textEdit;
            property alias textArea1:txt1;
            property alias textArea2:txt2;
            color :"black";
            width: txt1.width+r2.width+15; height: (r2.height>txt1.height?r2.height:txt1.height)+10;
            Text{id:txt1; color: "white"
                width: 100;
                anchors.left: parent.left; anchors.leftMargin: 5;
                height:30;
                anchors.verticalCenter: parent.verticalCenter;
                horizontalAlignment:Text.AlignRight;
                verticalAlignment: Text.AlignVCenter;
                wrapMode:Text.Wrap;
            }
            Rectangle{ id:r2; color: "white";
                anchors.left: txt1.right;
                anchors.leftMargin: 5;
                width: txt2.paintedWidth>txt2.width?txt2.paintedWidth:txt2.width;
                height: txt2.paintedHeight>txt2.height?txt2.paintedHeight:txt2.height;
                anchors.verticalCenter: parent.verticalCenter;
                TextEdit{ id:txt2; width: 80; height: 30; color:"blue";
                    anchors.left: r2.left;
                    cursorVisible: true;
                    focus: true;
                }
            }
        }
    }
    Rectangle{
        id:phsImageContainer
        visible:false;
        anchors.topMargin:20;
        anchors.top: showTestlinksBtn.bottom;
        width:phsImage.width;height: phsImage.height+textId1.height
        Text {id:textId1 ;anchors.top: parent.top;text: "PHS Image";}
        Image {
            id: phsImage
            anchors.bottom: parent.bottom;
            source: ""
        }
    }
    Connections{target: pif.premiumHandset; onLcdRefreshed:{getPhsImage(phsImgUrl)}  }
}
