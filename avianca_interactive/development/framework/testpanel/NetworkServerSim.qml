import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

SharedModel{
    id:sharedModelServer
    isServer:true;
    width: 100
    height:100;

    property variant clientMessage: {"requestToShare":false,"__metaData":{},"__messageIdentifier":"","__messageText":"","__senderSeatNo":"","reason":"","invitationStatus":false,"status":"","imgName":"","imgPath":"","seatNo":"","imgText":"","__imgName":"","__imgPath":"","__imgText":"","senderSeatUsbDisconnect":false,"getImage":false,"img":"","image":""};
    property variant messageToServer: {"messageType":"","sharingStatus":"","reason":"","imgName":"","imgPath":""};
    property variant clientProperties: {"name":"","reason":"","serverHostname":""}
    property string intendedSeat: "";
    property variant imgMessage;
    property bool usbClientConnected: false;
    property string imgReceiverSeat: "";

    onNetworkProtocolMessageReceived:{
        console.log("NetworkServerSim.qml | onNetworkProtocolMessageReceived | message :"+message)
        clientMessage = {"requestToShare":false,"__metaData":{},"__messageIdentifier":"","__messageText":"","__senderSeatNo":"","reason":"","invitationStatus":false,"status":"","imgName":"","imgPath":"","seatNo":"","imgText":"","__imgName":"","__imgPath":"","__imgText":"","senderSeatUsbDisconnect":false,"getImage":false,"img":"","image":""};
        var msg = eval('(' + message + ')');
        if(msg.messageType=="init"){
            client.properties = {"name":msg.name,"reason":msg.reason,"serverHostname":msg.serverHostname};
            clientProperties = client.properties
            intendedSeat = client.properties.serverHostname
            client.send({messageType: "initAck"});
            if(msg.reason=="USBSharing")usbClientConnected=true;
        }else if(msg.reason=="USBSharing"){
            console.log("NetworkServerSim.qml | received message from USBSharing client. message is " + message);
            Store.client = client
            if(msg.getImage){
                imgReceiverSeat=__getHostName(client.properties.name)
            }
            sharedModelServer.clientMessage = msg
        }
        else if(msg.reason=="MessageSharing"){
            console.log("NetworkServerSim.qml | onNetworkProtocolMessageReceived |client :"+client+" message " + message);
            Store.client = client
            console.log("msg.requestToShare :"+msg.requestToShare+",msg.invitationStatus"+msg.invitationStatus)
            sharedModelServer.clientMessage = msg
        }
    }
    onSynchronized:{
        console.log("NetworkServerSim.qml | onSynchronized | remoteClients length: "+sharedModelServer.remoteClients.length)
    }

    onMessageToServerChanged: {
        if(messageToServer.reason==""){console.log("NetworkServerSim.qml |onMessageToServerChanged | No Message received | returning "); return;}
        console.log("NetworkServerSim.qml | onMessageToServerChanged :"+messageToServer+", Store.client:"+Store.client)
        Store.client.send(messageToServer)
    }

    function __getSeatNumber(seatNo){
        return seatNo.replace("seat00","").replace("seat0","");
    }

    function __getHostName(seatNo){
        if(seatNo.length==2) seatNo="seat00"+seatNo;
        else if(seatNo.length==3) seatNo="seat0"+seatNo;
        else seatNo="seat"+seatNo;
        return seatNo;
    }
}




