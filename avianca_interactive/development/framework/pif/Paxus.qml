import QtQuick 1.1
import Panasonic.Paxus 1.0

// PURPOSE: To Interface to PIF plugin for Paxus Logging
// For immediate verification whether loggin worked try reading usageTable of /tmp/paxusClientCache.db SQLite Db on seat.
// seat core stores is data on seat for sometime before it sends it on fs/cs ~core/paxus3air/usage/paxus3air_usage.cs1

Item {
    id: paxusLog
    property alias currentContent: paxus.activeApp;
    property alias currentGame: paxus.activeGame;

    property bool playlistPlay: false;
    Paxus { id: paxus}

    // Logging Interactive screen. Used to know the screens PAX navigated. There is no start/end in this. Requires a string - screen name.
    function interactiveScreenLog(screenName) {
        core.debug("Paxus.qml | interactiveScreenLog called");
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if(screenName == undefined){core.info("Paxus.qml | Screen name not provided, exiting..."); return false;}
        if(!core.pc) paxus.interactiveScreen = screenName;
        core.info("Paxus.qml | Screen name logged: "+screenName);
    }

    // Logging Interactive language. Interactive Language that PAX selected. Should not tag language that interactive defaults on welcome screen before language is selected. Takes only three-letter iso code.
    function interactiveLanguageLog(langiso) {
        core.debug("Paxus.qml | interactiveLanguageLog called");
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if (langiso == undefined){core.info("Paxus.qml | Language ID not provided, exiting ..."); return false;}
        if (!core.pc) paxus.interactiveLanguage = langiso;
        core.info("Paxus.qml | Interactive language logged: " + langiso);
    }

    // Log Start time when PAX entered an applications or content. This is for non-media content. It takes string - content or app name. This API should follow with Log Stop time (stopContentLog). However, if it doesn't then core internally calls stop before logging start of next app.
    function startContentLog(contentid) {
        core.debug("Paxus.qml | Start content log called");
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if (contentid == undefined || contentid == ""){core.info("Paxus.qml | Content ID not provided, exiting..."); return false;}
        core.info("Paxus.qml | Values received content ID: " + contentid);
        if(!core.pc){
            if (currentContent != "") stopContentLog();
            paxus.activeApp = contentid;
        }
        core.info("Paxus.qml | Content start logged: " + contentid);
    }

    // Log Stop time when PAX leaves an applications or content for which start content log was called. This is for non-media content. It takes string - content or app name. It should always be after start log time call.
    function stopContentLog() {
        core.debug("Paxus.qml | Stop content log called for content: " + currentContent);
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if (!core.pc){
            if (currentContent == "") return; // Nothing to stop logging.
            paxus.activeApp = '';
        }
        core.info("Paxus.qml | CONTENT STOP LOGGED");
    }

    function startGameLog(gamename){
        core.debug("Paxus.qml | Start game log called gamename : "+gamename);
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if (gamename == undefined || gamename == ""){core.info("Paxus.qml | Game Name not provided, exiting..."); return false;}
        core.info("Paxus.qml | Values received content ID: " + gamename);
        if(!core.pc){
            if (currentGame != "") stopGameLog();
            paxus.activeGame = gamename;
        }
        core.info("Paxus.qml | GAME start logged: " + gamename);
    }

    function stopGameLog(){
        core.debug("Paxus.qml | Stop game log called for game: " + currentGame);
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if (!core.pc){
            if (currentGame == "") return; // Nothing to stop logging.
            paxus.activeGame = '';
        }
        core.info("Paxus.qml | GAME STOP LOGGED");
    }

    // Log called when track is added for the first time into playlist. Incase of multiple playlist, call the same API again.
    function playlistCreatedLog() {
        core.debug("Paxus.qml | Playlist created log called");
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if(!core.pc) paxus.playlistState = Paxus.CreatedPlaylist;
        core.info("Paxus.qml | Playlist created logged");
    }

    // Log called when playlist is played. Incase of multiple playlist, call the same API.
    function playlistPlayLog() {
        core.debug("Paxus.qml | Playlist play log called");
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if(!core.pc){
            if (playlistPlay == true) playlistStopLog();
            paxus.playlistState = Paxus.StartedPlaylist;
        }
        playlistPlay = true;
        core.info("Paxus.qml | Playlist play logged");
    }

    // Log called when playlist is stopped. Incase of multiple playlist, call the same API.
    function playlistStopLog() {
        core.debug("Paxus.qml | Playlist stop log called");
        if(!pif.getPaxusServiceStatus()){core.info("Paxus.qml | Paxus service is disabled.Exiting"); return false;}
        if(!core.pc){
            if (playlistPlay==true) paxus.playlistState = Paxus.StoppedPlaylist;
        }
        playlistPlay=false;
        core.info("Paxus.qml | Playlist stop logged");
    }

    // Initialize paxus App (if needed)
    function frmwkInitApp(){core.info("Paxus.qml | Initialise app called");}

    // Reset paxus App
    function frmwkResetApp(){core.info("Paxus.qml | Reset app called"); playlistStopLog(); stopContentLog();}
}
