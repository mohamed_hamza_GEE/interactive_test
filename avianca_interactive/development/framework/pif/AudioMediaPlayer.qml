import QtQuick 1.1
import Panasonic.Pif 1.0

Item {
    id: aMediaPlayer
    //constants
    property int cMEDIA_PLAY: 1;
    property int cMEDIA_PAUSE : 2;
    property int cMEDIA_REWIND : 3;
    property int cMEDIA_FORWARD : 4;
    property int cMEDIA_STOP : 5;
    // Preset
    property int __timerInterval: 10;
    property int __skipTimerInterval: 2;
    property int __skipPreviousTimeout: 0;
    property int __usbMaxFileSize: 0;
    property string __skipSpeeds:"1,2,8,16,32,64";
    property string __playerType:"";
    // Play AVOD
    property int __elapseTime: 0;           // in sync with mediaElapseTime except when speed != 1
    property int __amid: 0;
    property int __albumid: 0;
    property int __cmid: 0;
    property int __cid: 0;
    property int __playerIndex: 0;          // Media Player index.
    property int __modelIndex: 0;           // Model index. When in shuffle, it differs from media player index.
    property string __simPlayerToModelMap:"";  // Comma separated list of model indexes keyed on playerIndex
    property int __elapseTimeFormat: 0;
    property string __mediaType: "audioAggregate";
    property string __duration: "00:00:00";
    property int __playType: 0;
    property int __repeatType: 0;       // 0 => no repeat, 1 => repeat all, 2 => repeat current track;
    property bool __shuffle: false;
    property bool __vbMode:false;
    property int __vbStartTime:0;
    property int __rating: 254;
    // Media player runtime
    property int __mediaState : cMEDIA_STOP;
    property int __skipSpeed : 1;			// Value from config.ini (PifVideoSpeed). 1x is normal play.
    property int __coreTimerCount: 1;
    property int __coreTimerCntr: 1;
    property double __prevTime:0;
    property variant __mediaErrCode: ['NoPlayerError','InvalidPosition','InvalidItem','PlayFailed','StopFailed','InvalidPlayRate','InvalidLanguage','InvalidSubtitle','InvalidCommand','OtherPlayerError'];
    property int 	__mediaElapseTime : 0;			// media elapse time in secs.
    property string __mediaElapseText: "";			// media elapse time in time format.
    property int	__mediaRemainTime : 0;			// media remaining time in secs.
    property string __mediaRemainText: "";			// media remaining time in time format.
    property int 	__mediaStartTime : 0;             // media Start time in secs.
    property string __mediaStartText: "";             // media Start time in time format.
    property int 	__mediaEndTime : 0;				// media End time in secs.
    property string __mediaEndText: " ";              // media End time in time format.
    property double __mediaPositionSetAt:0; // Time at which new media position was set.
    property bool __eosEventReceived:false;

    property variant __durationArray:[]
    property variant mediaModel:simpleModel;
    property alias mediaPlaylist:mediaPlaylist;
    property alias mediaPlayer:mediaPlayer;
    property alias fakePlayAckTimer: fakePlayAckTimer
    property alias fakeStopAckTimer: fakeStopAckTimer
    property alias simPlayAckTimer: simPlayAckTimer
    property alias skipPreviousTimer: skipPreviousTimer

    signal sigAggPlayBegan(int amid, int playType, int albumid);
    signal sigMidPlayBegan(int amid,int cmid,int playIndex, int playType, int albumid);
    signal sigMidPlayStopped(int amid,int cmid,int playIndex);
    signal sigAggPlayStopped(int amid,int cmid,int playIndex,string cause);
    signal sigMidPlayCompleted(int amid,int cmid,int playIndex);
    signal sigAggPlayCompleted(int amid,int cmid,int playIndex);
    signal sigMediaPaused();
    signal sigMediaForward(int speed);
    signal sigMediaRewind(int speed);
    signal sigMediaElapsed(int timeformat, int elapseTime, int endTime, string elapseText, string remainText, string endText, int remainTime);
    signal sigMediaError(string cause,int errorCode);
    signal sigMediaElapseTimeBeforeStop(int elapseTime);

    signal sigMediaRepeatState(bool repeatAll, bool repeatItem);
    signal sigMediaShuffleState(bool shuffle);
    // Public Functions
    function getMediaElapseTime() {return __mediaElapseTime;}
    function getMediaElapseText() {return __mediaElapseText;}
    function getmediaRemainTime() {return __mediaRemainTime;}
    function getMediaRemainText() {return __mediaRemainText;}
    function getMediaEndTime() {return __mediaEndTime;}
    function getMediaEndText() {return __mediaEndText;}
    function getMediaState() {return __mediaState;}
    function getMediaShuffle() {return __shuffle;}
    function getMediaRepeat() {return __repeatType;}
    function getPlayingMid () {return (__mediaType=="broadcastAudio")?(__cmid+1):__cmid;}
    function getPlayingAlbumId () {return __albumid;}
    function getPlayingAggMid () {return __amid;}
    function getPlayingIndex () {return __modelIndex}
    function getMediaModel () {return mediaModel;}
    function getMediaModelCount () {return mediaModel.count;}
    function getAllMediaPlayInfo() {return {playType: __playType, elapseTime : __elapseTime, amid : __amid, duration : __duration, elapseTimeFormat : __elapseTimeFormat, cid : __cid, cmid : (__mediaType=="broadcastAudio")?(__cmid+1):__cmid, playIndex : getPlayingIndex(), mediaType : __mediaType};}
    function getVbMode() { return __vbMode;}
    function getMediaRating() {return __rating;}
    function getPlayingCategoryId (){return __cid;}

    function setMediaShuffle (sState) {
        sState = !sState?false:true; __shuffle = sState;
        __mediaShuffle(sState);
        if (core.pc && mediaModel.count) __simPlayerToModelMap=(sState)?core.coreHelper.randomizeArray(__arrayWithIndexValue(mediaModel.count)).join(','):__arrayWithIndexValue(mediaModel.count).join(',');
        core.info ("setMediaShuffle | __shuffle: "+__shuffle+" mediaPlayer.shuffle: "+mediaPlayer.shuffle);
        sigMediaShuffleState(sState);
        return true;
    }
    function setMediaRepeat (repeatType) {
        __repeatType = (repeatType==1||repeatType==2)?repeatType:0;
        mediaPlayer.repeatItem = __repeatType==2?true:false;
        mediaPlayer.repeatAll = __repeatType==1?true:false;
        core.info ("setMediaRepeat | __repeatType: "+__repeatType+" mediaPlayer.repeatItem: "+mediaPlayer.repeatItem+" mediaPlayer.repeatAll: "+mediaPlayer.repeatAll);
        sigMediaRepeatState(mediaPlayer.repeatAll, mediaPlayer.repeatItem);
        return true;
    }
    function playNext () {
        core.info (__playerType+" | playNext | Empty function definition");return true;
    }
    function playPrevious () {
        core.info (__playerType+" | playPrevious | Empty function definition");return true;
    }
    function suspendPlaying () {
        core.info (__playerType+" | suspendPlaying | Empty function definition");return true;
    }
    function unSuspendPlaying () {
        core.info (__playerType+" | unSuspendPlaying | Empty function definition");return true;
    }
    function pause () {
        core.info (__playerType+" | pause | Empty function definition");return true;
    }
    function resume () {
        core.info (__playerType+" | resume | Empty function definition");return true;
    }
    function setPlayingMid (mid) {
        core.info (__playerType+" | setPlayingMid | mid: "+mid);
        if(__shuffle) __mediaShuffle(false);
        if (__vbMode) __elapseTime=__getVbElapsedTime(__duration);

        var indx = mediaModel.indexOf({"mid":(__mediaType=="broadcastAudio"?mid-1:mid)},0);
        if(__vbMode) {var ret = mediaPlayer.playTrack(indx,__elapseTime); __prevTime=new Date().getTime();}
        else var ret = mediaPlayer.gotoItem(indx==-1?0:indx);
        if(__shuffle) __mediaShuffle(true);
        core.info (__playerType+" | setPlayingMid | Return status: "+ret);
        return __mediaPlayCheck(indx==-1?0:indx,ret);
    }

    function setPlayingMidInShuffledMode (mid) {
        core.info (__playerType+" | setPlayingMidInShuffledMode | mid: "+mid);
        if (__vbMode) __elapseTime=__getVbElapsedTime(__duration);

        var indx = mediaModel.indexOf({"mid":(__mediaType=="broadcastAudio"?mid-1:mid)},0);
        if(__vbMode) {var ret = mediaPlayer.playTrack(indx,__elapseTime); __prevTime=new Date().getTime();}
        else var ret = mediaPlayer.gotoItem(indx==-1?0:indx);
        core.info (__playerType+" | setPlayingMidInShuffledMode | Return status: "+ret);
        return __mediaPlayCheck(indx==-1?0:indx,ret);
    }

    function setPlayingIndex (indx) {       // -1 => resume else play indx
        var ret;
        core.info (__playerType+" | setPlayingIndex | Index: "+indx+",__elapseTime: "+__elapseTime);
        if(__shuffle) __mediaShuffle(false);

        if (__vbMode) __elapseTime=__getVbElapsedTime(__duration);

        if (indx!=-1) {
            if(__vbMode) {ret = mediaPlayer.playTrack((!indx||indx>=mediaModel.count)?0:indx,__elapseTime); __prevTime=new Date().getTime();}
            else {ret = mediaPlayer.gotoItem((!indx||indx>=mediaModel.count)?0:indx);
                if(core.pc)__elapseTime=0;
            }
        }else{
            if(__mediaType=="mp3"){
                ret = mediaPlayer.gotoItem(__modelIndex);
                if(__elapseTime){
                    core.info("AudioMediaPlayer.qml | setPlayingIndex | __elapseTime: "+__elapseTime+",__mediaEndTime :"+__mediaEndTime)
                    var elapseTime = (pif.getMonitorType()==pif.cLRU_ECO || pif.getMonitorType()==pif.cLRU_ECOV2 || pif.getMonitorType()==pif.cLRU_ELITE || pif.getMonitorType()==pif.cLRU_ELITEV2 || pif.getMonitorType()==pif.cLRU_HDPSEB)?Math.floor(parseInt(__elapseTime*100,10)/__mediaEndTime):__elapseTime;
                    mediaPlayer.position = (!elapseTime)?0:elapseTime;
                    __prevTime=new Date().getTime();
                }
            }else{
                if(__elapseTime) {ret = mediaPlayer.playTrack(__modelIndex,__elapseTime); __prevTime=new Date().getTime();}
                else ret = mediaPlayer.gotoItem(__modelIndex);
            }
        }
        if(__shuffle) __mediaShuffle(true);
        core.info (__playerType+" | setPlayingIndex | Return status: "+ret);
        return __mediaPlayCheck(indx!=-1?indx:__playerIndex,ret);
    }

    function setMediaPosition (pos) {
        core.info (" setMediaPosition | pos: "+pos);
        if (__mediaState == cMEDIA_STOP) {core.error (__playerType+" | setMediaPosition | Media Position set request made when media is stopped. Exiting..."); return false;}
        if (__vbMode) {core.error (__playerType+" | setMediaPosition | Media Position set requested when virtual broadcast mode is enabled. Exiting..."); return false;}
        __mediaPositionSetAt= (new Date().getTime())/1000;
        mediaPlayer.position = (!pos)?0:pos;
        return true;
    }

    function pauseElapseTimeUpdate(){
        if(core.pc) {
            core.error(__playerType+" | pauseElapseTimeUpdate | Pausing/Resuming elapse time update cannot be done for PC");
            return;
        }
        __pauseMediaTimer();
    }

    function resumeElapseTimeUpdate(){
        if(core.pc) {
            core.error(__playerType+" | resumeElapseTimeUpdate | Pausing/Resuming elapse time update cannot be done for PC");
            return;
        }
        core.info(__playerType+" | resumeElapsedTimeUpdate | Resume Media Timer");
        if (__mediaState == cMEDIA_STOP) {core.error (__playerType+" | resumeElapsedTimeUpdate | Elapsed time update requested when media is stopped. Exiting..."); return false;}
        __coreTimerCntr=__coreTimerCount;
        __startMediaTimer(__timerInterval*1000,mediaPlayer.position);
    }

    // Private Functions
    function __mediaShuffle (state) {
        mediaPlayer.shuffle = state;
        if(!core.pc) __playerIndex = mediaPlayer.currentPlaylistIndex;
    }

    function __setMediaPlayParams(p) {
        __elapseTime = p.elapseTime;
        __albumid = p.amid;
        __duration = p.duration;
        __elapseTimeFormat = p.elapseTimeFormat;
        __cid = p.cid;
        __playerIndex = p.playIndex;
        __modelIndex = p.playIndex;
        __mediaType = p.mediaType;
        __playType = p.playType;
        __vbMode = p.vbMode;
        __rating = p.rating;
        __amid=p.amid;
        setMediaRepeat(p.repeatType);
        setMediaShuffle(false);
    }

    function __setMediaModel (model,cid,amid,mediaType) {
        mediaPlaylist.playlistUpdateEnabled = false;
        mediaModel.clear();
        var ptr; var mmid; var aggregate_mid; var isAmidInModel=false; var tempArray=new Array();
        for(var i=0;i<model.count;i++) {
            ptr=model.at(i); mmid = parseInt(ptr.mid,10);
            for(var field in ptr) if(field == "aggregate_parentmid") isAmidInModel = true;
            aggregate_mid = (isAmidInModel)?parseInt(ptr.aggregate_parentmid,10):amid;

            if(mediaType=="broadcastAudio"){
                mmid = parseInt(ptr.categorymedia_channel,10)-1;
                mediaModel.append({"amid":ptr.mid,"mid":mmid,"media_type":mediaType,"duration":ptr.duration, "cid":cid});
                core.info(__playerType+" | AMID: "+aggregate_mid+" MID: "+mmid+" media_type: "+mediaType+" duration: "+ptr.duration+" cid: "+cid);
            }else if(mediaType!="mp3"){
                mmid = parseInt(ptr.mid,10);
                mediaModel.append({"amid":aggregate_mid,"mid":mmid,"media_type":ptr.media_type,"duration":ptr.duration, "cid":cid});
                core.info(__playerType+" | AMID: "+aggregate_mid+" MID: "+mmid+" media_type: "+ptr.media_type+" duration: "+ptr.duration+" cid: "+cid);
                if(ptr.duration=="" || ptr.duration==undefined) core.error(__playerType+" | Duration provided for MID: "+mmid+" is not valid...")
            }else if(mediaType=="mp3" && (ptr.filetype).toLowerCase()=="mp3"){
                if(parseInt(ptr.filesize,10)<=Math.ceil(__usbMaxFileSize*1048576)){
                    mmid=i; tempArray.push("00:00:00"); __durationArray=tempArray;
                    mediaModel.append({"amid":aggregate_mid,"mid":i,"media_type":mediaType,"media_filename":ptr.pathfilename,"filenameonly":ptr.filenameonly,"duration":"00:00:00","cid":cid})
                    core.info(__playerType+" | AMID: "+aggregate_mid+" MID: "+mmid+" media_type: "+mediaType+" duration: 00:00:00 cid: "+cid);
                }else {
                    core.info(__playerType+" | File size of Mid: "+i+" ("+ptr.filenameonly+") is not supported, please increase the PifUSBMaxFileSixe in config.ini to play this file");
                }
            }
        }
        mediaPlaylist.playlistUpdateEnabled = true;
        mediaPlaylist.model=mediaModel;

        return (mediaModel.count==0)?false:true;
    }

    function __setMediaModelByMid (amid,mediaType,duration,cid) {
        mediaPlaylist.playlistUpdateEnabled = false;
        mediaModel.clear();
        mediaModel.append({"amid":amid,"mid":amid, "media_type":mediaType, "duration":duration, "cid":cid});
        core.info(__playerType+" | AMID: "+amid+"MID: "+amid+" media_type: "+mediaType+" duration: "+duration+" cid: "+cid);
        mediaPlaylist.playlistUpdateEnabled = true;
        mediaPlaylist.model=mediaModel;
        return true;
    }

    function __mediaPlay (p) {
        var ret;
        core.debug (__playerType+" | PLAY Called:");
        __setMediaPlayParams(p);
        mediaPlaylist.aggregate = (p.playType==1)?true:false;
        __mediaPositionSetAt=(new Date().getTime())/1000;
        __playerIndex=p.playIndex;
        __eosEventReceived=false;
        if(__elapseTime){ret = mediaPlayer.playTrack(__playerIndex,__elapseTime);}
        else {ret = mediaPlayer.gotoItem(__playerIndex);}
        setMediaShuffle(p.shuffle);
        core.info (__playerType+" | Playing __playerIndex: "+__playerIndex+" __elapseTime: "+__elapseTime+" Return status: "+ret);
        if (!core.pc) {fakePlayAckTimer.restart(); return ret;} else {simPlayAckTimer.elapse=__elapseTime; simPlayAckTimer.restart(); return true;}
    }

    function __mediaPlayCheck (indx,ret) {
        if (__mediaState == cMEDIA_STOP) if (!core.pc) fakePlayAckTimer.restart(); else {simPlayAckTimer.elapse=__elapseTime; simPlayAckTimer.restart();}
        else if (core.pc) { __playerIndex = indx; __setSimModelIndex(__playerIndex); simItemChangeAckTimer.restart();}
        return (core.pc)?true:ret;
    }

    function __mediaPlayAck (pos,pindex,mindex) {
        core.info (__playerType+" | mediaPlayCallAck CALLED: position: "+pos+" playIndex: "+pindex+" modelIndex: "+mindex+" __mediaState: " + __mediaState);
        __mediaState = cMEDIA_PLAY;
        __playerIndex = pindex;
        __modelIndex = mindex;
        __setPlayingMidInfo(mindex);
        __skipSpeed = 1;
        fakePlayAckTimer.stop();
        __startMediaTimer(__timerInterval*1000,pos);
        if (__skipPreviousTimeout && pos==0) skipPreviousTimer.restart();
        sigAggPlayBegan(__amid,__playType,__albumid);
        sigMidPlayBegan(__amid,(__mediaType=="broadcastAudio"?(__cmid+1):__cmid),mindex,__playType,__albumid);
        return true;
    }

    function __mediaItemChangeAck(pindex,mindex) {
        core.info (__playerType+" | __mediaItemChangeAck | playerIndex: "+pindex+" modelIndex: "+mindex+" __mediaState: " + __mediaState);
        __mediaState = cMEDIA_PLAY;
        __playerIndex = pindex;
        __modelIndex = mindex;
        __eosEventReceived = false;
        __setPlayingMidInfo(mindex);
        core.info(__playerType+" | __mediaItemChangeAck | amid: "+__amid+" mid: "+__cmid+" duration: "+__duration);
        __startMediaTimer(__timerInterval*1000);
        if (__skipPreviousTimeout) skipPreviousTimer.restart();
        sigMidPlayBegan(__amid,(__mediaType=="broadcastAudio"?(__cmid+1):__cmid),mindex,__playType,__albumid);
        return true;
    }

    function __mediaStop () {
        if(__mediaState == cMEDIA_STOP){ core.error(__playerType+" | STOP requested when media is already stopped. Exiting..."); return false;}
        var ret = mediaPlayer.stop();
        core.info (__playerType+" | Stop requested: Return status: "+ret);
        if (core.pc) {simStopAckTimer.restart(); return true;} else {fakeStopAckTimer.restart(); return ret;}
    }

    function __mediaStopAck (pos,pindex,mindex,cause) {
        core.info (__playerType+" | __mediaStopAck called: position: "+pos+" playIndex: "+pindex+" modelIndex: "+mindex+" __mediaState: "+__mediaState+" Cause: "+cause);
        __mediaState = cMEDIA_STOP;
        __playerIndex = pindex;
        __modelIndex = mindex;
        if (fakeStopAckTimer.running) fakeStopAckTimer.stop();
        __mediaPositionAck((__mediaElapseTime!=0)?pos:0);   // __mediaElapseTime==0 incase eos occured
        __skipSpeed = 1;
        core.info (__playerType+" | Elapsetime Of Mid: "+__cmid+" is "+__elapseTime);
        sigMediaElapseTimeBeforeStop(__elapseTime);
        __stopMediaTimer();
        if (skipPreviousTimer.running) skipPreviousTimer.stop();
        sigMidPlayStopped(__amid,(__mediaType=="broadcastAudio"?(__cmid+1):__cmid),mindex);
        sigAggPlayStopped(__amid,(__mediaType=="broadcastAudio"?(__cmid+1):__cmid),mindex,cause);
        return true;
    }

    function __mediaFakeStopForceAV() {
        core.info(__playerType+" | __mediaFakeStopForceAV called");
        if(getMediaState()!=cMEDIA_STOP)__mediaStopAck(0,mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex,'fakeStop');
        if(getMediaState()!=cMEDIA_STOP)__mediaStopAck(0,mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex,'fakeStop');
        __resetElapseValues();
        __albumid=0;__cmid=0;
    }

    function __mediaEndOfStreamAck (pos,pindex,mindex) {
        core.info (__playerType+" | __mediaEndOfStreamAck | position: "+pos+" playIndex: "+pindex+" modelIndex: "+mindex+" __mediaState: "+__mediaState);
        __playerIndex = pindex;
        __modelIndex = mindex;
        __eosEventReceived = true;
        __mediaPositionAck(0);
        if (pindex >= mediaModel.count-1 && __repeatType==0) {
            core.info (__playerType+" | __mediaEndOfStreamAck | End of playlist reached");
            sigMidPlayCompleted(__amid,(__mediaType=="broadcastAudio"?(__cmid+1):__cmid),mindex);
            sigAggPlayCompleted(__amid,(__mediaType=="broadcastAudio"?(__cmid+1):__cmid),mindex);
        } else {
            core.info (__playerType+" | __mediaEndOfStreamAck | Elapsetime Of Mid: "+__cmid+" is "+__elapseTime+". Playing next index in playlist...");
            __skipSpeed = 1;
            __stopMediaTimer(); if(__repeatType==2){ __eosEventReceived = false; __startMediaTimer(__timerInterval*1000);}
            sigMidPlayCompleted(__amid,(__mediaType=="broadcastAudio"?(__cmid+1):__cmid),mindex);
        }
        return true;
    }

    function __mediaPrevious() {
        core.info(__playerType+" | __mediaPrevious | __playerIndex: "+__playerIndex+" Model count: "+mediaModel.count+" __repeatType: "+__repeatType+" __shuffle: "+__shuffle+" mediaPlayer.repeatItem: "+mediaPlayer.repeatItem+" mediaPlayer.repeatAll: "+mediaPlayer.repeatAll+" mediaPlayer.shuffle: "+mediaPlayer.shuffle);
        if ((!__skipPreviousTimeout && !skipPreviousTimer.running)||(__skipPreviousTimeout && skipPreviousTimer.running)||(__mediaState == cMEDIA_STOP)) return __playPrevious();
        else if (__skipPreviousTimeout && !skipPreviousTimer.running && __playerIndex >= 0) {skipPreviousTimer.start(); return setMediaPosition(0);}
        else return false;
    }

    function __playPrevious () {
        core.debug(__playerType+" | __playPrevious called");
        if (__playerIndex <= 0 && __repeatType!=1) {core.info(__playerType+" | __mediaPrevious | Reached end of model. Exiting..."); return false;}
        else {
            if(skipPreviousTimer.running) skipPreviousTimer.stop();
            var ret = mediaPlayer.previous();
            core.info(__playerType+" | __mediaPrevious | __playerIndex: "+__playerIndex+" Model count: "+mediaModel.count+" Return status: "+ret);
            if (core.pc) {
                if (__playerIndex > 0) __playerIndex--; else __playerIndex=mediaModel.count-1;
                __setSimModelIndex(__playerIndex); simItemChangeAckTimer.restart(); return true;
            } else return ret;
        }
    }

    function __mediaNext() {
        core.info(__playerType+" | __mediaNext | __playerIndex: "+__playerIndex+" Model count: "+mediaModel.count+" __repeatType: "+__repeatType+" __shuffle: "+__shuffle+" mediaPlayer.repeatItem: "+mediaPlayer.repeatItem+" mediaPlayer.repeatAll: "+mediaPlayer.repeatAll+" mediaPlayer.shuffle: "+mediaPlayer.shuffle);
        if (__playerIndex >= mediaModel.count-1 && __repeatType!=1) {core.info(__playerType+" | __mediaNext | Reached end of model. Exiting..."); return false;}
        else {
            var ret = mediaPlayer.next();
            core.info(__playerType+" | __mediaNext | __playerIndex: "+__playerIndex+" Model count: "+mediaModel.count+" Return status: "+ret);
            if (core.pc) {
                if (__playerIndex < mediaModel.count-1) __playerIndex++;
                else __playerIndex=0;
                __setSimModelIndex(__playerIndex);
                simItemChangeAckTimer.restart();
                return true;
            }
            return ret;
        }
    }

    function __mediaPause(forcePause) { // forcePause used when media to be paused immediately after play.
        if (__mediaState == cMEDIA_PAUSE) {core.error (__playerType+" | __mediaPause | PAUSE requested when media is already paused. Exiting..."); return false;}
        if (__vbMode) {core.error (__playerType+" | __mediaPause | PAUSE requested when virtual broadcast mode is enabled. Exiting..."); return false;}
        if(!forcePause && __mediaState == cMEDIA_STOP) {core.error(__playerType+" | __mediaPause | PAUSE requested when media is already stopped. Exiting..."); return false;}
        var ret = mediaPlayer.pause();
        core.info (__playerType+" | __mediaPause requested | Return status: "+ret);
        if (core.pc) {simPauseAckTimer.restart(); return true;} else return ret;
    }

    function __mediaPauseAck (pos) {
        core.info (__playerType+" | __mediaPauseAck | position: "+pos+" __mediaState: " + __mediaState);
        __mediaState = cMEDIA_PAUSE;
        __mediaElapseTime = pos;
        __pauseMediaTimer();
        sigMediaPaused();
        return true;
    }

    function __skipAdMedia(skip_one){
        core.info (__playerType+" | Skipping Ad with skip_one : "+skip_one);
        mediaPlayer.skipAd(skip_one);
        return true;
    }

    function __mediaResume() {
        if (__mediaState == cMEDIA_PLAY) {core.error (__playerType+" | __mediaResume | RESUME requested when media is already playing. Exiting..."); return false;}
        if (__mediaState == cMEDIA_STOP) {core.error (__playerType+" | __mediaResume | RESUME requested when media is already stopped. Exiting..."); return false;}
        var ret = mediaPlayer.resume();
        core.info (__playerType+" | __mediaResume requested | Return status: "+ret);
        if (core.pc) {simResumeAckTimer.restart(); return true;} else return ret;
    }

    function __mediaResumeAck (pos) {
        core.info (__playerType+" | __mediaResumeAck | position: "+pos+" __mediaState: " + __mediaState);
        var prevMediaState = __mediaState;
        __mediaState = cMEDIA_PLAY;
        __mediaElapseTime = pos;
        __skipSpeed = 1;
        if (prevMediaState==cMEDIA_PAUSE) __prevTime = new Date().getTime();
        __updateMediaTimer(__timerInterval*1000);
        sigMidPlayBegan(__amid,(__mediaType=="broadcastAudio"?(__cmid+1):__cmid),__modelIndex,__playType,__albumid);
        return true;
    }

    function __mediaForward(retainspeed) {
        if (__mediaState == cMEDIA_STOP) {core.error (__playerType+" | __mediaForward | FORWARD requested when media is already stopped. Exiting..."); return false;}
        if (__mediaState == cMEDIA_REWIND) __skipSpeed = 1;		// Reset index to begin forwarding...  1x is normal play
        var arr = __skipSpeeds.split(',');
        var cspeedIndx = arr.indexOf(String(__skipSpeed));
        if(!retainspeed) cspeedIndx = (cspeedIndx >= arr.length-1)?0:cspeedIndx+1;
        if (arr[cspeedIndx]==1) return __mediaResume();
        else {
            mediaPlayer.playRate = 100*arr[cspeedIndx];
            core.info (__playerType+" | __mediaForward | Requested Speed: "+arr[cspeedIndx]);
            if ((!core.pc && (__mediaType=='audio' || __mediaType=='audioAggregate')) || core.pc) {__skipSpeed = arr[cspeedIndx]; simForwardAckTimer.restart(); return true;} else return true;
        }
    }

    function __mediaForwardAck (speed) {
        core.info (__playerType+" | __mediaForwardAck | speed: "+speed+" __mediaState: " + __mediaState);
        __mediaState = cMEDIA_FORWARD;
        __skipSpeed = speed;
        __updateMediaTimer(__skipTimerInterval*1000);
        sigMediaForward(__skipSpeed);
        return true;
    }

    function __mediaRewind(retainspeed) {
        if (__mediaState == cMEDIA_STOP) {core.error (__playerType+" | __mediaRewind | REWIND requested when media is already stopped. Exiting..."); return false;}
        if (mediaPlayer.position==0) {core.error (__playerType+" | __mediaRewind | mediaplayer position is 0. Hence, exiting..."); return false;}
        if (__mediaState == cMEDIA_FORWARD) __skipSpeed = 1;		// Reset index to begin rewinding...    1x is normal play
        var arr = __skipSpeeds.split(',');
        var cspeedIndx = arr.indexOf(String(__skipSpeed));
        if(!retainspeed) cspeedIndx = (cspeedIndx >= arr.length-1)?0:cspeedIndx+1;
        if (arr[cspeedIndx]==1) return __mediaResume();
        else {
            mediaPlayer.playRate = -100*arr[cspeedIndx];
            core.info (__playerType+" | __mediaRewind | Requested Speed: "+arr[cspeedIndx]);
            if ((!core.pc && (__mediaType=='audio' || __mediaType=='audioAggregate')) || core.pc) {__skipSpeed = arr[cspeedIndx]; simRewindAckTimer.restart(); return true;} else return true;
        }
    }

    function __mediaRewindAck (speed) {
        core.info (__playerType+" | __mediaRewindAck | speed: "+speed+" __mediaState: " + __mediaState);
        __mediaState = cMEDIA_REWIND;
        __skipSpeed = speed;
        __updateMediaTimer(__skipTimerInterval*1000);
        sigMediaRewind(__skipSpeed);
        return true;
    }

    function __mediaRestart() {
        core.info (__playerType+" | __mediaRestart()");
        var ret = mediaPlayer.playTrack(__playerIndex,__elapseTime);
        core.info (__playerType+" | __mediaRestart() | Return status: "+ret); return ret;
    }

    function __mediaPositionAck (pos) {
        __mediaElapseTime = (pos <= __mediaEndTime)?(pos>0?pos:0):__mediaEndTime;
        __elapseTime = __mediaElapseTime;
        __processMediaElapse();
        if (core.pc && __mediaElapseTime>=__mediaEndTime && __mediaEndTime!=0) __simMediaEndOfStream();            // sim eos
        else if (core.pc && __skipSpeed>1 && __mediaElapseTime==0) __mediaResumeAck(0);     // sim reached to beginning
        return true;
    }

    function __aspectRatioChangeAck (newAspectRatio) {return true;}

    function __mediaError (errcode) {
        core.info (__playerType+" | __mediaError | Error: "+__mediaErrCode[errcode]+" Error Code: "+errcode);
        if (errcode == MediaPlayer.PlayFailed)  __mediaStopAck(mediaPlayer.position,__playerIndex,__modelIndex,'playFailed');
        else if (errcode == MediaPlayer.StopFailed) __mediaStopAck(mediaPlayer.position,__playerIndex,__modelIndex,'stopFailed');
        else {core.info (__playerType+" | __mediaError | UnHandled Error State");sigMediaError(__mediaErrCode[errcode],errcode)}
    }

    function __setPlayingMidInfo(mindex){
        __cmid = parseInt(mediaModel.getValue(mindex,"mid"),10);
        __amid = parseInt(mediaModel.getValue(mindex,"amid"),10);
        __duration = (__mediaType=="mp3")?__durationArray[mindex]:mediaModel.getValue(mindex,"duration");
    }

    function __getMediaPosition() {
        var currentTime = (new Date().getTime())/1000;
        if((currentTime-__mediaPositionSetAt)>0.5) {
            core.info(__playerType+" | __getMediaPosition | Requesting duration from core");
            mediaPlayer.requestPosition();
        }
    }

    function __setVbStartTime(time){
        __vbStartTime = time;
    }

    function __getVbElapsedTime (duration){
        return Math.floor((new Date().getTime() / 1000) - __vbStartTime)%core.coreHelper.convertHMSToSec(duration);
    }

    // PC Simulation
    function __simMediaEndOfStream () {
        __mediaEndOfStreamAck (__mediaElapseTime,__playerIndex,__modelIndex);
        if (__playerIndex >= mediaModel.count-1 && __repeatType==0) {__mediaStopAck (__mediaElapseTime,__playerIndex,__modelIndex,"stop"); return true;}
        else if (__playerIndex >= mediaModel.count-1 && __repeatType==1) {__playerIndex=0; __setSimModelIndex(__playerIndex);}
        else if (__repeatType!=2) {__playerIndex++; __setSimModelIndex(__playerIndex);}
        simItemChangeAckTimer.restart(); return true;
    }

    function __setSimModelIndex (indx) { core.debug("indx: "+indx+" __simPlayerToModelMap: "+__simPlayerToModelMap+" __modelIndex: "+__modelIndex); __modelIndex = __simPlayerToModelMap.split(',')[indx]; core.debug("__modelIndex: "+__modelIndex);}

    function __arrayWithIndexValue(length) {
        if (!length) return false; var i=length; var arr=[];
        while (i--) arr.push(length-1-i);
        return arr;
    }

    // ************************* MEDIA TIMER CALLS *************************
    function __computeMediaTime() {
        core.debug(__playerType+" | computeMediaTime __coreTimerCount: "+__coreTimerCount+" __coreTimerCntr: "+__coreTimerCntr);
        if (!core.pc && (__mediaType=='video' || __mediaType=='videoAggregate') && __skipSpeed>1) __getMediaPosition();        // for Rack & vod & in forward/rewind mode
        else if (!core.pc && __mediaType!='mp3' && ++__coreTimerCntr>__coreTimerCount) { __coreTimerCntr=1; __getMediaPosition();}
        else {
            var ctime = new Date().getTime();
            __mediaPositionAck(__mediaElapseTime + (__mediaState==cMEDIA_REWIND?-1:__mediaState==cMEDIA_PAUSE?0:1)*Math.round(__skipSpeed*(ctime - __prevTime)/1000));
            __prevTime = ctime;
        }
    }

    //function __computeElapseTime(){if(__elapseTime==0){return 0}else {return Math.round(((new Date().getTime())-__prevTime)/1000)+__elapseTime;}}
    function __computeElapseTime(){
        core.info( __playerType+" | __computeElapseTime | __elapseTime: "+__elapseTime)
        if(__elapseTime==0){return 0}
        else {
            var elapseTime = /*Math.round(((new Date().getTime())-__prevTime)/1000)+*/__elapseTime;
            return elapseTime
        }
    }

    function __processMediaElapse () {
        core.verbose(__playerType+" | Process Media Elapse Called");
        __mediaElapseText = core.coreHelper.getTimeFormatFromSecs(__mediaElapseTime, __elapseTimeFormat);
        __mediaRemainTime = __mediaEndTime - __mediaElapseTime;
        __mediaRemainText = core.coreHelper.getTimeFormatFromSecs(__mediaRemainTime, __elapseTimeFormat);
        core.info(__playerType+" | __processMediaElapse | ELAPSE (SECS): "+__mediaElapseTime+",  END (SECS): "+__mediaEndTime);
        core.debug(__playerType+" | __processMediaElapse | ELAPSE (SECS): "+__mediaElapseTime+" ELAPSE (TEXT): "+__mediaElapseText+ " REMAINING (SECS): " + __mediaRemainTime + " REMAINING (TEXT): "+ __mediaRemainText +    " END (SECS): "+__mediaEndTime+" END (TEXT): "+__mediaEndText+" TIME FORMAT: "+__elapseTimeFormat+" __skipSpeed: "+__skipSpeed+" __elapse: "+__elapseTime+" __mediaRemainTime: "+__mediaRemainTime);
        sigMediaElapsed(__elapseTimeFormat, __mediaElapseTime, __mediaEndTime, __mediaElapseText, __mediaRemainText, __mediaEndText, __mediaRemainTime);
        return true;
    }

    function __startMediaTimer (interval,stime) {
        core.info(__playerType+" | Start Media Timer | interval: "+interval);
        __resetElapseValues();
        __mediaElapseTime = (!stime)?0:stime;
        __mediaElapseText = core.coreHelper.getTimeFormatFromSecs(__mediaElapseTime, __elapseTimeFormat);
        __mediaStartText = core.coreHelper.getTimeFormatFromSecs(__mediaStartTime, __elapseTimeFormat);
        __updateEndElapseValues();
        mediaTimer.interval = (!interval)?10000:interval;
        if(__mediaEndTime==0 && __mediaType!="mp3") mediaTimer.stop(); else mediaTimer.restart();
        sigMediaElapsed(__elapseTimeFormat, __mediaElapseTime, __mediaEndTime, __mediaElapseText, __mediaRemainText, __mediaEndText, __mediaRemainTime);
    }

    function __updateEndElapseValues(){
        __mediaEndTime = core.coreHelper.convertHMSToSec((__mediaType=="mp3")?__durationArray[__modelIndex]:__duration);
        __mediaEndText = core.coreHelper.getTimeFormatFromSecs(__mediaEndTime, __elapseTimeFormat);
        __mediaRemainTime = __mediaEndTime;
        __mediaRemainText = __mediaEndText;
        sigMediaElapsed(__elapseTimeFormat, __mediaElapseTime, __mediaEndTime, __mediaElapseText, __mediaRemainText, __mediaEndText, __mediaRemainTime);
    }

    function __pauseMediaTimer () {
        core.info(__playerType+" | Pause Media Timer");
        if (!mediaTimer.running) {core.error (__playerType+" | Pause Media Timer | Timer not running. Exiting..."); return false;}
        mediaTimer.stop(); __computeMediaTime(); return true;
    }
    function __updateMediaTimer (interval) { core.info(__playerType+" | Update Media Timer | interval: "+interval); mediaTimer.interval = (!interval)?10000:interval; if(__mediaEndTime==0){mediaTimer.stop(); sigMediaElapsed(__elapseTimeFormat, __mediaElapseTime, __mediaEndTime, __mediaElapseText, __mediaRemainText, __mediaEndText, __mediaRemainTime);} else mediaTimer.restart();}
    function __resetElapseValues () {__mediaElapseTime = 0; __prevTime = new Date().getTime(); __mediaElapseText = ""; __mediaStartTime = 0; __mediaStartText = ""; __mediaRemainTime = 0; __mediaRemainText = ""; __mediaStartTime = 0; __mediaStartText = ""; __mediaEndTime = 0; __mediaEndText = "";}
    function __stopMediaTimer () {core.info(__playerType+" | Stop Media Timer"); mediaTimer.stop(); __resetElapseValues();}

    function __removeFromMediaModel(mid,index){
        var indexofMP; var aliasModelIndex=__modelIndex;
        mediaPlaylist.playlistUpdateEnabled=false;

        if(mediaModel.count==1){
            mediaModel.clear();
            mediaPlaylist.playlistUpdateEnabled=true;
            __mediaStop();
        }else {
            indexofMP=core.coreHelper.searchEntryInModel(mediaModel,(index==undefined?"media_filename":"mid"),mid);
            mediaModel.remove(indexofMP,1);
            mediaPlaylist.playlistUpdateEnabled=true;

            if(indexofMP<aliasModelIndex) {__setMediaPlaylistIndex(aliasModelIndex); __modelIndex = __playerIndex;}
            else if(indexofMP==aliasModelIndex){
                if(aliasModelIndex>=mediaModel.count)__playerIndex=(mediaModel.count-1);
                if(__mediaState==cMEDIA_PLAY) setPlayingIndex(__playerIndex); else if(__mediaState==cMEDIA_PAUSE) __mediaStop();
                __modelIndex = __playerIndex;
            }
            else{__setMediaPlaylistIndex(aliasModelIndex+1); __modelIndex = __playerIndex;}

            if(core.pc && mediaModel.count) __simPlayerToModelMap=(__shuffle)?core.coreHelper.randomizeArray(__arrayWithIndexValue(mediaModel.count)).join(','):__arrayWithIndexValue(mediaModel.count).join(',');
            if(__shuffle) __mediaShuffle(true);
        }
        return true;
    }

    function __setMediaPlaylistIndex(indx){__playerIndex=indx-1; mediaPlaylist.playlistIndex=__playerIndex; return true;}

    function __clearMediaModel(){
        mediaPlaylist.playlistUpdateEnabled=false;
        mediaModel.clear();
        mediaPlaylist.playlistUpdateEnabled=true;
        __mediaStop();__albumid=0;__cmid=0;
        return true;
    }

    function __appendToMediaModel(cModel,index,cid,cdAlbumId){
        var aliasModelIndex=__modelIndex;
        mediaPlaylist.playlistUpdateEnabled=false;
        mediaModel.append({"amid":parseInt(cdAlbumId,10),"mid":cModel.getValue(index,"mid"),"media_type":cModel.getValue(index,"media_type"),"duration":cModel.getValue(index,"duration"),"cid":cid});
        mediaPlaylist.playlistUpdateEnabled=true;
        mediaPlaylist.playlistIndex=aliasModelIndex;
        if(__shuffle) __mediaShuffle(true);
        if (core.pc && mediaModel.count) __simPlayerToModelMap=(__shuffle)?core.coreHelper.randomizeArray(__arrayWithIndexValue(mediaModel.count)).join(','):__arrayWithIndexValue(mediaModel.count).join(',');
        return true;
    }

    function __updateMediaModelProperty(mindex,propertyName,propertyValue){
        var aliasModelIndex=__modelIndex;
        mediaPlaylist.playlistUpdateEnabled = false;
        mediaModel.setProperty(mindex,propertyName,core.coreHelper.convertSecToHMS(propertyValue))
        mediaPlaylist.playlistUpdateEnabled = true;
        mediaPlaylist.playlistIndex=aliasModelIndex;
        if(__shuffle) __mediaShuffle(true);
    }

    function __moveTracks(fromPosition,toPosition){
        var indexofMP; var aliasModelIndex=__modelIndex;var numberOfItemsToMove=1;
        mediaPlaylist.playlistUpdateEnabled=false;
        for(var counter=0;counter<fromPosition.length;counter++){mediaModel.move(fromPosition[counter],toPosition[counter],numberOfItemsToMove);}
        mediaPlaylist.playlistUpdateEnabled=true;      
        indexofMP=core.coreHelper.searchEntryInModel(mediaModel,"mid",getPlayingMid());
        __setMediaPlaylistIndex(indexofMP+1); __modelIndex = __playerIndex;        
        if(core.pc && mediaModel.count) __simPlayerToModelMap=(__shuffle)?core.coreHelper.randomizeArray(__arrayWithIndexValue(mediaModel.count)).join(','):__arrayWithIndexValue(mediaModel.count).join(',');
        if(__shuffle) __mediaShuffle(true);
        return true;
    }

    Timer {id:mediaTimer; interval:500; running:false; repeat:true; triggeredOnStart: true; onTriggered:__computeMediaTime();
        onIntervalChanged: {
            var n=10;      //Elapsetime request to core is every 10secs.
            __coreTimerCount = (interval >= n*1000)?1:Math.ceil(n*1000/interval);
            __coreTimerCntr=1;
        }
    }
    Timer {id:simPlayAckTimer; interval:50; property int elapse:0; onTriggered: __mediaPlayAck(elapse,__playerIndex,__modelIndex);}
    Timer {id:simStopAckTimer; interval:50; onTriggered: __mediaStopAck(__mediaElapseTime,__playerIndex,__modelIndex,'stop');}
    Timer {id:simItemChangeAckTimer; interval:50; onTriggered: __mediaItemChangeAck(__playerIndex,__modelIndex);}
    Timer {id:simPauseAckTimer; interval:50; onTriggered: __mediaPauseAck(__mediaElapseTime);}
    Timer {id:simResumeAckTimer; interval:50; onTriggered: __mediaResumeAck(__mediaElapseTime);}
    Timer {id:simForwardAckTimer; interval:50; onTriggered: __mediaForwardAck(__skipSpeed);}
    Timer {id:simRewindAckTimer; interval:50; onTriggered: __mediaRewindAck(__skipSpeed);}

    Timer {id:fakePlayAckTimer; interval:1000; onTriggered: __mediaPlayAck(__elapseTime,mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex);}
    Timer {id:fakeStopAckTimer; interval:1000; onTriggered: __mediaStopAck(__elapseTime,mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex,'fakeStop');}
    Timer {id:skipPreviousTimer; interval:__skipPreviousTimeout*1000; onTriggered: core.debug(__playerType+" | skipPreviousTimer triggered");}

    SimpleModel { id: simpleModel;}
    MediaPlaylist { id: mediaPlaylist; }
    MediaPlayer { id: mediaPlayer; deviceManager: deviceMngr; playlist:mediaPlaylist; position: 0; playRate: 100; currentPlaylistIndex: 0; mediaPlayerSettingsPersistent: true;
        onPositionChanged: {    // position
            core.info (__playerType+" | Media PLAYER ON-POSITION-CHANGED SIGNAL | ELAPSE TIME: "+mediaPlayer.position);
            if (pif.getEntOn()==0 && !pif.getFallbackMode()) return false;
            __prevTime = new Date().getTime();
            if (__mediaType!="mp3") __mediaPositionAck(mediaPlayer.position);
        }

        onPlayerModeChanged: {  // play & stop
            core.info (__playerType+" | Media PLAYER ON-PLAYER-MODE-CHANGED SIGNAL | PLAYER-MODE: "+mediaPlayer.playerMode+" PLAY-RATE-STATUS: "+mediaPlayer.playRateStatus+" PLAY RATE: "+mediaPlayer.playRate+" PLAYLIST INDEX: "+mediaPlayer.currentPlaylistIndex+" MODEL INDEX: "+mediaPlayer.currentModelIndex+" ELAPSE TIME: "+mediaPlayer.position);
            if (pif.getEntOn()==0 && !pif.getFallbackMode()) return false;
            if (mediaPlayer.playerMode==MediaPlayer.ActiveMode){
                (__mediaType!="mp3")?__mediaPlayAck(mediaPlayer.position,mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex):__mediaPlayAck(__computeElapseTime(),mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex); }
            // Play
            else if (mediaPlayer.playerMode==MediaPlayer.InactiveMode){
                (__mediaType!="mp3")?__mediaStopAck(mediaPlayer.position,mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex,'stop'):__mediaStopAck(__computeElapseTime(),mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex,'stop');}        // Stopped
            else core.debug(__playerType+" | Media PLAYER: ON-PLAYER-MODE-CHANGED CONDITION NOT HANDLED.");
        }

        onPlayRateChanged: {    // resume, pause, forward & rewind
            core.info (__playerType+" | Media PLAYER ON-PLAY-RATE-CHANGED SIGNAL: | PLAYER-MODE: "+mediaPlayer.playerMode+" PLAY-RATE-STATUS: "+mediaPlayer.playRateStatus+" PLAY RATE: "+mediaPlayer.playRate+" PLAYLIST INDEX: "+mediaPlayer.currentPlaylistIndex+" ELAPSE TIME: "+mediaPlayer.position+" eosEventReceived: "+__eosEventReceived);
            if ((pif.getEntOn()==0 && !pif.getFallbackMode()) || __mediaState==cMEDIA_STOP) return false;
            if (mediaPlayer.playerMode != MediaPlayer.ActiveMode) {core.info(__playerType+" | Media PLAYER: ON-PLAY-RATE-CHANGED | Media player inactive. Ignoring Play Rate event. "); return false;}
            if (mediaPlayer.playRate == 100 && __mediaState!=cMEDIA_PLAY)
                (__mediaType!="mp3")?__mediaResumeAck (mediaPlayer.position):__mediaResumeAck (__elapseTime);    // Resume
            else if (mediaPlayer.playRate == 0 && !__eosEventReceived)
                (__mediaType!="mp3")?__mediaPauseAck (mediaPlayer.position):__mediaPauseAck (__elapseTime);    // Pause
            else if (mediaPlayer.playRate > 100) __mediaForwardAck (mediaPlayer.playRate/100);         // Forward
            else if (mediaPlayer.playRate < -99) __mediaRewindAck (-1*mediaPlayer.playRate/100);       // Rewind
            else core.info(__playerType+" | Media PLAYER: ON-PLAY-RATE-CHANGED | Play Rate change ignored.");
        }

        onMediaPlayerEOS: {core.info (__playerType+" | Media PLAYER ON-MEDIA-PLAYER-EOS CHANGED SIGNAL | PLAYER-MODE: "+mediaPlayer.playerMode+" PLAY-RATE-STATUS: "+mediaPlayer.playRateStatus+" PLAY RATE: "+mediaPlayer.playRate+" PLAYLIST INDEX: "+mediaPlayer.currentPlaylistIndex+" ELAPSE TIME: "+mediaPlayer.position); __mediaEndOfStreamAck(mediaPlayer.position,__playerIndex,mediaPlayer.currentModelIndex);}

        onMediaPlayerError: {core.info (__playerType+" | Media PLAYER ON-PLAYER-ERROR-CHANGED SIGNAL"); __mediaError(mediaPlayer.playerError);}

        onAspectRatioChanged: {core.info (__playerType+" | Media PLAYER ON-ASPECT-RATIO-CHANGED SIGNAL | aspectRatio: "+newAspectRatio); __aspectRatioChangeAck (newAspectRatio);}

        onCurrentItemChanged: { core.info(__playerType+" | Media PLAYER ON-CURRENT-ITEM-CHANGED SIGNAL | PLAYLIST INDEX: "+mediaPlayer.currentPlaylistIndex+" MODEL INDEX: "+mediaPlayer.currentModelIndex); __mediaItemChangeAck(mediaPlayer.currentPlaylistIndex,mediaPlayer.currentModelIndex);}

        onPlayModeChanged:{__eosEventReceived=false; core.debug (__playerType+" | Media PLAYER ON-PLAY-MODE-CHANGED SIGNAL | PLAYER-MODE: "+mediaPlayer.playerMode+" PLAY-RATE-STATUS: "+mediaPlayer.playRateStatus+" PLAY RATE: "+mediaPlayer.playRate+" PLAYLIST INDEX: "+mediaPlayer.currentPlaylistIndex+" ELAPSE TIME: "+mediaPlayer.position);}

        // Unused but logged to investigate system event issues
        onPlayRateStatusChanged: core.debug (__playerType+" | Media PLAYER ON-PLAY-RATE-STATUS CHANGED SIGNAL | PLAYER-MODE: "+mediaPlayer.playerMode+" PLAY-RATE-STATUS: "+mediaPlayer.playRateStatus+" PLAY RATE: "+mediaPlayer.playRate+" PLAYLIST INDEX: "+mediaPlayer.currentPlaylistIndex+" MEDIA STATE: "+__mediaState+" ELAPSE TIME: "+mediaPlayer.position);
    }
}

