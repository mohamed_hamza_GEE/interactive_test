import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

Item{
     id: smsEmail;

     property variant headerList;
     property string authTransactionId: "";
     property string authCardHash: "";
     property string authLast4Digits: "";
     property string authErrorMessage: "";
     property string authCardType: "";
     property double sendSmsPrice: 0.0;
     property double receiveSmsPrice: 0.0;
     property double sendEmailPrice: 0.0;
     property double receiveEmailPrice: 0.0;
     property string currency: "USD";
     property string priceMessage: "";
     property string userName: "default";
     property string userAddress: "default";
     property string senderName: "";
     property string senderAddress: "";
     property int numSmss: 0;
     property int numUnreadSmss: 0;
     property int numEmails: 0;
     property int numUnreadEmails: 0;
     property string last4Digits: "";
     property string messageIncoming: "";
     property string recipientAddress: "";
     property string ccAddress: "";
     property string subject: "";
     property string messageOutgoing: "";
     property bool clearHeaderList:false;

     signal queryStatusCallEnded(int queryStatus)
     signal authenticationInputNeeded();
     signal authenticationError()
     signal authenticationComplete();
     signal requestLoginCallEnded(int queryStatus);
     signal queryPricesCallEnded(int queryStatus);
     signal setUserInfoCallEnded(int queryStatus);
     signal queryInboxSummaryCallEnded(int queryStatus);
     signal queryHeadersCallEnded(int queryStatus);
     signal queryContentCallEnded(int queryStatus);
     signal sendMessageCallEnded(int queryStatus);
     signal requestLogoutCallEnded(int queryStatus);
     signal seatResetCallEnded(int queryStatus);

    Connections{
        target: core.viewController.testPanel?core.viewController.testPanel:null;
        onSigTestPanelMessage:{
            core.info("SmsEmail.qml | onSigTestPanelMessage | reason: "+reason);
            if (reason!="SmsEmail") return false;
            if (parameter1=="incomingEmailMessage") __incomingEmailMessage(parameter2,parameter3,parameter4);
            else if (parameter1=="incomingSmsMessage") __incomingSmsMessage(parameter2,parameter3);
            else if (parameter1=="SwipeCardSuccess") {authenticationCompleteTimer.restart();}
            else if (parameter1=="SwipeCardFail") {authenticationError();}
            else if (parameter1=="numMsg") {Store.headerListJson["numberOfMessages"]=parameter2;pif.smsEmail.getMessages();}
            else core.error("SmsEmail.qml | Unknown Request");
        }
    }

    function beginQueryStatus(){
        core.info("MessagingServiceSim.qml | beginQueryStatus | Called ");
        appActiveTimer.restart();
    }

     function authenticate(){
         core.info("MessagingServiceSim.qml | authenticate | Called ");
         authenticateTimer.restart();
         return true;
     }

     function getAuthenticationData(){
         core.info("MessagingServiceSim.qml | getAuthenticationData | Called")
         return true;
     }

    function beginRequestLogin(){
        core.info("MessagingServiceSim.qml | beginRequestLogin | Called")
        requestLoginTimer.restart();
    }

     function beginQueryPrices(){
         core.info("MessagingServiceSim.qml | beginQueryPrices | Called")
         sendSmsPrice=1.5;
         receiveSmsPrice=1.0;
         sendEmailPrice=2.0;
         receiveEmailPrice=1.0;
         currency="USD";
         priceMessage="You can now avail sending sms free of charge for the next two minutes"
         queryPricesTimer.restart();
     }

     function cancelTransaction(){
         core.info("MessagingServiceSim.qml | cancelTransaction | Called");
         return true;
     }

     function beginSetUserInfo(){
         core.info("MessagingServiceSim.qml | beginSetUserInfo | Called");
         setUserInfoTimer.restart();
     }

    function beginQueryInboxSummary(){
        core.info("MessagingServiceSim.qml | beginQueryInboxSummary | Called");
        numSmss=0;
        numUnreadSmss=0;
        numEmails=0;
        numUnreadEmails=0;
        if(!Store.headerListJson[userAddress]){
            core.log("MessagingServicesSim.qml | Setting to default");
            userName="default";
            userAddress="default"
        }
        var ptr=Store.headerListJson[userAddress]["messages"];
        var counter=Store.headerListJson[userAddress]["messages"].length
        for(var i=0;i<counter;i++){
            if(ptr[i].type==0 || ptr[i].type==1){
                numEmails=numEmails+1;
                if(ptr[i].viewed==0)numUnreadEmails=numUnreadEmails+1;
            }else if(ptr[i].type==2 || ptr[i].type==3){
                numSmss=numSmss+1;
                if(ptr[i].viewed==0)numUnreadSmss=numUnreadSmss+1;
            }
        }
        last4Digits="0202";
        inboxSummaryTimer.restart();
    }

    function beginQueryHeaders(startIndex,numMessages){
        core.info("MessagingServiceSim.qml | beginQueryHeaders | Called Start Index :"+startIndex+", numMessages: "+numMessages);
        headerList.clear();
        var ptr=Store.headerListJson[userAddress]["messages"];
        var counter=Store.headerListJson[userAddress]["messages"].length;
        headerList.clear();
        for(var i=0;i<counter;i++){
          //  if(i<=Store.headerListJson["numberOfMessages"]){
                headerList.append({"messageId":ptr[i].messageId,"type":parseInt(ptr[i].type,10),"timeStamp":ptr[i].timeStamp,"viewed":parseInt(ptr[i].viewed,10),"status":parseInt(ptr[i].status,10),"recipient":ptr[i].recipient,"cc":ptr[i].cc,"sender":ptr[i].sender,"subject":ptr[i].subject,"costOfTransaction":ptr[i].costOfTransaction,"timeOfTransaction":ptr[i].timeOfTransaction,"currency":ptr[i].currency})
          //  }
        }

        core.log("MessagingServiceSim.qml | beginQueryHeaders | numberOfMessages: "+Store.headerListJson["numberOfMessages"]+" | headerList: "+headerList.count);
        if(Store.headerListJson["numberOfMessages"]>headerList.count){
            for(var j=headerList.count;j<Store.headerListJson["numberOfMessages"];j++){
                headerList.append({"messageId":userName+j,"type":3,"timeStamp":dateString(),"viewed":0,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Test "+j,"costOfTransaction":"1.2","timeOfTransaction":"","currency":"USD"})
                Store.headerListJson[userAddress]["messages"].push({"messageId":userName+j,"type":3,"timeStamp":dateString(),"viewed":0,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Test "+j,"costOfTransaction":"1.2","timeOfTransaction":"","currency":"USD","messageBody":""});
            }
        }
        counter=Store.headerListJson[userAddress]["messages"].length;
        ptr=Store.headerListJson[userAddress]["messages"];
        for(var i=0;i<counter;i++){
            if(ptr[i].type==0 || ptr[i].type==1){
                numEmails=numEmails+1;
                if(ptr[i].viewed==0)numUnreadEmails=numUnreadEmails+1;
            }else if(ptr[i].type==2 || ptr[i].type==3){
                numSmss=numSmss+1;
                if(ptr[i].viewed==0)numUnreadSmss=numUnreadSmss+1;
            }
        }
        core.log("MessagingServiceSim.qml | beginQueryHeaders | headerList count "+headerList.count);       
        queryHeadersTimer.restart();
    }

    function beginQueryContent(messageId){
        core.info("MessagingServiceSim.qml | beginQueryContent | messageId :"+messageId);
        var ptr = Store.headerListJson[userAddress]["messages"];
        var counter=Store.headerListJson[userAddress]["messages"].length
        for(var i=0;i<counter;i++){
            if(ptr[i].messageId==messageId){
                messageIncoming=ptr[i].messageBody;
                ptr[i].viewed=1;
                break;
            }
        }
        if(queryContentTimer.running) queryContentTimer2.restart();else queryContentTimer.restart();
    }

    function dateString(){
        var d=new Date();
      function pad(n){return n<10 ? '0'+n : n}
      return d.getUTCFullYear()+''
          + pad(d.getUTCMonth()+1)+''
          + pad(d.getUTCDate())+''
          + pad(d.getUTCHours())+''
          + pad(d.getUTCMinutes())+''
          + pad(d.getUTCSeconds())
    }

    function beginSendMessage(){
        core.info("MessagingServiceSim.qml | sendMessage() ");      
        numSmss=0;
        numUnreadSmss=0;
        numEmails=0;
        numUnreadEmails=0;
        var type = pif.smsEmail.__getType();
        var ptr=Store.headerListJson[userAddress]["messages"];
        var counter=Store.headerListJson[userAddress]["messages"].length
        Store.MessageIdCounter=counter+1;        
        ptr.push({"messageId":userName+""+Store.MessageIdCounter,"type":type,"timeStamp":dateString(),"viewed":1,"status":1,"recipient":recipientAddress,"cc":ccAddress,"sender":senderAddress,"subject":subject,"costOfTransaction":"","timeOfTransaction":"","currency":currency,"messageBody":messageOutgoing});
        for(var i=0;i<counter;i++){
            if(ptr[i].type==0 || ptr[i].type==1){
                numEmails=numEmails+1;
                if(ptr[i].viewed==0)numUnreadEmails=numUnreadEmails+1;
            }else if(ptr[i].type==2 || ptr[i].type==3){
                numSmss=numSmss+1;
                if(ptr[i].viewed==0)numUnreadSmss=numUnreadSmss+1;
            }
        }
        headerList.clear();
        for(var j=0;j<=counter;j++){
            headerList.append({"messageId":ptr[j].messageId,"type":parseInt(ptr[j].type,10),"timeStamp":ptr[j].timeStamp,"viewed":parseInt(ptr[j].viewed,10),"status":parseInt(ptr[j].status,10),"recipient":ptr[j].recipient,"cc":ptr[j].cc,"sender":ptr[j].sender,"subject":ptr[j].subject,"costOfTransaction":ptr[j].costOfTransaction,"timeOfTransaction":ptr[j].timeOfTransaction,"currency":ptr[j].currency})
        }
        sendMessageTimer.restart();
    }

    function beginRequestLogout(){
        core.info("MessagingServiceSim.qml | beginRequestLogout() | Logging out of PMS");
        requestLogoutTimer.restart();
    }

    function beginSeatReset(){
        core.info("MessagingServiceSim.qml | beginSeatReset() | Resetting Seat");
        seatResetTimer.restart();
    }

    function __incomingEmailMessage(from,subject,messageBody){
        core.info("MessagingServiceSim.qml | incomingEmailMessage | From :"+from+" ,date :"+dateString()+" ,Subject: "+subject+" ,messageBody: "+messageBody)
        numSmss=0;
        numUnreadSmss=0;
        numEmails=0;
        numUnreadEmails=0;
        var ptr=Store.headerListJson[userAddress]["messages"];
        var counter=Store.headerListJson[userAddress]["messages"].length
        Store.MessageIdCounter=counter+1;
        messageIncoming=messageBody;
        ptr.push({"messageId":userName+""+Store.MessageIdCounter,"type":1,"timeStamp":dateString(),"viewed":0,"status":3,"recipient":"","cc":"","sender":from,"subject":subject,"costOfTransaction":"1.5","timeOfTransaction":"","currency":currency,"messageBody":messageBody});
        for(var i=0;i<counter;i++){
            if(ptr[i].type==0 || ptr[i].type==1){
                numEmails=numEmails+1;
                if(ptr[i].viewed==0)numUnreadEmails=numUnreadEmails+1;
            }else if(ptr[i].type==2 || ptr[i].type==3){
                numSmss=numSmss+1;
                if(ptr[i].viewed==0)numUnreadSmss=numUnreadSmss+1;
            }
        }
        headerList.clear();
        for(var j=0;j<counter;j++){
            headerList.append({"messageId":ptr[j].messageId,"type":parseInt(ptr[j].type,10),"timeStamp":ptr[j].timeStamp,"viewed":parseInt(ptr[j].viewed,10),"status":parseInt(ptr[j].status,10),"recipient":ptr[j].recipient,"cc":ptr[j].cc,"sender":ptr[j].sender,"subject":ptr[j].subject,"costOfTransaction":ptr[j].costOfTransaction,"timeOfTransaction":ptr[j].timeOfTransaction,"currency":ptr[j].currency})
        }
        pif.smsEmail.getMessages();
    }

    function __incomingSmsMessage(from,messageBody){
        core.info("MessagingServiceSim.qml | __incomingSmsMessage | From :"+from+" ,date :"+dateString()+" ,messageBody: "+messageBody)
        numSmss=0;
        numUnreadSmss=0;
        numEmails=0;
        numUnreadEmails=0;
        var ptr=Store.headerListJson[userAddress]["messages"];
        var counter=Store.headerListJson[userAddress]["messages"].length
        Store.MessageIdCounter=counter+1;
        messageIncoming=messageBody;
        ptr.push({"messageId":userName+""+Store.MessageIdCounter,"type":3,"timeStamp":dateString(),"viewed":0,"status":3,"recipient":"","cc":"","sender":from,"subject":"","costOfTransaction":"1.5","timeOfTransaction":"","currency":currency,"messageBody":messageBody});
        for(var i=0;i<counter;i++){
            if(ptr[i].type==0 || ptr[i].type==1){
                numEmails=numEmails+1;
                if(ptr[i].viewed==0)numUnreadEmails=numUnreadEmails+1;
            }else if(ptr[i].type==2 || ptr[i].type==3){
                numSmss=numSmss+1;
                if(ptr[i].viewed==0)numUnreadSmss=numUnreadSmss+1;
            }
        }
        headerList.clear();
        for(var j=0;j<counter;j++){
            headerList.append({"messageId":ptr[j].messageId,"type":parseInt(ptr[j].type,10),"timeStamp":ptr[j].timeStamp,"viewed":parseInt(ptr[j].viewed,10),"status":parseInt(ptr[j].status,10),"recipient":ptr[j].recipient,"cc":ptr[j].cc,"sender":ptr[j].sender,"subject":ptr[j].subject,"costOfTransaction":ptr[j].costOfTransaction,"timeOfTransaction":ptr[j].timeOfTransaction,"currency":ptr[j].currency})
        }
        pif.smsEmail.getMessages();
    }

    function __onQueryStatusCallEnded(){
        queryStatusCallEnded(0);
    }

     function __onAuthenticationInputNeeded(){
         authenticationInputNeeded();
         //authenticationCompleteTimer.restart();
     }

     function __onAuthenticationComplete(){
         authTransactionId="A2382332BHJSKS";
         authCardHash="NBDAJ128731";
         authLast4Digits="0202";
         authCardType="visa";
         authenticationComplete();
     }

     function __onRequestLoginCallEnded(){
         requestLoginCallEnded(0);
     }

     function __onQueryPricesCallEnded(){
         queryPricesCallEnded(0);
     }

    function __onSetUserInfoCallEnded(){
        core.info("MessagingServiceSim.qml | __onSetUserInfoCallEnded | username :"+userName+", UserAddress : "+userAddress);
        setUserInfoCallEnded(0)
    }

     function __onQueryInboxSummaryCallEnded(){
        core.log("MessagingServiceSim.qml | | numSmss :"+numSmss+" ,numUnreadSmss :"+numUnreadSmss+", numEmails :"+numEmails+" ,numUnreadEmails :"+numUnreadEmails+" ,last4Digits :"+last4Digits)
        queryInboxSummaryCallEnded(0);
     }

    function __onQueryHeadersCallEnded(){
        queryHeadersCallEnded(0);
    }

    function __onQueryContentCallEnded(){
        queryContentCallEnded(0);
    }

    function __onSendMessageCallEnded(){
        core.info("MessagingServiceSim.qml | __onSendMessageCallEnded() ")
        sendMessageCallEnded(0);
    }

    function __onRequestLogoutCallEnded(){
        core.info("MessagingServiceSim.qml | __onRequestLogoutCallEnded()")
        __frameworkResetApp();
        requestLogoutCallEnded(4);
    }

    function __onSeatResetCallEnded(){
        core.info("MessagingServiceSim.qml | __onSeatResetCallEnded()");
        __frameworkResetApp();
        seatResetCallEnded(4);
    }

    function __frameworkResetApp(){
        core.info("MessagingServicesSim.qml | Resetting Values ")
        authTransactionId= "";authCardHash= "";authLast4Digits= "";authErrorMessage= "";authCardType= "";sendSmsPrice= 0.0;receiveSmsPrice= 0.0;
        sendEmailPrice= 0.0;receiveEmailPrice= 0.0;currency= "";priceMessage= "";userName= "";userAddress= "";senderName= "";senderAddress= "";numSmss= 0;numUnreadSmss= 0;
        numEmails= 0;numUnreadEmails= 0;last4Digits= "";messageIncoming= "";recipientAddress= "";ccAddress= "";subject= "";
        messageOutgoing= "";
    }

    function __readSimData () {
         fileRequest.source = "";
         fileRequest.source = core.settings.intPath+"config/smsEmailPC.json";
     }

    Configuration{
        id: fileRequest;
        onStatusChanged:{
            if(status==Pif.Ready && fileRequest.source!='' && fileRequest.value){
                core.info("MessagingServiceSim.qml | Configuration onStatusChanged | Data Ready");
                Store.headerListJson=fileRequest.value;
            }else if(status==Pif.Error && fileRequest.source!=''){
                core.info("MessagingServiceSim.qml | Configuration onStatusChanged | ErrorStatus: "+status+" ERROR reading file: "+source);
                core.info("MessagingServiceSim.qml | Configuration onStatusChanged | Defaulting to Inbuilt Json");
                Store.headerListJson={
                    "numberOfUsers": 2,
                    "abc@gmail.com":{
                                       "userName":"abc",
                                       "messages":[
                                              {"messageId":"abc1","type":0,"timeStamp":"20130715102508","viewed":0,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Hi 1","costOfTransaction":"1.5","timeOfTransaction":"","currency":"USD","messageBody":"Hi t1. It is a good day . see you take care"},
                                              {"messageId":"abc2","type":3,"timeStamp":"20130715102508","viewed":1,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Hi 2","costOfTransaction":"2","timeOfTransaction":"","currency":"USD","messageBody":"Hi t2. It is a good day . see you take care"},
                                              {"messageId":"abc3","type":2,"timeStamp":"20130715102508","viewed":1,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Hi 3","costOfTransaction":"2.5","timeOfTransaction":"","currency":"USD","messageBody":"Hi t3. It is a good day . see you take care"},
                                              {"messageId":"abc4","type":1,"timeStamp":"20130715102508","viewed":0,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Hi 4","costOfTransaction":"1.5","timeOfTransaction":"","currency":"USD","messageBody":"Hi t4. It is a good day . see you take care"}
                                        ]
                                     },
                    "default":{
                                       "userName":"default",
                                       "messages":[
                                              {"messageId":"default1","type":1,"timeStamp":"20130715102508","viewed":0,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Hi dt1","costOfTransaction":"1.5","timeOfTransaction":"","currency":"USD","messageBody":"Hi dt1. It is a good day . see you take care"},
                                              {"messageId":"default2","type":1,"timeStamp":"20130715102508","viewed":0,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Hi dt2","costOfTransaction":"2","timeOfTransaction":"","currency":"USD","messageBody":"Hi dt2. It is a good day . see you take care"},
                                              {"messageId":"default3","type":1,"timeStamp":"20130715102508","viewed":0,"status":1,"recipient":"xyz@gmail.com","cc":"","sender":"abc","subject":"Hi dt3","costOfTransaction":"2.5","timeOfTransaction":"","currency":"USD","messageBody":"Hi dt3. It is a good day . see you take care"}
                                        ]
                              }
                  }
            }
        }
    }

    Component.onCompleted: {
         core.info("MessagingServiceSim.qml | chatManagerSIM | Component loaded: "+core.settings.intPath);
        Store.MessageIdCounter=0;Store.headerListJson=[];__readSimData();
    }

     Timer { id:appActiveTimer;interval:50; onTriggered: __onQueryStatusCallEnded();}
     Timer { id:authenticateTimer;interval:50; onTriggered: __onAuthenticationInputNeeded();}
     Timer { id:authenticationCompleteTimer;interval:50; onTriggered: __onAuthenticationComplete();}
     Timer { id:requestLoginTimer;interval:50; onTriggered: __onRequestLoginCallEnded();}
     Timer { id:queryPricesTimer;interval:50; onTriggered: __onQueryPricesCallEnded();}
     Timer { id:setUserInfoTimer;interval:50; onTriggered: __onSetUserInfoCallEnded();}
     Timer { id:inboxSummaryTimer;interval:50; onTriggered: __onQueryInboxSummaryCallEnded();}
     Timer { id:queryHeadersTimer;interval:50; onTriggered: __onQueryHeadersCallEnded();}
     Timer { id:queryContentTimer;interval:50;onTriggered: __onQueryContentCallEnded();}
     Timer { id:queryContentTimer2;interval:50;onTriggered:__onQueryContentCallEnded();}
     Timer { id:sendMessageTimer;interval:50; onTriggered: __onSendMessageCallEnded();}
     Timer { id:requestLogoutTimer;interval:50; onTriggered: __onRequestLogoutCallEnded();}
     Timer { id:seatResetTimer;interval:50; onTriggered: __onSeatResetCallEnded();}

}
