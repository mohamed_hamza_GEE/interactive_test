import QtQuick 1.1
import "../components"
import Panasonic.Pif 1.0

Item{
    id:lht
    property variant evidClient:pif.vip.evidClient;
    property int volume:0;
    property int volSource:0;
    property int brightnessLevel:0;
    property int contrastLevel:0;

    property string backlightLevel:"OFF";

    signal brightnessChanged(int brightnessLevel);
    signal contrastChanged(int contrastLevel);
    signal backlightStateChanged(int backlightState);
    signal sigMonitorModeChanged(string mode);
    signal evidChanged(string evidName, variant value);

    Connections{
        target: evidClient?evidClient:null
        onMessageReceived: {__evidMessageReceived(command, value);}
        onConnectedChanged:{if(evidClient.connected){__getEvidValues();}}
    }

    //Public api
    function initialise(){core.info("Lht.qml | initialise.");}

    function setBrightnessByStep(step){
        core.info("Lht.qml | Set Brightness by step called. Step:"+step);
        var brightvalue=brightnessLevel+pif.getBrightnessStep()*(String(parseInt(step,10))!="NaN"?parseInt(step,10):0);
        brightvalue = brightvalue<0?0:brightvalue>100?100:brightvalue;
        var evidBrightness = pif.getLruData("Monitor_Brightness_Control_Type");
        if (!evidBrightness) return false;
        evidClient.send(evidBrightness,brightvalue);
        return true;
    }

    function setBrightnessByValue(brightvalue){
        core.info("Lht.qml | Set Brightness by value called. value:" + brightvalue);
        brightvalue = brightvalue<0?0:brightvalue>100?100:brightvalue;
        var evidBrightness = pif.getLruData("Monitor_Brightness_Control_Type");
        if (!evidBrightness) return false;
        evidClient.send(evidBrightness,brightvalue);
        return true;
    }

    function setContrastByStep(step){   // contrastlevel => 1 or 2... to increase Contrast, -1 or -2... to decrease Contrast, 0 or blank: no change
        core.info("Lht.qml | Set Contrast by step called. Step:"+step);
        var contrastvalue=contrastLevel+pif.getContrastStep()*(String(parseInt(step,10))!="NaN"?parseInt(step,10):0);
        contrastvalue = contrastvalue<0?0:contrastvalue>100?100:contrastvalue;
        var evidContrast = pif.getLruData("Monitor_Contrast_VID");
        if (!evidContrast) return false;
        evidClient.send(evidContrast,contrastvalue);
        return true;
    }

    function setContrastByValue(contrastvalue){     // contrastvalue => between 0 to 100.
        core.info("Lht.qml | Set Contrast by value called");
        contrastvalue = !contrastvalue||contrastvalue<0?0:contrastvalue>100?100:contrastvalue;
        var evidContrast = pif.getLruData("Monitor_Contrast_VID");
        if (!evidContrast) return false;
        evidClient.send(evidContrast,contrastvalue);
        return true;
    }

    function setBacklightOn(){
        var evidScreenOn = pif.getLruData("Monitor_Power_VID");
        core.info("Lht.qml | setBacklightOn: "+evidScreenOn);
        if (evidScreenOn) evidClient.send(evidScreenOn,"ON");
    }

    function setBacklightOff(){
        var evidScreenOn = pif.getLruData("Monitor_Power_VID");
        core.info("Lht.qml | setBacklightOff: "+evidScreenOn);
        if (evidScreenOn) evidClient.send(evidScreenOn,"OFF");
    }

    function setMonitorMode(playingSource){
        core.info("Lht.qml | Set Monitor mode called. mode: " + playingSource);
        var monitorVid = pif.getLruData("Monitor_VID");
        if (!monitorVid) return false;
        switch(playingSource){
        case "hdmi1":{
            evidClient.send(monitorVid,"HDMI_1");//"HDMI_1"
            break;
        }
        case "hdmi2":{
            evidClient.send(monitorVid,"HDMI_2");//"HDMI_2"
            break;
        }
        case "vga":{
            evidClient.send(monitorVid,"VGA");//"VGA"
            break;
        }
        case "interactive":{
            evidClient.send(monitorVid,"COMPONENT");//"COMPONENT"
            break;
        }
        }
        return true;
    }

    function getBrightnessLevel(){return brightnessLevel;}

    function getBacklightState(){
        if(backlightLevel=="ON") return 1;
        else return 0;
    }

    function getContrastLevel(){return contrastLevel;}

    //Private functions
    function __getEvidValues(){
        //get the values of volume, brightness, contrast, screen on/off, monitor mode.

        var evidBrightness = pif.getLruData("Monitor_Brightness_Control_Type");
        if (evidBrightness) {
            core.info("Lht.qml | Requesting current value evidBrightness: "+evidBrightness);
            evidClient.requestValue(evidBrightness);
        }

        var evidContrast = pif.getLruData("Monitor_Contrast_VID");
        if (evidContrast) {
            core.info("Lht.qml | Requesting current value evidContrast: "+evidContrast);
            evidClient.requestValue(evidContrast);
        }

        var evidScreenOn = pif.getLruData("Monitor_Power_VID");
        if (evidScreenOn) {
            core.info("Lht.qml | Requesting current value evidScreenOn: "+evidScreenOn);
            evidClient.requestValue(evidScreenOn);
        }
    }

    function __evidMessageReceived(command, value){
        core.info("Lht.qml | __evidMessageReceived called command: " + command + " value: " + value);
        if(command === pif.getLruData("Monitor_Brightness_Control_Type")){
            brightnessLevel = value;
            brightnessChanged(value);
        }else if(command === pif.getLruData("Monitor_Contrast_VID")){
            contrastLevel = value;
            contrastChanged(value);
        }else if(command === pif.getLruData("Monitor_Power_VID")){
            backlightLevel = value;
            if(backlightLevel == "ON")backlightStateChanged(1);
            else backlightStateChanged(0);
        }else if(command === pif.getLruData("Monitor_VID")){
            core.info("Lht.qml | signal sigMonitorModeChanged() will be fired");
            sigMonitorModeChanged(value);
        }else{evidChanged(command, value);}
    }

    Component.onCompleted: {
        core.info("Lht.qml | LHT component load complete "+pif.vip.evidClient);
        if(pif.vip.evidClient && pif.vip.evidClient.connected){__getEvidValues();}
    }
}
