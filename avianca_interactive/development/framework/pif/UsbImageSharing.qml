import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    id:imgSharing;
    property bool __isEnable: false;
    property int pendingInvitation : 0;

    signal sigConnectionRefused(string seatNo);
    signal sigImageReceived(variant message, string seatNo);
    signal sigSharingStatus(string status, string seatNo);
    signal sigInvitationReceived(string seatNo, string imgName, string imgPath, string imgText);
    signal sigUsbDisconnectedAtSender(string seatNo);

    function enable(){__isEnable=true; return true;}
    function disable(){
        if(!__isEnable){ core.info("UsbImageSharing.qml | usb image sharing is already disabled. Exiting...."); return false;}
        else {__isEnable=false;frmwkResetApp();}
        return true;
    }
    function acceptInvite(index){__setInvitationStatus("accept",index);}
    function declineInvite(index){__setInvitationStatus("decline",index);}
    function optOutInvite(index){__setInvitationStatus("optout",index);}
    function getSharingStatus(){return __isEnable;}
    function getSenderSharedImagesModel(){return __senderSharedImagesModel;}
    function getReceivedImagesModel(){return __receivedImagesModel;}
    function getInvitationReceivedModel(){return __invitationReceivedModel;}

    function initialize(img,receiverSeatNo,imgText){
        if(pif.usb.getUsbConnected()){Store.usbDisconnected=false;}else {Store.usbDisconnected=true;core.info("UsbImageSharing.qml | initialize | Usb is disconnected ");return false;}
        if(receiverSeatNo=="" || !receiverSeatNo){core.info("UsbImageSharing.qml | initialize | receiverSeatNo Not Set ");return false;}
        if(!imgText) imgText="";
        var tmpArray=img.split(",");var imgNameArray=[];
        for(var counter in tmpArray){
            var x=tmpArray[counter].split("/");
            imgNameArray.push(x[x.length-1]);
        }
        var imgName=imgNameArray.join(",");
        var receiverSeatNoArray=receiverSeatNo.split(",");
        for(var counter in receiverSeatNoArray){
            var indxOfMid=Store.senderRecipientsList.indexOf(receiverSeatNoArray[counter].toLowerCase());
            if(indxOfMid<0){Store.senderRecipientsList.push(receiverSeatNoArray[counter].toLowerCase());}
            pendingInvitation++;
            for(var counter1 in tmpArray){
                __senderSharedImagesModel.append({"seatNo":receiverSeatNoArray[counter].toLowerCase(),"imgName":imgNameArray[counter1],"imgPath":tmpArray[counter1],"imgText":imgText,"userDefined1":"","userDefined2":""});
                var indxOfBlockedMid=Store.blockedRecipientsList.indexOf(receiverSeatNoArray[counter].toLowerCase());
                if(indxOfBlockedMid>=0){Store.blockedRecipientsList.splice(indxOfBlockedMid,1)}
            }
            imgName = imgName.replace(/'/g, "\'");
            img = img.replace(/'/g, "\'");
            imgText = imgText.replace(/'/g, "\'");
            Store.senderRequestQueue.push({imgName:imgName,imgPath:img,seatNo:receiverSeatNoArray[counter].toLowerCase(),imgText:imgText});
        }
        core.debug("UsbImageSharing.qml | initialize | img: "+img+" | imgText: "+imgText+" | senderSeat: "+pif.getSeatNumber()+" receiverSeat: "+receiverSeatNo);
        __processSenderQueue();
    }

    function getSenderPendingInvitationCount(){return pendingInvitation;}

    function __processSenderQueue(){
        if(!Store.isClient1Active){
            if(Store.senderRequestQueue.length==0){core.debug("UsbImageSharing.qml | __processSenderQueue | No request pending"); return true;}
            Store.isClient1Active=true;
            var tmp=Store.senderRequestQueue[0];
            core.info("UsbImageSharing.qml | __processSenderQueue | processing: "+JSON.stringify(tmp));
            var receiver=__getHostName(tmp.seatNo);
            __setClient1ServerHostName(receiver);
        }else {
            core.debug("UsbImageSharing.qml | __processSenderQueue | isClient1Active: "+Store.isClient1Active+" | Current Receiver: "+client1.serverHostname);
        }
    }

    function __setInvitationStatus(action,index){
        if(!index){index=0;}
        var tmp=__invitationReceivedModel.at(index);
        core.info("UsbImageSharing.qml | __setInvitationStatus | action: "+action+" | index:"+index+" | seatNo: "+tmp.seatNo+" | imgName: "+tmp.imgName);
        if(tmp.seatNo=="" || !tmp.seatNo){core.info("UsbImageSharing.qml | __setInvitationStatus | SeatNo Not Set ");return false;}
        __invitationReceivedModel.remove(index,true);
        if(action=="optout"){disable();}
        Store.receiverInvitationQueue.push({"seatNo":tmp.seatNo,"action":action,"imgName":tmp.imgName,"imgPath":tmp.imgPath,"imgText":tmp.imgText});
         __processIntivationRequest();
        if(action=="accept"){
            var imgNameArr =tmp.imgName.split(",");
            var imgPathArr =tmp.imgPath.split(",");
            for(var j in imgNameArr){
                __receivedImagesModel.append({"seatNo":tmp.seatNo,"imgName":imgNameArr[j],"imgPath":imgPathArr[j],"imgText":tmp.imgText,"userDefined1":"","userDefined2":""});
            }
        }
        return true;
    }

    function __processIntivationRequest(){
        if(!Store.isClient2Active){
            if(Store.receiverInvitationQueue.length==0){core.debug("UsbImageSharing.qml | __processIntivationRequest | No request pending");return true;}
            Store.isClient2Active=true;
            var sender=__getHostName(Store.receiverInvitationQueue[0].seatNo);
            __setClient2ServerHostName(sender);
        }else {
            core.debug("UsbImageSharing.qml | __processIntivationRequest | isClient2Active: "+Store.isClient2Active+" | Current Receiver: "+client2.serverHostname);
            return false;
        }
    }

    function __generateJsonMessage(result){
        var retJsonMsg = result;
        return JSON.stringify(retJsonMsg);
    }

    function __getHostName(seatNo){
        if(seatNo.length==2) seatNo="seat00"+seatNo;
        else if(seatNo.length==3) seatNo="seat0"+seatNo;
        else seatNo="seat"+seatNo;
        return seatNo;
    }

    function __getSeatNumber(seatNo){
        return seatNo.replace("seat00","").replace("seat0","");
    }

    function __setClient1ServerHostName(seatNo){
        if(seatNo==undefined){ core.info("UsbImageSharing.qml | __setClient1ServerHostName | Need seatNo to continue. Exiting...."); return false;}
        else {client1.online=false; client1.serverHostname=seatNo; client1.online=true;
            core.info("UsbImageSharing.qml | __setClient1ServerHostName | seatNo: "+seatNo);
            return true;
        }
    }

    function __setClient2ServerHostName(seatNo){
        if(seatNo==undefined){ core.info("UsbImageSharing.qml | __setClient2ServerHostName | Need seatNo to continue. Exiting...."); return false;}
        else {client2.online=false; client2.serverHostname=seatNo; client2.online=true;
            core.info("UsbImageSharing.qml | __setClient2ServerHostName | seatNo: "+seatNo);
            return true;
        }
    }

    function __setClient3ServerHostName(seatNo){
        if(seatNo==undefined){ core.info("UsbImageSharing.qml | __setClient3ServerHostName | Need seatNo to continue. Exiting...."); return false;}
        else {client3.online=false; client3.serverHostname=seatNo; client3.online=true;
            core.info("UsbImageSharing.qml | __setClient3ServerHostName | seatNo: "+seatNo);
            return true;
        }
    }

    function __requestToShare(senderSeatNo,imgName,imgPath,imgText){
        core.debug("UsbImageSharing.qml | __requestToShare | Invitation Received:  senderSeatNo is " + senderSeatNo+" |  imgName  is " + imgName+" | imgText: "+imgText  );
            core.debug("UsbImageSharing.qml | emitting signal Siginvitation "+senderSeatNo+" | "+imgName+" | imgText "+imgText);
            __invitationReceivedModel.append({"seatNo":senderSeatNo,"imgName":imgName,"imgPath":imgPath,"imgText":imgText});
            sigInvitationReceived(senderSeatNo,imgName,imgPath,imgText);
    }

    function getImage(img,senderNo){
        core.info("UsbImageSharing.qml | getImage | senderNo: "+senderNo+" | img: "+img);
        senderNo = __getHostName(senderNo);
        Store.receiverImageRequest={img:img,senderNo:senderNo}
        __setClient3ServerHostName(senderNo);
    }

    function getImageAck(clientObj,img){
        core.debug("UsbImageSharing.qml | getImageAck | step 5 : sending img to receiver: "+clientObj.properties.name)
        clientObj.sendMessage("image",img);
    }

    function __processUsbDisconnectedAtSender(){
        if(!Store.isClient1Active){
            if(Store.senderRecipientsList.length==0){core.debug("UsbImageSharing.qml | __processUsbDisconnectedAtSender | No request pending");Store.usbDisconnected=false;return true;}
            Store.isClient1Active=true;

            var sender=__getHostName(Store.senderRecipientsList[0]);
            __setClient1ServerHostName(sender);
        }else {
            core.debug("UsbImageSharing.qml | __processUsbDisconnectedAtSender | isClient1Active: "+Store.isClient1Active+" | Current Receiver: "+client1.serverHostname);
            return false;
        }
    }

    function isSeatPresentInRecipientsList(seatNum){
        seatNum=seatNum.toLowerCase();
        var indxOfMid=Store.senderRecipientsList.indexOf(seatNum);
        if(indxOfMid>=0){return true;} else return false;
    }

    function isSeatPresentInBlockedRecipientsList(seatNum){
        seatNum=seatNum.toLowerCase();
        var indxOfMid=Store.blockedRecipientsList.indexOf(seatNum);
        if(indxOfMid>=0){return true;} else return false;
    }

    Connections{
        target: pif.__sharedModelServer;
        onSigUsbSharingDataReceived:{
            core.debug("UsbImageSharing.qml | onSigUsbSharingDataReceived | message is " + JSON.stringify(msgObj));
            if(msgObj.requestToShare){
                client.send({messageType: "requestToShareAck",sharingStatus:(__isEnable?"enable":"disable"),reason:"USBSharing",imgName:msgObj.__imgName,imgPath:msgObj.__imgPath});
                if(!__isEnable){return false;}
                __requestToShare(msgObj.__senderSeatNo,msgObj.__imgName,msgObj.__imgPath,msgObj.__imgText);
            }else if(msgObj.invitationStatus){
                core.debug("UsbImageSharing.qml | onSigUsbSharingDataReceived | invitationStatus rl: "+isSeatPresentInRecipientsList(client.properties.name)+" | bkl: "+isSeatPresentInBlockedRecipientsList(client.properties.name));
                if(isSeatPresentInRecipientsList(client.properties.name) && !isSeatPresentInBlockedRecipientsList(client.properties.name)){
                    sigSharingStatus(msgObj.status,__getSeatNumber(client.properties.name));
                    pendingInvitation--;
                    client.send({messageType: "invitationStatusAck",reason:"USBSharing"});
                    if(msgObj.status=="optout"){
                        var indxOfMid=Store.senderRecipientsList.indexOf(client.properties.name.toLowerCase());
                        if(indxOfMid>=0){Store.senderRecipientsList.splice(indxOfMid,1);}
                        removeEntriesFromModels(client.properties.name.toLowerCase());
                        removeEntryFromSenderModel(client.properties.name.toLowerCase(),msgObj.imgPath);
                    }
                }else{
                    Store.senderRecipientsList.push(client.properties.name.toLowerCase());
                    Store.blockedRecipientsList.push(client.properties.name.toLowerCase());
                    Store.usbDisconnected=true;
                    __processUsbDisconnectedAtSender();
                }
            }else if(msgObj.getImage){
                core.info("UsbImageSharing.qml | getImage request from UsbImageSharing client "+client.properties.name);
                core.debug("UsbImageSharing.qml | onSigUsbSharingDataReceived | getImage rl: "+isSeatPresentInRecipientsList(client.properties.name)+" | bkl: "+isSeatPresentInBlockedRecipientsList(client.properties.name));
                if(isSeatPresentInRecipientsList(client.properties.name) && !isSeatPresentInBlockedRecipientsList(client.properties.name)){
                    getImageAck(client,msgObj.img)}
                else{
                    Store.senderRecipientsList.push(client.properties.name.toLowerCase());
                    Store.blockedRecipientsList.push(client.properties.name.toLowerCase());
                    Store.usbDisconnected=true;
                    __processUsbDisconnectedAtSender();
                }
            }
            else if(msgObj.senderSeatUsbDisconnect){
                sigUsbDisconnectedAtSender(msgObj.seatNo);
                client.send({messageType: "senderSeatUsbDisconnectAck",reason:"USBSharing"});
                removeEntriesFromModels(msgObj.seatNo);
            }
        }
    }

    function removeEntriesFromModels(seatNo){
        for(var counter=0;counter<__receivedImagesModel.count;counter++){
            var tmp=__receivedImagesModel.at(counter);
            if(tmp.seatNo==seatNo){__receivedImagesModel.remove(counter,true);counter--;};
        }

        for(var counter=0;counter<__invitationReceivedModel.count;counter++){
            var tmp=__invitationReceivedModel.at(counter);
            if(tmp.seatNo==seatNo){__invitationReceivedModel.remove(counter,true);counter--;}
        }
    }

    function removeEntryFromSenderModel(seatNo,imgPath){
        var str = imgPath;
        var imgPathArr = str.split(',');
        for(var counter=__senderSharedImagesModel.count-1; counter >= 0; counter--){
            var tmp = __senderSharedImagesModel.at(counter);
            for(var i = 0; i < imgPathArr.length; i++){
                if(tmp.seatNo==seatNo && tmp.imgPath==imgPathArr[i]){__senderSharedImagesModel.remove(counter,true);}
            }
        }
    }

    function __serverConnectedAck(client){
       core.debug("UsbImageSharing.qml | __serverConnectedAck | step 1: __init : init message sent to server "+client.serverHostname);
       client.sendMessage("json",__generateJsonMessage({name:pif.getSeatNumber(),reason:"USBSharing",messageType:"init","serverHostname":client.serverHostname}));
    }

    function __disconnectLocalClient1(){client1.online=false;Store.isClient1Active=false;var x=Store.senderRequestQueue.shift();__processSenderQueue();return true;}
    function __disconnectLocalClient1UsbDisconnect(){client1.online=false;Store.isClient1Active=false;__processUsbDisconnectedAtSender();return true;}
    function __disconnectLocalClient2(){client2.online=false;Store.isClient2Active=false;Store.receiverInvitationQueue.shift();__processIntivationRequest();return true;}
    function __disconnectLocalClient3(){client3.online=false;return true;}

    function __client1ReceivedAck(message,messageType){
        core.info("UsbImageSharing.qml | __client1ReceivedAck, message:"+JSON.stringify(message)+" messageType: "+messageType)
        if(message.messageType=="initAck" && Store.usbDisconnected==true) {
            core.debug("UsbImageSharing.qml | __client1ReceivedAck | step 2: initAck | usbDisconnected: "+Store.usbDisconnected);
            var tmp=Store.senderRecipientsList.shift();
            client1.sendMessage("json",__generateJsonMessage({senderSeatUsbDisconnect:"true",seatNo:pif.getSeatNumber(),reason:"USBSharing"}));
        }else if(message.messageType=="initAck") {
            core.debug("UsbImageSharing.qml | __client1ReceivedAck | step 2: initAck ");
            var tmp=Store.senderRequestQueue[0];
            client1.sendMessage("json",__generateJsonMessage({requestToShare:true,__imgName:tmp.imgName,__imgPath:tmp.imgPath,__imgText:tmp.imgText,__senderSeatNo:pif.getSeatNumber(),reason:"USBSharing"}));
        }else if(message.messageType=="requestToShareAck") {
            core.info("UsbImageSharing.qml | __client1ReceivedAck | step 3: requestToShareAck | sharingStatus: "+message.sharingStatus);
            if(message.sharingStatus!=undefined){sigSharingStatus(message.sharingStatus,__getSeatNumber(client1.serverHostname));
                if(message.sharingStatus=="disable"){
                    pendingInvitation--;
                    removeEntriesFromModels(client1.serverHostname.toLowerCase());
                    removeEntryFromSenderModel(__getSeatNumber(client1.serverHostname),message.imgPath);
                }
            }
            __disconnectLocalClient1();
        }else if(message.messageType=="senderSeatUsbDisconnectAck") {
            core.debug("UsbImageSharing.qml | __client1ReceivedAck | senderSeatUsbDisconnectAck");
            __disconnectLocalClient1UsbDisconnect();
        }
    }

    function __client2ReceivedAck(message,messageType){
        core.debug("UsbImageSharing.qml | __client2ReceivedAck, message:"+JSON.stringify(message)+" messageType: "+messageType)
        if(message.messageType=="initAck") {
            core.debug("UsbImageSharing.qml | __client2ReceivedAck | step 2: initAck ");
            var tmp=Store.receiverInvitationQueue[0];
            client2.sendMessage("json",__generateJsonMessage({invitationStatus:true,status:tmp.action,imgName:tmp.imgName,imgPath:tmp.imgPath,reason:"USBSharing"}));
        }else if(message.messageType=="invitationStatusAck"){
            __disconnectLocalClient2();
        }
    }

    function __client3ReceivedAck(message,messageType){
        if(messageType=="image"){core.debug("UsbImageSharing.qml | __client3ReceivedAck | messageType: "+messageType+" | message Length: "+message.length);
        }else {core.debug("UsbImageSharing.qml | __client3ReceivedAck, message:"+JSON.stringify(message)+" messageType: "+messageType)}
        if(message.messageType=="initAck") {
            core.debug("UsbImageSharing.qml | __client3ReceivedAck | step 2: initAck ");
             client3.sendMessage("json",__generateJsonMessage({getImage:true,img:Store.receiverImageRequest.img,reason:"USBSharing"}));
        }else if(message.queueSync=="SET"){core.debug("UsbImageSharing.qml | __client3ReceivedAck | message.queueSync "+message.queueSync);
        }else if(messageType=="image"){ sigImageReceived(message,__getSeatNumber(client3.serverHostname));__disconnectLocalClient3();}
    }

    Connections{target: pif.usb;onSigUsbConnected:Store.usbDisconnected=false; onSigUsbDisconnected:{Store.usbDisconnected=true;__processUsbDisconnectedAtSender();}}

    NetworkClient{
        id:client1;
        onConnected: __serverConnectedAck(client1);
        onReceived: __client1ReceivedAck(message,messageType);
        onOnlineChanged: core.debug("UsbImageSharing.qml | client1 onOnlineChanged "+online);
        onServerHostnameChanged: core.debug("UsbImageSharing.qml | client1 onServerHostnameChanged "+serverHostname);
        onDisconnected: {core.debug("UsbImageSharing.qml | client1 onDisconnected");if(!Store.isClient1Active){__disconnectLocalClient1();}}
        onError:{core.debug("UsbImageSharing.qml | client1 onError "+error);sigConnectionRefused(__getSeatNumber(serverHostname));pendingInvitation--;__disconnectLocalClient1();}
    }

    NetworkClient{
        id:client2;
        onConnected: __serverConnectedAck(client2);
        onReceived: __client2ReceivedAck(message,messageType);
        onOnlineChanged: core.debug("UsbImageSharing.qml | client2 onOnlineChanged "+online);
        onServerHostnameChanged: core.debug("UsbImageSharing.qml | client2 onServerHostnameChanged "+serverHostname);
        onDisconnected: {core.debug("UsbImageSharing.qml | client2 onDisconnected");__disconnectLocalClient2();}
        onError:{core.debug("UsbImageSharing.qml | client2 onError "+error);sigConnectionRefused(__getSeatNumber(serverHostname)); __disconnectLocalClient2();}
    }

    NetworkClient{
        id:client3;
        onConnected: __serverConnectedAck(client3);
        onReceived: __client3ReceivedAck(message,messageType);
        onOnlineChanged: core.debug("UsbImageSharing.qml | client3 onOnlineChanged "+online);
        onServerHostnameChanged: core.debug("UsbImageSharing.qml | client3 onServerHostnameChanged "+serverHostname);
        onDisconnected: {core.debug("UsbImageSharing.qml | client3 onDisconnected");__disconnectLocalClient3();}
        onError:{core.debug("UsbImageSharing.qml | client3 onError "+error);sigConnectionRefused(__getSeatNumber(serverHostname)); __disconnectLocalClient3();}
    }

    SimpleModel{id:__senderSharedImagesModel}
    SimpleModel{id:__receivedImagesModel}
    SimpleModel{id:__invitationReceivedModel}

    function frmwkResetApp(){
        core.info("UsbImageSharing.qml | frmwkResetApp | Called ");
        Store.senderRequestQueue=[]; Store.receiverImageRequest={};Store.receiverInvitationQueue=[];Store.senderRecipientsList=[];Store.blockedRecipientsList=[];
        client1.online=false;Store.isClient1Active=false;
        client2.online=false;Store.isClient2Active=false;
        client3.online=false;Store.usbDisconnected=false;
        pendingInvitation=0;
        __senderSharedImagesModel.clear();__invitationReceivedModel.clear();__receivedImagesModel.clear();
    }

    Component.onCompleted: {frmwkResetApp();}
}
