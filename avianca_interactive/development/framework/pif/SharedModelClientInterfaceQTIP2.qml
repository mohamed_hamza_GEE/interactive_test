import QtQuick 1.1
import Panasonic.Pif 1.0
import Panasonic.remoteKey 1.0
import "../components"
import "../blank.js" as Store

// PURPOSE: Interface between client & Shared Model. File loaded on client side only.

SharedModelClientQTIP2{
    id:sharedModelClientInterface;
    isClient: true;

    signal sigDataReceivedFromSeat(string api, variant params);
    signal connected();
    signal disconnected();
    signal dataSynchronized();
    signal sigPlaylistError(int errorCode, int pid,string playlistType);
    signal sigPlaylistAdded(int success, int pid,string playlistType);
    signal sigPlaylistMoved(int success, int pid,string playlistType);
    signal sigPlaylistIndex(int playlistIndex,string mediaType)
    signal sigPlaylistTrackRemoved(int pid,string playlistType);
    signal sigPlaylistAllTrackRemoved(int pid,string playlistType);
    signal sigPlaylistAggregateMidRemoved(string playlistType, int pid);
    property int cSUCCESS : 0;
    property int cMAXIMUM_PLAYLIST_REACHED: 1;
    property int cPLAYLIST_EXISTS: 2;
    property int cPLAYLIST_DOES_NOT_EXIST: 3;
    property int cPLAYLIST_FULL: 4;
    property int cSAME_PLAYLIST_NAME: 5;
    property int cMID_ALREADY_IN_PLAYLIST: 6;
    property bool triggerBinding: false;
    property bool triggerAlbumBinding:false;
    property bool paymentDoneFromKarma: true;
    property variant karmaRemote:null;

    onServerHostnameChanged: core.debug("SharedModelClientInterface.qml | serverHostname is | " + serverHostname);
    onSynchronized: {core.info("SharedModelClientInterface.qml | onSynchronized called "); dataSynchronized();}
    onLocalClientConnectedChanged: {
        if (localClientConnected) { core.info("SharedModelClientInterface.qml | LocalClientConnected"); sharedModelClientInterface.connected();}
        else { core.info("SharedModelClientInterface.qml | LocalClientDisconnected | Doing interactiveReset "); sharedModelClientInterface.disconnected(); pif.__interactiveResetAck();}
    }

    Connections{
        target: pif.aod
        onSigMidPlayBegan:{sendFrameworkApi("frameworkPasStart",{"deviceName":Store.pasDeviceName})}
        onSigMidPlayStopped:{sendFrameworkApi("frameworkPasStop")}
    }

    Connections{
        target: pif.ppv?pif.ppv:null;
        onSigPaymentSuccessful: {
            if(!paymentDoneFromKarma) {paymentDoneFromKarma=true; return true;}
            sendFrameworkApi('frameworkPaymentSuccessful',{amid:amid})
        }
    }

    function __loadkarmaRemoteKeyHandlerComp(){
        core.info("__loadkarmaRemoteKeyHandlerComp() called.");
        karmaRemote = karmaRemoteComp.createObject(sharedModelClientInterface);
    }

    function dataReceived(api,params){
        if(!params){var params = [];}
        core.log("SharedModelClientInterface.qml | dataReceived | "+api+" | "+JSON.stringify(params));
        if(api=="playVodAck" || api=="playTrailerAck" || api=="playVodAggregateAck"){
            if(Store.vodMediaResume){Store.vodMediaResume=false;core.debug("Received message from seat | apiName: "+ api + " | vodMediaResume is true not emmiting playVodAck");return true;}
            params = {mid:params.mid,mediaType:params.mediaType,aggregateMid:params.aggregateMid,soundtrackLid:params.soundtrackLid,soundtrackType:params.soundtrackType,subtitleLid:params.subtitleLid,subtitleType:params.subtitleType,elapsedTime:params.elapsedTime}
            core.debug("Received message from seat | apiName: "+ api + " | mid : "+ params.mid + ", aggregateMid : " + params.aggregateMid + ", mediaType : "+params.mediaType+" , soundtrackLid : " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType + ", subtitleLid : " + params.subtitleLid + ", subtitleType : " + params.subtitleType + ", elapsedTime : " + params.elapsedTime);

        } else if(api=="aspectRatioAck"){
            params = {aspectRatio:params.aspectRatio}; core.debug("Received message from seat | apiName: "+ api + " | aspectRatio : " + params.aspectRatio);

        } else if(api=="videoSoundtrackAck"){
            params = {soundtrackLid:params.soundtrackLid,soundtrackType:params.soundtrackType}
            core.debug("Received message from seat | apiName: "+ api + " |  soundtrackLid : " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType);

        } else if(api=="videoSubtitleAck"){
            params = {subtitleLid:params.subtitleLid,subtitleType:params.subtitleType}
            core.debug("Received message from seat | apiName: "+ api + " | subtitleLid : "+ params.subtitleLid + ", subtitleType : " + params.subtitleType);

        } else if(api=="setMediaPositionAck"){
            params = {elapsedTime:params.elapsedTime}; core.debug("Received message from seat | apiName: "+ api + " | elapsedTime : " + params.elapsedTime);

        }else if(api=="mediaPauseAck" || api=="mediaResumeAck" || api=="videoStopAck" || api=="audioStopAck" || api=="closeAppAck"){
            if(api=="mediaResumeAck"){Store.vodMediaResume=true;}else {Store.vodMediaResume=false;}
            core.debug("Received message from seat | apiName: "+ api);

        } else if(api=="mediaFastForwardAck" || api=="mediaRewindAck"){
            params = {mediaPlayRate:params.mediaPlayRate}; core.debug("Received message from seat | apiName: "+ api + " | mediaPlayRate : " + params.mediaPlayRate);

        } else if(api=="seatbackVolume"){
            params = {seatbackVolume:seatbackVolume}; core.debug("Received message from seat | seatbackVolume : " + params.seatbackVolume);

        } else if(api=="seatbackPaVolume"){
            params = {seatbackPaVolume:seatbackPaVolume}; core.debug("Received message from seat | seatbackPaVolume : " + params.seatbackPaVolume);

        } else if(api=="headsetVolume"){
            params = {headsetVolume:headsetVolume}; core.debug("Received message from seat | headsetVolume : " + params.headsetVolume);

        } else if(api=="muteVolume"){
            params = {muteVolume:muteEnabled}; core.debug("Received message from seat | muteVolume : " + params.muteVolume);

        } else if(api=="seatbackBrightness"){
            params = {seatbackBrightness:seatbackBrightness}; core.debug("SharedModelClientInterface.qml | seatbackBrightness change, seatbackBrightness : " + params.seatbackBrightness);

        } else if(api=="backlightEnabled"){
            params = {backlightEnabled:serverBacklightEnabled}; core.debug("SharedModelClientInterface.qml |  backlightEnabled change,  : " + params.backlightEnabled);
	    
        } else if(api=="playAodAck"){
            params = {mid:params.mid,aggregateMid:params.aggregateMid,mediaRepeat:params.mediaRepeat,mediaType:params.mediaType};
            core.debug("Received message from seat | apiName: "+ api + " | mid : "+ params.mid + ", aggregateMid : " + params.aggregateMid + ", mediaRepeat : " + params.mediaRepeat+', mediaType : '+params.mediaType);
        } else if(api=="mediaRepeatAck"){
            params = {mediaRepeat:params.mediaPlayer1Repeat}; core.debug("Received message from seat | apiName: "+ api + " | mediaRepeat : "+ params.mediaRepeat);

        } else if(api=="mediaShuffleAck"){
            params = {mediaShuffle:mediaPlayer1Shuffle}; core.debug("Received message from seat | apiName: "+ api + " | mediaShuffle : "+ params.mediaShuffle);

        } else if(api=="changeLanguage"){
            params = {languageIso:languageIso,languageLid:languageLid}; core.debug("Received message from seat | apiName: "+ api + " | languageIso : "+ params.languageIso+ " | languageLid : "+ params.languageLid);

        } else if(api=="launchAppAck"){
           params = {launchAppId:launchAppId}; core.debug("Received message from seat | apiName: "+ api + " | launchAppId : "+ params.launchAppId);

        } else if(api=="iPodConnected"){
            params = {iPodConnected:iPodConnected}; core.debug("Received message from seat | apiName: "+ api + " | ipodConnected : "+ params.iPodConnected)

        } else if(api=="usbConnected"){
            params = {usbConnected:usbConnected}; core.debug("Received message from seat | apiName: "+ api + " | usbConnected : "+ params.usbConnected)

        } else if(api.indexOf("userDefined")!=-1 || api.indexOf("framework")!=-1){ //playlistType type mapped to mediaType not playlistType from signal see smci
            if (api == "frameworkPlaylistPartiallyAdded"){sigPlaylistError(cPLAYLIST_FULL,apiParameters.pid,apiParameters.playlistType);return true;}
            else if (api == "frameworkPlaylistAdded"){ sigPlaylistAdded(cSUCCESS,apiParameters.pid,apiParameters.playlistType);return true;}
            else if (api == "frameworkAlbumMidsUpdated"){
                triggerAlbumBinding=false;
                for(var i in apiParameters.albumMid[apiParameters.playlistType]){
                    if(!Store.AlbumPresent[apiParameters.playlistType][i]){
                        Store.AlbumPresent[apiParameters.playlistType][i]=apiParameters.albumMid[apiParameters.playlistType][i];
                    }
                    else {
                        Store.AlbumPresent[apiParameters.playlistType][i]=apiParameters.albumMid[apiParameters.playlistType][i];
                    }
                }
                triggerAlbumBinding=true;
            }

            else if (api == "frameworkPlaylistMoved"){sigPlaylistMoved(cSUCCESS,apiParameters.pid,apiParameters.playlistType); return true;}
            else if(api == "frameworkPlaylistTrackRemoved"){sigPlaylistTrackRemoved(apiParameters.pid,apiParameters.playlistType);return true;}
            else if(api == "frameworkPlaylistAllTrackRemoved"){sigPlaylistAllTrackRemoved(apiParameters.pid,apiParameters.playlistType);return true;}
            else if(api == "frameworkPlaylistAggregateMidRemoved"){sigPlaylistAggregateMidRemoved(apiParameters.playlistType,apiParameters.pid);return true;}
            else if (api == "frameworkplayingPlaylistIndex"){
                core.debug("SMCI.qml | Received Message from Seat | api : frameworkplayingPlaylistIndex|  playlistIndex :"+apiParameters.playlistIndex+"mediaType :"+apiParameters.mediaType)
                sigPlaylistIndex(apiParameters.playlistIndex,apiParameters.mediaType)
            } else if(api == "frameworkPowerDownEvent"){
                if(pif.vod){if(pif.lastSelectedVideo.__mediaState!=pif.vod.cMEDIA_STOP)pif.lastSelectedVideo.stop()}
            } else if(api == "frameworkInteractiveOverride"){
                pif.__interactiveOverrideChangeAck(apiParameters.interactiveOverride);
            }
            if (api == 'frameworkServiceBlockChange'){ pif.__serviceAccessChangeAck (apiParameters.accessType, apiParameters.serviceMid);return true;}
            else if (api == 'frameworkMidBlockChange'){ pif.__midAccessChangeAck(apiParameters.accessType, apiParameters.mid);return true;}
            else if (api == 'frameworkSeatRatingChange'){ pif.__seatRatingChangeAck(apiParameters.seatRating);return true;}
            if (api == 'frameworkSetSeatBackStatus'){core.debug("SMCI.qml | Received Message from Seat | api : frameworkSetSeatBackStatus | Processing ");interactiveStartup.eventHandler(Store.cDelayKarmaLoading);return true;}
            if (api == 'frameworkPaymentSuccessful'){ paymentDoneFromKarma=false; pif.ppv.sigPaymentSuccessful(apiParameters.amid); return true;}

            core.debug("Received message from seat | apiName: "+ api); sigDataReceivedFromSeat(api,apiParameters); return true;

        } else if(api=="showKeyboard"){
            params = {showKeyboard:serverKeyboardVisible}; core.debug("Received message from seat | apiName: "+ api + " | "+ params.showKeyboard)

        } else if(api=="keyboardData"){
            params = {keyboardData:keyboardData}; core.debug("Received message from seat | apiName: "+ api + " | "+ params.keyboardData)

        }else if(api=="usbAgreement"){
            params = {usbAgreement:usbAgreement}; core.debug("Received message from seat | apiName: "+ api + " | "+ params.usbAgreement)

        } else if(api=="iPodAgreement"){
            params = {iPodAgreement:iPodAgreement}; core.debug("Received message from seat | apiName: "+ api + " | "+ params.iPodAgreement)

        } else if(api=="playBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="play";

        } else if(api=="stopBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="stop";

        } else if(api=="pauseBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="pause";

        } else if(api=="resumeBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="resume";

        } else if(api=="fastForwardBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="fastforward";

        } else if(api=="rewindBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="rewind";

        } else if(api=="nextBlueRayAck" || api=="previousBlueRayAck" || api=="menuBlueRayAck" || api=="upBlueRayAck" || api=="downBlueRayAck" || api=="leftBlueRayAck" || api=="rightBlueRayAck" || api=="enterBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);

        } else if(api=="setBlueRayMediumTypeAck"){
            params = {blueRayMediumType:blueRayMediumType}; core.debug("Received message from seat | apiName: "+ api);

        } else if(api=="interactiveReset"){
            core.debug("Received message from seat | apiName: "+api); pif.__interactiveResetAck();

        } else if(api=="playlists"){
            triggerBinding = false;
            Store.aodPlaylistArray = [];
            Store.vodPlaylistArray = [];
            Store.kidsAodPlaylistArray = [];
            Store.kidsVodPlaylistArray = [];
			Store.aodAlbumMid=[];
			Store.vodAlbumMid=[];
			Store.kidsAodAlbumMid=[];
			Store.kidsVodPlaylistArray=[];
            for(var aodCounter in playlists.aod){
                var aodText = aodCounter;
                var aodPlaylistId = parseInt(aodText.split("|")[0],10);
                for (var a in playlists.aod[aodCounter]){
                    Store.aodPlaylistArray[playlists.aod[aodCounter][a].mid]=1;
                    if(!Store.aodAlbumMid[aodPlaylistId])Store.aodAlbumMid[aodPlaylistId] = new Array();
                    if(!Store.aodAlbumMid[aodPlaylistId][playlists.aod[aodCounter][a].albumMid]){
                        Store.aodAlbumMid[aodPlaylistId][playlists.aod[aodCounter][a].albumMid]= new Array();
                    }
                    if(Store.aodAlbumMid[aodPlaylistId][playlists.aod[aodCounter][a].albumMid].indexOf(playlists.aod[aodCounter][a].mid)==-1)
                        Store.aodAlbumMid[aodPlaylistId][playlists.aod[aodCounter][a].albumMid][Store.aodAlbumMid[aodPlaylistId][playlists.aod[aodCounter][a].albumMid].length] = playlists.aod[aodCounter][a].mid;
                }
            }
            for(var vodCounter in playlists.vod){
                var vodText = vodCounter;
                var vodPlaylistId = parseInt(vodText.split("|")[0],10);
                for (var a in playlists.vod[vodCounter]){
                    Store.vodPlaylistArray[playlists.vod[vodCounter][a].mid]=1;
                    if(!Store.vodAlbumMid[vodPlaylistId])Store.vodAlbumMid[vodPlaylistId] = new Array();
                    if(!Store.vodAlbumMid[vodPlaylistId][playlists.vod[vodCounter][a].albumMid]){
                        Store.vodAlbumMid[vodPlaylistId][playlists.vod[vodCounter][a].albumMid]= new Array();
                    }
                    if(Store.vodAlbumMid[vodPlaylistId][playlists.vod[vodCounter][a].albumMid].indexOf(playlists.vod[vodCounter][a].mid)==-1)
                        Store.vodAlbumMid[vodPlaylistId][playlists.vod[vodCounter][a].albumMid][Store.vodAlbumMid[vodPlaylistId][playlists.vod[vodCounter][a].albumMid].length] = playlists.vod[vodCounter][a].mid;

                }
            }
            for(var kidsAodCounter in playlists.kidsaod){
                var kidsAodText = kidsAodCounter;
                var kidsAodPlaylistId = parseInt(kidsAodText.split("|")[0],10);
                for (var a in playlists.kidsaod[kidsAodCounter]){
                    Store.kidsAodPlaylistArray[playlists.kidsaod[kidsAodCounter][a].mid]=1;
                    if(!Store.kidsAodAlbumMid[kidsAodPlaylistId])Store.kidsAodAlbumMid[kidsAodPlaylistId] = new Array();
                    if(!Store.kidsAodAlbumMid[kidsAodPlaylistId][playlists.kidsaod[kidsAodCounter][a].albumMid]){
                        Store.kidsAodAlbumMid[kidsAodPlaylistId][playlists.kidsaod[kidsAodCounter][a].albumMid]= new Array();
                    }
                    if(Store.kidsAodAlbumMid[kidsAodPlaylistId][playlists.kidsaod[kidsAodCounter][a].albumMid].indexOf(playlists.kidsaod[kidsAodCounter][a].mid)==-1)
                        Store.kidsAodAlbumMid[kidsAodPlaylistId][playlists.kidsaod[kidsAodCounter][a].albumMid][Store.kidsAodAlbumMid[kidsAodPlaylistId][playlists.kidsaod[kidsAodCounter][a].albumMid].length] = playlists.kidsaod[kidsAodCounter][a].mid;
                }
            }
            for(var kidsVodCounter in playlists.kidsvod){
                var kidsVodText = kidsVodCounter;
                var kidsVodPlaylistId = parseInt(kidsVodText.split("|")[0],10);
                for (var a in playlists.kidsvod[kidsVodCounter]){
                    Store.kidsVodPlaylistArray[playlists.kidsvod[kidsVodCounter][a].mid]=1;
                    if(!Store.kidsVodAlbumMid[kidsVodPlaylistId])Store.kidsVodAlbumMid[kidsVodPlaylistId] = new Array();
                    if(!Store.kidsVodAlbumMid[kidsVodPlaylistId][playlists.kidsvod[kidsVodCounter][a].albumMid]){
                        Store.kidsVodAlbumMid[kidsVodPlaylistId][playlists.kidsvod[kidsVodCounter][a].albumMid]= new Array();
                    }
                    if(Store.kidsVodAlbumMid[kidsVodPlaylistId][playlists.kidsvod[kidsVodCounter][a].albumMid].indexOf(playlists.kidsvod[kidsVodCounter][a].mid)==-1)
                        Store.kidsVodAlbumMid[kidsVodPlaylistId][playlists.kidsvod[kidsVodCounter][a].albumMid][Store.kidsVodAlbumMid[kidsVodPlaylistId][playlists.kidsvod[kidsVodCounter][a].albumMid].length] = playlists.kidsvod[kidsVodCounter][a].mid;
                }
            }
            triggerBinding = true;
        } else if (api == "eventInfoData"){
            params = {event:eventInfoData.event,eventInfo:eventInfoData.eventInfo}; core.debug("Received message from seat | apiName: "+ api + " | event : "+ params.event+", eventInfo:"+params.eventInfo);
        } else if (api == "seatToVkarmaSeatChatRequest"){
            params = {apiName:seatToVkarmaSeatChatRequest.apiName,parameters:seatToVkarmaSeatChatRequest.params,callbackFunction:seatToVkarmaSeatChatRequest.callbackFunction,returnObj:seatToVkarmaSeatChatRequest.returnObj}
        } else if (api == "seatToVkarmaSeatChatWithParams"){
            params = {apiName:seatToVkarmaSeatChatWithParams.apiName,parameters:seatToVkarmaSeatChatWithParams.params,callbackFunction:seatToVkarmaSeatChatWithParams.callbackFunction,extraParams:seatToVkarmaSeatChatWithParams.extraParams,returnObj:seatToVkarmaSeatChatWithParams.returnObj}
        } else {
            core.debug("Received message from seat | apiName: "+api);
        }
        sigDataReceivedFromSeat(api,params); return true;
    }

    function __iterateAssocArray(params){var logger=[];for(var key in params){ logger.push(" params."+key+": "+params[key])} return logger;}

    //public functions
    function playVodByMid(params){ //apiName: playVod, aggegerate mid has to be 0
        if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        params = {mediaType:(params.mediaType && params.mediaType!="")?params.mediaType:"vod",identifier:params.mid,aggregateIdentifier:0,soundtrackLid:params.soundtrackLid,soundtrackType:params.soundtrackType,subtitleLid:params.subtitleLid,subtitleType:params.subtitleType,elapsedTime:params.elapsedTime,isTrailer:false,cid:params.cid}
        core.debug("Send message to seat | apiName: playVod |"+__iterateAssocArray(params));
        sendApiData("launchMedia",params);
    }

    function playVodAggegerate(params){ //apiName: playVodAggegerate
        if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        params = {mediaType:"vod",identifier:params.mid,aggregateIdentifier:params.amid,soundtrackLid:params.soundtrackLid,soundtrackType:params.soundtrackType,subtitleLid:params.subtitleLid,subtitleType:params.subtitleType,elapsedTime:params.elapsedTime,isTrailer:false,cid:params.cid}
        core.debug("Send message to seat | apiName: playVodAggegerate |"+__iterateAssocArray(params));
        sendApiData("launchMedia",params);
    }

    function playTrailer(params){ //apiName: playTrailer
        if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        params = {mediaType:"vod",identifier:params.mid,aggregateIdentifier:params.aggregateMid,soundtrackLid:params.soundtrackLid,soundtrackType:params.soundtrackType,subtitleLid:params.subtitleLid,subtitleType:params.subtitleType,elapsedTime:params.elapsedTime,isTrailer:true}
        core.debug("Send message to seat | apiName: playTrailer |"+__iterateAssocArray(params));
        sendApiData("launchMedia",params);
    }

    function toggleVodStretchState(){ // aspectRatioAck won't be received when request is made from vKarma to seat
         if(mediaPlayer2AspectRatio=="disable") return false;
        core.debug("Send message to seat | toggleVodStretchState | aspectRatio : " + mediaPlayer2AspectRatio);
        sendPropertyData("mediaPlayer2AspectRatio",(mediaPlayer2AspectRatio=="16x9"?"16x9Stretched":mediaPlayer2AspectRatio=="4x3"?"4x3Stretched":mediaPlayer2AspectRatio=="16x9Stretched"?"16x9":"4x3"));
    }

    function setPlayingSoundtrack(params){core.debug("Send message to seat | apiName: videoSoundtrack |"+__iterateAssocArray(params));sendApiData("videoSoundtrack",params);} //apiName: videoSoundtrack
    function setPlayingSubtitle(params){ core.debug("Send message to seat | apiName: videoSubtitle |"+__iterateAssocArray(params)); sendApiData("videoSubtitle",params);}       // apiName: videoSubtitle, View should pass subtitleLid=-1 for "None" as subtitle
    function stopVideo(){ core.debug("Send message to seat | apiName: videoStop");sendApiData("controlMedia",{value:"stop",target: "mediaPlayer2" });}

    // AOD operations
    function playAodByMid(params){
        core.debug("Send message to seat | apiName: playAod |"+__iterateAssocArray(params));
        params = {mediaType:"aod",identifier:params.mid,aggregateIdentifier:params.aggregateMid,mediaRepeat:params.mediaRepeat}
        sendApiData("launchMedia",params);
    }

    function getAggregateMidList(params){ // E.g params => {playlistId:<playlistId>,type:<aod>}
        if(params.playlistId=="" || params.playlistId==undefined || params.type=="" || params.type==undefined){
            core.debug("SMCI.qml | Returning as params list is incorrect");
            return;
        }

        var playlistId=params.playlistId;
        var type=params.type;

        var playlistArr = {};
        playlistId=Math.abs(playlistId);
        playlistArr = (type=="aod")?Store.aodAlbumMid:(type=="vod")?Store.vodAlbumMid:(type=="kidsaod")?Store.kidsAodAlbumMid:(type=="kidsvod")?Store.kidsVodAlbumMid:Store.aodAlbumMid;
        var aggregateMidArr=[];
        var midArr=[];
        var sortedMidArr=[];

        if (!playlistArr[playlistId]) return {};
        for(var x in playlistArr[playlistId]){
            if(aggregateMidArr.join(",").indexOf(x)==-1)
                aggregateMidArr.push(x);
        }
        midArr = playlistArr[playlistId];
        sortedMidArr=getAllPlaylistMids({"playlistIndex":playlistId,"mediaType":type});
        return {"aggregateMidArr":aggregateMidArr,"midArr":midArr,"sortedMidArr":sortedMidArr};
    }

    //repeatTrack "none": no  looping, "current": looping of the current track, "all": looping of all the tracks in the album
    function repeatTrack(params){  core.debug("Send message to seat | apiName: mediaRepeat |"+__iterateAssocArray(params)); sendPropertyData("mediaPlayer1Repeat",params.mediaRepeat);}
    function shuffleTrack(){ core.debug("Send message to seat | mediaShuffle : " + mediaPlayer1Shuffle); sendPropertyData("mediaPlayer1Shuffle",!mediaPlayer1Shuffle);}
    function stopAudio(){ core.debug("Send message to seat | apiName: audioStop");sendApiData("controlMedia",{value:"stop",target: "mediaPlayer1" })}

    // Common VOD & AOD operations
    function setPlayingMediaPosition(params){ core.debug("Send message to seat | apiName: setMediaPosition |"+__iterateAssocArray(params)); sendApiData("setMediaPosition",params)}
    function forwardPlayingMedia(){ core.debug("Send message to seat | apiName: mediaFastForward"); sendApiData("controlMedia",{value:"fastForward"});}
    function rewindPlayingMedia(){  core.debug("Send message to seat | apiName: mediaRewind"); sendApiData("controlMedia",{value:"rewind"});}
    function pausePlayingMedia(){ core.debug("Send message to seat | apiName: mediaPause"); sendApiData("controlMedia",{value:"pause"});}
    function resumePlayingMedia(){ core.debug("Send message to seat | apiName: mediaResume"); sendApiData("controlMedia",{value:"resume"});}
    function playNextMedia(){ core.debug("Send message to seat | apiName: mediaNext"); sendApiData("controlMedia",{value:"next"});}
    function playPreviousMedia(){ core.debug("Send message to seat | apiName: mediaPrevious"); sendApiData("controlMedia",{value:"previous"});}

    function createPlaylist(params){ //{"playlistIndex":-1,"name":"aodPlaylist1","mediaType":"aod"}
        core.debug("SMCI.qml | createPlaylist called ")
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.name || params.name==undefined){core.debug("SMCI.qml | name is not valid , Exiting"); return;}
        if (!params.mediaType || params.mediaType==undefined)params.mediaType="aod"
        var plist = playlists
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        if(Math.abs(params.playlistIndex)>((params.mediaType=="aod")?pif.getNumberOfAodPlaylist():(params.mediaType=="vod")?pif.getNumberOfVodPlaylist():(params.mediaType=="kidsaod")?pif.getNumberOfKidsAodPlaylist():(params.mediaType=="kidsvod")?pif.getNumberOfKidsVodPlaylist():pif.getNumberOfAodPlaylist())){
            core.debug("SMCI.qml | createPlaylist.qml| Maximum Number of Playlist Reached, Exiting ")
            return cMAXIMUM_PLAYLIST_REACHED;
        }
        for (var x in pointer){
            if(Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)){
                core.debug("SMCI.qml | createPlaylist.qml | Playlist already exists, Exiting")
                return cPLAYLIST_EXISTS;
            }
        }
        playlists=plist;
        core.debug("Send message to seatback | apiName: createPlaylist | playlistIndex is " + params.playlistIndex + ",name is "+params.name+", mediaType : " + params.mediaType);
        sendApiData("createPlaylist",params)
        return cSUCCESS;
    }

    function updatePlaylistName(params){
        core.debug("smci.qml | updatePlaylistName called ")
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.name || params.name==undefined){core.debug("SMCI.qml | name is not valid , Exiting"); return;}
        if (!params.mediaType || params.mediaType==undefined)params.mediaType="aod"
        var plist =playlists;
        var reference;
        var name;
        var playlistName;
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        for (var x in pointer){
            name = x.split("|")
            if(Math.abs(params.playlistIndex)==parseInt(name[0],10)){
                playlistName=name[0] +"|"+ name[1];
                reference=pointer[x];
                break;
            }
        }
        if (!reference){
            core.debug("SMCI.qml | updatePlaylistName | Playlist does not exist, cannot update name, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        if(Math.abs(params.playlistIndex)+"|"+params.name==playlistName) {
            core.info("SMCI.qml | updatePlaylistName | Same Name used, Exiting ");
            return cSAME_PLAYLIST_NAME;
        }
        playlists=plist
        core.debug("Send message to seatback | apiName: updatePlaylistName | playlistIndex is " + params.playlistIndex + ",name is "+params.name+", mediaType : " + params.mediaType);
        sendApiData("updatePlaylistName",params)
        return cSUCCESS;
    }

    //if midVal is passed as third parameter, midVal need to be taken as mid. otherwise, we need to take the mid from the model that is passed.
    function addMidToPlaylist(model,params,midVal){ //apiName: addAllMidsToPlaylist, {"index": 1,"playlistIndex":-1,"aggregateMid":102,"cid":190,"mediaType":"aod"}
        var mid;var modelCount=0;
        core.debug("smci.qml | addMidToPlaylist()");
        if(!midVal || midVal==undefined) {mid = parseInt(model.getValue(params.index,"mid"),10);modelCount=model.count}
        else {mid = midVal;modelCount=model.count}
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.cid || params.cid==undefined) params.cid=0;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if (!params.aggregateMid && (params.mediaType=="vod" || params.mediaType=="kidsvod")){
            params.aggregateMid = mid
        }
        if(!params.aggregateMid || params.aggregateMid==undefined){core.debug("SMCI.qml | aggregateMid is not valid. Exiting.");return false;}
        if(!params.rating || params.rating==undefined) params.rating=254;
        var pointer = (params.mediaType=="aod")?playlists.aod:(params.mediaType=="vod")?playlists.vod:(params.mediaType=="kidsaod")?playlists.kidsaod:(params.mediaType=="kidsvod")?playlists.kidsvod:playlists.aod;
        var arrPointer = (params.mediaType=="aod")?Store.aodPlaylistArray:(params.mediaType=="vod")?Store.vodPlaylistArray:(params.mediaType=="kidsaod")?Store.kidsAodPlaylistArray:(params.mediaType=="kidsvod")?Store.kidsVodPlaylistArray:Store.aodPlaylistArray;
        var reference;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)) reference = pointer[x];
        }
        if (!reference){
            core.info("SMCI.qml | addMidToPlaylist | Playlist does not exist, Cannot Add Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        if(arrPointer[mid]){
            core.info("SMCI.qml | addMidToPlaylist() | Mid already in Playlist, Exiting ")
            return cMID_ALREADY_IN_PLAYLIST;
        }
        __addMidToPlaylist({"mids":[mid],"playlistIndex":params.playlistIndex,"aggregateMid":params.aggregateMid,"cid":params.cid,"mediaType":params.mediaType,"rating":params.rating,"modelCount":modelCount});
        return cSUCCESS;
    }

    function addAllMidsToPlaylist(model,params){ //apiName: addAllMidsToPlaylist, {"playlistIndex":-1,"aggregateMid":102,"cid":190,"mediaType":"aod","rating":150}
        core.debug("smci.qml | addAllMidsToPlaylist()");
        var midList = [];
        for(var i=0; i<model.count;i++){
            midList.push(parseInt(model.getValue(i,"mid"),10))
        }
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.cid || params.cid==undefined) params.cid=0;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if (!params.aggregateMid && (params.mediaType=="vod" || params.mediaType=="kidsvod")){
            params.aggregateMid = midList[0];
        }
        if(!params.aggregateMid || params.aggregateMid==undefined){core.debug("SMCI.qml | aggregateMid is not valid. Exiting.");return false;}
        if(!params.rating || params.rating==undefined) params.rating=254;
        var pointer = (params.mediaType=="aod")?playlists.aod:(params.mediaType=="vod")?playlists.vod:(params.mediaType=="kidsaod")?playlists.kidsaod:(params.mediaType=="kidsvod")?playlists.kidsvod:playlists.aod;
        var reference;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)) reference = pointer[x];
        }
        if (!reference){
            core.info("SMCI.qml | addMidToPlaylist | Playlist does not exist, Cannot Add Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        __addMidToPlaylist({"mids":midList,"playlistIndex":params.playlistIndex,"aggregateMid":params.aggregateMid,"cid":params.cid,"mediaType":params.mediaType,"rating":params.rating,"modelCount":model.count})
        return cSUCCESS;
    }

    function __addMidToPlaylist(params){
        core.info("Send message to seatback | apiName: addMidToPlaylist | playlistIndex is " + params.playlistIndex + ", mids is : " + params.mids + ", aggregateMid : " + params.aggregateMid + ", cid : " + params.cid + ", mediaType : " + params.mediaType + ", rating :"+params.rating);
        sendApiData("addMidToPlaylist",params);
    }

    function removeMidFromPlaylist(params){//apiName:removeMidFromPlayist,{"mids":[209]"mediatype":"aod"}
        core.debug("smci.qml | removeMidFromPlayist()");
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        core.debug("Send message to seat | apiName: removeMidFromPlayist | mids is : " + params.mids+", mediaType: "+params.mediaType);
        sendApiData("removeMidFromPlayist",params);
        return cSUCCESS;
    }

    function removeAggregateMid(params){//apiName:removeAggregateMid,{"amid":209 "mediatype":"aod"}
        core.debug("smci.qml | removeAggregateMid()");
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if(params.mediaType=="mp3") core.debug("smci.qml | removeAggregateMid() | Not applicable for UsbPlaylist");
        core.debug("Send message to seat | apiName: removeAggregateMid | amid is : " + params.amid+", mediaType: "+params.mediaType);
        sendApiData("removeAggregateMid",params);
        return cSUCCESS;
    }

    function removeAllFromPlaylist(params){//apiName:removeAllFromPlaylist , "playlistIndex":-1,"mediaType":"aod"
        core.debug("smci.qml | removeAllFromPlaylist()");
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        var pointer = (params.mediaType=="aod")?playlists.aod:(params.mediaType=="vod")?playlists.vod:(params.mediaType=="kidsaod")?playlists.kidsaod:(params.mediaType=="kidsvod")?playlists.kidsvod:(params.mediaType=="mp3")?playlists.usb:playlists.aod;
        var reference;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)) reference = pointer[x];
        }
        if (!reference){
            core.info("SMCI.qml | addMidToPlaylist | Playlist does not exist, Cannot Remove Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        core.debug("Send message to seat | apiName: removeAllFromPlaylist | playlistIndex is " + params.playlistIndex + " , mediaType : " + params.mediaType);
        sendApiData("removeAllFromPlaylist",params);
        return cSUCCESS;
    }

    function isTrackAddedInPlaylist (params){ //{"mid":25,"mediaType":"aod"}
        var triggerBindingCheck = triggerBinding;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if(!params.mid || params.mid==undefined)return;
        var pointer = (params.mediaType=="aod")?Store.aodPlaylistArray:(params.mediaType=="vod")?Store.vodPlaylistArray:(params.mediaType=="kidsaod")?Store.kidsAodPlaylistArray:(params.mediaType=="kidsvod")?Store.kidsVodPlaylistArray:Store.aodPlaylistArray;
        if(!pointer[params.mid]) return false;
        else return true;
    }

    function isAggegerateMidAddedInPlaylist (params){ //{"amid":25,"mediaType":"aod"}
        var triggerBindingCheck = triggerBinding;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if(!params.amid || params.amid==undefined)return;
        var pointer = (params.mediaType=="aod")?Store.aodAlbumMid:(params.mediaType=="vod")?Store.vodAlbumMid:(params.mediaType=="kidsaod")?Store.kidsAodAlbumMid:(params.mediaType=="kidsvod")?Store.kidsVodAlbumMid:Store.aodAlbumMid;
        for(var pid in pointer){if(pointer[pid][params.amid]); return true;}
        return false;
    }

    function isAlbumInPlaylist(params){//{"amid":25,"mediaType":"aod"}
        var triggerBindingCheck = triggerAlbumBinding;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if(!params.amid || params.amid==undefined)return;
        var ret = (Store.AlbumPresent[params.mediaType][params.amid])?Store.AlbumPresent[params.mediaType][params.amid]:false;
        triggerAlbumBinding=false;
        return ret;
    }

    function isAllTracksAddedInPlaylist(model,params){//{"playlistIndex":-1,"mediaType":"aod"}
        var triggerBindingCheck = triggerBinding;
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        var mid;
        if(!model) return false;
        var ref = (params.mediaType=="aod")?Store.aodPlaylistArray:(params.mediaType=="vod")?Store.vodPlaylistArray:(params.mediaType=="kidsaod")?Store.kidsAodPlaylistArray:(params.mediaType=="kidsvod")?Store.kidsVodPlaylistArray:Store.aodPlaylistArray;
        for (var i=0; i<model.count;i++){
            mid = parseInt(model.getValue(i,"mid"),10)
            if(!ref[mid]) return false
            else continue;
        }
        return true;
    }

    function getAllPlaylistMids(params){ //{"playListIndex":-1,"mediaType":"aod"}
        core.info("SMCI.qml |getAllPlaylistMids | playListIndex :"+params.playlistIndex+" mediaType :"+params.mediaType)
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        var plist = playlists
        var midList = [];
        var reference;
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)){
                reference = pointer[x];
            }
        }
        if(!reference){
            core.info("SMCI.qml | getAllPlaylistMids | Playlist does not exist, Cannot Remove Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        for (var counter in reference){
            midList.push(reference[counter].mid)
        }
        playlists = plist
        core.info("SMCI.qml | getAllPlaylistMids| Midlist  :"+midList)
        return midList;
    }

    function getUsbPlaylistModel(params){ //{"playListIndex":-1,"mediaType":"mp3"}
        core.info("SMCI.qml | getUsbPlaylistModel | playListIndex :"+params.playlistIndex+" mediaType :"+params.mediaType)
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="mp3";
        var plist = playlists
        var midList = [];
        var reference;
        var pointer = plist.usb;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)){
                reference = pointer[x];
            }
        }
        if(!reference){
            core.info("SMCI.qml | getUsbPlaylistModel | Playlist does not exist, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }

        if(!Store.usbPlaylistModel){Store.usbPlaylistModel = Qt.createQmlObject('import QtQuick 1.1;import Panasonic.Pif 1.0;JsonModel{}',sharedModelClientInterface);core.info("SMCI.qml | getAllPlaylistMids| Creating usbPlaylistModel :"+Store.usbPlaylistModel)}
        Store.usbPlaylistModel.source=reference;
        playlists = plist
        core.info("SMCI.qml | getAllPlaylistMids| usbPlaylistModel count  :"+Store.usbPlaylistModel.count)
        return Store.usbPlaylistModel;
    }


    function getAllPlaylistAggregateMids(params){ //{"playlistIndex":-1,"mediaType":"aod"}
        core.info("SMCI.qml | getAllPlaylistAggregateMids | playListIndex :"+params.playlistIndex+" mediaType :"+params.mediaType)
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        var plist = playlists
        var aggregateMidList = [];
        var uniqueAggreagteMidList = [];
        var reference;
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)){
                reference = pointer[x];
            }
        }
        if(!reference){
            core.info("SMCI.qml | getAllPlaylistAggregateMids | Playlist does not exist, Cannot Remove Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        for (var counter in reference){
            aggregateMidList.push(reference[counter].albumMid)
        }
        for(var i=0;i<aggregateMidList.length;i++){
            if(uniqueAggreagteMidList.indexOf(aggregateMidList[i])==-1)uniqueAggreagteMidList.push(aggregateMidList[i])
        }
        core.debug("SMCI.qml | getAllPlaylistAggregateMids | Aggregate MidList :"+uniqueAggreagteMidList)
        return uniqueAggreagteMidList;
    }

    function getPlaylistAggregateMid(params){ //{"mid":mid,"playlistIndex":-1,"mediaType":"aod"}
        core.info("SMCI.qml | getPlaylistAggregateMid | mid: "+params.mid+" | playListIndex :"+params.playlistIndex+" mediaType :"+params.mediaType);
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        var reference = (params.mediaType=="aod")?Store.aodAlbumMid:(params.mediaType=="vod")?Store.vodAlbumMid:(params.mediaType=="kidsaod")?Store.kidsAodAlbumMid:(params.mediaType=="kidsvod")?Store.kidsVodAlbumMid:Store.aodAlbumMid;
        if(!reference[Math.abs(params.playlistIndex)]){core.info("SMCI.qml | getPlaylistAggregateMid | Playlist does not exist")}
        var pointer=reference[Math.abs(params.playlistIndex)];
        for(var amid in pointer){
            for(var counter in pointer[amid]){
                if(pointer[amid][counter] == params.mid){
                    return amid;
                }
            }
        }
        return 0;
    }
	
    function getMidsInPlaylistByAggMid(params){//{"playlistIndex":-1,"mediaType":"aod","aggregateMid":102}
        core.info("SMCI.qml | getMidsInPlaylistByAggMid | playListIndex :"+params.playlistIndex+" mediaType :"+params.mediaType + " aggregateMid : " + params.aggregateMid);
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if(!params.aggregateMid || params.aggregateMid==undefined){core.debug("SMCI.qml | aggregateMid is not valid. Exiting.");return false;}
        for(var b in Store.aodAlbumMid){
            for(var c in Store.aodAlbumMid[b]){
                for(var d=Store.aodAlbumMid[b][c].length-1; d>=0; d--){
                    if(!Store.aodPlaylistArray[Store.aodAlbumMid[b][c][d]])Store.aodAlbumMid[b][c].splice(d,1);
                }
            }
        }
        for(var b in Store.vodAlbumMid){
            for(var c in Store.vodAlbumMid[b]){
                for(var d=Store.vodAlbumMid[b][c].length-1; d>=0; d--){
                    if(!Store.vodPlaylistArray[Store.vodAlbumMid[b][c][d]])Store.vodAlbumMid[b][c].splice(d,1);
                }
            }
        }
        for(var b in Store.kidsAodAlbumMid){
            for(var c in Store.kidsAodAlbumMid[b]){
                for(var d=Store.kidsAodAlbumMid[b][c].length-1; d>=0; d--){
                    if(!Store.kidsAodPlaylistArray[Store.kidsAodAlbumMid[b][c][d]])Store.kidsAodAlbumMid[b][c].splice(d,1);
                }
            }
        }
        for(var b in Store.kidsVodAlbumMid){
            for(var c in Store.kidsVodAlbumMid[b]){
                for(var d=Store.kidsVodAlbumMid[b][c].length-1; d>=0; d--){
                    if(!Store.kidsVodPlaylistArray[Store.kidsVodAlbumMid[b][c][d]])Store.kidsVodAlbumMid[b][c].splice(d,1);
                }
            }
        }
        if(params.mediaType=="aod"){
            if(Store.aodAlbumMid[Math.abs(params.playlistIndex)][params.aggregateMid])
                return Store.aodAlbumMid[Math.abs(params.playlistIndex)][params.aggregateMid];
            else
                return null;
        }
        else if(params.mediaType=="vod"){
            if(Store.vodAlbumMid[Math.abs(params.playlistIndex)][params.aggregateMid])
                return Store.vodAlbumMid[Math.abs(params.playlistIndex)][params.aggregateMid];
            else
                return null;
        }
        else if(params.mediaType=="kidsaod"){
            if(Store.kidsAodAlbumMid[Math.abs(params.playlistIndex)][params.aggregateMid])
                return Store.kidsAodAlbumMid[Math.abs(params.playlistIndex)][params.aggregateMid];
            else
                return null;
        }
        else if(params.mediaType=="kidsvod"){
            if(Store.kidsVodAlbumMid[Math.abs(params.playlistIndex)][params.aggregateMid])
                return Store.kidsVodAlbumMid[Math.abs(params.playlistIndex)][params.aggregateMid];
            else
                return null;
        }
    }
    function getPlaylistName(params){//{"playlistIndex":-1;"mediaType":"aod"}
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        var plist = playlists;
        var reference;
        var name
        var playlistName;
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:(params.mediaType=="mp3")?playlists.usb:playlists.aod;
        for (var x in pointer){
            name = x.split("|")
            if (Math.abs(params.playlistIndex)==parseInt(name[0],10)){
                playlistName = name[1]
                reference=pointer[x]
                break;
            }
        }
        if(!reference){
            core.info("SMCI.qml | playlistName | Playlist does not exist, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        playlists = plist;
        core.info("SMCI.qml | getPlaylistName | PlaylistName :"+playlistName)
        return playlistName;
    }

    function playJukebox(params){//{"playlistIndex":-1,"mids":209,"mediaType":"aod"}
        core.debug("smci.qml | playJukebox() ");
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mid || params.mid==undefined){core.debug("SMCI.qml | Mid is not valid. Exiting.");return false;}
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        core.debug("Send message to seat | apiName: playJukebox | playlistIndex is " + params.playlistIndex + ", mid is : " + params.mid + ", mediaType : " + params.mediaType);
        sendApiData("playJukebox",params);
        return cSUCCESS;
    }

    function getPlaylistCount(params){//{"playlistIndex":-1,"mediaType":"aod"}
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        var reference;
        var name;
        var playlistCount;
        var pointer = (params.mediaType=="aod")?playlists.aod:(params.mediaType=="vod")?playlists.vod:(params.mediaType=="kidsaod")?playlists.kidsaod:(params.mediaType=="kidsvod")?playlists.kidsvod:(params.mediaType=="mp3")?playlists.usb:playlists.aod;
        for (var x in pointer){
            name = x.split("|")
            if (Math.abs(params.playlistIndex)==parseInt(name[0],10)){
                var playlistName = parseInt(name[0],10)+"|"+name[1]
                playlistCount = pointer[playlistName].length
                reference = pointer[x];
            }
        }
        if(!reference){
            core.info("SMCI.qml | playlistName | Playlist does not exist, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        core.info("SMCI.qml | getPlaylistCount | playlistCount :"+playlistCount)
        return playlistCount;
    }

    // Volume, Brightness settings
    function setSeatbackVolume(volumeLevel){
        core.debug("Send message to seat | setSeatbackVolume | volumeLevel: " + volumeLevel);
        if(muteEnabled)sendPropertyData("muteEnabled",!muteEnabled);
        sendPropertyData("seatbackVolume",volumeLevel);
        updateApiDelayTimmer("seatbackVolume",{seatbackVolume:volumeLevel});
    }
    function setSeatbackVolumeByStep(step) {    // step: -1, -2... or 1, 2...
        core.debug("Send message to seat | setSeatbackVolumeByStep | volume Step: " + step);
        step=(String(parseInt(step,10))!="NaN")?parseInt(step,10):0;
        var volume=seatbackVolume+pif.getVolumeStep()*step;
        if(muteEnabled)sendPropertyData("muteEnabled",!muteEnabled);
        sendPropertyData("seatbackVolume",(volume>100?100:volume<0?0:volume));
        updateApiDelayTimmer("seatbackVolume",{seatbackVolume:(volume>100?100:volume<0?0:volume)});
    }
    function setHeadsetVolume(volumeLevel){
        core.debug("Send message to seat | setHeadsetVolume | volumeLevel: " + volumeLevel);
        sendPropertyData("headsetVolume",volumeLevel);
        updateApiDelayTimmer("headsetVolume",{headsetVolume:volumeLevel});
    }
    function setHeadsetVolumeByStep(step) {
        core.debug("Send message to seat | setHeadsetVolumeByStep | volume Step: " + step);
        step=(String(parseInt(step,10))!="NaN")?parseInt(step,10):0;
        var volume=headsetVolume+pif.getVolumeStep()*step;
        sendPropertyData("headsetVolume",(volume>100?100:volume<0?0:volume));
        updateApiDelayTimmer("headsetVolume",{headsetVolume:(volume>100?100:volume<0?0:volume)});
    }
    function toggleMuteState(){
        core.debug("Send message to seat | toggleMuteState current muteVolume: "+muteEnabled);
        sendPropertyData("muteEnabled",!muteEnabled); return true;
    }

    function setSeatbackBrightness(brightnessLevel){ core.debug("Send message to seat | setSeatbackBrightness | brightnessLevel : " + brightnessLevel); sendPropertyData("seatbackBrightness",brightnessLevel);updateApiDelayTimmer("seatbackBrightness",{seatbackBrightness:brightnessLevel});}
    function setSeatbackBrightnessByStep(step) {    // step: -1, -2... or 1, 2...
        core.debug("Send message to seat | setSeatbackBrightnessByStep | Brightness Step: " + step);
        step=(String(parseInt(step,10))!="NaN")?parseInt(step,10):0;
        var brightness=seatbackBrightness+pif.getBrightnessStep()*step;
        sendPropertyData("seatbackBrightness",(brightness>100?100:brightness<0?0:brightness));
        updateApiDelayTimmer("seatbackBrightness",{seatbackBrightness:(brightness>100?100:brightness<0?0:brightness)});
    }

    function playBlueRay(){core.debug("Send message to seat | apiName: playBlueRay ");sendApiData("playBlueRay","");}
    function stopBlueRay(){ core.debug("Send message to seat | apiName: stopBlueRay"); sendApiData("stopBlueRay","");}
    function pauseBlueRay(){ core.debug("Send message to seat | apiName: pauseBlueRay"); sendApiData("pauseBlueRay","");}
    function resumeBlueRay(){ core.debug("Send message to seat | apiName: resumeBlueRay"); sendApiData("resumeBlueRay","");}
    function forwardBlueRay(){ core.debug("Send message to seat | apiName: fastForwardBlueRay"); sendApiData("fastForwardBlueRay","");}
    function rewindBlueRay(){  core.debug("Send message to seat | apiName: rewindBlueRay"); sendApiData("rewindBlueRay","");}
    function playNextBlueRay(){ core.debug("Send message to seat | apiName: nextBlueRay"); sendApiData("nextBlueRay","");}
    function playPreviousBlueRay(){ core.debug("Send message to seat | apiName: previousBlueRay"); sendApiData("previousBlueRay","");}
    function navigateInBlueRay(action){
        switch(action){
        case "menuTitle":{core.debug("Send message to seat | apiName: menuTitleBlueRay ");sendApiData("menuTitleBlueRay","");break;}
        case "menuDisc":{core.debug("Send message to seat | apiName: menuDiscBlueRay ");sendApiData("menuDiscBlueRay","");break;}
        case "up":{core.debug("Send message to seat | apiName: upBlueRay ");sendApiData("upBlueRay","");break;}
        case "down":{core.debug("Send message to seat | apiName: downBlueRay ");sendApiData("downBlueRay","");break;}
        case "left":{core.debug("Send message to seat | apiName: leftBlueRay ");sendApiData("leftBlueRay","");break;}
        case "right":{core.debug("Send message to seat | apiName: rightBlueRay ");sendApiData("rightBlueRay","");break;}
        case "enter":{core.debug("Send message to seat | apiName: enterBlueRay ");sendApiData("enterBlueRay","");break;}
        default:{core.debug("Blue ray player. Invalid navigation request.");break;}
        }
    }
    function setBlueRayMediumType(blueRayMediumType){
        core.debug("Send message to seat | apiName: setBlueRayMediumType | mediumType : " + blueRayMediumType);
        if(blueRayMediumType=="BD"||blueRayMediumType=="DVD"||blueRayMediumType=="CD"||mediumType=="NONE")sendPropertyData("setBlueRayMediumType",blueRayMediumType);
        else{core.error("Invalid mediumType.");}
    }

    function toggleBacklight(){ core.debug("Send message to seat | toggleBacklight : " + serverBacklightEnabled); sendPropertyData("serverBacklightEnabled",!serverBacklightEnabled); }
    function languageChange(params){ core.debug("Send message to seat  | langugeChange |"+__iterateAssocArray(params)); //sendApiData("changeLanguage",params);
            sendPropertyData("languageLid",dataController.getLidByLanguageISO(String(params.languageIso)));
    }
    function launchApp(params){ core.debug("Send message to seat | launchApp | launchAppId : " + params.launchAppId);
        //sendApiData("launchApp",params);
        params = {mediaType:"game",identifier:params.launchAppId}
        sendApiData("launchMedia",params);
    }
    function closeApp(){ core.debug("Send message to seat  | closeApp called"); sendApiData("closeApp",{});}
    function sendKeyboardDataToSeat(val){sendPropertyData("keyboardData",val);}
    function showVkb(){sendPropertyData("serverKeyboardVisible",true);}
    function hideVkb(){sendPropertyData("serverKeyboardVisible",false);}
    function launchHospitality(){sendApiData("launchHospitality","");}
    function launchShopping(){sendApiData("launchShopping","");}
    function launchSurvey(){sendApiData("launchSurvey","");}
    function launchSeatChat(){sendApiData("launchSeatChat","");}
    function launchChatroom(){sendApiData("launchChatroom","");}
    function launchLTN(){sendApiData("launchLTN","");}
    function launchConnectingGate(){sendApiData("launchConnectingGate","");}
    function launchArrivalInfo(){sendApiData("launchArrivalInfo","");}
    function launchiXplor(){sendApiData("launchiXplor","");}
    function launchUsb(){sendApiData("launchUsb","");}
    function launchIpod(val){sendApiData("launchIpod",{launchAppId:val});}
    function launchCategory(val){sendApiData("launchCategory",{launchAppId:val});}

    function setUsbAgreement(state){sendPropertyData("usbAgreement",state);}
    function setIpodAgreement(state){sendPropertyData("iPodAgreement",state);}

    function sendUserDefinedApi(userDefinedApi,apiParams){ if(userDefinedApi.indexOf("userDefined")!=-1) sendApiData(userDefinedApi,(apiParams)?apiParams:''); else core.debug("SharedModelClientInterface.qml | userDefinedApi should be sufficed with word userDefined");}
    function sendFrameworkApi(frameworkApi,apiParams){ if(frameworkApi.indexOf("framework")!=-1) sendApiData(frameworkApi,(apiParams)?apiParams:''); else core.debug("SharedModelClientInterface.qml | frameworkApi should be sufficed with word frameworkApi");}

    function getLanguageIso(){return languageIso;}
    function getSeatbackBrightness(){return seatbackBrightness;}
    function getSeatbackVolume() {return seatbackVolume;}
    function getSeatbackPaVolume() {return seatbackPaVolume;}
    function getHeadsetVolume() {return headsetVolume;}
    function getBacklightStatus() {return serverBacklightEnabled;}
    function getPlayingMidInfo() {return (mediaPlayer1PlayStatus!="stop" && mediaPlayer1PlayStatus!="")?{mid:mediaPlayer1MediaIdentifier,aggregateMid:mediaPlayer1MediaAggregateIdentifier,midType:mediaPlayer1MediaType,mediaStatus:mediaPlayer1PlayStatus,shuffle:mediaPlayer1Shuffle,repeat:mediaPlayer1Repeat}:{mid:mediaPlayer2MediaIdentifier,aggregateMid:mediaPlayer2MediaAggregateIdentifier,subtitleLid:mediaPlayer2SubtitleLid,subtitleType:mediaPlayer2SubtitleType,soundtrackLid:mediaPlayer2SoundtrackLid,aspectratio:mediaPlayer2AspectRatio,midType:mediaPlayer2MediaType,mediaStatus:mediaPlayer2PlayStatus,shuffle:mediaPlayer2Shuffle,repeat:mediaPlayer2Repeat};}
    function getPlayingAlbumId () {return (mediaPlayer1PlayStatus!="stop" && mediaPlayer1PlayStatus!="")?mediaPlayer1MediaAlbumId:mediaPlayer2MediaAlbumId;}
    function getSoundtrackInfo() {return {soundtrackLid:mediaPlayer2SoundtrackLid,soundtrackType:2};}
    function getSubtitleInfo() {return {subtitleLid:mediaPlayer2SubtitleLid,subtitleType:mediaPlayer2SubtitleType};}
    function getElapsedTime() {return (mediaPlayer1PlayStatus!="stop" && mediaPlayer1PlayStatus!="")?mediaPlayer1ElapsedTime:mediaPlayer2ElapsedTime;}
    function getRemainingTime(duration) {return (!duration)?"":(duration-getElapsedTime());}
    function getAspectRatio() {return mediaPlayer2AspectRatio;}
    function getMediaType() {return (mediaPlayer1PlayStatus!="stop" && mediaPlayer1PlayStatus!="")?mediaPlayer1MediaType:mediaPlayer2MediaType;}
    function getMediaPlayRate() {return Math.abs(mediaPlayer2PlayRate);}
    function getCurrentMediaPlayerState() {return (mediaPlayer1PlayStatus!="stop" && mediaPlayer1PlayStatus!="")?mediaPlayer1PlayStatus:mediaPlayer2PlayStatus;}
    function getMediaRepeatValue() {return mediaPlayer1Repeat;}
    function getMediaShuffle() {return mediaPlayer1Shuffle;}
    function getUsbConnectedStatus() {return usbConnected;}
    function getIpodConnectedStatus() {return iPodConnected;}
    function getCurrentLaunchAppId() {return launchAppId;}
    function getKeyboardDisplayStatus() {return serverKeyboardVisible;}
    function getKeyboardData() {return keyboardData;}
    function getUsbAgreement() {return usbAgreement;}
    function getIpodAgreement() {return iPodAgreement;}
    function getPlaylistInfo() {return playlists;}
    function getMuteState(){return muteEnabled;}
    function getAudioPlayerdata(){sendFrameworkApi("frameworkGetAudioPlayerdata",{"mediaType":mediaType});}
    function getMidElapsedTime(mid){return (mid && resumeTimeMap[mid])?resumeTimeMap[mid]:0;}
    function getMidInfo(mid,key){return (midInforArr[mid])?(key=="subtitleLid"&& !midInforArr[mid]["subtitleEnabled"])?-1:midInforArr[mid][key]:undefined;}
    function getPasDeviceName(){return Store.pasDeviceName;}
    function setPasDeviceName(value){Store.pasDeviceName=value;}
    function playPas(deviceName){sendFrameworkApi("frameworkPasStart",{"deviceName":deviceName})}
    function stopPas(){sendFrameworkApi("frameworkPasStop")}
    function getPlayingPlaylistIndex(params){sendFrameworkApi("frameworkGetPlayingPlaylistIndex",{"mediaType":params.mediaType})}
    function delayKarmaLoading(){
        core.log("SMCI.qml | delayKarmaLoading | Called getDelayKarmaLoading: "+viewController.getDelayKarmaLoading());
        sendFrameworkApi("frameworkGetSeatBackStatus",{});
    }
    function updateApiDelayTimmer(api,params){apiDelayTimmer.api=api;apiDelayTimmer.params=params;apiDelayTimmer.restart();}
	function getNoOfDimmableWindows(){return noOfDimmableWindows;}
    /*
        sendCharToSeatback(keyCode, mode)
        This is used for sending keys from video karma to seatback.
        Params:
        keyCode : key code of the character.
        mode: mode is 0 for all characters and 42 for getting the upper case letters of alphabets.
    */
    function sendCharToSeatback(keyCode, mode){
        core.info ("SMCI.qml | sendCharToSeatback CALLED keyCode: " + keyCode + " mode: " + mode);
        if(!karmaRemote) __loadkarmaRemoteKeyHandlerComp();
        if(keyCode != 0 && karmaRemote) {karmaRemote.sendKey(keyCode, mode);}
        else {core.info ("SMCI.qml | Error Keycode is " + keyCode);}
    }
    Timer{id:apiDelayTimmer;property string api:"";property variant params;repeat:false;running:false;interval:100;onTriggered: {core.info("SMCI.qml | delayTimmer | api: "+api+" | params: "+params);sigDataReceivedFromSeat(api,params);}}
    Component{id: karmaRemoteComp; KarmaRemoteKeyHandler{id: karmaRemoteKeyHandler;}}
    Component.onCompleted: {
        Store.aodPlaylistArray = [];
        Store.vodPlaylistArray = [];
        Store.kidsAodPlaylistArray = [];
        Store.kidsVodPlaylistArray = [];
        Store.aodAlbumMid = [];
        Store.vodAlbumMid = [];
        Store.kidsAodAlbumMid = [];
        Store.kidsVodAlbumMid = [];
        Store.usbPlaylistArray = [];
        Store.usbPlaylistModel = "";
        Store.pasDeviceName="";
        Store.cDelayKarmaLoading=5001;
        Store.AlbumPresent=[];
        Store.AlbumPresent["aod"]=[];
        Store.AlbumPresent["vod"]=[];
        Store.AlbumPresent["kidsaod"]=[];
        Store.AlbumPresent["kidsvod"]=[]
        Store.vodMediaResume=false;
        core.debug("SharedModelClientInterface.qml | Shared Model Client Interface component load complete");
        if(viewController.getDelayKarmaLoading()){interactiveStartup.registerToStage(interactiveStartup.cStageShowScreen,[delayKarmaLoading],[Store.cDelayKarmaLoading]);}
    }
}
