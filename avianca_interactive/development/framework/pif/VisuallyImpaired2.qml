import QtQuick 1.1
import Panasonic.Pif 1.0

Item{
    id:visuallyImpaired

    property int __mediaState:cVI_STOP;
    property int cVI_STOP: 0;
    property int cVI_PLAYING: 1;
    property string currentFile:"";
    property bool repeatCue:false;
    property string viMediaType:"audiofile_local"
    property variant __mediaErrCode: ['NoPlayerError','InvalidPosition','InvalidItem','PlayFailed','StopFailed','InvalidPlayRate','InvalidLanguage','InvalidSubtitle','InvalidCommand','OtherPlayerError'];
    property variant viMediaModel:simpleModel;
    property variant deviceMngr;

    signal sigPlayError();
    signal sigLocalFilePlayBegan();
    signal sigLocalFileEos();
    signal sigLocalFilePlayStopped();

    function getMediaState(){return __mediaState;}

    function playLocalAudioFile(filepath,repeat){
        core.info("VisuallyImpaired.qml | playLocalAudioFile | repeat:"+repeat+", filepath:"+filepath);
        if(filepath==undefined || filepath==""){core.info("VisuallyImpaired.qml | playLocalAudioFile | filepath not provided , Exiting"); return false}
        currentFile=filepath;
        repeatCue = (repeat!=undefined && repeat==true)?true:false;
        __setMediaModel(viMediaType,filepath);
        return __mediaPlay();
    }

    function stopLocalAudioFile(){
        core.info("VisuallyImpaired.qml | stopLocalAudioFile ");
        if(__mediaState==cVI_STOP){core.info("VisuallyImpaired.qml | stopLocalAudioFile | Stop requested when media is already stopped");return false;}
        var ret = viMediaPlayer.stop();
        if(core.pc)simStopAckTimer.restart();
        return ret;
    }

    function frmwkInitApp(){
        currentFile="";repeatCue=false;
        stopLocalAudioFile();
    }

    function frmwkResetApp(){
        currentFile="";repeatCue=false;
        stopLocalAudioFile();
    }

    function __setMediaModel(mediaType,localfilePath){
        console.log("Setting __setMediaModelByCue");
        viMediaPlaylist.playlistUpdateEnabled = false;
        viMediaModel.clear();
        viMediaModel.append({"media_type":mediaType,"filePath":localfilePath});
        core.debug("VisuallyImpaired.qml  | __setMediaModel | media_type: "+mediaType+", remotefilePath:"+localfilePath);
        viMediaPlaylist.playlistUpdateEnabled = true;
        viMediaPlaylist.model=viMediaModel;
        return true;
    }

    function __mediaPlay () {
        var ret;
        core.info("VisuallyImpaired.qml  | PLAY Called");
        if(repeatCue)viMediaPlayer.repeatItem=true;
        else viMediaPlayer.repeatItem=false
        ret = viMediaPlayer.playTrack(0,0);
        core.info ("VisuallyImpaired.qml |  Return status: "+ret);
        if(core.pc){simPlayAckTimer.restart();if(simEosAckTimer2.running)simEosAckTimer.restart(); else simEosAckTimer2.restart();}
     //   fakeEosAckTimer.restart();// Note: this a fake timer of 15sec, just to simulate EOS as it is not received;
        return ret;
    }

    function __mediaPlayAck(){
        __mediaState=cVI_PLAYING;
        sigLocalFilePlayBegan();
    }
    function __mediaStopAck(){
        __mediaState=cVI_STOP;
        sigLocalFilePlayStopped();
    }
    function __mediaError (errcode) {
        core.info ("VisuallyImpaired.qml | __mediaError | Error: "+__mediaErrCode[errcode]+" Error Code: "+errcode);
        if (errcode == MediaPlayer.PlayFailed)  __mediaStopAck();
        else if (errcode == MediaPlayer.StopFailed) __mediaStopAck();
        else {core.info ("VisuallyImpaired.qml | __mediaError | UnHandled Error State");sigPlayError()}
    }

    SimpleModel { id: simpleModel;}
    MediaPlaylist { id: viMediaPlaylist; }
    MediaPlayer { id: viMediaPlayer; deviceManager: deviceMngr; playlist:viMediaPlaylist; position: 0; playRate: 100; currentPlaylistIndex: 0; mediaPlayerSettingsPersistent: true;
        onPositionChanged: {    // position
            core.info (" VisuallyImpaired.qml | Media PLAYER ON-POSITION-CHANGED SIGNAL | ELAPSE TIME: "+viMediaPlayer.position);
        }

        onPlayerModeChanged: {  // play & stop
            core.info ("VisuallyImpaired.qml | Media PLAYER ON-PLAYER-MODE-CHANGED SIGNAL | PLAYER-MODE: "+viMediaPlayer.playerMode+" PLAY-RATE-STATUS: "+viMediaPlayer.playRateStatus+" PLAY RATE: "+viMediaPlayer.playRate+" PLAYLIST INDEX: "+viMediaPlayer.currentPlaylistIndex+" MODEL INDEX: "+viMediaPlayer.currentModelIndex);
            if (pif.getEntOn()==0 && !pif.getFallbackMode()) return false;
            if (viMediaPlayer.playerMode==MediaPlayer.ActiveMode)__mediaPlayAck()
            else if (viMediaPlayer.playerMode==MediaPlayer.InactiveMode)__mediaStopAck();
            else core.debug("VisuallyImpaired.qml | Media PLAYER: ON-PLAYER-MODE-CHANGED CONDITION NOT HANDLED.");
        }

        onPlayRateChanged: {    // resume, pause, forward & rewind
            core.info ("VisuallyImpaired.qml | Media PLAYER ON-PLAY-RATE-CHANGED SIGNAL: | PLAYER-MODE: "+viMediaPlayer.playerMode+" PLAY-RATE-STATUS: "+viMediaPlayer.playRateStatus+" PLAY RATE: "+viMediaPlayer.playRate+" PLAYLIST INDEX: "+viMediaPlayer.currentPlaylistIndex+" ELAPSE TIME: "+viMediaPlayer.position);
        }

        onMediaPlayerEOS: {core.info ("VisuallyImpaired.qml | Media PLAYER ON-MEDIA-PLAYER-EOS CHANGED SIGNAL | PLAYER-MODE: "+viMediaPlayer.playerMode+" ELAPSE TIME: "+viMediaPlayer.position);sigLocalFileEos();}

        onMediaPlayerError: {core.info ("VisuallyImpaired.qml | Media PLAYER ON-PLAYER-ERROR-CHANGED SIGNAL"); __mediaError(viMediaPlayer.playerError);}

    }

    Timer {id:simPlayAckTimer; interval:50; onTriggered: __mediaPlayAck();}
    Timer {id:simStopAckTimer; interval:50; onTriggered: {if(simEosAckTimer.running)simEosAckTimer.stop();if(simEosAckTimer2.running)simEosAckTimer2.stop();__mediaStopAck();}}
    Timer{id:simEosAckTimer; interval: 5000; onTriggered: sigLocalFileEos();}
    Timer{id:simEosAckTimer2; interval: 5000; onTriggered: sigLocalFileEos();}
   // Timer{id:fakeEosAckTimer;interval:15000; onTriggered: sigLocalFileEos();}
    Component.onCompleted: {
        core.info("VisuallyImpaired2.qml | Component onCompleted");
    }
}
