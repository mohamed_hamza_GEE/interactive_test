import QtQuick 1.1
import "../components"
import Panasonic.Pif 1.0

Item{
    id:blueRay
    //constants
    property int cMEDIA_PLAY: 1;
    property int cMEDIA_PAUSE : 2;
    property int cMEDIA_REWIND : 3;
    property int cMEDIA_FORWARD : 4;
    property int cMEDIA_STOP : 5;

    property bool __restart:false;

    property variant evid:pif.vip.evidClient;
    property int blueRayStatus: cMEDIA_STOP;
    property string blueRayMediumType: "NONE";  // The possible medium types are "CD", blue ray player("BD"), "DVD", "NONE"

    signal sigMidPlayBegan();
    signal sigMidPlayStopped();
    signal sigMediaPaused();
    signal sigMediaForward();
    signal sigMediaRewind();

    Connections{
        target: evid?evid:null
        onMessageReceived: {__evidMessageReceived(command, value);}
        onConnectedChanged:{if(evid.connected){__getEvidValues();}}
    }

    //Public functions
    function initialise(){core.info("BlueRay.qml | initialise() called.");}
    function play(){
        core.info("BlueRay.qml | play() called. current Blu-Ray state is " + blueRayStatus);
        if(blueRayStatus!=cMEDIA_STOP){
            __restart = true;
            core.info("BlueRay.qml | since the blu-ray is not in STOP state, it is stopped and played again.");
            stop();
        }
        else{
            core.info("BlueRay.qml | set blu-ray player to PLAY state");
            evid.send(pif.getLruData("Blue_Ray_Control"), "PLAY");
        }
    }

    function stop(){
        core.info("BlueRay.qml | stop() called. Current blu-ray state is " + blueRayStatus);
        if(blueRayStatus!=cMEDIA_STOP){
            core.info("BlueRay.qml | set blu-ray player to STOP state");
            evid.send(pif.getLruData("Blue_Ray_Control"), "STOP");
        }else{
            core.info("BlueRay.qml | blu-ray player not set to STOP state");
        }
    }
    function pause(){
        core.info("BlueRay.qml | pause() called. set blu-ray player to PAUSE state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "PAUSE");}

    function resume(){
        core.info("BlueRay.qml | resume() called. Set blu-ray player to PLAY state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "PLAY");}

    function forward(){
        core.info("BlueRay.qml | forward() called. set blu-ray player to FASTFWD state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "FASTFWD");}

    function rewind(){
        core.info("BlueRay.qml | rewind() called. set blu-ray player to FASTRWD state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "FASTRWD");}

    function playNext(){
        core.info("BlueRay.qml | playNext() called. set blu-ray player to NEXT state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "NEXT");}

    function playPrevious(){
        core.info("BlueRay.qml | playPrevious() called. set blu-ray player to PREVIOUS state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "PREVIOUS");}

    function menuTitle(){
        core.info("BlueRay.qml | menuTitle() called. set blu-ray player to MENU_TITLE state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "MENU_TITLE");}

    function menuDisc(){
        core.info("BlueRay.qml | menuDisc() called. set blu-ray player to MENU_DISC state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "MENU_DISC");}

    function up(){
        core.info("BlueRay.qml | up() called. set blu-ray player to UP state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "UP");}

    function down(){
        core.info("BlueRay.qml | down() called. set blu-ray player to DOWN state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "DOWN");}

    function left(){
        core.info("BlueRay.qml | left() called. set blu-ray player to LEFT state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "LEFT");}

    function right(){
        core.info("BlueRay.qml | right() called. set blu-ray player to RIGHT state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "RIGHT");}

    function enter(){
        core.info("BlueRay.qml | enter() called. set blu-ray player to ENTER state");
        evid.send(pif.getLruData("Blue_Ray_Control"), "ENTER");}

    function setMediumType(blueRayMediumType){
        core.info("BlueRay.qml | setMediumType() called. blueRayMediumType: " + blueRayMediumType);
        evid.send(pif.getLruData("Blue_Ray_Medium_Type"), blueRayMediumType);}

    function getBlueRayStatus(){return blueRayStatus;}

    //Private functions
    function __evidMessageReceived(command, value){
        if(command === pif.getLruData("Blue_Ray_Control")){
            core.info("BlueRay.qml | evid Message Received command: " + command + " value " + value);
            if(value=="PLAY"){
                blueRayStatus=cMEDIA_PLAY;
                var connectionType = pif.getLruData("Blue_Ray_Connection");
                core.info("BlueRay.qml | connectionType:" + connectionType);
                if(parseInt(connectionType,10) != 1)return;
                if(pif.lastSelectedVideo != pif.ipod){
                    core.info("BlueRay.qml | stopping the lastSelectedVideo");
                    pif.lastSelectedVideo.stop();
                }
                sigMidPlayBegan();
            }
            else if(value=="STOP"){blueRayStatus=cMEDIA_STOP;if(__restart){__restart=false;play();}else sigMidPlayStopped();}
            else if(value=="PAUSE"){blueRayStatus=cMEDIA_PAUSE;sigMediaPaused();}
            else if(value=="FFWD"){blueRayStatus=cMEDIA_FORWARD;sigMediaForward();}
            else if(value=="FRWD"){blueRayStatus=cMEDIA_REWIND;sigMediaRewind();}
        }
        else if(command === pif.getLruData("Blue_Ray_Medium_Type")){
            core.info("BlueRay.qml | evid Message Received command: " + command + " value " + value);
            blueRayMediumType = value;
        }
    }

    function __getEvidValues(){
        evid.requestValue(pif.getLruData("Blue_Ray_Control"));
        evid.requestValue(pif.getLruData("Blue_Ray_Medium_Type"));
    }

    Component.onCompleted: {
        core.info("BlueRay.qml | BlueRay component load complete "+pif.vip.evidClient);
        if(pif.vip.evidClient && pif.vip.evidClient.connected){__getEvidValues();}
    }
}
