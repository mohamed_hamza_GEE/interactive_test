import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    id:imgSharing;
    property bool __isEnable: false;
    property int pendingInvitation : 0;

    signal sigConnectionRefused(string seatNo);
    signal sigSharingStatus(string status, string seatNo);
    signal sigInvitationReceived(string seatNo, string metaData, string messageIdentifier, string messageText,int index);

    function enable(){__isEnable=true; return true;}
    function disable(){
        if(!__isEnable){ core.info("MessageSharing.qml | usb image sharing is already disabled. Exiting...."); return false;}
        else {__isEnable=false;frmwkResetApp();}
        return true;
    }
    function acceptInvite(index){__setInvitationStatus("accept",index);}
    function declineInvite(index){__setInvitationStatus("decline",index);}
    function optOutInvite(index){__setInvitationStatus("optout",index);}
    function getSharingStatus(){return __isEnable;}
    function getSenderSharedModel(){return __senderSharedModel;}
    function getReceivedMessageModel(){return __receivedModel;}
    function getInvitationReceivedModel(){return __invitationReceivedModel;}

    function initialize(metaData,receiverSeatNo,messageIdentifier,messageText){
        if(receiverSeatNo=="" || !receiverSeatNo){core.info("MessageSharing.qml | initialize | receiverSeatNo Not Set ");return false;}
        if(metaData=="" || !metaData){core.info("MessageSharing.qml | initialize | metaData Not Set ");return false;}
        if(!messageText) messageText="";
        var receiverSeatNoArray=receiverSeatNo.split(",");
        for(var counter in receiverSeatNoArray){
            if(receiverSeatNoArray[counter].toLowerCase()!=pif.getSeatNumber().toLowerCase()){
            var indxOfMid=Store.senderRecipientsList.indexOf(receiverSeatNoArray[counter]);
            if(indxOfMid<0){Store.senderRecipientsList.push(receiverSeatNoArray[counter]);}
            pendingInvitation++;
            var tmpArr =metaData.split(",");
            for(var j in tmpArr){
                __senderSharedModel.append({"seatNo":receiverSeatNoArray[counter],"metaData":tmpArr[j],"messageIdentifier":messageIdentifier,"messageText":messageText,"userDefined1":"","userDefined2":""});
            }
            metaData=metaData.replace(/'/g, "\'");
            messageIdentifier=messageIdentifier.replace(/'/g, "\'");
            messageText=messageText.replace(/'/g, "\'");
            Store.senderRequestQueue.push({metaData:metaData,messageIdentifier:messageIdentifier,seatNo:receiverSeatNoArray[counter],messageText:messageText});
            }
        }
        core.debug("MessageSharing.qml | initialize | metaData: "+metaData+" | messageText: "+messageText+" | messageIdentifier: "+messageIdentifier+" | senderSeat: "+pif.getSeatNumber()+" receiverSeat: "+receiverSeatNo);
        __processSenderQueue();
    }

    function getSenderPendingInvitationCount(){return pendingInvitation;}

    function addToReceiverInvitationQueue(seatNo,action,metaData,messageIdentifier,messageText){
        Store.receiverInvitationQueue.push({"seatNo":seatNo,"action":action,"metaData":metaData,"messageIdentifier":messageIdentifier,"messageText":messageText});
    }

    function __processSenderQueue(){

        if(!Store.isClient1Active){
            if(Store.senderRequestQueue.length==0){core.debug("MessageSharing.qml | __processSenderList | No request pending"); return true;}
            Store.isClient1Active=true;
            var tmp=Store.senderRequestQueue[0];
            core.info("MessageSharing.qml | __processSenderQueue | processing: "+JSON.stringify(tmp));
            var receiver=__getHostName(tmp.seatNo);
            __setClient1ServerHostName(receiver);
        }else {
            core.debug("MessageSharing.qml | __processSenderQueue | isClient1Active: "+Store.isClient1Active+" | Current Receiver: "+client1.serverHostname);
        }
    }

    function __setInvitationStatus(action,index){
        if(!index){index=0;}
        var tmp=__invitationReceivedModel.at(index);
        core.info("MessageSharing.qml | __setInvitationStatus | action: "+action+" | index:"+index+" | seatNo: "+tmp.seatNo+" | metaData: "+tmp.metaData);
        if(tmp.seatNo=="" || !tmp.seatNo){core.info("MessageSharing.qml | __setInvitationStatus | SeatNo Not Set ");return false;}
        __invitationReceivedModel.remove(index,true);
        if(action=="optout"){disable();}
        Store.receiverInvitationQueue.push({"seatNo":tmp.seatNo,"action":action,"metaData":tmp.metaData,"messageIdentifier":tmp.messageIdentifier,"messageText":tmp.messageText});
         __processIntivationRequest();
        if(action=="accept"){
            var tmpArr =tmp.metaData.split(",");
            for(var j in tmpArr){
                __receivedModel.append({"seatNo":tmp.seatNo,"metaData":tmpArr[j],"messageIdentifier":tmp.messageIdentifier,"messageText":tmp.messageText,"userDefined1":"","userDefined2":""});
            }
        }
        return true;
    }

    function __processIntivationRequest(){
        if(!Store.isClient2Active){
            if(Store.receiverInvitationQueue.length==0){core.debug("MessageSharing.qml | __processIntivationRequest | No request pending");return true;}
            Store.isClient2Active=true;
            var sender=__getHostName(Store.receiverInvitationQueue[0].seatNo);
            __setClient2ServerHostName(sender);
        }else {
            core.debug("MessageSharing.qml | __processIntivationRequest | isClient2Active: "+Store.isClient2Active+" | Current Receiver: "+client2.serverHostname);
            return false;
        }
    }

    function __generateJsonMessage(result){
        var retJsonMsg = result;
        return JSON.stringify(retJsonMsg);
    }

    function __getHostName(seatNo){
        if(seatNo.length==2) seatNo="seat00"+seatNo;
        else if(seatNo.length==3) seatNo="seat0"+seatNo;
        else seatNo="seat"+seatNo;
        return seatNo;
    }

    function __getSeatNumber(seatNo){
        return seatNo.replace("seat00","").replace("seat0","");
    }

    function __setClient1ServerHostName(seatNo){
        if(seatNo==undefined){ core.info("MessageSharing.qml | __setClient1ServerHostName | Need seatNo to continue. Exiting...."); return false;}
        else {client1.online=false; client1.serverHostname=seatNo; client1.online=true;
            core.info("MessageSharing.qml | __setClient1ServerHostName | seatNo: "+seatNo);
            return true;
        }
    }

    function __setClient2ServerHostName(seatNo){
        if(seatNo==undefined){ core.info("MessageSharing.qml | __setClient2ServerHostName | Need seatNo to continue. Exiting...."); return false;}
        else {client2.online=false; client2.serverHostname=seatNo; client2.online=true;
            core.info("MessageSharing.qml | __setClient2ServerHostName | seatNo: "+seatNo);
            return true;
        }
    }

    function __requestToShare(senderSeatNo,metaData,messageIdentifier,messageText){
        core.debug("MessageSharing.qml | requestToShare | Invitation Received:  senderSeatNo is " + senderSeatNo+" |  metaData  is " + metaData+" | messageIdentifier: "+messageIdentifier  );
            __invitationReceivedModel.append({"seatNo":senderSeatNo,"metaData":metaData,"messageIdentifier":messageIdentifier,"messageText":messageText});
        sigInvitationReceived(senderSeatNo,metaData,messageIdentifier,messageText,(__invitationReceivedModel.count-1));
    }

    Connections{
        target: pif.__sharedModelServer;
        onSigMessageSharingDataReceived:{
            core.debug("MessageSharing.qml | onSigUsbSharingDataReceived | message is " + JSON.stringify(msgObj)+"__isEnable:"+__isEnable);
            if(msgObj.requestToShare){
                client.send({messageType: "requestToShareAck",sharingStatus:(__isEnable?"enable":"disable"),reason:"MessageSharing"});
                if(!__isEnable){return false;}
                __requestToShare(msgObj.__senderSeatNo,msgObj.__metaData,msgObj.__messageIdentifier,msgObj.__messageText);
            }else if(msgObj.invitationStatus){
                sigSharingStatus(msgObj.status,__getSeatNumber(client.properties.name));
                pendingInvitation--;
                client.send({messageType: "invitationStatusAck",reason:"MessageSharing"});
                if(msgObj.status=="optout"){
                    var indxOfMid=Store.senderRecipientsList.indexOf(client.properties.name);
                    if(indxOfMid>=0){Store.senderRecipientsList.splice(indxOfMid,1);}
                }
            }
        }
    }

    function __serverConnectedAck(client){
       core.debug("MessageSharing.qml | __serverConnectedAck | step 1: __init : init message sent to server "+client.serverHostname);
       client.sendMessage("json",__generateJsonMessage({name:pif.getSeatNumber(),reason:"MessageSharing",messageType:"init","serverHostname":client.serverHostname}));
    }

    function __disconnectLocalClient1(){client1.online=false;Store.isClient1Active=false;var x=Store.senderRequestQueue.shift();__processSenderQueue();return true;}
    function __disconnectLocalClient2(){client2.online=false;Store.isClient2Active=false;Store.receiverInvitationQueue.shift(); __processIntivationRequest();return true;}

    function __client1ReceivedAck(message,messageType){
        core.debug("MessageSharing.qml | __client1ReceivedAck, message:"+JSON.stringify(message)+" messageType: "+messageType)
        if(message.messageType=="initAck") {
            core.debug("MessageSharing.qml | __client1ReceivedAck | step 2: initAck ");
            var tmp=Store.senderRequestQueue[0];
            client1.sendMessage("json",__generateJsonMessage({requestToShare:true,__metaData:tmp.metaData,__messageIdentifier:tmp.messageIdentifier,__messageText:tmp.messageText,__senderSeatNo:pif.getSeatNumber(),reason:"MessageSharing"}));
        }else if(message.messageType=="requestToShareAck") {
            core.debug("MessageSharing.qml | __client1ReceivedAck | step 3: requestToShareAck | sharingStatus: "+message.sharingStatus);
            if(message.sharingStatus!=undefined){sigSharingStatus(message.sharingStatus,__getSeatNumber(client1.serverHostname));if(message.sharingStatus=="disable"){pendingInvitation--;}}
            __disconnectLocalClient1();
        }
    }

    function __client2ReceivedAck(message,messageType){
        core.debug("MessageSharing.qml | __client2ReceivedAck, message:"+JSON.stringify(message)+" messageType: "+messageType)
        if(message.messageType=="initAck") {
            core.debug("MessageSharing.qml | __client2ReceivedAck | step 2: initAck ");
            var tmp=Store.receiverInvitationQueue[0];
            client2.sendMessage("json",__generateJsonMessage({invitationStatus:true,status:tmp.action,imgName:tmp.imgName,reason:"MessageSharing"}));
        }else if(message.messageType=="invitationStatusAck"){
            __disconnectLocalClient2();
        }
    }

    NetworkClient{
        id:client1;
        onConnected: __serverConnectedAck(client1);
        onReceived: __client1ReceivedAck(message,messageType);
        onOnlineChanged: core.debug("MessageSharing.qml | client1 onOnlineChanged "+online);
        onServerHostnameChanged: core.debug("MessageSharing.qml | client1 onServerHostnameChanged "+serverHostname);
        onDisconnected: {core.debug("MessageSharing.qml | client1 onDisconnected");if(!Store.isClient1Active){__disconnectLocalClient1();}}
        onError:{core.debug("MessageSharing.qml | client1 onError "+error);sigConnectionRefused(__getSeatNumber(serverHostname));pendingInvitation--;__disconnectLocalClient1();}
     }

    NetworkClient{
        id:client2;
        onConnected: __serverConnectedAck(client2);
        onReceived: __client2ReceivedAck(message,messageType);
        onOnlineChanged: core.debug("MessageSharing.qml | client2 onOnlineChanged "+online);
        onServerHostnameChanged: core.debug("MessageSharing.qml | client2 onServerHostnameChanged "+serverHostname);
        onDisconnected: {core.debug("MessageSharing.qml | client2 onDisconnected");if(!Store.isClient2Active){__disconnectLocalClient2();}}
        onError:{core.debug("MessageSharing.qml | client2 onError "+error);sigConnectionRefused(__getSeatNumber(serverHostname)); __disconnectLocalClient2();}
    }

    SimpleModel{id:__senderSharedModel}
    SimpleModel{id:__receivedModel}
    SimpleModel{id:__invitationReceivedModel}

    function frmwkResetApp(){
        core.info("MessageSharing.qml | frmwkResetApp | Called ");
        Store.senderRequestQueue=[]; Store.receiverImageRequest={};Store.receiverInvitationQueue=[];Store.senderRecipientsList=[];Store.validateSeatnoQueue=[];
        client1.online=false;Store.isClient1Active=false;
        client2.online=false;Store.isClient2Active=false;
        pendingInvitation=0;
        __senderSharedModel.clear();__receivedModel.clear();__invitationReceivedModel.clear();
    }

    Component.onCompleted: {frmwkResetApp();}
}
