import QtQuick 1.1
import "../components"
import Panasonic.Pif 1.0

Item{
    id:vip
    property int volume:0;
    property int volSource:0;
    property int defaultAudioVolume:0;
    property int defaultVideoVolume:0;
    property int oldVolumeLevel:0;      // volume level before muting the volume.
    property bool muted:false;
    property int brightnessLevel:0;
    property int brightnessDefault:0;
    property int displayType:0;
    property string currentPlayingSource:"";
    property int contrastDefault:0;
    property bool turnedMonitorON:false;
    property bool resumeBluRayOnPAOff:false;
    property bool setHDMI1ModeOnPAOff:false;

    property int cPAC_DISPLAY:1;
    property int cLHT_DISPLAY:2;

    property alias evidClient:evidClientVip;
    property variant lht;
    property variant blueRay;

    signal sigDataReceived(string api,variant params);
    signal volumeChanged(int volumeLevel,int volumeSource);
    signal paVolumeChanged(int volumeLevel);
    signal brightnessChanged(int brightnessLevel);
    signal contrastChanged(int contrastLevel);
    signal backlightStateChanged(int backlightState);
    signal evidStateChanged(string evidName, variant value);
    signal sigMidPlayBegan();
    signal sigMidPlayStopped();
    signal sigMediaPaused();
    signal sigMediaForward();
    signal sigMediaRewind();
    signal sigToggleMuteState(bool muteState);

    Connections{
        target: evidClient?evidClient:null
        onMessageReceived: {__evidMessageReceived(command, value);}
    }

    Connections {
        target:pif
        onVolumeChanged:{
            var evidVolume = pif.getLru1DArrayData("APA_VIDs");
            if(evidVolume)return;
            volume=volumeLevel;
            volSource=volumeSource;
            vip.volumeChanged(volumeLevel,volumeSource);
        }
        onPaVolumeChanged:{
            var evidVolume = pif.getLru1DArrayData("APA_VIDs");
            if(evidVolume)return;
            vip.paVolumeChanged(volumeLevel);
        }
        onBrightnessChanged:{
            if(displayType != cPAC_DISPLAY)return;
            brightnessChanged(brightnessLevel);
        }
        onBacklightStateChanged:{
            if(displayType != cPAC_DISPLAY)return;
            backlightStateChanged(backlightState);
        }
        onContrastChanged:{
            if(displayType != cPAC_DISPLAY)return;
            contrastChanged(contrastLevel);
        }
        onPaStateChanged:{
            core.info("Vip.qml | PA state changed. paState " + paState + " turnedMonitorON " + turnedMonitorON + " resumeBluRayOnPAOff " + resumeBluRayOnPAOff + " setHDMI1ModeOnPAOff " + setHDMI1ModeOnPAOff);
            if(displayType != cLHT_DISPLAY)return;

            if(blueRay){
                if(paState > 0 && blueRay.getBlueRayStatus()==blueRay.cMEDIA_PLAY){
                    core.info("Vip.qml | blu ray player need to be paused.")
                    blueRay.pause();resumeBluRayOnPAOff = true;
                }
                else if(paState > 0 && (blueRay.getBlueRayStatus()==blueRay.cMEDIA_PAUSE || blueRay.getBlueRayStatus()==blueRay.cMEDIA_REWIND ||blueRay.getBlueRayStatus()==blueRay.cMEDIA_FORWARD)){
                    core.info("Vip.qml | blu ray player unchanged.")
                    setHDMI1ModeOnPAOff = true;
                }
                else if(paState == 0 && resumeBluRayOnPAOff == true){
                    core.info("Vip.qml | blu ray player to be resumed. monitor mode to be changed to HDMI1");
                    blueRay.resume();setMonitorSource("hdmi1");resumeBluRayOnPAOff = false;
                }
                else if(paState == 0 && setHDMI1ModeOnPAOff == true){
                    core.info("Vip.qml | blu ray player unchanged. monitor mode to be changed to HDMI1");
                    setMonitorSource("hdmi1");setHDMI1ModeOnPAOff = false;
                }
            }
            if(paState == 0 && turnedMonitorON == true){
                setBacklightOff();turnedMonitorON = false;
                core.info("Vip.qml | LHT monitor turned Off. turnedMonitorON " + turnedMonitorON);
            }
        }
    }

    Connections{
        target: (pif.sharedModelServerInterface)?pif.sharedModelServerInterface:null;
        onSigDataReceived:{
            if(api.indexOf("BlueRay") != -1){
                if(api == "playBlueRay"){blueRay.play();}
                else if(api == "stopBlueRay"){blueRay.stop();}
                else if(api == "pauseBlueRay"){blueRay.pause();}
                else if(api == "resumeBlueRay"){blueRay.resume();}
                else if(api == "fastForwardBlueRay"){blueRay.forward();}
                else if(api == "rewindBlueRay"){blueRay.rewind();}
                else if(api == "nextBlueRay"){blueRay.playNext();}
                else if(api == "previousBlueRay"){blueRay.playPrevious();}
                else if(api == "menuTitleBlueRay"){blueRay.menuTitle();}
                else if(api == "menuDiscBlueRay"){blueRay.menuDisc();}
                else if(api == "upBlueRay"){blueRay.up();}
                else if(api == "downBlueRay"){blueRay.down();}
                else if(api == "leftBlueRay"){blueRay.left();}
                else if(api == "rightBlueRay"){blueRay.right();}
                else if(api == "enterBlueRay"){blueRay.enter();}
                else if(api == "setBlueRayMediumType"){blueRay.setMediumType(params.blueRayMediumType);}
            }
            else{ var apiParams=params; apiParams.source="vkarma"; vip.sigDataReceived(api,apiParams);}
        }
    }

    Connections{
        target: (pif.gcpInterface)?pif.gcpInterface:null;
        onSigDataFromGCP:{
            core.info("Vip.qml | onSigDataFromGCP | api: " + api);
            var apiParams=params; apiParams.source="gcp";
            vip.sigDataReceived(api,apiParams);
        }
    }

    Connections{
        target: (pif.lastSelectedVideo!=pif.ipod)?pif.lastSelectedVideo:null;
        onSigMidPlayBegan:{
            var connectionType;
            core.info("Vip.qml | Connections: target:pif.lastSelectedVideo | onSigMidPlayBegan");
            currentPlayingSource = "interactive";
            __setMode(currentPlayingSource);
            connectionType = pif.getLruData("Blue_Ray_Connection");
            core.info("BlueRay.qml | connectionType:" + connectionType);
            if(displayType == cLHT_DISPLAY && parseInt(connectionType,10) == 1){
                if(blueRay.getBlueRayStatus() != blueRay.cMEDIA_STOP){
                    core.info("Blu ray player will be stopped.");
                    blueRay.stop();
                }
                else core.info("Blu ray player state is stop.");
            }
        }
    }
    Connections{
        target: pif.lastSelectedAudio
        onSigMidPlayBegan:{
            currentPlayingSource = "interactive";
            __setMode(currentPlayingSource);
        }
    }

    Connections{
        target: (pif.ipod)?pif.ipod:null;
        onSigAggPlayBegan:{
            if(currentPlayingSource == "vga") __setMode("vga");
            else if(currentPlayingSource == "hdmi1") __setMode("hdmi1");
            else if(currentPlayingSource == "hdmi2") __setMode("hdmi2");
        }
    }

    Connections {
        target: (pif.seatInteractive)?pif.seatInteractive:null;
        onSigDataReceived:{
            if(api=="frameworkPasStart"){
                pif.playPAS(params.deviceName);
            }else if(api=="frameworkPasStop"){
                pif.stopPAS();
            }
        }
    }

    //Public functions

    function getDisplayType(){ return displayType;}

    function setMonitorSource(src){
        core.info("Vip.qml | set Monitor source called. source: " + src);
        if(src=="vga") __setVGA();
        else if(src=="hdmi1") __setHDMI1();
        else if(src=="hdmi2") __setHDMI2();
        else if(src=="interactive") __setInteractiveMode();
    }

    function setVolumeByStep(step){
        core.info("Vip.qml | Set Volume by step called. Step:"+step);
        var minVol = (pif.getPAState()==0)?0:pif.getMinPAVolume();
        var volumevalue=volume+ pif.getVolumeStep()*(String(parseInt(step,10))!="NaN"?parseInt(step,10):minVol);
        var volumeTmp=volumevalue<minVol?minVol:volumevalue>100?100:volumevalue;
        var evidVolume = pif.getLru1DArrayData("APA_VIDs");
        if (evidVolume){
            for(var i = 0; i < evidVolume.length; i++)
                if (evidVolume[i]!=="") evidClient.send(evidVolume[i],Math.round(volumeTmp/3)); // LHT volume range is 0-33, hence the "/ 3"
        }else {
            pif.setVolumeByValue(volumeTmp);volume=volumeTmp;
        }
        return true;
    }

    function setVolumeByValue(volumevalue){
        core.info("Vip.qml | Set Volume by value called value: "+volumevalue);
        var minVol = (pif.getPAState()==0)?0:pif.getMinPAVolume();
        var volumeTmp=!volumevalue||volumevalue<minVol?minVol:volumevalue>100?100:volumevalue;
        var evidVolume = pif.getLru1DArrayData("APA_VIDs");
        if (evidVolume){
            for(var i = 0; i < evidVolume.length; i++)
                if (evidVolume[i]!=="") evidClient.send(evidVolume[i],Math.round(volumeTmp/3)); // LHT volume range is 0-33, hence the "/ 3"
        }else {
            pif.setVolumeByValue(volumeTmp);volume=volumeTmp;
        }
        return true;
    }

    function toggleMuteState(){
        core.info("Vip.qml | Toggle Mute State called ");
        var evidVolume = pif.getLru1DArrayData("APA_VIDs");
        if (evidVolume){
            if(muted) {
                for(var i = 0; i < evidVolume.length; i++)
                    if (evidVolume[i]!=="") evidClient.send(evidVolume[i],oldVolumeLevel);
                muted=false;
            }else {
                oldVolumeLevel = volume;
                for(var i = 0; i < evidVolume.length; i++)
                    if (evidVolume[i]!=="") evidClient.send(evidVolume[i],0);
                muted=true;
            }
            sigToggleMuteState(muted);
        }else {pif.toggleMuteState();}
        return true;
    }

    function setBrightnessByStep(step){
        if(displayType==cLHT_DISPLAY){lht.setBrightnessByStep(step);}
        else if(displayType==cPAC_DISPLAY){pif.setBrightnessByStep(step);}
    }

    function setBrightnessByValue(brightvalue){
        if(displayType==cLHT_DISPLAY){lht.setBrightnessByValue(brightvalue);}
        else if(displayType==cPAC_DISPLAY){pif.setBrightnessByValue(brightvalue);}
    }

    function setContrastByStep(step){   // contrastlevel => 1 or 2... to increase Contrast, -1 or -2... to decrease Contrast, 0 or blank: no change
        if(displayType==cLHT_DISPLAY){lht.setContrastByStep(step);}
        else if(displayType==cPAC_DISPLAY){pif.setContrastByStep(step);}
    }

    function setContrastByValue(contrastvalue){
        if(displayType==cLHT_DISPLAY){lht.setContrastByValue(contrastvalue);}
        else if(displayType==cPAC_DISPLAY){pif.setContrastByValue(contrastvalue);}
    }

    function setBacklightOn(){
        if(displayType==cLHT_DISPLAY){lht.setBacklightOn();}
        else if(displayType==cPAC_DISPLAY){pif.setBacklightOn();}
    }

    function setBacklightOff(){
        if(displayType==cLHT_DISPLAY){lht.setBacklightOff();}
        else if(displayType==cPAC_DISPLAY){pif.setBacklightOff();}
    }

    function getVolumeLevel(){return volume;}

    function getDefaultAudioVolume(){return defaultAudioVolume;}
    function getDefaultVideoVolume(){return defaultVideoVolume;}

    function getVolumeStep(){return pif.getVolumeStep();}
    function getMinPAVolume(){return pif.getMinPAVolume();}

    function getBrightnessLevel(){
        if(displayType == cLHT_DISPLAY){return lht.getBrightnessLevel();}
        else if(displayType == cPAC_DISPLAY){return pif.getBrightnessLevel();}
    }

    function getBrightnessStep(){return pif.getBrightnessStep();}

    function getDefaultBrightness(){return brightnessDefault;}

    function getContrastLevel(){
        if(displayType == cLHT_DISPLAY){return lht.getContrastLevel();}
        else if(displayType == cPAC_DISPLAY){return pif.getContrastLevel();}
    }

    function getDefaultContrast(){return contrastDefault;}

    function getBacklightState(){
        if(displayType == cLHT_DISPLAY){return lht.getBacklightState();}
        else if(displayType == cPAC_DISPLAY){return pif.getBacklightState();}
    }

    function stopBluRay(){blueRay.stop();}

    function getBlueRayStatus(){ return blueRay.getBlueRayStatus();}

    function frmwkInitApp(){
        core.info("Vip.qml | frmwkInitApp() ");
        __setDisplayType();

        if(pif.sharedModelServerInterface)pif.sharedModelServerInterface.__setPifReference(pif.vip);
        if(pif.gcpInterface)pif.gcpInterface.__setPifReference(pif.vip);
        pif.screenSaver.__setPifReference(pif.vip);

        defaultAudioVolume = pif.getDefaultAudioVolume();
        setVolumeByValue(pif.getDefaultAudioVolume());
        defaultVideoVolume = pif.getDefaultVideoVolume();
        brightnessDefault = pif.getDefaultBrightness();
        setBrightnessByValue(brightnessDefault);
        contrastDefault = pif.getDefaultContrast();
        setContrastByValue(contrastDefault);
    }

    function frmwkResetApp(){
    }

    //Private functions
    function __evidMessageReceived(command, value){
        var evidVolume = pif.getLru1DArrayData("APA_VIDs");
        if (evidVolume) {
            for(var i = 0; i < evidVolume.length; i++){
                if(evidVolume[i] && command===evidVolume[i]){
                    core.debug("Vip.qml | __evidMessageReceived command: " + command + " value: " + value);
                    core.info("Vip.qml | __evidMessageReceived | volume VID matches");
                    volume = (value == 33) ? 100 : value * 3; // LHT volume range is 0-33
                    core.info("Vip.qml | __evidMessageReceived | New volume level is  " + volume);
                    volSource=0;
                    volumeChanged(volume,volSource);
                }
            }
        }
    }

    function __setDisplayType(){
        core.info("Vip.qml | __setDisplayType | set display type called");
        displayType=cLHT_DISPLAY;
        var connectionArr = pif.getLru2DArrayData("LRU_Connection_Info");
        if(connectionArr) for(var i=0; i<connectionArr.length;i++){if(connectionArr[i][4]=="311") {displayType=cPAC_DISPLAY;break;}}
        if(displayType==cLHT_DISPLAY){lht.initialise();core.info("Vip.qml | __setDisplayType | Display type set to LHT");}
        else {core.info("Vip.qml | __setDisplayType | Display type set to PAC");}
        blueRay.initialise();
    }

    function __setMode(currPlayingSource){
        switch(currPlayingSource){
        case "interactive":{
            if(displayType == cLHT_DISPLAY){
                lht.setMonitorMode(currPlayingSource);
            }
            else if(displayType == cPAC_DISPLAY){
                pif.setLcdmInput(8);
            }
            break;
        }
        case "hdmi1":{
            if(displayType == cLHT_DISPLAY){
                lht.setMonitorMode(currPlayingSource);
            }
            else if(displayType == cPAC_DISPLAY){
                pif.setLcdmInput(6);
            }
            break;
        }
        case "hdmi2":{
            if(displayType == cLHT_DISPLAY){
                lht.setMonitorMode(currPlayingSource);
            }
            else if(displayType == cPAC_DISPLAY){
                pif.setLcdmInput(6);
            }
            break;
        }
        case "vga":{
            if(displayType == cLHT_DISPLAY){
                lht.setMonitorMode(currPlayingSource);
            }
            else if(displayType == cPAC_DISPLAY){
                pif.setLcdmInput(2);
            }
            break;
        }
        }
    }

    function __setHDMI1(){
        var source = "none";
        core.info("Vip.qml | set HDMI1 called.");
        currentPlayingSource="hdmi1";
        var sourceArr = pif.getLru2DArrayData("Monitor_AV_Sources");
        if(sourceArr){core.info("HDMI audio is "+ sourceArr[1][1]);
           source = (sourceArr[1][1] == "spdif") ? "camera" : (sourceArr[1][1] == "aux1") ? "svideo1" : (sourceArr[1][1] == "aux2") ? "svideo2" : "none";}
        core.info("sourceType passed to AVCaptureViewer is " + source);
        pif.ipod.play({pipID:-1,sourceType:source,withBGAudio:false});
    }

    function __setHDMI2(){
        var source = "none";
        core.info("Vip.qml | set HDMI2 called.");
        currentPlayingSource="hdmi2";
        var sourceArr = pif.getLru2DArrayData("Monitor_AV_Sources");
        if(sourceArr){core.info("HDMI audio is "+ sourceArr[1][1]);
          source = (sourceArr[1][1] == "spdif") ? "camera" : (sourceArr[1][1] == "aux1") ? "svideo1" : (sourceArr[1][1] == "aux2") ? "svideo2" : "none";}
        core.info("sourceType passed to AVCaptureViewer is " + source);
        pif.ipod.play({pipID:-1,sourceType:source,withBGAudio:false});
    }

    function __setVGA(){
        core.info("Vip.qml | set VGA called.");
        currentPlayingSource="vga";
        pif.ipod.play({pipID:-1,sourceType:"none",withBGAudio:false});
    }

    function __setInteractiveMode(){
        core.info("Vip.qml | set Interactive Mode called.");
        currentPlayingSource="interactive";
        pif.lastSelectedVideo.stop();
        __setMode("interactive");
    }

    function __loadComponents(){
        lht = lhtComp.createObject(vip);
        blueRay = bluerayComp.createObject(vip);
        core.info("Vip.qml | __loadComponents | lht: "+lht+" | bluray: "+blueRay);
    }

    EvidInterface{
        id:evidClientVip
    }

    Component {
        id: lhtComp;
        Lht{
            id:lht;
            onBrightnessChanged:{
                if(displayType != cLHT_DISPLAY)return;
                vip.brightnessChanged(brightnessLevel);
            }
            onBacklightStateChanged:{
                core.info("Vip.qml | onBacklightStateChanged. backlightState " + backlightState);
                if(displayType != cLHT_DISPLAY)return;
                if(backlightState==0 && pif.getPAState()==2){
                    setBacklightOn();turnedMonitorON = true;
                    core.info("Vip.qml | monitor turned ON. turnedMonitorON " + turnedMonitorON);
                }
                vip.backlightStateChanged(backlightState);
            }
            onContrastChanged:{
                if(displayType != cLHT_DISPLAY)return;
                vip.contrastChanged(contrastLevel);
            }
            onSigMonitorModeChanged:{
                core.info("Vip.qml | signal sigMonitorModeChanged() received. mode: " + mode);
                if(displayType != cLHT_DISPLAY)return;
                if(mode == "HDMI_1") vip.__setHDMI1();
                else if(mode == "COMPONENT") vip.__setInteractiveMode();
                else if(mode == "VGA") vip.__setVGA();
            }
            onEvidChanged:{
                evidStateChanged(evidName,value);
            }
        }
    }

    Component {
        id: bluerayComp;
        BlueRay{
            id:blueRay;
            onSigMidPlayBegan:{
                vip.sigMidPlayBegan();if(currentPlayingSource!="hdmi1"){pif.vip.setMonitorSource("hdmi1");pif.screenSaver.stopScreenSaver();}
            }
            onSigMidPlayStopped:{
                if(currentPlayingSource=="hdmi1"){setMonitorSource("interactive");}
                vip.sigMidPlayStopped();pif.unSuspendAudioPlay();
            }
            onSigMediaPaused:{
                vip.sigMediaPaused();
            }
            onSigMediaForward:{
                vip.sigMediaForward();
            }
            onSigMediaRewind:{
                vip.sigMediaRewind();
            }
        }
   }

    Component.onCompleted: {core.info("Vip.qml | Vip component load complete");}
}
