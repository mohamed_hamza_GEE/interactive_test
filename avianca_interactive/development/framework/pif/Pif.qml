import QtQuick 1.1
import Panasonic.Pif 1.0

PifData {
    id: pif
    property variant lastSelectedAudio:aod;
    property variant lastSelectedVideo:vod;
    property variant vod;
    property variant aod;
    property variant ctMessage;
    property variant broadcastVideo;
    property variant broadcastAudio;
    property variant vkarmaToSeat;
    property variant telephony;
    property variant vkb;
    property variant ipod;
    property variant exphone;
    property variant aodPlaylist;
    property variant kidsAodPlaylist;
    property variant paxus;
    property variant usb;
    property variant seatChat;
    property variant windowDimmable;
    property variant usbMp3;
    property variant usbImageSharing;
    property variant premiumHandset;
    property variant extv;
    property variant ppv;
    property variant vodPlaylist;
    property variant kidsVodPlaylist;
    property alias deviceMngr: deviceMngr;
    property alias screenSaver: screenSaver;
    property variant launchApp;
    property variant sharedModelServerInterface;
    property variant __sharedModelServer;
    property variant engine4Interface:Item{function sendApiData(api,params){core.info("Pif.qml | No Engine4 client available");}}
    property variant seatInteractive;
    property variant vip;
    property variant gcpInterface;
    property variant android;
    property variant smsEmail;
    property variant transientDataSimulation;
    property variant messageSharing;
    property variant usbPlaylist;
    property variant textToSpeech;
    property variant emailMessageSharing;
    property variant visuallyImpaired;

    onInteractiveOverrideChanged: {
        if(interactiveOverride){
            if (lastSelectedAudio) {
                if(lastSelectedAudio.getSuspendStatus()){
                    core.info("Pif.qml | onInteractiveOverrideChanged | Clearing suspend state");
                    lastSelectedAudio.__clearSuspend();
                }
            }
        }
    }

    function suspendAudioPlay() {return (!lastSelectedAudio||!lastSelectedAudio.suspendPlaying())?false:true;}
    function unSuspendAudioPlay() {if(!lastSelectedAudio||!lastSelectedAudio.unSuspendPlaying()) {setVolumeSource(cAOD_VOLUME_SRC);return false;} else {return true;}}
    function getLSAmediaSource(){return lastSelectedAudio.mediaSource;}
    function getLSVmediaSource(){return (lastSelectedVideo)?lastSelectedVideo.mediaSource:"";}
    function setHeartBeatInterval(interval){heartBeat.interval = interval; return true;}
    function loadAod(){
        core.info("Pif.qml | load aod ");
        if(!aod){
            aod = Qt.createQmlObject('import QtQuick 1.1; Aod{deviceMngr:pif.deviceMngr;}',pif);
            interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[aod.frmwkInitApp]);
        }
        return aod;
    }

    function loadSharedModelClientInterface(serverIP){
        core.info("Pif.qml | loadSharedModelClientInterface serverIP : " + serverIP);
        if(__getCompList().indexOf("QTIP2")>=0){
            var obj=core.coreHelper.loadDynamicComponent("pif/SharedModelClientInterfaceQTIP2.qml",pif);
        } else if(__getCompList().indexOf("NetworkClient")>=0){
            var obj=core.coreHelper.loadDynamicComponent("pif/SharedClientInterface.qml",pif);
            obj.online = false;
        } else {
            var obj=core.coreHelper.loadDynamicComponent("pif/SharedModelClientInterface.qml",pif);
        }
        obj.serverHostname=serverIP;
        if(__getCompList().indexOf("NetworkClient")>=0) obj.online = true;
        return obj;
    }

    function loadSharedModelBrowserComponentInterface(serverIP){
        core.info("Pif.qml | loadSharedModelBrowserComponentInterface serverIP : " + serverIP);
        var obj=core.coreHelper.loadDynamicComponent("pif/SharedModelBrowserComponentInterface.qml",pif)
        obj.serverIP=serverIP;return obj;
    }

    function loadSharedModelServerInterface(){
        core.info("Pif.qml | loadSharedModelServerInterface ");
        __loadSharedModelServer();
        if(!sharedModelServerInterface){
            if(__getCompList().indexOf("QTIP2")>=0){
                sharedModelServerInterface=core.coreHelper.loadDynamicComponent("pif/SharedModelServerInterfaceQTIP2.qml",pif);
            }else {
                sharedModelServerInterface=core.coreHelper.loadDynamicComponent("pif/SharedModelServerInterface.qml",pif);
            }
            interactiveStartup.registerToStage(interactiveStartup.cStageShowScreen,[sharedModelServerInterface.setDefaultValues]);
        }
        return sharedModelServerInterface;
    }

    function loadSharedModelSimClientInterface(){
        core.info("Pif.qml | loadSharedModelSimClientInterface ");
        __sharedModelServer=core.coreHelper.loadDynamicComponent("testpanel/NetworkClientSim.qml",pif);

    }

    function loadGcpInterface(){
        core.info ("Pif.qml | LOADING Gcp Interface");
        if(!gcpInterface){
            gcpInterface = core.coreHelper.loadDynamicComponent("pif/GcpInterface.qml",pif);
            core.info ("pif | GcpInterface Reference: "+gcpInterface);
            gcpInterface.pifInput=navInput;
            interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[gcpInterface.frmwkInitApp]);
            interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[gcpInterface.frmwkResetApp]);
            seatInteractive=loadSharedModelServerInterface();
            return gcpInterface;
        }
    }

    function __loadSharedModelServer(){
        if(!__sharedModelServer){
            core.info ("Pif.qml | LOADING __SharedModelServer...");
            if(__getCompList().indexOf("QTIP2")>=0){
                __sharedModelServer=core.coreHelper.loadDynamicComponent("components/SharedModelServerQTIP2.qml",pif);
            }else {
                __sharedModelServer=core.coreHelper.loadDynamicComponent("components/SharedModelServer.qml",pif);
            }
            core.info ("Pif.qml | __sharedModelServer: " + __sharedModelServer);
        }
    }

    function __setLastSelectedAudio(objRef){
        if(objRef==undefined || lastSelectedAudio==objRef) return false;
        lastSelectedAudio.stop(); lastSelectedAudio.__clearSuspend(); lastSelectedAudio=objRef; return true;
    }

    function __setLastSelectedVideo(objRef){
        if(objRef==undefined || lastSelectedVideo==objRef) return false;
        if(lastSelectedVideo) lastSelectedVideo.stop(); lastSelectedVideo=objRef; return true;
    }

    function __resetLastSelectedMedia(){
        if(getInteractiveOverride()){ // We are faking a stop, As core is not doing it, Synergy is opened for same LF#873979
            core.info("Pif.qml | __resetLastSelectedMedia | Received RCT event, core will stop the active media if any | Exiting...");
            lastSelectedVideo.__mediaFakeStopForceAV();
            lastSelectedAudio.__mediaFakeStopForceAV();
            __clearMidArrForcedAV()
            return true;
        }
        if (lastSelectedAudio) {lastSelectedAudio.stop(); lastSelectedAudio.__clearSuspend();}
        if (lastSelectedVideo) lastSelectedVideo.stop();
    }

    function forceHeartBeat () { heartBeat.feed(); core.info ("pif | Force Heartbeat sent");}

    function __loadDynamicComponents(){
        if(getMonitorType()==cLRU_VKARMA) {
            if(getBrowserInSeatState()) {vkarmaToSeat=loadSharedModelBrowserComponentInterface(getVkarmaSeatIp());}
            else {
                core.info("Pif.qml | __loadDynamicComponents | VkarmaSeatIp: "+getVkarmaSeatIp())
                if(getVkarmaSeatIp()!="" && getVkarmaSeatIp()!="0.0.0.0"){
                    vkarmaToSeat=loadSharedModelClientInterface(getVkarmaSeatIp());
                    enableKeyForwarding();
                }
            }
        }
        else {
            aod = Qt.createQmlObject('import QtQuick 1.1; Aod{deviceMngr:pif.deviceMngr;}',pif);
            interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[aod.frmwkInitApp]);
            vod = Qt.createQmlObject('import QtQuick 1.1; Vod{deviceMngr:pif.deviceMngr;}',pif);
            interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[vod.frmwkInitApp]);
            ctMessage = core.coreHelper.loadDynamicComponent("pif/CtMessage.qml",pif);
        }

        core.info("Pif.qml | Loading LaunchApp... "+((pif.android)?"pif/AndroidLaunchApp.qml":"pif/LaunchApp.qml"))
        launchApp = core.coreHelper.loadDynamicComponent((pif.android)?"pif/AndroidLaunchApp.qml":"pif/LaunchApp.qml",pif);
        interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[launchApp.frmwkInitApp]);
        interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[launchApp.frmwkResetApp]);

        var complist = __getCompList();
        for (var i=0;i<complist.length;i++){
            if (complist[i] == "VirtualKeyboard") { core.info ("pif | Loading Virtual Keyboard..."); vkb=core.coreHelper.loadDynamicComponent("pif/VirtualKeyboard.qml",pif); core.info ("pif | VirtualKeyboard Reference: "+vkb);}
            else if(complist[i] == "Aod") {
                core.info ("pif | Loading Aod...");
                if(!aod){
                    aod = Qt.createQmlObject('import QtQuick 1.1; Aod{deviceMngr:pif.deviceMngr;}',pif);
                    interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[aod.frmwkInitApp]);}
            }else if(complist[i] == "Vod") {
                core.info ("pif | Loading Vod...");
                if(!vod){
                    vod = Qt.createQmlObject('import QtQuick 1.1; Vod{deviceMngr:pif.deviceMngr;}',pif);
                    interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[vod.frmwkInitApp]);}
            }else if(complist[i] == "Ctmessage") {
                core.info ("pif | Loading Ctmessage...");
                if(!ctMessage){
                    ctMessage = core.coreHelper.loadDynamicComponent("pif/CtMessage.qml",pif);}
            }else if(complist[i] == "Launchapp") {
                core.info("Pif.qml | Loading LaunchApp... "+((pif.android)?"pif/AndroidLaunchApp.qml":"pif/LaunchApp.qml"))
                if(!launchApp){
                    launchApp = core.coreHelper.loadDynamicComponent((pif.android)?"pif/AndroidLaunchApp.qml":"pif/LaunchApp.qml",pif);
                    interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[launchApp.frmwkInitApp]);
                    interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[launchApp.frmwkResetApp]);}
            }else if(complist[i] == "Telephony") {
                core.info ("pif | Loading Telephony..."); telephony=core.coreHelper.loadDynamicComponent("pif/Telephony.qml",pif);
                telephony.pifInput=navInput; telephony.deviceManager=deviceMngr; telephony.initialiseComponent();
                core.info ("pif | Telephony  Reference: "+telephony);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[telephony.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[telephony.frmwkResetApp]);
            }else if(complist[i] == "Ipod") { core.info ("pif | Loading Ipod..."); ipod = Qt.createQmlObject('import QtQuick 1.1; Ipod{deviceMngr:pif.deviceMngr;}',pif); core.info ("pif | Ipod Reference: "+ipod);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[ipod.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[ipod.frmwkResetApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageEntOffScreen,[ipod.frmwkResetApp]);
            }else if(complist[i] =="Exphone"){core.info ("pif | Loading Exphone..."); exphone=core.coreHelper.loadDynamicComponent("pif/Exphone.qml",pif);core.info ("pif | Exphone Reference: "+exphone);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[exphone.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[exphone.frmwkResetApp]);
            }else if(complist[i] =="AodPlaylist"){core.info("pif | Loading AodPlaylist...");aodPlaylist=Qt.createQmlObject('import QtQuick 1.1; Playlist{__playlistType:"AodPlaylist";}',pif);core.info ("pif | AodPlaylist Reference: "+aodPlaylist);
                interactiveStartup.registerToStage((pif.getRetainPlaylistdata()?interactiveStartup.cStageSetViewData:interactiveStartup.cStageInitializeStates),[aodPlaylist.frmwkInitApp]);
                interactiveStartup.registerToStage((pif.getRetainPlaylistdata()?interactiveStartup.cStagePreCloseFlight:interactiveStartup.cStageResetStates),[aodPlaylist.frmwkResetApp]);
            }else if(complist[i] =="VodPlaylist"){core.info("pif | Loading VodPlaylist..."); vodPlaylist=Qt.createQmlObject('import QtQuick 1.1; Playlist{__playlistType:"VodPlaylist";}',pif);core.info ("pif | VodPlaylist Reference: "+vodPlaylist);
                interactiveStartup.registerToStage((pif.getRetainPlaylistdata()?interactiveStartup.cStageSetViewData:interactiveStartup.cStageInitializeStates),[vodPlaylist.frmwkInitApp]);
                interactiveStartup.registerToStage((pif.getRetainPlaylistdata()?interactiveStartup.cStagePreCloseFlight:interactiveStartup.cStageResetStates),[vodPlaylist.frmwkResetApp]);
            }else if(complist[i] =="KidsAodPlaylist"){core.info("pif | Loading KidsAodPlaylist..."); kidsAodPlaylist=Qt.createQmlObject('import QtQuick 1.1; Playlist{__playlistType:"KidsAodPlaylist";}',pif);core.info ("pif | KidsAodPlaylist Reference: "+kidsAodPlaylist);
                interactiveStartup.registerToStage((pif.getRetainPlaylistdata()?interactiveStartup.cStageSetViewData:interactiveStartup.cStageInitializeStates),[kidsAodPlaylist.frmwkInitApp]);
                interactiveStartup.registerToStage((pif.getRetainPlaylistdata()?interactiveStartup.cStagePreCloseFlight:interactiveStartup.cStageResetStates),[kidsAodPlaylist.frmwkResetApp]);
            }else if(complist[i] =="KidsVodPlaylist"){core.info("pif | Loading KidsVodPlaylist..."); kidsVodPlaylist=Qt.createQmlObject('import QtQuick 1.1; Playlist{__playlistType:"KidsVodPlaylist";}',pif);core.info ("pif | KidsVodPlaylist Reference: "+kidsVodPlaylist);
                interactiveStartup.registerToStage((pif.getRetainPlaylistdata()?interactiveStartup.cStageSetViewData:interactiveStartup.cStageInitializeStates),[kidsVodPlaylist.frmwkInitApp]);
                interactiveStartup.registerToStage((pif.getRetainPlaylistdata()?interactiveStartup.cStagePreCloseFlight:interactiveStartup.cStageResetStates),[kidsVodPlaylist.frmwkResetApp]);
            }else if(complist[i] == "Paxus"){core.info("pif | Loading Paxus..."); paxus=core.coreHelper.loadDynamicComponent("pif/Paxus.qml",pif);core.info("pif | PaxusLog Reference: "+paxus);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[paxus.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[paxus.frmwkResetApp]);
            }else if(complist[i] == "BroadcastVideo"){core.info ("pif | Loading BroadcastVideo...");
                broadcastVideo = Qt.createQmlObject('import QtQuick 1.1; BroadcastVideo{deviceMngr:pif.deviceMngr;}',pif);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[broadcastVideo.frmwkInitApp]); core.info ("pif | BroadcastVideo  Reference: "+broadcastVideo);
            }else if(complist[i] == "Usb"){core.info ("pif | Loading Usb...");usb=core.coreHelper.loadDynamicComponent("pif/Usb.qml",pif);usb.deviceManager=deviceMngr;
                core.info ("pif | Usb Reference: "+usb);
                usb.initialiseComponent();
                interactiveStartup.registerToStage((pif.getUsbDataRetainStatus()?interactiveStartup.cStagePreCloseFlight:interactiveStartup.cStageResetStates),[usb.frmwkResetApp]);
                usbMp3 = Qt.createQmlObject('import QtQuick 1.1; UsbMp3{deviceMngr:pif.deviceMngr;}',pif);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[usbMp3.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[usbMp3.frmwkResetApp]);
            }else if(complist[i] =="UsbPlaylist"){core.info("pif | Loading UsbPlaylist...");
                usbPlaylist=core.coreHelper.loadDynamicComponent("pif/UsbPlaylist.qml",pif);
                core.info ("pif | UsbPlaylist Reference: "+usbPlaylist);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[usbPlaylist.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[usbPlaylist.frmwkResetApp]);
            }else if(complist[i] == "SeatChat"){core.info ("pif | Loading SeatChat...");seatChat=core.coreHelper.loadDynamicComponent("pif/SeatChat.qml",pif);
                core.info ("pif | SeatChat Reference: "+seatChat);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[seatChat.exitChat]);
            }else if(complist[i] == "SeatChatNewApis"){core.info ("pif | Loading SeatChat with new api's...");seatChat=core.coreHelper.loadDynamicComponent("pif/SeatChatNew.qml",pif);
                core.info ("pif | SeatChat Reference: "+seatChat);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[seatChat.exitChat]);
            }else if(complist[i] == "SeatChatVkarma"){core.info ("pif | Loading SeatChat with new api's...");seatChat=core.coreHelper.loadDynamicComponent("pif/SeatChatNew.qml",pif);
                core.info ("pif | SeatChat Reference: "+seatChat);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[seatChat.exitChat]);
            }else if(complist[i] == "UsbImageSharing"){
                core.info ("pif | Loading UsbImageSharing...");__loadSharedModelServer();
                usbImageSharing=core.coreHelper.loadDynamicComponent("pif/UsbImageSharing.qml",pif);
                interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[usbImageSharing.frmwkResetApp]);
            }else if(complist[i] == "MessageSharing"){
                core.info ("pif | Loading MessageSharing...");__loadSharedModelServer();messageSharing=core.coreHelper.loadDynamicComponent("pif/MessageSharing.qml",pif);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[messageSharing.frmwkResetApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[messageSharing.frmwkResetApp]);
            }else if(complist[i] == "EmailMessageSharing"){
                core.info ("pif | Loading emailMessageSharing...");__loadSharedModelServer();emailMessageSharing=core.coreHelper.loadDynamicComponent("pif/EmailMessageSharing.qml",pif);
                interactiveStartup.registerToStage((pif.getMessageRetainStatus()?interactiveStartup.cStagePreCloseFlight:interactiveStartup.cStageInitializeStates),[emailMessageSharing.frmwkResetAppEmail]);
                interactiveStartup.registerToStage((pif.getMessageRetainStatus()?interactiveStartup.cStagePreCloseFlight:interactiveStartup.cStageResetStates),[emailMessageSharing.frmwkResetAppEmail]);
            }else if(complist[i] == "Ppv"){core.info ("pif | Loading PPV...");ppv=core.coreHelper.loadDynamicComponent("pif/Ppv.qml",pif);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[ppv.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[ppv.frmwkResetApp]);
            }else if(complist[i] == "Extv"){
                core.info ("pif | Loading extv...");
                extv = Qt.createQmlObject('import QtQuick 1.1; Extv{deviceMngr:pif.deviceMngr;}',pif);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[extv.frmwkInitApp]);
            }else if(complist[i] =="Engine4"){
                core.info ("pif | Loading Engine4 Interface...");
                if(getHandsetType()==cSTD_KARMA_HANDSET){
                    engine4Interface=core.coreHelper.loadDynamicComponent("pif/Engine4Interface.qml",pif);core.info ("pif | Engine4 Interface Reference: "+engine4Interface);
                    interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[engine4Interface.frmwkInitApp]);
                    interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[engine4Interface.frmwkResetApp]);
                }else core.error("Engine4 Interface not loaded since Std Karma Handset not connected.");
            }else if(complist[i] == "BroadcastAudio"){core.info ("pif | Loading BroadcastAudio...");
                broadcastAudio = Qt.createQmlObject('import QtQuick 1.1; BroadcastAudio{deviceMngr:pif.deviceMngr;}',pif);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[broadcastAudio.frmwkInitApp]);
                core.info ("pif | BroadcastAudio  Reference: "+broadcastAudio);
            }else if(complist[i] == "WindowDimmable"){
                core.info ("pif | Loading WindowDimmable...");
                if (getNoOfDimmableWindows()) {
                    windowDimmable = Qt.createQmlObject('import QtQuick 1.1; WindowDimmable{deviceMngr:pif.deviceMngr;}',pif);
                    core.info ("pif | WindowDimmable Reference: "+windowDimmable);
                } else core.error("WindowDimmable not loaded since number Of Dimmable Windows is zero.");
            }else if(complist[i] == "Vip"){
                var lcdmDisplay=false; var connectionArr = pif.getLru2DArrayData("LRU_Connection_Info");
                if(connectionArr) for(var j=0; j<connectionArr.length;j++){if(connectionArr[j][4]=="311") {lcdmDisplay=true;break;}}
                if(getLruData("Aircraft_SubType") === "VIP" && (lcdmDisplay || pif.getLruData("Monitor_Brightness_Control_Type"))){
                    core.info ("pif | Loading Vip...");
                    vip = core.coreHelper.loadDynamicComponent("pif/Vip.qml",pif);core.info ("pif | Vip Reference: "+vip);
                    interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[vip.frmwkInitApp]);
                    interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[vip.frmwkResetApp]);
                    vip.__loadComponents();
                    loadGcpInterface();
                }
            }else if(complist[i] == "CTtoSeatMessage" && !ctMessage){core.info("pif | Loading CTtoSeatMessage...");ctMessage=core.coreHelper.loadDynamicComponent("pif/CtMessage.qml",pif);
            }else if(complist[i] == "SmsEmail"){
                core.info ("pif | Loading smsEmail...");
                smsEmail=core.coreHelper.loadDynamicComponent("pif/SmsEmail.qml",pif);core.info ("pif | SmsEmail Reference: "+smsEmail);
                interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[smsEmail.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[smsEmail.frmwkResetApp]);
            }else if(complist[i] == "TextToSpeech" && android){
                core.info ("pif | Loading TextToSpeech...");
                textToSpeech=core.coreHelper.loadDynamicComponent("pif/AndroidTextToSpeech.qml",pif);
                core.info ("pif | TextToSpeech Reference: "+textToSpeech);
            }else if(complist[i] == "VisuallyImpaired"){
                core.info ("pif | Loading VisuallyImpaired...");
                if(pif.getMediaPlayerForViMode())visuallyImpaired=Qt.createQmlObject('import QtQuick 1.1; VisuallyImpaired2{deviceMngr:pif.deviceMngr;}',pif);
                else visuallyImpaired=core.coreHelper.loadDynamicComponent("pif/VisuallyImpaired.qml",pif);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[visuallyImpaired.frmwkInitApp]);
                interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[visuallyImpaired.frmwkResetApp]);
                core.info ("pif | VisuallyImpaired Reference: "+visuallyImpaired);
            }else {core.info ("pif | Component not supported: "+complist[i]);}
        }

        if(getHandsetType()==cPREMIUM_HANDSET) {premiumHandset=core.coreHelper.loadDynamicComponent("pif/PremiumHandset.qml",pif);premiumHandset.setPremiumHandsetDisplayArr(getPremiumHandsetDisplayArr());core.info ("pif | PremuimHandset Reference: "+premiumHandset);}
        else if(getHandsetType()==cVIDEO_KARMA_HANDSET) {
            core.info ("pif | Loading Shared Model Server Interface.");
            seatInteractive=loadSharedModelServerInterface();
        }
    }
    Component.onCompleted: {
        interactiveStartup.registerToStage(interactiveStartup.cStageLoadDynamicComponents,[__loadDynamicComponents]);
        interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[frmwkInitApp]);
        interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[__resetLastSelectedMedia]);
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOffScreen,[frmwkEntOff])
        interactiveStartup.registerToStage(interactiveStartup.cStageCloseFlightScreen,[frmwkCloseFlight])
    }

    DeviceManager { id:deviceMngr; type: (core.pc)?"simulation":"ual"; simulation: (core.pc)?true:false;}
    Watchdog { id: heartBeat; interval:60; deviceManager: deviceMngr;}		    // Heartbeat (in secs)
    ScreenSaver{ id: screenSaver;}
    Timer { id: heartBeatTimer; interval:parseInt(60*1000/3,10); running:true; repeat:true; triggeredOnStart:true; onTriggered: {heartBeat.feed(); core.info ("pif | Heartbeat sent");}}
}
