import QtQuick 1.1
import Panasonic.Pif 1.0

Item {
    id:exphone;
    property int cUNKNOWN_SERVICE:0
    property int cINCOMING_CALL:1
    property int cINCOMING_SMS:2
    property int cINCOMING_SMS_TEXT:3
    property int cREG_TIMEOUT:10;
    property int cMAX_DEVICES:4;

    //Public properties
    property int confirmationNumber:0;

    property alias deviceList:deviceList;
    property alias validDeviceList:validDeviceList;

    signal sigRegisterExphoneStage1(string deviceName,int requestStatus,string regCN,string regNumber);
    signal sigRegisterExphoneStage2(string deviceName,int registerStatus);
    signal sigIncomingCall(string deviceName,string callerNumber);
    signal sigIncomingSms(string deviceName,string callerNumber,string smsEncoding,string smsText );

    //Public functions
    function addDevice(deviceName,enableVoice,enableSms,enableSmsText){
        core.info("Exphone | ADD DEVICE CALLED DEVICE NAME IS " + deviceName + " ENABLE VOICE IS " + enableVoice + " ENABLE SMS IS " + enableSms + " ENABLE SMS TEXT IS " + enableSmsText);
        if(deviceName==undefined||deviceName == ""){core.error("Exphone | DEVICE NAME NOT PROVIDED. EXITING..."); return 1;}
        if(deviceName.length < 2||deviceName.length > 12){core.error("Exphone | DEVICE NAME NOT WITHIN LIMIT. EXITING..."); return 2;}
        if(enableVoice==undefined||enableVoice == ""){core.warn("Exphone | ENABLE VOICE NOT PROVIDED. ");enableVoice = false;}
        if(enableSms==undefined||enableSms == ""){core.warn("Exphone | ENABLE SMS NOT PROVIDED.");enableSms = false;}
        if(enableSmsText==undefined || enableSmsText == ""){core.warn("Exphone | ENABLE SMS TEXT NOT PROVIDED.");enableSmsText = false;}
        if(validDeviceList.count >= cMAX_DEVICES){core.error("Exphone | MAXIMUM DEVICE LIMIT REACHED. EXITING...");return 4;}
        for(var counter = 0;counter < validDeviceList.count;counter++) if(validDeviceList.getValue(counter,"nickName")==deviceName){core.error("Exphone | DEVICE NAME ALREADY USED. EXITING");return 3;}
        if(core.pc){
            deviceList.append({"nickName":deviceName,"deregisterStatus":2});
            __updateValidDeviceList();sigRegisterExphoneStage1(deviceName, 0, "9999", "1234");
        }
        else{phoneNotifier.beginRegisterPhone(deviceName, enableVoice, enableSms, enableSmsText);}
        return 0;
    }

    function removeDevice(deviceName){
        var isAvailable = 0;var ret;
        core.info("Exphone | REMOVE DEVICE CALLED DEVICE NAME IS " + deviceName);
        if (deviceName==undefined||deviceName== ""){core.error("Exphone | DEVICE NAME NOT PROVIDED. EXITING");return 1;}
        isAvailable=__isDeviceAvailable(deviceName);
        if(isAvailable){
            if(core.pc){
                for(var counter=0;counter<deviceList.count;counter++){
                    if(deviceList.getValue(counter,"nickName")==deviceName){
                        deviceList.set(counter,{"deregisterStatus": 0});__updateValidDeviceList();
                        core.error("Exphone | DEVICE REMOVED SUCCESSFULLY.");return 0;
                    }
                }
            }
            else{ret=phoneNotifier.deregisterPhone(deviceName);__updateValidDeviceList();}
        }
        else{core.error("Exphone | DEVICE NOT AVAILABLE.");return 2;}
        if(ret == 0){core.error("Exphone | DEVICE REMOVED SUCCESSFULLY.");return 0;}
        else{core.error("Exphone | FAILED REMOVING DEVICE.");return 3;}
    }

    function updateDevice(deviceName,serviceName,isEnabled){
        var service; var ret;
        core.info("Exphone | UPDATE DEVICE CALLED DEVICE NAME: "+deviceName+" SERVICE NAME: "+serviceName+" ISENABLED: "+isEnabled);
        if(__isDeviceAvailable(deviceName)){
            service = serviceName.toLowerCase();
            if (service=="voice") ret = phoneNotifier.setVoiceService(deviceName,isEnabled);
            else if (service=="sms") ret = phoneNotifier.setSmsService(deviceName,isEnabled);
            else if (service=="smstext") ret = phoneNotifier.setSmsTextService(deviceName,isEnabled);
            else {core.error("Exphone | SERVICE NAME IS NOT VALID. EXITING..."); return 2;}
            if(ret == 0){core.debug("Exphone | UPDATE DEVICE SUCCESSFUL"); return 0;}
            else{core.debug("Exphone | UPDATE DEVICE FAILED"); return 3;}
        }
        else {core.error("Exphone | DEVICE NOT AVAILABLE."); return 1;}
    }

    function removeAllDevices(){
        core.info("Exphone | REMOVE ALL DEVICES CALLED");
        for(var counter = 0;counter < deviceList.count; counter++) removeDevice(deviceList.getValue(counter,"nickName"));
    }

    // Initialize exphone App
    function frmwkInitApp(){core.info("Exphone | INITIALISE APP CALLED");__updateValidDeviceList()}

    // Reset exphone App
    function frmwkResetApp(){core.info("Exphone | RESET APP CALLED");removeAllDevices();validDeviceList.clear();}

    //Private functions
    function __updateValidDeviceList(){
        core.info("Exphone | UPDATE VALID DEVICE LIST CALLED");
        var registerStatus; var deregisterStatus;
        validDeviceList.clear();
        for(var counter=0;counter < deviceList.count;counter++){
            registerStatus=deviceList.getValue(counter,"registerStatus");
            deregisterStatus=deviceList.getValue(counter,"deregisterStatus");
            core.info("Exphone | counter : "+counter + " deregisterStatus : "+deregisterStatus + " registerStatus : "+registerStatus);
            if(registerStatus!=0 || deregisterStatus==0) continue;
            validDeviceList.append({"nickName":deviceList.getValue(counter,"nickName"),
                                   "requestStatus":deviceList.getValue(counter,"requestStatus"),
                                   "regCN":deviceList.getValue(counter,"regCN"),
                                   "regNumber":deviceList.getValue(counter,"regNumber"),
                                   "registerStatus":registerStatus,"deregisterStatus":deregisterStatus});
        }
        core.info("Exphone | VALID DEVICE LIST COUNT : "+validDeviceList.count);
    }

    function __isDeviceAvailable(deviceName){
        for(var counter = 0; counter < validDeviceList.count; counter++) if(validDeviceList.getValue(counter,"nickName") == deviceName) return 1;
        return 0;
    }

    SimpleModel{id:deviceList}
    SimpleModel{id:validDeviceList}

    XPhoneNotifier{
        id: phoneNotifier;
        phoneDataList:deviceList;
        regTimeout: cREG_TIMEOUT;
        onRegisterPhoneCallEnded:{
            core.info("Exphone | REGISTER PHONE CALL ENDED DEVICE NAME " + nickName + " REQUEST STATUS  " + requestStatus + " CONFIRMATION NUMBER  " + regCN + " SMS NUMBER  " + regNumber);
            confirmationNumber=regCN;sigRegisterExphoneStage1(nickName,requestStatus,regCN,regNumber);
        }
        onRegisterStatusReceived:{
            core.info("Exphone | REGISTER STATUS RECEIVED DEVICE NAME " + nickName + " STATUS  "+registerStatus);
            sigRegisterExphoneStage2(nickName, registerStatus);__updateValidDeviceList();
        }
        onIncomingVoiceReceived:{
            core.info("Exphone | INCOMING VOICE RECEIVED DEVICE NAME " + nickName + " CALLER NUMBER " + callerNumber);
            if(callerNumber == null || callerNumber == "" || callerNumber == undefined) {core.info("Exphone | NOT TAKING ANY ACTION. CALLING NUMBER IS NULL OR NOT DEFINED.");return;}
            sigIncomingCall(nickName,callerNumber);
        }
        onIncomingSmsReceived:{
            core.info("Exphone | INCOMING SMS RECEIVED DEVICE NAME " + nickName + " CALLER NUMBER " + callerNumber + " SMS ENCODING " + smsEncoding + " SMS TEXT " + smsText);
            sigIncomingSms(nickName,callerNumber,smsEncoding,smsText);
        }
    }
}
