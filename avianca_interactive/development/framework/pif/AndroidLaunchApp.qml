import QtQuick 1.0
import Panasonic.Pif 1.0
import Panasonic.AndroidAccess 1.0
import "../blank.js" as Data

Item {
    property int cNON_GUI_EXECUTABLE: 1;
    property int cPDF_VIEWER: 2;
    property int cGAME_USING_ICORE: 3;
    property int cGAME_USING_PIF: 4;
    property int cGUI_EXECUTABLE: 5;
    property int cTEXT_VIEWER: 6;
    property int cUSE_ANDROID_ACCESS_HANDLER: 7;
    property int cCONTENT_APP: 8;
    property int cUSE_ANDROID_INTENT: 9;

    property string cINTENT_ACTION_VIEW: "ACTION_VIEW";
    property string cINTENT_ACTION_EDIT: "ACTION_EDIT";
    property string cINTENT_ACTION_MAIN: "ACTION_MAIN";

    property int prevVolumeType:0;
    property int prevInputMode:0;
    property int launchType:cNON_GUI_EXECUTABLE;

    property string launchId: "";
    property string launchArgs: "";

    property bool launchInProgress:false;
    property bool mpgAudio:false;

    signal sigAppLaunched(string launchid,int launchType,string launchArgs);
    signal sigAppExit(string launchid,int launchType,string launchArgs);

    Connections{target:pif; onSigFallbackMode: if(fallbackMode) frmwkResetApp()}

    function launchApp(path, type, id, args, audio, extraParam,pkgname,classname,intentAction,intentType) {
        core.info('AndroidLaunchApp.qml | launchApp called | args '+args);
        if (!type) {core.error("AndroidLaunchApp.qml | type not provided. EXITING..."); return false;}
        if (id==undefined || id=="") {core.error("AndroidLaunchApp.qml | id not provided. EXITING..."); return false;}

        // Path needed for all besides games, textviewer & pdf for Linux, added the same for Android as of now... as there is no documentation for this on Splat
        if((type==cNON_GUI_EXECUTABLE || type==cGUI_EXECUTABLE) && (path == undefined || path == "")){core.error ("AndroidLaunchApp.qml | path not provided. Need it to continue, EXITING..."); return false;}

        if(type!=cNON_GUI_EXECUTABLE){ pif.screenSaver.stopScreenSaver(); pif.setExternalAppActive(true);}

        prevInputMode = pif.getNavInput();

        if (type==cPDF_VIEWER || type==cTEXT_VIEWER) pif.setPointerMode();

        launchInProgress = true; launchId = id; launchType = type; launchArgs = args; mpgAudio=audio
        if(!mpgAudio) {pif.suspendAudioPlay(); prevVolumeType = pif.getVolumeSource(); pif.setVolumeSource(pif.cPC_VOLUME_SRC);}

        if(type == cUSE_ANDROID_ACCESS_HANDLER)_androidaccesshandler.runPackage(id);
        else if(type == cUSE_ANDROID_INTENT) {
            var intentAct = (intentAction==cINTENT_ACTION_VIEW)?cINTENT_ACTION_VIEW:(intentAction==cINTENT_ACTION_EDIT)?cINTENT_ACTION_EDIT:(intentAction!="" && intentAction!=undefined)?intentAction:cINTENT_ACTION_MAIN;
            __runPackage(pkgname,classname,extraParam,intentAct,args,intentType);

        }
        else{
            if(!pkgname)pkgname="";
            if(!classname)classname="";
            if(!extraParam)extraParam="";
            if(extraParam == "") {myAndroidVfsLauncher.launch(args,pkgname,classname);}
            else {
                myAndroidVfsLauncher.launchWithExtras(args,pkgname,classname,extraParam);
                for(var key in extraParam) var fileName=extraParam[key];
                if(fileName!="") args=args+": "+fileName
            }
            if(pkgname!="") {
                if(Data.launchLookUp.indexOf(pkgname) != -1) Data.launchLookUp.push(pkgname);
            }
        }
        if(type==cGAME_USING_PIF){if(pif.paxus){core.info("AndroidLaunchApp.qml | launchApp, args :  "+args); pif.paxus.startGameLog(args)}}
        if(type!=cNON_GUI_EXECUTABLE) pif.__enableIFEKeys();
    }

    function launchPdf(filePath,intentAction,audio,extraParam,pkgName,className,intentType){
        core.info("AndroidLaunchApp.qml |launchPdf | pdfPath:"+filePath);
        if(filePath==undefined || filePath==""){core.error ("AndroidLaunchApp.qml | filePath not provided. Need it to continue, EXITING..."); return false;}
        var pdfPath = "file://"+filePath;
        if(intentAction=="" || intentAction==undefined)intentAction=cINTENT_ACTION_VIEW;
        if(intentType=="" || intentType==undefined)intentType="application/pdf"
        if(audio=="" || audio==undefined)audio=false
        launchApp("",cUSE_ANDROID_INTENT,cPDF_VIEWER.toString(),pdfPath,audio,extraParam,pkgName,className,intentAction,intentType);
     }

    function forceAppToQuit(){
        var ret = 0;
        if(launchType == cUSE_ANDROID_ACCESS_HANDLER) {_androidaccesshandler.forceStopPackage(launchId);}
        else {ret = __forceClose();}
        core.info('AndroidLaunchApp.qml | forceAppToQuit called, return status: '+ret);
    }

    function __onLauncherExitSignal(){
        core.info("AndroidlaunchApp.qml |__onLauncherExitSignal | launchInProgress:"+launchInProgress+", launchType"+launchType)
        if(launchInProgress == false) return;
        pif.__disableIFEKeys();
        if(launchType==cUSE_ANDROID_INTENT)__clearIntentData();
        if(launchType!=cNON_GUI_EXECUTABLE){
            pif.screenSaver.startScreenSaver();
            pif.setExternalAppActive(false);
            (prevInputMode==pif.cINPUT_MOUSE)?pif.setPointerMode():pif.setKeyMode();
            prevInputMode=0;
        }
        if(!mpgAudio) {pif.setVolumeSource(prevVolumeType); prevVolumeType=0; pif.unSuspendAudioPlay();}
        launchInProgress = false;
        if(launchType!=cNON_GUI_EXECUTABLE) sigAppExit(launchId, launchType, launchArgs); // There is no signal fired from AndroidVfsLauncher component when app is exited, hence emitted the signal here till component provides the app exit acknowledgement.
        if(launchType==cGAME_USING_PIF){ if(pif.paxus) pif.paxus.stopGameLog();}
    }

    function __clearIntentData(){
        intent.action="";
        intent.packageName="";
        intent.className="";
        intent.type="";
        intent.data="";
        intent.extras="";
    }

    function getLaunchType() {core.info("AndroidLaunchApp.qml | getLaunchType"); return launchType;}

    function forceStopPackage(pkgName){
        core.info("AndroidLaunchApp.qml | forceStopPackage | packageName: "+pkgName)
        if(launchType==cUSE_ANDROID_ACCESS_HANDLER) _androidaccesshandler.forceStopPackage(pkgName);
        else myAndroidVfsLauncher.forceStopPackageForCfg(pkgName);
    }

    function closeSpecificApp(cfgname,ltype){
        core.info("AndroidLaunchApp.qml | forceStopPackage | packageName: "+cfgname)
        if(ltype==cUSE_ANDROID_ACCESS_HANDLER) _androidaccesshandler.forceStopPackage(cfgname);
        else myAndroidVfsLauncher.forceStopPackageForCfg(cfgname);
    }

    //Added below api's but there is no support available from the core to perform pause & resume operation on app
    function pauseApp(){if(!launchInProgress) return false; core.info("AndroidLaunchApp.qml | Requesting App to Pause"); return true;}
    function resumeApp(){if(!launchInProgress) return false; core.info("AndroidLaunchApp.qml | Requesting App to Resume"); return true;}
    function closeApp(){
        if(!launchInProgress) return false;
        core.info("AndroidLaunchApp.qml | Requesting App to Close");
        if(launchType == cUSE_ANDROID_ACCESS_HANDLER) {_androidaccesshandler.forceStopPackage(launchId);}
        else {__forceClose();}
        return true;
    }
    function killApp(){
        if(!launchInProgress) return false;
        core.info("AndroidLaunchApp.qml | Requesting Kill App");
        if(launchType == cUSE_ANDROID_ACCESS_HANDLER) {_androidaccesshandler.forceStopPackage(launchId);}
        else {__forceClose();}
        return true;
    }

    function __runPackage(pkgName,classname,params,action,path,intentType){
        core.info("AndroidLaunchApp.qml | __runPackage | packageName: "+path+",pkgName:"+pkgName+",className:"+classname+", action:"+action+",intentType:"+intentType);
        intent.action = action;
        if(pkgName!="" && pkgName!=undefined)intent.packageName=pkgName;
        if(classname!="" && classname!=undefined)intent.className=classname;
        if(params!="" && params!=undefined)intent.extras=params;
        if(intentType!="" && intentType!=undefined)intent.type=intentType;
        if(path!="")intent.data=path;
        intent.startActivity();
    }

    function __forceClose(){
        _androidaccesshandler.gotoInteractive();
        if(Data.launchLookUp.length!=0){
            for(var i=0;i<Data.launchLookUp.length;i++){
                myAndroidVfsLauncher.forceStopPackageForCfg(Data.launchLookUp[i])
            }
        }
        Data.launchLookUp = new Array();
        return true;
    }

    function frmwkInitApp(){core.info("AndroidLaunchApp.qml | Initialise app called"); Data.launchLookUp=new Array();}
    function frmwkResetApp(){core.info("AndroidLaunchApp.qml | Reset app called"); __forceClose();}

    function __clearGameData(){
        core.info("AndroidLaunchApp.qml | __clearGameData | ")
        myAndroidVfsLauncher.uninstallAllFromProfiles()
    }

    AndroidVfsLauncher {
        id: myAndroidVfsLauncher
        vfsAppsDir: "/tmp/pluginshare/nbd/vfs/apps"
        onErrorMessage: core.info("AndroidLaunchApp.qml | onErrorMessage | messageText "+messageText+" returnCode "+returnCode);
        onAboutToInstallAndRun: core.info("AndroidLaunchApp.qml | onAboutToInstallAndRun | rootApkFileName "+rootApkFileName+" packageName "+packageName+" className "+className);
        onAboutToUninstallAll: core.info("AndroidLaunchApp.qml | onAboutToUninstallAll");
        onAppInstalling: core.info("AndroidLaunchApp.qml | onAppInstalling | rootFileName "+rootFileName);
        onAppLaunching: {core.info("AndroidLaunchApp.qml | onAppLaunching | rootFileName "+rootFileName); if(launchType!=cNON_GUI_EXECUTABLE) sigAppLaunched(launchId, launchType, launchArgs);}
    }

    AndroidLifecycleListener{
        id:lifecyleListner;
        onResumed:{core.info("AndroidLaunchApp.qml | AndroidLifecycleListener sends Resume Command to Interactive pageName : "+pageName);__onLauncherExitSignal();}
        onPaused:{
            core.info("AndroidLaunchApp.qml | AndroidLifecycleListener sends pause Command to Interactive");
            if((launchType == cUSE_ANDROID_ACCESS_HANDLER || launchType == cUSE_ANDROID_INTENT) && launchInProgress == true)sigAppLaunched(launchId, launchType, launchArgs);
        }
    }

    AndroidIntent{
        id:intent
        category: ["CATEGORY_LAUNCHER","CATEGORY_DEFAULT","CATEGORY_BROWSABLE"];
        flags: ["FLAG_ACTIVITY_CLEAR_TOP"];

    }

    Component.onCompleted: {
        core.info("AndroidLaunchApp.qml | Component onCompleted")
        interactiveStartup.registerToStage(interactiveStartup.cStageCloseFlightScreen,[__clearGameData]);
    }
}

