import QtQuick 1.1
import Panasonic.Pif 1.0

VideoMediaPlayer{
    property variant deviceMngr;
    property bool __prePlayState:false;

    property string ipodMediaType:"";
    property string mediaSource:"ipodVideo";

    onSigAggPlayBegan:{if(!__pipID)return; if(__prePlayState)__prePlayState=false; else pif.screenSaver.stopScreenSaver();}

    onSigAggPlayStopped: {if(!__pipID)return; if(!__prePlayState){if(pif.getLSVmediaSource() == mediaSource){ pif.unSuspendAudioPlay(); pif.screenSaver.startScreenSaver();}}}

    function play(params){
        core.info("ipod.qml | play called | params: "+params);
        params=__validateParams(params);
        return __playIpodVideo(params);
    }

    function stop() {core.info("ipod.qml | stop requested."); return __mediaStop();}

    function setPlayingSubtitle(lid) { return true;}
    function setPlayingSoundtrack(lid) { return true;}
    function playNext () { return true;}
    function playPrevious () { return true;}
    function pause () { return true;}
    function resume () { return true;}
    function forward (retainspeed) { return true;}
    function rewind (retainspeed) { return true;}
    function setPIPByID (pipID) {return __setPIPByID(pipID,__getPipReference());}
    function setPIPByArray(pipArr) { return __setPIPByArray(pipArr);}
    function pipOn() { return __pipOn();}
    function pipOff() { return __pipOff();}
    function getIpodMediaType(){ return ipodMediaType;}

    function suspendPlaying() {
        if(__mediaState==cMEDIA_STOP) return false;
        stop();
        return true;
    }

    function unSuspendPlaying() {return true;}

    function frmwkInitApp(){}

    function frmwkResetApp(){__mediaStop();}

    function __playIpodVideo(p){
        if (__mediaState!=cMEDIA_STOP) {__prePlayState=true; stop();}
        else if(!p.withBGAudio) pif.suspendAudioPlay();
        if(p.pipID==0) { ipodMediaType="audio"; pif.__setLastSelectedAudio(pif.ipod);}
        else{ ipodMediaType="video";pif.__setLastSelectedVideo(pif.ipod);}
        var s; for (var i in p) s+=i+" = "+p[i]+", "; core.debug(s);
        return __mediaPlay(p,pif.__getPipReference());
    }

    function __mediaPlay(p,piparr){
        core.debug ("ipod.qml | PLAY Called:");
        avCaptureViewer.videoOnly = false;
        avCaptureViewer.inputSource=p.sourceType;
        __setMediaPlayParams(p);
        if(p.pipID > 0){
            __setActualAspectRatio(p.aspectRatio);
            __setPIPByID(__pipID,piparr);
            avCaptureViewer.audioOnly = false;
            avCaptureViewer.viewPort = viewPort;
        }else{
            avCaptureViewer.audioOnly = true;
            avCaptureViewer.viewPort = null;
        }
        if (p.sourceType=="none") avCaptureViewer.close();
        else avCaptureViewer.open();
        playAckTimer.restart(); return true;
    }

    function __mediaPlayAck (pos,pindex,mindex) {
        core.info ("Ipod.qml | mediaPlayCallAck CALLED: position: "+pos+" playIndex: "+pindex+" __mediaState: " + __mediaState);
        __mediaState = cMEDIA_PLAY;
        __skipSpeed = 1;
        sigAggPlayBegan(0,0,0);
        return true;
    }

    function __mediaStop () {
        if(__mediaState == cMEDIA_STOP){ core.error(__playerType+" | STOP requested when media is already stopped. Exiting..."); return false;}
        stopAckTimer.restart(); return true;
    }

    function __mediaStopAck(pos,pindex,mindex,cause){
        if(__mediaState == cMEDIA_STOP){ core.error("ipod.qml | STOP requested when media is already stopped. Exiting..."); return false;}
        avCaptureViewer.close();
        __mediaState = cMEDIA_STOP;
        ipodMediaType="";
        sigAggPlayStopped(0,0,0,"");
    }

    function __setMediaPlayParams(p){
        __pipID = p.pipID;
    }

    function __validateParams(p){
        //if(!p.pipArrIndex){core.error ("ipod.qml | PIP INDEX NOT PROVIDED. EXITING..."); return false;}
        var val=pif.getIpodCableVideo();
        p.sourceType=(val==1)?"composite1":(val==2)?"svideo1":(val==3)?"composite2":(val==4)?"svideo2":(p.sourceType)?p.sourceType:"composite1";
        p.ignoreSystemAspectRatio = p.ignoreSystemAspectRatio?p.ignoreSystemAspectRatio:true;
        p.stretch = p.stretch?p.stretch:true;
        p.pipID = p.pipID?p.pipID:0;
        p.aspectRatio = p.aspectRatio?p.aspectRatio:"16x9Adjustable";
        p.elapseTime = 0;
        p.sndtrkLid = 0;
        p.sndtrkType = 0;
        p.subtitleLid = 0;
        p.subtitleType = 0;
        p.subtitleEnabled = false;
        p.amid = 0;
        p.duration = 0;
        p.elapseTimeFormat = 1;
        p.playIndex = 0;
        p.mediaType = 1;
        p.isTrailer = 0;
        p.playType = 0;
        p.rating = String(parseInt(p.rating,10))!="NaN"?parseInt(p.rating,10):254;
        p.vbMode=(p.vbMode==undefined)?false:p.vbMode;
        p.withBGAudio = p.withBGAudio?p.withBGAudio:false;
        return p;
    }

    AVCaptureViewer{id:avCaptureViewer; deviceManager:deviceMngr;}
    Timer {id:playAckTimer; interval:50; property int elapse:0; onTriggered: __mediaPlayAck(0,0,0);}
    Timer {id:stopAckTimer; interval:50; onTriggered: __mediaStopAck(0,0,0,'stop');}
    Component.onCompleted: __playerType = "Ipod Media Player";
}
