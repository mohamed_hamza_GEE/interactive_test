import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    property int cNON_GUI_EXECUTABLE: 1;
    property int cPDF_VIEWER: 2;
    property int cGAME_USING_ICORE: 3;
    property int cGAME_USING_PIF: 4;
    property int cGUI_EXECUTABLE: 5;
    property int cTEXT_VIEWER: 6;
    property int cUSE_ANDROID_ACCESS_HANDLER: 7;
    property int cCONTENT_APP: 8;
    property int cGUI_EXECUTABLE_USING_PIF: 9;

    property string launchId: "";
    property bool launchInProgress:false;
    property int queueIndex:-1;
    property int prevVolumeType:0;
    property int prevInputMode: 0;
    property string runGamePath;
    property bool isPostStartFired: false;
    property int launchType:cNON_GUI_EXECUTABLE;
    property string launchArgs: "";

    signal sigAppLaunched(string launchid,int launchType,string launchArgs);
    signal sigAppExit(string launchid,int launchType,string launchArgs);
    signal sigQueueComplete();

    Connections{target:pif; onSigFallbackMode: if(fallbackMode) frmwkResetApp()}

    function onLauncherExitSignal(){
        core.debug("launchApp.qml | onLauncherExitSignal");
        __launchEnd(launchId); __launchManager();
        sigAppExit(launchId,launchType,launchArgs);
        isPostStartFired = false;
    }

    function launchApp(launchPath, launchType, launchId, launchArgs, mpgAudio) {
        core.info("LaunchApp.qml | launchApp called");
        if (!launcherID) {core.error("LaunchApp.qml | launch App component not defined. EXITING..."); return false;}
        if (!launchType) {core.error("LaunchApp.qml | launchType not provided. EXITING..."); return false;}
        if (launchId == undefined || launchId == "") {core.error("LaunchApp.qml | launchId not provided. EXITING..."); return false;}

        // launchPath needed for all besides games, textviewer & pdf
        if((launchType==cNON_GUI_EXECUTABLE || launchType==cGUI_EXECUTABLE || launchType==cGUI_EXECUTABLE_USING_PIF) && (launchPath == undefined || launchPath == "")){core.error ("LaunchApp.qml | launchPath not provided. Need it to continue, EXITING..."); return false;}
        if (launchArgs == undefined || launchArgs == "") launchArgs = "";
        mpgAudio = (!mpgAudio)?false:true;
        core.info("LaunchApp.qml | received values, launchPath: "+launchPath+" launchType: "+launchType+" launchArgs: "+launchArgs+" launchId: "+launchId+" set mpgAudio: "+mpgAudio);
        if (!__appendQueue(launchId,launchPath,launchType,launchArgs,mpgAudio)) {core.info("LaunchApp.qml | launch id is already present in queue. EXITING..."); return false;}
        if(core.simulation){sigAppLaunched(launchId,launchType,launchArgs);pif.transientDataSimulation.__computeGameExitAppTimeout();}
        return __launchManager();
    }

    function forceAppToQuit() {
        core.info("LaunchApp.qml | forceAppToQuit");
        for (var index = (Store.LaunchQueue.length-1); index >=0; --index) if (queueIndex == index){launcherID.close(); index = -1;}
    }

    function getLaunchType(id) {
        core.info("LaunchApp.qml | getLaunchType");var lptr = Store.LaunchArray[id];
        if(lptr)return lptr.launchType; else return 0;
    }

    function pauseApp(){if(!launchInProgress) return false; core.info("LaunchApp.qml | Requesting App to Pause"); launcherID.pause(); return true;}
    function resumeApp(){if(!launchInProgress) return false; core.info("LaunchApp.qml | Requesting App to Resume"); launcherID.resume(); return true;}
    function closeApp(){if(!launchInProgress) return false; core.info("LaunchApp.qml | Requesting App to Close"); launcherID.close(); return true;}
    function killApp(){if(!launchInProgress) return false; core.info("LaunchApp.qml | Requesting Kill App"); launcherID.kill(); return true;}

    function frmwkResetApp() {
        core.info("LaunchApp.qml | frmwkResetApp"); // dont kill shell scripts or non-GUI binary
        for(var index = (Store.LaunchQueue.length-1); index >=0; --index) if(Store.LaunchArray[Store.LaunchQueue[index]]['launchType'] != 1) if (queueIndex==index) launcherID.close();     // Close currently running App.
        if(Store.LaunchQueue.length==0){ launchInProgress=false; queueIndex=-1;}   // If the launched shell script is still busy, we can't set these variables.
        isPostStartFired = false;
    }

    function frmwkInitApp() {
        core.info("LaunchApp.qml | frmwkInitApp");
        runGamePath=pif.getRunGamePath();
        Store.LaunchArray = []; Store.LaunchQueue = [];
        launchInProgress = false; queueIndex = -1; isPostStartFired = false;
    }

    function __appendQueue(launchId,launchPath,launchType,launchArgs,mpgAudio){
        core.info("LaunchApp.qml | __appendQueue");
        if(Store.LaunchArray[launchId] != undefined){core.error ("LaunchApp.qml | launch id is already present. EXITING..."); return false;}
        Store.LaunchArray[launchId] = []; var lptr = Store.LaunchArray[launchId];
        lptr.launchPath = launchPath;
        lptr.launchType = launchType;
        lptr.launchArgs = launchArgs;
        lptr.mpgAudio = mpgAudio;
        Store.LaunchQueue.push(launchId);
        return true;
    }

    function __deleteQueue(launchId){
        core.info("LaunchApp.qml | __deleteQueue, launchId: " + launchId);
        if(launchId==undefined) {core.error("LaunchApp.qml | launchId is not valid."); return false;}
        if(Store.LaunchArray[launchId] != undefined){
            delete Store.LaunchArray[launchId];
            for(var index=0; index<Store.LaunchQueue.length; index++) if(Store.LaunchQueue[index] == launchId) Store.LaunchQueue.splice(index,1);
            queueIndex--;
        }
        return true;
    }

    function __launch(index){
        core.info("LaunchApp.qml | __launch | index: "+index);
        if(Store.LaunchQueue[index] == undefined)	{core.error("LaunchApp.qml | __launch, index is not valid. EXITING..."); return false;}
        var id = Store.LaunchQueue[index]; var lptr = Store.LaunchArray[id];
        launcherID.path = lptr.launchPath;
        launcherID.type = __getLaunchType(lptr.launchType);
        core.debug("LaunchApp.qml | launcherID.type :- "+launcherID.type); core.debug("LaunchApp.qml | launchType: "+lptr.launchType)
        prevInputMode = pif.getNavInput();

        if (lptr.launchType == cNON_GUI_EXECUTABLE) { launcherID.args = lptr.launchArgs;

        } else if (lptr.launchType == cGAME_USING_ICORE || lptr.launchType == cGAME_USING_PIF) {
            pif.screenSaver.stopScreenSaver(); pif.setExternalAppActive(true);
            launcherID.args = lptr.launchArgs; launcherID.path = runGamePath;pif.__enableIFEKeys();

        } else if (lptr.launchType == cPDF_VIEWER) {
            pif.screenSaver.stopScreenSaver(); pif.setExternalAppActive(true); pif.setPointerMode();
            launcherID.args = lptr.launchArgs;
            if(launcherID.path==""){core.info("LaunchApp.qml | __launch | Assigning default pdf viewer path...");launcherID.path="/int_support/usbm/xpdf";}
            core.info("LaunchApp.qml | __launch | pdf viewer path | launchPath: "+launcherID.path);
            pif.__enableIFEKeys();

        } else if (lptr.launchType == cGUI_EXECUTABLE || lptr.launchType == cGUI_EXECUTABLE_USING_PIF) {
            pif.screenSaver.stopScreenSaver(); pif.setExternalAppActive(true);
            launcherID.args = lptr.launchArgs;pif.__enableIFEKeys();

        } else if (lptr.launchType == cTEXT_VIEWER) {
            pif.screenSaver.stopScreenSaver(); pif.setExternalAppActive(true); pif.setPointerMode();
            launcherID.args = lptr.launchArgs; launcherID.path = "";pif.__enableIFEKeys();

        } else core.error("LaunchApp.qml | launch type not supported");

        launchInProgress = true; launchId = id; launchType = lptr.launchType; launchArgs = launcherID.args;
        if(!lptr.mpgAudio){ pif.suspendAudioPlay(); prevVolumeType = pif.getVolumeSource(); pif.setVolumeSource(pif.cPC_VOLUME_SRC);}
        launcherID.launch();
        core.info("LaunchApp.qml | __launch | launchPath: "+launcherID.path+" launchType: "+launcherID.type+" launchArgs: "+launcherID.args);
        return true;
    }

    function __launchManager(){
        core.info("LaunchApp.qml | __launchManager | queueIndex:" + queueIndex);
        if(Store.LaunchQueue.length){
            if(!launchInProgress){
                if(Store.LaunchQueue[queueIndex+1] != undefined){ queueIndex++; __launch(queueIndex);}
                else { core.info ("LaunchApp.qml | Reached end of the queue"); sigQueueComplete(); queueIndex = -1; }
            }else {core.info("LaunchApp.qml | Previous app is still running"); return false;}
        } else { core.info("LaunchApp.qml | The launch queue is empty."); sigQueueComplete(); queueIndex = -1; return false;}
        return true;
    }

    function __launchEnd(launchId){
        core.info("LaunchApp.qml | __launchEnd, launchId: " + launchId);
        if (Store.LaunchArray[launchId]['launchType'] != cNON_GUI_EXECUTABLE) {
            pif.screenSaver.startScreenSaver();
            (prevInputMode == pif.cINPUT_MOUSE)?pif.setPointerMode():pif.setKeyMode();
            prevInputMode=0; pif.setExternalAppActive(false);pif.__disableIFEKeys();
        }
        if(!Store.LaunchArray[launchId]['mpgAudio']) {
            if(pif.lastSelectedVideo.getMediaState()==pif.vod.cMEDIA_STOP){
                pif.setVolumeSource(prevVolumeType);pif.unSuspendAudioPlay();
            }
            prevVolumeType=0;
        }
        launchInProgress = false;
        __deleteQueue(launchId);
    }

    function __getLaunchType(type){
        core.info("LaunchApp.qml | __getLaunchType, type: " + type);
        return (type==cGAME_USING_PIF || type==cGUI_EXECUTABLE_USING_PIF)?"session":(type==cGAME_USING_ICORE)?"core":(type==cPDF_VIEWER)?"pdfviewer":(type==cTEXT_VIEWER)?"textviewer":"generic";
    }

    Component.onCompleted: {
        core.debug("Launch App onCompleted called.");
        Store.LaunchArray = []; Store.LaunchQueue = [];
        launcherID.close();
    }

    Launcher {
        id: launcherID; path: ""; args: ""; type:"";
        onPrestart:core.info("launchApp.qml | onPrestart, action: "+action);
        onPoststart:{core.info("launchApp.qml | onPoststart, action: "+action); isPostStartFired = true; sigAppLaunched(launchId,launchType,launchArgs);}
        onExit:{core.info("launchApp.qml | onExit, action: "+action+" launchId: " + launchId); onLauncherExitSignal();}
        onPause:core.info("launchApp.qml | onPause, action: "+action);
        onResume:core.info("launchApp.qml | onResume, action: "+action);
        onPathChanged:core.info("launchApp.qml | onPathChanged, path: "+path);
        onArgsChanged:core.info("launchApp.qml | onArgsChanged, args: "+args);
        onTypeChanged:core.info("launchApp.qml | onTypeChanged, type: "+type);
    }
}
