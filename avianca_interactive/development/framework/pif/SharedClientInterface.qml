import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

SharedClient{
    id: networkClientInterface
    signal sigDataReceivedFromSeat(string api, variant params);
    property string __currentAspectRatio;

    function __iterateAssocArray(params){var logger=[];for(var key in params){ logger.push(" params."+key+": "+params[key])} return logger;}

    function dataReceived(messageCommand,messageParam,lang){
        var api;var params
        //        if(!messageParam)var params=[];
        core.info("NetworkClientInterface.qml | command :"+messageCommand+", language :"+lang)

        if(messageCommand=="audioCurrentTitle"){
            audioCurrentTitle = messageParam;
            params = {"functionName":"midDataReady","midTitle":audioCurrentTitle};
            sigDataReceivedFromSeat("frameworkUpdateAudioPlayerdata",params)
        } else if(messageCommand=="audioCurrentArtist"){
            audioCurrentArtist = messageParam;
            params = {"functionName":"midDataReady","midArtist":audioCurrentArtist};
            sigDataReceivedFromSeat("frameworkUpdateAudioPlayerdata",params);

        } else if(messageCommand=="audioCurrentDurationSeconds"){
            audioCurrentDurationSeconds = messageParam;
            params = {"functionName":"midDataReady","midDuration":audioCurrentDurationSeconds};
            sigDataReceivedFromSeat("frameworkUpdateAudioPlayerdata",params);

        } else if(messageCommand=="audioElapsedTime"){
            if(mediaPlayer1PlayStatus=="stop")return;
            mediaPlayer1ElapsedTime = messageParam;
            params = {elapsedTime:mediaPlayer1ElapsedTime};
            sigDataReceivedFromSeat("setMediaPositionAck",params);

        } else if(messageCommand=="audioPlayerState"){
            core.info("SharedClientInterface.qml | audioPlayerState | messageParam :"+messageParam)
            if(messageParam=="") return false;
            if(messageParam=="playing"){
                params=[];
                mediaPlayer1PlayStatus = "play";
                sigDataReceivedFromSeat("playAodAck",params);
                //{mid:mediaPlayer1MediaIdentifier,aggregateMid:mediaPlayer1MediaAggregateIdentifier,mediaRepeat:mediaPlayer1Repeat,mediaType:mediaPlayer1MediaType}
            }
            if (messageParam=="paused"){
                mediaPlayer1PlayStatus = "pause";
                params = [];
                sigDataReceivedFromSeat("mediaPauseAck",params);
            }
            if (messageParam=="stopped"){
                mediaPlayer1PlayStatus = "stop";
                params = [];
                sigDataReceivedFromSeat("audioStopAck",params);
            }

        }  else if(messageCommand=="isAlbumMode"){
            isAlbumMode = messageParam;
            params = {isAlbumMode:isAlbumMode};
            sigDataReceivedFromSeat("isAlbumMode",params);

        }  else if(messageCommand=="audioTimeTotal"){
            audioTimeTotal = messageParam;
            params = {"functionName":"midDataReady","midDuration":audioTimeTotal};
            sigDataReceivedFromSeat("frameworkUpdateAudioPlayerdata",params);

        }  else if(messageCommand=="isShuffle"){
            mediaPlayer1Shuffle = messageParam;
            params ={mediaShuffle:mediaPlayer1Shuffle};
            sigDataReceivedFromSeat("mediaShuffleAck",params);

        }  else if(messageCommand=="seatRating"){
            pif.__seatRatingChangeAck(parseInt(messageParam,10));

        }  else if(messageCommand=="resetInteractive"){
            pif.__interactiveResetAck();
            params = [];
            sigDataReceivedFromSeat("interactiveReset",params);

        }  else if(messageCommand=="volume"){
            seatbackVolume = parseInt(messageParam,10);
            params = {seatbackVolume:seatbackVolume};
            sigDataReceivedFromSeat("seatbackVolume",params);

        }  else if(messageCommand=="backlight"){
            serverBacklightEnabled = messageParam;
            params = {backlightEnabled:serverBacklightEnabled};
            sigDataReceivedFromSeat("backlightEnabled",params);

        }  else if(messageCommand=="showKeyboard"){
            params = {text:messageParam.in_text,characterLimit:messageParam.in_character_limit,in_state:messageParam.in_state}
            sigDataReceivedFromSeat("showKeyboard",params);

        }  else if(messageCommand=="closeKeyboard"){
            params =[];
            sigDataReceivedFromSeat("closeKeyboard",params);

        }  else if(messageCommand=="videoOldPausePosition"){
            videoOldPausePosition = parseInt(messageParam,10);
            params = {videoOldPausePosition:videoOldPausePosition}
            sigDataReceivedFromSeat("videoOldPausePosition",params)

        } else if(messageCommand=="videoSubtitleId"){
            mediaPlayer2SubtitleLid = parseInt(messageParam,10);
            params = {subtitleLid:mediaPlayer2SubtitleLid/*,subtitleType:params.subtitleType>>>>> They are not passing it*/};
            sigDataReceivedFromSeat("videoSubtitleAck",params);
            sigDataReceivedFromSeat("frameworkUpdateVideoPlayerdata",{"functionName":"midDataReady","subtitleLid":mediaPlayer2SubtitleLid});

        } else if(messageCommand=="videoSoundtrackId"){
            mediaPlayer2SoundtrackLid = parseInt(messageParam,10);
            params = {soundtrackLid:mediaPlayer2SoundtrackLid/*,soundtrackType:params.soundtrackType>>>>They are not passing it */}
            sigDataReceivedFromSeat("videoSoundtrackAck",params);
            sigDataReceivedFromSeat("frameworkUpdateVideoPlayerdata",{"functionName":"midDataReady","soundtrackLid":mediaPlayer2SoundtrackLid});

        } else if(messageCommand=="videoPlayerState"){
           core.info("SharedClientInterface.qml | videoPlayerState | messageParam :"+messageParam)
            mediaPlayer2PlayStatus = (messageParam=="playing")?"play":(messageParam=="paused")?"pause":(messageParam=="stopped")?"stop":"play"
		            
            
            if(mediaPlayer2PlayStatus=="")return false;
            params =[];
            if(mediaPlayer2PlayStatus=="play"){
                sigDataReceivedFromSeat("videoPlayAck",params);
            }
            if(mediaPlayer2PlayStatus=="pause")sigDataReceivedFromSeat("mediaPauseAck",params);
            if(mediaPlayer2PlayStatus=="stop")sigDataReceivedFromSeat("videoStopAck",params);


        } else if(messageCommand=="videoCurrentTitle"){
            videoCurrentTitle = messageParam;
            params = {"functionName":"midDataReady","midTitle":videoCurrentTitle};
            sigDataReceivedFromSeat("frameworkUpdateVideoPlayerdata",params);

        } else if(messageCommand=="videoCurrentID"){
            videoCurrentID = parseInt(messageParam,10);
            params = {"functionName":"midDataReady","mid":videoCurrentID};
            sigDataReceivedFromSeat("frameworkUpdateVideoPlayerdata",params);

        } else if(messageCommand=="videoAspectRatio"){
            if(messageParam=="")return false;
            if(messageParam=="16:9"){
                __currentAspectRatio="16x9";
                mediaPlayer2AspectRatio="16x9"
            } else if(messageParam=="4:3"){
                __currentAspectRatio="4x3";
                mediaPlayer2AspectRatio="4x3"
            } else if(messageParam=="zoom"){
                if(__currentAspectRatio=="16x9")mediaPlayer2AspectRatio="16x9Stretched"
                else if(__currentAspectRatio=="4x3")mediaPlayer2AspectRatio="4x3Stretched"
            }
            params = {aspectRatio:mediaPlayer2AspectRatio};
            sigDataReceivedFromSeat("aspectRatioAck",params);
            sigDataReceivedFromSeat("frameworkUpdateVideoPlayerdata",{"functionName":"midDataReady","aspectRatio":mediaPlayer2AspectRatio});

        } else if(messageCommand=="videoRatioDisabled"){
            params = {aspectRatio:messageParam};
            sigDataReceivedFromSeat("userDefinedAspectRatio",params);
            //sigDataReceivedFromSeat("frameworkUpdateVideoPlayerdata",{"functionName":"midDataReady","aspectRatio":mediaPlayer2AspectRatio});

        } else if(messageCommand=="videoElapsedTimeSeconds"){
            if(mediaPlayer2PlayStatus=="stop")return;
            mediaPlayer2ElapsedTime = parseInt(messageParam,10);
            params = {elapsedTime:mediaPlayer2ElapsedTime};
            sigDataReceivedFromSeat("setMediaPositionAck",params);

        } else if(messageCommand=="videoTimeTotal"){
            videoTimeTotal = parseInt(messageParam,10);
            params = {"functionName":"midDataReady","duration":videoTimeTotal};
            sigDataReceivedFromSeat("frameworkUpdateVideoPlayerdata",params);

        } else if(messageCommand=="videoPlaybackSpeed"){
            if(parseInt(messageParam,10)==0){
                //Do Nothing
            } else if(parseInt(messageParam,10)==1){
               mediaPlayer2PlayRate=parseInt(messageParam,10);
            } else if (messageParam<0){
                mediaPlayer2PlayRate = parseInt(messageParam,10);
                params = {mediaPlayRate:(parseInt(mediaPlayer2PlayRate,10)*-1)};
                sigDataReceivedFromSeat("mediaRewindAck",params);
            }
            else if(messageParam==2 || messageParam==4 || messageParam==8 || messageParam==16){
                mediaPlayer2PlayRate = parseInt(messageParam,10);
                params ={mediaPlayRate:mediaPlayer2PlayRate};
                sigDataReceivedFromSeat("mediaFastForwardAck",params);
            }
            // From here they will be user defined
        } else if(messageCommand=="seatBeltDuration"){
            seatBeltDuration = parseInt(messageParam,10);
            params = {seatBeltDuration:seatBeltDuration};
            sigDataReceivedFromSeat("seatBeltDuration",params);

        } else if(messageCommand=="accessibility"){
            accessibility = messageParam;
            params = {accessibility:accessibility};
            sigDataReceivedFromSeat("accessibility",params);

        } else if(messageCommand=="isVideoOn"){
            isVideoOn = messageParam;
            params = {isVideoOn:isVideoOn};
            sigDataReceivedFromSeat("isVideoOn",params);

        } else if(messageCommand=="isAudioOn"){
            isAudioOn = messageParam;
            params = {isAudioOn:isAudioOn};
            sigDataReceivedFromSeat("isAudioOn",params);

        } else if(messageCommand=="isGamesOn"){
            isGamesOn = messageParam;
            params = {isGamesOn:isGamesOn};
            sigDataReceivedFromSeat("isGamesOn",params);

        } else if(messageCommand=="isTvOn"){
            isTvOn = messageParam;
            params = {isTvOn:isTvOn};
            sigDataReceivedFromSeat("isTvOn",params);

        } else if(messageCommand=="isSurveyOn"){
            isSurveyOn = messageParam;
            params = {isSurveyOn:isSurveyOn};
            sigDataReceivedFromSeat("isSurveyOn",params);

        }else if(messageCommand=="isMapOn"){
            isMapOn = messageParam;
            params = {isMapOn:isMapOn};
            sigDataReceivedFromSeat("isMapOn",params);

        } else if(messageCommand=="isShoppingOn"){
            isShoppingOn = messageParam
            params = {isShoppingOn:isShoppingOn};
            sigDataReceivedFromSeat("isShoppingOn",params);

        } else if(messageCommand=="isHospitalityOn"){
            isHospitalityOn = messageParam;
            params = {isHospitalityOn:isHospitalityOn};
            sigDataReceivedFromSeat("isHospitalityOn",params);

        } else if(messageCommand=="isSeatChatOn"){
            isSeatChatOn = messageParam;
            params = {isSeatChatOn:isSeatChatOn};
            sigDataReceivedFromSeat("isSeatChatOn",params);

        } else if(messageCommand=="catalogBrowsingValue"){ // Check if we need to hold the values
            params ={"application":messageParam.p_parameter,"enable":messageParam.p_value};
            sigDataReceivedFromSeat("catalogBrowsing",params);

        } else if(messageCommand=="gameLoading"){
            gameLoading = messageParam;
            params = {gameLoading: gameLoading};
            sigDataReceivedFromSeat("gameLoading",params);

        } else if(messageCommand=="languageChange"){
                languageLid = parseInt(lang,10);
                languageIso=dataController.getLanguageIsoByLid(String(languageLid))[0];
                sigDataReceivedFromSeat("changeLanguage",{languageIso:languageIso,languageLid:languageLid})

        } else if(messageCommand=="changePage"){
            languageLid = parseInt(lang,10);
            languageIso=dataController.getLanguageIsoByLid(String(languageLid))[0];
            params = {changePage:messageParam,languageIso:languageIso,languageLid:languageLid};
            sigDataReceivedFromSeat("changePage",params);

        } else if(messageCommand=="inputEvent"){
            params = {inputEvent:messageParam};
            sigDataReceivedFromSeat("inputEvent",params);
        }else if (messageCommand == 'contentUnblocked'){
            var mid = messageParam;
            pif.__midAccessChangeAck('unblocked',parseInt(mid,10));
        }else if (messageCommand == 'contentBlocked'){
            var mid = messageParam;
            pif.__midAccessChangeAck('blocked',parseInt(mid,10));
        }else {
            core.info("NetworkClientInterface.qml | Invalid Command :"+messageCommand);
        }
    }

    function setSeatbackVolume(volumeLevel){ //int volumeLevel
        core.info("NetworkClientInterface.qml | setSeatbackVolume | volumeLevel: " + volumeLevel);
        sendCommand("command","volume",volumeLevel);
    }

    function launchApp(params){ //params = {"launchAppId":<launchAppId>,"mid":<mid>, "filename":<filename>}
        core.info("NetworkClientInterface.qml | launchApp | launchAppId : " + params.launchAppId + "mid :"+params.mid+", filename:"+params.filename);
//        params ={"filename":params.filename,"mid":params.mid}
        sendCommand("command","playGame",params.filename);
    }

    function interactiveStart(languageCode){ //languageIso
        core.info("NetworkClientInterface.qml | interactiveStart | languageIso: " + languageCode);
        sendCommand("command","startInteractive",languageCode);
    }

    function playAodByMid(params){// params =  {"mid":<mid>,"aggregateMid":<aggregateMid>,"mediaRepeat":<mediaRepeat>,"index":<index>}
        core.info("NetworkClientInterface.qml | apiName: playAod |"+__iterateAssocArray(params));
        params = {"index":params.index,albumId:params.aggregateMid};
        sendCommand("command","playSongById",params);
    }

    function rewindPlayingMedia(params){// params = {"mediaType":<mediaType>}
        core.info("NetworkClientInterface.qml | rewindPlayingMedia | mediaType:"+params.mediaType);
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        if(params.mediaType=="aod")sendCommand("command","rewindAudio");
        else if (params.mediaType=="vod")sendCommand("command","rewindVideo",true);
    }

    function forwardPlayingMedia(params){ //params = {"mediaType":"aod or "vod"}
        core.info("NetworkClientInterface.qml | forwardPlayingMedia | mediaType :"+params.mediaType);
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        if(params.mediaType=="aod")sendCommand("command","fastForwardAudio");
        else if (params.mediaType=="vod")sendCommand("command","fastForwardVideo",true); // Check with chirag
    }

     function setPlayingMediaPosition(params){// params = {"elapsedTime":<elapsedTime>,"mediaType":"aod" or "vod"}
        core.info("NetworkClientInterface.qml | setPlayingMediaPositionn | params ="+__iterateAssocArray(params));
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        if(params.mediaType=="aod")sendCommand("command","setPositionAudio",params.elapsedTime);
        else if (params.mediaType=="vod")sendCommand("command","setPositionVideo",params.elapsedTime);
    }

    function pausePlayingMedia(params){ // params ={"mediaType":<mediaType>,"videoLastPostion":<videoLastPosition>}
        core.info("NetworkClientInterface.qml | pausePlayingMedia");
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        if(params.mediaType=="aod")sendCommand("command","pauseAudio");
        else if (params.mediaType=="vod")sendCommand("command","pauseVideo",params.videoLastPostion);
    }

    function resumePlayingMedia(params){// params ={"mediaType":<mediaType>,"videoLastPostion":<videoLastPosition>}
        core.info("NetworkClientInterface.qml | resumePlayingMedia");
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        if(params.mediaType=="aod")sendCommand("command","resumeAudio");
        else if(params.mediaType=="vod")sendCommand("command","resumeVideo",params.videoLastPostion);
    }

    function resumeFromFFRwd(){ // Not supported in F4
        core.info("NetworkClientInterface.qml | resumeFromFFRwd");
        sendCommand("command","resumeFromFFRwd");
    }

    function playPreviousMedia(){
        core.info("NetworkClientInterface.qml | playPreviousMedia");
        sendCommand("command","playPreviousAudio");
    }

    function playNextMedia(){
        core.info("NetworkClientInterface.qml | playNextMedia");
        sendCommand("command","playNextAudio");
    }

    function stopAudio(){
        core.info("NetworkClientInterface.qml | stopAudio");
        sendCommand("command","stopAudio");
    }

     function shuffleTrack(){
        core.info("NetworkClientInterface.qml | shuffleTrack | mediaShuffle : " + mediaPlayer1Shuffle+"Toggling shuffle");
        sendCommand("command","togglePlayOrderAudio");
    }

    function toggleBacklight(){ //Not receiving ACK>>>>Report
        core.info("NetworkClientInterface.qml | toggleBacklight : " + serverBacklightEnabled);
        sendCommand("command","backlight",!serverBacklightEnabled);
    }

    function pageChange(params){ //category_attr_cidtype// { "pageName": nextPageName, "cid": cid, "cidParent": cidParent }
        core.info("NetworkClientInterface.qml | pageChange ");
        params = {"pageName":(params.pageName)?params.pageName:"","cid":(params.cid)?params.cid:"","cidParent":(params.cidParent)?params.cidParent:""}
        sendCommand("command","changePage",params);
    }

    function launchMap(){
        core.info("NetworkClientInterface.qml | launchMap");
        sendCommand("command","showMap");
    }

    function playVodByMid(params){ //params = {"mid":<mid>,"cid":<cid>,"soundtrackLid":<soundtrackLid>,"soundtrackType":<soundtrackType>,"soundtrackTypeIndex":<soundtrackTypeIndex>,"subtitleLid":<subtitleLid>,"subtitleType":<subtitleType>,"subtitleTypeIndex":<subtitleTypeIndex>,"isTrailer":false,"cid":<cid>,"mediaType":<mediaType>,"videoMode":<videoMode>,"subMID":<subMID>}
        core.info("NetworkClientInterface.qml | playVodByMid : ");
        params = {"mid":params.mid,"cid":params.cid,"soundtrackId":(params.soundtrackLid)?params.soundtrackLid:-1,"soundtrackType":params.soundtrackType,"soundtrackTypeIndex":params.soundtrackTypeIndex,"subtitleId":(params.subtitleLid)?params.subtitleLid:-1,"subtitleType":params.subtitleType,"subtitleTypeIndex":params.subtitleTypeIndex,"isTrailer":false,"cid":params.cid,"videoMode":params.videoMode,"subMID":params.subMID}
        core.debug("Send message to seat | apiName: playVod |"+__iterateAssocArray(params));
        sendCommand("command","startMovie",params)
    }

    function playVodAggegerate(params){ //params = {"mid":<mid>,"cid":<cid>,"soundtrackLid":<soundtrackLid>,"soundtrackType":<soundtrackType>,"soundtrackTypeIndex":<soundtrackTypeIndex>,"subtitleLid":<subtitleLid>,"subtitleType":<subtitleType>,"subtitleTypeIndex":<subtitleTypeIndex>,"isTrailer":false,"cid":<cid>,"mediaType":<mediaType>,"videoMode":<videoMode>,"subMid":<subMid>}
        if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        params = {"mid":params.mid,"cid":params.cid,"soundtrackId":(params.soundtrackLid)?params.soundtrackLid:-1,"soundtrackType":params.soundtrackType,"soundtrackTypeIndex":params.soundtrackTypeIndex,"subtitleId":(params.subtitleLid)?params.subtitleLid:-1,"subtitleType":params.subtitleType,"subtitleTypeIndex":params.subtitleTypeIndex,"isTrailer":false,"cid":params.cid,"videoMode":params.videoMode,"subMid":params.subMid}
        core.info("NetworkClientInterface.qml | playVodAggegerate |"+__iterateAssocArray(params));
        sendCommand("command","startTvShow",params)
    }

    function playTrailer(params){ //params = {"mid":<mid>,"cid":<cid>,"soundtrackLid":<soundtrackLid>,"soundtrackType":<soundtrackType>,"soundtrackTypeIndex":<soundtrackTypeIndex>,"subtitleLid":<subtitleLid>,"subtitleType":<subtitleType>,"subtitleTypeIndex":<subtitleTypeIndex>,"cid":<cid>,"videoMode":<videoMode>,"elapsedTime":<elapsedTime>,"subMID":<subMID>}
        core.info("NetworkClientInterface.qml | playTrailer : ");
        params = {"mid":params.mid,"cid":params.cid,"soundtrackId":(params.soundtrackLid)?params.soundtrackLid:-1,"soundtrackType":params.soundtrackType,"soundtrackTypeIndex":params.soundtrackTypeIndex,"subtitleId":(params.subtitleLid)?params.subtitleLid:-1,"subtitleType":params.subtitleType,"subtitleTypeIndex":params.subtitleTypeIndex,"isTrailer":true,"cid":params.cid,"videoMode":params.videoMode,"subMID":params.subMID}
        core.info("Send message to seat | apiName: playTrailer |"+__iterateAssocArray(params));
        sendCommand("command","startMovie",params) //Check this
    }

    function toggleVodStretchState(){
        if(mediaPlayer2AspectRatio=="disable") {
            core.info("NetworkClientInterface.qml | toggleVodStretchState | AspectRatio Disabled")
            return false;
        }
        core.info("NetworkClientInterface.qml | toggleVodStretchState | aspectRatio : " + mediaPlayer2AspectRatio);
        sendCommand("command","toggleRatioVideo");
    }

    function stopVideo(){
        core.info("NetworkClientInterface.qml | stopVideo");
        sendCommand("command","stopVideo");
    }

    function setPlayingSoundtrack(params){ //params={"soundtrackLid:<soundtrackLid>,"soundtrackType":<soundtrackType>,"soundtrackTypeIndex":<soundtrackTypeIndex>}
        core.info("NetworkClientInterface.qml | setPlayingSoundtrack |"+__iterateAssocArray(params));
        params = {"soundtrackId":(params.soundtrackLid)?params.soundtrackLid:-1,"soundtrackType":(params.soundtrackType)?params.soundtrackType:"","soundtrackTypeIndex":(params.soundtrackTypeIndex)?params.soundtrackTypeIndex:-1,"subtitleId":(params.subtitleId)?params.subtitleId:-1,"subtitleType":(params.subtitleType)?params.subtitleType:"","subtitleTypeIndex":(params.subtitleTypeIndex)?params.subtitleTypeIndex:-1,"subMid":(params.subMid)?params.subMid:-1};
        sendCommand("command","videoChangeLanguage",params);
    }

    function closeKeyboard(params){
        core.info("NetworkClientInterface.qml | closeKeyboard");
        sendCommand("command","closeKeyboard",params);
    }

    function sendKeyboardDataToSeat(val){
        core.info("NetworkClientInterface.qml | sendKeyboardMesage");
        sendCommand("command","sendMessageKeyboard",val);
    }

    function inputEvent(in_bSetBacklight){ //bool
        core.info("NetworkClient.qml | inputEvent | in_bSetBacklight:"+in_bSetBacklight);
        sendCommand("command","inputEvent",in_bSetBacklight);
    }

    //getter function

    function getLanguageIso(){return languageIso;}
    function getSeatbackVolume() {return seatbackVolume;}
    function getBacklightStatus() {return serverBacklightEnabled;}
    function getAspectRatio() {return mediaPlayer2AspectRatio;}
    function getMediaPlayRate() {return Math.abs(mediaPlayer2PlayRate);}
    function getCurrentMediaPlayerState(type){
    if(type=="" || type==undefined)type="aod";
    if(type=="aod") return mediaPlayer1PlayStatus
    else if(type=="vod")return mediaPlayer2PlayStatus 
    }
    function getMediaShuffle() {return mediaPlayer1Shuffle;}

}
