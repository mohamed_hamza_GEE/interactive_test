import QtQuick 1.1
import Panasonic.Pif 1.0

Item {
    property int __prevVodState:pif.vod.cMEDIA_STOP;
    property int __prevAodState:pif.aod.cMEDIA_STOP;

    signal sigDataReceived(string api,variant params);

    //Public functions
    function sendApiData(api,params){
        core.info ("Engine4Interface.qml | sendApiData | api: "+api+" params: "+params);
        if (api==undefined || api==''){core.error("API NOT PROVIDED. EXITING..."); return 1;}
        if(!params) params=[];
        var str = '<remote_message><api value="'+api+'" />';
        if(params.length){
            str+= '<params>'
            for(var i=0; i<params.length;i++){str+= '<param value="'+params[i]+'" index="'+i+'" />';}
            str+= '</params></remote_message>';
        }else{str += '</remote_message>';}
        core.info ("Engine4Interface.qml | sendApiData | str: "+str);
        seatRemote.send(str);
    }

    function sendAodElapseTime(){
        var elapseTime = pif.lastSelectedAudio.getMediaElapseTime();
        var mid = pif.lastSelectedAudio.getPlayingMid();
        var midType = "audio";
        sendApiData("set_elapsed_time",[elapseTime,mid,midType]);
    }

    function sendVodElapseTime(){
        var elapseTime = pif.lastSelectedVideo.getMediaElapseTime()
        var cmid = pif.lastSelectedVideo.getPlayingMid();
        var midType = "movie";
        sendApiData("set_elapsed_time",[elapseTime,cmid,midType]);
    }

    function frmwkInitApp(){core.info("Engine4Interface.qml | INITIALISE APP CALLED");}

    function frmwkResetApp(){core.info("Engine4Interface.qml | RESET APP CALLED");}

    //Private functions
    function __dataReceived(message){
        core.info("Engine4Interface.qml | __dataReceived");
        if (!message) return;
        core.verbose("Engine4Interface.qml | STRING RECEIVED: "+message);
        xmlParser.text=message;
        var api = xmlParser.query("/remote_message/api/@value/string()");
        api = api.replace(/\s+$/,"");
        var valStr = xmlParser.query("/remote_message/params/param/@value/string()");
        valStr = valStr.replace(/\s+$/,"");
        var valArr = valStr.split(" ")
        var indStr = xmlParser.query("/remote_message/params/param/@index/string()");
        var indArr = indStr.split(" ");
        var params = new Array();
        for(var i=0; i<=indArr.length; i++){params[parseInt(indArr[i],10)] = valArr[i];}
        __processReceivedData(api, params);
    }

    function __processReceivedData(api, params){
        core.info ("Engine4Interface.qml |  __processReceivedData");
        if (api==undefined || api=='') {core.error("API NOT PROVIDED. EXITING..."); return true;}
        if (params==undefined || params=='' || params.length==0) {var p = new Array();}
        else var p = params;
        core.info ("Engine4Interface.qml | API: "+api+" PARAMS: "+p);

        if(api=="init" || api=="vod_play" || api=="vodclip_play" || api=="tv_play" || api=="aod_play" || api=="aod_shuffle"
                || api=="aod_next" || api=="aod_prev" || api=="game_launch" || api=="help_launch" || api=="ltn_launch"
                || api=="vtravelled_launch" || api=="voyager_launch" || api=="rca_launch" || api=="usb_launch" || api=="ipod_launch"
                || api=="help_launch" || api == "about_tam_launch" || api == "ereader_launch" ||  api == "language"
                || api=="vod_rw" || api == "vod_ff" || api == "vod_pause" ||  api == "vod_resume"
                ||  api == "tam_kids_launch" ||  api == "tam_apps_launch" ||  api == "dutyfree_launch" || api=="vod_stop" || api=="seatchat_launch"|| api=="vkb_launch" || api=="extv_play" || api=="aod_cid") {
            sigDataReceived(api, p);
            if (api=="init") __setWindowDimmableState();
        }else if (api=="vod_jump"){     // jump vod
            if (p.length==0) p[0]=0;
            if(pif.lastSelectedVideo.getMediaState()==pif.vod.cMEDIA_PLAY){pif.lastSelectedVideo.setMediaPosition(p[0]);sendVodElapseTime();}
            sigDataReceived(api, p);
        }else if (api=="vod_subtitle"){     // set vod subtitle
            if (p.length==0) p[0]=0; if (p.length==1) p[1]=0;
            pif.lastSelectedVideo.setPlayingSubtitle(p[0]);
            sigDataReceived(api, p);
        }else if (api=="vod_soundtrack"){   // set vod soundtrack
            if (p.length==0) p[0]=0; if (p.length==1) p[1]=0;
            pif.lastSelectedVideo.setPlayingSoundtrack(p[0]);
            sigDataReceived(api, p);
        }else if (api=="vod_aspect"){       // Change Aspect Ratio
            pif.lastSelectedVideo.toggleStretchState();
        }else if (api=="get_elapsed_time"){     // For syncing media duration sliders
            if(pif.lastSelectedVideo.getMediaState()!=pif.vod.cMEDIA_STOP){sendVodElapseTime();}
            else if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP){sendAodElapseTime();}
        }
        else if (api=="aod_pause"){pif.lastSelectedAudio.pause();}        // Pause current track
        else if (api=="aod_resume"){    // Resume current track
            if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP){pif.lastSelectedAudio.resume();}
            else{
                if(pif.lastSelectedAudio.getPlayingAlbumId())pif.lastSelectedAudio.setPlayingIndex(-1);
                else return true;}
        }
        else if (api=="aod_stop"){pif.lastSelectedAudio.stop();}          // Stop playing current track
        else if (api=="aod_jump"){         // Jump current track
            if(p.length==0) p[0]=0;
            if(pif.lastSelectedAudio.getMediaState()==pif.aod.cMEDIA_PLAY){pif.lastSelectedAudio.setMediaPosition(p[0]);sendAodElapseTime();}
            sigDataReceived(api, p);
        }else if (api=="dim_setvalue"){         // Set window Dimmable value
            if (pif.windowDimmable && p.length!==0) {
                core.info("Engine4Interface.qml | __setWindowDimmableState | Dim value received: "+p[0]);
                pif.windowDimmable.setDimLevel(Math.round(p[0]/25)+1);
            }
        }else if (api=="dim_request"){         // Get window Dimmable value
            __setWindowDimmableState();
            if (pif.windowDimmable && pif.getNoOfDimmableWindows()) sendApiData("dim_setvalue",[pif.windowDimmable.getDimLevelInPercent()]);
        }
    }

    function __setWindowDimmableState () {
        core.info("Engine4Interface.qml | __setWindowDimmableState | dimavailable: "+pif.getNoOfDimmableWindows());
        sendApiData("dimavailable_true",[pif.getNoOfDimmableWindows()?1:0]);
    }

    Xml{id:xmlParser;}

    DeviceManager{id:deviceMngr; type:(core.pc)?"simulation":"ual"; simulation:(core.pc)?true:false;}

    RemoteControl{id:seatRemote; objectName:"remoteMessenger"; mode:RemoteControl.Receiver; enableCustomMessaging:true; onNewMessage:{__dataReceived(message);}}

    AudioChannel{
        id: engine4AudioChannel; audioMixer: pif.audioMixerID; type: "mp3"; volume: 50; muted: false;
        remoteController: seatRemote;
        allowRemoteControl: true;
        objectName: "remoteAudio";
        onHandleEventCalled: {
            core.info ("Engine4Interface.qml | ON AOD VOLUME CHANGED TYPE: mp3, VOLUME LEVEL: "+engine4AudioChannel.volume);
            pif.setVolumeByValue(engine4AudioChannel.volume);
        }
    }

    DisplayController{
        id: displayID; deviceManager: deviceMngr; brightnessLevel: 50; backlightLevel: 1; remoteController: seatRemote;
        allowRemoteControl: true; objectName: "remoteDisplay";
        onBrightnessLevelChanged:{core.info("Engine4Interface.qml | ON BRIGHTNESS LEVEL CHANGED CALLED. BRIGHTNESS LEVEL: "+brightnessLevel);}
    }

    EventListener {
        id: eventListener;deviceManager: deviceMngr; remoteController: seatRemote;allowRemoteControl: true;objectName: "remoteIFEData";
        events: [
            EventData {name:"OPEN_FLIGHT"},
            EventData {name:"ENTERTAINMENT_ON"},
            EventData {name:"X2_PA_STATE"},
            EventData {name:"FLTDATA_TIME_TO_DESTINATION"},
            EventData {name:"FLTDATA_TIME_AT_DESTINATION"},
            EventData {name:"FLTDATA_GROUND_SPEED"},
            EventData {name:"LCD_BACKLIGHT_CHANGE"},
            EventData {name:"CMI_FOR_INTERACTIVE_DISPLAY"},
            EventData {name:"MID_BLOCKING_CHANGE"},
            EventData {name:"SEAT_RATING_CHANGE"},
            EventData {name:"MID_SERVICE_BLOCKING_CHANGE"},
            EventData {name:"SEAT_RATING_SETTING"},
            EventData {name:"VOLUME_CHANGE"},
            EventData {name:"BRIGHTNESS_CHANGE"},
            EventData {name:"CONTRAST_CHANGE"},
            EventData {name:"HUE_CHANGE"},
            EventData {name:"INTERACTIVE_RESTART"},
            EventData {name:"FLTDATA_DESTINATION_BAGGAGE_ID"},
            EventData {name:"FLTDATA_DEPARTURE_BAGGAGE_ID"},
            EventData {name:"DEST_CITY_TEMP"},
            EventData {name:"ROUTE_ID"},
            EventData {name:"CGDUS_NEW_DATA"},
            EventData {name:"MEDIA_DATE"},
            EventData {name:"SATCOM_LINK_STATUS"},
            EventData {name:"GCS_KU_STATE"},
            EventData {name:"DELAY_ENTERTAINMENT_OFF"},
            EventData {name:"FLTDATA_FLIGHT_NUMBER"},
            EventData {name:"INFO_MODE_ON"},
            EventData {name:"GCS_EXCONNECT_SERVICE_ON_SM"},
            EventData {name:"INTERACTIVE_STATE"},
            EventData {name:"IPOD_CONNECT"},
            EventData {name:"IPOD_DISCONNECT"},
            EventData {name:"IPOD_CHARGE_ONLY_CONNECT"},
            EventData {name:"IPOD_CHARGE_ONLY_DISCONNECT"},
            EventData {name:"FLTDATA_TIME_SINCE_TAKEOFF"},
            EventData {name:"FLTDATA_TIME_AT_ORIGIN"},
            EventData {name:"WEIGHT_ON_WHEELS"},
            EventData {name:"FLTDATA_ESTIMATED_ARRIVAL_TIME"},
            EventData {name:"FLTDATA_DESTINATION_ID"},
            EventData {name:"FLTDATA_DEPARTURE_ID"},
            EventData {name:"CAPSENSE_STATE"},
            EventData {name:"EXP_INSEAT_NOTIFICATION_AVAIL"},
            EventData {name:"PSS_ATTENDANT_CALL_CHANGE"},
            EventData {name:"DST_RULE_DESTINATION"},
            EventData {name:"GMT_OFFSET_DESTINATION"},
            EventData {name:"FLTDATA_ALTITUDE"},
            EventData {name:"REMOTE_CONTROLLED_STATE"},
            EventData {name:"FLTDATA_TRUE_AIR_SPEED"},
            EventData {name:"FLTDATA_DISTANCE_TO_DESTINATION"},
            EventData {name:"FLTDATA_OUTSIDE_AIR_TEMP"}
        ]
        onEventReceived: {
            var eventName = event.name;
            core.info ("Engine4Interface.qml | remoteIFEData | Received Event Name: " + eventName);
            core.info ("Engine4Interface.qml | remoteIFEData | Received List of Paramaters: " + event.paramList);// Semicolon separated list of parameter values.
        }
        onEventsChanged: {
            core.error ("Engine4Interface.qml | remoteIFEData | SOME EVENT CHANGED");
        }
    }

    // eventListener1 is a dummy listener added so that Engine4 loads up quickly else it waits for a min. -- Jude
    EventListener {
        id: eventListener1; deviceManager: deviceMngr; remoteController: seatRemote; allowRemoteControl: true; objectName: "remoteSeatData";
        events: [
            EventData {name:"OPEN_FLIGHT"}
        ]
        onEventReceived: {
            var eventName = event.name;
            core.info("Engine4Interface.qml | remoteSeatData | Received Event Name: " + eventName);
            core.info("Engine4Interface.qml | remoteSeatData | Received List of Paramaters: " + event.paramList);// Semicolon separated list of parameter values.
        }
        onEventsChanged: {
            core.error("Engine4Interface.qml | remoteSeatData | SOME EVENT CHANGED");
        }
    }

    Connections{
        target:pif
        onVolumeChanged:{core.info("Engine4Interface.qml | ON VOLUME CHANGED GUI EVENT: VOLUME LEVEL: "+volumeLevel);engine4AudioChannel.volume = volumeLevel;}
    }

    Connections{
        target:pif.lastSelectedVideo;

        onSigMidPlayBegan:{
            //For Trailer and full screen vod same api and parameter need to be passed.
            if(__prevVodState==pif.vod.cMEDIA_STOP || __prevVodState==pif.vod.cMEDIA_PLAY)sendApiData("nowplaying_vod",["play",pif.lastSelectedVideo.getMediaElapseTime(),cmid,pif.lastSelectedVideo.getSoundtrackLid(),pif.lastSelectedVideo.getSubtitleLid(),pif.lastSelectedVideo.getAspectRatio()]);
            else if(__prevVodState==pif.vod.cMEDIA_REWIND||__prevVodState==pif.vod.cMEDIA_FORWARD||__prevVodState==pif.vod.cMEDIA_PAUSE)sendApiData("nowplaying_vod",["resume",pif.lastSelectedVideo.getMediaElapseTime()]);
            sendVodElapseTime();
            __prevVodState=pif.vod.cMEDIA_PLAY;
        }
        onSigStretchState:{
            sendApiData("nowplaying_vod",[stretch?"aspect_ratio_change":"aspect_ratio_fixed",""]);
        }
        onSigAggPlayStopped:{
            __prevVodState=pif.vod.cMEDIA_STOP;
            sendApiData("nowplaying_vod",["stop"]);
            sendApiData("nowplaying_clear");
        }
        onSigAggPlayCompleted:{
            __prevVodState=pif.vod.cMEDIA_STOP;
            sendApiData("nowplaying_vod",["stop"]);
            sendApiData("nowplaying_clear");
        }
        onSigMediaPaused:{
            __prevVodState=pif.vod.cMEDIA_PAUSE;
            sendApiData("nowplaying_vod",["pause",pif.lastSelectedVideo.getMediaElapseTime()]);
        }
        onSigMediaForward:{
            __prevVodState=pif.vod.cMEDIA_FORWARD;
            sendApiData("nowplaying_vod",["ff",speed*100]);
        }
        onSigMediaRewind:{
            __prevVodState=pif.vod.cMEDIA_REWIND;
            sendApiData("nowplaying_vod",["rw",-1*(speed*100)]);
        }
        onSigSoundtrackSet:{
            sendApiData("nowplaying_vod",["soundtrack_change",soundtracklid,2]);
        }
        onSigSubtitleSet:{
            if(subtitlelid > 0)
                sendApiData("nowplaying_vod",["subtitle_change",subtitlelid,pif.lastSelectedVideo.getSubtitleType()]);
            else
                sendApiData("nowplaying_vod",["subtitle_change",-1]);
        }
    }

    Connections{
        target:pif.lastSelectedAudio;

        onSigMidPlayBegan:{
            if(__prevAodState==pif.aod.cMEDIA_STOP ||__prevAodState==pif.aod.cMEDIA_PLAY)sendApiData("nowplaying_aod",["play",pif.lastSelectedAudio.getMediaElapseTime(),pif.lastSelectedAudio.getPlayingMid()]);
            else if(__prevAodState==pif.aod.cMEDIA_REWIND||__prevAodState==pif.aod.cMEDIA_FORWARD||__prevAodState==pif.aod.cMEDIA_PAUSE)sendApiData("nowplaying_aod",["resume",pif.lastSelectedAudio.getMediaElapseTime()]);
            sendAodElapseTime();
            __prevAodState=pif.aod.cMEDIA_PLAY;
        }
        onSigAggPlayStopped:{
            __prevAodState=pif.aod.cMEDIA_STOP;
            sendApiData("nowplaying_aod",["stop"]);
        }
        onSigMediaPaused:{
            __prevAodState=pif.aod.cMEDIA_PAUSE;
            sendApiData("nowplaying_aod",["pause",pif.lastSelectedAudio.getMediaElapseTime()]);
        }
        onSigMediaShuffleState:{
            if(shuffle)sendApiData("nowplaying_aod",["shuffle","on"]);
            else sendApiData("nowplaying_aod",["shuffle","off"]);
        }
    }

    Connections{
        target:pif.launchApp;
        onSigAppLaunched:{var arr=launchArgs.split(" ");sendApiData("nowplaying_game",["start",arr[0]]);}
        onSigAppExit:{var arr=launchArgs.split(" ");sendApiData("nowplaying_game",["stop",arr[0]]);sendApiData("nowplaying_clear");}
    }
    Connections{
        target:(pif.windowDimmable)?pif.windowDimmable:null;
        onSigDimLevelChanged:sendApiData("dim_setvalue",[pif.windowDimmable.getDimLevelInPercent()]);
    }
    Component.onCompleted: __setWindowDimmableState();
}
