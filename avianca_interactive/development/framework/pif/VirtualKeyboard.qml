import QtQuick 1.1
import "../blank.js" as VKBData
import VirtualKeyboard 1.0

Item {
    id : vkb;
    property variant currentLangModel;
    property variant langModel;
    property variant langShiftModel;
    property string __languageISO:"eng";
    property bool capsKeyStatus:false;
    property bool shiftKeyStatus:false;
    property variant shadowIMEProxy:null;

    function getLanguageISO() { return __languageISO;}

    /*
        setLanguage(iso)
        To set the language in vkb.
        PARAMS: iso: language ISO to be used in vkb.
        returns true on success, false on fail.
    */
    function setLanguage(iso){
        core.info ("virtualKeyboard.qml | CHANGE LANGUAGE CALLED LANGUAGE ISO IS " + iso);
        if(VKBData.IsoArr[iso]==undefined) {core.error("virtualKeyboard.qml | LANGUAGE ISO IS NOT PRESENT IN THE VKB ARRAY."); return false;}
        var status = core.pc?0:keyboard_device.changeLanguage(VKBData.IsoArr[iso]);
        if (status) {core.error ("virtualKeyboard.qml | CANNOT CHANGE LANGUAGE IN VKB."); return false;}
        __languageISO=iso;
        return true;
    }
    /*
        sendChar(keyCode)
        PARAMS:
        keyCode : key code of the character.
    */
    function sendChar(keyCode){
        core.info ("VirtualKeyboard.qml | SEND CHAR CALLED keyCode: " + keyCode);
        if(keyCode != 0){
            if(typeof keyCode == "number"){
                var deviceKeyCode = keyCode;
            }else{
                var deviceKeyCode = keyCode.charCodeAt(0);
            }
            keyboard_device.sendChar(deviceKeyCode);
            if(shiftKeyStatus==true) shiftKeyStatus = false;
        }else{
            core.info ("VirtualKeyboard.qml | Keycode is " + keyCode)
        }
    }

    /*
        setKeyboardStatus(status)
        Set the keyboard status like Caps lock ON/OFF and shift key ON/OFF.
        PARAMS:
        status set to 1 for caps lock ON/OFF and
        status set to 2 for shift key ON/OFF.
    */
    function setKeyboardStatus(status){
        core.info ("VirtualKeyboard.qml | SET KEYBOARD STATUS CALLED status: " + status);
        if(status == 1){
            capsKeyStatus = !capsKeyStatus;
        }else if(status == 2){
            shiftKeyStatus = !shiftKeyStatus;
        }
    }

    function sendTextToSeatback(cursorPosition,text){
        core.info ("VirtualKeyboard.qml | SEND TEXT TO SEATBACK CALLED cursorPosition: " + cursorPosition + " text: " + text);
        if(text.length){if(shadowIMEProxy){shadowIMEProxy.commitEntireText(cursorPosition,text); return 0;}}
        else{return 1;}
    }

    /*
      // Deprecated. Use setLanguage(iso).
    selectLanguageVKB(vkbLang,languageModel,languageShiftModel)
    To set the language in vkb.
    PARAMS:
    vkbLang: language ISO to be used in vkb.
    languageModel: The model for that language when the shift key is disabled.
    languageShiftModel: The model for that language when the shift key is enabled.
    */
    function selectLanguageVKB(vkbLang,languageModel,languageShiftModel){
        var status;
        core.info ("virtualKeyboard.qml | CHANGE LANGUAGE CALLED LANGUAGE ISO IS " + vkbLang);
        if(VKBData.IsoArr[vkbLang] != undefined){
            if(core.pc==0)status=keyboard_device.changeLanguage(VKBData.IsoArr[vkbLang]);
            else status=0;
        }else {core.error("virtualKeyboard.qml | LANGUAGE ISO IS NOT PRESENT IN THE VKB ARRAY.");status = 1;}
        if (status==0){
            __languageISO=vkbLang;langModel=languageModel;langShiftModel=languageShiftModel;currentLangModel=langModel;
        }else{core.error ("virtualKeyboard.qml | CANNOT CHANGE LANGUAGE IN VKB.");}
    }

    /*
      // Deprecated. Use sendChar instead.
    sendKey(modifierVal,keyCode)
    PARAMS:
    The following information is present in the EnglishRegularQwertyModel/EnglishShiftQwertyModel present in the components folder.
    modifierVal : modifier from the model.
    keyCode : key_code from the model.
    */
    function sendKey(modifierVal,keyCode){
        core.info("virtualKeyboard.qml | SEND KEY CALLED keyCode: "+keyCode+" modifierVal: "+modifierVal);
        if(modifierVal=="CAPS"){capsKeyStatus=!capsKeyStatus;__shiftKeyEnable(capsKeyStatus);}
        else if(modifierVal=="SHIFT"){shiftKeyStatus=true;__shiftKeyEnable(!capsKeyStatus);}
        else if(modifierVal=="UNI"){keyboard_device.sendExtKey(keyCode);}
        else{
            if(typeof keyCode=="number"){var deviceKeyCode=keyCode;}
            else{var deviceKeyCode=keyCode.charCodeAt(0);}
            if(keyCode!=0){
                keyboard_device.sendKey(deviceKeyCode,modifierVal);
                if(shiftKeyStatus==true){shiftKeyStatus=false;__shiftKeyEnable(capsKeyStatus);}
            }else{core.info("virtualKeyboard.qml | Keycode is "+keyCode);}
        }
    }
    function __shiftKeyEnable(status){
        core.info("virtualKeyboard.qml | SHIFT KEY ENABLE STATUS IS " + status);
        if(status==true)currentLangModel=langShiftModel;else currentLangModel=langModel;
    }
    Component.onCompleted:{
        VKBData.IsoArr = {"eng":0,"ita":0,"fre":0,"ger":0,"spa":0,"zhk":2,"jpn":5,"ara":13,"tha":14};
        try{
            shadowIMEProxy = Qt.createQmlObject('import QtQuick 1.1; import Panasonic.Pif 1.0; ShadowIMEProxy{}',vkb);
        }catch(e){
            core.error('virtualKeyboard.qml | ERROR LOADING ShadowIMEProxy | error : '+e+' ');
        }
    }

    KeyboardDevice{id:keyboard_device}
}
