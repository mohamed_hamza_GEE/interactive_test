import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    id:gcpInterface;

    property variant pifInput;
    property variant __pifRef:pif;
    property variant __mediaData:[];
    property variant __gcpClient:null;
    property int __prevVodState:pif.vod.cMEDIA_STOP;
    property int __prevAodState:pif.aod.cMEDIA_STOP;
    property string extvChannelName:null;

    signal sigDataFromGCP(string api,variant params);

    function __setPifReference(Obj){
        __pifRef = Obj;
    }

    function __initialiseArrData(){
        Store.mirrorArr = [];
        Store.appLaunchArr = [];
        Store.appLaunchArr["ltn"] = "launchLTN";
        Store.appLaunchArr["hospitality"] = "launchHospitality";
        Store.appLaunchArr["shopping"] = "launchShopping";
        Store.appLaunchArr["survey"] = "launchSurvey";
        Store.appLaunchArr["seatchat"] = "launchSeatChat";
        Store.appLaunchArr["connectinggate"] = "launchConnectingGate";
        Store.appLaunchArr["ixplore"] = "launchiXplor";
        Store.appLaunchArr["usbMediaPlayer"] = "launchUsb";
        Store.appLaunchArr["ipod"] = "launchIpod";
    }

    function convertMsgFormat(msg){
        var params = {};
        var apiName = "";
        core.info("GcpInterface.qml | convert msg format called. methodname: " + msg.methodname);
        if(msg.mirror) Store.mirrorArr[msg.methodname] = msg.mirror.id;
        switch(msg.methodname){
        case "hsVolumeUp":{
            var minVol = (pif.getPAState())?0:pif.getMinPAVolume();
            var volumevalue = pif.vip.getVolumeLevel()+ pif.getVolumeStep();
            var volumeTmp = volumevalue<minVol?minVol:volumevalue>100?100:volumevalue;
            pif.vip.setVolumeByValue(volumeTmp);
            break;
        }
        case "hsVolumeDown":{
            var minVol = (pif.getPAState())?0:pif.getMinPAVolume();
            var volumevalue = pif.vip.getVolumeLevel() - pif.getVolumeStep();
            var volumeTmp = volumevalue<minVol?minVol:volumevalue>100?100:volumevalue;
            pif.vip.setVolumeByValue(volumeTmp);
            break;
        }
        case "hsPlayPause":
        case "avodPlayPause":{
            var aodState = pif.lastSelectedAudio.getMediaState();
            var vodState = pif.lastSelectedVideo.getMediaState();
            core.info("GcpInterface.qml | aodState: " + aodState);
            if( aodState == pif.aod.cMEDIA_PLAY){
                core.info("GcpInterface.qml | pif.lastSelectedAudio.pause() called.");
                pif.lastSelectedAudio.pause();
            }else if(aodState == pif.aod.cMEDIA_PAUSE){
                core.info("GcpInterface.qml | pif.lastSelectedAudio.resume() called.");
                pif.lastSelectedAudio.resume();
            }
            if( vodState == pif.vod.cMEDIA_PLAY)pif.lastSelectedVideo.pause();
            else if(vodState != pif.vod.cMEDIA_STOP)pif.lastSelectedVideo.resume();
            break;
        }
        case "hsStop":
        case "avodStop":{
            var aodState = pif.lastSelectedAudio.getMediaState();
            var vodState = pif.lastSelectedVideo.getMediaState();
            if( aodState != pif.aod.cMEDIA_STOP){
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.aod.stop();
            }else if(vodState != pif.vod.cMEDIA_STOP){
                if(pif.lastSelectedVideo.getMediaState()!=pif.vod.cMEDIA_STOP)pif.vod.stop();
            }
            break;
        }
        case "hsFastForward":
        case "avodFastForward":{
            var vodState = pif.lastSelectedVideo.getMediaState();
            if(vodState != pif.vod.cMEDIA_STOP)pif.lastSelectedVideo.forward();
            break;
        }
        case "hsRewind":
        case "avodRewind":{
            var vodState = pif.lastSelectedVideo.getMediaState();
            if(vodState != pif.vod.cMEDIA_STOP)pif.lastSelectedVideo.rewind();
            break;
        }
        case "hsNext":
        case "avodNext":{
            if(pif.lastSelectedAudio.getMediaState() != pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.playNext();
            if(pif.lastSelectedVideo.getMediaState() != pif.vod.cMEDIA_STOP)pif.lastSelectedVideo.playNext();
            break;
        }
        case "hsPrevious":
        case "avodPrevious":{
            if(pif.lastSelectedAudio.getMediaState() != pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.playPrevious();
            if(pif.lastSelectedVideo.getMediaState() != pif.vod.cMEDIA_STOP)pif.lastSelectedVideo.playPrevious();
            break;
        }
        case "navUp":{
            pifInput.postQKeyEvent(6, 0x01000013, 0x01000013, 0, "Up");
            break;
        }
        case "navDown":{
            pifInput.postQKeyEvent(6, 0x01000015, 0x01000015, 0, "Down");
            break;
        }
        case "navLeft":{
            pifInput.postQKeyEvent(6, 0x01000012, 0x01000012, 0, "Left");
            break;
        }
        case "navRight":{
            pifInput.postQKeyEvent(6, 0x01000014, 0x01000014, 0, "Right");
            break;
        }
        case "navSelect":{
            pifInput.postQKeyEvent(6, 0x01000005, 0x01000005, 0, "Enter");
            break;
        }
        case "navBack":{
            pifInput.postQKeyEvent(6, 161, 161, 0, "Back");
            break;
        }
        case "navHome":{
            pifInput.postQKeyEvent(6, 171, 171, 0, "Home");
            break;
        }
        case "mpSetPosition":{
            if(msg.args.position < 0) return;
            if(pif.vod.getMediaState()==pif.vod.cMEDIA_PLAY){pif.vod.setMediaPosition(msg.args.position);}
            else if(pif.aod.getMediaState()==pif.aod.cMEDIA_PLAY){pif.aod.setMediaPosition(msg.args.position);}
            break;
        }
        case "mpGetPosition":{
            var elapseTime = 0;
            if(pif.lastSelectedAudio.getMediaState() != pif.aod.cMEDIA_STOP){elapseTime = pif.lastSelectedAudio.getMediaElapseTime();}
            else if(pif.lastSelectedVideo.getMediaState() != pif.vod.cMEDIA_STOP){elapseTime = pif.lastSelectedVideo.getMediaElapseTime();}
            sendMsgToGcp(createJsonMsg("mpGetPosition",{"position":elapseTime},Store.mirrorArr["mpGetPosition"]));
            break;
        }
        case "mpRepeatItem":{
            if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP){
                if(msg.args.repeat==true && pif.lastSelectedAudio.getMediaRepeat()==0) pif.lastSelectedAudio.setMediaRepeat(2);
                else if(msg.args.repeat==true && pif.lastSelectedAudio.getMediaRepeat()==1) pif.lastSelectedAudio.setMediaRepeat(2);
                else if(msg.args.repeat==true && pif.lastSelectedAudio.getMediaRepeat()==2) return;
                else if(msg.args.repeat==false && pif.lastSelectedAudio.getMediaRepeat()==0) return;
                else if(msg.args.repeat==false && pif.lastSelectedAudio.getMediaRepeat()==1) return;
                else if(msg.args.repeat==false && pif.lastSelectedAudio.getMediaRepeat()==2) pif.lastSelectedAudio.setMediaRepeat(0);
            }
            break;
        }
        case "mpRepeatAll":{
            if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP){
                if(msg.args.repeat==true && pif.lastSelectedAudio.getMediaRepeat()==0) pif.lastSelectedAudio.setMediaRepeat(1);
                else if(msg.args.repeat==true && pif.lastSelectedAudio.getMediaRepeat()==1) return;
                else if(msg.args.repeat==true && pif.lastSelectedAudio.getMediaRepeat()==2) pif.lastSelectedAudio.setMediaRepeat(1);
                else if(msg.args.repeat==false && pif.lastSelectedAudio.getMediaRepeat()==0) return;
                else if(msg.args.repeat==false && pif.lastSelectedAudio.getMediaRepeat()==1) pif.lastSelectedAudio.setMediaRepeat(0);
                else if(msg.args.repeat==false && pif.lastSelectedAudio.getMediaRepeat()==2) return;
            }
            break;
        }
        case "mpShuffle":{
            params = {mediaShuffle:msg.args.shuffle};
            apiName = "mediaShuffle";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "mpSubtitle":{
            if(!msg.args.enabled)return false;
            if(pif.lastSelectedVideo.getMediaState() == pif.vod.cMEDIA_STOP) return false;
            pif.vod.setPlayingSubtitle(msg.args.lid);
            break;
        }
        case "mpSoundtrack":{
            if(!msg.args.enabled)return false;
            if(pif.lastSelectedVideo.getMediaState() == pif.vod.cMEDIA_STOP) return false;
            pif.vod.setPlayingSoundtrack(msg.args.lid);
            break;
        }
        case "avodPlayTrack":{
            var params;
            if(msg.args.mid<=0){ core.error("GcpInterface.qml | MID INVALID."); return false;}
            if(msg.args.mediaType=="audio"){
                params = {mid:msg.args.mid,elapsedTime:msg.args.elapsedTime};
                apiName = "playAod";sigDataFromGCP(apiName,params);}
            else if(msg.args.mediaType=="video"){
                params = {mid:msg.args.mid,elapsedTime:msg.args.elapsedTime};
                apiName = "playVod";sigDataFromGCP(apiName,params);}
            break;
        }
        case "avodPlayAlbum":{
            if(msg.args.mediaType == "audioAggregate"){
                params = {aggregateMid:msg.args.mid,elapsedTime:msg.args.elapsedTime}
                apiName = "playAod";sigDataFromGCP(apiName,params);
            }else if(msg.args.mediaType == "videoAggregate"){
                params = {aggregateMid:msg.args.mid,elapsedTime:msg.args.elapsedTime}
                apiName = "playVod";sigDataFromGCP(apiName,params);
            }
            break;
        }
        case "avodResume":
        case "avodPlay":{
            var aodState = pif.lastSelectedAudio.getMediaState();
            var vodState = pif.lastSelectedVideo.getMediaState();
            if(aodState != pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.resume();
            if(vodState != pif.vod.cMEDIA_STOP)pif.lastSelectedVideo.resume();
            break;
        }
        case "avodPause":{
            var aodState = pif.lastSelectedAudio.getMediaState();
            var vodState = pif.lastSelectedVideo.getMediaState();
            core.info("GcpInterface.qml | aodState: " + aodState);
            if(aodState == pif.aod.cMEDIA_PLAY){
                core.info("GcpInterface.qml | pif.lastSelectedAudio.pause() called ");
                pif.lastSelectedAudio.pause();
            }
            if(vodState == pif.vod.cMEDIA_PLAY)pif.lastSelectedVideo.pause();
            break;
        }
        case "addToPlaylist":{
            params = {startIndex:msg.args.startIndex,size:msg.args.size,type:msg.args.type,mids:msg.args.mids,mirrorId:msg.mirror.id};
            apiName = "addToPlaylist";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "removeFromPlaylist":{
            params = {type:msg.args.type,mids:msg.args.mids,mirrorId:msg.mirror.id};
            apiName = "removeFromPlaylist";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "playlistPlayTrack":{
            params = {index:msg.args.index,elapsedTime:msg.args.elapsedTime,type:msg.args.type};
            apiName = "playlistPlayTrack";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "getPlaylist":{
            params = {startIndex:msg.args.startIndex,endIndex:msg.args.endIndex,blockSize:msg.args.blockSize,type:msg.args.type,mirrorId:msg.mirror.id};
            apiName = "getPlaylist";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "avodPlayLSV":{
            params = {};
            apiName = "avodPlayLSV";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "avodPlayLSA":{
            params = {};
            apiName = "avodPlayLSA";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "auxPlay":{
            pif.vip.setMonitorSource(msg.args.source);
            break;
        }
        case "appLaunch":{
            apiName = Store.appLaunchArr[msg.args.app];
            sigDataFromGCP(apiName,params);
            break;
        }
        case "intLanguage":{
            params = {languageIso:msg.args.iso};
            apiName = "changeLanguage";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "dispBrightness":{
            var brightnessValue=parseInt(msg.args.level,10);
            var brightnessTmp=brightnessValue<0?0:brightnessValue>100?100:brightnessValue;
            pif.vip.setBrightnessByValue(brightnessTmp);
            break;
        }
        case "dispBacklight":{
            if(msg.args.level==1)pif.vip.setBacklightOn();
            else if(msg.args.level==0)pif.vip.setBacklightOff();
            break;
        }
        case "broadcastPlay":{
            if(typeof msg.args.bid=="string"){
                extvChannelName = msg.args.bid;
                core.dataController.extv.getExtvList(getExtvModel);
            }else{
                params = {"channelID":msg.args.bid};
                apiName = "playBroadcast";
                sigDataFromGCP(apiName,params);
            }
            break;
        }
        case "hsChannelUp":{
            apiName = "hsChannelUp";
            sigDataFromGCP(apiName,params);
            break;
        }
        case "hsChannelDown":{
            apiName = "hsChannelDown";
            sigDataFromGCP(apiName,params);
            break;
        }
        }
    }

    function getExtvModel(eModel,status,type){
        var index;
        index = core.coreHelper.searchEntryInModel(eModel,"callSign",extvChannelName);
        sigDataFromGCP("playExtv",{channelID:index});
    }

    function createJsonMsg(methodName,params,reflectionId){
        core.info("GcpInterface.qml | createJsonMsg() called. methodName: " + methodName);
        reflectionId=(reflectionId)?reflectionId:(Store.mirrorArr && Store.mirrorArr[methodName])?Store.mirrorArr[methodName]:null;
        var msg = { type : 'jsonwsp/response',
            version: '1.0',
            method: methodName,
            result: params,
            reflection: { 'id' : reflectionId }
        }
        return JSON.stringify(msg).replace(/["]/g,"'");
    }

    function sendMsgToGcp(msg){
        core.info("GcpInterface.qml | sendMsgToGcp() called.");
        //if(__gcpClient)__gcpClient.sendMessage("JSON", msg);
        //else return false;
        if(pif.__sharedModelServer){
            core.info("GcpInterface.qml | pif.__sharedModelServer is valid.");
            pif.__sharedModelServer.sendMessage(msg,"GCP");
        }else{
            core.info("GcpInterface.qml | pif.__sharedModelServer is invalid.");
        }
    }

    Connections{
        target: (pif.__sharedModelServer)?pif.__sharedModelServer:null;
        onSigGcpClientConnected:{
            __gcpClient = client;
        }
        onSigGcpDataReceived:{
            core.info("GcpInterface.qml | onSigGcpDataReceived signal from shared model server");
            convertMsgFormat(msgObj);
        }
    }

    Connections{
        target:pif.lastSelectedVideo;
        onSigMidPlayBegan: {
            var vodInfo;
            core.info("GcpInterface.qml | target:pif.lastSelectedVideo sigMidPlayBegan signal received");
            vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();

            if(__prevVodState==pif.vod.cMEDIA_STOP || __prevVodState==pif.vod.cMEDIA_PLAY){
                sendMsgToGcp(createJsonMsg("avodPlayTrack",{"mid": vodInfo.amid,"elapsedTime": vodInfo.elapseTime,"mediaType": vodInfo.mediaType,"playlistIndex":0}));
                sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": 100}));
            }
            else if(__prevVodState==pif.vod.cMEDIA_REWIND||__prevVodState==pif.vod.cMEDIA_FORWARD||__prevVodState==pif.vod.cMEDIA_PAUSE){
                sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": 100}));
            }
            __prevVodState = pif.vod.cMEDIA_PLAY;
        }
        onSigSoundtrackSet:{
            var vodInfo;var enabled = false;
            core.info("GcpInterface.qml | target:pif.lastSelectedVideo sigSoundtrackSet signal received");
            vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();
            if(soundtracklid > 0) enabled = true;
            sendMsgToGcp(createJsonMsg("mpSoundtrack",{"lid": soundtracklid,"type": vodInfo.sndtrkType,"enabled": enabled}));
        }
        onSigSubtitleSet:{
            var vodInfo;var enabled = false;
            core.info("GcpInterface.qml | target:pif.lastSelectedVideo sigSubtitleSet signal received");
            vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();
            if(subtitlelid > 0) enabled = true;
            sendMsgToGcp(createJsonMsg("mpSubtitle",{"lid": subtitlelid,"type": vodInfo.subtitleType,"enabled": enabled}));
        }
        onSigMediaElapsed:{
            core.info("GcpInterface.qml | target:pif.lastSelectedVideo sigMediaElapsed signal received");
            sendMsgToGcp(createJsonMsg("mpSetPosition",{"position":elapseTime}));
        }
        onSigMediaPaused:{
            core.info("GcpInterface.qml | target:pif.lastSelectedVideo sigMediaPaused signal received");
            __prevVodState=pif.vod.cMEDIA_PAUSE;
            sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": 0}));
        }
        onSigAggPlayStopped:{
            core.info("GcpInterface.qml | target:pif.lastSelectedVideo sigAggPlayStopped signal received");
            __prevVodState=pif.vod.cMEDIA_STOP;
            sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": 0}));
        }
        onSigMediaForward:{
            core.info("GcpInterface.qml | target:pif.lastSelectedVideo sigMediaForward signal received");
            __prevVodState=pif.vod.cMEDIA_FORWARD;
            sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": speed*100}));
        }
        onSigMediaRewind:{
            core.info("GcpInterface.qml | target:pif.lastSelectedVideo sigMediaRewind signal received");
            __prevVodState=pif.vod.cMEDIA_REWIND;
            sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": -1*(speed*100)}));
        }
    }

    Connections{
        target:pif.lastSelectedAudio;
        onSigMidPlayBegan:{
            core.info("GcpInterface.qml | target:pif.lastSelectedAudio received sigMidPlayBegan");
            if(__prevAodState==pif.aod.cMEDIA_STOP || __prevAodState==pif.aod.cMEDIA_PLAY){
                var aodInfo=pif.lastSelectedAudio.getAllMediaPlayInfo();
                sendMsgToGcp(createJsonMsg("avodPlayTrack",{"mid": aodInfo.amid,"elapsedTime": aodInfo.elapseTime,"mediaType": aodInfo.mediaType,"playlistIndex":0}));
                sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": 100}));
            }
            else if(__prevAodState==pif.aod.cMEDIA_REWIND||__prevAodState==pif.aod.cMEDIA_FORWARD||__prevAodState==pif.aod.cMEDIA_PAUSE){
                sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": 100}));
            }
            __prevAodState=pif.aod.cMEDIA_PLAY;
        }
        onSigMediaElapsed:{
            core.info("GcpInterface.qml | target:pif.lastSelectedAudio received sigMediaElapsed");
            sendMsgToGcp(createJsonMsg("mpSetPosition",{"position":elapseTime}));
        }
        onSigAggPlayStopped:{
            core.info("GcpInterface.qml | target:pif.lastSelectedAudio received sigAggPlayStopped");
            __prevAodState=pif.aod.cMEDIA_STOP;
            sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": 0}));
        }
        onSigMediaPaused:{
            core.info("GcpInterface.qml | target:pif.lastSelectedAudio received sigMediaPaused");
            __prevAodState=pif.aod.cMEDIA_PAUSE;
            sendMsgToGcp(createJsonMsg("mpPlayRate",{"playRate": 0}));
        }
        onSigMediaShuffleState:{
            core.info("GcpInterface.qml | target:pif.lastSelectedAudio received sigMediaShuffleState");
            sendMsgToGcp(createJsonMsg("mpShuffle",{"shuffle":shuffle}));
        }
        onSigMediaRepeatState:{
            core.info("GcpInterface.qml | target:pif.lastSelectedAudio received sigMediaRepeatState");
            var repeat = pif.lastSelectedAudio.getMediaRepeat();
            if(repeat==0){core.info("repeat:" + repeat);}
            else if(repeat==1){sendMsgToGcp(createJsonMsg("mpRepeatAll",{"repeat":true}));}
            else{sendMsgToGcp(createJsonMsg("mpRepeatItem",{"repeat":true}));}
        }
    }

    Connections{
        target:__pifRef
        onBrightnessChanged:{
            sendMsgToGcp(createJsonMsg("dispBrightness",{"level":brightnessLevel}));
        }
        onBacklightStateChanged:{
            sendMsgToGcp(createJsonMsg("dispBacklight",{"level":backlightState}));
        }
    }

    SimpleModel{id:extvModel}

    Component.onCompleted: {
        core.info("GcpInterface.qml | GcpInterface | Component loaded ");
        if (core.pc || pif.getSimulationData().Gcp) Qt.createQmlObject('import QtQuick 1.1; import "../testpanel"; GcpSIM{}',pif);
        __initialiseArrData();
    }
}
