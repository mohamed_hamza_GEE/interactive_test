import QtQuick 1.1
import Panasonic.Pif 1.0

AudioMediaPlayer{
    property variant deviceMngr;
    property bool __suspend;
    property int __prevMediaState:0;
    property string mediaSource:"broadcastAudio";

    onSigAggPlayBegan:{if(pif.getLSAmediaSource()=="broadcastAudio") pif.setVolumeSource(pif.cAOD_VOLUME_SRC);}

    Connections{target:pif; onSigFallbackMode: if(fallbackMode) frmwkResetApp();}

    function play (amid, params) {
        core.info("BroadcastAudio.qml | play called | amid: "+amid+" params: "+params);
        params = __validateParams(params,parseInt(amid,10)); if (!params) return false;
        return __playBroadcastAudio(params);
    }

    function playByModel (model, params) {
        core.info("BroadcastAudio.qml | playByModel called | params: "+params);
        if (!model || !model.count) {core.error ("BroadcastAudio.qml | Empty Model or Model not provided. EXITING..."); return false;}
        params.playIndex = (params.playIndex==undefined || params.playIndex>=model.count)?undefined:params.playIndex;
        params = __validateParams(params,(params.amid?params.amid:parseInt(model.at(0).aggregate_parentmid,10))); if (!params) return false;
        return __playBroadcastAudio(params,model);
    }

    function stop() {return __mediaStop();}

    function suspendPlaying() {
        if (__suspend||__mediaState==cMEDIA_STOP) return false;
        __prevMediaState = __mediaState;
        __suspend = true;
        stop();
        return true;
    }

    function unSuspendPlaying() {
        if (!__suspend) return false;
        __suspend = false;
        setPlayingIndex(-1);
        __prevMediaState=0;
        return true;
    }
    function playNext () { return __mediaNext();}
    function playPrevious () { return __mediaPrevious();}
    function pause () {return true;}
    function resume () {return true;}
    function forward () {return true;}
    function rewind () {return true;}
    function restart() { return __mediaRestart();}

    function frmwkInitApp(){}
    function frmwkResetApp(){clearMediaPlayer();}
    function clearMediaPlayer(){__clearMediaModel();__clearSuspend();return true}


    // Private functions
    function __clearSuspend(){__suspend=false; __prevMediaState=0; return true;}

    function __playBroadcastAudio(params,model) {   // model optional
        pif.__setLastSelectedAudio(pif.broadcastAudio);
        var s; for (var i in params) s+=i+" = "+params[i]+", "; core.debug(s);
        if (model) __setMediaModel(model,params.cid,params.amid,params.mediaType);
        else __setMediaModelByMid(params.amid-1,params.mediaType,params.duration,params.cid);
        return __mediaPlay(params);
    }

    function __validateParams (p,amid) {
        p.amid = (String(parseInt(amid,10))!="NaN")?amid:1;
        var ret=__getMidArrReference(); var ptr=(ret[p.amid])?ret[p.amid]:[];
        p.elapseTimeFormat = (String(parseInt(p.elapseTimeFormat,10))!="NaN")?parseInt(p.elapseTimeFormat,10):(ptr.elapseTimeFormat?ptr.elapseTimeFormat:1);
        p.cid = (String(parseInt(p.cid,10))!="NaN")?parseInt(p.cid,10):(ptr.cid?ptr.cid:0);
        p.playIndex=(String(parseInt(p.playIndex,10))!="NaN")?p.playIndex:(ptr.playIndex?ptr.playIndex:0);
        p.mediaType = (p.mediaType)?p.mediaType:(ptr.mediaType?ptr.mediaType:"broadcastAudio");
        p.repeatType=(String(parseInt(p.repeatType,10))!="NaN")?p.repeatType:(ptr.repeatType?ptr.repeatType:0);
        p.playType=p.playType?p.playType:(ptr.playType?ptr.playType:0);
        p.shuffle=(!p.shuffle)?false:true;
        p.elapseTime=(p.elapseTime)?p.elapseTime:0;
        p.duration = (p.duration==undefined)?"00:00:00":p.duration;
        if(p.rating==undefined)core.error("BroadcastAudio.qml | Missing rating. Setting it to 254.");
        p.rating=(String(parseInt(p.rating,10))!="NaN")?parseInt(p.rating,10):(ptr.rating?ptr.rating:254);
        p.vbMode=(p.vbMode==undefined)?(ptr.vbMode?ptr.vbMode:false):p.vbMode;
        return p;
    }

    Component.onCompleted: {
        __playerType = "Broadcast Audio Media Player";
        __timerInterval = pif.__getAudioPlayTimerInterval();        // BG audio begins before cStageInitializeStates
        __skipPreviousTimeout = pif.__getSkipPreviousTimeout();
    }
}
