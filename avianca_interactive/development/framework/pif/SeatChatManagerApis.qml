import QtQuick 1.1
import Panasonic.Pif 1.0

Item{
    id:seatChatManagerApis

    function clearServerData(seatNumber){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(seatNumber);
            var params = {"apiName":"seatChatManager.clearServerData","apiParameters":tempArray}
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params)
        } else {
            seatChatManager.clearServerData(seatNumber)
        }
    }

    function loginUserSeatNumber(seatNumber){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(seatNumber);
            var params = {"apiName":"seatChatManager.loginUserSeatNumber","apiParameters":tempArray}
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params)
        } else {
            seatChatManager.loginUserSeatNumber(seatNumber)
        }
    }

    function requestPublicSessions(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.requestPublicSessions","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params)
        } else {
            seatChatManager.requestPublicSessions();
        }
    }

    function requestAllUserList(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.requestAllUserList","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params)
        } else {
            seatChatManager.requestAllUserList();
        }
    }

    function requestUserProfile(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.requestUserProfile","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params)
        } else {
            seatChatManager.requestUserProfile();
        }
    }

    function requestUserBuddies(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.requestUserBuddies","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params)
        } else {
            seatChatManager.requestUserBuddies();
        }
    }

    function requestUserPrivateSessions(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.requestUserPrivateSessions","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params)
        } else {
            seatChatManager.requestUserPrivateSessions();
        }
    }

    function resetUnreadMsgCount(serverSessionId){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId)
            var params = {"apiName":"seatChatManager.resetUnreadMsgCount","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params)
        } else {
            seatChatManager.resetUnreadMsgCount(serverSessionId);
        }
    }

    function sendMessageToSession(serverSessionId,sessionId,message){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            tempArray.push(message);
            var params = {"apiName":"seatChatManager.sendMessageToSession","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.sendMessageToSession(serverSessionId,sessionId,message);
        }
    }

    function sendConciergeMsg(message){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(message);
            var params = {"apiName":"seatChatManager.sendConciergeMsg","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.sendConciergeMsg(message);
        }
    }

    function joinPublicSession(serverSessionId,sessionId){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            var params = {"apiName":"seatChatManager.joinPublicSession","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.joinPublicSession(serverSessionId,sessionId);
        }
    }

    function leavePublicSession(serverSessionId,sessionId){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            var params = {"apiName":"seatChatManager.leavePublicSession","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.leavePublicSession(serverSessionId,sessionId);
        }
    }

    function leavePrivateSession(serverSessionId,sessionId){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            var params = {"apiName":"seatChatManager.leavePrivateSession","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.leavePrivateSession(serverSessionId,sessionId);
        }
    }

    function setUserAlias(seatName){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(seatName);
            var params = {"apiName":"seatChatManager.setUserAlias","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.setUserAlias(seatName);
        }
    }

    function setUserAvatar(seatAvatar){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(seatAvatar);
            var params = {"apiName":"seatChatManager.setUserAvatar","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.setUserAvatar(seatAvatar);
        }

    }

    function setUserStatus(status){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(status);
            var params = {"apiName":"seatChatManager.setUserStatus","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.setUserStatus(status);
        }
    }

    function requestSessionHistory(serverSessionId,sessionId){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            var params = {"apiName":"seatChatManager.requestSessionHistory","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.requestSessionHistory(serverSessionId,sessionId);
        }
    }

    function createPrivateSession(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.createPrivateSession","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
            return -1
        } else {
            return seatChatManager.createPrivateSession();
        }
    }

    function sendPrivateInvitation(serverSessionId,sessionId,seatNum,message){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            tempArray.push(seatNum);
            tempArray.push(message);
            var params = {"apiName":"seatChatManager.sendPrivateInvitation","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.sendPrivateInvitation(serverSessionId,sessionId,seatNum,message);
        }
    }

    function acceptPrivateInvitation(serverSessionId,sessionId,seatNum){ // Needs return Value As true or false
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            tempArray.push(seatNum);
            var params = {"apiName":"seatChatManager.acceptPrivateInvitation","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.acceptPrivateInvitation(serverSessionId,sessionId,seatNum);
        }
    }

    function declinePrivateInvitation(serverSessionId,sessionId,seatNum){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            tempArray.push(seatNum);
            var params = {"apiName":"seatChatManager.declinePrivateInvitation","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.declinePrivateInvitation(serverSessionId,sessionId,seatNum);
        }
    }

    function blockUser(seatNumber){ //May Need to wait for many seats or shared Model que will handle it
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(seatNumber);
            var params = {"apiName":"seatChatManager.blockUser","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.blockUser(seatNumber);
        }
    }

    function unblockUser(seatNumber){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(seatNumber);
            var params = {"apiName":"seatChatManager.unblockUser","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.unblockUser(seatNumber);
        }
    }

    function blockInvites(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.blockInvites","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.blockInvites();
        }
    }

    function unblockInvites(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.unblockInvites","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.unblockInvites();
        }
    }

    function resetUser(){
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.resetUser","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.resetUser();
        }
    }

    function closePrivateSession(serverSessionId,message){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(message)
            var params = {"apiName":"seatChatManager.closePrivateSession","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.closePrivateSession(serverSessionId,message);
        }
    }

    function removeUserFromPrivateSession(serverSessionId,seatNumber){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(seatNumber)
            var params = {"apiName":"seatChatManager.removeUserFromPrivateSession","apiParameters":tempArray};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatRequest",params);
        } else {
            seatChatManager.removeUserFromPrivateSession(serverSessionId,seatNumber);
        }
    }

    function getHashValue(hashTable,key){
        if(__seatChatVkarma){
            var  ret = (hashTable[key]!=undefined)?hashTable[key]:""
            return ret
        } else {
            return seatChatManager.getHashValue(hashTable,key);
        }
    }

    function getTotalSessionCount(callback,extraParams){ // Return value Expected
        if(__seatChatVkarma){
            if(extraParams!=undefined){
                var tempArray = [];
                var params = {"apiName":"seatChatManager.getTotalSessionCount","apiParameters":tempArray,"callbackFunction":callback,"extraParams":extraParams};
                pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatExtraParams",params);  // need to decide api name
                return -1;
            }else{
                var tempArray = [];
                var params = {"apiName":"seatChatManager.getTotalSessionCount","apiParameters":tempArray,"callbackFunction":callback};
                pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params);  // need to decide api name
                return -1;
            }
        } else {
            return seatChatManager.getTotalSessionCount();
        }
    }

    function getPrivateSessionCount(callback,extraParams){
        if(__seatChatVkarma){
            if(extraParams!=undefined){
                var tempArray = [];
                var params = {"apiName":"seatChatManager.getPrivateSessionCount","apiParameters":tempArray,"callbackFunction":callback,"extraParams":extraParams};
                pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatExtraParams",params);  // need to decide api name
                return -1;
            }else{
                var tempArray = [];
                var params = {"apiName":"seatChatManager.getPrivateSessionCount","apiParameters":tempArray,"callbackFunction":callback};
                pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params);  // need to decide api name
                return -1;
            }
        } else {
            return seatChatManager.getPrivateSessionCount();
        }
    }


    function getPublicSessionList(from,to,callback,extraParams){ //Return value expected
        if(__seatChatVkarma){
            if(extraParams!=undefined){
                var tempArray = [];
                tempArray.push(from);
                tempArray.push(to);
                var params = {"apiName":"seatChatManager.getPublicSessionList","apiParameters":tempArray,"callbackFunction":callback,"extraParams":extraParams};
                pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatExtraParams",params);  // need to decide api name
                return -1;
            } else {
                var tempArray = [];
                tempArray.push(from);
                tempArray.push(to);
                var params = {"apiName":"seatChatManager.getPublicSessionList","apiParameters":tempArray,"callbackFunction":callback};
                pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params);  // need to decide api name
                return -1;
            }
        } else {
            return seatChatManager.getPublicSessionList(from,to);
        }
    }

    function getPrivateSessionList(from,to,callback){ // Return value Expected
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(from);
            tempArray.push(to);
            var params = {"apiName":"seatChatManager.getPrivateSessionList","apiParameters":tempArray,"callbackFunction":callback};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params);  // need to decide api name
            return -1;
        } else {
            return seatChatManager.getPrivateSessionList(from,to);
        }
    }

    function getSessionHistory(serverSessionId,sessionId,callback){ // Return value Expected
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            var params = {"apiName":"seatChatManager.getSessionHistory","apiParameters":tempArray,"callbackFunction":callback};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params); // APi need to be decided
            return -1;
        } else {
            return seatChatManager.getSessionHistory(serverSessionId,sessionId);
        }
    }

    function getSessionParticipantCount(serverSessionId,sessionId,callback,extraParams){
        if(__seatChatVkarma){
            core.info("getSessionParticipantCount component |serverSessionId: "+serverSessionId+"extraParams:"+extraParams)
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            var params = {"apiName":"seatChatManager.getSessionParticipantCount","apiParameters":tempArray,"callbackFunction":callback,"extraParams":extraParams};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatExtraParams",params);
            return -1;
        } else {
            return seatChatManager.getSessionParticipantCount(serverSessionId,sessionId);
        }
    }

    function getSessionParticipantList(serverSessionId,sessionId,from,to,callback,extraParams){
        if(__seatChatVkarma){
            console.log("getSessionParticipantList COmponent | serverSessionId:"+serverSessionId+",from:"+from+", to:"+to+", extraParams:"+extraParams)
            var tempArray = [];
            tempArray.push(serverSessionId);
            tempArray.push(sessionId);
            tempArray.push(from);
            tempArray.push(to);
            var params = {"apiName":"seatChatManager.getSessionParticipantList","apiParameters":tempArray,"callbackFunction":callback,"extraParams":extraParams};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatExtraParams",params);
            return -1;
        } else {
            return seatChatManager.getSessionParticipantList(serverSessionId,sessionId,from,to);
        }
    }

    function getBlockedUserCount(callback){  //required Callback
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.getBlockedUserCount","apiParameters":tempArray,"callbackFunction":callback};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params);
            return -1;
        } else {
            return seatChatManager.getBlockedUserCount();
        }
    }

    function getBlockedUserList(from,to,callback){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(from);
            tempArray.push(to);
            var params = {"apiName":"seatChatManager.getBlockedUserList","apiParameters":tempArray,"callbackFunction":callback};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params);
            return -1;
        } else {
            return seatChatManager.getBlockedUserList(from,to);
        }
    }

    function getAllChatUserCount(callback){ //Return Value Expected
        if(__seatChatVkarma){
            var tempArray = [];
            var params = {"apiName":"seatChatManager.getAllChatUserCount","apiParameters":tempArray,"callbackFunction":callback};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params);
            return -1;
        } else {
            return seatChatManager.getAllChatUserCount();
        }
    }

    function getAllChatUserListWithRange(from,to,callback){
        if(__seatChatVkarma){
            var tempArray = [];
            tempArray.push(from);
            tempArray.push(to)
            var params = {"apiName":"seatChatManager.getAllChatUserListWithRange","apiParameters":tempArray,"callbackFunction":callback};
            pif.vkarmaToSeat.sendApiData("seatChatVkarmaToSeatCallBackRequest",params);
            return -1;
        } else {
            return seatChatManager.getAllChatUserListWithRange(from,to);
        }
    }
}
