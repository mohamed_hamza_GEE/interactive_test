import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

MessageSharing{
    id:emailApp

    property string nickname:"";
    signal inboxMessageReceived(); //to get message do getInbox().get(0);

    function getInbox(){return inbox}
    function getSentbox(){return sentBox}
    function getAllUserModel(){return allUserModel}

    function setNickname(name){
        core.info("EmailMessageSharing.qml | setNickname | name: "+name);
        nickname=name;
        core.debug("EmailMessageSharing.qml | setNickname | allUserModel.count: "+allUserModel.count);
        for(var i=0;i<allUserModel.count;i++){
            core.debug("EmailMessageSharing.qml | setNickname | i: "+i+" | seatNo: "+allUserModel.getValue(i,"seatNo"));
            if(allUserModel.getValue(i,"seatNo")){addToReceiverInvitationQueue(allUserModel.getValue(i,"seatNo"),"nickname##"+nickname,"","email","");}
         }
        __processIntivationRequest();
    }

    function enableEmailMessageSharing(){
        core.debug("EmailMessageSharing.qml | enableEmailMessageSharing | enable: "+__isEnable);
        enable();
        for(var i=0;i<allUserModel.count;i++){
            core.debug("EmailMessageSharing.qml | enableEmailMessageSharing | i: "+i+" | seatNo: "+allUserModel.getValue(i,"seatNo"));
            addToReceiverInvitationQueue(allUserModel.getValue(i,"seatNo"),"available","","email","");
         }
        __processIntivationRequest();
    }
    function disableEmailMessageSharing(){
        disable();
        core.debug("EmailMessageSharing.qml | disableEmailMessageSharing | enable: "+__isEnable);
        //inbox.clear();sentBox.clear();
        for(var i=0;i<allUserModel.count;i++){
            core.debug("EmailMessageSharing.qml | disableEmailMessageSharing | i: "+i+" | seatNo: "+allUserModel.getValue(i,"seatNo"));
            addToReceiverInvitationQueue(allUserModel.getValue(i,"seatNo"),"optout","","email","");
         }
        __processIntivationRequest();
    }

    function acceptMessage(index){
        var tmp=inbox.at(index);
        core.info("EmailMessageSharing.qml | acceptMessage |  index: "+index+" | seatNo: "+tmp.seatNo);
        //if(isSeatBlocked(tmp.seatNo)){return true}
        //addToReceiverInvitationQueue(tmp.seatNo,"accept","","email","");
        //__processIntivationRequest();
    }

    function declineMessage(index){
        var tmp=inbox.at(index);
        tmp.seatNo=tmp.seatNum.toUpperCase();
        core.info("EmailMessageSharing.qml | declineMessage |  index: "+index+" | seatNo: "+tmp.seatNo);
        if(!isSeatBlocked(tmp.seatNo)){Store.blockList.push(tmp.seatNo);}
        addToReceiverInvitationQueue(tmp.seatNo,"decline","","email","");
        declineInvite(index);
        __processIntivationRequest();
    }

    function optOut(index){
        disableEmailMessageSharing();
//        for(var i=0;i<allUserModel.count;i++){
//            addToReceiverInvitationQueue(allUserModel.getValue(i,"seatNo"),"optout","","email","");
//         }
//        __processIntivationRequest();
      //  optOutInvite(index);
    }

    function setUserStatus(index,status){
        var tmp=allUserModel.at(index);
        core.info("EmailMessageSharing.qml | setUserStatus |  index: "+index+" | seatNo: "+tmp.seatNo+" | status: "+status);
        allUserModel.setProperty(index,"status",status);
        addToReceiverInvitationQueue(allUserModel.getValue(index,"seatNo"),status,"","email","");
        __processIntivationRequest();
    }

    function setBlockStatus(index,status){
        var tmp=allUserModel.at(index);
        tmp.seatNo=tmp.seatNo.toUpperCase();
        core.info("EmailMessageSharing.qml | setUserStatus |  index: "+index+" | seatNo: "+tmp.seatNo+" | status: "+status);
        if(status=="blocked"){allUserModel.setProperty(index,"blocked",1);}else {allUserModel.setProperty(index,"blocked",0);}
        if(status=="blocked" && !isSeatBlocked(tmp.seatNo)){Store.blockList.push(tmp.seatNo);}
        if(status!="blocked" && isSeatBlocked(tmp.seatNo)){
            Store.blockList=[];
            for(var counter=0;i<=allUserModel.count;i++){
                if(allUserModel.getValue(counter,"blocked")){Store.blockList.push(tmp.seatNo);}
            }
        }
    }

    function isSeatBlocked(seatNum){
        seatNum=seatNum.toUpperCase();
        var indxOfMid=Store.blockList.indexOf(seatNum);
        if(indxOfMid>=0){return true;} else return false;
    }

    function getUserNickName(seat){
        var a = coreHelper.searchEntryInModel(allUserModel,"seatNo",seat)
        if(a==-1)return ""
        return allUserModel.getValue(a,"nickname")
    }

    function sendEmail(to,cc,subject,body){
        if(!to){core.info("EmailMessageSharing.qml | To Cannot be empty");}
        core.info("EmailMessageSharing.qml | sendEmail | to: "+to+" | cc: "+cc+" | subject: "+subject+" | body: "+body);
        var receiver=to;
        if(cc){
            var tmpReceiverArr=receiver.split(",");var tmpToArr=to.split(",");var tmpToCC=cc.split(",");
            for(var x in tmpToCC){if(tmpToArr.indexOf(tmpToCC[x])===-1){tmpReceiverArr.push(tmpToCC[x])}}
            receiver=tmpReceiverArr.join(",")
        }
        var msg='{"to":"'+to+'","cc":"'+cc+'","subject":"'+subject+'","body":"'+body+'","nickname":"'+nickname+'"}';
        sentBox.insert(0,{"to":to,"cc":cc,"subject":subject,"body":body,"reply":0,"time":getTime(),"nickname":nickname});
        var tmp=receiver.split(",");
        for(var x in tmp){
            if(tmp[x]){
                if(tmp[x].toUpperCase()!=pif.getSeatNumber().toUpperCase()){
                var userIdx=core.coreHelper.searchEntryInModel(allUserModel,"seatNo",tmp[x].toUpperCase());
                if(userIdx==-1){allUserModel.append({"seatNo":tmp[x].toUpperCase(),"nickname":"",status:"",blocked:0})};
                }
            }
        }
        initialize(msg,receiver,"email",nickname);
    }

    function readInboxMessage(index){
        core.info("EmailMessageSharing.qml | readEmailMessage | index :"+index)
        if(index==undefined){core.debug("EmailMessageSharing.qml | readEmailMessage | Index not provided, Exiting");return false}
        inbox.setProperty(index,"read",1)
    }

    function setSentBoxReplyStatus(index,status){
        core.info("EmailMessageSharing.qml | setSentBoxReplyStatus | index :"+index)
        if(index==undefined){core.debug("EmailMessageSharing.qml | setSentBoxReplyStatus | Index not provided, Exiting");return false}
        sentBox.setProperty(index,"reply",status)
    }

    function addToInbox(to,cc,subject,body,nickname,seatNo){
        if(!to){to=""}if(!cc){cc=""}if(!subject){subject=""}if(!body){body=""}if(!nickname){nickname=""}if(!seatNo){seatNo=pif.getSeatNumber().toUpperCase()}
        core.info("EmailMessageSharing.qml | addToInbox | "+to+" | "+cc+" | "+subject+" | "+body+" | "+nickname);
        inbox.insert(0,{"seatNo":seatNo,"to":to,"cc":cc,"subject":subject,"body":body,"read":0,"nickname":nickname,"time":getTime()});
    }

    function getTime(){
        core.info("EmailMessageSharing.qml | getTime ")
        var currentTime = new Date()
        var hours = currentTime.getHours()
        var minutes = currentTime.getMinutes()
        var suffix = "AM";
        if (hours >= 12) {
        suffix = "PM";
        hours = hours - 12;
        }
        if (hours == 0) {
        hours = 12;
        }

        hours=hours<10?"0"+hours:hours;
        minutes=minutes<10?"0"+minutes:minutes;

        var time = hours+":"+minutes+" "+suffix;
        return time
    }

    onSigInvitationReceived:{
        core.info(" EmailMessageSharing.qml | onSigInvitationReceived | index: "+index+" | "+seatNo+" | messageIdentifier: "+messageIdentifier+" | metaData: "+metaData);
        if(messageIdentifier!="email"){return false}
        if(isSeatBlocked(seatNo.toUpperCase())){core.info(" EmailMessageSharing.qml | seatNo: "+seatNo+" is BLOCKED returning");return true;}
        var msg = eval('(' + metaData + ')');
        core.info("EmailMessageSharing.qml | onSigInvitationReceived | index: "+index+" | "+msg.to+" | "+msg.cc+" | "+msg.subject+" | "+msg.body+" | "+msg.nickname);
        inbox.insert(0,{"seatNo":seatNo.toUpperCase(),"to":msg.to,"cc":msg.cc,"subject":msg.subject,"body":msg.body,"read":0,"nickname":msg.nickname,"time":getTime()});
        var userIdx=core.coreHelper.searchEntryInModel(allUserModel,"seatNo",seatNo.toUpperCase());
        if(userIdx!=-1){allUserModel.setProperty(userIdx,"nickname",msg.nickname)
        }else {allUserModel.append({"seatNo":seatNo.toUpperCase(),"nickname":msg.nickname,"status":"",blocked:0});}
        inboxMessageReceived();
    }

    onSigSharingStatus:{
         core.info(" EmailMessageSharing.qml | onSigSharingStatus | seatNo: "+seatNo+" | status: "+status);
         var userIdx=core.coreHelper.searchEntryInModel(allUserModel,"seatNo",seatNo.toUpperCase());
        if(status.indexOf("nickname##")!=-1){
            var tmpNickname = status.replace("nickname##","");
            if(userIdx!=-1){allUserModel.setProperty(userIdx,"nickname",tmpNickname);}else {core.info(" EmailMessageSharing.qml | onSigSharingStatus | seatNo: "+seatNo+" | Not found ");}
            for(var i=0;i<inbox.count;i++){
                if(inbox.getValue(i,"seatNo")==seatNo.toUpperCase()){inbox.setProperty(i,"nickname",tmpNickname)}
            }
        }else{
            if(userIdx!=-1){allUserModel.setProperty(userIdx,"status",status);}else {core.info(" EmailMessageSharing.qml | onSigSharingStatus | seatNo: "+seatNo+" | Not found ");}
        }
    }

    SimpleModel{
        id: inbox
    }

    SimpleModel{
        id: sentBox
    }

    SimpleModel{
        id: allUserModel
    }

    function frmwkResetAppEmail(){
        core.info("EmailMessageSharing.qml | frmwkResetAppEmail | Called ");
        frmwkResetApp();
        inbox.clear();sentBox.clear();allUserModel.clear();
        Store.blockList=[];nickname="";
    }

    Component.onCompleted: {
        core.info(" EmailMessageSharing.qml | Component.onCompleted ");
        Store.blockList=[];
    }
}
