import QtQuick 1.1
import Panasonic.Pif 1.0

VideoMediaPlayer{
    property variant deviceMngr;
    property bool __prePlayState:false;
    property string mediaSource:"extvBroadcast";
    property string __streamChangeAPI;

    onSigAggPlayBegan: {pif.setVolumeSource(pif.cVOD_VOLUME_SRC); if (__prePlayState) __prePlayState=false; else pif.screenSaver.stopScreenSaver();}
    onSigAggPlayStopped: {if (!__prePlayState) {if(pif.getLSVmediaSource() == mediaSource){ pif.unSuspendAudioPlay(); pif.screenSaver.startScreenSaver();}}}

    Connections{target:pif; onSigFallbackMode: if(fallbackMode) frmwkResetApp();}

    function playByModel (model, params) {
        core.info("Extv.qml | playByModel called");
        if (!model || !model.count) {core.error ("Extv.qml | Empty Model or Model not provided. EXITING..."); return false;}
        params.playIndex = (params.playIndex==undefined || params.playIndex>=model.count)?undefined:params.playIndex;
        params = __validateParams(params); if (!params) return false;
        return __playExtv(params,model);
    }

    function setPlayingSoundtrack(audioPid) { return __setSoundtrack(audioPid);}
    function playNext () {
        __streamChange("__mediaNext()");
    }
    function playPrevious () {
        __streamChange("__mediaPrevious()");
    }
    function __streamChange(api){
        core.info("Extv.qml | Stopping Stream and setting __streamChange api "+api);
        __streamChangeAPI=api;
        stop();
    }

    function stop() {return __mediaStop();}
    function setPIPByID (pipID) {return __setPIPByID(pipID,__getPipReference());}

    function __playExtv (params,model) {
        if (pif.getLSVmediaSource() != mediaSource) pif.__setLastSelectedVideo(extv);
        if (__mediaState!=cMEDIA_STOP) {__prePlayState=true; __mediaForceStopAck();}
        else pif.suspendAudioPlay();
        var s=__playerType+' | ';; for (var i in params) s+=i+" = "+params[i]+", "; core.debug(s);
        mediaPlaylist.audioPid=0;
        if (model) __setMediaModel(model);
        return __mediaPlay(params,__getPipReference());
    }

    function __setMediaModel(model){
        mediaPlaylist.playlistUpdateEnabled = false;
        mediaModel=model;
        mediaPlaylist.playlistUpdateEnabled = true;
        mediaPlaylist.model=mediaModel;
    }

    function __mediaStopAck (pos,pindex,mindex,cause) {
        core.info("Extv.qml | __streamChangeAPI to execute on __mediaStopAck : "+__streamChangeAPI);
        if(__streamChangeAPI!=""){
		    if (fakeStopAckTimer.running) fakeStopAckTimer.stop();
            eval(__streamChangeAPI);
            __streamChangeAPI="";
        }
        else{
            core.info ("VideoMediaPlayer.qml | __mediaStopAck called: position: "+pos+" playIndex: "+pindex+" __mediaState: "+__mediaState+" Cause: "+cause);
            __streamChangeAPI=""
            __mediaState = cMEDIA_STOP;
            __playerIndex = pindex;
            __modelIndex = mindex;
            __pipState = false;
            if (fakePlayAckTimer.running) fakePlayAckTimer.stop();
            if (fakeStopAckTimer.running) fakeStopAckTimer.stop();
            if (__skipSpeed==1) __mediaPositionAck((__mediaElapseTime!=0)?pos:0);   // __mediaElapseTime==0 incase eos occured
            else __skipSpeed = 1;
            core.info ("VideoMediaPlayer.qml | Elapsetime Of Mid: "+__cmid+" is "+__elapseTime);
            __stopMediaTimer();
            if (skipPreviousTimer.running) skipPreviousTimer.stop();
            sigMidPlayStopped(__amid,__cmid,mindex);
            sigAggPlayStopped(__amid,__cmid,mindex,cause);
            return true;
        }
    }

    function setPlayingIndex (indx,exec) {
        if(!exec)
            __streamChange("setPlayingIndex("+indx+","+true+")");
        else{
            var ret;
            core.info (__playerType+" | setPlayingIndex | Index: "+indx+",__elapseTime: "+__elapseTime);
            ret = mediaPlayer.gotoItem((!indx||indx>=mediaModel.count)?0:indx);
            if(core.pc)__elapseTime=0;
            core.info (__playerType+" | setPlayingIndex | Return status: "+ret);
            return __mediaPlayCheck(indx!=-1?indx:__playerIndex,ret);
        }
    }

    function __setPlayingMidInfo(mindex){__cmid=mediaModel.getValue(mindex,"id");}
    function __setMediaPlayerSoundtrack(){mediaPlayer.setSoundtrack(__sndtrkLid,__sndtrkType); return true;}

    function frmwkInitApp () {}
    function frmwkResetApp(){__mediaStop();}

    function __validateParams (p) {
        p.pipID = (String(parseInt(p.pipID,10))!="NaN")?parseInt(p.pipID,10):0;
        if (p.pipID==0){core.error ("Extv.qml | Pip index not provided. Exiting..."); return false;}
        p.playType=p.playType?p.playType:0;
        p.soundtrackEnabled = true;
        p.displayChNo = (p.displayChNo)?p.displayChNo:'0';
        p.ignoreSystemAspectRatio = p.ignoreSystemAspectRatio?p.ignoreSystemAspectRatio:false;
        p.stretch = p.stretch!=undefined?p.stretch:__getDefaultStretchState();
        p.cid = p.cid?p.cid:0;
        p.elapseTime = 0;
        p.isTrailer = false;
        p.sndtrkLid = String(parseInt(p.sndtrkLid,10))!="NaN"?parseInt(p.sndtrkLid,10):0;
        p.sndtrkType= String(parseInt(p.sndtrkType,10))!="NaN"?parseInt(p.sndtrkType,10):0;
        p.subtitleLid = String(parseInt(p.subtitleLid,10))!="NaN"?parseInt(p.subtitleLid,10):0;
        if (p.subtitleLid==-1 || p.subtitleLid==0) {p.subtitleEnabled = false; p.subtitleLid=0;} else p.subtitleEnabled = true;
        p.subtitleType= p.subtitleEnabled?String(parseInt(p.subtitleType,10))!="NaN"?parseInt(p.subtitleType,10):2:0;
        p.playIndex=(String(parseInt(p.playIndex,10))!="NaN")?p.playIndex:0;
        p.mediaType = p.mediaType?p.mediaType:"extvBroadcast";
        p.amid = (String(parseInt(p.amid,10))!="NaN")?p.amid:0;
        p.duration = (p.duration==undefined)?"00:00:00":p.duration;
        p.aspectRatio = p.aspectRatio?p.aspectRatio:"";
        p.repeatType=(String(parseInt(p.repeatType,10))!="NaN")?p.repeatType:1;
        p.elapseTimeFormat = (String(parseInt(p.elapseTimeFormat,10))!="NaN")?parseInt(p.elapseTimeFormat,10):1;
        p.rating = String(parseInt(p.rating,10))!="NaN"?parseInt(p.rating,10):254;
        p.vbMode=(p.vbMode==undefined)?false:p.vbMode;
        return p;
    }

    Component.onCompleted: __playerType = "Extv Video Media Player";
}
