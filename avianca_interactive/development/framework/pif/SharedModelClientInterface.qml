import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

// PURPOSE: Interface between client & Shared Model. File loaded on client side only.

SharedModelClient{
    id:sharedModelClientInterface;
    isClient: true;

    signal sigDataReceivedFromSeat(string api, variant params);
    signal connected();
    signal disconnected();
    signal dataSynchronized();
    signal sigPlaylistError(int errorCode);
    signal sigPlaylistAdded(int success);
    signal sigPlaylistIndex(int playlistIndex,string mediaType)
    property int cSUCCESS : 0;
    property int cMAXIMUM_PLAYLIST_REACHED: 1;
    property int cPLAYLIST_EXISTS: 2;
    property int cPLAYLIST_DOES_NOT_EXIST: 3;
    property int cPLAYLIST_FULL: 4;
    property int cSAME_PLAYLIST_NAME: 5;
    property int cMID_ALREADY_IN_PLAYLIST: 6;
    property bool triggerBinding: false;
    property bool paymentDoneFromKarma: true;

    onServerHostnameChanged: core.debug("SharedModelClientInterface.qml | serverHostname is | " + serverHostname);
    onSynchronized: {core.info("SharedModelClientInterface.qml | onSynchronized called "); dataSynchronized();}
    onLocalClientConnectedChanged: {
        if (localClientConnected) { core.info("SharedModelClientInterface.qml | LocalClientConnected"); sharedModelClientInterface.connected();}
        else { core.info("SharedModelClientInterface.qml | LocalClientDisconnected"); sharedModelClientInterface.disconnected();}
    }

    Connections{
        target: (pif.aod)?pif.aod:null
        onSigMidPlayBegan:{sendFrameworkApi("frameworkPasStart",{"deviceName":Store.pasDeviceName})}
        onSigMidPlayStopped:{sendFrameworkApi("frameworkPasStop")}
    }

    Connections{
        target: pif.ppv?pif.ppv:null;
        onSigPaymentSuccessful: {
            if(!paymentDoneFromKarma) {paymentDoneFromKarma=true; return true;}
            sendFrameworkApi('frameworkPaymentSuccessful',{amid:amid})
        }
    }

    function dataReceived(api){
        var params = [];
        if(api=="playVodAck" || api=="playTrailerAck"){
            params = {mid:mid,aggregateMid:aggregateMid,soundtrackLid:soundtrackLid,soundtrackType:soundtrackType,subtitleLid:subtitleLid,subtitleType:subtitleType,elapsedTime:elapsedTime}
            core.debug("Received message from seat | apiName: "+ api + " | mid : "+ params.mid + ", aggregateMid : " + params.aggregateMid + ", soundtrackLid : " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType + ", subtitleLid : " + params.subtitleLid + ", subtitleType : " + params.subtitleType + ", elapsedTime : " + params.elapsedTime);

        } else if(api=="aspectRatioAck"){
            params = {aspectRatio:aspectRatio}; core.debug("Received message from seat | apiName: "+ api + " | aspectRatio : " + params.aspectRatio);

        } else if(api=="videoSoundtrackAck"){
            params = {soundtrackLid:soundtrackLid,soundtrackType:soundtrackType}
            core.debug("Received message from seat | apiName: "+ api + " |  soundtrackLid : " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType);

        } else if(api=="videoSubtitleAck"){
            params = {subtitleLid:subtitleLid,subtitleType:subtitleType}
            core.debug("Received message from seat | apiName: "+ api + " | subtitleLid : "+ params.subtitleLid + ", subtitleType : " + params.subtitleType);

        } else if(api=="setMediaPositionAck"){
            params = {elapsedTime:elapsedTime}; core.debug("Received message from seat | apiName: "+ api + " | elapsedTime : " + params.elapsedTime);

        } else if(api=="mediaPauseAck" || api=="mediaResumeAck" || api=="videoStopAck" || api=="audioStopAck" || api=="closeAppAck"){
            core.debug("Received message from seat | apiName: "+ api);

        } else if(api=="mediaFastForwardAck" || api=="mediaRewindAck"){
            params = {mediaPlayRate:mediaPlayRate}; core.debug("Received message from seat | apiName: "+ api + " | mediaPlayRate : " + params.mediaPlayRate);

        } else if(api=="seatbackVolume"){
            params = {seatbackVolume:seatbackVolume}; core.debug("Received message from seat | seatbackVolume : " + params.seatbackVolume);

        } else if(api=="headsetVolume"){
            params = {headsetVolume:headsetVolume}; core.debug("Received message from seat | headsetVolume : " + params.headsetVolume);

        } else if(api=="muteVolume"){
            params = {muteVolume:muteVolume}; core.debug("Received message from seat | muteVolume : " + params.muteVolume);

        } else if(api=="muteHeadsetVolume"){
            params = {muteHeadsetVolume:muteHeadsetVolume}; core.debug("Received message from seat | muteHeadsetVolume : " + params.muteHeadsetVolume);

        } else if(api=="seatbackBrightness"){
            params = {seatbackBrightness:seatbackBrightness}; core.debug("SharedModelClientInterface.qml | seatbackBrightness change, seatbackBrightness : " + params.seatbackBrightness);

        } else if(api=="playAodAck"){
            params = {mid:mid,aggregateMid:aggregateMid,mediaRepeat:mediaRepeat};
            core.debug("Received message from seat | apiName: "+ api + " | mid : "+ params.mid + ", aggregateMid : " + params.aggregateMid + ", mediaRepeat : " + mediaRepeat);

        } else if(api=="mediaRepeatAck"){
            params = {mediaRepeat:mediaRepeat}; core.debug("Received message from seat | apiName: "+ api + " | mediaRepeat : "+ params.mediaRepeat);

        } else if(api=="mediaShuffleAck"){
            params = {mediaShuffle:mediaShuffle}; core.debug("Received message from seat | apiName: "+ api + " | mediaShuffle : "+ params.mediaShuffle);

        } else if(api=="changeLanguage"){
            params = {languageIso:languageIso}; core.debug("Received message from seat | apiName: "+ api + " | languageIso : "+ params.languageIso);

        } else if(api=="launchAppAck"){
            params = {launchAppId:launchAppId}; core.debug("Received message from seat | apiName: "+ api + " | launchAppId : "+ params.launchAppId);

        } else if(api=="iPodConnected"){
            params = {iPodConnected:iPodConnected}; core.debug("Received message from seat | apiName: "+ api + " | ipodConnected : "+ params.iPodConnected)

        } else if(api=="usbConnected"){
            params = {usbConnected:usbConnected}; core.debug("Received message from seat | apiName: "+ api + " | usbConnected : "+ params.usbConnected)

        } else if(api.indexOf("userDefined")!=-1 || api.indexOf("framework")!=-1){
            if (api == "frameworkPlaylistPartiallyAdded"){sigPlaylistError(cPLAYLIST_FULL); return true;}
            if (api == "frameworkPlaylistAdded"){sigPlaylistAdded(cSUCCESS); return true;}
            if (api == "frameworkplayingPlaylistIndex"){
                core.debug("SMCI.qml | Received Message from Seat | api : frameworkplayingPlaylistIndex|  playlistIndex :"+apiParameters.playlistIndex+"mediaType :"+apiParameters.mediaType)
                sigPlaylistIndex(apiParameters.playlistIndex,apiParameters.mediaType)
            }
            if (api == 'frameworkServiceBlockChange'){ pif.__serviceAccessChangeAck (apiParameters.accessType, apiParameters.serviceMid);return true;}
            else if (api == 'frameworkMidBlockChange'){ pif.__midAccessChangeAck(apiParameters.accessType, apiParameters.mid);return true;}
            else if (api == 'frameworkSeatRatingChange'){ pif.__seatRatingChangeAck(apiParameters.seatRating);return true;}
            if (api == 'frameworkPaymentSuccessful'){ paymentDoneFromKarma=false; pif.ppv.sigPaymentSuccessful(apiParameters.amid);return true;}
            core.debug("Received message from seat | apiName: "+ api); sigDataReceivedFromSeat(api,apiParameters); return true;

        } else if(api=="showKeyboard"){
            params = {showKeyboard:showKeyboard}; core.debug("Received message from seat | apiName: "+ api + " | "+ params.showKeyboard)

        } else if(api=="keyboardData"){
            params = {keyboardData:keyboardData}; core.debug("Received message from seat | apiName: "+ api + " | "+ params.keyboardData)

        }else if(api=="usbAgreement"){
            params = {usbAgreement:usbAgreement}; core.debug("Received message from seat | apiName: "+ api + " | "+ params.usbAgreement)

        } else if(api=="iPodAgreement"){
            params = {iPodAgreement:iPodAgreement}; core.debug("Received message from seat | apiName: "+ api + " | "+ params.iPodAgreement)

        } else if(api=="playBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="play";

        } else if(api=="stopBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="stop";

        } else if(api=="pauseBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="pause";

        } else if(api=="resumeBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="resume";

        } else if(api=="fastForwardBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="fastforward";

        } else if(api=="rewindBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);blueRayStatus="rewind";

        } else if(api=="nextBlueRayAck" || api=="previousBlueRayAck" || api=="menuBlueRayAck" || api=="upBlueRayAck" || api=="downBlueRayAck" || api=="leftBlueRayAck" || api=="rightBlueRayAck" || api=="enterBlueRayAck"){
            core.debug("Received message from seat | apiName: "+ api);

        } else if(api=="setBlueRayMediumTypeAck"){
            params = {blueRayMediumType:blueRayMediumType}; core.debug("Received message from seat | apiName: "+ api);

        } else if(api=="interactiveReset"){
            core.debug("Received message from seat | apiName: "+api); pif.__interactiveResetAck();

        } else if(api=="playlists"){
            core.debug("Received message from seat | apiName: "+api);
            triggerBinding = false;
            var plist = playlists;
            Store.aodPlaylistArray = [];
            Store.vodPlaylistArray = [];
            Store.kidsAodPlaylistArray = [];
            Store.kidsVodPlaylistArray = [];
            var aodPointer = plist.aod
            for(var aodCounter in aodPointer){
                for (var j in aodPointer[aodCounter]){
                    Store.aodPlaylistArray[aodPointer[aodCounter][j].mid]=1;
                }
            }
            var vodPointer = plist.vod
            for(var vodCounter in vodPointer){
                for (var k in vodPointer[vodCounter]){
                    Store.vodPlaylistArray[vodPointer[vodCounter][k].mid]=1;
                }
            }
            var kidsAodPointer = plist.kidsaod
            for(var kidsAodCounter in kidsAodPointer){
                for (var l in kidsAodPointer[kidsAodCounter]){
                    Store.kidsAodPlaylistArray[kidsAodPointer[kidsAodCounter][l].mid]=1;
                }
            }
            var kidsVodPointer = plist.kidsvod
            for(var kidsVodCounter in kidsVodPointer){
                for (var m in kidsVodPointer[kidsVodCounter]){
                    Store.kidsVodPlaylistArray[kidsVodPointer[kidsVodCounter][m].mid]=1;
                }
            }
            playlists=plist;
            triggerBinding = true;
        }  else {
            core.debug("Received message from seat | apiName: "+api);
        }
        sigDataReceivedFromSeat(api,params); return true;
    }

    function __iterateAssocArray(params){var logger=[];for(var key in params){ logger.push(" params."+key+": "+params[key])} return logger;}

    //public functions
    function playVodByMid(params){ //apiName: playVod
        if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        core.debug("Send message to seat | apiName: playVod |"+__iterateAssocArray(params));
        sendApiData("playVod",params);
    }

    function playExtv(params){ //apiName: playExtv
        //if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        core.debug("Send message to seat | apiName: playExtv |"+__iterateAssocArray(params));
        sendApiData("playExtv",params);
    }

    function playTrailer(params){ //apiName: playTrailer
        if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        core.debug("Send message to seat | apiName: playTrailer |"+__iterateAssocArray(params));
        sendApiData("playTrailer",params);
    }

    function toggleVodStretchState(){ // aspectRatioAck won't be received when request is made from vKarma to seat
        if(aspectRatio=="disable") return false;
        core.debug("Send message to seat | toggleVodStretchState | aspectRatio : " + aspectRatio);
        sendPropertyData("aspectRatio",(aspectRatio=="16x9"?"16x9Stretched":aspectRatio=="4x3"?"4x3Stretched":aspectRatio=="16x9Stretched"?"16x9":"4x3"));
    }

    function setPlayingSoundtrack(params){core.debug("Send message to seat | apiName: videoSoundtrack |"+__iterateAssocArray(params));sendApiData("videoSoundtrack",params);} //apiName: videoSoundtrack
    function setPlayingSubtitle(params){ core.debug("Send message to seat | apiName: videoSubtitle |"+__iterateAssocArray(params)); sendApiData("videoSubtitle",params);}       // apiName: videoSubtitle, View should pass subtitleLid=-1 for "None" as subtitle
    function stopVideo(){ core.debug("Send message to seat | apiName: videoStop"); sendApiData("videoStop","");}

    // AOD operations
    function playAodByMid(params){core.debug("Send message to seat | apiName: playAod |"+__iterateAssocArray(params)); sendApiData("playAod",params);}

    //repeatTrack "none": no  looping, "current": looping of the current track, "all": looping of all the tracks in the album
    function repeatTrack(params){  core.debug("Send message to seat | apiName: mediaRepeat |"+__iterateAssocArray(params)); sendApiData("mediaRepeat",params);}
    function shuffleTrack(){ core.debug("Send message to seat | mediaShuffle : " + mediaShuffle); sendPropertyData("mediaShuffle",!mediaShuffle);}
    function stopAudio(){ core.debug("Send message to seat | apiName: audioStop"); sendApiData("audioStop","");}

    // Common VOD & AOD operations
    function setPlayingMediaPosition(params){ core.debug("Send message to seat | apiName: setMediaPosition |"+__iterateAssocArray(params)); sendApiData("setMediaPosition",params)}
    function forwardPlayingMedia(){ core.debug("Send message to seat | apiName: mediaFastForward"); sendApiData("mediaFastForward","");}
    function rewindPlayingMedia(){  core.debug("Send message to seat | apiName: mediaRewind"); sendApiData("mediaRewind","");}
    function pausePlayingMedia(){ core.debug("Send message to seat | apiName: mediaPause"); sendApiData("mediaPause","");}
    function resumePlayingMedia(){ core.debug("Send message to seat | apiName: mediaResume"); sendApiData("mediaResume","");}
    function playNextMedia(){ core.debug("Send message to seat | apiName: mediaNext"); sendApiData("mediaNext","");}
    function playPreviousMedia(){ core.debug("Send message to seat | apiName: mediaPrevious"); sendApiData("mediaPrevious","");}

    function createPlaylist(params){ //{"playlistIndex":-1,"name":"aodPlaylist1","mediaType":"aod"}
        core.debug("SMCI.qml | createPlaylist called ")
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.name || params.name==undefined){core.debug("SMCI.qml | name is not valid , Exiting"); return;}
        if (!params.mediaType || params.mediaType==undefined)params.mediaType="aod"
        var plist = playlists
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        if(Math.abs(params.playlistIndex)>((params.mediaType=="aod")?pif.getNumberOfAodPlaylist():(params.mediaType=="vod")?pif.getNumberOfVodPlaylist():(params.mediaType=="kidsaod")?pif.getNumberOfKidsAodPlaylist():(params.mediaType=="kidsvod")?pif.getNumberOfKidsVodPlaylist():pif.getNumberOfAodPlaylist())){
            core.debug("SMCI.qml | createPlaylist.qml| Maximum Number of Playlist Reached, Exiting ")
            return cMAXIMUM_PLAYLIST_REACHED;
        }
        for (var x in pointer){
            if(Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)){
                core.debug("SMCI.qml | createPlaylist.qml | Playlist already exists, Exiting")
                return cPLAYLIST_EXISTS;
            }
        }
        playlists=plist;
        core.debug("Send message to seatback | apiName: createPlaylist | playlistIndex is " + params.playlistIndex + ",name is "+params.name+", mediaType : " + params.mediaType);
        sendApiData("createPlaylist",params)
        return cSUCCESS;
    }

    function updatePlaylistName(params){
        core.debug("smci.qml | updatePlaylistName called ")
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.name || params.name==undefined){core.debug("SMCI.qml | name is not valid , Exiting"); return;}
        if (!params.mediaType || params.mediaType==undefined)params.mediaType="aod"
        var plist =playlists;
        var reference;
        var name;
        var playlistName;
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        for (var x in pointer){
            name = x.split("|")
            if(Math.abs(params.playlistIndex)==parseInt(name[0],10)){
                playlistName=name[0] +"|"+ name[1];
                reference=pointer[x];
                break;
            }
        }
        if (!reference){
            core.debug("SMCI.qml | updatePlaylistName | Playlist does not exist, cannot update name, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        if(Math.abs(params.playlistIndex)+"|"+params.name==playlistName) {
            core.info("SMCI.qml | updatePlaylistName | Same Name used, Exiting ");
            return cSAME_PLAYLIST_NAME;
        }
        playlists=plist
        core.debug("Send message to seatback | apiName: updatePlaylistName | playlistIndex is " + params.playlistIndex + ",name is "+params.name+", mediaType : " + params.mediaType);
        sendApiData("updatePlaylistName",params)
        return cSUCCESS;
    }

    function addMidToPlaylist(model,params){ //apiName: addAllMidsToPlaylist, {"index": 1,"playlistIndex":-1,"aggregateMid":102,"cid":190,"mediaType":"aod"}
        core.debug("smci.qml | addMidToPlaylist()");
        var mid = parseInt(model.getValue(params.index,"mid"),10)
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.cid || params.cid==undefined) params.cid=0;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if (!params.aggregateMid && (params.mediaType=="vod" || params.mediaType=="kidsvod")){
            params.aggregateMid = mid
        }
        if(!params.aggregateMid || params.aggregateMid==undefined){core.debug("SMCI.qml | aggregateMid is not valid. Exiting.");return false;}
        if(!params.rating || params.rating==undefined) params.rating=254;
        var pointer = (params.mediaType=="aod")?playlists.aod:(params.mediaType=="vod")?playlists.vod:(params.mediaType=="kidsaod")?playlists.kidsaod:(params.mediaType=="kidsvod")?playlists.kidsvod:playlists.aod;
        var arrPointer = (params.mediaType=="aod")?Store.aodPlaylistArray:(params.mediaType=="vod")?Store.vodPlaylistArray:(params.mediaType=="kidsaod")?Store.kidsAodPlaylistArray:(params.mediaType=="kidsvod")?Store.kidsVodPlaylistArray:Store.aodPlaylistArray;
        var reference;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)) reference = pointer[x];
        }
        if (!reference){
            core.info("SMCI.qml | addMidToPlaylist | Playlist does not exist, Cannot Add Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        if(arrPointer[mid]){
            core.info("SMCI.qml | addMidToPlaylist() | Mid already in Playlist, Exiting ")
            return cMID_ALREADY_IN_PLAYLIST;
        }
        __addMidToPlaylist({"mids":[mid],"playlistIndex":params.playlistIndex,"aggregateMid":params.aggregateMid,"cid":params.cid,"mediaType":params.mediaType,"rating":params.rating});
        return cSUCCESS;
    }

    function addAllMidsToPlaylist(model,params){ //apiName: addAllMidsToPlaylist, {"playlistIndex":-1,"aggregateMid":102,"cid":190,"mediaType":"aod","rating":150}
        core.debug("smci.qml | addAllMidsToPlaylist()");
        var midList = [];
        for(var i=0; i<model.count;i++){
            midList.push(parseInt(model.getValue(i,"mid"),10))
        }
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.cid || params.cid==undefined) params.cid=0;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if (!params.aggregateMid && (params.mediaType=="vod" || params.mediaType=="kidsvod")){
            params.aggregateMid = midList[0];
        }
        if(!params.aggregateMid || params.aggregateMid==undefined){core.debug("SMCI.qml | aggregateMid is not valid. Exiting.");return false;}
        if(!params.rating || params.rating==undefined) params.rating=254;
        var pointer = (params.mediaType=="aod")?playlists.aod:(params.mediaType=="vod")?playlists.vod:(params.mediaType=="kidsaod")?playlists.kidsaod:(params.mediaType=="kidsvod")?playlists.kidsvod:playlists.aod;
        var reference;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)) reference = pointer[x];
        }
        if (!reference){
            core.info("SMCI.qml | addAllMidsToPlaylist | Playlist does not exist, Cannot Add Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        __addMidToPlaylist({"mids":midList,"playlistIndex":params.playlistIndex,"aggregateMid":params.aggregateMid,"cid":params.cid,"mediaType":params.mediaType,"rating":params.rating})
        return cSUCCESS;
    }

    function __addMidToPlaylist(params){
        core.info("Send message to seatback | apiName: addMidToPlaylist | playlistIndex is " + params.playlistIndex + ", mids is : " + params.mids + ", aggregateMid : " + params.aggregateMid + ", cid : " + params.cid + ", mediaType : " + params.mediaType + ", rating :"+params.rating);
        sendApiData("addMidToPlaylist",params);
    }

    function removeMidFromPlaylist(params){//apiName:removeMidFromPlayist,{"mids":[209]"mediatype":"aod"}
        core.debug("smci.qml | removeMidFromPlayist()");
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        core.debug("Send message to seat | apiName: removeMidFromPlayist | , mids is : " + params.mids+", mediaType");
        sendApiData("removeMidFromPlayist",params);
        return cSUCCESS;
    }

    function removeAllFromPlaylist(params){//apiName:removeAllFromPlaylist , "playlistIndex":-1,"mediaType":"aod"
        core.debug("smci.qml | removeAllFromPlaylist()");
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        var pointer = (params.mediaType=="aod")?playlists.aod:(params.mediaType=="vod")?playlists.vod:(params.mediaType=="kidsaod")?playlists.kidsaod:(params.mediaType=="kidsvod")?playlists.kidsvod:playlists.aod;
        var reference;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)) reference = pointer[x];
        }
        if (!reference){
            core.info("SMCI.qml | removeAllFromPlaylist | Playlist does not exist, Cannot Remove Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        core.debug("Send message to seat | apiName: removeAllFromPlaylist | playlistIndex is " + params.playlistIndex + " , mediaType : " + params.mediaType);
        sendApiData("removeAllFromPlaylist",params);
        return cSUCCESS;
    }

    function isTrackAddedInPlaylist (params){ //{"mid":25,"mediaType":"aod"}
        var triggerBindingCheck = triggerBinding;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        if(!params.mid || params.mid==undefined)return;
        var pointer = (params.mediaType=="aod")?Store.aodPlaylistArray:(params.mediaType=="vod")?Store.vodPlaylistArray:(params.mediaType=="kidsaod")?Store.kidsAodPlaylistArray:(params.mediaType=="kidsvod")?Store.kidsVodPlaylistArray:Store.aodPlaylistArray;
        if(!pointer[params.mid]) return false;
        else return true;
    }

    function isAllTracksAddedInPlaylist(model,params){//{"playlistIndex":-1,"mediaType":"aod"}
        var triggerBindingCheck = triggerBinding;
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        var mid;
        var ref = (params.mediaType=="aod")?Store.aodPlaylistArray:(params.mediaType=="vod")?Store.vodPlaylistArray:(params.mediaType=="kidsaod")?Store.kidsAodPlaylistArray:(params.mediaType=="kidsvod")?Store.kidsVodPlaylistArray:Store.aodPlaylistArray;
        for (var i=0; i<model.count;i++){
            mid = parseInt(model.getValue(i,"mid"),10)
            if(!ref[mid]) return false
            else continue;
        }
        return true;
    }

    function getAllPlaylistMids(params){ //{"playListIndex":-1,"mediaType":"aod"}
        core.info("SMCI.qml |getAllPlaylistMids | playListIndex :"+params.playlistIndex+" mediaType :"+params.mediaType)
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        var plist = playlists
        var midList = [];
        var reference;
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)){
                reference = pointer[x];
            }
        }
        if(!reference){
            core.info("SMCI.qml | getAllPlaylistMids | Playlist does not exist, Cannot Remove Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        for (var counter in reference){
            midList.push(reference[counter].mid)
        }
        playlists = plist
        core.info("SMCI.qml | getAllPlaylistMids| Midlist  :"+midList)
        return midList;
    }

    function getAllPlaylistAggregateMids(params){ //{"playListIndex":-1,"mediaType":"aod"}
        core.info("SMCI.qml | getAllPlaylistAggregateMids | playListIndex :"+params.playlistIndex+" mediaType :"+params.mediaType)
        if(!params.playlistIndex || params.playlistIndex==undefined)params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined)params.mediaType="aod";
        var plist = playlists
        var aggregateMidList = [];
        var uniqueAggreagteMidList = [];
        var reference;
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        for (var x in pointer){
            if (Math.abs(params.playlistIndex)==parseInt(x.split("|")[0],10)){
                reference = pointer[x];
            }
        }
        if(!reference){
            core.info("SMCI.qml | getAllPlaylistMids | Playlist does not exist, Cannot Remove Mids, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        for (var counter in reference){
            aggregateMidList.push(reference[counter].albumMid)
        }
        for(var i=0;i<aggregateMidList.length;i++){
            if(uniqueAggreagteMidList.indexOf(aggregateMidList[i])==-1)uniqueAggreagteMidList.push(aggregateMidList[i])
        }
        core.debug("SMCI.qml | getAllPlaylistAggregateMids | Aggregate MidList :"+uniqueAggreagteMidList)
        return uniqueAggreagteMidList;
    }
	
    function getPlaylistName(params){//{"playlistIndex":-1;"mediaType":"aod"}
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        var plist = playlists;
        var reference;
        var name
        var playlistName;
        var pointer = (params.mediaType=="aod")?plist.aod:(params.mediaType=="vod")?plist.vod:(params.mediaType=="kidsaod")?plist.kidsaod:(params.mediaType=="kidsvod")?plist.kidsvod:plist.aod;
        for (var x in pointer){
            name = x.split("|")
            if (Math.abs(params.playlistIndex)==parseInt(name[0],10)){
                playlistName = name[1]
                reference=pointer[x]
                break;
            }
        }
        if(!reference){
            core.info("SMCI.qml | playlistName | Playlist does not exist, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        playlists = plist;
        core.info("SMCI.qml | getPlaylistName | PlaylistName :"+playlistName)
        return playlistName;
    }

    function playJukebox(params){//{"playlistIndex":-1,"mids":209,"mediaType":"aod"}
        core.debug("smci.qml | playJukebox() ");
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mid || params.mid==undefined){core.debug("SMCI.qml | Mid is not valid. Exiting.");return false;}
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        core.debug("Send message to seat | apiName: playJukebox | playlistIndex is " + params.playlistIndex + ", mid is : " + params.mid + ", mediaType : " + params.mediaType);
        sendApiData("playJukebox",params);
        return cSUCCESS;
    }

    function getPlaylistCount(params){//{"playlistIndex":-1,"mediaType":"aod"}
        if(!params.playlistIndex || params.playlistIndex==undefined) params.playlistIndex=-1;
        if(!params.mediaType || params.mediaType==undefined) params.mediaType="aod";
        var reference;
        var name;
        var playlistCount;
        var pointer = (params.mediaType=="aod")?playlists.aod:(params.mediaType=="vod")?playlists.vod:(params.mediaType=="kidsaod")?playlists.kidsaod:(params.mediaType=="kidsvod")?playlists.kidsvod:playlists.aod;
        for (var x in pointer){
            name = x.split("|")
            if (Math.abs(params.playlistIndex)==parseInt(name[0],10)){
                var playlistName = parseInt(name[0],10)+"|"+name[1]
                playlistCount = pointer[playlistName].length
                reference = pointer[x];
            }
        }
        if(!reference){
            core.info("SMCI.qml | playlistName | Playlist does not exist, Exiting")
            return cPLAYLIST_DOES_NOT_EXIST;
        }
        core.info("SMCI.qml | getPlaylistCount | playlistCount :"+playlistCount)
        return playlistCount;
    }

    // Volume, Brightness settings
    function setSeatbackVolume(volumeLevel){
        core.debug("Send message to seat | setSeatbackVolume | volumeLevel: " + volumeLevel);
        if(muteVolume)sendPropertyData("muteVolume",!muteVolume);
        sendPropertyData("seatbackVolume",volumeLevel);
    }
    function setSeatbackVolumeByStep(step) {    // step: -1, -2... or 1, 2...
        core.debug("Send message to seat | setSeatbackVolumeByStep | volume Step: " + step);
        step=(String(parseInt(step,10))!="NaN")?parseInt(step,10):0;
        var volume=seatbackVolume+pif.getVolumeStep()*step;
        if(muteVolume)sendPropertyData("muteVolume",!muteVolume);
        sendPropertyData("seatbackVolume",(volume>100?100:volume<0?0:volume)); return true;
    }
    function setHeadsetVolume(volumeLevel){
        core.debug("Send message to seat | setHeadsetVolume | volumeLevel: " + volumeLevel);
        if(muteHeadsetVolume)sendPropertyData("muteHeadsetVolume",!muteHeadsetVolume);
        sendPropertyData("headsetVolume",volumeLevel);
    }
    function setHeadsetVolumeByStep(step) {
        core.debug("Send message to seat | setHeadsetVolumeByStep | volume Step: " + step);
        step=(String(parseInt(step,10))!="NaN")?parseInt(step,10):0;
        var volume=headsetVolume+pif.getVolumeStep()*step;
        if(muteHeadsetVolume)sendPropertyData("muteHeadsetVolume",!muteHeadsetVolume);
        sendPropertyData("headsetVolume",(volume>100?100:volume<0?0:volume)); return true;
    }
    function toggleMuteState(){
        core.debug("Send message to seat | toggleMuteState current muteVolume: "+muteVolume);
        sendPropertyData("muteVolume",!muteVolume); return true;
    }

    function toggleHeadsetMuteState(){
        core.debug("Send message to seat | toggleHeadsetMuteState current muteHeadsetVolume: "+muteHeadsetVolume);
        sendPropertyData("muteHeadsetVolume",!muteHeadsetVolume); return true;
    }
    function setSeatbackBrightness(brightnessLevel){ core.debug("Send message to seat | setSeatbackBrightness | brightnessLevel : " + brightnessLevel); sendPropertyData("seatbackBrightness",brightnessLevel);}
    function setSeatbackBrightnessByStep(step) {    // step: -1, -2... or 1, 2...
        core.debug("Send message to seat | setSeatbackBrightnessByStep | Brightness Step: " + step);
        step=(String(parseInt(step,10))!="NaN")?parseInt(step,10):0;
        var brightness=seatbackBrightness+pif.getBrightnessStep()*step;
        sendPropertyData("seatbackBrightness",(brightness>100?100:brightness<0?0:brightness)); return true;
    }

    function playBlueRay(){core.debug("Send message to seat | apiName: playBlueRay ");sendApiData("playBlueRay","");}
    function stopBlueRay(){ core.debug("Send message to seat | apiName: stopBlueRay"); sendApiData("stopBlueRay","");}
    function pauseBlueRay(){ core.debug("Send message to seat | apiName: pauseBlueRay"); sendApiData("pauseBlueRay","");}
    function resumeBlueRay(){ core.debug("Send message to seat | apiName: resumeBlueRay"); sendApiData("resumeBlueRay","");}
    function forwardBlueRay(){ core.debug("Send message to seat | apiName: fastForwardBlueRay"); sendApiData("fastForwardBlueRay","");}
    function rewindBlueRay(){  core.debug("Send message to seat | apiName: rewindBlueRay"); sendApiData("rewindBlueRay","");}
    function playNextBlueRay(){ core.debug("Send message to seat | apiName: nextBlueRay"); sendApiData("nextBlueRay","");}
    function playPreviousBlueRay(){ core.debug("Send message to seat | apiName: previousBlueRay"); sendApiData("previousBlueRay","");}
    function navigateInBlueRay(action){
        switch(action){
        case "menuTitle":{core.debug("Send message to seat | apiName: menuTitleBlueRay ");sendApiData("menuTitleBlueRay","");break;}
        case "menuDisc":{core.debug("Send message to seat | apiName: menuDiscBlueRay ");sendApiData("menuDiscBlueRay","");break;}
        case "up":{core.debug("Send message to seat | apiName: upBlueRay ");sendApiData("upBlueRay","");break;}
        case "down":{core.debug("Send message to seat | apiName: downBlueRay ");sendApiData("downBlueRay","");break;}
        case "left":{core.debug("Send message to seat | apiName: leftBlueRay ");sendApiData("leftBlueRay","");break;}
        case "right":{core.debug("Send message to seat | apiName: rightBlueRay ");sendApiData("rightBlueRay","");break;}
        case "enter":{core.debug("Send message to seat | apiName: enterBlueRay ");sendApiData("enterBlueRay","");break;}
        default:{core.debug("Blue ray player. Invalid navigation request.");break;}
        }
    }
    function setBlueRayMediumType(blueRayMediumType){
        core.debug("Send message to seat | apiName: setBlueRayMediumType | mediumType : " + blueRayMediumType);
        if(blueRayMediumType=="BD"||blueRayMediumType=="DVD"||blueRayMediumType=="CD"||mediumType=="NONE")sendPropertyData("setBlueRayMediumType",blueRayMediumType);
        else{core.error("Invalid mediumType.");}
    }

    function toggleBacklight(){ core.debug("Send message to seat | toggleBacklight : " + backlightEnabled); sendPropertyData("backlightEnabled",!backlightEnabled); }
    function languageChange(params){ core.debug("Send message to seat  | langugeChange |"+__iterateAssocArray(params)); sendApiData("changeLanguage",params);}
    function launchApp(params){ core.debug("Send message to seat | launchApp | launchAppId : " + params.launchAppId); sendApiData("launchApp",params);}
    function closeApp(){ core.debug("Send message to seat  | closeApp called"); sendApiData("closeApp",{});}
    function sendKeyboardDataToSeat(val){sendPropertyData("keyboardData",val);}
    function showVkb(){sendPropertyData("showKeyboard",true);}
    function hideVkb(){sendPropertyData("showKeyboard",false);}
    function launchHospitality(){sendApiData("launchHospitality","");}
    function launchShopping(){sendApiData("launchShopping","");}
    function launchSurvey(){sendApiData("launchSurvey","");}
    function launchSeatChat(){sendApiData("launchSeatChat","");}
    function launchChatroom(){sendApiData("launchChatroom","");}
    function launchLTN(){sendApiData("launchLTN","");}
    function launchConnectingGate(){sendApiData("launchConnectingGate","");}
    function launchArrivalInfo(){sendApiData("launchArrivalInfo","");}
    function launchiXplor(){sendApiData("launchiXplor","");}
    function launchUsb(){sendApiData("launchUsb","");}
    function launchIpod(val){sendApiData("launchIpod",{launchAppId:val});}
    function launchCategory(val){sendApiData("launchCategory",{launchAppId:val});}

    function setUsbAgreement(state){sendPropertyData("usbAgreement",state);}
    function setIpodAgreement(state){sendPropertyData("iPodAgreement",state);}

    function sendUserDefinedApi(userDefinedApi,apiParams){ if(userDefinedApi.indexOf("userDefined")!=-1) sendApiData(userDefinedApi,(apiParams)?apiParams:''); else core.debug("SharedModelClientInterface.qml | userDefinedApi should be sufficed with word userDefined");}
    function sendFrameworkApi(frameworkApi,apiParams){ if(frameworkApi.indexOf("framework")!=-1) sendApiData(frameworkApi,(apiParams)?apiParams:''); else core.debug("SharedModelClientInterface.qml | frameworkApi should be sufficed with word frameworkApi");}

    function getLanguageIso(){return languageIso;}
    function getSeatbackBrightness(){return seatbackBrightness;}
    function getSeatbackVolume() {return seatbackVolume;}
    function getHeadsetVolume() {return headsetVolume;}
    function getBacklightStatus() {return backlightEnabled;}
    function getPlayingMidInfo() {return {mid:mid,aggregateMid:aggregateMid};}
    function getSoundtrackInfo() {return {soundtrackLid:soundtrackLid,soundtrackType:soundtrackType};}
    function getSubtitleInfo() {return {subtitleLid:subtitleLid,subtitleType:subtitleType};}
    function getElapsedTime() {return elapsedTime;}
    function getRemainingTime(duration) {return (!duration || !mediaType)?"":(duration-elapsedTime);}
    function getAspectRatio() {return aspectRatio;}
    function getMediaType() {return mediaType;}
    function getMediaPlayRate() {return mediaPlayRate;}
    function getCurrentMediaPlayerState() {return mediaPlayerState;}
    function getMediaRepeatValue() {return mediaRepeat;}
    function getMediaShuffle() {return mediaShuffle;}
    function getUsbConnectedStatus() {return usbConnected;}
    function getIpodConnectedStatus() {return iPodConnected;}
    function getCurrentLaunchAppId() {return launchAppId;}
    function getKeyboardDisplayStatus() {return showKeyboard;}
    function getKeyboardData() {return keyboardData;}
    function getUsbAgreement() {return usbAgreement;}
    function getIpodAgreement() {return iPodAgreement;}
    function getPlaylistInfo() {return playlists;}
    function getMuteState(){return muteVolume;}
    function getHeadsetMuteState(){return muteHeadsetVolume;}
    function getAudioPlayerdata(){sendFrameworkApi("frameworkGetAudioPlayerdata",{"mediaType":mediaType});}
    function getMidElapsedTime(mid){return (mid && resumeTimeMap[mid])?resumeTimeMap[mid]:0;}
    function getPasDeviceName(){return Store.pasDeviceName;}
    function setPasDeviceName(value){Store.pasDeviceName=value;}
    function playPas(deviceName){sendFrameworkApi("frameworkPasStart",{"deviceName":deviceName})}
    function stopPas(){sendFrameworkApi("frameworkPasStop")}
    function getPlayingPlaylistIndex(params){sendFrameworkApi("frameworkGetPlayingPlaylistIndex",{"mediaType":params.mediaType})}
    function getNoOfDimmableWindows(){return noOfDimmableWindows;}

    Component.onCompleted: {
        Store.aodPlaylistArray = [];
        Store.vodPlaylistArray = [];
        Store.kidsAodPlaylistArray = [];
        Store.kidsVodPlaylistArray = [];
        Store.pasDeviceName="";
        core.debug("SharedModelClientInterface.qml | Shared Model Client Interface component load complete");
    }
}
