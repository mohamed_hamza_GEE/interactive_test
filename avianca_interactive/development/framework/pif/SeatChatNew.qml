import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Info
import Panasonic.chatmgr 1.0


Item {
    id:seatChat
    property variant manager;
    property variant seatChatManager;
    property string cAvailable: "Available";
    property string cAway: "Away";
    property string cDoNotDisturb: "DoNotDisturb";
    property string cInvisible: "Invisible";
    property string cBusy: "Busy";
    property bool __appActive:false;
    property string __seatNumber: "";
    property string __seatNickname: "";
    property string __seatAvatar:"";
    property string __seatStatus: cAvailable;
    property bool __appSetActiveCompleted: true;
    property int __seatState: 0;
    property int totalPublicSessionUnReadMessages: 0;
    property int totalPrivateSessionUnReadMessages: 0;
    property bool __seatBlockAllInvites: true;
    property bool __exitSeatChatScreen:false;
    property bool __initialiseSeatChatData:false
    property bool __seatChatVkarma:false;
    property alias sessionListModel:sessionListModel    // private chatroom sessions
    property alias publicsessionListModel:publicsessionListModel    // public chatroom sessions
    property alias buddyListModel:buddyListModel            // List of buddies in current chatroom session
    property alias blockListModel :blockListModel           // blocked buddies for this seat
    property alias chatHistoryModel:chatHistoryModel        // List of chat messages in current chatroom session
    property alias sessionListWithBuddyList:sessionListWithBuddyList
    property alias allBuddiesNSessionModel :allBuddiesNSessionModel         // List of buddies in across all chatroom sessions
    property alias allBuddiesNBlockListModel :allBuddiesNBlockListModel
    property alias allChatUsersModel :allChatUsersModel     // List of seat where seat Chat app is active
    property alias allActiveBuddiesModel:allActiveBuddiesModel  // List of all buddies who are acitve and NOT blocked.
    property alias pendingInvitationList:pendingInvitationList
    property alias pendingInvitationListCount:pendingInvitationList.count
    property alias receivedPendingInvitationModel: receivedPendingInvitationModel
    property alias joinedPublicSessionListModel: joinedPublicSessionListModel
    property alias activeChatUsersModel:activeChatUsersModel;

    signal sigSeatChatAppReady(int id)      //ID 1 - For App Ready  NOT used need yet
    signal sigJoinPublicSessionStatus(bool status, string sessionId);
    signal sigChatMessageReceived(string seatNum, string session, string message); // you have to listen this signal only fir session Id from signal is equal to currentSessionId.
    signal sigBuddyAdd (string session , string seatNum, string seatName);
    signal sigUserInfoUpdated(string seatNum, string seatName, int seatState, string seatStatus, string blockAllInvites, string avatar, string serverSessionId);
    signal sigMySeatInfoUpdated(string seatNum, string seatName, int seatState, string seatStatus, string blockAllInvites, string avatar);
    signal sigBuddyExit (string seatNum, string session, string seatName);
    signal sigSessionAdded(string session)
    signal sigSessionDeleted(string session)
    signal sigSessionListRefreshed()
    signal sigBuddyListRefreshed()
    signal sigAllUsers(int allUsersCount)
    signal sigSessionIdCreatedOnServer(string session)
    signal sigInvitationSent(string session, variant seatNumberArr)
    signal sigInvitationReceived(string seatNum, string session, string seatName,string message)
    signal sigSentInvitationStatus(string seatNum, string session, int status,string message) // If next seat Accept Inv. status = 1 and On reject status = 0.
    signal sigCurrentSessionIdChanged(string sessionId);
    signal sigCommunicationError();
    signal sigPublicSessionListRefreshed();
    signal sigExitPublicSessionStatus(bool status,string sessionId)
    signal sigChatHistoryModelCount(int chatHistoryModelCount);
    signal sigPrivateSessionClosed(string serverSessionId, string creatorSeatNumber, string message);
    signal sigUserRemoved(string serverSessionId, string creatorSeatNumber);
    signal sigJoinedPublicSessionListRefreshed();
    signal sigUserBlocked(string seatnumber);
    signal sigUserUnBlocked(string seatnumber);
    signal sigCreatorOfSessionExited(string serverSessionId,string seatNumber);
    signal sigSessionCreatorListRefreshed();
    signal sigActiveUserListRefreshed();
    signal sigUserExitedSeatChat(string seatNumber,string seatName);

    Connections{
        target:(pif.vkarmaToSeat)?pif.vkarmaToSeat:null
        onSigDataReceivedFromSeat:{
            if(api == "eventInfoData"){
                core.info("SeatChatNew.qml | onSigDataReceivedFromSeat | api:"+api)
                __handleChatEvent(params.event,params.eventInfo);
            } else if(api == "seatToVkarmaSeatChatRequest"){
                core.info("SeatChatNew.qml | onSigDataReceivedFromSeat | api:"+api)
                eval(params.callbackFunction+"(params.returnObj)");
            } else if (api == "seatToVkarmaSeatChatWithParams"){
                core.info("SeatChatNew.qml | onSigDataReceivedFromSeat | api:"+api)
                eval(params.callbackFunction+"(params.returnObj[0],params.returnObj[1])");
            }
        }
    }

    Connections{
        target:(pif.isSeatChatOnVkarma())?pif.seatInteractive:null;
        onSigDataReceived:{
            core.info("SeatChatNew.qml | onSigDataReceived | api :"+api)
            if(api=="seatChatVkarmaToSeatRequest"){
                var newparams = params.apiParameters
                for(var i in params.apiParameters){
                    if(typeof params.apiParameters[i]=="string")
                        newparams[i]="'"+params.apiParameters[i]+"'";
                }
                newparams = newparams.join(",");
                eval(params.apiName+"("+newparams+")")
            } else if (api=="seatChatVkarmaToSeatCallBackRequest"){
                newparams = params.apiParameters
                for(i in params.apiParameters){
                    if(typeof params.apiParameters[i]=="string")
                        newparams[i]="'"+params.apiParameters[i]+"'";
                }
                newparams = newparams.join(",");
                var ret = eval(params.apiName+"("+newparams+")");
                var plist = {}
                for(var key in params){
                    plist[key] = params[key];
                }
                plist["returnObj"]=ret;
                plist["uniqueId"]=Math.random();
                pif.seatInteractive.sendPropertyData("seatToVkarmaSeatChatRequest",plist);
            } else if (api == "seatChatVkarmaToSeatExtraParams"){
                newparams = params.apiParameters;
                for(i in params.apiParameters){
                    if(typeof params.apiParameters[i]=="string")
                        newparams[i]="'"+params.apiParameters[i]+"'";
                }
                newparams = newparams.join(",");
                var ret = eval(params.apiName+"("+newparams+")");
                var plist = {}
                for(var key in params){
                    plist[key] = params[key];
                }
                var newArr = [];
                newArr.push(ret);
                newArr.push(params.extraParams)
                plist["returnObj"]=newArr;
                plist["uniqueId"]=Math.random();
                pif.seatInteractive.sendPropertyData("seatToVkarmaSeatChatWithParams",plist);
            }
        }
    }

    function addToQueue(seatNum,seatName,seatStatus,session,inviteResponseMessage){
        pendingInvitationList.append({ 'seat':seatNum.toUpperCase(), 'seatName':seatName, 'seatStatus':seatStatus, 'session':session, 'inviteResponseMessage':inviteResponseMessage });
        core.info('SeatChatNew.qml | addToQueue | added to queue pendingInvitationListCount ='+pendingInvitationListCount);
        return true;
    }

    function removeFromQueue(session){
        for(var i=0 ; i< pendingInvitationList.count; i++ )
            if(pendingInvitationList.at(i).session==session)pendingInvitationList.remove(i,1)
        core.info('SeatChatNew.qml | removeFromQueue | deleted from queue pendingInvitationListCount='+pendingInvitationListCount);
        return true;
    }

    function getCurrentSessionId(){ return Info.CurrentSessionId;}

    function getSeatNumber() { return __seatNumber; }

    function getInvitationQueue(){return Info.InvitationQueue;}

    function getSeatNickname(seatNum) {
        if (seatNum==undefined) return __seatNickname;
       else return (Info.SeatsNum[seatNum.toUpperCase()])?Info.SeatsNum[seatNum.toUpperCase()]['seatName']:"";
    }

    function getSeatAvatar(seatNum){
        if(seatNum==undefined) return __seatAvatar;
        else return (Info.SeatsNum[seatNum.toUpperCase()])?Info.SeatsNum[seatNum.toUpperCase()]['seatAvatar']:"";
    }
    
    function getSeatName(seat){
        return  (Info.SeatsName && Info.SeatsName[seat])?Info.SeatsName[seat]:"";
    }

    function getSeatStatus(seatNum) {
        if (seatNum==undefined) return __seatStatus;
        else return (Info.SeatsNum[seatNum.toUpperCase()])?Info.SeatsNum[seatNum.toUpperCase()]['seatStatus']:cAvailable;
    }

    function getSeatState() { return __seatState;}

    function getSeatBlockAllInvites() { return __seatBlockAllInvites;}

    function getMaxSessionCount() { return Info.MaxSessionCount;}

    function getPendingInvitesSent(serverSessionId){
        core.info("SeatChatNew.qml | getPendingInviteSent | serverSessionId: "+serverSessionId);
        var pendingList = new Array
        for (var x in Info.__pendingInvites[serverSessionId]){
            pendingList.push(x)
        }
        return pendingList;
    }

    function getPublicSessionJoined(serverSessionId){
        core.info("SeatChatNew.qml | getPublicSessionJoined | serverSessionId: "+serverSessionId)
        if(!Info.joinedPublicChatRoom[serverSessionId]) return false;
        else return true;
    }

    function getIsPublicSession(serverSessionId){
        core.info("SeatChatNew.qml | getIsPublicSession | serverSessionId: "+serverSessionId)
        if(!Info.publicChatRoom[serverSessionId]) return false;
        else return true;
    }

    function getIsSeatBlocked(seatNumber){
        var seatNo = seatNumber.toUpperCase()
        if(!Info.blockedSeatsArr[seatNo])return false;
        else return true
    }

    function getSessionCreator(serverSessionId){
        if(serverSessionId==undefined || serverSessionId==""){core.info("SeatChatNew.qml | getSessionCreator,serverSessionId not provided, exiting");return false}
        if(Info.sessionCreatorSeatNumber[serverSessionId])return Info.sessionCreatorSeatNumber[serverSessionId];
        else return "";
    }

    function getSeatNameArray(){return Info.SeatsName;}

    function clearServerDataOnStartup(){
        core.info("SeatChatNew.qml | clearServerDataOnStartup ");
        manager.clearServerData(pif.getSeatNumber().toUpperCase());
    }

    function setAppActive(){//requestAvailableUserStates();requestPublicSessions();requestAllUserList();requestUserProfile();requestUserBuddies();requestUserPrivateSessions();--These API are called by default on setting user seatnumber
        core.info("SeatChatNew.qml | setAppActive | __appActive: "+__appActive);
        if (__appActive) return true;
        __appSetActiveCompleted=false;
        manager.loginUserSeatNumber(pif.getSeatNumber().toUpperCase());
        setCurrentSessionId(0); __seatNickname="";__seatAvatar=""; __seatStatus=cAvailable; __seatState=0; __seatBlockAllInvites=false; Info.SessionCount=0;
        Info.SeatsNum=[];Info.SeatsName=[];Info.privateSessions=[];__seatNumber="";Info.publicChatRoom=[];Info.joinedPublicChatRoom=[]; Info.chatMsgCount=[];
        Info.InvitationQueue=[];Info.TempBuddyList=[];Info.__pendingInvites=[];Info.TempSeatInfo=[];Info.localChatHistory=[];
        Info.createdPrivateSession=[]; Info.sessionForActiveUsers="";Info.activeChatUsersNames=[];Info.sessionCreatorSeatNumber=[];Info.blockedSeatsArr=[];manager.requestUserPrivateSessions();
        __appActive=true;
        return true;
    }
    
    function loginSeatChat(){ // Do not use this method for seatchat
        core.info("SeatChat.qml | setAppActive | __appActive: "+__appActive);
        if (__appActive) return true;
        manager.loginUserSeatNumber(pif.getSeatNumber().toUpperCase());
        setCurrentSessionId(0); __seatNickname="";__seatAvatar=""; __seatStatus=cAvailable; __seatState=0; __seatBlockAllInvites=false; Info.SessionCount=0;
        Info.SeatsNum=[];Info.SeatsName=[];Info.privateSessions=[];__seatNumber="";Info.publicChatRoom=[];Info.joinedPublicChatRoom=[]; Info.chatMsgCount=[];
        Info.InvitationQueue=[];Info.TempBuddyList=[];Info.__pendingInvites=[];Info.TempSeatInfo=[];Info.localChatHistory=[];
        Info.createdPrivateSession=[]; Info.sessionForActiveUsers="";Info.activeChatUsersNames=[];Info.sessionCreatorSeatNumber=[];Info.blockedSeatsArr=[];
        manager.requestAllUserList();
        __appActive=true;
        return true;
    }

    function initialiseSeatChatData(){
        core.info("SeatChatNew.qml | initialiseSeatChatData Called");
        if(__initialiseSeatChatData) {core.info("SeatChatNew.qml | initialiseSeatChatData | SeatChatData already initialised, returning");return false;}
        __initialiseSeatChatData=true;
        manager.requestAllUserList(); // We may remove this
        manager.requestUserProfile();
        manager.requestUserBuddies();
        manager.requestPublicSessions();
        return true;
    }

    function __removeSessionListModel(serverSessionId){
        core.info("SeatChatNew.qml | __removeSessionListModel() serverSessionId is " + serverSessionId);
        var ret = false
        for(var i=0;i< sessionListModel.count;i++){
            if(sessionListModel.get(i).session==serverSessionId)
            {
                sessionListModel.remove(i,1);
                Info.SessionCount=Info.SessionCount-1;
                ret = true;
                sigSessionListRefreshed();
            }
        }
        core.info("SeatChatNew.qml | __removeSessionListModel | Info.SessionCount is "+Info.SessionCount);
        return ret;
    }

    function __removeJoinedPublicSession(serverSessionId){
        core.info("SeatChatNew.qml | __removeJoinedPublicSession() serverSessionId is " + serverSessionId);
        for (var i=0;i<joinedPublicSessionListModel.count;i++){
            if(joinedPublicSessionListModel.get(i).session==serverSessionId){
                joinedPublicSessionListModel.remove(i,1);
            }
        }
        return true;
    }

    function __appendSessionListModel(serverSessionId){
        core.info("SeatChatNew.qml | __appendSessionListModel | Started");
        var sessionParticipantCount = manager.getSessionParticipantCount(serverSessionId,"","__appendSessionListParticipantCount",serverSessionId);
        core.info("SeatChat.qml | __appendSessionListModel | session:"+serverSessionId+", sessionParticipantCount: "+sessionParticipantCount);
        if(sessionParticipantCount<0)return false;
        else {
            __appendSessionListParticipantCount(sessionParticipantCount,serverSessionId)
        }
    }

    function __appendSessionListParticipantCount(sessionParticipantCount,session){
        core.info("SeatChatNew.qml | __appendSessionListParticipantCount | sessionParticipantCount:"+sessionParticipantCount)
        if(sessionParticipantCount==0 && (!Info.privateSessions[session])){
            sessionListModel.append( { "session": session ,"unReadMsgCount":0,"participantSeatNo":"","participantCount":0} )
            Info.privateSessions[session]=session
            Info.SessionCount=Info.SessionCount+1;
            sigSessionListRefreshed();
        } else {
            var sessionParticipantList = manager.getSessionParticipantList(session, "", 0, sessionParticipantCount-1,"__appendSessionListModelCallBack",session);
            if(sessionParticipantList<0)return false
            else __appendSessionListModelCallBack(sessionParticipantList,session);
        }
    }

    function __appendSessionListModelCallBack(buddyList,serverSessionId){
        core.info("SeatChatNew.qml | __appendSessionListModelCallBack Called");
        var participantList = "";
        for (var j in buddyList){
            var seat = manager.getHashValue(buddyList[j],"seatnumber").toUpperCase();
            participantList = seat+","+participantList;
        }
        participantList = participantList.substring(0,participantList.length-1);
        if((!Info.privateSessions[serverSessionId])){
            sessionListModel.append( { "session": serverSessionId ,"unReadMsgCount":0,"participantSeatNo":(participantList.length!=0)?participantList:"","participantCount":(buddyList.length!=0)?buddyList.length:0} )
            Info.privateSessions[serverSessionId]=serverSessionId
            Info.SessionCount=Info.SessionCount+1;
        }else{
            for(var i=0;i<sessionListModel.count;i++){
                if(sessionListModel.get(i).session == serverSessionId){
                    sessionListModel.setProperty(i,"participantSeatNo",(participantList.length!=0)?participantList:"");
                    sessionListModel.setProperty(i,"participantCount",(buddyList.length!=0)?buddyList.length:0);
                }
            }
        }
        sigSessionListRefreshed();
    }

    function __appendJoinedPublicSession(serverSessionId){
        core.info("SeatChatNew.qml | __appendJoinedPublicSession called | serverSessionId:"+serverSessionId);
        var totalSessionCount = manager.getTotalSessionCount("__joinedTotalSessionCount",serverSessionId);
        if(totalSessionCount<0)return false;
        else __joinedTotalSessionCount(totalSessionCount,serverSessionId);

    }

    function __joinedTotalSessionCount(totalSessionCount,serverSessionId){
        core.info("SeatChatNew.qml | __joinedTotalSessionCount | totalSessionCount:"+totalSessionCount+",serverSessionId:"+serverSessionId)
        if(totalSessionCount==0){
            joinedPublicSessionListModel.clear();
            return false;
        } else {
            var pSessionInfo = manager.getPublicSessionList(0,totalSessionCount-1,"__appendJoinedPublicSessionCallBack",serverSessionId);
            if (pSessionInfo<0)return false;
            else __appendJoinedPublicSessionCallBack(pSessionInfo,serverSessionId);
        }
    }

    function __appendJoinedPublicSessionCallBack(pSessionInfo,serverSessionId){
        core.info("SeatChatNew.qml | __appendJoinedPublicSessionCallBack | serverSessionId:"+serverSessionId)
        for(var i in pSessionInfo){
            if(manager.getHashValue(pSessionInfo[i],"serversessionid") == serverSessionId){
                core.info("seatChat.qml | getPublicChatRooms | unReadMsgCount is " + manager.getHashValue(pSessionInfo[i],"numofunreadmsgs"));
                joinedPublicSessionListModel.append({"session":manager.getHashValue(pSessionInfo[i],"serversessionid"),"sessionName":manager.getHashValue(pSessionInfo[i],"name"),"unReadMsgCount":manager.getHashValue(pSessionInfo[i],"numofunreadmsgs"),"participantCount":manager.getHashValue(pSessionInfo[i],"numofparticipants")})
            }
        }
        sigJoinedPublicSessionListRefreshed();
        return true;
    }

    function getAppActiveState () { return __appSetActiveCompleted&&__appActive?true:false;}
    function getLoginState () { return __appActive?true:false;}

    function setCurrentSessionId(sid){
        var unReadMsgCountOfSession;
        core.info("SeatChatNew.qml | setCurrentSessionId() | currentSessionId:" + sid);
        Info.CurrentSessionId=sid;
        for(var i=0;i<publicsessionListModel.count;i++){
            if(publicsessionListModel.get(i).sessionId == sid){
                unReadMsgCountOfSession = publicsessionListModel.get(i).unReadMsgCount;
                publicsessionListModel.setProperty(i,"unReadMsgCount", 0);
                totalPublicSessionUnReadMessages-=unReadMsgCountOfSession;
                manager.resetUnreadMsgCount(sid);
            }
        }
        for(var i=0;i<sessionListModel.count;i++){
            if(sessionListModel.get(i).session == sid){
                unReadMsgCountOfSession = sessionListModel.get(i).unReadMsgCount;
                sessionListModel.setProperty(i,"unReadMsgCount", 0);
                totalPrivateSessionUnReadMessages-=unReadMsgCountOfSession;
                manager.resetUnreadMsgCount(sid);
            }
        }
        for(var i=0;i<joinedPublicSessionListModel.count;i++){
            if(joinedPublicSessionListModel.get(i).session == sid){
                joinedPublicSessionListModel.setProperty(i,"unReadMsgCount", 0);
            }
        }
        core.info("SeatChatNew.qml | setCurrentSessionId() | totalPublicSessionUnReadMessages is " + totalPublicSessionUnReadMessages);
        core.info("SeatChatNew.qml | setCurrentSessionId() | totalPrivateSessionUnReadMessages is " + totalPrivateSessionUnReadMessages);
        __exitSeatChatScreen = false;
        sigCurrentSessionIdChanged(Info.CurrentSessionId);
        return true;
    }
    function sendChatMessage(session,msg){
        msg=__wordFilter(msg);
        manager.sendMessageToSession(session,"",escape(msg));
        if(Info.CurrentSessionId==session)
            var hours = new Date().getHours();
        var hoursLeadingZero = (hours<10)?"0"+hours:hours
        var minutes = new Date().getMinutes();
        var minutesLeadingZero = (minutes<10)?"0"+minutes:minutes;
        var msgTime = hoursLeadingZero+":"+minutesLeadingZero
        __createMsgList(session, pif.getSeatNumber().toUpperCase(),__seatNickname,msg,msgTime,false);
        return true;
    }


    function addCustomMessageToChatHistory(serverSessionId,seatNum,seatName,message){ //Do not use this API to add chat to public
        core.info("SeatChatNew.qml | addCustomMessageToChatHistory | serverSessionId:"+serverSessionId+"message:"+serverSessionId);
        if(serverSessionId==0){core.info("SeatChatNew.qml | addChatMessageToChatHistory  | Not a Valid Session"); return}
        __createMsgList(serverSessionId,seatNum,seatName,message,"",true)
    }

    function sendCabinCrewMessage(messageStr){
        core.info("SeatChatNew.qml | sendCabinCrewMessage | message :"+messageStr);
        messageStr=__wordFilter(messageStr);
        manager.sendConciergeMsg(messageStr);
    }

    function getPublicChatRooms(){
        core.info("SeatChatNew.qml | getPublicChatRooms called");
        var totalSessionCount = manager.getTotalSessionCount("__getTotalSessionPublicChatCallBack")
        if(totalSessionCount<0)return false;
        else {
            return __getTotalSessionPublicChatCallBack(totalSessionCount);
        }
    }

    function __getTotalSessionPublicChatCallBack(totalSessionCount){
        core.info("SeatChatNew.qml | __getTotalSessionPublicChat Called :"+totalSessionCount);
        if(totalSessionCount==0){
            publicsessionListModel.clear();
            totalPublicSessionUnReadMessages = 0;
            sigPublicSessionListRefreshed();
            return false;
        } else {
            var privateSessionCount = manager.getPrivateSessionCount("__getPrivateSessionCountCallBack",totalSessionCount)
            if(privateSessionCount<0) return false
            else{
                return __getPrivateSessionCountCallBack(privateSessionCount,totalSessionCount)
            }
        }
    }

    function __getPrivateSessionCountCallBack(privateSessionCount,totalSessionCount){
        core.info("SeatChatNew.qml | __getTotalSessionPublicChat Called :"+privateSessionCount);
        var pSessionInfo
        if(privateSessionCount==0){
            pSessionInfo = manager.getPublicSessionList(0,totalSessionCount-1,"__getPublicChatRoomsCallBack",totalSessionCount);
            if(pSessionInfo<0)return false;
            else {
                return  __getPublicChatRoomsCallBack(pSessionInfo,totalSessionCount);
            }
        } else {
            var publicSessionCount = totalSessionCount - privateSessionCount;
            pSessionInfo = manager.getPublicSessionList(0,totalSessionCount-1,"__getPublicChatRoomsCallBack",publicSessionCount);
            if(pSessionInfo<0)return false;
            else {
                return  __getPublicChatRoomsCallBack(pSessionInfo,publicSessionCount);
            }
        }
    }

    function __getPublicChatRoomsCallBack(pSessionInfo,publicSessionCount){
        core.info("SeatChatNew.qml | __getPublicChatRoomsCallBack Called | publicSessionCount:"+publicSessionCount);
        publicsessionListModel.clear();
        totalPublicSessionUnReadMessages = 0;
        for(var i in pSessionInfo){
            core.info("SeatChatNew.qml | getPublicChatRooms | unReadMsgCount is " + manager.getHashValue(pSessionInfo[i],"numofunreadmsgs"));
            totalPublicSessionUnReadMessages+= parseInt(manager.getHashValue(pSessionInfo[i],"numofunreadmsgs"),10);
            publicsessionListModel.append({"sessionId":manager.getHashValue(pSessionInfo[i],"serversessionid"),"sessionName":manager.getHashValue(pSessionInfo[i],"name"),"unReadMsgCount":manager.getHashValue(pSessionInfo[i],"numofunreadmsgs"),"participantCount":manager.getHashValue(pSessionInfo[i],"numofparticipants")})
            Info.publicChatRoom[manager.getHashValue(pSessionInfo[i],"serversessionid")]=manager.getHashValue(pSessionInfo[i],"serversessionid");
        }
        if(pif.isActiveSessionsNeeded()){
        Info.sessionForActiveUsers=publicsessionListModel.get(publicSessionCount-1).sessionId
        publicsessionListModel.remove(publicSessionCount-1);
        if(Info.sessionForActiveUsers!="")joinPublicChat(Info.sessionForActiveUsers);
        }
        sigPublicSessionListRefreshed();
        return true
    }

    function __updateJoinedPublicSessionCount(serverSessionId){
        core.info("SeatChatNew.qml | __updateJoinedPublicSessionCount Called");
        var sessionParticipantCount = manager.getSessionParticipantCount(serverSessionId,"","__updateJoinedPublicSessionCountCallback",serverSessionId);
        if(sessionParticipantCount<0)return false
        else __updateJoinedPublicSessionCountCallback(sessionParticipantCount,serverSessionId)
    }

    function __updateJoinedPublicSessionCountCallback(sessionParticipantCount,serverSessionId){
        core.info("SeatChatNew.qml | __updateJoinedPublicSessionCountCallback | sessionParticipantCount:"+sessionParticipantCount+", serverSessionId:"+serverSessionId);
        for(var i=0;i<joinedPublicSessionListModel.count;i++){
            if(joinedPublicSessionListModel.get(i).session==serverSessionId){
                joinedPublicSessionListModel.setProperty(i,"participantCount",sessionParticipantCount);
            }
        }
    }

    function joinPublicChat(sessionId){
        core.info("SeatChatNew.qml | joinPublicChat | sessionId: "+sessionId)
        setCurrentSessionId(sessionId);
        manager.joinPublicSession(sessionId,"");
        chatHistoryModel.clear();
        return true;
    }

    function exitChatSession(session) {
        core.debug("SeatChatNew.qml | exitChatSession | session: "+session);
        if(Info.publicChatRoom[session]) manager.leavePublicSession(session,"");
        else manager.leavePrivateSession(session,"");
        delete Info.__pendingInvites[session];
        chatHistoryModel.clear(); buddyListModel.clear();
        return true;
    }

    // User can switch to some other screen without logging out of seatchat.
    function exitChatScreen(exitScreen) {
        core.info("SeatChatNew.qml | exitChatScreen | exitScreen: "+exitScreen);
        if(exitScreen==undefined){core.error("exitScreen is Undefined.Exiting");return false;}
        __exitSeatChatScreen = exitScreen;
        return true;
    }

    function validateSeatName(seatName){
        core.info("SeatChatNew.qml | validateSeatName | seatName:"+seatName);
        if(!pif.isActiveSessionsNeeded()){core.info("SeatChatNew.qml | validateSeatName, you cant make this request "); return 0}
        for (var i in Info.activeChatUsersNames){
            if(seatName.toUpperCase()==Info.activeChatUsersNames[i].toUpperCase()){
                return false
            }
        }
        return true
    }

    function setSeatName(seatNameStr){
        core.info("SeatChatNew.qml | setSeatName: "+seatNameStr);
        var str=__wordFilter(seatNameStr);
        if(str!=(" "+seatNameStr+" ")) return false;
        core.debug("SeatChatNew.qml | setSeatName | Filtered String: "+escape(seatNameStr));
        manager.setUserAlias(escape(seatNameStr));
        Info.TempSeatInfo["seatNickname"]=seatNameStr;
        return true;
    }

    function setSeatAvatar(seatAvatarStr){
        core.info("SeatChatNew.qml | setUserAvatar | avatar :"+seatAvatarStr)
        manager.setUserAvatar(seatAvatarStr);
        Info.TempSeatInfo["seatAvatar"]=seatAvatarStr;
    }

    // Supported Constants to be used are listed on top.
    function setSeatStatus (seatStatus){
        core.info("SeatChatNew.qml | setSeatStatus | seatStatus :"+seatStatus)
        var status = seatStatus==cAvailable?cAvailable:seatStatus==cAway?cAway:seatStatus==cDoNotDisturb?cDoNotDisturb:seatStatus==cInvisible?cInvisible:seatStatus==cBusy?cBusy:"";
        if (!status) {core.error("seatChat.qml | UnSupported SeatStatus: "+seatStatus); return false;}
        manager.setUserStatus(status);
        Info.TempSeatInfo["seatStatus"]=seatStatus;
        return true;
    }

    function getChatHistory(session) {
        if(session==0){core.error("SeatChatNew.qml | getChatHistory | session:"+session+", Not a valid Session, Exiting");return false;}
        manager.requestSessionHistory(session,"");
    }

    function __getChatHistory(session) {
        core.info("SeatChatNew.qml | __getChatHistory | session:"+session);
        chatHistoryModel.clear();
        __updateBuddyListBySessionId(session);
        var historyInfo = manager.getSessionHistory(session,"","__getChatHistoryCallBack");
        if(historyInfo<0) return false;
        else __getChatHistoryCallBack(historyInfo)
    }

    function __getChatHistoryCallBack(historyInfo){
        core.info("SeatChatNew.qml | __getChatHistoryCallBack Called ");
        for(var i in historyInfo) {
            //core.info ("seatChat.qml | historyInfo: i: "+i+"msg :"+unescape(manager.getHashValue(historyInfo[i],"txtmessage")))
            chatHistoryModel.append({"time":'',"seat":manager.getHashValue(historyInfo[i],"seatnumber").toUpperCase(), "seatName":unescape(manager.getHashValue(historyInfo[i],"seatname")), "msg":unescape(manager.getHashValue(historyInfo[i],"txtmessage")),"externallyAdded":false})
        }
        sigChatHistoryModelCount(chatHistoryModel.count)
    }


    function getPrivateChatHistory(session) {
        core.info("SeatChatNew.qml | getPrivateChatHistory | session: "+session)
        if(session==0){core.error("SeatChatNew.qml | getChatHistory | session:"+session+", Not a valid Session, Exiting");return false;}
        __getPrivateChatHistory(session); }

    function __getPrivateChatHistory(session) {
        chatHistoryModel.clear();
        if(!Info.localChatHistory[session]){
            core.info("SeatChatNew.qml | __getChatHistory | session :"+session+" does not exist")
            __updateBuddyListBySessionId(session);
            sigChatHistoryModelCount(chatHistoryModel.count);
            return;
        }
        var historyInfo = Info.localChatHistory[session];
        core.info("SeatChatNew.qml | historyInfo : ");
        for(var i in historyInfo) {
            //core.info ("seatChat.qml | historyInfo: i: "+i+"msg :"+unescape(historyInfo[i].txtmessage))
            chatHistoryModel.append({"time":historyInfo[i].time,"seat":historyInfo[i].seatnumber.toUpperCase(), "seatName":unescape(historyInfo[i].seatname), "msg":unescape(historyInfo[i].txtmessage),"externallyAdded":historyInfo[i].externallyAdded})
        }
        __updateBuddyListBySessionId(session);
        sigChatHistoryModelCount(chatHistoryModel.count)
    }

    function getBuddyListBySessionId(session){ core.info("seatChat.qml | getBuddyListBySessionId is called..."); __updateBuddyListBySessionId(session); return true; }

    //getAllusers()//Avoid calling this API, as this is called on startup
    function getAllusers() { manager.requestAllUserList(); return true; }


    function validateSeatNum(seatNum,session){
        //return (Info.SeatsNum[seatStr.toUpperCase()] && (!Info.TempBuddyList[seatStr.toUpperCase()] || session==0)) ? true:  false;
        core.info("SeatChat.qml | validateSeatNum | seatNum:"+seatNum+", session: "+session)
        if(session!=0){
            // __updateTempBuddyList(session); //we will do this in __updateBuddyListBySessionId -- Dwayne
            if(!Info.SeatsNum[seatNum.toUpperCase() ]) {core.info("SeatChat.qml | validateSeatNum | Info.SeatsNum | Seat not present in All users list ");return false;}
            else if (Info.TempBuddyList[seatNum.toUpperCase()]){ core.info("SeatChat.qml | validateSeatNum | Seat already present in session "); return false;}
            else if(!Info.__pendingInvites[session]) { core.info("SeatChat.qml | validateSeatNum | No Pending Invites for Session "); return true;}
            else if (!Info.__pendingInvites[session][seatNum.toUpperCase()]) { core.info("SeatChat.qml | validateSeatNum | Seat does not exist in the pending invites, seat has been invited to session ");return true;}
            else { core.info("SeatChat.qml | validateSeatNum | returning false") ;return false;}
        }
        else{
            if(!Info.SeatsNum[seatNum.toUpperCase()] ) {core.info("SeatChat.qml | validateSeatNum | Info.SeatsNum | Seat not present in All users list ");return false;}
            else if (!Info.__pendingInvites[session]) {core.info("SeatChat.qml | validateSeatNum | No Pending Invites for Session ");return true;}
            else if (!Info.__pendingInvites[session][seatNum.toUpperCase()]) {core.info("SeatChat.qml | validateSeatNum | Seat does not exist in the pending invites, seat has been invited to session "); return true;}
            else { core.info("SeatChat.qml | validateSeatNum | returning false") ;return false;}
        }
    }

    function checkSeatExists(seatNum){
        if(!Info.SeatsNum[seatNum.toUpperCase() ]) {
            core.debug("SeatChat.qml | checkSeatExists | Seat not present in All users list seatNum: "+seatNum);return false;
        }else {core.debug("SeatChat.qml | checkSeatExists | Seat present in All users list seatNum: "+seatNum);return true;}
    }

    function sendInvitation(session,seatNumArr,message){
        core.info("SeatChatNew.qml | sendInvitation | session:"+session+",seatNumArr:"+seatNumArr)
        Info.__seatNumArr = [];
        Info.__seatNumArr = seatNumArr;
        Info.__message = message;
        if(Info.SessionCount >= Info.MaxSessionCount && session==0 )return false;
        if(session == 0) {
            var sessionId = manager.createPrivateSession();
            if(sessionId=="") return false;        //Review
            session=sessionId;              //Not needed
        }
        else __sendingInvitation(session)
        return true;
    }

    onSigSessionIdCreatedOnServer:{
        core.log('SeatChat | onSessionIdCreatedOnServer | seatNumArr ='+session);
        if(!Info.createdPrivateSession[session])Info.createdPrivateSession[session]=session;
        __sendingInvitation(session)
    }

    function __sendingInvitation(session){
        for (var x in  Info.__seatNumArr){
            core.debug('SeatChat.qml | onSessionIdCreatedOnServer | seatNumArr ='+ Info.__seatNumArr[x].toUpperCase());
            manager.sendPrivateInvitation(session,"", Info.__seatNumArr[x],escape(Info.__message));
            if(!Info.__pendingInvites[session])    Info.__pendingInvites[session]=[]
            Info.__pendingInvites[session][Info.__seatNumArr[x].toUpperCase()]=Info.__seatNumArr[x].toUpperCase()
        }
        sigInvitationSent(session,Info.__seatNumArr)
    }

    function acceptInvitation(session, seat) {
        core.info("SeatChat.qml |  acceptInvitation | session :"+session+ ", seat :"+seat+"Info.CreateSessionsOnAcceptInvite"+Info.CreateSessionsOnAcceptInvite+"Info.SessionCount :"+Info.SessionCount)
        if(Info.SessionCount <= Info.MaxSessionCount && session!=0 && Info.CreateSessionsOnAcceptInvite){
            // Info.SessionCount=Info.SessionCount+1;
            __appendSessionListModel(session);
            for(var i=0;i<receivedPendingInvitationModel.count;i++){
                if(receivedPendingInvitationModel.at(i).sessionId==session && receivedPendingInvitationModel.at(i).seatNumber.toUpperCase()==seat.toUpperCase())receivedPendingInvitationModel.remove(i,1);
            }
            sigSessionAdded(session)
            core.info("SeatChat.qml | acceptInvitation | Info.SessionCount is "+Info.SessionCount);
            return  (manager.acceptPrivateInvitation(session,"", seat.toUpperCase()))?true:false;
        } else if(Info.SessionCount <= Info.MaxSessionCount && session!=0 && !Info.CreateSessionsOnAcceptInvite){
            for(var i=0;i<receivedPendingInvitationModel.count;i++){
                if(receivedPendingInvitationModel.at(i).sessionId==session && receivedPendingInvitationModel.at(i).seatNumber.toUpperCase()==seat.toUpperCase())receivedPendingInvitationModel.remove(i,1);
            }
            return  (manager.acceptPrivateInvitation(session,"", seat.toUpperCase()))?true:false;
        }
        else {
            core.info("acceptInvitation| Returning false")
            return false}
    }

    function declineInvitation(session,seat) {
        core.info("SeatChat.qml | declineInvitation| session:"+session+", seat :"+seat)
        for(var j=0;j<receivedPendingInvitationModel.count;j++){
            if(receivedPendingInvitationModel.at(j).sessionId==session && receivedPendingInvitationModel.at(j).seatNumber.toUpperCase()==seat.toUpperCase()){
                receivedPendingInvitationModel.remove(j,1);}
        }
        if(Info.CreateSessionsOnAcceptInvite){
            for(var i=0;i<sessionListModel.count;i++){
                if(session==sessionListModel.get(i).session)return false;
            }
        }
        manager.declinePrivateInvitation(session,"", seat.toUpperCase()); return true;
    }

    function clearHistory(){
        core.info("SeatChat.qml | clearHistory() called. ");
        for(var x in Info.localChatHistory){
            delete Info.localChatHistory[x];
        }
        Info.localChatHistory=[];
        chatHistoryModel.clear();
        for(var i=0;i<sessionListModel.count;i++){
            sessionListModel.setProperty(i,"unReadMsgCount", 0);
            manager.resetUnreadMsgCount(sessionListModel.get(i).session);
        }
        totalPrivateSessionUnReadMessages=0;
    }

    function blockSeat(seatArr){
        core.debug("seatChat.qml | blockSeat | blocked list: "+seatArr.join(','));
        for(var i in seatArr) manager.blockUser(seatArr[i].toUpperCase());
        return true;
    }

    function unblockSeat(seatArr){
        core.debug("seatChat.qml | unblockSeat list: "+seatArr.join(','));
        for(var i in seatArr) manager.unblockUser(seatArr[i].toUpperCase())
        return true;
    }

    function blockInvites(){manager.blockInvites(); return true;}

    function unblockInvites(){manager.unblockInvites(); return true;}

    function getTotalBuddyCount(sessionId){
        // This function have to test on rack. As per splat it should return total buddy count. But yet not sure this count will including own  seat or not.
        return manager.getSessionParticipantCount(sessionId,"");
    }

    function exitChat() {
        core.info("SeatChat.qml | exitChat() called")
        for(var i=0;i<blockListModel.count;i++){
            manager.unblockUser(blockListModel.get(i).seat)
        }
        Info.SessionCount=0; setSeatName("");setSeatAvatar("");setSeatStatus(cAvailable);manager.leavePublicSession(Info.sessionForActiveUsers); manager.resetUser();
        setCurrentSessionId(0); sessionListModel.clear(); publicsessionListModel.clear(); buddyListModel.clear();
        blockListModel.clear(); chatHistoryModel.clear(); sessionListWithBuddyList.clear();pendingInvitationList.clear();
        Info.publicChatRoom=[];Info.joinedPublicChatRoom=[]; Info.chatMsgCount=[]; Info.InvitationQueue=[];
        Info.__pendingInvites=[]; allChatUsersModel.clear(); allBuddiesNSessionModel.clear();joinedPublicSessionListModel.clear();
        Info.sessionForActiveUsers="";Info.activeChatUsersNames=[]; Info.sessionCreatorSeatNumber=[];__appActive=false;
        Info.SeatsName=[];Info.TempSeatInfo=[];Info.privateSessions=[];receivedPendingInvitationModel.clear();
        activeChatUsersModel.clear();Info.blockedSeatsArr=[];__initialiseSeatChatData=false;__seatAvatar="";__seatStatus=cAvailable;__appSetActiveCompleted=false;return true;
    }

    function closeCreatedPrivateSession(serverSessionId,message){
        core.info("SeatChatNew.qml | closeCreatedPrivateSession | serverSessionId:"+serverSessionId+",message:"+message);
        if(serverSessionId==undefined || serverSessionId==0){core.info("SeatChatNew.qml | closeCreatedPrivateSession | Not Valid Session, Exiting");return false}
        if(!Info.createdPrivateSession[serverSessionId]){core.info("SeatChatNew.qml | closeCreatedPrivateSession | Session not created by you,Exiting");return false;}
        if(message==undefined)message="";
        manager.closePrivateSession(serverSessionId,message);
        return true;
    }

    function removeSeatFromCreatedPrivateSession(serverSessionId,seatnumber){
        core.info("SeatChatNew.qml | removeSeatFromCreatedPrivateSession | serverSessionId:"+serverSessionId+",seatnumber:"+seatnumber);
        if(serverSessionId==undefined || serverSessionId==0){core.info("SeatChatNew.qml | closeCreatedPrivateSession | Not Valid Session, Exiting");return false}
        if(!Info.createdPrivateSession[serverSessionId]){core.info("SeatChatNew.qml | closeCreatedPrivateSession | Session not created by you,Exiting");return false;}
        manager.removeUserFromPrivateSession(serverSessionId,seatnumber.toUpperCase());
    }

    function getRestrictedWords () {    // return "hi,hello,this,is"
        return Info.RestrictedWords.split("|").join(",");
    }

    function setRestrictedWords (wordstring) {  // Input: "hi,hello,this,is"
        Info.RestrictedWords = wordstring?wordstring.split(",").join("|"):"";   // trim spaces & replacing comma with |
        core.info("SeatChat | setRestrictedWords called | RestrictedWords: "+Info.RestrictedWords);
        return true;
    }

    function __processSeatStatus (status) { return status==cAway?cAway:status==cDoNotDisturb?cDoNotDisturb:status==cInvisible?cInvisible:status==cBusy?cBusy:cAvailable;}

    function __updateUnreadMsgInfo(serverSessionId){
        var msgCount=0;
        var i;
        core.info("seatChat.qml | __updateUnreadMsgInfo() called serverSessionId: " + serverSessionId);
        for(i=0;i<publicsessionListModel.count;i++){
            if(publicsessionListModel.get(i).sessionId == serverSessionId){
                msgCount = publicsessionListModel.get(i).unReadMsgCount;
                msgCount++;
                publicsessionListModel.setProperty(i,"unReadMsgCount", msgCount);
                totalPublicSessionUnReadMessages++;
                core.info("publicsesionListModel  msgCount is  " + msgCount);
            }
        }
        msgCount=0;
        for(i=0;i<sessionListModel.count;i++){
            core.log("serverSessionId is " + serverSessionId + "  sessionListModel.get(i).session is " + sessionListModel.get(i).session);
            if(sessionListModel.get(i).session == serverSessionId){
                msgCount = sessionListModel.get(i).unReadMsgCount;
                core.info("sessionListModel.get(i).unReadMsgCount  is  " + msgCount);
                msgCount++;
                sessionListModel.setProperty(i,"unReadMsgCount", msgCount);
                totalPrivateSessionUnReadMessages++;
                sigSessionListRefreshed();
                core.info("New msgCount set in the model is  " + msgCount);
                core.info("totalPrivateSessionUnReadMessages is " + totalPrivateSessionUnReadMessages);
            }
        }
        msgCount=0;
        for(i=0;i<joinedPublicSessionListModel.count;i++){
            if(joinedPublicSessionListModel.get(i).session == serverSessionId){
                msgCount = joinedPublicSessionListModel.get(i).unReadMsgCount;
                msgCount++;
                joinedPublicSessionListModel.setProperty(i,"unReadMsgCount", msgCount);
                sigJoinedPublicSessionListRefreshed();
                core.info("publicsesionListModel  msgCount is  " + msgCount);
            }
        }
    }

    function __removeUserFromActiveChatUsersList(seatNumber,seatName){
        core.info("SeatChatNew.qml | __removeUserFromActiveChatUsersList | seatnumber :"+seatNumber+",seatName:"+seatName);
        for(var i=0; i<activeChatUsersModel.count;i++){
            if(seatNumber==activeChatUsersModel.get(i).seatNumber){
                activeChatUsersModel.remove(i);
                sigUserExitedSeatChat(seatNumber,seatName);
                sigActiveUserListRefreshed();
            }
        }
    }

    function __updateActiveChatUsersInfo(seatNumber,seatName,seatStatus,avatar){
        core.info("SeatChatNew.qml | __updateActiveChatUsersInfo | seatNumber: "+seatNumber);
        for(var i=0;i<activeChatUsersModel.count;i++){
            if(seatNumber==activeChatUsersModel.get(i).seatNumber){
                if(activeChatUsersModel.get(i).seatName!=seatName)activeChatUsersModel.setProperty(i,"seatName",seatName);
                if(activeChatUsersModel.get(i).seatStatus!=seatStatus)activeChatUsersModel.setProperty(i,"seatStatus",seatStatus);
                if(activeChatUsersModel.get(i).avatar!=avatar)activeChatUsersModel.setProperty(i,"avatar",avatar);
                Info.activeChatUsersNames[seatNumber]=seatName
                sigActiveUserListRefreshed();
            }
        }
    }

    function __handleChatEvent(event,eventInfo) {
        if(!__appActive) return false;
        var responseStatus = manager.getHashValue(eventInfo,"responsestatus");
        var serverSessionId = manager.getHashValue(eventInfo,"serversessionid");
        var seatNum = manager.getHashValue(eventInfo,"seatnumber").toUpperCase();
        var blockSeatNumber = manager.getHashValue(eventInfo,"blockseatnumber").toUpperCase();
        var unBlockSeatNumber = manager.getHashValue(eventInfo,"unblockseatnumber").toUpperCase();
        var seatName = unescape(manager.getHashValue(eventInfo,"seatname"));
        var session = manager.getHashValue(eventInfo,"sessionid");
        var message = unescape(manager.getHashValue(eventInfo,"txtmessage"));
        var inviteResponseMessage = unescape(manager.getHashValue(eventInfo,"message"));
        var seatStatus= __processSeatStatus(manager.getHashValue(eventInfo,"status"));
        var seatState = manager.getHashValue(eventInfo,"state");
        var avatar = manager.getHashValue(eventInfo,"avatar");
        seatState = String(parseInt(seatState,10))!="NaN"?parseInt(seatState,10):0;
        var blockAllInvites= manager.getHashValue(eventInfo,"blockallinvites");
        var creatorSeatNumber = manager.getHashValue(eventInfo,"creatorseatnumber");

        core.info("SeatChat.qml | __handleChatEvent | seatNum: "+seatNum +"  Event: " + event +"  Session: "+serverSessionId)

        switch(event) {
        case 0:{    // ## onchatinvitationreceived: sessionid, seatnumber, seatname
            core.info("SeatChatNew.qml | EVENT onchatinvitationreceived Num 0 | serverSessionId:"+serverSessionId);
            if(serverSessionId!=0 && !Info.CreateSessionsOnAcceptInvite){
                //Info.SessionCount=Info.SessionCount+1;
                __appendSessionListModel(serverSessionId);
                sigSessionAdded(serverSessionId)
                core.info("SeatChatNew.qml | EVENT onchatinvitationreceived | Info.SessionCount is "+Info.SessionCount);
            }
            if (serverSessionId!=0)receivedPendingInvitationModel.append({"sessionId":serverSessionId,"seatNumber":seatNum,"seatName":seatName})
            sigInvitationReceived(seatNum,serverSessionId,seatName,inviteResponseMessage)
        }
        break;
        case 1:{    // ## onchatinvitationaccepted: sessionid, seatnumber
            core.info("SeatChatNew.qml | EVENT onchatinvitationaccepted Num 1");
            //__updateSessionListData();
            sigSentInvitationStatus(seatNum,serverSessionId,1,inviteResponseMessage); __removeFromPendingList (Info.__pendingInvites[serverSessionId],seatNum, serverSessionId)
        }
        break;
        case 2:{    // ## onnewsigChatMessageReceived: sessionid, seatnumber, seatname, txtmessage, time
            core.info("SeatChatNew.qml | EVENT onnewsigChatMessageReceived Num 2")
            core.info("sessionListModel.count is " + sessionListModel.count + " serverSessionId  is  " + serverSessionId + " Info.CurrentSessionId is " + Info.CurrentSessionId);
            if(Info.CurrentSessionId==serverSessionId){
                __createMsgList(serverSessionId, seatNum,seatName, message,manager.getHashValue(eventInfo,"time"),false);
                if(__exitSeatChatScreen){ __updateUnreadMsgInfo(serverSessionId);}
                else {
                    manager.resetUnreadMsgCount(serverSessionId);
                }
            }
            else{ __updateUnreadMsgInfo(serverSessionId);
                if(!Info.localChatHistory[serverSessionId]){
                    Info.localChatHistory[serverSessionId]=[];
                }
                Info.localChatHistory[serverSessionId].push({"seatnumber":seatNum,"seatname":seatName,"time":manager.getHashValue(eventInfo,"time"),"txtmessage":message,"externallyAdded":false})
            }
            sigChatMessageReceived(seatNum, serverSessionId, message);
        }
        break;
        case 3:{    // ## onparticipantadded: serversessionid, sessionid, seatnumber, seatname, state, status, avatar.
            core.info("SeatChatNew.qml | EVENT onparticipantadded Num 3")
           if(Info.joinedPublicChatRoom[serverSessionId])__updateJoinedPublicSessionCount(serverSessionId)
            __updateBuddyListBySessionId(serverSessionId);
            Info.SeatsName[seatNum] = seatName;
            if(Info.sessionForActiveUsers==serverSessionId){
                activeChatUsersModel.append({"seatNumber":seatNum,"seatName":seatName,"seatStatus":seatStatus,"avatar":avatar});
                Info.activeChatUsersNames[seatNum]=seatName;
                sigActiveUserListRefreshed();
            }
            sigBuddyAdd (serverSessionId, seatNum, seatName);
        }
        break;
        case 4:{    // ## onparticipantdeleted: sessionid, seatnumber, seatname,serverSessionId
            core.info("SeatChatNew.qml | EVENT onparticipantdeleted Num 4");
            if(Info.joinedPublicChatRoom[serverSessionId])__updateJoinedPublicSessionCount(serverSessionId)
            __updateBuddyListBySessionId(serverSessionId); sigBuddyExit(seatNum,serverSessionId,seatName); __removeFromPendingList (Info.__pendingInvites[serverSessionId],seatNum, serverSessionId);
            if(Info.sessionForActiveUsers==serverSessionId)__removeUserFromActiveChatUsersList(seatNum,seatName);
            if(Info.activeChatUsersNames[seatNum] || Info.activeChatUsersNames[seatNum]=="")delete Info.activeChatUsersNames[seatNum]
            if(Info.sessionCreatorSeatNumber[serverSessionId]==seatNum){
                core.info("SeatChatNew.qml | onparticipantdeleted | Creator Has Exited ")
                sigCreatorOfSessionExited(serverSessionId,seatNum);
            }
            core.info("sessionListModel.count is " + sessionListModel.count);
        }
        break;
        case 5:{    // ## onuserprofileupdated: sessionid, seatnumber, seatname, state, status
            core.info("SeatChatNew.qml | EVENT onuserprofileupdated Num 5 ");
            if(serverSessionId!=""){
                if(pif.getSeatNumber().toUpperCase()==seatNum.toUpperCase()){
                    __seatNumber=seatNum;__seatNickname=seatName;__seatAvatar=avatar; __seatStatus=seatStatus; __seatState= seatState; //__seatBlockAllInvites=blockAllInvites==1?true:false;
                    Info.SeatsName[seatNum] = seatName;
                    sigMySeatInfoUpdated(seatNum, seatName, seatState, seatStatus, __seatBlockAllInvites, avatar);
                } else {
                    if(serverSessionId==Info.sessionForActiveUsers)__updateActiveChatUsersInfo(seatNum,seatName,seatStatus,avatar);
                    __updateBuddyListBySessionId(serverSessionId);
                    Info.SeatsName[seatNum] = seatName;
                    sigUserInfoUpdated(seatNum, seatName, seatState, seatStatus, blockAllInvites==1?true:false, avatar,serverSessionId);
                }
            }
        }
        break;
        // Depreated
        case 6:{    // ## onsessionadded: sessionid//serverSessionId
            core.info("SeatChatNew.qml | EVENT onsessionadded Num 6");
            core.info("onsessionadded event. Ignoring this event since it is deprecated.")
        }
        break;
        case 7:{    // ## onprivatesessiondeletedfromcache: sessionid//serverSessionId
            core.info("SeatChatNew.qml | EVENT onprivatesessiondeletedfromcache Num 7");
            var status =__removeSessionListModel(serverSessionId);
            if(Info.privateSessions[serverSessionId])delete Info.privateSessions[serverSessionId];
            if(Info.localChatHistory[serverSessionId])delete Info.localChatHistory[serverSessionId];
            if(Info.createdPrivateSession[serverSessionId])delete Info.createdPrivateSession[serverSessionId];
            if(Info.__pendingInvites[serverSessionId])delete Info.__pendingInvites[serverSessionId]
            if(status)sigSessionDeleted(serverSessionId);
        }
        break;
        case 9:{    // ## onuserbuddylistrefreshed:
            core.info("SeatChatNew.qml | EVENT onuserbuddylistrefreshed Num 9");
            if (Info.CurrentSessionId) {
                __updateBuddyListBySessionId(Info.CurrentSessionId);
                //sigBuddyListRefreshed();
            }
        }
        break;
        case 10:{   // ## onsessionparticipantlistrefreshed: sessionid
            core.info("SeatChatNew.qml | EVENT onsessionparticipantlistrefreshed Num 10");
            if(!Info.sessionCreatorSeatNumber[serverSessionId])Info.sessionCreatorSeatNumber[serverSessionId]=creatorSeatNumber;
            if(Info.sessionForActiveUsers==serverSessionId)__updateActiveChatUsers(serverSessionId)
            sigSessionCreatorListRefreshed();
            if (Info.CurrentSessionId) {
                __updateBuddyListBySessionId(serverSessionId);
                //sigBuddyListRefreshed();
            }
        }
        break;
        case 14:{//## onchatinactive
            //None //
        } break;
        case 17:{//None // ## onblockedlistrefreshed
            core.info("SeatChatNew.qml | EVENT onblockedlistrefreshed Num 17");
            if (Info.CurrentSessionId) __updateBuddyListBySessionId(Info.CurrentSessionId);
            __updateBlockList();
        } break;
        case 18:{ //seatnumber, seatname, state , status, blockallinvites // ## onusrprofilereceived
            core.info("SeatChatNew.qml | EVENT onusrprofilereceived Num 18");
            if(pif.getSeatNumber().toUpperCase()==seatNum.toUpperCase()){
                __seatNumber=seatNum;__seatNickname=seatName;__seatAvatar=avatar; __seatStatus=seatStatus; __seatState= seatState; __seatBlockAllInvites=blockAllInvites=='yes'?true:false;
                Info.SeatsName[seatNum] = seatName;
                sigMySeatInfoUpdated(seatNum, seatName, seatState, seatStatus, __seatBlockAllInvites,avatar);
            } else {
                __updateBuddyListBySessionId(serverSessionId);
                Info.SeatsName[seatNum] = seatName;
                sigUserInfoUpdated(seatNum, seatName, seatState, seatStatus, blockAllInvites==1?true:false,avatar,"");
            }

        }
        break;
        case 19:{   //seatnumber, sessionid // ## onchatinvitationrejected
            core.info("SeatChatNew.qml | EVENT onchatinvitationrejected Num 19");
            sigSentInvitationStatus(seatNum,serverSessionId,0,inviteResponseMessage);
            __removeFromPendingList (Info.__pendingInvites[serverSessionId],seatNum, serverSessionId);
            core.info("Info.SessionCount is "+Info.SessionCount);
        }
        break;
        case 20:{ // onsessionready //## responsestatus, serversessionid, sessionid
            core.info("SeatChatNew.qml | EVENT onsessionready Num 20");
            if(!Info.sessionCreatorSeatNumber[serverSessionId])Info.sessionCreatorSeatNumber[serverSessionId]=pif.getSeatNumber().toUpperCase();
            __appendSessionListModel(serverSessionId);
            sigSessionAdded(serverSessionId)
            sigSessionIdCreatedOnServer(serverSessionId)
	    sigSessionCreatorListRefreshed();
            
            core.info("SeatChatNew.qml | EVENT onsessionready | Info.SessionCount is "+Info.SessionCount);
        } break;
        case 21:{ //onpublicsessionjoinsuccess
            core.info("SeatChatNew.qml | EVENT onpublicsessionjoinsuccess Num 21");
            getChatHistory(serverSessionId);
            if(!Info.joinedPublicChatRoom[serverSessionId])__appendJoinedPublicSession(serverSessionId);
            if(!Info.joinedPublicChatRoom[serverSessionId])Info.joinedPublicChatRoom[serverSessionId]=serverSessionId;
            sigJoinPublicSessionStatus(true,serverSessionId);
        }
        break;
        case 22:{ //sessionid, onpublicsessionjoinfailure
            core.info("SeatChatNew.qml | EVENT onpublicsessionjoinfailure Num 22");
            sigJoinPublicSessionStatus(false,serverSessionId);
        }
        break;
        case 23:{//sessionid // ## onpublicsessionexitsuccess
            core.info("SeatChatNew.qml | EVENT onpublicsessionexitsuccess Num 23");
            __updateBuddyListBySessionId(serverSessionId);
            chatHistoryModel.clear();
            if(Info.joinedPublicChatRoom[serverSessionId])__removeJoinedPublicSession(serverSessionId)
            buddyListModel.clear();
            if(Info.joinedPublicChatRoom[serverSessionId]) delete Info.joinedPublicChatRoom[serverSessionId];
            sigExitPublicSessionStatus(true,serverSessionId);
        }
        break;
        case 24:{  //sessionid // ## onpublicsessionexitfailure
            core.info("SeatChatNew.qml | EVENT onpublicsessionexitfailure Num 24");
            sigExitPublicSessionStatus(false,serverSessionId)
        }
        break;
        case 25:{ //None // ## onallchatusersrefreshed
            core.info("SeatChatNew.qml | EVENT onallchatusersrefreshed Num 25");
            __getAllUsers(); sigAllUsers(allChatUsersModel.count);
        }
        break;
        case 27:{
            core.info("SeatChatNew.qml | EVENT onupdusraliasresponded Num 27 responseStatus:" + responseStatus);
            if(responseStatus=="OK"){
                __seatNickname=Info.TempSeatInfo["seatNickname"];
                core.info("__seatNickname is "+ __seatNickname);
                Info.SeatsName[__seatNumber] = __seatNickname;
                sigMySeatInfoUpdated(__seatNumber, __seatNickname, seatState, __seatStatus, __seatBlockAllInvites, __seatAvatar);
            }
        }
        break;
        case 28:{
            core.info("SeatChatNew.qml | EVENT onupdusrstateresponded Num 28");
        }
        break;
        case 29:{
            core.info("SeatChatNew.qml | EVENT onupdusrstatusresponded Num 29 responseStatus: "+responseStatus);
            if(responseStatus=="OK"){
                __seatStatus=Info.TempSeatInfo["seatStatus"];
                core.info("__seatStatus is "+ __seatStatus);
                sigMySeatInfoUpdated(__seatNumber, __seatNickname, seatState, __seatStatus, __seatBlockAllInvites, __seatAvatar);
            }
        }
        break;
        case 30:{
            core.info("SeatChatNew.qml | EVENT onupdusravatarresponded Num 30 responseStatus: "+responseStatus);
            if(responseStatus=="OK"){
                __seatAvatar=Info.TempSeatInfo["seatAvatar"];
                core.info("__seatAvatar is "+ __seatAvatar);
                sigMySeatInfoUpdated(__seatNumber, __seatNickname, seatState, __seatStatus, __seatBlockAllInvites, __seatAvatar);
            }
        }
        break;
        case 31:{sigCommunicationError()} break;
        case 32:{ //onrequestsessionhistoryresponded
            core.info("SeatChatNew.qml | EVENT onrequestsessionhistoryresponded Num 32");
            if (Info.CurrentSessionId==serverSessionId)
                __getChatHistory(serverSessionId)
        }
        break;
        case 33:{ //onpublicsessionsrefreshed
            core.info("SeatChatNew.qml | EVENT onpublicsessionsrefreshed Num 33");
            if(pif.isActiveSessionsNeeded())getPublicChatRooms()
            //do not populate models
            if (!__appSetActiveCompleted) { __appActive=true; __appSetActiveCompleted=true;sigSeatChatAppReady(1); }
        }
        break;
        case 34:{ //onprivatesessionsrefreshed
            core.info("SeatChatNew.qml | EVENT onprivatesessionsrefreshed Num 34");
            __updateSessionListData();
            if (Info.CurrentSessionId) {
                __updateBuddyListBySessionId(Info.CurrentSessionId);
                sigSessionListRefreshed();
            }
        }
        break;
        case 35:{ //onblockuserresponded
            core.info("SeatChatNew.qml | EVENT onblockuserresponded Num 35 | blockSeatNumber: "+blockSeatNumber);
            if (Info.CurrentSessionId) __updateBuddyListBySessionId(Info.CurrentSessionId);
            if(!Info.blockedSeatsArr[blockSeatNumber]){
                Info.blockedSeatsArr[blockSeatNumber]=blockSeatNumber;
            }
            sigUserBlocked(blockSeatNumber);
            for(var i=0;i<receivedPendingInvitationModel.count;i++){
                if(receivedPendingInvitationModel.at(i).seatNumber.toUpperCase()==blockSeatNumber){
                    receivedPendingInvitationModel.remove(i,1);
                    i--;
                }
            }
            for(var j=0;j<pendingInvitationList.count;j++){
                if(pendingInvitationList.at(j).seat==blockSeatNumber){
                    pendingInvitationList.remove(j,1);
                    j--;
                }
            }
        }
        break;
        case 36:{ //onunblockuserresponded
            core.info("SeatChatNew.qml | EVENT onunblockuserresponded Num 36 | unBlockSeatNumber: "+unBlockSeatNumber);
            if (Info.CurrentSessionId) __updateBuddyListBySessionId(Info.CurrentSessionId);
            if(Info.blockedSeatsArr[unBlockSeatNumber]){
                delete Info.blockedSeatsArr[unBlockSeatNumber];
            }
           sigUserUnBlocked(unBlockSeatNumber);
        }
        break;
        case 37:{
            core.info("SeatChatNew.qml | EVENT onblockinvitesresponded Num 37");
            if(responseStatus=="OK"){
                __seatBlockAllInvites=true;
                receivedPendingInvitationModel.clear();
                pendingInvitationList.clear();
            }
        }
        break;
        case 38:{
            core.info("SeatChatNew.qml | EVENT onunblockinvitesresponded Num 38");
            if(responseStatus=="OK"){
                __seatBlockAllInvites=false;
            }
        }
        break;
        case 46:{
            core.info("SeatChatNew.qml | EVENT onparticipantremoved Num 46 | serverSessionId:"+serverSessionId+",creatorSeatNumber: "+creatorSeatNumber);
            __removeSessionListModel(serverSessionId);
            if(Info.privateSessions[serverSessionId])delete Info.privateSessions[serverSessionId];
            if(Info.localChatHistory[serverSessionId])delete Info.localChatHistory[serverSessionId];
            if(Info.createdPrivateSession[serverSessionId])delete Info.createdPrivateSession[serverSessionId];
            if(Info.__pendingInvites[serverSessionId])delete Info.__pendingInvites[serverSessionId];
            sigUserRemoved(serverSessionId,creatorSeatNumber);

        }
        break;
        case 47:{
            core.info("SeatChatNew.qml | EVENT onprivatesessionclosed Num 47 | serverSessionId:"+serverSessionId+", creatorSeatNumber:"+creatorSeatNumber+",message: "+message);
            __removeSessionListModel(serverSessionId);
            if(Info.privateSessions[serverSessionId])delete Info.privateSessions[serverSessionId];
            if(Info.localChatHistory[serverSessionId])delete Info.localChatHistory[serverSessionId];
            if(Info.createdPrivateSession[serverSessionId])delete Info.createdPrivateSession[serverSessionId];
            if(Info.__pendingInvites[serverSessionId])delete Info.__pendingInvites[serverSessionId];
            sigPrivateSessionClosed(serverSessionId,creatorSeatNumber,message);

        }
        break;
        }
    }

    function __updateSessionListData(){
        core.info("SeatChat.qml | __updateSessionListData | Started");
        sessionListModel.clear()
        totalPrivateSessionUnReadMessages = 0;
        Info.SessionCount=0;
        var buddyList;
        var participantList = "";
        var totalSessionCount = manager.getTotalSessionCount("__updateTotalSessionCount");
        if(totalSessionCount<0)return false;
        else __updateTotalSessionCount(totalSessionCount);
    }

    function __updateTotalSessionCount(totalSessionCount){
        core.info("SeatChatNew.qml | __updateTotalSessionCount |  totalSessionCount:"+totalSessionCount)
        if(totalSessionCount==0){
            return false;
        } else {
            var sessionlist = manager.getPrivateSessionList(0,totalSessionCount-1,"__updateTotalSessionList");
            if(sessionlist<0) return false;
            else __updateTotalSessionList(sessionlist)
        }
    }

    function __updateTotalSessionList(sessionlist){
        core.info("SeatChatNew.qml | __updateTotalSessionList called | sessionlist:"+sessionlist.length)
        Info.sessionListArray =  [];
        for (var i in sessionlist){
            Info.sessionListArray.push(manager.getHashValue(sessionlist[i],"serversessionid"));
        }
        if(Info.sessionListArray.length!=0)__getSessionParticipantCountForSession(sessionlist)
    }


    function __getSessionParticipantCountForSession(sessionlist){
        core.info("SeatChatNew.qml | __getSessionParticipantCountForSession | ")
        var sessionParticipantCount = manager.getSessionParticipantCount(Info.sessionListArray[0], "","__getSessionParticipantList",sessionlist);
        if(sessionParticipantCount<0)return false
        else __getSessionParticipantList(sessionParticipantCount,sessionlist);
    }

    function __getSessionParticipantList(sessionParticipantCount,sessionlist){
        core.info(" SeatChatNew.qml | __getSessionParticipantList | sessionParticipantCount:"+sessionParticipantCount)
        if(sessionParticipantCount==0){
            __populateBuddiesForSession("",sessionlist);
            return false
        } else {
            var sessionParticipantList = manager.getSessionParticipantList(Info.sessionListArray[0], "", 0, sessionParticipantCount-1,"__populateBuddiesForSession",sessionlist);
            if(sessionParticipantList<0)return false;
            else __populateBuddiesForSession(sessionParticipantList,sessionlist);
        }
    }

    function __populateBuddiesForSession(sessionParticipantList,sessionlist){
        core.info("SeatChatNew.qml | __populateBuddiesForSession | sessionParticipantList:"+sessionParticipantList+", Info.sessionListArray[0](session)"+Info.sessionListArray[0]+"sessionParticipantList.length:"+sessionParticipantList.length);
        var participantList = "";
        for (var i in sessionlist){
            if(manager.getHashValue(sessionlist[i],"serversessionid") == Info.sessionListArray[0]){
                totalPrivateSessionUnReadMessages+=parseInt(manager.getHashValue(sessionlist[i],"numofunreadmsgs"),10);
                if(Info.DeleteInactiveSessionOnStartup){
                    if(sessionParticipantList.length!=0){
                        for(var j in sessionParticipantList) {
                            var seat = manager.getHashValue(sessionParticipantList[j],"seatnumber").toUpperCase()
                            participantList = seat+","+participantList;
                        }
                        participantList = participantList.substring(0,participantList.length-1)
                        sessionListModel.append( { "session": manager.getHashValue(sessionlist[i],"serversessionid"),"unReadMsgCount":manager.getHashValue(sessionlist[i],"numofunreadmsgs"),"participantSeatNo":(participantList.length!=0)?participantList:"","participantCount":(sessionParticipantList.length!=0)?sessionParticipantList.length:0} )
                        Info.SessionCount=Info.SessionCount+1
                        Info.privateSessions[manager.getHashValue(sessionlist[i],"serversessionid")]=manager.getHashValue(sessionlist[i],"serversessionid")
                    } else {
                        core.info("SeatChat.qml | __updateSessionListData | Exiting session :"+manager.getHashValue(sessionlist[i],"serversessionid"))
                        exitChatSession(manager.getHashValue(sessionlist[i],"serversessionid"))
                    }
                } else if (!Info.DeleteInactiveSessionOnStartup){
                    for( j in sessionParticipantList) {
                        seat = manager.getHashValue(sessionParticipantList[j],"seatnumber").toUpperCase()
                        participantList = seat+","+participantList;
                    }
                    participantList = participantList.substring(0,participantList.length-1)
                    sessionListModel.append( { "session": manager.getHashValue(sessionlist[i],"serversessionid"),"unReadMsgCount":manager.getHashValue(sessionlist[i],"numofunreadmsgs"),"participantSeatNo":(participantList.length!=0)?participantList:"","participantCount":(sessionParticipantList.length!=0)?sessionParticipantList.length:0} )
                    Info.SessionCount=Info.SessionCount+1
                    Info.privateSessions[manager.getHashValue(sessionlist[i],"serversessionid")]=manager.getHashValue(sessionlist[i],"serversessionid")
                }
            }
        }
        Info.sessionListArray.splice(0,1);
        if(Info.sessionListArray.length!=0)__getSessionParticipantCountForSession(sessionlist);
        else delete Info.sessionListArray;
	//delete this after issue is resolved
        for (var k=0;k<sessionListModel.count;k++){
            core.info("SeatChatNew.qml | __populateBuddiesForSession | sessions :"+sessionListModel.get(k).session);
        }
    }
    // This function needs to be removed

    function __getBuddyList(session,callback){ //This API is now not being used and will be removed  ---dwayne
        core.info("SeatChat.qml | __getBuddyList | Started");
        var sessionParticipantCount = manager.getSessionParticipantCount(session, "");
        core.info("SeatChat.qml | __getBuddyList | session:"+session+", sessionParticipantCount: "+sessionParticipantCount);
        if(sessionParticipantCount<=0)return [];
        else {
            var sessionParticipantList = manager.getSessionParticipantList(session, "", 0, sessionParticipantCount-1);
        }
        core.info("SeatChat.qml | __getBuddyList | sessionParticipantList: "+sessionParticipantList);
        return sessionParticipantList;
    }

    function __updateBuddyInInactiveSession(session) {
        var sessionParticipantCount = manager.getSessionParticipantCount(session,"","__updateParticipantCountInInactiveSession",session);
        core.info("SeatChat.qml | __updateBuddyInInactiveSession | session:"+session+", sessionParticipantCount: "+sessionParticipantCount);
        if(sessionParticipantCount<0)return false;
        else __updateParticipantCountInInactiveSession(sessionParticipantCount,session);

    }

    function __updateParticipantCountInInactiveSession(sessionParticipantCount,session){
        core.info("SeatChatNew.qml | __updateParticipantCountInInactiveSession | sessionParticipantCount:"+sessionParticipantCount+"session:"+session)
        if(sessionParticipantCount==0){
            for(var j=0;j<sessionListModel.count;j++){
                if(sessionListModel.get(j).session == session){
                    sessionListModel.setProperty(j,"participantSeatNo", "");
                    sessionListModel.setProperty(j,"participantCount", 0);
                }
            }
            sigSessionListRefreshed();
        } else {
            var sessionParticipantList = manager.getSessionParticipantList(session, "", 0, sessionParticipantCount-1,"__updateBuddyInInactiveSessionCallback",session);
            if(sessionParticipantList<0)return false;
            else __updateBuddyInInactiveSessionCallback(sessionParticipantList,session)
        }
    }

    function __updateBuddyInInactiveSessionCallback(buddies,session){
        var participantList = "";
        for(var i in buddies) {
            core.debug("seatChat.qml | __updateBuddyInInactiveSession |  i: "+i+" manager.getHashValue: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase());
            var seat = manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
            var status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
            if(Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()])Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()].seatno = manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
            if(Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()])Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()].seatAvatar = manager.getHashValue(buddies[i],"avatar")
            if(Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()])Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()].seatName = unescape(manager.getHashValue(buddies[i],"seatname"))
            if(Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()])Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()].seatStatus = status
            participantList = seat+","+participantList;
            core.info("participantList"+participantList);
        }
        participantList =  participantList.substring(0,participantList.length-1)
        core.info("__updateBuddyInInactiveSession  participantList is  " + participantList + " participantCount is  " + buddies.length);
        for(var j=0;j<sessionListModel.count;j++){
            if(sessionListModel.get(j).session == session){
                sessionListModel.setProperty(j,"participantSeatNo", participantList);
                sessionListModel.setProperty(j,"participantCount", buddies.length);
            }
        }
        sigSessionListRefreshed();
    }

    function __updateBuddyListBySessionId(session)   {
        core.info("seatChat.qml | __updateBuddyListBySessionId | currentSessionId="+Info.CurrentSessionId +" From Evnt session="+session)
        if(session!=Info.CurrentSessionId){__updateBuddyInInactiveSession(session); return false;}
        var sessionParticipantCount = manager.getSessionParticipantCount(session,"","__updateParticipantCountInActiveSession",session);
        if(sessionParticipantCount<0)return false;
        else __updateParticipantCountInActiveSession(sessionParticipantCount,session);

    }

    function __updateParticipantCountInActiveSession(sessionParticipantCount,session){
        core.info("SeatChatNew.qml | __updateParticipantCountInInactiveSession | sessionParticipantCount:"+sessionParticipantCount)
        if(sessionParticipantCount==0){
            Info.TempBuddyList=[];
            buddyListModel.clear();
            for(var j=0;j<sessionListModel.count;j++){
                if(sessionListModel.get(j).session == session){
                    sessionListModel.setProperty(j,"participantSeatNo", "");
                    sessionListModel.setProperty(j,"participantCount", 0);
                }
            }
            sigBuddyListRefreshed();
            sigSessionListRefreshed();
        } else {
            var sessionParticipantList = manager.getSessionParticipantList(session, "", 0, sessionParticipantCount-1,"__updateBuddyListBySessionIdCallback",session);
            if(sessionParticipantList<0)return false
            else __updateBuddyListBySessionIdCallback(sessionParticipantList,session)
        }
    }

    function __updateBuddyListBySessionIdCallback(buddies,session){
        core.info("SeatChatNew.qml |__updateBuddyListBySessionIdCallback  | session:"+session);
        var status;
        var participantList = "";
        Info.TempBuddyList=[];
        buddyListModel.clear();
        for(var i in buddies) {
            core.debug("seatChat.qml | __updateBuddyListBySessionIdCallback |  i: "+i+" manager.getHashValue: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase());
            var seat = manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
            participantList = seat+","+participantList;
            Info.TempBuddyList[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
            core.info("participantList"+participantList);
            if(pif.getSeatNumber().toUpperCase() !=manager.getHashValue(buddies[i],"seatnumber").toUpperCase() && manager.getHashValue(buddies[i],"seatnumber")!=""){
                status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                core.debug("seatChat.qml | __updateBuddyListBySessionId | seatnum: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase() +" seatname: "+manager.getHashValue(buddies[i],"seatname") +" seatstate: "+manager.getHashValue(buddies[i],"state") +" seatstatus: "+status +" blockstatus: "+manager.getHashValue(buddies[i],"blockedstatus"));
                Info.SeatsName[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()] = unescape(manager.getHashValue(buddies[i],"seatname"));
                if(Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()])Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()].seatno = manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
                if(Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()])Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()].seatAvatar = manager.getHashValue(buddies[i],"avatar")
                if(Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()])Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()].seatName = unescape(manager.getHashValue(buddies[i],"seatname"))
                if(Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()])Info.SeatsNum[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()].seatStatus = status

                buddyListModel.append({"seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                          "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                          "seatstate":manager.getHashValue(buddies[i],"state"),
                                          "seatstatus":status,
                                          "blockstatus":manager.getHashValue(buddies[i],"blockedstatus"),
                                          "avatar":manager.getHashValue(buddies[i],"avatar")
                                      })
            }
        }
       sigBuddyListRefreshed();
        participantList =  participantList.substring(0,participantList.length-1)
        core.info("SeatChatNew.qml | __updateBuddyListBySessionId()  participantList is  " + participantList + " participantCount is  " + buddies.length);
        for(var j=0;j<sessionListModel.count;j++){
            if(sessionListModel.get(j).session == session){
                sessionListModel.setProperty(j,"participantSeatNo", participantList);
                sessionListModel.setProperty(j,"participantCount", buddies.length);
            }
        }
        sigSessionListRefreshed();
    }

    function __updateActiveChatUsers(session)   {
        core.info("seatChat.qml | __updateBuddyListBySessionId | currentSessionId="+Info.CurrentSessionId +" From Evnt session="+session)
        var sessionParticipantCount = manager.getSessionParticipantCount(session,"","__updateActiveChatUsersCount",session);
        if(sessionParticipantCount<0)return false;
        else __updateActiveChatUsersCount(sessionParticipantCount,session);
    }
    function __updateActiveChatUsersCount(sessionParticipantCount,session){
        core.info("SeatChatNew.qml | __updateParticipantCountInInactiveSession | sessionParticipantCount:"+sessionParticipantCount)
        if(sessionParticipantCount==0){
            activeChatUsersModel.clear();
            sigActiveUserListRefreshed();
        } else {
            var sessionParticipantList = manager.getSessionParticipantList(session, "", 0, sessionParticipantCount-1,"__updateActiveChatUserlistCallBack",session);
            if(sessionParticipantList<0)return false
            else __updateActiveChatUserlistCallBack(sessionParticipantList,session)
        }
    }

    function __updateActiveChatUserlistCallBack(buddies,session){
        core.info("SeatChatNew.qml |__updateBuddyListBySessionIdCallback  | session:"+session);
        var status;
        var participantList = "";
        activeChatUsersModel.clear();
        for(var i in buddies) {
            status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
            core.debug("seatChat.qml | __updateBuddyListBySessionIdCallback |  i: "+i+" manager.getHashValue: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase());
            activeChatUsersModel.append({"seatNumber":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                            "seatName":unescape(manager.getHashValue(buddies[i],"seatname")),
                                            "seatStatus":status,
                                            "avatar":manager.getHashValue(buddies[i],"avatar")
                                        })
            Info.activeChatUsersNames[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=unescape(manager.getHashValue(buddies[i],"seatname"))
        }
        sigActiveUserListRefreshed();
    }

    //      We have added code to update this array in __updateBuddyListBySessionId()
    function __updateTempBuddyList(session){ // Need to remove function
        Info.TempBuddyList=[];
        var buddies = __getBuddyList(session);
        if(buddies.length==0) return false;
        core.debug("seatChat.qml | __updateTempBuddyList |  My seatnumber: "+pif.getSeatNumber().toUpperCase()+" buddies.length="+ buddies.length)
        for(var i in buddies) {
            core.debug("seatChat.qml | __updateTempBuddyList |  i: "+i+" manager.getHashValue: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase());
            if(manager.getHashValue(buddies[i],"seatnumber")!=""){
                var status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                Info.TempBuddyList[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
            }
        }
    }

    function getAllBuddiesNSessionList(){
        core.debug("seatChat.qml | getAllBuddiesNSessionList called");
        allBuddiesNSessionModel.clear()
        var status;
        for(var j = 0; j < Info.SessionCount; j++) {
            if(sessionListModel.count==0) return;
            core.debug("seatChat.qml | getAllBuddiesNSessionList | private chatsession name: "+sessionListModel.get(j).session);
            var buddies = __getBuddyList(sessionListModel.get(j).session)
            if(buddies.length==0) return false;
            for(var i in buddies) {
                if(pif.getSeatNumber().toUpperCase() !=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
                        && manager.getHashValue(buddies[i],"seatnumber")!="" ){
                    status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                    core.debug("seatChat.qml | getAllBuddiesNSessionList | seatnum: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase() +" session: "+sessionListModel.get(j).session +" seatname: "+manager.getHashValue(buddies[i],"seatname") +" seatstate: "+manager.getHashValue(buddies[i],"state") +" seatstatus: "+status +" blockstatus: "+manager.getHashValue(buddies[i],"blockedstatus"));
                    allBuddiesNSessionModel.append({ "session":sessionListModel.get(j).session,
                                                       "seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                                       "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                                       "seatstate":manager.getHashValue(buddies[i],"state"),
                                                       "status":manager.getHashValue(buddies[i],"status"),
                                                       "blockstatus":manager.getHashValue(buddies[i],"blockedstatus"),
                                                       "avatar":manager.getHashValue(buddies[i],"avatar")   })
                }

            }
        }
        return true;
    }

    function getallBuddiesNBlockList(){
        core.debug("getallBuddiesNBlockList called");
        allBuddiesNBlockListModel.clear();
        allActiveBuddiesModel.clear();
        var tmpSeatNumber=[]; var status;
        for(var j = 0; j < Info.SessionCount; j++) {
            core.debug("getallBuddiesNBlockList | getallBuddiesNBlockList | private chatsession name: "+sessionListModel.get(j).session);
            var buddies = __getBuddyList(sessionListModel.get(j).session)
            if(buddies.length==0) continue;
            for(var i in buddies) {
                if(pif.getSeatNumber().toUpperCase() !=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
                        && manager.getHashValue(buddies[i],"seatnumber")!=""
                        && !tmpSeatNumber[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]){
                    status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                    core.debug("seatChat.qml | getallBuddiesNBlockList | seatnum: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase()+" session: "+sessionListModel.get(j).session+" seatname: "+manager.getHashValue(buddies[i],"seatname")+" seatstate: "+manager.getHashValue(buddies[i],"state") +" seatstatus: "+status +" blockstatus: "+manager.getHashValue(buddies[i],"blockedstatus"));
                    allBuddiesNBlockListModel.append({  "session":sessionListModel.get(j).session,
                                                         "seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                                         "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                                         "seatstate":manager.getHashValue(buddies[i],"state"),
                                                         "status":manager.getHashValue(buddies[i],"status"),
                                                         "blockstatus":manager.getHashValue(buddies[i],"blockedstatus"),
                                                         "avatar":manager.getHashValue(buddies[i],"avatar") })
                    if(manager.getHashValue(buddies[i],"blockedstatus") != 1)
                        allActiveBuddiesModel.append({   "seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                                         "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                                         "seatstate":manager.getHashValue(buddies[i],"state"),
                                                         "status":manager.getHashValue(buddies[i],"status"),
                                                         "avatar":manager.getHashValue(buddies[i],"avatar")})
                    tmpSeatNumber[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=1;
                }

            }
        }
        for(var j in Info.joinedPublicChatRoom) {
            core.info("SeatChat.qml | getallBuddiesNBlockList | public chatsession name: "+j+"Info.joinedPublicChatRoom :"+Info.joinedPublicChatRoom[j]);
            var buddies = __getBuddyList(j);
            if(buddies.length==0) continue;
            for(var i in buddies) {
                core.debug("seatChat.qml | getallBuddiesNBlockList |  i: "+i+" manager.getHashValue: "+manager.getHashValue(buddies[i],"seatnumber"));
                if(pif.getSeatNumber().toUpperCase() !=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
                        && manager.getHashValue(buddies[i],"seatnumber")!=""
                        && !tmpSeatNumber[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]){
                    status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                    core.debug("seatChat.qml | getallBuddiesNBlockList | seatnum: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase() +" session: "+j +" seatname: "+manager.getHashValue(buddies[i],"seatname") +" seatstate: "+manager.getHashValue(buddies[i],"state") +" seatstatus: "+status +" blockstatus: "+manager.getHashValue(buddies[i],"blockedstatus"));
                    allBuddiesNBlockListModel.append({ "session":j,
                                                         "seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                                         "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                                         "seatstate":manager.getHashValue(buddies[i],"state"),
                                                         "seatstatus":status,
                                                         "blockstatus":manager.getHashValue(buddies[i],"blockedstatus"),
                                                         "avatar":manager.getHashValue(buddies[i],"avatar")
                                                     })
                    tmpSeatNumber[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=1;
                }
            }
        }

        var blockedUserCount = manager.getBlockedUserCount()
        if (blockedUserCount<=0)return 0;
        else{
            var blockList = manager.getBlockedUserList(0,blockedUserCount)
        }
        core.debug("seatChat.qml | getallBuddiesNBlockList | Adding Blocked Users that are not part of any chatroom sessions")
        for(var i in blockList) {
            core.debug("seatChat.qml | getallBuddiesNBlockList | blockList["+i+"] = "+manager.getHashValue(blockList[i],"seatnumber").toUpperCase());
            if( !tmpSeatNumber[manager.getHashValue(blockList[i],"seatnumber").toUpperCase()]) {
                core.debug("seatChat.qml | getallBuddiesNBlockList | seatnum: "+manager.getHashValue(blockList[i],"seatnumber").toUpperCase()
                           +" session: 0 seatname: seatstate: seatstatus: "+cAvailable
                           +" blockstatus: 1");
                allBuddiesNBlockListModel.append({ "session":"0", "seatnum":manager.getHashValue(blockList[i],"seatnumber").toUpperCase(),"seatname":"","seatstate":"","status":cAvailable,"blockstatus":"1","avatar":"" })//review session
            }
        }
        return true;
    }

    function __updateBlockList() {
        var blockedUserCount = manager.getBlockedUserCount("__updateBlockListCount");
        core.info("SeatChat.qml | __updateBlockList | blockedUserCount: "+blockedUserCount)
        if (blockedUserCount<0) return false;
        else{
            __updateBlockListCount(blockedUserCount)
        }
        return true;
    }

    function __updateBlockListCount(blockedUserCount){
        core.info("SeatChatnew.qml | __updateBlockListCount | blockedUserCount: "+blockedUserCount)
        if(blockedUserCount==0){
            blockListModel.clear();
            Info.blockedSeatsArr=[];
            return false;
        } else {
            var blockList = manager.getBlockedUserList(0,blockedUserCount-1,"__updateBlockListCallBack");
            if(blockList<0)return false;
            else __updateBlockListCallBack(blockList)
        }
    }

    function __updateBlockListCallBack(blockList){
        blockListModel.clear();
        var seatNo;
        for(var i in blockList){
            blockListModel.append( {"seat": manager.getHashValue(blockList[i],"seatnumber").toUpperCase(),"seatname":unescape(manager.getHashValue(blockList[i],"seatname")), "seatStatus":manager.getHashValue(blockList[i],"status"),"avatar":manager.getHashValue(blockList[i],"avatar"),"blockedstatus":manager.getHashValue(blockList[i],"blockedstatus")});
            seatNo = manager.getHashValue(blockList[i],"seatnumber").toUpperCase()
            if(!Info.blockedSeatsArr[seatNo]){
                Info.blockedSeatsArr[seatNo]=seatNo;
            }
        }
    }

    function __createMsgList(session,seatNum,seatName,message,msgTime,addedExternally){
        core.info("seatChat.qml | __createMsgList | seatNum: "+seatNum+" Message: "+message);
        if(!Info.localChatHistory[session]){
            Info.localChatHistory[session]=[];
        }
        Info.localChatHistory[session].push({"seatnumber":seatNum,"seatname":seatName,"time":msgTime,"txtmessage":message,"externallyAdded":addedExternally})
        if(Info.CurrentSessionId==session)chatHistoryModel.append({"time":msgTime,"seat":seatNum.toUpperCase(), "seatName":unescape(seatName), "msg":message,"externallyAdded":addedExternally})
        return true;
    }

    function __wordFilter(text) {
        if (!Info.RestrictedWords) return " "+text+" ";
        core.debug ("SeatChat.qml | __wordFilter | Info.RestrictedWords: "+Info.RestrictedWords);
        var myRegExp = '((?=\\W)|\\b)('+Info.RestrictedWords.replace(/([^\w\|])/g, '\\$1')+')((?=\\s)|\\b)'
        var regFilter = new RegExp(myRegExp, 'gim');
        return (" "+text+" ").replace(regFilter, function (word){
            return word.replace(/./g, '*');
        });
    }

    function __getAllUsers (){
        allChatUsersModel.clear();
        Info.SeatsNum = [];
        var allChatUserCount = manager.getAllChatUserCount("__getAllUsersCount");
        core.info("SeatChat.qml | __getAllUsers | allChatUserCount is "+allChatUserCount);
        if(allChatUserCount<0)return false;
        else{
            __getAllUsersCount(allChatUserCount)
        }
    }

    function __getAllUsersCount(allChatUserCount){
        if(allChatUserCount==0)return false;
        else var allChatUser= manager.getAllChatUserListWithRange(0,allChatUserCount-1,"__getAllUsersCallBack");
        if(allChatUser<0)return false
        else __getAllUsersCallBack(allChatUser)
    }

    function __getAllUsersCallBack(allChatUser){
        var seatno;
        allChatUsersModel.clear();
        Info.SeatsNum = [];
        for(var i in allChatUser) {
            seatno = manager.getHashValue(allChatUser[i],"seatnumber").toUpperCase();
            Info.SeatsNum[seatno] = {
                "seatno":seatno,
                "seatName":manager.getHashValue(allChatUser[i],"seatname"),
                "seatStatus":manager.getHashValue(allChatUser[i],"status"),
                "seatAvatar":manager.getHashValue(allChatUser[i],"avatar")
            }
            allChatUsersModel.append({'seat':manager.getHashValue(allChatUser[i],"seatnumber").toUpperCase(), 'seatname':unescape(manager.getHashValue(allChatUser[i],"seatname")),'seatstate':manager.getHashValue(allChatUser[i],"state"), 'status':__processSeatStatus(manager.getHashValue(allChatUser[i],"status")),'avatar':manager.getHashValue(allChatUser[i],"avatar") })
            Info.SeatsName[manager.getHashValue(allChatUser[i],"seatnumber").toUpperCase()] = unescape(manager.getHashValue(allChatUser[i],"seatname"));
        }
        core.info("__getAllUsers allChatUsersModel.count: "+allChatUsersModel.count);
    }

    function __removeFromPendingList(arr,seatNum,session){
        core.info("SeatChat.qml | __removeFromPendingList | seatNum :"+seatNum+", session:"+session)
        seatNum=seatNum.toUpperCase();
        for(var x in arr) {
            if(seatNum==Info.__pendingInvites[session][seatNum]){
                delete Info.__pendingInvites[session][seatNum]; break;
            }
        }
    }

    Component.onCompleted: {
        if (core.pc){
            if(pif.__getCompList().indexOf("SeatChatNewApis")>=0){
                seatChatManager = Qt.createQmlObject('import QtQuick 1.1;import "../testpanel"; ChatManagerNewSIM{onChatEvent:{if(pif.isSeatChatOnVkarma())pif.seatInteractive.sendPropertyData("eventInfoData",{"event":event,"eventInfo":eventInfo,"uniqueId":Math.random()});else __handleChatEvent(event,eventInfo);}}',seatChat);
            }
        }
        else {
            if(pif.__getCompList().indexOf("SeatChatNewApis")>=0)seatChatManager = chatManagerComp.createObject(seatChat);
        }
        __seatChatVkarma = (pif.__getCompList().indexOf("SeatChatVkarma")>=0)?true:false
        core.info ("seatChat.qml | Chat Manager Reference: "+manager);
        setRestrictedWords(pif.__getRestrictedWords());       // "hi,hello,this,is"
        Info.MaxSessionCount=pif.__getMaxSessionCount();Info.TempSeatInfo=[];
        Info.CreateSessionsOnAcceptInvite=pif.__sessionsOnAcceptInviteStatus();
        Info.DeleteInactiveSessionOnStartup=pif.__getDeleteInActiveSessionsStatus();
        core.info("seatChat.qml Component On Completed | Info.CreateSessionsOnAcceptInvite:"+Info.CreateSessionsOnAcceptInvite+", Info.DeleteInactiveSessionOnStartup: "+Info.DeleteInactiveSessionOnStartup+", Info.MaxSessionCount :"+Info.MaxSessionCount)
    }

    ListModel {id: sessionListModel}
    ListModel {id: publicsessionListModel}
    ListModel {id: buddyListModel}
    ListModel {id: blockListModel}
    ListModel {id: chatHistoryModel}
    ListModel {id: sessionListWithBuddyList}
    ListModel {id:allChatUsersModel}
    ListModel {id:allBuddiesNSessionModel}
    ListModel {id:allActiveBuddiesModel}
    SimpleModel {id:allBuddiesNBlockListModel}
    SimpleModel {id: pendingInvitationList }
    SimpleModel {id: receivedPendingInvitationModel}
    ListModel{id:joinedPublicSessionListModel}
    ListModel{id:activeChatUsersModel}

    SeatChatManagerApis{
        id:manager;
    }

    Component {
        id: chatManagerComp;
        ChatManager {
            id: seatChatManager1
            chatServerIp: core.settings.remotePath.split("/")[2];
            gatewayIp: core.settings.remotePath.split("/")[2];
            gatewayPort: "57777";
            listenPort: "57777";
            onChatEvent: {
                if(pif.isSeatChatOnVkarma())pif.seatInteractive.sendPropertyData("eventInfoData",{"event":event,"eventInfo":eventInfo,"uniqueId":Math.random()});
                else __handleChatEvent(event,eventInfo);
            }
        }
    }
}

