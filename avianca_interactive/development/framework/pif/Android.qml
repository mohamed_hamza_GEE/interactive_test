import QtQuick 1.1
import Panasonic.AndroidAccess 1.0
import "../blank.js" as Store

Item {
    property int cBAR_VISIBLE:1;
    property int cBAR_DIM:2;
    property int cBAR_HIDE:3;
    property int cPORTRAIT:0
    property int cLANDSCAPE:1
    property int cINVERTED_PORTRAIT:2
    property int cINVERTED_LANDSCAPE:3
    property int cBAR_IMMERSIVE:4;
    property int cBAR_IMMERSIVE_NONE: 5;
    property int cBAR_IMMERSIVE_STICKY:6;
    property int __barStatusOnINT;
    property alias androidNotificationModel: androidNotificationModel

    signal sigNotificationReceived();
    signal sigCustomIntentReceived(string intent_uri);

    function getSystemBarVisibility(){return aui.systemBarVisible}
    function setWallpaperImageFile(path){aui.wallpaperImageFile = path;}
    function getFontName(){return font.name}
    function setSystemBarTimeout(val){aui.systemBarTimeout=val; return true;}
    function setSystemBarVisibility(type){
        if(type==cBAR_DIM) aui.systemBarVisibilityType=AndroidUI.SYSTEM_BAR_DIM;
        else if(type==cBAR_HIDE) aui.systemBarVisibilityType=AndroidUI.SYSTEM_BAR_HIDE;
        else aui.systemBarVisibilityType=AndroidUI.SYSTEM_BAR_VISIBLE;
        return true;
    }
    function __interactiveResetAndroid(){ core.info("Android | __interactiveResetAndroid");  return _androidaccesshandler.restartInteractive(); }

    function keepScreenOn(){ androidSettings.flagKeepScreenOn=true;  return true; }
    function keepScreenOff(){androidSettings.flagKeepScreenOn=false; return true;}
    function showNotificationPanel(){ return _androidaccesshandler.showNotificationPanel();}
    function hideNotificationPanel(){ return _androidaccesshandler.hideNotificationPanel();}
    function generateNotification(notifName, autoCancel, smallIcon, largeIconPath, tickerText, contentTitle, contentText, contentInfo, pageName){
        if (notifName==undefined || notifName=="") {core.error("Android.qml | Notification Name not provided. EXITING..."); return false;}
        if (autoCancel==undefined || autoCancel=="") autoCancel = true;
        if (smallIcon==undefined) smallIcon = "";
        if (largeIconPath==undefined) largeIconPath = "";
        if (tickerText==undefined) tickerText = "";
        if (contentTitle==undefined) contentTitle = "";
        if (contentText==undefined) contentText = "";
        if (contentInfo==undefined) contentInfo = "";
        if (pageName==undefined) pageName = "";
        return _androidaccesshandler.generateNotification(notifName, autoCancel, smallIcon, largeIconPath, tickerText, contentTitle, contentText, contentInfo, pageName);
    }
    function generateCustomNotification(notifName, autoCancel, ongoing, showWhen, smallIcon, largeIconPath,tickerText, contentTitle, contentText, contentInfo, contentAction, contentExtra, deleteAction){
        if (notifName==undefined || notifName=="") {core.error("Android.qml | Notification Name not provided. EXITING..."); return false;}
        if (autoCancel==undefined  || autoCancel=="") autoCancel = true;
        if (ongoing==undefined || autoCancel=="") ongoing = true;
        if (showWhen==undefined || autoCancel=="") showWhen = false;
        if (smallIcon==undefined) smallIcon = "";
        if (largeIconPath==undefined) largeIconPath = "";
        if (tickerText==undefined) tickerText = "";
        if (contentTitle==undefined) contentTitle = "";
        if (contentText==undefined) contentText = "";
        if (contentInfo==undefined) contentInfo = "";
        if (contentAction==undefined) contentAction = "";
        if (contentExtra==undefined) contentExtra  = "";
        if (deleteAction==undefined) deleteAction = "";
        core.info("Android.qml | generateCustomNotification | notifName: "+notifName+", autoCancel: "+autoCancel+", ongoing: "+ongoing+", showWhen: "+showWhen);
        core.info("Android.qml | generateCustomNotification | smallIcon: "+smallIcon+", largeIconPath: "+largeIconPath);
        core.info("Android.qml | generateCustomNotification | tickerText: "+tickerText+", contentTitle: "+contentTitle+", contentText: "+contentText+", contentInfo: "+contentInfo);
        core.info("Android.qml | generateCustomNotification | contentAction: "+contentAction+", contentExtra: "+contentExtra+", deleteAction: "+deleteAction);
        return _androidaccesshandler.generateCustomNotification(notifName, autoCancel, ongoing, showWhen, smallIcon, largeIconPath,tickerText, contentTitle, contentText, contentInfo, contentAction, contentExtra, deleteAction);
    }
    function removeNotification(notifName){ return _androidaccesshandler.removeNotification(notifName);}
    function clearAllNotifications(){ return _androidaccesshandler.clearAllNotifications();}
    function updateAutoBarSwitch(flag){aui.autoHideSystemBarOnSwitch=flag;}
    function setLocale(localeLang){
        if(!localeLang || localeLang==""){core.error("Android | localeLang not provided. EXITING..."); return false;}
        androidSettings.locale =localeLang; return true;}
    function resisterCustomIntent(intent_uri){
        core.log("Android.qml | resisterCustomIntent | intent_uri: "+intent_uri);
        if(intent_uri=="" || !intent_uri) return false;
        ail.registerIntent(intent_uri);
        Store.customIntent[intent_uri]=1;
        return true;
    }
    function enableInputMethod(inputMethod){
        core.info("Android.qml | enableInputMethod | inputMethod: "+inputMethod);
        if(inputMethod=="" || !inputMethod) {core.info("Android.qml | enableInputMethod | No inputMethod passed."); return false;}
        //core.info("Android.qml | enableInputMethod | isInputMethodEnabled = " + androidSettings.isInputMethodEnabled(inputMethod));
        //if(androidSettings.isInputMethodEnabled(inputMethod)) {core.info("Android.qml | enableInputMethod | inputMethod already enabled."); return true;}
        for(var i=0; i<androidSettings.allInputMethods.length; i++){
            //core.info("Android.qml | enableInputMethod | allInputMethods["+i+"] : "+androidSettings.allInputMethods[i]);
            if(androidSettings.allInputMethods[i]==inputMethod){ androidSettings.defaultInputMethod = inputMethod; return true;/*androidSettings.setInputMethodEnabled(inputMethod);*/}
        }
        return false;
    }

    function setScreenOrientation(mode){
        core.info("Android.qml | Setting Android Orientation "+mode);
        if(mode==cPORTRAIT)
            androidSettings.screenOrientation=AndroidSettings.SCREEN_ORIENTATION_PORTRAIT;
        else if(mode==cLANDSCAPE)
            androidSettings.screenOrientation=AndroidSettings.SCREEN_ORIENTATION_LANDSCAPE;
        else if(mode==cINVERTED_PORTRAIT)
            androidSettings.screenOrientation=AndroidSettings.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
        else if(mode==cINVERTED_LANDSCAPE)
            androidSettings.screenOrientation=AndroidSettings.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
    }

    function setImmersiveMode(immersiveMode){
        core.info("Android.qml | setImmersiveMode | mode :"+immersiveMode);
        if(immersiveMode==cBAR_IMMERSIVE)aui.immersiveMode = AndroidUI.IMMERSIVE;
        else if (immersiveMode==cBAR_IMMERSIVE_STICKY)aui.immersiveMode = AndroidUI.IMMERSIVE_STICKY;
        else aui.immersiveMode = AndroidUI.IMMERSIVE_NONE
    }

    function hideDialogBox(packageName){
        core.info("Android.qml | hideDialogBox | packageName:"+packageName)
        aui.confirmPackageForImmersiveMode(packageName);
    }

    function resetAndroid(){
        core.info("Android.qml | Android reset requested");
        androidSettings.resetAndroid();
    }

    function movePopupLauncherToForeground(){
         core.info("Android.qml | movePopupLauncherToForeground");
        _androidaccesshandler.movePopupLauncherToForeground();
    }

    function enableAndroidVKB(){
        core.info("Android.qml | enabling Android VKB");
        aui.softKeyboardEnabled=true;
    }
    
    function disableAndroidVKB(){
        core.info("Android.qml | disabling Android VKB");
        aui.softKeyboardEnabled=false;
    }

    AndroidUI { id: aui; autoHideSystemBarOnSwitch:true }
    AndroidSettings { id:androidSettings }
    AndroidNotificationModel {
        id: androidNotificationModel
        onNotificationReceived: {core.info("Android | onNotificationReceived | sourePackage: "+sourcePackage+" tickerText: "+tickerText);sigNotificationReceived();}
    }
    AndroidIntentListener {
        id: ail
        onReceived:{
            core.info("Android.qml | AndroidIntentListener | action: "+action);
            switch(action){
            case "aero.panasonic.reading_light_toggle":{pif.setReadingLight(!pif.getReadingLight());}
            return;
            case "aero.panasonic.attendant_call_toggle":{pif.setAttendantCall(!pif.getAttendantCall());}
            return;
            }
            for(var i in Store.customIntent){
                if(action==i) {sigCustomIntentReceived(action); return;}
            }
        }
        Component.onCompleted:{
            registerIntent("aero.panasonic.reading_light_toggle");
            registerIntent("aero.panasonic.attendant_call_toggle");
        }
    }
    FontLoader{
        id: defaultFont1;
        Component.onCompleted: {
            source = "/tmp/interactive/fonts/DroidNaskhUI-Regular.ttf";
        }
    }

    FontLoader{
        id: defaultFont2;
        Component.onCompleted: {
            source = "/tmp/interactive/fonts/DroidSansFallbackFull.ttf";
        }
    }

    Component.onCompleted: {
        _paeventhandler.enabled = false;
        androidSettings.flagKeepScreenOn=true
        Store.customIntent=new Array();
      //  if(core.coreHelper.fileExists("/tmp/interactive/fonts/DroidSansFallback.ttf"))defaultFontComp.createObject(parent);
    }
}
