import QtQuick 1.1
import Panasonic.Pif 1.0

VideoMediaPlayer{
    property variant deviceMngr;
    property bool __prePlayState:false;
    property bool __withBGAudioParams:false;
    property string mediaSource:"broadcastVideo";

    onSigAggPlayBegan:{
        if (getSoundtrackEnabled()) pif.setVolumeSource(pif.cVOD_VOLUME_SRC); else pif.setVolumeSource(pif.cAOD_VOLUME_SRC);
        if (__prePlayState) __prePlayState=false; else pif.screenSaver.stopScreenSaver();
    }
    onSigAggPlayStopped: {
        if (__prePlayState) {
            if (getSoundtrackEnabled() && __withBGAudioParams && (pif.getLSVmediaSource() != mediaSource)) pif.unSuspendAudioPlay();
            else if (!getSoundtrackEnabled() && !__withBGAudioParams) pif.suspendAudioPlay();
            __withBGAudioParams=false;
        } else {
            if (getSoundtrackEnabled()) pif.unSuspendAudioPlay();
            if(pif.getLSVmediaSource()==mediaSource) pif.screenSaver.startScreenSaver();
        }
    }

    Connections{target:pif; onSigFallbackMode: if(fallbackMode) frmwkResetApp();}

    function play (params) {
        core.info("BroadcastVideo.qml | play called | params: "+params);
        params = __validateParams(params); if (!params) return false;
        if (pif.getLSVmediaSource() != mediaSource) pif.__setLastSelectedVideo(pif.broadcastVideo);
        if (__mediaState!=cMEDIA_STOP) { __prePlayState=true; __withBGAudioParams = params.withBGAudio; __mediaForceStopAck();}
        else if (!params.withBGAudio) pif.suspendAudioPlay();
        var s; for (var i in params) s+=i+" = "+params[i]+", "; core.debug(s);
        __setMediaModelByMid(params.channelID, params.mediaType, params.duration, params.cid);
        return __mediaPlay(params,pif.__getPipReference());
    }

    function stop() {return __mediaStop();}
    function restart() { return __mediaRestart();}
    function setPIPByID (pipID) {return __setPIPByID(pipID,__getPipReference());}
    function setPIPByArray(pipArr) { return __setPIPByArray(pipArr);}
    function pipOn() { return __pipOn();}
    function pipOff() { return __pipOff();}

    function frmwkInitApp () {__timerInterval = __getVideoPlayTimerInterval();}
    function frmwkResetApp(){__mediaStop();}

    // Private Functions
    function __validateParams (p) {
        p.channelID = (String(parseInt(p.channelID,10))!="NaN")?parseInt(p.channelID,10):0;
        if (p.channelID==0){core.error ("BroadcastVideo.qml | CHANNEL ID NOT PROVIDED. EXITING..."); return false;}
        p.channelID = p.channelID - 1;		// Since pif reads lru.cfg for channels and it's zero based so need to decreament by 1
        p.pipID = (String(parseInt(p.pipID,10))!="NaN")?parseInt(p.pipID,10):0;
        if (p.pipID==0){core.error ("BroadcastVideo.qml | PIP INDEX NOT PROVIDED. EXITING..."); return false;}
        p.withBGAudio = p.withBGAudio?true:false;
        p.soundtrackEnabled = !p.withBGAudio;
        p.displayChNo = (p.displayChNo)?p.displayChNo:'0';
        p.ignoreSystemAspectRatio = p.ignoreSystemAspectRatio?p.ignoreSystemAspectRatio:false;
        p.stretch = p.stretch!=undefined?p.stretch:__getDefaultStretchState();
        p.cid = p.cid?p.cid:0;
        p.elapseTime = 0;
        p.sndtrkLid = String(parseInt(p.sndtrkLid,10))!="NaN"?parseInt(p.sndtrkLid,10):1;      // default 1, required for paxus
        p.sndtrkType= String(parseInt(p.sndtrkType,10))!="NaN"?parseInt(p.sndtrkType,10):0;
        p.subtitleLid = String(parseInt(p.subtitleLid,10))!="NaN"?parseInt(p.subtitleLid,10):0;
        if (p.subtitleLid==-1 || p.subtitleLid==0) {p.subtitleEnabled = false; p.subtitleLid=0;} else p.subtitleEnabled = true;
        p.subtitleType= p.subtitleEnabled?String(parseInt(p.subtitleType,10))!="NaN"?parseInt(p.subtitleType,10):2:0;
        p.amid = (String(parseInt(p.amid,10))!="NaN")?p.amid:0;
        p.duration = (p.duration==undefined)?"00:00:00":p.duration;
        p.elapseTimeFormat = (String(parseInt(p.elapseTimeFormat,10))!="NaN")?parseInt(p.elapseTimeFormat,10):1;
        p.playIndex = 0;
        p.mediaType = p.mediaType?p.mediaType:"broadcastVideo";
        p.isTrailer = false;
        p.aspectRatio = p.aspectRatio?p.aspectRatio:"";
        p.playType = p.playType?p.playType:0;
        if(p.rating==undefined)core.error("BroadcastVideo.qml | Missing rating. Setting it to 254.");
        p.rating = String(parseInt(p.rating,10))!="NaN"?parseInt(p.rating,10):254;
        p.vbMode=(p.vbMode==undefined)?false:p.vbMode;
        return p;
    }

    Component.onCompleted: __playerType = "Broadcast Video Media Player";
}
