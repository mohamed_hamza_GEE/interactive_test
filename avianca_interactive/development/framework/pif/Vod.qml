import QtQuick 1.1
import Panasonic.Pif 1.0

VideoMediaPlayer{
    property variant deviceMngr;
    property bool __prePlayState:false;
    property string mediaSource:"vod";
    signal sigVideoSourceChanged(int mid,int duration,int adMedia,int trickPlay);

    onSigAggPlayBegan: {pif.setVolumeSource(pif.cVOD_VOLUME_SRC); if (__prePlayState) __prePlayState=false; else pif.screenSaver.stopScreenSaver();}
    onSigAggPlayStopped: {
        __getAllVodPlayInfo();
        if(getIsTrailer()) __isTrailer=false;
        if (!__prePlayState) {if(pif.getLSVmediaSource() == mediaSource){ pif.unSuspendAudioPlay(); pif.screenSaver.startScreenSaver();}}
    }

    Connections{target:pif; onSigVideoSSRC:sigVideoSourceChanged(mid,duration,adMedia,trickPlay);}
    Connections{target:dataController.mediaServices; onSigMidBlockChanged:__stopBlockedMid(mid,status);}
    Connections{target:pif; onSigFallbackMode: if(fallbackMode) frmwkResetApp();
        onSigStreamerGroupStatusChanged: if(groupStatusChanged){__checkAndStopPlayingInactiveMid();}
    }

    function play (amid, params, resume) {  // optional: resume (default:false)
        core.info("vod.qml | play called | amid: "+amid+" resume: "+resume+" params: "+params);
        params = __validateParams(params,parseInt(amid,10),parseInt(amid,10),resume); if (!params) return false;
        params.isTrailer = (!params.isTrailer)?false:true;
        return __playVod(params);
    }

    function playByModel (model, params, resume) {
        core.info("vod.qml | playByModel called | resume: "+resume+" params: "+params);
        if (!model || !model.count) {core.error ("Vod.qml | Empty Model or Model not provided. EXITING..."); return false;}
        params.playIndex = (params.playIndex==undefined || params.playIndex>=model.count)?undefined:params.playIndex;
        var mid=parseInt(model.at(params.playIndex!=undefined?params.playIndex:0).mid,10);
        params = __validateParams(params,(params.amid?params.amid:parseInt(model.at(0).aggregate_parentmid,10)),mid,resume); if (!params) return false;
        params.isTrailer=false;
        return __playVod(params,model);
    }

    function playTrailer (amid, params) {params.isTrailer=true; return play (amid, params, false);}

    function stop() {return __mediaStop();}

    function suspendPlaying() {
        if (__mediaState==cMEDIA_STOP) return false;
        stop();
        return true;
    }

    function unSuspendPlaying() {return true;}
    function setPlayingSubtitle(lid,subtitleType) { if (lid < 0) return __setSubtitle(0,false); else return __setSubtitle(lid,true,subtitleType);}
    function setPlayingSoundtrack(lid) { return __setSoundtrack(lid);}
    function playNext () { return __mediaNext();}
    function playPrevious () { return __mediaPrevious();}
    function pause () { return __mediaPause();}
    function resume () { return __mediaResume();}
    function forward (retainspeed) { return __mediaForward(!retainspeed?false:true);}
    function rewind (retainspeed) { return __mediaRewind(!retainspeed?false:true);}
    function restart() { return __mediaRestart();}
    function setPIPByID (pipID) {return __setPIPByID(pipID,__getPipReference());}
    function setPIPByArray(pipArr) { return __setPIPByArray(pipArr);}
    function __setVideoSpeeds(speedStr){__skipSpeeds=speedStr;}

    function pipOn() { return __pipOn();}
    function pipOff() { return __pipOff();}

    function skipAdMedia(skip_one){   /* skip_one=false will skip all ads;skip_one=true will skip only current ad */
        if (__mediaState==cMEDIA_STOP) {
            core.error("Vod.qml | Could not skip ad as the media is already stopped");
            return false;
        }
        if(!skip_one || skip_one==undefined || skip_one=="")
            return __skipAdMedia(false);
        else
            return __skipAdMedia(true);
    }

    // Private Functions
    function __getAllVodPlayInfo() {
        var p=getAllMediaPlayInfo();
        if (!p.isTrailer) {     // Not maintaining trailer mid information.
            var mptr =__getMidArrReference();
            core.debug("Vod.qml | getAllVodPlayInfo | Stored info for amid: "+p.amid+" playIndex: "+p.playIndex+" elapseTime: "+p.elapseTime+" cmid: "+p.cmid);
            mptr[p.amid] = p;
            if(p.amid!=p.cmid){ // to track last played info of each episode mid
                mptr[p.cmid] = p;
                mptr[p.amid]["lastPlayed"]=p.cmid;
            }
            return true;
        } else return false;
    }
    function __playVod (params,model) {   // model optional
        if (!params.isTrailer) {var mptr = __getMidArrReference(); mptr[params.amid] = params;}
        if (pif.getLSVmediaSource() != mediaSource) pif.__setLastSelectedVideo(vod);
        if (__mediaState!=cMEDIA_STOP) {__prePlayState=true; __mediaForceStopAck();}
        else pif.suspendAudioPlay();
        var s; for (var i in params) s+=i+" = "+params[i]+", "; core.debug(s);
        if (model) __setMediaModel(model,params.cid,params.amid,params.mediaType);
        else __setMediaModelByMid(params.amid,params.mediaType,params.duration,params.cid);
        return __mediaPlay(params,__getPipReference());
    }

    function frmwkInitApp () {
        __timerInterval = __getVideoPlayTimerInterval();
        __skipTimerInterval = __getVideoSkipTimerInterval();
        __skipSpeeds = getVideoSpeeds();
    }

    function frmwkResetApp(){__mediaStop();}

    function __validateParams (p,amid,mid,resume) {
        p.amid = (String(parseInt(amid,10))!="NaN")?amid:0;
        if (p.amid==0) {core.error ("Vod.qml | Mising amid... Need it to continue. EXITING..."); return false;}
        var ret=__getMidArrReference(); var ptr=(ret[mid])?ret[mid]:[];
        p.pipID = (p.pipID)?p.pipID:(ptr.pipID?p.pipID:0);
        if (p.pipID==0 && p.webPipArray==undefined){core.error ("Vod.qml | PIP INDEX NOT PROVIDED. EXITING..."); return false;}
        p.duration = (p.duration==undefined)?(ptr.duration?ptr.duration:"00:00:00"):p.duration;
        p.elapseTimeFormat = (String(parseInt(p.elapseTimeFormat,10))!="NaN")?parseInt(p.elapseTimeFormat,10):(ptr.elapseTimeFormat?ptr.elapseTimeFormat:1);
        p.cid = (String(parseInt(p.cid,10))!="NaN")?parseInt(p.cid,10):(ptr.cid?ptr.cid:0);
        p.sndtrkLid = (String(parseInt(p.sndtrkLid,10))!="NaN")?parseInt(p.sndtrkLid,10):(ptr.sndtrkLid?ptr.sndtrkLid:0);
        p.sndtrkType=ptr.sndtrkType?ptr.sndtrkType:2;
        p.subtitleLid = (String(parseInt(p.subtitleLid,10))!="NaN")?parseInt(p.subtitleLid,10):0;
        if (p.subtitleLid==-1) {p.subtitleEnabled = false; p.subtitleLid=0;}
        else if (p.subtitleLid==0) {
            if (ptr.subtitleLid==undefined) {p.subtitleEnabled = false; p.subtitleLid=0;}
            else {p.subtitleEnabled = ptr.subtitleEnabled; p.subtitleLid=ptr.subtitleLid;}
        } else p.subtitleEnabled = true;
        p.subtitleType=(p.subtitleType==undefined || p.subtitleType==-1)?(ptr.subtitleType?ptr.subtitleType:2):p.subtitleType;
        p.ignoreSystemAspectRatio = (p.ignoreSystemAspectRatio!=undefined)?p.ignoreSystemAspectRatio:(ptr.ignoreSystemAspectRatio?p.ignoreSystemAspectRatio:false);
        p.stretch = (p.stretch!=undefined)?p.stretch:(ptr.stretch && __getRememberAspectRatio()?ptr.stretch:__getDefaultStretchState());
        p.playIndex=(String(parseInt(p.playIndex,10))!="NaN")?p.playIndex:(ptr.playIndex?ptr.playIndex:0);
        p.elapseTime = (!resume)?0:(ptr.elapseTime?ptr.elapseTime:0);
        p.mediaType = (p.mediaType)?p.mediaType:(ptr.mediaType?ptr.mediaType:"videoAggregate");
        p.playType=p.playType?p.playType:0;// For sting videos, playType is 1 otherwise playType is 0
        p.aspectRatio = p.aspectRatio?p.aspectRatio:"";
        p.soundtrackEnabled = true;
        p.vbMode=(p.vbMode==undefined)?(ptr.vbMode?ptr.vbMode:false):p.vbMode;
        if(p.rating==undefined)core.error("Vod.qml | Missing rating. Setting it to 254.");
        p.rating=(String(parseInt(p.rating,10))!="NaN")?parseInt(p.rating,10):((ptr.rating && ptr.rating!=0)?ptr.rating:254);
        return p;
    }
    function __stopBlockedMid (mid, status){
        core.info("Vod.qml |  Stop Blocked Mid");
        if((status=="blocked")&&(mid==pif.vod.getPlayingAlbumId())){return __mediaStop();}
        else return false;
    }
    function __checkAndStopPlayingInactiveMid(){
        core.info("Vod.qml | __checkAndStopPlayingInactiveMid | Stop unavailable Mid on nbdgroup status change");
        if(!dataController.getMidGroupStatus(pif.vod.getPlayingAlbumId())){return __mediaStop();}
        else return false;
    }
    Component.onCompleted: {
        __playerType = "Vod Media Player";
    }
}
