import QtQuick 1.1
import Panasonic.Pif 1.0
import '../blank.js' as Info

Item{
    id:ppv;
    signal sigCardSwipeNeeded;
    signal sigCardSwipeSuccessful ;
    signal sigCardSwipeError;
    signal sigPaymentSuccessful(int amid);
    signal sigPaymentError;
    signal sigPPVSystemError();
    signal sigPaymentCardTypesReceived(string supportedCardTypes);
    signal sigPaymentHistoryDataReceived(string historyName, string historyData);

    property int __ppvStatus:0
    property int __amid:0;
    property bool __promptBeforePurchase;

    function frmwkInitApp() { core.info("Ppv.qml | frmwkInitApp | frmwkInitApp");  frmwkResetApp(); return true;}
    function frmwkResetApp(){
        paymentToken.resetToken();
        paymentToken.appId="PPV";
        if(__ppvStatus!=0) cancelPaymentProcess();
        __promptBeforePurchase=false;
        return true;
    }

    function getLast4Digits(){ return paymentToken.last4Digits; }

    function purchaseMid(mid,price,currencyType,confirmPurchase){
        var seatNum = core.pif.getSeatNumber();
        core.info("Ppv.qml | purchaseMid | mid="+mid+ " price="+price + " currencyType="+currencyType+" confirmPurchase="+confirmPurchase);
        paymentToken.orderId=mid; __amid=mid; paymentToken.amount=price; paymentToken.currency=currencyType;
        paymentToken.seatNumber = seatNum.toUpperCase();
        __promptBeforePurchase=(confirmPurchase==undefined || confirmPurchase==null || confirmPurchase=="")?false:confirmPurchase==true?true:false
        var params = new Array();params[0] = mid;params[2] = "eng";
        if (core.pc) {__ppvStatus=0; simCardSwipeNeededTimer.restart();}else {core.dataController.getMediaByLanguageIso(params,__process_callback);} return true;
    }

    function getTransactionId(){ return paymentToken.transactionId; }

    function getSwipedCCtype(){return Info.ccTypeArr[paymentToken.cardType]['ccName']}

    function cancelPaymentProcess(){
        core.info("Ppv.qml | cancelPaymentProcess | __ppvStatus="+__ppvStatus);
        if(__ppvStatus==2) __cancelPayment();
        if(__ppvStatus==1) __cancelSwipe();
        __ppvStatus=0;
        __promptBeforePurchase=false;
        return true;
    }

    function getPaymentCardTypes(format){
        core.info("Ppv.qml | getPaymentCardTypes | format: "+format);
        paymentToken.cardTypesFormat=(format.toLowerCase()=="xml")?"xml":"json"
        paymentToken.beginGetCardTypes();
        return true;
    }

    function getPaymentHistory(option,format){
        core.info("Ppv.qml | getPaymentHistory | option: "+option+" | historyFormat: "+format);
        if(option.toLowerCase()!="seat" && option.toLowerCase()!="card" && option.toLowerCase()!="order"){core.info("Ppv.qml | getPaymentHistory | Option not supported"); return false;}
        if(format){if(format=="json_charge" || format=="json_tab") paymentToken.historyFormat=format; else core.info("Ppv.qml | getPaymentHistory | historyFormat not supported");}
        paymentToken.beginGetHistory(option);
        return true;
    }

    function addPaymentMetadata(key,val){
        core.info("Ppv.qml | addPaymentMetadata | key: "+key+" | val: "+val);
        paymentToken.allowAddMetadata=true;
        paymentToken.metadataKey=key;
        paymentToken.metadataValue=val;
        paymentToken.addMetadata();
        paymentToken.allowAddMetadata=false;
        return true;
    }

    function __process_callback(dmodel){
        if(dmodel.count>0){paymentToken.orderDescription = dmodel.getValue(0,"title");}else{sigPPVSystemError();}
        paymentToken.beginSwipe();
    }

    function __onCardSwipeNeededAck(){
        core.info("Ppv.qml | __onCardSwipeNeededAck | __ppvStatus="+__ppvStatus) ;
        if(__ppvStatus!=0) return false; simCardSwipeNeededTimer.stop()
        __ppvStatus=1; sigCardSwipeNeeded(); return true;
    }

    function __onCardSwipeCompletedAck(){
        core.info("Ppv.qml | __onCardSwipeCompletedAck | __ppvStatus="+__ppvStatus);
        if(__ppvStatus!=1) return false;
        simCardSwipeNeededTimer.stop();simCardSwipeNeededTimer.stop(); __ppvStatus=2; sigCardSwipeSuccessful();
        if(!__promptBeforePurchase){
            if(core.pc){simPaymentTimer.restart();}
            else
                paymentToken.processPayment();
        }
        return true;
    }

    /* Call this api only when Interactive has a requirement to confirm purchase after successful credit card swipe */
    function processPayment(){
        core.info("Ppv.qml | processPayment requested | confirmPurchase : "+__promptBeforePurchase);
        if(!__promptBeforePurchase) return;
        if(core.pc){simPaymentTimer.restart();}
        else
            paymentToken.processPayment();
    }

    function __onCardSwipeErrorAck(){ core.info("Ppv.qml | __onCardSwipeErrorAck | __ppvStatus="+__ppvStatus); sigCardSwipeError(); __ppvStatus=0;__promptBeforePurchase=false;simCardSwipeNeededTimer.stop(); simCardSwipeSuccessfulTimer.stop(); return true; }

    function __onPaymentCompleteAck(){
        core.info("Ppv.qml | __onPaymentCompleteAck | __ppvStatus="+__ppvStatus);
        __promptBeforePurchase=false;
        if(__ppvStatus!=2)return false; __ppvStatus=0;
        simCardSwipeNeededTimer.stop();simCardSwipeNeededTimer.stop();simPaymentTimer.stop()
        sigPaymentSuccessful(__amid); return true;
    }

    function __onPaymentErrorAck(){
        core.info("Ppv.qml | __onPaymentErrorAck | __ppvStatus="+__ppvStatus);
        sigPaymentError(); __ppvStatus=0; simCardSwipeNeededTimer.stop();
        simCardSwipeSuccessfulTimer.stop(); simPaymentTimer.stop();
        __promptBeforePurchase=false;
        return true;
    }

    function __cancelSwipe(){ core.info("Ppv.qml | __cancelSwipe | __ppvStatus="+__ppvStatus); paymentToken.cancelTransaction(); simCardSwipeNeededTimer.stop(); simCardSwipeSuccessfulTimer.stop(); return true; }

    function __cancelPayment(){core.info("Ppv.qml | __cancelPayment | __ppvStatus="+__ppvStatus); paymentToken.cancelPayment(); simPaymentTimer.stop(); return true;}

    Timer{
        id:simCardSwipeNeededTimer; interval: 500; repeat: false;
        onTriggered: { __ppvStatus==0 ? ( __onCardSwipeNeededAck() | simCardSwipeSuccessfulTimer.start() ):''; }
    }

    Timer{
        id:simCardSwipeSuccessfulTimer ; interval: 3000; repeat:false;
        onTriggered: {__ppvStatus==1?( __onCardSwipeCompletedAck()):'';}
    }

    Timer{
        id:simPaymentTimer; interval: 3000; repeat:false;
        onTriggered: { __onPaymentCompleteAck();}
    }

    PaymentToken{
        id: paymentToken
        cardTypesFormat:'json'
        appId: "PPV"
        onCardSwipeNeeded: { core.info("Ppv.qml | onCardSwipeNeeded | on CardSwipNeeded received a card swipe needed event"); __onCardSwipeNeededAck();}
        onCardSwipeComplete: {core.info("Ppv.qml | onCardSwipeComplete | on CardSwipNeeded Received a card swipe complete event");__onCardSwipeCompletedAck(); }
        onCardSwipeError: { core.info("Ppv.qml | onCardSwipeError | on CardSwipNeeded Received a card swipe error ");__onCardSwipeErrorAck();  }
        onPaymentComplete: { core.info("Ppv.qml | onPaymentComplete | on CardSwipNeeded Received a payment complete event."); __onPaymentCompleteAck();  }
        onPaymentError: { core.info("Ppv.qml | onPaymentError | on CardSwipNeeded Received a payment error "); __onPaymentErrorAck();  }
        onCardTypesComplete: {
            core.info("Ppv.qml | onCardTypesComplete");
            var supportedCardTypesObj=eval(supportedCardTypes)
            for (var i in supportedCardTypesObj){
                Info.ccTypeArr[supportedCardTypesObj[i]['id']]={ 'ccName': supportedCardTypesObj[i]['name'], 'class':supportedCardTypesObj[i]['class']}
               core.debug("Ppv.qml | onCardTypesComplete | Info.ccTypeArr["+i+"][id]="+Info.ccTypeArr[supportedCardTypesObj[i]['id']])
            }
            sigPaymentCardTypesReceived(supportedCardTypesObj);
        }
        onHistoryDataComplete: {core.info("Ppv.qml | onHistoryDataComplete"); sigPaymentHistoryDataReceived(historyName,historyData);}
        onGetHistoryError: {core.info("Ppv.qml | onGetHistoryError | error: "+errorMsg);}
    }

    Component.onCompleted: { if(!core.pc) paymentToken.beginGetCardTypes(); Info.ccTypeArr=[] }
 }
