import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item{
    property int __prevVodState:pif.vod.cMEDIA_STOP;
    property int __prevAodState:pif.aod.cMEDIA_STOP;
    property variant __audioPlayerdataRef;
    property variant __videoPlayerdataRef;
    property int __playlistId: 0;
    property int __aggregateMid: 0;
    property int __cid: 0
    property string __mediaType: "";
    property int __rating:0;
    property variant __pifRef:pif;
    property bool __ignorePlayBegan:false; // this will ignore updating few shared model properties which are not required when track is resumed provided it was paused before
    property bool executingSeatChatApiRequest:false;
    property bool executingSeatChatApiParamsRequest:false;
    property bool playlistUpdateRequiredOnKarma:true;
    property bool paymentDoneFromSeat: true;

    signal clientConnected();
    signal clientDisConnected();
    signal sigDataReceived(string api,variant params);

    Connections{
        target:pif
        onServiceAccessChanged:{ if(pif.getMonitorType()!=pif.cLRU_VKARMA) sendFrameworkApi('frameworkServiceBlockChange',{serviceMid:serviceMid,accessType:accessType})}
        onMidAccessChanged:{{ if(pif.getMonitorType()!=pif.cLRU_VKARMA) sendFrameworkApi('frameworkMidBlockChange',{mid:mid,accessType:accesstype})}}
        onSeatRatingChanged:{{ if(pif.getMonitorType()!=pif.cLRU_VKARMA) sendFrameworkApi('frameworkSeatRatingChange',{seatRating:seatRating})}}
    }
    Connections{target: core.pif.ppv?core.pif.ppv:null;
        onSigPaymentSuccessful:{
            if(!paymentDoneFromSeat) {paymentDoneFromSeat=true; return true;}
            if(pif.getMonitorType()!=pif.cLRU_VKARMA) sendFrameworkApi('frameworkPaymentSuccessful',{amid:amid})
        }
    }
    function sendApiData(api,params){
        core.info("SharedModelServerInterface.qml | sendApiData("+api+",params)");
        pif.__sharedModelServer.sendApiData(api,params);
    }

    function sendPropertyData(propertyName,propertyValue){
        if(propertyName=="resumeTimeMap" ||propertyName=="midInforArr"){core.info("SharedModelServerInterface.qml | sendPropertyData: "+propertyName);}else {core.info("SharedModelServerInterface.qml | sendPropertyData("+propertyName+","+propertyValue+")");}
        pif.__sharedModelServer.sendPropertyData(propertyName,propertyValue);
    }

    function sendUserDefinedApi(userDefinedApi,apiParams){ if(userDefinedApi.indexOf("userDefined")!=-1) sendApiData(userDefinedApi,(apiParams)?apiParams:''); else core.debug("SharedModelServerInterface.qml | userDefinedApi should be sufficed with word userDefined");}
    function sendFrameworkApi(frameworkApi,apiParams){ if(frameworkApi.indexOf("framework")!=-1) sendApiData(frameworkApi,(apiParams)?apiParams:''); else core.debug("SharedModelServerInterface.qml | frameworkAPI should be sufficed with word framework");}
    function setAudioPlayerdataRef(val){__audioPlayerdataRef=val; return true;}
    function setVideoPlayerdataRef(val){__videoPlayerdataRef=val; return true;}
    function setDefaultValues(){
        sendPropertyData("seatbackVolume",__pifRef.getDefaultAudioVolume())
        sendPropertyData("seatbackPaVolume",__pifRef.getMinPAVolume())
        sendPropertyData("seatbackBrightness",__pifRef.getDefaultBrightness())
    }
    function sendKeyboardData(val){sendPropertyData("keyboardData",val);}
    function showVkb(){sendPropertyData("serverKeyboardVisible",true);}
    function hideVkb(){sendPropertyData("serverKeyboardVisible",false);}

    // reference 2 can be either pif or vip. The below function will change the reference 2. This will be called from Vip.qml
    function __setPifReference(val){
        __pifRef = val;
    }
    function __addingMidToPlaylist(dModel){
        core.info("SharedModelServerInterface.qml | __addingMidToPlaylist | dModel Count :"+dModel.count)
        if(dModel.count && __mediaType=="aod") pif.aodPlaylist.addAllTracks(dModel,__cid,__aggregateMid,__playlistId,__rating);
        else if (dModel.count && __mediaType=="vod")pif.vodPlaylist.addAllTracks(dModel,__cid,__aggregateMid,__playlistId,__rating);
        else if (dModel.count && __mediaType=="kidsaod")pif.kidsAodPlaylist.addAllTracks(dModel,__cid,__aggregateMid,__playlistId,__rating);
        else if (dModel.count && __mediaType=="kidsvod")pif.kidsVodPlaylist.addAllTracks(dModel,__cid,__aggregateMid,__playlistId,__rating);
    }
    function __createPlaylist (params){
        core.info("SMSI.qml | __createPlaylist | playlistIndex :"+params.playlistIndex+" , name : "+params.name+ " , mediaType : "+params.mediaType)
        var plist = pif.__sharedModelServer.playlists;
        if (params.mediaType=="aod"){
            plist.aod[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.aodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        } else if (params.mediaType=="vod"){
            plist.vod[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.vodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        } else if (params.mediaType=="kidsaod"){
            plist.kidsaod[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.kidsAodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        } else if (params.mediaType=="kidsvod"){
            plist.kidsvod[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.kidsVodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        } else if (params.mediaType=="mp3"){
            plist.usb[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.usbPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        pif.__sharedModelServer.playlists = plist;
    }


    function __updatePlaylistName (params){
        core.info("SMSI.qml | __updatePlaylistName | playlistIndex :"+params.playlistIndex+" , name : "+params.name+ " , mediaType : "+params.mediaType)
        var plist  = pif.__sharedModelServer.playlists;
        if (params.mediaType=="aod"){
            var aodPlaylistName = Store.aodPlaylistArr[Math.abs(params.playlistIndex)];
            core.log("aodPlaylistName :"+aodPlaylistName+ "New Name : "+params.name)
            plist.aod[Math.abs(params.playlistIndex) + "|" + params.name] = plist.aod[aodPlaylistName]
            delete plist.aod[aodPlaylistName];
            Store.aodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        else if (params.mediaType=="vod"){
            var vodPlaylistName = Store.vodPlaylistArr[Math.abs(params.playlistIndex)];
            core.log("vodPlaylistName :"+vodPlaylistName+ "New Name : "+params.name)
            plist.vod[Math.abs(params.playlistIndex) + "|" + params.name] = plist.vod[vodPlaylistName]
            delete plist.vod[vodPlaylistName];
            Store.vodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        else if (params.mediaType=="kidsaod"){
            var kidsAodPlaylistName = Store.kidsAodPlaylistArr[Math.abs(params.playlistIndex)];
            core.log("kidsAodPlaylistName :"+kidsAodPlaylistName+ "New Name : "+params.name)
            plist.kidsaod[Math.abs(params.playlistIndex) + "|" + params.name] = plist.kidsaod[kidsAodPlaylistName]
            delete plist.kidsaod[kidsAodPlaylistName];
            Store.kidsAodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        else if (params.mediaType=="kidsvod"){
            var kidsVodPlaylistName = Store.kidsVodPlaylistArr[Math.abs(params.playlistIndex)];
            core.log("kidsVodPlaylistName :"+kidsVodPlaylistName+ "New Name : "+params.name)
            plist.kidsvod[Math.abs(params.playlistIndex) + "|" + params.name] = plist.kidsvod[kidsVodPlaylistName]
            delete plist.kidsvod[kidsVodPlaylistName];
            Store.kidsVodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        pif.__sharedModelServer.playlists = plist;
    }

    Connections{
        target: pif.__sharedModelServer;

        onSigSMClientDisConnected:{clientDisConnected();}
        onSigSMSynchronized:{clientConnected();}
        onSigSMDataReceived:{
            core.info("SharedModelServerInterface.qml | api is " + api + " params is " + JSON.stringify(apiParameters));

            var params = [];
            if(api=="launchMedia" && (apiParameters.mediaType=="vod" || apiParameters.mediaType=="vodAggregate" || apiParameters.mediaType=="broadcastVideo" || apiParameters.mediaType=="extv")){
                params = {mid:apiParameters.identifier,aggregateMid:(apiParameters.aggregateIdentifier==0)?apiParameters.identifier:apiParameters.aggregateIdentifier,mediaType:apiParameters.mediaType,soundtrackLid:apiParameters.soundtrackLid,soundtrackType:(apiParameters.soundtrackType?apiParameters.soundtrackType:2),subtitleLid:apiParameters.subtitleLid,subtitleType:(apiParameters.subtitleType?apiParameters.subtitleType:2),elapsedTime:apiParameters.elapsedTime,cid:apiParameters.cid}
                core.info("Received message from SM Server | apiName: "+ api + " | mid : "+ params.mid + ",mediaType: "+params.mediaType+", aggregateMid : " + params.aggregateMid + ", soundtrackLid : " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType + ", subtitleLid : " + params.subtitleLid + ", subtitleType : " + params.subtitleType + ", elapsedTime : " + params.elapsedTime + ", cid : " + params.cid);
                pif.screenSaver.restartScreenSaver();if(apiParameters.isTrailer){sigDataReceived("playTrailer",params)}else {if(apiParameters.aggregateIdentifier==0){sigDataReceived("playVod",params)}else {sigDataReceived("playVodAggregate",params)}};

            } else if(api=="aspectRatio"){
                params = {aspectRatio:pif.__sharedModelServer.aspectRatio}
                core.info("Received message from SM Server | apiName: "+ api +" | aspectRatio: " + params.aspectRatio);
                if(pif.lastSelectedVideo.getIsScreenAdjustable())pif.lastSelectedVideo.toggleStretchState();
                sigDataReceived(api,params);

            } else if(api=="videoSoundtrack") {
                params = {soundtrackLid:apiParameters.soundtrackLid,soundtrackType:apiParameters.soundtrackType}
                core.info("Received message from SM Server | apiName: "+ api +" | soundtrackLid: " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType);
                pif.vod.setPlayingSoundtrack(params.soundtrackLid);
                sigDataReceived(api,params);

            } else if(api=="videoSubtitle"){
                params = {subtitleLid:apiParameters.subtitleLid,subtitleType:apiParameters.subtitleType}
                core.info("Received message from SM Server | apiName: "+ api +" | subtitleLid: " + params.subtitleLid + ", subtitleType : " + params.subtitleType);
                pif.vod.setPlayingSubtitle(params.subtitleLid,params.subtitleType);
                sigDataReceived(api,params);

            } else if(api=="setMediaPosition"){
                params = {elapsedTime:apiParameters.elapsedTime}
                core.info("Received message from SM Server | apiName: "+ api +" | elapsedTime: " + params.elapsedTime);
                if(pif.vod.getMediaState()==pif.vod.cMEDIA_PLAY){pif.vod.setMediaPosition(params.elapsedTime);}
                else if(pif.lastSelectedAudio.getMediaState()==pif.lastSelectedAudio.cMEDIA_PLAY){pif.lastSelectedAudio.setMediaPosition(params.elapsedTime);}
                sigDataReceived(api,params);

            }else if(api=="controlMedia" && apiParameters.value=="fastForward"){ // forward request to GUI
                core.info("Received message from SM Server | apiName: mediaFastForward "+ api);
                sigDataReceived("mediaFastForward","");
            }else if(api=="controlMedia" && apiParameters.value=="rewind"){ // forward request to GUI
                core.info("Received message from SM Server | apiName: mediaRewind "+ api);
                sigDataReceived("mediaRewind","");
            }

            /*else if(api=="mediaFastForward" || api=="mediaRewind"){ // forward request to GUI
                core.info("Received message from SM Server | apiName: "+ api);
                sigDataReceived(api,"");

            } */
            else if(api=="controlMedia" && apiParameters.value=="pause"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: mediaPause "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.pause();
                else if(pif.lastSelectedVideo.getMediaState()!=pif.vod.cMEDIA_STOP)sigDataReceived("mediaPause","");

            } else if(api=="controlMedia" && apiParameters.value=="resume"){ //  check for play nad payse also forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: mediaResume "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.resume();
                else if(pif.lastSelectedVideo.getMediaState()!=pif.vod.cMEDIA_STOP)sigDataReceived("mediaResume","");

            } else if(api=="controlMedia" && apiParameters.value=="stop" && apiParameters.target=="mediaPlayer2"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: videoStop");
                sigDataReceived("videoStop","");

            } else if(api=="controlMedia" && apiParameters.value=="stop" && apiParameters.target=="mediaPlayer1"){
                core.info("Received message from SM Server | apiName: audioStop "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.stop();

            } else if(api=="controlMedia" && apiParameters.value=="next"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP){
                    params={mediaType:"aod"}
                    pif.lastSelectedAudio.playNext();
                }
                else
                    params={mediaType:"vod"}
                sigDataReceived("mediaNext",params);

            } else if(api=="controlMedia" && apiParameters.value=="previous"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP){
                    params={mediaType:"aod"}
                    pif.lastSelectedAudio.playPrevious();
                }
                else
                    params={mediaType:"vod"}
                sigDataReceived("mediaPrevious",params);

            } else if(api=="changeLanguage"){
                params = {languageIso:apiParameters.languageIso,languageLid:apiParameters.languageLid}
                core.info("Received message from SM Server | apiName: " + api + " | languageIso " + params.languageIso+ " | languageLid " + params.languageLid);
                sigDataReceived(api,params);

            } else if(api=="seatbackVolume"){
                params = {seatbackVolume:pif.__sharedModelServer.seatbackVolume}
                core.info("Received message from SM Server | change seatbackVolume to : " + params.seatbackVolume);
                __pifRef.setVolumeByValue(params.seatbackVolume);
                sigDataReceived(api,params);

            } else if(api=="headsetVolume"){
                params = {headsetVolume:pif.__sharedModelServer.headsetVolume}
                core.info("Received message from SM Server | change headsetVolume to : " + params.headsetVolume);
                pif.setVolumeByValue(params.headsetVolume);
                sigDataReceived(api,params);

            } else if(api=="muteVolume"){
                params = {muteVolume:pif.__sharedModelServer.muteEnabled}
                core.info("Received message from SM Server | change muteVolume to : " + params.muteVolume);
                sigDataReceived(api,params);

            } else if(api=="seatbackBrightness"){
                params = {seatbackBrightness:pif.__sharedModelServer.seatbackBrightness}
                core.info("Received message from SM Server | change seatbackBrightness to : " + params.seatbackBrightness);
                __pifRef.setBrightnessByValue(params.seatbackBrightness)
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);

            } else if(api=="launchMedia" && apiParameters.mediaType=="aod"){
                params = {mid:apiParameters.identifier,aggregateMid:apiParameters.aggregateIdentifier,playAll:apiParameters.playAll,mediaRepeat:apiParameters.mediaRepeat}
               core.info("Received message from SM Server | apiName: " +api+ " | mid: " + params.mid + ", aggregateMid: " + params.aggregateMid + ", playAll: " + params.playAll+ ", mediaRepeat: " + params.mediaRepeat);
                sigDataReceived("playAod",params);

            } else if(api=="mediaRepeat"){
                params = {mediaRepeat:apiParameters.mediaRepeat}
                core.info("Received message from SM Server | apiName: "+ api + " | mediaRepeat: " + params.mediaRepeat);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP){
                    if(params.mediaRepeat=="none") pif.lastSelectedAudio.setMediaRepeat(0);
                    else if(params.mediaRepeat=="current") pif.lastSelectedAudio.setMediaRepeat(2);
                    else if(params.mediaRepeat=="all") pif.lastSelectedAudio.setMediaRepeat(1);
                }
                sigDataReceived(api,params);

            } else if(api=="mediaShuffle"){
                params = {mediaShuffle:pif.__sharedModelServer.mediaPlayer1Shuffle}
                core.info("Received message from SM Server | apiName: "+ api + " | mediaShuffle: " + params.mediaShuffle);
                sigDataReceived(api,params);

            } else if(api=="launchApp"){
                params = {launchAppId:apiParameters.launchAppId}
                if(typeof(params.launchAppId)!="number") params.launchAppId=parseInt(params.launchAppId,10)
                core.info("Received message from SM Server | apiName: "+ api + " | launchAppId: " + params.launchAppId);
                pif.screenSaver.restartScreenSaver();sigDataReceived("launchApp",params);

            }else if(api=="launchMedia" && (apiParameters.mediaType=="game" || apiParameters.mediaType=="application")){
                params = {launchAppId:apiParameters.identifier,mediaType:apiParameters.mediaType}
                core.info("Received message from SM Server | apiName: "+ api + " | launchMedia | mediaType: "+params.mediaType+" identifier: launchAppId: " + params.launchAppId);
                pif.screenSaver.restartScreenSaver();sigDataReceived("launchApp",params);

            } else if(api=="closeApp"){
                core.info("Received message from SM Server | apiName: "+ api);
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);

            }else if(api=="backlightEnabled"){
                (pif.__sharedModelServer.serverBacklightEnabled)? __pifRef.setBacklightOn():__pifRef.setBacklightOff();
                core.info("Received message from SM Server | backlightEnabled " + pif.__sharedModelServer.serverBacklightEnabled);
                sigDataReceived(api,params);

            } else if(api=="showKeyboard"){
                params = {showKeyboard:pif.__sharedModelServer.serverKeyboardVisible}
                core.info("Received message from SM Server | apiName: "+ api + " | showKeyboard "+ params.showKeyboard)
                sigDataReceived(api,params);

            } else if(api=="keyboardData"){
                params = {keyboardData:pif.__sharedModelServer.keyboardData}
                core.info("Received message from SM Server | apiName: "+ api + " | keyboardData : "+ params.keyboardData)
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);

            }else if(api=="launchIpod" || api=="launchCategory"){
                params = {launchAppId:apiParameters.launchAppId}
                core.info("Received message from SM Server | apiName: "+ api + " | "+ params.launchAppId)
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);

            } else if(api=="usbAgreement"){
                params = {usbAgreement:pif.__sharedModelServer.usbAgreement}
                core.info("Received message from SM Server | apiName: "+ api + " | "+ params.usbAgreement)
                sigDataReceived(api,params);

            } else if(api=="iPodAgreement"){
                params = {iPodAgreement:pif.__sharedModelServer.iPodAgreement}
                core.info("Received message from SM Server | apiName: "+ api + " | "+ params.iPodAgreement)
                sigDataReceived(api,params);

            } else if (api=="createPlaylist"){
                params = {playlistIndex:apiParameters.playlistIndex,name:apiParameters.name,mediaType:apiParameters.mediaType}
                core.info("Received message from seat | apiName: "+ api + " | playlistIndex : "+ params.playlistIndex + ", name : "+ params.name + " , mediaType :"+params.mediaType)
                __createPlaylist(params)

            } else if (api=="updatePlaylistName"){
                params = {playlistIndex:apiParameters.playlistIndex,name:apiParameters.name,mediaType:apiParameters.mediaType}
                core.info("Received message from seat | apiName: "+ api + " | playlistIndex : "+ params.playlistIndex + ", name : "+ params.name + " , mediaType :"+params.mediaType)
                __updatePlaylistName(params)

            } else if(api=="addMidToPlaylist"){
                params = {playlistIndex:apiParameters.playlistIndex,mids:apiParameters.mids,aggregateMid:apiParameters.aggregateMid,cid:apiParameters.cid,mediaType:apiParameters.mediaType,rating:apiParameters.rating,modelCount:apiParameters.modelCount};
                core.info("Received message from seat | apiName: "+ api + " | playlistIndex : "+ params.playlistIndex + ", mids: " + params.mids + ", aggregateMid : " + params.aggregateMid + ", cid : " + params.cid+ " , rating :"+params.rating+ " , modelCount :"+params.modelCount);
                Store.midList=params.mids;
                __playlistId = params.playlistIndex;
                __aggregateMid = params.aggregateMid;
                __cid = params.cid;
                __mediaType = params.mediaType;
                __rating = params.rating;
                if(__mediaType=="aod" && params.modelCount &&  params.modelCount>0) pif.aodPlaylist.updateAggegerateMidCount(params.aggregateMid,params.modelCount)
                else if (__mediaType=="vod" && params.modelCount  && params.modelCount>0)pif.vodPlaylist.updateAggegerateMidCount(params.aggregateMid,params.modelCount)
                else if (__mediaType=="kidsaod"  && params.modelCount && params.modelCount>0)pif.kidsAodPlaylist.updateAggegerateMidCount(params.aggregateMid,params.modelCount)
                else if ( __mediaType=="kidsvod" && params.modelCount  && params.modelCount>0)pif.kidsVodPlaylist.updateAggegerateMidCount(params.aggregateMid,params.modelCount)
                core.dataController.getPlaylistData([Store.midList,"","","","",100],__addingMidToPlaylist);
            } else if(api=="removeMidFromPlayist"){
                params = {mids:apiParameters.mids,mediaType:apiParameters.mediaType};
                core.info("Received message from SM Server | apiName: "+ api +" | , mids : " + params.mids+ " , mediaType : "+params.mediaType);
                var midLists=params.mids;
                for (var i=0;i<midLists.length;i++){
                    if (params.mediaType=="aod")pif.aodPlaylist.removeTrack(midLists[i])
                    else if (params.mediaType=="vod")pif.vodPlaylist.removeTrack(midLists[i])
                    else if (params.mediaType=="kidsaod")pif.kidsAodPlaylist.removeTrack(midLists[i])
                    else if (params.mediaType=="kidsvod")pif.kidsVodPlaylist.removeTrack(midLists[i])
                    else if (params.mediaType=="mp3")pif.usbPlaylist.removeTrack(midLists[i])
                }

            } else if(api=="removeAggregateMid"){
                params = {amid:apiParameters.amid,mediaType:apiParameters.mediaType};
                core.info("Received message from SM Server | apiName: "+ api +" | , amid : " + params.amid+ " , mediaType : "+params.mediaType);
                if (params.mediaType=="aod")pif.aodPlaylist.removeAggregateMid(params.amid);
                else if (params.mediaType=="vod")pif.vodPlaylist.removeAggregateMid(params.amid);
                else if (params.mediaType=="kidsaod")pif.kidsAodPlaylist.removeAggregateMid(params.amid);
                else if (params.mediaType=="kidsvod")pif.kidsVodPlaylist.removeAggregateMid(params.amid);                
            } else if(api=="removeAllFromPlaylist"){
                params = {playlistIndex:apiParameters.playlistIndex,mediaType:apiParameters.mediaType};
                core.info("Received message from SM Server | apiName: "+ api +" | playlistIndex: " + params.playlistIndex + " , mediaType : "+params.mediaType);
                var playlistIndex = params.playlistIndex;
                if(params.mediaType=="aod")pif.aodPlaylist.removeAllTracks(playlistIndex)
                else if (params.mediaType=="vod")pif.vodPlaylist.removeAllTracks(playlistIndex)
                else if (params.mediaType=="kidsaod")pif.kidsAodPlaylist.removeAllTracks(playlistIndex)
                else if (params.mediaType=="kidsvod")pif.kidsVodPlaylist.removeAllTracks(playlistIndex)
                else if (params.mediaType=="mp3")pif.usbPlaylist.removeAllTracks(playlistIndex)

            } else if (api=="playJukebox"){
                params = {playlistIndex:apiParameters.playlistIndex,mids:apiParameters.mid,mediaType:apiParameters.mediaType};
                core.info("Received message from SM Server | apiName: "+ api +" | playlistIndex: " + params.playlistIndex + ", mid : " + params.mid + " , mediaType : "+params.mediaType)
                sigDataReceived(api,params)
            }
            else if((api=="playBlueRay") || (api=="stopBlueRay") || (api=="pauseBlueRay") || (api=="resumeBlueRay") || (api=="fastForwardBlueRay") || (api=="rewindBlueRay")||(api=="nextBlueRay")||(api=="previousBlueRay")||(api=="menuTitleBlueRay")||(api=="menuDiscBlueRay")||(api=="upBlueRay")||(api=="downBlueRay")||(api=="leftBlueRay")||(api=="rightBlueRay")||(api=="enterBlueRay")){
                core.info("Received message from SM Server | apiName: "+ api);
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,"");
            }
            else if(api=="setBlueRayMediumType"){
                params = {blueRayMediumType:apiParameters.blueRayMediumType};
                core.info("Received message from SM Server | apiName: "+ api);
                sigDataReceived(api,params);

            }
            else if (api=="seatChatVkarmaToSeatRequest"){
                 core.info("Received message from SM Server | apiName: "+ api);
                params = {apiName:apiParameters.apiName,apiParameters:apiParameters.apiParameters};
                sigDataReceived(api,params);
            } else if(api=="seatChatVkarmaToSeatCallBackRequest"){
                core.info("Received message from SM Server | apiName: "+ api + ", executingApi:"+executingSeatChatApiRequest);
                if(executingSeatChatApiRequest){
                    params = {apiName:apiParameters.apiName,apiParameters:apiParameters.apiParameters,callbackFunction:apiParameters.callbackFunction};
                    addToSeatChatQue(api,params);
                } else {
                    executingSeatChatApiRequest=true;
                    params = {apiName:apiParameters.apiName,apiParameters:apiParameters.apiParameters,callbackFunction:apiParameters.callbackFunction};
                    sigDataReceived(api,params);
                }
            } else if(api=="seatChatVkarmaToSeatExtraParams"){
                core.info("Received message from SM Server | apiName: "+ api + ", executingApi:"+executingSeatChatApiParamsRequest);
                if(executingSeatChatApiParamsRequest){
                    params = {apiName:apiParameters.apiName,apiParameters:apiParameters.apiParameters,callbackFunction:apiParameters.callbackFunction,extraParams:apiParameters.extraParams};
                    addToSeatChatQue(api,params);
                } else {
                    executingSeatChatApiParamsRequest=true;
                    params = {apiName:apiParameters.apiName,apiParameters:apiParameters.apiParameters,callbackFunction:apiParameters.callbackFunction,extraParams:apiParameters.extraParams};
                    sigDataReceived(api,params);
                }
           } else if (api=="seatToVkarmaSeatChatRequest"){
                if(executingSeatChatApiRequest)executingSeatChatApiRequest=false;
                if(!executingSeatChatApiRequest && Store.queArray.length!=0){
                    core.info("SharedModelServerQTIP2.qml  | onSeatToVkarmaSeatChatRequestChanged | sending signal form que");
                    sigDataReceived(Store.queArray[0]["api"],Store.queArray[0]["apiParameters"])
                    removeFromSeatChatQue("seatToVkarmaSeatChatRequest");
                }
            } else if (api=="seatToVkarmaSeatChatWithParams"){
                if(executingSeatChatApiParamsRequest)executingSeatChatApiParamsRequest=false;
                if(!executingSeatChatApiParamsRequest && Store.queParamsArray.length!=0){
                    core.info("SharedModelServerQTIP2.qml  | onSeatToVkarmaSeatChatRequestChanged | sending signal form que");
                    sigDataReceived(Store.queParamsArray[0]["api"],Store.queParamsArray[0]["apiParameters"])
                    removeFromSeatChatQue("seatToVkarmaSeatChatWithParams");
                }
            } else if(api=="frameworkPaymentSuccessful"){
                paymentDoneFromSeat=false;
                pif.ppv.sigPaymentSuccessful(apiParameters.amid);

            } else if(api.indexOf("userDefined")!=-1 || api.indexOf("framework")!=-1){
                core.info("Received message from SM Server | apiName: "+ api + " received")
                sigDataReceived(api,apiParameters);
            } else { //launchUsb etc
                core.info("Received message from SM Server | apiName: " + api);
                sigDataReceived(api,params);
            }
        }
    }

    function assignAspectRatio(){
        var propertyValue;
        core.debug("SharedModelServerInterface.qml | assignAspectRatio | isScreenAdjustable: "+pif.lastSelectedVideo.getIsScreenAdjustable()+" aspectRatio: "+pif.lastSelectedVideo.getAspectRatio()+" stretchState: "+pif.lastSelectedVideo.getStretchState())
        if(pif.lastSelectedVideo.getIsScreenAdjustable()==false){sendPropertyData("mediaPlayer2AspectRatio","disable"); return}

        if(pif.lastSelectedVideo.getAspectRatio().search("16x9Fixed")!=-1 || pif.lastSelectedVideo.getAspectRatio().search("4x3Fixed")!=-1) propertyValue="disable"
        else if(pif.lastSelectedVideo.getAspectRatio().search("16x9Adjustable")!=-1) propertyValue=(pif.lastSelectedVideo.getStretchState())?"16x9Stretched":"16x9"
        else if(pif.lastSelectedVideo.getAspectRatio().search("4x3Adjustable")!=-1) propertyValue=(pif.lastSelectedVideo.getStretchState())?"4x3Stretched":"4x3"
        sendPropertyData("mediaPlayer2AspectRatio",propertyValue)
    }

    function addToSeatChatQue(api,params){
        core.info("SharedModelServerInterfaceQTIP2.qml | addToSeatChatQue | api:"+api)
        if(api=="seatChatVkarmaToSeatCallBackRequest"){
            Store.queArray.push({"api":api,"params":params});
        } else if (api=="seatChatVkarmaToSeatExtraParams"){
            Store.queParamsArray.push({"api":api,"apiParameters":params});
        }
    }

    function removeFromSeatChatQue(api){
        core.info("SharedModelServerInterfaceQTIP2.qml  | removeFromSeatChatQue | api :"+api);
        if(api=="seatToVkarmaSeatChatRequest"){
            Store.queArray.splice(0,1);
        } else if(api=="seatToVkarmaSeatChatWithParams"){
            Store.queParamsArray.splice(0,1);
        }
    }

    Connections {
        // target is refrence 1
        target: pif.lastSelectedVideo;

        onSigMidPlayBegan: {
            var vodInfo;
            core.info("SharedModelServerInterface.qml | sigMidPlayBegan signal received")
            vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();

            if(__prevVodState==pif.vod.cMEDIA_STOP || __prevVodState==pif.vod.cMEDIA_PLAY){
                if(vodInfo.isTrailer){
                    //sendApiData("playTrailerAck",{mid:vodInfo.amid,soundtrackLid:vodInfo.sndtrkLid,soundtrackType:vodInfo.sndtrkType,subtitleLid:vodInfo.subtitleLid,subtitleType:vodInfo.subtitleType,elapsedTime:vodInfo.elapseTime});
                    sendPropertyData("mediaPlayer2IsTrailer",true);
                }
                else{
                    sendPropertyData("mediaPlayer2IsTrailer",false);
                    //sendApiData("playVodAck",{mid:vodInfo.amid,soundtrackLid:vodInfo.sndtrkLid,soundtrackType:vodInfo.sndtrkType,subtitleLid:vodInfo.subtitleLid,subtitleType:vodInfo.subtitleType,elapsedTime:vodInfo.elapseTime});
                }
                sendPropertyData("mediaPlayer2MediaType",(vodInfo.mediaType=="extvBroadcast"?"extv":vodInfo.mediaType));
                sendPropertyData("mediaPlayer2MediaIdentifier",vodInfo.cmid);
                sendPropertyData("mediaPlayer2MediaAggregateIdentifier",(vodInfo.amid==vodInfo.cmid)?0:vodInfo.amid);
                sendPropertyData("mediaPlayer2ElapsedTime",vodInfo.elapseTime);
                sendPropertyData("mediaPlayer2SubtitleLid",vodInfo.subtitleLid);
                sendPropertyData("mediaPlayer2SubtitleType",pif.lastSelectedVideo.getSubtitleType());
                sendPropertyData("mediaPlayer2SoundtrackLid",vodInfo.sndtrkLid);
                sendPropertyData("mediaPlayer2AspectRatio","");
                sendPropertyData("mediaPlayer2AvailableAspectRatios","");
                sendPropertyData("mediaPlayer2PlayRate",100);
                sendPropertyData("mediaPlayer2Shuffle",false);
                sendPropertyData("mediaPlayer2Repeat","none");
                sendPropertyData("mediaPlayer2MediaAlbumId",albumid);
                //sendPropertyData("mediaPlayerNSoundtrackEnabled","play");
                if(pif.__sharedModelServer.mediaPlayer1PlayStatus!="play"){
                    sendPropertyData("mediaPlayer2PlayStatus","play");
                }else {
                    sendPropertyData("mediaPlayer2PlayStatus","");
                    sendPropertyData("mediaPlayer2PlayStatus","play");
                }
            }
            else if(__prevVodState==pif.vod.cMEDIA_REWIND||__prevVodState==pif.vod.cMEDIA_FORWARD||__prevVodState==pif.vod.cMEDIA_PAUSE){
                core.info("Send message | apiName: mediaResumeAck");
                sendApiData("mediaResumeAck","");
                sendPropertyData("mediaPlayer2PlayRate",100);
                if(pif.__sharedModelServer.mediaPlayer1PlayStatus!="play"){
                    sendPropertyData("mediaPlayer2PlayStatus","play");
                }else {
                    sendPropertyData("mediaPlayer2PlayStatus","");
                    sendPropertyData("mediaPlayer2PlayStatus","play");
                }
            }
            __prevVodState = pif.vod.cMEDIA_PLAY;
        }
        onSigAspectRatioChanged: assignAspectRatio();
        onSigStretchState: assignAspectRatio();
        onSigSoundtrackSet:{
            //var vodInfo;
           // vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();
            core.info("SharedModelServerInterface.qml | apiName: videoSoundtrackAck | soundtrackLid : " + soundtracklid)
            //sendApiData("videoSoundtrackAck",{soundtrackLid:soundtracklid,soundtrackType:vodInfo.sndtrkType})
            sendPropertyData("mediaPlayer2SoundtrackLid",soundtracklid);
        }
        onSigSubtitleSet:{
            //var vodInfo;
            //vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();
            core.info("SharedModelServerInterface.qml | apiName: videoSubtitleAck | subtitleLid : " + subtitlelid)
            //sendApiData("videoSubtitleAck",{subtitleLid:subtitlelid,subtitleType:vodInfo.subtitleType})
            sendPropertyData("mediaPlayer2SubtitleLid",subtitlelid);
            sendPropertyData("mediaPlayer2SubtitleType",pif.lastSelectedVideo.getSubtitleType());
        }
        onSigMediaElapsed:{
            core.info("SharedModelServerInterface.qml | apiName: setMediaPositionAck | elapseTimeFormat : " + timeformat + ", vodElapseTime : " + elapseTime);
            //sendApiData("setMediaPositionAck",{elapsedTime:elapseTime});
            sendPropertyData("mediaPlayer2ElapsedTime",elapseTime);
        }
        onSigMediaPaused:{
            core.info("SharedModelServerInterface.qml | apiName: mediaPauseAck");
            __prevVodState=pif.vod.cMEDIA_PAUSE;
            //sendApiData("mediaPauseAck","");
            sendPropertyData("mediaPlayer2PlayStatus","pause");
        }
        onSigAggPlayStopped:{
            core.info("SharedModelServerInterface.qml | apiName: videoStopAck");
            __prevVodState=pif.vod.cMEDIA_STOP;
            //sendApiData("videoStopAck","");
            sendPropertyData("mediaPlayer2PlayRate",100);
            sendPropertyData("mediaPlayer2PlayStatus","stop");
            var vodElapsedTimeMap=pif.__sharedModelServer.resumeTimeMap;
            if(cmid!=amid){
                var elapseTime=pif.getMidInfo(amid,'elapseTime');
                core.debug("SharedModelServerInterface.qml | apiName: videoStopAck - resumeTimeMap | cmid: "+cmid+" | amid: "+amid+" | elapseTime: "+elapseTime);
                vodElapsedTimeMap[amid]=elapseTime;
				if(pif.getMidInfo(amid,'lastPlayed')!=undefined) vodElapsedTimeMap[cmid]=elapseTime;
            }else {
                core.debug("SharedModelServerInterface.qml | apiName: videoStopAck - resumeTimeMap | cmid: "+cmid+" | elapseTime: "+elapseTime);
                var elapseTime=pif.getMidInfo(cmid,'elapseTime');
                vodElapsedTimeMap[cmid]=elapseTime;
            }

            var midInforArr=pif.__getMidArrReference();
            sendPropertyData("midInforArr",midInforArr);
            sendPropertyData("resumeTimeMap",vodElapsedTimeMap);
        }
        onSigMediaForward:{
            core.info("SharedModelServerInterface.qml | apiName: mediaFastForwardAck");
            __prevVodState=pif.vod.cMEDIA_FORWARD;
            //sendApiData("mediaFastForwardAck",{mediaPlayRate:speed*100});
            sendPropertyData("mediaPlayer2PlayRate",speed*100);
            sendPropertyData("mediaPlayer2PlayStatus","pause");
        }
        onSigMediaRewind:{
            core.info("SharedModelServerInterface.qml | apiName: mediaRewindAck");
            __prevVodState=pif.vod.cMEDIA_REWIND;
            //sendApiData("mediaRewindAck",{mediaPlayRate:speed*100});
            var speedInt=parseInt(speed,10);
            speedInt=(speedInt>=0)?(speedInt*-1):speedInt;
            sendPropertyData("mediaPlayer2PlayRate",speedInt*100);
            sendPropertyData("mediaPlayer2PlayStatus","pause");
        }
        onSigMidPlayCompleted:{
            core.info("SharedModelServerInterface.qml | apiName: videoStopAck");
            __prevVodState=pif.vod.cMEDIA_STOP;
            //sendApiData("videoStopAck","");
            sendPropertyData("mediaPlayer2PlayRate",100);
            sendPropertyData("mediaPlayer2PlayStatus","stop");
        }
    }

    Connections{
        // target is refrence 1
        target:pif.lastSelectedAudio;

        onSigMidPlayBegan:{
            if(__prevAodState==pif.aod.cMEDIA_STOP || __prevAodState==pif.aod.cMEDIA_PLAY || __prevAodState==pif.aod.cMEDIA_PAUSE){
                var repeat=pif.lastSelectedAudio.getMediaRepeat();
                var repeatStr;
                if(repeat==0)repeatStr="none";
                else if(repeat==1)repeatStr="all"
                else repeatStr="current";
                var aodInfo=pif.lastSelectedAudio.getAllMediaPlayInfo();
                core.info("SharedModelServerInterface.qml | apiName: playAodAck");
                if(!__ignorePlayBegan){
                    sendPropertyData("mediaPlayer1MediaType",aodInfo.mediaType);
                    sendPropertyData("mediaPlayer1MediaAggregateIdentifier",amid);
                    sendPropertyData("mediaPlayer1Shuffle",pif.lastSelectedAudio.getMediaShuffle());
                    sendPropertyData("mediaPlayer1Repeat",repeatStr);
                    sendPropertyData("mediaPlayer1MediaAlbumId",albumid);
                }
                sendPropertyData("mediaPlayer1MediaIdentifier",cmid);
                sendPropertyData("mediaPlayer1PlayRate",100);

                if(pif.__sharedModelServer.mediaPlayer1PlayStatus!="play"){
                    sendPropertyData("mediaPlayer1PlayStatus","play");
                }else {
                    sendPropertyData("mediaPlayer1PlayStatus","");
                    sendPropertyData("mediaPlayer1PlayStatus","play");
                }
            }
            else if(__prevAodState==pif.aod.cMEDIA_REWIND||__prevAodState==pif.aod.cMEDIA_FORWARD||__prevAodState==pif.aod.cMEDIA_PAUSE){
                core.info("SharedModelServerInterface.qml | apiName: mediaResumeAck");
                sendApiData("mediaResumeAck","");
                //sendPropertyData("mediaPlayerState","play");
                sendPropertyData("mediaPlayer1PlayRate",100);
                sendPropertyData("mediaPlayer1PlayStatus","play");
            }
            __prevAodState=pif.aod.cMEDIA_PLAY;
            __ignorePlayBegan=false;
        }
        onSigMediaElapsed:{
            core.info("SharedModelServerInterface.qml | apiName: setMediaPositionAck | elapsedTimeFormat : " + timeformat + ", audioElapseTime : " + elapseTime);
            //sendApiData("setMediaPositionAck",{elapsedTime:elapseTime});
             sendPropertyData("mediaPlayer1ElapsedTime",elapseTime);
        }
        onSigAggPlayStopped:{
            __prevAodState=pif.aod.cMEDIA_STOP;
            core.info("SharedModelServerInterface.qml | apiName: audioStopAck");
            //sendApiData("audioStopAck","");
            //sendPropertyData("mediaPlayerState","stop");
            sendPropertyData("mediaPlayer1PlayRate",100);
            sendPropertyData("mediaPlayer1PlayStatus","stop");
        }
        onSigMediaPaused:{
            __prevAodState=pif.aod.cMEDIA_PAUSE;
            __ignorePlayBegan=true;
            core.info("SharedModelServerInterface.qml | apiName: mediaPauseAck");
            //sendApiData("mediaPauseAck","");
            //sendPropertyData("mediaPlayerState","pause");
            sendPropertyData("mediaPlayer1PlayStatus","pause");
        }
        onSigMediaShuffleState:{
            core.info("SharedModelServerInterface.qml | Shuffle property changed :: audioShuffle : " + shuffle);
            sendPropertyData("mediaPlayer1Shuffle",shuffle);
        }
        onSigMediaRepeatState:{
            core.info("SharedModelServerInterface.qml | Media Repeat property changed");
            var repeat=pif.lastSelectedAudio.getMediaRepeat();
            var repeatStr;
            if(repeat==0)repeatStr="none";
            else if(repeat==1)repeatStr="all";
            else repeatStr="current";
            sendPropertyData("mediaPlayer1Repeat",repeatStr);
        }
    }

    Connections{
        target:playlistUpdateRequiredOnKarma?pif.aodPlaylist:null;

        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigTracksAdded, pid : "+pid+ " , Mids : "+mids+", playlistType :"+playlistType+", aggMid:"+aggMid+", albumAdded:"+albumAdded)
            var midList = mids
            var aggregateMidArray = {};
            core.info("smsi.qml | onSignalTracksAdded midList :"+midList+"Error code :"+errorCode)
            if (playlistType=="AodPlaylist"){
                var aggregateMid = parseInt(pif.aodPlaylist.getAggregateMid(midList[0],pid),10)
                var cid = parseInt(pif.aodPlaylist.getCategoryId(midList[0],pid),10)
            }
            var plist = pif.__sharedModelServer.playlists;
            for (var i=0;i<midList.length;i++){
                if (playlistType=="AodPlaylist"){
                    var aodPlaylistName = Store.aodPlaylistArr[pid]
                    plist.aod[aodPlaylistName].push({"mid": midList[i], "albumMid": aggregateMid, "cid": cid});
                }
            }

            pif.__sharedModelServer.playlists = plist;
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigTracksAdded playlist updated");
            if(errorCode==1){
                core.info("smsi.qml | onSigTracksAdded | Tracks Partially Added")
                sendFrameworkApi("frameworkPlaylistPartiallyAdded",{playlistType:"aod",pid:pid});
            }
            if(errorCode==0){
                core.info("smsi.qml | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded",{playlistType:"aod",pid:pid});
            }
            if(!aggregateMidArray["aod"]){
                aggregateMidArray["aod"]={};
                aggregateMidArray["aod"][aggMid]=albumAdded
            }
            else aggregateMidArray["aod"][aggMid]=albumAdded
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"aod",albumMid:aggregateMidArray})
        }

        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigTracksRemoved, pid : "+pid+ ", Mid :"+mid+" ,playlistType :"+playlistType+"Index :"+indx+",removingAggMid:"+removingAggMid)
            if(!removingAggMid){
                var plist = pif.__sharedModelServer.playlists;
                var aggregateMidArray = {};
                if (playlistType=="AodPlaylist"){
                    for (var aodCounter in plist.aod){
                        for (var i in plist.aod[aodCounter]){
                            if (mid==plist.aod[aodCounter][i].mid) plist.aod[aodCounter].splice(i,1)
                        }
                    }
                }
                pif.__sharedModelServer.playlists = plist;
                if(!aggregateMidArray["aod"]){
                    aggregateMidArray["aod"]={};
                    aggregateMidArray["aod"][aggMid]=albumAdded
                }
                else aggregateMidArray["aod"][aggMid]=albumAdded
                sendFrameworkApi("frameworkPlaylistTrackRemoved",{playlistType:"aod",pid:pid});
                sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"aod",albumMid:aggregateMidArray})
            }
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigTracksRemoved, pid : "+pid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            var albumAdded;
            var aggregateMidArray = {};
            if (playlistType=="AodPlaylist"){
                var aodPlaylistName = Store.aodPlaylistArr[pid]
                plist.aod[aodPlaylistName] = new Array();
            }
            pif.__sharedModelServer.playlists = plist
            for(var i=0; i<aggMidArr.length;i++){
                albumAdded = pif.aodPlaylist.isAlbumInPlaylist(aggMidArr[i])
                if(!aggregateMidArray["aod"]){
                    aggregateMidArray["aod"]={};
                    aggregateMidArray["aod"][aggMidArr[i]]=albumAdded
                }
                else aggregateMidArray["aod"][aggMidArr[i]]=albumAdded
            }
            sendFrameworkApi("frameworkPlaylistAllTrackRemoved",{playlistType:"aod",pid:pid});
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"aod",albumMid:aggregateMidArray})
        }

        onSigAggregateMidRemoved:{
            core.info("SharedModelServerInterface.qml | aodPlaylist |onSigAggregateMidRemoved, amid : "+amid+ " ,playlistType :"+playlistType);
            var plist = pif.__sharedModelServer.playlists;
            var aggregateMidArray = {};
            var tempArr = new Array();
            if (playlistType=="AodPlaylist"){
                for (var aodCounter in plist.aod){
                    for (var k in plist.aod[aodCounter]){
                        if (parseInt(amid,10)!=parseInt((plist.aod[aodCounter][k].albumMid),10)) {
                            tempArr.push({"mid":plist.aod[aodCounter][k].mid,"albumMid":plist.aod[aodCounter][k].albumMid,"cid":plist.aod[aodCounter][k].cid})
                        }
                    }
                    plist.aod[aodCounter] = new Array();
                    for (var j in tempArr){
                        plist.aod[aodCounter].push({"mid":tempArr[j].mid,"albumMid":tempArr[j].albumMid,"cid":tempArr[j].cid})
                    }
                    tempArr = [];
                }
            }
            pif.__sharedModelServer.playlists = plist;
            if(!aggregateMidArray["aod"]){
                aggregateMidArray["aod"]={};
                aggregateMidArray["aod"][amid]=false;
            }
            else aggregateMidArray["aod"][amid]=false;
            sendFrameworkApi("frameworkPlaylistAggregateMidRemoved",{playlistType:"aod",pid:pid});
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"aod",albumMid:aggregateMidArray})
        }

        onSigTracksMoved:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigTracksMoved | pid : "+pid+ ", playlistType :"+playlistType)
            if (playlistType=="AodPlaylist"){
                var plist = pif.__sharedModelServer.playlists;var aodPlaylistName = Store.aodPlaylistArr[pid];
                var cModel= pif.aodPlaylist.getPlaylistModel(pid);
                var tmp=[];
                for(var counter=0;counter<cModel.count;counter++){
                    var row=cModel.at(counter);tmp.push({"mid":row.mid, "aggregate_parentmid": row.mid, "cid": row.mid});
                }
                plist.aod[aodPlaylistName]=tmp;
                pif.__sharedModelServer.playlists = plist;
                sendFrameworkApi("frameworkPlaylistMoved",{playlistType:"aod",pid:pid});
            }
        }

        onSigSyncAlbumMidlist:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigSyncAlbumMidlist | aggMidArr : "+aggMidArr);
            var albumAdded;
            var aggregateMidArray = {};
            for(var i=0; i<aggMidArr.length;i++){
                albumAdded = pif.aodPlaylist.isAlbumInPlaylist(aggMidArr[i])
                if(!aggregateMidArray["aod"]){
                    aggregateMidArray["aod"]={};
                    aggregateMidArray["aod"][aggMidArr[i]]=albumAdded
                }
                else aggregateMidArray["aod"][aggMidArr[i]]=albumAdded
            }
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"aod",albumMid:aggregateMidArray})
        }
    }

    Connections{
        target:playlistUpdateRequiredOnKarma?pif.vodPlaylist:null;
        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | vodPlaylist | onSigTracksAdded, pid : "+pid+ " , Mids : "+mids+", playlistType :"+playlistType)
            var midList = mids;
            var aggregateMidArray = {};
            if (playlistType=="VodPlaylist"){
                var aggregateMid = parseInt(pif.vodPlaylist.getAggregateMid(midList[0],pid),10)
                var cid = parseInt(pif.vodPlaylist.getCategoryId(midList[0],pid),10)
            }
            var plist = pif.__sharedModelServer.playlists;
            for (var i=0;i<midList.length;i++){
                if (playlistType=="VodPlaylist"){
                    var vodPlaylistName = Store.vodPlaylistArr[pid]
                    plist.vod[vodPlaylistName].push({"mid": midList[i], "albumMid": aggregateMid, "cid": cid});
                }
            }
            pif.__sharedModelServer.playlists = plist;
            core.info("SharedModelServerInterface.qml | vodPlaylist | onSigTracksAdded playlist updated");
            if(errorCode==1){
                core.info("smsi.qml | onSigTracksAdded | Tracks Partially Added")
                sendFrameworkApi("frameworkPlaylistPartiallyAdded",{playlistType:"vod",pid:pid});
            }
            if(errorCode==0){
                core.info("smsi.qml | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded",{playlistType:"vod",pid:pid});
            }
            if(!aggregateMidArray["vod"]){
                aggregateMidArray["vod"]={};
                aggregateMidArray["vod"][aggMid]=albumAdded
            }
            else aggregateMidArray["vod"][aggMid]=albumAdded
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"vod",albumMid:aggregateMidArray})
        }

        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | vodPlaylist |onSigTracksRemoved, pid : "+pid+ ", Mid :"+mid+" ,playlistType :"+playlistType+"Index :"+indx+",removingAggMid:"+removingAggMid)
            if(!removingAggMid){
                var plist = pif.__sharedModelServer.playlists;
                var aggregateMidArray = {};
                if (playlistType=="VodPlaylist"){
                    for (var vodCounter in plist.vod){
                        for (var j in plist.vod[vodCounter]){
                            if (mid==plist.vod[vodCounter][j].mid) plist.vod[vodCounter].splice(j,1)
                        }
                    }
                }
                pif.__sharedModelServer.playlists = plist
                if(!aggregateMidArray["vod"]){
                    aggregateMidArray["vod"]={};
                    aggregateMidArray["vod"][aggMid]=albumAdded;
                }
                else aggregateMidArray["vod"][aggMid]=albumAdded;
                sendFrameworkApi("frameworkPlaylistTrackRemoved",{playlistType:"vod",pid:pid});
                sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"vod",albumMid:aggregateMidArray})
            }
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | vodPlaylist | onSigTracksRemoved, pid : "+pid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            var albumAdded;
            var aggregateMidArray = {};
            if (playlistType=="VodPlaylist"){
                var vodPlaylistName = Store.vodPlaylistArr[pid]
                plist.vod[vodPlaylistName] = new Array();
            }
            pif.__sharedModelServer.playlists = plist
            for(var i=0; i<aggMidArr.length;i++){
                albumAdded = pif.vodPlaylist.isAlbumInPlaylist(aggMidArr[i])
                if(!aggregateMidArray["vod"]){
                    aggregateMidArray["vod"]={};
                    aggregateMidArray["vod"][aggMidArr[i]]=albumAdded
                }
                else aggregateMidArray["vod"][aggMidArr[i]]=albumAdded
            }
            sendFrameworkApi("frameworkPlaylistAllTrackRemoved",{playlistType:"vod",pid:pid});
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"vod",albumMid:aggregateMidArray})
        }

        onSigAggregateMidRemoved:{
            core.info("SharedModelServerInterface.qml | vodPlaylist |onSigAggregateMidRemoved, amid : "+amid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            var tempArr = new Array();
            var aggregateMidArray = {};
            if (playlistType=="VodPlaylist"){
                for (var vodCounter in plist.vod){
                    for (var k in plist.vod[vodCounter]){
                        if (parseInt(amid,10)!=parseInt((plist.vod[vodCounter][k].albumMid),10)) {
                            tempArr.push({"mid":plist.vod[vodCounter][k].mid,"albumMid":plist.vod[vodCounter][k].albumMid,"cid":plist.vod[vodCounter][k].cid})
                        }
                    }
                    plist.vod[vodCounter] = new Array();
                    for (var j in tempArr){
                        plist.vod[vodCounter].push({"mid":tempArr[j].mid,"albumMid":tempArr[j].albumMid,"cid":tempArr[j].cid})
                    }
                    tempArr = [];
                }
            }
            pif.__sharedModelServer.playlists = plist;
            if(!aggregateMidArray["vod"]){
                aggregateMidArray["vod"]={};
                aggregateMidArray["vod"][amid]=false;
            }
            else aggregateMidArray["vod"][amid]=false;
            sendFrameworkApi("frameworkPlaylistAggregateMidRemoved",{playlistType:"vod",pid:pid});
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"vod",albumMid:aggregateMidArray})
        }

        onSigSyncAlbumMidlist:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigSyncAlbumMidlist | aggMidArr : "+aggMidArr);
            var albumAdded;
            var aggregateMidArray = {};
            for(var i=0; i<aggMidArr.length;i++){
                albumAdded = pif.vodPlaylist.isAlbumInPlaylist(aggMidArr[i])
                if(!aggregateMidArray["vod"]){
                    aggregateMidArray["vod"]={};
                    aggregateMidArray["vod"][aggMidArr[i]]=albumAdded
                }
                else aggregateMidArray["vod"][aggMidArr[i]]=albumAdded
            }
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"vod",albumMid:aggregateMidArray})
        }
    }

    Connections{
        target:playlistUpdateRequiredOnKarma?pif.kidsAodPlaylist:null;

        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | kidsAodPlaylist | onSigTracksAdded, pid : "+pid+ " , Mids : "+mids+", playlistType :"+playlistType)
            var midList = mids
            var aggregateMidArray = {};
            core.info("smsi.qml | onSignalTracksAdded midList :"+midList+"Error code :"+errorCode)
            if (playlistType=="KidsAodPlaylist"){
                var aggregateMid = parseInt(pif.kidsAodPlaylist.getAggregateMid(midList[0],pid),10)
                var cid = parseInt(pif.kidsAodPlaylist.getCategoryId(midList[0],pid),10)
            }
            var plist = pif.__sharedModelServer.playlists;
            for (var i=0;i<midList.length;i++){
                if (playlistType=="KidsAodPlaylist"){
                    var kidsAodPlaylistName = Store.kidsAodPlaylistArr[pid]
                    plist.kidsaod[kidsAodPlaylistName].push({"mid": midList[i], "albumMid": aggregateMid, "cid": cid});
                }
            }
            pif.__sharedModelServer.playlists = plist;
            if(errorCode==1){
                core.info("smsi.qml | onSigTracksAdded | Tracks Partially Added")
                sendFrameworkApi("frameworkPlaylistPartiallyAdded",{playlistType:"kidsaod",pid:pid});
            }
            if(errorCode==0){
                core.info("smsi.qml | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded",{playlistType:"kidsaod",pid:pid});
            }
            if(!aggregateMidArray["kidsaod"]){
                aggregateMidArray["kidsaod"]={};
                aggregateMidArray["kidsaod"][aggMid]=albumAdded
            }
            else aggregateMidArray["kidsaod"][aggMid]=albumAdded
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"aod",albumMid:aggregateMidArray})
        }

        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | kidsAodPlaylist | onSigTracksRemoved, pid : "+pid+ ", Mid :"+mid+" ,playlistType :"+playlistType+" ,Index :"+indx+",removingAggMid"+removingAggMid)
            if(!removingAggMid){
                var plist = pif.__sharedModelServer.playlists;
                var aggregateMidArray = {};
                if (playlistType=="KidsAodPlaylist"){
                    for (var kidsAodCounter in plist.kidsaod){
                        for (var i in plist.kidsaod[kidsAodCounter]){
                            if (mid==plist.kidsaod[kidsAodCounter][i].mid) plist.kidsaod[kidsAodCounter].splice(i,1)
                        }
                    }
                }
                pif.__sharedModelServer.playlists = plist;
                if(!aggregateMidArray["kidsaod"]){
                    aggregateMidArray["kidsaod"]={};
                    aggregateMidArray["kidsaod"][aggMid]=albumAdded
                }
                else aggregateMidArray["kidsaod"][aggMid]=albumAdded;
                sendFrameworkApi("frameworkPlaylistTrackRemoved",{playlistType:"kidsaod",pid:pid});
                sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsaod",albumMid:aggregateMidArray})
            }
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | kidsAodPlaylist | onSigTracksRemoved, pid : "+pid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            var albumAdded;
            var aggregateMidArray = {};
            if (playlistType=="KidsAodPlaylist"){
                var KidsAodPlaylistName = Store.kidsAodPlaylistArr[pid]
                plist.kidsaod[KidsAodPlaylistName] = new Array();
            }
            pif.__sharedModelServer.playlists = plist
            for(var i=0; i<aggMidArr.length;i++){
                albumAdded = pif.kidsAodPlaylist.isAlbumInPlaylist(aggMidArr[i])
                if(!aggregateMidArray["kidsaod"]){
                    aggregateMidArray["kidsaod"]={};
                    aggregateMidArray["kidsaod"][aggMidArr[i]]=albumAdded
                }
                else aggregateMidArray["kidsaod"][aggMidArr[i]]=albumAdded

            }
            sendFrameworkApi("frameworkPlaylistAllTrackRemoved",{playlistType:"kidsaod",pid:pid});
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsaod",albumMid:aggregateMidArray})
        }
        onSigAggregateMidRemoved:{
            core.info("SharedModelServerInterface.qml | kidsAodPlaylist |onSigAggregateMidRemoved, amid : "+amid+ " ,playlistType :"+playlistType);
            var plist =  pif.__sharedModelServer.playlists;
            var tempArr = new Array();
            var aggregateMidArray = {};
            if (playlistType=="KidsAodPlaylist"){
                for (var kidsAodCounter in plist.kidsaod){
                    for (var k in plist.kidsaod[kidsAodCounter]){
                        if (parseInt(amid,10)!=parseInt((plist.kidsaod[kidsAodCounter][k].albumMid),10)) {
                            tempArr.push({"mid":plist.kidsaod[kidsAodCounter][k].mid,"albumMid":plist.kidsaod[kidsAodCounter][k].albumMid,"cid":plist.kidsaod[kidsAodCounter][k].cid})
                        }
                    }
                    plist.kidsaod[kidsAodCounter] = new Array();
                    for (var j in tempArr){
                        plist.kidsaod[kidsAodCounter].push({"mid":tempArr[j].mid,"albumMid":tempArr[j].albumMid,"cid":tempArr[j].cid})
                    }
                    tempArr = [];
                }
            }
            pif.__sharedModelServer.playlists = plist;
            if(!aggregateMidArray["kidsaod"]){
                aggregateMidArray["kidsaod"]={};
                aggregateMidArray["kidsaod"][amid]=false;
            }
            else aggregateMidArray["kidsaod"][amid]=false;
            sendFrameworkApi("frameworkPlaylistAggregateMidRemoved",{playlistType:"kidsaod",pid:pid});
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsaod",albumMid:aggregateMidArray})
        }

        onSigSyncAlbumMidlist:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigSyncAlbumMidlist | aggMidArr : "+aggMidArr);
            var albumAdded;
            var aggregateMidArray = {};
            for(var i=0; i<aggMidArr.length;i++){
                albumAdded = pif.kidsAodPlaylist.isAlbumInPlaylist(aggMidArr[i])
                if(!aggregateMidArray["kidsaod"]){
                    aggregateMidArray["kidsaod"]={};
                    aggregateMidArray["kidsaod"][aggMidArr[i]]=albumAdded
                }
                else aggregateMidArray["kidsaod"][aggMidArr[i]]=albumAdded
            }
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsaod",albumMid:aggregateMidArray})
        }
    }

    Connections{
        target:playlistUpdateRequiredOnKarma?pif.kidsVodPlaylist:null;

        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | kidsVodPlaylist | onSigTracksAdded, pid : "+pid+ " , Mids : "+mids+", playlistType :"+playlistType+", aggMid:"+aggMid)
            var midList = mids
            var aggregateMidArray = {};
            core.info("smsi.qml | onSignalTracksAdded midList :"+midList+"Error code :"+errorCode)
            if (playlistType=="KidsVodPlaylist"){
                var aggregateMid = parseInt(pif.kidsVodPlaylist.getAggregateMid(midList[0],pid),10)
                var cid = parseInt(pif.kidsVodPlaylist.getCategoryId(midList[0],pid),10)
            }
            var plist = pif.__sharedModelServer.playlists;
            for (var i=0;i<midList.length;i++){
                if (playlistType=="KidsVodPlaylist"){
                    var kidsVodPlaylistName = Store.kidsVodPlaylistArr[pid]
                    plist.kidsvod[kidsVodPlaylistName].push({"mid": midList[i], "albumMid": aggregateMid, "cid": cid});
                }
            }
            pif.__sharedModelServer.playlists = plist;
            if(errorCode==1){
                core.info("smsi.qml | onSigTracksAdded | Tracks Partially Added")
                sendFrameworkApi("frameworkPlaylistPartiallyAdded",{playlistType:"kidsvod",pid:pid});
            }
            if(errorCode==0){
                core.info("smsi.qml | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded",{playlistType:"kidsvod",pid:pid});
            }
            if(!aggregateMidArray["kidsvod"]){
                aggregateMidArray["kidsvod"]={};
                aggregateMidArray["kidsvod"][aggMid]=albumAdded
            }
            else aggregateMidArray["kidsvod"][aggMid]=albumAdded
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsvod",albumMid:aggregateMidArray})
        }

        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | kidsVodPlaylist | onSigTracksRemoved, pid : "+pid+ ", Mid :"+mid+" ,playlistType :"+playlistType+", Index :"+indx+",removingAggMid:"+removingAggMid);
            if(!removingAggMid){
                var plist = pif.__sharedModelServer.playlists;
                var aggregateMidArray = {};
                if (playlistType=="KidsVodPlaylist"){
                    for (var kidsVodCounter in plist.kidsvod){
                        for (var i in plist.kidsvod[kidsVodCounter]){
                            if (mid==plist.kidsvod[kidsVodCounter][i].mid) plist.kidsvod[kidsVodCounter].splice(i,1)
                        }
                    }
                }
                pif.__sharedModelServer.playlists = plist;
                if(!aggregateMidArray["kidsvod"]){
                    aggregateMidArray["kidsvod"]={};
                    aggregateMidArray["kidsvod"][aggMid]=albumAdded
                }
                else aggregateMidArray["kidsvod"][aggMid]=albumAdded;
                sendFrameworkApi("frameworkPlaylistTrackRemoved",{playlistType:"kidsvod",pid:pid});
                sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsvod",albumMid:aggregateMidArray})
            }
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | kidsVodPlaylist | onSigTracksRemoved, pid : "+pid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            var albumAdded;
            var aggregateMidArray = {};
            if (playlistType=="KidsVodPlaylist"){
                var kidsVodPlaylistName = Store.kidsVodPlaylistArr[pid]
                plist.kidsvod[kidsVodPlaylistName] = new Array();
            }
            pif.__sharedModelServer.playlists = plist
            for(var i=0; i<aggMidArr.length;i++){
                albumAdded = pif.kidsVodPlaylist.isAlbumInPlaylist(aggMidArr[i]);
                if(!aggregateMidArray["kidsvod"]){
                    aggregateMidArray["kidsvod"]={};
                    aggregateMidArray["kidsvod"][aggMidArr[i]]=albumAdded
                }
                else aggregateMidArray["kidsvod"][aggMidArr[i]]=albumAdded
            }
            sendFrameworkApi("frameworkPlaylistAllTrackRemoved",{playlistType:"kidsvod",pid:pid});
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsvod",albumMid:aggregateMidArray})
        }

        onSigAggregateMidRemoved:{
            core.info("SharedModelServerInterface.qml | kidsVodPlaylist |onSigAggregateMidRemoved, amid : "+amid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            var tempArr = new Array();
            var aggregateMidArray = {};
            if (playlistType=="KidsVodPlaylist"){
                for (var kidsVodCounter in plist.kidsvod){
                    for (var k in plist.kidsvod[kidsVodCounter]){
                        if (parseInt(amid,10)!=parseInt((plist.kidsvod[kidsVodCounter][k].albumMid),10)) {
                            tempArr.push({"mid":plist.kidsvod[kidsVodCounter][k].mid,"albumMid":plist.kidsvod[kidsVodCounter][k].albumMid,"cid":plist.kidsvod[kidsVodCounter][k].cid})
                        }
                    }
                    plist.kidsvod[kidsVodCounter] = new Array();
                    for (var j in tempArr){
                        plist.kidsvod[kidsVodCounter].push({"mid":tempArr[j].mid,"albumMid":tempArr[j].albumMid,"cid":tempArr[j].cid})
                    }
                    tempArr = [];
                }
            }
            pif.__sharedModelServer.playlists = plist;
            if(!aggregateMidArray["kidsvod"]){
                aggregateMidArray["kidsvod"]={};
                aggregateMidArray["kidsvod"][amid]=false;
            }
            else aggregateMidArray["kidsvod"][amid]=false;
            sendFrameworkApi("frameworkPlaylistAggregateMidRemoved",{playlistType:"kidsvod",pid:pid});
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsvod",albumMid:aggregateMidArray})
        }

        onSigSyncAlbumMidlist:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigSyncAlbumMidlist | aggMidArr : "+aggMidArr);
            var albumAdded;
            var aggregateMidArray = {};
            for(var i=0; i<aggMidArr.length;i++){
                albumAdded = pif.kidsVodPlaylist.isAlbumInPlaylist(aggMidArr[i])
                if(!aggregateMidArray["kidsvod"]){
                    aggregateMidArray["kidsvod"]={};
                    aggregateMidArray["kidsvod"][aggMidArr[i]]=albumAdded
                }
                else aggregateMidArray["kidsvod"][aggMidArr[i]]=albumAdded
            }
            sendFrameworkApi("frameworkAlbumMidsUpdated",{playlistType:"kidsvod",albumMid:aggregateMidArray})
        }
    }

    Connections{
        target:pif.usbPlaylist;

        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | usbPlaylist | onSigTracksAdded, pid : "+pid+ " , status: "+status);
            if(status==pif.usbPlaylist.cTRACK_ADDED){
                var plist = pif.__sharedModelServer.playlists;
                var model = pif.usbPlaylist.getPlaylistModel();
                var aodPlaylistName = Store.usbPlaylistArr[pid]
                for (var counter=plist.usb[aodPlaylistName].length;counter<model.count;counter++){
                    plist.usb[aodPlaylistName].push({"media_type":model.getValue(counter,"media_type"),"duration":model.getValue(counter,"duration"),"cid":model.getValue(counter,"cid"),"pathfilename":model.getValue(counter,"pathfilename"),
                                                        "filetype":model.getValue(counter,"filetype"),"filenameonly":model.getValue(counter,"filenameonly"),"filesize":model.getValue(counter,"filesize"),
                                                        "mp3album":model.getValue(counter,"mp3album"),"mp3artist":model.getValue(counter,"mp3artist"),"mp3song":model.getValue(counter,"mp3song"),
                                                        "mp3title":model.getValue(counter,"mp3title"),"mp3year":model.getValue(counter,"mp3year")
                                                    });
                }
                pif.__sharedModelServer.playlists = plist;
                core.info("smsi.qml | usbPlaylist | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded",{playlistType:"mp3",pid:pid});
            }
        }
        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | usbPlaylist | onSigTracksRemoved, pid : "+pid+" pathfilename: "+pathfilename)
            var plist = pif.__sharedModelServer.playlists;
            var aodPlaylistName = Store.usbPlaylistArr[pid]
            for (var usbCounter=0;usbCounter<plist.usb[aodPlaylistName].length;usbCounter++){
                if (pathfilename==plist.usb[aodPlaylistName][usbCounter].pathfilename) plist.usb[aodPlaylistName].splice(usbCounter,1)
            }
            pif.__sharedModelServer.playlists = plist;
            sendFrameworkApi("frameworkPlaylistTrackRemoved",{playlistType:"mp3",pid:pid});
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | usbPlaylist |onSigTracksRemoved, pid : "+pid)
            var plist = pif.__sharedModelServer.playlists;
            var aodPlaylistName = Store.usbPlaylistArr[pid]
            plist.usb[aodPlaylistName] = new Array();
            pif.__sharedModelServer.playlists = plist
            sendFrameworkApi("frameworkPlaylistAllTrackRemoved",{playlistType:"mp3",pid:pid});
        }
        onSigTracksMoved:{
            core.info("SharedModelServerInterface.qml | usbPlaylist | onSigTracksMoved | pid : "+pid)
            var plist = pif.__sharedModelServer.playlists;var aodPlaylistName = Store.usbPlaylistArr[pid];
            var cModel= pif.usbPlaylist.getPlaylistModel(pid);
            var tmp=[];
            for(var counter=0;counter<cModel.count;counter++){
                var row=cModel.at(counter);
                tmp.push({"media_type":cModel.getValue(counter,"media_type"),"duration":cModel.getValue(counter,"duration"),"cid":cModel.getValue(counter,"cid"),"pathfilename":cModel.getValue(counter,"pathfilename"),
                             "filetype":cModel.getValue(counter,"filetype"),"filenameonly":cModel.getValue(counter,"filenameonly"),"filesize":cModel.getValue(counter,"filesize"),
                             "mp3album":cModel.getValue(counter,"mp3album"),"mp3artist":cModel.getValue(counter,"mp3artist"),"mp3song":cModel.getValue(counter,"mp3song"),
                             "mp3title":cModel.getValue(counter,"mp3title"),"mp3year":cModel.getValue(counter,"mp3year")
                         });
            }
            plist.usbPlaylist[aodPlaylistName]=tmp;
            pif.__sharedModelServer.playlists = plist;
            sendFrameworkApi("frameworkPlaylistMoved",{playlistType:"mp3",pid:pid});
        }
    }


    Connections{
        // target is refrence 2(i.e either pif or pif.vip)
        target:__pifRef;

        onVolumeChanged:{
            core.info("SharedModelServerInterface.qml | volume change event : volume level: "+volumeLevel);
            sendPropertyData("seatbackVolume",volumeLevel);
        }
        onPaVolumeChanged:{
            core.info("SharedModelServerInterface.qml | pa volume change event : volume level: "+volumeLevel);
            sendPropertyData("seatbackPaVolume",volumeLevel);
        }
        onVolumeSourceChanged: sendPropertyData("seatbackVolume",volumeLevel);
        onBrightnessChanged:{
            core.info("SharedModelServerInterface.qml | brightness change event : brightness level: "+brightnessLevel);
            sendPropertyData("seatbackBrightness",brightnessLevel);
        }
        onBacklightStateChanged:{
            core.info("SharedModelServerInterface.qml | backlight state change event : backlight State: "+backlightState);
            sendPropertyData("serverBacklightEnabled",backlightState);
        }
    }

    Connections{
        target:(pif.vip)?pif.vip:null;

        onSigMidPlayBegan:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMidPlayBegan");
            sendApiData("playBlueRayAck","");
        }
        onSigMediaPaused:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMediaPaused");
            sendApiData("pauseBlueRayAck","");
        }
        onSigMediaForward:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMediaForward");
            sendApiData("fastForwardBlueRayAck","");
        }
        onSigMediaRewind:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMediaRewind");
            sendApiData("rewindBlueRayAck","");
        }
        onSigMidPlayStopped:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMidPlayStopped");
            sendApiData("stopBlueRayAck","");
        }
    }

    Connections{
        // target is refrence 1 since these signals winn never come from vip
        target:pif

        onIpodConnectedChanged:{
            core.info("SharedModelServerInterface.qml | ipod state change event : ipod state: "+ipodConnected);
            sendPropertyData("iPodConnected",ipodConnected);
        }
        onInteractiveResetChanged:{
            core.info("SharedModelServerInterface.qml | apiName: interactiveReset");
            sendApiData("interactiveReset","");
        }
        onPowerDownEventReceived:{
            core.info("SharedModelServerInterface.qml | apiName: interactiveReset");
            sendFrameworkApi("frameworkPowerDownEvent")
        }
        onInteractiveOverrideChanged:{
            core.info("SharedModelServerInterface.qml | apiName: interactiveOverride | value: "+interactiveOverride);
            sendFrameworkApi("frameworkInteractiveOverride",{interactiveOverride:interactiveOverride})
        }
    }

    Connections{
        target:pif.launchApp;

        onSigAppLaunched:{
            core.info("SharedModelServerInterface.qml | onSigAppLaunched signal received " + launchid + " " + launchType);
            if(launchType==pif.launchApp.cGAME_USING_ICORE || launchType==pif.launchApp.cGAME_USING_PIF)                
                sendPropertyData("mediaPlayer3MediaType","game");
                //sendPropertyData("mediaPlayer3MediaIdentifier",launchid); We couldn't set this since mediaPlayer3MediaIdentifier is a integer.

                sendApiData("launchAppAck",{launchAppId:launchid})

                if(pif.__sharedModelServer.mediaPlayer3PlayStatus!="play"){
                    sendPropertyData("mediaPlayer3PlayStatus","play");
                }else {
                    sendPropertyData("mediaPlayer3PlayStatus","");
                    sendPropertyData("mediaPlayer3PlayStatus","play");
                }
        }
        onSigAppExit:{
            core.info("SharedModelServerInterface.qml | onAppExitGUI signal received " + launchid + " " + launchType);
            if(launchType==pif.launchApp.cGAME_USING_ICORE || launchType==pif.launchApp.cGAME_USING_PIF) sendPropertyData("mediaPlayer3PlayStatus","stop");
            sendApiData("closeAppAck",{launchAppId:""})
        }
    }

    Connections{
        target:pif.usb;

        onSigUsbConnected:{
            core.info("SharedModelServerInterface.qml | onSigUsbConnected");
            sendPropertyData("usbConnected",1);
        }
        onSigUsbDisconnected:{
            core.info("SharedModelServerInterface.qml | onSigUsbDisconnected | UsbCount: "+pif.usb.getUsbCount());
            if(!pif.usb.getUsbCount()) sendPropertyData("usbConnected",0);
        }
    }

    Connections{
        target: __audioPlayerdataRef;
        onSigUpdateSMCAudioPlayerdata: {
            sendFrameworkApi("frameworkUpdateAudioPlayerdata",params);
        }
    }

    Connections{
        target: __videoPlayerdataRef;
        onSigUpdateSMCVideoPlayerdata: {
            sendFrameworkApi("frameworkUpdateVideoPlayerdata",params);
        }
    }

    Connections {
        target: pif.seatInteractive;
        onSigDataReceived:{
            if(api=="frameworkGetPlayingPlaylistIndex"){
                var mediaType = params.mediaType;
                var numberOfAodPlaylist = pif.getNumberOfAodPlaylist();
                var numberOfVodPlaylist = pif.getNumberOfVodPlaylist();
                if (mediaType=="audio"){
                    var playingPlaylistIndex = pif.aod.getPlayingAlbumId()
                    if(Math.abs(playingPlaylistIndex)<=numberOfAodPlaylist){
                        sendFrameworkApi("frameworkplayingPlaylistIndex",{playlistIndex:playingPlaylistIndex,mediaType:"aod"})
                    }
                    else{
                        var kidsPlaylistIndex = pif.getNumberOfAodPlaylist()+ playingPlaylistIndex
                        sendFrameworkApi("frameworkplayingPlaylistIndex",{playlistIndex:kidsPlaylistIndex,mediaType:"kidsaod"})
                    }
                } else if(mediaType=="video"){
                    playingPlaylistIndex = pif.vod.getPlayingAlbumId()
                    if(Math.abs(playingPlaylistIndex)<=numberOfVodPlaylist){
                        sendFrameworkApi("frameworkplayingPlaylistIndex",{playlistIndex:playingPlaylistIndex,mediaType:"vod"})
                    }
                    else{
                        var vodplaylistIndex = pif.getNumberOfVodPlaylist()+ playingPlaylistIndex
                        sendFrameworkApi("frameworkplayingPlaylistIndex",{playlistIndex:playlistIndex,mediaType:"kidsvod"})
                    }
                }
            }else if(api=="frameworkGetSeatBackStatus"){
                core.log("SharedModelServerInterface.qml | onSigDataReceived | frameworkGetSeatBackStatus | isSMSLoading: "+Store.isSMSLoading);
                if(!Store.isSMSLoading){sendFrameworkApi("frameworkSetSeatBackStatus");}
            }
        }
    }

    function processEntChanged(){Store.isSMSLoading=false;sendFrameworkApi("frameworkSetSeatBackStatus");}
	function setViewData(){sendPropertyData("noOfDimmableWindows",pif.getNoOfDimmableWindows());}
    function resetVariables(){core.info("SharedModelServerInterface.qml | resetVariables ");sendPropertyData("resumeTimeMap",[]);sendPropertyData("midInforArr",[]);}
    function initializeVariables(){core.info("SharedModelServerInterface.qml | initializeVariables ");sendPropertyData('languageLid',core.settings.languageID);}


    Component.onCompleted :{
        core.info("SharedModelServerInterface.qml | SharedModelServerInterface Load COmpleted ");
        Store.aodPlaylistArr = [];
        Store.vodPlaylistArr = [];
        Store.kidsAodPlaylistArr = [];
        Store.kidsVodPlaylistArr = [];
        Store.usbPlaylistArr = [];
        Store.midList =[];
        Store.queArray = [];
        Store.queParamsArray = [];
        Store.isSMSLoading=true;
        playlistUpdateRequiredOnKarma=pif.getPlaylistUpdateRequiredOnKarma();
        if(viewController.getDelayKarmaLoading()){
            interactiveStartup.registerToStage(interactiveStartup.cStageEntOnScreen,[processEntChanged]);
            interactiveStartup.registerToStage(interactiveStartup.cStageEntOffScreen,[processEntChanged]);
            interactiveStartup.registerToStage(interactiveStartup.cStageCloseFlightScreen,[processEntChanged]);
        }
		interactiveStartup.registerToStage(interactiveStartup.cStageSetViewData,[setViewData]);
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOffScreen,[resetVariables]);
        interactiveStartup.registerToStage(interactiveStartup.cStageCloseFlightScreen,[resetVariables]);
        interactiveStartup.registerToStage(interactiveStartup.cStageInitializeStates,[initializeVariables]);
    }
}
