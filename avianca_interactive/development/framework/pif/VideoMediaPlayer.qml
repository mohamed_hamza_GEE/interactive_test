import QtQuick 1.1
import Panasonic.Pif 1.0

AudioMediaPlayer {
    id: videoMediaPlayer
    property alias viewPort:viewPort;
    // Play VOD
    property int __pipID:100;
    property bool __ignoreSystemAspectRatio: false;
    property bool __stretch: false;
    property bool __isTrailer: false;
    property string __mediaType: "videoAggregate";
    property int __sndtrkLid: 0;
    property int __sndtrkType: 2;
    property int __subtitleLid: 0;
    property int __subtitleType: 2;
    property bool __subtitleEnabled: false;
    property bool __soundtrackEnabled: true;
    // Aspect Ratio
    property string __sysAspectRatio: "16x9Adjustable";
    property string __sysAspectValue: "16x9";
    property bool __sysAspectAdjustable: true;
    property string __actualAspectRatio: "16x9Adjustable";
    property string __actualAspectValue: "16x9";
    property bool __actualAspectAdjustable: true;
    // PIP
    property string __pipIndex0:"0,0,0,1,1,true";
    property string __pipIndex1:"0,0,0,1,1,true";
    property string __pipIndex2:"0,0,0,1,1,true";
    // Media player runtime
    property bool __pipState : false;		// true => PIP ON, false => PIP off
    // Signals
    signal sigAspectRatioChanged(string coreAspectRatio,string aspectRatio);
    signal sigStretchState(bool stretch);
    signal sigSoundtrackSet(int soundtracklid);
    signal sigSubtitleSet(int subtitlelid, bool subtitleEnabled);

    // Public Functions
    function getAllMediaPlayInfo() {return {playType: __playType, ignoreSystemAspectRatio : __ignoreSystemAspectRatio, stretch : __stretch, elapseTime : __elapseTime, sndtrkLid : __sndtrkLid, sndtrkType : __sndtrkType, subtitleLid : __subtitleLid, subtitleType : __subtitleType, subtitleEnabled : __subtitleEnabled, amid : __amid, duration : __duration, elapseTimeFormat : __elapseTimeFormat, cid : __cid, cmid : __cmid, playIndex : getPlayingIndex(), mediaType : __mediaType, pipID : __pipID, isTrailer : __isTrailer, aspectRatio: __actualAspectRatio};}
    function toggleStretchState () {
        core.info ("VideoMediaPlayer.qml | toggleStretchState called");
        if (__actualAspectAdjustable) {
            __stretch=(__stretch)?false:true;
            __setPIP();
            sigStretchState(__stretch);
        } else core.info ("VideoMediaPlayer.qml | DIDNT APPLY STRETCH REQUEST SINCE MID IS FIXED TYPE");
        return true;
    }
    // ************************* VIEW PORT CALLS *************************
    function __setPIP() {
        core.debug("VideoMediaPlayer.qml | __setPIP called.");
        var pipIndex = __stretch && __actualAspectAdjustable?2:(__actualAspectValue=="16x9"?1:0);
        var pip = (pipIndex==0)?__pipIndex0:(pipIndex==1)?__pipIndex1:__pipIndex2;
        pip = pip.split(',');
        viewPort.position.x = pip[1];
        viewPort.position.y = pip[2];
        viewPort.dimension.width = pip[3];
        viewPort.dimension.height = pip[4];
        viewPort.videoColorKey = pif.getPIPColorKey();
        viewPort.videoColorMask = pif.getPIPColorMask();
        viewPort.enableColorKey = pip[5];
        viewPort.refresh();
        __pipState = true;
        core.info("VideoMediaPlayer.qml | PIP WINDOW SET");
        return true;
    }

    function __setPIPByID (pipID,piparr) {
        core.info("VideoMediaPlayer.qml | __setPIPByID Called. pipID: "+pipID);
        if (!pipID || !piparr) {core.error ("VideoMediaPlayer.qml | Either Missing PIP ID or Pip Array. EXITING..."); return false;}
        var t=pipID*10;
        __pipIndex0 = piparr[t++].join(',');
        __pipIndex1 = piparr[t++].join(',');
        __pipIndex2 = piparr[t].join(',');
        return __setPIP();
    }

    function __setPIPByArray(pipArr) {
        core.info("VideoMediaPlayer.qml | __setPIPByArray CALLED. pipArr: "+pipArr);
        if (pipArr[0]==undefined) {core.error ("VideoMediaPlayer.qml | Missing PIP data. EXITING..."); return false;}
        viewPort.position.x = pipArr[0];
        viewPort.position.y = pipArr[1];
        viewPort.dimension.width = pipArr[2];
        viewPort.dimension.height = pipArr[3];
        viewPort.videoColorKey = pif.getPIPColorKey();
        viewPort.videoColorMask = pif.getPIPColorMask();
        viewPort.enableColorKey = pipArr[4];
        viewPort.refresh();
        __pipState = true;
        core.info("VideoMediaPlayer.qml | SET PIP WINDOW BY ARRAY");
        return true;
    }

    function __pipOn() {
        core.info("VideoMediaPlayer.qml | PIP-ON CALLED");
        viewPort.open = true;
        __pipState = true;
        core.info ("VideoMediaPlayer.qml | PIP TURNED ON. __pipState: "+__pipState);
        return true;
    }

    function __pipOff() {
        core.info("VideoMediaPlayer.qml | PIP-OFF CALLED");
        viewPort.open = false;
        __pipState = false;
        core.info ("VideoMediaPlayer.qml | PIP TURNED OFF. __pipState: "+__pipState);
        return true;
    }

    function getPipState() {return __pipState;}
    function getIsScreenAdjustable() { return __actualAspectAdjustable;}
    function getAspectRatio() { return __actualAspectRatio;}
    function getStretchState() { return __stretch;}
    function getSoundtrackLid() { return __sndtrkLid;}
    function getSubtitleLid() { return __subtitleEnabled?__subtitleLid:-1;}
    function getSubtitleEnabled() { return __subtitleEnabled;}
    function getSubtitleType() {return __subtitleType;}
    function getSoundtrackEnabled() { return __soundtrackEnabled;}
    function getIsTrailer() { return __isTrailer;}
    function getDuration() { return __duration;}

    function setPIPByArray (pipArr) {
        core.info (__playerType+" | setPIPByArray | Empty function definition");return true;
    }
    function pipOn () {
        core.info (__playerType+" | pipOn | Empty function definition");return true;
    }
    function pipOff () {
        core.info (__playerType+" | pipOff | Empty function definition");return true;
    }
    function setPlayingSubtitle (lid) {
        core.info (__playerType+" | setPlayingSubtitle | Empty function definition");return true;
    }
    function setPlayingSoundtrack (lid) {
        core.info (__playerType+" | setPlayingSoundtrack | Empty function definition");return true;
    }
    function forward (retainspeed) {
        core.info (__playerType+" | forward | Empty function definition");return true;
    }
    function rewind (retainspeed) {
        core.info (__playerType+" | rewind | Empty function definition");return true;
    }
    function setEnableSubtitle(enable) {
        __subtitleEnabled=enable;
        mediaPlayer.subtitleEnabled = __subtitleEnabled;
        core.info("VideoMediaPlayer.qml | __setEnableSubtitle | Subtitle Enable: "+__subtitleEnabled);
        return true;
    }
    // Private Functions
    function __setMediaPlayParams(p) {
        __ignoreSystemAspectRatio = p.ignoreSystemAspectRatio;
        __stretch = p.stretch;
        __elapseTime = p.elapseTime;
        __sndtrkLid = p.sndtrkLid;
        __sndtrkType = p.sndtrkType;
        __subtitleLid = p.subtitleLid;
        __subtitleType = p.subtitleType;
        __subtitleEnabled = p.subtitleEnabled;
        __soundtrackEnabled = p.soundtrackEnabled;
        __albumid = p.amid;
        __duration = p.duration;
        __elapseTimeFormat = p.elapseTimeFormat;
        __cid = p.cid;
        __playerIndex = p.playIndex;
        __modelIndex = p.playIndex;
        __mediaType = p.mediaType;
        __pipID = p.pipID;
        __isTrailer = p.isTrailer;
        __playType = p.playType;
        __amid=p.amid;
        __rating=p.rating;
        setMediaRepeat(p.repeatType);
        setMediaShuffle(false);
        mediaPlayer.soundtrackLID = __sndtrkLid;
        mediaPlayer.soundtrackType = __sndtrkType;
        mediaPlayer.subtitleLID = __subtitleLid;
        mediaPlayer.subtitleType = __subtitleType;
        mediaPlayer.subtitleEnabled = __subtitleEnabled;
        mediaPlayer.soundtrackEnabled = __soundtrackEnabled;
    }

    function __mediaPlay (p,piparr) {
        var ret;
        core.debug ("VideoMediaPlayer.qml | PLAY Called:");
        __setMediaPlayParams(p);
        mediaPlaylist.aggregate = (p.playType==1)?true:false;
        mediaPlayer.playlist = mediaPlaylist;
        if (__ignoreSystemAspectRatio) __setActualAspectRatio(p.aspectRatio?p.aspectRatio:"16x9Adjustable"); else __setActualAspectRatio(__sysAspectRatio);
        sigAspectRatioChanged((getAspectRatio()=="16x9Fixed"?"aspect-16:9-fixed":getAspectRatio()=="4x3Fixed"?"aspect-4:3-fixed":getAspectRatio()=="4x3Adjustable"?"aspect-4:3-notfixed":"aspect-16:9-notfixed"),getAspectRatio())
        if(p.webPipArray!=undefined){__setPIPByArray(p.webPipArray)}else{ __setPIPByID(__pipID,piparr);}
        __playerIndex=p.playIndex;
        if(__elapseTime){ret = mediaPlayer.playTrack(__playerIndex,__elapseTime);}
        else{ret = mediaPlayer.gotoItem(__playerIndex);}
        core.info ("VideoMediaPlayer.qml | Playing __playerIndex: "+__playerIndex+" __elapseTime: "+__elapseTime+" Return status: "+ret);
        if (!core.pc) {fakePlayAckTimer.restart(); return ret;} else {simPlayAckTimer.elapse=__elapseTime; simPlayAckTimer.restart(); return true;}
    }

    function __mediaPlayAck (pos,pindex,mindex) {
        core.info ("VideoMediaPlayer.qml | mediaPlayCallAck CALLED: position: "+pos+" playIndex: "+pindex+" __mediaState: " + __mediaState);
        __mediaState = cMEDIA_PLAY;
        __playerIndex = pindex;
        __modelIndex = mindex;
        __setPlayingMidInfo(mindex);
        __skipSpeed = 1;
        fakePlayAckTimer.stop();
        __startMediaTimer(__timerInterval*1000,pos);
        if (__skipPreviousTimeout && pos==0) skipPreviousTimer.restart();
        sigAggPlayBegan(__amid,__playType,__albumid);
        sigMidPlayBegan(__amid,__cmid,mindex,__playType,__albumid);
        sigSoundtrackSet(__sndtrkLid);
        sigSubtitleSet(__subtitleLid,__subtitleEnabled);
        sigStretchState(__stretch);
        return true;
    }

    function __mediaStopAck (pos,pindex,mindex,cause) {
        core.info ("VideoMediaPlayer.qml | __mediaStopAck called: position: "+pos+" playIndex: "+pindex+" __mediaState: "+__mediaState+" Cause: "+cause);
        __mediaState = cMEDIA_STOP;
        __playerIndex = pindex;
        __modelIndex = mindex;
        __pipState = false;
        if (fakePlayAckTimer.running) fakePlayAckTimer.stop();
        if (fakeStopAckTimer.running) fakeStopAckTimer.stop();
        if (__skipSpeed==1) __mediaPositionAck((__mediaElapseTime!=0)?pos:0);   // __mediaElapseTime==0 incase eos occured
        else __skipSpeed = 1;
        core.info ("VideoMediaPlayer.qml | Elapsetime Of Mid: "+__cmid+" is "+__elapseTime);
        __stopMediaTimer();
        if (skipPreviousTimer.running) skipPreviousTimer.stop();
        sigMidPlayStopped(__amid,__cmid,mindex);
        sigAggPlayStopped(__amid,__cmid,mindex,cause);
        return true;
    }

    function __mediaForceStopAck () {
        __pipOff();
        __mediaStopAck(__elapseTime,__playerIndex,__modelIndex,'forceStop');
    }

    // ************************* ASPECT RATIO CALLS *************************
    function __setSystemAspectRatio(asp) {
        core.info("VideoMediaPlayer.qml | Set System Aspect Ratio Called. AspectRatio: "+asp);
        __sysAspectRatio = asp;
        __sysAspectValue = (__sysAspectRatio.search("16x9")!= -1)?"16x9":"4x3";
        __sysAspectAdjustable = (__sysAspectRatio.search("Fixed")== -1)?true:false;		// 4x3Fixed, 16x9Fixed, 4x3Adjustable, 16x9Adjustable
    }

    function __setActualAspectRatio(asp){
        core.info("VideoMediaPlayer.qml | Set Actual Aspect Ratio Called. AspectRatio: "+asp);
        __actualAspectRatio = asp;
        __actualAspectValue = (__actualAspectRatio.search("16x9")!= -1)?"16x9":"4x3";
        __actualAspectAdjustable = (__actualAspectRatio.search("Fixed")== -1)?true:false;		// 4x3Fixed, 16x9Fixed, 4x3Adjustable, 16x9Adjustable
    }

    function __aspectRatioChangeAck (newAspectRatio) {
        __setSystemAspectRatio((newAspectRatio=="aspect-16:9-fixed")?"16x9Fixed":(newAspectRatio=="aspect-4:3-fixed")?"4x3Fixed":(newAspectRatio=="aspect-4:3-notfixed")?"4x3Adjustable":"16x9Adjustable");
        if (__ignoreSystemAspectRatio) return false;
        __setActualAspectRatio (__sysAspectRatio);
        __setPIP();
        sigAspectRatioChanged(newAspectRatio,__sysAspectRatio);
        return true;
    }

    function __setSubtitle(lid,enable,subtitleType) {           // code to be added for supporting subtitle in model for playbymodel -- Jude
        __subtitleLid = lid;
        __subtitleEnabled=enable;
        __subtitleType=(subtitleType)?subtitleType:__subtitleType;
        if (enable) mediaPlayer.setSubtitle(__subtitleLid,__subtitleType);     // for lid=0, core uses last selected subtitlelid.
        mediaPlayer.subtitleEnabled=enable;
        core.info("VideoMediaPlayer.qml | __setSubtitle | Subtitle enable: "+__subtitleEnabled+" Lid: "+__subtitleLid);
        sigSubtitleSet(__subtitleLid, __subtitleEnabled);
        return true;
    }

    function __setSoundtrack(sndtrkLid) {           // code to be added for supporting soundtrack in model for playbymodel -- Jude
        core.debug("VideoMediaPlayer.qml | __setSoundtrack | sndtrkLid: "+sndtrkLid);
        __sndtrkLid = (sndtrkLid)?sndtrkLid:0;   //__sndtrkLid;
        __setMediaPlayerSoundtrack();
        core.info("VideoMediaPlayer.qml | __setSoundtrack | Soundtrack Set: "+__sndtrkLid);
        sigSoundtrackSet(__sndtrkLid);
        return true;
    }

    function __setMediaPlayerSoundtrack(){mediaPlayer.setSoundtrack(__sndtrkLid,__sndtrkType); return true;}

    Component.onCompleted: mediaPlayer.viewPort = viewPort;
    ViewPort { id: viewPort; position.x: 0; position.y: 0; dimension.height: 0; dimension.width: 0;	deviceManager: deviceMngr; }
}
