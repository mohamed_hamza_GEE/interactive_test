import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item{
    property int __prevVodState:pif.vod.cMEDIA_STOP;
    property int __prevAodState:pif.aod.cMEDIA_STOP;
    property variant __audioPlayerdataRef;
    property variant __videoPlayerdataRef;
    property int __playlistId: 0;
    property int __aggregateMid: 0;
    property int __cid: 0
    property string __mediaType: "";
    property int __rating:0;
    property variant __pifRef:pif;
    property bool playlistUpdateRequiredOnKarma:true;
    property bool paymentDoneFromSeat: true;

    signal clientConnected();
    signal clientDisConnected();
    signal sigDataReceived(string api,variant params);


    Connections{
        target:pif
        onServiceAccessChanged:{ if(pif.getMonitorType()!=pif.cLRU_VKARMA) sendFrameworkApi('frameworkServiceBlockChange',{serviceMid:serviceMid,accessType:accessType})}
        onMidAccessChanged:{{ if(pif.getMonitorType()!=pif.cLRU_VKARMA) sendFrameworkApi('frameworkMidBlockChange',{mid:mid,accessType:accesstype})}}
        onSeatRatingChanged:{{ if(pif.getMonitorType()!=pif.cLRU_VKARMA) sendFrameworkApi('frameworkSeatRatingChange',{seatRating:seatRating})}}
    }

    Connections{
        target: core.pif.ppv?core.pif.ppv:null;
        onSigPaymentSuccessful:{
            if(!paymentDoneFromSeat) {paymentDoneFromSeat=true; return true;}
            if(pif.getMonitorType()!=pif.cLRU_VKARMA) sendFrameworkApi('frameworkPaymentSuccessful',{amid:amid})
        }
    }

    function sendApiData(api,params){
        core.info("SharedModelServerInterface.qml | sendApiData("+api+",params)");
        pif.__sharedModelServer.sendApiData(api,params);
    }

    function sendPropertyData(propertyName,propertyValue){
        if(propertyName=="resumeTimeMap"){core.info("SharedModelServerInterface.qml | sendPropertyData: "+propertyName);}else {core.info("SharedModelServerInterface.qml | sendPropertyData("+propertyName+","+propertyValue+")");}
        pif.__sharedModelServer.sendPropertyData(propertyName,propertyValue);
    }

    function sendUserDefinedApi(userDefinedApi,apiParams){ if(userDefinedApi.indexOf("userDefined")!=-1) sendApiData(userDefinedApi,(apiParams)?apiParams:''); else core.debug("SharedModelServerInterface.qml | userDefinedApi should be sufficed with word userDefined");}
    function sendFrameworkApi(frameworkApi,apiParams){ if(frameworkApi.indexOf("framework")!=-1) sendApiData(frameworkApi,(apiParams)?apiParams:''); else core.debug("SharedModelServerInterface.qml | frameworkAPI should be sufficed with word framework");}
    function setAudioPlayerdataRef(val){__audioPlayerdataRef=val; return true;}
    function setVideoPlayerdataRef(val){__videoPlayerdataRef=val; return true;}
    function setDefaultValues(){
        sendPropertyData("seatbackVolume",__pifRef.getDefaultAudioVolume())
        sendPropertyData("seatbackBrightness",__pifRef.getDefaultBrightness())
    }
    function resetVariables(){core.info("SharedModelServerInterface.qml | resetVariables ");sendPropertyData("resumeTimeMap",[]);}

    // reference 2 can be either pif or vip. The below function will change the reference 2. This will be called from Vip.qml
    function __setPifReference(val){
        __pifRef = val;
    }
    function __addingMidToPlaylist(dModel){
        core.info("SharedModelServerInterface.qml | __addingMidToPlaylist | dModel Count :"+dModel.count)
        if(dModel.count && __mediaType=="aod") pif.aodPlaylist.addAllTracks(dModel,__cid,__aggregateMid,__playlistId,__rating);
        else if (dModel.count && __mediaType=="vod")pif.vodPlaylist.addAllTracks(dModel,__cid,__aggregateMid,__playlistId,__rating);
        else if (dModel.count && __mediaType=="kidsaod")pif.kidsAodPlaylist.addAllTracks(dModel,__cid,__aggregateMid,__playlistId,__rating);
        else if (dModel.count && __mediaType=="kidsvod")pif.kidsVodPlaylist.addAllTracks(dModel,__cid,__aggregateMid,__playlistId,__rating);
    }
    function __createPlaylist (params){
        core.info("SMSI.qml | __createPlaylist | playlistIndex :"+params.playlistIndex+" , name : "+params.name+ " , mediaType : "+params.mediaType)
        var plist = pif.__sharedModelServer.playlists;
        if (params.mediaType=="aod"){
            plist.aod[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.aodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        } else if (params.mediaType=="vod"){
            plist.vod[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.vodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        } else if (params.mediaType=="kidsaod"){
            plist.kidsaod[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.kidsAodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        } else if (params.mediaType=="kidsvod"){
            plist.kidsvod[Math.abs(params.playlistIndex) + "|" + params.name] = new Array();
            Store.kidsVodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        pif.__sharedModelServer.playlists = plist;
    }


    function __updatePlaylistName (params){
        core.info("SMSI.qml | __updatePlaylistName | playlistIndex :"+params.playlistIndex+" , name : "+params.name+ " , mediaType : "+params.mediaType)
        var plist  = pif.__sharedModelServer.playlists;
        if (params.mediaType=="aod"){
            var aodPlaylistName = Store.aodPlaylistArr[Math.abs(params.playlistIndex)];
            core.log("aodPlaylistName :"+aodPlaylistName+ "New Name : "+params.name)
            plist.aod[Math.abs(params.playlistIndex) + "|" + params.name] = plist.aod[aodPlaylistName]
            delete plist.aod[aodPlaylistName];
            Store.aodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        else if (params.mediaType=="vod"){
            var vodPlaylistName = Store.vodPlaylistArr[Math.abs(params.playlistIndex)];
            core.log("vodPlaylistName :"+vodPlaylistName+ "New Name : "+params.name)
            plist.vod[Math.abs(params.playlistIndex) + "|" + params.name] = plist.vod[vodPlaylistName]
            delete plist.vod[vodPlaylistName];
            Store.vodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        else if (params.mediaType=="kidsaod"){
            var kidsAodPlaylistName = Store.kidsAodPlaylistArr[Math.abs(params.playlistIndex)];
            core.log("kidsAodPlaylistName :"+kidsAodPlaylistName+ "New Name : "+params.name)
            plist.kidsaod[Math.abs(params.playlistIndex) + "|" + params.name] = plist.kidsaod[kidsAodPlaylistName]
            delete plist.kidsaod[kidsAodPlaylistName];
            Store.kidsAodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        else if (params.mediaType=="kidsvod"){
            var kidsVodPlaylistName = Store.kidsVodPlaylistArr[Math.abs(params.playlistIndex)];
            core.log("kidsVodPlaylistName :"+kidsVodPlaylistName+ "New Name : "+params.name)
            plist.kidsvod[Math.abs(params.playlistIndex) + "|" + params.name] = plist.kidsvod[kidsVodPlaylistName]
            delete plist.kidsvod[kidsVodPlaylistName];
            Store.kidsVodPlaylistArr[Math.abs(params.playlistIndex)] = Math.abs(params.playlistIndex) + "|" + params.name;
        }
        pif.__sharedModelServer.playlists = plist;
    }

    Connections{
        target: pif.__sharedModelServer;

        onSigSMClientDisConnected:{clientDisConnected();}
        onSigSMSynchronized:{clientConnected();}
        onSigSMDataReceived:{
            core.info("SharedModelServerInterface.qml | api is " + api + " params is " + apiParameters);

            var params = [];
            if(api=="playVod" || api=="playTrailer"){
                params = {mid:apiParameters.mid,aggregateMid:apiParameters.aggregateMid,soundtrackLid:apiParameters.soundtrackLid,soundtrackType:apiParameters.soundtrackType,subtitleLid:apiParameters.subtitleLid,subtitleType:apiParameters.subtitleType,elapsedTime:apiParameters.elapsedTime,cid:apiParameters.cid}
                core.info("Received message from SM Server | apiName: "+ api + " | mid : "+ params.mid + ", aggregateMid : " + params.aggregateMid + ", soundtrackLid : " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType + ", subtitleLid : " + params.subtitleLid + ", subtitleType : " + params.subtitleType + ", elapsedTime : " + params.elapsedTime + ", cid : " + params.cid);
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);
            } else if(api=="playExtv" ){
                params = {mid:apiParameters.mid,soundtrackLid:apiParameters.soundtrackLid,soundtrackType:apiParameters.soundtrackType,subtitleLid:apiParameters.subtitleLid,subtitleType:apiParameters.subtitleType,cid:apiParameters.cid}
                core.info("Received message from SM Server | apiName: "+ api + " | mid : "+ params.mid + ", aggregateMid : " + params.aggregateMid + ", soundtrackLid : " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType + ", subtitleLid : " + params.subtitleLid + ", subtitleType : " + params.subtitleType + ", elapsedTime : " + params.elapsedTime + ", cid : " + params.cid);
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);
            } else if(api=="aspectRatio"){
                params = {aspectRatio:pif.__sharedModelServer.aspectRatio}
                core.info("Received message from SM Server | apiName: "+ api +" | aspectRatio: " + params.aspectRatio);
                if(pif.lastSelectedVideo.getIsScreenAdjustable())pif.lastSelectedVideo.toggleStretchState();
                sigDataReceived(api,params);

            } else if(api=="videoSoundtrack") {
                params = {soundtrackLid:apiParameters.soundtrackLid,soundtrackType:apiParameters.soundtrackType}
                core.info("Received message from SM Server | apiName: "+ api +" | soundtrackLid: " + params.soundtrackLid + ", soundtrackType : " + params.soundtrackType);
                pif.vod.setPlayingSoundtrack(params.soundtrackLid);
                sigDataReceived(api,params);
            } else if(api=="videoSubtitle"){
                params = {subtitleLid:apiParameters.subtitleLid,subtitleType:apiParameters.subtitleType}
                core.info("Received message from SM Server | apiName: "+ api +" | subtitleLid: " + params.subtitleLid + ", subtitleType : " + params.subtitleType);
                pif.vod.setPlayingSubtitle(params.subtitleLid,params.subtitleType);
                sigDataReceived(api,params);

            } else if(api=="setMediaPosition"){
                params = {elapsedTime:apiParameters.elapsedTime}
                core.info("Received message from SM Server | apiName: "+ api +" | elapsedTime: " + params.elapsedTime);
                if(pif.vod.getMediaState()==pif.vod.cMEDIA_PLAY){pif.vod.setMediaPosition(params.elapsedTime);}
                else if(pif.aod.getMediaState()==pif.aod.cMEDIA_PLAY){pif.aod.setMediaPosition(params.elapsedTime);}
                sigDataReceived(api,params);
            } else if(api=="mediaFastForward" || api=="mediaRewind"){ // forward request to GUI
                core.info("Received message from SM Server | apiName: "+ api);
                sigDataReceived(api,"");

            } else if(api=="mediaPause"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.pause();
                else if(pif.lastSelectedVideo.getMediaState()!=pif.vod.cMEDIA_STOP)sigDataReceived(api,"");
            } else if(api=="mediaResume"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.resume();
                else if(pif.lastSelectedVideo.getMediaState()!=pif.vod.cMEDIA_STOP)sigDataReceived(api,"");

            } else if(api=="videoStop"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: "+ api);
                sigDataReceived(api,"");
            } else if(api=="audioStop"){
                core.info("Received message from SM Server | apiName: "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.stop();

            } else if(api=="mediaNext"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.playNext();
                sigDataReceived(api,"");
            } else if(api=="mediaPrevious"){ // forward request to GUI for VOD operation
                core.info("Received message from SM Server | apiName: "+ api);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP)pif.lastSelectedAudio.playPrevious();
                sigDataReceived(api,"");

            } else if(api=="changeLanguage"){
                params = {languageIso:apiParameters.languageIso}
                core.info("Received message from SM Server | apiName: " + api + " | languageIso " + params.languageIso);
                sigDataReceived(api,params);
            } else if(api=="seatbackVolume"){
                params = {seatbackVolume:pif.__sharedModelServer.seatbackVolume}
                core.info("Received message from SM Server | change seatbackVolume to : " + params.seatbackVolume);
                __pifRef.setVolumeByValue(params.seatbackVolume);
                sigDataReceived(api,params);

            } else if(api=="headsetVolume"){
                params = {headsetVolume:pif.__sharedModelServer.headsetVolume}
                core.info("Received message from SM Server | change headsetVolume to : " + params.headsetVolume);
                pif.setVolumeByValue(params.headsetVolume);
                sigDataReceived(api,params);
            } else if(api=="muteVolume"){
                params = {muteVolume:pif.__sharedModelServer.muteVolume}
                core.info("Received message from SM Server | change muteVolume to : " + params.muteVolume);
                __pifRef.toggleMuteState();sigDataReceived(api,params);
            } else if(api=="muteHeadsetVolume"){
                params = {muteHeadsetVolume:pif.__sharedModelServer.muteHeadsetVolume}
                core.info("Received message from SM Server | change muteHeadsetVolume to : " + params.muteHeadsetVolume);
                pif.toggleMuteState();sigDataReceived(api,params);
            } else if(api=="seatbackBrightness"){
                params = {seatbackBrightness:pif.__sharedModelServer.seatbackBrightness}
                core.info("Received message from SM Server | change seatbackBrightness to : " + params.seatbackBrightness);
                __pifRef.setBrightnessByValue(params.seatbackBrightness)
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);
            } else if(api=="playAod"){
                params = {mid:apiParameters.mid,aggregateMid:apiParameters.aggregateMid,mediaRepeat:apiParameters.mediaRepeat}
                core.info("Received message from SM Server | apiName: " +api+ " | mid: " + params.mid + ", aggregateMid: " + params.aggregateMid + ", mediaRepeat: " + params.mediaRepeat);
                sigDataReceived(api,params);

            } else if(api=="mediaRepeat"){
                params = {mediaRepeat:apiParameters.mediaRepeat}
                core.info("Received message from SM Server | apiName: "+ api + " | mediaRepeat: " + params.mediaRepeat);
                if(pif.lastSelectedAudio.getMediaState()!=pif.aod.cMEDIA_STOP){
                    if(params.mediaRepeat=="none") pif.lastSelectedAudio.setMediaRepeat(0);
                    else if(params.mediaRepeat=="current") pif.lastSelectedAudio.setMediaRepeat(2);
                    else if(params.mediaRepeat=="all") pif.lastSelectedAudio.setMediaRepeat(1);
                }
                sigDataReceived(api,params);
            } else if(api=="mediaShuffle"){
                params = {mediaShuffle:pif.__sharedModelServer.mediaShuffle}
                core.info("Received message from SM Server | apiName: "+ api + " | mediaShuffle: " + params.mediaShuffle);
                sigDataReceived(api,params);

            } else if(api=="launchApp"){
                params = {launchAppId:apiParameters.launchAppId}
                if(typeof(params.launchAppId)!="number") params.launchAppId=parseInt(params.launchAppId,10)
                core.info("Received message from SM Server | apiName: "+ api + " | launchAppId: " + params.launchAppId);
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);
            } else if(api=="closeApp"){
                core.info("Received message from SM Server | apiName: "+ api);
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);

            } else if(api=="backlightEnabled"){
                (pif.__sharedModelServer.backlightEnabled)? __pifRef.setBacklightOn():__pifRef.setBacklightOff();
                core.info("Received message from SM Server | backlightEnabled " + pif.__sharedModelServer.backlightEnabled);
                sigDataReceived(api,params);
            } else if(api=="showKeyboard"){
                params = {showKeyboard:pif.__sharedModelServer.showKeyboard}
                core.info("Received message from SM Server | apiName: "+ api + " | showKeyboard "+ params.showKeyboard)
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);

            } else if(api=="keyboardData"){
                params = {keyboardData:pif.__sharedModelServer.keyboardData}
                core.info("Received message from SM Server | apiName: "+ api + " | keyboardData : "+ params.keyboardData)
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);
            }else if(api=="launchIpod" || api=="launchCategory"){
                params = {launchAppId:apiParameters.launchAppId}
                core.info("Received message from SM Server | apiName: "+ api + " | "+ params.launchAppId)
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,params);

            } else if(api=="usbAgreement"){
                params = {usbAgreement:pif.__sharedModelServer.usbAgreement}
                core.info("Received message from SM Server | apiName: "+ api + " | "+ params.usbAgreement)
                sigDataReceived(api,params);
            } else if(api=="iPodAgreement"){
                params = {iPodAgreement:pif.__sharedModelServer.iPodAgreement}
                core.info("Received message from SM Server | apiName: "+ api + " | "+ params.iPodAgreement)
                sigDataReceived(api,params);

            } else if (api=="createPlaylist"){
                params = {playlistIndex:apiParameters.playlistIndex,name:apiParameters.name,mediaType:apiParameters.mediaType}
                core.info("Received message from seat | apiName: "+ api + " | playlistIndex : "+ params.playlistIndex + ", name : "+ params.name + " , mediaType :"+params.mediaType)
                __createPlaylist(params)
            } else if (api=="updatePlaylistName"){
                params = {playlistIndex:apiParameters.playlistIndex,name:apiParameters.name,mediaType:apiParameters.mediaType}
                core.info("Received message from seat | apiName: "+ api + " | playlistIndex : "+ params.playlistIndex + ", name : "+ params.name + " , mediaType :"+params.mediaType)
                __updatePlaylistName(params)

            } else if(api=="addMidToPlaylist"){
                params = {playlistIndex:apiParameters.playlistIndex,mids:apiParameters.mids,aggregateMid:apiParameters.aggregateMid,cid:apiParameters.cid,mediaType:apiParameters.mediaType,rating:apiParameters.rating};
                core.info("Received message from seat | apiName: "+ api + " | playlistIndex : "+ params.playlistIndex + ", mids: " + params.mids + ", aggregateMid : " + params.aggregateMid + ", cid : " + params.cid+ " ,rating :"+params.rating);
                Store.midList=params.mids;
                __playlistId = params.playlistIndex;
                __aggregateMid = params.aggregateMid;
                __cid = params.cid;
                __mediaType = params.mediaType;
                __rating = params.rating;
                core.dataController.getPlaylistData([Store.midList,"","","","",100],__addingMidToPlaylist)
            } else if(api=="removeMidFromPlayist"){
                params = {mids:apiParameters.mids,mediaType:apiParameters.mediaType};
                core.info("Received message from SM Server | apiName: "+ api +" | , mids : " + params.mids+ " , mediaType : "+params.mediaType);
                var midLists=params.mids;
                for (var i=0;i<midLists.length;i++){
                    if (params.mediaType=="aod")pif.aodPlaylist.removeTrack(midLists[i])
                    else if (params.mediaType=="vod")pif.vodPlaylist.removeTrack(midLists[i])
                    else if (params.mediaType=="kidsaod")pif.kidsAodPlaylist.removeTrack(midLists[i])
                    else if (params.mediaType=="kidsvod")pif.kidsVodPlaylist.removeTrack(midLists[i])
                }

            } else if(api=="removeAllFromPlaylist"){
                params = {playlistIndex:apiParameters.playlistIndex,mediaType:apiParameters.mediaType};
                core.info("Received message from SM Server | apiName: "+ api +" | playlistIndex: " + params.playlistIndex + " , mediaType : "+params.mediaType);
                var playlistIndex = params.playlistIndex;
                if(params.mediaType=="aod")pif.aodPlaylist.removeAllTracks(playlistIndex)
                else if (params.mediaType=="vod")pif.vodPlaylist.removeAllTracks(playlistIndex)
                else if (params.mediaType=="kidsaod")pif.kidsAodPlaylist.removeAllTracks(playlistIndex)
                else if (params.mediaType=="kidsvod")pif.kidsVodPlaylist.removeAllTracks(playlistIndex)
            } else if (api=="playJukebox"){
                params = {playlistIndex:apiParameters.playlistIndex,mids:apiParameters.mid,mediaType:apiParameters.mediaType};
                core.info("Received message from SM Server | apiName: "+ api +" | playlistIndex: " + params.playlistIndex + ", mid : " + params.mid + " , mediaType : "+params.mediaType)
                sigDataReceived(api,params)

            } else if(api=="frameworkPaymentSuccessful"){
                paymentDoneFromSeat=false;
                pif.ppv.sigPaymentSuccessful(apiParameters.amid);

            } else if(api.indexOf("userDefined")!=-1 || api.indexOf("framework")!=-1){
                core.info("Received message from SM Server | apiName: "+ api + " received")
                sigDataReceived(api,apiParameters);
            } else if((api=="playBlueRay") || (api=="stopBlueRay") || (api=="pauseBlueRay") || (api=="resumeBlueRay") || (api=="fastForwardBlueRay") || (api=="rewindBlueRay")||(api=="nextBlueRay")||(api=="previousBlueRay")||(api=="menuTitleBlueRay")||(api=="menuDiscBlueRay")||(api=="upBlueRay")||(api=="downBlueRay")||(api=="leftBlueRay")||(api=="rightBlueRay")||(api=="enterBlueRay")){
                core.info("Received message from SM Server | apiName: "+ api);
                pif.screenSaver.restartScreenSaver();sigDataReceived(api,"");

            } else if(api=="setBlueRayMediumType"){
                params = {blueRayMediumType:apiParameters.blueRayMediumType};
                core.info("Received message from SM Server | apiName: "+ api);
                sigDataReceived(api,params);

            } else { //launchUsb etc
                core.info("Received message from SM Server | apiName: " + api);
                sigDataReceived(api,params);
            }
        }
    }

    function assignAspectRatio(){
        var propertyValue;
        core.debug("SharedModelServerInterface.qml | assignAspectRatio | isScreenAdjustable: "+pif.lastSelectedVideo.getIsScreenAdjustable()+" aspectRatio: "+pif.lastSelectedVideo.getAspectRatio()+" stretchState: "+pif.lastSelectedVideo.getStretchState())
        if(pif.lastSelectedVideo.getIsScreenAdjustable()==false){sendPropertyData("aspectRatio","disable"); return}

        if(pif.lastSelectedVideo.getAspectRatio().search("16x9Fixed")!=-1 || pif.lastSelectedVideo.getAspectRatio().search("4x3Fixed")!=-1) propertyValue="disable"
        else if(pif.lastSelectedVideo.getAspectRatio().search("16x9Adjustable")!=-1) propertyValue=(pif.lastSelectedVideo.getStretchState())?"16x9Stretched":"16x9"
        else if(pif.lastSelectedVideo.getAspectRatio().search("4x3Adjustable")!=-1) propertyValue=(pif.lastSelectedVideo.getStretchState())?"4x3Stretched":"4x3"
        sendPropertyData("aspectRatio",propertyValue)
    }

    Connections {
        // target is refrence 1
        target: pif.vod;

        onSigMidPlayBegan: {
            var vodInfo;
            core.info("SharedModelServerInterface.qml | sigMidPlayBegan signal received")
            vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();

            if(__prevVodState==pif.vod.cMEDIA_STOP || __prevVodState==pif.vod.cMEDIA_PLAY){
                sendPropertyData("mediaType",vodInfo.mediaType);
                if(vodInfo.isTrailer){
                    sendApiData("playTrailerAck",{mid:vodInfo.cmid,aggregateMid:vodInfo.amid,soundtrackLid:vodInfo.sndtrkLid,soundtrackType:vodInfo.sndtrkType,subtitleLid:vodInfo.subtitleLid,subtitleType:vodInfo.subtitleType,elapsedTime:vodInfo.elapseTime});
                }
                else{
                    sendApiData("playVodAck",{mid:vodInfo.cmid,aggregateMid:vodInfo.amid,soundtrackLid:vodInfo.sndtrkLid,soundtrackType:vodInfo.sndtrkType,subtitleLid:vodInfo.subtitleLid,subtitleType:vodInfo.subtitleType,elapsedTime:vodInfo.elapseTime});
                }
                sendPropertyData("mediaPlayerState","play");
            }
            else if(__prevVodState==pif.vod.cMEDIA_REWIND||__prevVodState==pif.vod.cMEDIA_FORWARD||__prevVodState==pif.vod.cMEDIA_PAUSE){
                core.info("Send message | apiName: mediaResumeAck");
                sendApiData("mediaResumeAck","");
                sendPropertyData("mediaPlayerState","play");
            }
            __prevVodState = pif.vod.cMEDIA_PLAY;
        }
        onSigAspectRatioChanged: assignAspectRatio();
        onSigStretchState: assignAspectRatio();
        onSigSoundtrackSet:{
            var vodInfo;
            vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();
            core.info("SharedModelServerInterface.qml | apiName: videoSoundtrackAck | soundtrackLid : " + soundtracklid)
            sendApiData("videoSoundtrackAck",{soundtrackLid:soundtracklid,soundtrackType:vodInfo.sndtrkType})
        }
        onSigSubtitleSet:{
            var vodInfo;
            vodInfo=pif.lastSelectedVideo.getAllMediaPlayInfo();
            core.info("SharedModelServerInterface.qml | apiName: videoSubtitleAck | subtitleLid : " + subtitlelid)
            sendApiData("videoSubtitleAck",{subtitleLid:subtitlelid,subtitleType:vodInfo.subtitleType})
        }
        onSigMediaElapsed:{
            core.info("SharedModelServerInterface.qml | apiName: setMediaPositionAck | elapseTimeFormat : " + timeformat + ", vodElapseTime : " + elapseTime);
            sendApiData("setMediaPositionAck",{elapsedTime:elapseTime});
        }
        onSigMediaPaused:{
            core.info("SharedModelServerInterface.qml | apiName: mediaPauseAck");
            __prevVodState=pif.vod.cMEDIA_PAUSE;
            sendApiData("mediaPauseAck","");
            sendPropertyData("mediaPlayerState","pause");
        }
        onSigAggPlayStopped:{
            core.info("SharedModelServerInterface.qml | apiName: videoStopAck");
            __prevVodState=pif.vod.cMEDIA_STOP;
            sendApiData("videoStopAck","");
            sendPropertyData("mediaPlayerState","stop");
            var elapseTime=pif.getMidInfo(cmid,'elapseTime');
            core.debug("SharedModelServerInterface.qml | apiName: videoStopAck - resumeTimeMap | cmid: "+cmid+" | elapseTime: "+elapseTime);
            var vodElapsedTimeMap=pif.__sharedModelServer.resumeTimeMap;
            vodElapsedTimeMap[cmid]=elapseTime;
            sendPropertyData("resumeTimeMap",vodElapsedTimeMap);
        }
        onSigMediaForward:{
            core.info("SharedModelServerInterface.qml | apiName: mediaFastForwardAck");
            __prevVodState=pif.vod.cMEDIA_FORWARD;
            sendApiData("mediaFastForwardAck",{mediaPlayRate:speed*100});
            sendPropertyData("mediaPlayerState","fastforward");
        }
        onSigMediaRewind:{
            core.info("SharedModelServerInterface.qml | apiName: mediaRewindAck");
            __prevVodState=pif.vod.cMEDIA_REWIND;
            sendApiData("mediaRewindAck",{mediaPlayRate:speed*100});
            sendPropertyData("mediaPlayerState","rewind");
        }
        onSigMidPlayCompleted:{
            core.info("SharedModelServerInterface.qml | apiName: videoStopAck");
            __prevVodState=pif.vod.cMEDIA_STOP;
            sendApiData("videoStopAck","");
            sendPropertyData("mediaPlayerState","stop");
        }
    }
    Connections {
        // target is refrence 1
        target: pif.extv;

        onSigMidPlayBegan: {
            var extvInfo;
            core.info("SharedModelServerInterface.qml | extv sigMidPlayBegan signal received")
            extvInfo=pif.extv.getAllMediaPlayInfo();

                sendPropertyData("mediaType",extvInfo.mediaType);

                sendApiData("playExtvAck",{mid:extvInfo.cmid,soundtrackLid:extvInfo.sndtrkLid,soundtrackType:extvInfo.sndtrkType,subtitleLid:extvInfo.subtitleLid,subtitleType:extvInfo.subtitleType});

                sendPropertyData("mediaPlayerState","play");


        }
        onSigAspectRatioChanged: assignAspectRatio();
        onSigStretchState: assignAspectRatio();
        onSigSoundtrackSet:{
            var extvInfo;
            extvInfo=pif.extv.getAllMediaPlayInfo();
            core.info("SharedModelServerInterface.qml | apiName: videoSoundtrackAck | soundtrackLid : " + soundtracklid)
            sendApiData("videoSoundtrackAck",{soundtrackLid:soundtracklid,soundtrackType:extvInfo.sndtrkType})
        }

        onSigAggPlayStopped:{
            core.info("SharedModelServerInterface.qml | apiName: videoStopAck");
            sendApiData("videoStopAck","");
            sendPropertyData("mediaPlayerState","stop");
            var elapseTime=pif.getMidInfo(cmid,'elapseTime');
            core.debug("SharedModelServerInterface.qml | apiName: videoStopAck - resumeTimeMap | cmid: "+cmid+" | elapseTime: "+elapseTime);
            var vodElapsedTimeMap=pif.__sharedModelServer.resumeTimeMap;
            vodElapsedTimeMap[cmid]=elapseTime;
            sendPropertyData("resumeTimeMap",vodElapsedTimeMap);
        }

        onSigMidPlayCompleted:{
            core.info("SharedModelServerInterface.qml | apiName: videoStopAck");
            __prevVodState=pif.vod.cMEDIA_STOP;
            sendApiData("extvStopAck","");
            sendPropertyData("mediaPlayerState","stop");
        }
    }
    Connections{
        // target is refrence 1
        target:pif.lastSelectedAudio;

        onSigMidPlayBegan:{
            if(__prevAodState==pif.aod.cMEDIA_STOP || __prevAodState==pif.aod.cMEDIA_PLAY){
                var repeat=pif.lastSelectedAudio.getMediaRepeat();
                var repeatStr;
                if(repeat==0)repeatStr="none";
                else if(repeat==1)repeatStr="all"
                else repeatStr="current";
                var aodInfo=pif.lastSelectedAudio.getAllMediaPlayInfo();
                core.info("SharedModelServerInterface.qml | apiName: playAodAck");
                sendPropertyData("mediaType",aodInfo.mediaType);
                sendApiData("playAodAck",{mid:cmid,aggregateMid:amid,mediaRepeat:repeatStr});
                sendPropertyData("mediaPlayerState","play");
            }
            else if(__prevAodState==pif.aod.cMEDIA_REWIND||__prevAodState==pif.aod.cMEDIA_FORWARD||__prevAodState==pif.aod.cMEDIA_PAUSE){
                core.info("SharedModelServerInterface.qml | apiName: mediaResumeAck");
                sendApiData("mediaResumeAck","");
                sendPropertyData("mediaPlayerState","play");
            }
            __prevAodState=pif.aod.cMEDIA_PLAY;
        }
        onSigMediaElapsed:{
            core.info("SharedModelServerInterface.qml | apiName: setMediaPositionAck | elapsedTimeFormat : " + timeformat + ", audioElapseTime : " + elapseTime);
            sendApiData("setMediaPositionAck",{elapsedTime:elapseTime});
        }
        onSigAggPlayStopped:{
            __prevAodState=pif.aod.cMEDIA_STOP;
            core.info("SharedModelServerInterface.qml | apiName: audioStopAck");
            sendApiData("audioStopAck","");
            sendPropertyData("mediaPlayerState","stop");
        }
        onSigMediaPaused:{
            __prevAodState=pif.aod.cMEDIA_PAUSE;
            core.info("SharedModelServerInterface.qml | apiName: mediaPauseAck");
            sendApiData("mediaPauseAck","");
            sendPropertyData("mediaPlayerState","pause");
        }
        onSigMediaShuffleState:{
            core.info("SharedModelServerInterface.qml | Shuffle property changed :: audioShuffle : " + shuffle);
            sendPropertyData("mediaShuffle",shuffle);
        }
        onSigMediaRepeatState:{
            core.info("SharedModelServerInterface.qml | Media Repeat property changed");
            var repeat=pif.lastSelectedAudio.getMediaRepeat();
            var repeatStr;
            if(repeat==0)repeatStr="none";
            else if(repeat==1)repeatStr="all";
            else repeatStr="current";
            sendPropertyData("mediaRepeat",repeatStr);
        }
    }

    Connections{
        target:playlistUpdateRequiredOnKarma?pif.aodPlaylist:null;

        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigTracksAdded, pid : "+pid+ " , Mids : "+mids+", playlistType :"+playlistType)
            var midList = mids
            core.info("smsi.qml | onSignalTracksAdded midList :"+midList+"Error code :"+errorCode)
            if (playlistType=="AodPlaylist"){
                var aggregateMid = parseInt(pif.aodPlaylist.getAggregateMid(midList[0],pid),10)
                var cid = parseInt(pif.aodPlaylist.getCategoryId(midList[0],pid),10)
            }
            var plist = pif.__sharedModelServer.playlists;
            for (var i=0;i<midList.length;i++){
                if (playlistType=="AodPlaylist"){
                    var aodPlaylistName = Store.aodPlaylistArr[pid]
                    plist.aod[aodPlaylistName].push({"mid": midList[i], "albumMid": aggregateMid, "cid": cid});
                }
            }
            if(errorCode==1){
                core.info("smsi.qml | onSigTracksAdded | Tracks Partially Added")
                sendFrameworkApi("frameworkPlaylistPartiallyAdded");
            }
            if(errorCode==0){
                core.info("smsi.qml | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded");
            }
            pif.__sharedModelServer.playlists = plist;
        }

        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigTracksRemoved, pid : "+pid+ ", Mid :"+mid+" ,playlistType :"+playlistType+"Index :"+indx)
            var plist = pif.__sharedModelServer.playlists;
            if (playlistType=="AodPlaylist"){
                for (var aodCounter in plist.aod){
                    for (var i in plist.aod[aodCounter]){
                        if (mid==plist.aod[aodCounter][i].mid) plist.aod[aodCounter].splice(i,1)
                    }
                }
            }
            pif.__sharedModelServer.playlists = plist
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | aodPlaylist |onSigTracksRemoved, pid : "+pid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            if (playlistType=="AodPlaylist"){
                var aodPlaylistName = Store.aodPlaylistArr[pid]
                plist.aod[aodPlaylistName] = new Array();
            }
            pif.__sharedModelServer.playlists = plist
        }
    }

    Connections{
        target:playlistUpdateRequiredOnKarma?pif.vodPlaylist:null;
        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | vodPlaylist | onSigTracksAdded, pid : "+pid+ " , Mids : "+mids+", playlistType :"+playlistType)
            var midList = mids;
            if (playlistType=="VodPlaylist"){
                var aggregateMid = parseInt(pif.vodPlaylist.getAggregateMid(midList[0],pid),10)
                var cid = parseInt(pif.vodPlaylist.getCategoryId(midList[0],pid),10)
            }
            var plist = pif.__sharedModelServer.playlists;
            for (var i=0;i<midList.length;i++){
                if (playlistType=="VodPlaylist"){
                    var vodPlaylistName = Store.vodPlaylistArr[pid]
                    plist.vod[vodPlaylistName].push({"mid": midList[i], "albumMid": aggregateMid, "cid": cid});
                }
            }
            if(errorCode==1){
                core.info("smsi.qml | onSigTracksAdded | Tracks Partially Added")
                sendFrameworkApi("frameworkPlaylistPartiallyAdded");
            }
            if(errorCode==0){
                core.info("smsi.qml | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded");
            }
            pif.__sharedModelServer.playlists = plist;
        }

        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | vodPlaylist |onSigTracksRemoved, pid : "+pid+ ", Mid :"+mid+" ,playlistType :"+playlistType+"Index :"+indx)
            var plist = pif.__sharedModelServer.playlists;
            if (playlistType=="VodPlaylist"){
                for (var vodCounter in plist.vod){
                    for (var j in plist.vod[vodCounter]){
                        if (mid==plist.vod[vodCounter][j].mid) plist.vod[vodCounter].splice(j,1)
                    }
                }
            }
            pif.__sharedModelServer.playlists = plist
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | vodPlaylist | onSigTracksRemoved, pid : "+pid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            if (playlistType=="VodPlaylist"){
                var vodPlaylistName = Store.vodPlaylistArr[pid]
                plist.vod[vodPlaylistName] = new Array();
            }
            pif.__sharedModelServer.playlists = plist
        }
    }

    Connections{
        target:playlistUpdateRequiredOnKarma?pif.kidsAodPlaylist:null;

        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | kidsAodPlaylist | onSigTracksAdded, pid : "+pid+ " , Mids : "+mids+", playlistType :"+playlistType)
            var midList = mids
            core.info("smsi.qml | onSignalTracksAdded midList :"+midList+"Error code :"+errorCode)
            if (playlistType=="KidsAodPlaylist"){
                var aggregateMid = parseInt(pif.kidsAodPlaylist.getAggregateMid(midList[0],pid),10)
                var cid = parseInt(pif.kidsAodPlaylist.getCategoryId(midList[0],pid),10)
            }
            var plist = pif.__sharedModelServer.playlists;
            for (var i=0;i<midList.length;i++){
                if (playlistType=="KidsAodPlaylist"){
                    var kidsAodPlaylistName = Store.kidsAodPlaylistArr[pid]
                    plist.kidsaod[kidsAodPlaylistName].push({"mid": midList[i], "albumMid": aggregateMid, "cid": cid});
                }
            }
            if(errorCode==1){
                core.info("smsi.qml | onSigTracksAdded | Tracks Partially Added")
                sendFrameworkApi("frameworkPlaylistPartiallyAdded");
            }
            if(errorCode==0){
                core.info("smsi.qml | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded");
            }
            pif.__sharedModelServer.playlists = plist;
        }

        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | kidsAodPlaylist | onSigTracksRemoved, pid : "+pid+ ", Mid :"+mid+" ,playlistType :"+playlistType+" ,Index :"+indx)
            var plist = pif.__sharedModelServer.playlists;
            if (playlistType=="KidsAodPlaylist"){
                for (var kidsAodCounter in plist.kidsaod){
                    for (var i in plist.kidsaod[kidsAodCounter]){
                        if (mid==plist.kidsaod[kidsAodCounter][i].mid) plist.kidsaod[kidsAodCounter].splice(i,1)
                    }
                }
            }
            pif.__sharedModelServer.playlists = plist
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | aodPlaylist | onSigTracksRemoved, pid : "+pid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            if (playlistType=="KidsAodPlaylist"){
                var KidsAodPlaylistName = Store.kidsAodPlaylistArr[pid]
                plist.kidsaod[KidsAodPlaylistName] = new Array();
            }
            pif.__sharedModelServer.playlists = plist
        }
    }

    Connections{
        target:playlistUpdateRequiredOnKarma?pif.kidsVodPlaylist:null;

        onSigTracksAdded:{
            core.info("SharedModelServerInterface.qml | kidsVodPlaylist | onSigTracksAdded, pid : "+pid+ " , Mids : "+mids+", playlistType :"+playlistType)
            var midList = mids
            core.info("smsi.qml | onSignalTracksAdded midList :"+midList+"Error code :"+errorCode)
            if (playlistType=="KidsVodPlaylist"){
                var aggregateMid = parseInt(pif.kidsVodPlaylist.getAggregateMid(midList[0],pid),10)
                var cid = parseInt(pif.kidsVodPlaylist.getCategoryId(midList[0],pid),10)
            }
            var plist = pif.__sharedModelServer.playlists;
            for (var i=0;i<midList.length;i++){
                if (playlistType=="KidsVodPlaylist"){
                    var kidsVodPlaylistName = Store.kidsVodPlaylistArr[pid]
                    plist.kidsvod[kidsVodPlaylistName].push({"mid": midList[i], "albumMid": aggregateMid, "cid": cid});
                }
            }
            if(errorCode==1){
                core.info("smsi.qml | onSigTracksAdded | Tracks Partially Added")
                sendFrameworkApi("frameworkPlaylistPartiallyAdded");
            }
            if(errorCode==0){
                core.info("smsi.qml | onSigTracksAdded | All Tracks Added")
                sendFrameworkApi("frameworkPlaylistAdded");
            }
            pif.__sharedModelServer.playlists = plist;
        }

        onSigTracksRemoved:{
            core.info("SharedModelServerInterface.qml | kidsVodPlaylist | onSigTracksRemoved, pid : "+pid+ ", Mid :"+mid+" ,playlistType :"+playlistType+", Index :"+indx)
            var plist = pif.__sharedModelServer.playlists;
            if (playlistType=="KidsVodPlaylist"){
                for (var kidsVodCounter in plist.kidsvod){
                    for (var i in plist.kidsvod[kidsVodCounter]){
                        if (mid==plist.kidsvod[kidsVodCounter][i].mid) plist.kidsvod[kidsVodCounter].splice(i,1)
                    }
                }
            }
            pif.__sharedModelServer.playlists = plist
        }

        onSigAllTracksRemoved:{
            core.info("SharedModelServerInterface.qml | kidsVodPlaylist | onSigTracksRemoved, pid : "+pid+ " ,playlistType :"+playlistType)
            var plist = pif.__sharedModelServer.playlists;
            if (playlistType=="KidsVodPlaylist"){
                var kidsVodPlaylistName = Store.kidsVodPlaylistArr[pid]
                plist.kidsvod[kidsVodPlaylistName] = new Array();
            }
            pif.__sharedModelServer.playlists = plist
        }
    }


    Connections{
        // target is refrence 2(i.e either pif or pif.vip)
        target:__pifRef;

        onVolumeChanged:{
            core.info("SharedModelServerInterface.qml | volume change event : volume level: "+volumeLevel);
            sendPropertyData("seatbackVolume",volumeLevel);
        }
        onVolumeSourceChanged: sendPropertyData("seatbackVolume",volumeLevel);
        onBrightnessChanged:{
            core.info("SharedModelServerInterface.qml | brightness change event : brightness level: "+brightnessLevel);
            sendPropertyData("seatbackBrightness",brightnessLevel);
        }
        onBacklightStateChanged:{
            core.info("SharedModelServerInterface.qml | backlight state change event : backlight State: "+backlightState);
            sendPropertyData("backlightEnabled",backlightState);
        }
    }

    Connections{
        target:(pif.vip)?pif.vip:null;

        onSigMidPlayBegan:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMidPlayBegan");
            sendApiData("playBlueRayAck","");
        }
        onSigMediaPaused:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMediaPaused");
            sendApiData("pauseBlueRayAck","");
        }
        onSigMediaForward:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMediaForward");
            sendApiData("fastForwardBlueRayAck","");
        }
        onSigMediaRewind:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMediaRewind");
            sendApiData("rewindBlueRayAck","");
        }
        onSigMidPlayStopped:{
            core.info("SharedModelServerInterface.qml | Blue Ray onSigMidPlayStopped");
            sendApiData("stopBlueRayAck","");
        }
    }

    Connections{
        // target is reference 1
        target:pif

        onVolumeChanged:{
            if(pif.vip){
                core.info("SharedModelServerInterface.qml | volume change event : volume level: "+volumeLevel);
                sendPropertyData("headsetVolume",volumeLevel);
            }
        }
        onIpodConnectedChanged:{
            core.info("SharedModelServerInterface.qml | ipod state change event : ipod state: "+ipodConnected);
            sendPropertyData("iPodConnected",ipodConnected);
        }
        onInteractiveResetChanged:{
            core.info("SharedModelServerInterface.qml | apiName: interactiveReset");
            sendApiData("interactiveReset","");
        }
    }

    Connections{
        target:pif.launchApp;

        onSigAppLaunched:{
            core.info("SharedModelServerInterface.qml | onSigAppLaunched signal received " + launchid + " " + launchType);
            if(launchType==pif.launchApp.cGAME_USING_ICORE || launchType==pif.launchApp.cGAME_USING_PIF || launchType==pif.launchApp.cUSE_ANDROID_ACCESS_HANDLER) sendApiData("launchAppAck",{launchAppId:launchid})
        }
        onSigAppExit:{
            core.info("SharedModelServerInterface.qml | onAppExitGUI signal received " + launchid + " " + launchType);
            if(launchType==pif.launchApp.cGAME_USING_ICORE || launchType==pif.launchApp.cGAME_USING_PIF || launchType==pif.launchApp.cUSE_ANDROID_ACCESS_HANDLER) sendApiData("closeAppAck",{launchAppId:""})
        }
    }

    Connections{
        target:pif.usb;

        onSigUsbConnected:{
            core.info("SharedModelServerInterface.qml | onSigUsbConnected");
            sendPropertyData("usbConnected",1);
        }
        onSigUsbDisconnected:{
            core.info("SharedModelServerInterface.qml | onSigUsbDisconnected | UsbCount: "+pif.usb.getUsbCount());
            if(!pif.usb.getUsbCount()) sendPropertyData("usbConnected",0);
        }
    }

    Connections{
        target: __audioPlayerdataRef;
        onSigUpdateSMCAudioPlayerdata: {
            sendFrameworkApi("frameworkUpdateAudioPlayerdata",params);
        }
    }

    Connections{
        target: __videoPlayerdataRef;
        onSigUpdateSMCVideoPlayerdata: {
            sendFrameworkApi("frameworkUpdateVideoPlayerdata",params);
        }
    }

    Connections {
        target: pif.seatInteractive;
        onSigDataReceived:{
            if(api=="frameworkGetPlayingPlaylistIndex"){
                var mediaType = params.mediaType;
                var numberOfAodPlaylist = pif.getNumberOfAodPlaylist();
                var numberOfVodPlaylist = pif.getNumberOfVodPlaylist();
                if (mediaType=="audio"){
                    var playingPlaylistIndex = pif.aod.getPlayingAlbumId()
                    if(Math.abs(playingPlaylistIndex)<=numberOfAodPlaylist){
                        sendFrameworkApi("frameworkplayingPlaylistIndex",{playlistIndex:playingPlaylistIndex,mediaType:"aod"})
                    }
                    else{
                        var kidsPlaylistIndex = pif.getNumberOfAodPlaylist()+ playingPlaylistIndex
                        sendFrameworkApi("frameworkplayingPlaylistIndex",{playlistIndex:kidsPlaylistIndex,mediaType:"kidsaod"})
                    }
                } else if(mediaType=="video"){
                    playingPlaylistIndex = pif.vod.getPlayingAlbumId()
                    if(Math.abs(playingPlaylistIndex)<=numberOfVodPlaylist){
                        sendFrameworkApi("frameworkplayingPlaylistIndex",{playlistIndex:playingPlaylistIndex,mediaType:"vod"})
                    }
                    else{
                        var vodplaylistIndex = pif.getNumberOfVodPlaylist()+ playingPlaylistIndex
                        sendFrameworkApi("frameworkplayingPlaylistIndex",{playlistIndex:playlistIndex,mediaType:"kidsvod"})
                    }
                }
            }
        }
    }

    function setViewData(){sendPropertyData("noOfDimmableWindows",pif.getNoOfDimmableWindows());}

    Component.onCompleted :{
        core.info("SharedModelServerInterface.qml | SharedModelServerInterface Load COmpleted ");
        Store.aodPlaylistArr = [];
        Store.vodPlaylistArr = [];
        Store.kidsAodPlaylistArr = [];
        Store.kidsVodPlaylistArr = [];
        Store.midList =[]
        playlistUpdateRequiredOnKarma=pif.getPlaylistUpdateRequiredOnKarma();
        interactiveStartup.registerToStage(interactiveStartup.cStageSetViewData,[setViewData]);
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOffScreen,[resetVariables]);
        interactiveStartup.registerToStage(interactiveStartup.cStageCloseFlightScreen,[resetVariables]);
    }
}
