import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as SystemData

Item{
    id:playlist;

    // CONSTANTS
    property int cTRACK_ADDED:0;
    property int cPALYLIST_FULL:1;
    property int cTRACK_ALREADY_PRESENT:2;
    property int cPLAYLIST_NOT_CREATED:3;
    property int cTRACK_MODEL: 0;
    property int cINDEX: 1;
    property int cAGGREGATEMID: 2;

    property int numberOfPlaylist:0;
    property string __playlistType:"";

    property int __blockedAmid:0;
    property int __prevState;
    property int __blockRequestType:0;   // 0 => No removal of amid from playlist, 1 => removal of single amid, 2 => removal of list of amids
    property variant __blockedAmidArr:[];

    signal sigCreateMetadataPlaylist(int num, string playlistType);
    signal sigTracksAdded(variant mids, int pid, string playlistType,int errorCode,int aggMid, bool albumAdded);
    signal sigTracksRemoved(int indx, int mid, int pid, string playlistType,int aggMid, bool removingAggMid, bool albumAdded);
    signal sigAllTracksRemoved(int pid, string playlistType, variant aggMidArr);
    signal sigTracksMoved(variant from,variant to, int pid,string playlistType);
    signal sigAggregateMidRemoved(int amid, string playlistType, int pid);
    signal sigSyncAlbumMidlist(variant aggMidArr)

    Connections{
        target:dataController.mediaServices;
        onSigMidBlockChanged: if(numberOfPlaylist) __removeBlockedMid(mid,status);
        onSigMidPaidStatusChanged: {if(!pif.getRemovePaidMidsFromPlaylist()) return; if(numberOfPlaylist) __removeBlockedMidList(midList,status,cause);}
    }

    Connections{target:core.pif;onSeatRatingChanged: if(numberOfPlaylist) __removeAlbumsBySeatRating(seatRating);}

    Connections{
        target:pif.aod;
        onSigMidPlayStopped:{
            core.info("Playlist.qml | onSigMidPlayStopped |"+__playlistType+" | onSigMidPlayStopped: __blockRequestType "+__blockRequestType);
            if(!__blockRequestType) return;
            if(__blockRequestType==1){

                removeAggregateMid(__blockedAmid);
            }else if(__blockRequestType==2){
                for(var i=0; i < __blockedAmidArr.length; i++){removeAggregateMid(__blockedAmidArr[i])}
            }
            __blockRequestType=0;
            if(aod.getMediaModelCount()==0)return;
            if(__prevState!=aod.cMEDIA_STOP)aod.setPlayingIndex(0);
        }
    }

    Connections{
        target:pif.vod;
        onSigMidPlayStopped:{
            core.info(__playlistType+" | onSigMidPlayStopped: __blockRequestType "+__blockRequestType);
            if(!__blockRequestType) return;
            if(__blockRequestType==1){
                removeTrack(__blockedAmid);
            }else if(__blockRequestType==2){
                for(var i=0; i < __blockedAmidArr.length; i++){removeTrack(__blockedAmidArr[i]);}
            }
            __blockRequestType=0;
        }
    }

    Connections{
        target:(pif.seatInteractive && pif.getPlaylistUpdateRequiredOnKarma())?pif.seatInteractive:null;
        onClientConnected:{
            core.info(__playlistType+"  | onClientConnected ");
            __requestAlbumMidsSync();
        }
    }

    onSigTracksAdded:{
        if(!SystemData.PlaylistArr[pid]["AggMidList"][aggMid]){
            SystemData.PlaylistArr[pid]["AggMidList"][aggMid] = new Array();
        }
        for(var i = 0;i < mids.length;i++){
            var len = SystemData.PlaylistArr[pid]["AggMidList"][aggMid].length;
            SystemData.PlaylistArr[pid]["AggMidList"][aggMid][len] = mids[i];
        }
    }

    onSigAllTracksRemoved:{
        SystemData.PlaylistArr[pid]["AggMidList"] = new Array();
    }

    onSigTracksRemoved:{
        var len = SystemData.PlaylistArr[pid]["AggMidList"][aggMid].length;
        for(var i = len-1;i>=0;i--){
            var tmid = SystemData.PlaylistArr[pid]["AggMidList"][aggMid][i];
            if(tmid == mid){
                SystemData.PlaylistArr[pid]["AggMidList"][aggMid].splice(i,1);
            }
        }
    }

    // Private functions
    function __removeBlockedMid(amid,status){
        core.info("Playlist.qml | __removeBlockedMid |"+__playlistType+" | Remove Blocked Mid amid: " + amid+" | status: "+status);if(status!="blocked") return;
        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){
            __prevState=pif.aod.getMediaState();
            var playingPlaylistId = pif.aod.getPlayingAlbumId(); var isPlaylistPlaying=false;
            for(var counter=1;counter<=numberOfPlaylist;counter++) {
                if (playingPlaylistId==SystemData.PlaylistArr[counter]["playlistAlbumId"]) {
                    var ptr = SystemData.PlaylistArr[counter][cAGGREGATEMID]
                    // amid is present in playing playlist
                    if (ptr.indexOf(amid)!==-1) isPlaylistPlaying=true;
                    break;
                }
            }
            if(__prevState!=pif.aod.cMEDIA_STOP && isPlaylistPlaying){ // Playlist is in mediaplayer and is playing
                __blockedAmid=amid; __blockRequestType=1; pif.aod.stop();
            } else removeAggregateMid(amid);
        }else if(__playlistType=="VodPlaylist" || __playlistType=="KidsVodPlaylist"){
            __prevState=pif.vod.getMediaState();
            if(__prevState!=pif.vod.cMEDIA_STOP){
                var tempMid = pif.vod.getPlayingMid();
                if(isTrackInPlaylist(tempMid)){__blockRequestType=1;__blockedAmid = tempMid;pif.vod.stop();}
                else{removeAggregateMid(amid);}
            }
            else{removeAggregateMid(amid);}
        }
    }

    function __removeBlockedMidList(amidArr,status,cause){
        core.info("Playlist.qml | __removeBlockedMidList |"+__playlistType+" | Remove Blocked Mid List | status: "+status+" | cause: "+cause);
        if(status=="pay" && cause=="PPV_REFUND") checkPlayingBlockedRequest2(amidArr);
    }

    function __removeAlbumsBySeatRating(seatRating){
        var rating; var amidList = []; var uniqueAmidList = [];
        for (var counter=1;counter<=numberOfPlaylist;counter++){
            if (!SystemData.PlaylistArr[counter]) continue;
            for (var i=0;i<SystemData.PlaylistArr[counter][cTRACK_MODEL].count;i++){
                rating = parseInt(SystemData.PlaylistArr[counter][cTRACK_MODEL].getValue(i,"rating"),10);
                if ((rating > seatRating) && rating<=254)amidList.push(parseInt(SystemData.PlaylistArr[counter][cTRACK_MODEL].getValue(i,"aggregate_parentmid"),10));
            }
        }
        for(var j=0;j<amidList.length;j++) if(uniqueAmidList.indexOf(amidList[j])==-1) uniqueAmidList.push(amidList[j]);
        checkPlayingBlockedRequest2(uniqueAmidList);
    }

    function __requestAlbumMidsSync(){
        core.info( __playlistType+" | __requestAlbumMidsSync | ");
        var aggMidArr = [];
        var numOfPlaylists;var playlistId;
        if(__playlistType=="AodPlaylist"){
            numOfPlaylists = pif.getNumberOfAodPlaylist();
        }else if(__playlistType=="VodPlaylist"){
            numOfPlaylists = pif.getNumberOfVodPlaylist();
        }else if(__playlistType=="KidsAodPlaylist"){
            numOfPlaylists = pif.getNumberOfKidsAodPlaylist();
        }else if(__playlistType=="KidsVodPlaylist"){
            numOfPlaylists = pif.getNumberOfKidsVodPlaylist();
        }
        for(var i=0;i<numOfPlaylists;i++){
            playlistId=getPlaylistAlbumId(i);
            var tempArr = [];
            for (var x in SystemData.PlaylistArr[Math.abs(playlistId)][cAGGREGATEMID]){
                tempArr.push(SystemData.PlaylistArr[Math.abs(playlistId)][cAGGREGATEMID][x]);
            }
            for (var j=0;j<tempArr.length;j++){
                if(aggMidArr.indexOf(tempArr[j])<0)aggMidArr.push(tempArr[j]);
            }
        }
        sigSyncAlbumMidlist(aggMidArr)
        return true;
    }

    function getMidsInPlaylistByAggMid(pid,aggMid){
        pid=Math.abs(pid);
        return SystemData.PlaylistArr[pid]["AggMidList"][aggMid];
    }

    function checkPlayingBlockedRequest2 (amidArr) {
        var isPlaylistPlaying=false;
        core.info("Playlist.qml | checkPlayingBlockedRequest2 | __playlistType "+__playlistType+" ,amidArr :"+amidArr)
        if (__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){
            __prevState=pif.aod.getMediaState();
            var playingPlaylistId = pif.aod.getPlayingAlbumId();
            for(var counter=1;counter<=numberOfPlaylist;counter++) {
                if (playingPlaylistId==SystemData.PlaylistArr[counter]["playlistAlbumId"]) {
                    var ptr = SystemData.PlaylistArr[counter][cAGGREGATEMID]
                    for(var j in amidArr) { // amid is present in playing playlist
                        if (ptr.indexOf(amidArr[j])!==-1) {isPlaylistPlaying=true; break;}
                    }
                    break;
                }
            }
            if(isPlaylistPlaying && __prevState!=pif.aod.cMEDIA_STOP){// Playlist is in mediaplayer and is playing
                core.info("Playlist.qml | checkPlayingBlockedRequest2 | Stopping mid")
                __blockRequestType=2; __blockedAmidArr=amidArr; pif.aod.stop();
            }
            else {
                for( var k in amidArr){
                    removeAggregateMid(amidArr[k])
                }
            }
            return true;
        }
        else if(__playlistType=="VodPlaylist" || __playlistType=="KidsVodPlaylist"){
            __prevState=pif.vod.getMediaState();
            if(__prevState!=pif.vod.cMEDIA_STOP){
                var tempMid = pif.vod.getPlayingMid();
                if(isTrackInPlaylist(tempMid)){
                    __blockRequestType=2; __blockedAmidArr=amidArr;pif.vod.stop();
                }
                else{for(var k in amidArr){removeAggregateMid(amidArr[k]);}}
            }
            else{for(var k in amidArr){removeAggregateMid(amidArr[k]);}}
        }
        return false;
    }

    // Public functions
    function addTrack(model,index,cid,aggregateMid,playlistId,rating,skipMediaModelUpdate){
        core.info("Playlist.qml | addTrack | "+__playlistType+" | ADD TRACKS");var tempArray=[];
        if(playlistId==undefined||playlistId=="")playlistId=-1;
        if(skipMediaModelUpdate==undefined)skipMediaModelUpdate=false;
        playlistId=Math.abs(playlistId);if(cid==undefined||cid=="")cid=0;
        if(rating==undefined||rating==""||rating<0)core.error("Playlist.qml | Missing rating. Setting it to 254.");
        if(rating==undefined||rating==""||rating<0)rating=254;

        var cModel = SystemData.PlaylistArr[playlistId][cTRACK_MODEL];
        if(cModel==undefined){core.error("Playlist.qml | addTrack | "+__playlistType+" | Playlist not created");return cPLAYLIST_NOT_CREATED;}

        if(__playlistType=="AodPlaylist" && pif.getMaxTracksAodPlaylist()>0 && cModel.count == pif.getMaxTracksAodPlaylist()){core.info("Playlist.qml | addTrack | "+__playlistType+" | Playlist full");sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));return cPALYLIST_FULL;}
        else if(__playlistType=="VodPlaylist" && pif.getMaxTracksVodPlaylist()>0 && cModel.count == pif.getMaxTracksVodPlaylist()){core.info("Playlist.qml | "+__playlistType+" | Playlist full");sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));return cPALYLIST_FULL;}
        else if(__playlistType=="KidsAodPlaylist" && pif.getMaxTracksKidsAodPlaylist()>0 && cModel.count == pif.getMaxTracksKidsAodPlaylist()){core.info("Playlist.qml | addTrack | "+__playlistType+" | Playlist full");sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));return cPALYLIST_FULL;}
        else if(__playlistType=="KidsVodPlaylist" && pif.getMaxTracksKidsVodPlaylist()>0 && cModel.count == pif.getMaxTracksKidsVodPlaylist()){core.info("Playlist.qml | addTrack | "+__playlistType+" | Playlist full");sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));return cPALYLIST_FULL;}
        else if(pif.getMaxTracksInAllPlaylist()>0 && pif.getMaxTracksInAllPlaylist()==pif.getTotalTracksInAllPlaylists()){core.info("Playlist.qml | addTrack | "+__playlistType+" | All Playlist full");sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));return cPALYLIST_FULL;}
        var tempMid = parseInt(model.getValue(index,"mid"),10);
        var i=isTrackInPlaylist(tempMid);
        if(i)return cTRACK_ALREADY_PRESENT;

        cModel.append({"aggregate_parentmid":parseInt(aggregateMid,10),"mid":tempMid,"media_type":model.getValue(index,"media_type"),"duration":model.getValue(index,"duration"),"cid":cid,"rating":rating});
        SystemData.PlaylistArr[playlistId][cINDEX][SystemData.PlaylistArr[playlistId][cINDEX].length]=tempMid;
        SystemData.PlaylistArr[playlistId][cAGGREGATEMID][tempMid]=aggregateMid;
        if(SystemData.AggregateMidCountArr[aggregateMid]){
        SystemData.AggregateMidCountArr[aggregateMid]=(SystemData.AggregateMidCountArr[aggregateMid]<model.count?model.count:SystemData.AggregateMidCountArr[aggregateMid]);
        }else {SystemData.AggregateMidCountArr[aggregateMid]=model.count}
        tempArray[tempArray.length] = tempMid;
        if((__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist") && SystemData.PlaylistArr[playlistId]["playlistAlbumId"]==pif.aod.getPlayingAlbumId() && skipMediaModelUpdate==false){pif.aod.__appendToMediaModel(cModel,(cModel.count-1),cid,aggregateMid);}
        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){
            if(!SystemData.PlaylistArr[playlistId]["paxusLogged"] && pif.paxus!=undefined){
                pif.paxus.playlistCreatedLog();
                SystemData.PlaylistArr[playlistId]["paxusLogged"]=1;
            }
        }
        sigTracksAdded(tempArray,playlistId,__playlistType,cTRACK_ADDED,aggregateMid,isAlbumInPlaylist(aggregateMid));
        return cTRACK_ADDED;
    }

    function addAllTracks(model,cid,aggregateMid,playlistId,rating,skipMediaModelUpdate){
        core.info("Playlist.qml | addAllTracks | "+__playlistType+" | Add all tracks");var tempArray=[];var playingPlaylistId;
        if(playlistId==undefined||playlistId=="")playlistId=-1;
        if(skipMediaModelUpdate==undefined)skipMediaModelUpdate=false;
        playlistId=Math.abs(playlistId);if(cid==undefined||cid=="")cid=0;
        if(rating==undefined||rating==""||rating<0)core.error("Playlist.qml | Missing rating. Setting it to 254.");
        if(rating==undefined||rating==""||rating<0)rating=254;

        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){playingPlaylistId = pif.aod.getPlayingAlbumId();}
        var cModel = SystemData.PlaylistArr[playlistId][cTRACK_MODEL];

        if(cModel==undefined){core.error("Playlist.qml | addAllTracks | "+__playlistType+" | Playlist not created");return cPLAYLIST_NOT_CREATED;}

        for(var counter=0;counter<model.count;counter++){
            if((__playlistType=="AodPlaylist") && pif.getMaxTracksAodPlaylist()>0 && (cModel.count == pif.getMaxTracksAodPlaylist())){
                core.info("Playlist.qml | addAllTracks | "+__playlistType+" | Playlist full");
                sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));
                return cPALYLIST_FULL;
            }
            else if((__playlistType=="VodPlaylist") && pif.getMaxTracksVodPlaylist()>0  && (cModel.count == pif.getMaxTracksVodPlaylist())){
                core.info("Playlist.qml | addAllTracks | "+__playlistType+" | Playlist full");
                sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));
                return cPALYLIST_FULL;
            }
            else if((__playlistType=="KidsAodPlaylist") && pif.getMaxTracksKidsAodPlaylist()>0 && (cModel.count == pif.getMaxTracksKidsAodPlaylist())){
                core.info("Playlist.qml | addAllTracks | "+__playlistType+" | Playlist full");
                sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));
                return cPALYLIST_FULL;
            }
            else if((__playlistType=="KidsVodPlaylist") && pif.getMaxTracksKidsVodPlaylist()>0 && (cModel.count == pif.getMaxTracksKidsVodPlaylist())){
                core.info("Playlist.qml | addAllTracks | "+__playlistType+" | Playlist full");
                sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));
                return cPALYLIST_FULL;
            }else if(pif.getMaxTracksInAllPlaylist()>0 && pif.getMaxTracksInAllPlaylist()==pif.getTotalTracksInAllPlaylists()){
                core.info("Playlist.qml | addTrack | "+__playlistType+" | All Playlist full");
                sigTracksAdded(tempArray,playlistId,__playlistType,cPALYLIST_FULL,aggregateMid,isAlbumInPlaylist(aggregateMid));
                return cPALYLIST_FULL;
            }

            var tempMid=parseInt(model.getValue(counter,"mid"),10);
            var i=isTrackInPlaylist(tempMid);if(i) continue;
            cModel.append({"aggregate_parentmid":parseInt(aggregateMid,10),"mid":parseInt(model.getValue(counter,"mid"),10),"media_type":model.getValue(counter,"media_type"),"duration":model.getValue(counter,"duration"),"cid":cid,"rating":rating});
            if((__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist") && playingPlaylistId==SystemData.PlaylistArr[playlistId]["playlistAlbumId"] && skipMediaModelUpdate==false){pif.aod.__appendToMediaModel(cModel,(cModel.count-1),cid,aggregateMid);}
            SystemData.PlaylistArr[playlistId][cINDEX][SystemData.PlaylistArr[playlistId][cINDEX].length]=tempMid;
            SystemData.PlaylistArr[playlistId][cAGGREGATEMID][tempMid]=aggregateMid;
            if(SystemData.AggregateMidCountArr[aggregateMid]){
            SystemData.AggregateMidCountArr[aggregateMid]=(SystemData.AggregateMidCountArr[aggregateMid]<model.count?model.count:SystemData.AggregateMidCountArr[aggregateMid]);
            }else {SystemData.AggregateMidCountArr[aggregateMid]=model.count}
            tempArray[tempArray.length] = tempMid;
        }
        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){
            if(!SystemData.PlaylistArr[playlistId]["paxusLogged"] && pif.paxus!=undefined){
                pif.paxus.playlistCreatedLog();
                SystemData.PlaylistArr[playlistId]["paxusLogged"]=1;
            }

        }
        sigTracksAdded(tempArray,playlistId,__playlistType,cTRACK_ADDED,aggregateMid,isAlbumInPlaylist(aggregateMid));
        return cTRACK_ADDED;
    }

    function removeTrack(mid){
        core.info("Playlist.qml | removeTrack() | mid :"+mid)
        var indxOfMid=-1;var indxOfMP=-1;var playingPlaylistId;core.info("Playlist.qml | removeTrack | "+__playlistType+" | Remove track");mid=parseInt(mid,10);
        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){playingPlaylistId=pif.aod.getPlayingAlbumId();}
        for(var counter=1;counter<=numberOfPlaylist;counter++){
            indxOfMid = SystemData.PlaylistArr[counter][cINDEX].indexOf(mid);
            if(indxOfMid<0)continue;
            indxOfMP=core.coreHelper.searchEntryInModel(SystemData.PlaylistArr[counter][cTRACK_MODEL],"mid",mid);
            if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){
                if(indxOfMP!=-1 && playingPlaylistId==SystemData.PlaylistArr[counter]["playlistAlbumId"]){
                    pif.aod.__removeFromMediaModel(mid,indxOfMP);
                    if(pif.aod.getMediaModelCount()==0) pif.aod.__clearSuspend();
                }
            }
            SystemData.PlaylistArr[counter][cTRACK_MODEL].remove(indxOfMP,1);
            SystemData.PlaylistArr[counter][cINDEX].splice(indxOfMid,1);
            var albumMid=SystemData.PlaylistArr[counter][cAGGREGATEMID][mid];
            delete SystemData.PlaylistArr[counter][cAGGREGATEMID][mid];;

            sigTracksRemoved(indxOfMP,mid,counter,__playlistType,albumMid,false,isAlbumInPlaylist(albumMid));
            return true;
        }
        return false;
    }

    function removeAllTracks(playlistId){
        core.info("Playlist.qml | removeAllTracks | "+__playlistType+" | Remove track");if(playlistId==undefined||playlistId=="")playlistId=-1;playlistId=Math.abs(playlistId);
        var tempArr = []; var aggMidArr = [];
        var playingPlaylistId=pif.aod.getPlayingAlbumId()
        if(SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){core.error("Playlist.qml | removeAllTracks | "+__playlistType+" | Playlist not created");return false;}
        if((__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist") && playingPlaylistId==SystemData.PlaylistArr[playlistId]["playlistAlbumId"]){ pif.aod.__clearMediaModel(); pif.aod.__clearSuspend();}
        for (var x in SystemData.PlaylistArr[playlistId][cAGGREGATEMID]){
            tempArr.push(SystemData.PlaylistArr[playlistId][cAGGREGATEMID][x])
        }
        for (var i=0;i<tempArr.length;i++){
            if(aggMidArr.indexOf(tempArr[i])<0)aggMidArr.push(tempArr[i]);
        }
        SystemData.PlaylistArr[playlistId][cTRACK_MODEL].clear();
        SystemData.PlaylistArr[playlistId][cINDEX]=new Array();
        SystemData.PlaylistArr[playlistId][cAGGREGATEMID]=new Array();
        sigAllTracksRemoved(playlistId,__playlistType,aggMidArr);
        return true;
    }

    function removeAggregateMid(amid){
        core.info("Playlist.qml | removeAggregateMid | amid"+amid+ "__playlistType :"+__playlistType)
        var __modelIndex;var __indxOfMid;var __mid;var playingPlaylistId;var pid
        if(SystemData.AggregateMidCountArr[amid]){
            for(var counter=1;counter<=numberOfPlaylist;counter++){
                do{
                    __modelIndex=core.coreHelper.searchEntryInModel(SystemData.PlaylistArr[counter][cTRACK_MODEL],"aggregate_parentmid",amid);
                    if(__modelIndex!=-1){
                        pid=counter;
                        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){playingPlaylistId=pif.aod.getPlayingAlbumId();}
                        else{playingPlaylistId=pif.vod.getPlayingAlbumId();}
                        __mid = parseInt(SystemData.PlaylistArr[counter][cTRACK_MODEL].getValue(__modelIndex,"mid"),10);
                        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){
                            if(playingPlaylistId==SystemData.PlaylistArr[counter]["playlistAlbumId"])pif.aod.__removeFromMediaModel(__mid,0);
                        }else{
                            if(playingPlaylistId==SystemData.PlaylistArr[counter]["playlistAlbumId"])pif.vod.__removeFromMediaModel(__mid,0);
                        }
                        SystemData.PlaylistArr[counter][cTRACK_MODEL].remove(__modelIndex,1);
                        __indxOfMid = SystemData.PlaylistArr[counter][cINDEX].indexOf(__mid);
                        SystemData.PlaylistArr[counter][cINDEX].splice(__indxOfMid,1);
                        sigTracksRemoved(__modelIndex,__mid,counter,__playlistType,amid,true,isAlbumInPlaylist(amid));

                    }
                }while(__modelIndex!=-1)
            }
            for(var i=1;i<=numberOfPlaylist;i++){
                for(var x in SystemData.PlaylistArr[i][cAGGREGATEMID]){if(SystemData.PlaylistArr[i][cAGGREGATEMID][x]==amid)delete SystemData.PlaylistArr[i][cAGGREGATEMID][x];}
            }
            delete SystemData.AggregateMidCountArr[amid];
            sigAggregateMidRemoved(amid,__playlistType,pid);
            return true;
        }
        return false;
    }

    function removeAggregateMids(model){
        var mid;
        for(var i=0; i<model.count; i++){
            mid = parseInt(model.getValue(i,"mid"),10);
            for(var counter=1;counter <= numberOfPlaylist;counter++){
                if(SystemData.PlaylistArr[counter][cINDEX].indexOf(mid) >= 0) removeTrack(mid);
            }
        }
    }

    /*
       removeAlbumMids() function to be depricated. please do not use.
    */
    function removeAlbumMids(model){
        removeAggregateMids(model);
    }

    function moveTrackUp(fromPosition,toPosition,playlistId,dModel){
            core.log("Playlist.qml | "+__playlistType+" | moveTrackUp | fromPosition: "+fromPosition+" | toPosition: "+toPosition);
            moveTrack(fromPosition,toPosition,playlistId,dModel);
    }
    function moveTrackDown(fromPosition,toPosition,playlistId,dModel){
             core.log("Playlist.qml | "+__playlistType+" | moveTrackDown | fromPosition: "+fromPosition+" | toPosition: "+toPosition);
            //array needs to be reversed for GUI to display correctly
            fromPosition.reverse(); toPosition.reverse();
            moveTrack(fromPosition,toPosition,playlistId,dModel);
    }

    function moveTrack(fromPosition,toPosition,playlistId,dModel){
        core.info("Playlist.qml | moveTrack | fromPosition: "+fromPosition+" | toPosition: "+toPosition);
        var numberOfItemsToMove=1;
        if(playlistId==undefined || playlistId=="") playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(!fromPosition || typeof(fromPosition)!="object"){core.info("Playlist.qml | "+__playlistType+" | moveTrack | fromPosition needs to be an array");return false}
        if(!toPosition || typeof(toPosition)!="object"){core.info("Playlist.qml | "+__playlistType+" | moveTrack | toPosition needs to be an array");return false}

        var cModel = SystemData.PlaylistArr[playlistId][cTRACK_MODEL];
        if(!(cModel && cModel.count)){core.info("Playlist.qml | "+__playlistType+" | moveTrack | Playlist not found")}
        if(fromPosition.length!=toPosition.length){core.info("Playlist.qml | "+__playlistType+" | moveTrack | fromPosition and toPosition length must be same");return false}
        for(var counter=0;counter<fromPosition.length;counter++){
            if(fromPosition[counter]<0 || fromPosition[counter]>cModel.count){core.info("Playlist.qml | "+__playlistType+" | moveTrack | Invalid fromPosition: "+fromPosition[counter]);return false}
            if(toPosition[counter]<0 || toPosition[counter]>=cModel.count){core.info("Playlist.qml | "+__playlistType+" | moveTrack | Invalid toPosition: "+toPosition[counter]);return false}
        }
        for(counter=0;counter<fromPosition.length;counter++){
            cModel.move(fromPosition[counter],toPosition[counter],numberOfItemsToMove);
            dModel.move(fromPosition[counter],toPosition[counter],numberOfItemsToMove);
        }

        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){
            if(playlistId==Math.abs(pif.aod.getPlayingAlbumId()))pif.aod.__moveTracks(fromPosition,toPosition);
        }else{
            if(playlistId==Math.abs(pif.vod.getPlayingAlbumId()))pif.vod.__moveTracks(fromPosition,toPosition);
        }

        sigTracksMoved(fromPosition,toPosition,playlistId,__playlistType);
        return true;
    }

    function getTotalTracks(playlistId){
        if(playlistId==undefined||playlistId=="")playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(!SystemData.PlaylistArr[playlistId] || SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){core.error("Playlist.qml | getTotalTrack | "+__playlistType+" | Playlist not created");return 0;}
        return SystemData.PlaylistArr[playlistId][cTRACK_MODEL].count;
    }

    function getAllPlaylistTotalTracks(){
        var allPlaylistTrackCount=0;
        for(var playlistId in SystemData.PlaylistArr){
                core.debug("Playlist | "+__playlistType+" |  getAllPlaylistTotalTracks |  pid: "+playlistId+" count: "+SystemData.PlaylistArr[playlistId][cTRACK_MODEL].count)
                allPlaylistTrackCount += SystemData.PlaylistArr[playlistId][cTRACK_MODEL].count;
         }
        return allPlaylistTrackCount;
    }

    function getPlaylistModel(playlistId){
        if(playlistId==undefined||playlistId=="")playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(!SystemData.PlaylistArr[playlistId] || SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){core.error("Playlist.qml | getPlaylistMode | "+__playlistType+" | Playlist not created");return 0;}
        return SystemData.PlaylistArr[playlistId][cTRACK_MODEL];
    }

    function getAllPlaylistMids(playlistId){
        if(playlistId==undefined||playlistId=="")playlistId=-1;
        playlistId=Math.abs(playlistId);
        var tempArray=[];
        if(!SystemData.PlaylistArr[playlistId] || SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){core.error("Playlist.qml | getAllPlaylistMids | "+__playlistType+" | Playlist not created");return tempArray;}
        for(var i=0; i<SystemData.PlaylistArr[playlistId][cTRACK_MODEL].count; i++){
            tempArray.push(SystemData.PlaylistArr[playlistId][cTRACK_MODEL].getValue(i,"mid"));
        }
        return tempArray;
    }

    function isTrackInPlaylist(mid){
        var indxOfPlaylist=-1;mid=parseInt(mid,10);
        for(var counter=1;counter<=numberOfPlaylist;counter++){
            indxOfPlaylist=-1;
            indxOfPlaylist = SystemData.PlaylistArr[counter][cINDEX].indexOf(mid);
            if(indxOfPlaylist<0)continue;
            else return true;
        }
        return false;
    }

    function isAllTracksInPlaylist(model,playlistId){
        if(playlistId==undefined)playlistId=-1;
        playlistId=Math.abs(playlistId);
        var mid; var numTracksInPlaylist = 0; var allTracksInPlaylist = 0;
        for(var i=0; i < model.count; i++){
            mid = parseInt(model.getValue(i,"mid"),10);
            if(allTracksInPlaylist==1) i = model.count;
            if(SystemData.PlaylistArr[playlistId][cINDEX].indexOf(mid) >= 0){allTracksInPlaylist = 0; numTracksInPlaylist +=1;}
            else allTracksInPlaylist = 1;
        }
        if (numTracksInPlaylist==model.count) return true;
        else return false;
    }

    function isAlbumInPlaylist(albumMid){
        var count=0;
        core.info("Playlist.qml | isAlbumInPlaylist | "+__playlistType+" | isAlbumInPlaylist called albumMid is  " + albumMid);
        for(var i=1;i<=numberOfPlaylist;i++){
            for(x in SystemData.PlaylistArr[i][cAGGREGATEMID]){
                if(SystemData.PlaylistArr[i][cAGGREGATEMID][x]==albumMid)count++;
            }
        }
        if(count==SystemData.AggregateMidCountArr[albumMid]) return true;
        else return false;
    }

    function getAggregateMidList(playlistId){
        var aggregateMidArr=[];
        var midArr=[];
        var sortedMidArr=[];
        if(playlistId==undefined)playlistId=-1;
        playlistId=Math.abs(playlistId);
        if (!SystemData.PlaylistArr[playlistId]) return {};
        for(x in SystemData.PlaylistArr[playlistId][cAGGREGATEMID]){
            if(aggregateMidArr.join(",").indexOf(SystemData.PlaylistArr[playlistId][cAGGREGATEMID][x])==-1){aggregateMidArr.push(SystemData.PlaylistArr[playlistId][cAGGREGATEMID][x]);midArr[SystemData.PlaylistArr[playlistId][cAGGREGATEMID][x]] = new Array();}
            midArr[SystemData.PlaylistArr[playlistId][cAGGREGATEMID][x]].push(x);
        }
        sortedMidArr=getAllPlaylistMids(playlistId);
        return {"aggregateMidArr":aggregateMidArr,"midArr":midArr,"sortedMidArr":sortedMidArr};
    }

    function getOrderedAggregateMidList(playlistId){
        var aggregateMidArr=[];var midArr=[];
        if(playlistId==undefined)playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(!SystemData.PlaylistArr[playlistId] || SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){core.error("Playlist.qml | getOrderedAggregateMidList | "+__playlistType+" | Playlist not created");return {"aggregateMidArr":aggregateMidArr,"midArr":midArr};}
        for(var i=0; i<SystemData.PlaylistArr[playlistId][cTRACK_MODEL].count; i++){
            if(aggregateMidArr.indexOf(SystemData.PlaylistArr[playlistId][cTRACK_MODEL].getValue(i,"aggregate_parentmid"))==-1){aggregateMidArr.push(SystemData.PlaylistArr[playlistId][cTRACK_MODEL].getValue(i,"aggregate_parentmid"));}
            midArr.push(SystemData.PlaylistArr[playlistId][cTRACK_MODEL].getValue(i,"mid"));
        }
        return {"aggregateMidArr":aggregateMidArr,"midArr":midArr};
    }

    function getAggregateMid(trackMid,playlistId){
        if(playlistId==undefined)playlistId=-1;
        playlistId=Math.abs(playlistId);
        return SystemData.PlaylistArr[playlistId][cAGGREGATEMID][trackMid]?SystemData.PlaylistArr[playlistId][cAGGREGATEMID][trackMid]:0
    }

    function __getAggregateMid(trackMid,playlistId){
        if(playlistId==undefined) playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(__playlistType.indexOf("Kids")!=-1) playlistId=Math.abs((__playlistType=="kidsVodPlaylist")?pif.getNumberOfVodPlaylist()-playlistId:pif.getNumberOfAodPlaylist()-playlistId)
        return SystemData.PlaylistArr[playlistId]?SystemData.PlaylistArr[playlistId][cAGGREGATEMID][trackMid]?SystemData.PlaylistArr[playlistId][cAGGREGATEMID][trackMid]:0:0
    }

    function getCategoryId(trackMid,playlistId){
        if(playlistId==undefined) playlistId=-1;
        playlistId=Math.abs(playlistId);
        var cModel=SystemData.PlaylistArr[playlistId][cTRACK_MODEL];
        var cid;
        for(var i=0; i<cModel.count; i++) if(cModel.getValue(i,"mid")==trackMid) cid=cModel.getValue(i,"cid");
        return cid;
    }

    /*
       getAlbumMid(trackMid,playlistId) function to be depricated. please do not use.
    */
    function getAlbumMid(trackMid,playlistId){ return getAggregateMid(trackMid,playlistId);}

    function getPlaylistAlbumId(playlistId){
        core.info("Playlist.qml | getPlaylistAlbumId | playlistIndex :"+playlistId);
        if(playlistId==undefined||playlistId=="")playlistId=-1;
        return SystemData.PlaylistArr[Math.abs(playlistId)]["playlistAlbumId"]
    }

    function frmwkResetApp(){
        core.info("Playlist.qml | frmwResetApp | "+__playlistType+" | frmwkResetApp called");
        if(__playlistType=="AodPlaylist" || __playlistType=="KidsAodPlaylist"){
            if(pif.aod.getMediaState()!=pif.aod.cMEDIA_STOP){
               var playingPlaylistId = pif.aod.getPlayingAlbumId();
               for(var counter=1;counter<=numberOfPlaylist;counter++){
                  if(playingPlaylistId==SystemData.PlaylistArr[counter]["playlistAlbumId"])
                      pif.aod.__clearMediaModel();
               }
            }
        }
        SystemData.PlaylistArr=[];
    }

    function updateAggegerateMidCount(amid,count){
        core.debug("Playlist.qml | "+__playlistType+" | updateAggegerateMidCount | amid: "+amid+" | count: "+count);
        SystemData.AggregateMidCountArr[amid]=count;
    }

    function frmwkInitApp(){
        var params;
        core.info("Playlist.qml | frmwkInitApp | "+__playlistType+" | frmwkInitApp called");
        if(__playlistType=="AodPlaylist"){numberOfPlaylist=pif.getNumberOfAodPlaylist();}
        else if(__playlistType=="VodPlaylist"){numberOfPlaylist=pif.getNumberOfVodPlaylist();}
        else if(__playlistType=="KidsAodPlaylist"){numberOfPlaylist=pif.getNumberOfKidsAodPlaylist();}
        else if(__playlistType=="KidsVodPlaylist"){(numberOfPlaylist)=pif.getNumberOfKidsVodPlaylist();}
        SystemData.PlaylistArr=[];
        SystemData.AggregateMidCountArr=[];
        for(var i=1;i<=numberOfPlaylist;i++){
            SystemData.PlaylistArr[i]=[];
            if(__playlistType=="AodPlaylist"){
                SystemData.PlaylistArr[i]["playlistAlbumId"]=-i;
            } else if(__playlistType=="KidsAodPlaylist"){
                SystemData.PlaylistArr[i]["playlistAlbumId"]=-(pif.getNumberOfAodPlaylist())-i;
            } else if(__playlistType=="VodPlaylist"){
                SystemData.PlaylistArr[i]["playlistAlbumId"]=-i;
            } else if(__playlistType=="KidsVodPlaylist"){
                SystemData.PlaylistArr[i]["playlistAlbumId"]=-(pif.getNumberOfVodPlaylist())-i;
            }
            SystemData.PlaylistArr[i][cTRACK_MODEL]=Qt.createQmlObject('import QtQuick 1.1; import Panasonic.Pif 1.0; SimpleModel {}',playlist);
            SystemData.PlaylistArr[i][cINDEX]=[];
            SystemData.PlaylistArr[i][cAGGREGATEMID]=[];
            SystemData.PlaylistArr[i]["AggMidList"]=[];
            SystemData.PlaylistArr[i]["paxusLogged"]=0;
            params = {playlistIndex:i,name:__playlistType + " " + i,mediaType:(__playlistType=="AodPlaylist")?"aod":(__playlistType=="VodPlaylist")?"vod":(__playlistType=="KidsAodPlaylist")?"kidsaod":(__playlistType=="KidsVodPlaylist")?"kidsvod":"aod"};
            if(pif.sharedModelServerInterface)pif.sharedModelServerInterface.__createPlaylist(params);
        }
        sigCreateMetadataPlaylist(numberOfPlaylist,__playlistType);
    }

    Component.onCompleted: {
        SystemData.PlaylistArr=[];
        SystemData.AggregateMidCountArr=[];
    }
}
