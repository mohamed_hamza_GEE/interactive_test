import QtQuick 1.1
import Panasonic.Pif 1.0

AudioMediaPlayer{
    property variant deviceMngr;
    property bool __suspend;
    property int __prevMediaState:0;
    property int __tracksCount:0;
    property string mediaSource:"aod";

    onSigAggPlayBegan:{if(pif.getLSAmediaSource()=="aod") pif.setVolumeSource(pif.cAOD_VOLUME_SRC);}
    onSigAggPlayStopped: __getAllAodPlayInfo()

    Connections{target:dataController.mediaServices; onSigMidBlockChanged:__stopBlockedMid(mid,status); onSigMidPaidStatusChanged:__stopBlockedMidInList(midList,status,cause);}
    Connections{target:core.pif;onSeatRatingChanged:__stopAlbumsBySeatRating(seatRating);onVbStartTimeChanged:__vbStartTimeChanged();}
    Connections{target:pif;
        onSigFallbackMode: if(fallbackMode) frmwkResetApp();
        onSigStreamerGroupStatusChanged: if(groupStatusChanged){__checkAndStopPlayingInactiveMid();}
    }

    function play (amid, params) {
        core.info("Aod.qml | play called | amid: "+amid+" params: "+params);__tracksCount=1;
        __setVbStartTime(pif.getVbStartTime());
        params = __validateParams(params,parseInt(amid,10)); if (!params) return false;        
        return __playAod(params);
    }

    function playByModel (model, params) {
        core.info("Aod.qml | playByModel called | params: "+params);
        if (!model || !model.count) {core.error ("Aod.qml | Empty Model or Model not provided. EXITING..."); return false;}
        params.playIndex = (params.playIndex==undefined || params.playIndex>=model.count)?undefined:params.playIndex;__tracksCount=model.count;
        __setVbStartTime(pif.getVbStartTime());
        params = __validateParams(params,(params.amid?params.amid:parseInt(model.at(0).aggregate_parentmid,10))); if (!params) return false;
        return __playAod(params,model);
    }

    function stop() {if (pauseRequestTimer.running) pauseRequestTimer.stop(); return __mediaStop();}

    function suspendPlaying() {
        if (__suspend||__mediaState==cMEDIA_STOP) return false;
        __prevMediaState = __mediaState;
        __suspend = true;
        stop();
        return true;
    }

    function unSuspendPlaying() {
        if (!__suspend) return false;
        __suspend = false;
        setPlayingIndex(-1);
        if (__prevMediaState==cMEDIA_PAUSE) pauseRequestTimer.restart();
        __prevMediaState=0;
        return true;
    }
    function playNext () { return __mediaNext();}
    function playPrevious () { return __mediaPrevious();}
    function pause () { return __mediaPause();}
    function resume () { return __mediaResume();}
    function forward () { return __mediaForward();}
    function rewind () { return __mediaRewind();}

    function restart() { return __mediaRestart();}
    function frmwkInitApp(){}
    function frmwkResetApp(){__clearMediaModel();__clearSuspend();}

    function clearMediaPlayer(){__clearMediaModel();__clearSuspend();return true}

    // Private functions
    function __getAllAodPlayInfo() {
        var p=getAllMediaPlayInfo();
        core.debug("Aod.qml | getAllAodPlayInfo | Stored info for amid: "+p.amid+" playIndex: "+p.playIndex+" elapseTime: "+p.elapseTime+" cmid: "+p.cmid);
        var mptr =__getMidArrReference();
        mptr[p.cmid] = p;
        mptr[p.amid]=[];
        mptr[p.amid]["lastPlayed"]=p.cmid;
        return true;
    }

    function __clearSuspend(){__suspend=false; __prevMediaState=0; return true;}

    function getSuspendStatus(){ return __suspend;}

    function __playAod (params,model) {   // model optional
        pif.__setLastSelectedAudio(pif.aod);
        if (pauseRequestTimer.running) pauseRequestTimer.stop();
        var s; for (var i in params) s+=i+" = "+params[i]+", "; core.debug(s);
        if (model) __setMediaModel(model,params.cid,params.amid,params.mediaType);
        else __setMediaModelByMid(params.amid,params.mediaType,params.duration,params.cid);
        return __mediaPlay(params);
    }

    function __validateParams (p,amid) {
        p.amid = (String(parseInt(amid,10))!="NaN")?amid:0;
        if (p.amid==0) {core.error ("Aod.qml | Missing amid... Need it to continue. EXITING..."); return false;}
        var ret=__getMidArrReference(); var ptr=(ret[p.amid])?ret[p.amid]:[];
        p.elapseTimeFormat = (String(parseInt(p.elapseTimeFormat,10))!="NaN")?parseInt(p.elapseTimeFormat,10):(ptr.elapseTimeFormat?ptr.elapseTimeFormat:1);
        p.cid = (String(parseInt(p.cid,10))!="NaN")?parseInt(p.cid,10):(ptr.cid?ptr.cid:0);
        p.playIndex=(String(parseInt(p.playIndex,10))!="NaN")?p.playIndex:(ptr.playIndex?ptr.playIndex:0);
        p.mediaType = (p.mediaType)?p.mediaType:(ptr.mediaType?ptr.mediaType:"audioAggregate");
        p.repeatType=(p.vbMode && __tracksCount)?2:(String(parseInt(p.repeatType,10))!="NaN")?p.repeatType:(ptr.repeatType?ptr.repeatType:0);
        p.playType=p.playType?p.playType:(ptr.playType?ptr.playType:0);// For sting audio playType is 1. Otherwise playType is 0.
        p.shuffle=(!p.shuffle)?false:true;
        p.vbMode=(p.vbMode==undefined)?(ptr.vbMode?ptr.vbMode:false):p.vbMode;
        p.duration = (p.duration==undefined)?(ptr.duration?ptr.duration:(p.vbMode)?"02:00:00":"00:00:00"):p.duration;
        p.elapseTime=(p.vbMode)?(__getVbElapsedTime(p.duration)):(p.elapseTime?p.elapseTime:0);
        if(p.rating==undefined)core.error("Aod.qml | Missing rating. Setting it to 254.");
        p.rating=(String(parseInt(p.rating,10))!="NaN")?parseInt(p.rating,10):(ptr.rating?ptr.rating:254);
        return p;
    }

    function __stopBlockedMid(mid,status){
        core.info("Aod.qml |  Stop Blocked Mid");
        if((status=="blocked")&&(mid==pif.aod.getPlayingAlbumId())){__clearMediaModel();__clearSuspend();return true}
        else return false;
    }

    function __stopBlockedMidInList(amidList,status,cause){        
        if(status=="pay" && cause=="PPV_REFUND"){
            core.info("Aod.qml |  PPV Refund | Stop Blocked Mid In List. amid list: " + amidList);
            var amid=pif.aod.getPlayingAlbumId();core.info("Playing Album id: "+ amid);
            for(var i in amidList){if(amidList[i]==amid){__clearMediaModel();__clearSuspend();return true;}}
        }
        return false;
    }

    function __stopAlbumsBySeatRating(seatRating){
        core.info("Aod.qml |  Stop Album By Seat Rating. seatRating: " + seatRating);
        var rating=pif.aod.getMediaRating();
        if(rating>seatRating  && pif.aod.getPlayingAlbumId()>=0 && rating<=254){__clearMediaModel();__clearSuspend();return true;}
    }

    function __checkAndStopPlayingInactiveMid(){
        if(!dataController.getMidGroupStatus(pif.aod.getPlayingAlbumId())){
             core.info("Aod.qml | __checkAndStopPlayingInactiveMid |Stop unavailable Mid on nbdgroup status change " + pif.aod.getPlayingAlbumId());
            __clearMediaModel();__clearSuspend();return true
        }
    }

    function __vbStartTimeChanged (){
        __setVbStartTime(vbStartTime);
    }

    Component.onCompleted: {
        __playerType = "Aod Media Player";
        __timerInterval = pif.__getAudioPlayTimerInterval();        // BG audio begins before cStageInitializeStates
        __skipPreviousTimeout = pif.__getSkipPreviousTimeout();
    }
    Timer {
           id:pauseRequestTimer; interval:300;repeat: true;
           onTriggered: {
               if(__mediaState==cMEDIA_PLAY){
                   __mediaPause(true);pauseRequestTimer.stop();
               }
           }
       }
}
