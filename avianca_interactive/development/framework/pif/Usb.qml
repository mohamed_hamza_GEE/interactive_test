import QtQuick 1.1
import Panasonic.Pif 1.0
import '../blank.js' as Info
Item{
    id:usb;

    property variant deviceManager;
    property variant modelFileFilters:[];
    property alias browserModel:browserModel;
    property variant folderBrowserModel
    property string cUSB_IN:'USB_STORAGE_DEVICE_CONNECT';
    property string cUSB_OUT:'USB_STORAGE_DEVICE_DISCONNECT';
    property string __folderPath;
    property bool __usbConnected: false;
    property int __maxUsb:2;
    property int __activeUSB:0;
    property int usbCount:0
    property variant usbeventnotifier;

    signal sigUsbConnected(string rootPath);
    signal sigUsbDisconnected(string rootPath);
    signal sigUsbModelReady();

    function getUsbCount(){return usbCount;}
    function setActiveUsb(id){ __activeUSB=id; return;}
    function getMountPath(){return Info.__mountPath;}
    function getFolderPath(){return __folderPath;}
    function getUsbConnected(){return __usbConnected;}
    function isFolderInUsb(path){return browserModel.isFolder(path);}

    function initialiseComponent(){
        usbeventnotifier=usbNotifierComp.createObject(usb,{});
    }

    function usbChangeDetected(change,uri,source){
        if(!__usbConnected && change==cUSB_OUT ){
            core.info("Usb.qml | usbChangeDetected | 2nd USB Inserted / Removed or 1st USB removed after restart/close flight/Ent. Off")
            return false;
        }

        core.info ('Usb.qml | usbChangeDetected | mount path : ' + uri + 'cUSB_IN='+cUSB_IN+'  Info.__mountPath.length='+Info.__mountPath.lenght);
        if (change==cUSB_IN){
            __usbConnected=true;
            for (var x in Info.__mountPath){
                if(Info.__mountPath[x]==''){ Info.__mountPath[x]=uri;
                  //  readFolder(uri,"*.png,*.jpg,*.jpeg,*.gif,*.bmp,*.mp3,*.pdf,*.txt");
                    core.info ('Usb.qml | usbChangeDetected | mount path : ' + uri +' x='+x +'Info.__mountPath['+x+']='+Info.__mountPath[x])
                    break;
                }
            }
            usbCount=usbCount+1; sigUsbConnected(uri)
        }
        else if (change==cUSB_OUT){
            if (uri==Info.__mountPath[__activeUSB]) __folderPath='';
            for (var x in Info.__mountPath) { if(Info.__mountPath[x]==uri){ Info.__mountPath[x]=''; Info.__sortOn[x]='';Info.__sortOrder[x]=''; break; } }
            var __noUsb=1;
            for (var x in Info.__mountPath) if(Info.__mountPath[x]!=''){ __noUsb=0; break; }
            if(__noUsb==1) __usbConnected=false;
            if(pif.launchApp.getLaunchType(pif.launchApp.launchId)==pif.launchApp.cPDF_VIEWER) pif.launchApp.killApp();
            if(parseInt(pif.launchApp.launchId,10)==pif.launchApp.cPDF_VIEWER)pif.launchApp.killApp();
            usbCount=usbCount-1; sigUsbDisconnected(uri);
        }
        else{ core.error ('Usb.qml | usbChangeDetected | unknown event ' + change) }
        return true;
    }

    function readFolder(path, filterList,uri){
        core.debug("Usb.qml | readFolder | path: "+path+" | filterList: "+filterList);
        if(!isFolderInUsb(path) || path=='' || !path || path==undefined) return false;
        var browserFilter;
        if(filterList=="") browserFilter=[];
        else if(filterList==undefined) browserFilter=browserModel.fileFilters;
        else {
            var filterListArr=filterList.split(','); var filters='';
            for(var i in filterListArr) filters=filters+"'"+filterListArr[i]+"',";
            var filterStr="["+filters+"]"; browserFilter=eval(filterStr);
            core.info("Usb.qml | readfolder | processed filterList : "+browserFilter)
        }
        Info.tmpFilterChanged=true;
        browserModel.fileFilters=browserFilter;
        Info.tmpUsbPath=path;
        delayTimer.restart();

        if(!pif.getUsbPreserveSortingState()){browserModel.setSortOrder(pif.getUsbDefaultSortOrder());browserModel.sort(pif.getUsbDefaultSortOn())}
        else {browserModel.setSortOrder( Info.__sortOrder[uri]);browserModel.sort(Info.__sortOn[uri]);}
        return true;
    }

    function sort(sortOn,sortOrder,usbId){
        if (usbId=='') uri=Info.__mountPath[0];
        if(sortOn==undefined || sortOn=="" || !sortOn) sortOn="filenameonly"
        if(sortOrder==undefined || sortOrder=="" || !sortOrder) sortOrder="ascending"
        Info.__sortOn[usbId]=sortOn; Info.__sortOrder[usbId]=sortOrder; browserModel.setSortOrder(sortOrder); browserModel.sort(sortOn);
        core.info("Usb.qml | Sort | Sorting performed on: "+browserModel.sortOn+" in: "+browserModel.sortOrder +" order")
        sigUsbModelReady();
    }

    function checkFileType(fileType,type){
        return (((Info.filterArray[type].replace(/,/g,")") + ")").replace(/\./g,"(").replace(/\*/g,"")).indexOf("("+fileType+")") != -1);
    }
    function isImage(fileType){return checkFileType(fileType.toLowerCase(),"usb_images"); }
    function isDocument(fileType){return checkFileType(fileType.toLowerCase(),"usb_doc"); }
    function isMusic(fileType){return checkFileType(fileType.toLowerCase(),"usb_music"); }
    function isDirectory(filePath){return pif.usb.isFolderInUsb(filePath)}

    function readPrevFolder(filterList){
        core.debug("Usb.qml | readPrevFolder | Current path: "+__folderPath);
        var browserFilter;
        if(filterList=="") browserFilter=[];
        else if(filterList==undefined) browserFilter=browserModel.fileFilters;
        else {browserFilter=filterList;}
        core.info("Usb.qml | readPrevFolder | processed filterList : "+browserFilter)
        var pathElements = __folderPath.split('/'); var newPath=""; var lastIndex=1;
        if(pathElements[pathElements.length-1] == "") lastIndex=2;
        for(var i =0;i<pathElements.length-lastIndex;i++){
            if(i>0) newPath = newPath + "/"; newPath = newPath + pathElements[i];
        }
        readFolder(getMountPath()[0]+newPath,browserFilter);
    }

    function getFilterArray(type){
        if(!type){ return Info.filterArray;}
        else {return Info.filterArray[type];}
    }

    function setFilterArray(filterArray,type){
        if(!type){ Info.filterArray=filterArray;}
        else {Info.filterArray[type]=filterArray;}
    }

    function setUsbPath(){
        Info.tmpFilterChanged=false;
        browserModel.folder=Info.tmpUsbPath;
    }

    function frmwkInitApp() { core.info("Usb.qml | frmwkInitApp");}

    function frmwkResetApp(){
        core.info("Usb.qml | frmwkResetApp");
        browserModel.folder=""; modelFileFilters=[]; Info.tmpUsbPath=[];Info.tmpFilterChanged=false;Info.__mountPath=[]; Info.__sortOn=[]; Info.__sortOrder=[];
        __folderPath=''; __usbConnected=false; __activeUSB=0; usbCount=0;
        for (var x=0 ; x<__maxUsb; x++){
            Info.__mountPath[x]=""; Info.__sortOn[x]=""; Info.__sortOrder[x]="";
        }
    }

    ResourceBrowserModel {
        id:browserModel
        //folder:""
        //fileFilters:modelFileFilters
        elementFilters:[ mp3filter ]
        showdotfiles: pif.getUsbDotFilesState()
        sortOn:pif.getUsbDefaultSortOn()
        sortOrder:pif.getUsbDefaultSortOrder()
        onFolderChanged:{
            if(Info.tmpFilterChanged==true){return true;}
            core.info("Usb.qml | onFolderChanged | Abs. path of currently visited folder : " +folder +"   Info.__mountPath[0]="+Info.__mountPath[__activeUSB])
            var realArr=folder.split(Info.__mountPath[__activeUSB]);
            __folderPath=(realArr.length>1)?realArr[1]:realArr[0]
            core.info("Usb.qml | onFolderChanged | __folderPath considering Usb at root level : " +__folderPath)
            sigUsbModelReady();
        }
    }

    Component{
        id:usbNotifierComp
        UsbCoreEventNotifier {
            enabled: true;
            deviceManager:usb.deviceManager
            onChangeDetected:{
                core.info('Usb.qml | UsbCoreEventNotifier | onChangeDetected | Event ' + change);
                usbChangeDetected(change,uri);
            }
        }
    }

    MP3ElementFilter {id: mp3filter}

    Component.onCompleted: {
        Info.__mountPath=[]; Info.__sortOn=[]; Info.__sortOrder=[];
        for (var x=0 ; x<__maxUsb; x++){
            Info.__mountPath[x]=''; Info.__sortOn[x]=''; Info.__sortOrder[x]='';
        }
        if(pif.getFolderResourceBrowserModel()){
            folderBrowserModel = Qt.createQmlObject('import QtQuick 1.1; import Panasonic.Pif 1.0; ResourceBrowserModel { folder:pif.usb.browserModel.folder; fileFilters:[" "]; elementFilters:pif.usb.browserModel.elementFilters; showdotfiles: pif.getUsbDotFilesState(); sortOn:pif.getUsbDefaultSortOn(); sortOrder:pif.getUsbDefaultSortOrder(); }',usb)
        }
        Info.filterArray={
                "usb_images":'*.png,*.jpg,*.jpeg,*.gif,*.bmp',
                "usb_music":"*.mp3",
                "usb_doc":"*.pdf,*.txt",
                "usb_dir":'*.png,*.jpg,*.jpeg,*.gif,*.bmp,*.mp3,*.pdf,*.txt',
       }
        Info.tmpUsbPath="";Info.tmpFilterChanged=false;
    }
    Timer{id:delayTimer;repeat:false; interval:200; onTriggered:setUsbPath()}
}
