import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as SystemData

Item{
    id:usbPlaylist;

    property int cPALYLIST_FULL:0;
    property int cTRACK_ADDED:1;
    property int cPLAYLIST_NOT_CREATED:2;
    property int cTRACK_MODEL: 0;
    property int cINDEX: 1;
    property int numberOfPlaylist:0;

    signal sigCreateMetadataPlaylist(int num, string playlistType);
    signal sigTracksAdded(int pid,int status);
    signal sigTracksRemoved(int pid,string pathfilename);
    signal sigAllTracksRemoved(int pid);
    signal sigTracksMoved(variant from,variant to, int pid);

    Connections{
    	target: pif.usb; 
        onSigUsbConnected: if(SystemData.PlaylistArr.length==0) frmwkInitApp();
        onSigUsbDisconnected: {frmwkResetApp();}
    }

    function addTrack(model,index,cid,playlistId){
        core.info("UsbPlaylist.qml | addTrack | ADD TRACKS");

        if(playlistId==undefined || playlistId=="") playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(cid==undefined || cid=="") cid=0;

        var cModel = SystemData.PlaylistArr[playlistId][cTRACK_MODEL];
        if(cModel==undefined){core.error("UsbPlaylist.qml | addTrack | Playlist not created"); return cPLAYLIST_NOT_CREATED;}

        if(model.getValue(index,"filetype")!="mp3") {core.error("UsbPlaylist.qml | Provided index is not a valid mp3 file exiting..."); return;}
        if(pif.getMaxTracksUsbPlaylist()>0 && cModel.count == pif.getMaxTracksUsbPlaylist()){
            core.info("UsbPlaylist.qml | addTrack | playlist full");
            sigTracksAdded(playlistId,cPALYLIST_FULL);
            return cPALYLIST_FULL;
        }else if(pif.getMaxTracksInAllPlaylist()>0 && pif.getMaxTracksInAllPlaylist()==pif.getTotalTracksInAllPlaylists()){core.info("UsbPlaylist.qml | addTrack | All Playlist full");sigTracksAdded(playlistId,cPALYLIST_FULL);return cPALYLIST_FULL;}

        var tempPathFileName = model.getValue(index,"pathfilename");
        cModel.append({"media_type":"mp3","duration":model.getValue(index,"duration")?model.getValue(index,"duration"):"00:00:00","cid":cid,"pathfilename":tempPathFileName,
                       "filetype":model.getValue(index,"filetype"),"filenameonly":model.getValue(index,"filenameonly"),"filesize":model.getValue(index,"filesize"),
                       "mp3album":model.getValue(index,"mp3album"),"mp3artist":model.getValue(index,"mp3artist"),"mp3song":model.getValue(index,"mp3song"),
                       "mp3title":model.getValue(index,"mp3title"),"mp3year":model.getValue(index,"mp3year")
                      });

        SystemData.PlaylistArr[playlistId][cINDEX][tempPathFileName]=1;
        if(SystemData.PlaylistArr[playlistId]["playlistAlbumId"]==pif.usbMp3.getPlayingAlbumId()){
            pif.usbMp3.__appendToMediaModel(cModel,(cModel.count-1),cid,pif.usbMp3.getPlayingAlbumId());
        }

        if(!SystemData.PlaylistArr[playlistId]["paxusLogged"] && pif.paxus!=undefined){
            pif.paxus.playlistCreatedLog();
            SystemData.PlaylistArr[playlistId]["paxusLogged"]=1;
        }

        sigTracksAdded(playlistId,cTRACK_ADDED);
        return cTRACK_ADDED;
    }

    function addAllTracks(model,cid,playlistId){
        core.info("UsbPlaylist.qml | addAllTracks");
        if(playlistId==undefined || playlistId=="") playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(cid==undefined || cid=="") cid=0;

        var cModel = SystemData.PlaylistArr[playlistId][cTRACK_MODEL];
        if(cModel==undefined){core.error("UsbPlaylist.qml | addTrack | Playlist not created"); return cPLAYLIST_NOT_CREATED;}

        for(var counter=0;counter<model.count;counter++){
            if(pif.getMaxTracksUsbPlaylist()>0 && cModel.count == pif.getMaxTracksUsbPlaylist()){
                core.info("UsbPlaylist.qml | addTrack | Playlist full");
                sigTracksAdded(playlistId,cPALYLIST_FULL);
                return cPALYLIST_FULL;
            }else if(pif.getMaxTracksInAllPlaylist()>0 && pif.getMaxTracksInAllPlaylist()==pif.getTotalTracksInAllPlaylists()){
                core.info("UsbPlaylist.qml | addTrack | All Playlist full");
                sigTracksAdded(playlistId,cPALYLIST_FULL);return cPALYLIST_FULL;
            }

            if(model.getValue(counter,"filetype")!="mp3") continue;
            var tempPathFileName = model.getValue(counter,"pathfilename");
            var i=isTrackInPlaylist(tempPathFileName);
            if(i) continue;

            cModel.append({"media_type":"mp3","duration":model.getValue(counter,"duration")?model.getValue(counter,"duration"):"00:00:00","cid":cid,"pathfilename":tempPathFileName,
                           "filetype":model.getValue(counter,"filetype"),"filenameonly":model.getValue(counter,"filenameonly"),"filesize":model.getValue(counter,"filesize"),
                           "mp3album":model.getValue(counter,"mp3album"),"mp3artist":model.getValue(counter,"mp3artist"),"mp3song":model.getValue(counter,"mp3song"),
                           "mp3title":model.getValue(counter,"mp3title"),"mp3year":model.getValue(counter,"mp3year")
                          });


            if(SystemData.PlaylistArr[playlistId]["playlistAlbumId"]==pif.usbMp3.getPlayingAlbumId()){
                pif.usbMp3.__appendToMediaModel(cModel,(cModel.count-1),cid,pif.usbMp3.getPlayingAlbumId());
            }

            SystemData.PlaylistArr[playlistId][cINDEX][tempPathFileName]=1;
            if(!SystemData.PlaylistArr[playlistId]["paxusLogged"] && pif.paxus!=undefined){
                pif.paxus.playlistCreatedLog();
                SystemData.PlaylistArr[playlistId]["paxusLogged"]=1;
            }
        }

        sigTracksAdded(playlistId,cTRACK_ADDED);
        return cTRACK_ADDED;
    }

    function removeTrack(pathfilename){
        core.info("UsbPlaylist.qml | removeTrack | pathfilename: "+pathfilename)
        var pathfilenameExits=false; var indxOfMP=-1;

        for(var counter=1;counter<=numberOfPlaylist;counter++){
            pathfilenameExits = SystemData.PlaylistArr[counter][cINDEX][pathfilename];
            if(!pathfilenameExits || pathfilenameExits==undefined) continue;

            indxOfMP=core.coreHelper.searchEntryInModel(SystemData.PlaylistArr[counter][cTRACK_MODEL],"pathfilename",pathfilename);

            if(indxOfMP!=-1 && pif.usbMp3.getPlayingAlbumId()==SystemData.PlaylistArr[counter]["playlistAlbumId"]){
                pif.usbMp3.__removeFromMediaModel(pathfilename);
                if(pif.usbMp3.getMediaModelCount()==0) pif.usbMp3.__clearSuspend();
            }

            SystemData.PlaylistArr[counter][cTRACK_MODEL].remove(indxOfMP,1);
            delete SystemData.PlaylistArr[counter][cINDEX][pathfilename];

            sigTracksRemoved(counter,pathfilename);
            return true;
        }
        return false;
    }

    function moveTrackUp(fromPosition,toPosition,playlistId){
            core.log("Playlist.qml | moveTrackUp | fromPosition: "+fromPosition+" | toPosition: "+toPosition);
            moveTrack(fromPosition,toPosition,playlistId);
    }
    function moveTrackDown(fromPosition,toPosition,playlistId){
             core.log("Playlist.qml | moveTrackDown | fromPosition: "+fromPosition+" | toPosition: "+toPosition);
            //array needs to be reversed for GUI to display correctly
            fromPosition.reverse(); toPosition.reverse();
            moveTrack(fromPosition,toPosition,playlistId);
    }

    function moveTrack(fromPosition,toPosition,playlistId){
        core.info("UsbPlaylist.qml | moveTrack | fromPosition: "+fromPosition+" | toPosition: "+toPosition);
        var numberOfItemsToMove=1;
        if(playlistId==undefined || playlistId=="") playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(!fromPosition || typeof(fromPosition)!="object"){core.info("UsbPlaylist.qml | moveTrack | fromPosition needs to be an array");return false}
        if(!toPosition || typeof(toPosition)!="object"){core.info("UsbPlaylist.qml | moveTrack | toPosition needs to be an array");return false}

        var cModel = SystemData.PlaylistArr[playlistId][cTRACK_MODEL];
        if(!(cModel && cModel.count)){core.info("UsbPlaylist.qml | moveTrack | Playlist not found")}
        if(fromPosition.length!=toPosition.length){core.info("UsbPlaylist.qml | moveTrack | fromPosition and toPosition length must be same");return false}
        for(var counter=0;counter<fromPosition.length;counter++){
            if(fromPosition[counter]<0 || fromPosition[counter]>cModel.count){core.info("UsbPlaylist.qml | moveTrack | Invalid fromPosition: "+fromPosition[counter]);return false}
            if(toPosition[counter]<0 || toPosition[counter]>=cModel.count){core.info("UsbPlaylist.qml | moveTrack | Invalid toPosition: "+toPosition[counter]);return false}
        }

        for(counter=0;counter<fromPosition.length;counter++){cModel.move(fromPosition[counter],toPosition[counter],numberOfItemsToMove);}
        if(playlistId==Math.abs(pif.usbMp3.getPlayingAlbumId())){pif.usbMp3.__moveTracks(fromPosition,toPosition);}
        sigTracksMoved(fromPosition,toPosition,playlistId);
        return true;
    }

    function removeAllTracks(playlistId){
        core.info("UsbPlaylist.qml | removeAllTracks");
        if(playlistId==undefined || playlistId=="") playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){ core.error("UsbPlaylist.qml | removeAllTracks | Playlist not created");return false;}
        if(pif.usbMp3.getPlayingAlbumId()==SystemData.PlaylistArr[playlistId]["playlistAlbumId"]){ pif.usbMp3.__clearMediaModel(); pif.usbMp3.__clearSuspend();}
        SystemData.PlaylistArr[playlistId][cTRACK_MODEL].clear();
        SystemData.PlaylistArr[playlistId][cINDEX]=new Array();
        sigAllTracksRemoved(playlistId);
        return true;
    }

    function getTotalTracks(playlistId){
        if(playlistId==undefined || playlistId=="") playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(!SystemData.PlaylistArr[playlistId] || SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){core.error("UsbPlaylist.qml | getTotalTrack | Playlist not created"); return 0;}
        return SystemData.PlaylistArr[playlistId][cTRACK_MODEL].count;
    }

    function getAllPlaylistTotalTracks(){
        var allPlaylistTrackCount=0;
        for(var playlistId in SystemData.PlaylistArr){
                core.debug(" Playlist | UsbPlaylist |  getAllPlaylistTotalTracks |  pid: "+playlistId+" count: "+SystemData.PlaylistArr[playlistId][cTRACK_MODEL].count)
                allPlaylistTrackCount += SystemData.PlaylistArr[playlistId][cTRACK_MODEL].count;
         }
        return allPlaylistTrackCount;
    }

    function getPlaylistModel(playlistId){
        if(playlistId==undefined || playlistId=="") playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(!SystemData.PlaylistArr[playlistId] || SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){core.error("UsbPlaylist.qml | getPlaylistModel"); return 0;}
        return SystemData.PlaylistArr[playlistId][cTRACK_MODEL];
    }

    function getPlaylistAlbumId(playlistId){
        core.info("UsbPlaylist.qml | getPlaylistAlbumId | playlistIndex :"+playlistId);
        if(playlistId==undefined||playlistId=="") playlistId=-1;
        playlistId=Math.abs(playlistId);
        if(!SystemData.PlaylistArr[playlistId] || SystemData.PlaylistArr[playlistId][cTRACK_MODEL]==undefined){core.error("UsbPlaylist.qml | getPlaylistAlbumId"); return 0;}
        return SystemData.PlaylistArr[playlistId]["playlistAlbumId"];
    }

    function isTrackInPlaylist(pathfilename){
        var indxOfPlaylist;
        for(var counter=1;counter<=numberOfPlaylist;counter++){
            if(SystemData.PlaylistArr[counter]) indxOfPlaylist = SystemData.PlaylistArr[counter][cINDEX][pathfilename];
            if(indxOfPlaylist==undefined) continue;
            else return true;
        }
        return false;
    }

    function getMp3Count(model){var count=0; for(var i=0;i<model.count;i++) if(model.getValue(i,"filetype")=="mp3") count++; return count;}

    function frmwkInitApp(){
        var params;
        core.info("UsbPlaylist.qml | frmwkInitApp called");
        numberOfPlaylist=pif.getNumberOfUsbPlaylist();
        SystemData.PlaylistArr=[];
        for(var i=1;i<=numberOfPlaylist;i++){
            SystemData.PlaylistArr[i]=[];
            SystemData.PlaylistArr[i]["playlistAlbumId"]=-i;
            SystemData.PlaylistArr[i][cTRACK_MODEL]=Qt.createQmlObject('import QtQuick 1.1; import Panasonic.Pif 1.0; SimpleModel {}',usbPlaylist);
            SystemData.PlaylistArr[i][cINDEX]=[];
            SystemData.PlaylistArr[i]["paxusLogged"]=0;
            params = {playlistIndex:i,name:"UsbPlaylist "+i,mediaType:"mp3"};
            if(pif.sharedModelServerInterface) pif.sharedModelServerInterface.__createPlaylist(params);
        }
        sigCreateMetadataPlaylist(numberOfPlaylist,"UsbPlaylist");
    }

    function frmwkResetApp(){
        core.info("UsbPlaylist.qml | frmwkResetApp called");
        for(var counter=1;counter<=numberOfPlaylist;counter++){removeAllTracks(counter);}
        SystemData.PlaylistArr=[];
    }

    Component.onCompleted: SystemData.PlaylistArr=[];
}
