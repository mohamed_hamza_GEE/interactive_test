import QtQuick 1.1
import Panasonic.Pif 1.0

Item{
    id:windowDimmable;
    property variant deviceMngr;
    property variant windowDimmer;
    property int __dimLevel: 0;
    signal sigDimLevelChanged(int dimLevel);

    Connections{
        target: (windowDimmer)?windowDimmer:null;
        onWindowDimLevelChanged: {
            core.info("WindowDimmable.qml | onWindowDimLevelChanged | windowId: "+windowId+" dimLevel: "+dimLevel);
            for(var i=0; i<windowDimmer.count;i++){core.debug("WindowDimmable.qml | onWindowDimLevelChanged | windowDimmer.at("+i+").windowid: "+windowDimmer.at(i).windowid+" dimLevel: "+windowDimmer.at(i).dimlevel);}
            __setDimLevel();
        }
    }

    function frmwkInitApp() { core.info("WindowDimmable.qml | frmwkInitApp"); return true;}
    function frmwkResetApp(){
        __setDimLevel();
        return true;
    }

    function getDimmableState(){
        core.info("WindowDimmable.qml | getDimmableState | pif.getNoOfDimmableWindows() = "+pif.getNoOfDimmableWindows()+" | windowDimmer.count = "+windowDimmer.count);
        return (pif.getNoOfDimmableWindows() && windowDimmer.count>0)?true:false;
    }

    function getDimLevel(){ return __dimLevel;}
    function getDimLevelInPercent(){
        if (getMaxLevel()==getMinLevel()) return 0;
        return (__dimLevel-getMinLevel())*100/(getMaxLevel()-getMinLevel());
    }

    function setDimLevel(dimValue){
        core.info("WindowDimmable.qml | setDimlevel | dimValue: "+dimValue) ;
        dimValue=parseInt(dimValue,10);
        for(var i=0; i<windowDimmer.count;i++){
            var id = windowDimmer.at(i).windowid;
            windowDimmer.setDimLevel(id, dimValue);
        }
        return true;
    }

    function getMinLevel(){
        core.info("WindowDimmable.qml | getMinLevel called");
        return windowDimmer.count>0?windowDimmer.at(0).minlevel:1;
    }

    function getMaxLevel(){
        core.info("WindowDimmable.qml | getMaxLevel called");
        return windowDimmer.count>0?windowDimmer.at(0).maxlevel:1;
    }

    function __setDimLevel() { __dimLevel = windowDimmer.count>0?windowDimmer.at(0).dimlevel:0; sigDimLevelChanged(__dimLevel);}

    Component.onCompleted: {
        core.info("WindowDimmable.qml | Component loaded.");
        if (core.pc) windowDimmer = core.coreHelper.loadDynamicComponent("pif/WindowDimmerSIM.qml",windowDimmable);
        else windowDimmer = windowDimmerComp.createObject(windowDimmable);
        core.info ("WindowDimmable.qml | windowDimmer Reference: "+windowDimmer);
        __setDimLevel();
    }

    Component {
        id: windowDimmerComp;
        WindowDimmer{id: windowDimmer; deviceManager: deviceMngr;}
    }
}
