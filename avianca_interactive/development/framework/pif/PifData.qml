import QtQuick 1.1
import Panasonic.Pif 1.0
import "pifData.js" as PifData
Item {
    id: pifData
    // Constants
    property int cLRU_GEODE:1;
    property int cLRU_PSEB:2;
    property int cLRU_FE:3;
    property int cLRU_ECO:4;
    property int cLRU_VKARMA:5;
    property int cLRU_ELITE:6;
    property int cLRU_VIA:7;
    property int cLRU_LXGEODE:8;
    property int cLRU_HDPSEB:9;
    property int cLRU_SM17:10;
    property int cLRU_ELITEV2:11;
    property int cLRU_ECOV2:12;
    property int cLRU_ALTUS:13;

    property int cNO_HANDSET:0;
    property int c3K_HANDSET:1;
    property int cDPCU_HANDSET:2;
    property int cSTD_HANDSET:3;
    property int cPREMIUM_HANDSET:4;
    property int cSTD_KARMA_HANDSET:5;
    property int cVIDEO_KARMA_HANDSET:6;
    property int cCANDYBAR_HANDSET:7;
    property int cSEAT_HANDSET:8;

    property int cINPUT_KEY:1;
    property int cINPUT_MOUSE:2;
    property int cUSB_DEVICE:1;
    property int cIPOD_DEVICE:2;

    property int cAOD_VOLUME_SRC:0;
    property int cVOD_VOLUME_SRC:1;
    property int cPC_VOLUME_SRC:2;
    property int cPA_VOLUME_SRC:3;
    property int cUSB_VOLUME_SRC:4;

    property int cPORTRAIT:0
    property int cLANDSCAPE:1
    property int cINVERTED_PORTRAIT:2
    property int cINVERTED_LANDSCAPE:3

    property int __eventsSynced:0;
    property string __orientationType: "p";
    property bool __preBacklightOffState: false;

    property alias navInput:navInput;
    property alias eventListener:eventListener;
    property alias audioMixerID: mixerID;
    property bool muteState:false;
    property bool eventForDefaultBrightness: false; 

    // Signals
    signal openFlightChanged(int openFlight);
    signal alldoorsClosedChanged(int alldoorsClosed)
    signal entOnChanged(int entOn);
    signal paStateChanged(int paState);
    signal mediaDateChanged(int mediaDate);
    signal groundSpeedChanged(int groundSpeed);
    signal altitudeChanged(int altitude);// altitude is in feets.
    signal sourceIATAChanged(string sourceIATA);
    signal destinationIATAChanged(string destinationIATA);
    signal sourceICAOChanged(string sourceICAO);
    signal flightNumberChanged(string flightNumber);
    signal routeIdChanged(int routeId);
    signal cgDataAvailableChanged(int cgDataAvailable);
    signal satComStatusChanged(int satComStatus);
    signal gcsStatusChanged(bool gcsStatus);
    signal delayedEntOffChanged(int delayedEntOff);
    signal attendantCallChanged(bool attendantCall);
    signal readingLightChanged(bool readingLight);
    signal seatRatingChanged(int seatRating);
    signal infoModeChanged(bool infoMode);
    signal interactiveStateChanged(int interactiveState);
    signal ipodConnectedChanged(int ipodConnected);
    signal wowStateChanged(bool wowState);
    signal capSensorStateChanged(int capSensorState);
    signal destICAOcodeChanged(int destICAOcode);
    signal exphoneStatusChanged(int exphoneStatus);
    signal exphoneServerStateChanged(int exphoneServerState);
    signal exphoneNotificationStateChanged(int exphoneNotificationState);
    signal gmtOffsetOfSourceChanged(real gmtOffsetOfSource);
    signal gmtOffsetOfDestinationChanged(real gmtOffsetOfDestination);
    signal interactiveOverrideChanged(int interactiveOverride);
    signal trueAirSpeedChanged(int trueAirSpeed);
    signal distToDestinationChanged(int distToDestination);
    signal outsideAirTempChanged(int outsideAirTemp);
    signal orientationChanged(int orientation);
    signal stowedChanged(int stowed);
    signal timeToDestinationChanged(int timeToDestination);
    signal arrivalTimeChanged(string arrivalTime);
    signal timeAtOriginChanged(string timeAtOrigin);
    signal timeSinceTakeOffChanged(string timeSinceTakeOff);
    signal timeAtDestinationChanged(string timeAtDestination);
    signal extvStateChanged(int extvState);
    signal extvVersionChanged(int extvVersion);
    signal deviceChargingChanged(int deviceCharging);
    signal usbPortStatusChanged(int usbPortStatus);
    signal ipodPortStatusChanged(int ipodPortStatus);
    signal volumeChanged(int volumeLevel,int volumeSource);
    signal volumeSourceChanged(int volumeLevel,int volumeSource);
    signal paVolumeChanged(int volumeLevel);
    signal brightnessChanged(int brightnessLevel);
    signal contrastChanged(int contrastLevel);
    signal serviceAccessChanged(string accessType,int serviceMid);
    signal midAccessChanged(string accesstype,int mid);
    signal backlightStateChanged(int backlightState);
    signal interactiveResetChanged();
    signal destinationCityTempChanged(int destinationCityTemp);
    signal distFromOriginChanged(int distFromOrigin);
    signal externalAppActiveChanged(bool externalAppActive);
    signal airFrameTailNumberChanged(string tailNumber);
    signal liveTvSystemStatusChanged(int status);
    signal fastenSeatBeltChanged(int status);
    signal audioMixerEOS();
    signal audioMixerPlay(int status);
    signal accessibilityModeChanged(bool accessibilityMode);
    signal powerDownEventReceived();
    signal vhsHandsetInCraddleState(int state);
    signal sigPaxInfoDisable(bool paxInfoDisable);
	signal sigIpodChargeConnect();
	signal sigIpodDischargeConnect();

    signal sigPpvStatusChanged(int mid, string status,string cause);
    signal sigCtMessageReceived(int ctMessageType, string ctMsgString);
    signal sigSurveyReceived(int surveyId,string surveyTitle);
    signal sigInflightMenuStatusChanged(string appName,string type,int catalogId,string status,string cabinClass);
    signal sigCgStatusChanged(string appName, string status);
    signal sigVideoSSRC(int mid,int duration,int adMedia,int trickPlay);
    signal vbStartTimeChanged(int vbStartTime);
    signal usbStorageDeviceUnknown();
    signal usbMountFailed();
    signal audioLostSeatReboot();
    signal videoLostSeatReboot();
    signal audioRecoveredSeatReboot();
    signal audioDeviceAdded();
    signal audioDeviceRemoved();
    signal sigFallbackMode(bool fallbackMode);
    signal sigIgnoreFallbackEventsChanged(bool ignoreFallback)
    signal sigToggleMuteState(bool muteState);
    signal sigPdiDataUpdateCountChanged(int pdiDataUpdateCount);
    signal sigStreamerGroupStatusChanged(bool groupStatusChanged)
    signal sigHandsetAvailableChanged(bool handsetAvailable)

    onSigIgnoreFallbackEventsChanged: {
        if(PifData.streamerStates){
            var fallbackMode = String(parseInt(PifData.streamerStates[0],10))!="NaN"?parseInt(PifData.streamerStates[0],10):1;
            var groupCount = String(parseInt(PifData.streamerStates[1],10))!="NaN"?parseInt(PifData.streamerStates[1],10):0;
            var groupStates =PifData.groupStates;
            PifData.FallbackMode=(fallbackMode==0?1:0)
            core.info("PifData.qml | Fallback State Change Event: Fallback Mode "+ PifData.FallbackMode +" Group Count: "+groupCount+" Group States: "+ groupStates);
            sigFallbackMode(PifData.FallbackMode);
        }
    }
    // Public functions
    function getMidInfo(mid,key){return (PifData.MidArr[mid])?(key=="subtitleLid"&& !PifData.MidArr[mid]["subtitleEnabled"])?-1:PifData.MidArr[mid][key]:undefined;}
    function getVideoSpeeds(){return PifData.VideoSpeedArr.join(',');}
    function getPIPColorKey(){return PifData.PIPColor[0]}
    function getPIPColorMask(){return PifData.PIPColor[1]}
    function getCabinClass(){return PifData.CabinClass;}
    function getCabinClassName(){return PifData.CabinClassName;}
    function getSeatNumber(){return PifData.SeatNumber}
    function getSeatRow(){return PifData.SeatRow}
    function getOpenFlight(){return PifData.OpenFlight}
    function getAlldoorsClosed(){return PifData.alldoorsClosed}
    function getEntOn(){return PifData.EntOn}
    function getInteractiveState(){return PifData.InteractiveState}
    function getInteractiveOverride(){return PifData.InteractiveOverride}
    function getMediaDate(){return PifData.MediaDate}
    function getSourceIATA(){return PifData.SourceIATA;}
    function getDestinationIATA(){return PifData.DestinationIATA;}
    function getSourceICAO(){return PifData.SourceICAO;}
    function getFlightNumber(){return PifData.FlightNumber;}
    function getHandsetType(){return PifData.handset;}
    function getMonitorType(){return PifData.MonitorType;}
    function getTimeToDestination(){return PifData.TimeToDestination;}
    function getTimeAtDestination(){return PifData.TimeAtDestination;}
    function getExternalAppActive(){return PifData.ExternalAppActive;}
    function getGcsStatus(){return (PifData.GcsInstalled && PifData.GcsStatus)?true:false;}
    function getVolumeLevel(){return PifData.Volume;}
    function getVolumeSource(){return PifData.VolumeSource;}
    function getDefaultAudioVolume(){return PifData.DefaultAudioVolume;}
    function getDefaultVideoVolume(){return PifData.DefaultVideoVolume;}
    function getBrightnessLevel(){return PifData.BrightnessLevel;}
    function getDefaultBrightness(){return PifData.BrightnessDefault;}
    function getContrastLevel(){return PifData.ContrastLevel;}
    function getDefaultContrast(){return PifData.ContrastDefault;}
    function getPAState(){return PifData.PaState;}
    function getIpodState(){return PifData.IpodConnected;}
    function getWowState(){return PifData.WowState;}
    function getCapSensorState(){return PifData.CapSensorState;}
    function getStowedState(){return PifData.Stowed;}
    function getExtvState(){return PifData.ExtvState;}
    function getExtvVersion(){return PifData.ExtvVersion;}
    function getSatcomState(){return PifData.SatComStatus;}
    function getVkarmaSeatIp(){return (PifData.eX2_Feedforward_Host)?PifData.eX2_Feedforward_Host:PifData.Attached_SM_IP;}
    function getAttendantCall(){return PifData.AttendantCall;}
    function getReadingLight(){return PifData.ReadingLight;}
    function getSeatRating(){return PifData.SeatRating;}
    function getVolumeStep(){return PifData.VolumeStep;}
    function getBrightnessStep(){return PifData.BrightnessStep;}
    function getContrastStep(){return PifData.ContrastStep;}
    function getBacklightState(){ return displayID.backlightLevel;}
    function getMinPAVolume(){return PifData.MinPAVolume;}
    function getNumberOfAodPlaylist(){return PifData.NumberOfAodPlaylist;}
    function getNumberOfKidsAodPlaylist(){return PifData.NumberOfKidsAodPlaylist;}
    function getNumberOfUsbPlaylist(){return PifData.NumberOfUsbPlaylist;}
    function getDefaultTelephonyVolume(){return PifData.DefaultTelephonyVolume;}
    function getMaxTracksAodPlaylist(){return PifData.MAXTracksInAodPlaylist;}
    function getMaxTracksKidsAodPlaylist(){return PifData.MAXTracksInKidsAodPlaylist;}
    function getMaxTracksUsbPlaylist(){return PifData.MAXTracksInUsbPlaylist;}
    function getRemovePaidMidsFromPlaylist(){return PifData.RemovePaidMidsFromPlaylist;}
    function getOrientation(){return PifData.Orientation;}
    function getCgDataStatus(){return PifData.CgDataAvailable;}
    function getNavInput(){return navInput.inputMode;}
    function getRunGamePath(){return PifData.runGamePath;}
    function getDelayedEntOff(){return PifData.DelayedEntOff;}
    function getCorePssState(){return PifData.corePssState;}
    function getInfoMode(){return PifData.InfoMode;}
    function getPifLruData(param){return PifData.LruParameters[param];}  // Not to be used... to be deprecated. Instead use getLruData
    function getLruData(param){return PifData.LruParameters[param];}
    function getLru1DArrayData(param){return PifData.Lru1DParameters[param];}
    function getLru2DArrayData(param){return PifData.Lru2DParameters[param];}
    function getPaxusScreenLogStatus(){return PifData.EnablePaxusScreenLog;}
    function getExphoneServerState(){return PifData.ExphoneServerState;}
    function getExphoneNotificationState(){return PifData.ExphoneNotificationState;}
    function getExphoneAvailable(){return PifData.ExphoneAvailable;}
    function getPremiumHandsetDisplayArr(){return PifData.PremiumHandsetDisplayArr; }
    function getDestinationCityTemp(){return PifData.DestinationCityTemp; }
    function getDestICAOcode(){return PifData.DestICAOcode; }
    function getPermitIncomingCall(){return PifData.PermitIncomingCall;}
    function getRetainPlaylistdata(){return PifData.RetainPlaylistdata;}
    function getPlaylistUpdateRequiredOnKarma(){return PifData.PlaylistUpdateRequiredOnKarma;}
    function getUsbDataRetainStatus(){return PifData.RetainUsbData;}
    function getUsbDotFilesState(){return PifData.ShowDotFiles;}
    function getUsbPreserveSortingState(){return PifData.PreserveSorting;}
    function getUsbDefaultSortOn(){return PifData.DefaultSortOn;}
    function getUsbDefaultSortOrder(){return PifData.DefaultSortOrder;}
    function getUsbMaxfileSize(){return PifData.MaxFileSize;}
    function getUsbMaxImagefileSize(){return PifData.MaxImageFileSize;}
    function getIpodCableVideo(){return PifData.IpodCableVideo;}
    function getGroundSpeed(){return PifData.GroundSpeed;}
    function getAltitude(){return PifData.Altitude;}
    function getDistFromOrigin(){return PifData.DistFromOrigin;}
    function getTimeSinceTakeOff(){return PifData.TimeSinceTakeOff;}
    function getDistToDestination(){return PifData.DistToDestination;}
    function getOutsideAirTemperature(){return PifData.OutsideAirTemp;}
    function getTrueAirSpeed(){return PifData.TrueAirSpeed;}
    function getTimeAtOrigin(){return PifData.TimeAtOrigin;}
    function getArrivalTime(){return PifData.ArrivalTime;}
    function getGmtOffsetOfDestination(){return (PifData.GmtOffsetOfDestination+PifData.DstOfDestination);}
    function getGmtOffsetOfSource(){return (PifData.GmtOffsetOfSource+PifData.DstOfSource);}
    function getOrientationType(){return __orientationType;}
    function getNumberOfVodPlaylist(){return PifData.NumberOfVodPlaylist;}
    function getNumberOfKidsVodPlaylist(){return PifData.NumberOfKidsVodPlaylist;}
    function getMaxTracksVodPlaylist(){return PifData.MAXTracksInVodPlaylist;}
    function getMaxTracksKidsVodPlaylist(){return PifData.MAXTracksInKidsVodPlaylist;}
    function getMaxTracksInAllPlaylist(){return PifData.MaxTracksInAllPlaylist;}
    function getRouteId(){return PifData.RouteId;}
    function getBrowserInSeatState(){return PifData.IsBrowserInSeat;}
    function getNoOfDimmableWindows(){return PifData.NoOfDimmableWindows;}
    function getVbStartTime(){return PifData.VbStartTime;}
    function getDeviceConnected(){return PifData.DeviceConnected;}
    function getFolderResourceBrowserModel(){return PifData.FolderResourceBrowserModel;}
    function getAutoRotation(){return PifData.AutoRotation;}
    function getSimulationData(){return PifData.SimData;}
    function getFallbackMode(){return PifData.FallbackMode;}
    function getPaxusServiceStatus(){return PifData.PaxusServiceStatus;}
    function getIgnoreFallbackEvents(){return PifData.IgnoreFallbackEvents;}
    function getMessageRetainStatus(){return PifData.RetainSharedEmailMessages;}
    function getFontList() {return PifData.FontList;}
    function getMessageBodyNeeded(){return PifData.MessageBodyNeeded}
    function getAirFrameTailNumber(){return PifData.AirframeTailNumber;}
    function __getPifDataRef(){return PifData;}
    function __getIntWaitTimeBeforeReset(){return PifData.IntWaitBeforeReset;}
    function getMediaPlayerForViMode(){return PifData.viModeMediaPlayer;}
    function getVhsCraddleState(){return PifData.VHSCradleState;}
    function getPdiDataUpdateCount(){return PifData.pdiDataUpdateCount;}
    function getLiveTvSystemOnStatus(){return PifData.LiveTvSystemOn;}
    function getPaxInfoDisable(){return PifData.PaxInfoDisable;}
    function getActiveNbdGroups(){
        var streamerGroups=PifData.groupStates;
        var active =[];
        for (var key in streamerGroups){
            if(streamerGroups[key]==1){active.push(key)}
        }
        return active;
    }
    function getHandsetAvailable(){return PifData.HandsetAvailable}

    function enablePaxusService(){PifData.PaxusServiceStatus = true;return true;}
    function disablePaxusService(){PifData.PaxusServiceStatus = false;return true;}
    function setExternalAppActive(val){ PifData.ExternalAppActive=(val==true)?true:false; externalAppActiveChanged(PifData.ExternalAppActive); return true;}
    function setPointerMode () {
        core.info ("pifData.qml | Set Mouse Mode called.");
        navInput.dpadMode = "mouse"; navInput.navigationMode = "mouse"; navInput.inputMode = cINPUT_MOUSE;
        if (!core.pc) environment.cursor = Qt.ArrowCursor;
    }

    function setKeyMode () {
        core.info ("pifData.qml | Set Key Mode called.");
        navInput.dpadMode = "key"; navInput.navigationMode = "key"; navInput.inputMode = cINPUT_KEY;
        if (!core.pc) environment.cursor = Qt.BlankCursor;
    }

    function handsetDisplay(displayString) {
        return standardHandsetDisplay (displayString);
    }

    function standardHandsetDisplay (displayString) {
        core.info ("pifdata.qml | Handset Display called. Display string: "+displayString);
        if(!(PifData.handset == cSTD_HANDSET || PifData.handset == cDPCU_HANDSET || PifData.handset == c3K_HANDSET )) {core.error("pifdata.qml | Standard Handset or PCU handset or 3KI Handset is not connected. Exiting..."); return false;}
        if(displayString==""||displayString==undefined) {core.error("pifdata.qml | Empty Display String. Exiting..."); return false;}
        navInput.displayText = displayString; return true;
    }

    function enableKeyForwarding(){navInput.forwarding=true;return true;}

    //We need this only in Android OS for enabling premium handset buttons.
    function setButtonInput(enable){
        core.info ("pifdata.qml | Set Button Input. enable: "+enable);
        if(enable === undefined || enable === ""){core.error("Invalid param. Exiting.");return false;}
        if(pif.android)navInput.buttonInputEnabled=enable;
    }

    /* Use this for setting button mapping orientation,currently used for 3ki handsets only whose orientation needs to be set to landscape */
    function setButtonMapOrientation(type){
        if(type==cLANDSCAPE)
            navInput.orientationMode="landscape";
        else if(type==cPORTRAIT)
            navInput.orientationMode="portrait";
        else
            navInput.orientationMode="";   /*(none, uses core's default value */
    }


    function __enableIFEKeys(){
        var IFEKeys= __getIFEKeys();if(IFEKeys.length==0) return false;
        core.info ("pifdata.qml | __enableIFEKeys called");navInput.buttonInputEnabled=true;
    }
    function __disableIFEKeys(){
        var IFEKeys= __getIFEKeys();if(IFEKeys.length==0) return false;
        core.info ("pifdata.qml | __disableIFEKeys called");navInput.buttonInputEnabled=false;
    }

    function __createIFEKeyList(){
        PifData.IFEKeysMap={
            "IFE_KEY_CENTER_TOP": 36,					// Home Key
            "IFE_KEY_CENTER_BOTTOM": 152,				// INFO Key
            "IFE_KEY_REWIND": 241,				// REWIND Key
            "IFE_KEY_SKIP_NEXT": 168,			// SKIP NEXT Key
            "IFE_KEY_PLAY": 164,				// PLAY Key
            "IFE_KEY_FORWARD": 242,				// FORWARD Key
            "IFE_KEY_SKIP_PREVIOUS": 159,        // SKIP PREVIOUS key
            "IFE_KEY_STOP": 166,             	// STOP
            "IFE_KEY_ENTER": 96,              	// ENTER(IFE)
            "IFE_KEY_LEFT_PAD": 1,               // LEFT PAD
            "IFE_KEY_RIGHT_PAD": 2,              // RIGHT PAD
            "IFE_KEY_START": 13,              	// START
            "IFE_KEY_SELECT": 32,              	// SELECT
            "IFE_KEY_LEFT_D_PAD": 37,            // LEFT D-PAD
            "IFE_KEY_UP_D_PAD": 38,              // UP D-PAD
            "IFE_KEY_RIGHT_D_PAD": 39,           // RIGHT D-PAD
            "IFE_KEY_DOWN_D_PAD": 40,            // DOWN D-PAD
            "IFE_KEY_BUTTON_A": 120,             // BUTTON-A (RED)
            "IFE_KEY_BUTTON_X": 121,             // BUTTON-X (BLUE)
            "IFE_KEY_BUTTON_Y": 27,              // BUTTON-Y (GREEN)
            "IFE_KEY_BUTTON_B": 9,               // BUTTON-B (YELLOW)
            "IFE_KEY_CHUP": 129,               	// CHANNEL UP
            "IFE_KEY_CHDOWN": 41,               // CHANNEL DOWN
            "IFE_KEY_SPECIAL_KEY1": 243,        // Special Key 1
            "IFE_KEY_SPECIAL_KEY2": 240        // Special Key 2
        };
        PifData.IFEKeyList=[];
        if(PifData.IFEKeys.length){
            for (var i=0;i<PifData.IFEKeys.length;i++)
                if (PifData.IFEKeysMap[PifData.IFEKeys[i][0]]) PifData.IFEKeyList[PifData.IFEKeysMap[PifData.IFEKeys[i][0]]]=PifData.IFEKeys[i][1];
        }
    }

    function setBacklightOn() { displayID.backlightLevel=1; }
    function setBacklightOff() { if (displayID.backlightLevel==1) displayID.backlightLevel=0; }
    function enableAccessibilityMode(){
        core.info("Pifdata.qml | enableAccessibility");
        seatServices.accessibility=true;
        return true;
    }
    function disableAccessibilityMode(){
        core.info("Pifdata.qml | enableAccessibility");
        seatServices.accessibility=false;
        return true;
    }
    function getAccessibilityMode(){return seatServices.accessibility;}

    function setVolumeByStep(step){     // step => 1 or 2... to increase volume, -1 or -2... to decrease volume, 0 or blank: no change to volume
        core.info("pifdata.qml | Set Volume by step called. Step:"+step);
        var minVol = (PifData.PaState==0)?0:PifData.MinPAVolume;
        var volumevalue=volumeID.volume+PifData.VolumeStep*(String(parseInt(step,10))!="NaN"?parseInt(step,10):minVol);
        volumeID.volume=volumevalue<minVol?minVol:volumevalue>100?100:volumevalue;
        core.info("pifdata.qml | Volume level set: "+volumeID.volume);return true;
    }

    function setVolumeByValue(volumevalue){       // value => between 0 to 100.
        core.info("pifdata.qml | Set Volume by value called");
        var minVol = (PifData.PaState==0)?0:PifData.MinPAVolume;
        volumeID.volume=!volumevalue||volumevalue<minVol?minVol:volumevalue>100?100:volumevalue;
        core.info("pifdata.qml | Volume set: "+volumeID.volume);return true;
    }

    function toggleMuteState(){
        core.info("pifdata.qml | Toggle Mute State called. Current mute state: "+muteState);
        volumeID.muted=!volumeID.muted;
        muteState = !muteState;
        sigToggleMuteState(muteState);
        return true;
    }

    function getMuteState(){return muteState;}

    function frmwkInitApp() {
        core.info("pifData.qml | frmwkInitApp");
        PifData.resetVolumeLevels();
        PifData.VolumeSource=-1;
        setVolumeSource(cAOD_VOLUME_SRC);
        if(displayID.brightnessLevel!=PifData.BrightnessDefault){
            PifData.BrightnessLevel=-1;
            setBrightnessByValue(PifData.BrightnessDefault);}
        PifData.ContrastLevel=-1;
        setContrastByValue(PifData.ContrastDefault);
        __createIFEKeyList();
        setAutoRotation(true);
    }

    function frmwkEntOff(){
        core.info("pifData.qml | frmwkEntOff");
        PifData.MidArr=[];
    }

    function frmwkCloseFlight(){
        core.info("pifData.qml | frmwkCloseFlight");
        PifData.MidArr=[];
    }

    function __clearMidArrForcedAV(){
        core.info("pifData.qml | clearMidArrForcedAV");
        PifData.MidArr=[];
    }

    function __volumeChangeAck() {
        PifData.Volume=volumeID.volume;
        core.info("pifData.qml | __volumeChangeAck: VolumeSource: "+PifData.VolumeSource+" Volume: "+PifData.Volume+" volumeID.volume: "+volumeID.volume);
        if (volumeID.volumeUnchanged) volumeID.volumeUnchanged=false;
        else if (PifData.PaState==0) volumeChanged(PifData.Volume,PifData.VolumeSource);
        else paVolumeChanged(PifData.Volume);
        return true;
    }

    function setVolumeSource(newsrc) {
        if (newsrc==PifData.VolumeSource || PifData.VolumeSourceStringArr[newsrc]==undefined) return false;
        core.info("pifData.qml | setVolumeSource: newsrc:"+newsrc+" VolumeSource: "+PifData.VolumeSource+" Volume: "+PifData.Volume);
        if (PifData.VolumeSource!=-1) PifData.VolumeArr[PifData.VolumeSource]=PifData.Volume;
        PifData.VolumeSource=newsrc;
        core.debug("pifData.qml | setVolumeSource: PifData.VolumeArr[newsrc]: "+PifData.VolumeArr[newsrc]);
        if (PifData.Volume!=PifData.VolumeArr[newsrc]) volumeID.volumeUnchanged=true;
        else core.info("pifData.qml | __setVolumeSource: VolumeSource: "+PifData.VolumeSource+" Volume: "+PifData.VolumeArr[newsrc]);

        if(newsrc!=cPA_VOLUME_SRC && newsrc!=volumeID.volumeSrcBeforePA){
            mixerID.removeChannel(volumeID);/* Avoided setting source on and after PA as per Devtest comments */
        volumeID.type=PifData.VolumeSourceStringArr[newsrc];
        mixerID.addChannel(volumeID);
            volumeID.volume=PifData.VolumeArr[newsrc];
            if (PifData.VolumeSource!=-1) volumeSourceChanged(PifData.VolumeArr[newsrc],PifData.VolumeSource);
        }
        else {
            core.info("pifData.qml | setVolumeSource | PA Change Detected not setting volume ");
        }
        return true;
    }

    function setBrightnessByStep(step){    // brightlevel => 1 or 2... to increase brightness, -1 or -2... to decrease brightness, 0 or blank: no change
        core.info("pifdata.qml | Set Brightness by step called. Step:"+step);
        var brightvalue=displayID.brightnessLevel+PifData.BrightnessStep*(String(parseInt(step,10))!="NaN"?parseInt(step,10):0);
        displayID.brightnessLevel=brightvalue<0?0:brightvalue>100?100:brightvalue;
        if(core.pc || pif.android) __brightnessTriggered(displayID.brightnessLevel);
        core.info("pifdata.qml | Brightness level set: "+displayID.brightnessLevel);
        __preBacklightOffState=false;
        return true;
    }

    function setBrightnessByValue(brightvalue){     // brightvalue => between 0 to 100.
        core.info("pifdata.qml | Set Brightness by value called");
        displayID.brightnessLevel=!brightvalue||brightvalue<0?0:brightvalue>100?100:brightvalue;
        core.info("pifdata.qml | Brightness value set: "+displayID.brightnessLevel);
        if (core.pc || (brightvalue==displayID.brightnessLevel && PifData.BrightnessLevel==-1)) {
            __brightnessTriggered(displayID.brightnessLevel);
	    eventForDefaultBrightness=true;
        }
        __preBacklightOffState=false;
        return true;
    }

    function setBrightnessForScreenSaver(brightvalue){
        core.info("pifdata.qml | Set Brightness by value for screensaver called | brightvalue: "+brightvalue);
        PifData.BrightnessLevel=-1;
        if(brightvalue==-1 || brightvalue==0){
            brightvalue=getDefaultBrightness();
            core.info("pifdata.qml | setBrightnessForScreenSaver | improper value received, assigning default brightness value... "+brightvalue);
        }
        setBrightnessByValue(brightvalue);
    }

    function setContrastByStep(step){   // contrastlevel => 1 or 2... to increase Contrast, -1 or -2... to decrease Contrast, 0 or blank: no change
        core.info("pifdata.qml | Set Contrast by step called. Step:"+step);
        var contrastvalue=displayID.contrastLevel+PifData.ContrastStep*(String(parseInt(step,10))!="NaN"?parseInt(step,10):0);
        displayID.contrastLevel=contrastvalue<0?0:contrastvalue>100?100:contrastvalue;
        core.info("pifdata.qml | Contrast level set: "+displayID.contrastLevel);
        if (core.pc) __contrastTriggered(displayID.contrastLevel);
        return true;
    }

    function setContrastByValue(contrastvalue){     // contrastvalue => between 0 to 100.
        core.info("pifdata.qml | Set Contrast by value called");
        displayID.contrastLevel=!contrastvalue||contrastvalue<0?0:contrastvalue>100?100:contrastvalue;
        core.info("pifdata.qml | Contrast value set: "+displayID.contrastLevel);
        if (core.pc || (contrastvalue==displayID.contrastLevel && PifData.ContrastLevel==-1)) __contrastTriggered(displayID.contrastLevel);
        return true;
    }

    function setReadingLight(updatedvalue){
        if (updatedvalue == undefined) {core.info("pifdata.qml | setPSSReadingLight | New reading light value not provided, exiting..."); return false;}
        core.info("pifdata.qml | setPSSReadingLight | New reading light value: " + updatedvalue);
        seatServices.readingLight = updatedvalue;
        if(core.pc){PifData.ReadingLight = updatedvalue; readingLightChanged(PifData.ReadingLight);}
        return true;
    }

    function setAttendantCall(updatedvalue){
        if (updatedvalue == undefined) {core.info("pifdata.qml | setAttendantCall | New attendant call value not provided, exiting..."); return false;}
        core.info("pifdata.qml | setAttendantCall | New attendant call value: " + updatedvalue);
        seatServices.attendantCall = updatedvalue;
        if(core.pc){PifData.AttendantCall = updatedvalue; attendantCallChanged(PifData.AttendantCall);}
        return true;
    }

    function executePowerDownEvent(){
        core.info("EventReceiver.qml | executePowerDown called");
        __resetLastSelectedMedia()
        if(pif.launchApp.launchInProgress)pif.launchApp.closeApp();
        if(pif.textToSpeech){if(pif.textToSpeech.__mediaState!=pif.textToSpeech.cTTS_STOP)pif.textToSpeech.stopSpeaking();}
        if(pif.visuallyImpaired){if(pif.visuallyImpaired.__mediaState!=pif.visuallyImpaired.cVI_STOP)pif.visuallyImpaired.stopLocalAudioFile();}
        if(pif.screenSaver.__screenSaverState)pif.screenSaver.stopScreenSaver();
        if(pif.ppv){if(pif.ppv.__ppvStatus==1 || pif.ppv.__ppvStatus==2)pif.ppv.cancelPaymentProcess();}
        if(pif.telephony){if(pif.telephony.callInProgress)pif.telephony.endCall();}
    }
    function __setInternalSeatRating(e){__seatRatingChangeAck(e);return true;}

    function setSeatRating(rating){
        if (rating == undefined) {core.info("pifdata.qml | setSeatRating | New seat rating value not provided, exiting..."); return false;}
        core.info("pifdata.qml | setSeatRating | New seat rating: " + rating);
        seatServices.seatRating = rating;
        if(core.pc) __setInternalSeatRating(rating);
        return true;
    }
    function setVideoSpeeds(speedStr){
        core.info("PifData.qml | setVideoSpeeds | speedStr :"+speedStr);
        if(speedStr=="" || speedStr==undefined){core.error("PifData.qml | setVideoSpeeds | speedStr not Provided, Exiting");return false};
        pif.vod.__setVideoSpeeds(speedStr);
    }

    function getTotalTracksInAllPlaylists(){
        var allPlaylistTrackCount=0;
        if(pif.aodPlaylist){allPlaylistTrackCount+=pif.aodPlaylist.getAllPlaylistTotalTracks();}
        if(pif.vodPlaylist){allPlaylistTrackCount+=pif.vodPlaylist.getAllPlaylistTotalTracks();}
        if(pif.kidsAodPlaylist){allPlaylistTrackCount+=pif.kidsAodPlaylist.getAllPlaylistTotalTracks();}
        if(pif.kidsVodPlaylist){allPlaylistTrackCount+=pif.kidsVodPlaylist.getAllPlaylistTotalTracks();}
        if(pif.usbPlaylist){allPlaylistTrackCount+=pif.usbPlaylist.getAllPlaylistTotalTracks();}
        core.debug("Pif.qml | getTotalTracksInAllPlaylists: "+allPlaylistTrackCount);
        return allPlaylistTrackCount;
    }

    function enablePssAlertState(){core.info("pifdata.qml | enablePssAlertState called"); if(PifData.MonitorType!=cLRU_ECO && PifData.MonitorType!=cLRU_ECOV2 && PifData.MonitorType!=cLRU_ELITE && PifData.MonitorType!=cLRU_ELITEV2) return false; else {seatServices.pssAlert="enable"; return true;}}
    function disablePssAlertState(){if(!PifData.InteractivePssPopup) return false; core.info("pifdata.qml | disablePssAlertState called"); if(PifData.MonitorType!=cLRU_ECO && PifData.MonitorType!=cLRU_ECOV2 && PifData.MonitorType!=cLRU_ELITE && PifData.MonitorType!=cLRU_ELITEV2) return false; else {seatServices.pssAlert="disable"; return true;}}
    function setGcsStatus(gcsStatus) {PifData.GcsInstalled=gcsStatus; PifData.GcsStatus=gcsStatus; gcsStatusChanged(gcsStatus);}
    function setAutoRotation(status) {PifData.AutoRotation=(status || status==undefined)?true:false; if(PifData.AutoRotation && PifData.Orientation!=PifData.SysOrientation) __setOrientation(PifData.SysOrientation); return true;}
    function lockOrientation(arg){ // arg: [cLANDSCAPE]
        if(arg==undefined){core.error("pifData.qml | lockOrientation | argument not provided. EXITING..."); return false;}
        setAutoRotation(false); PifData.LockOrientation=arg; __setOrientation(arg[0]);
    }
    function unlockOrientation(){setAutoRotation(true); __setOrientation(PifData.SysOrientation);}
    function setLcdmInput(val){seatServices.lcdmInputType = val;}
    function isSeatChatOnVkarma() {return (PifData.SeatChatIsOnVkarma && getHandsetType()==pif.cVIDEO_KARMA_HANDSET)?true:false}
    function isActiveSessionsNeeded(){return PifData.ActiveChatUserNeeded}
    // Private functions
    function __getRestrictedWords(){return PifData.RestrictedWords}
    function __getMaxSessionCount(){return PifData.MaxSessionCount}
    function __getDeleteInActiveSessionsStatus() {return PifData.DeleteInActiveSessions}

    function __sessionsOnAcceptInviteStatus(){return PifData.SessionsOnAcceptInvite}
    function __getPipReference(){return PifData.PIPArr;}
    function __getMidArrReference(){return PifData.MidArr;}
    function __getAudioPlayTimerInterval(){return PifData.AudioPlayTimerInterval}
    function __getVideoPlayTimerInterval(){return PifData.VideoPlayTimerInterval}
    function __getVideoSkipTimerInterval(){return PifData.VideoSkipTimerInterval}
    function __getSkipPreviousTimeout(){return PifData.SkipPreviousTimeout}
    function __getRememberAspectRatio(){return PifData.RememberAspectRatio}
    function __getDefaultStretchState(){return PifData.DefaultStretchState}
    function __setConfiguration(cfgData,lruData){return PifData.setConfiguration(cfgData,lruData);}
    function __setSimConfiguration(simData){return PifData.setSimConfiguration(simData);}
    function __getCompList() {return PifData.CompList;}
    function __getFontList() {return PifData.FontList;}
    function __getIFEKeys() {return PifData.IFEKeys;}
    function __setEntOn(e){PifData.EntOn=e;entOnChanged(e);return true;}
    function __setOpenFlight(f){PifData.OpenFlight=f; openFlightChanged(f); return true;}
    function __setallDoors(f){PifData.alldoorsClosed=f; alldoorsClosedChanged(f); return true;}
    function __setPAState(e){PifData.PaState=e; __paStateChangeAck(); paStateChanged(e); return true;}
    function __setIpodState(e){PifData.IpodConnected=e;ipodConnectedChanged(e);return true;}
    function __setWowState(e){PifData.WowState=e;wowStateChanged(e);return true;}
    function __setCapSensorState(e){PifData.CapSensorState=e;capSensorStateChanged(e);return true;}
    function __setOrientation(e){
        if(e<0) return;
        core.info("pifData.qml | PifData.StowOrientation = "+PifData.StowOrientation + " PifData.VHSCradleState = "+PifData.VHSCradleState)

        if(PifData.VHSCradleState==1){
                var stow_orientation=PifData.StowOrientation.toLowerCase();
                core.info("pifData.qml | IN CRADLE Stow Orientation : "+stow_orientation);
                if(stow_orientation == "left") e=0;
                else if(stow_orientation == "right") e=2;
                else if(stow_orientation == "up") e=1;
                else if(stow_orientation == "down")  e=3;
				else {
                    core.info("pifData.qml | Something wrong with LRU settings,entering the default,neither left nor right");
				}
		}

        core.info("pifData.qml | PifData.StowOrientation = "+PifData.StowOrientation + " PifData.VHSCradleState = "+PifData.VHSCradleState)
        core.info("pifData.qml | Orientation set after reading StowOrientation and Cradle values :  " + e);
        PifData.Orientation=e;orientationChanged(e);
        __orientationType=(e==1||e==3)?"l":"p";return true;
    }
    function __setStowedState(e){PifData.Stowed=e;stowedChanged(e);return true;}
    function __setExtvState(e){PifData.ExtvState=e;extvStateChanged(e);return true;}
    function __setCtMsg(e,f){PifData.CtMsg[0]=e;PifData.CtMsg[1]=f;sigCtMessageReceived(e,f);return true;}
    function __setSatcomState(e){PifData.SatComStatus=e;satComStatusChanged(e);return true;}
    function __setCorePssState(){PifData.corePssState=(PifData.MonitorType!=cLRU_ECO && PifData.MonitorType!=cLRU_ECOV2 && PifData.MonitorType!=cLRU_ELITE && PifData.MonitorType!=cLRU_ELITEV2)?false:((PifData.PaState==0) && (PifData.InteractiveOverride==0))?(seatServices.pssAlert=="enable")?true:false:true;return true;}
    function __setInfoMode(e){PifData.InfoMode=e;infoModeChanged(e);return true;}
    function __setInteractiveOverride(e){PifData.InteractiveOverride=e;interactiveOverrideChanged(e);return true;}
    function __setExphoneState(e){PifData.ExphoneServerState=e?true:false;exphoneServerStateChanged(PifData.ExphoneServerState); return true;}
    function __setExphoneNotificationState(e){PifData.ExphoneNotificationState=e?true:false;exphoneNotificationStateChanged(PifData.ExphoneNotificationState); return true;}
    function __setGmtOffsetOfSource(e){PifData.GmtOffsetOfSource=e;gmtOffsetOfSourceChanged(PifData.GmtOffsetOfSource+PifData.DstOfSource); return true;}
    function __setGmtOffsetOfDestination(e){PifData.GmtOffsetOfDestination=e;gmtOffsetOfDestinationChanged(PifData.GmtOffsetOfDestination+PifData.DstOfDestination); return true;}
    function __setVbStartTime(e){PifData.VbStartTime=e; vbStartTimeChanged(PifData.VbStartTime);return true;}
    function __setInteractiveState(e){PifData.InteractiveState=e;interactiveStateChanged(e);return true;}
    function __setGroundSpeed(g){PifData.GroundSpeed=g;groundSpeedChanged(PifData.GroundSpeed); return true;}
    function __setTrueAirSpeed(a){PifData.TrueAirSpeed=a;trueAirSpeedChanged(PifData.TrueAirSpeed); return true;}
    function __setAltitude(l){PifData.Altitude=l;altitudeChanged(PifData.Altitude); return true;}
    function __setMediaDate(d){PifData.MediaDate=d;mediaDateChanged(PifData.MediaDate); return true;}
    function __setRouteId(r){PifData.RouteId=r;routeIdChanged(PifData.RouteId); return true;}
    function __setBlockMid(m){__midAccessChangeAck('blocked',m); return true;}
    function __setUnBlockMid(m){__midAccessChangeAck('unblocked',m); return true;}
    function __setBlockServiceMid(m){__serviceAccessChangeAck('blocked',m); return true;}
    function __setUnBlockServiceMid(m){__serviceAccessChangeAck('unblocked',m); return true;}
    function __setppvMessage(a,b,c){__ppvCtMessageAck(a,b,c); return true;}
    function __setExtvVersion(e){PifData.ExtvVersion=e;extvVersionChanged(PifData.ExtvVersion); return true;}
    function __setCgDataAvaliable(e){PifData.CgDataAvailable=e;cgDataAvailableChanged(PifData.CgDataAvailable); return true;}
    function __setFallbackMode(f){PifData.FallbackMode=f;sigFallbackMode(PifData.FallbackMode); return true;}
    function __setGroupStates(states){console.log("Pifdata.qml | __setGroupStates | test states = "+states);PifData.groupStates=states; sigStreamerGroupStatusChanged(true);return true;}

    function __getDevice(portNum){
        var device;
        if(PifData.IsSeatEco9 && portNum == 1) device = 1;          // USB device.
        else if(PifData.IsSeatEco9 && portNum == 2) device = 2;     // ipod device.
        else if(!PifData.IsSeatEco9 && portNum == 1) device = 1;
        else if(!PifData.IsSeatEco9 && portNum == 2) device = 2;
        core.debug("pifData.qml | Device is " + device);return device;
    }

    function __interactiveOverrideChangeAck(overrideChange){
        __setCorePssState();

        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __interactiveOverrideChangeAck | Queuing override change in eventQueue: "+overrideChange);
            PifData.eventQueue.forceAudioVideoOn = overrideChange;
        }else {
            core.debug("pifData.qml | __interactiveOverrideChangeAck | Interactive Override: "+overrideChange);
            __setInteractiveOverride(overrideChange);
        }
    }

    function __ppvCtMessageAck (mid,midstate,midstring) {
        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __ppvCtMessageAck | Queuing ppv Ct Message in eventQueue: "+mid+" mid string: "+midstring);
            if (!PifData.eventQueue["ppvCtMessage"]) PifData.eventQueue["ppvCtMessage"] = [];
            PifData.eventQueue["ppvCtMessage"].push([mid,midstate,midstring].join(","));
        } else {
            core.debug("pifData.qml | __ppvCtMessageAck | mid: "+mid+" mid string: "+midstring);
            sigPpvStatusChanged(mid,midstate,midstring);
        }
    }

    function __midAccessChangeAck (midstate, mid) {
        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __midAccessChangeAck | Queuing mid access change in eventQueue: "+mid);
            if (!PifData.eventQueue["midAccess"]) PifData.eventQueue["midAccess"] = [];
            PifData.eventQueue["midAccess"][mid]=midstate;
        } else {
            core.debug("pifData.qml | __midAccessChangeAck | mid: "+mid);
            midAccessChanged(midstate,mid);
        }
    }

    function __serviceAccessChangeAck (midstate, mid) {
        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __serviceAccessChangeAck | Queuing service access change in eventQueue: "+mid);
            if (!PifData.eventQueue["serviceAccess"]) PifData.eventQueue["serviceAccess"] = [];
            PifData.eventQueue["serviceAccess"][mid]=midstate;
        } else {
            core.debug("pifData.qml | __serviceAccessChangeAck | mid: "+mid);
            serviceAccessChanged(midstate,mid);
        }
    }

    function __seatRatingChangeAck(rating){
        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __seatRatingChangeAck | Queuing seat rating change in eventQueue: "+rating);
            PifData.eventQueue["seatRating"]=rating;
        } else {
            core.debug("pifData.qml | __seatRatingChangeAck | rating: "+rating);
            PifData.SeatRating = rating;
            seatRatingChanged(rating);
        }
    }

    function __openFlightChangeAck(flightChange){
        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __openFlightChangeAck | Queuing flight change in eventQueue: "+flightChange);
            PifData.eventQueue["openFlight"] = flightChange;
        } else {
            core.debug("pifData.qml | __openFlightChangeAck | OpenFlight: "+flightChange);
            __setOpenFlight(flightChange);
        }
    }

    function __entOnChangeAck(entChange){
        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __entOnChangeAck | Queuing entertainment change in eventQueue: "+entChange);
            PifData.eventQueue.entOn = entChange;
        } else {
            core.debug("pifData.qml | __entOnChangeAck | entertainmentOn: "+entChange);
            __setEntOn(entChange);
        }
    }

    function __allDoorsClosedAck(flightChange){
        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __allDoorsClosedAck | Queuing flight change in eventQueue: "+flightChange);
            PifData.eventQueue["allDoorsClosed"] = flightChange;
        } else {
            core.debug("pifData.qml | __allDoorsClosedAck | allDoorsClosed: "+flightChange);
            __setallDoors(flightChange);
        }
    }

    function __interactiveResetAck() {
        if(PifData.PaState!=0) {
            core.debug("pifData.qml | __interactiveResetAck | Queuing Interactive Reset in eventQueue");
            PifData.eventQueue.interactiveReset = 1;
        } else {
            core.debug("pifData.qml | __interactiveResetAck | Reset Interactive");
            interactiveResetChanged();
            var intResetTime = (pif.__getIntWaitTimeBeforeReset())*1000;
            interactiveResetTimer.interval=intResetTime;
            interactiveResetTimer.restart();
        }
    }

    Timer{id:delayPAVolumeTimer;repeat: false;running: false;interval:200;onTriggered: {
            core.info("pifData.qml | delayPAVolumeTimer | volumeID.volume: "+volumeID.volume+" | PifData.VolumeSource: "+PifData.VolumeSource);
            PifData.Volume=volumeID.volume;
            PifData.VolumeArr[PifData.VolumeSource]=PifData.Volume;
            if (PifData.VolumeSource!=-1) volumeSourceChanged(PifData.VolumeArr[PifData.VolumeSource],PifData.VolumeSource);
        }
    }
    function __paStateChangeAck(){
        __setCorePssState();

        if(PifData.PaState==0){
            if(PifData.eventQueue.interactiveReset!=undefined && PifData.eventQueue.interactiveReset) {__interactiveResetAck(); return true;}
            if(PifData.eventQueue.openFlight!=undefined && !PifData.eventQueue.openFlight && PifData.OpenFlight) __setOpenFlight(PifData.eventQueue.openFlight);
            if(PifData.eventQueue.forceAudioVideoOn!=undefined && PifData.eventQueue.forceAudioVideoOn && !PifData.InteractiveOverride) __setInteractiveOverride(PifData.eventQueue.forceAudioVideoOn);
            if(PifData.eventQueue.entOn!=undefined) __setEntOn(PifData.eventQueue.entOn);
            if(PifData.eventQueue.forceAudioVideoOn!=undefined && !PifData.eventQueue.forceAudioVideoOn && PifData.InteractiveOverride) __setInteractiveOverride(PifData.eventQueue.forceAudioVideoOn);
            if(PifData.eventQueue.openFlight!=undefined && PifData.eventQueue.openFlight && !PifData.OpenFlight) __setOpenFlight(PifData.eventQueue.openFlight);
            if(PifData.eventQueue.allDoorsClosed!=undefined && PifData.eventQueue.allDoorsClosed && !PifData.alldoorsClosed) __setallDoors(PifData.eventQueue.allDoorsClosed);
            // Queue for blocking
            if (!((PifData.eventQueue.openFlight!=undefined && !(PifData.eventQueue.openFlight && PifData.OpenFlight))
               || (PifData.eventQueue.openFlight!=undefined && !PifData.eventQueue.openFlight)
            )) processQueueForBlocking();

            PifData.eventQueue=[];
            if (volumeID.volumeSrcBeforePA!=-1) { setVolumeSource(volumeID.volumeSrcBeforePA); volumeID.volumeSrcBeforePA = -1;delayPAVolumeTimer.restart();}
        } else {
            if (volumeID.volumeSrcBeforePA==-1)volumeID.volumeSrcBeforePA = PifData.VolumeSource;
            setVolumeSource(cPA_VOLUME_SRC);delayPAVolumeTimer.restart();
        }
        if (PifData.ExternalAppActive && pif.launchApp) if (PifData.PaState == 0) pif.launchApp.resumeApp(); else if (PifData.PaState == 2 || PifData.PaState == 1) pif.launchApp.pauseApp();
    }

    function processQueueForBlocking() {
        var ptr; var i;
        if (PifData.eventQueue.ppvCtMessage) {  // ppv
            var sptr;
            ptr = PifData.eventQueue.ppvCtMessage;
            for (i in ptr) {
                sptr = ptr[i].split(",");
                core.debug("pifData.qml | __paStateChangeAck | PPV Message | mid: "+sptr[0]+" mid state: "+sptr[1]+" ppv state: "+sptr[2]);
                sigPpvStatusChanged(sptr[0],sptr[1],sptr[2]);
            }
        }
        if (PifData.eventQueue.midAccess) { // mid acess
            ptr = PifData.eventQueue.midAccess;
            for (i in ptr) {core.debug("pifData.qml | __paStateChangeAck | Mid Access | mid: "+i+" mid state: "+ptr[i]); midAccessChanged(ptr[i],i)}
        }
        if (PifData.eventQueue.serviceAccess) { // service access
            ptr = PifData.eventQueue.serviceAccess;
            for (i in ptr) {core.debug("pifData.qml | __paStateChangeAck | Service Access | mid: "+i+" service state: "+ptr[i]); serviceAccessChanged(ptr[i],i)}
        }
        // seat rating
        if (PifData.eventQueue.seatRating!=undefined) {PifData.SeatRating = PifData.eventQueue.seatRating; core.debug("pifData.qml | __paStateChangeAck | rating: "+PifData.SeatRating); seatRatingChanged(PifData.SeatRating);}
    }

    function __interactiveReset(){
        core.info("pifData.qml | __interactiveReset called");
        //interactiveResetChanged();
        enablePssAlertState();
        if (!getBacklightState()) setBacklightOn();
        pif.__resetLastSelectedMedia();
        if (pif.launchApp) pif.launchApp.closeApp();
        setBacklightOff();
        if(pif.android) pif.android.__interactiveResetAndroid()
        else if(core.coreHelper.fileExists("/etc/init.d/099_seatAppBase.sh")) pif.launchApp.launchApp("/etc/init.d/099_seatAppBase.sh",1,"INT_RESTART","restart",false);
        else pif.launchApp.launchApp("/etc/init.d/99_seatAppBase.sh",1,"INT_RESTART","restart",false);
    }

    function restartInteractive(){
        __interactiveReset();
    }

    function __formatNegativeValues(n){
        if(n != undefined && n.length > 1){
            var mult = (n[0] == "8") ? -1 : 1;
            return parseFloat((n.substring(1) * mult),10);
        } else return parseFloat(n,10);
    }

    function __adjustGmtMinutes(n){
        if(!n) return 0;
        var mins = String(n).split('.');
        if (mins[1]==undefined || parseInt(mins[1],10)==0) return parseFloat(n,10);
        else mins[1]=60*parseFloat("0."+mins[1]);
        return parseFloat(mins.join('.'),10);
    }

    function __brightnessTriggered(level){
        var tmpBrightness = PifData.BrightnessLevel;
        PifData.BrightnessLevel = level;
        if (tmpBrightness!=-1) brightnessChanged(level);
    }

    function __contrastTriggered(level){
        var tmpContrast = PifData.ContrastLevel;
        PifData.ContrastLevel = level;
        if (tmpContrast!=-1) contrastChanged(level);
    }

    function playPAS(deviceName){
        if (!PifData.PASDeviceList[deviceName]) {core.info("pifData.qml | playPAS | Device not found: "+deviceName); return false;}
        core.info("pifData.qml | playPAS | Device found: "+deviceName);
        var ptr = PifData.PASDeviceList[deviceName];
        volumeID.getPasInfoCount();     // not sure of it's use here
        if (!volumeID.playingDeviceName) {
            volumeID.volumeBeforePAS = volumeID.volume;
            volumeID.typeBeforePAS = volumeID.type;
        }
        volumeID.playingDeviceName = deviceName;
        volumeID.type=ptr.type;
        volumeID.volume = ptr.volume;
        if(!core.pc){volumeID.pasPlay (ptr.ip, ptr.port);}
    }

    function stopPAS(){
        if (!volumeID.playingDeviceName) return false;
        core.info("pifData.qml | stopPAS | Stopping device: "+volumeID.playingDeviceName+" | Current volume:"+volumeID.volume+"  Restoring Type: "+volumeID.typeBeforePAS+" Restoring Volume: "+volumeID.volumeBeforePAS);
        var ptr = PifData.PASDeviceList[volumeID.playingDeviceName];
        ptr.volume = volumeID.volume
        volumeID.type = volumeID.typeBeforePAS;
        volumeID.volume = volumeID.volumeBeforePAS;
        volumeID.playingDeviceName = "";
        if(pif.lastSelectedAudio.getMediaState()==pif.aod.cMEDIA_PLAY)pif.lastSelectedAudio.restart();
        else if(pif.lastSelectedVideo.getMediaState()==pif.vod.cMEDIA_PLAY)pif.lastSelectedVideo.restart();
        return true;
    }
	
    function filePlay(fileSource){
        core.log("pifData.qml | filePlay | fileSource: "+fileSource);
        if(fileSource=="" || !fileSource){core.log("pifData.qml | filePlay | fileSource can not be blank"); return false;}
        mixerID.play(fileSource);
        return true;
    }

    function loadTransientData(){
        core.info("PifData.qml |loadTransientData | Loading Transient Data Simulation ")
        pif.transientDataSimulation=core.coreHelper.loadDynamicComponent(core.settings.intPath+"framework/testpanel/TransientDataSimulation.qml",pif);
    }
    Component.onCompleted:{
        if (core.pc) eventsTimer.restart();
        PifData.eventQueue=[];
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOnScreen,[disablePssAlertState]);
        interactiveStartup.registerToStage(interactiveStartup.cStageEntOffScreen,[disablePssAlertState]);
        interactiveStartup.registerToStage(interactiveStartup.cStageResetStates,[enablePssAlertState]);
        if (core.simulation)interactiveStartup.registerToStage(interactiveStartup.cStageLoadDynamicComponents,[loadTransientData]);
        var filename=(core.pc)?"/tmp/seatappbase":"/etc/sysconfig/plugins/seatappbase"
        var pluginData=coreHelper.getConfigFileData(filename,"sh");
        PifData.setMonitorTypeScript(pluginData.MONITOR_TYPE);
        PifData.runGamePath=(pluginData.RUN_GAME)?pluginData.RUN_GAME:PifData.runGamePath;
        if(pluginData.ANDROID=="true") pif.android=core.coreHelper.loadDynamicComponent(core.settings.intPath+"framework/pif/Android.qml",pif);
        core.info("pifData.qml | pif.android: "+pif.android);
    }
    System { id: environment; Component.onCompleted: {}}
    // Seat Services
    SeatServices {id:seatServices;
        onSeatRatingChanged:core.info("pifData.qml | Seat rating changed called, new value: "+seatRating);
        onReadingLightChanged:core.info("pifData.qml | Reading light changed called, new value: "+readingLight);
        onAttendantCallChanged:core.info("pifData.qml | Attendant call changed called, new value: "+attendantCall);
        onPssAlertChanged:{core.info("pifData.qml | PSS alert changed called, new value: "+pssAlert); __setCorePssState();}
        onAccessibilityChanged:{core.info("pifData.qml | Accessibility changed called, new value: "+accessibility); accessibilityModeChanged(accessibility)}
    }    
    AudioMixer{
        id:mixerID;
        deviceManager:deviceMngr;
    }

    Connections{
        target:mixerID //since not all qtengine are compitable with below signals
        onPlayed: {core.log("pifData.qml | AudioMixer | onPlayed called. | result: "+result);pifData.audioMixerPlay(result)}
        onEos: {core.log("pifData.qml | AudioMixer | onEos called.");pifData.audioMixerEOS()}
    }

    AudioChannel{id:volumeID;audioMixer:mixerID;type:"mp3";volume:20;muted:false;
        property bool volumeUnchanged:false;
        property int volumeSrcBeforePA:-1;
        property int volumeBeforePAS:0;
        property string typeBeforePAS:"";
        property string playingDeviceName:"";
        onVolumeChanged: {core.info("pifData.qml | On volume changed, volume level: "+volumeID.volume); __volumeChangeAck();}
    }
    DisplayController{ id: displayID;deviceManager:deviceMngr;brightnessLevel:50;backlightLevel:1;contrastLevel:50;
        onBrightnessLevelChanged: core.info("pifdata.qml | On Brightness level changed called, brightness level: "+brightnessLevel);   // not used because event does not come thru when update of brightness happens from core. It only comes when interactive updates brightness.
        onBacklightLevelChanged: {core.info("pifdata.qml | On Backlight level changed called, backlight level: "+backlightLevel); if(backlightLevel==0) __preBacklightOffState=true;}
        onContrastLevelChanged: core.info("pifdata.qml | On Contrast level changed called, contrast level: "+contrastLevel);
        onHueLevelChanged: core.info("pifdata.qml | On Hue level changed called, hue level: "+hueLevel);
    }
    function postKeyPressEvent(keyCode,nativeScanCode,modifiers,text){
        core.info("pifData.qml | postKeyPressEvent | keyCode: "+keyCode+" nativeScanCode: "+nativeScanCode+" modifiers: "+modifiers+" text: "+text);
        modifiers=(modifiers)?modifiers:Qt.NoModifier; text=(text)?text:"";
        navInput.postQKeyEvent(6,keyCode,nativeScanCode,modifiers,text);
    }
    function postKeyReleaseEvent(keyCode,nativeScanCode,modifiers,text){
        core.info("pifData.qml | postKeyReleaseEvent | keyCode: "+keyCode+" nativeScanCode: "+nativeScanCode+" modifiers: "+modifiers+" text: "+text);
        modifiers=(modifiers)?modifiers:Qt.NoModifier; text=(text)?text:"";
        navInput.postQKeyEvent(7,keyCode,nativeScanCode,modifiers,text);
    }
    PifInput{
        id:navInput;deviceManager:deviceMngr;dpadMode:"key";navigationMode:"key";keyMap:"unix";volumeKeysEnabled:false;property int inputMode:0;
        onKeyEventPosted: {
            if(PifData.IFEKeyList[event.nativeScanCode]){
                if(event.type==6){    // Key Press
                    core.info("pifData.qml | onKeyEventPosted | IFEKeyPress: "+event.nativeScanCode+" Text: "+event.text);
                    var keyList = PifData.IFEKeyList[event.nativeScanCode].split(",");
                    for(var j in keyList) viewController.nativeKeyPressed({"nativeScanCode": parseInt(keyList[j],10)});
                }else if(event.type==7){ // Key Release
                    core.info("pifData.qml | onKeyEventPosted | IFEKeyRelease: "+event.nativeScanCode+" Text: "+event.text);
                    var keyList = PifData.IFEKeyList[event.nativeScanCode].split(",");
                    for(var j in keyList) viewController.nativeKeyReleased({"nativeScanCode": parseInt(keyList[j],10)});
                }else core.info("pifData.qml | onKeyEventPosted | Unknown IFEKeyType: "+event.nativeScanCode+" Text: "+event.text);
            }
        }
    }

    function __checkEventsSynced(){
        core.info("PifData.qml | __checkEventsSynced() called");
        if(__eventsSynced==0) __eventsSynced=1;
        else if(__eventsSynced==1){__eventsSynced=2;eventsTimer.restart();}
    }

    function __processAltitude(alt){
        core.debug("PifData.qml | __processAltitude "+alt);
        if(alt==''){
            PifData.Altitude=0;
        } else{
            var altCheck = alt.substring(0,5);
            if(parseInt(altCheck,10)=="80000"){
                PifData.Altitude = -1*(parseInt(alt.substring(5,8),10));
            } else{
                PifData.Altitude = parseInt(alt,10);
            }
        }
        core.info("PifData.qml | Altitude: " + PifData.Altitude)
        altitudeChanged(PifData.Altitude)
    }


    function eventFlightData(eventName,eventParams,ignoreSimData){
        core.info("pifData.qml | eventFlightData() | Received Event Name: "+eventName+" Paramaters: "+eventParams);		// Semicolon separated list of parameter values.
        if(eventName == "DEST_CITY_TEMP"){
            PifData.DestinationCityTemp=eventParams[0];
            core.info ("PifData.qml | Destination City Temprature: " + PifData.DestinationCityTemp);
            destinationCityTempChanged(PifData.DestinationCityTemp);
        }else if(eventName == "FLTDATA_GROUND_SPEED"){
            PifData.GroundSpeed = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("PifData.qml | GroundSpeed: "+PifData.GroundSpeed);
            groundSpeedChanged(PifData.GroundSpeed);
        }else if(eventName == "FLTDATA_TIME_TO_DESTINATION"){
            if (PifData.SimData.TimeToDestination!=undefined && ignoreSimData==true) return;
            PifData.TimeToDestination = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("PifData.qml | Time To Destination: "+PifData.TimeToDestination);
            timeToDestinationChanged(PifData.TimeToDestination);
        }else if(eventName == "FLTDATA_TIME_AT_DESTINATION"){
            if(PifData.SimData.TimeAtDestination!=undefined && ignoreSimData==true) return;
            PifData.TimeAtDestination = (eventParams[0]=='' || isNaN(eventParams[0]))?"0000":eventParams[0]; core.info("PifData.qml | Time At Destination: "+PifData.TimeAtDestination);
            timeAtDestinationChanged(PifData.TimeAtDestination);
        }else if(eventName == "FLTDATA_TIME_SINCE_TAKEOFF"){
            if(PifData.SimData.TimeSinceTakeOff!=undefined && ignoreSimData==true) return;
            PifData.TimeSinceTakeOff = (eventParams[0]=='' || isNaN(eventParams[0]))?"0000":eventParams[0]; core.info("PifData.qml | Time Since Take Off: "+PifData.TimeSinceTakeOff);
            timeSinceTakeOffChanged(PifData.TimeSinceTakeOff);
        }else if(eventName == "FLTDATA_TIME_AT_ORIGIN"){
            PifData.TimeAtOrigin = (eventParams[0]=='' || isNaN(eventParams[0]))?"0000":eventParams[0]; core.info("PifData.qml | Time At Origin: "+PifData.TimeAtOrigin);
            timeAtOriginChanged(PifData.TimeAtOrigin);
        }else if(eventName == "FLTDATA_ESTIMATED_ARRIVAL_TIME"){
            PifData.ArrivalTime = (eventParams[0]=='' || isNaN(eventParams[0]))?"0000":eventParams[0]; core.info("PifData.qml | Arrival Time: "+PifData.ArrivalTime);
            arrivalTimeChanged(PifData.ArrivalTime);
        }else if(eventName == "FLTDATA_TRUE_AIR_SPEED"){
            PifData.TrueAirSpeed = (eventParams[0]=='' || isNaN(eventParams[0]))?0:eventParams[0]; core.info("PifData.qml | True Air Speed: "+PifData.TrueAirSpeed);
            trueAirSpeedChanged(PifData.TrueAirSpeed);
        }else if(eventName == "FLTDATA_DISTANCE_TO_DESTINATION"){
            PifData.DistToDestination = (eventParams[0]=='' || isNaN(eventParams[0]))?0:eventParams[0]; core.info("PifData.qml | Distance To Destination: "+PifData.DistToDestination );
            distToDestinationChanged(PifData.DistToDestination);
        }else if(eventName == "FLTDATA_OUTSIDE_AIR_TEMP"){
            PifData.OutsideAirTemp = String(parseInt(eventParams[0],10))!="NaN"?__formatNegativeValues(eventParams[0]):0; core.info("PifData.qml | Outside Air Temperature: "+PifData.OutsideAirTemp);
            outsideAirTempChanged(PifData.OutsideAirTemp);
        }else if(eventName == "FLTDATA_DISTANCE_FROM_ORIGIN"){
            if(PifData.SimData.DistFromOrigin!=undefined && ignoreSimData==true) return;
            PifData.DistFromOrigin = (eventParams[0]=='' || isNaN(eventParams[0]))?0:eventParams[0]; core.info("PifData.qml | Distance From Origin: "+PifData.DistFromOrigin);
            distFromOriginChanged(PifData.DistFromOrigin);
        }else if(eventName == "FSB_ALL_ZONES"){
            if (PifData.SimData.FastenSeatBelt!=undefined && ignoreSimData==true) return;
            PifData.FastenSeatBelt = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?1:0:0;
            core.info("PifData.qml | FastenSeatBelt: "+PifData.FastenSeatBelt);
            fastenSeatBeltChanged(PifData.FastenSeatBelt);
        }else if(eventName == "FLTDATA_DEPARTURE_BAGGAGE_ID"){
            if(PifData.SimData.SourceIATA!=undefined && ignoreSimData==true) return;
            PifData.SourceIATA = eventParams[0];core.info("PifData.qml | Source IATA: " + PifData.SourceIATA);
            sourceIATAChanged(PifData.SourceIATA);
        }else if(eventName == "FLTDATA_DESTINATION_BAGGAGE_ID"){
            if(PifData.SimData.DestinationIATA!=undefined && ignoreSimData==true) return;
            PifData.DestinationIATA = eventParams[0];core.info("PifData.qml | Destination IATA: " + PifData.DestinationIATA);
            destinationIATAChanged(PifData.DestinationIATA);
        }else if(eventName == "FLTDATA_FLIGHT_NUMBER"){
            if(PifData.SimData.FlightNumber!=undefined && ignoreSimData==true) return;
            PifData.FlightNumber = (eventParams[0]=='' || (eventParams[0]==undefined))?"":eventParams[0];core.info("PifData.qml | FlightNumber: "+PifData.FlightNumber);
            flightNumberChanged(PifData.FlightNumber);
        }else if(eventName == "FLTDATA_DESTINATION_ID"){
            if(PifData.SimData.DestICAOcode!=undefined && ignoreSimData==true) return;
            PifData.DestICAOcode = (eventParams[0]=='' || eventParams[0]==undefined)?"":eventParams[0]; core.info("PifData.qml | DestICAOcode: "+PifData.DestICAOcode);
            destICAOcodeChanged(PifData.DestICAOcode);
        }else if(eventName == "FLTDATA_DEPARTURE_ID"){
            if(PifData.SimData.SourceICAO!=undefined && ignoreSimData==true) return;
            PifData.SourceICAO = eventParams[0];core.info("PifData.qml | Source ICAO: " + PifData.SourceICAO);
            sourceICAOChanged(PifData.SourceICAO);
        }else if(eventName == "FLTDATA_ALTITUDE"){
            var alt = eventParams[0];
            __processAltitude(alt)
        }else if(eventName == "GMT_OFFSET_DESTINATION"){
            if(PifData.SimData.GmtOffsetOfDestination!=undefined && ignoreSimData==true) return;
            PifData.GmtOffsetOfDestination = String(parseInt(eventParams[0],10))!="NaN"?__adjustGmtMinutes(__formatNegativeValues(eventParams[0])):0; core.info("PifData.qml | GMT Offset Of Destination: "+PifData.GmtOffsetOfDestination);
            gmtOffsetOfDestinationChanged(PifData.GmtOffsetOfDestination+PifData.DstOfDestination);
        }else if(eventName == "GMT_OFFSET_DEPARTURE"){
            if(PifData.SimData.GmtOffsetOfSource!=undefined && ignoreSimData==true) return;
            PifData.GmtOffsetOfSource = String(parseInt(eventParams[0],10))!="NaN"?__adjustGmtMinutes(__formatNegativeValues(eventParams[0])):0; core.info("PifData.qml | GMT Offset Of Source: "+PifData.GmtOffsetOfSource);
            gmtOffsetOfSourceChanged(PifData.GmtOffsetOfSource+PifData.DstOfSource);
        }else if(eventName == "DST_RULE_DESTINATION"){
            if(PifData.SimData.GmtOffsetOfDestination!=undefined && ignoreSimData==true) return;
            PifData.DstOfDestination = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("PifData.qml | DST Of Destination: "+PifData.DstOfDestination);
            gmtOffsetOfDestinationChanged(PifData.GmtOffsetOfDestination+PifData.DstOfDestination);
        }else if(eventName == "DST_RULE_DEPARTURE"){
            if(PifData.SimData.GmtOffsetOfSource!=undefined && ignoreSimData==true) return;
            PifData.DstOfSource = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("PifData.qml | DST Of Source: "+PifData.DstOfSource);
            gmtOffsetOfSourceChanged(PifData.GmtOffsetOfSource+PifData.DstOfSource);
        }else core.info("PifData.qml | Event not handled");
        if (!eventListenerFlightData.eventsReceived) { eventListenerFlightData.eventsReceived = true; __checkEventsSynced();}
    }

    function eventData(eventName,eventParams,ignoreSimData){
        core.info("pifData.qml | eventData() | Received Event Name: "+eventName+" Paramaters: "+eventParams);		// Semicolon separated list of parameter values.
        if(eventName == "INFO_MODE_ON"){
            if(PifData.SimData.InfoMode!=undefined && ignoreSimData==true) return;
            //if(pif.android) return; // As Android 4.4 does not show  it's own seatinfo, interactive needs to show it
            PifData.InfoMode = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?true:false:false;
            core.info("PifData.qml | Info Mode: "+PifData.InfoMode);
            infoModeChanged(PifData.InfoMode);
        }else if(eventName == "OPEN_FLIGHT"){
            if(!pif.getFallbackMode()){
            	if (PifData.SimData.OpenFlight!=undefined && ignoreSimData==true) return;
            	__openFlightChangeAck(String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?1:0:1);
            }
        }else if(eventName == "ALL_DOORS_CLOSED"){
            if (PifData.SimData.AllDoorsClosed!=undefined && ignoreSimData==true) return;
            __allDoorsClosedAck(String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?1:0:1);
        }else if(eventName == "ENTERTAINMENT_ON"){
             if(!pif.getFallbackMode()){
            	if (PifData.SimData.EntOn!=undefined && ignoreSimData==true) return;
            	__entOnChangeAck(String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?1:0:1);
             }
        }else if(eventName == "MEDIA_DATE"){
            if (PifData.SimData.MediaDate!=undefined && ignoreSimData==true) return;
            PifData.MediaDate = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("PifData.qml | MediaDate: "+PifData.MediaDate); mediaDateChanged(PifData.MediaDate);
        }else if(eventName == "X2_PA_STATE"){
            if (PifData.SimData.PaState!=undefined && ignoreSimData==true) return;
            PifData.PaState = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("pifData.qml | PA State: "+PifData.PaState); __paStateChangeAck(); paStateChanged(PifData.PaState);
        }else if(eventName == "CMI_FOR_INTERACTIVE_DISPLAY"){
            PifData.CtMsg[0]=eventParams[0];
            PifData.CtMsg[1]=eventParams[1];
            core.info("PifData.qml | ct message type: " + PifData.CtMsg[0]+" message: "+PifData.CtMsg[1]);
            if(PifData.CtMsg[0]==1) sigCtMessageReceived(PifData.CtMsg[0], PifData.CtMsg[1]);   // CT to Seat Message
            else if(PifData.CtMsg[0]==3){   // Survey
                var msg = core.coreHelper.parseXMLQuery(PifData.CtMsg[1],"/msg/string()");
                msg = msg.split("|");
                sigSurveyReceived(msg[2],msg[3]);
            }else if(PifData.CtMsg[0]==5){  // Hospitality/Shopping
                var msg = core.coreHelper.parseXMLQuery(PifData.CtMsg[1],"/msg/string()");
                msg = msg.split("|");
                sigInflightMenuStatusChanged(msg[0].toLowerCase(),msg[1],msg[2],(msg[3]==0)?"blocked":"unblocked",msg[4]);
            }else if(PifData.CtMsg[0]==6){  // Connecting gate
                var msg = core.coreHelper.parseXMLQuery(PifData.CtMsg[1],"/msg/string()");
                msg = msg.split("|");
                sigCgStatusChanged(msg[0].toLowerCase(),msg[1]);
            }else if(PifData.CtMsg[0]==2){  // PPV
                var msg = core.coreHelper.parseXMLQuery(PifData.CtMsg[1],"/msg/string()");
                msg = msg.split("|");
                if (msg[2] == "PPV_CASH"){
                    __ppvCtMessageAck(parseInt(msg[0],10),'paidorfree','PPV_CASH')
                }else if (msg[2] == "PPV_CHARGE"){
                    __ppvCtMessageAck(parseInt(msg[0],10),'paidorfree','PPV_CHARGE')
                }else if (msg[2] == "PPV_COMP"){
                    __ppvCtMessageAck(parseInt(msg[0],10),'paidorfree','PPV_COMP')
                }else if (msg[2] == "PPV_REFUND"){
                    __ppvCtMessageAck(parseInt(msg[0],10),'pay','PPV_REFUND')
                }else if (msg[2] == "PPV_PURCHASE"){
                    __ppvCtMessageAck(parseInt(msg[0],10),'paidorfree','PPV_PURCHASE')
                }
            }else{
                core.info('PifData.qml | Unidentified CT Message.')
            }
        }else if(eventName == "ROUTE_ID"){
            if(PifData.SimData.RouteId!=undefined && ignoreSimData==true) return;
            PifData.RouteId = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("PifData.qml | Route ID: "+PifData.RouteId);
            routeIdChanged(PifData.RouteId);
        }else if(eventName == "CGDUS_NEW_DATA"){
            if(PifData.SimData.CgDataAvailable!=undefined && ignoreSimData==true) return;
            PifData.CgDataAvailable = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0;core.info("PifData.qml | CG Data Available: "+PifData.CgDataAvailable);
            cgDataAvailableChanged(PifData.CgDataAvailable);
        }else if(eventName == "SATCOM_LINK_STATUS"){
            PifData.SatComStatus = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0;core.info("PifData.qml | SAT com Status: "+PifData.SatComStatus);
            satComStatusChanged(PifData.SatComStatus);
        }else if(eventName == "GCS_KU_STATE"){
            if(PifData.SimData.GcsStatus!=undefined && ignoreSimData==true) return;
            PifData.GcsStatus = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?true:false:false; core.info("PifData.qml | GCS Status: "+PifData.GcsStatus);
            gcsStatusChanged ((PifData.GcsInstalled && PifData.GcsStatus)?true:false);
        }else if(eventName == "GCS_EXCONNECT_SERVICE_ON_SM"){
            if(PifData.SimData.GcsStatus!=undefined && ignoreSimData==true) return;
            PifData.GcsInstalled = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?true:false:false; core.info("PifData.qml | GCS Installed: "+PifData.GcsInstalled);
            gcsStatusChanged ((PifData.GcsInstalled && PifData.GcsStatus)?true:false);
        }else if(eventName == "EXP_SERVER_STATUS"){
            if((PifData.UseExphoneInSeat || PifData.SimData.ExphoneServerState!=undefined) && ignoreSimData==true) return;
            var tempExphoneState=PifData.ExphoneServerState;
            PifData.ExphoneStateRaw = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):1; core.info("PifData.qml | ExphoneStateRaw: "+PifData.ExphoneStateRaw);
            PifData.ExphoneServerState=(PifData.ExphoneStateRaw==3)?true:false;
            PifData.ExphoneAvailable=(PifData.ExphoneStateRaw==3 && PifData.ExphoneNotificationRaw==1)?true:false;
            core.info("PifData.qml | ExphoneServerState: "+PifData.ExphoneServerState);
            if(tempExphoneState!=PifData.ExphoneServerState) exphoneServerStateChanged(PifData.ExphoneServerState);
        }else if(eventName == "EXP_NOTIFICATION"){
            if((PifData.UseExphoneInSeat || PifData.SimData.ExphoneNotificationState!=undefined)&& ignoreSimData==true) return;
            var tempExphoneNotificationState=PifData.ExphoneNotificationState;
            PifData.ExphoneNotificationRaw = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):1; core.info("PifData.qml | ExphoneNotificationRaw: "+PifData.ExphoneNotificationRaw);
            PifData.ExphoneNotificationState=(PifData.ExphoneNotificationRaw==0)?true:false;
            PifData.ExphoneAvailable=(PifData.ExphoneStateRaw==3 && PifData.ExphoneNotificationRaw==0)?true:false;
            core.info("PifData.qml | ExphoneNotificationState: "+PifData.ExphoneNotificationState);
            if(tempExphoneNotificationState!=PifData.ExphoneNotificationState) exphoneNotificationStateChanged(PifData.ExphoneNotificationState);
        }else if(eventName == "DELAY_ENTERTAINMENT_OFF"){
            if(PifData.SimData.DelayedEntOff!=undefined && ignoreSimData==true) return;
            PifData.DelayedEntOff = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0;
            core.info("PifData.qml | Delayed Ent. Off: "+PifData.DelayedEntOff);
            delayedEntOffChanged(PifData.DelayedEntOff);
        }else if(eventName == "PSS_ATTENDANT_CALL_CHANGE"){
            if(PifData.SimData.AttendantCall!=undefined && ignoreSimData==true) return;
            PifData.AttendantCall = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?true:false:false;
            core.info("PifData.qml | Attendant Call Change: "+PifData.AttendantCall);
            attendantCallChanged(PifData.AttendantCall);
        }else if(eventName == "PSS_READING_LIGHT_CHANGE"){
            if(PifData.SimData.ReadingLight!=undefined && ignoreSimData==true) return;
            PifData.ReadingLight = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?true:false:false;
            core.info("PifData.qml | Reading Light: "+PifData.ReadingLight);
            readingLightChanged(PifData.ReadingLight);
        }else if(eventName == "SEAT_RATING_CHANGE"){
            var rating = PifData.SeatRating;
            if(eventParams[0]==3) rating = 254;
            else if(eventParams[0]==1) rating = eventParams[1];
            __seatRatingChangeAck(rating);
        }else if(eventName == "INTERACTIVE_STATE"){
            if(PifData.SimData.InteractiveState!=undefined && ignoreSimData==true) return;
            PifData.InteractiveState = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("PifData.qml | Interactive State: "+PifData.InteractiveState);
            interactiveStateChanged(PifData.InteractiveState);
        }else if(eventName == "IPOD_CONNECT"){
            PifData.IpodConnected = true;
            ipodConnectedChanged(PifData.IpodConnected);
        }else if(eventName == "IPOD_DISCONNECT"){
            PifData.IpodConnected = false;
            ipodConnectedChanged(PifData.IpodConnected);
        }else if(eventName == "WEIGHT_ON_WHEELS"){
            PifData.WowState = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?true:false:false; core.info("PifData.qml | Wow State: "+PifData.WowState);
            wowStateChanged(PifData.WowState);
        }else if(eventName == "CAPSENSE_STATE"){
            PifData.CapSensorState = String(parseInt(eventParams[1],10))!="NaN"?eventParams[1]==1?1:0:0; core.info("PifData.qml | CapSensor State: "+PifData.CapSensorState);
            if(PifData.CapSensorState==1)capSensorStateChanged(PifData.CapSensorState);
        }else if(eventName == "EXP_INSEAT_NOTIFICATION_AVAIL"){
            if((!PifData.UseExphoneInSeat || PifData.SimData.ExphoneStatus!=undefined)&& ignoreSimData==true) return;
            PifData.ExphoneStatus = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10)>0?true:false:false;  // Any positive value will mean that it is active according to SWE. -Krishnan.
            core.info("PifData.qml | ExphoneStatus: "+PifData.ExphoneStatus);
            exphoneStatusChanged(PifData.ExphoneStatus);
        }else if(eventName == "REMOTE_CONTROLLED_STATE"){
            __interactiveOverrideChangeAck(String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?1:0:0);
        }else if(eventName == "ORIENTATION_STATE"){
            if(String(parseInt(eventParams[1],10))=="NaN") return;
            core.info ("PifData.qml | Min Orientation: "+eventParams[0]+" Orientation: "+ eventParams[1]+" Max Orientation: "+eventParams[2]);
            if(parseInt(eventParams[1],10)>parseInt(eventParams[2],10)){core.info("PifData.qml | Orientation value is greater than Max Orientation");return;}
            if(parseInt(eventParams[1],10)<parseInt(eventParams[0],10)){core.info("PifData.qml | Orientation value is less than Min Orientation");return;}
            PifData.SysOrientation=parseInt(eventParams[1],10);
            if(!PifData.AutoRotation && PifData.LockOrientation.indexOf(PifData.SysOrientation)==-1) return;
            else __setOrientation(PifData.SysOrientation);
        }        
        else if(eventName == "STOW_SENSOR_CHANGE"){
            if(PifData.SimData.Stowed!=undefined && ignoreSimData==true) return;
            PifData.Stowed = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?true:false:false;
            core.info("PifData.qml | Stowed State: "+PifData.Stowed);
            stowedChanged(PifData.Stowed);
        }else if(eventName == "EXTV_STATE"){
            PifData.ExtvState = eventParams[0]; core.info("PifData.qml | Extv State: "+PifData.ExtvState);
            extvStateChanged(PifData.ExtvState);
        }else if(eventName == "EXTV_CHANNEL_LISTING_VERSION"){
            if(PifData.SimData.ExtvVersion!=undefined && ignoreSimData==true) return;
            PifData.ExtvVersion = eventParams[0]; core.info("PifData.qml | Extv Version: "+PifData.ExtvVersion);
            extvVersionChanged(PifData.ExtvVersion);
        }else if(eventName == "MOUSE_ATTACHED_CHANGE"){
            PifData.DeviceCharging=(PifData.DeviceCharging == 0)?1:(PifData.DeviceCharging == 1)?0:PifData.DeviceCharging;
            core.info("PifData.qml | Device Charging: "+PifData.DeviceCharging);
            deviceChargingChanged(PifData.DeviceCharging);
        }else if(eventName == "VOLUME_CHANGE"){
            var min = eventParams[0];
            var lvl = eventParams[1];
            var max = eventParams[2];
            core.info("PifData.qml | Volume Min: "+min+" Max: "+max+" Level: "+lvl);
        }else if(eventName == "BRIGHTNESS_CHANGE"){
            //if(seatapi.screenSaver.screenSaverType==1 && seatapi.screenSaver._valBeforeDim>0) {seatapi.error ("BRIGHTNESS_CHANGE not listened as called from screensaver dimming"); return; }
            var min = eventParams[0];
            var lvl = eventParams[1];     // Brightness level
            var max = eventParams[2];
            core.info("PifData.qml | Brightness Min: "+min+" Max: "+max+" Level: "+lvl);
            if(pif.android){brightnessChangeTimer.brightnessValue=lvl; brightnessChangeTimer.restart();}
            else if(eventForDefaultBrightness) eventForDefaultBrightness=false;
            else __brightnessTriggered(lvl);
        }else if(eventName == "CONTRAST_CHANGE"){
            var min = eventParams[0];
            var lvl = eventParams[1];     // Contrast level
            var max = eventParams[2];
            core.info("PifData.qml | Contrast Min: "+min+" Max: "+max+" Level: "+lvl);
            __contrastTriggered(lvl)
        }else if(eventName == "INTERACTIVE_RESTART"){
            core.info ("PifData.qml | Interactive restart: " + eventParams[0]);
            __interactiveResetAck();
        }else if (eventName == "LCD_BACKLIGHT_CHANGE") {
            var backlight=parseInt(eventParams[0],10);     // backlight On(1)/Off(0)
            core.info ("PifData.qml | Backlight State: " + backlight);
            backlightStateChanged(backlight);
        }else if(eventName == "MID_SERVICE_BLOCKING_CHANGE"){
            var mid=eventParams[2];
            var blockState=eventParams[1];
            core.info("PifData.qml | MID Service Blocking Change Event: Block "+blockState+" Mid: "+mid);
            if(blockState==2)__serviceAccessChangeAck('blocked',mid);
            else if(blockState==3)__serviceAccessChangeAck('unblocked',mid);
        }else if(eventName == "MID_BLOCKING_CHANGE"){
            var blockid=eventParams[0];
            var blockState=eventParams[1];
            var mid=eventParams[2];
            core.info("PifData.qml | MID Blocking Change Event, blockid: "+blockid+" Block "+blockState+" Mid: "+mid);
            if(blockid==5){
                if(blockState==PifData.MidBlocked){ __midAccessChangeAck('blocked',mid); core.info("PifData.qml | MID Blocking Change Event, mid: "+mid+" is blocked");}
                else if(blockState==PifData.MidUnblocked){ __midAccessChangeAck('unblocked',mid); core.info("PifData.qml | MID Blocking Change Event, mid: "+mid+" is unblocked");}
            }
            else if(blockid==6){ __midAccessChangeAck('default',mid); core.info("PifData.qml | MID Blocking Change Event, restoring mid: "+mid+" access to default");}
        }else if(eventName == "SLIDER_USB_POWER0" || eventName == "SLIDER_USB_POWER1" || eventName == "SLIDER_USB_POWER2"){
            var portNum = parseInt(eventName.charAt(eventName.length - 1),10);
            var status = parseInt(eventParams[1],10);
            var device = __getDevice(portNum);
            if(device == cUSB_DEVICE){PifData.UsbPortStatus = status;usbPortStatusChanged(PifData.UsbPortStatus);}
            else if(device == cIPOD_DEVICE){PifData.IpodPortStatus = status;ipodPortStatusChanged(PifData.IpodPortStatus);}
            core.info("PifData.qml | USB port status: "+PifData.UsbPortStatus + " IPOD port status: " + PifData.IpodPortStatus);
        }else if(eventName == "VB_START_TIME"){
            if(PifData.SimData.VbStartTime!=undefined && ignoreSimData==true) return;
            __setVbStartTime(String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0);
            core.info("PifData.qml | VB Elapsed Time: "+PifData.VbStartTime);
        }else if(eventName == "VB_CYCLE_TIME"){
            if(PifData.SimData.VbCycleTime!=undefined && ignoreSimData==true) return;
            PifData.VbCycleTime = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0; core.info("PifData.qml | VB Cycle Time: "+PifData.VbCycleTime);
        }else if (eventName == "VIDEO_SSRC_ALERT"){
            var mid = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0;
            var duration = String(parseInt(eventParams[1],10))!="NaN"?parseInt(eventParams[1],10):0;
            var adMedia = String(parseInt(eventParams[2],10))!="NaN"?parseInt(eventParams[2],10):0;
            var trickPlay = String(parseInt(eventParams[3],10))!="NaN"?parseInt(eventParams[3],10):0;
            core.info("PifData.qml | Video source Change Event: Mid "+mid+" Duration: "+duration+" AdMedia: "+ adMedia+" TricPlay: "+trickPlay);
            sigVideoSSRC(mid,duration,adMedia,trickPlay);
        }else if (eventName == "USB_STORAGE_DEVICE_UNKNOWN"){
            usbStorageDeviceUnknown();
        }else if (eventName == "USB_MOUNT_FAILED"){
            usbMountFailed();
        }else if (eventName == "GAMEPAD_ATTACHED_CHANGE"){
            core.debug("PifData.qml | GAMEPAD ATTACHED CHANGE EVENT | eventParams[0] :- "+eventParams[0]);
            core.debug("PifData.qml | GAMEPAD ATTACHED CHANGE EVENT | eventParams[1] :- "+eventParams[1]);
            core.debug("PifData.qml | GAMEPAD ATTACHED CHANGE EVENT | eventParams[2] :- "+eventParams[2]);
            core.info ("PifData.qml | GAMEPAD ATTACHED CHANGE EVENT ");
        }else if (eventName == "KEYBOARD_ATTACHED_CHANGE"){
            core.debug("PifData.qml | KEYBOARD ATTACHED CHANGE EVENT | eventParams[0] :- "+eventParams[0]);
            core.debug("PifData.qml | KEYBOARD ATTACHED CHANGE EVENT | eventParams[1] :- "+eventParams[1]);
            core.debug("PifData.qml | KEYBOARD ATTACHED CHANGE EVENT | eventParams[2] :- "+eventParams[2]);
            core.info ("PifData.qml | KEYBOARD ATTACHED CHANGE EVENT ");
        }else if (eventName == "AUDIO_LOST_SEAT_REBOOT" || eventName == "AUDIO_LOST_DSP_REBOOT"){
            audioLostSeatReboot();
        }else if (eventName == "VIDEO_LOST_SEAT_REBOOT" || eventName == "VIDEO_LOST_DSP_REBOOT"){
            videoLostSeatReboot();
        } else if(eventName == "AUDIO_RECOVERED_SEAT_REBOOT" || eventName == "AUDIO_RECOVERED_DSP_REBOOT"){
            audioRecoveredSeatReboot();
        }else if (eventName == "AUDIO_DEVICE_ADDED"){
            audioDeviceAdded();
        }else if (eventName == "AUDIO_DEVICE_REMOVED"){
            audioDeviceRemoved();
        }else if (eventName == "STREAMERS_STATES"){
            core.info("PifData.qml | EVENT:"+eventName+", PifData.UseFallbackStatus:"+PifData.UseFallbackStatus+",PifData.SimData.FallbackMode:"+PifData.SimData.FallbackMode+",getIgnoreFallbackEvents():"+getIgnoreFallbackEvents()+"parseInt(eventParams[0],10):"+parseInt(eventParams[0],10))
            if(PifData.FallbackMode ==1){
                if (JSON.stringify(PifData.groupStates) === JSON.stringify(eventParams.slice(2,eventParams.length))) return;
                else{
            PifData.streamerStates=eventParams;
                    var groupCount = String(parseInt(eventParams[1],10))!="NaN"?parseInt(eventParams[1],10):0;
                    PifData.groupStates = eventParams.slice(2,eventParams.length)
                    core.info("PifData.qml | Group States Change Event: Group Count: "+(String(parseInt(eventParams[1],10))!="NaN"?parseInt(eventParams[1],10):0)+" Group States: "+ PifData.groupStates);
                    sigStreamerGroupStatusChanged(true)
                }
            }else{
            if ((PifData.UseFallbackStatus || PifData.SimData.FallbackMode!=undefined) && ignoreSimData==true) return;
            if(getIgnoreFallbackEvents()) return;
                PifData.streamerStates=eventParams;
            var fallbackMode = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):1;
            var groupCount = String(parseInt(eventParams[1],10))!="NaN"?parseInt(eventParams[1],10):0;
                PifData.groupStates = eventParams.slice(2,eventParams.length);
            PifData.FallbackMode=(fallbackMode==0?1:0)
                core.info("PifData.qml | Fallback State Change Event: Fallback Mode "+ PifData.FallbackMode +" Group Count: "+groupCount+" Group States: "+ PifData.groupStates);
            sigFallbackMode(PifData.FallbackMode);
            }
        }else if (eventName == "FALLBACK_STATUS"){
            core.info("PifData.qml | EVENT:"+eventName+", PifData.UseFallbackStatus:"+PifData.UseFallbackStatus+",PifData.SimData.FallbackMode:"+PifData.SimData.FallbackMode+",getIgnoreFallbackEvents():"+getIgnoreFallbackEvents())
            if((!PifData.UseFallbackStatus || PifData.SimData.FallbackMode!=undefined) && ignoreSimData==true) return;
            if(getIgnoreFallbackEvents()) return;	    
            var fallbackType = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10):0;
            var fallbackState = String(parseInt(eventParams[1],10))!="NaN"?parseInt(eventParams[1],10):0;
            var serversDown = String(parseInt(eventParams[2],10))!="NaN"?parseInt(eventParams[2],10):0;
            PifData.FallbackMode = fallbackState;
            core.info("PifData.qml | Fallback State Change Event: Fallback type: "+fallbackType+" Fallback Mode: "+ PifData.FallbackMode +" no. of servers down: "+serversDown);
            sigFallbackMode(PifData.FallbackMode);
        } else if(eventName == "AIRFRAME_TAIL_NUMBER"){
            if(PifData.SimData.AirframeTailNumber!=undefined && ignoreSimData==true) return;
            PifData.AirframeTailNumber = (eventParams[0]=='' || (eventParams[0]==undefined))?"":eventParams[0]; core.info("PifData.qml | AirframeTailNumber: "+PifData.AirframeTailNumber); airFrameTailNumberChanged(PifData.AirframeTailNumber);
        } else if(eventName == "CBB_LIVE_TV_SYSTEM_ON"){
            if (PifData.SimData.LiveTvSystemOn!=undefined && ignoreSimData==true) return;
            PifData.LiveTvSystemOn = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?1:0:0;
            core.info("PifData.qml | LiveTvSystemOn: "+PifData.LiveTvSystemOn);
            liveTvSystemStatusChanged(PifData.LiveTvSystemOn);
        }else if(eventName == "HANDSET_IN_CRADDLE_CHANGE"){
            if(String(parseInt(eventParams[1],10))=="NaN") return;
            core.info ("PifData.qml | VHS Cradle Min State: "+eventParams[0]+" VHS Cradle State: "+ eventParams[1]+" VHS Cradle Max State: "+eventParams[2]);
            if(parseInt(eventParams[1],10)>parseInt(eventParams[2],10)){core.info("PifData.qml | VHS Cradle State is greater than Max state");return;}
            if(parseInt(eventParams[1],10)<parseInt(eventParams[0],10)){core.info("PifData.qml | VHS Cradle State is less than Min state");return;}
            PifData.VHSCradleState=parseInt(eventParams[1],10);
            if(!PifData.AutoRotation && PifData.LockOrientation.indexOf(PifData.SysOrientation)==-1) return;
            else __setOrientation(getOrientation());
        }else if (eventName == "POWER_DOWN_READY_STATE_1"){
            core.info("PifData.qml | POWER_DOWN_READY_STATE_1 Event Received");
            powerDownEventReceived();
        }else if(eventName == "HANDSET_AVAILABLE_SETTING") {
            PifData.HandsetAvailable = String(parseInt(eventParams[0],10))!="NaN"?eventParams[0]==1?1:0:0
            sigHandsetAvailableChanged(PifData.HandsetAvailable);
        } else if(eventName == "PAX_INFO_DISABLE") {
            if (PifData.SimData.PaxInfoDisable!=undefined && ignoreSimData==true) return;
            PifData.PaxInfoDisable = String(parseInt(eventParams[0],10))!="NaN"?parseInt(eventParams[0],10)>0?true:false:false; core.info("PifData.qml | paxInfoDisable: " + PifData.PaxInfoDisable);
            sigPaxInfoDisable(PifData.PaxInfoDisable);
        }else if(eventName == "IPOD_CHARGE_ONLY_CONNECT") {
            sigIpodChargeConnect();
        }else if(eventName == "IPOD_CHARGE_ONLY_DISCONNECT") {
            sigIpodDischargeConnect();
        }else core.info("PifData.qml | Event not handled");
        if (!eventListener.eventsReceived) { eventListener.eventsReceived = true; __checkEventsSynced();}
    }

    EventListener {
        id: eventListenerFlightData; deviceManager: deviceMngr; property bool eventsReceived: false;
        onEventReceived: {
            core.info("pifData.qml | eventListenerFlightData | Received Event Name: "+event.name+" Paramaters: "+event.paramList);		// Semicolon separated list of parameter values.
            var paramListArray = [];
            for(var i=0;i<event.paramCount;i++){
                paramListArray.push(event.getParamAtIndex(i));
            }
            core.debug("pifData.qml | Params Array for event "+event.name+" : "+paramListArray);
            eventFlightData(event.name,paramListArray,true)
        }
        events: [
            EventData { name: "DEST_CITY_TEMP" },
            EventData { name: "FLTDATA_GROUND_SPEED" },
            EventData { name: "FLTDATA_TIME_TO_DESTINATION" },
            EventData { name :"FLTDATA_TIME_AT_DESTINATION"},
            EventData { name: "FLTDATA_TIME_SINCE_TAKEOFF" },
            EventData { name: "FLTDATA_TIME_AT_ORIGIN" },
            EventData { name: "FLTDATA_ESTIMATED_ARRIVAL_TIME" },
            EventData { name: "FLTDATA_TRUE_AIR_SPEED"},
            EventData { name: "FLTDATA_DISTANCE_TO_DESTINATION"},
            EventData { name: "FLTDATA_OUTSIDE_AIR_TEMP"},
            EventData { name: "FLTDATA_DISTANCE_FROM_ORIGIN"},            
            EventData { name: "FLTDATA_DEPARTURE_BAGGAGE_ID" },
            EventData { name: "FLTDATA_DESTINATION_BAGGAGE_ID" },
            EventData { name: "FLTDATA_FLIGHT_NUMBER" },
            EventData { name: "FLTDATA_DESTINATION_ID" },
            EventData { name: "FLTDATA_DEPARTURE_ID" },
            EventData { name: "FLTDATA_ALTITUDE" },
            EventData { name: "GMT_OFFSET_DESTINATION" },
            EventData { name: "GMT_OFFSET_DEPARTURE" },
            EventData { name: "DST_RULE_DESTINATION" },
            EventData { name: "DST_RULE_DEPARTURE" },
            EventData { name : "FSB_ALL_ZONES"}
        ]
    }

    EventListener {
        id: eventListener; deviceManager: deviceMngr; property bool eventsReceived: false;
        onEventReceived: {
            core.info("pifData.qml | eventListener | Received Event Name: "+event.name+" Paramaters: "+event.paramList);		// Semicolon separated list of parameter values.
            var paramListArray = [];
            for(var i=0;i<event.paramCount;i++){
                paramListArray.push(event.getParamAtIndex(i));
            }
            core.debug("pifData.qml | Params Array for event "+event.name+" : "+paramListArray);
            eventData(event.name,paramListArray,true)
        }
        events: [
            EventData { name: "INFO_MODE_ON" },
            EventData { name: "OPEN_FLIGHT" },
            EventData { name: "ENTERTAINMENT_ON" },
            EventData { name: "MEDIA_DATE" },
            EventData { name: "X2_PA_STATE" },
            EventData { name: "CMI_FOR_INTERACTIVE_DISPLAY" },
            EventData { name: "ROUTE_ID" },
            EventData { name: "CGDUS_NEW_DATA" },
            EventData { name: "DELAY_ENTERTAINMENT_OFF" },
            EventData { name: "PSS_ATTENDANT_CALL_CHANGE" },
            EventData { name: "PSS_READING_LIGHT_CHANGE" },
            EventData { name: "VOLUME_CHANGE" },
            EventData { name: "BRIGHTNESS_CHANGE" },
            EventData { name: "CONTRAST_CHANGE" },
            EventData { name: "LCD_BACKLIGHT_CHANGE" },
            EventData { name: "SEAT_RATING_CHANGE" },
            EventData { name: "INTERACTIVE_STATE" },
            EventData { name: "WEIGHT_ON_WHEELS" },
            EventData { name: "CAPSENSE_STATE" },
            EventData { name: "REMOTE_CONTROLLED_STATE" },
            EventData { name: "ORIENTATION_STATE" },
            EventData { name: "STOW_SENSOR_CHANGE" },            
            EventData { name: "MID_SERVICE_BLOCKING_CHANGE" },
            EventData { name: "MID_BLOCKING_CHANGE" },
            EventData { name: "INTERACTIVE_RESTART" },
            EventData { name: "PPV_CASH"},
            EventData { name: "PPV_CHARGE"},
            EventData { name: "PPV_COMP"},
            EventData { name: "PPV_PURCHASE"},
            EventData { name: "PPV_REFUND"}
        ]
    }

    EventListener {
        id: eventListener2; deviceManager: deviceMngr; property bool eventsReceived: false;
        onEventReceived: {
            core.info("pifData.qml | eventListener | Received Event Name: "+event.name+" Paramaters: "+event.paramList);		// Semicolon separated list of parameter values.
            var paramListArray = [];
            for(var i=0;i<event.paramCount;i++){
                paramListArray.push(event.getParamAtIndex(i));
            }
            core.debug("pifData.qml | Params Array for event "+event.name+" : "+paramListArray);
            eventData(event.name,paramListArray,true)
        }
        events: [
            EventData {name: "EXTV_STATE" },
            EventData {name: "EXTV_CHANNEL_LISTING_VERSION" },
            EventData {name: "MOUSE_ATTACHED_CHANGE" },
            EventData {name: "SLIDER_USB_POWER0" },
            EventData {name: "SLIDER_USB_POWER1" },
            EventData {name: "SLIDER_USB_POWER2" },
            EventData {name: "SATCOM_LINK_STATUS" },
            EventData {name: "GCS_KU_STATE" },
            EventData {name: "GCS_EXCONNECT_SERVICE_ON_SM" },
            EventData {name: "IPOD_CONNECT" },
            EventData {name: "IPOD_DISCONNECT" },
            EventData {name: "ALL_DOORS_CLOSED" },
            EventData {name: "EXP_NOTIFICATION"},
            EventData {name: "EXP_INSEAT_NOTIFICATION_AVAIL" },
            EventData {name: "VB_START_TIME"},
            EventData {name: "VIDEO_SSRC_ALERT"},
            EventData {name: "HANDSET_IN_CRADDLE_CHANGE"},
            EventData {name: "USB_STORAGE_DEVICE_UNKNOWN"},
            EventData {name: "USB_MOUNT_FAILED"},
            EventData {name: "GAMEPAD_ATTACHED_CHANGE"},
            EventData {name: "KEYBOARD_ATTACHED_CHANGE"},
            EventData {name: "AUDIO_LOST_SEAT_REBOOT"},
            EventData {name: "AUDIO_LOST_DSP_REBOOT"},
            EventData {name: "VIDEO_LOST_SEAT_REBOOT"},
            EventData {name: "VIDEO_LOST_DSP_REBOOT"},
            EventData {name: "AUDIO_RECOVERED_SEAT_REBOOT"},
            EventData {name: "AUDIO_RECOVERED_DSP_REBOOT"},
            EventData {name: "AUDIO_DEVICE_ADDED"},
            EventData {name: "AUDIO_DEVICE_REMOVED"},
            EventData {name: "STREAMERS_STATES"},
            EventData {name: "FALLBACK_STATUS"},
            EventData {name: "AIRFRAME_TAIL_NUMBER"},
            EventData {name: "CBB_LIVE_TV_SYSTEM_ON"},
            EventData {name: "EXP_SERVER_STATUS"},
            EventData {name: "POWER_DOWN_READY_STATE_1"},
            EventData { name: "HANDSET_AVAILABLE_SETTING"},
            EventData { name: "PAX_INFO_DISABLE"},
			EventData { name: "IPOD_CHARGE_ONLY_CONNECT"},
			EventData { name: "IPOD_CHARGE_ONLY_DISCONNECT"}
        ]
    }

    Timer {id: eventsTimer; interval: 200; onTriggered: interactiveStartup.eventHandler(interactiveStartup.cPifLoaded);}
    Timer {id:simDisplayControllerTimer; interval:20; property int contrast:-2; property int brightness:-2;
        onTriggered: {
            if (brightness!=-2) __brightnessTriggered(brightness); brightness=-2;
            if (contrast!=-2) __contrastTriggered(contrast); contrast=-2;
        }
    }
    //Timer {id:orientationTimer; repeat: false; interval: 1000; onTriggered: {__setOrientation(PifData.SysOrientation);}}
    Timer {id:brightnessChangeTimer; interval: 500; property int brightnessValue; onTriggered: {if(__preBacklightOffState){__preBacklightOffState=false; PifData.BrightnessLevel=brightnessValue; return;} __brightnessTriggered(brightnessValue);}}
    Timer {id:interactiveResetTimer; repeat: false; onTriggered: __interactiveReset();}
}
