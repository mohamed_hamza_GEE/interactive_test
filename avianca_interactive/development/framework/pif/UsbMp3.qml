import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Data

AudioMediaPlayer{
    property alias mp3Info: mp3Info
    property variant deviceMngr;
    property bool __suspend;
    property bool __delayUnsuspend:false;
    property bool __playingFromPlaylist:false;
    property int __prevMediaState:0;
    property string mediaSource:"usb";
    property string __mediaPathname:"";
    property string playedDirectory:"";
    property variant mp3params;

    onSigAggPlayBegan:{if(pif.getLSAmediaSource()=="usb") pif.setVolumeSource(pif.cUSB_VOLUME_SRC);}
    onSigMidPlayBegan:{
       if(getLSAmediaSource()=="usb") {
            mp3Info.requestDuration(mediaModel.getValue(playIndex,"media_filename"));
            __mediaPathname = mediaModel.getValue(playIndex,"media_filename");
            mp3Info.source = __mediaPathname;
        }
    }
    onSigMidPlayStopped: playedDirectory=""
    Connections{target:pif.usb; onSigUsbDisconnected: {stop(); __clearSuspend();}}

    function setPlayingFileName(fname){
        core.debug (__playerType+" | setPlayingFileName | fname: "+fname);
        if(__shuffle) __mediaShuffle(false);
        var indx = mediaModel.indexOf({"media_filename":fname},0);
        var ret = mediaPlayer.gotoItem(indx==-1?0:indx);
        if(__shuffle) __mediaShuffle(true);
        core.info (__playerType+" | setPlayingFileName | Return status: "+ret);
        return __mediaPlayCheck(indx==-1?0:indx,ret);
    }

    function playByModel(path,params,filterList){
        core.info("UsbMp3.qml | playByModel called | params: "+params+",path "+path);
        mp3params = params
        __populateMp3Model(path,filterList);
    }

    function __playByModelContinue(){
        core.info("UsbMp3.qml | __playByModelContinue | count: "+resourceBrowserMp3Model.count)
        if (!resourceBrowserMp3Model || !resourceBrowserMp3Model.count) {core.error ("UsbMp3.qml | Empty Model or Model not provided. EXITING..."); return false;}
        mp3params.playIndex = (mp3params.playIndex==undefined || mp3params.playIndex>=resourceBrowserMp3Model.count)?undefined:mp3params.playIndex;
        var params = __validateParams(mp3params,(mp3params.amid?mp3params.amid:1)); if (!params) return false;
        __playingFromPlaylist=false;
        return __playUsbMp3(params,resourceBrowserMp3Model);
    }

    function playJukeboxModel(model,params){
        core.info("UsbMp3.qml | playJukeboxModel called | params: "+params);
        if (!model || !model.count) {core.error ("UsbMp3.qml | Empty Model or Model not provided. EXITING..."); return false;}
        params.playIndex = (params.playIndex==undefined || params.playIndex=="")?0:params.playIndex;
        params = __validateParams(params,(params.amid?params.amid:-1)); if (!params) return false;
        __playingFromPlaylist=true;
        return __playUsbMp3(params,model);
    }

    function stop() {if (pauseRequestTimer.running) pauseRequestTimer.stop(); return __mediaStop();}

    function suspendPlaying() {
        if (__suspend||__mediaState==cMEDIA_STOP) return false;
        __prevMediaState = __mediaState;
        __suspend = true;
        stop();
        return true;
    }

    function unSuspendPlaying() {
        core.info("UsbMp3 | unSuspendPlaying")
        if (!__suspend) return false;
        if(pif.getLSAmediaSource()=="usb") {
            __delayUnsuspend=true;
            mp3Info.requestDuration(mediaModel.getValue(__modelIndex,"media_filename"));
             __mediaPathname = mediaModel.getValue(__modelIndex,"media_filename");
             mp3Info.source = __mediaPathname;
            return true;
        }
    }

    function unSuspendPlayingContinue(){
        core.info("UsbMp3.qml | unSuspendPlayingContinue")
        __suspend = false;
        setPlayingIndex(-1);
        if (__prevMediaState==cMEDIA_PAUSE) pauseRequestTimer.restart();
        __prevMediaState=0;
        return true;
    }

    function playNext () { return __mediaNext();}
    function playPrevious () { return __mediaPrevious();}
    function pause () { return __mediaPause();}
    function resume () { return __mediaResume();}
    function forward () { return __mediaForward();}
    function rewind () { return __mediaRewind();}
    function restart() { return __mediaRestart();}

    function setMediaPosition (pos) {
        if (__mediaState == cMEDIA_STOP) {core.error (__playerType+" | setMediaPosition | Media Position set request made when media is stopped. Exiting..."); return false;}
        __mediaElapseTime=pos;
        __elapseTime=pos;
        resume(); // Position can only be updated while media is playing, as per documentation on Splat.
        pos=(pif.getMonitorType()==pif.cLRU_ECO || pif.getMonitorType()==pif.cLRU_ECOV2 || pif.getMonitorType()==pif.cLRU_ELITE || pif.getMonitorType()==pif.cLRU_ELITEV2 || pif.getMonitorType()==pif.cLRU_HDPSEB)?Math.floor(parseInt(pos*100,10)/__mediaEndTime):pos;
        mediaPlayer.position = (!pos)?0:pos;
        __processMediaElapse();
        core.info(__playerType+" | setMediaPosition, value: "+pos+" (in % for Eco, Elite, Elitev2, Hdpseb monitors)");
        return true;
    }

    function frmwkInitApp(){ __usbMaxFileSize=getUsbMaxfileSize();}
    function frmwkResetApp(){core.info("UsbMp3.qml | frmwkResetApp"); stop();}
    function clearMediaPlayer(){__clearMediaModel();__clearSuspend();return true}
    function isPlayingFromPlayist(){return __playingFromPlaylist;}

    // Private functions
    function __playUsbMp3(params,model) {   // model optional
        pif.__setLastSelectedAudio(pif.usbMp3);
        if (pauseRequestTimer.running) pauseRequestTimer.stop();
        var ret = __playUsbMp3Callbk(params,model);
        var s=__playerType+' | '; for (var i in params) s+=i+" = "+params[i]+", "; core.debug(s);
        return ret;
    }

    function __playUsbMp3Callbk(p,m) {
        core.info("UsbMp3.qml | __playUsbMp3Callbk called p.amid:"+p.amid);
        var ret;
        if (m) ret = __setMediaModel(m,p.cid,p.amid,p.mediaType);
        if(!ret) { core.info("UsbMp3.qml | __playUsbMp3Callbk, model not referenced to mediaPlayer, EXITING..."); return ret;}
        if(p.playIndex!=0) p.playIndex=core.coreHelper.searchEntryInModel(mediaModel,"media_filename",p.playIndex);
        ret = __mediaPlay(p); if(ret) playedDirectory=(__playingFromPlaylist)?"":pif.usb.getFolderPath();
        return ret;
    }

    function __validateParams (p,amid) {
        p.amid=amid;
        p.duration=(p.duration==undefined)?"00:00:00":p.duration;
        p.elapseTimeFormat=(String(parseInt(p.elapseTimeFormat,10))!="NaN")?parseInt(p.elapseTimeFormat,10):1;
        p.cid=(String(parseInt(p.cid,10))!="NaN")?parseInt(p.cid,10):0;
        p.playIndex=(p.playIndex!="")?p.playIndex:0;
        p.elapseTime=0;
        p.mediaType=(p.mediaType)?p.mediaType:"mp3";
        p.repeatType=(String(parseInt(p.repeatType,10))!="NaN")?p.repeatType:2;
        p.playType=p.playType?p.playType:0;
        p.shuffle=(!p.shuffle)?false:true;
        p.vbMode=(p.vbMode==undefined)?false:p.vbMode;
        p.rating=254;
        return p;
    }

    function __clearSuspend(){playedDirectory=""; __suspend=false; __prevMediaState=0; return true;}
    function __populateMp3Model(path,filterList){
        core.info("UsbMp3.qml | __populateMp3Model | path: "+path)
        resourceBrowserMp3Model.folder=path;
    }

    function __appendToMediaModel(cModel,index,cid,cdAlbumId){
        var aliasModelIndex=__modelIndex;
        var tempArray=__durationArray;
        mediaPlaylist.playlistUpdateEnabled=false;

        var mid=(parseInt(mediaModel.getValue(mediaModel.count-1,"mid"),10))+1;
        if(parseInt(cModel.getValue(index,"filesize"),10)<=Math.ceil(__usbMaxFileSize*1048576)){
            tempArray.push("00:00:00"); __durationArray=tempArray;
            mediaModel.append({"amid":parseInt(cdAlbumId,10),"mid":mid,"media_type":"mp3","media_filename":cModel.getValue(index,"pathfilename"),"filenameonly":cModel.getValue(index,"filenameonly"),"duration":"00:00:00","cid":cid})
        }

        mediaPlaylist.playlistUpdateEnabled=true;
        mediaPlaylist.playlistIndex=aliasModelIndex;
        if(__shuffle) __mediaShuffle(true);
        if (core.pc && mediaModel.count) __simPlayerToModelMap=(__shuffle)?core.coreHelper.randomizeArray(__arrayWithIndexValue(mediaModel.count)).join(','):__arrayWithIndexValue(mediaModel.count).join(',');
        return true;
    }

    Component.onCompleted: {
        __playerType = "Usb Media Player";
        __timerInterval = pif.__getAudioPlayTimerInterval();
        __skipPreviousTimeout = pif.__getSkipPreviousTimeout();
    }
    MP3Info {
        id:mp3Info
        onDurationCompleted:{
            core.info("UsbMp3.qml | onDurationCompleted ")
            if(__mediaPathname==source && __mediaEndTime!=duration){
                var temp=__durationArray; temp[__modelIndex]=core.coreHelper.convertSecToHMS(duration); __durationArray=temp;
                core.info("UsbMp3.qml | onDurationCompleted, medial model property duration is updated with value: "+core.coreHelper.convertSecToHMS(duration)+" for track: "+source)
                __updateEndElapseValues();
                if(__delayUnsuspend==true){
                    unSuspendPlayingContinue();
                    __delayUnsuspend=false;
                }
            }
        }
    }
    ResourceBrowserModel {id:resourceBrowserMp3Model; showdotfiles:false;fileFilters:["*.mp3"];
        onFolderChanged:{
            core.info(" UsbMp3.qml | onFolderChanged | folder:"+resourceBrowserMp3Model.folder)
            __playByModelContinue();
        }
    }
    Timer {id:pauseRequestTimer; interval:50; onTriggered: __mediaPause(true);}
}
