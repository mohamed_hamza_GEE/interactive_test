// Event Listener Values
var OpenFlight=0;
var EntOn=1;
var alldoorsClosed=0;
var MediaDate=0;
var SimData=[];
var PaState=0;
var GroundSpeed = 0;
var Altitude = 0;   // We are receiving the altitude from transiant data in feet.
var CtMsg = [];
var SourceIATA = "";
var DestinationIATA = "";
var SourceICAO = "";
var FlightNumber = "";
var RouteId = 0;
var CgDataAvailable = 0;
var SatComStatus = 0;
var GcsStatus = false;
var GcsInstalled = false;
var DelayedEntOff = 0;
var AttendantCall = false;
var ReadingLight = false;
var SeatRating = 0;
var InfoMode = false;
var InteractiveState = 0;
var IpodConnected = false;
var WowState = false;
var CapSensorState = 0;
var DestICAOcode = "";
var ExphoneServerState = false;
var ExphoneNotificationState = false;
var ExphoneAvailable = false;
var GmtOffsetOfDestination = 0;
var GmtOffsetOfSource = 0;
var DstOfDestination = 0;
var DstOfSource = 0;
var InteractiveOverride = 0;
var TrueAirSpeed = 0;               // Gives the current true air speed in knots.
var DistToDestination = 0;          //Distance to destination in nautical miles
var OutsideAirTemp = 0;             // Outside air temperature in celsius
var Orientation = 0;
var VHSCradleState=0;
var StowOrientation="";/* Possible values=>left,right,up and down */
var SysOrientation = 0;
var AutoRotation=true;
var LockOrientation=[];
var MinPAVolume = 0;                // Minimum PA volume specified in the lru.cfg
var Stowed = false;                 // true = Monitor is stowed.
var TimeToDestination = 0;     //  Gives remaining time to reach destination. Value is in minutes format.
var ArrivalTime = "0000";           // Gives estimated arrival time. Value is in HH:MM time format but without ":"
var TimeAtOrigin = "0000";          // Gives time at origin. The format is HH:MM time format but without ":"
var TimeSinceTakeOff = "0000";      // Gives the time since the flight took off... Value is in HHMM format but without ":".
var TimeAtDestination = "0000";     // Gives time at destination. Value is in HH:MM time format but without ":"
var ExtvState = 0;
var ExtvVersion = "0";
var DeviceCharging = 0;
var UsbPortStatus = 0;              // The status of the usb port. -1 => unknown state ,0 => disabled and 1 => enabled.
var IpodPortStatus = 0;             // The status of the ipod port.  -1 => unknown state ,0 => disabled and 1 => enabled.
var ExternalAppActive=false;
var runGamePath="/usr/local/bin/rungame";
var corePssState=true; // core always set seatServices.pssAlert="enable" at startup
var DestinationCityTemp=0;
var DistFromOrigin = 0;             //Distance from origin in nautical miles
var VbStartTime = 0;
var VbCycleTime = 0;
var IsBrowserInSeat=false;
var NoOfDimmableWindows=0;     // Number of dimmable windows managed by this seat.
var DeviceConnected = "";
var FolderResourceBrowserModel=false;
var PASDeviceList=[];          // Number of PAS Devices from lru.cfg (PAS parameter)
var InteractivePssPopup=true;
var GameTimeout=0;
var FallbackMode = 0; // 0 => Interactive mode, 1 => Fallback mode
var AirframeTailNumber="";
var LiveTvSystemOn=0;
var FastenSeatBelt=0;
var IntWaitBeforeReset=2;
var MessageBodyNeeded=false;
var viModeMediaPlayer=false;
var pdiDataUpdateCount=0;
var PaxInfoDisable=false;
var HandsetAvailable = 0;

//Most of the interactives doesn't support fallback by default.
//When the fallback events comes, framework and interactive shouldn't do anything.
var IgnoreFallbackEvents = true;
var streamerStates=[];
var groupStates=[];
var MidBlocked=2;
var MidUnblocked=3;

// AudioVideo
var MidArr = [];
var PIPArr = [];
var PIPColorArr = [];
var VideoSpeedArr = [];
var PIPColor;
var VideoPlayTimerInterval=10;
var AudioPlayTimerInterval=10;
var VideoSkipTimerInterval=2;
var NoOfVideoPlaylists=0;
var RememberAspectRatio=false;
var DefaultStretchState=false;
var SkipPreviousTimeout=0;

// List of Dynamic Components
var CompList;
var FontList=[];
var IFEKeys;

// List of custom lru parameters
var LruParameters=[];
var Lru1DParameters=[];
var Lru2DParameters=[];
var IpodCableVideo=0;

// Seat Information
var MonitorType=0;
var RawSeatRow=0;
var SeatRow=RawSeatRow+1;
var RawSeatLetter=0;
var SeatLetter=String.fromCharCode(65+RawSeatLetter).toLowerCase();     //'a';
var SeatNumber='';
var CabinClass=0;
var CabinClassName='';

//Seat IP address for Karma
var Attached_SM_IP="";
var eX2_Feedforward_Host="";

//Volume values in Percent
var VolumeStep=10;
var DefaultAudioVolume=50;
var DefaultVideoVolume=50;
var VolumeSource=0;
var Volume=DefaultAudioVolume;
var VolumeArr=[];
var VolumeSourceStringArr=['av','av','cpu','av','mp3'];     // aod,vod,pc,pa,usb

//Brightness values in Percent
var BrightnessStep=10;
var BrightnessDefault=50;
var BrightnessLevel=50;

//Contrast values in Percent
var ContrastStep=10;
var ContrastDefault=50;
var ContrastLevel=50;

//Telephony
var PermitIncomingCall=true;
var DefaultTelephonyVolume=70;

//Handset
var handset=0;
var handsetNo=0;
var noOfHandsets=0;
var PremiumHandsetDisplayArr = [];

//Playlist
var NumberOfAodPlaylist=1;
var NumberOfKidsAodPlaylist=1;
var NumberOfUsbPlaylist=1;
var MAXTracksInAodPlaylist=100;
var MAXTracksInKidsAodPlaylist=100;
var MAXTracksInUsbPlaylist=100;
var NumberOfVodPlaylist=1;
var NumberOfKidsVodPlaylist=1;
var MAXTracksInVodPlaylist=10;
var MAXTracksInKidsVodPlaylist=10;
var MaxTracksInAllPlaylist=0;
var RemovePaidMidsFromPlaylist=true;
var RetainPlaylistdata=false; // true - playlist won't get cleared in Ent Off, False - playlist will get cleared in Ent Off
var PlaylistUpdateRequiredOnKarma=true; // true - seatback will notify Karma about playlist change, False - seatback won't notify Karma about playlist change

//Paxus
var EnablePaxusScreenLog=false;
var PaxusServiceStatus=true;

//Usb
var RetainUsbData=false;
var ShowDotFiles=false;
var PreserveSorting=true;
var DefaultSortOrder="ascending";
var DefaultSortOn="filenameonly";
var MaxFileSize=100;
var MaxImageFileSize=15;

//Seat Chat;
var RestrictedWords='';
var MaxSessionCount=1000;
var DeleteInActiveSessions=false;
var SessionsOnAcceptInvite=false;
var SeatChatIsOnVkarma=false;
var ActiveChatUserNeeded=false;
//eXphone (ExphoneState_Raw supports 6 values from 1 to 6 & ExphoneNotification_Raw supports values 0 & 1)
var ExphoneStateRaw=1;  // When 3 eXphone service becomes available
var ExphoneNotificationRaw=1;   // When 0 interactive needs to show eXphone turned On popup
var UseExphoneInSeat=false;     // Set to true if TD 288 is used for Xphone (e.g. in CX) else its TD 277 & TD 266

var RetainSharedEmailMessages=false;

//Fallback
var UseFallbackStatus=false; // Set to true if required to use event "FALLBACK_STATUS" instead of "STREAMERS_STATES" event for triggering fallback mode
function setConfiguration(cfgData,lruData) {
    CompList = coreHelper.cfgVariantObjToArray(cfgData.PifCompList);
    FontList = coreHelper.cfgVariantObjToArray(cfgData.PifFontList);
    IFEKeys = coreHelper.cfgVariantObjToArray(cfgData.PifIFEKeysList);
    //Custom lru.cfg name=value
    var CustomLruParameters = coreHelper.cfgVariantObjToArray(cfgData.PifLruParameters);
    for(var i=0; i<CustomLruParameters.length; i++) if(lruData[CustomLruParameters[i]]!=undefined) LruParameters[CustomLruParameters[i]] = coreHelper.replaceReservedChars(lruData[CustomLruParameters[i]]);
    //Custom lru.cfg 1-D Array name=value
    CustomLruParameters = coreHelper.cfgVariantObjToArray(cfgData.PifLru1DParameters);
    for(i=0; i<CustomLruParameters.length; i++) if(lruData[CustomLruParameters[i]]!=undefined) Lru1DParameters[CustomLruParameters[i]] = coreHelper.cfgVariantObjToArray(lruData[CustomLruParameters[i]]);
    //Custom lru.cfg 2-D Array name=value
    CustomLruParameters = coreHelper.cfgVariantObjToArray(cfgData.PifLru2DParameters);
    for(i=0; i<CustomLruParameters.length; i++) if(lruData[CustomLruParameters[i]]!=undefined) Lru2DParameters[CustomLruParameters[i]] = coreHelper.updateCfgArray(lruData[CustomLruParameters[i]]);
    if (!MonitorType) MonitorType = cLRU_GEODE;
    core.info("MonitorType: "+MonitorType);
    // Video
    PIPColorArr[cLRU_GEODE] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_LXGEODE] = ['#000000','#ffffff'];
    PIPColorArr[cLRU_PSEB] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_SM17] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_ECO] = ['#100000','#f0f0f0'];
    PIPColorArr[cLRU_ECOV2] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_FE] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_VIA] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_HDPSEB] = ['#001000','#f0f0f0'];
    PIPColorArr[cLRU_ELITE] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_ELITEV2] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_ALTUS] = ['#100000','#ffffff'];
    PIPColorArr[cLRU_VKARMA] = ['#100000','#f0f0f0'];
    PIPColor = PIPColorArr[MonitorType];
    PIPArr = coreHelper.updateCfgArray (cfgData.PifPIP, "index", "index");
    VideoSpeedArr = coreHelper.cfgVariantObjToArray(cfgData.PifVideoSpeed);
    VideoPlayTimerInterval = cfgData.PifVideoPlayTimerInterval ? parseInt(cfgData.PifVideoPlayTimerInterval,10):VideoPlayTimerInterval;
    AudioPlayTimerInterval = cfgData.PifAudioPlayTimerInterval ? parseInt(cfgData.PifAudioPlayTimerInterval,10):AudioPlayTimerInterval;
    VideoSkipTimerInterval = cfgData.PifVideoSkipTimerInterval ? parseInt(cfgData.PifVideoSkipTimerInterval,10):VideoSkipTimerInterval;
    SkipPreviousTimeout = cfgData.PifSkipPreviousTimeout? parseInt(cfgData.PifSkipPreviousTimeout,10):SkipPreviousTimeout;
    RememberAspectRatio = cfgData.PifRememberAspectRatio ? cfgData.PifRememberAspectRatio=="true"?true:false:RememberAspectRatio;
    DefaultStretchState = cfgData.PifDefaultStretchState ? cfgData.PifDefaultStretchState=="true"?true:false:DefaultStretchState;
    PermitIncomingCall=cfgData.PifPermitIncomingCall?cfgData.PifPermitIncomingCall=="true"?true:false:PermitIncomingCall;
    if (coreHelper.replaceReservedChars(cfgData.PifHandsetMode)=="mouse") setPointerMode(); else setKeyMode();
    NumberOfAodPlaylist=cfgData.PifNumberOfAodPlaylist?parseInt(cfgData.PifNumberOfAodPlaylist,10):NumberOfAodPlaylist;
    NumberOfKidsAodPlaylist=cfgData.PifNumberOfKidsAodPlaylist?parseInt(cfgData.PifNumberOfKidsAodPlaylist,10):NumberOfKidsAodPlaylist;
    NumberOfUsbPlaylist=cfgData.PifNumberOfUsbPlaylist?parseInt(cfgData.PifNumberOfUsbPlaylist,10):NumberOfUsbPlaylist;
    MAXTracksInAodPlaylist=cfgData.PifMAXTracksInAodPlaylist?parseInt(cfgData.PifMAXTracksInAodPlaylist,10):MAXTracksInAodPlaylist;
    MAXTracksInKidsAodPlaylist=cfgData.PifMAXTracksInKidsAodPlaylist?parseInt(cfgData.PifMAXTracksInKidsAodPlaylist,10):MAXTracksInKidsAodPlaylist;
    MAXTracksInUsbPlaylist=cfgData.PifMAXTracksInUsbPlaylist?parseInt(cfgData.PifMAXTracksInUsbPlaylist,10):MAXTracksInUsbPlaylist;
    RemovePaidMidsFromPlaylist=cfgData.PifRemovePaidMidsFromPlaylist?(cfgData.PifRemovePaidMidsFromPlaylist=="true")?true:false:RemovePaidMidsFromPlaylist;
    EnablePaxusScreenLog=cfgData.PifEnablePaxusScreenLog?(cfgData.PifEnablePaxusScreenLog=="true"?true:false):EnablePaxusScreenLog;
    RetainUsbData=cfgData.PifRetainUsbData?(cfgData.PifRetainUsbData=="true"?true:false):RetainUsbData;
    RetainPlaylistdata=cfgData.PifRetainPlaylistdata?(cfgData.PifRetainPlaylistdata=="true"?true:false):RetainPlaylistdata;
    PlaylistUpdateRequiredOnKarma=cfgData.PifPlaylistUpdateRequiredOnKarma?(cfgData.PifPlaylistUpdateRequiredOnKarma=="true"?true:false):PlaylistUpdateRequiredOnKarma;
    RetainSharedEmailMessages=cfgData.PifRetainSharedEmailMessages?(cfgData.PifRetainSharedEmailMessages=="true"?true:false):RetainSharedEmailMessages;
    ShowDotFiles=cfgData.PifUsbShowDotFiles?(cfgData.PifUsbShowDotFiles=="true")?true:false:ShowDotFiles;
    PreserveSorting=cfgData.PifUsbPreserveSorting?(cfgData.PifUsbPreserveSorting=="true")?true:false:PreserveSorting;
    DefaultSortOrder=cfgData.PifUsbDefaultSortOrder?cfgData.DefaultSortOrder:DefaultSortOrder;
    DefaultSortOn=cfgData.PifUsbDefaultSortOn?cfgData.DefaultSortOn:DefaultSortOn;
    MaxFileSize=cfgData.PifUsbMaxFileSize?cfgData.PifUsbMaxFileSize:MaxFileSize;
    MaxImageFileSize=cfgData.PifUsbMaxImageFileSize?cfgData.PifUsbMaxImageFileSize:MaxImageFileSize;
    IpodCableVideo = lruData.Ipod_Cable_Video?parseInt(lruData.Ipod_Cable_Video,10):IpodCableVideo;
    StowOrientation = lruData.Stow_Orientation?coreHelper.replaceReservedChars(lruData.Stow_Orientation):StowOrientation;
    RestrictedWords=cfgData.PifRestrictedWords?cfgData.PifRestrictedWords:RestrictedWords
    MaxSessionCount=cfgData.PifMaxSessionCount?cfgData.PifMaxSessionCount:MaxSessionCount
    DeleteInActiveSessions=cfgData.PifDeleteInActiveSessions?(cfgData.PifDeleteInActiveSessions=="true")?true:false:DeleteInActiveSessions
    SessionsOnAcceptInvite=cfgData.PifSessionsOnAcceptInvite?(cfgData.PifSessionsOnAcceptInvite=="true")?true:false:SessionsOnAcceptInvite
    SeatChatIsOnVkarma=cfgData.PifSeatChatIsOnVkarma?(cfgData.PifSeatChatIsOnVkarma=="true")?true:false:SeatChatIsOnVkarma
    ActiveChatUserNeeded=cfgData.PifActiveChatUsers?(cfgData.PifActiveChatUsers=="true")?true:false:ActiveChatUserNeeded
    MessageBodyNeeded=cfgData.PifMessageBodyNeeded?(cfgData.PifMessageBodyNeeded=="true")?true:false:MessageBodyNeeded
    UseExphoneInSeat=cfgData.PifUseExphoneInSeat?(cfgData.PifUseExphoneInSeat=="true")?true:false:UseExphoneInSeat;
    UseFallbackStatus=cfgData.PifUseFallbackStatus?(cfgData.PifUseFallbackStatus=="true")?true:false:UseFallbackStatus;
    IsBrowserInSeat=cfgData.PifIsBrowserInSeat?(cfgData.PifIsBrowserInSeat=="true")?true:false:IsBrowserInSeat;
    DeviceConnected=(cfgData.PifDeviceConnected)?cfgData.PifDeviceConnected:DeviceConnected;
    FolderResourceBrowserModel=cfgData.PifFolderResourceBrowserModel ? (cfgData.PifFolderResourceBrowserModel=="true")?true:false:FolderResourceBrowserModel
    var PAS=cfgData.PifPASDeviceList?coreHelper.cfgVariantObjToArray(cfgData.PifPASDeviceList):undefined;
    if (PAS) for (i=0;i<PAS.length;i++) PASDeviceList[PAS[i]]=[];
    InteractivePssPopup=cfgData.PifInteractivePssPopup?cfgData.PifInteractivePssPopup=="true"?true:false:InteractivePssPopup;
    IgnoreFallbackEvents=lruData.Enable_Local_Streamer?parseInt(lruData.Enable_Local_Streamer,10)==1?false:true:IgnoreFallbackEvents;
    IntWaitBeforeReset=cfgData.PifIntWaitBeforeReset?cfgData.PifIntWaitBeforeReset:IntWaitBeforeReset
    //case where config is set after streamer event is received
    if(!IgnoreFallbackEvents && !pif.getFallbackMode() && parseInt(PifData.streamerStates[0],10)==0){sigIgnoreFallbackEventsChanged(IgnoreFallbackEvents)}
    viModeMediaPlayer=cfgData.PifViModeMediaPlayer?cfgData.PifViModeMediaPlayer=="true"?true:false:viModeMediaPlayer;

    // Lru Data
    RawSeatRow = lruData.Seat_Row?parseInt(lruData.Seat_Row,10):RawSeatRow;
    SeatRow = RawSeatRow+1;
    RawSeatLetter = lruData.Seat_Seat?parseInt(lruData.Seat_Seat,10):RawSeatLetter;
    SeatLetter = String.fromCharCode(65+RawSeatLetter).toLowerCase();
    SeatNumber = lruData.Seat_Number?coreHelper.replaceReservedChars(lruData.Seat_Number).toLowerCase():SeatRow+SeatLetter;
    CabinClass = lruData.Cabin_Class?parseInt(lruData.Cabin_Class,10):CabinClass;
    CabinClassName = lruData.Cabin_Class_Name?coreHelper.replaceReservedChars(lruData.Cabin_Class_Name):CabinClassName;
    Attached_SM_IP = lruData.Attached_SM_IP?coreHelper.replaceReservedChars(lruData.Attached_SM_IP):Attached_SM_IP;
    eX2_Feedforward_Host = lruData.eX2_Feedforward_Host?coreHelper.replaceReservedChars(lruData.eX2_Feedforward_Host):eX2_Feedforward_Host;

    PAS = lruData.PAS&&PAS?coreHelper.updateCfgArray(lruData.PAS,"",0):undefined;
    if (PAS!==undefined){
        for (i in PASDeviceList){
            if (PAS[i]) {
                PASDeviceList[i] = {ip:PAS[i][1],port:PAS[i][2],type:"av",volume:DefaultAudioVolume};
                core.debug("PifData.js | setConfiguration | PASDeviceList: "+i+" DeviceInfo: "+PAS[i]+" RawSeatRow: "+RawSeatRow+" RawSeatLetter: "+RawSeatLetter);
                if(!core.pc){
                    var ret = volumeID.getPasInfo();
                    core.debug ("PifData.js | setConfiguration | getPasInfo status: "+ret);
                    ret = volumeID.pasAdvertise (RawSeatLetter, RawSeatRow, PAS[i], PASDeviceList[i].ip, PASDeviceList[i].port)
                    core.debug ("PifData.js | setConfiguration | pasAdvertised status: "+ret);
                }
            } else PASDeviceList[i]=undefined;
        }
    }
    BrightnessStep = lruData.Brightness_Step?parseInt(lruData.Brightness_Step,10):(cfgData.DefBrightnessStep?parseInt(cfgData.DefBrightnessStep,10):BrightnessStep);
    BrightnessDefault = lruData.Brightness_Default?parseInt(lruData.Brightness_Default,10):BrightnessDefault;

    MinPAVolume = lruData.Minimum_PA_Volume?parseInt(lruData.Minimum_PA_Volume,10):MinPAVolume;
    VolumeStep = lruData.Volume_Step?parseInt(lruData.Volume_Step,10):(cfgData.DefVolumeStep?parseInt(cfgData.DefVolumeStep,10):VolumeStep);
    DefaultVideoVolume = lruData.Default_Video_Volume?parseInt(lruData.Default_Video_Volume,10):DefaultVideoVolume; VolumeArr[cVOD_VOLUME_SRC]=DefaultVideoVolume;
    DefaultAudioVolume = lruData.Default_Audio_Volume?parseInt(lruData.Default_Audio_Volume,10):DefaultAudioVolume; VolumeArr[cAOD_VOLUME_SRC]=DefaultAudioVolume;

    ContrastStep = lruData.Contrast_Step?parseInt(lruData.Contrast_Step,10):(cfgData.DefContrastStep?parseInt(cfgData.DefContrastStep,10):ContrastStep);
    ContrastDefault = lruData.Contrast_Default?parseInt(lruData.Contrast_Default,10):ContrastDefault;
    NumberOfVodPlaylist=cfgData.PifNumberOfVodPlaylist?parseInt(cfgData.PifNumberOfVodPlaylist,10):NumberOfVodPlaylist;
    NumberOfKidsVodPlaylist=cfgData.PifNumberOfKidsVodPlaylist?parseInt(cfgData.PifNumberOfKidsVodPlaylist,10):NumberOfKidsVodPlaylist;
    MAXTracksInVodPlaylist=cfgData.PifMAXTracksInVodPlaylist?parseInt(cfgData.PifMAXTracksInVodPlaylist,10):MAXTracksInVodPlaylist;
    MAXTracksInKidsVodPlaylist=cfgData.PifMAXTracksInKidsVodPlaylist?parseInt(cfgData.PifMAXTracksInKidsVodPlaylist,10):MAXTracksInKidsVodPlaylist;
    MaxTracksInAllPlaylist=cfgData.PifMaxTracksInAllPlaylist?parseInt(cfgData.PifMaxTracksInAllPlaylist,10):MaxTracksInAllPlaylist;
    DefaultTelephonyVolume=cfgData.PifDefaultTelephonyVolume?parseInt(cfgData.PifDefaultTelephonyVolume,10):DefaultTelephonyVolume;
    NoOfDimmableWindows=String(parseInt(lruData.Number_of_EDW,10))!="NaN"?parseInt(lruData.Number_of_EDW,10):NoOfDimmableWindows;
    resetVolumeLevels();

    var handsetValues = [];
    handsetValues.push([c3K_HANDSET,6]);
    handsetValues.push([cDPCU_HANDSET,28,45,202]);
    handsetValues.push([cSTD_HANDSET,14,18,19,20,21,29,24,36,42]);
    handsetValues.push([cPREMIUM_HANDSET,15,17,22,23,35,41,44]);
    handsetValues.push([cSTD_KARMA_HANDSET,46]);
    handsetValues.push([cVIDEO_KARMA_HANDSET,50,54]);
    handsetValues.push([cCANDYBAR_HANDSET,55]);
    handsetValues.push([cSEAT_HANDSET,31]);

    var handsetTypeArray=[];
    noOfHandsets = 0;
    handsetNo = 0;
    handset=cNO_HANDSET;

    if (lruData.Remote_Number_of_Handsets != undefined && lruData.Remote_Number_of_Handsets>0) {		// Remote Handset detected
        noOfHandsets = parseInt(lruData.Remote_Number_of_Handsets,10);
        handsetNo = parseInt(eval("lruData.Remote_Handset_Type.i0"),10);
        core.info("NO. OF REMOTE HANDSET AVAILABLE: "+noOfHandsets+" HANDSET NO. IN USE: "+handsetNo);
    } else if(lruData.Handset_Available != undefined && lruData.Handset_Available == 1 && lruData.Number_of_Handsets != undefined &&  lruData.Number_of_Handsets>0){			// Handset detected
        noOfHandsets = parseInt(lruData.Number_of_Handsets,10);
        if(lruData.Handsets){
            core.info("'Handsets' parameter found in lru.cfg");
            handsetNo = parseInt(eval("lruData.Handsets.i"+0+".i0"),10);
        } else if(lruData.Handsets_v2){
            core.info("'Handsets_v2' parameter found in lru.cfg");
            handsetNo = parseInt(eval("lruData.Handsets_v2.i"+0+".i0"),10);
        }
    } else { 	// No Handset detected
        core.info("NO HANDSET CONNECTED");
        return;
    }
    // Get handset Type
    for(var j=0; j<handsetValues.length; j++){
        for(var k=1; k<handsetValues[j].length; k++){
            if(handsetValues[j][k] == handsetNo){
                handset = parseInt(handsetValues[j][0],10);
                core.info("HANDSET TYPE IN USE: "+handset);
                break;
            }
        }
    }
    if (handset==cNO_HANDSET) core.info("UNKNOWN HANDSET TYPE CONNECTED.");
    if(handset==cPREMIUM_HANDSET) PremiumHandsetDisplayArr = coreHelper.updateCfgArray (cfgData.PifPremiumHandsetDisplay, "id,qmlFileName,priority,timeout,refreshTime", "id");

    core.info ("PifData.js | setConfiguration | SEAT NUMBER: "+SeatNumber+" CABIN CLASS: "+CabinClass+" BRIGHTNESS DEFAULT: "+BrightnessDefault+" BRIGHTNESS STEP: "+BrightnessStep+" DEFAULT VIDEO VOLUME: "+DefaultVideoVolume+" DEFAULT AUDIO VOLUME: " +DefaultAudioVolume+" VOLUME STEP: "+VolumeStep);
}

function setSimConfiguration(sData) {
    SimData=sData;
    OpenFlight = SimData.OpenFlight?parseInt(SimData.OpenFlight,10):1;
    alldoorsClosed = SimData.AllDoorsClosed?parseInt(SimData.AllDoorsClosed,10):1;
    EntOn = SimData.EntOn?parseInt(SimData.EntOn,10):1;
    MediaDate = SimData.MediaDate==0?parseInt(Qt.formatDateTime(new Date(),"yyyyMMdd"),10):SimData.MediaDate?parseInt(SimData.MediaDate,10):MediaDate;
    mediaDateChanged(MediaDate);
    RouteId = SimData.RouteId?parseInt(SimData.RouteId,10):RouteId; routeIdChanged(RouteId);
    MonitorType = (SimData.MonitorType)?parseInt(SimData.MonitorType,10):MonitorType;
    core.info("MonitorType with Simulation: "+MonitorType);
    PIPColor = PIPColorArr[MonitorType];
    SourceIATA = SimData.SourceIATA?SimData.SourceIATA:SourceIATA; sourceIATAChanged(SourceIATA);
    DestinationIATA = SimData.DestinationIATA?SimData.DestinationIATA:DestinationIATA; destinationIATAChanged(DestinationIATA);
    SourceICAO = SimData.SourceICAO?SimData.SourceICAO:SourceICAO; sourceICAOChanged(SourceICAO);
    FlightNumber = SimData.FlightNumber?SimData.FlightNumber:FlightNumber; flightNumberChanged(FlightNumber);
    Orientation = SimData.Orientation?parseInt(SimData.Orientation,10):Orientation; orientationChanged(Orientation);
    TimeAtDestination = SimData.TimeAtDestination?SimData.TimeAtDestination:TimeAtDestination; timeAtDestinationChanged(TimeAtDestination);
    TimeToDestination = SimData.TimeToDestination?parseInt(SimData.TimeToDestination,10):TimeToDestination; timeToDestinationChanged(TimeToDestination);
    TimeSinceTakeOff = SimData.TimeSinceTakeOff?SimData.TimeSinceTakeOff:TimeSinceTakeOff; timeSinceTakeOffChanged(TimeSinceTakeOff);
    CgDataAvailable = SimData.CgDataAvailable?parseInt(SimData.CgDataAvailable,10):CgDataAvailable; cgDataAvailableChanged(CgDataAvailable);
    InfoMode = SimData.InfoMode?SimData.InfoMode=="true"?true:false:InfoMode; infoModeChanged(PifData.InfoMode);
    ReadingLight = SimData.ReadingLight?SimData.ReadingLight=="true"?true:false:ReadingLight; readingLightChanged(PifData.ReadingLight);
    AttendantCall = SimData.AttendantCall?SimData.AttendantCall=="true"?true:false:AttendantCall; attendantCallChanged(PifData.AttendantCall);
    DelayedEntOff = SimData.DelayedEntOff?parseInt(SimData.DelayedEntOff,10):DelayedEntOff; delayedEntOffChanged(PifData.DelayedEntOff);
    DestICAOcode = SimData.DestICAOcode?SimData.DestICAOcode:DestICAOcode; destICAOcodeChanged(DestICAOcode);
    var gstatus = SimData.GcsStatus?SimData.GcsStatus=="true"?true:false:(GcsStatus&&GcsInstalled)?true:false;
    GcsStatus=gstatus; GcsInstalled=gstatus; gcsStatusChanged (gstatus);
    Stowed = SimData.Stowed?SimData.Stowed=="true"?true:false:Stowed; stowedChanged(PifData.Stowed);
    InteractiveState = SimData.InteractiveState?parseInt(SimData.InteractiveState,10):InteractiveState; interactiveStateChanged(InteractiveState);
    DistFromOrigin = SimData.DistFromOrigin?parseInt(SimData.DistFromOrigin,10):DistFromOrigin; distFromOriginChanged(DistFromOrigin);
    GmtOffsetOfDestination = SimData.GmtOffsetOfDestination?parseFloat(SimData.GmtOffsetOfDestination,10):GmtOffsetOfDestination; gmtOffsetOfDestinationChanged(GmtOffsetOfDestination+DstOfDestination);
    GmtOffsetOfSource = SimData.GmtOffsetOfSource?parseFloat(SimData.GmtOffsetOfSource,10):GmtOffsetOfSource; gmtOffsetOfSourceChanged(GmtOffsetOfSource+DstOfSource);
    DstOfDestination = SimData.DstOfDestination?parseFloat(SimData.DstOfDestination,10):DstOfDestination; //gmtOffsetOfSourceChanged(GmtOffsetOfSource+DstOfSource);
    DstOfSource = SimData.DstOfSource?parseFloat(SimData.DstOfSource,10):DstOfSource; //gmtOffsetOfSourceChanged(GmtOffsetOfSource+DstOfSource);
    ExphoneServerState=SimData.ExphoneServerState?SimData.ExphoneServerState=="true"?true:false:ExphoneServerState; exphoneServerStateChanged(ExphoneServerState);
    ExphoneNotificationState=SimData.ExphoneNotificationState?SimData.ExphoneNotificationState=="true"?true:false:ExphoneNotificationState; exphoneNotificationStateChanged(ExphoneNotificationState);
    VbStartTime = SimData.VbStartTime?(new Date().getTime() - parseInt(SimData.VbStartTime,10)):VbStartTime;
    VbCycleTime = SimData.VbCycleTime?parseInt(SimData.VbCycleTime,10):VbCycleTime;
    TimeAtOrigin = SimData.TimeAtOrigin?parseInt(SimData.TimeAtOrigin,10):TimeAtOrigin; timeAtOriginChanged(TimeAtOrigin);
    GroundSpeed = SimData.GroundSpeed?parseInt(SimData.GroundSpeed,10):GroundSpeed;groundSpeedChanged(GroundSpeed);
    TrueAirSpeed = SimData.TrueAirSpeed?parseInt(SimData.TrueAirSpeed,10):TrueAirSpeed;trueAirSpeedChanged(TrueAirSpeed);
    Altitude = SimData.Altitude?parseInt(SimData.Altitude,10):Altitude;altitudeChanged(Altitude);
    SeatRating = SimData.SeatRating?parseInt(SimData.SeatRating,10):SeatRating;seatRatingChanged(SeatRating);
    ExtvVersion = SimData.ExtvVersion?parseInt(SimData.ExtvVersion,10):ExtvVersion;extvVersionChanged(ExtvVersion);
    SimData.Gcp=SimData.Gcp?SimData.Gcp=="true"?true:false:false;
    SimData.Evid=SimData.Evid?SimData.Evid=="true"?true:false:false;
    DistToDestination = SimData.DistToDestination?SimData.DistToDestination:DistToDestination; distToDestinationChanged(DistToDestination);
    GameTimeout = SimData.GameTimeout?parseInt(SimData.GameTimeout,10):GameTimeout;
    FallbackMode = SimData.FallbackMode?SimData.FallbackMode=="true"?1:0:FallbackMode; sigFallbackMode(FallbackMode);
    AirframeTailNumber = SimData.AirframeTailNumber?SimData.AirframeTailNumber:""; airFrameTailNumberChanged(AirframeTailNumber);
    LiveTvSystemOn = SimData.LiveTvSystemOn?SimData.LiveTvSystemOn:LiveTvSystemOn;liveTvSystemStatusChanged(LiveTvSystemOn);
    FastenSeatBelt = SimData.FastenSeatBelt?SimData.FastenSeatBelt:FastenSeatBelt;fastenSeatBeltChanged(FastenSeatBelt);
    PaxInfoDisable = SimData.PaxInfoDisable?SimData.PaxInfoDisable:PaxInfoDisable;sigPaxInfoDisable(PaxInfoDisable);
}

function setMonitorTypeScript(monitorString) {
    core.info("MonitorString received from script: "+monitorString);
    MonitorType=(monitorString=='geode')?cLRU_GEODE:(monitorString=='eco9')?cLRU_ECO:(monitorString=='eco9v2')?cLRU_ECOV2:(monitorString=='pseb')?cLRU_PSEB:(monitorString=='vkarma')?cLRU_VKARMA:(monitorString=='fe')?cLRU_FE:(monitorString=='elite')?cLRU_ELITE:(monitorString=='via')?cLRU_VIA:(monitorString=='hdpseb')?cLRU_HDPSEB:(monitorString=='lxgeode')?cLRU_LXGEODE:(monitorString=='sm17')?cLRU_SM17:(monitorString=='elitev2')?cLRU_ELITEV2:(monitorString=='altus')?cLRU_ALTUS:MonitorType;
}

function resetVolumeLevels() {
    VolumeArr=[DefaultAudioVolume,DefaultVideoVolume,DefaultAudioVolume,MinPAVolume>DefaultAudioVolume?MinPAVolume:DefaultAudioVolume,DefaultAudioVolume];     // aod,vod,pc,pa,usb
}
