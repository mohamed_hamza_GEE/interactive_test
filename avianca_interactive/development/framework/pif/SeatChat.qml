import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Info
import Panasonic.chatmgr 1.0


Item {
    id:seatChat
    property variant manager;
    property string cAvailable: "Available";
    property string cAway: "Away";
    property string cDoNotDisturb: "DoNotDisturb";
    property bool __appActive:false;
    property string __seatNumber: "";
    property string __seatNickname: "";
    property string __seatStatus: cAvailable;
    property bool __appSetActiveCompleted: true;
    property int __seatState: 0;
    property bool __seatBlockAllInvites: true;
    property alias sessionListModel:sessionListModel    // private chatroom sessions
    property alias publicsessionListModel:publicsessionListModel    // public chatroom sessions
    property alias buddyListModel:buddyListModel            // List of buddies in current chatroom session
    property alias blockListModel :blockListModel           // blocked buddies for this seat
    property alias chatHistoryModel:chatHistoryModel        // List of chat messages in current chatroom session
    property alias sessionListWithBuddyList:sessionListWithBuddyList
    property alias allBuddiesNSessionModel :allBuddiesNSessionModel         // List of buddies in across all chatroom sessions
    property alias allBuddiesNBlockListModel :allBuddiesNBlockListModel
    property alias allChatUsersModel :allChatUsersModel     // List of seat where seat Chat app is active
    property alias allActiveBuddiesModel:allActiveBuddiesModel  // List of all buddies who are acitve and NOT blocked.
    property alias pendingInvitationList:pendingInvitationList
    property alias pendingInvitationListCount:pendingInvitationList.count

    signal sigSeatChatAppReady(int id)      //ID 1 - For App Ready  NOT used need yet
    signal sigJoinPublicSessionStatus(bool status, string sessionId);
    signal sigChatMessageReceived(string seatNum, string session, string message); // you have to listen this signal only fir session Id from signal is equal to currentSessionId.
    signal sigBuddyAdd (string session , string seatNum, string status);
    signal sigUserInfoUpdated(string seatNum, string seatName, int seatState, string seatStatus, string blockAllInvites);
    signal sigMySeatInfoUpdated(string seatNum, string seatName, int seatState, string seatStatus, string blockAllInvites);
    signal sigBuddyExit (string seatNum, string session);
    signal sigSessionAdded(string session)
    signal sigSessionDeleted(string session)
    signal sigSessionListRefreshed()
    signal sigBuddyListRefreshed()
    signal sigAllUsers(int allUsersCount)
    signal sigSessionIdCreatedOnServer(string session)
    signal sigInvitationSent(string session)
    signal sigInvitationReceived(string seatNum, string session, string seatName)
    signal sigSentInvitationStatus(string seatNum, string session, int status) // If next seat Accept Inv. status = 1 and On reject status = 0.
    signal sigCurrentSessionIdChanged(string sessionId);
    signal sigCommunicationError();
    signal sigChatHistoryModelCount(int chatHistoryModelCount)

    function addToQueue(seatNum,seatName,seatStatus,session){
        pendingInvitationList.append({ 'seat':seatNum.toUpperCase(), 'seatName':seatName, 'seatStatus':seatStatus, 'session':session });
        core.info('SeatChat.qml | addToQueue | added to queue pendingInvitationListCount ='+pendingInvitationListCount);
        return true;
    }
    function removeFromQueue(session){
        for(var i=0 ; i< pendingInvitationList.count; i++ )
            if(pendingInvitationList.at(i).session==session)pendingInvitationList.remove(i,1)
        core.info('SeatChat.qml | removeFromQueue | deleted from queue pendingInvitationListCount='+pendingInvitationListCount);
        return true;
    }
    function getCurrentSessionId(){ return Info.CurrentSessionId;}
    function getSeatNumber() { return __seatNumber; }
    function getInvitationQueue(){return Info.InvitationQueue;}
    function getSeatNickname(seatNum) {
        if (seatNum==undefined) return __seatNickname;
        else return (Info.SeatsNum[seatNum.toUpperCase()])?Info.SeatsNum[seatNum.toUpperCase()]['seatName']:"";
    }
    function getSeatStatus(seatNum) {
        if (seatNum==undefined) return __seatStatus;
        else return (Info.SeatsNum[seatNum.toUpperCase()])?Info.SeatsNum[seatNum.toUpperCase()]['seatStatus']:cAvailable;
    }
    function getSeatState() { return __seatState;}
    function getSeatBlockAllInvites() { return __seatBlockAllInvites;}
    function getMaxSessionCount() { return Info.MaxSessionCount;}

    function setAppActive(){
        core.info("SeatChat.qml | setAppActive | __appActive: "+__appActive);
        if (__appActive) return true;
        __appSetActiveCompleted=false
        manager.setActive(pif.getSeatNumber().toUpperCase());
        setCurrentSessionId(0); __seatNickname=""; __seatStatus=cAvailable; __seatState=0; __seatBlockAllInvites=false; Info.SessionCount=0; Info.ActiveChatListUsers=[];
        Info.SeatsNum=[]; __seatNumber="";Info.publicChatRoom=[]; Info.chatMsgCount=[]; Info.InvitationQueue=[]; Info.TempBuddyList=[];Info.__pendingInvites=[];
        __appActive=true;
        return true;
    }

    function getAppActiveState () { return __appSetActiveCompleted&&__appActive?true:false;}
    function setCurrentSessionId(sid){ Info.CurrentSessionId=sid; sigCurrentSessionIdChanged(Info.CurrentSessionId);return true; }
    function sendChatMessage(session,msg){
        msg=__wordFilter(msg);
        manager.sendChatMsg(session,msg);
        if(Info.CurrentSessionId==session)
            __createMsgList(session, pif.getSeatNumber().toUpperCase(),__seatNickname,msg,'');
        return true;
    }

    function getPublicChatRooms(){
        core.info("getPublicChatRooms called")
        var pSessionInfo = manager.getPublicSessions(0,1000); var pSessions = [];
        publicsessionListModel.clear();
        for(var i in pSessionInfo){
            publicsessionListModel.append({"sessionId":manager.getHashValue(pSessionInfo[i],"sessionid"),"sessionName":manager.getHashValue(pSessionInfo[i],"name")})
            Info.publicChatRoom[manager.getHashValue(pSessionInfo[i],"sessionid")]=manager.getHashValue(pSessionInfo[i],"sessionid");
        }
        return true;
    }

    function joinPublicChat(sessionId){
        core.info("joinPublicChat called sessionId: "+sessionId)
        setCurrentSessionId(sessionId);
        manager.joinPublicChat(sessionId);
        chatHistoryModel.clear();
        return true;
    }

    function exitChatSession(session) {
        core.debug("seatChat.qml | exitChatSession | session: "+session);
        if(Info.publicChatRoom[session]) manager.leavePublicChat(session);
        else manager.exitChatSession(session);
        delete Info.__pendingInvites[session];
        chatHistoryModel.clear(); buddyListModel.clear(); setCurrentSessionId(0);
        return true;
    }

    function setSeatName(seatNameStr){
        core.info("seatChat.qml | setSeatName: "+seatNameStr);
        var str=__wordFilter(seatNameStr);
        if(str!=(" "+seatNameStr+" ")) return false;
        core.debug("seatChat.qml | Filtered String: "+escape(seatNameStr));
        manager.setAlias(escape(seatNameStr));
        //__seatNickname=seatNameStr
         Info.TempSeatInfo["seatNickname"]=seatNameStr;
        return true;
    }

    function setSeatState(seatState){ manager.setUserState(String(seatState)); return true;}
    // Supported Constants to be used are listed on top.
    function setSeatStatus (seatStatus){
        var status = seatStatus==cAvailable?cAvailable:seatStatus==cAway?cAway:seatStatus==cDoNotDisturb?cDoNotDisturb:"";
        if (!status) {core.error("seatChat.qml | UnSupported SeatStatus: "+seatStatus); return false;}
        manager.setUserStatus(status);
        Info.TempSeatInfo["seatStatus"]=seatStatus;
        return true;
    }

    function getChatHistory(session) {
        core.info("SeatChat.qml | getChatHistory | session :"+session)
        manager.requestSessionHistory('',session);
    }

    function __getChatHistory(session) {
        core.info("SeatChat.qml | __getChatHistory| session received : "+session)
        chatHistoryModel.clear();
        __updateBuddyListBySessionId(session);
        var from = 0;
        if(Info.chatMsgCount[Info.CurrentSessionId]==undefined)  from=0;
        else from=Info.chatMsgCount[Info.CurrentSessionId]
        var historyInfo = manager.getSessionHistory('',session);
        for(var i in historyInfo) {
            core.info ("seatChat.qml | historyInfo: i: "+i)
            chatHistoryModel.append({"time":'',"seat":manager.getHashValue(historyInfo[i],"seatnumber").toUpperCase(), "seatName":unescape(manager.getHashValue(historyInfo[i],"seatname")), "msg":manager.getHashValue(historyInfo[i],"txtmessage")})
        }
        sigChatHistoryModelCount(chatHistoryModel.count)
    }

    function getBuddyListBySessionId(session){ core.info("seatChat.qml | getBuddyListBySessionId is called..."); __updateBuddyListBySessionId(session); return true; }

    function getAllusers() { manager.refreshAllChatUsers(); return true; }

    function validateSeatNum(seatNum,session){
        //return (Info.SeatsNum[seatStr.toUpperCase()] && (!Info.TempBuddyList[seatStr.toUpperCase()] || session==0)) ? true:  false;

        if(session!=0){
            __updateTempBuddyList(session);
            if(!Info.SeatsNum[seatNum.toUpperCase() ]) {return false;}
            else if (Info.TempBuddyList[seatNum.toUpperCase()]){ return false;}
            else if(!Info.__pendingInvites[session]) { return true;}
            else if (!Info.__pendingInvites[session][seatNum]) { return true;}
            else return false;
        }
        else{
            if(!Info.SeatsNum[seatNum.toUpperCase()] ) return false;
            else if (!Info.__pendingInvites[session]) return true;
            else if (!Info.__pendingInvites[session][seatNum]) return true;
            else return false;
        }
    }

    function sendInvitation(session,seatNumArr){
        core.info("SeatChat.qml | sendInvitation | session: "+session+" seatNumArr: "+seatNumArr)
        Info.__seatNumArr = new Array()
        Info.__seatNumArr = seatNumArr
        if(Info.SessionCount >= Info.MaxSessionCount && session==0 )return false;
        if(session == 0) {
            var sessionId = manager.createNewSession();
            if(sessionId=="") return false;
            session=sessionId;
        }
        else __sendingInvitation(session)
        return true;
    }

    onSigSessionIdCreatedOnServer:{
        core.log('SeatChat | onSessionIdCreatedOnServer | seatNumArr ='+session)
        __sendingInvitation(session)
    }

    function __sendingInvitation(session){
        for (var x in  Info.__seatNumArr){
            core.debug('SeatChat.qml | onSessionIdCreatedOnServer | seatNumArr ='+ Info.__seatNumArr[x])
            manager.sendInvitation(session, Info.__seatNumArr[x])
            if(!Info.__pendingInvites[session])    Info.__pendingInvites[session]=[]
            Info.__pendingInvites[session][Info.__seatNumArr[x]]=Info.__seatNumArr[x]
        }
        sigInvitationSent(session)
    }

    function acceptInvitation(session, seat) { return  (Info.SessionCount <= Info.MaxSessionCount)? manager.acceptInvitation(session, seat.toUpperCase()) | true : false ; }
    function declineInvitation(session,seat) { manager.declineInvitation(session, seat.toUpperCase()); return true;    }

    function clearHistory() {
        Info.chatMsgCount[Info.CurrentSessionId]=manager.getTotalMessageCount(Info.CurrentSessionId);
        core.debug ("seatChat.qml | Info.chatMsgCount["+Info.CurrentSessionId+"]: "+Info.chatMsgCount[Info.CurrentSessionId]);
        chatHistoryModel.clear();
        return true;
    }

    function blockSeat(seatArr){
        core.debug("seatChat.qml | blockSeat | blocked list: "+seatArr.join(','));
        for(var i in seatArr) manager.blockSeat(seatArr[i].toUpperCase());
        return true;
    }

    function unblockSeat(seatArr){
        core.debug("seatChat.qml | unblockSeat list: "+seatArr.join(','));
        for(var i in seatArr) manager.unblockSeat(seatArr[i].toUpperCase())
        return true;
    }

    function blockInvites(){ __seatBlockAllInvites=true; manager.blockInvites(); return true;}

    function unblockInvites(){ __seatBlockAllInvites=false; manager.unblockInvites(); return true;}

    function getTotalBuddyCount(sessionId){
        // This function have to test on rack. As per splat it should return total buddy count. But yet not sure this count will including own  seat or not.
        return manager.getTotalBuddyCount(sessionId);
    }

    function exitChat() {
        Info.SessionCount=0; manager.exitChat();  setCurrentSessionId(0); sessionListModel.clear(); publicsessionListModel.clear(); buddyListModel.clear();
        blockListModel.clear(); chatHistoryModel.clear(); sessionListWithBuddyList.clear();pendingInvitationList.clear();
        Info.publicChatRoom=[]; Info.chatMsgCount=[]; Info.InvitationQueue=[]; Info.__pendingInvites=[];Info.TempSeatInfo=[];
        allChatUsersModel.clear(); allBuddiesNSessionModel.clear(); __appActive=false; return true;
    }

    function getRestrictedWords () {    // return "hi,hello,this,is"
        return Info.RestrictedWords.split("|").join(",");
    }

    function setRestrictedWords (wordstring) {  // Input: "hi,hello,this,is"
        Info.RestrictedWords = wordstring?wordstring.split(" ").join("").split(",").join("|"):"";   // trim spaces & replacing comma with |
        core.info("SeatChat | setRestrictedWords called | RestrictedWords: "+Info.RestrictedWords);
        return true;
    }

    function __processSeatStatus (status) { return status==cAway?cAway:status==cDoNotDisturb?cDoNotDisturb:cAvailable;}

    function __handleChatEvent(event,eventInfo) {
        if(!__appActive) return false;
        var responseStatus = manager.getHashValue(eventInfo,"responsestatus");
        var seatNum = manager.getHashValue(eventInfo,"seatnumber").toUpperCase();
        var seatName = unescape(manager.getHashValue(eventInfo,"seatname"));
        var session = manager.getHashValue(eventInfo,"sessionid");
        var message = manager.getHashValue(eventInfo,"txtmessage");
        var seatStatus= __processSeatStatus(manager.getHashValue(eventInfo,"status"));
        var seatState= manager.getHashValue(eventInfo,"state");
        seatState = String(parseInt(seatState,10))!="NaN"?parseInt(seatState,10):0;
        var blockAllInvites= manager.getHashValue(eventInfo,"blockallinvites");
        core.info("SeatChat.qml | __handleChatEvent | seatNum: "+seatNum +"  Event: " + event +"  Session: "+session)

        switch(event) {
        case 0:{    // ## onchatinvitationreceived: sessionid, seatnumber, seatname
            sigInvitationReceived(seatNum,session,seatName)
        }
        break;
        case 1:{    // ## onchatinvitationaccepted: sessionid, seatnumber
            //__updateSessionListData();
            sigSentInvitationStatus(seatNum,session,true); __removeFromPendingList (Info.__pendingInvites[session],seatNum, session)
        }
        break;
        case 2:{    // ## onnewsigChatMessageReceived: sessionid, seatnumber, seatname, txtmessage, time
            if(Info.CurrentSessionId==session)
                __createMsgList(session, seatNum,seatName, message,manager.getHashValue(eventInfo,"time"));
            sigChatMessageReceived(seatNum, session, message);
        }
        break;
        case 3:{    // ## onsigBuddyAdded: sessionid, seatnumber, seatname
            if(!Info.ActiveChatListUsers[session]) Info.ActiveChatListUsers[session]= [];
            Info.ActiveChatListUsers[session][seatNum]=seatNum;
            __updateBuddyListBySessionId(session);
            sigBuddyAdd (session, seatNum, seatName);
        }
        break;
        case 4:{    // ## onbuddydeleted: sessionid, seatnumber, seatname
            __updateBuddyListBySessionId(session); sigBuddyExit(seatNum,session); __removeFromPendingList (Info.__pendingInvites[session],seatNum, session);
        }
        break;
        case 5:{    // ## onuserinfoupdated: sessionid, seatnumber, seatname, state, status
            if(pif.getSeatNumber().toUpperCase()==seatNum.toUpperCase()){
                __seatNumber=seatNum;__seatNickname=seatName; __seatStatus=seatStatus; __seatState= seatState; //__seatBlockAllInvites=blockAllInvites==1?true:false;
                sigMySeatInfoUpdated(seatNum, seatName, seatState, seatStatus, __seatBlockAllInvites);
            } else {
                __updateBuddyListBySessionId(session);
                sigUserInfoUpdated(seatNum, seatName, seatState, seatStatus, blockAllInvites==1?true:false);
            }
        }
        break;
        case 6:{    // ## onsessionadded: sessionid
            Info.SessionCount=Info.SessionCount+1;
            sigSessionAdded(session)
            __updateSessionListData();
        }
        break;
        case 7:{    // ## onsessiondeleted: sessionid
            __updateSessionListData();
            sigSessionDeleted(session);
        }
        break;
        case 8:{    // ## onallsessionsrefreshed: None
            __updateSessionListData();
            if (Info.CurrentSessionId) {
                __updateBuddyListBySessionId(Info.CurrentSessionId);
                sigSessionListRefreshed();
            }
            if (!__appSetActiveCompleted) { __appActive=true; __appSetActiveCompleted=true; sigSeatChatAppReady(1); }
        }
        break;
        case 9:{    // ## onallbuddylistsrefreshed:
            if (Info.CurrentSessionId) {
                __updateBuddyListBySessionId(Info.CurrentSessionId);
                sigBuddyListRefreshed();
            }
        }
        break;
        case 10:{   // ## onbuddylistsrefreshed: sessionid
            if (Info.CurrentSessionId) {
                __updateBuddyListBySessionId(session);
                sigBuddyListRefreshed();
            }
        }
        break;
        case 11:{ getChatHistory(Info.CurrentSessionId); } break;   //None // ## onalltextmessagesrefreshed
        case 12:{ getChatHistory(session); } break; //sessionid // ## ontextmessagesrefreshed
        /* case 13:{//None // ## onchatactive } break; case 14:{//None //  ## onchatinactive } break; */
        case 15:{   // ## onbuddyblocked: seatnumber, seatname
            if (Info.CurrentSessionId) __updateBuddyListBySessionId(Info.CurrentSessionId);
            __updateBlockList();
        } break;
        case 16:{   // ## onbuddyunblocked: seatnumber, seatname
            if (Info.CurrentSessionId) __updateBuddyListBySessionId(Info.CurrentSessionId);
            __updateBlockList();
        } break;
        case 17:{
            if (Info.CurrentSessionId) __updateBuddyListBySessionId(Info.CurrentSessionId);
            __updateBlockList();
        } break;//None // ## onblockedlistrefreshed
        case 18:{
            //seatnumber, seatname, state , status, blockallinvites // ## onusrprofileupdated
            if(pif.getSeatNumber().toUpperCase()==seatNum.toUpperCase()){
                __seatNumber=seatNum;__seatNickname=seatName; __seatStatus=seatStatus; __seatState= seatState; __seatBlockAllInvites=blockAllInvites=='yes'?true:false;
                sigMySeatInfoUpdated(seatNum, seatName, seatState, seatStatus, __seatBlockAllInvites);
            } else {
                __updateBuddyListBySessionId(session);
                sigUserInfoUpdated(seatNum, seatName, seatState, seatStatus, blockAllInvites==1?true:false);
            }
        }
        break;
        case 19:{ sigSentInvitationStatus(seatNum,session,false); __removeFromPendingList (Info.__pendingInvites[session],seatNum, session);} break;//seatnumber, sessionid // ## onchatinvitationrejected
        case 20:{
            sigSessionIdCreatedOnServer(session)
        } break;//sessionid // ## onsessionready
        case 21:{
            getChatHistory(session);
            sigJoinPublicSessionStatus(true,session);
        }
        break;//sessionid,sessionname // ## onpublicsessionjoinsuccess
        case 22:{ sigJoinPublicSessionStatus(false,session);  } break;         //sessionid, onpublicsessionjoinfailure
        case 23:{//sessionid // ## onpublicsessionexitsuccess
            __updateBuddyListBySessionId(session);
            publicsessionListModel.clear();
            chatHistoryModel.clear();
            buddyListModel.clear();
            sigExitPublicSessionStatus(true,session);
        }
        break;
        case 24:{ sigExitPublicSessionStatus(false,session) } break;//sessionid // ## onpublicsessionexitfailure
        case 25:{ __getAllUsers(); sigAllUsers(allChatUsersModel.count); } break;//None // ## onallchatusersrefreshed

        case 27:{
            core.info("SeatChatNew.qml | EVENT onupdusraliasresponded Num 27 responseStatus:" + responseStatus);
            if(responseStatus=="OK"){
                __seatNickname=Info.TempSeatInfo["seatNickname"];
                core.info("__seatNickname is "+ __seatNickname);
                Info.SeatsName[__seatNumber] = __seatNickname;
                sigMySeatInfoUpdated(__seatNumber, __seatNickname, seatState, __seatStatus, __seatBlockAllInvites, __seatAvatar);
            }
        }
        break;

        case 29:{
            core.info("SeatChatNew.qml | EVENT onupdusrstatusresponded Num 29 responseStatus: "+responseStatus);
            if(responseStatus=="OK"){
               __seatStatus=Info.TempSeatInfo["seatStatus"];
               core.info("__seatStatus is "+ __seatStatus);
               sigMySeatInfoUpdated(__seatNumber, __seatNickname, seatState, __seatStatus, __seatBlockAllInvites, __seatAvatar);
            }
        }
        break;

        case 31:{sigCommunicationError()}
        case 32:{
            if (Info.CurrentSessionId==session)
                __getChatHistory(session)
        }
        }
    }

    function __updateSessionListData()    {
        core.info("SeatChat.qml | __updateSessionListData | Started");
        sessionListModel.clear()
        Info.SessionCount=0;
        var sessionlist = manager.getUsrSessions(0,1000);
        for ( var i in sessionlist){
            core.debug("SeatChat.qml | __updateSessionListData | Ended sessionlist["+i+"] ="+manager.getHashValue(sessionlist[i],"sessionid") )
            sessionListModel.append( { "session": manager.getHashValue(sessionlist[i],"sessionid")  } )
            Info.SessionCount=Info.SessionCount+1
        }
        core.debug("SeatChat.qml | __updateSessionListData | Ended sessionListModel.count ="+sessionListModel.count)
        return Info.SessionCount;
    }

    function __getBuddyList(session){
        var sessionParticipantCount = manager.getSessionParticipantCount("",session);
        core.info("SeatChat.qml | __getBuddyList | session :"+session+", sessionParticipantCount : "+sessionParticipantCount)
        if(sessionParticipantCount<=0) return [];
        else var sessionParticipantList = manager.getSessionParticipantList("",session, 0, sessionParticipantCount-1);
        return sessionParticipantList;
    }

    function __updateBuddyListBySessionId(session)   {
        core.info("seatChat.qml | __updateBuddyListBySessionId | currentSessionId="+Info.CurrentSessionId +" From Evnt session="+session)
        if(session!=Info.CurrentSessionId) return false;
        var status;
        buddyListModel.clear();
        var buddies = __getBuddyList(session);
        core.debug("seatChat.qml | __updateBuddyListBySessionId |  My seatnumber: "+pif.getSeatNumber().toUpperCase()+" buddies.length="+ buddies.length)
        if(buddies.length==0) return false;
        for(var i in buddies) {
            core.debug("seatChat.qml | __updateBuddyListBySessionId |  i: "+i+" manager.getHashValue: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase());
            if(pif.getSeatNumber().toUpperCase() !=manager.getHashValue(buddies[i],"seatnumber").toUpperCase() && manager.getHashValue(buddies[i],"seatnumber")!=""){
                status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                core.debug("seatChat.qml | __updateBuddyListBySessionId | seatnum: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase() +" seatname: "+manager.getHashValue(buddies[i],"seatname") +" seatstate: "+manager.getHashValue(buddies[i],"state") +" seatstatus: "+status +" blockstatus: "+manager.getHashValue(buddies[i],"blockstatus"));
                buddyListModel.append({"seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                          "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                          "seatstate":manager.getHashValue(buddies[i],"state"),
                                          "seatstatus":status,
                                          "blockstatus":manager.getHashValue(buddies[i],"blockstatus")
                                      })
                __updateTempBuddyList(session);// Info.TempBuddyList[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
            }
        }
        //__updateSessionListWithBuddyData();
        core.debug("seatChat.qml | __updateBuddyListBySessionId |  buddyListModel.count="+ buddyListModel.count)
        return true;
    }

    function __updateTempBuddyList(session){
        var buddies = __getBuddyList(session);
        Info.TempBuddyList=[];
        core.debug("seatChat.qml | __updateTempBuddyList |  My seatnumber: "+pif.getSeatNumber().toUpperCase()+" buddies.length="+ buddies.length)
        for(var i in buddies) {
            core.debug("seatChat.qml | __updateTempBuddyList |  i: "+i+" manager.getHashValue: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase());
            if(manager.getHashValue(buddies[i],"seatnumber")!=""){
                var status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                Info.TempBuddyList[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
            }
        }
    }



    function getAllBuddiesNSessionList(){
        core.debug("seatChat.qml | getAllBuddiesNSessionList called");
        allBuddiesNSessionModel.clear()
        var status;
        for(var j = 0; j < Info.SessionCount; j++) {
            core.debug("seatChat.qml | getAllBuddiesNSessionList | private chatsession name: "+sessionListModel.get(j).session);
            var buddies = __getBuddyList(sessionListModel.get(j).session)
            for(var i in buddies) {
                if(pif.getSeatNumber().toUpperCase() !=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
                        && manager.getHashValue(buddies[i],"seatnumber")!="" ){
                    status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                    core.debug("seatChat.qml | getAllBuddiesNSessionList | seatnum: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase() +" session: "+sessionListModel.get(j).session +" seatname: "+manager.getHashValue(buddies[i],"seatname") +" seatstate: "+manager.getHashValue(buddies[i],"state") +" seatstatus: "+status +" blockstatus: "+manager.getHashValue(buddies[i],"blockstatus"));
                    allBuddiesNSessionModel.append({ "session":sessionListModel.get(j).session,
                                                       "seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                                       "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                                       "seatstate":manager.getHashValue(buddies[i],"state"),
                                                       "status":manager.getHashValue(buddies[i],"status"),
                                                       "blockstatus":manager.getHashValue(buddies[i],"blockstatus") })
                }

            }
        }
        return true;
    }

    function getallBuddiesNBlockList(){
        core.debug("getallBuddiesNBlockList called");
        allBuddiesNBlockListModel.clear();
        allActiveBuddiesModel.clear();
        var tmpSeatNumber=[]; var status;
        for(var j = 0; j < Info.SessionCount; j++) {
            core.debug("getallBuddiesNBlockList | getallBuddiesNBlockList | private chatsession name: "+sessionListModel.get(j).session);
            var buddies = __getBuddyList(sessionListModel.get(j).session)
            for(var i in buddies) {
                if(pif.getSeatNumber().toUpperCase() !=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
                        && manager.getHashValue(buddies[i],"seatnumber")!=""
                        && !tmpSeatNumber[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]){
                    status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                    core.debug("seatChat.qml | getallBuddiesNBlockList | seatnum: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase()+" session: "+sessionListModel.get(j).session+" seatname: "+manager.getHashValue(buddies[i],"seatname")+" seatstate: "+manager.getHashValue(buddies[i],"state") +" seatstatus: "+status +" blockstatus: "+manager.getHashValue(buddies[i],"blockstatus"));
                    allBuddiesNBlockListModel.append({  "session":sessionListModel.get(j).session,
                                                         "seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                                         "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                                         "seatstate":manager.getHashValue(buddies[i],"state"),
                                                         "status":manager.getHashValue(buddies[i],"status"),
                                                         "blockstatus":manager.getHashValue(buddies[i],"blockstatus") })
                    if(manager.getHashValue(buddies[i],"blockstatus") != 1)
                        allActiveBuddiesModel.append({   "seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                                         "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                                         "seatstate":manager.getHashValue(buddies[i],"state"),
                                                         "status":manager.getHashValue(buddies[i],"status")})
                    tmpSeatNumber[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=1;
                }

            }
        }
        for(var j in Info.publicChatRoom) {
            core.debug("SeatChat.qml | getallBuddiesNBlockList | public chatsession name: "+j);
            var buddies = __getBuddyList(j);
            for(var i in buddies) {
                core.debug("seatChat.qml | getallBuddiesNBlockList |  i: "+i+" manager.getHashValue: "+manager.getHashValue(buddies[i],"seatnumber"));
                if(pif.getSeatNumber().toUpperCase() !=manager.getHashValue(buddies[i],"seatnumber").toUpperCase()
                        && manager.getHashValue(buddies[i],"seatnumber")!=""
                        && !tmpSeatNumber[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]){
                    status = __processSeatStatus(manager.getHashValue(buddies[i],"status"));
                    core.debug("seatChat.qml | getallBuddiesNBlockList | seatnum: "+manager.getHashValue(buddies[i],"seatnumber").toUpperCase() +" session: "+j +" seatname: "+manager.getHashValue(buddies[i],"seatname") +" seatstate: "+manager.getHashValue(buddies[i],"state") +" seatstatus: "+status +" blockstatus: "+manager.getHashValue(buddies[i],"blockstatus"));
                    allBuddiesNBlockListModel.append({ "session":j,
                                                         "seatnum":manager.getHashValue(buddies[i],"seatnumber").toUpperCase(),
                                                         "seatname":unescape(manager.getHashValue(buddies[i],"seatname")),
                                                         "seatstate":manager.getHashValue(buddies[i],"state"),
                                                         "seatstatus":status,
                                                         "blockstatus":manager.getHashValue(buddies[i],"blockstatus")
                                                     })
                    tmpSeatNumber[manager.getHashValue(buddies[i],"seatnumber").toUpperCase()]=1;
                }
            }
        }
        var blockList = manager.getBlockedUserList(0,500)
        core.debug("seatChat.qml | getallBuddiesNBlockList | Adding Blocked Users that are not part of any chatroom sessions")
        for(var i in blockList) {
            core.debug("seatChat.qml | getallBuddiesNBlockList | blockList["+i+"] = "+manager.getHashValue(blockList[i],"seatnumber").toUpperCase());
            if( !tmpSeatNumber[manager.getHashValue(blockList[i],"seatnumber").toUpperCase()]) {
                core.debug("seatChat.qml | getallBuddiesNBlockList | seatnum: "+manager.getHashValue(blockList[i],"seatnumber").toUpperCase()
                           +" session: 0 seatname: seatstate: seatstatus: "+cAvailable
                           +" blockstatus: 1");
                allBuddiesNBlockListModel.append({ "session":"0", "seatnum":manager.getHashValue(blockList[i],"seatnumber").toUpperCase(),"seatname":"","seatstate":"","status":cAvailable,"blockstatus":"1" })
            }
        }
        return true;
    }

    function __updateBlockList() {
        blockListModel.clear()
        var blockList = manager.getBlockedUserList(0,500);
        for(var i in blockList)
        {
            core.info("&&&&&&&&&&&&& | BlockSeat"+manager.getHashValue(blockList[i],"seatnumber").toUpperCase()+"status: "+manager.getHashValue(blockList[i],"status"))
            blockListModel.append( {"seat": manager.getHashValue(blockList[i],"seatnumber").toUpperCase(),"seatname":manager.getHashValue(blockList[i],"seatname"), "seatStatus":manager.getHashValue(blockList[i],"status").toString()})

        }

        return true;
    }

    function __createMsgList(session, seatNum,seatName, message,msgTime){
        core.info("seatChat.qml | __createMsgList | seatNum: "+seatNum+" Message: "+message);
        chatHistoryModel.append({"time":msgTime,"seat":seatNum.toUpperCase(), "seatName":unescape(seatName), "msg":message})
        return true;
    }

    function __wordFilter(text) {
        if (!Info.RestrictedWords) return " "+text+" ";
        core.debug ("SeatChat.qml | __wordFilter | Info.RestrictedWords: "+Info.RestrictedWords);
        var myRegExp = '((?=\\W)|\\b)('+Info.RestrictedWords.replace(/([^\w\|])/g, '\\$1')+')((?=\\s)|\\b)'
        var regFilter = new RegExp(myRegExp, 'gim');
        return (" "+text+" ").replace(regFilter, function (word){
                                          return word.replace(/./g, '*');
                                      });
    }

    function __getAllUsers (){
        allChatUsersModel.clear();
        Info.SeatsNum = [];
        var allChatUser= manager.getAllChatUsers(); var seatno;
        for(var i in allChatUser) {
            seatno = manager.getHashValue(allChatUser[i],"seatnumber").toUpperCase();
            Info.SeatsNum[seatno] = {
                "seatno":seatno,
                "seatName":manager.getHashValue(allChatUser[i],"seatname"),
                "seatStatus":manager.getHashValue(allChatUser[i],"status")
            }
            allChatUsersModel.append({'seat':manager.getHashValue(allChatUser[i],"seatnumber").toUpperCase(), 'seatname':unescape(manager.getHashValue(allChatUser[i],"seatname")),'seatstate':manager.getHashValue(allChatUser[i],"state"), 'status':__processSeatStatus(manager.getHashValue(allChatUser[i],"status")),'blockstatus':manager.getHashValue(allChatUser[i],"blockstatus") })
        }
        core.info("__getAllUsers allChatUsersModel.count: "+allChatUsersModel.count);
        return true;
    }

    function __removeFromPendingList(arr,seatNum,session){seatNum=seatNum.toUpperCase(); for(var x in arr) { if(seatNum==Info.__pendingInvites[session][seatNum]){ delete Info.__pendingInvites[session][seatNum]; break; } } }

    Component.onCompleted: {
        if (core.pc) manager = Qt.createQmlObject('import QtQuick 1.1;import "../testpanel"; ChatManagerSIM{onChatEvent:__handleChatEvent(event,eventInfo);}',seatChat);
        else manager = chatManagerComp.createObject(seatChat);
        core.info ("seatChat.qml | Chat Manager Reference: "+manager);
        setRestrictedWords(pif.__getRestrictedWords());       // "hi,hello,this,is"
        Info.MaxSessionCount= pif.__getMaxSessionCount();
        Info.TempSeatInfo=[];
    }

    ListModel {id: sessionListModel}
    ListModel {id: publicsessionListModel}
    ListModel {id: buddyListModel}
    ListModel {id: blockListModel}
    ListModel {id: chatHistoryModel}
    ListModel {id: sessionListWithBuddyList}
    ListModel {id:allChatUsersModel}
    ListModel {id:allBuddiesNSessionModel}
    ListModel {id:allActiveBuddiesModel}
    SimpleModel {id:allBuddiesNBlockListModel}
    SimpleModel {id: pendingInvitationList }

    Component {
        id: chatManagerComp;
        ChatManager {
            id: manager
            chatServerIp: core.settings.remotePath.split("/")[2];
            gatewayIp: core.settings.remotePath.split("/")[2];
            gatewayPort: "57777";
            listenPort: "57777";
            onChatEvent: __handleChatEvent(event,eventInfo);
        }
    }
}
