import QtQuick 1.1
import Panasonic.Pif 1.0

Item {
    property int ctPmid;
    property int ctTimeout;
    property string ctMessage;
    property string ctMessageTitle;
    property int ctMid;
    property string ctCommand;
    property string ctMandatory;
    signal ctToSeatMsgReceived();

    function ctMsgDataReady(dModel){
        ctMessage = dModel.getValue(0,"seat_message_text");
        ctMessageTitle = dModel.getValue(0,"seat_message_title")?dModel.getValue(0,"seat_message_title"):"";
        ctMid = dModel.getValue(0,"seat_message_mid");
        ctCommand = dModel.getValue(0,"seat_message_command");
        ctMandatory = dModel.getValue(0,"seat_message_mandatory")
        ctToSeatMsgReceived();
    }
    Connections{
        target: pif
        onSigCtMessageReceived:{
            if(ctMessageType==1){
                ctXmlParser.text = ctMsgString;
                ctPmid = parseInt(ctXmlParser.query("/msg/@pmid/string()"));
                ctTimeout = parseInt(ctXmlParser.query("/msg/@timeout/string()"));
                core.log("ctMessage | pmid: "+ctPmid+" timeout: "+ctTimeout);
                if(ctPmid){
                    core.log("ctMessage | predefined message ");
                    dataController.getCtToSeatMessage([ctPmid],ctMsgDataReady);
                }else{
                    core.log("ctMessage | custom message ");
                    ctMessage = ctXmlParser.query("/msg/string()");
                    ctMessageTitle = ""
                    ctToSeatMsgReceived();
                }
            }
        }
    }
    Xml{ id:ctXmlParser;}
}
