import QtQuick 1.1
import Panasonic.Pif 1.0

Item {
    id: phoneInterface;
    // Constants
    property int cSEAT_TO_SEAT_CALL:1;
    property int cSEAT_TO_GRD_CALL:2;

    //TELEPHONY ERROR CODES.
    property int cSEAT_TO_GROUND_SERVICE_NOT_AVAILABLE:1;
    property int cAIR_TO_GROUND_CONNECTION_FAILED:2;
    property int cCALLING_GROUND_NUMBER_FAILS:3;
    property int cNUMBER_BUSY:4;
    property int cCALLING_SEAT_NUMBER_FAILS:5;
    property int cSEAT_TO_SERVICE_SERVICE_NOT_AVAILABLE:6;

    //Public
    property variant deviceManager;
    property variant telephony;
    property variant pifInput;
    property bool permitIncomingCallValue:false;
    property string seatNumber:"";
    property variant groundNumber:"";			// Dialed number
    property bool seat2SeatAvailable:false;
    property bool seat2GroundAvailable:false;
    property bool incomingCall:false;		//Default value: 1- There is an incoming call
    property bool callInProgress:false;    // Set when call is made and when call is accepted.
    property string incomingPhoneNumber:"";		// Phone no of Incoming Call
    property int queuePosition:-1;
    property int outgoingCallMade:0;
    property variant volumeLevel;
    property bool authenticationComplete:false;
    property string last4Digits:telephony.last4Digits;

    // Signals
    signal sigIncomingCall(variant number);
    signal sigConnectedEvent();
    signal sigCardSwiped();
    signal sigDisconnectedEvent();
    signal sigCallReleaseEvent();
    signal sigSeatToSeatCallFailed(int errorCode);
    signal sigAirToGroundCallFailed(int errorCode);
    signal sigAuthenticationError();
    signal sigRequestCreditCardSwipe();
    signal sigAuthenticationComplete();
    signal sigPhoneQueuePosition(int position);
    signal sigAirToGroundCallAvailable();
    signal sigSeatToSeatCallAvailable();

    onVolumeLevelChanged:{core.info("telephone.qml | telephony Volume Level is " + volumeLevel);}

    function initialiseComponent(){
        telephony = telephoneComp.createObject(phoneInterface,{});
        volumeLevel = telephony.volumeLevel;
    }

    function getPhoneServiceStatus(callType){
        core.info("telephone.qml | GET PHONE SERVICE STATUS CALLED FOR "+callType);
        if(callType!=undefined||callType!=""){outgoingCallMade=callType;}
        if(core.pc){__onPhoneServiceStatusAck(0,1);return ;}
        telephony.requestPhoneServiceStatus();
    }

    function getCardLastFourdigits(){if(authenticationComplete){return last4Digits;}else{ return false;}}

    function initiateConnection(telephoneNumber){
        var result;
        core.info("telephone.qml | initiateConnection ");
        if(!telephoneNumber || telephoneNumber=="") {
            core.info("telephony.qml | number not provided,hence exiting");
            return;
        }
        groundNumber=telephoneNumber;
        telephony.groundNumber=telephoneNumber;
        result=telephony.initiateConnection();
        if(result){
            callInProgress=true;
            core.info("telephone.qml | initiateConnection SUCCESSFUL");
        }
        else
            core.info("telephone.qml | initiateConnection FAILED");
        return result;
    }

    function makeGroundCall(telephoneNumber){
        var result;core.info("telephone.qml | MAKE GROUND CALL INITIATED. PHONE NUMBER :"+telephoneNumber);
        if(callInProgress==true){core.error ("telephone.qml | ANOTHER CALL IS IN PROGRESS. EXITING...");return 2;}
        if(telephoneNumber==undefined||telephoneNumber==""||telephoneNumber==0){core.error("telephone.qml | PHONE NUMBER NOT VALID. EXITING");return 3;}
        groundNumber=telephoneNumber;telephony.groundNumber=telephoneNumber;
        result=telephony.callGroundNumber(groundNumber);
        if(result){
            if(authenticationComplete){
                result=telephony.initiateConnection();
                if(result==false){
                    if(core.pc){__onConnectedPhoneAck();}
                    else {sigAirToGroundCallFailed(cAIR_TO_GROUND_CONNECTION_FAILED);}
                }
                else {callInProgress=true; return 0;}
            }
            else {return 0;}
        }
        else{
            if(core.pc){__onAuthenticationInputNeededAck();return 0;}
            else{sigAirToGroundCallFailed(cCALLING_GROUND_NUMBER_FAILS);return 4;}
        }
    }

    function makeGroundCallFree(telephoneNumber){
        var result;core.info("telephone.qml | MAKE GROUND CALL INITIATED. PHONE NUMBER :"+telephoneNumber);
        if(callInProgress==true){core.error ("telephone.qml | ANOTHER CALL IS IN PROGRESS. EXITING...");return 2;}
        if(telephoneNumber==undefined||telephoneNumber==""||telephoneNumber==0){core.error("telephone.qml | PHONE NUMBER NOT VALID. EXITING");return 3;}
        groundNumber=telephoneNumber;telephony.groundNumber=telephoneNumber;
        result=telephony.callGroundNumberFree(groundNumber);
        if(result){callInProgress=true; return 0;}
        else{
            if(core.pc){__onConnectedPhoneAck();return 0;}
            else{sigAirToGroundCallFailed(cCALLING_GROUND_NUMBER_FAILS);return 4;}
        }
    }

    function requestCardSwipe(){
        var result;core.info("telephone.qml | REQUEST CARD SWIPE CALLED.");
        if(callInProgress==true){core.error("telephone.qml | ANOTHER CALL IS IN PROGRESS. EXITING...");return 1;}
        result=telephony.callGroundNumber();
        if(result){return 0;}
        else {
            if(core.pc){__onAuthenticationInputNeededAck();return 0;}
            else{sigAirToGroundCallFailed(cCALLING_GROUND_NUMBER_FAILS);return 2;}
        }
    }

    function makeSeatCall(seatNum){
        var result;core.info("telephone.qml | MAKE SEAT CALL INITIATED. SEAT NUMBER:"+seatNum);
        if(callInProgress==true){core.error("telephone.qml | ANOTHER CALL IS IN PROGRESS. EXITING...");return 1;}
        if(seatNum==undefined||seatNum==""||seatNum == 0){core.error("telephone.qml | SEAT NUMBER NOT VALID. EXITING...");return 2;}
        result=core.coreHelper.validateSeatNoForApps(seatNum);
        if(!result){core.error("telephone.qml | SEAT NUMBER IS NOT VALID.");return 2;}
        seatNumber=seatNum;telephony.seatNumber=seatNumber;
        result=telephony.callSeatNumber();
        if(result){callInProgress=true;core.info("telephone.qml | MAKE SEAT CALL SUCCESSFUL");return 0;}
        else {
            if(core.pc){callInProgress=true;core.error("telephone.qml | MAKE SEAT CALL SUCCESSFUL");return 0;}
            else {core.error("telephone.qml | MAKE SEAT CALL FAILED");sigSeatToSeatCallFailed(cCALLING_SEAT_NUMBER_FAILS);return 3;}
        }
    }

    function confirmCall(){
        var ret;core.info("telephone.qml | CONFIRM CALL CALLED");
        ret=telephony.confirmQueuedPhoneCall();
        core.info("telephone.qml | CONFIRM CALL RETURNS  "+ret);
        if(ret == false){core.error ("telephone.qml | CONFIRM CALL FAILED");}
        else {core.info("telephone.qml | CONFIRM CALL SUCCESSFUL");}
    }

    function acceptCall(){
        var result;core.info("telephone.qml | ACCEPT INCOMING CALL");
        result=telephony.acceptIncomingCall();
        if(result){callInProgress=true;core.info("telephone.qml | ACCEPT INCOMING CALL SUCCESSFUL");return true;}
        else {
            if(core.pc){callInProgress=true;core.info("telephone.qml | ACCEPT INCOMING CALL SUCCESSFUL");return true;}
            else{core.info("telephone.qml | ACCEPT INCOMING CALL FAILED");return false;}
        }
    }

    function rejectCall(){
        var result;core.info("telephone.qml | REJECT CALL.");
        result=telephony.rejectIncomingCall();
        if(result){core.info("telephone.qml | REJECT CALL SUCCESSFUL");return true;}
        else {
            if(core.pc){core.error("telephone.qml | REJECT CALL SUCCESSFUL"); return true;}
            else{core.info("telephone.qml | REJECT CALL FAILED");return false;}
        }
    }

    function permitIncomingCall(value){
        core.info("telephone.qml | PERMIT INCOMING CALL. value : "+value);
        if(value==undefined){core.error ("telephone.qml | VALUE IS UNDEFINED.EXITING");return 0;}
        permitIncomingCallValue=value;
    }

    function getIncomingNumber(){}

    function endCall(){
        var result;core.info("telephone.qml | END CALL INITIATED. | CALLINPROGRESS IS "+callInProgress);
        if(callInProgress==true){
            result=telephony.endCall();
            if(result==false){
                if(core.pc){core.error ("telephone.qml | END CALL SUCCESSFUL");return true;}
                else {core.error("telephone.qml | END CALL FAILS.");return false;}
            }
            else {core.info("telephone.qml | END CALL SUCCESSFUL.");callInProgress=false;return true;}
        }
    }

    function phoneVolumeUp(){
        var result;core.info("telephone.qml | PHONE VOLUME UP CALLED");
        result=telephony.setVolumeUp();
        if(result==false){core.error("telephone.qml | PHONE VOLUME UP FAILS.");return false;}
        else{core.info("telephone.qml | PHONE VOLUME UP SUCCESS.");return true;}
    }

    function phoneVolumeDown(){
        var result;core.info("telephone.qml | PHONE VOLUME DOWN CALLED");
        result=telephony.setVolumeDown();
        if(result==false){core.error("telephone.qml | PHONE VOLUME DOWN FAILS.");return false;}
        else{core.info("telephone.qml | PHONE VOLUME DOWN SUCCESS.");return true;}
    }

    function setPhoneVolumeByValue(value){
        core.info("telephone.qml | Set Phone Volume by value called. value:"+value);if(value==undefined && value==0){core.info("value not proper.");return false;}
        telephony.volumeLevel = value;volumeLevel = telephony.volumeLevel;core.info("telephone.qml | Volume set: "+volumeLevel);return true;
    }

    function getVolumeLevel(){core.info("telephone.qml | GET VOLUME LEVEL CALLED");return volumeLevel;}

    /*
        function sendDTMFtone()
        Call this function to send the DTMF tone.
        Param:
        numpadKey: The numpad key string. Possible values are
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '#'.
        Return:
        True: the PIf function is called successfully.
        False: the PIf function is not called.
    */
    function sendDTMFtone(numpadKey){
        var validCharacters="0123456789*#";core.info("telephone.qml | SEND DTMF TONE CALLED NUMPAD KEY IS "+numpadKey);
        if (numpadKey==undefined){core.error("telephone.qml | NUMPAD KEY IS NOT PASSED" );return false;}
        if(validCharacters.indexOf(numpadKey) > 0){
            core.info("telephone.qml | NUMPAD KEY IS VALID");telephony.playTone(numpadKey);return true;
        }
        else {core.error("telephone.qml | NUMPAD KEY IS NOT VALID" ); return false;}
    }

    function frmwkResetApp(){
        core.info("telephone.qml | RESET APP CALLED");incomingPhoneNumber="";telephony.endCall();callInProgress=false;
        incomingCall=false;queuePosition=-1;groundNumber="";seatNumber="";outgoingCallMade=0;incomingPhoneNumber="";authenticationComplete=false;
    }

    function frmwkInitApp(){core.info("telephone.qml | INITIALISE APP CALLED");permitIncomingCall(pif.getPermitIncomingCall());}

    //Private
    function __getCallInProgressState(){return callInProgress;}
    function __setCallInProgressState(e){callInProgress = e;}

    function __onIncomingPhoneAck(number){
        core.info("telephone.qml | ON-INCOMING-PHONE RECEIVED INCOMING NUMBER "+number);
        incomingCall=true;incomingPhoneNumber=number;sigIncomingCall(number);
    }

    function __onQueuedPhoneAck(){
        core.info("telephone.qml | ON-QUEUED-PHONE RECEIVED QUEUE POSITION "+telephony.queuedPhonePosition);
        if(telephony.queuedPhonePosition < 0){core.error ("telephone.qml | QUEUE POSITION IS NEGATIVE. DISCARDING");}
        else if(telephony.queuedPhonePosition == 0){}
        else{sigPhoneQueuePosition(telephony.queuedPhonePosition);}
    }

    function __onConnectedPhoneAck(){
        core.info("telephone.qml | ON-CONNECTED-PHONE RECEIVED");
        pifInput.volumeKeysEnabled=true;
        setPhoneVolumeByValue(pif.getDefaultTelephonyVolume());
        if(outgoingCallMade==cSEAT_TO_GRD_CALL)telephony.toneEnabled=true;
        sigConnectedEvent();
    }

    function __onDisconnectedPhoneAck(){core.info("telephone.qml | ON-DISCONNECTED-PHONE RECEIVED");callInProgress=false;sigDisconnectedEvent();}

    function __onReleasedPhoneAck(){
        core.info("telephone.qml | ON-RELEASED-PHONE RECEIVED");callInProgress=false;
        incomingCall=false;queuePosition=-1;telephony.toneEnabled=false;
        pifInput.volumeKeysEnabled=false;groundNumber="";seatNumber="";
        outgoingCallMade=0;incomingPhoneNumber="";sigCallReleaseEvent();authenticationComplete=false;
    }

    function __setAir2GroundService(f){__onPhoneServiceStatusAck(0,f);}
    function __getAir2GroundService(){return seat2GroundAvailable;}
    function __setSeat2SeatService(e){__onPhoneServiceStatusAck(e,0);}
    function __getSeat2SeatService(){return seat2SeatAvailable;}

    function __onPhoneServiceStatusAck(newSeat2SeatServiceAvailable,newA2gServiceAvailable) {
        core.info("telephone.qml | ON-PHONE-SERVICE-STATUS RECEIVED newSeat2SeatServiceAvailable is "+newSeat2SeatServiceAvailable+" newA2gServiceAvailable is "+newA2gServiceAvailable);
        if(core.pc){seat2SeatAvailable=newSeat2SeatServiceAvailable;seat2GroundAvailable=newA2gServiceAvailable;}
        else{seat2SeatAvailable=newSeat2SeatServiceAvailable;seat2GroundAvailable=newA2gServiceAvailable;}
        core.info("telephone.qml | ON-PHONE-SERVICE-STATUS RECEIVED outgoingCallMade is "+outgoingCallMade+" cSEAT_TO_GRD_CALL is "+cSEAT_TO_GRD_CALL);
        if(outgoingCallMade==cSEAT_TO_SEAT_CALL){
            if(seat2SeatAvailable){sigSeatToSeatCallAvailable();}else{sigSeatToSeatCallFailed(cSEAT_TO_SERVICE_SERVICE_NOT_AVAILABLE);}
        }else if(outgoingCallMade==cSEAT_TO_GRD_CALL){
            if(seat2GroundAvailable){core.info("Calling air to ground available");sigAirToGroundCallAvailable();}
            else{sigAirToGroundCallFailed(cSEAT_TO_GROUND_SERVICE_NOT_AVAILABLE);}
        }
    }

    function __onAuthenticationInputNeededAck(){core.info("telephone.qml | ON-AUTHENTICATION-INPUT-NEEDED RECEIVED");sigRequestCreditCardSwipe();}
    function __onAuthenticationPaymentInputNeededAck(){core.info("telephone.qml | ON-AUTHENTICATION-PAYMENT-INPUT-NEEDED RECEIVED");sigRequestCreditCardSwipe();}
    function __onCardSwipeAck(){core.info("telephone.qml | ON-CARD-SWIPE RECEIVED");sigCardSwiped();}

    function __onAuthenticationCompleteAck(){
        var result;core.info("telephone.qml | ON-AUTHENTICATION-COMPLETE RECEIVED");
        if(core.pc){last4Digits="0202";}
        authenticationComplete=true;
        sigAuthenticationComplete();
        if(groundNumber=="") return 1;
        result=telephony.initiateConnection();
        core.info("telephone.qml | ON-AUTHENTICATION-COMPLETE RECEIVED result "+result);
        if(result==false){
            if(core.pc){__onConnectedPhoneAck();}
            else {sigAirToGroundCallFailed(cAIR_TO_GROUND_CONNECTION_FAILED);}
        }else {callInProgress=true;}
    }
    function __onAuthenticationErrorAck(){core.info("telephone.qml | ON-AUTHENTICATION-ERROR RECEIVED");sigAuthenticationError();}
    Component{
        id: telephoneComp;
        Telephony {
            id: telephony;
            deviceManager:phoneInterface.deviceManager;
            onIncomingPhone : {__onIncomingPhoneAck(number);}
            onAcceptingPhone : {core.info("telephone.qml | onAcceptingPhone");}
            onConnectedPhone : {__onConnectedPhoneAck();}
            onDisconnectedPhone : {__onDisconnectedPhoneAck();}
            onReleasedPhone : {__onReleasedPhoneAck();}
            onIdlePhone : {__onReleasedPhoneAck();}
            onPhoneServiceStatus : {__onPhoneServiceStatusAck(newS2sServiceAvailable,newA2gServiceAvailable);}
            onAuthenticationInputNeeded : {__onAuthenticationInputNeededAck();}
            onAuthenticationPaymentInputNeeded : {__onAuthenticationPaymentInputNeededAck();}
            onCardSwipe :{__onCardSwipeAck();}
            onAuthenticationComplete : {__onAuthenticationCompleteAck();}
            onAuthenticationError : {__onAuthenticationErrorAck();}
            onQueuedPhone:{__onQueuedPhoneAck();}
            onConnectionErrorChanged:{
                core.info("telephone.qml | onConnectionError "+connectionError)
                if(connectionError==telephony.CCUserBusy){      //This is number 7
                    callInProgress = false;
                    sigAirToGroundCallFailed(cNUMBER_BUSY);
                }
            }
            onConnectionStateChanged:{core.info("telephone.qml | connectionState "+connectionState);}
        }
    }
    Component.onCompleted: {core.info("telephone.qml | TELEPHONY COMPONENT LOAD COMPLETE");}
}
