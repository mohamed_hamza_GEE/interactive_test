import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as SystemData

Item {

    signal lcdRefreshed(string phsImgUrl);
    Connections{
        target: pif
        onBacklightStateChanged: {
            if(backlightState==0){
                core.info("PremiumHandset | Stopping refresh time on backlight off")
                __refreshTimer.stop();
            }
            else if(getActivePhsID()!=0){
                core.info("PremiumHandset | Restarting refresh time on backlight on, active PHS id "+getActivePhsID());
                refreshHandset(getActivePhsID());
                __refreshTimer.restart();
            }
        }
    }

    function getActivePhsID(){return SystemData.ActivePhsID;}
    function setPremiumHandsetDisplayArr(val){
        SystemData.PhsQArr=[];
        SystemData.ActivePhsID=0;
        SystemData.premiumHandsetDisplayArr=val;
    }
    function updatePremiumHandsetQ(id, status){
        core.log("PremiumHandset | updatePremiumHandsetQ | id: "+id+" status: "+status);
        var pIndex=SystemData.PhsQArr.indexOf(id);
        if(!status){
            if(pIndex!=-1){
                SystemData.PhsQArr.splice(pIndex,1);
                __hideOnHandset(id);
                if(SystemData.PhsQArr.length) __showOnHandset(SystemData.PhsQArr[0])
                else SystemData.ActivePhsID=0;
            }else{
                core.log("PremiumHandset | updatePremiumHandsetQ | Popup not present in Q")
            }
        }else{
            if(pIndex!=-1){
                core.log("PremiumHandset | updatePremiumHandsetQ | Popup already exists in Q")
                if(id==SystemData.ActivePhsID) refreshHandset(SystemData.ActivePhsID);
            }else{
                var pInserted=0;
                for(var i=0;i<SystemData.PhsQArr.length;i++){
                    if(parseInt(SystemData.premiumHandsetDisplayArr[id]["priority"],10) < parseInt(SystemData.premiumHandsetDisplayArr[SystemData.PhsQArr[i]]["priority"],10)){
                        SystemData.PhsQArr.splice(i,0,id);
                        pInserted=1;
                        break;
                    }
                }
                if(!pInserted) SystemData.PhsQArr.push(id);
                if(SystemData.ActivePhsID!=0 && SystemData.ActivePhsID!=SystemData.PhsQArr[0]){
                    __hideOnHandset(SystemData.ActivePhsID);
                }
                __showOnHandset(SystemData.PhsQArr[0])
            }
        }
    }
    function __showOnHandset(id){
        core.log("PremiumHandset | showOnHandset | id: "+id)
        if(SystemData.ActivePhsID==id) { core.log("PremiumHandset | showOnHandset | Already showing id: "+SystemData.ActivePhsID); return; }
        SystemData.ActivePhsID=id;
        var qmlFile=SystemData.premiumHandsetDisplayArr[id]["qmlFileName"];
        core.log("PremiumHandset | refreshHandset | id: "+id+" qmlFile: "+qmlFile);
        phsLoader.source="../../view/premiumHandset/"+qmlFile+".qml";
        if (parseInt(SystemData.premiumHandsetDisplayArr[id]["refreshTime"],10)>0){
            __refreshTimer.interval=parseInt(SystemData.premiumHandsetDisplayArr[id]["refreshTime"],10);
            __refreshTimer.start();
        }
        if(parseInt(SystemData.premiumHandsetDisplayArr[id]["timeout"],10)>0){
            __systemPopupTimer.interval=parseInt(SystemData.premiumHandsetDisplayArr[id]["timeout"],10);
            __systemPopupTimer.start();
        }
    }
    function refreshHandset(id){
     core.info("Refreshing handset");
        if(!pif.getBacklightState()) return;
        if(id==SystemData.ActivePhsID) lcdimage.sendItemToLCD(phsLoader.item);
	if(core.pc){
            lcdimage.saveItem(phsLoader.item,"/tmp/phsImg.jpg");
            lcdRefreshed("/tmp/phsImg.jpg");
        }
    }
    Loader{
        id: phsLoader; visible: false;
        onStatusChanged: {
            if (phsLoader.status == Loader.Ready) {
                if(phsLoader.item.init) phsLoader.item.init();
                lcdimage.sendItemToLCD(phsLoader.item);
            }
        }
    }
    function __hideOnHandset(id){
        core.log("PremiumHandset | hideOnHandset | id: "+id);
        __refreshTimer.stop();
        __systemPopupTimer.stop();
        if(id==SystemData.ActivePhsID && phsLoader.item && phsLoader.item.clearOnExit) phsLoader.item.clearOnExit();
    }
    PhsLcdImage { id: lcdimage }
    Timer { id: __systemPopupTimer; onTriggered: updatePremiumHandsetQ(SystemData.ActivePhsID, false); }
    Timer { id: __refreshTimer; repeat: true; onTriggered: refreshHandset(SystemData.ActivePhsID); }
}
