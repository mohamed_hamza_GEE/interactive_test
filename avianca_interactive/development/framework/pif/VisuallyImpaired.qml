import QtQuick 1.1
import Panasonic.Pif 1.0

Item{
    id:visuallyImpaired

    property int __mediaState:cVI_STOP;
    property int cVI_STOP: 0;
    property int cVI_PLAYING: 1;
    property int cVI_PAUSE:2;
    property string emptyWaveFile:"";
    property string currentFile:"";
    property bool playingEmptyFile: false;
    property bool repeatCue: false;
    property bool contentMediaPaused:false;

    signal sigPlayError();
    signal sigLocalFilePlayBegan();
    signal sigLocalFileEos();
    signal sigLocalFilePlayStopped();

    Connections{
        target:pif;
        onAudioMixerEOS:{
            core.info("VisuallyImpaired.qml | onAudioMixerEOS | playingEmptyFile:"+playingEmptyFile+",repeatCue:"+repeatCue+",__mediaState:"+__mediaState);
            if(playingEmptyFile){
                playingEmptyFile=false;
                if(__mediaState!=cVI_PAUSE){
                    __mediaState=cVI_STOP;
                    if(contentMediaPaused){contentMediaPaused=false; pif.aod.resume()}
                    sigLocalFilePlayStopped();
                    return;
                }
            }
            if(repeatCue){
                pif.filePlay(currentFile);
                return;
            }
            if(__mediaState!=cVI_STOP){
                __mediaState=cVI_STOP;
                currentFile="";
                repeatCue=false
                if(contentMediaPaused){contentMediaPaused=false; pif.aod.resume()}
                sigLocalFileEos();
            }
        }

        onAudioMixerPlay:{
            core.info("VisuallyImpaired.qml | onAudioMixerPlay | status:"+status)
            if(status!=1 && !playingEmptyFile){
                core.info("VisuallyImpaired.qml | Error in playing file");
                sigPlayError();
            } else {
                if(!playingEmptyFile){
                    __mediaState=cVI_PLAYING;
                    sigLocalFilePlayBegan();
                }
            }
        }
    }

    function getMediaState(){return __mediaState;}

    function setEmptyFilePath(filePath){
        core.info("VisuallyImpaired.qml | setEmptyFilePath | emptyfilePath:"+filePath)
        if(filePath==undefined || filePath==""){core.info("VisuallyImpaired.qml | setEmptyFilePath | FilePath not provided, Exiting"); return false}
        emptyWaveFile=filePath;
        return true;
    }

    function playLocalAudioFile(filepath,repeat){
        core.info("VisuallyImpaired.qml | playLocalAudioFile | repeat:"+repeat+", filepath:"+filepath);
        if(emptyWaveFile==""){core.info("VisuallyImpaired.qml | playLocalAudioFile | EmptWave file needs to be provided first, Exiting"); return false}
        if(filepath==undefined || filepath==""){core.info("VisuallyImpaired.qml | playLocalAudioFile | filepath not provided , Exiting"); return false}
        currentFile=filepath;
        repeatCue = (repeat!=undefined && repeat==true)?true:false;
        if(pif.aod.getMediaState()==pif.aod.cMEDIA_PLAY) { pif.aod.pause();contentMediaPaused=true}
        var ret = pif.filePlay(currentFile);
        return ret
    }

    function pauseLocalAudioFile(){
        core.info("VisuallyImpaired.qml | pauseLocalAudioFile ");
        if(emptyWaveFile==""){core.info("VisuallyImpaired.qml | playLocalAudioFile | EmptWave file needs to be provided first, Exiting"); return false}
        var ret = pif.filePlay(emptyWaveFile);
        if(ret){playingEmptyFile=true;__mediaState=cVI_PAUSE;}
        return ret;
    }

    function resumeLocalAudioFile(){
        core.info("VisuallyImpaired.qml | pauseLocalAudioFile  | file :"+currentFile);
        if(currentFile=="" || __mediaState!=cVI_PAUSE){core.info("VisuallyImpaired.qml | resumeLocalAudioFile | resuming file error , Exiting");return false;};
        var ret = pif.filePlay(currentFile);
        return ret;
    }


    function stopLocalAudioFile(){
        core.info("VisuallyImpaired.qml | stopLocalAudioFile ");
        if(__mediaState==cVI_STOP){core.info("VisuallyImpaired.qml | stopLocalAudioFile | Stop requested when media is already stopped");return false;}
        if(emptyWaveFile==""){core.info("VisuallyImpaired.qml | playLocalAudioFile | EmptWave file needs to be provided first, Exiting"); return false;}
        repeatCue=false;
        var ret = pif.filePlay(emptyWaveFile);
        if(ret)playingEmptyFile=true;
        return ret;
    }

    function frmwkInitApp(){
        currentFile="";repeatCue=false;
        stopLocalAudioFile();
    }

    function frmwkResetApp(){
        currentFile="";repeatCue=false;
        stopLocalAudioFile();
    }
}
