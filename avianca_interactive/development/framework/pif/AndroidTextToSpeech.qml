import QtQuick 1.1
import Panasonic.AndroidAccess 1.0
import Panasonic.Pif 1.0

Item {
	id:atts
    property int __mediaState:cTTS_STOP;
    property int cTTS_STOP: 0;
    property int cTTS_PLAYING: 1;
    property int cTTS_PAUSE:2;
    property int previousVolumeLevel:0;
    property string previousVolumeSource: 'aod'
    property string currentFileName: '';

	signal speechStarted();		
	signal speechDone();
	signal speechError();
    signal sigPlayError();
    signal sigLocalFilePlayBegan();
    signal sigLocalFileEos();
    signal sigTTSFilePlayStopped();



    /*Connections{
        target:pif;
        onAudioMixerEOS:{
            core.info("AndroidTextToSpeech.qml | onAudioMixerEOS | __mediaState:"+__mediaState);
            if(__mediaState!=cTTS_STOP){
                __mediaState=cTTS_STOP;
                currentFileName="";
                if(previousVolumeLevel){
                    pif.setVolumeSource(previousVolumeSource);
                    pif.setVolumeByValue(previousVolumeLevel)
                    previousVolumeSource='aod'
                    previousVolumeLevel=0;
                }
                sigLocalFileEos();
            }
        }

        onAudioMixerPlay:{
            core.info("AndroidTextToSpeech.qml | onAudioMixerPlay | status:"+status)
            if(status!=1){
                core.info("AndroidTextToSpeech.qml | Error in playing file");
                sigPlayError();
            } else {
                    __mediaState=cTTS_PLAYING;
                    sigLocalFilePlayBegan();
            }
        }
    }*/

    function setLocale(str){
        if(tts.isLanguageAvailable(str)){
            core.info("AndroidTextToSpeech | setLocale : "+str);
            tts.locale=str;
            return true;
        }else{
            core.info("AndroidTextToSpeech | setLocale : "+str+" Language not available");
            return false;
        }
    }
    function getPitch(){
        core.info("AndroidTextToSpeech | getPitch");
        return tts.pitch;
    }
    function setPitch(val){
        core.info("AndroidTextToSpeech | setPitch : "+val);
        tts.pitch=val;
        return true;
    }
    function getSpeechRate(){
        core.info("AndroidTextToSpeech | getSpeechRate");
        return tts.speechRate;
    }
    function setSpeechRate(val){
        core.info("AndroidTextToSpeech | setSpeechRate : "+val);
        tts.speechRate=val;
        return true;
    }
    function speakString(str){
        core.debug("AndroidTextToSpeech | speakString : "+str);
        return tts.speak(str);
    }

    function speakStringByFile(str,path){
         tts.synthesizeToFile(str,path)
    }

    function stopSpeaking(){
        core.debug("AndroidTextToSpeech | stopSpeaking");
        return tts.stop();
    }

    function isSpeakingString(){
        var val = tts.isSpeaking();
        core.debug("AndroidTextToSpeech | isSpeakingString : "+val);
        return val;
    }

    function getTTSMediaState(){return __mediaState;}

    TextToSpeech {
        id: tts
        locale: "en_US"
        pitch: 1.0
        speechRate: 1.0
		onSpeechStarted:{ core.debug("AndroidTextToSpeech | speechStarted");atts.speechStarted()}		
		onSpeechDone:{ core.debug("AndroidTextToSpeech | speechDone");atts.speechDone()}		
		onSpeechError:{ core.debug("AndroidTextToSpeech | speechError");atts.speechError()}
        onSynthesizeToFileDone: {
            core.debug("AndroidTextToSpeech | SynthesizeToFileDone");
            currentFileName = filename           
            myAudioMixer.play(filename);
        }
        onSynthesizeToFileError:{
            core.debug("AndroidTextToSpeech | speechError");atts.speechError()
        }
    }

    AudioMixer {
        id: myAudioMixer
        deviceManager: pif.deviceMngr
    }

    Connections{
        target:myAudioMixer //since not all qtengine are compitable with below signals
        onPlayed:{
            core.info("AndroidTextToSpeech.qml | onAudioMixerPlay | result:"+result)
            if(result!=1){
                core.info("AndroidTextToSpeech.qml | Error in playing file");
                sigPlayError();
            } else {
                    __mediaState=cTTS_PLAYING;
                    sigLocalFilePlayBegan();
            }
        }
        onEos:{
            core.info("AndroidTextToSpeech.qml | onAudioMixerEOS | __mediaState:"+__mediaState);
            if(__mediaState!=cTTS_STOP){
                __mediaState=cTTS_STOP;
                currentFileName="";
                sigLocalFileEos();
            }

        }
    }

    AudioChannel {
        id: avAudioChannel
        audioMixer: myAudioMixer
        type: "mp3"
        minVolume: 10
        maxVolume: 90
        volume: 20
        muted: false
    }

}
