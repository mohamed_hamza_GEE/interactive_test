import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item{
    id:smsEmail
    //Constants
    property int cEMAIL_SENTBOX: 0;
    property int cEMAIL_INBOX: 1;
    property int cSMS_SENTBOX: 2;
    property int cSMS_INBOX: 3;

    //property
    property bool __appActive: false;
    property bool __userInfoUpdated: false;
    property double __sendSmsPrice: 0.0;
    property double __receiveSmsPrice: 0.0;
    property double __sendEmailPrice: 0.0;
    property double __receiveEmailPrice: 0.0;
    property string __currency: "";
    property string __userName: "";
    property string __userAddress:"";
    property int __numSmss: 0;
    property int __numUnreadSmss: 0;
    property int __numEmails: 0;
    property int __numUnreadEmails: 0;
    property int __type: 0;   //For simulation purpose only;
    //property alias inbox :inbox;
    //property alias sentBox :sentBox;
    property variant myMessaging;

    //FE signals
    signal sigAppActive (bool status);
    signal sigSwipeCard ();
    signal sigAuthenticationError();
    signal sigAuthenticationSuccess();
    signal sigPricesAvailable();
    signal sigSetUserInfoCompleted();
    signal sigSetUserInfoFailed();
    signal sigUserInfo(string userName, string userAddress);
    signal sigMessagesUpdated(variant inbox,variant sentBox);
    signal sigMessagesUpdatedFailed();
    signal sigMessageBodyReady(int type,string recipient,string date,string subject, string message);
    signal sigMessageBodyFailed();
    signal sigMessageSentSuccess();
    signal sigMessageSentFailed();
    signal sigLogoutSuccessful();
    signal sigLogoutFailed();
    signal sigSeatResetSuccess();
    signal sigNewMessageReceived();

    EventListener {
        id: smsEmailEventListener; deviceManager: pif.deviceMngr;
        onEventReceived: {
            core.info("SmsEMail.qml | onEvent Received | MESSAGING_SERVICE_NOTIFY ")
            if(events.getParamAtIndex(5)=="ReplyNotification"){
                core.info("SmsEMail.qml() | Incoming Message |  Event paramList  = " + event.paramList);
                sigNewMessageReceived();
                getMessages();
            }
        }
        events: [
            EventData { name: "MESSAGING_SERVICE_NOTIFY" }
        ]
    }

    Connections{
        target:myMessaging
        onQueryStatusCallEnded:{__appInitialise(queryStatus)}
        onAuthenticationInputNeeded:{__swipeCard()}
        onAuthenticationError:{__authenticationError()}
        onAuthenticationComplete:{__onAuthenticationCompleteAck()}
        onRequestLoginCallEnded:{__onRequestLoginCallEndedAck(queryStatus)}
        onQueryPricesCallEnded:{__onQueryPricesCallEndedAck(queryStatus)}
        onSetUserInfoCallEnded:{core.info("SmsEmail.qml | onSetUserInfoCallEnded | queryStatus: "+queryStatus);if(core.pc){__onSetUserInfoCallEndedAck(queryStatus)}}
        //onGetUserInfoCallEnded:{__getUserInfoAck()}
        onQueryInboxSummaryCallEnded:{__onQueryInboxSummaryCallEndedAck(queryStatus)}
        onQueryHeadersCallEnded:{__onQueryHeadersCallEndedAck(queryStatus)}
        onQueryContentCallEnded:{__onQueryContentCallEndedAck(queryStatus)}
        onSendMessageCallEnded:{__onSendMessageCallEndedAck(queryStatus)}
        onRequestLogoutCallEnded:{__onRequestLogoutCallEndedAck(queryStatus)}
        onSeatResetCallEnded:{__onSeatResetCallEndedAck(queryStatus)}

    }

    //getterfunctions
    function getUserInfo(){
        core.info("SmsEmail.qml | getUserInfo()")
        myMessaging.beginGetUserInfo();
    }

    function getUserName(){
        core.info("SmsEmail.qml | getUserName()");
        return __userName;
    }

    function getUserAddress(){
        core.info("SmsEmail.qml | getUserAddress()");
        return __userAddress;
    }

    function getIsAppActive(){
       //core.info("SmsEmail.qml | getUserAddress()");
        return __appActive;
    }

    function getInbox(){
       //core.info("SmsEmail.qml | getUserAddress()");
        return inbox;
    }


    function getSentBox(){
       //core.info("SmsEmail.qml | getUserAddress()");
        return sentBox;
    }


    function getMessages(){
        core.info("SmsEmail.qml | getMessages() called ");
        if(!__appActive){core.info("SmsEmail.qml | authenticateCard() | App is Not Active, Exiting ");return false;}
        if(!__userInfoUpdated){core.info("SmsEmail.qml | numSmss() | User Info not Set, Exiting ");return false};
        myMessaging.beginQueryInboxSummary();
    }

    function getMessageBody(messageIdStr){
        core.info("SmsEmail.qml | getMessageBody() | messageidStr :"+messageIdStr);
        if(messageIdStr==undefined || messageIdStr==""){core.info("SmsEmail.qml | getMessageBody | MessageId not provided , Exiting");return false}
        Store.messageId=messageIdStr;
        myMessaging.beginQueryContent(messageIdStr);
    }

    function getMessageBodyById(messageIdStr){
        if(messageIdStr==undefined || messageIdStr==""){core.info("SmsEmail.qml | getMessageBody | MessageId not provided , Exiting");return false}
        if(Store.receivedMessageBodyArr[messageIdStr])return Store.receivedMessageBodyArr[messageIdStr];
        else if (Store.sentMessageBodyArr[messageIdStr])return Store.sentMessageBodyArr[messageIdStr];
        else return ""
    }

    function getNumSmss(){
        if(!__userInfoUpdated){core.info("SmsEmail.qml | numSmss() | User Info not Set, Exiting ");return 0};
        return __numSmss;
    }

    function getNumEmails(){
        if(!__userInfoUpdated){core.info("SmsEmail.qml | numSmss() | User Info not Set, Exiting ");return 0};
        return __numEmails;
    }

    function getNumUnreadSmss(){
        if(!__userInfoUpdated){core.info("SmsEmail.qml | numSmss() | User Info not Set, Exiting ");return 0};
        return __numUnreadSmss;
    }

    function getSendSmsPrice(){return __sendSmsPrice;}

    function getReceiveSmsPrice(){return __receiveSmsPrice;}

    function getSendEmailPrice(){return __sendEmailPrice;}

    function getReceiveEmailPrice(){return __receiveEmailPrice;}

    function getCurrency(){return __currency;}

    //setter functions
    function initialise(){
        core.info("SmsEmail.qml | initialise() called")
        if(__appActive){core.info("SmsEmail.qml | initialise() | App Already active"); return true}
        myMessaging.beginQueryStatus();
        return true;
    }

    function authenticate(){
        core.info("SmsEmail.qml | authenticateCard() called")
        if(!__appActive){core.info("SmsEmail.qml | authenticateCard() | App is Not Active, Exiting ");return false;}
        resetVariables();
        myMessaging.authenticate();
        return true;
    }

    function cancelTransaction(){
        core.info("SmsEmail.qml | cancelTransaction()");
        var transactionStatus = myMessaging.cancelTransaction();
        if(transactionStatus==true){
            core.info("SmsEmail.qml | cancelTransaction Succesful");
            return true;
        } else if(transactionStatus==false){
            core.info("SmsEmail.qml | cancelTransaction Unsuccesful");
            return false;
        }
    }

    function setUserInfo(userNameStr,userAddressStr){
        core.info("SMsEmail.qml | setUserInfo() called | UserName :"+userNameStr+" UserAddress :"+userAddressStr);
        if(!__appActive){core.info("SmsEmail.qml | authenticateCard() | App is Not Active, Exiting ");return false;}
        if(userNameStr==undefined || userNameStr==""){core.info("SmsEMail.qml | setUserInfo | UserName not Provided ,Exiting"); return false}
        if (userAddressStr==undefined || userAddressStr==""){core.info("SmsEMail.qml | setUserInfo | User Address not Provided ,Exiting"); return false}
        __userName=userNameStr;
        __userAddress=userAddressStr;
        myMessaging.userName=__userName;
        myMessaging.userAddress=__userAddress;
        myMessaging.senderName=__userName;
        myMessaging.senderAddress=__userAddress;
        myMessaging.beginSetUserInfo();
        if(!core.pc){fakeUserInfoAckTimer.restart();}
        return true;
    }

    function sendEmail(recipientAddressStr,ccAddressStr,subjectStr,messageStr){
        core.info("SmsEmail.qml | sendEmail | Recipient Address :"+recipientAddressStr+" ccAddress :"+ccAddressStr+" Subject :"+subjectStr+" Message :"+messageStr)
        if((recipientAddressStr==undefined || recipientAddressStr=="") && (ccAddressStr==undefined || ccAddressStr=="")){core.info("Recipient and cc Address not Provided, Exiting "); return false;}
        if(subjectStr==undefined || subjectStr=="")core.info("SmsEmail.qml | Subject not Provided ");
        if(messageStr==undefined || messageStr=="")core.info("SmsEmail.qml | Message not provided");
        myMessaging.recipientAddress=recipientAddressStr;
        myMessaging.ccAddress=ccAddressStr;
        myMessaging.subject=subjectStr;
        myMessaging.messageOutgoing=messageStr;
        myMessaging.senderName=__userName;
        myMessaging.senderAddress=__userAddress;
        __type=0;               //for simulation purpose
        myMessaging.beginSendMessage();
    }

    function sendSms(recipientAddressStr,messageStr){
        core.info("SmsEmail.qml | sendSms | Recipient Address :"+recipientAddressStr+" Message :"+messageStr)
        if(recipientAddressStr==undefined || recipientAddressStr=="" ){core.info("Recipient not Provided, Exiting "); return false;}
        if(messageStr==undefined || messageStr=="")core.info("SmsEmail.qml | Message not provided");
        myMessaging.recipientAddress=recipientAddressStr;
        myMessaging.messageOutgoing=messageStr;
        myMessaging.senderName=__userName;
       // myMessaging.senderAddress=__userAddress;   //Not needed,check during integration
        __type=2;       //for simulation purpose only
        myMessaging.beginSendMessage();
    }

    function requestLogout(){
        core.info("SmsEmail.qml | requestLogout() called ");
        myMessaging.beginRequestLogout();
    }

    function seatReset(){
        core.info("SmsEmail.qml | beginSeatReset() called ");
        myMessaging.beginSeatReset();       
    }

    function frmwkInitApp(){
        core.info("SmsEmail.qml | INITIALISE APP CALLED");
        __appActive=false;
        resetAllUserInfo();
    }

    function frmwkResetApp(){
        core.info("SmsEmail.qml | RESET APP CALLED");        
        resetAllUserInfo();
        if(__appActive){seatReset();}
        __appActive=false;
    }

    function resetVariables(){
        core.debug("SmsEmail.qml | resetVariables ");
        inbox.clear();sentBox.clear();__sendSmsPrice=0.0;__receiveSmsPrice=0.0;__sendEmailPrice=0.0;__receiveEmailPrice=0.0;__currency="";
        __numSmss=0;__numUnreadSmss=0;__numEmails=0;__numUnreadEmails=0;Store.authTransactionId="";Store.authCardHash="";
        Store.authLast4Digits="";Store.authCardType="";Store.messageId="";Store.sentMessageBodyArr=[];Store.sentMessageIdArr=[];
        Store.receivedMessageIdArr=[];Store.receivedMessageBodyArr=[];
    }

    function resetAllUserInfo(){
        core.debug("SmsEmail.qml | resetUserInfo ");
        __userInfoUpdated=false; __userName="";__userAddress="";
        resetVariables();
    }

    // __functions
    function __appInitialise (queryStatus){
        if(queryStatus==0 || queryStatus==4){
            core.info("SmsEmail.qml | __appInitialise()| App is active queryStatus: "+queryStatus);
            __appActive=true;
            sigAppActive(true);
        } else {
            core.info("SmsEmail.qml | __appInitialise()| App not active queryStatus: "+queryStatus)
            sigAppActive(false);
        }
    }

    function __swipeCard(){
        core.info("SmsEmail.qml | onAuthenticationInputNeeded ")
        sigSwipeCard();
    }

    function __authenticationError(){
        core.info("SmsEmail.qml | onAuthenticationError")
        sigAuthenticationError()
    }

    function __onAuthenticationCompleteAck(){
        core.info("SmsEmail.qml | onAuthenticationComplete");
        var authenticationData = myMessaging.getAuthenticationData()
        if(authenticationData==true){
            Store.authCardHash=myMessaging.authCardHash;
            Store.authTransactionId=myMessaging.authTransactionId;
            Store.authLast4Digits=myMessaging.authLast4Digits;
            Store.authCardType=myMessaging.authCardType;
            core.info("SmsEmail.qml | Authentication complete | Logging into PMS");
            myMessaging.beginRequestLogin()
        } else if(authenticationData==false){
            core.info("SmsEmail.qml | Card Authentication Error");
            sigAuthenticationError()            //check signal to be emmited
            return false;
        }
    }

    function __onRequestLoginCallEndedAck(queryStatus){
        core.info("SmsEmail.qml | onRequestLoginCallEnded queryStatus: "+queryStatus)
        if(queryStatus==0 || queryStatus==8){
            core.info("SmsEmail.qml | Logged into Messaging Service | Quering Prices ")
            myMessaging.beginQueryPrices()
        } else {
            core.info("SmsEmail.qml | Logging into Messaging Failed ")
            sigAuthenticationError();          //check signal to be emmited
        }
    }

    function __onQueryPricesCallEndedAck(queryStatus){
        core.info ("SmsEmail.qml | onQueryPricesCallEnded queryStatus: "+queryStatus);
        if(queryStatus==0){
            core.info("SmsEmail | Query Prices Success")
            __sendSmsPrice=myMessaging.sendSmsPrice;
            __sendEmailPrice=myMessaging.sendEmailPrice;
            __receiveSmsPrice=myMessaging.receiveSmsPrice;
            __receiveEmailPrice=myMessaging.receiveEmailPrice;
            __currency=myMessaging.currency;
            sigAuthenticationSuccess();
            sigPricesAvailable();
            core.info("SmsEmail | Query Prices | __sendSmsPrice: "+__sendSmsPrice+", __sendEmailPrice: "+__sendEmailPrice+",__receiveSmsPrice: "+__receiveSmsPrice+", __receiveEmailPrice: "+__receiveEmailPrice+", __currency :"+__currency)
        }else{
            core.info("SmsEmail.qml | Prices not Available")
            sigAuthenticationError();      //check signal to be emmited
        }
    }

    function __onSetUserInfoCallEndedAck(queryStatus){
        core.info("SmsEmail | onSetUserInfoCallEnded queryStatus: "+queryStatus)
        fakeUserInfoAckTimer.stop();
        if(queryStatus==0 || queryStatus==25){
            __userInfoUpdated=true;
            sigSetUserInfoCompleted();
        }else{sigSetUserInfoFailed();}
    }

    function __getUserInfoAck(){
        core.info("SmsEmail.qml | onGetUserInfoCallEnded");
        __userName=myMessaging.userName;
        __userAddress=myMessaging.userAddress;
        sigUserInfo(__userName,__userAddress)
    }

    function __onQueryInboxSummaryCallEndedAck(queryStatus){
        core.info("SmsEmail.qml | onQueryInboxSummaryCallEnded: "+queryStatus)
        if (queryStatus==0){
            __setTotalCounts();
            core.info("SmsEmail.qml | onQueryInboxSummaryCallEnded | Number of Smss :"+__numSmss+", Number of Emails :"+__numEmails+", Number of unread Smss : "+__numUnreadSmss+", Number of Unread Emails :"+__numUnreadEmails)
            var totalMessages = __numSmss + __numEmails;
            myMessaging.beginQueryHeaders(0,totalMessages);
            if(totalMessages==0) fakeQueryHeaderAckTimer.restart();
        }
    }

    function __setTotalCounts(){
        __numSmss = myMessaging.numSmss;
        __numEmails = myMessaging.numEmails;
        __numUnreadSmss = myMessaging.numUnreadSmss;
        __numUnreadEmails = myMessaging.numUnreadEmails;
    }

    function __onQueryHeadersCallEndedAck(queryStatus){
        core.info("SmsEmail.qml | onQueryHeadersCallEnded: "+queryStatus)
        fakeQueryHeaderAckTimer.stop();
        if(queryStatus==0){
            core.log("SmsEmail.qml | onQueryHeadersCallEnded | myHeaderList.count: "+myHeaderList.count);
            inbox.clear();
            sentBox.clear();
            for (var index=0;index<myHeaderList.count;index++){
                core.log("SmsEmail.qml | onQueryHeadersCallEnded >> "+JSON.stringify("messageId: "+myHeaderList.getValue(index,"messageId")+" type: "+parseInt(myHeaderList.getValue(index,"type"),10)+" timeStamp: "+myHeaderList.getValue(index,"timeStamp")+" viewed: "+parseInt(myHeaderList.getValue(index,"viewed"),10)+" status: "+parseInt(myHeaderList.getValue(index,"status"),10)+" recipient: "+myHeaderList.getValue(index,"recipient")+" cc: "+myHeaderList.getValue(index,"cc")+" sender: "+myHeaderList.getValue(index,"sender")+" subject: "+myHeaderList.getValue(index,"subject")+" costOfTransaction: "+myHeaderList.getValue(index,"costOfTransaction")+" timeOfTransaction: "+myHeaderList.getValue(index,"timeOfTransaction")+" currency: "+myHeaderList.getValue(index,"currency")));
                var type = parseInt(myHeaderList.getValue(index,"type"),10);
                if(type==1 || type==3){
                    inbox.append({"messageId":myHeaderList.getValue(index,"messageId"),"type":parseInt(myHeaderList.getValue(index,"type"),10),"timeStamp":myHeaderList.getValue(index,"timeStamp"),"viewed":parseInt(myHeaderList.getValue(index,"viewed"),10),"status":parseInt(myHeaderList.getValue(index,"status"),10),"recipient":myHeaderList.getValue(index,"recipient"),"cc":myHeaderList.getValue(index,"cc"),"sender":myHeaderList.getValue(index,"sender"),"subject":myHeaderList.getValue(index,"subject"),"costOfTransaction":myHeaderList.getValue(index,"costOfTransaction"),"timeOfTransaction":myHeaderList.getValue(index,"timeOfTransaction"),"currency":myHeaderList.getValue(index,"currency")})
                } else if (type==0 || type==2){
                    sentBox.append({"messageId":myHeaderList.getValue(index,"messageId"),"type":parseInt(myHeaderList.getValue(index,"type"),10),"timeStamp":myHeaderList.getValue(index,"timeStamp"),"viewed":parseInt(myHeaderList.getValue(index,"viewed"),10),"status":parseInt(myHeaderList.getValue(index,"status"),10),"recipient":myHeaderList.getValue(index,"recipient"),"cc":myHeaderList.getValue(index,"cc"),"sender":myHeaderList.getValue(index,"sender"),"subject":myHeaderList.getValue(index,"subject"),"costOfTransaction":myHeaderList.getValue(index,"costOfTransaction"),"timeOfTransaction":myHeaderList.getValue(index,"timeOfTransaction"),"currency":myHeaderList.getValue(index,"currency")})
                }
            }
            if(pif.getMessageBodyNeeded()){
                for(var i=0; i<sentBox.count;i++){
                    if(!Store.sentMessageBodyArr[sentBox.at(i).messageId])Store.sentMessageIdArr.push(sentBox.at(i).messageId)
                }
                if(Store.sentMessageIdArr.length!=0)__queryQue();
                else __queryInboxMessages();
            }
            core.info("SmsEmail.qml | onQueryHeadersCallEnded: inbox: "+inbox.count+" | sentBox: "+sentBox.count);
            __setTotalCounts();
            sigMessagesUpdated(inbox,sentBox);
        }else{sigMessagesUpdatedFailed();}
    }

    function __queryInboxMessages(){
        core.info("SmsEmail.qml | __queryInboxMessages |  inbox count:"+inbox.count)
        for(var j=0;j<inbox.count;j++){
            if(parseInt(inbox.at(j).viewed,10)==1 && !Store.receivedMessageBodyArr[inbox.at(j).messageId]){
                Store.receivedMessageIdArr.push(inbox.at(j).messageId)
            }
        }
        core.log("SmsEmail.qml | __queryInboxMessages | ReceivedMessage Array length: "+Store.receivedMessageIdArr.length)
        if(Store.receivedMessageIdArr.length!=0)__queryReceivedQue();
    }

    function __queryQue(){
        core.info("SmsEmail | __queryQue | called | length :"+Store.sentMessageIdArr.length)
        for (var i=0;i<Store.sentMessageIdArr.length;i++){
            if(!Store.sentMessageBodyArr[Store.sentMessageIdArr[i]]){
                getMessageBody(Store.sentMessageIdArr[i]);
                break;
            } else{
                __removeMessageId();
                break;
            }
        }
    }

    function __queryReceivedQue(){
        core.info("SmsEmail | __queryReceivedQue | called | length :"+Store.receivedMessageIdArr.length)
        for (var i=0;i<Store.receivedMessageIdArr.length;i++){
            if(!Store.receivedMessageBodyArr[Store.receivedMessageIdArr[i]]){
                getMessageBody(Store.receivedMessageIdArr[i]);
                break;
            } else{
                __removeReceivedMessageId();
                break;
            }
        }
    }

    function __removeMessageId(){
        core.info("SmsEmail.qml | __removeMessageId | Messageid: "+Store.sentMessageIdArr[0])
        Store.sentMessageIdArr.splice(0,1);
        if(Store.sentMessageIdArr.length!=0)__queryQue();
        else __queryInboxMessages();
    }

    function __removeReceivedMessageId(){
        core.info("SmsEmail.qml | __removeMessageId | Messageid: "+Store.receivedMessageIdArr[0])
        Store.receivedMessageIdArr.splice(0,1);
        if(Store.receivedMessageIdArr.length!=0)__queryReceivedQue();
    }

    function __onQueryContentCallEndedAck(queryStatus){
        core.info("SmsEmail.qml | onQueryContentCallEnded: "+queryStatus)
        if(queryStatus==0){
            var type;
            for(var index=0; index<inbox.count; index++){if(Store.messageId==inbox.getValue(index,"messageId")){if(inbox.getValue(index,"viewed")==0){inbox.setProperty(index,"viewed",1)}}}

            for(var index=0; index<myHeaderList.count; index++){
                if(Store.messageId==myHeaderList.getValue(index,"messageId")){
                    type=parseInt(myHeaderList.getValue(index,"type"),10)
                    if(myHeaderList.getValue(index,"viewed")==0){myHeaderList.setProperty(index,"viewed",1)}
                    if(type==0){
                        core.info("SmsEmail.qml | Email SentBox |")
                        var to = myHeaderList.getValue(index,"recipient");
                        var date = myHeaderList.getValue(index,"timeStamp");
                        var subject = myHeaderList.getValue(index,"subject");
                        var message = myMessaging.messageIncoming;                      //check
                        if(pif.getMessageBodyNeeded()){
                            if(Store.sentMessageIdArr.length!=0){
                                if(!Store.sentMessageBodyArr[Store.sentMessageIdArr[0]])Store.sentMessageBodyArr[Store.sentMessageIdArr[0]]=myMessaging.messageIncoming;
                                __removeMessageId();
                            }else {
                                if(!Store.sentMessageBodyArr[Store.messageId])Store.sentMessageBodyArr[Store.messageId]=myMessaging.messageIncoming;
                            }
                        }
                        core.debug("SmsEmail.qml | Receipient: "+to+", timeStamp :"+date+", Subject:"+subject+"MessageBody: "+message);
                        sigMessageBodyReady(cEMAIL_SENTBOX,to,date,subject,message)
                    } else if(type==1){
                        core.info("SmsEmail.qml | Email Inbox")
                        var from = myHeaderList.getValue(index,"sender");
                        date = myHeaderList.getValue(index,"timeStamp");
                        subject = myHeaderList.getValue(index,"subject");
                        message = myMessaging.messageIncoming;                         //Check
                        if(pif.getMessageBodyNeeded()){
                            if(Store.receivedMessageIdArr.length!=0){
                                if(!Store.receivedMessageBodyArr[Store.receivedMessageIdArr[0]])Store.receivedMessageBodyArr[Store.receivedMessageIdArr[0]]=myMessaging.messageIncoming;
                                __removeReceivedMessageId();
                            }else {
                                if(!Store.receivedMessageBodyArr[Store.messageId])Store.receivedMessageBodyArr[Store.messageId]=myMessaging.messageIncoming;
                            }
                        }
                        core.debug("SmsEmail.qml | Sender: "+from+", timeStamp :"+date+", Subject:"+subject+"MessageBody: "+message);
                        sigMessageBodyReady(cEMAIL_INBOX,from,date,subject,message)
                    } else if (type==2){
                        core.info("SmsEmail.qml | Sms SentBox ")
                        to = myHeaderList.getValue(index,"recipient");
                        date = myHeaderList.getValue(index,"timeStamp");
                        message = myMessaging.messageIncoming;                         //Check
                        if(pif.getMessageBodyNeeded()){
                            if(Store.sentMessageIdArr.length!=0){
                                if(!Store.sentMessageBodyArr[Store.sentMessageIdArr[0]])Store.sentMessageBodyArr[Store.sentMessageIdArr[0]]=myMessaging.messageIncoming;
                                __removeMessageId();
                            }else {
                                if(!Store.sentMessageBodyArr[Store.messageId])Store.sentMessageBodyArr[Store.messageId]=myMessaging.messageIncoming;
                            }
                        }
                        core.debug("SmsEmail.qml | Receipient: "+to+", timeStamp :"+date+", MessageBody: "+message);
                        sigMessageBodyReady(cSMS_SENTBOX,to,date,"",message)
                    } else if (type==3){
                        core.info("SmsEmail.qml | Sms Inbox" )
                        from = myHeaderList.getValue(index,"sender");
                        date = myHeaderList.getValue(index,"timeStamp");
                        message = myMessaging.messageIncoming;                         //Check
                        if(pif.getMessageBodyNeeded()){
                            if(Store.receivedMessageIdArr.length!=0){
                                if(!Store.receivedMessageBodyArr[Store.receivedMessageIdArr[0]])Store.receivedMessageBodyArr[Store.receivedMessageIdArr[0]]=myMessaging.messageIncoming;
                                __removeReceivedMessageId();
                            }else {
                                if(!Store.receivedMessageBodyArr[Store.messageId])Store.receivedMessageBodyArr[Store.messageId]=myMessaging.messageIncoming;
                            }
                        }
                        core.debug("SmsEmail.qml | Sender: "+from+", timeStamp :"+date+", MessageBody: "+message);
                        sigMessageBodyReady(cSMS_INBOX,from,date,"",message);
                    }
                }
            }
        } else {
            core.info("SmsEmail.qml | __onQueryContentCallEndedAck | Content not available ")
            sigMessageBodyFailed();
            return false;
        }
        return true;
    }

    function __onSendMessageCallEndedAck(queryStatus){
        core.info("SmsEmail.qml | onSendMessageCallEnded  queryStatus :"+queryStatus)
        if(queryStatus==0 || queryStatus==12){
            sigMessageSentSuccess();
            getMessages()
        }else {sigMessageSentFailed();}
    }

    function __onRequestLogoutCallEndedAck(queryStatus){
        core.info("SmsEmail.qml | onRequestLogoutCallEnded: "+queryStatus);
        if(queryStatus==0 || queryStatus==17){
            core.info("SmsEmail.qml | Request logout Successful");
            sigLogoutSuccessful()
        }else {sigLogoutFailed();}
    }

    function __onSeatResetCallEndedAck(queryStatus){
        core.info("SMsEMail.qml | onSeatResetCallEnded: "+queryStatus)
        if(queryStatus==0 || queryStatus==4){
            core.info("SmsEmaiol.qml | Seat Reset Successful")
            sigSeatResetSuccess();
        }
    }

    function __getType(){ //for simulation purpose only
        return __type;
    }

    Component.onCompleted: {
        if (core.pc) myMessaging = Qt.createQmlObject('import QtQuick 1.1; MessagingServiceSim{id: myMessaging}',smsEmail);
        else myMessaging = smsEmailManagerComp.createObject(smsEmail);
        myMessaging.clearHeaderList=true;
        myMessaging.headerList=myHeaderList;
    }

    Timer {id:fakeUserInfoAckTimer; interval:1000; onTriggered: __onSetUserInfoCallEndedAck(0);}
    Timer {id:fakeQueryHeaderAckTimer; interval:1000; onTriggered: __onQueryHeadersCallEndedAck(0);}

    SimpleModel {
        id:inbox
    }
    SimpleModel {
        id:sentBox
    }
    SimpleModel {
        id: myHeaderList
    }

    Component {
        id: smsEmailManagerComp;
        MessagingService { id: myMessaging }
    }


}
