import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
     id: windowDimmer;
     property int count:0;
     signal windowDimLevelChanged(int windowId, int dimLevel);

     function setDimLevel (windowId,level) {
         for (var i=0; i<Store.WindowDimArr.length; i++) {
             if (Store.WindowDimArr[i].windowid==windowId){
                 Store.WindowDimArr[i].dimlevel=level;
                 Store.setDimRequestCounter.push([windowId,level].join(','));
                 dTimer.restart();
             }
         }
     }
     function at(index) {
         return Store.WindowDimArr[index];
     }
     function populateSimData () {
         core.info("WindowDimmerSIM | populateSimData");
         Store.setDimRequestCounter=[];
         Store.WindowDimArr = [];
         Store.WindowDimArr[0] = {"windowid": 1, "minlevel": 1, "maxlevel": 5,"dimlevel": 2};
         Store.WindowDimArr[1] = {"windowid": 2, "minlevel": 1, "maxlevel": 5,"dimlevel": 3};
         Store.WindowDimArr[2] = {"windowid": 3, "minlevel": 1, "maxlevel": 5,"dimlevel": 4};
         count = Store.WindowDimArr.length;
     }
     Component.onCompleted: {
        core.info("WindowDimmerSIM | Component loaded.");
        populateSimData();
     }
     Timer {
         id: dTimer;
         interval: 100;
         onTriggered: {
             for (var i=0; i<Store.setDimRequestCounter.length; i++) windowDimLevelChanged(Store.setDimRequestCounter[i].split(',')[0],Store.setDimRequestCounter[i].split(',')[1]);
             Store.setDimRequestCounter=[];
         }
     }
 }
