import QtQuick 1.1
import Panasonic.Pif 1.0

Item {
    id: screenSaver
    property int __screenSaverType: 0;
    property int __screenSaverState: 1;
    property variant __screenSaverTimeout: 5;
    property int __dimBrightness: 30;
    property int __dimTimeout: 10;
    property int __valBeforeDim: 0;
    property bool __customScreenState: false;
    property bool __blockEvent: false;
    property string __retainScreenSaver;
    property bool __enableScreenSaver: true;
    signal sigKeepScreenOn();
    signal sigKeepScreenOff();
    signal customScreenSaverOn();
    signal customScreenSaverOff();
    property variant __pifRef:pif;
    property bool __backlightOnFromSS: false;

    Connections{
        target: pif
        onBacklightStateChanged: {
            if(!backlightState){
                __blockEvent=true;
            }else if(__screenSaverState){
                blockEventTimer.restart();
                if(!pif.getPAState()) screenSaverTimer.restart();
            }else {
                __blockEvent=false;
            }
        }
        onPaStateChanged:{
            if(!__enableScreenSaver) return;
            if(pif.getPAState() && __screenSaverState){
                core.info("ScreenSaver | onPaStateChanged | screenSaverTimer.running: "+screenSaverTimer.running)
                core.info("ScreenSaver | onPaStateChanged | dimTimer.running: "+dimTimer.running)
                if(screenSaverTimer.running) {
                    __retainScreenSaver="ScreenSaver"; screenSaverTimer.pause();
                }
                else if(dimTimer.running) {
                    __retainScreenSaver="DimScreenSaver"; dimTimer.pause();
                }
            }else{
                if(__screenSaverState) {
                    core.info("ScreenSaver | onPaStateChanged | __retainScreenSaver: "+__retainScreenSaver);
                    (__retainScreenSaver=="DimScreenSaver")?dimTimer.resume():screenSaverTimer.resume();
                }
                __retainScreenSaver="";
            }
        }
    }

    function enableScreenSaver(){
        __enableScreenSaver=true;
        startScreenSaver()
    }
    function disableScreenSaver(){
        __enableScreenSaver=false;
        stopScreenSaver();
    }

    function startScreenSaver(){
        core.info("ScreenSaver | startScreenSaver")
        if(!__enableScreenSaver) return;
        if(!(pif.getExternalAppActive() && pif.getPAState()))
            __screenSaverState=1;
        if(!pif.getPAState()) {screenSaverTimer.restart(); sigKeepScreenOff();}
    }
    function stopScreenSaver(){
        core.info("ScreenSaver | stopScreenSaver")
        __screenSaverState=0;
        screenSaverTimer.stop();
        sigKeepScreenOn();
    }
    function restartScreenSaver(){
        if(!__enableScreenSaver) return;
        if(__screenSaverState && !pif.getPAState()) screenSaverTimer.restart();
        if(!pif.getBacklightState()) pif.setBacklightOn();
        if(__screenSaverType==1){
            if(__valBeforeDim){
                dimTimer.stop();
                pif.setBrightnessForScreenSaver(__valBeforeDim);
                __valBeforeDim=0;
            }
        }else if(__screenSaverType==2){
            if(__customScreenState){
                __customScreenState=false;
                customScreenSaverOff();
            }
        }
    }
    function setScreenSaverType(val){__screenSaverType=val; return true;}    // 0: Basic; 1: Advanced(Dim light before screenOff); 2: custom
    function setScreenSaverTimeout(val){__screenSaverTimeout=val; return true;}     // screen saver time out in Mins. Default value = 5 mins
    function setDimBrightness(val){__dimBrightness=val; return true;}    // only for type:1 | dim brightness. Default value = 30
    function setDimTimeout(val){__dimTimeout=val; return true;}     // only for type:1 | time for which screen will be dim in secs. Default value = 10 secs

    // reference 2 can be either pif or vip. The below function will change the reference 2. This will be called from Vip.qml
    function __setPifReference(val){
        __pifRef = val;
    }

    SequentialAnimation{
        id: screenSaverTimer;
        onPausedChanged:{
            core.info("Screen Saver is paused : "+paused)
        }

        PauseAnimation { duration: __screenSaverTimeout*60000;}
        ScriptAction{
            script: {
                if(!pif.getBacklightState()) return;
                if(__screenSaverType==0){
                    core.debug("ScreenSaver | screenSaverTimer triggered | swiching off backlight")
                    pif.setBacklightOff();
                }else if(__screenSaverType==1){
                    core.debug("ScreenSaver | screenSaverTimer triggered | dimming screen")
                    core.info("ScreenSaver | Dimming screen | __valBeforeDim: "+__valBeforeDim)
                    core.info("ScreenSaver | Dimming screen | pif.getBrightnessLevel(): "+pif.getBrightnessLevel())
                    __valBeforeDim = pif.getBrightnessLevel();
                    core.info("ScreenSaver | Dimming screen | __valBeforeDim Set : "+__valBeforeDim)
                    if(__valBeforeDim>__dimBrightness) {
                        core.info("ScreenSaver | Dimming screen | setting dim brighness.... | __dimBrightness: "+__dimBrightness)
                        pif.setBrightnessForScreenSaver(__dimBrightness);
                    }
                    dimTimer.start();
                }else if(__screenSaverType==2){
                    core.debug("ScreenSaver | screenSaverTimer triggered | custom screensaver on")
                    __customScreenState=true;
                    customScreenSaverOn();
                }
            }
        }
    }

    SequentialAnimation{
        id: dimTimer
        PauseAnimation { duration: __dimTimeout*1000;}
        ScriptAction{
            script: {
                core.debug("ScreenSaver | dimTimer triggered | swiching off backlight | pif.android: "+pif.android)
                pif.setBacklightOff();
                if(!pif.android){
                    pif.setBrightnessForScreenSaver(__valBeforeDim);
                    __valBeforeDim=0;
                }
            }
        }
    }
    UserEventFilter{
        enabled: __blockEvent;
        onKeyPressed: {blockEventTimer.stop(); event.accepted=true;
            if(!pif.getBacklightState() && !__screenSaverState){
                core.debug("ScreenSaver | UserEventFilter __blockEvent | swiching on backlight")
                pif.setBacklightOn();
            }
        }
        onKeyReleased: {event.accepted=true; __blockEvent=false;}
        onMouseMoved: {event.accepted=true; }
        onMousePressed: {blockEventTimer.stop(); event.accepted=true; }
        onMouseReleased: {event.accepted=true; __blockEvent=false;}
    }
    UserEventFilter{
        enabled: __screenSaverState;
        onKeyPressed: navEventReceived(event,"key");
        onKeyReleased: { if(__backlightOnFromSS) event.accepted=true; __backlightOnFromSS=false; }
        onMousePressed: navEventReceived(event,"mouse");
        onMouseReleased: { if(__backlightOnFromSS) event.accepted=true; __backlightOnFromSS=false; }
        function navEventReceived(event,type){
            if(!pif.getPAState()) screenSaverTimer.restart();
            if(!pif.getBacklightState()){
                core.debug("ScreenSaver | navEventReceived | swiching on backlight | pif.android: "+pif.android)
                pif.setBacklightOn();
                if(pif.android && __valBeforeDim){
                    core.info("ScreenSaver | navEventReceived | Setting Brightness | value : "+__valBeforeDim)
                    pif.setBrightnessForScreenSaver(__valBeforeDim);
                    __valBeforeDim=0;
                }
                event.accepted=true; __backlightOnFromSS=true;
            }
            if(__screenSaverType==1){
                if(__valBeforeDim){
                    core.debug("ScreenSaver | navEventReceived | dimming off")
                    dimTimer.stop();
                    pif.setBrightnessForScreenSaver(__valBeforeDim);
                    __valBeforeDim=0;
                    event.accepted=true;
                }
            }else if(__screenSaverType==2){
                if(__customScreenState){
                    core.debug("ScreenSaver | navEventReceived | custom screensaver off")
                    __customScreenState=false;
                    customScreenSaverOff();
                }
            }
        }
    }
    Timer{id: blockEventTimer; interval: 500; onTriggered: __blockEvent=false;}
}
