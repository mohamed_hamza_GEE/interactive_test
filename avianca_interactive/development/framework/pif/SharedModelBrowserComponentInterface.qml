import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"

SharedModelBrowserComponent{
    id:sharedModelClientInterface;

    signal connected();
    signal disconnected();
    signal dataSynchronized();

    function __iterateAssocArray(params){var logger=[];for(var key in params){ logger.push(" params."+key+": "+params[key])} return logger;}

    //public functions
    function playVodByMid(params){ //apiName: playVod
        if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        core.debug("Send message to seat | apiName: playVod |"+__iterateAssocArray(params));
        sendApiData("playVod",params);
    }

    function playTrailer(params){ //apiName: playTrailer
        if(!params.elapsedTime || params.elapsedTime==undefined) params.elapsedTime=0;
        core.debug("Send message to seat | apiName: playTrailer |"+__iterateAssocArray(params));
        sendApiData("playTrailer",params);
    }

    function toggleVodStretchState(){ // aspectRatioAck won't be received when request is made from vKarma to seat
        if(aspectRatio=="Not Adjustable") return false;
        core.debug("Send message to seat | stretchVod | aspectRatio : " + aspectRatio);
        (aspectRatio=="Fullscreen")? sendPropertyData('{"aspectRatio" : "4x3"}'): sendPropertyData('{"aspectRatio" : "Fullscreen"}');
    }

    function setPlayingSoundtrack(params){core.debug("Send message to seat | apiName: videoSoundtrack |"+__iterateAssocArray(params));sendApiData("videoSoundtrack",params);} //apiName: videoSoundtrack
    function setPlayingSubtitle(params){ core.debug("Send message to seat | apiName: videoSubtitle |"+__iterateAssocArray(params)); sendApiData("videoSubtitle",params);}       // apiName: videoSubtitle, View should pass subtitleLid=-1 for "None" as subtitle
    function stopVideo(){ core.debug("Send message to seat | apiName: videoStop"); sendApiData("videoStop",'{}');}

    // AOD operations
    function playAodByMid(params){core.debug("Send message to seat | apiName: playAod |"+__iterateAssocArray(params)); sendApiData("playAod",params);}

    //repeatTrack "none": no  looping, "current": looping of the current track, "all": looping of all the tracks in the album
    function repeatTrack(params){  core.debug("Send message to seat | apiName: mediaRepeat |"+__iterateAssocArray(params)); sendApiData("mediaRepeat",params);}

    function shuffleTrack(){ core.debug("Send message to seat | mediaShuffle : " + mediaShuffle); sendPropertyData('{"mediaShuffle":' + !mediaShuffle + '}');}
    function stopAudio(){ core.debug("Send message to seat | apiName: audioStop"); sendApiData("audioStop",'{}');}

    // Common VOD & AOD operations
    function setPlayingMediaPosition(params){ core.debug("Send message to seat | apiName: setMediaPosition |"+__iterateAssocArray(params)); sendApiData("setMediaPosition",params)}
    function forwardPlayingMedia(){ core.debug("Send message to seat | apiName: mediaFastForward"); sendApiData("mediaFastForward",'{}');}
    function rewindPlayingMedia(){  core.debug("Send message to seat | apiName: mediaRewind"); sendApiData("mediaRewind",'{}');}
    function pausePlayingMedia(){ core.debug("Send message to seat | apiName: mediaPause"); sendApiData("mediaPause",'{}');}
    function resumePlayingMedia(){ core.debug("Send message to seat | apiName: mediaResume"); sendApiData("mediaResume",'{}');}
    function playNextMedia(){ core.debug("Send message to seat | apiName: mediaNext"); sendApiData("mediaNext",'{}');}
    function playPreviousMedia(){ core.debug("Send message to seat | apiName: mediaPrevious"); sendApiData("mediaPrevious",'{}');}

    function addMidToPlaylist(params){ //apiName: addMidToPlaylist
        core.info("Control in  addMidToPlaylist() ");
        sendApiData("addMidToPlaylist",params);
    }

    function removeMidFromPlayist(params){ //apiName: removeMidFromPlayist
        sendApiData("removeMidFromPlayist",params);
    }

    function removeAllFromPlaylist(params){ //apiName: removeAllFromPlaylist
        if(!params.playlistIndex || params.playlistIndex==undefined){core.error("playlistIndex is invalid"); return false;}
        core.debug("Send message to seat | apiName: removeAllFromPlaylist |"+__iterateAssocArray(params));
        sendApiData("removeAllFromPlaylist",params);
    }

    function getAllPlaylistMids(){
        core.info("Control in getAllPlaylistMids(). playlists is "+ playlists);
        return playlists;
    }

    function isTrackInPlaylist(mid){
        core.info("Control in isTrackInPlaylist(mid)" + " mid is " + mid + " playlists is "+ playlists);
        if(playlists.indexOf(mid) == -1) return false;
        else return true;
    }

    // Volume, Brightness settings
    function setSeatbackVolume(volumeLevel){ core.debug("Send message to seat | setSeatbackVolume | volumeLevel: " + volumeLevel); sendPropertyData('{"seatbackVolume":' + volumeLevel + '}');}
    function setSeatbackVolumeByStep(step) {    // step: -1, -2... or 1, 2...
        core.debug("Send message to seat | setSeatbackVolumeByStep | volume Step: " + step);
        step=(String(parseInt(step,10))!="NaN")?parseInt(step,10):0;
        var volume=seatbackVolume+pif.getVolumeStep()*step;
        sendPropertyData('{"seatbackVolume":' + (volume>100?100:volume<0?0:volume) + '}'); return true;
    }

    function setSeatbackBrightness(brightnessLevel){ core.debug("Send message to seat | setSeatbackBrightness | brightnessLevel : " + brightnessLevel); sendPropertyData('{"seatbackBrightness":' + brightnessLevel + '}');}
    function setSeatbackBrightnessByStep(step) {    // step: -1, -2... or 1, 2...
        core.debug("Send message to seat | setSeatbackBrightnessByStep | Brightness Step: " + step);
        step=(String(parseInt(step,10))!="NaN")?parseInt(step,10):0;
        var brightness=seatbackBrightness+pif.getBrightnessStep()*step;
        sendPropertyData('{"seatbackBrightness":' + (brightness>100?100:brightness<0?0:brightness) + '}'); return true;
    }

                                                                                                          //sendPropertyData('{"seatbackVolume":' + (volume>100?100:volume<0?0:volume) + '}');
    function toggleBacklight(){ core.debug("Send message to seat | toggleBacklight : " + backlightEnabled); sendPropertyData("backlightEnabled",!backlightEnabled); }
    function languageChange(params){ core.debug("Send message to seat  | langugeChange |"+__iterateAssocArray(params)); sendApiData("changeLanguage",params);}
    function launchApp(params){ core.debug("Send message to seat | launchApp | launchAppId : " + params.launchAppId); sendApiData("launchApp",params);}
    function closeApp(){ core.debug("Send message to seat  | closeApp called"); sendApiData("closeApp",{});}

    function sendKeyboardDataToSeat(val){sendPropertyData('{"keyboardData":"' + val+ '"}');}
    function showVkb(){sendPropertyData('{"showKeyboard":' + true + '}');}
    function hideVkb(){sendPropertyData('{"showKeyboard":' + false + '}');}
    function launchHospitality(){sendApiData("launchHospitality",'{}');}
    function launchShopping(){sendApiData("launchShopping",'{}');}
    function launchSurvey(){sendApiData("launchSurvey",'{}');}
    function launchSeatChat(){sendApiData("launchSeatChat",'{}');}
    function launchChatroom(){sendApiData("launchChatroom",'{}');}
    function launchLTN(){sendApiData("launchLTN",'{}');}
    function launchConnectingGate(){sendApiData("launchConnectingGate",'{}');}
    function launchArrivalInfo(){sendApiData("launchArrivalInfo",'{}');}
    function launchiXplor(){sendApiData("launchiXplor",'{}');}
    function launchUsb(){sendApiData("launchUsb",'{}');}
    function launchIpod(val){sendApiData("launchIpod",'{"launchAppId":"' + val + '"}');}
    function launchCategory(val){sendApiData("launchCategory",'{"launchAppId":"' + val + '"}');}

    function setUsbAgreement(state){sendPropertyData('{"usbAgreement":' + state + '}');}

    function setIpodAgreement(state){sendPropertyData('{"iPodAgreement":' + state + '}');}

    function sendUserDefinedApi(userDefinedApi,apiParams){ if(userDefinedApi.indexOf("userDefined")!=-1) sendApiData(userDefinedApi,(apiParams)?apiParams:''); else core.debug("SharedModelClientInterface.qml | userDefinedApi should be sufficed with word userDefined");}

    function getLanguageIso(){return languageIso;}
    function getSeatbackBrightness(){return seatbackBrightness;}
    function getSeatbackVolume() {return seatbackVolume;}
    function getBacklightStatus() {return backlightEnabled;}
    function getPlayingMidInfo() {return {mid:mid,aggregateMid:aggregateMid};}
    function getSoundtrackInfo() {return {soundtrackLid:soundtrackLid,soundtrackType:soundtrackType};}
    function getSubtitleInfo() {return {subtitleLid:subtitleLid,subtitleType:subtitleType};}
    function getElapsedTime() {return elapsedTime;}
    function getAspectRatio() {return aspectRatio;}
    function getMediaType() {return mediaType;}
    function getMediaPlayRate() {return mediaPlayRate;}
    function getCurrentMediaPlayerState() {return mediaPlayerState;}
    function getMediaRepeatValue() {return mediaRepeat;}
    function getMediaShuffle() {return mediaShuffle;}
    function getUsbConnectedStatus() {return usbConnected;}
    function getIpodConnectedStatus() {return iPodConnected;}
    function getCurrentLaunchAppId() {return launchAppId;}
    function getKeyboardDisplayStatus() {return showKeyboard;}
    function getKeyboardData() {return keyboardData;}
    function getUsbAgreement() {return usbAgreement;}
    function getIpodAgreement() {return iPodAgreement;}
    function getPlaylistInfo() {return playlists;}

    Component.onCompleted: core.debug("SharedModelBrowserComponentInterface.qml | Shared Model Browser Component Interface load complete");

}
