import QtQuick 1.1
import Panasonic.Pif 1.0

//Purpose:
// Reads interactive configuration, lru configuration & update core settings and Logging.

Rectangle {
    color: 'black';
    property int cOFF: 0;
    property int cERROR: 1;
    property int cWARN: 2;
    property int cINFO: 3;
    property int cDEBUG: 4;
    property int cLOG: 5;
    property int cVERBOSE: 6;
    property bool pc: false;
    property int testMode: 0;
    property bool simulation: false;
    property string logLevel: "info";
    property string pifLogLevel: "info";
    property variant translations;
    property variant configTool;
    property QtObject settings:QtObject{
        id: settings
        // Language
        property int languageID:0;
        property string languageISO:'';
        // Path
        property string intPath:'/tmp/interactive/';
        property string contentPath:'/tmp/interactive/content';
        property string viewAssetPath:'/tmp/interactive/viewAssets';
        property string assetPath:'/tmp/interactive/viewAssets/assets1';
        property string remotePath:'http://172.17.0.24';
        // Resolution
        property int totalResolutions:1;
        property string resolutionMap:"1024x600,1024x600";
        property string orientationMap:'1|L,1|L,1|L,1|IL';
        property int resolutionIndex:1;
        property int screenWidth:1024;
        property int screenHeight:600;
        property string screenResolution:"1024x600";
        property string defaultISO: "eng"
        property int defaultLID: 1
    }
    property int __logLevel:cLOG;
    property int __pifLogLevel:cINFO;
    property double __prevTime:0;
    property bool __configLoad:false;
    property string __simOrientationType:'';

    // ************************************* Private ***************************************
    Connections{target: pif; onOrientationChanged: if (__configLoad && pif.getMonitorType()==pif.cLRU_VKARMA) {__setOrientation(orientation); viewController.__setViewPaths();}}

    function __loadConfiguration(){
        settings.intPath = coreHelper.cleanPath(Qt.resolvedUrl("../"));		// Qt.resolvedUrl prepends file://
        var lruData = __getLruData(); if (!lruData) return false;
        var cfgData = __getConfigData("config.ini"); if (!cfgData) return false;
        if (pc||core.simulation) {var simData = __getConfigData("simulation.ini"); __simOrientationType=simData.OrientationType?simData.OrientationType:__simOrientationType;} else var simData;

        if (cfgData.ResolutionIndex) settings.resolutionIndex = cfgData.ResolutionIndex;
        // Set Path
        if (cfgData.ContentPath) settings.contentPath = coreHelper.replaceReservedChars(cfgData.ContentPath);
        settings.contentPath = coreHelper.cleanPath(Qt.resolvedUrl(settings.intPath+settings.contentPath));
        if (cfgData.ViewAssetPath) settings.viewAssetPath = coreHelper.replaceReservedChars(cfgData.ViewAssetPath);
        settings.viewAssetPath = coreHelper.cleanPath(Qt.resolvedUrl(settings.intPath+settings.viewAssetPath));
        if (cfgData.AssetPath) settings.assetPath = coreHelper.replaceReservedChars(cfgData.AssetPath);
        settings.assetPath = coreHelper.cleanPath(Qt.resolvedUrl(settings.viewAssetPath+"/"+settings.assetPath+settings.resolutionIndex));
        if (simData && simData.RemotePath) settings.remotePath = coreHelper.replaceReservedChars(simData.RemotePath);

        // set Default Language
        settings.defaultISO = (cfgData.DefLanguageISO)?coreHelper.replaceReservedChars(cfgData.DefLanguageISO).toLowerCase():settings.defaultISO;
        settings.defaultLID = (cfgData.DefLanguageID)?cfgData.DefLanguageID:settings.defaultLID;

        // Load Qt Translator
        if (core.coreHelper.fileExists("../content/translations/"+settings.defaultISO+".qm")) { translations = Qt.createQmlObject('import QtQuick 1.1; import Panasonic.Pif 1.0; TranslationFileLoader{}',core); core.info("Core | Qt Translator Loaded.");}

        // Resolution
        if (cfgData.ResolutionMapping) {
            var mapArr = []; var rmap=cfgData.ResolutionMapping;
            for (var i in rmap) mapArr.push(coreHelper.replaceReservedChars(rmap[i]));
            settings.totalResolutions=i.split('i')[1]; settings.resolutionMap=mapArr.join(',');
            if (rmap['i'+settings.resolutionIndex]) settings.screenResolution = coreHelper.replaceReservedChars(rmap['i'+settings.resolutionIndex]);
            else if (rmap.i0) settings.screenResolution = coreHelper.replaceReservedChars(rmap.i0);
        }
        pif.__setConfiguration(cfgData,lruData); if (simData) pif.__setSimConfiguration(simData);

        // Set Window & Screen dimensions

        settings.screenWidth = settings.screenResolution.split("x")[0];
        settings.screenHeight = settings.screenResolution.split("x")[1];
        core.width=settings.screenWidth;
        core.height=settings.screenHeight;

        if(pif.getMonitorType()==pif.cLRU_VKARMA){
            if(pif.android){
                core.width=(settings.screenWidth>settings.screenHeight)?settings.screenWidth:settings.screenHeight;
                core.height=(settings.screenHeight<settings.screenWidth)?settings.screenHeight:settings.screenWidth;
            }
            else{
                core.width=(settings.screenWidth>settings.screenHeight)?settings.screenHeight:settings.screenWidth;
                core.height=(settings.screenHeight<settings.screenWidth)?settings.screenWidth:settings.screenHeight;
            }

            var res=settings.resolutionMap.split(',')[settings.totalResolutions==0?0:1];
            if (settings.totalResolutions==1) settings.orientationMap = (parseInt(res.split('x')[0],10)>=parseInt(res.split('x')[1],10))?'1|L,1|L,1|L,1|IL':'1|P,1|P,1|IP,1|P';
            else if (settings.totalResolutions==2) settings.orientationMap = (parseInt(res.split('x')[0],10)>=parseInt(res.split('x')[1],10))?"2|P,1|L,2|IP,1|IL":"1|P,2|L,1|IP,2|IL";
            
             pif.__setOrientation(pif.getOrientation());
            __setOrientation(pif.getOrientation());
        }

        // Load Theme Manager
        if (core.coreHelper.fileExists("../view/ThemeManager.qml")) {
            configTool=core.coreHelper.loadDynamicComponent("../view/ThemeManager.qml",core);
            configTool.dir= Qt.resolvedUrl(settings.contentPath+"/configtool/");
            core.info("Core | ThemeManager Loaded.");
        }

        // Set Interactive Language
        setInteractiveLanguage(settings.defaultLID,settings.defaultISO,true);

        dataController.__setConfiguration(cfgData); if (simData) dataController.__setSimConfiguration(simData);
        viewController.__setConfiguration(cfgData);
        __fontLoader();
        info("Core | settings.totalResolutions: "+settings.totalResolutions);
        info("Core | settings.orientationMap: "+settings.orientationMap);
        info("Core | settings.resolutionMap: "+settings.resolutionMap);
        info("Core | settings.contentPath: "+settings.contentPath);
        info("Core | settings.viewAssetPath: "+settings.viewAssetPath);
        info("Core | settings.assetPath: "+settings.assetPath);
        info("Core | settings.languageID: "+settings.languageID);
        info("Core | settings.languageISO: "+settings.languageISO);
        info("Core | settings.screenResolution: "+settings.screenResolution);
        info("Core | settings.screenWidth: "+settings.screenWidth);
        info("Core | settings.screenHeight: "+settings.screenHeight);
        info("Core | settings.resolutionIndex: "+settings.resolutionIndex);
        info("Core | core.width: "+core.width);
        info("Core | core.height: "+core.height);
        __configLoad = true; return true;
    }

    function __setAndroidOrientation(orientation_type){
        switch(orientation_type){
        case 'IP':{
            pif.android.setScreenOrientation(pif.android.cPORTRAIT);
            break;
        }
        case 'L':{
            pif.android.setScreenOrientation(pif.android.cLANDSCAPE);
            break;
        }
        case 'P':{
            pif.android.setScreenOrientation(pif.android.cINVERTED_PORTRAIT);
            break;
        }
        case 'IL':{
            pif.android.setScreenOrientation(pif.android.cINVERTED_LANDSCAPE);
            break;
        }
        }
    }

    function __setOrientation(orientation) {
        var resIndex=settings.orientationMap.split(',')[orientation].split('|')[0];
        var orientationType=(!__simOrientationType)?settings.orientationMap.split(',')[orientation].split('|')[1]:__simOrientationType;
        settings.screenWidth=settings.resolutionMap.split(',')[resIndex].split('x')[0];
        settings.screenHeight=settings.resolutionMap.split(',')[resIndex].split('x')[1];
        core.info("Core | orientationType: "+orientationType+" orientation: "+orientation+" resIndex: "+resIndex+" viewController.width: "+viewController.width+" viewController.height: "+viewController.height+" settings.orientationMap: "+settings.orientationMap);

        if(pif.android)
            __setAndroidOrientation(orientationType);
        else{

            viewController.rotation=(orientationType=='P')?0:(orientationType=='L')?90:(orientationType=='IP')?180:(orientationType=='IL')?270:viewController.rotation;
            viewController.x=(orientationType=='P')||(orientationType=='IP')?0:(orientationType=='L')||(orientationType=='IL')?(core.width-core.height)/2:viewController.x;
            viewController.y=(orientationType=='P')||(orientationType=='IP')?0:(orientationType=='L')||(orientationType=='IL')?(core.height-core.width)/2:viewController.y;
        }
        if (!(orientationType=='P'||orientationType=='L'||orientationType=='IP'||orientationType=='IL')) {core.info("Core | Unknown Orientation Type: "+orientationType); return false;}

        settings.resolutionIndex=resIndex;
        settings.assetPath=settings.viewAssetPath+"/assets"+resIndex;
        settings.screenResolution= settings.screenWidth+'x'+settings.screenHeight;
        return true;
    }

    // Configurations
    function __getLruData () {
        var cfile= "/etc/lru.cfg";
        if (!coreHelper.fileExists(cfile)) {
            if (pc) {
                cfile="../config/lruPC.cfg";
                if (!coreHelper.fileExists(cfile)) {core.debug("Lru file: "+cfile+" not found. Exiting..."); return false;}
            } else {core.debug("File: "+cfile+" not found. Exiting..."); return false;}
        }
        return coreHelper.getConfigFileData(cfile,"lru");
    }

    function __getConfigData (file) {
        var cfile="../config/"+file;
        if (!coreHelper.fileExists(cfile)) {core.debug("Config file: "+cfile+" not found. Exiting..."); return false;}
        return coreHelper.getConfigFileData(cfile,"lru");
    }

    function __setPifLogLevel(){
        log("Config.qml| CHECK PIF LOG LEVEL CALLED");
        switch(pifLogLevel.toLowerCase()){
        case "off":{ __pifLogLevel = cOFF; pifLogging.consoleLogLevel = PifLogging.Emergency; break;}
        case "error":{ __pifLogLevel = cERROR; pifLogging.consoleLogLevel = PifLogging.Error; break;}
        case "warn":{ __pifLogLevel = cWARN; pifLogging.consoleLogLevel = PifLogging.Warning; break;}
        case "info":{ __pifLogLevel = cINFO; pifLogging.consoleLogLevel = PifLogging.Info; break; }
        case "debug":{ __pifLogLevel = cDEBUG; pifLogging.consoleLogLevel = PifLogging.Verbose; break;}
        case "log":{ __pifLogLevel = cLOG; pifLogging.consoleLogLevel = PifLogging.Verbose; break; }
        case "verbose":{
            __pifLogLevel = cVERBOSE; pifLogging.file=true;
            if(pif.android) pifLogging.fileLogLevel=PifLogging.Highest;
            else{
                pifLogging.fileName="/tmp/interactive.log";
                pifLogging.consoleLogLevel=PifLogging.Highest;
            }
            break;
        }
        }
        log("Config.qml| PIF LOG LEVEL SET: "+__pifLogLevel);
    }

    function __fontLoader(){
        core.info ("Config.qml | __fontLoader | Loading Fonts...");
        var fontList = pif.__getFontList();
        for (var j=0;j<fontList.length;j++){
            Qt.createQmlObject('import QtQuick 1.1;  FontLoader{id:fon; source:  "'+core.settings.intPath+'fonts/'+fontList[j]+'"}',pif);
        }
    }

    function __setLogLevel(){
        log("Config.qml| CHECK LOG LEVEL CALLED");
        switch(logLevel.toLowerCase()){
        case "off": {__logLevel = cOFF; break;}
        case "error": {__logLevel = cERROR; break;}
        case "warn": {__logLevel = cWARN; break;}
        case "info": {__logLevel = cINFO; break; }
        case "debug": {__logLevel = cDEBUG; break; }
        case "log": {__logLevel = cLOG; break; }
        case "verbose":{ __logLevel = cVERBOSE; break;}
        }
        log("Config.qml| INTERACTIVE LOG LEVEL SET: "+__logLevel);
    }

    function getResolutionIndex(){return settings.resolutionIndex;}
    Component.onCompleted:{
        log ("Config.qml| CONFIG COMP LOAD COMPLETE");
        __prevTime = 0;
        __setLogLevel();
        __setPifLogLevel();
    }

    PifLogging { id: pifLogging; console:true; file: false; }
}
