import QtQuick 1.1
import Panasonic.Pif 1.0

Rectangle {
    id: seatinfo
    width: viewController.width;
    height: viewController.height;
    color: "#2132A4";
    property string screenInfoText;
    onHeightChanged: checkAndChangeFontSize();

    function checkAndChangeFontSize(){
        if (idText1.text=="") return;
        var pFont = idText1.font.pixelSize;
        var pHeight = idText1.paintedHeight;
        if(viewController.height!=0 && (viewController.height-idText1.y) < pHeight){ idText1.font.pixelSize = pFont - 1; checkAndChangeFontSize();}
    }

    FileReader {
        id: fileReader;
        source:(!core.pc)?"/var/log/screen.info":"/tmp/interactive/framework/components/seatInfo/SeatInfo.txt";
        onFileOpenError: { core.debug("SeatInfo.qml | onFileOpenError called. Cannot open file: "+fileReader.source); screenInfoText = "Cannot open file: "+fileReader.source;}
        onReadWhileNotOpenError: core.debug("SeatInfo.qml | onReadWhileNotOpenError called");
        onEndOfFile: core.debug("SeatInfo.qml | onEndOfFile called");
        onFileOpened: { core.debug("SeatInfo.qml | onFileOpened called"); screenInfoText = fileReader.readAll();}
    }

    Text{
        id: idText1;
        x: 50;
        y: 30;
        font.family: "pac futura nd, Arial"
        horizontalAlignment: Text.AlignLeft
        width: viewController.width;
        color: "white"
        font.pixelSize: 20
        text : screenInfoText;
        onTextChanged: checkAndChangeFontSize();
    }
}
