import QtQuick 1.1
import Panasonic.Pif 1.0

Rectangle {
    id: forceAudioVideo
    width: viewController.width;
    height: viewController.height;
    color: "black";

    UserEventFilter {
        id: eventFilter;
        enabled: core.testMode==1?false:true;
        onKeyPressed: { event.accepted=true; }
        onKeyReleased: { event.accepted=true; }
        onMousePressed: { event.accepted=true; }
        onMouseReleased: { event.accepted=true; }
    }
}
