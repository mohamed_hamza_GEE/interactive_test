WorkerScript.onMessage = function(message){
    var doc = new XMLHttpRequest();
    //console.log(" getFile.js | WorkerScript.onMessage | file : "+message.file);
    doc.onreadystatechange = function(){
        if(doc.readyState == XMLHttpRequest.DONE){
            if(doc.responseText){
                var jsonData  = JSON.parse(doc.responseText);
                WorkerScript.sendMessage({ 'message': message,'data':jsonData,'error':0});
		//console.log(" getFile.js | onreadystatechange | file : "+message.file);
            }
            else{
                WorkerScript.sendMessage({ 'message': message,'data':jsonData,'error':1 });
            }
        }
    }
    doc.open("GET", message.file);
    doc.send();
}
