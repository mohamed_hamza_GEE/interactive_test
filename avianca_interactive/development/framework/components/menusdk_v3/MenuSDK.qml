import QtQuick 1.1

Item{
    property real cTEMPLATE_VERSION : 3.0;
    property int debug : 0;
    property int dataReceivedCounter : 0;
    property int dataRequestCounter : 0;
    property string flightNo : "";
    property string source : "";
    property string destination : "";
    property string flightSetId : "";
    property string rootPath : "";
    property string language : "";
    property string version : "";
    property string menuDataVersion : "";
    property string defaultLanguage : "";
    property variant menuObject:null;
    property variant menuTree:null;
    property variant menuMetaData:null;             //It has the metadata of all tree nodes for current language.
    property variant defaultMenuMetaData:null;      //It has the metadata of all tree nodes for default language.
    property variant fileNodeProperty: null;
    property variant metaDataList:null;
    property variant supportedLanguages:null;
    signal initComplete(int errorCode);
    signal fileNodePropertyReceived(variant properties);
    signal fileNodeMetaDataReceived(variant metadata);
    signal languageChanged(int errorCode);
    signal metaDataReceived(variant metadata);
    signal sdcVersion(string version);
    //if(debug)console.log("");

    /*signal initComplete(int errorCode) will be fired when the init() is complete.
    function flow
    init() -> _getSDCVersion() -> _parseJson(7) -> _parseSdcVersion() ->_parseFlightSetJson(jsonObject) ->
    _getContentOfRootJson() -> _parseJson(2) -> _parseRootJson() -> _getMenuTreeInfo() -> _parseJson(3) ->
    _parseMenuTreeJson() -> _getMenuMetaData() -> _parseJson(4) -> _parseMenuMetaData() ->
    _getDefaultMenuMetaData() -> _parseJson(8) -> _parseDefaultMenuMetaData() -> signal initComplete()*/
    function init(flightId, source, destination, language, pathToMenu, version){
        console.log("MenuSDK::init() called. flightId :" + flightId + " source :" + source + " destination : " + destination + " language : " + language + " pathToMenu : " + pathToMenu + " version : " + version);
        if(flightId == undefined || source == undefined || destination == undefined || language == undefined || pathToMenu == undefined || version == undefined){
             console.log("MenuSDK::Invalid input");return false;
        }
        this.language = language;
        _setRootPath(pathToMenu);
        this.destination = destination;
        flightNo = flightId;
        this.source = source;
        this.version = version;
        _getSDCVersion();
        return true;
    }

    //signal languageChanged(int errorCode) will be fired when the changeLanguage() is complete.
    function changeLanguage(languageCode){
        console.log("MenuSDK::changeLanguage() called.");
        if(languageCode == undefined||languageCode==""){
            console.log("MenuSDK::Invalid input");
            languageChanged(1);
        }
        language = languageCode;
        _getMenuMetaData(true);
    }


    //signal sdcVersion(menuDataVersion);
    function getSDCVersion(){
        sdcVersion(menuDataVersion);
    }

    function _getSDCVersion(){
        var filePath = _getRootPath() + "/sdcVersion.json";
        console.log("MenuSDK Accessing file " + filePath);
        worker.sendMessage({ 'file':filePath, id:7});
        //_parseSdcVersion() will be called on success. sendError() on failure.
    }

    function getMenuInfo(){
        console.log("MenuSDK::getMenuInfo() called.");
        return menuObject;
    }

    function getRootNode(){
        console.log("MenuSDK::getRootNode() called.");
        if(menuObject.rootId) return getNode(menuObject.rootId);
        else return null;
    }

    function getChildList(key){
        console.log("MenuSDK::getChildList() called.");
        if(key==undefined||key==""){console.log("Invalid key"); return null}
        if(!_isNodeExist(key)){console.log("MenuSDK::Invalid key"); return null}
        if(menuTree && menuTree[key]["childList"])
            return menuTree[key]["childList"];
        else {console.log("MenuSDK::Child doesn't exist"); return null}
    }

    function getNode(key){
        console.log("MenuSDK::getNode() called. key:" + key);
        if(key==undefined||key==""){console.log("MenuSDK::Invalid key"); return null}
        if(!_isNodeExist(key)){console.log("MenuSDK::Invalid key"); return null}
        if(menuTree) return menuTree[key];
        else {console.log("MenuSDK::Invalid Menu Tree"); return null}
    }

    function getNodes(nodekeys){
        var data = new Object();
        var x;
        console.log("MenuSDK::getNodes() called.");
        if(nodekeys==undefined||nodekeys==""){console.log("MenuSDK::Invalid keys"); return null}
        for(var i in nodekeys){
            x = getNode(nodekeys[i]);
            if(x){
                data[nodekeys[i]] = x;
            }
        }
        return data;
    }

    function getFilteredChildKeys(nodekey,key,value){
        if(nodekey==undefined||nodekey==""){console.log("MenuSDK::Invalid key"); return null}
        return _getFilteredChildKeys(nodekey,key,value);
    }

    function getFilteredChildObjects(nodekey,key,value){
        if(nodekey==undefined||nodekey==""){console.log("MenuSDK::Invalid key"); return null}
        return getNodes(_getFilteredChildKeys(nodekey,key,value));
    }

    function _getFilteredChildKeys(nodekey,key,value){
        var childs1 = new Array();
        var childs2 = new Array();
        if(!_isNodeExist(nodekey)) return null;
        if(!menuTree[nodekey]["childList"])return null;
        childs1 = menuTree[nodekey]["childList"];
        for(var i in childs1){
            if(menuTree[childs1[i]]!=undefined){
                if(Array.isArray(menuTree[childs1[i]][key]) && menuTree[childs1[i]][key].indexOf(value)!=-1){
                    childs2.push(childs1[i]);
                }else if(menuTree[childs1[i]][key]==value){
                    childs2.push(childs1[i]);
                }
            }
        }
        return childs2;
    }

    /*
    signal fileNodeMetaDataReceived(variant metadata) will be fired when the getMetaData() is complete.
    if defaultLang == true, get default language data if data for current language is not available.
    function flow.
    getMetaData -> _getMetaData() -> _getFileProperties() ->_parseJson(5) ->_parsefileNodeProperty() ->_parseJson(6) ->
    _parsefileNodeMetaData() -> _setMetaDataListObj()
    */
    function getMetaData(nodekey,defaultLang){
        console.log("MenuSDK::getMetaData() called.");
        if(nodekey==undefined||nodekey==""){console.log("Invalid key"); fileNodeMetaDataReceived({"error":1});return;}
        if(defaultLang==undefined||defaultLang=="")defaultLang=false;
        _getMetaData(nodekey,0,defaultLang)
    }

    //signal metaDataReceived(variant metadata) will be fired when the getMetaDataOfNodes() is complete.
    function getMetaDataOfNodes(nodekeys,defaultLang){
        console.log("MenuSDK::getMetaDataOfNodes() called.");
        metaDataList = null;
        if(nodekeys==undefined||nodekeys==""){console.log("Invalid keys"); metaDataReceived({"error":1});return;}
        if(defaultLang==undefined||defaultLang=="")defaultLang=false;
        dataRequestCounter = nodekeys.length;
        dataReceivedCounter = 0;
        for(var i in nodekeys){
            _getMetaData(nodekeys[i],1,defaultLang);
        }
    }

    /*
    flag=0, _getMetaData() called by getMetaData(nodekey)
    flag=1, _getMetaData() called by getMetaDataOfNodes(nodekey)

    function flow.
    _getMetaData() -> _getFileProperties() ->_parseJson(5) ->_parsefileNodeProperty() ->_parseJson(6) ->
    _parsefileNodeMetaData() -> _setMetaDataListObj()
    */
    function _getMetaData(nodekey, flag, defaultLang){
        var s = new Array();
        if(debug)console.log("In _getMetaData() nodekey:: " + nodekey + "  defaultLang::  " + defaultLang);
        if(!_isNodeExist(nodekey)) return null;
        if(menuTree[nodekey]["nodeType"] == "treeNode"){
            if(menuMetaData[nodekey]){
                if(menuMetaData && (flag==1)){
                    _setMetaDataListObj(menuMetaData[nodekey],nodekey);
                }
                else fileNodeMetaDataReceived(menuMetaData[nodekey]);
            }else{
                if(flag==0 && defaultLang==true){
                    if(defaultMenuMetaData[nodekey]){
                        fileNodeMetaDataReceived(defaultMenuMetaData[nodekey]);
                    }else{
                        fileNodeMetaDataReceived({"error":1});
                    }
                }
                else if(flag==0 && defaultLang==false){
                    fileNodeMetaDataReceived({"error":1});
                }
                else if(flag==1 && defaultLang==true){
                    if(defaultMenuMetaData[nodekey]){
                        _setMetaDataListObj(defaultMenuMetaData[nodekey],nodekey);
                    }
                    else{
                        _checkCount();
                    }
                }
                else if(flag==1 && defaultLang==false){
                    _checkCount();
                }
            }
        }else if(menuTree[nodekey]["nodeType"] == "fileNode"){
            if(flag==1)_getFileProperties(nodekey,2,defaultLang);
            else _getFileProperties(nodekey,1,defaultLang);
        }
    }

    //signal fileNodePropertyReceived(variant properties) will be fired when the getFileProperties() is complete.
    function getFileProperties(nodekey){
        console.log("MenuSDK::getFileProperties() called.");
        _getFileProperties(nodekey,0);
    }

    /*
      flag = 0, getFileProperties() is calling the _getFileProperties()
      flag = 1, getMetaData(nodekey) is calling the _getFileProperties()
      flag = 2, getMetaDataOfNodes(nodekeys) is calling the _getFileProperties()
    */
    function _getFileProperties(nodekey, flag, defaultLang){
        var s = new Array();
        var fileNodeFolderPath;
        if(debug)console.log("In _getFileProperties() nodekey:: " + nodekey);
        if(!_isNodeExist(nodekey)) return null;
        if(menuTree[nodekey]["nodeType"] == "fileNode"){
            var t = _getRootPath() + "/" + menuTree[nodekey]["link"];
            s = t.split("/");
            s.pop();
            fileNodeFolderPath = s.join("/");
            console.log("MenuSDK Accessing file " + t);
            worker.sendMessage({ 'file':t, id:5,'flag':flag,'nodeKey':nodekey,'fileNodeFolderPath':fileNodeFolderPath, 'defaultLang':defaultLang});
            //_parsefileNodeProperty() will be called on success. sendError() on failure.
        }
    }

    function _isNodeExist(key){
        if(menuTree){
            return menuTree[key]!=undefined;
        }
        else return false;
    }

    function _setRootPath(pathToMenu){
        rootPath = pathToMenu;
    }

    function _getRootPath(){
        return rootPath;
    }

    function _getContentOfRootJson(){
        var filePath = _getRootPath() + "/root.json";
        console.log("MenuSDK Accessing file " + filePath);
        worker.sendMessage({ 'file':filePath, id:2});
        //_parseRootJson() will be called on success. sendError() on failure.
    }

    function _getMenuTreeInfo(){
        var file = _getRootPath() + "/" + menuObject.link;
        console.log("MenuSDK Accessing file " + file);
        worker.sendMessage({ 'file':file, id:3});
        //_parseMenuTreeJson() will be called on success. sendError() on failure.
    }

    /*
      flag = true, called by changeLanguage(languageCode)
      flag = false, called by _parseMenuTreeJson()
    */
    function _getMenuMetaData(flag){
        console.log("MenuSDK Accessing file " + _getRootPath() + "/" + language +"/" + menuObject.metadataLinks);
        if(flag)worker.sendMessage({ 'file':_getRootPath() + "/" + language +"/" + menuObject.metadataLinks, id:4,'flag':1});
        else worker.sendMessage({ 'file':_getRootPath() + "/" + language +"/" + menuObject.metadataLinks, id:4,'flag':0});
        //_parseMenuMetaData() will be called on success. sendError() on failure.
    }

    function _getDefaultMenuMetaData(flag){
        console.log("MenuSDK Accessing file " + _getRootPath() + "/" + defaultLanguage +"/" + menuObject.metadataLinks);
        if(flag)worker.sendMessage({ 'file':_getRootPath() + "/" + defaultLanguage +"/" + menuObject.metadataLinks, id:8,'flag':1});
        else worker.sendMessage({ 'file':_getRootPath() + "/" + defaultLanguage +"/" + menuObject.metadataLinks, id:8,'flag':0});
        //_parseDefaultMenuMetaData() will be called on success. sendError() on failure.
    }

    function _parseJson(jsonObject, type, flag, key, fileNodeFolderPath, defaultLang){
        switch(type){
            case 1:{
                _parseFlightSetJson(jsonObject);
                break;
            }case 2:{
                _parseRootJson(jsonObject);
                break;
            }case 3:{
                _parseMenuTreeJson(jsonObject);
                break;
            }case 4:{
                _parseMenuMetaData(jsonObject,flag);
                break;
            }case 5:{
                _parsefileNodeProperty(jsonObject, flag, key, fileNodeFolderPath, defaultLang);
                break;
            }case 6:{
                _parsefileNodeMetaData(jsonObject,flag,key);
                break;
            }case 7:{
                _parseSdcVersion(jsonObject);
                break;
            }case 8:{
                _parseDefaultMenuMetaData(jsonObject,flag);
                break;
            }case 9:{
                _parsefileNodeMetaData2(jsonObject,flag,key);
                break;
            }
        }
    }

    function sendError(type, flag, nodeKey, fileNodeFolderPath, defaultLang){
        //console.log("sendError() ")
        switch(type){
            case 1:{
                console.log("MenuSDK file not present.\n");
                initComplete(1);
                break;
            }case 2:{
                 console.log("MenuSDK file not present.\n");
                 initComplete(1);
                break;
            }case 3:{
                 console.log("MenuSDK file not present.\n");
                 initComplete(1);
                break;
            }case 4:{
                 console.log("MenuSDK file not present.\n");
                 if(flag==1)languageChanged(1);
                 else initComplete(1);
                break;
            }case 5:{
                 console.log("MenuSDK file not present.\n");
                 if(flag==0)fileNodePropertyReceived({'error':1});       //triggered by getFileProperties()
                 else if(flag==1)fileNodeMetaDataReceived({'error':1});  //triggered by getMetaData()
                 else if(flag==2)_checkCount();                          //triggered by getMetaDataOfNodes()
                 break;
            }case 6:{
                 if(defaultLang){
                     //console.log("MenuSDK file not present. trying to get file in default language\n");
                     if(flag==1 || flag==2){
                        var path = fileNodeFolderPath + "/" + defaultLanguage +"/" +fileNodeProperty["metadataLinks"];
                        console.log("MenuSDK Accessing file " + path);
                        worker.sendMessage({ 'file':path, id:9, 'flag':flag, 'nodeKey':nodeKey});
                        //_parsefileNodeMetaData2() will be called on success. sendError() on failure.
                     }
                 }
                 else{
                     if(flag==1){
                         fileNodeMetaDataReceived({'error':1});
                     }else if(flag==2){
                        _checkCount();
                     }
                 }
                 break;
            }case 7:{
                 console.log("MenuSDK file not present.\n");
                 initComplete(1);
                 break;
            }case 9:{
                 if(flag==1){
                     fileNodeMetaDataReceived({'error':1});  //triggered by getMetaData()
                 }
                 break;
            }
        }
    }

    function _parseFlightSetJson(jsonObject){
        if(debug)console.log("jsonObject['flightSetList'].length is " + jsonObject['flightSetList'].length);
        if(jsonObject['flightSetList'].length){
            for(var i in jsonObject['flightSetList']){
                for(var j in jsonObject['flightSetList'][i]['list']){
                    if(jsonObject['flightSetList'][i]['list'][j]['destination']==destination && jsonObject['flightSetList'][i]['list'][j]['flightNo']==flightNo && jsonObject['flightSetList'][i]['list'][j]['source']==source){
                        flightSetId = jsonObject['flightSetList'][i]['flightSetId'];
                    }
                }
            }
        }
        else flightSetId = "";
        _getContentOfRootJson();
    }

    function _parseSdcVersion(jsonObject){
        var sdcVersion = jsonObject['sdcVersion'];
        var sdcCompatibilityVersion = jsonObject['sdcCompatibilityVersion'];
        var templateVersion = jsonObject['templateVersion'];
        var givenSdcVersion = version;
        sdcVersion = parseFloat(sdcVersion).toFixed(2);
        givenSdcVersion = parseFloat(givenSdcVersion).toFixed(2);
        sdcCompatibilityVersion = parseFloat(sdcCompatibilityVersion).toFixed(2);
        templateVersion = parseFloat(templateVersion).toFixed(2);
        if(cTEMPLATE_VERSION != templateVersion){
            console.log("MenuSDK::Improper template version");
            initComplete(2);
        }else{
            if((givenSdcVersion <= sdcVersion) && (givenSdcVersion >= sdcCompatibilityVersion)){
                if(debug)console.log("Proper SDC version");
                menuDataVersion = templateVersion;
                var filePath = _getRootPath() + "/flightsets.json";
                console.log("MenuSDK Accessing file " + filePath);
                worker.sendMessage({ 'file':filePath, id:1});
                //_parseFlightSetJson() will be called on success. sendError() on failure.
            }
            else{
                console.log("MenuSDK::Improper SDC version");
                initComplete(3);
            }
        }
    }

    function _parseRootJson(jsonObject){
        var isLanguageSupported = false;
        var flightsetIndex = -1;
        var defaultFlightsetIndex;
        for(var i in jsonObject['rootMenus']){
            if(jsonObject['rootMenus'][i]['flightset'] == flightSetId){
                flightsetIndex = i;
                break;
            }
            if(jsonObject['rootMenus'][i]['flightset'] == "default"){
                defaultFlightsetIndex = i;
            }
        }
        if(flightsetIndex == -1){
            flightsetIndex = defaultFlightsetIndex;
        }
        menuObject = jsonObject['rootMenus'][flightsetIndex];
        defaultLanguage = menuObject.defaultLanguage;
        supportedLanguages = menuObject.languages;
        for(var j in supportedLanguages){
            if(supportedLanguages[j]==language){
                isLanguageSupported=true;break;
            }
        }
        if(!isLanguageSupported) language = defaultLanguage;
        //console.log("defaultLanguage::  " + defaultLanguage);
        _getMenuTreeInfo();
    }

    function _parseMenuTreeJson(jsonObject){
        menuTree = jsonObject;
        _getMenuMetaData(false);
    }

    function _parseMenuMetaData(jsonObject,flag){
        menuMetaData = jsonObject;
        if(defaultLanguage == language){
            defaultMenuMetaData = menuMetaData;
            if(flag)languageChanged(0);
            else initComplete(0);
        }else{
            //request metadata of tree nodes for default language.
            _getDefaultMenuMetaData(flag);
        }
    }

    function _parseDefaultMenuMetaData(jsonObject, flag){
        //console.log("In _parseDefaultMenuMetaData()  defaultMenuMetaData is ::\n");
        defaultMenuMetaData = jsonObject;
        _printContent(defaultMenuMetaData);
        if(flag)languageChanged(0);
        else initComplete(0);
    }

    function _parsefileNodeProperty(jsonObject, flag, key, fileNodeFolderPath, defaultLang){
        if(debug)console.log("In _parsefileNodeProperty() flag:: " + flag + " key:: " + key);
        fileNodeProperty = jsonObject;
        //if(debug)console.log("Content of JSON Object is \n");
        //if(debug)_printContent(jsonObject);
        if(flag == 0){fileNodePropertyReceived(fileNodeProperty);}
        else{
            var path = fileNodeFolderPath + "/" + language +"/" +fileNodeProperty["metadataLinks"];
            console.log("MenuSDK Accessing file " + path);
            worker.sendMessage({ 'file':path, id:6, 'flag':flag, 'nodeKey':key, 'fileNodeFolderPath':fileNodeFolderPath, 'defaultLang':defaultLang});
            //_parsefileNodeMetaData() will be called on success. sendError() on failure.
        }
    }

    function _parsefileNodeMetaData(jsonObject,flag,key){
        if(flag == 1)fileNodeMetaDataReceived(jsonObject);
        else if(flag == 2){
            _setMetaDataListObj(jsonObject,key);
        }
    }


    function _parsefileNodeMetaData2(jsonObject,flag,key){
        //console.log("In _parsefileNodeMetaData2() key:: " + key);
        if(flag == 1)fileNodeMetaDataReceived(jsonObject);
        else if(flag == 2){
            _setMetaDataListObj(jsonObject,key);
        }
    }


    function _parsefileNodeMetaData3(jsonObject,flag,key){
        if(flag == 1)fileNodeMetaDataReceived(jsonObject);
        else if(flag == 2){
            _setMetaDataListObj(jsonObject,key);
        }
    }

    function _setMetaDataListObj(jsonObject,key){
        var x = new Object();
        if(metaDataList)x = metaDataList;
        x[key] = jsonObject;
        metaDataList = x;
        _checkCount();
    }

    function _checkCount(){
        dataReceivedCounter++;
        if(dataReceivedCounter==dataRequestCounter){
            if(metaDataList) metaDataReceived(metaDataList);
            else metaDataReceived({'error':1});
        }
    }

    function _printContent(data){
        for (var key in data){
            if (typeof(data[key]) == "object" && data[key] != null){
                console.log(key + " : " + data[key]);
                _printContent(data[key]);
            }else{
                console.log(key + " : " + data[key]);
            }
        }
    }

    WorkerScript {
        id: worker
        source: "getFile.js"
        onMessage: {
            if(messageObject.error==0){
                //console.log("success messageObject.message.id:: " + messageObject.message.id + "  messageObject.message.flag :: " + messageObject.message.flag);
                _parseJson(messageObject.data, messageObject.message.id, messageObject.message.flag, messageObject.message.nodeKey, messageObject.message.fileNodeFolderPath,messageObject.message.defaultLang);
            }
            else{
                //console.log("failure messageObject.message.id:: " + messageObject.message.id + "  messageObject.message.flag :: " + messageObject.message.flag + "   messageObject.message.fileNodeFolderPath ::  " + messageObject.message.fileNodeFolderPath);
                sendError(messageObject.message.id, messageObject.message.flag, messageObject.message.nodeKey, messageObject.message.fileNodeFolderPath, messageObject.message.defaultLang);
            }
        }
    }
    Component.onCompleted:{
        console.log("menuSDK component loaded.");
    }
}
