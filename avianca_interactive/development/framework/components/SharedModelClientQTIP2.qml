import QtQuick 1.1
import Panasonic.Pif 1.0

SharedModel{
    id: sm

    property string apiName;
    property string languageIso;
    property int seatbackVolume;
    property int seatbackPaVolume;
    property int headsetVolume;
    //property int seatbackBrightness;
    //property int handsetBrightness;
    //property bool backlightEnabled:true;
    //property int mid;
    //property int aggregateMid;
    //property int soundtrackLid;
    //property int soundtrackType;
    //property int subtitleLid;
    //property int subtitleType;
    //property int elapsedTime;
   // property string aspectRatio; // possible values: "4x3","16x9","Fullscreen","Not Adjustable"
    //property string mediaRepeat;  // possible values: "none","current","all"
    //property bool mediaShuffle:false;
    //property string mediaType;
    //property string mediaPlayRate;
    //property string mediaPlayerState; // possible values: "play", "pause", "resume", "stop", "fastforward", "rewind"
    property variant launchAppId:[]; //variant type should have default initialisation as []
    property bool iPodConnected:false;
    property bool usbConnected:false;
    //property bool showKeyboard;
    //property string keyboardData;
    property bool usbAgreement;
    property bool iPodAgreement;
    //property variant playlists:{"aod":{},"vod":{},"kidsaod":{},"kidsvod":{}};
    property variant apiParameters:[];
    //property bool muteVolume:false;
    property string blueRayStatus: "stop";
    property string blueRayMediumType: "NONE";  // possible medium type can be "BD", "DVD", "CD" and "NONE".
    //property variant resumeTimeMap: [];
	property int noOfDimmableWindows:0;

    //QTIP 2.0
    property int seatbackBrightness;
    property int handsetBrightness;
    //property int clientBacklightEnabled;
    property bool serverBacklightEnabled:true;
    property bool serverKeyboardVisible:false;
    property int languageLid;
    property bool allowIncomingCalls;
    property int telephonyVolume;
    property string interactiveMode;
    property bool clientKeyboardVisible;
    property string keyboardData;
    property int volume;
    property bool muteEnabled;
    property variant resumeTimeMap: [];
    property variant midInforArr: [];
    property variant playlists:{"aod":{},"vod":{},"kidsaod":{},"kidsvod":{},"usb":{}};

    //Media Player 1 for AOD
    property string mediaPlayer1MediaType;
    property int mediaPlayer1MediaIdentifier;
    property int mediaPlayer1MediaAggregateIdentifier;
    property int mediaPlayer1ElapsedTime;
    property string mediaPlayer1PlayStatus;
    property int mediaPlayer1SubtitleLid;
    property int mediaPlayer1SoundtrackLid;
    property string mediaPlayer1AspectRatio;
    property variant mediaPlayer1AvailableAspectRatios;
    property bool mediaPlayer1Shuffle:false;
    property string mediaPlayer1Repeat;
    property int mediaPlayer1PlayRate;
    property bool mediaPlayer1SoundtrackEnabled;
    property int mediaPlayer1MediaAlbumId;  //contains albumid/playlist ID. This property won't be available in non-woi applications like PAC and ICS.

    //Media Player 2 for VOD
    property string mediaPlayer2MediaType;
    property int mediaPlayer2MediaIdentifier;
    property int mediaPlayer2MediaAggregateIdentifier;
    property int mediaPlayer2ElapsedTime;
    property string mediaPlayer2PlayStatus;
    property int mediaPlayer2SubtitleLid;
    property int mediaPlayer2SubtitleType; //This property won't be available in non-woi applications like PAC and ICS.
    property int mediaPlayer2SoundtrackLid;
    property string mediaPlayer2AspectRatio;
    property variant mediaPlayer2AvailableAspectRatios;
    property bool mediaPlayer2Shuffle:false;
    property string mediaPlayer2Repeat;
    property int mediaPlayer2PlayRate;
    property bool mediaPlayer2SoundtrackEnabled;
    property bool mediaPlayer2IsTrailer:false;
    property int mediaPlayer2MediaAlbumId;  //contains albumid/playlist ID. This property won't be available in non-woi applications like PAC and ICS.

    // Media Player 3 for Games
    property string mediaPlayer3MediaType;
    property int mediaPlayer3MediaIdentifier;
    property string mediaPlayer3PlayStatus;

    //SeatChat
    property variant eventInfoData;  // {event:<event>,eventInfo:<eventInfo>}
    property variant seatToVkarmaSeatChatRequest; // {"apiName":<apiName>,"params":<params>,"callBackFunction":<callBackFunction>,"returnObj":<returnObj>}
    property variant seatToVkarmaSeatChatWithParams; //{"apiName":<apiName>,"params":<params>,"callBackFunction":<callBackFunction>,"returnObj":<returnObj>,"extraParams":<extraParams>}



    onApiNameChanged: {if(!privateObj.ignorePropertyChange && apiName!="") dataReceived(apiName);}
    onSeatbackVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("seatbackVolume");}
    onSeatbackPaVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("seatbackPaVolume");}
    onHeadsetVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("headsetVolume");}
    onSeatbackBrightnessChanged: {if(!privateObj.ignorePropertyChange) dataReceived("seatbackBrightness");}

    //onMediaShuffleChanged: { if(!privateObj.ignorePropertyChange) dataReceived("mediaShuffleAck");}
    onMediaPlayer1ShuffleChanged: { if(!privateObj.ignorePropertyChange) dataReceived("mediaShuffleAck");}
    onServerBacklightEnabledChanged: { if(!privateObj.ignorePropertyChange) dataReceived("backlightEnabled");}
    onIPodConnectedChanged: { if(!privateObj.ignorePropertyChange) dataReceived("iPodConnected");}
    onUsbConnectedChanged: { if(!privateObj.ignorePropertyChange) dataReceived("usbConnected");}
    //onShowKeyboardChanged: { if(!privateObj.ignorePropertyChange) dataReceived("showKeyboard");}
    onServerKeyboardVisibleChanged:  { if(!privateObj.ignorePropertyChange) dataReceived("showKeyboard");}
    onKeyboardDataChanged: { if(!privateObj.ignorePropertyChange) dataReceived("keyboardData");}
    onUsbAgreementChanged: { if(!privateObj.ignorePropertyChange) dataReceived("usbAgreement");}
    onIPodAgreementChanged: { if(!privateObj.ignorePropertyChange) dataReceived("iPodAgreement");}
    onMuteEnabledChanged:  {if(!privateObj.ignorePropertyChange) dataReceived("muteVolume");}
    onPlaylistsChanged: {if(!privateObj.ignorePropertyChange) dataReceived("playlists");}

    //QTIP 2.0
    onLanguageLidChanged: {if(!privateObj.ignorePropertyChange){
            core.debug("SharedModelClient.qml | onLanguageLidChanged: "+languageLid);
            languageIso=dataController.getLanguageIsoByLid(String(languageLid))[0];
            dataReceived("changeLanguage",{languageIso:languageIso,languageLid:languageLid});
        }
    }
    onMediaPlayer2PlayStatusChanged:{
            if(privateObj.ignorePropertyChange){return false;}
            if(mediaPlayer2PlayStatus=="") return false;
            if(mediaPlayer2PlayStatus=="play"){dataReceived((mediaPlayer2IsTrailer)?"playTrailerAck":(mediaPlayer2MediaAggregateIdentifier==0)?"playVodAck":"playVodAggregateAck",{mid:mediaPlayer2MediaIdentifier,mediaType:mediaPlayer2MediaType,aggregateMid:(mediaPlayer2MediaAggregateIdentifier==0)?mediaPlayer2MediaIdentifier:mediaPlayer2MediaAggregateIdentifier,soundtrackLid:mediaPlayer2SoundtrackLid,soundtrackType:2,subtitleLid:mediaPlayer2SubtitleLid,subtitleType:mediaPlayer2SubtitleType,elapsedTime:mediaPlayer2ElapsedTime});}
            if(mediaPlayer2PlayStatus=="pause"){dataReceived("mediaPauseAck");}
            if(mediaPlayer2PlayStatus=="stop"){dataReceived("videoStopAck");}
    }
    onMediaPlayer2ElapsedTimeChanged:{if(!privateObj.ignorePropertyChange)dataReceived("setMediaPositionAck",{elapsedTime:mediaPlayer2ElapsedTime});}
    onMediaPlayer2AspectRatioChanged: {if(!privateObj.ignorePropertyChange)dataReceived("aspectRatioAck",{aspectRatio:mediaPlayer2AspectRatio});}
    onMediaPlayer2SoundtrackLidChanged: {if(!privateObj.ignorePropertyChange)dataReceived("videoSoundtrackAck",{soundtrackLid:mediaPlayer2SoundtrackLid,soundtrackType:2});}
    onMediaPlayer2SubtitleLidChanged: {if(!privateObj.ignorePropertyChange)dataReceived("videoSubtitleAck",{subtitleLid:mediaPlayer2SubtitleLid,subtitleType:mediaPlayer2SubtitleType});}
    onMediaPlayer2PlayRateChanged: {if(privateObj.ignorePropertyChange){return false;};if(mediaPlayer2PlayRate==100){return true;}if(mediaPlayer2PlayRate<0){dataReceived("mediaRewindAck",{mediaPlayRate:(parseInt(mediaPlayer2PlayRate,10)*-1)})} else {dataReceived("mediaFastForwardAck",{mediaPlayRate:mediaPlayer2PlayRate})}}

    onMediaPlayer1PlayStatusChanged:{
            if(privateObj.ignorePropertyChange){return false;}
            if(mediaPlayer1PlayStatus=="") return false;
            if(mediaPlayer1PlayStatus=="play"){dataReceived("playAodAck",{mid:mediaPlayer1MediaIdentifier,aggregateMid:mediaPlayer1MediaAggregateIdentifier,mediaRepeat:mediaPlayer1Repeat,mediaType:mediaPlayer1MediaType});}
            if(mediaPlayer1PlayStatus=="pause"){dataReceived("mediaPauseAck");}
            if(mediaPlayer1PlayStatus=="stop"){dataReceived("audioStopAck");}
    }
    onMediaPlayer1ElapsedTimeChanged:{if(!privateObj.ignorePropertyChange)dataReceived("setMediaPositionAck",{elapsedTime:mediaPlayer1ElapsedTime});}
    onMediaPlayer1RepeatChanged: {if(!privateObj.ignorePropertyChange)dataReceived("mediaRepeatAck",{mediaRepeat:mediaPlayer1Repeat});}

// not neeed since apiData() is send from seatback
//    onMediaPlayer3PlayStatusChanged:{
//            if(privateObj.ignorePropertyChange){return false;}
//            if(mediaPlayer3PlayStatus=="") return false;
//            if(mediaPlayer3PlayStatus=="play"){dataReceived("launchAppAck",{launchAppId:mediaPlayer3MediaType});} //mediaPlayer3MediaIdentifier willn't be able to hold "game" since it is an integer. So we are using mediaPlayer3MediaType.
//            if(mediaPlayer3PlayStatus=="stop"){dataReceived("closeAppAck");}
//    }

   //SeatChat
    onEventInfoDataChanged: {if(!privateObj.ignorePropertyChange && eventInfoData!="") dataReceived("eventInfoData");}
    onSeatToVkarmaSeatChatRequestChanged: {
       if(!privateObj.ignorePropertyChange && seatToVkarmaSeatChatRequest!="")dataReceived("seatToVkarmaSeatChatRequest");
    }
    onSeatToVkarmaSeatChatWithParamsChanged: {if(!privateObj.ignorePropertyChange && seatToVkarmaSeatChatWithParams!="")dataReceived("seatToVkarmaSeatChatWithParams");}



    function dataReceived(apiName){ core.debug("SharedModelClient.qml | dataReceived(api) called: "+apiName);}

    function sendApiData(api,params){       // Sending bunch of property change to the shared model
        core.debug("SharedModelClient.qml | Control in sendApiData("+api+",params)")
        privateObj.ignorePropertyChange = true;
        //sm.apiName = "";
        //apiParameters=params;
        //sm.apiName = api;
        sm.sendCommandToServer(api,params);
        privateObj.ignorePropertyChange = false;
    }

    function sendPropertyData(propertyName,propertyValue){      // Sending single property change to the shared model
        privateObj.ignorePropertyChange=true;
        core.debug("SharedModelClient.qml | sendPropertyData("+propertyName+","+propertyValue+")");
        sm.setPropertyValue(propertyName, propertyValue);
        privateObj.ignorePropertyChange=false;
    }

    Component.onCompleted: { privateObj.properties = sm.userProperties(); core.info("SharedModelClient.qml | SHARED MODEL CLIENT LOAD COMPLETE")}

    QtObject{id:privateObj; property bool ignorePropertyChange: false; property variant properties:[];}
}
