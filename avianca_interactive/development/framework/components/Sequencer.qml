import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Info

Item {
    id:imageSequencer;
    //CONSTANTS
    property int cSEQUENCEPLAY: 1;
    property int cSEQUENCEPAUSE: 2;
    property int cSEQUENCEREWIND: 3;
    property int cSEQUENCEFORWARD: 4;
    property int cSEQUENCESTOP : 5;

    property int cPLAY: 1;
    property int cPAUSE: 2;
    property int cREWIND: 3;
    property int cFORWARD: 4;
    property int cSTOP : 5;

    property int playingIndex:-1;
    property int playingInterval: 0;
    property bool loop:true;
    property int modelCount:0;
    property int indexCount:modelCount-1;
    property int sequenceState:cSEQUENCESTOP;
    property int lastRequest:cSTOP;

    property alias slideShowModel: slideShowModel

    signal sigPlayedIndex(int index);
    signal sigSequencerCompleted();
    signal sigSequencerPlayBegan();
    signal sigSequencerPaused();
    signal sigSequencerResumed();
    signal sigSequencerPlayStopped();
    signal sigSequencerNextIndex(int index);
    signal sigSequencerPreviousIndex(int index);
    signal sigSequencerShuffled();
    signal sigModelReady();

    function onTimerTriggered(){return setNextIndex();}
    function setRepeat(flag){core.debug("Sequencer.qml | setRepeat, flag: "+flag); loop=flag;}

    function getSequenceState(){
        return sequenceState
    }

    function initiateSequencer(interval,repeat,triggerOnStart){
        core.debug("Sequencer.qml | initiateSequencer, modelCount: "+slideShowModel.count+" interval: "+interval+" repeat: "+repeat+" triggerOnStart: "+triggerOnStart);
        modelCount = slideShowModel.count; loop = repeat;
        changeTimerSequencer(interval)
        sequenceTimer.triggeredOnStart = triggerOnStart;
    }

    function changeTimerSequencer(interval){
        core.debug("Sequencer.qml | changeTimerSequencer, interval: "+interval);
        playingInterval = interval*1000;
        sequenceTimer.interval = playingInterval;
    }

    function reinitialiseSequencer(interval,triggerOnStart){
        core.debug("Sequencer.qml | reinitialiseSequencer, interval: "+interval+" triggerOnStart: "+triggerOnStart);
        sequenceTimer.running = false;
        changeTimerSequencer(interval)
        sequenceTimer.running = true;
        sequenceTimer.triggeredOnStart = triggerOnStart;
    }

    function playSequencer(index){
        core.debug("Sequencer.qml | playSequencer, index: "+index);
        if(index && (index < modelCount)) playingIndex = index; else playingIndex = 0;
        sigPlayedIndex(playingIndex);
        sequenceTimer.running = true; sequenceState = cSEQUENCEPLAY; lastRequest = cPLAY;
        sigSequencerPlayBegan();
    }

    function pauseSequencer(){
        core.debug("Sequencer.qml | pauseSequencer");
        sequenceTimer.running = false; sequenceState = cSEQUENCEPAUSE; lastRequest = cPAUSE;
        sigSequencerPaused();
    }

    function resumeSequencer(){
        core.debug("Sequencer.qml | resumeSequencer");
        sequenceTimer.running = true; sequenceState = cSEQUENCEPLAY; lastRequest = cPLAY;
        sigSequencerResumed();
    }

    function setPreviousIndex(){
        if((playingIndex==0) && !loop){ core.debug("Sequencer.qml | First index reached & repeat is off, Exiting..."); return false;}
        var running = false;
        if(sequenceTimer.running){running = true; sequenceTimer.running = false}
        if(playingIndex>0) playingIndex --; else playingIndex = indexCount;
        if(running) sequenceTimer.running = true;
        sigPlayedIndex(playingIndex); sigSequencerPreviousIndex(playingIndex);
        core.debug("Sequencer.qml | setPreviousIndex, playingIndex: "+playingIndex);
        return true;        
    }

    function setNextIndex(){
        if((playingIndex==indexCount) && !loop){
            core.debug("Sequencer.qml | Last index reached & repeat is off, Exiting...");
            sequenceTimer.running=false; sequenceState=cSEQUENCESTOP;
            sigSequencerCompleted();
            return false;
        }
        var running = false;
        if(sequenceTimer.running){running = true; sequenceTimer.running = false}
        if(playingIndex<indexCount) playingIndex ++; else playingIndex = 0;
        if(running) sequenceTimer.running = true;
        sigPlayedIndex(playingIndex); sigSequencerNextIndex(playingIndex);
        core.debug("Sequencer.qml | setNextIndex, playingIndex: "+playingIndex);
        return true;       
    }

    function stopSequencer(){
        core.debug("Sequencer.qml | stopSequencer");
        sequenceTimer.running = false;
        sequenceState = cSEQUENCESTOP; playingIndex = -1; lastRequest = cSTOP;
        sigSequencerPlayStopped();
    }

    function setShuffle(flag){
        core.debug("Sequencer.qml | setShuffle, flag: "+flag);
        if(flag){
            slideShowModel.shuffle();
        }else {
            __populateSlideShowModel();
        }
        sigSequencerShuffled();
    }

    function populateSlideShowModel(path,filterList){
        resourceBrowserSlideShowModel.fileFilters=(filterList)?filterList:["*.jpg","*.jpeg","*.png","*.gif","*.bmp"];
        //resourceBrowserSlideShowModel.folder=path;
        Info.tmpPath=path;
        delayTimer.restart();
    }

    function __populateSlideShowModel(){
        slideShowModel.clear();
        for(var i=0;i<resourceBrowserSlideShowModel.count;i++){
            var ptr = resourceBrowserSlideShowModel.at(i);
            if(!(resourceBrowserSlideShowModel.isFolder(ptr.pathfilename)) && (parseInt(ptr.filesize,10)<=Math.ceil(pif.getUsbMaxImagefileSize()*1048576))){
                    slideShowModel.append({"pathfilename":ptr.pathfilename,"filenameonly":ptr.filenameonly,"filesize":ptr.filesize,"filetype":ptr.filetype,"lastmodifiedstring":ptr.lastmodifiedstring,"lastmodified":ptr.lastmodified});
            }
        }
        core.debug("Sequencer.qml | __populateSlideShowModel | slideShowModel.count: "+slideShowModel.count);
        sigModelReady();       
        return true;
    }

    SimpleModel{id:slideShowModel;}
    ResourceBrowserModel {
        id:resourceBrowserSlideShowModel; showdotfiles:false;
       // fileFilters:["*.jpg","*.jpeg","*.png","*.gif"]
        onFolderChanged: __populateSlideShowModel();
    }
    Timer { id:delayTimer; interval:200;running:false; repeat:false; onTriggered:  {
              resourceBrowserSlideShowModel.folder=Info.tmpPath;
        } }
    Timer { id:sequenceTimer; running:false; repeat:true; onTriggered: if(pif.getPAState()==0)onTimerTriggered(); }
}
