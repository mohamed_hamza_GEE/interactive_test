import QtQuick 1.1

Item {
    id: audioPlayerdataSMClient
    property int aMid:0;
    property string aMidTitle:'';
    property string aMidArtist:'';
    property string aMidPoster:'';
    property string aMidSynopsisPoster:'';
    property string aYear:'';
    property string aGenre:'';
    property int mid:0;
    property string midTitle:'';
    property string midArtist:'';
    property string midPoster:'';
    property string midSynopsisPoster:'';
    property bool midDataFetched: false;
    property variant __ref:pif.vkarmaToSeat;

    //Usb Metadata
    property string midFileName: '';
    property string midPathFileName:'';
    property int midDuration;

    Connections {
        target: __ref;
        onSigDataReceivedFromSeat:{
            if(api=="frameworkUpdateAudioPlayerdata"){
                eval(params.functionName+"(params)");
            }else if(api=="audioStopAck"){
                aMidTitle=''; aMidArtist=''; aMidPoster=''; aMidSynopsisPoster=''; aYear=''; aGenre=''; aMid=0; midFileName='';
                midTitle=''; midArtist=''; mid=0; midPoster=''; midSynopsisPoster='';
            }
        }
    }

    function setReference(ref){__ref=ref; return true;}

    function aMidDataReady (params) {
        aMidTitle = (params.aMidTitle)?params.aMidTitle:(aMidTitle!='')?aMidTitle:'';
        aMid = (params.aMid)?params.aMid:(aMid!=0)?aMid:0;
        aMidArtist = (params.aMidArtist)?params.aMidArtist:(aMidArtist!='')?aMidArtist:'';
        aMidPoster = (params.aMidPoster)?params.aMidPoster:(aMidPoster!='')?aMidPoster:'';
        aMidSynopsisPoster = (params.aMidSynopsisPoster)?params.aMidSynopsisPoster:(aMidSynopsisPoster!='')?aMidSynopsisPoster:'';
        aYear = (params.aYear)?params.aYear:(aYear!='')?aYear:'';
        aGenre = (params.aGenre)?params.aGenre:(aGenre!='')?aGenre:'';
    }

    function midDataReady (params) {
        midTitle = (params.midTitle)?params.midTitle:(midTitle!='')?midTitle:'';
        midArtist = (params.midArtist)?params.midArtist:(midArtist!='')?midArtist:'';
        midPoster = (params.midPoster)?params.midPoster:(midPoster!='')?midPoster:'';
        midSynopsisPoster = (params.midSynopsisPoster)?params.midSynopsisPoster:(midSynopsisPoster!='')?params.midSynopsisPoster:'';
        mid = (params.mid)?params.mid:(mid!=0)?mid:0;
        midDuration = (params.midDuration)?params.midDuration:(midDuration!=undefined || midDuration!=0)?midDuration:undefined;
    }

    function usbMidDataReady(params){
        aMidTitle = params.aMidTitle;
        midTitle = params.midTitle;
        midArtist = params.midArtist;
        midPathFileName = params.midPathFileName;
        midFileName = params.midFileName;
        midSynopsisPoster = params.midSynopsisPoster;
        midDuration = params.midDuration;
    }
}
