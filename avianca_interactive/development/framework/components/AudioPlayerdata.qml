import QtQuick 1.1

Item {
    id: audioPlayerData
    property int aMid:0;
    property string aMidTitle:'';
    property string aMidArtist:'';
    property string aMidPoster:'';
    property string aMidSynopsisPoster:'';
    property string aYear:'';
    property string aGenre:'';
    property int mid:0;
    property string midTitle:'';
    property string midArtist:'';
    property string midPoster:'';
    property string midSynopsisPoster:'';
    property bool midDataFetched: false;

    //Usb Metadata
    property string midFileName: '';
    property string midPathFileName:'';
    property int midDuration;
    signal sigUpdateSMCAudioPlayerdata(variant params);

    Connections {
        target: core;
        onSigLanguageChanged:{
            if((pif.getLSAmediaSource()!="usb") && (aMid)){
                core.dataController.getAodPlayerData([aMid],aMidDataReady);midDataFetched=true; // Delay fetching of mid data
            }
            return true;
        }
    }

    Connections {
        target: pif.lastSelectedAudio;
        onSigMidPlayBegan: {
            if(pif.getLSAmediaSource()=="usb"){mid=cmid; metadataTimer.restart();}
            else {
                if (amid!=aMid && amid>0){ core.dataController.getAodPlayerData([amid],aMidDataReady); aMid=amid; mid = cmid; midDataFetched=true;} // Delay fetching of mid data
                if (cmid!=mid){ if(!midDataFetched) core.dataController.getAodPlayerData([cmid],midDataReady); mid = cmid;}
            }
            return true;
        }

        onSigMidPlayStopped: {
            aMidTitle=''; aMidArtist=''; aMidPoster=''; aMidSynopsisPoster=''; aYear=''; aGenre=''; aMid=0; midFileName='';
            midTitle=''; midArtist=''; mid=0; midPoster=''; midSynopsisPoster='';
        }
    }

    Connections {
        target: (pif.seatInteractive)?pif.seatInteractive:null;
        onSigDataReceived:{
            if(api=="frameworkGetAudioPlayerdata"){
                if(params.mediaType=="mp3") metadataTimer.restart();
                else if(params.mediaType=="audio" || params.mediaType=="audioAggregate"){updateSMCaMidDataReady();updateSMCmidDataReady();}
            }
        }
    }

    function aMidDataReady (dmodel) {
        core.debug("aMidDataReady: Count: "+dmodel.count);
        if(dmodel.count){
            var t = dmodel.at(0);
            aMidTitle = (t.title)?t.title:t.short_title;
            aMid = t.mid;
            aMidArtist = (t.artist)?t.artist:"";
            aMidPoster = (t.poster)?t.poster:"";
            aMidSynopsisPoster = (t.synopsisposter)?t.synopsisposter:"";
            aYear = (t.year)?t.year:"";
            aGenre = (t.genre)?t.genre:"";
            core.debug("aMidDataReady: aMidPoster: "+aMidPoster); core.debug("aMidDataReady: aMidSynopsisPoster: "+aMidSynopsisPoster);
        }
        if (midDataFetched){ if(pif.getLSAmediaSource()!="broadcastAudio") core.dataController.getAodPlayerData([mid],midDataReady); midDataFetched=false;}
        updateSMCaMidDataReady();
    }
    function updateSMCaMidDataReady(){
        var params={"functionName":"aMidDataReady", "aMidTitle":aMidTitle, "aMid": aMid, "aMidArtist": aMidArtist, "aMidPoster": aMidPoster, "aMidSynopsisPoster": aMidSynopsisPoster, "aYear": aYear, "aGenre": aGenre};
        sigUpdateSMCAudioPlayerdata(params);
    }

    function midDataReady (dmodel) {
        core.debug("midDataReady: Count: "+dmodel.count);
        var t = dmodel.at(0);
        midTitle = (t.title)?t.title:t.short_title;
        midArtist = (t.artist)?t.artist:"";
        midPoster = (t.poster)?t.poster:"";
        midSynopsisPoster = (t.synopsisposter)?t.synopsisposter:"";
        core.debug("midDataReady: midPoster: "+midPoster); core.debug("midDataReady: midSynopsisPoster: "+midSynopsisPoster);
        mid = t.mid;
        midDuration = pif.aod.getMediaEndTime();
        updateSMCmidDataReady()
    }
    function updateSMCmidDataReady(){
        var params={"functionName":"midDataReady", "midTitle":midTitle, "midArtist": midArtist, "midPoster": midPoster, "midSynopsisPoster": midSynopsisPoster, "mid": mid, "midDuration": midDuration};
        sigUpdateSMCAudioPlayerdata(params);
    }

    function usbMidDataReady(){
        aMidTitle = pif.usbMp3.mp3Info.album;
        midTitle = pif.usbMp3.mp3Info.title;
        midArtist = pif.usbMp3.mp3Info.artist;
        midPathFileName = pif.usbMp3.__mediaPathname;
        midDuration = pif.usbMp3.getMediaEndTime();
        midSynopsisPoster = "image://mp3image/"+pif.usbMp3.__mediaPathname;
        var arr=(pif.usbMp3.__mediaPathname.split('/')); midFileName = arr[arr.length-1];
        var params={"functionName":"usbMidDataReady", "aMidTitle":aMidTitle, "midTitle": midTitle, "midArtist": midArtist, "midPathFileName": midPathFileName, "midFileName": midFileName, "midSynopsisPoster": midSynopsisPoster, "midDuration": midDuration};
        sigUpdateSMCAudioPlayerdata(params);
    }

    Timer{id:metadataTimer; interval:200; onTriggered:usbMidDataReady();}
}
