import QtQuick 1.1
import Panasonic.Pif 1.0
import Panasonic.PopupManager 1.0

PopupContainer {
    id: externalWidgetContainer
    property variant popupRef
    property int extAppWidth: screenWidth;
    property int extAppHeight: screenHeight;
    property string assetImagePath: "/tmp/interactive/viewAssets/assets0/externalPopup/images/"
    property int languageID
    property string languageISO
    winVisible: true;

    function setPopupDimension(x,y,w,h){
        console.log("ExternalWidgetContainer | setPopupDimension | x:  "+x+" y: "+y+" width: "+w+" height: "+h);
        sendMessage("SYSTEM#SetDimension#"+x+"#"+y+"#"+w+"#"+h);
    }
    function closePopup(){
        sendMessage("SYSTEM#ClosePopup#");
    }
    function sendMsgToInt(msgString){
        console.log("ExternalWidgetContainer.qml | sendMsgToInt | msgString: "+msgString)
        sendMessage('POPUP#'+msgString);
    }
    function __hide(){
        externalWidgetContainer.winVisible = false;
        return;
    }
    function __show(tmpName) {
        console.log(" ExternalWidgetContainer | __show | file:"+tmpName+".qml")
        var  component = Qt.createComponent("/tmp/interactive/view/externalWidgets/"+tmpName+".qml");
        popupRef = component.createObject(externalWidgetContainer);
        popupRef.focus=true;
        focusTimer.start();
    }
    onWinFocusChanged: { if (winFocus) { focusTimer.stop(); } }
    onMessageReceived: {
        console.log("ExternalWidgetContainer | onMessageReceived | message: "+message);
        var mesgArr = message.split('#')
        if(mesgArr[0]=='SYSTEM' ){
            if(mesgArr[1]=='init'){
                if(popupRef) popupRef.destroy()
                languageID=mesgArr[3]
                languageISO=mesgArr[4]
                 __show(mesgArr[2])
                externalWidgetContainer.winVisible = true;
                externalWidgetContainer.raiseWindow();
                externalWidgetContainer.refreshScreenDimensions();
            }else if(mesgArr[1]=='hide'){
                __hide();
            }
        }else if(mesgArr[0]=='VIEW'){
            if(mesgArr.length>1){
                mesgArr.shift();
                var intMsg = mesgArr.join("#");
            }else{
                var intMsg = ""
            }
            if(popupRef) popupRef.init(intMsg);
        }
    }
    FontLoader{
        id: defaultFont1;
        Component.onCompleted: {
            source = "/tmp/interactive/fonts/DroidNaskhUI-Regular.ttf";
        }
    }

    FontLoader{
        id: defaultFont2;
        Component.onCompleted: {
            source = "/tmp/interactive/fonts/DroidSansFallbackFull.ttf";
        }
    }

    TranslationFileLoader { id: extPopupStyle; source: "/tmp/interactive/viewAssets/assets0/externalPopup/style.qm" }
    Timer {
        id: focusTimer; interval: 500;
        onRunningChanged: { if (running) { externalWidgetContainer.activateFocus(); } }
        onTriggered: { externalWidgetContainer.activateFocus(); }
    }
}
