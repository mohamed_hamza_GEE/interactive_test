import QtQuick 1.1
import Panasonic.Pif 1.0
import Panasonic.PopupManager 1.0

Item{
    id:externalWidgetPopup
    z:40
    visible:false
    property int popupWindowX: 0;
    property int popupWindowY: 0;
    property int popupWindowWidth: viewController.width;
    property int popupWindowHeight:  viewController.height;
    property string messageStr;
    property string fileNameStr;
    property int cPRIORITY_CRITICAL:2;
    property int cPRIORITY_HIGH:1;
    property int cPRIORITY_NORMAL:0;

    signal externalWidgetClosed();
    signal externalWidgetDisplayed();
    signal externalWidgetMsg(string popupMsg);

    Connections{
        target:pif
        onExternalAppActiveChanged:{
            core.info("ViewController.qml | onExternalAppActiveChanged");
            if(externalAppActive==false && externalWidgetPopup.visible){
                thisExternalWidget.sendMessage("SYSTEM#hide");
                thisExternalWidget.visible=false;
                externalWidgetPopup.visible=false;
            }
        }
    }

    function getExternalPopupRef() { return thisExternalWidget;}

    function showPopup(fileName,extPriority){
        core.info("ExternalWidgetPopup | showPopup | file:"+fileName);
        if(pif.getInteractiveOverride()) return;
        if(!pif.getOpenFlight()) return;
        if(fileName==undefined || fileName==""){core.info("ExternalWidgetPopup.qml | showPopup | fileName not provided, exiting"); return false;}
        fileNameStr=fileName;
        if(extPriority==undefined || extPriority=="")extPriority=cPRIORITY_NORMAL;
        extPriority=(extPriority==cPRIORITY_CRITICAL)?cPRIORITY_CRITICAL:(extPriority==cPRIORITY_HIGH)?cPRIORITY_HIGH:cPRIORITY_NORMAL
        __setExternalPopUpPriority(extPriority);
        extPopupTimer.restart();
    }

    function closeExtPopup(){
        core.info("ExternalWidgetPopup.qml | closeExtPopup ");
        __setExternalPopUpPriority(cPRIORITY_NORMAL);
        thisExternalWidget.sendMessage("SYSTEM#hide");
        thisExternalWidget.visible=false;
        externalWidgetPopup.visible=false;
        externalWidgetClosed();
    }

    function sendMsgToExtPopup(textMsg){
        core.info("ExternalWidgetPopup | sendMsgToExtPopup | textMsg: "+textMsg)
        if(!pif.getOpenFlight()) return;
        messageStr = textMsg;
        if(externalWidgetPopup.visible) thisExternalWidget.sendMessage('VIEW#'+textMsg);
    }

    function __setExternalPopUpPriority(priority){
        core.info("ExternalWidgetPopup | __setExternalPopUpPriority | priority "+priority);
        thisExternalWidget.__setPopUpPriority(priority);
    }

    function __setPopupDimension(x,y,w,h){
        core.info("ExternalWidgetPopup.qml | __setPopupDimension | x: "+x+", y: "+y+", w: "+w+",h :"+h);
        popupWindowX=x;
        popupWindowY=y
        popupWindowWidth=w;
        popupWindowHeight=h;
    }

    Popup{
        id: thisExternalWidget
        x: externalWidgetPopup.popupWindowX
        y: externalWidgetPopup.popupWindowY
        width: externalWidgetPopup.popupWindowWidth;
        height: externalWidgetPopup.popupWindowHeight;

        source: '/tmp/interactive/framework/components/ExternalPopupContainer.qml'
        visible: false;

        onMessageReceived:{
            console.log("ExternalWidgetPopup | onMessageReceived | message: "+message);
            var mesgArr = message.split('#')
            if(mesgArr[0]=='SYSTEM'){
                if(mesgArr[1]=='SetDimension'){
                    __setPopupDimension(mesgArr[2],mesgArr[3],mesgArr[4],mesgArr[5])
                }else if(mesgArr[1]=='ClosePopup'){
                    externalWidgetPopup.closeExtPopup();
                }
            }
            if(mesgArr[0]=='POPUP'){
                externalWidgetPopup.externalWidgetMsg(mesgArr[1]);
            }
        }

        Component.onCompleted:{
            thisExternalWidget.priority=Popup.Normal;
        }

        function __setPopUpPriority(priority){
            console.log("ExternalWidgetPopup.qml | __setPopUpPriority | priority :"+priority)
            if(priority==externalWidgetPopup.cPRIORITY_CRITICAL)
                thisExternalWidget.priority=Popup.Critical;
            else if(priority==externalWidgetPopup.cPRIORITY_HIGH)
                thisExternalWidget.priority=Popup.High;
            else
                thisExternalWidget.priority=Popup.Normal;
        }
    }

    Timer{
        id: extPopupTimer; interval: 2;
        onTriggered: {
            externalWidgetPopup.visible=true;
            thisExternalWidget.visible=true;
            thisExternalWidget.sendMessage('SYSTEM#init#'+fileNameStr+'#'+core.settings.languageID+'#'+core.settings.languageISO);
            if(messageStr!=""){thisExternalWidget.sendMessage('VIEW#'+messageStr);messageStr=""}
            externalWidgetDisplayed();
        }
    }
}
