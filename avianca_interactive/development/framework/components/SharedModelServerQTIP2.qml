import QtQuick 1.1
import Panasonic.Pif 1.0

SharedModel{
    id:sm
    isServer: true;

    property string apiName;
    property string languageIso;
    property int seatbackVolume;
    property int seatbackPaVolume;
    property int headsetVolume;
    //property int seatbackBrightness;
   // property int handsetBrightness;
   // property bool backlightEnabled:true;
    //property int mid;
    //property int aggregateMid;
    //property int soundtrackLid;
    //property int soundtrackType;
    //property int subtitleLid;
    //property int subtitleType;
    //property int elapsedTime;
    //property string aspectRatio; //possible values: "4x3","16x9","Fullscreen","Not Adjustable"
    //property string mediaRepeat; //possible values: "none","current","all"
    //property bool mediaShuffle:false;
    //property string mediaType;
    //property string mediaPlayRate;
    //property string mediaPlayerState; // possible values: "play", "pause", "resume", "stop", "fastforward", "rewind"
    property variant launchAppId:[] //variant type should have default initialisation as []
    property bool iPodConnected:false;
    property bool usbConnected:false;
    //property bool showKeyboard;
    //property string keyboardData;
    property bool usbAgreement;
    property bool iPodAgreement;
    //property variant playlists:{"aod":{},"vod":{},"kidsaod":{},"kidsvod":{}};
    property variant apiParameters:[];
    //property bool muteVolume:false;
    property string blueRayStatus: "stop";
    property string blueRayMediumType: "NONE";  // possible medium type can be "BD", "DVD", "CD" and "NONE".
    //property variant resumeTimeMap: [];
	property int noOfDimmableWindows:0;

    //QTIP 2.0
    property int seatbackBrightness;
    property int handsetBrightness;
    //property int clientBacklightEnabled;
    property bool serverBacklightEnabled:true;
    property bool serverKeyboardVisible:false;
    property int languageLid;
    property bool allowIncomingCalls;
    property int telephonyVolume;
    property string interactiveMode;
    property bool clientKeyboardVisible;
    property string keyboardData;
    property int volume;
    property bool muteEnabled;
    property variant resumeTimeMap: [];
    property variant midInforArr: [];
    property variant playlists:{"aod":{},"vod":{},"kidsaod":{},"kidsvod":{},"usb":{}};

    //Media Player 1 for AOD
    property string mediaPlayer1MediaType;
    property int mediaPlayer1MediaIdentifier;
    property int mediaPlayer1MediaAggregateIdentifier;
    property int mediaPlayer1ElapsedTime;
    property string mediaPlayer1PlayStatus;
    property int mediaPlayer1SubtitleLid;
    property int mediaPlayer1SoundtrackLid;
    property string mediaPlayer1AspectRatio;
    property variant mediaPlayer1AvailableAspectRatios;
    property bool mediaPlayer1Shuffle:false;
    property string mediaPlayer1Repeat;
    property int mediaPlayer1PlayRate;
    property bool mediaPlayer1SoundtrackEnabled;
    property int mediaPlayer1MediaAlbumId;  //contains albumid/playlist ID. This property won't be available in non-woi applications like PAC and ICS.

    //Media Player 2 for VOD
    property string mediaPlayer2MediaType;
    property int mediaPlayer2MediaIdentifier;
    property int mediaPlayer2MediaAggregateIdentifier;
    property int mediaPlayer2ElapsedTime;
    property string mediaPlayer2PlayStatus;
    property int mediaPlayer2SubtitleLid;
    property int mediaPlayer2SubtitleType;  //This property won't be available in non-woi applications like PAC and ICS.
    property int mediaPlayer2SoundtrackLid;
    property string mediaPlayer2AspectRatio;
    property variant mediaPlayer2AvailableAspectRatios;
    property bool mediaPlayer2Shuffle:false;
    property string mediaPlayer2Repeat;
    property int mediaPlayer2PlayRate;
    property bool mediaPlayer2SoundtrackEnabled;
    property bool mediaPlayer2IsTrailer:false;
    property int mediaPlayer2MediaAlbumId;  //contains albumid/playlist ID. This property won't be available in non-woi applications like PAC and ICS.

    //SeatChat
    property variant eventInfoData;
    property variant seatToVkarmaSeatChatRequest; // {"apiName":<apiName>,"params":<params>,"callBackFunction":<callBackFunction>,"returnObj":<returnObj>}
    property variant seatToVkarmaSeatChatWithParams; //{"apiName":<apiName>,"params":<params>,"callBackFunction":<callBackFunction>,"returnObj":<returnObj>,"extraParams":<extraParams>}

    // Media Player 3 for Games
    property string mediaPlayer3MediaType;
    property int mediaPlayer3MediaIdentifier;
    property string mediaPlayer3PlayStatus;



    signal sigUsbSharingDataReceived(variant client,variant msgObj);
    signal sigMessageSharingDataReceived(variant client,variant msgObj);
    signal sigUsbSharingClientConnected();
    signal sigUsbSharingClientDisConnected();
    signal sigSMClientConnected();
    signal sigSMClientDisConnected();
    signal sigSMDataReceived(string api,variant apiParameters);
    signal sigSMSynchronized();
    signal sigGcpClientConnected(variant client);
    signal sigGcpDataReceived(variant client,variant msgObj);

    onApiNameChanged: {if(!privateObj.ignorePropertyChange && apiName!="") dataReceived(apiName);}
    onSeatbackVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("seatbackVolume");}
    onSeatbackPaVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("seatbackPaVolume");}
    onHeadsetVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("headsetVolume");}
    onSeatbackBrightnessChanged: {if(!privateObj.ignorePropertyChange) dataReceived("seatbackBrightness");}
    //onAspectRatioChanged: { if(!privateObj.ignorePropertyChange) dataReceived("aspectRatio");}
    //onMediaShuffleChanged: { if(!privateObj.ignorePropertyChange) dataReceived("mediaShuffle");}
    onMediaPlayer1ShuffleChanged:  { if(!privateObj.ignorePropertyChange) sigSMDataReceived("mediaShuffle",{mediaShuffle:mediaPlayer1Shuffle})}
    //onBacklightEnabledChanged: { if(!privateObj.ignorePropertyChange) dataReceived("backlightEnabled");}
    onServerBacklightEnabledChanged:  { if(!privateObj.ignorePropertyChange) dataReceived("backlightEnabled");}
    onIPodConnectedChanged: { if(!privateObj.ignorePropertyChange) dataReceived("iPodConnected");}
    onUsbConnectedChanged: { if(!privateObj.ignorePropertyChange) dataReceived("usbConnected");}
    //onShowKeyboardChanged: { if(!privateObj.ignorePropertyChange) dataReceived("showKeyboard");}
    onServerKeyboardVisibleChanged: { if(!privateObj.ignorePropertyChange) dataReceived("showKeyboard");}
    onKeyboardDataChanged: { if(!privateObj.ignorePropertyChange) dataReceived("keyboardData");}
    onUsbAgreementChanged: { if(!privateObj.ignorePropertyChange) dataReceived("usbAgreement");}
    onIPodAgreementChanged: { if(!privateObj.ignorePropertyChange) dataReceived("iPodAgreement");}
    onMuteEnabledChanged: {if(!privateObj.ignorePropertyChange) dataReceived("muteVolume");}
    onLanguageLidChanged: { if(!privateObj.ignorePropertyChange){
            core.debug("SharedModelServer.qml | onLanguageLidChanged: "+languageLid);
            languageIso=dataController.getLanguageIsoByLid(String(languageLid))[0];
            sigSMDataReceived("changeLanguage",{languageIso:languageIso,languageLid:languageLid});
        }
    }

    onMediaPlayer1RepeatChanged: {if(!privateObj.ignorePropertyChange)sigSMDataReceived("mediaRepeat",{mediaRepeat:mediaPlayer1Repeat});}
    onMediaPlayer2AspectRatioChanged:{if(!privateObj.ignorePropertyChange) dataReceived("aspectRatio");}


    //SeatChat
    onEventInfoDataChanged: {if(eventInfoData!="")core.info("SharedModelServerQTIP2.qml | onEventInfoDataChanged | event :"+eventInfoData.event)}
    onSeatToVkarmaSeatChatRequestChanged: {
        core.info("SharedModelServerQTIP2.qml | onSeatToVkarmaSeatChatRequestChanged | apiName:"+seatToVkarmaSeatChatRequest.apiName);
        if (seatToVkarmaSeatChatRequest!="")dataReceived("seatToVkarmaSeatChatRequest");
    }

    onSeatToVkarmaSeatChatWithParamsChanged: {
        core.info("SharedModelServerQTIP2.qml | onSeatToVkarmaSeatChatWithParamsChanged | apiName:"+seatToVkarmaSeatChatWithParams.apiName);
        if(seatToVkarmaSeatChatWithParams!="")dataReceived("seatToVkarmaSeatChatWithParams");
    }


    onCommandReceived: {
        core.log("SharedModelServer.qml | onCommandReceived  >> "+command+" | args: "+JSON.stringify(args));
        sigSMDataReceived(command,args);
    }

    onNetworkProtocolMessageReceived:{
        core.info("SharedModelServer.qml | received message is " + message);
        var msg = eval('(' + message + ')');
        if(msg.messageType=="init"){
            client.properties = {"name":msg.name,"reason":msg.reason};
            core.info("client.properties.reason  is  " + client.properties.reason);
            client.send({messageType: "initAck"});
            if(msg.reason=="USBSharing")sigUsbSharingClientConnected();
        }
        else if(msg.reason=="USBSharing"){
            core.info("SharedmodelServer.qml | received message from USBSharing client. message is " + message);
            sigUsbSharingDataReceived(client,msg);
        }else if(msg.reason=="MessageSharing"){
            core.info("SharedmodelServer.qml | received message from MessageSharing client. message is " + message);
            sigMessageSharingDataReceived(client,msg);
        }
        else if (msg.reason==undefined) {
            if (client.properties.reason==undefined){
                client.properties = {"name":"","reason":pif.getDeviceConnected()};
                core.info("client.properties.reason  is  " + client.properties.reason);
                if(client.properties.reason=="GCP") sigGcpClientConnected(client);
            }
            if(client.properties.reason=="GCP") sigGcpDataReceived(client,msg);
        }
    }

    onRemoteClientDisconnected:{
        sendPropertyData("eventInfoData","");
        sendPropertyData("seatToVkarmaSeatChatRequest","");
        sendPropertyData("seatToVkarmaSeatChatWithParams","");
        if(client.properties && client.properties.reason=="USBSharing")sigUsbSharingClientDisConnected();
        else sigSMClientDisConnected();
    }

    onSynchronized:{
        sigSMClientConnected();
        sigSMSynchronized();
    }

    function sendMessage(message,reason){
        core.debug(" SharedModelServer.qml | sendMessage(). message is  " + JSON.stringify(message) + " reason is  " + reason+" sm.remoteClients.length: "+sm.remoteClients.length);
        for (var i = 0; i < sm.remoteClients.length; i++){
            core.debug( " sm.remoteClients[i].properties.reason is  " + sm.remoteClients[i].properties.reason);
            if(sm.remoteClients[i].properties.reason==reason){
                core.info("SharedmodelServer.qml | sending message to client. message is " + JSON.stringify(message));
                sm.remoteClients[i].send(message);
            }
        }
    }

    function dataReceived(apiName){ core.debug("SharedModelServer.qml | dataReceived(api) called: "+apiName);sigSMDataReceived(apiName,apiParameters);}

    function sendApiData(api,params){       // Sending bunch of property change to the shared model
        core.verbose("SharedModelServer.qml | Control in sendApiData("+api+",params)")
        if(api=="changeLanguage"){sendPropertyData("languageLid",dataController.getLidByLanguageISO(params.languageIso));return true;}
        privateObj.ignorePropertyChange = true;
        sm.apiName = "";
        if(api.indexOf("userDefined")!=-1 || api.indexOf("framework")!=-1) {sm.apiParameters=params;}
        else {
            var smproperties = privateObj.properties;
            for (var i in smproperties){
                if(params[smproperties[i]] || params[smproperties[i]]==0){      // Added to check with 0 as integer may have this value, example when "None" is selected for subtitle, it has subtitleLid=0
                    core.verbose("SharedModelServer.qml | Changing shared model property: " + smproperties[i] + " to: " + params[smproperties[i]]);
                    sm.setPropertyValue(smproperties[i], params[smproperties[i]]);
                }else if(typeof(params[smproperties[i]])=="boolean" && !params[smproperties[i]]){       // for boolean datatype with false value
                    core.verbose("SharedModelServer.qml | Changing shared model property: " + smproperties[i] + " to: " + params[smproperties[i]]);
                    sm.setPropertyValue(smproperties[i], params[smproperties[i]]);
                }
            }
        }
        sm.apiName = api;
        privateObj.ignorePropertyChange = false;
    }

    function sendPropertyData(propertyName,propertyValue){      // Sending single property change to the shared model
        privateObj.ignorePropertyChange=true;
        core.verbose("SharedModelServer.qml | sendPropertyData("+propertyName+","+propertyValue+")");
        sm.setPropertyValue(propertyName, propertyValue);
        privateObj.ignorePropertyChange=false;
    }

    Component.onCompleted: { privateObj.properties = sm.userProperties(); core.info("SharedModelServer.qml | SHARED MODEL SERVER LOAD COMPLETE")}

    QtObject{id:privateObj; property bool ignorePropertyChange: false; property variant properties:[];}
}
