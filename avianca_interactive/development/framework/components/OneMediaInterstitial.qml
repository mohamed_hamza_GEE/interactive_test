import QtQuick 1.1
import Panasonic.OneMedia 1.0

Item {
    property string appZonePath;
    property int addWidth;
    property int addHeight;
    property string defaultImagePath: "oneMediaInterstitial/Interstitial.jpg";
    signal adInfoReceived(string fileType, string fileName, int interstitialDuration);
    signal adDestinationReceived(string destinationType, string destinationName);
    signal adInfoNotReceived();

    function getAdInfo(){
        core.log("OneMediaInterstitial | getImage");
        adProvider.appZone=appZonePath;
        adProvider.languageId=core.settings.languageID;
        if(!core.pc) adProvider.beginGetAdInfo();
        else adInfoReceived("image/jpeg", defaultImagePath, 2);
    }
    function getAdDestination(){
        core.log("OneMediaInterstitial | getAdDestination | destinationType: "+adProvider.destinationType);
        if(adProvider.destinationType==0){ adDestinationReceived(0,""); return; }
        adProvider.beginGetAdDestination();
    }
    AdProvider {
        id: adProvider; type: "Interstitial"; dimension.width: addWidth; dimension.height: addHeight;
        onGetAdInfoCallEnded: {
            core.log("OneMediaInterstitial | onGetAdInfoCallEnded | status: " + status);
            if(status!=0) {adInfoNotReceived(); return;}
            core.log("OneMediaInterstitial | onGetAdInfoCallEnded | fileType: " + adProvider.fileType+" fileName: "+adProvider.fileName+" interstitialDuration: "+adProvider.interstitialDuration+" destinationType: "+adProvider.destinationType);
            adInfoReceived(adProvider.fileType, adProvider.fileName, adProvider.interstitialDuration);
        }
        onGetAdDestinationCallEnded: {
            core.log("OneMediaInterstitial | onGetAdDestinationCallEnded | status: " + status);
            if(status!=0) return;
            core.log("OneMediaInterstitial | onGetAdDestinationCallEnded | destinationType : " + adProvider.destinationType+" destinationName: "+adProvider.destinationName);
            adDestinationReceived(adProvider.destinationType, adProvider.destinationName);
        }
        onErrorMessage: { core.log("OneMediaInterstitial | onErrorMessage | messageText : "+messageText); }
    }
}
