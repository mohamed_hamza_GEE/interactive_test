import QtQuick 1.1

Item {
    id: videoMetaData
    property int aMid:0;
    property string aMidTitle:'';
    property int mid:0;
    property string midTitle:'';
    property bool isTrailer:false;
    property int subtitleLid:0;
    property int soundtrackLid:0;
    property string duration:"";
    property string aspectRatio:"";
    property bool midDataFetched: false;

    signal sigUpdateSMCVideoPlayerdata(variant params);

    Connections {
        target: core;
        onSigLanguageChanged:{
            if(mid)core.dataController.getVodPlayerData([mid],midDataReady);
            return true;
        }
    }

    Connections {
        target: pif.lastSelectedVideo;

        onSigMidPlayBegan: {
            core.debug("VideoPlayerData.qml | onSigMidPlayBegan cmid: "+cmid+" | Type: "+pif.getLSVmediaSource());
            if(pif.getLSVmediaSource()=="vod"){
                if (amid!=aMid && amid>0){ core.dataController.getVodPlayerData([amid],aMidDataReady); aMid=amid; mid = cmid; midDataFetched=true;} // Delay fetching of mid data
                if (cmid!=mid){if(!midDataFetched) core.dataController.getVodPlayerData([cmid],midDataReady);}
                isTrailer = pif.vod.getIsTrailer();
                subtitleLid = pif.vod.getSubtitleLid();
                soundtrackLid = pif.vod.getSoundtrackLid();
                duration = pif.vod.getDuration();
                aspectRatio = pif.vod.getAspectRatio();
            }
        }

        onSigMidPlayStopped: {
            aMidTitle=''; aMid=0;
            midTitle=''; mid=0;
        }
    }
    function aMidDataReady (dmodel) {
        core.debug("VideoPlayerData.qml | aMidDataReady: Count: "+dmodel.count);
        if(dmodel.count){
            var t = dmodel.at(0);
            aMidTitle = (t.title)?t.title:t.short_title;
            aMid = t.mid;
        }
        if (midDataFetched){ if(pif.getLSAmediaSource()!="broadcastVideo") core.dataController.getVodPlayerData([mid],midDataReady); midDataFetched=false;}
    }
    function midDataReady (dmodel) {
        core.debug("VideoPlayerData.qml | midDataReady: Count: "+dmodel.count);
        var t = dmodel.at(0);
        midTitle = (t.title)?t.title:t.short_title;
        mid = t.mid;
        var params={"functionName":"midDataReady", "midTitle": midTitle, "mid": mid,"duration":duration,"isTrailer":isTrailer, "subtitleLid":subtitleLid,"soundtrackLid":soundtrackLid,"aspectRatio":aspectRatio,"aMid":aMid,"aMidTitle":aMidTitle};
        sigUpdateSMCVideoPlayerdata(params);
    }

}
