import QtQuick 1.1
import Panasonic.Pif 1.0

Item {
    id: remoteDataLoader
    property int cErrorNoError: 0;
    // Remote Request
    property variant __remoteCallBack;
    property int __makeUrlUnique:0;
    property string __url:'';
    property string __urlParams:'';
    property string __requestType:'get';
    property int __retryTimeout:core.pc?1:4; //secs
    property int __requestRetries:0;
    property int __requestId:0;
    property bool isAppActive: false;

    //Signal
    signal sigDataReady(int errCode, string url, string responseString, variant responseObj,int requestId);

    function getData(url,retries,urlParams,requestType,randomTries,requestId,newUrlFormat) {      // string,string,string,[int],[bool],[function]
        if (!url) return false;
        __url=((url.indexOf('?')==-1 && !newUrlFormat)?url+"?":url);
        webRequest.url=__url;
        __makeUrlUnique=1;
        var arr=__formUrlParams(urlParams,newUrlFormat);
        __urlParams=arr[1];
        __requestId=requestId?requestId:0;
        if (requestType=='post') {webRequest.post(arr[0]);__requestType=requestType;} else {webRequest.get(arr[0]);__requestType='get';}
        core.info("remoteDataLoader | getData | url: "+__url+arr[0]);
        __requestRetries=String(parseInt(retries,10))=="NaN"?0:retries==-1?200:retries;
        retryTimer.retries=__requestRetries;
        return true;
    }

    function requestLastUrl(withDelay){     // [bool]
        retryTimer.retries=__requestRetries;
        if (!withDelay) return __requestDataAgain(); else return retryTimer.restart();
    }

    function terminateRequest() {
        webRequest.url='';
        if (__requestType=="post") webRequest.post(''); else webRequest.get('');
        retryTimer.stop(); retryTimer.retries=0;
        return true;
    }

    function __formUrlParams(urlParams,newUrlFormat) {
        var params = [];
        if (urlParams) params.push(urlParams); else urlParams='';
        if (!newUrlFormat)params.push('loadid='+__makeUrlUnique++);
        return [params.join('&'),urlParams];
    }

    function __requestDataAgain() {
        var arr=__formUrlParams(__urlParams);
        webRequest.url=__url;
        core.info("remoteDataLoader | __requestDataAgain | url: "+__url+arr[0]);
        if (__requestType=="post") webRequest.post(arr[0]); else webRequest.get(arr[0]);
        return true;
    }

    WebRequest {
        id: webRequest;
        onError:{
            if(pif.getFallbackMode()){
                 core.info("remoteDataLoader | webRequest OnError | Errorcode: "+code+" | No retries Entered fallback mode ");
                //terminateRequest();
                retryTimer.stop(); retryTimer.retries=0;
                sigDataReady(code,__url,'','',__requestId); //retryTimer.errorOccurred=false;
            }else{
                core.info("remoteDataLoader | webRequest OnError | Errorcode: "+code+" | No of retries left: "+retryTimer.retries);
                retryTimer.errorOccurred=true;
                if (__url=='') return;
                if(code==203){retryTimer.retries=0;retryTimer.errorOccurred=false;sigDataReady(code,__url,'','',__requestId);}
                if (--retryTimer.retries>0) retryTimer.restart(); else {sigDataReady(code,__url,'','',__requestId); retryTimer.errorOccurred=false;}
            }
        }
        onResponse:{
            if (requestUrl==''||retryTimer.errorOccurred) {retryTimer.errorOccurred=false; return false;}
            core.info("remoteDataLoader | webRequest | onResponse data received.");
            retryTimer.stop(); retryTimer.retries=0;
            sigDataReady(cErrorNoError,requestUrl,data,value,__requestId);
        }
    }
    Timer { id:retryTimer; interval: __retryTimeout*1000; property int retries:0; property bool errorOccurred:false;  onTriggered: __requestDataAgain();}
}
