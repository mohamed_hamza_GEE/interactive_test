import QtQuick 1.1
import Panasonic.Pif 1.0
import "../../framework/components"
import "../components"
Item{
    id:thisUSBInterface;
    property string currentFilters: pif.usb.getFilterArray("usb_images");
    property bool isRootFolder: true;
    property string usbRootPath: pif.usb.getMountPath()[0];
    property alias slideShow: slideShow;

    signal sigModelUpdated(variant dModel);

    property string currentFolderPath: pif.usb.getFolderPath();
    property int startIndex: 0;
    property int indexOfSlideShow: 0;
    function getSlideShowModel(){return slideShow.slideShowModel;}

    //configuration
    property string slideShowType: "autoStart"; //autoStart/playPause/loadSlideShow
    property string backButtonFileType: "backbutton";
    property bool isBackButtonRequired:false;
    property bool isMp3DurationRequired:true;

    SimpleModel{ id:usbModel;}

//    onCurrentFolderPathChanged: {
//                populateModel();
//      //  populateModelTimer.restart();
//    }

    Connections{
        target: pif.usb
        onSigUsbModelReady:{
              populateModel();
        }
        onSigUsbConnected:{
            core.log("CompUSBInterface.qml | rootPath : '"+rootPath+"'");
            usbRootPath=rootPath;
        }
    }

    function setFilter(template_id){
        core.log("CompUSBInterface.qml | setFilter("+template_id+")");
        if(template_id.indexOf("usb") != -1)currentFilters = pif.usb.getFilterArray(template_id);
    }

    function processFile(filePath,filetype,index){
        if(index == undefined) index=0;
        core.log("CompUSBInterface.qml | processFile("+filePath+","+filetype+")");
        if(filetype == backButtonFileType)  pif.usb.readPrevFolder(currentFilters);
        else if(pif.usb.isFolderInUsb(filePath)) readDirectory(filePath);
        else if( pif.usb.isMusic(filetype)) playPauseUSBMp3(index,filePath);
        else if( pif.usb.isImage(filetype)) startSlideShow(filePath,index);
        else if( pif.usb.isDocument(filetype))launchPDF(filePath);
    }

    function readDirectory(path,filter){
        usbRootPath = usbRootPath ? usbRootPath: pif.usb.getMountPath()[0];
        if(path == undefined){path=usbRootPath;}
        if(filter==undefined) filter=currentFilters; else currentFilters=filter;
        core.log("CompUSBInterface.qml | readDirectory | path : '" + path + "' | filter : '" + filter + "' | pif.usb.getFolderPath() : '" + (usbRootPath + pif.usb.getFolderPath()) + "'")
        pif.usb.readFolder(path,filter);
        //if(path==currentFolderPath){populateModel()}
      //  populateModelTimer.restart();
     //  populateModel()
    }

    function populateModel(){
        if(fetchDurationTimer.running){fetchedDurationIndex=-1;fetchDurationTimer.stop();}
        isRootFolder = (currentFolderPath == "" || currentFolderPath == "/");
        if(isMp3DurationRequired || isBackButtonRequired){
            usbModel.clear();
            if(!isRootFolder && isBackButtonRequired){ usbModel.append({ "pathfilename":"", "filenameonly":"Previous Folder", "filetype":backButtonFileType, "filesize":"", "lastmodified":"", "lastmodifiedstring":"","mp3artist":"","duration":""}); }
            for(var i=0;i<pif.usb.browserModel.count;i++){
                var ptr = pif.usb.browserModel.at(i);
                var mp3artist = ptr.filetype == "mp3"?ptr.mp3artist:"";
                usbModel.append({"pathfilename":ptr.pathfilename,"filenameonly":ptr.filenameonly,"filetype":ptr.filetype,"filesize":ptr.filesize,"lastmodified":ptr.lastmodified,"lastmodifiedstring":ptr.lastmodifiedstring,"mp3artist":mp3artist,"duration":""});
            }
            fetchDurationTimer.restart();
            sigModelUpdated(usbModel);
        }else {
            sigModelUpdated(pif.usb.browserModel);
        }
    }

    function convertSizeToMB(fsize,returnAbsValue,appendText){return(coreHelper.convertSizeToMB(fsize,returnAbsValue)+ appendText?(" " +appendText):"")}

    function isPlayingMP3(filePath){ return (pif.usbMp3.getMediaState() != pif.usbMp3.cMEDIA_STOP && pif.getLSAmediaSource()=="usb" && pif.usbMp3.mp3Info.source == filePath); }

    function playPauseUSBMp3(index,filePath){
        if(pif.usbMp3.getMediaState() != pif.usbMp3.cMEDIA_STOP && isPlayingMP3(filePath)){
            if(pif.usbMp3.getMediaState() == pif.usbMp3.cMEDIA_PLAY)
                pif.usbMp3.pause();
            else if(pif.usbMp3.getMediaState() == pif.usbMp3.cMEDIA_PAUSE)
                pif.usbMp3.resume();
        }else{
            var usbParams = new Object(); usbParams["playIndex"]=filePath;usbParams["repeatType"]=1;
            pif.usbMp3.playByModel((usbRootPath+pif.usb.getFolderPath()),usbParams, pif.usb.getFilterArray("usb_music"));
        }
    }
    function launchPDF(filePath){
        if(pif.android)pif.launchApp.launchPdf(core.coreHelper.escapeSpaceInString(filePath),pif.launchApp.cINTENT_ACTION_VIEW,true)
        else pif.launchApp.launchApp("",pif.launchApp.cPDF_VIEWER,"USBPDF",core.coreHelper.escapeSpaceInString(filePath),true);
        if(core.pc)pif.setExternalAppActive(true);
    }


    MP3Info { id: mp3info;}
    property int fetchedDurationIndex: -1
    Timer{ id:fetchDurationTimer; interval: 200; onRunningChanged:{if(running)fetchedDurationIndex=0;} onTriggered:{ fetDuration();} }
    Connections {
        target: mp3info;
        onDurationCompleted: {
            core.log("CompUSBInterface.qml | onDurationCompleted | duration " + duration + " | fetchedDurationIndex : " + fetchedDurationIndex + " | " + source);
            if(fetchedDurationIndex<0 || usbModel.getValue(fetchedDurationIndex,"pathfilename") != source)return;
            usbModel.setProperty(thisUSBInterface.fetchedDurationIndex,"duration",mp3info.formatTime(duration));
            thisUSBInterface.fetchedDurationIndex++;
            fetDuration();
        }
    }
    function fetDuration(){
        core.log("CompUSBInterface.qml | fetDuration() | fetchedDurationIndex " + thisUSBInterface.fetchedDurationIndex);
        for(var i=thisUSBInterface.fetchedDurationIndex;i<usbModel.count;i++){
            if(pif.usb.isMusic(usbModel.getValue(i,"filetype"))){
                thisUSBInterface.fetchedDurationIndex=i; mp3info.requestDuration(usbModel.getValue(i,"pathfilename")); return;
            }
        }
    }

    function startSlideShow(path,index){
        if(index==undefined) index=0; if(path==undefined || path=="") path=usbRootPath;
        path = (usbRootPath + pif.usb.getFolderPath());
        core.log("CompUSBInterface.qml | startSlideShow() " + path)
        slideShow.populateSlideShowModel(path);
        startIndex=index;
        //if(slideShowType!="loadSlideShow"){playSlideShowTimer.restart();}else {loadSlideShowTimer.restart();}
    }

    function playSlideShow(index,interval,repeat,triggerOnStart){
        indexOfSlideShow=index?index:startIndex;
        interval=interval?interval:5;
        repeat=repeat?repeat:false;
        triggerOnStart=triggerOnStart?triggerOnStart:false
        slideShow.initiateSequencer(interval,repeat,triggerOnStart);
        slideShow.playSequencer(indexOfSlideShow);
    }

//    Timer{
//        id:populateModelTimer;
//        interval:500;
//        onTriggered: populateModel();
//    }

    Connections{
        target:slideShow
        onSigModelReady:{
            slideShow.initiateSequencer(5,false,false);
            indexOfSlideShow=0;
            for(var i=0;i<slideShow.slideShowModel.count;i++){
                if(usbModel.getValue(startIndex,"pathfilename") == slideShow.slideShowModel.getValue(i,"pathfilename")){ indexOfSlideShow=i; break; }
            }
            if(slideShowType!="loadSlideShow"){
                slideShow.playSequencer(indexOfSlideShow);
                if(slideShowType=="playPause"){slideShow.pauseSequencer();}
            }
        }
    }

//    Timer{id:loadSlideShowTimer; interval:800; onTriggered: {
//            slideShow.initiateSequencer(5,false,false);
//            indexOfSlideShow=0;
//            for(var i=0;i<slideShow.slideShowModel.count;i++){
//                if(usbModel.getValue(startIndex,"pathfilename") == slideShow.slideShowModel.getValue(i,"pathfilename")){ indexOfSlideShow=i; break; }
//            }
//        }
//    }

//    Timer{id:playSlideShowTimer; interval:800; onTriggered: {
//            slideShow.initiateSequencer(5,false,false);
//            indexOfSlideShow=0;
//            for(var i=0;i<slideShow.slideShowModel.count;i++){
//                if(usbModel.getValue(startIndex,"pathfilename") == slideShow.slideShowModel.getValue(i,"pathfilename")){ indexOfSlideShow=i; break; }
//            }
//            slideShow.playSequencer(indexOfSlideShow);
//            if(slideShowType=="playPause"){slideShow.pauseSequencer();}
//        }
//    }
    
    Sequencer{
        id:slideShow;
        function onTimerTriggered(){
            if(pif.getBacklightState() == 0 || viewController.getActiveSystemPopupID() > 0 || viewController.getActiveScreenPopupID() > 0 )return;
            return setNextIndex();
        }
    }
}
