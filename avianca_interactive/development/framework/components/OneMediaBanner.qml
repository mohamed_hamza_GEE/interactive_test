import QtQuick 1.1
import Panasonic.OneMedia 1.0

Item {
    property string appZonePath;
    property int addWidth;
    property int addHeight;
    property string defaultImagePath1: "oneMediaBanner/Banner1.jpg";
    property string defaultImagePath2: "oneMediaBanner/Banner2.jpg";
    property string defaultImagePath3: "oneMediaBanner/Banner3.jpg";
    signal destinationInfo(string destinationType, string destinationName);
    signal bannerImageLoaded(string fileType, string fileName);
    signal bannerImageNotLoaded();
    signal bannerInfoReceived(string fileType, string fileName);
    signal bannerInfoError();
    property int __count:-1
    property variant __oneMediaDataPc: [
        {"image":defaultImagePath1,"destinationType":"2","destinationName":"1000"},
        {"image":defaultImagePath2,"destinationType":"1","destinationName":""},
        {"image":defaultImagePath3,"destinationType":"0","destinationName":""}
    ];

    height: addHeight; width: addWidth;
    function showImage(){
        core.log("OneMediaBanner | showImage");
        adProvider.appZone=appZonePath;
        adProvider.languageId=core.settings.languageID;
        clearImage();
        if(!core.pc && !core.simulation) adProvider.beginGetAdInfo();
        else {
            if(__count>=__oneMediaDataPc.length-1){__count=-1;__count++}
            else { __count++;}
            omImg.source="";
            omImg.source=__oneMediaDataPc[__count].image;
            bannerInfoReceived("image/jpeg", omImg.source);
        }
    }

    function clearImage(){
        omImg.source="";
        omAnimImg.source="";
    }
    function getDestinationType(){
        core.log("OneMediaBanner | getDestinationType | destinationType: "+adProvider.destinationType);
        if(!core.pc && !core.simulation){
            if(adProvider.destinationType==0){ destinationInfo(0,""); return; }
            adProvider.beginGetAdDestination();
        } else{
            destinationInfo(__oneMediaDataPc[__count].destinationType,__oneMediaDataPc[__count].destinationName)
        }
    }
    AdProvider {
        id: adProvider; type: "Banner"; dimension.width: addWidth; dimension.height: addHeight;
        onGetAdInfoCallEnded:{
            core.log("OneMediaBanner | onGetAdInfoCallEnded | fileType: " + adProvider.fileType+" fileName: "+adProvider.fileName+" status : "+status+" destinationType: "+adProvider.destinationType);
            if(status==0){
                if(adProvider.fileType=="image/gif"){
                    omAnimImg.source=""; omAnimImg.source=adProvider.fileName;
                }else{
                    omImg.source=""; omImg.source=adProvider.fileName;
                }
                bannerInfoReceived(adProvider.fileType, omImg.source);
            }else if(status==-1){	//error
                omImg.source="";
                omAnimImg.source="";
                bannerInfoError();
                bannerImageNotLoaded();
            }
        }
        onGetAdDestinationCallEnded:{
            core.log("OneMediaBanner | onGetAdDestinationCallEnded | status: " + status);
            if(status==0) destinationInfo(adProvider.destinationType, adProvider.destinationName);
        }
        onErrorMessage:{
            core.log("OneMediaBanner | onErrorMessage | messageText: " + messageText);
        }
    }
    Image {
        id: omImg; asynchronous: true; width: adProvider.dimension.width; height: adProvider.dimension.height;
        onStatusChanged:{
            core.log("OneMediaBanner |  Image status of Zone Path : "+appZonePath+" : "+status);
            if(status == Image.Ready && source!=""){
                bannerImageLoaded(adProvider.fileType, omImg.source);
            }
            else if(status==Image.Error){
                bannerImageNotLoaded();
            }
        }
    }
    AnimatedImage{
        id: omAnimImg; asynchronous: true; width: adProvider.dimension.width; height: adProvider.dimension.height
        onStatusChanged:{
            core.log("OneMediaBanner |  Image status of Zone Path : "+appZonePath+" : "+status);
            if(status == Image.Ready && source!=""){
                bannerImageLoaded(adProvider.fileType, omAnimImg.source);
            }
            else if(status==Image.Error){
                bannerImageNotLoaded();
            }
        }
    }
    MouseArea { anchors.fill: parent; onReleased: getDestinationType(); }
}
