import QtQuick 1.1
import Panasonic.Pif 1.0

NetworkClient {
        id: client
        online: false
        property bool isConnected: false

        //audio
        property int mediaPlayer1ElapsedTime: 0; //Will be updated when "audioElapseTime" changes
        property string mediaPlayer1PlayStatus: ""; // Will be updated when "audioPlayerState" changes
        property bool isAlbumMode: false; // Updated when "isAlbumMode" changes
        property int audioTimeTotal: 0;
        property bool mediaPlayer1Shuffle: false; // Updated when "isShuffle" changes
        property string audioCurrentTitle: "" ;// Updated when "audioCurrentTitle" chnages, may be a part of AudioPlayerdataSMClient
        property string audioCurrentArtist: "" // Updated when "audioCurrentArtist" chnages, may be a part of AudioPlayerdataSMClient
        property int currentDuration: 0; // Updated when "audioCurrentDurationSecoonds" changes
        property int audioCurrentDurationSeconds: 0;

        //Video
        property int mediaPlayer2SubtitleLid; //Updated when "videoSubtitleId" changes// Check property type
        property int mediaPlayer2SoundtrackLid; //Updated when "videoSoundTrackId" changes// Check property type
        property string mediaPlayer2PlayStatus:""; // Updated when videoPlayerState changes
        property string currentTitle:"";// Updated when "videoCurrentTitle" chnages, may be a part of VideoPlayerdataSMClient
        property string mediaPlayer2MediaAlbumId: ""; //Updated when "videoCurrentId" Changes// COuld be a part of VideoPlayerdataSMClient
        property string mediaPlayer2AspectRatio: ""; //updated  when "videoAspectRatio" changes//Also be updated if aspect ratio is diabled
        property int mediaPlayer2ElapsedTime:0; //Updated when "videoElapseTimeSeconds" changes
        property int videoTimeTotal: 0; // Updated whe videoTimeTotalc changes
        property int mediaPlayer2PlayRate; // Updated when videoPlayBackSpeed Changes
        property int videoOldPausePosition;
        property int languageLid;
        property string languageIso
        property string videoCurrentTitle: "";
        property string videoCurrentID;
        property int seatbackVolume;
        property bool serverBacklightEnabled:true;
        property QtObject delayResetTimer:Timer{
            id:delayResetTimer; repeat: false;interval:10000; onTriggered:__requestSyncOnDelay()
        }

        //General May not use these
        property int seatBeltDuration: 4;
        property bool accessibility: false
        property bool isVideoOn: false
        property bool isAudioOn: false
        property bool isGamesOn: false;
        property bool isTvOn: false;
        property bool isSurveyOn: false;
        property bool isMapOn: false;
        property bool isShoppingOn: false;
        property bool isHospitalityOn: false;
        property bool isSeatChatOn: false;
        property bool gameLoading: false;

        onServerHostnameChanged: core.debug("SharedModelClientInterface.qml | serverHostname is | " + serverHostname);

        onConnecting: core.info("NetworkClientInterface.qml | Connecting to Server");
        onConnected: {
            core.info("NetworkClientInterface.qml | Connected to Server");
            isConnected = true;
        }
        onDisconnected: {
            core.info("NetworkClientInterface.qml | Disconnected from Server");
            isConnected = false;
            online = false;
            if(!core.pc)pif.__interactiveReset();
        }
        onReceived: { // (String messageType, Object message)
            core.info("NetworkClientInterface.qml | onReceived | messageType :"+message.type);
           // if (message.type == "buttonClick") receivedButtonClick(message.page, message.id);
           // else if (message.type == "pageSync") receivedPageSync(message.page);
             if (message.type == "command")dataReceived(message.command, JSON.parse(message.param), message.language)
            else core.info("NetworkClientInterface.qml | Unknown message type :"+ message.type );
        }
        onError: { core.info("NetworkClientInterface.qml onError: " + error);}


        function sendCommand(type,p_command,p_param){
            core.info("NetworkClientInterface.qml | sendCommand | type: "+type+", p_command:"+p_command)
            if(type=="command"){
                client.sendMessage("variant", {type: "command", command: p_command, param: JSON.stringify(p_param), language: languageLid});
            } else if (type=="buttonClick"){
                client.sendMessage("variant", {type: "buttonClick",page:p_command,id:p_param});
            }
        }

        function __requestsync(){
            core.info("SharedClient.qml | __requestsync | Delaying reboot");
            if(!online){online=true;}
            delayResetTimer.restart();
        }

        function __requestSyncOnDelay(){
            core.info("SharedClient.qml | __requestSyncOnDelay | "+online);
            if(!isConnected && !core.pc){core.info("SharedClient.qml | __requestsync | Client not connected | Rebooting");pif.__interactiveReset();return}
            core.info("SharedClient.qml | __requestsync | Client Connected | Requesting Sync ")
            client.sendMessage("variant", {type: "syncRequest"});
        }


       Component.onCompleted: {
            core.info("NetworkClient.qml | Component on Load Completed")
            interactiveStartup.registerToStage(interactiveStartup.cStageSetViewData,[__requestsync]);
        }
 }
