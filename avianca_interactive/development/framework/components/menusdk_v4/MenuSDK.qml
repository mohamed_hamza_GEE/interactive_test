import QtQuick 1.1

Item{
    property int cNORMAL : 1;
    property int cEXCEPTION : 0;

    property int debug : 0;
    property int flightsetIndexNormal : -1;             //index of the user defined flightdata in root.json of normal menu
    property int defaultFlightsetIndexNormal : -1;      //index of the fallbackdata in root.json of normal menu
    property int noFlightsetIndexNormal : -1;           //index of the noFlightData in root.json of normal menu
    property int flightsetIndexException : -1;          //index of the user defined flightdata in root.json of exception menu
    property int defaultFlightsetIndexException : -1;   //index of the fallbackData in root.json of exception menu
    property int noFlightsetIndexException : -1;        //index of the noFlightData in root.json of exception menu
    property int dataReceivedCounter : 0;
    property int dataRequestCounter : 0;
    property int currentFlightTime : 0;
    property int noFallbackDataIfDataNotPresent : 0;
    property bool loadDefaultMetaData : false;
    property bool loadDefaultCommonMetaData : false;
    property int menuUsed : -1;                         // To know if it is using normal menu or exception menu.

    property string flightNo : '';
    property string source : '';
    property string destination : '';
    property string language : '';
    property string version : '';
    property string menuDataVersion : "04";
    property string oldMenuDataVersion : "03"; // Backward Compatibility changes
    property string defaultLanguage : "";
    property string classMapValue : '';  //It is a string.
    property string normalMenuPath : '';
    property string exceptionMenuPath : '';
    property string flightSetIdException : '';
    property string flightSetIdNormal : '';

    property string commonMetaRootPath : '';
    property string commonMetaSdcFolderName : '';
    property string commonMetaLang : '';
    property string defaultCommonMetaLanguage : '';
    property string metaDataLink : '';

    property variant menuObject:null;       // the menu selected in the root.json
    property variant menuTree:null;         // treeNode object.
    property variant menuMetaData: null;    //It has the metadata of all tree nodes for current language.
    property variant defaultMenuMetaData: null;   //It has the metadata of all tree nodes for default language.
    property variant fileNodeProperty: null;
    property variant metaDataList:null;
    property variant supportedLanguages : null;
    property variant tempObject:null;
    property variant rootObjectNormal:null;
    property variant rootObjectException:null;
    property variant commonMetadata: null;
    property variant defaultCommonMetadata: null;
    property variant commonMetaObject: null;

    signal initComplete(int errorCode);
    signal fileNodePropertyReceived(variant properties);
    signal fileNodeMetaDataReceived(variant metadata);
    signal nodeMetaDataInDefaultLangReceived(variant metadata);
    signal languageChanged(int errorCode);
    signal metaDataReceived(variant metadata);
    signal sdcVersion(string version);
    /*
        errorCode will have the below values.
        0   no error
        1   language param is not valid.
        2   rootPath param is not valid.
        3   sdcFolderName param is not valid.
        4   content of the metadata json file is null.
    */
    signal initCommonMetaComplete(int errorCode);

    /*
        0   no error
        1   language param is not valid.
        2   content of the metadata json file is null.
    */
    signal commonMetaLanguageChange(int errorCode);



    /*signal initComplete(int errorCode) will be fired when the init() is complete.
      0 = No error
      1 = if any important file is not found
      2 = Improper template version
      3 = Improper SDC version
      4 = normalMenuPath not proper.
      5 = FlightSetId present in flightset.json but not present in root.json of normal menu.
      6 = FlightSetId present in flightset.json but not present in root.json of exception menu.
      7 = data not available.

    */
    //version 3 signature
    //function init(flightId, source, destination, language, normalMenuPath, version)
    //version 4 signature
    function init(flightId, source, destination, language, normalMenuPath, version,
                  currentFlightTime, classMap, exceptionMenuPath, configObject){
        console.log("Menu SDK init Called." + " flightId " +flightId + " source "   + source  + " destination "   + destination  + " language "   + language  + " normalMenuPath "   + normalMenuPath  + " version "   + version
                   + " currentFlightTime "   + currentFlightTime  + " classMap "   + classMap  + " exceptionMenuPath "   + exceptionMenuPath);
        this.flightNo = flightId;
        this.source = source;
        this.destination = destination;
        this.language = language;
        this.normalMenuPath = normalMenuPath;
		
		this.version = version; //Backward compatibility changes.
		if(normalMenuPath == undefined||normalMenuPath == null)
		{
		    console.log("Error::Exception Menu Path is invalid");
		    initComplete(4);
		}
        else
        {
            _getSDCVersion();
        }
		
		if(currentFlightTime) this.currentFlightTime = currentFlightTime;
        if(classMap) this.classMapValue = classMap;
        if(exceptionMenuPath) this.exceptionMenuPath = exceptionMenuPath;
        if(configObject && configObject.noFallbackDataIfDataNotPresent){
            this.noFallbackDataIfDataNotPresent = configObject.noFallbackDataIfDataNotPresent;
        }else{
            this.noFallbackDataIfDataNotPresent = false;
        }
        if(configObject && configObject.loadDefaultMetaData){
            this.loadDefaultMetaData = configObject.loadDefaultMetaData;
        }else{
            this.loadDefaultMetaData = false;
        }
        console.log("noFallbackDataIfDataNotPresent is " + this.noFallbackDataIfDataNotPresent);

        if(exceptionMenuPath == undefined||exceptionMenuPath == null || version == oldMenuDataVersion)
        {
            console.log("Warning::Exception Menu Path is invalid");
            if(normalMenuPath == undefined||normalMenuPath == null)
            {
                console.log("Error::Exception Menu Path is invalid");
                initComplete(4);
            }
            else
                _loadFlightSets(cNORMAL);
        }
        else
        {
            _loadFlightSets(cEXCEPTION);
        }
    }
    
    function _getSDCVersion(){
		_getJsonFile(cNORMAL, 7, "sdcVersion.json");
        //_parseSdcVersion() will be called on success. sendError() on failure.
    }
    
     function _parseSdcVersion(jsonObject){
        this.version = jsonObject['templateVersion'];
       
    }

    function _loadFlightSets(path){
        _getJsonFile(path, 101, "flightsets.json");
    }

    //signal languageChanged(int errorCode) will be fired when the changeLanguage() is complete.
    function changeLanguage(languageCode){
        console.log("MenuSDK::changeLanguage() called.");
        if(languageCode == undefined||languageCode==""){
            console.log("MenuSDK::Invalid input");
            languageChanged(1);
        }
        language = languageCode;
        if(language == defaultLanguage){
            menuMetaData = defaultMenuMetaData;
            languageChanged(0);
        }
        else{
            if(menuObject && menuObject.metadataLinks){
                _getMenuMetaData(menuUsed, 4, menuObject.metadataLinks, language, true);
            }else{
                console.log("MenuSDK::Invalid menuObject");languageChanged(2);
            }
        }
    }

    //signal sdcVersion(menuDataVersion);
    function getSDCVersion(){
        sdcVersion(menuDataVersion);
    }

    function getMenuInfo(){
        console.log("MenuSDK::getMenuInfo() called.");
        return menuObject;
    }

    function getRootNode(){
        console.log("MenuSDK::getRootNode() called.");
        if(menuObject.rootId) return getNode(menuObject.rootId);
        else return null;
    }

    function getChildList(key){
        console.log("MenuSDK::getChildList() called.");
        if(key==undefined||key==""){console.log("Invalid key"); return null}
        if(!_isNodeExist(key)){console.log("MenuSDK::Invalid key"); return null}
        if(menuTree && menuTree[key]["childList"])
            return menuTree[key]["childList"];
        else {console.log("MenuSDK::Child doesn't exist"); return null}
    }

    function getNode(key){
        console.log("MenuSDK::getNode() called. key:" + key);
        if(key==undefined||key==""){console.log("MenuSDK::Invalid key"); return null}
        if(!_isNodeExist(key)){console.log("MenuSDK::Invalid key"); return null}
        if(menuTree) return menuTree[key];
        else {console.log("MenuSDK::Invalid Menu Tree"); return null}
    }

    function getNodes(nodekeys){
        var data = new Object();
        var x;
        console.log("MenuSDK::getNodes() called.");
        if(nodekeys==undefined||nodekeys==""){console.log("MenuSDK::Invalid keys"); return null}
        for(var i in nodekeys){
            x = getNode(nodekeys[i]);
            if(x){
                data[nodekeys[i]] = x;
            }
        }
        return data;
    }

    function getFilteredChildKeys(nodekey, key, value){
        if(nodekey==undefined||nodekey==""){console.log("MenuSDK::Invalid key"); return null}
        return _getFilteredChildKeys(nodekey,key,value);
    }

    function getFilteredChildObjects(nodekey, key, value){
        if(nodekey==undefined||nodekey==""){console.log("MenuSDK::Invalid key"); return null}
        return getNodes(_getFilteredChildKeys(nodekey,key,value));
    }

    function _getFilteredChildKeys(nodekey, key, value){
        var childs1 = new Array();
        var childs2 = new Array();
        if(!_isNodeExist(nodekey)) return null;
        if(!menuTree[nodekey]["childList"])return null;
        childs1 = menuTree[nodekey]["childList"];
        for(var i in childs1){
            if(menuTree[childs1[i]]!=undefined){
                if(Array.isArray(menuTree[childs1[i]][key]) && menuTree[childs1[i]][key].indexOf(value)!=-1){
                    childs2.push(childs1[i]);
                }else if(menuTree[childs1[i]][key]==value){
                    childs2.push(childs1[i]);
                }
            }
        }
        return childs2;
    }

    function getNodeObjectsByKey(key, value){
        console.log("MenuSDK::getNodeObjectsByKey() called. key: " + key + "  value:  " + value);
        var obj = new Object();
        var keyArray;
        if(key == undefined||key == ""){console.log("MenuSDK::Invalid key"); return null}
        if(value == undefined||value == ""){
            keyArray = Object.keys(menuTree);
            _getNodeObjectsByKey(menuTree, key, value, keyArray, obj, keyArray[0]);
            return obj;
        }else{
            keyArray = Object.keys(menuTree);
            _getNodeObjectsByKey2(menuTree, key, value, keyArray, obj, keyArray[0]);
            return obj;
        }
    }

    function _getNodeObjectsByKey(data, keyName, value, keyArray, obj, parentKey){
        for (var key in data){
            if (typeof(data[key]) == "object" && data[key] != null){
                if(keyArray.indexOf(key) >= 0){
                    parentKey = key;
                }
                _getNodeObjectsByKey(data[key], keyName, value, keyArray, obj, parentKey);
            }else{
                //console.log("key is "+ key + "  data[key] is " + data[key] + " parentKey is " + parentKey + " keyName is  " + keyName + "  menuTree[data[key]] is " + menuTree[data[key]])
                if(menuTree[data[key]] && menuTree[data[key]][keyName]){
                    obj[data[key]]= menuTree[data[key]];
                }
            }
        }
    }

    function _getNodeObjectsByKey2(data, keyName, value, keyArray, obj, parentKey){
        for (var key in data){
            if (typeof(data[key]) == "object" && data[key] != null){
                if(keyArray.indexOf(key) >= 0){
                    parentKey = key;
                }
                _getNodeObjectsByKey2(data[key], keyName, value, keyArray, obj, parentKey);
            }else{
                //console.log("key is "+ key + "  data[key] is " + data[key] + " parentKey is " + parentKey + " keyName is  " + keyName + "  menuTree[data[key]] is " + menuTree[data[key]] + "  menuTree[data[key]][keyName] is " + menuTree[data[key]][keyName])
                if(menuTree[data[key]] && menuTree[data[key]][keyName] && (menuTree[data[key]][keyName].indexOf(value) > -1)){
                    obj[data[key]]= menuTree[data[key]];
                }
            }
        }
    }

    /*
    signal fileNodeMetaDataReceived(variant metadata) will be fired when the getMetaData() is complete.
    if defaultLang == true, get default language data if data for current language is not available.
    function flow.
    getMetaData -> _getMetaData() -> _getFileProperties() ->_parseJson(5) ->_parsefileNodeProperty() ->_parseJson(6) ->
    _parsefileNodeMetaData() -> _setMetaDataListObj()
    */
    function getMetaData(nodekey, defaultLang){
        console.log("MenuSDK::getMetaData() called.");
        if(nodekey==undefined||nodekey==""){console.log("Invalid key"); fileNodeMetaDataReceived({"error":1});return;}
        if(defaultLang==undefined||defaultLang=="")defaultLang=false;
        _getMetaData(nodekey, 0, defaultLang)
    }

    //signal metaDataReceived(variant metadata) will be fired when the getMetaDataOfNodes() is complete.
    function getMetaDataOfNodes(nodekeys, defaultLang){
        console.log("MenuSDK::getMetaDataOfNodes() called.");
        metaDataList = null;
        if(nodekeys==undefined||nodekeys==""){console.log("Invalid keys"); metaDataReceived({"error":1});return;}
        if(defaultLang==undefined||defaultLang=="")defaultLang=false;
        dataRequestCounter = nodekeys.length;
        dataReceivedCounter = 0;
        for(var i in nodekeys){
            _getMetaData(nodekeys[i], 1, defaultLang);
        }
    }

    /*
    flag=0, _getMetaData() called by getMetaData(nodekey)
    flag=1, _getMetaData() called by getMetaDataOfNodes(nodekey)

    function flow.
    _getMetaData() -> _getFileProperties() ->_parseJson(5) ->_parsefileNodeProperty() ->_parseJson(6) ->
    _parsefileNodeMetaData() -> _setMetaDataListObj()
    */
    function _getMetaData(nodekey, flag, defaultLang){
        var s = new Array();
        if(debug)console.log("In _getMetaData() nodekey:: " + nodekey + "  defaultLang::  " + defaultLang);
        if(!_isNodeExist(nodekey)) return null;
        if(menuTree[nodekey]["nodeType"] == "treeNode"){
            if(menuMetaData[nodekey]){
                if(menuMetaData && (flag==1)){
                    _setMetaDataListObj(menuMetaData[nodekey], nodekey);
                }
                else fileNodeMetaDataReceived(menuMetaData[nodekey]);
            }else{
                if(flag==0 && defaultLang==true){
                    if(defaultMenuMetaData[nodekey]){
                        fileNodeMetaDataReceived(defaultMenuMetaData[nodekey]);
                    }else{
                        fileNodeMetaDataReceived({"error":1});
                    }
                }
                else if(flag==0 && defaultLang==false){
                    fileNodeMetaDataReceived({"error":1});
                }
                else if(flag==1 && defaultLang==true){
                    if(defaultMenuMetaData[nodekey]){
                        _setMetaDataListObj(defaultMenuMetaData[nodekey], nodekey);
                    }
                    else{
                        _checkCount();
                    }
                }
                else if(flag==1 && defaultLang==false){
                    _checkCount();
                }
            }
        }else if(menuTree[nodekey]["nodeType"] == "fileNode"){
            if(flag==1)_getFileProperties(nodekey, 2, defaultLang);
            else _getFileProperties(nodekey, 1, defaultLang);
        }
    }

    //signal fileNodePropertyReceived(variant properties) will be fired when the getFileProperties() is complete.
    function getFileProperties(nodekey){
        console.log("MenuSDK::getFileProperties() called.");
        _getFileProperties(nodekey, 0);
    }

    /*
      flag = 0, getFileProperties() is calling the _getFileProperties()
      flag = 1, getMetaData(nodekey) is calling the _getFileProperties()
      flag = 2, getMetaDataOfNodes(nodekeys) is calling the _getFileProperties()
      flag = 3, getMetaDataInDefaultLang(nodekey) is calling the _getFileProperties()
    */
    function _getFileProperties(nodekey, flag, defaultLang){
        var s = new Array();
        var fileNodeFolderPath;
        if(debug)console.log("In _getFileProperties() nodekey:: " + nodekey);
        if(!_isNodeExist(nodekey)) return null;
        if(menuTree[nodekey]["nodeType"] == "fileNode"){
            var t = _getRootPath() + "/" + menuTree[nodekey]["link"];
            s = t.split("/");
            s.pop();
            fileNodeFolderPath = s.join("/");
            console.log("MenuSDK Accessing file " + t);
            worker.sendMessage({ 'file':t,  id:5,  'requestType':menuUsed,  'flag':flag,   'nodeKey':nodekey,   'fileNodeFolderPath':fileNodeFolderPath,  'defaultLang':defaultLang});
            //_parsefileNodeProperty() will be called on success. sendError() on failure.
        }
    }
    function _isNodeExist(key){
        if(menuTree){
            return menuTree[key]!=undefined;
        }
        else return false;
    }
    
    function _getRootPath(){
        return menuUsed ? normalMenuPath : exceptionMenuPath;
    }
    
    /*
      flag = true, called by changeLanguage(languageCode)
      flag = false, called by _parseMenuTreeJson()
      _parseMenuMetaData() will be called on success. sendError() on failure.
    */
   function _getMenuMetaData(requestType, id, file, lang, flag){
        var filePath;
        menuUsed = requestType;
        filePath = requestType ? normalMenuPath : exceptionMenuPath;
        filePath = filePath + "/" + lang +"/" + file;
        console.log("MenuSDK Accessing file " + filePath);
        if(flag)worker.sendMessage({ 'file':filePath, id:id, requestType:requestType, 'flag':1});
        else worker.sendMessage({ 'file': filePath, id:id, requestType:requestType, 'flag':0});
        //_parseMenuMetaData() will be called on success. sendError() on failure.
    }

    function _parseFlightSetJson(jsonObject, requestType){
        var flightSetId = "";
        if(debug)console.log("Control in _parseFlightSetJson()");
        if(jsonObject['flightSetList'].length){
            for(var i in jsonObject['flightSetList']){
                for(var j in jsonObject['flightSetList'][i]['list']){
                    if(jsonObject['flightSetList'][i]['list'][j]['destination'] == destination &&
                            jsonObject['flightSetList'][i]['list'][j]['flightNo'] == flightNo &&
                            jsonObject['flightSetList'][i]['list'][j]['source'] == source){
                        flightSetId = jsonObject['flightSetList'][i]['flightSetId'];
                    }
                }
            }
        }

        if(requestType == cNORMAL){
            flightSetIdNormal = flightSetId;
            if(debug)console.log("flightSetIdNormal is "+ flightSetIdNormal);
            _getJsonFile(cNORMAL, 102, "root.json");
        }
        else if(requestType == cEXCEPTION){
            flightSetIdException = flightSetId;
            if(debug)console.log("flightSetIdException is "+ flightSetIdException);
            _getJsonFile(cEXCEPTION, 102, "root.json");
        }
    }

    function _parseRootJson(jsonObject, requestType){
        var isLanguageSupported = false;
        var index = -1;
        var localObject;

        if(debug)console.log("Control in _parseRootJson()");
        for(var i in jsonObject['rootMenus']){
            if(requestType == cNORMAL){
                if(jsonObject['rootMenus'][i]['flightset'] == flightSetIdNormal){
                    flightsetIndexNormal = i;
                    break;
                }

                if(jsonObject['rootMenus'][i]['flightset'] == "default"){
                    defaultFlightsetIndexNormal = i;
                }

                if(jsonObject['rootMenus'][i]['flightset'] == "noflightset"){
                    noFlightsetIndexNormal = i;
                }
            }
            else if(requestType == cEXCEPTION){
                if(jsonObject['rootMenus'][i]['flightset'] == flightSetIdException){
                    flightsetIndexException = i;
                    break;
                }

                if(jsonObject['rootMenus'][i]['flightset'] == "default"){ //It won't be there.
                    defaultFlightsetIndexException = i;
                }

                if(jsonObject['rootMenus'][i]['flightset'] == "noflightset"){
                    noFlightsetIndexException = i;
                }
            }
        }

        if(debug)console.log("flightsetIndexNormal " + flightsetIndexNormal + " defaultFlightsetIndexNormal " + defaultFlightsetIndexNormal+" noFlightsetIndexNormal " + noFlightsetIndexNormal+" flightsetIndexException " + flightsetIndexException+" defaultFlightsetIndexException " + defaultFlightsetIndexException+" noFlightsetIndexException " + noFlightsetIndexException);

        if(requestType == cNORMAL)
        {
            if(debug)console.log("requestType is cNORMAL");
            //flightSetIdNormal is valid and flightsetIndexNormal is invalid i.e -1
            if(flightSetIdNormal != "" && flightsetIndexNormal == -1)
                initComplete(5);

            //flightSetId of flightset.json(normal menu) is "" and
            //noflightsetdata is not present in root.json(normal menu)
            if((flightSetIdNormal == "" && noFlightsetIndexNormal != -1) ||

            //flightSetId of flightset.json(normal menu) is valid and
            //user defined flightsetdata is present
            (flightSetIdNormal != "" && flightsetIndexNormal != -1))
            {
                if(flightSetIdNormal == "" && noFlightsetIndexNormal != -1)
                    localObject = jsonObject['rootMenus'][noFlightsetIndexNormal];
                if(flightSetIdNormal != "" && flightsetIndexNormal != -1)
                    localObject = jsonObject['rootMenus'][defaultFlightsetIndexNormal];
                
                //   Backward compatibility
                console.log(" version : "+version)
                console.log(" oldMenuDataVersion : "+oldMenuDataVersion)
                if(version == oldMenuDataVersion) // Menu SDK version 3
                {
					if(localObject['link']){
						if(debug)console.log("Result link::  " + localObject['menuList'][index]['link']);
						tempObject = localObject;
						rootObjectNormal = localObject;
						_getJsonFile(cNORMAL, 103, localObject['link']);
					}
					else{
						localObject = jsonObject['rootMenus'][defaultFlightsetIndexNormal];
						if(localObject['link']){
							if(debug)console.log("Result link::  " + localObject['link']);
							tempObject = localObject['menuList'][index];
							rootObjectNormal = localObject;
							_getJsonFile(cNORMAL, 103, localObject['link']);
						}else{
							initComplete(7);
						}
					}
                }
                else // Menu sdk version 4
                {
					index = _processFlightData(localObject, cNORMAL);
					if(localObject['menuList'][index] && localObject['menuList'][index]['link']){
						if(debug)console.log("Result link::  " + localObject['menuList'][index]['link']);
						tempObject = localObject['menuList'][index];
						rootObjectNormal = localObject;
						_getJsonFile(cNORMAL, 103, localObject['menuList'][index]['link']);
					}
					else{
						if(!noFallbackDataIfDataNotPresent){
							localObject = jsonObject['rootMenus'][defaultFlightsetIndexNormal];
							index = _processFlightData(localObject, cNORMAL);
							if(localObject['menuList'][index] && localObject['menuList'][index]['link']){
								if(debug)console.log("Result link::  " + localObject['menuList'][index]['link']);
								tempObject = localObject['menuList'][index];
								rootObjectNormal = localObject;
								_getJsonFile(cNORMAL, 103, localObject['menuList'][index]['link']);
							}
						}else{
							initComplete(7);
						}
					}
                }
            }
            //flightSetId of flightset.json(normal menu) is "" and
            //user defined flightsetdata is not present
            else if(flightSetIdNormal == "" && flightsetIndexNormal == -1)
            {
                if(debug)console.log("flightSetIdNormal : " + flightSetIdNormal + " flightsetIndexNormal: " + flightsetIndexNormal + "  noFallbackDataIfDataNotPresent: " + noFallbackDataIfDataNotPresent);
                //   Backward compatibility
                if(version == oldMenuDataVersion) // Menu SDK version 3
                {
					localObject = jsonObject['rootMenus'][defaultFlightsetIndexNormal];
					if(debug)console.log(" localObject[link] " + localObject['link']);
					tempObject = localObject;
					rootObjectNormal = localObject;
					if(localObject['link']){
						if(debug)console.log("Result link::  " + localObject['link']);
						_getJsonFile(cNORMAL, 103, localObject['link']);
					}
                }
                else // Menu sdk v4
                {
					if(!noFallbackDataIfDataNotPresent){
						localObject = jsonObject['rootMenus'][defaultFlightsetIndexNormal];
						if(debug)console.log(" localObject[menuList][0][link] " + localObject['menuList'][0]['link']);
						index = _processFlightData(localObject, cNORMAL);
						tempObject = localObject['menuList'][index];
						rootObjectNormal = localObject;
						if(localObject['menuList'][index] && localObject['menuList'][index]['link']){
							if(debug)console.log("Result link::  " + localObject['menuList'][index]['link']);
							_getJsonFile(cNORMAL, 103, localObject['menuList'][index]['link']);
						}
					}else{
						initComplete(7);
					}
                }
            }
        }
        else if(requestType == cEXCEPTION)
        {
            if(debug)console.log("requestType is cEXCEPTION");
            //flightSetId of flightset.json(exception menu) is valid and flightsetIndexException is not present in root.json(exception menu)
            if(flightSetIdException != "" && flightsetIndexException == -1)
                initComplete(6);

            //flightSetId of flightset.json(exception menu) is "" and
            //noflightsetdata is not present in root.json(exception menu)
            if(flightSetIdException == "" && noFlightsetIndexException == -1)
            {
                _getJsonFile(cNORMAL, 101, "flightsets.json");
            }
            //flightSetId of flightset.json(exception menu) is "" and
            //noflightsetdata is present in root.json(exception menu)
            else if((flightSetIdException == "" && noFlightsetIndexException != -1) ||

            //flightSetId of flightset.json(exception menu) is valid and
            //flightsetIndexException is present in root.json(exception menu)
            (flightSetIdException != "" && flightsetIndexException != -1))
            {
                if(flightSetIdException == "" && noFlightsetIndexException != -1)
                    localObject = jsonObject['rootMenus'][noFlightsetIndexException];
                if(flightSetIdException != "" && flightsetIndexException != -1)
                    localObject = jsonObject['rootMenus'][flightsetIndexException];

                index = _processFlightData(localObject, cEXCEPTION);
                if(debug)console.log("index  " + index );
                if(localObject['menuList'][index] && localObject['menuList'][index]['link']){
                    if(debug)console.log("  Result link ::  " + localObject['menuList'][index]['link']);
                    tempObject = localObject['menuList'][index];
                    rootObjectException = localObject;
                    _getJsonFile(cEXCEPTION, 103, localObject['menuList'][index]['link']);
                }
                else{
                    _getJsonFile(cNORMAL, 101, "flightsets.json");
                }
            }
        }
    }

    function _parseMenuMetaData(jsonObject, requestType, flag){
        if(debug)console.log("control in _parseMenuMetaData()  requestType:: " + requestType + "flag :: " + flag);
        menuMetaData = jsonObject;
        if(debug){console.log("menuMetaData is");_printContent(menuMetaData);}
        //If the requested language is same as default language
        if(defaultLanguage == language){
            defaultMenuMetaData = menuMetaData;
            if(flag)languageChanged(0);
            else initComplete(0);
        }else{
            //If the requested language is other than default language
            //request metadata of tree nodes for default language.
            if(!defaultMenuMetaData)
               _getMenuMetaData(requestType, 8, menuObject.metadataLinks, defaultLanguage, flag);
            if(flag)languageChanged(0);
            else initComplete(0);
        }
    }

    function _parseDefaultMenuMetaData(jsonObject, flag){
        if(debug){
            console.log("In _parseDefaultMenuMetaData()  defaultMenuMetaData is ::\n");
            _printContent(jsonObject);
        }
        menuMetaData = jsonObject;
        defaultMenuMetaData = jsonObject;
        if(flag)languageChanged(0);
        else initComplete(0);
    }

    function _parsefileNodeProperty(jsonObject, flag, key, fileNodeFolderPath, defaultLang){
        if(debug)console.log("In _parsefileNodeProperty() flag:: " + flag + " key:: " + key);
        fileNodeProperty = jsonObject;
        if(debug){console.log("Content of JSON Object is \n");_printContent(jsonObject);}
        if(flag == 0){fileNodePropertyReceived(fileNodeProperty);}
        else{
            var requestLang = language;
            var languageArray = new Array();
            languageArray = fileNodeProperty["languages"];
						if(languageArray.length>0){
					      if(languageArray.indexOf(language) ==-1)
					      {
					          requestLang = fileNodeProperty["defaultLanguage"];
					      }
						}

            var requestId = 6;
            if(flag==3){
                requestLang = fileNodeProperty["defaultLanguage"];
                requestId = 10;
            }
            var path = fileNodeFolderPath + "/" + requestLang +"/" +fileNodeProperty["metadataLinks"];
            console.log("MenuSDK Accessing file " + path);
            worker.sendMessage({ 'file':path,  id:requestId,  requestType:menuUsed,  'flag':flag,  'nodeKey':key,  'fileNodeFolderPath':fileNodeFolderPath,  'defaultLang':defaultLang});
            //_parsefileNodeMetaData() will be called on success. sendError() on failure.
        }
    }
        
    function _parsefileNodeMetaData(jsonObject, flag, key){
        if(flag == 1)fileNodeMetaDataReceived(jsonObject);
        else if(flag == 2){
            _setMetaDataListObj(jsonObject,key);
        }
    }

    function _parsefileNodeMetaData2(jsonObject, flag, key){
        //console.log("In _parsefileNodeMetaData2() key:: " + key);
        if(flag == 1)fileNodeMetaDataReceived(jsonObject);
        else if(flag == 2){
            _setMetaDataListObj(jsonObject,key);
        }
    }

    function _parsefileNodeMetaData3(jsonObject, flag, key){
        if(flag == 1)fileNodeMetaDataReceived(jsonObject);
        else if(flag == 2){
            _setMetaDataListObj(jsonObject,key);
        }
    }

    function _setMetaDataListObj(jsonObject, key){
        var x = new Object();
        if(metaDataList)x = metaDataList;
        x[key] = jsonObject;
        metaDataList = x;
        _checkCount();
    }

    function _checkCount(){
        dataReceivedCounter++;
        if(dataReceivedCounter==dataRequestCounter){
            if(metaDataList) metaDataReceived(metaDataList);
            else metaDataReceived({'error':1});
        }
    }

    function _cloneObject(obj){
        var clone = {};
        for(var i in obj) {
            if(typeof(obj[i])=="object" && obj[i] != null)
                clone[i] = _cloneObject(obj[i]);
            else
                clone[i] = obj[i];
        }
        return clone;
    }

    function _createMenuObject(requestType){
        var a;
        var isLanguageSupported = false;
        if(debug)console.log("Control in createMenuObject()");
        a = _cloneObject(tempObject);
        if(requestType == cNORMAL)
        {
            a["flightset"] = rootObjectNormal.flightset;
            a["defaultLanguage"] = rootObjectNormal.defaultLanguage;
            a["languages"] = rootObjectNormal.languages;
            a["languageLabels"] = rootObjectNormal.languageLabels;
            defaultLanguage = rootObjectNormal.defaultLanguage;
            supportedLanguages = rootObjectNormal.languages;
        }
        else if(requestType == cEXCEPTION)
        {
            a["flightset"] = rootObjectException.flightset;
            a["defaultLanguage"] = rootObjectException.defaultLanguage;
            a["languages"] = rootObjectException.languages;
            a["languageLabels"] = rootObjectException.languageLabels;
            defaultLanguage = rootObjectException.defaultLanguage;
            supportedLanguages = rootObjectException.languages;
        }
        for(var j in supportedLanguages){
            if(supportedLanguages[j]==language){
                isLanguageSupported=true;break;
            }
        }
        if(!isLanguageSupported) language = defaultLanguage;
        if(debug) console.log("In createMenuObject()  defaultLanguage  " + defaultLanguage + " isLanguageSupported " + isLanguageSupported);
        return a;
    }

    function _createObjects(requestType, jsonObject, language, flag){
        if(debug)console.log("control in _createObjects()");
        menuObject = _createMenuObject(requestType);
        menuTree = jsonObject;
        _printContent(menuObject);
        //requesting metadata file
        _getMenuMetaData(requestType, 4, menuObject.metadataLinks, language, flag);
    }

    function _parseTreeNodeJson(jsonObject, requestType){
        var classArray = new Array();
        if(debug)console.log("Control in _parseTreeNodeJson()");
        for(var key in jsonObject){
            //console.log("key is  " + key + " jsonObject[key] is " + jsonObject[key]);

            if(jsonObject[key]["nodeType"] == "rootNode"){

                //cabinClass entry is present in the rootNode
                if(jsonObject[key]["cabinClass"])
                {
                    if(debug)console.log("rootNode matches. jsonObject[key][cabinClass] is " + jsonObject[key]["cabinClass"]);
                    classArray = jsonObject[key]["cabinClass"];
                    
                    if(version==oldMenuDataVersion){
                        _createObjects(requestType, jsonObject, language, false);
                        break;
                    }else{
                        //cabinClass array is empty
                        if(classArray.length == 0){
                            _createObjects(requestType, jsonObject, language, false);
                            break;
                        }
                        //given class is present in the cabinClass array.
                        else if(classArray.indexOf(classMapValue) >= 0){
                            _createObjects(requestType, jsonObject, language, false);
                            break;
                        }
                        else{
                            _getJsonFile(cNORMAL, 101, "flightsets.json");
                            break;
                        }
                    }
                }
                //cabinClass entry is not present in the rootNode
                else
                {
                    _createObjects(requestType, jsonObject, language, false);
                    break;
                }
            }
        }
    }

    function _processFlightData(mObject, requestType){
        var matchingIndex = -1;
        if(debug){
           console.log("Control in _processFlightData()");
           console.log("mObject['menuList'].length  " + mObject['menuList'].length);
        }
        if(!mObject){initComplete(7); return;}

        for(var i = 0; i < mObject['menuList'].length; i++)
        {
            if(debug)console.log("mObject['menuList'][i]['timeline'].length  " + mObject['menuList'][i]['timeline'].length);

            if(mObject['menuList'][i]['timeline'].length)
            {
                for(var j=0; j < mObject['menuList'][i]['timeline'].length; j++)
                {
                    if(debug){
                           console.log("mObject['menuList'][i]['timeline'][j].startDate  " + mObject['menuList'][i]['timeline'][j].startDate);
                           console.log("mObject['menuList'][i]['timeline'][j].endDate  " + mObject['menuList'][i]['timeline'][j].endDate);
                           console.log("mObject['menuList'][i].deviationMenu  " + mObject['menuList'][i].deviationMenu);
                        }

                    if(currentFlightTime > parseInt(mObject['menuList'][i]['timeline'][j].startDate,10) && parseInt(mObject['menuList'][i]['timeline'][j].endDate,10) > currentFlightTime)
                    {
                        if(debug)console.log("Timeline matches for i " + i );
                        if(mObject['menuList'][i].deviationMenu)
                            return i;
                        else
                            matchingIndex = i;
                    }
                }
            }
            //If timeLine is empty array.
            else
            {
                return i;
            }
        }
        return matchingIndex;
    }

    function _getJsonFile(requestType, id, file, flag){
        var filePath;
        if(debug)console.log("Control in _getJsonFile()");
        filePath = requestType ? normalMenuPath : exceptionMenuPath;
        filePath = filePath + "/" + file;
        console.log("MenuSDK Accessing file " + filePath);
        worker.sendMessage({ 'file':filePath, id:id, requestType:requestType, 'flag': flag});
    }

    function initCommonMeta(language, rootPath, configObject){
        console.log("Menu SDK initCommonMeta() Called." + " language "   + language  + " rootPath "   + rootPath);

        //Need to check if language is part of the array.

        if(language == undefined){
            initCommonMetaComplete(1);
            return;
        }
        if(rootPath == undefined){
            initCommonMetaComplete(2);
            return;
        }
        if(configObject && configObject.loadDefaultMetaData){
            this.loadDefaultCommonMetaData = configObject.loadDefaultMetaData;
        }else{
            this.loadDefaultCommonMetaData = false;
        }
        commonMetaRootPath = rootPath;
        commonMetaLang = language;
        _getJsonFileCommonMeta(104, "main.json");
    }

    function _getJsonFileCommonMeta(id, file, flag){
        var filePath;
        if(debug)console.log("Control in _getJsonFileCommonMeta()");
        filePath = commonMetaRootPath + "/" + file;
        console.log("MenuSDK Accessing file " + filePath);
        worker.sendMessage({ 'file':filePath, id:id, 'flag': flag});
    }

    function _parseCommonMeta1(jsonObject, flag){
        var filePath;
        if(debug){
            console.log("Control in _parseCommonMeta1(). jsonObject is ");
            _printContent(jsonObject);
        }
        commonMetaObject = jsonObject;
        defaultCommonMetaLanguage = jsonObject.defaultLanguage;
        metaDataLink = jsonObject.metadataLinks;
        filePath = commonMetaRootPath + "/" + commonMetaLang + "/" + metaDataLink;
        console.log("MenuSDK Accessing file " + filePath);
        if(commonMetaLang == jsonObject.defaultLanguage){
            worker.sendMessage({ 'file':filePath, id:105, 'flag': 1});
        }
        else{
            worker.sendMessage({ 'file':filePath, id:105, 'flag': 0});
        }
    }

    function _parseCommonMeta2(jsonObject, flag){
        var filePath;
        console.log("Control in _parseCommonMeta2().");
        if(!jsonObject){
            initCommonMetaComplete(4);
            return;
        }
        else{
            if(flag == 1){
                defaultCommonMetadata = jsonObject;
                commonMetadata = jsonObject;
                initCommonMetaComplete(0);
            }
            else{
                commonMetadata = jsonObject;
                filePath = commonMetaRootPath + "/" + defaultCommonMetaLanguage + "/" + metaDataLink;
                console.log("MenuSDK Accessing file " + filePath);
                worker.sendMessage({ 'file':filePath, id:106, 'flag': 0});
            }
            //console.log("jsonObject::");
            //_printContent(commonMetaJsonObject);
        }
    }

    function _parseCommonMeta3(jsonObject, flag){
        var filePath;
        console.log("Control in _parseCommonMeta3().");
        if(!jsonObject){
            initCommonMetaComplete(4);
            return;
        }
        else{
            defaultCommonMetadata = jsonObject;
            initCommonMetaComplete(0);
        }
    }

    /*
        Error ==>
        1   key is not valid.
        2   attribute is not valid.
        3   identifier is not valid.
        4   attribute is not present in the metadata.
        5   key identifier combination is not available in the metadata.
    */
    //Example (key="featured", attribute="texts", identifier="title")
    function getCommonMetaData(key, attribute, identifier){
        var keyName;
        var obj;
        console.log("Menu SDK getCommonMetaData() Called." + " key "   + key  + " attribute "   + attribute  + " identifier "   + identifier);
        //console.log("commonMetaJsonObject::");
        //_printContent(commonMetaJsonObject);
        if(key == undefined){
            return [{'Error':1}];
        }
        if(attribute == undefined){
            return [{'Error':2}];
        }
        if(identifier == undefined){
            return [{'Error':3}];
        }
        keyName = key + "-" + identifier;
        if(commonMetadata[attribute]){
            //console.log("commonMetadata[attribute]::");
            //_printContent(commonMetadata[attribute]);
            if(commonMetadata[attribute][keyName]){
                if(debug){
                    console.log("commonMetadata[attribute][keyName]::");
                    _printContent(commonMetadata[attribute][keyName]);
                }
                return commonMetadata[attribute][keyName];
            }
            else
                return [{'Error':5}];
        }
        else
            return [{'Error':4}];
    }

    function _parseCommonMeta4(jsonObject, flag){
        var filePath;
        console.log("Control in _parseCommonMeta4().");
        if(!jsonObject){
            commonMetaLanguageChange(2);
            return;
        }
        else{
            commonMetadata = jsonObject;
            commonMetaLanguageChange(0);
        }
    }

    function changeLanguageCommonMeta(language){
        var filePath;
        console.log("Menu SDK changeLanguageCommonMeta() Called.");

        //Need to check if language is part of the array.

        if(language == undefined){
            commonMetaLanguageChange(1);
            return;
        }
        if(language == defaultCommonMetaLanguage){
            commonMetadata = defaultCommonMetadata;
            commonMetaLanguageChange(0);
        }
        else{
            filePath = commonMetaRootPath + "/" + language + "/" + metaDataLink;
            console.log("MenuSDK Accessing file " + filePath);
            worker.sendMessage({ 'file':filePath, id:107});
        }
    }

    function _printContent(data){
        for (var key in data){
            if (typeof(data[key]) == "object" && data[key] != null){
                console.log(key + " : " + data[key]);
                _printContent(data[key]);
            }else{
                console.log(key + " : " + data[key]);
            }
        }
    }

    /*
    signal fileNodeMetaDataInDefaultLangReceived(variant metadata) will be fired when the getMetaDataInDefaultLang() is complete.
    get default language data
    */
    function getMetaDataInDefaultLang(nodekey){
        console.log("MenuSDK::getMetaDataInDefaultLang() called.");
        if(nodekey==undefined||nodekey==""){console.log("Invalid key"); nodeMetaDataInDefaultLangReceived({"error":1});return;}
        if(!_isNodeExist(nodekey)) return null;
        if(menuTree[nodekey]["nodeType"] == "treeNode"){
            nodeMetaDataInDefaultLangReceived(defaultMenuMetaData[nodekey]);
        }else if(menuTree[nodekey]["nodeType"] == "fileNode"){
            _getFileProperties(nodekey,3,false);
        }
    }

    function _parsefileNodeMetaDataInDefaultLang(jsonObject, flag, key){
        console.log("In _parsefileNodeMetaDataInDefaultLang ");
        nodeMetaDataInDefaultLangReceived(jsonObject);
    }

    function getCommonMetaInfo(){
        console.log("MenuSDK::getCommonMetaInfo() called.");
        return commonMetaObject;
    }

    function _parseJson(jsonObject, id, requestType, flag, nodeKey, fileNodeFolderPath, defaultLang){
        switch(id){
            case 4:{
                if(debug)console.log("before _parseMenuMetaData() requestType:: " + requestType);
                _parseMenuMetaData(jsonObject, requestType, flag);
                break;
            }
            case 5:{
                _parsefileNodeProperty(jsonObject, flag, nodeKey, fileNodeFolderPath, defaultLang);
                break;
            }
            case 6:{
                _parsefileNodeMetaData(jsonObject, flag, nodeKey);
                break;
            }
            case 7:{
                _parseSdcVersion(jsonObject);
                break;
            }
            case 8:{
                _parseDefaultMenuMetaData(jsonObject, flag);
                break;
            }
            case 9:{
                _parsefileNodeMetaData2(jsonObject, flag, nodeKey);
                break;
            }
            case 101:{
                _parseFlightSetJson(jsonObject, requestType);
                break;
            }
            case 102:{
                _parseRootJson(jsonObject, requestType);
                break;
            }
            case 103:{
                _parseTreeNodeJson(jsonObject, requestType);
                break;
            }
            case 104:{
                _parseCommonMeta1(jsonObject, flag);
                break;
            }
            case 105:{
                _parseCommonMeta2(jsonObject, flag);
                break;
            }
            case 106:{
                _parseCommonMeta3(jsonObject, flag);
                break;
            }
            case 107:{
                _parseCommonMeta4(jsonObject, flag);
                break;
            }case 10:{
                 _parsefileNodeMetaDataInDefaultLang(jsonObject, flag, nodeKey);
                 break;
            }
        }
    }

    function _sendError(id, requestType, flag, nodeKey, fileNodeFolderPath, defaultLang){
        console.log("MenuSDK file not present.\n");
        switch(id){
            case 4:{
                if(loadDefaultMetaData){
                    console.log("Categories metadata not present for requested language. trying to get file in default language\n : "+defaultLanguage);
                    _getMenuMetaData(requestType, 8, menuObject.metadataLinks, defaultLanguage, flag);
                    break;
                }else{
                    if(flag == 1)languageChanged(1);
                    else initComplete(1);
                    break;
                }
            }
            case 5:{
                if(flag==0)fileNodePropertyReceived({'error':1});       //triggered by getFileProperties()
                else if(flag==1)fileNodeMetaDataReceived({'error':1});  //triggered by getMetaData()
                else if(flag==2)_checkCount();                          //triggered by getMetaDataOfNodes()
                break;
            }
            case 6:{
                if(defaultLang){
                    console.log("MenuSDK file not present. trying to get file in default language\n");
                    if(flag==1 || flag==2){
                        var path = fileNodeFolderPath + "/" + defaultLanguage +"/" +fileNodeProperty["metadataLinks"];
                        console.log("MenuSDK Accessing file " + path);
                        worker.sendMessage({ 'file':path, id:9, 'flag':flag, 'nodeKey':nodeKey});
                        //_parsefileNodeMetaData2() will be called on success. sendError() on failure.
                    }
                }
                else{
                    if(flag==1){
                        fileNodeMetaDataReceived({'error':1});
                    }else if(flag==2){
                        _checkCount();
                    }
                }
                break;
            }
            case 9:{
                if(flag==1){
                    fileNodeMetaDataReceived({'error':1});  //triggered by getMetaData()
                }
                break;
            }
            case 101:{
                if(requestType == cEXCEPTION)
                    _getJsonFile(cNORMAL, 101, "flightsets.json");
                else if(requestType == cNORMAL)
                    initComplete(4);
                break;
            }
            case 102:{
                if(requestType == cEXCEPTION){
                    initComplete(4);    //It can be problem with canvas update tool or handcrafted files.
                    // flightset.json is present but root.json is missing
                }else if(requestType == cNORMAL){
                    initComplete(4);    //It can be problem with canvas update tool or handcrafted files.
                    // flightset.json is present but root.json is missing
                }
                break;
            }
            case 103:{
                if(requestType == cEXCEPTION){
                    initComplete(1);    //It can be problem with canvas update tool or handcrafted files.
                    // flightset.json and root.json is present but menu.json is missing
                }else if(requestType == cNORMAL){
                    initComplete(1);    //It can be problem with canvas update tool or handcrafted files.
                    // flightset.json and root.json is present but menu.json is missing
                }
                break;
            }
            case 104:{
                if(requestType == cEXCEPTION){
                    initCommonMetaComplete(1);    //It can be problem with canvas update tool or handcrafted files.
                    // flightset.json and root.json is present but menu.json is missing
                }else if(requestType == cNORMAL){
                    initCommonMetaComplete(1);    //It can be problem with canvas update tool or handcrafted files.
                    // flightset.json and root.json is present but menu.json is missing
                }
                break;
            }
            case 105:{
                if(loadDefaultCommonMetaData){
                    console.log("Metadata not present for requested language. trying to get file in default language\n : "+defaultCommonMetaLanguage);
                    var filePath = commonMetaRootPath + "/" + defaultCommonMetaLanguage + "/" + commonMetaObject.metadataLinks;
                    console.log("MenuSDK Accessing file " + filePath);
                    worker.sendMessage({ 'file':filePath, id:105, 'flag': 1});
                }else{
                    initCommonMetaComplete(1);
                }
                break;
            }
            case 107:{
                if(loadDefaultCommonMetaData){
                    console.log("Metadata not present for requested language. trying to get file in default language\n : "+defaultCommonMetaLanguage);
                    var filePath = commonMetaRootPath + "/" + defaultCommonMetaLanguage + "/" + commonMetaObject.metadataLinks;
                    console.log("MenuSDK Accessing file " + filePath);
                    worker.sendMessage({ 'file':filePath, id:105, 'flag': 1});
                }else{
                    commonMetaLanguageChange(2);
                }
                break;
            }
            case 10:{
                nodeMetaDataInDefaultLangReceived({'error':1});
                break;
            }
        }
    }

    WorkerScript {
        id: worker
        source: "getFile.js"
        onMessage: {
            if(messageObject.error==0){
                if(debug)console.log("success messageObject.message.id:: " + messageObject.message.id + " messageObject.message.requestType::  " + messageObject.message.requestType +"  messageObject.message.flag :: " + messageObject.message.flag);
                _parseJson(messageObject.data, messageObject.message.id, messageObject.message.requestType,
                           messageObject.message.flag, messageObject.message.nodeKey,
                           messageObject.message.fileNodeFolderPath, messageObject.message.defaultLang);
            }
            else{
                if(debug)console.log("failure messageObject.message.id:: " + messageObject.message.id + "  messageObject.message.flag :: " + messageObject.message.flag + "   messageObject.message.fileNodeFolderPath ::  " + messageObject.message.fileNodeFolderPath);
                _sendError(messageObject.message.id, messageObject.message.requestType,
                           messageObject.message.flag, messageObject.message.nodeKey,
                           messageObject.message.fileNodeFolderPath, messageObject.message.defaultLang);
            }
        }
    }

    Component.onCompleted:{
        console.log("menuSDK component loaded.");
    }
}
