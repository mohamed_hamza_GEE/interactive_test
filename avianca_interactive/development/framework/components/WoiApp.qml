import QtQuick 1.1
import Panasonic.Pif 1.0
import '../blank.js' as Store
import '../components'

DataLoader {
    id: woiApp
    property string appContentPath
    property string source
    property string destination
    property string catFieldList
    property string synFieldList
    property bool __appStatus: false
    signal __dataReady(variant model)
    //property alias listModel:listModel
    onSigLocalDataReady:{
        if(errCode==0){
            core.info('WoiApp.qml | onSigLocalDataReady='+requestId);
            if(requestId==1) __readFlightSetId(responseObj['flightSetList'])
            else if(requestId==2) __readRootFile(responseObj['rootMenus'])
            else if(requestId==3) __createMainMenu(responseObj['childList'],false,pid)
            else if(requestId==4) __createMainMenu(responseObj,true,pid)
        }
    }

    SimpleModel{id:listModel}

    function getLable(iso,id,lable){return (Store.txtArr[iso] && Store.txtArr[iso][id])?Store.txtArr[iso][id][lable]:Store.txtArr[Store.defaultLang][id][lable];}
    function setDefaultLanguage(iso){Store.defaultLang=iso; return true;}
    function getAppStatus(){ return __appStatus; }
    function startPreload(){ __getFlightSetId();return true}
    function getCurrentLevel(callBack){
        __storeCallBack(callBack)
        return listModel;
    }

    function getNextLevel(id, callBack){
        if(id==undefined) id=0;
        __storeCallBack(callBack)
        __createSimpleMode(id,callBack);
    }

    function getPreviousLevel ( id, callBack ){
        var breakLoop=0;
        for(var i in Store.woiAppArr){ if(breakLoop==1){break;}
            for(var j in Store.woiAppArr[i])
                if(Store.woiAppArr[i][j]['id']==id) {var pid=Store.woiAppArr[i][j]['pid'];breakLoop=1; break;}
        }
        __storeCallBack(callBack)
        return  __createSimpleMode(pid);
    }

    function __storeCallBack(callBack){
        if(Store.callBack) __dataReady.disconnect(Store.callBack)
        Store.callBack=callBack;
        __dataReady.connect(callBack)
        return true;
    }

    function __createSimpleMode(id,callBack){
        for(var i in Store.woiAppArr)
            for(var j in Store.woiAppArr[i])
                console.log("WoiApp.qml | Store.woiAppArr["+i+"]["+j+"]="+Store.woiAppArr[i][j]['title'])
        listModel.clear()
        if(Store.woiAppArr[id]==undefined) return false;
        for(var i in Store.woiAppArr[id]){ listModel.append(Store.woiAppArr[id][i]) }
        __dataReady(listModel)
        __dataReady.disconnect(callBack)
    }

    function __createMainMenu(response,fileNote,pid){
        if(Store.index==undefined) Store.index=0
        if(Store.pid==undefined) Store.pid=0
        else Store.pid=pid
        var responseObj=new Object;
        if(fileNote){responseObj[0]=response}
        else {responseObj=response}
        if(Store.txtArr==undefined)Store.txtArr=[]

        for (var x in responseObj){
            var classFound=1
            if(responseObj[x]['attributes']!=undefined){
                classFound=0
                for(var c in responseObj[x]['attributes']['class']) {
                    core.debug('WoiApp.qml | __createMainMenu | Class='+responseObj[x]['attributes']['class'][c].toLowerCase())
                    if(responseObj[x]['attributes']['class'][c].toLowerCase()==pif.getCabinClassName().toLowerCase()) classFound=1;
                }
            }
            if(classFound==0) continue;
            if(responseObj[x]['formType']!='categoryTerminalNode'){
                Store.index=Store.index+1;
                if(Store.woiAppArr==undefined) Store.woiAppArr=[];
                if(Store.woiAppArr[Store.pid]==undefined)Store.woiAppArr[Store.pid]=[];
                Store.woiAppArr[Store.pid][Store.index]={'id':Store.index, 'pid':Store.pid,
                    'title':responseObj[x]['metadata']['title'][0]['value'],
                    'fileUrl':responseObj[x]['metadata']['fileUrl'],
                    'viewer':responseObj[x]['metadata']['viewer']}
            }
            for(var t in responseObj[x]['metadata']['poster']){
                if(responseObj[x]['metadata']['poster'][t]['posterSize']!=undefined)
                    Store.woiAppArr[Store.pid][Store.index]["size_"+responseObj[x]['metadata']['poster'][t]['posterSize']]=responseObj[x]['metadata']['poster'][t]['fileUrl']
            }

            __createTxtArr(responseObj)
            if(responseObj[x]['formType']=='categoryTerminalNode'){
                Store.pidBak_2=Store.pid
                Store.pid=Store.index; Store.bakpid=Store.pid;
                for(var i in responseObj[x]['fileList']){
                    if(responseObj[x]['fileList'][i]['link']){
                        if(responseObj[x]['fileList'][i]['link']){  __getFileData(responseObj[x]['fileList'][i]['link'],Store.pid); Store.pid=Store.bakpid }
                    }
                }
                Store.pid=Store.pidBak_2

                return true;
            }
            if(responseObj[x]['link']){
                console.log("WoiApp.qml | =================== Start");

                Store.PidBackUp.push(Store.pid);
                console.log("WoiApp.qml |Store.pidBak_1="+Store.pidBak_1);
                Store.pid=Store.index;
                console.log("WoiApp.qml |Store.pid="+Store.pid);
                __getMenuArr(responseObj[x]['link'],Store.pid);
                console.log("WoiApp.qml | Store.PidBackUp.length="+Store.PidBackUp.length)
                Store.pid=Store.PidBackUp[Store.PidBackUp.length-1] ;
                Store.PidBackUp.pop()
                console.log("WoiApp.qml |Store.pid="+Store.pid);
                console.log("WoiApp.qml | =================== End");
            }
        }
        __appStatus=true;
    }

    function __createTxtArr(responseObj){
        for(var j in Store.fieldList){
            for(var t in responseObj[x]['metadata'][Store.fieldList[j]]){
                if(Store.txtArr[responseObj[x]['metadata'][Store.fieldList[j]][t]['langId']]==undefined) Store.txtArr[responseObj[x]['metadata'][Store.fieldList[j]][t]['langId']]=[]
                Store.txtArr[responseObj[x]['metadata'][Store.fieldList[j]][t]['langId']][Store.index]=[]
                Store.txtArr[responseObj[x]['metadata'][Store.fieldList[j]][t]['langId']][Store.index][Store.fieldList[j]]=responseObj[x]['metadata'][Store.fieldList[j]][0]['value']
            }
        }
        return true;
    }

    function __readRootFile(responseObj){
        core.log('WoiApp.qml | __readRootFile | Store.flightSetId='+Store.flightSetId)
        var catLink
        for(var x in responseObj){
            if(responseObj[x]['attributes']['default']=='yes') catLink= responseObj[x]['link']
            else if (Store.flightSetId==responseObj[x]['attributes']['flightSet']) catLink=  responseObj[x]['link']
        }
        __getMenuArr(catLink)
    }

    function __getFileData(catLink,pid){
        core.info('WoiApp.qml | __getMenuArr '+catLink);
        Store.fieldList=catFieldList.split(',')
        getLocalDataWithoutDelay(appContentPath+catLink,4,pid)
    }

    function __getMenuArr(catLink,pid){
        core.info('WoiApp.qml | __getMenuArr '+catLink);
        Store.fieldList=synFieldList.split(',')
        getLocalDataWithoutDelay(appContentPath+catLink,3,pid)
    }

    function __readFlightSetId(responseObj){
        core.log('WoiApp.qml | __readFlightSetId | flightSet Id ='+ destination + '==='+source )
        for(var x in responseObj){
            if(responseObj[x]['list'][0]['destination']== destination && responseObj[x]['list'][0]['source']==source){ Store.flightSetId=responseObj[x]['flightSet'];break;}
        }
        __getRootFile();
    }

    function __getFlightSetId(){
        core.info('WoiApp.qml | __getFlightSetId ');
        getLocalDataWithoutDelay(appContentPath+'flightsets.json',1)
    }

    function __getRootFile(){
        core.info('WoiApp.qml | __getRootFile ');
        getLocalDataWithoutDelay(appContentPath+'root.json',2)
    }

    Component.onCompleted: {Store.PidBackUp=[]}
}
