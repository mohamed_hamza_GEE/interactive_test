import Qt 4.7
import '../testLinks/TestLinkArr.js' as LinkArr
import '../testLinks/TestLinkUtils.js' as LinkFx

Rectangle {
    property string linkLabel: textId.text
    property string id
    x:5
    width: textId.paintedWidth + 30
    height: 30
    opacity: 0
    radius: 5
    color: "#5C5C5C";
    Text {
		id:textId ; 
		anchors.centerIn: parent; 
		text: linkLabel;
		color: "#FFFFFF";
	}

    Behavior on opacity{
        NumberAnimation{
            duration : 600
        }
    }
    MouseArea{ 
		anchors.fill: parent; 
		onClicked:{
            core.debug('CompTestLinkButton.qml | onClicked | linkId='+id);
            for (var i in LinkArr.testLikeArr){
                for(var j in LinkArr.testLikeArr[i]){
                    if(LinkArr.testLikeArr[i][j]==id){
                        LinkFx.showLinks(j)
                        core.debug('CompTestLinkButton.qml | onClicked | LinkArr['+i+']['+j+']='+LinkArr.testLikeArr[i][j] +' linkId='+id)
                    }
                }
            }
        }
    }
}
