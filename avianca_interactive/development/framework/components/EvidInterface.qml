import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item{
    id:evidInterface

    property variant evidClient;
    property bool connected;
    signal messageReceived(string command,string value);

    Connections{
        target: evidClient?evidClient:null
        onMessageReceived: {evidInterface.__evidMessageReceived(command, value);}
        onConnectedChanged:{core.info("EvidInterface.qml | onConnectedChanged connected: " + connected);evidInterface.connected=evidClient.connected}
    }

    function requestValue(evid){evidClient.requestValue(evid);}
    function send(evid,value){evidClient.send(evid,value);}

    function __evidMessageReceived(command, value){
        core.info("EvidInterface.qml | __evidMessageReceived command: "+command+" | value: "+value);
        messageReceived(command,value)
    }

    function setRowMapping(rowMap){
        if (core.pc  || pif.getSimulationData().Evid){
            evidClient.setRowMapping(rowMap);
        }
    }

    Timer {
        id: t;
        repeat: true;
        interval: 1000;
        running: !evidClient.connected;
        triggeredOnStart: true;
        onTriggered: evidClient.connectToServer("rmu", 19158);
    }

    Component {
        id: evidClientComp;
        EvidClient{}
    }

    DataLoader {
        id: jsonLoader;
        onSigLocalDataReady:{__readJsonObject(errCode,responseObj);}
    }

    Component.onCompleted: {
        core.info("EvidInterface.qml | Evid component load complete");
       if (core.pc  || pif.getSimulationData().Evid)evidClient = Qt.createQmlObject('import QtQuick 1.1;import "../testpanel"; EvidSim{}',evidInterface);
        else evidClient = evidClientComp.createObject(evidInterface);
        core.info ("EvidInterface.qml |  evidClient Reference: "+evidClient);connected=evidClient.connected
    }
}
