import QtQuick 1.1
import Panasonic.Pif 1.0
import Panasonic.IBROWSER 1.0

Item{

    property alias serverIP:iBrowserServer.ipAddress;

    function __formatMessage(message){
        var msg = message;
        return JSON.stringify(msg).replace(/["]/g,"'");
    }

    function __sendMessage(message){
        core.info("BrowserComponent.qml | __sendMessage() message is " + message);
        iBrowserServer.sendCommand(message);
    }

    function __messageReceived(message){   // message received from iBrowser2
        core.info("BrowserComponent.qml | __messageReceived");
        if (!message) return;
        core.verbose("BrowserComponent.qml | STRING RECEIVED: "+message);
        var msg = eval('(' + message + ')');
        __processReceivedData(msg.api, msg.properties);
    }

    function __processReceivedData(api, params){
        dataReceived(api, params);
    }

    function dataReceived(apiName, params){ core.debug("BrowserComponent.qml | dataReceived(api) called: "+apiName);}

    function updateBrowserProperty(properties){
        var msg;
        core.debug("BrowserComponent.qml | updateBrowserProperty()");
	msg = '{ "api" : "null", "properties" :'+ properties +'}'
        __sendMessage(msg);
    }

    function updateBrowserApi(api, params){
        var msg;
        core.debug("BrowserComponent.qml | updateBrowserApi("+api+")");
	msg = '{ "api" :'+'"'+ api+'"'+', "properties" :'+ params+'}'
        __sendMessage(msg);
    }

    IBrowserServer {
        id: iBrowserServer;

        onCommandReceived:{
            core.debug("BrowserComponent.qml | Received Msg from browser : " +receivedCommand);
            __messageReceived(receivedCommand);
        }
    }
}
