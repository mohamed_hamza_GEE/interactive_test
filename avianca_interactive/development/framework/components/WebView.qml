import QtQuick 1.1
import QtWebKit 1.0

//	PURPOSE: To launch URLs in webview browser.
//	URLs for CS/FS must use IP address.
//	Dont use hostname(web-server) for URLs for CS/FS since some of the selections inside Interactive Voyager work with IP address only.
//	GUI needs to handle putting the interactive in mouse or handset mode.
//	GUI will receive closeWebViewGUI signal only if close was initiated by WebView. If GUI initialied close (closeWebView) then no signal is sent out.

FocusScope{
    id:webView
    focus:true;
    property string url:core.pc? __blankUrl:'';
    property string paxusAppName: "";
    property string __blankUrl: core.pc?"/tmp/interactive/framework/components/webView/blank.html":'';
    property int noWebConnectivity: 0;		// 0 => web connectivity exist,	1 => checking web connectivity, 2 => no web connectivity
    property variant pipArray;
    signal	closeWebViewGUI(string param);

    Connections{target:core.pif.vod;
        onSigMidPlayBegan:{
            pif.vod.setPIPByArray(pipArray);
			myWebView.evaluateJavaScript("playbackStateChange('PLAYING')");
            core.info("WebView | onSigMidPlayBegan")
        }
        onSigMidPlayStopped:{
             core.info("WebView | onSigMidPlayStopped")
			myWebView.evaluateJavaScript("playbackStateChange('STOPPED')");
            myWebView.evaluateJavaScript("endOfStream()");
        }
        onSigMidPlayCompleted:{
               core.info("WebView | onSigMidPlayCompleted")
            myWebView.evaluateJavaScript("endOfStream()");
        }
		
		onSigMediaPaused:{
            core.info("WebView | onSigMediaPaused")
            myWebView.evaluateJavaScript("playbackStateChange('PAUSED')");
        }
		
		onSigMediaForward:{
            core.info("WebView | onSigMediaForward")
            myWebView.evaluateJavaScript("fastForwardVideo()");
        }

		onSigMediaRewind:{
            core.info("WebView | onSigMediaRewind")
            myWebView.evaluateJavaScript("rewindVideo()");
        }

    }

    function init() {
        core.debug("WebView | init | url: "+url);
        if (url==__blankUrl) {core.debug("WebView | NO URL ASSIGNED... EXITING..."); return false;}
        pif.setExternalAppActive(true);
        pif.screenSaver.stopScreenSaver();
        myWebView.url=url;
        if (paxusAppName!="" && pif.paxus) pif.paxus.startContentLog(paxusAppName);
        return true;
    }
    function closeWebView(){
        core.debug("WebView | closeWebView");
        pif.setExternalAppActive(false);
        pif.screenSaver.startScreenSaver();
        noWebConnectivity = 0;
        url = __blankUrl;
        checkWebLoader.sourceComponent=null;
        myWebView.url = __blankUrl;
        if (paxusAppName!="" && pif.paxus) { paxusAppName=""; pif.paxus.stopContentLog();}
    }
    function setparams(mid){
        dataController.getMediaByMid([mid],midInfoRecieved)
    }
    function resizePlayerVideo(){
         pif.vod.setPIPByArray(pipArray);
    }
    function midInfoRecieved(dmodel){
        var params=new Object();
        params["cid"]=0
        params["webPipArray"]=pipArray
        params["mediaType"]=dmodel.getValue(0,'media_type')
        params["amid"]=dmodel.getValue(0,'mid')
        params["mid"]=dmodel.getValue(0,'mid')
        params["duration"]=dmodel.getValue(0,'duration')
        pif.vod.play(dmodel.getValue(0,'mid'),params,false);


    }
    onNoWebConnectivityChanged: {
        core.debug("WebView | onNoWebConnectivityChanged | noWebConnectivity: "+noWebConnectivity);
        if (noWebConnectivity == 1) {
            core.debug("WebView | Checking for web connectivity to headend");
            checkWebLoader.sourceComponent=checkWebComponent;
            checkWebLoader.item.url = "http://172.17.0.24/index.html";		// to check connectivity to headend.
        } else if (noWebConnectivity == 2) {
            core.debug("WebView | No web connectivity to headend detected... closing Web App");
            closeWebView();
            closeWebViewGUI("");
        }
    }
    Rectangle{
        id:webViewRect; color:"#100000"; width: webView.width; height: webView.height; focus:true;
        property alias myWebView:myWebView
        Loader{ id: checkWebLoader; }
        WebView {
            id: myWebView; focus:true; settings.pluginsEnabled: true;
            preferredWidth: parent.width; preferredHeight: parent.height;
            url: core.pc?__blankUrl:'';
            javaScriptWindowObjects: QtObject {
                WebView.windowObjectName: "qml"
                function close_asxi() {
                    core.debug("WebView | CLOSE WINDOW REQUESTED FROM VOYAGER APP");
                    closeWebView();
                    closeWebViewGUI("");
                }
                function closeWindow() {
                    core.debug("WebView | CLOSE WINDOW REQUESTED FROM APP");
                    closeWebView();
                    closeWebViewGUI("");
                }

                //video
                function getVideoColorKey() {
                    return pif.getPIPColorKey();
                }
                function playWebVideo(mid, x, y, width, height) {
                     pipArray=[x,y,width,height,true]
                    setparams(mid)

                }
                function resizeVideo(x, y, width, height) {
                    pipArray=[x,y,width,height,true]
                    resizePlayerVideo()
                }
                function togglePlayPauseVideo(){
                    if(pif.vod.getMediaState()==pif.vod.cMEDIA_PLAY){
                        pif.vod.pause();
                    }else if(pif.vod.getMediaState()==pif.vod.cMEDIA_PAUSE){
                        pif.vod.resume();
                    }
                }
                function pauseVideo(){
                    pif.vod.pause();
                }
                function resumeVideo(){
                     pif.vod.resume();
                }
                function fastForwardVideo(){
                    pif.vod.forward(false)
                }
                function rewindVideo(){
                    pif.vod.rewind(false)
                }
                function stopVideo(){
                    pif.vod.stop();
                }

                function printToConsole(log){
                    core.info("WebView | Microsite Log : "+log);
                }


            }
            onUrlChanged: { core.debug("WebView | onUrlChanged | url: "+url); }
            onLoadFinished: { core.debug("WebView | onLoadFinished | noWebConnectivity: "+noWebConnectivity+" url: "+url); }
            onLoadFailed: {
                core.debug("WebView | onLoadFailed | noWebConnectivity: "+noWebConnectivity+" url: "+url);
                if (noWebConnectivity == 0) noWebConnectivity = 1;		// prevent any further failures to interrupt connectivity check
            }
        }
    }
    Component{
        id: checkWebComponent;
        WebView {
            id: checkWebConnect; preferredWidth: 0; preferredHeight: 0; settings.pluginsEnabled: true;
            onUrlChanged: {core.debug("WebView | SECONDARY WEBVIEW onUrlChanged | url: "+url);}
            onLoadFinished: {
                core.debug("WebView | SECONDARY WEBVIEW onLoadFinished | url: "+url);
                if (noWebConnectivity == 1) { noWebConnectivity = 0; checkWebConnect.url = __blankUrl;}
            }
            onLoadFailed: {
                core.debug("WebView | SECONDARY WEBVIEW onLoadFailed | url: "+url);
                if (noWebConnectivity == 1) {noWebConnectivity = 2; checkWebConnect.url = __blankUrl;}
            }
        }
    }

    Connections{
        target: pif; onInteractiveOverrideChanged: {if(interactiveOverride && (url!=__blankUrl)){core.debug("WebView | CLOSE WINDOW REQUESTED ON INTERACTIVE OVERRIDE");closeWebView();closeWebViewGUI("");}}
    }
}
