import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item{
    id:evid
    property bool fireSignal:true;
    property variant vidClient;

    signal lightStateChanged(string name,string state);
    signal windowShadeStateChanged(string name,string state);
    signal cameraZoomChanged(string name,variant level);
    signal audioStateChanged(string name,variant level);
    signal monitorPowerChanged(string name,string state);
    signal genericStateChanged(string name,variant value);
    signal evidJsonLoaded();

    Connections{
        target: vidClient?vidClient:null
        onMessageReceived: {__vidMessageReceived(command, value);}
    }

    //Public functions
    function getVidByModel(type,vidModel){
        core.info("Evid.qml | getVidByModel type:"+type);
        vidModel.clear();
        for(var x in Store.arr){
            if(Store.arr[x]['type']==type){
                vidModel.append({name:Store.arr[x]['name'], type: Store.arr[x]['type'], min: Store.arr[x]['min'], max: Store.arr[x]['max'], step: Store.arr[x]['step'], templateId:Store.arr[x]['templateId'],validInputs:Store.arr[x]['validInputs'], vid:Store.arr[x]['vid']});
            }
        }
    }

    function getVidData(name,fieldName){
        core.info("Evid.qml | getVidData name:"+name + " fieldName: "+ fieldName);
        if(Store.arr[name] && Store.arr[name][fieldName]) return Store.arr[name][fieldName];
        else { core.info("Evid.qml | getVidData | >> NOT FOUND  name:"+name + " fieldName: "+ fieldName);return -1;}
    }

    function setVidData(name, value){
        core.info("Evid.qml | setVidData name:"+name + " value: "+ value);
        if(Store.arr[name] && Store.arr[name]["vid"]){
            if(Store.arr[name]["type"]=="audio_control" && (value==1 || value==-1)){
                    setVidDataByStep(name,value);
              }else {
			vidClient.send(Store.arr[name]["vid"],value);
              }
		}else { core.info("Evid.qml | setVidData | >> NOT FOUND  name:"+name + " | value: "+ value);return -1;}
    }
    function setVidDataByStep(name,step){ // FE will send +1, -1, step is in json o no need to send
        core.info("Evid.qml | setVidDataByStep by step called | name: "+name+" | Step: "+step);
        core.debug("Evid.qml | setVidDataByStep | Min: "+Store.arr[name]["min"]+" | Max: "+Store.arr[name]["max"]+" | Step: "+parseInt(Store.arr[name]["step"],10));
        if(Store.arr[name] && Store.arr[name]["vid"]  && Store.arr[name]["min"]!=-1 && Store.arr[name]["max"]!=-1){
            step=parseInt(step,10);
            var stepValue=parseInt(Store.arr[name]["value"],10)+(parseInt(Store.arr[name]["step"],10)*step);
            stepValue = stepValue<Store.arr[name]["min"]?Store.arr[name]["min"]:stepValue>Store.arr[name]["max"]?Store.arr[name]["max"]:stepValue;
            vidClient.send(Store.arr[name]["vid"],stepValue);
        }else {
            core.info("Evid.qml | setVidDataByStep | Not Found | Min: "+Store.arr[name]["min"]+" | Max: "+Store.arr[name]["min"]);
        }
        return true;
    }

    function setRowMapping(rowMap){
        //eg : Store.replaceSeatRow=[];Store.replaceSeatRow['User3']=[];Store.replaceSeatRow['User3']['1b']='s10'
        core.info("Evid.qml | setRowMapping()");
        Store.replaceSeatRow=rowMap;
        Store.arr = [];
        if (core.pc  || pif.getSimulationData().Evid){vidClient.setRowMapping(rowMap);}
        __loadVidJson();
    }

    //Private functions
    function __loadVidJson(){
        core.info("Evid.qml | __loadVidJson()");
        var getParams = '../../config/evid.json';
        jsonLoader.getLocalData(getParams);
    }

    function __readJsonObject(errCode,responseObj){
        core.info("Evid.qml | __readJsonObject() called errCode: " + errCode);
        var seatClass;
        if(errCode==0 && responseObj){
            seatClass = pif.getCabinClassName();
            core.info("Evid.qml | Reading vids of class: " + seatClass);
            for(var x in responseObj[seatClass]){
                Store.arr[responseObj[seatClass][x].name] = new Array();
                Store.arr[responseObj[seatClass][x].name]["name"] = responseObj[seatClass][x].name;

                var tmpVid=responseObj[seatClass][x].vid;
                if(tmpVid.indexOf("*SEAT_ROW*")>=0){
                    if(Store.replaceSeatRow && Store.replaceSeatRow[pif.getCabinClassName()] && Store.replaceSeatRow[pif.getCabinClassName()][pif.getSeatNumber()]){
                        tmpVid=tmpVid.replace("S*SEAT_ROW*",Store.replaceSeatRow[pif.getCabinClassName()][pif.getSeatNumber()]);
                    }else {
                        tmpVid=tmpVid.replace("*SEAT_ROW*",pif.getSeatRow());
                    }
                }

                Store.arr[responseObj[seatClass][x].name]["vid"] = tmpVid
                core.info("Evid.qml | __readJsonObject |  name: "+responseObj[seatClass][x].name+" | vid: " + tmpVid);
                Store.arr[responseObj[seatClass][x].name]["type"] = responseObj[seatClass][x].type;
                Store.arr[responseObj[seatClass][x].name]["templateId"] = responseObj[seatClass][x].templateId;
                if(responseObj[seatClass][x].validInputs){
                    Store.arr[responseObj[seatClass][x].name]["validInputs"] = responseObj[seatClass][x].validInputs.toString();
                }
                else{Store.arr[responseObj[seatClass][x].name]["validInputs"] = -1;}
                if(responseObj[seatClass][x].min)Store.arr[responseObj[seatClass][x].name]["min"] = responseObj[seatClass][x].min;
                else Store.arr[responseObj[seatClass][x].name]["min"] = -1;
                if(responseObj[seatClass][x].max)Store.arr[responseObj[seatClass][x].name]["max"] = responseObj[seatClass][x].max;
                else Store.arr[responseObj[seatClass][x].name]["max"] = -1;
                if(responseObj[seatClass][x].step)Store.arr[responseObj[seatClass][x].name]["step"] = responseObj[seatClass][x].step;
                else Store.arr[responseObj[seatClass][x].name]["step"] = -1;
                if(responseObj[seatClass][x].defaultVal)Store.arr[responseObj[seatClass][x].name]["defaultVal"] = responseObj[seatClass][x].defaultVal;
                else Store.arr[responseObj[seatClass][x].name]["defaultVal"] = -1;
                if(vidClient.connected)vidClient.requestValue(Store.arr[responseObj[seatClass][x].name]["vid"]);
            }
            evidJsonLoaded();
        }
    }

    function __vidMessageReceived(command, value){
        for(var x in Store.arr){
            if(Store.arr[x]['vid'] == command){
                core.info("Evid.qml | __vidMessageReceived() command: " + command + " value:" + value);
                Store.arr[x]['value'] = value;
                core.info("Evid.qml | __vidMessageReceived() MATCHED: " + command + " value:" + value+" | type: "+Store.arr[x]['type']);
                if(fireSignal){
                    if(Store.arr[x]['type'] == "light_control")lightStateChanged(Store.arr[x]['name'],value);
                    else if(Store.arr[x]['type'] == "window_shade_control")windowShadeStateChanged(Store.arr[x]['name'],value);
                    else if(Store.arr[x]['type'] == "camera")cameraZoomChanged(Store.arr[x]['name'],value);
                    else if(Store.arr[x]['type'] == "audio_control")audioStateChanged(Store.arr[x]['name'],value);
                    else if(Store.arr[x]['type'] =="monitor_power")monitorPowerChanged(Store.arr[x]['name'],value);
                    else if(Store.arr[x]['type'] =="generic_type")genericStateChanged(Store.arr[x]['name'],value);
                }
            }
        }
    }  

    DataLoader {
        id: jsonLoader;
        onSigLocalDataReady:{__readJsonObject(errCode,responseObj);}
    }
    Component.onCompleted: {
        core.info("Evid.qml | Evid FW component load complete ");
        if(!(pif.vip && pif.vip.evidClient))vidClient = Qt.createQmlObject('import QtQuick 1.1; EvidInterface{}',evid);
        else vidClient = pif.vip.evidClient
        core.info ("Evid.qml |  evidClient Reference: "+vidClient);
        __loadVidJson();Store.arr = [];Store.replaceSeatRow=[];
    }
}
