import QtQuick 1.1
import Panasonic.Pif 1.0

SharedModel{
    id: sm

    property string apiName;
    property string languageIso;
    property int seatbackVolume;
    property int headsetVolume;
    property int seatbackBrightness;
    property int handsetBrightness;
    property bool backlightEnabled:true;
    property int mid;
    property int aggregateMid;
    property int soundtrackLid;
    property int soundtrackType;
    property int subtitleLid;
    property int subtitleType;
    property int elapsedTime;
    property string aspectRatio; // possible values: "4x3","16x9","Fullscreen","Not Adjustable"
    property string mediaRepeat;  // possible values: "none","current","all"
    property bool mediaShuffle:false;
    property string mediaType;
    property string mediaPlayRate;
    property string mediaPlayerState; // possible values: "play", "pause", "resume", "stop", "fastforward", "rewind"
    property variant launchAppId:[]; // variant type should have default initialisation as []
    property bool iPodConnected:false;
    property bool usbConnected:false;
    property bool showKeyboard;
    property string keyboardData;
    property bool usbAgreement;
    property bool iPodAgreement;
    property variant playlists:{"aod":{},"vod":{},"kidsaod":{},"kidsvod":{}};
    property variant apiParameters:[];
    property bool muteVolume:false;
    property bool muteHeadsetVolume:false;
    property string blueRayStatus: "stop";
    property string blueRayMediumType: "NONE";  // possible medium type can be "BD", "DVD", "CD" and "NONE".
    property variant resumeTimeMap: [];
    property int noOfDimmableWindows:0;

    onApiNameChanged: {if(!privateObj.ignorePropertyChange && apiName!="") dataReceived(apiName);}
    onSeatbackVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("seatbackVolume");}
    onHeadsetVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("headsetVolume");}
    onSeatbackBrightnessChanged: {if(!privateObj.ignorePropertyChange) dataReceived("seatbackBrightness");}
    onAspectRatioChanged: { if(!privateObj.ignorePropertyChange) dataReceived("aspectRatioAck");}
    onMediaShuffleChanged: { if(!privateObj.ignorePropertyChange) dataReceived("mediaShuffleAck");}
    onBacklightEnabledChanged: { if(!privateObj.ignorePropertyChange) dataReceived("backlightEnabled");}
    onIPodConnectedChanged: { if(!privateObj.ignorePropertyChange) dataReceived("iPodConnected");}
    onUsbConnectedChanged: { if(!privateObj.ignorePropertyChange) dataReceived("usbConnected");}
    onShowKeyboardChanged: { if(!privateObj.ignorePropertyChange) dataReceived("showKeyboard");}
    onKeyboardDataChanged: { if(!privateObj.ignorePropertyChange) dataReceived("keyboardData");}
    onUsbAgreementChanged: { if(!privateObj.ignorePropertyChange) dataReceived("usbAgreement");}
    onIPodAgreementChanged: { if(!privateObj.ignorePropertyChange) dataReceived("iPodAgreement");}
    onMuteVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("muteVolume");}
    onMuteHeadsetVolumeChanged: {if(!privateObj.ignorePropertyChange) dataReceived("muteHeadsetVolume");}
    onPlaylistsChanged: {if(!privateObj.ignorePropertyChange) dataReceived("playlists");}

    function dataReceived(apiName){ core.debug("SharedModelClient.qml | dataReceived(api) called: "+apiName);}

    function sendApiData(api,params){       // Sending bunch of property change to the shared model
        core.debug("SharedModelClient.qml | Control in sendApiData("+api+",params)")
        privateObj.ignorePropertyChange = true;
        sm.apiName = "";
        apiParameters=params;
        sm.apiName = api;
        privateObj.ignorePropertyChange = false;
    }

    function sendPropertyData(propertyName,propertyValue){      // Sending single property change to the shared model
        privateObj.ignorePropertyChange=true;
        core.debug("SharedModelClient.qml | sendPropertyData("+propertyName+","+propertyValue+")");
        sm.setPropertyValue(propertyName, propertyValue);
        privateObj.ignorePropertyChange=false;
    }

    Component.onCompleted: { privateObj.properties = sm.userProperties(); core.info("SharedModelClient.qml | SHARED MODEL CLIENT LOAD COMPLETE")}

    QtObject{id:privateObj; property bool ignorePropertyChange: false; property variant properties:[];}
}
