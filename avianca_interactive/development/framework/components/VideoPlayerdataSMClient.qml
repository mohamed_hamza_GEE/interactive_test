import QtQuick 1.1

Item {
    id: videoPlayerdataSMClient
    property int aMid:0;
    property string aMidTitle:'';
    property int mid:0;
    property string midTitle:'';
    property variant __ref:pif.vkarmaToSeat;
    property string duration:"";
    property bool isTrailer:false;
    property int subtitleLid:0;
    property int soundtrackLid:0;
    property string aspectRatio:"";
    
    Connections {
        target: __ref;
        onSigDataReceivedFromSeat:{
            if(api=="frameworkUpdateVideoPlayerdata"){
                eval(params.functionName+"(params)");
            }else if(api=="videoStopAck"){
                aMidTitle=''; aMid=0;
                midTitle=''; mid=0;
            }
        }
    }

    function setReference(ref){__ref=ref; return true;}

    function midDataReady (params) {
        midTitle = (params.midTitle)?params.midTitle:(midTitle!='')?midTitle:'';
        mid = (params.mid)?params.mid:(mid!='')?mid:0;
        duration = (params.duration)?params.duration:(duration!="")?duration:"";
        isTrailer = (params.isTrailer)?params.isTrailer:(isTrailer==true)?true:false;
        subtitleLid = (params.subtitleLid)?params.subtitleLid:(subtitleLid!=0)?subtitleLid:0;
        soundtrackLid = (params.soundtrackLid)?params.soundtrackLid:(soundtrackLid!=0)?soundtrackLid:0;
        aspectRatio = (params.aspectRatio)?params.aspectRatio:(aspectRatio!="")?aspectRatio:"";
    }

    function aMidDataReady (params) {
        aMidTitle = (params.aMidTitle)?params.aMidTitle:(aMidTitle!='')?aMidTitle:'';
        aMid = (params.aMid)?params.aMid:(aMid!=0)?aMid:0;
    }
}
