import QtQuick 1.0
import "../blank.js" as SystemData

Item {
	property string defaultSrc:"";

    function __setValue(temp,filePathName,fieldName,value){
        if(temp[filePathName]){
            if(temp[filePathName][fieldName]){
                temp[filePathName][fieldName] = value;
            }
            else {temp[filePathName][fieldName] = value;}
        }
        else {
            temp[filePathName] = new Array();
            temp[filePathName][fieldName] = value;
        }
    }

    function setDefault(source){defaultSrc = source;}

	function setImageInfo(filePathName,fieldName,value,source){
		var temp;
        core.info("ImageInfo.qml | setImageInfo called | filePathName: "+filePathName+" fieldName: "+fieldName+" value: "+value+" source: "+source);
		if(source){
            temp = SystemData.imageInfo[source];
            if(temp){__setValue(temp,filePathName,fieldName,value);}
			else {
                SystemData.imageInfo[source] = new Array();
                SystemData.imageInfo[source][filePathName] = new Array();
                SystemData.imageInfo[source][filePathName][fieldName] = value;
			}
		}
		else {
            temp = SystemData.imageInfo[defaultSrc];
            if(temp){__setValue(temp,filePathName,fieldName,value);}
			else {
                SystemData.imageInfo[defaultSrc] = new Array();
                SystemData.imageInfo[defaultSrc][filePathName] = new Array();
                SystemData.imageInfo[defaultSrc][filePathName][fieldName] = value;
			}
		}
		return true;
	}

    function getImageInfo(filePathName,fieldName,source){
		var temp;
        core.info("ImageInfo.qml | getImageInfo called | filePathName: "+filePathName+" fieldName: "+fieldName+" source: "+source);
        if(SystemData.imageInfo){
            if(source){temp = SystemData.imageInfo[source];}
            else {temp = SystemData.imageInfo[defaultSrc];}
			if(!temp) return "false";
			if(temp[filePathName]){
                if(temp[filePathName][fieldName]){return temp[filePathName][fieldName];}
				else return "false";
			}
			else return "false";
		}
		else return "false";
	}

    function clearAll(){core.info("ImageInfo.qml | clearAll called");SystemData.imageInfo = new Array();}
}
