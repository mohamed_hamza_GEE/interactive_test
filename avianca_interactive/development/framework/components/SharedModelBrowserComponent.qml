import QtQuick 1.1
import Panasonic.Pif 1.0

BrowserComponent{

    id: browserComponent

    property string apiName;
    property string languageIso;
    property int seatbackVolume;
    property int seatbackBrightness;
    property int handsetBrightness;
    property bool backlightEnabled:true;
    property int mid;
    property int aggregateMid;
    property int soundtrackLid;
    property int soundtrackType;
    property int subtitleLid;
    property int subtitleType;
    property int elapsedTime;
    property string aspectRatio; // possible values: "4x3","16x9","Fullscreen","Not Adjustable"
    property string mediaRepeat:"none";  // possible values: "none","current","all"
    property bool mediaShuffle:false;
    property string mediaType;
    property string mediaPlayRate;
    property string mediaPlayerState : "stop"; // possible values: "play", "pause", "resume", "stop", "fastforward", "rewind"
    property variant launchAppId:[]; // variant type should have default initialisation as []
    property bool iPodConnected:false;
    property bool usbConnected:false;
    property bool showKeyboard;
    property string keyboardData;
    property bool usbAgreement;
    property bool iPodAgreement;
    property variant playlists:[];
    property variant apiParameters:[];

    signal sigDataReceivedFromSeat(string api, variant params);

    function __updateSharedModelProperty(propertyName, propertyValue){
        switch(propertyName){
            case "aspectRatio":{aspectRatio=propertyValue;break;}
            case "mediaShuffle":{mediaShuffle=!mediaShuffle;break;}
            case "seatbackVolume":{seatbackVolume=propertyValue;break;}
            case "seatbackBrightness":{seatbackBrightness=propertyValue;break;}
            case "backlightEnabled":{backlightEnabled=!backlightEnabled;break;}
            case "keyboardData":{keyboardData=propertyValue;break;}
            case "showKeyboard":{showKeyboard=propertyValue;break;}
            case "usbAgreement":{usbAgreement=propertyValue;break;}
            case "iPodAgreement":{iPodAgreement=propertyValue;break;}
            case "languageIso":{languageIso=propertyValue;break;}
            case "handsetBrightness":{handsetBrightness=propertyValue;break;}
            case "mid":{mid=propertyValue;break;}
            case "aggregateMid":{aggregateMid=propertyValue;break;}
            case "soundtrackLid":{soundtrackLid=propertyValue;break;}
            case "soundtrackType":{soundtrackType=propertyValue;break;}
            case "subtitleLid":{subtitleLid=propertyValue;break;}
            case "subtitleType":{subtitleType=propertyValue;break;}
            case "elapsedTime":{elapsedTime=parseInt(propertyValue,10);break;}
            case "mediaRepeat":{mediaRepeat=propertyValue;break;}
            case "mediaPlayRate":{mediaPlayRate=propertyValue;break;}
            case "mediaPlayerState":{mediaPlayerState=propertyValue;break;}
            case "launchAppId":{launchAppId=propertyValue;break;}
            case "iPodConnected":{iPodConnected=propertyValue;break;}
            case "usbConnected":{usbConnected=propertyValue;break;}
        }
    }

    function __iterateAssocArray(params){var logger=[];for(var key in params){ logger.push(" params."+key+": "+params[key])} return logger;}

    function dataReceived(apiName,params){
        core.debug("SharedModelBrowserComponent.qml | dataReceived(api) called: "+apiName);
        for(var key in params){
            core.debug("key is " + key + " params[key] is " + params[key]);
            __updateSharedModelProperty(key, params[key]);
        }
        if(apiName=="null"){
            for(var key in params){
                if(key=="seatbackVolume"){
                    params = {seatbackVolume:seatbackVolume};
                    apiName="seatbackVolume";
                    core.debug("Received message from seat | seatbackVolume : " + seatbackVolume);
                }
                else if(key=="seatbackBrightness"){
                    params = {seatbackBrightness:seatbackBrightness};
                    apiName="seatbackBrightness";
                    core.debug("Received message from seat | seatbackBrightness : " + seatbackBrightness);
                }
                else if(key=="backlightEnabled"){
                    params = {backlightEnabled:backlightEnabled};
                    apiName="backlightEnabled";
                    core.debug("Received message from seat | backlightEnabled : " + backlightEnabled);
                }
                else if(key=="iPodConnected"){
                    params = {iPodConnected:iPodConnected};
                    apiName="iPodConnected";
                    core.debug("Received message from seat | iPodConnected : " + iPodConnected);
                }
                else if(key=="usbConnected"){
                    params = {usbConnected:usbConnected};
                    apiName="usbConnected";
                    core.debug("Received message from seat | usbConnected : " + usbConnected);
                }
                else if(key=="showKeyboard"){
                    params = {showKeyboard:showKeyboard};
                    apiName="showKeyboard";
                    core.debug("Received message from seat | showKeyboard : " + showKeyboard);
                }
                else if(key=="keyboardData"){
                    params = {keyboardData:keyboardData};
                    apiName="keyboardData";
                    core.debug("Received message from seat | keyboardData : " + keyboardData);
                }
                else if(key=="usbAgreement"){
                    params = {usbAgreement:usbAgreement};
                    apiName="usbAgreement";
                    core.debug("Received message from seat | usbAgreement : " + usbAgreement);
                }
                else if(key=="iPodAgreement"){
                    params = {iPodAgreement:iPodAgreement};
                    apiName="iPodAgreement";
                    core.debug("Received message from seat | iPodAgreement : " + iPodAgreement);
                }
                else if(key=="playlists"){
                    params = {playlists:playlists};
                    apiName="playlists";
                    core.debug("Received message from seat | playlists : " + playlists);
                }
                sigDataReceivedFromSeat(apiName,params);  return true;
            }
        }else if(apiName=="userDefinedSeatrating"){
            core.info("SharedModelBrowserComponent.qml | userDefinedSeatrating | Received: seatRating: "+parseInt(params.seatRating,10));
            pif.__seatRatingChangeAck(parseInt(params.seatRating,10));
        }
        else if(apiName=="playVodAck" || apiName=="playTrailerAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | mid : "+ mid + ", aggregateMid : " + aggregateMid + ", soundtrackLid : " + soundtrackLid + ", soundtrackType : " + soundtrackType + ", subtitleLid : " + subtitleLid + ", subtitleType : " + subtitleType + ", elapsedTime : " + elapsedTime);
            mediaType="video";
            mediaPlayerState="play";
        } else if(apiName=="aspectRatioAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | aspectRatio : " + aspectRatio);
        } else if(apiName=="videoSoundtrackAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " |  soundtrackLid : " + soundtrackLid + ", soundtrackType : " + soundtrackType);
        } else if(apiName=="videoSubtitleAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | subtitleLid : "+ subtitleLid + ", subtitleType : " + subtitleType);
        } else if(apiName=="setMediaPositionAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | elapsedTime : " + elapsedTime);
        } else if(apiName=="mediaPauseAck") {
            mediaPlayerState="pause";
        } else if(apiName=="mediaResumeAck"){
            mediaPlayerState="resume";
        } else if(apiName=="videoStopAck"){
            mediaPlayerState="stop";
        }else if(apiName=="audioStopAck"){
            mediaPlayerState="stop";
        }else if(apiName=="closeAppAck"){
            core.debug("Received message from seat | apiName: "+ apiName);
        } else if(apiName=="mediaFastForwardAck"){
            mediaPlayerState="fastforward";
            core.debug("Received message from seat | apiName: "+ apiName + " | mediaPlayRate : " + mediaPlayRate);
        }else if(apiName=="mediaRewindAck"){
            mediaPlayerState="rewind";
            core.debug("Received message from seat | apiName: "+ apiName + " | mediaPlayRate : " + mediaPlayRate);
        } else if(apiName=="playAodAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | mid : "+ mid + ", aggregateMid : " + aggregateMid + ", mediaRepeat : " + mediaRepeat);
            mediaType="audio";
            mediaPlayerState="play";
        } else if(apiName=="mediaRepeatAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | mediaRepeat : "+ mediaRepeat);
        } else if(apiName=="mediaShuffleAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | mediaShuffle : "+ mediaShuffle);
        } else if(apiName=="changeLanguageAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | languageIso : "+ languageIso);
        } else if(apiName=="launchAppAck"){
            core.debug("Received message from seat | apiName: "+ apiName + " | launchAppId : "+ launchAppId);
        } else if(apiName=="addMidToPlaylistAck"){
            var tmp;
            core.debug("Received message from seat | apiName: "+ apiName + " params.playlists is  " + params.playlists);
        	for(var key in params){
            	core.debug("key is " + key + " params[key] is " + params[key]);
		 tmp = params[key];
		}
        var tmpPlaylists=String(params.playlists);
        playlists = tmpPlaylists.split(",");
	    core.debug(" playlists is  " + playlists + " playlists.length is  " + playlists.length);
		for(var i=0; i< playlists.length;i++){
			playlists[i] = parseInt(playlists[i],10);
		}
        } else if(apiName=="removeMidFromPlayistAck"){
            var tmp;
            core.debug("Received message from seat | apiName: "+ apiName + " params.playlists is  " + params.playlists);
	if(params.playlists){				
        	for(var key in params){
            	core.debug("key is " + key + " params[key] is " + params[key]);
		 tmp = params[key];
		}
        var tmpPlaylists=String(params.playlists);
        playlists = tmpPlaylists.split(",");
	    core.debug(" playlists is  " + playlists + " playlists.length is  " + playlists.length);
		for(var i=0; i< playlists.length;i++){
			playlists[i] = parseInt(playlists[i],10);
		}}else {playlists=[];}
        } else if(apiName=="removeAllFromPlaylistAck"){
            core.debug("Received message from seat | apiName: "+ apiName);
        }else if(apiName.indexOf("userDefined")!=-1 || apiName.indexOf("framework")!=-1){
            core.debug("Received message from seat | apiName: "+ apiName); sigDataReceivedFromSeat(apiName,params);
            if(apiName=="userDefinedInteractiveOverride"){
                core.debug("Received message from seat | userDefinedInteractiveOverride | interactiveOverrideState: "+params.interactiveOverrideState);
                pif.__interactiveOverrideChangeAck(params.interactiveOverrideState);
            }
            return true;
        } else if(apiName=="interactiveReset"){
            core.debug("Received message from seat | apiName: "+apiName);
        }else {
            core.debug("Received message from seat | apiName: "+apiName);
        }
        sigDataReceivedFromSeat(apiName,params);  return true;
    }

    function sendApiData(api,params){
        core.debug("SharedModelBrowserComponent.qml | sendApiData api: " + api + "|" + __iterateAssocArray(params));
        updateBrowserApi(api,params);
    }

    function sendPropertyData(properties){      // Sending single property change to the shared model
        core.debug("SharedModelBrowserComponent.qml | sendPropertyData()  properties is  " + properties);
        updateBrowserProperty(properties);
    }

    Component.onCompleted: { core.info("SharedModelBrowserComponent.qml | SHARED MODEL BROWSER COMPONENT LOAD COMPLETE")}
}
