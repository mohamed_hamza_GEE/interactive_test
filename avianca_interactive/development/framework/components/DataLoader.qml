import QtQuick 1.1
import Panasonic.Pif 1.0

RemoteDataLoader {
    id: dataLoader
    property string __source:'';
    property int __delayTimeout:1;
    property int __pid

    //Signal
    signal sigLocalDataReady(int errCode, string url, variant responseObj,int requestId, int pid);

    function getLocalData(source,requestId){
        __requestId=requestId?requestId:0;
        __source=source;
        delayTimer.start();
    }

    function __readData(){
        __source=__source+"?loadid="+(++__makeUrlUnique);
        core.info("DataLoader | __readData | source: "+__source);
        delayTimer.stop();
        fileRequest.source=__source;
    }

    function getLocalDataWithoutDelay(source,requestId,pid){
        __requestId=requestId?requestId:0;
        __source=source;
        if(pid!=undefined)
        __pid=pid
        else __pid=-1
        __readData();
    }

    Configuration{
        id: fileRequest
        onStatusChanged:{
            if(status==Pif.Ready && fileRequest.source!=''){
                core.info("DataLoader | Configuration onStatusChanged | Data Ready");
                sigLocalDataReady(cErrorNoError, source, value,__requestId,__pid);
            }else if(status==Pif.Error && fileRequest.source!=''){
                core.info("DataLoader | Configuration onStatusChanged | ERROR reading file: "+source);
                sigLocalDataReady(status,source,'',__requestId,__pid);
            }
        }
    }

    Timer { id:delayTimer;repeat:true; interval: __delayTimeout*1000; onTriggered: __readData();}
}
