import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    id: mediaModel
    property bool preloadInProgress: false;
    property variant mediaDb;
    property variant airportDb;
    property variant connectDb;
    property variant helpDb;
    property variant subtitlelModel;
    property double __prevTime:0;
    property variant mediaDbSqliteDatabaseRef;
	
	Connections{
        id:mediaSqliteRef
        target: mediaDbSqliteDatabaseRef!=undefined?mediaDbSqliteDatabaseRef:null;
        onStatusChanged: {
            core.info("MediaModels.qml | mediaDbSqliteDatabaseRef onStatusChanged | Store.cMediaDbReference = "+mediaDbSqliteDatabaseRef+" Status : "+mediaDbSqliteDatabaseRef.status);
            if(mediaDbSqliteDatabaseRef.status==Pif.Ready){
				core.info(" MediaModels.qml | mediaDbSqliteDatabaseRef onStatusChanged | Pif is Ready: "+Pif.Ready+" process execution");
				core.dataController.__loadNonRouteData(); 
			}
        }
    }

    // Public
    function getMediaModel (apiType,modelInstance) {return Store.ModelArr[apiType][modelInstance][Store.cSqlModelIndex];}
    function getListMediaModel (apiType,modelInstance) {return Store.ModelArr[apiType][modelInstance][Store.cListModelIndex];}
    function getCategoryModel (modelInstance) {return Store.ModelArr[0][modelInstance][Store.cSqlModelIndex];}

    function refreshModelsByIntLanguage(dbModel,callback,params){
        var sqlquery = '';
        for(var index=0;index<dbModel.length;index++){
            if (dbModel[index] && dbModel[index].query) sqlquery = dbModel[index].query; else continue;   // not an sqlListModel
            var prevlang = "lid="+dbModel[index].languageId;
            var reg = new RegExp(prevlang,'g');
            sqlquery = sqlquery.replace(reg,"lid="+core.settings.languageID);
            if(params && params['previousIso'] && params['newIso']){sqlquery = sqlquery.replace(params['previousIso'],params['newIso']);}
            __updateModel(dbModel[index],dbModel[index].apiType,dbModel[index].instanceId,sqlquery,(callback&&callback[index]?callback[index]:''));
        }
        delete sqlquery;delete index;delete prevlang;delete reg;
   }

    function destroyModel(modelType,modelInstance){
    core.debug("Media | destroyModel | modelType: "+modelType+" | modelInstance: "+modelInstance );
        var apiType=(modelType=="category")?0:(modelType=="media")?1:modelType;
        if (Store.ModelArr[apiType] && Store.ModelArr[apiType][modelInstance]) {
            if(Store.ModelArr[apiType][modelInstance][Store.cSqlModelIndex]){
                Store.ModelArr[apiType][modelInstance][Store.cSqlModelIndex].destroy();
                delete Store.ModelArr[apiType][modelInstance][Store.cSqlModelIndex];
                delete Store.ModelArr[apiType][modelInstance];
            }
        }
        delete apiType;
    }
    function destroyModelsCloseFlight(){
        for(var apiType in Store.ModelArr){
            for(var modelInstance in Store.ModelArr[apiType]){
                for(var cc in Store.ModelArr[apiType][modelInstance]){
                    destroyModel(apiType,modelInstance);
                }
            }
        }
        delete apiType;delete modelInstance;delete cc
    }
    function destroyAllMediaModels(){
        core.info("Media | destroyAllMediaModels");
        for(var apiType in Store.ModelArr){
            if(apiType!=0){ //0 is categories
                for(var modelInstance in Store.ModelArr[apiType]){
                    for(var cc in Store.ModelArr[apiType][modelInstance]){
                        if(!(modelInstance==120 || modelInstance==130)){ //skip languages
                            destroyModel(apiType,modelInstance);
                        }
                    }
                }
            }
        }
        delete apiType;delete modelInstance;delete cc
    }
    // Private
    function __modelUpdate (apiType,modelInstance,sqlquery,callback,queryMode,dbIndex) {	//[queryMode],[databaseId]
        if (!Store.ModelArr[apiType]) Store.ModelArr[apiType] = new Array();
        var mediaArrPtr = Store.ModelArr[apiType];
        if (!mediaArrPtr[modelInstance]) {
            mediaArrPtr[modelInstance] = [];
            mediaArrPtr = mediaArrPtr[modelInstance];
            var dbModel = sqlModelComp.createObject(mediaModel,{});
            mediaArrPtr[Store.cSqlModelIndex] = dbModel;
        } else {
            if(Store.DestroyModels){
                mediaArrPtr = mediaArrPtr[modelInstance];
                mediaArrPtr[Store.cSqlModelIndex].destroy();
                delete mediaArrPtr[Store.cSqlModelIndex];
                mediaArrPtr = Store.ModelArr[apiType];
                mediaArrPtr[modelInstance] = [];
                mediaArrPtr = mediaArrPtr[modelInstance];
                var dbModel = sqlModelComp.createObject(mediaModel,{});
                mediaArrPtr[Store.cSqlModelIndex] = dbModel;
            }else {
                mediaArrPtr = mediaArrPtr[modelInstance];
                var dbModel = mediaArrPtr[Store.cSqlModelIndex];
            }
        }

        if (queryMode) dbModel.queryMode=queryMode;
        dbModel.database = !dbIndex?__getMediaDbReference():__getDbReference(dbIndex);

        dbModel.apiType = apiType;
        dbModel.instanceId = modelInstance;

        __updateModel(dbModel,apiType,modelInstance,sqlquery,callback);
        delete dbModel;delete mediaArrPtr;
        return true;
    }

    function __updateModel(dbModel,apiType,modelInstance,sqlquery,callback){
        var mediaArrPtr = Store.ModelArr[apiType][modelInstance]
        if (mediaArrPtr[Store.cCallback1Index]) dbModel.modelReady.disconnect(mediaArrPtr[Store.cCallback1Index]);
        if (callback) { dbModel.modelReady.connect(callback); mediaArrPtr[Store.cCallback1Index]=callback;}
        dbModel.languageId = core.settings.languageID;
        dbModel.query = sqlquery;
        core.info("__updateModel | dbModel.enabled: "+dbModel.enabled);
        if(dbModel.enabled == true){dbModel.enabled = false;}
        dbModel.enabled = true;
        core.debug("__modelUpdate | enabling done");
        delete mediaArrPtr;
        return true;
    }

    function __setSimpleModel(dbModel,apiType,modelInstance,callback){
        var mediaArrPtr = Store.ModelArr[apiType][modelInstance];
        if (!mediaArrPtr[Store.cSimpleModelIndex]){
            var sModel = sModelComp.createObject(mediaModel,{});
            mediaArrPtr[Store.cSimpleModelIndex] = sModel;
        }else {var sModel = mediaArrPtr[Store.cSimpleModelIndex];}
        if(mediaArrPtr[Store.cCallback2Index]){sModel.modelReady.disconnect(mediaArrPtr[Store.cCallback2Index]);}
        mediaArrPtr[Store.cCallback2Index]=callback;
        sModel.modelReady.connect(callback);
        sModel.clear();sModel.appendModel(dbModel);
        sModel.modelReady(sModel,apiType);
        delete mediaArrPtr;delete sModel;
        return true;
    }

    function __setLanguageModel (apiType,langArr) {
        var aptr=Store.ModelArr[apiType][1];		// instanceId = 1;
        var sm=Store.ModelArr[apiType][1][Store.cSqlModelIndex];
        if (!aptr[Store.cListModelIndex]) aptr[Store.cListModelIndex] = lModelComp.createObject(mediaModel);
        var lModel = aptr[Store.cListModelIndex];lModel.clear();
        var language_locale = '';
        if(langArr[0]){
            for(var i=0;i<langArr.length;i++){
                for(var j=0;j<sm.count;j++){
                    if(langArr[i]==sm.getValue(j,"ISO639")) {
                        if(typeof sm.getValue(j,"language_locale") != 'undefined'){
                            language_locale=sm.getValue(j,"language_locale");
                        }
                        lModel.append({
                                          "LID"          : parseInt(sm.getValue(j,"LID"),10),
                                          "nameLID"      : parseInt(sm.getValue(j,"nameLID"),10),
                                          "name"         : sm.getValue(j,"name"),
                                          "ISO639"       : sm.getValue(j,"ISO639"),
                                          "language_locale" : language_locale
                                      });
                    }
                }
            }
        }else{
            for(var j=0;j<sm.count;j++){
                if(typeof sm.getValue(j,"language_locale") != 'undefined'){
                    language_locale=sm.getValue(j,"language_locale");
                }
                lModel.append({
                                  "LID"          : parseInt(sm.getValue(j,"LID"),10),
                                  "nameLID"      : parseInt(sm.getValue(j,"nameLID"),10),
                                  "name"         : sm.getValue(j,"name"),
                                  "ISO639"       : sm.getValue(j,"ISO639"),
                                  "language_locale" : language_locale
                             });
            }
        }
        delete aptr;delete sm;delete lModel;delete language_locale;delete i;delete j;
        return true;
    }

    function __preloadModels (params) {
        core.info("Media | __preloadModels begin");
        preloadInProgress = true;
        getNextMenu(params, __preloadCallback);
        preloadInProgress = false;
        __prevTime=0;
        core.info("Media | __preloadModels end");
        return true;
    }
    function __preloadCallback (model,params) {
        if (model.count==0) return;
        var nodeid; var tplid;
        if (preloadInProgress) __checknForceHearbeat();
        for (var i=0;i<model.count;i++) {
            nodeid = model.getValue(i,"cid")?model.getValue(i,"cid"):model.getValue(i,"mid");
            tplid = model.getValue(i,'category_attr_template_id')?model.getValue(i,'category_attr_template_id'):"";
            if(Store.PreloadMediaTemplateIds.length>0){
                if(Store.PreloadMediaTemplateIds.indexOf(tplid)>=0){getNextMenu([nodeid,tplid], __preloadCallback);}
            }else {
                if(tplid!=undefined && tplid!=""){ getNextMenu([nodeid,tplid], __preloadCallback);}
            }
        }
        delete nodeid;delete tplid;delete i;
    }

    function __loadDatabases (dbArr) {
        var dblist = []; var dbindx;
        for (var i in dbArr) dblist[i]=dbArr[i]['uId'];
        dbindx = dblist.indexOf("mediaDb");
        if (dbindx!=-1) {Store.cMediaDbReference = dbindx; __setDbReference(dbArr[dbindx]["dbpath"],dataController.mediaDb,dbindx);}
        dbindx = dblist.indexOf("airportDb");
        if (dbindx!=-1) {Store.cAirportDbReference = dbindx; __setDbReference(dbArr[dbindx]["dbpath"],dataController.airportDb,dbindx);}
        dbindx = dblist.indexOf("connectDb");
        if (dbindx!=-1) {Store.cConnectDbReference = dbindx; __setDbReference(dbArr[dbindx]["dbpath"],dataController.connectDb,dbindx);}
        dbindx = dblist.indexOf("helpDb");
        if (dbindx!=-1) {Store.cHelpDbReference = dbindx; __setDbReference(dbArr[dbindx]["dbpath"],dataController.helpDb,dbindx);}
    }

    function __setDbReference (dbpath,dbref,dbindx) {
        var conn = Qt.createQmlObject('import Panasonic.Pif 1.0; SqlConnection { dbname:"../../content/'+dbpath+'" }', mediaModel);
        dbref = dbComp.createObject(mediaModel);
        if(Store.cMediaDbReference==dbindx){core.info("MediaModels.qml |  __setDbReference | dbref   "+dbref);mediaDbSqliteDatabaseRef=dbref;}
        dbref.connection = conn;
        Store.DbReferenceArr[dbindx] = dbref;
        delete conn;
        return true;
    }

    function __setCategoryAttributeForTemplate(catAttr){ Store.categoryAttributeForTemplate = catAttr; }   // Set the category attribute for template ID 
    function __setPreloadMediaTemplateIds(attr){ Store.PreloadMediaTemplateIds = attr; }   // Set the tids to preload
    function __setDestroyModels(attr){ Store.DestroyModels = attr; }

    function __getMediaDbReference () {return Store.DbReferenceArr[Store.cMediaDbReference];}
    function __getDbReference (arrIndx) {return Store.DbReferenceArr[arrIndx];}
    function __checknForceHearbeat() {  // Synchronous query requests exceeds heartbeat max time.
        var d = new Date().getTime();
        if (d - __prevTime>20000) { __prevTime = d; pif.forceHeartBeat();}
        delete d;
    }

    Component.onCompleted: {
        if (!Store.ModelArr) Store.ModelArr = new Array([],[]);
        if (!Store.DbReferenceArr) Store.DbReferenceArr = new Array();
        Store.cSqlModelIndex=0;
        Store.cListModelIndex=1;
        Store.cCallback1Index=2;
        Store.cCallback2Index=3;
        Store.cSimpleModelIndex=4;
        Store.categoryAttributeForTemplate='category_attr_cidtype';
        Store.PreloadMediaTemplateIds=[];
        Store.DestroyModels=true;
        subtitlelModel = lModelComp.createObject(mediaModel);
        core.info ("Media | Component.onCompleted called");
    }

    Component{ id: dbComp; SqliteDatabase {}}
    Component{ id: lModelComp; ListModel {}}
    Component{ id: sModelComp; SimpleModel {signal modelReady(variant sModel, int apiType);}}
    Component{
        id: sqlModelComp;
        SqlListModel {
            id: sqlModel; enabled: false;
            queryMode: preloadInProgress?"Synchronous":"Asynchronous";
            property int apiType:0;
            property int instanceId:0;
            property int languageId: 0;

            signal modelReady(variant dModel, int apiType);
            onStatusChanged:{
                core.info("MediaModels.qml | onStatusChanged SqlListModel status:    "+status);
                if(status == SqlListModel.Ready){
                    enabled = false; queryMode = "Asynchronous";
                    core.info("MediaModels.qml | onStatusChanged count: "+count+" query: "+query+"  retryTimer stopped");
                    modelReady(sqlModel,apiType);
                }else if(SqlListModel.Error){
                    core.info("MediaModels.qml | onStatusChanged  | SqlListModel.Error encountered ");
                }
            }
            Component.onDestruction: core.debug("Media | onDestruction apiType: "+apiType+" | modelInstanceId: "+instanceId)
        }
    }    

    Timer {
		id: dbConnTimer;
		interval: 5000;
		running: mediaDbSqliteDatabaseRef!=undefined && mediaDbSqliteDatabaseRef.status ? mediaDbSqliteDatabaseRef.status != Pif.Ready : false;
		repeat: true
		onTriggered: {
			core.info("MediaModels.qml | dbConnTimer | onTriggered | mediaDbSqliteDatabaseRef.status: "+mediaDbSqliteDatabaseRef.status);
			if (mediaDbSqliteDatabaseRef.status != Pif.Ready) {
				core.info("MediaModels.qml | dbConnTimer | Trying to reconnect to database when status is not Ready...");
				Qt.quit(); // quitting Qt
			}else{
				dbConnTimer.running = false;
				dbConnTimer.repeat  = false;
				dbConnTimer.stop();
			}
		}
		onRunningChanged: core.info("MediaModels.qml | dbConnTimer | onRunningChanged | running: "+dbConnTimer.running);
	}
}
