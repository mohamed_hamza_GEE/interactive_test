import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item{
    id: exTv
    signal __sigModelReady(variant model,bool status,string type)
    signal sigStatusChanged(bool status);

    Connections{
        target:core.pif;
        onExtvVersionChanged:{core.info('Extv.qml | onExtvVersionChanged | extvVersion :'+extvVersion);Store.pif_extv_version=extvVersion; if(core.pc){core.log("Extv.qml | onExtvVersionChanged | simulate called");extvModel.simulate("content/extv/stationListSM.json",Store.pif_extv_version)} }
        onExtvStateChanged:{core.info('Extv.qml | onExtvStateChanged | extvState :'+core.pif.getExtvState());Store.liveTVAvailable = (core.pif.getExtvState())?true:false;if(!Store.liveTVAvailable){sigStatusChanged(false);}}
    }

    //public functions
    function getExtvList(callback){
        core.info("Extv.qml | getExtvList | Called ");
        __updateCallback(callback);
        /*if(Store.liveTVAvailable && extvModel.count>0){__sigModelReady(extvModel,true,'extvModel');}
        else {__sigModelReady(extvModel,false,'extvModel');}
        __disconnectCallback();*/

        if(Store.liveTVAvailable && extvModel.count>0){
                core.info("Extv.qml | getExtvList | pif_extv_version: "+Store.pif_extv_version+" | model_extv_version: "+Store.model_extv_version);
            __process_extv_version_num(Store.pif_extv_version);}
        else {__sigModelReady(extvModel,false,'extvModel');__disconnectCallback();}

    }

    function getSoundTrack(modelIndex,callback){
        core.info("Extv.qml | getSoundTrack | modelIndex: " + modelIndex);
        __updateCallback(callback);
        var audioObj = extvModel.at(modelIndex).audio;
        audioModel.source = extvModel.at(modelIndex).audio
        __sigModelReady(audioModel,true,'soundTrack');
        __disconnectCallback();
    }

    function resetVariables(){
        core.info('Extv.qml | resetVariables Called ');
        Store.callback ='';
        Store.extv_version=-1;Store.model_extv_version=0;Store.pif_extv_version=0;
        Store.liveTVAvailable=extvModel.liveTVAvailable;
        Store.dynamicChannelLookup=[];
    }

    //private functions
    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }

    function __disconnectCallback(){if(Store.callback){__sigModelReady.disconnect(Store.callback);}}

    function __process_extv_version_num(version){
        core.info('Extv.qml | __process_extv_version_num | Called | new extv version : '+version+' | previous extv version : '+Store.extv_version);
        if(!Store.liveTVAvailable){
            core.info('Extv.qml | __process_extv_version_num | liveTVAvailable :'+Store.liveTVAvailable+' | live tv not avaliable');
            __sigModelReady(extvModel,false,'extvModel');__disconnectCallback();return false;
        }else if(version==Store.extv_version){
            core.info('Extv.qml | __process_extv_version_num | No Change Detected |  new extv version : '+version+' | previous extv version : '+Store.extv_version);
            __sigModelReady(extvModel,true,'extvModel');__disconnectCallback();return true;
        }else {
            Store.extv_version = version
            __processModel();
        }
    }

    function __processModel(){
        core.info("Extv.qml | __processModel | extvModel.count "+extvModel.count)
        Store.dynamicChannelLookup=[]
        var tmpDynamicChannelLookup = []
        for(var i=0;i<extvModel.count;i++){
            core.info("Extv.qml | __processModel | id : "+extvModel.getValue(i,"id")+" | DynamicChannel : "+extvModel.getValue(i,"DynamicChannel")+" |  rxStatus : "+extvModel.getValue(i,"rxStatus"))
            if(extvModel.getValue(i,"DynamicChannel")=="No"){
                core.info("Extv.qml | __processModel | index : "+i+" | id : "+extvModel.getValue(i,"id")+" | mid : "+extvModel.getValue(i,"MID")+" | added to list ")
                tmpDynamicChannelLookup[i] = extvModel.getValue(i,"MID")
                Store.dynamicChannelLookup[extvModel.getValue(i,"MID")] = i
            }
        }
        core.info("Extv.qml | __processModel | Dynamic Channel list length : "+  tmpDynamicChannelLookup.length)
        if(tmpDynamicChannelLookup.length>0){
            core.info("Extv.qml | __processModel | Dynamic Channel list mids : "+  tmpDynamicChannelLookup.join(','))
            core.dataController.getMediaByMid([tmpDynamicChannelLookup.join(',')],__process_callback);
        }else {
            core.info('Extv.qml | __processModel | Process Complete ');
           __sigModelReady(extvModel,true,'extvModel');__disconnectCallback();
        }
    }

    function __process_callback(dmodel){
        core.info("Extv.qml | __process_callback | Model is ready | count : "+dmodel.count)
        if(dmodel.count>0){
            //core.info('Extv.qml | __process_callback | dataController.seatRating : '+dataController.getSeatRating())
            for(var j=0;j<dmodel.count;j++){
                core.info('Extv.qml | __process_callback | rating : '+dmodel.getValue(j,"rating")+' | mid : '+dmodel.getValue(j,"mid")+' | index : '+Store.dynamicChannelLookup[dmodel.getValue(j,"mid")])
                    for(var tmpField in Store.liveTvUpdateFiledList){
                        core.info("Extv.qml | __process_callback | updating : "+tmpField+" : "+Store.liveTvUpdateFiledList[tmpField]+" : "+dmodel.getValue(j,Store.liveTvUpdateFiledList[tmpField])+" | return value : "+extvModel.setValue(parseInt(Store.dynamicChannelLookup[dmodel.getValue(j,"mid")],10),tmpField,dmodel.getValue(j,Store.liveTvUpdateFiledList[tmpField]),'','',''))
                    }
                //if(dmodel.getValue(j,"rating")<=dataController.getSeatRating()){core.info('Extv.qml | __process_callback | rating : '+dmodel.getValue(j,"rating")+' | mid : '+dmodel.getValue(j,"mid")+' | Seat rating blocked ')}
            }
        }
        core.info('Extv.qml | __process_callback | Process Complete ');
        __sigModelReady(extvModel,true,'extvModel');__disconnectCallback();
    }

    Component.onCompleted: {
        Store.liveTvUpdateFiledList= {'name':'title','image':'poster','description':'description'};
        resetVariables();
        core.info ("Extv.qml | onCompleted | LOAD COMPLETE");
    }

    EXTVListModel {
        id: extvModel
        enableUpdates:true
        eventListener:extvModelEventListener;
        onReadingStationFileError :{
             core.info("Extv.qml | onReadingStationFileError | Called ");
             Store.liveTVAvailable = false;
             sigStatusChanged(false);
        }
        onCountChanged: {core.info("Extv.qml | onCountChanged: " + count);Store.extv_version=-1;sigStatusChanged(true);}
        onLiveTVAvailableChanged:{
            core.info("Extv.qml | onLiveTVAvailableChanged: " + liveTVAvailable);
            if(!liveTVAvailable){
                Store.liveTVAvailable = false;
                sigStatusChanged(false);
            }else {
                Store.liveTVAvailable = true;
                sigStatusChanged(true);
            }
        }
        onChannelVersionChanged:{core.info("Extv.qml | onChannelVersionChanged | newChannelVersion: "+newChannelVersion);Store.model_extv_version=newChannelVersion;}
        Component.onCompleted:{if(core.pc){Store.pif_extv_version=pif.getExtvVersion();}if(Store.pif_extv_version && core.pc){core.log("Extv.qml | onCompleted | simulate called");extvModel.simulate("content/extv/stationListSM.json",Store.pif_extv_version)}}
    }

    EventListener {
        id: extvModelEventListener

        deviceManager: pif.deviceMngr;
        events: [
            EventData { name: "EXTV_CHANNEL_LISTING_VERSION" }
        ]
    }

    JsonModel{ id:audioModel }

}
