import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

DataLoader {
    signal __sigModelReady(variant model,bool status);
    signal __sigalMidRating(int rating, bool status);
     signal __sigalMidRatedStatus(bool status)
    property bool __contentRating:false;

    onSigDataReady: {
        if (errCode==0){
            core.info("ContentRating.qml | dataLoader | onSigDataReady");
            __processModel(responseObj,responseString,requestId);
        }else {
            isAppActive=false;__sigModelReady(contentRating,false);
        }
    }

    onSigLocalDataReady:{
        if(errCode==0){
            core.info("ContentRating.qml | dataLoader | onSigLocalDataReady ");
            __processModel(responseObj,'fromConfigModel',requestId)
        }else{
           isAppActive=false; __sigModelReady(contentRating,false);
        }
    }

    //public function

    function getMediaRatingModel(callback){
        if (isAppActive==true){core.info('ContentRating.qml| getMediaRatingModel | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        var requestId=1;__updateCallback(callback);
        contentRating.clear();
        core.info('ContentRating.qml | getMediaRatingModel');
        var getParams =  '../../content/CRR/ratings.json'
         __getLocalData(getParams,requestId);

    }

    function getMidRatingByMid(mid,callback){
        if (isAppActive==true){core.info('ContentRating.qml| getMidRatingByMid | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        var requestId=2;__updateMidRatingCallback(callback);
        Store.mid=mid;
        core.info('ContentRating.qml | getMidRatingByMid | mid : '+mid);
        if(__contentRating){
            for(var i=0; i<contentRating.count; i++){
                if(contentRating.getValue(i,'MID')==mid){
                   isAppActive=false;Store.mid=0;
                    return contentRating.getValue(i,'rating');
                    //__sigalMidRating(contentRating.getValue(i,'rating'),true)
                }
            }
            isAppActive=false;
            return 0;
        }else{
            isAppActive=false;
            getMediaRatingModel();
        }
    }

    function fetchTopRatedMids(){
        if (isAppActive==true){core.info('ContentRating.qml| fetchTopRatedMids | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        var requestId=4;//__updateCallback(callback);
        core.info('ContentRating.qml | getMediaRatingModel');
        var getParams =  '../../content/CRR/top_rated.json'
        __getLocalData(getParams,requestId);
    }

    function getTopRatedMediaByType(content_type,callback,cid,tid){
        if(Store.topRatedArr[content_type]){
            var midarr=[];
            for(var i in Store.topRatedArr[content_type]){
                midarr.push(i)
            }
            var mids = midarr.join(',')
            core.info("ContentRating.qml | getTopRatedMediaByType |top rated mids under type : "+content_type+"are : "+mids)
            isAppActive=false;
            core.dataController.getMediaByMid([mids],callback)
        }else if(cid && cid !='' && tid && tid !=''){
            core.info("ContentRating.qml | getTopRatedMediaByType | calling getnextmenu for tid : "+tid+" cid : "+cid)
            isAppActive=false;
            core.dataController.getNextMenu([cid,tid],callback)
        }
        else{ isAppActive=false;__sigalMidRatedStatus(false);}
    }

    function setMidStarRating(mid,rating,callback){
        if (isAppActive==true){core.info('ContentRating.qml| setMidRatingByMid | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        __updateMidRatedStatusCallback(callback);
        var getParams =  ''
        var requestId=3
        core.debug('ContentRating.qml | setMidRating | mid : '+mid+'| rating : '+rating)
        if(pif.getFallbackMode()){
            __resetInFallbackMode();
        }else{
            if(dataController.__makeHeadendRequest()){
                getParams =  'function=seat_setStarRatingMedia&MID='+mid+'&rating='+rating+'&seat='+core.pif.getSeatNumber()+'&cabin_class='+core.pif.getCabinClassName()
                core.debug('ContentRating.qml | setMidRating | makeHeadendRequest: '+dataController.__makeHeadendRequest()+' | getParams  : '+getParams)
                __getRemoteData(getParams,requestId);
            }else{
                getParams =  '../../content/CRR/seat_setStarRatingMedia.json'
                __getLocalData(getParams,requestId);
            }
        }
    }

    //private function
    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }


    function __updateMidRatingCallback(callback){
        if (Store.midRatingCallback){__sigalMidRating.disconnect(Store.midRatingCallback);}
        if (callback){__sigalMidRating.connect(callback);Store.midRatingCallback = callback;}
    }

    function __updateMidRatedStatusCallback(callback){
        if (Store.midRatedStatusCallback){__sigalMidRatedStatus.disconnect(Store.midRatingCallback);}
        if (callback){__sigalMidRatedStatus.connect(callback);Store.midRatedStatusCallback = callback;}
    }

    function __processModel(value,data,requestId){
        if(requestId==1 || requestId==2){
            if(value.length>0){
                core.info("ContentRating.qml | __processModel | Data ready = " + !value['code'])
                __readJsonObject(requestId,value)
            }else{
                core.info("ContentRating.qml | __processModel | ERROR | value "+value+" | data :"+data)
                isAppActive=false;__sigModelReady(contentRating,false);
            }
        }else if(requestId==4){
            if(value.length>0){
                core.info("ContentRating.qml | __processModel")
                __readJsonObject(requestId,value)
            }else{
                core.info("ContentRating.qml | __processModel | ERROR | value "+value+" | data :"+data)
                isAppActive=false;__sigModelReady(contentRating,false);
            }
        }else{
            if(value['code'] == 0){
                core.info("ContentRating.qml | __processModel | Data ready = " + !value['code'])
                isAppActive=false;__sigalMidRatedStatus(true);
            }else{
                core.info("ContentRating.qml | __processModel | ERROR | value "+value+" | data :"+data)
                 isAppActive=false;__sigalMidRatedStatus(false);
            }
        }
    }

    function __readJsonObject(requestId,value){
        core.info('ContentRating.qml | __readJsonObject | id :'+requestId)
        if(requestId==0){core.info('ContentRating.qml | __readJsonObject | Data ready = false')}
        if(requestId==1||requestId==2){__loadContentRating(value);}
        if(requestId==4){__loadTopRatedMids(value)}
    }

    function __loadContentRating(value){
        core.info('ContentRating.qml | __loadContentRating | Called ');

        contentRating.clear();
        if(value.length){
            for(var counter=0;counter<value.length;counter++){
                contentRating.insert(counter,{"MID": value[counter].m,
                                               "uniqueId": value[counter].u,
                                               "countNumberOfRatings": value[counter].c,
                                               "rating": value[counter].r
                                     });

            }
            __contentRating=true;
        }else {
             core.info('ContentRating.qml | __loadContentRating | ERROR | Data not found ');
             isAppActive=false; __sigModelReady(contentRating,false);
        }

        core.info('ContentRating.qml | __loadContentRating | contentRating count : '+contentRating.count);
        if(Store.mid>0){isAppActive=false;
            for(var i=0; i<contentRating.count; i++){
                if(contentRating.getValue(i,'MID')==Store.mid){
                    isAppActive=false;
                    //__sigalMidRating(contentRating.getValue(i,'rating'),true);
                    Store.mid=0;return contentRating.getValue(i,'rating');
                }
            }
            isAppActive=false;return 0 ;
        }
        else{isAppActive=false;__sigModelReady(contentRating,true);}
    }

    function __loadTopRatedMids(value){
        core.info('ContentRating.qml | __loadTopRatedMids | Called ');
        if(value){
            for(var counter=0;counter<value.length;counter++){
                if(typeof Store.topRatedArr[value[counter].content_type]=='undefined') Store.topRatedArr[value[counter].content_type]=[];
                for(var counter1=0;counter1<value[counter].contents.length;counter1++){

                    //Store.topRatedArr[value[counter].content_type][value[counter].contents[counter1].MID]= [];
                    Store.topRatedArr[value[counter].content_type][value[counter].contents[counter1].MID]=value[counter].contents[counter1].stars
                }
            }

        }else {
             core.info('ContentRating.qml | __loadTopRatedMids | ERROR | Data not found ');
        }

       isAppActive=false;return true;
    }


    function __getRemoteData(getParams,requestId){
        getData(core.settings.remotePath+'/content_rating/api/star_api.php?',3,getParams,'','',requestId);
    }

    function __getLocalData(getParams,requestId){getLocalData(getParams,requestId);}

    function resetVariables(){
        core.info('ContentRating.qml | resetVariables Called ');
        isAppActive = false;
        contentRating.clear();
    }

    function __resetInFallbackMode(){
         core.info('Ltn.qml | __resetInFallbackMode | in fallback mode returning blank model');
        __sigModelReady(contentRating,false);
    }
    SimpleModel {id: contentRating}
    Component.onCompleted: {
        Store.mid = 0;
        Store.topRatedArr=[];
        core.info ("ContentRating.qml | Component.onCompleted | ContentRating LOAD COMPLETE");
    }
}
