import QtQuick 1.1
import Panasonic.Pif 1.0
import "media.js" as Media
MediaModels {
    id: dataController
    property variant ltn;
    property variant shopping;
    property variant hospitality;
    property variant survey;
    property variant connectingGate;
    property variant keywordSearch;
    property variant extv;
    property variant ans;
    property variant geniusPlaylist;
    property variant contentRating;
    property variant contentStaging;
    property variant woiApps;
    property variant pdi;
    property alias mediaServices:mediaServices;

    Connections{target: core.pif; onMediaDateChanged: Media.setMediaDate(mediaDate); onRouteIdChanged: Media.setRouteId(routeId);
        onSeatRatingChanged: Media.setSeatRating(seatRating); onServiceAccessChanged:dataController.mediaServices.__setServiceAccessType(accessType,serviceMid); onMidAccessChanged:dataController.mediaServices.setMidAccessType(accesstype,mid,1);
        onSigPpvStatusChanged:{if(cause == 'PPV_REFUND' && !(__getInteractiveLoadingStatus())){dataController.mediaServices.__setPpvRefundMidData(mid,status,cause);dataController.mediaServices.__getRouteInfo()}else{dataController.mediaServices.setPurchaseMidStatus(mid,status,cause);}}
        onOpenFlightChanged:{if(openFlight==0 && !Media.isRouteDataAvailable){core.info("Media | closeFLight - route data was not available | Releasing event lock ");interactiveStartup.eventHandler(Media.cApiRouteConfiguration)}}
        onSigFallbackMode:{if(!Media.isRouteDataAvailable){core.info("Media | loading default route data on fallback");Media.getRouteConfiguration()}}
    }
    Connections{target: core.pif.ppv?core.pif.ppv:null; onSigPaymentSuccessful:dataController.mediaServices.setPurchaseMidStatus(amid,'paidorfree','PPV_CREDIT');}
    // Public Functions for menu naviagtion
    function getIntLanguageModel () { return getListMediaModel(Media.cApiIntLanguages,1);}
    function getLanguageLabelsByLid (lidList) { return Media.getLanguageLabelsByLid(lidList);}
    function getLanguageLabelsByLidCustomized(lidList,lid) { return Media.getLanguageLabelsByLidCustomized(lidList,lid);}
    function getLidByLanguageISO (isocode) { return Media.getLidByLanguageISO(isocode);}
    function getLanguageIsoByLid (lidList){ return Media.getLanguageIsoByLid (lidList);}

    function getParentMenu (params,callback) {return Media.getParentMenu(params,callback,1);}
    function getNextMenu (params,callback) {return Media.getNextMenu(params,callback);}

    function getAudioTrackList(params,callback) { params[1]=Media.cApiAudioTrackList; return Media.getSimpleModelDataByApi(Media.cApiAudioTrackList,params,callback,(params[3])?params[3]:1);}
    function getMediaSynopsis (params,callback) { params[1]=Media.ActionCodeArr[params[1]].trId; return Media.getDataByApi(Media.cApiMediaSyn,params,callback,1);}
    function getVodPlayerData (params,callback) { params[1]=Media.cApiVideoPlayer; return Media.getDataByApi(Media.cApiVideoPlayer,params,callback,1);}
    function getAodPlayerData (params,callback) { params[1]=Media.cApiAudioPlayer; return Media.getDataByApi(Media.cApiAudioPlayer,params,callback,1);}

    function getSoundTracks (params,callback) {return Media.getSoundTracks(params,callback,1);}
    function getSubtitles (params,callback) {return Media.getSubtitles(params,callback,1);}
    function getSubtitlesWithLabel (params,callback,labelString) { return Media.getSubtitlesWithLabel(params,callback,labelString);}
    function getPlaylistData(params,callback){ params[1]= Media.cApiPlaylistData;return Media.getSimpleModelDataByApi(Media.cApiPlaylistData, params, callback, (params[5])?params[5]:1);}
    function getPlaylistDataWithParentInfo(params,callback){ params[1]= Media.cApiPlaylistDataWithParentInfo;return Media.getSimpleModelDataByApi(Media.cApiPlaylistDataWithParentInfo, params, callback, (params[5])?params[5]:1);}

    //Public function to get media information from database
    function getCategoryByCid(params,callback){ params[1]= Media.cApiCategoryByCid; return Media.getDataByApi(Media.cApiCategoryByCid, params, callback, 1);}
    function getMediaByMid(params,callback){ params[1]= Media.cApiMediaByMid;return Media.getDataByApi(Media.cApiMediaByMid, params, callback, (params[5])?params[5]:1);}

    //PPV apis
    function getBundlesData(params,callback){ params[1]= Media.cApiBundlesData;return Media.getDataByApi(Media.cApiBundlesData, params, callback,1)}
    function getBundlesDataWithMid(params,callback){ params[1]= Media.cApiBundlesDataWithMid;return Media.getDataByApi(Media.cApiBundlesDataWithMid, params, callback,1)}
    function getMediaByLanguageIso(params,callback){ params[1]= Media.cApiMediaByLanguageIso;return Media.getDataByApi(Media.cApiMediaByLanguageIso, params, callback,1);}
    function getBundlesChildData(params,callback){ params[1]= Media.cApiBundleChildData;return Media.getDataByApi(Media.cApiBundleChildData, params, callback,(params[2])?params[2]:1);}

    function getCategoryMediaByMid(params,callback){ params[1]= Media.cApiCategoryMediaByMid;return Media.getDataByApi(Media.cApiCategoryMediaByMid, params, callback, (params[5])?params[5]:1);}
    function getCategoryByTemplateId(params,callback){ params[1]= Media.cApiCategoryByTemplateId;return Media.getDataByApi(Media.cApiCategoryByTemplateId, params, callback, (params[5]?params[5]:1));}

    function getChildCategoryByTemplateId(params,callback){params[1]= Media.cApiChildCategoryByTemplateId;return Media.getDataByApi(Media.cApiChildCategoryByTemplateId, params, callback,(params[2]?params[2]:1));}
    function getChildMediaByTemplateId(params,callback){params[1]= Media.cApiChildMediaByTemplateId;return Media.getDataByApi(Media.cApiChildMediaByTemplateId, params, callback,(params[2]?params[2]:1));}
    function getChildMediaByAggregateMid(params,callback){params[1]= Media.cApiChildMediaByAggregateMid;return Media.getDataByApi(Media.cApiChildMediaByAggregateMid, params, callback,(params[2]?params[2]:1));}
    function getAggregateMidByChildMid(params,callback){params[1]= Media.cApiAggregateMidByChildMid;return Media.getDataByApi(Media.cApiAggregateMidByChildMid, params, callback,(params[2]?params[2]:1));}
    function getCategoryByFieldName(params,callback){params[1]= Media.cApiCategoryByFieldName;return Media.getDataByApi(Media.cApiCategoryByFieldName, params, callback,(params[6]?params[6]:1));}

    function getParentCategoryByMid(params,callback){params[1]= Media.cApiParentCategoryByMid;return Media.getDataByApi(Media.cApiParentCategoryByMid, params, callback, 1);}
    function getHelpRootCategories(callback){var params= ['-1','helpcategories'];return Media.getNextMenu(params,callback);}
    function getSourceCityByIATA(params,callback){params[1]= Media.cApiCityByIATA;return Media.getDataByApi(Media.cApiCityByIATA, params, callback, 1);}
    function getDestinationCityByIATA(params,callback){params[1]= Media.cApiCityByIATA;return Media.getDataByApi(Media.cApiCityByIATA, params, callback, 2);}
    function getCtToSeatMessage(params,callback){return Media.getCtToSeatMessage(params, callback);}
    function getCtToSeatMessageWithLid(params,callback){return Media.getCtToSeatMessageWithLid(params, callback);}
    function getVariableConfiguration(params,callback){return Media.getVariableConfiguration(params, callback, (params[1])?params[1]:1);}
    function getSeatRating(){return Media.SeatRating;}
    function getCategoryByICAO(params,callback){params[1]= Media.cApiCategoryByICAO;return Media.getDataByApi(Media.cApiCategoryByICAO, params, callback, (params[2]?params[2]:1));}
    function getNonRouteChildMediaByTemplateId(params,callback){ params[1]= Media.cApiNonRouteChildMediaByTemplateId; return Media.getDataByApi(Media.cApiNonRouteChildMediaByTemplateId, params, callback, 1);}
    function getMediaByFieldName(params,callback){ params[1]= Media.cApiMediaByFieldName; return Media.getDataByApi(Media.cApiMediaByFieldName, params, callback, 1);}
    function getContentTerminalMaps(params,callback){ params[1]= Media.cApiContentTerminalMaps; return Media.getDataByApi(Media.cApiContentTerminalMaps, params, callback, 1);}
    function populateAssociatedMids(){Media.getAssociatedMidsModel();}
    function getAssociatedMids(mid){return Media.associatedMids[mid]?Media.associatedMids[mid]:[];}
    function populateNbdGroupsMids(){Media.getNbdGroupsMidsModel();}
    function checkMidExistInNbd(mid){return Media.nbdGroupsMids[mid]?true:false;}
    function getNbdGroupsMidList(){return Media.nbdGroupsMidList;}
    function populateAllLablesByTid(tids,langISO){return Media.getAllLabelsModel(tids,langISO);}

    //Public function for internal variables
    function getMenuWhereClause(arrIndx){return Media.MenuWhereClauseArr[arrIndx];}
    function getMenuWhereClauseByTemplateId(template_id){return Media.MenuWhereClauseArr[Media.ActionCodeArr[template_id].wrId];}
    function updateMenuWhereClause(arrIndx,operation,whereString){return Media.updateMenuWhereClause(arrIndx,operation,whereString);}
    function getAircraftType(){return Media.AircraftType;}
    function getAircraftSubType(){return Media.AircraftSubType;}
    function getMediaConfigId(){return Media.MediaConfig;}
    function getCDCountByCID(cid){return Media.CDCountArr[cid];}
    function getRootNode(){return Media.PreloadMediaRootNode;}
    function getCategoryAttributeForTemplate(){return Media.categoryAttributeForTemplate;}
    function getCategoryLabel(iso,cid){return Media.CategoryLabelLookup[iso][cid];}
    function getKeywordSearchPosters(){var params=[];params[1]= Media.cApiKeywordSearchPosters; return Media.getDataByApi(Media.cApiKeywordSearchPosters, params, '', '');}
    function getKeywordSearchCategoryPosters(){var params=[];params[1]= Media.cApiKeywordSearchCategoryPosters; return Media.getDataByApi(Media.cApiKeywordSearchCategoryPosters, params, '', '');}
    // Public function to get list of ratings supported in interactive
    function getRatingList (callback) { return Media.getRatingList(callback);}
	
    function getSoundTrackType(){return Media.getSoundTrackType();}
    function getSubtitleType(){return Media.getSubtitleType();}
    function setSoundTrackType(type){Media.setSoundTrackType(type)}
    function setSubtitleType(type){Media.setSubtitleType(type)}
    function getSoundConfigId(){return Media.SoundConfig;}
    
    //PPV service blocking
    function setPPVServiceBlockStatus(status){Media.setPPVBlockClause(status,mediaServices.getPaidOrFreeMids());}
    function getPaidOrFreeMids(){return mediaServices.getPaidOrFreeMids()}
	function destroySqlModel(type,modelInstance){
        modelInstance=modelInstance?modelInstance:1
        switch(type){
        case "lookupModel":{destroyModel(Media.cApiLookUpModel,modelInstance);break}
        case "getParentMenu":{destroyModel(Media.cApiParentMenu,modelInstance);break}
        case "getCategoryByCid":{destroyModel(Media.cApiCategoryByCid,modelInstance);break}
        case "getChildMediaByAggregateMid":{destroyModel(Media.cApiChildMediaByAggregateMid,modelInstance);break}
        case "getChildMediaByTemplateId":{destroyModel(Media.cApiChildMediaByTemplateId,modelInstance);break}
        case "getMediaByMid":{destroyModel(Media.cApiMediaByMid,modelInstance);break}
        default:{core.info(" Media | destroySqlModel not defined for type: "+type);break}
        }
    }

    //nbdGroup
    function getMidGroupStatus(mid){return Media.getMidGroupStatus(mid);}

    // Private functions
    function __preloadData() {
        //return Media.PreloadMedia ? __preloadModels(Media.PreloadMediaRootNode):false;
        if(Media.PreloadMedia){
            __preloadModels(Media.PreloadMediaRootNode)
            if(Media.PreloadLabel){ Media.getAllLabelsModel();}
        }else
            return false;
    }
    function __setRouteData(params){core.pif.__setInternalSeatRating(params.seatrating);return Media.setRouteData(params);}
    function __setMidBlockList(mids){return Media.setMidBlockList(mids);}
    function __setCidBlockList(cids){return Media.setCidBlockList(cids);}
    function __getCDCount(){return Media.getCDCount()}
    function __getSeatRatingClause(){return Media.SeatRatingClause?Media.SeatRatingClause:'';}
    function __getMediaDateClause(){return "categorymedia_startdate<="+Media.MediaDate+" and "+Media.MediaDate+"<=categorymedia_enddate";}
    function __getCategoryDateClause(){return "category_startdate<="+Media.MediaDate+" and "+Media.MediaDate+"<=category_enddate";}
    function __getMediaConfigClause(){return Media.MediaConfigClause?Media.MediaConfigClause:'';}
    function __getMidBlockList(){return Media.BlockMIDClause;}
    function __getBundleCidLookupModel(bmid){Media.getBundleCidLookupModel(bmid);}
    function __getInteractiveLoadingStatus(){core.log("DataController | __getInteractiveLoadingStatus | status "+Media.InteractiveLoadingStatus);return Media.InteractiveLoadingStatus;}
    function __setInteractiveLoadingStatus(status){Media.InteractiveLoadingStatus=status;}

    function __loadNonRouteData(){__setInteractiveLoadingStatus(true);Media.getCategoryAttributeForTemplate();}

    function __loadRouteData(){
        if(__getInteractiveLoadingStatus()==false && pif.getFallbackMode()){core.info("DataController | __loadRouteData | InteractiveLoadingStatus "+__getInteractiveLoadingStatus()+" | fallbackMode: "+pif.getFallbackMode()+" | returning ");interactiveStartup.eventHandler(Media.cApiRouteData);return true;}
        __setInteractiveLoadingStatus(true);
        Media.getServiceModel(mediaServices.__processServiceModel);
        if(Media.HideEmptyCategory){Media.getAllEmptyCategories();}
        Media.getAllLanguages();Media.getIntLanguages();
        if(Media.PpvRequired){core.info("DataController | __loadRouteData | PpvRequired : "+Media.PpvRequired);Media.getBundleLookupModel(mediaServices.__processBundleModel);}
        Media.getLookUpModel(mediaServices.__processLookupModel);
        //Media.MakeHeadendRequest!=false?mediaServices.__getRouteInfo():interactiveStartup.eventHandler(Media.cApiRouteData);
        mediaServices.__setIsRefundCalled(false);
        if(pif.getFallbackMode()){
            core.info("DataController.qml | __loadRouteData | in fallback mode ")
            interactiveStartup.eventHandler(Media.cApiRouteData);__setInteractiveLoadingStatus(false)
        }else{
            mediaServices.__getRouteInfo();
        }
    }


    function __setConfiguration(dataSection){
        Media.setConfiguration(dataSection);
        mediaServices.__setConfiguration(dataSection);mediaServices.__setcApiRouteData(Media.cApiRouteData);mediaServices.__setIsParentBlockRequired(Media.IsParentBlockRequired);
    }

    function __makeHeadendRequest(){return Media.MakeHeadendRequest;}
    function __setSimConfiguration(simData){ Media.setSimConfiguration(simData); return true;}
    function __getMenuSDKVersion(){return Media.__getMenuSDKVersion();}
    function __loadDynamicComponents(){
        if(Media.CompList){
            for(x in Media.CompList){
                core.info("DataController | __loadDynamicComponents | Loading :"+Media.CompList[x]);
                if(Media.CompList[x] == "Ltn"){
                    ltn = core.coreHelper.loadDynamicComponent("datacontroller/Ltn.qml", dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[ltn.resetVariables]);
                    ltn.__setConfiguration(Media.LtnConfiguration);
                }else if (Media.CompList[x] == "Shopping"){
                    shopping=core.coreHelper.loadDynamicComponent("datacontroller/InflightMenu.qml", dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStageLoadShopping,[shopping.startPreload],[Media.cEventShopping]);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[shopping.resetVariables]);
                    shopping.__setAppAsShopping();shopping.__setConfiguration(Media.CabinClassArr,Media.ShopppingConfiguration,Media.cEventShopping);
                }else if (Media.CompList[x] == "Hospitality"){
                    hospitality=core.coreHelper.loadDynamicComponent("datacontroller/InflightMenu.qml", dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStageLoadHospitality,[hospitality.startPreload],[Media.cEventHospitality]);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[hospitality.resetVariables]);
                    hospitality.__setAppAsHospitality();hospitality.__setConfiguration(Media.CabinClassArr,Media.HospitalityConfiguration,Media.cEventHospitality);
                }else if (Media.CompList[x] == "Shopping2"){
                    shopping=core.coreHelper.loadDynamicComponent("datacontroller/InflightMenu2.qml", dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStageLoadShopping,[shopping.startPreload],[Media.cEventShopping]);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[shopping.resetVariables]);
                    shopping.__setAppAsShopping();shopping.__setConfiguration(Media.CabinClassArr,Media.ShopppingConfiguration,Media.cEventShopping);
                }else if (Media.CompList[x] == "Hospitality2"){
                    hospitality=core.coreHelper.loadDynamicComponent("datacontroller/InflightMenu2.qml", dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStageLoadHospitality,[hospitality.startPreload],[Media.cEventHospitality]);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[hospitality.resetVariables]);
                    hospitality.__setAppAsHospitality();hospitality.__setConfiguration(Media.CabinClassArr,Media.HospitalityConfiguration,Media.cEventHospitality);
                }else if (Media.CompList[x] == "ConnectingGate"){
                    connectingGate= core.coreHelper.loadDynamicComponent("datacontroller/ConnectingGate.qml", dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[connectingGate.resetVariables]);
                }else if (Media.CompList[x] == "KeywordSearch"){
                    keywordSearch= core.coreHelper.loadDynamicComponent("datacontroller/KeywordSearch.qml", dataController);
                    keywordSearch.__setApiId(Media.cApiKeywordSearch);keywordSearch.__setDataController(dataController);
                }else if (Media.CompList[x] == "Survey"){
                    survey= core.coreHelper.loadDynamicComponent("datacontroller/Survey.qml", dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStageLoadSurvey,[survey.startPreload],[Media.CEventSurvey]);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[survey.resetVariables]);
                    interactiveStartup.registerToStage(interactiveStartup.cStageEntOnScreen,[survey.processEntOn]);
                    survey.__setDataController(dataController);survey.__setConfiguration(Media.PreloadSurvey,Media.CEventSurvey,Media.TriggeredSurveyAppendInModel);
                }else if(Media.CompList[x] =="Extv"){
                    extv=core.coreHelper.loadDynamicComponent("datacontroller/Extv.qml",dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[extv.resetVariables]);
                }else if(Media.CompList[x] =="Ans"){
                    ans=core.coreHelper.loadDynamicComponent("datacontroller/Ans.qml",dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[ans.resetVariables]);
                }else if(Media.CompList[x] =="GeniusPlaylist"){
                    geniusPlaylist=core.coreHelper.loadDynamicComponent("datacontroller/GeniusPlaylist.qml",dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[geniusPlaylist.resetVariables]);
                }else if(Media.CompList[x] =="ContentRating"){
                    contentRating=core.coreHelper.loadDynamicComponent("datacontroller/ContentRating.qml",dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[contentRating.resetVariables]);
                }else if(Media.CompList[x] =="Pdi"){
                    pdi=core.coreHelper.loadDynamicComponent("datacontroller/Pdi.qml",dataController);
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[pdi.resetVariables]);
                }else if (Media.CompList[x] == "CDCount"){
                    interactiveStartup.registerToStage(interactiveStartup.cStageLoadRouteData3,[__getCDCount]);
                }else if(Media.CompList[x] =="ContentStaging"){
                    contentStaging=core.coreHelper.loadDynamicComponent("datacontroller/ContentStaging.qml",dataController);
//                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[contentRating.resetVariables]);
                }else if (Media.CompList[x] == "WoiApps"){
                    __loadWoiApps();
                }
            }
        }

    }

    function __loadWoiApps(){
        if(Media.WoiAppList){
                var appString = "import QtQuick 1.1; QtObject{"
                for(var y in Media.WoiAppList){
                    appString += "property variant "+Media.WoiAppList[y]+" :WoiApp{} ";
                }
                appString+="}"
                woiApps = Qt.createQmlObject(appString,dataController);
                for(var x in Media.WoiAppList){
                    var func = eval("dataController.woiApps."+Media.WoiAppList[x]+".resetVariables");
                    interactiveStartup.registerToStage(interactiveStartup.cStagePreCloseFlight,[func]);
                    var func2 = eval("dataController.woiApps."+Media.WoiAppList[x]+".__setConfiguration");
                    func2(Media.CabinClassArr,Media.WoiAppConfiguration[y],Media.WoiAppResolutionMap,Media.WoiAppIsoMap,Media.MenuSDKVersion,Media.WoiAppList[x],Media.WoiAppCustomConfiguration[x],Media.WoiAppAdvResolutionMap[x]);
                }
        }
    }

    function __resetMediaVariables(){
        core.info("DataController | resetMediaVariables | setting Default values ");
        Media.SeatRating = 254;
        var lrudata = core.__getLruData();
        Media.AircraftType=lrudata.Aircraft_Type?lrudata.Aircraft_Type:'';
        Media.AircraftSubType = lrudata.Aircraft_SubType?lrudata.Aircraft_SubType:'';
    }
    Component.onCompleted: {
        interactiveStartup.registerToStage(interactiveStartup.cStageLoadNonRouteData,[__loadNonRouteData],[Media.cApiMediaDbRefresh]);
        interactiveStartup.registerToStage(interactiveStartup.cStageLoadRouteData,[Media.getRouteConfiguration],[Media.cApiRouteConfiguration]);
        interactiveStartup.registerToStage(interactiveStartup.cStageLoadRouteData2,[__loadRouteData],[Media.cApiRouteData])
        interactiveStartup.registerToStage(interactiveStartup.cStagePreloadData,[__preloadData]);
        interactiveStartup.registerToStage(interactiveStartup.cStageLoadDynamicComponents,[__loadDynamicComponents]);
        interactiveStartup.registerToStage(interactiveStartup.cStageCloseFlightScreen,[mediaServices.__resetVariables,__resetMediaVariables(),destroySqlModel('lookupModel',1)]);
        interactiveStartup.eventHandler(interactiveStartup.cDataControllerLoaded);
    }

    MediaServices{ id:mediaServices;Component.onCompleted: {mediaServices.__setDataController(dataController);}}
}
