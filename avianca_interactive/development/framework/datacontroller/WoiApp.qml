import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store
DataLoader{
    id: woiApp
    signal __sigModelReady(variant model,bool status);
    signal sigLangChange(bool status);
    property int requestId: 0;
    property variant menusdk;
    property string currentParent:'';
    property string currentSynopsisNode:'';
    property string appName: '';


    Connections{
        target:menusdk?menusdk:null;
        onInitComplete:{
            core.info("WoiApp.qml | initComplete() signal received errorCode = "+errorCode);
            if(errorCode == 0){
                isAppActive=false;__sigModelReady(blankModel,true);
            }else{
                isAppActive=false;__sigModelReady(blankModel,false);
            }

        }
        onFileNodePropertyReceived:{
            if(!properties.error){
                core.info("WoiApp.qml | fileNodePropertyReceived() signal received");
                var data=[properties];
                __processModel(data,'fromNodePropertyAPI',requestId);
            }else{
                core.error("WoiApp.qml | fileNodePropertyReceived() signal ERROR = "+properties.error+"  received");
                isAppActive=false; __sigModelReady(blankModel,false);
            }
        }
        onFileNodeMetaDataReceived:{
            if(!metadata.error){
                core.info("WoiApp.qml | fileNodeMetaDataReceived() signal received");
                var data=[metadata];
                  __processModel(data,'fromMetaDataAPI',requestId);
                if(requestId==2){
                    menusdk.getFileProperties(currentSynopsisNode)
                }
            }else{
                core.error("WoiApp.qml | fileNodeMetaDataReceived() signal ERROR "+metadata.error+" received");
                isAppActive=false; __sigModelReady(blankModel,false)
            }
        }
        onLanguageChanged: {
            if(errorCode==0){
                core.info("WoiApp.qml | MenuLanguageChanged() signal received");
                if(Store.dmodel.length && Store.callbackArr.length){
                    __updateModelOnLangChange(Store.dmodel,Store.callbackArr);
                }else{
                    isAppActive=false; sigLangChange(true)
                }
            }else{
                core.error("WoiApp.qml | MenuLanguageChanged() signal ERROR = "+errorCode+" received");
                isAppActive=false; sigLangChange(false)
            }
        }
        onMetaDataReceived:{
            if(!metadata.error){
                core.info("WoiApp.qml | MetaDataReceived() signal received");
                var data=[metadata];
                core.coreHelper.printContent(metadata)
                __processModel(data,'fromAPI',requestId)
            }else{
                core.error("WoiApp.qml | MetaDataReceived() signal ERROR = "+metadata.error+" received");
                __processModel([],'fromAPI',requestId)
        	}
    	}
	}

    //public functions

    function appInit(flightId,source,destination,language,callback,inputdate){
        if(menusdk==null){core.info('WoiApp.qml | appInit |App cannot be initiated as menusdk is not loaded ');return false;}
        if (isAppActive==true){core.info('WoiApp.qml | appInit | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        __updateCallback(callback);
        core.info('WoiApp.qml | appInit called');
        requestId=1;
        var pathToMenu='/tmp/interactive/content/'+appName;
        var lang_code = Store.isoMap[language]?Store.isoMap[language]:Store.isoMap[settings.defaultISO];
        var cabin_class = Store.cabinClass[core.pif.getCabinClassName()];
        var currentFlightTime = inputdate?Math.round(inputdate/1000.0): Math.round(new Date().getTime()/1000.0); // parseInt(d.toUTCString());
        var exceptionMenuPath = '/tmp/interactive/content/'+appName+'_exmenu';
        core.info('WoiApp.qml | appInit | currentFlightTime:'+currentFlightTime+' |flightId:'+flightId+' |source:'+source+'|destination:'+destination+' |lang_code:'+lang_code+'|pathToMenu:'+pathToMenu+'|menuSdkVersion:'+Store.menuSdkVersion+'|cabin_class:'+cabin_class+'|exceptionMenuPath:'+exceptionMenuPath+'| customConfigData ='+Store.customConfigData);
        menusdk.init(flightId,source,destination,lang_code,pathToMenu,Store.menuSdkVersion,currentFlightTime,cabin_class,exceptionMenuPath,Store.customConfigData);
        return true;
    }



    function appLangChange(isocode,dmodel,callback){
        if(menusdk==null){core.info('WoiApp.qml | appLangChange |App cannot be initiated as menusdk is not loaded ');return false;}
        if (isAppActive==true){core.info('WoiApp.qml | appLangChange | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        Store.synopsisArr=[];
        var menuInfo=menusdk.getMenuInfo()
        var default_lang =  menuInfo.defaultLanguage;
        var avail_languages=menuInfo.languages
        core.info("WoiApp.qml | avail langs : "+avail_languages+" | isocode passed = "+isocode+"| Store.isoMap[isocode] ="+Store.isoMap[isocode])
        isocode = isocode.toLowerCase();
        var lang_code = Store.isoMap[isocode]?Store.isoMap[isocode]:default_lang;
        lang_code=(avail_languages.indexOf(lang_code)>0)?lang_code:default_lang;
        core.info('WoiApp.qml | appLangChange | lang_code = '+lang_code)
        Store.dmodel = [];Store.dmodel=dmodel;
        Store.callbackArr = [];Store.callbackArr = callback;
        menusdk.changeLanguage(lang_code);
    }


    function getNodeDataById(nodekey,callback){
        if(menusdk==null){core.info('WoiApp.qml | getNodeDataById |App cannot be initiated as menusdk is not loaded ');return false;}
        if (isAppActive==true){core.info('WoiApp.qml | getNodeDataById | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        __updateCallback(callback);
        core.info('WoiApp.qml | getNodeDataById | nodeId = '+nodekey)
        if(nodekey<=0){
            if(rootNodeModel.count){
                 isAppActive=false;
                __sigModelReady(rootNodeModel,true)
            }else{
                var arr = [];
                arr['data']=[menusdk.getRootNode()]
                rootNodeModel.source = arr['data'];
                isAppActive=false;
                __sigModelReady(rootNodeModel,true);
            }
        }else{
            var arr = [];
            arr['data']=[menusdk.getNode(nodekey)]
            treeNodesModel.source = [];
            treeNodesModel.source=arr['data']
            isAppActive=false;
            __sigModelReady(treeNodeByIdModel,true)
        }

    }

    function getNodeObjectById(nodekey){
         if(menusdk==null){core.info('WoiApp.qml | getNodeObjectById |App cannot be initiated as menusdk is not loaded ');return false;}
         if(typeof nodekey=='undefined'){ core.info('WoiApp.qml | getNodeObjectById | invalid nodeId = '+nodekey);return flase}
        core.info('WoiApp.qml | getNodeObjectById | nodeId = '+nodekey);
        if(nodekey<=0){
                var arr = [];
                arr=menusdk.getRootNode()
                return arr;
        }else{
            var arr = [];
            arr=menusdk.getNode(nodekey)
            return arr;
        }
    }

    function getNodeObjectsByAttribute(keyName,value){
        if(menusdk==null){core.info('WoiApp.qml | getNodeObjectsByAttribute |App cannot be initiated as menusdk is not loaded ');return false;}
        if(typeof keyName=='undefined'){ core.info('WoiApp.qml | getNodeObjectsByAttribute | invalid key = '+keyName);return flase}
       core.info('WoiApp.qml | getNodeObjectsByAttribute | nodeId = '+keyName);
       if(keyName && value){
               var arr = [];
               arr=menusdk.getNodeObjectsByKey(keyName,value)
               return arr;
       }
    }

    function getNodesWithMetadataById(nodekeys,callback){
         if(menusdk==null){core.info('WoiApp.qml | getNodesWithMetadataById |App cannot be initiated as menusdk is not loaded ');return false;}
        if (isAppActive==true){core.info('WoiApp.qml | getNodesWithMetadataById | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        __updateCallback(callback);requestId = 4;
        core.info('WoiApp.qml | getNodesWithMetadataById | nodeId = '+nodekeys)
        currentParent='';
         __processChildList(nodekeys);

    }

    function getNextList(parentId,callback,limit){
         if(menusdk==null){core.info('WoiApp.qml | getNextList |App cannot be initiated as menusdk is not loaded ');return false;}
        //limit["lower"] limit["upper"]
        if (isAppActive==true){core.info('WoiApp.qml | getNextList | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        __updateCallback(callback);
        requestId = 3
        core.info("WoiApp.qml | getNextList | parentId = "+parentId);
        if(parentId<0){
            if(typeof menusdk.getMenuInfo().rootId == 'undefined'){
                core.info("WoiApp.qml | getNextList | rootNode not found")
                isAppActive=false; __sigModelReady(blankModel,false);return false;
            }
            var rootId = menusdk.getMenuInfo().rootId;
            var rootnode=[];
            currentParent=rootId;
        }else if( parentId==null || menusdk.getNode(parentId).nodeType == 'fileNode'){
            core.info("WoiApp.qml | getNextList | node sent : "+parentId+" is a leafnode or invalid")
            isAppActive=false; __sigModelReady(blankModel,false);return false;
        }else{
            currentParent=parentId;
        }
        Store.treeChildArr=[];var defaultClass = menusdk.getMenuInfo().defaultCabinClass;var childlist=[];
        if(defaultClass==undefined){
            var node = menusdk.getNode(menusdk.getMenuInfo().rootId)
            if(node.cabinClass.length>0){
                childlist = menusdk.getFilteredChildKeys(currentParent,'cabinClass',Store.cabinClass[core.pif.getCabinClassName()]);
            }else{
                childlist=menusdk.getNode(currentParent).childList;
            }
        }else{
            if(defaultClass=='_NONE'){
                childlist=menusdk.getNode(currentParent).childList;
            }else if(defaultClass=='_UNAVAILABLE'){
                childlist = menusdk.getFilteredChildKeys(currentParent,'cabinClass',Store.cabinClass[core.pif.getCabinClassName()]);
            }else{
                childlist = menusdk.getFilteredChildKeys(currentParent,'cabinClass',Store.cabinClass[core.pif.getCabinClassName()]);
                if(!childlist.length){
                    childlist = menusdk.getFilteredChildKeys(currentParent,'cabinClass',defaultClass);
                }
            }
        }
        var newChildList=childlist;
        if(typeof limit!='undefined'){
            if(limit["upper"] && (typeof limit['lower']=='undefined' || limit['lower']=='' || limit['lower']==0)){
                newChildList= newChildList.slice(0,limit["upper"]);
            }else if(limit["upper"] && limit['lower']){
                newChildList= newChildList.slice(limit['lower'],limit["upper"]);
            }
        }
       //debug :  console.log("WoiApp.qml | getNextList |  childlist : "+ childlist);
        __processChildList(newChildList);
    }

    function getPreviousCategoryList(nodeId,callback,limit){
         if(menusdk==null){core.info('WoiApp.qml | getPreviousCategoryList |App cannot be initiated as menusdk is not loaded ');return false;}
        if (isAppActive==true){core.info('WoiApp.qml | getNextList | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        core.info("WoiApp.qml | getNextList | node : "+nodeId);
        if(menusdk.getNode(nodeId).nodeType=='fileNode'){
            core.info("WoiApp.qml | getNextList | node sent : "+nodeId+" is a leafnode cant fetch parent")
            isAppActive=false; __sigModelReady(blankModel,false);return false;
        }
        var currParent = (typeof menusdk.getNode(nodeId).parentId != 'undefined'&& menusdk.getNode(nodeId).parentId!='' )?menusdk.getNode(nodeId).parentId:'';
        if(currParent!=''){
            isAppActive=false;getNextList(currParent,callback,limit);
        }else{
            core.info("WoiApp.qml | getNextList | node sent : "+nodeId+" does not have previous list")
            isAppActive=false; __sigModelReady(blankModel,false);return false;
        }

    }

    function getLeafNodeSynopsis(nodeId,callback){
         if(menusdk==null){core.info('WoiApp.qml | getLeafNodeSynopsis |App cannot be initiated as menusdk is not loaded ');return false;}
        if (isAppActive==true){core.info('WoiApp.qml | getLeafNodeSynopsis | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        __updateCallback(callback);
        core.info("WoiApp.qml | getLeafNodeSynopsis | nodeId = "+nodeId);
        if(menusdk.getNode(nodeId).nodeType == 'fileNode'){
            requestId = 2; currentSynopsisNode=nodeId;
            if(typeof Store.synopsisArr[nodeId]=='undefined'){
                Store.synopsisArr[nodeId]=[];
            }else{
                var val = __processArr(Store.synopsisArr[nodeId],'syn')
                synopsis.source = val;
                isAppActive=false; __sigModelReady(synopsis,true);return true;
            }
            menusdk.getMetaData(currentSynopsisNode,1);
        }else{
            core.info("WoiApp.qml | getLeafNodeSynopsis | nodeId = "+nodeId+" is not a fileNode");
            isAppActive=false; __sigModelReady(blankModel,false);return false;
        }
    }


    //private function
    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }
    function __processChildList(childlist,callback){
        if(callback){__updateCallback(callback);}
        if(childlist.length){
            for(var key in childlist){
                if(typeof Store.treeChildArr[childlist[key]] =='undefined'){
                    Store.treeChildArr[childlist[key]]=[];
                    Store.treeChildArr[childlist[key]]['callType']='getNext';
                    Store.treeChildArr[childlist[key]]['parentId']=(Store.treeChildArr[childlist[key]]['parentId']!=''&& Store.treeChildArr[childlist[key]]['parentId'])?Store.treeChildArr[childlist[key]]['parentId']:currentParent;
                }
            }
            var data=[];
            var nodesdata= menusdk.getNodes(childlist);
            var newchildlist = [];
            for(var k in nodesdata){
                if(nodesdata[k]['nodeType']!='rootNode'){newchildlist.push(k)}
                for(var k1 in nodesdata[k]){
                    Store.treeChildArr[k][k1]=nodesdata[k][k1];                    
                }
            }
            if(newchildlist.length){menusdk.getMetaDataOfNodes(newchildlist,1);}
            else{
                __processModel([],'fromAPI',requestId)
            }
        }else{
            core.info("WoiApp.qml | __processChildList | childList empty ");
            isAppActive=false; __sigModelReady(blankModel,false);return false;
        }
    }
    function __processModel(value,datafrom,requestId){
        if(requestId==3 || requestId==4 || requestId==5){
            var treenode = false;
            var leafnode = false;
            if(value.length){
                for(var key in value[0]){
                    for(var k in value[0][key]){
                        if(Store.treeChildArr[key]['nodeType']=='fileNode'){leafnode=true}else {treenode=true}
                         __populateModelArray(value[0][key],'getNext',Store.treeChildArr[key]['nodeType'],key);
                    }
                }
            }
            var tempArr = __processArr(Store.treeChildArr,'getnext');
            Store.treeChildArr=[];
            if(leafnode){
                leafNodesModel.source = tempArr;
                isAppActive=false; __sigModelReady(leafNodesModel,true);
            }else if(requestId==4){
                nodesWithMetaDataModel.source  = tempArr;
                isAppActive=false; __sigModelReady(nodesWithMetaDataModel,true);
            }else{
                treeNodesModel.source = tempArr;
                isAppActive=false; __sigModelReady(treeNodesModel,true);
            }

        }else if(requestId == 2){
            if(datafrom=='fromMetaDataAPI'){
                if(value.length){
                    __populateModelArray(value[0],'synopsis');
                    return true;
                }else{
                    isAppActive=false;  __sigModelReady(blankModel,true);
                }
            }else{
                Store.synopsisArr[currentSynopsisNode]['callType']='synopsis';
                Store.synopsisArr[currentSynopsisNode]['nodeId']=currentSynopsisNode;
                for(var key1 in value[0]){
                        Store.synopsisArr[currentSynopsisNode][key1]=value[0][key1];
                }
                var val = __processArr( Store.synopsisArr[currentSynopsisNode],'syn');
                if(val.length){
                    synopsis.source = val;
                    isAppActive=false;  __sigModelReady(synopsis,true);
                }else{
                      isAppActive=false;  __sigModelReady(blankModel,true);
                }
            }
        }
    }

    function __updateModelOnLangChange(modelArr,callbackArr){
        for(var i = 0; i< modelArr.length; i++){
            if(modelArr[i].getValue(0,'callType')=='synopsis'){
                isAppActive=false;getLeafNodeSynopsis(modelArr[i].getValue(0,'nodeId'),callbackArr[i])
            }else{
                var parent = modelArr[i].getValue(0,'parentId')!=''?modelArr[i].getValue(0,'parentId'):'';
                var childlist = [];
                Store.treeChildArr=[];requestId=5;
                for(var l=0;l<modelArr[i].count;l++){
                    var obj = modelArr[i].at(l)
                    childlist.push(obj.nodeId)
                    Store.treeChildArr[obj.nodeId]=[];
                    Store.treeChildArr[obj.nodeId]['callType']='getNext';
                    Store.treeChildArr[obj.nodeId]['parentId']=obj.parentId;
                }
                isAppActive=false;__processChildList(childlist,callbackArr[i])
            }
        }
    }
    function __processConfig(responseObj,responseString,requestId){
        if(requestId==1){
            callAppInit(responseObj)
        }
    }
    function resetVariables(){
        core.info('WoiApp.qml | resetVariables Called ');
        rootNodeModel.source=[];
        treeNodeByIdModel.source=[];
        treeNodesModel.source=[];
        leafNodesModel.source=[];
        synopsis.source=[];
        nodesWithMetaDataModel.source=[];

    }

    function __populateModelArray(value,type,nodeType,currentKey){
        var fieldArrText = (type=='synopsis')?Store.fieldsArr['synopsis']:(nodeType=='treeNode')?Store.fieldsArr['nextListTree']:Store.fieldsArr['nextListLeaf'];
        var fieldArrImage = (type=='synopsis')?Store.fieldsArr['synopsisImage']:(nodeType=='treeNode')?Store.fieldsArr['nextListTreeImage']:Store.fieldsArr['nextListLeafImage'];
        var modelArr =[];
        for(var key in value){
            for(var k in value[key]){
                if(key=="texts"){
                    if(!(fieldArrText.indexOf(k)<0)){
                        var resindex = Store.resolutionAdvMapArr.length?0:__returnResolutionIndex(value[key][k],k);
                        for(var t in value[key][k][resindex]){
                        var new_key = k+"_"+t;
                            modelArr[new_key.toLowerCase()]=value[key][k][resindex][t];
                        }
                    }
                }
                else if(key=="images"){
                    if(!(fieldArrImage.indexOf(k)<0)){
                        var resindex = __returnResolutionIndex(value[key][k],k);
                        if(typeof resindex == 'object' && resindex.length > 1){
                            var cnt =0;modelArr[k]=[];
                            for(var index in resindex){
                                  modelArr[k][cnt]={};
                                for(var t in value[key][k][resindex[index]]){
                                    var new_key = k+"_"+t;
                                    modelArr[k][cnt][new_key.toLowerCase()]=value[key][k][resindex[index]][t];
                                }
                                cnt++;
                            }
                        }else{
                            for(var t in value[key][k][resindex]){
                                var new_key = k+"_"+t;
                                modelArr[new_key.toLowerCase()]=value[key][k][resindex][t];
                            }
                        }
                    }
                }
            }
        }
        if((type=='synopsis')){
            Store.synopsisArr[currentSynopsisNode]=modelArr;
        }else{
            for(var i in modelArr){
                Store.treeChildArr[currentKey][i]=modelArr[i];
            }
        }
        return true;
    }


    function __returnResolutionIndex(value,type){
        var resindex = 0;var resIndices=[];
        if(Store.resolutionMapArr.length){
            for(var x in value){
                if(value[x]["attribute"] == Store.resolutionMapArr[core.settings.resolutionIndex]){
                    resindex=x;resIndices.push(x);
                }
            }
            if(resIndices.length>1){
                return resIndices
            }
            return resindex;
        }else if(Store.resolutionAdvMapArr){

            for(var x in value){
                var key = "'"+core.settings.resolutionIndex+'-'+type+"'"
               
                if(value[x]["posterSize"] == Store.resolutionAdvMapArr[key]){
                    resindex=x;resIndices.push(x);
                }
            }
            if(resIndices.length>1){
                return resIndices
            }
            return resindex;
        }
    }

    function __processArr(val,type){
        var tempArr = [];var cnt = 0;
        if(type=='syn'){
            tempArr[cnt]={};
            for(var j in val){
                tempArr[cnt][j]=val[j];
            }
        }else{
            for (var i in val){
                tempArr[cnt]={};
                tempArr[cnt]['nodeId']=i;
                for(var j in val[i]){
                    tempArr[cnt][j]=val[i][j];
                }
                cnt++
            }
        }
        return tempArr;
    }

    function __resetInFallbackMode(){
        core.info('WoiApp.qml | __resetInFallbackMode | in fallback mode returning blank model');
        __sigModelReady(false);
    }

    function __setConfiguration(cabinClass,configurationData,resolutionMap,isoMap,menusdkversion,app_name,customConfigData,advResolutionMap){
       core.info("WoiApp.qml | __setConfiguration  menusdkversion : "+typeof menusdkversion+" "+menusdkversion[0]);
        Store.resolutionAdvMapArr=advResolutionMap?advResolutionMap: Store.resolutionAdvMapArr;
       
        if(configurationData){
            appName=app_name?app_name:'';
            Store.cabinClass=cabinClass?cabinClass:Store.cabinClass;
            Store.isoMap = isoMap?isoMap:Store.isoMap;
            Store.resolutionMapArr=resolutionMap?resolutionMap: Store.resolutionMapArr;
            Store.fieldsArr['nextListTree']= configurationData['nextListTree']?configurationData['nextListTree']:'';
            Store.fieldsArr['nextListTreeImage']= configurationData['nextListTreeImage']?configurationData['nextListTreeImage']:'';
            Store.fieldsArr['nextListLeaf']= configurationData['nextListLeaf']?configurationData['nextListLeaf']:'';
            Store.fieldsArr['nextListLeafImage']= configurationData['nextListLeafImage']?configurationData['nextListLeafImage']:'';
            Store.fieldsArr['synopsis']= configurationData['synopsis']?configurationData['synopsis']:'';
            Store.fieldsArr['synopsisImage']= configurationData['synopsisImage']?configurationData['synopsisImage']:'';
            Store.customConfigData=customConfigData;

            return true;
        }
    }
    function loadComponents(){
        core.info("WoiApp.qml | loadComponents  menusdkversion : "+Store.menuSdkVersion);
        if(core.pc==true){
            if(parseInt(Store.menuSdkVersion)>=4){
                console.log("Loading components/menusdk_v4")
                menusdk = core.coreHelper.loadDynamicComponent("components/menusdk_v4/MenuSDK.qml",woiApp);
            }else{
                menusdk = core.coreHelper.loadDynamicComponent("components/menusdk_v3/MenuSDK.qml",woiApp);
            }
        }else{
            menusdk = core.coreHelper.loadDynamicComponent("/tmp/menuSDK/MenuSDK.qml",woiApp);
        }
    }

    JsonModel {id: rootNodeModel}
    JsonModel {id: treeNodeByIdModel}
    JsonModel {id: treeNodesModel}
    JsonModel {id: nodesWithMetaDataModel}
    JsonModel {id: leafNodesModel}
    JsonModel {id: synopsis}
    JsonModel {id: blankModel}
    Component.onCompleted: {
        Store.treeChildArr=[];
        Store.fieldsArr=[];
        Store.cabinClass=[];
        Store.isoMap = [];
        Store.synopsisArr=[];
        Store.resolutionMapArr=[];
		Store.customConfigData = {};
        Store.menuSdkVersion= dataController.__getMenuSDKVersion();
        Store.resolutionAdvMapArr= [];
        loadComponents()
        core.info("WoiApp.qml | Component.onCompleted | WoiApp LOAD COMPLETE");
    }

}

