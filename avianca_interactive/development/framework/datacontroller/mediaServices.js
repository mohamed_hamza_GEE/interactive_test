/*Public function for look up tag_name, custom service*/
var tagLookUp = [];
var serviceLookUp = [];
var paxusLookUp = [];
var blockCidList = "";
var midBlockList = "";
var cidLookUp = [];
var showBlockCid = 'false';
var showBlockMid = 'false';
var blockCidArr = [];
var serviceCodeLookup = [];
var cidSmidArr = [];
var serviceCodeStatus = [];
var cidList = [];
var isParentBlockRequired = false;
var templateIdMap = [];
var cApiRouteData = 0;
var bundleLookup = [];
var payMidArr = [];
var refundMidData = [];
var isMidRefundCalled = false
var blockCidListPreOpenFlight = "";
var blockCidPreOpenFlight = 'false';
var exceptionParentBlockTemplates = [];
var cidListByBundleEnabled = 'false';
var AttrForPaxus = '';
var PreloadCategoryPosters = false;
var CustomPosterLabels = [];

function  __getServiceVals(cid, accessType, isXmlQuery, smid,servicecode ){//isXmlQuery should be set to 2 on open flight
    //__setCidAccessType(accessType,cid,isXmlQuery);
    __setServiceCodeLookup(cid,smid,servicecode);
    mediaServices.__setServiceAccessType(accessType,smid,cid,servicecode,isXmlQuery);
}

function __setCidAccessType(accessType,cid,isXmlQuery) {
    core.info("MediaServices | __setCidAccessType | cid "+cid+" | accessType "+accessType+" | isXmlQuery "+isXmlQuery)
    if(cid != undefined){
        __blockUnblockCid(cid,isXmlQuery,accessType)
//               core.info("MediaServices | __setCidAccessType | FINAL CID LIST : "+blockCidList)
        __sendBlockedCids(blockCidList);
        if(isParentBlockRequired && cidLookUp[cid]){__setParentCidAccessType(cidLookUp[cid]['pcid'],accessType);}
          //mediaServices.sigCidBlockChanged(cidList,accessType,0,'');
        return blockCidList;
    }
}

function __setParentCidAccessType(PkeyCid,accessType){
    if(!isParentBlockRequired){return false;}
    if(PkeyCid == -1 || PkeyCid == ""){return false;}
    else{
        var temparr = new Array();
        var chk = 0;
        var temp = []
        if(cidLookUp[PkeyCid]){temp =cidLookUp[PkeyCid]['childlist'];
            //            core.info("MediaServices | __setParentCidAccessType | Blocked parent cid to check to be blocked= "+ PkeyCid+ " of childlist " + temp)
            if(temp.length > 0)
                temparr = temp.split(',');
            for (var mykey=0; mykey < temparr.length; mykey++) {
                //                core.info("MediaServices | __setParentCidAccessType | temparr "+temparr[mykey] +" key "+ mykey + " blocked "+cidLookUp[parseInt(temparr[mykey])]['blocked']);
                if( cidLookUp[parseInt(temparr[mykey],10)]['blocked']==1){
                    //                     core.info("MediaServices | __setParentCidAccessType | Check"+chk);
                    chk++}
                //  else{break;}
            }
            if(exceptionParentBlockTemplates.indexOf(cidLookUp[PkeyCid]['tid'])<0){
                if(chk==temparr.length && temparr.length!=0){
                    cidLookUp[PkeyCid]['blocked'] = 1 //update parentcid's entry in paxus array with blocked=1
                    __blockUnblockCid(PkeyCid,1,accessType)
                    core.info("MediaServices | __setParentCidAccessType | Blocked parent cid = "+ PkeyCid+ " of childlist " + temp)
                    __setParentCidAccessType(cidLookUp[PkeyCid]['pcid'], accessType); //Recursive call to check its parent
                }
                else if(cidLookUp[PkeyCid]['blocked'] == 1){
                    cidLookUp[PkeyCid]['blocked'] = 0
                    __blockUnblockCid(PkeyCid,0,accessType)
                    core.info("MediaServices | __setParentCidAccessType | Unblocked parent cid = "+ PkeyCid+ " of childlist " + temp)
                    __setParentCidAccessType(cidLookUp[PkeyCid]['pcid'],accessType); //Recursive call to check its parent
                }
            }

        }
    }
    delete temparr;delete chk;delete temp;delete mykey;
}

function __blockUnblockCid(cid,status,accessType){
    if(accessType == 'blocked'){
        if(!blockCidList){
            blockCidList = "'"+cid+"'"+',';
        }else{
            if(blockCidList.search("'"+cid+"'") == -1 ) {        //Do not repeat CIDs in the comma separated list
                blockCidList = blockCidList+"'"+cid+"'"+",";
            }
        }
        if(!blockCidListPreOpenFlight && blockCidPreOpenFlight == true && status==2){
            blockCidListPreOpenFlight= "'"+cid+"'"+',';
        }else if(blockCidListPreOpenFlight.search("'"+cid+"'") == -1 && blockCidPreOpenFlight == true && status==2) {        //Do not repeat CIDs in the comma separated list
            blockCidListPreOpenFlight = blockCidListPreOpenFlight+"'"+cid+"'"+",";
        }
        if(isParentBlockRequired && cidLookUp[cid]){cidLookUp[cid]['blocked'] = status;}
        blockCidArr[cid] = status;
    }
    else{
        //Unblocked CIDs
        if(blockCidArr[cid]){delete blockCidArr[cid];}
        if(isParentBlockRequired && cidLookUp[cid]){cidLookUp[cid]['blocked'] = status;}
        if(blockCidList.search("'"+cid+"'") != -1 ){
            blockCidList = __filterMidFromCsv(blockCidList, cid);
        }
        if(blockCidListPreOpenFlight.search("'"+cid+"'") != -1 && blockCidPreOpenFlight == true){
            blockCidListPreOpenFlight = __filterMidFromCsv(blockCidListPreOpenFlight, cid);
        }
    }
}

function __filterMidFromCsv(blockList, node) { // Remove blocked cid or MID from csv once they get unblocked
    var values = blockList.split(",");
    for(var i = 0 ; i < values.length ; i++){if(eval(values[i]) == node){values.splice(i, 1);return values.join(",");}}
    delete values;delete i;
    return blockList;
}

function __sendBlockedCids(){if(blockCidPreOpenFlight==true){return blockCidListPreOpenFlight};if(showBlockCid == true){/*blockCidList = '';*/ return '';}  return blockCidList;}    // Empty the blocked category list as we need to display blocked CID

function __sendBlockedMids(){if(showBlockMid == true){/*midBlockList = '';*/ return '';} return midBlockList;}    // Empty the blocked mid list as we need to display blocked MID

function __updateCidBlocked(){
    var getBlockedCids = __sendBlockedCids();
    if(getBlockedCids){dataController.__setCidBlockList(getBlockedCids.slice(0, -1).replace(/'/g,""));}else {dataController.__setCidBlockList('');}
    delete getBlockedCids;
    return true;
}

function __updateMidBlocked(){
    var getBlockedMids = __sendBlockedMids();
    if(getBlockedMids){dataController.__setMidBlockList(getBlockedMids.slice(0, -1).replace(/'/g,""));}else {dataController.__setMidBlockList('');}
    delete getBlockedMids;
    return true;
}

function __setServiceCodeLookup(cid,smid,servicecode){
    if(serviceCodeLookup[smid] ==  undefined){
        serviceCodeLookup[smid]      = []
        serviceCodeLookup[smid][cid] =  servicecode
    }else {
        serviceCodeLookup[smid][cid] =  servicecode
    }
}
function _getChildList(model, pcid){
    var temp = "";
    for(var i=0; i<model.count; i++){
        if(pcid == model.getValue(i,'parentcid')){
            if(!(temp.length != 0 )){temp += model.getValue(i,'cid')+""}
            else{temp += ","+model.getValue(i,'cid')+""}
        }
    }
    delete temp;delete i;
    return temp;
}
