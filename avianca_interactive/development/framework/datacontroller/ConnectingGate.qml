import QtQuick 1.1
import "../blank.js" as Store

Item{
    id: screen

    property bool cgDataStatus:false;
    property bool appIsActive:false;
    property int  cgdata    :0;
    property bool cgStatus  :true;
    property int errorType :0;
    property int maxRetry  :0;
    property int loadId    :0;
    property string cgxpath:"/response/data/sector";
    property string agxpath:"/response/data";
    property bool connectingFlightReponse: false
    property bool arrivalDetailsReponse: false

    signal sigCgStatusChanged(bool status);
    signal __sigModelReady(variant model, bool status,string modelID,int errNum);

    Connections{
        target:core.pif;
        onCgDataAvailableChanged: {
            core.info("ConnectingGate.qml  | onCgDataAvailable | cgDataAvailable :  "+cgDataAvailable+" | cgStatus: "+cgStatus);resetVariables();cgdata = cgDataAvailable;
            if(cgdata>0 && cgStatus){sigCgStatusChanged(true);}else {sigCgStatusChanged(false);}
        }
        onSigCgStatusChanged:{core.info("ConnectingGate.qml  | onCgDataAvailable | cgDataAvailable :"+status);cgStatus=(status=="ENABLE")?true:false;var tmp=cgdata;resetVariables();cgdata =tmp;sigCgStatusChanged(cgStatus);}
    }

    //public function
    function getCgStatus(){return (cgdata && cgStatus)?true:false;}
    function getCgModel(){return cgModel;}
    function getCgModelByLang(){return cgModelByLang;}
    function getArrivalInfoModel(){return arrivalInfoModel;}
    function getAirportByIataModel(){return airportByIataModel;}

    function fetchData(callback){

        //cgdata = parseInt(core.pif.getCgDataStatus(),10);
        core.info('ConnectingGate.qml | fetchData | cgdata : '+cgdata+' | cgModel.count : '+cgModel.count+' | arrivalInfoModel : '+arrivalInfoModel.count+' | cgDataStatus : '+cgDataStatus+' | cgStatus : '+cgStatus);
        if(cgdata>0 && cgStatus && ((cgModel.count==0 || arrivalInfoModel.count==0 ) || !cgDataStatus )){
            __updateCallback(callback);
            if(retry.running==true){retry.stop()}
            maxRetry = 0;
            errorType= 0;
            loadId   = 0;
            appIsActive = false
            __updateCgAgSource();
       }else {
           core.info('ConnectingGate.qml | fetchData | Not Fetching Data');
            if(cgdata<=0 || !cgStatus){__updateCallback(callback);__sigModelReady(cgModel, false,'cgModel',3);__disconnectCallback()}
       }
    }

    function fetchDataByInteractiveLanguage(callback){

        //cgdata = parseInt(core.pif.getCgDataStatus(),10);
        core.info('ConnectingGate.qml | fetchDataByInteractiveLanguage | cgdata : '+cgdata+' | cgModel.count : '+cgModelByLang.count+' | arrivalInfoModel : '+arrivalInfoModel.count+' | cgDataStatus : '+cgDataStatus+' | cgStatus : '+cgStatus);
        if(cgdata>0 && cgStatus){
            __updateCallback(callback);
            if(retry.running==true){retry.stop()}
            maxRetry = 0;
            errorType= 0;
            loadId   = 0;
            appIsActive = false
            __updateCgAgSourceByLang();
       }else {
           core.info('ConnectingGate.qml | fetchData | Not Fetching Data');
            if(cgdata<=0 || !cgStatus){__updateCallback(callback);__sigModelReady(cgModelByLang, false,'cgModel',3);__disconnectCallback();}
       }
    }

    function sortCGData(params,callback){
        core.info("ConnectingGate.qml | sortCGData| params : "+params);
        __updateCallback(callback);
        var sortBy    = params[0]?params[0]:''
        var sortOrder = (params[1]=='dsc')?'-1':'1'

        var src2 = "";loadId++
        if(dataController.__makeHeadendRequest()){
            src2=core.settings.remotePath+"/cg/index.php?function=getAllConnectingFlightDetailsSortedByKey&params1="+sortBy+"&params2="+sortOrder+"&loadId="+loadId;
        }else {
            src2="../../content/cg/sortCGData_"+params[1]+".xml?loadId="+loadId;
        }
        core.info("ConnectingGate.qml | sortCGData | URL: "+src2);
        cgModel.source="";
        cgModel.source=src2;
    }

    function getAirportByIata(iata_code,callback){
        core.info('ConnectingGate.qml | getAirportByIata | iata_code : '+iata_code);
        __updateCallback(callback);
        if(!iata_code){ core.info('ConnectingGate.qml | getAirportByIata | ERROR | IATA CODE is not set | Not Processing ');return}

        var src2 = "";loadId++;
        if(dataController.__makeHeadendRequest()){
            src2=core.settings.remotePath+"/cg/index.php?function=getAirportByCode&params1="+iata_code+"&loadId="+loadId;
        }else {
            src2="../../content/cg/getAirportByCode_"+iata_code+".xml?loadId="+loadId;
        }

        core.info("ConnectingGate.qml | getAirportByIata | URL: "+src2);
        airportByIataModel.source=src2
    }

    function getAllConnectingFlightDetailsByInteractiveLanguage(callback){
       var interactive_lang =  core.settings.languageISO
        core.info('ConnectingGate.qml | getAllConnectingFlightDetailsByInteractiveLanguage | interactive_lang : '+interactive_lang);
        __updateCallback(callback);
        var src2 = "";loadId++;
        if(dataController.__makeHeadendRequest()){
            src2=core.settings.remotePath+"/cg/index.php?function=getAllConnectingFlightDetailsByLanguage&params1="+(interactive_lang.toUpperCase())+"&loadId="+loadId;
        }else {
            src2="../../content/cg/getAllConnectingFlightDetailsByInteractiveLanguage_"+(interactive_lang.toUpperCase())+".xml?loadId="+loadId;
        }

        core.info("ConnectingGate.qml | getAllConnectingFlightDetailsByInteractiveLanguage | URL: "+src2);
        cgModelByLang.source=src2
    }

    //private functions
    function __getCGSource() {
        var src1 = "";
        if(dataController.__makeHeadendRequest()){
            src1=core.settings.remotePath+"/cg/index.php?function=getAllConnectingFlightDetailsForFlight&loadId="+loadId;
        }else{
             src1="../../content/cg/cginfo.xml?loadId="+loadId;
        }
        core.info("ConnectingGate.qml |  __getCGSource | URL: "+src1);
        return src1;
    }

    function __getCGSourceByLang(){
        var interactive_lang =  core.settings.languageISO;
         var src1 = "";loadId++;
         if(dataController.__makeHeadendRequest()){
             src1=core.settings.remotePath+"/cg/index.php?function=getAllConnectingFlightDetailsByLanguage&params1="+(interactive_lang.toUpperCase())+"&loadId="+loadId;
         }else {
             src1="../../content/cg/getAllConnectingFlightDetailsByInteractiveLanguage_"+(interactive_lang.toUpperCase())+".xml?loadId="+loadId;
         }
          core.info("ConnectingGate.qml |  __getCGSourceByLang | URL: "+src1);
         return src1;
    }

    function __getAGSource() {
        var src2 = "";
        if(dataController.__makeHeadendRequest()){
            src2=core.settings.remotePath+"/cg/index.php?function=getAllArrivalDetailsForFlight&loadId="+loadId;
        }else {
            src2="../../content/cg/arrival.xml?loadId="+loadId;
        }
        core.info("ConnectingGate.qml | __getAGSource| URL: "+src2);
        return src2;
    }

    function __setOnCompleted() {
        core.info ("ConnectingGate.qml | __setOnCompleted | Data Ready");
        appIsActive=false
        maxRetry = 0
        if(retry.running==true){retry.stop()}
    }

    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);Store.callback = '';}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }
    function __disconnectCallback(){if(Store.callback){__sigModelReady.disconnect(Store.callback); Store.callback = '';}}
    function __updateCgAgSource(){
        if (appIsActive==true){
            core.info('ConnectingGate.qml | __updateCgAgSource | App is already Acitve current Reuqest is droped.... ');return;
        }else{appIsActive=true;}

        loadId++;
        arrivalInfoModel.source="";
        arrivalInfoModel.source=__getAGSource();
        setCGSourceTimer.restart();
    }

    function __updateCgAgSourceByLang(){
        if (appIsActive==true){
            core.info('ConnectingGate.qml | __updateCgAgSource | App is already Acitve current Reuqest is droped.... ');return;
        }else{appIsActive=true;}

        loadId++;
        cgModelByLang.source="";
        cgModelByLang.source= __getCGSourceByLang();
        arrivalInfoModel.source="";
        arrivalInfoModel.source=__getAGSource();
    }

    function resetVariables(){
        if(retry.running==true){ retry.stop() }
        errorType= 0;
        loadId=0;
        maxRetry=0;
        cgDataStatus= false;
        cgdata=0;
        appIsActive= false
        cgModel.source=''
        arrivalInfoModel.source=''
        airportByIataModel.source =''
        Store.callback = '';
    }

    // Connecting data - XML Model.
    XmlListModel {
        id: cgModel;

        source: "";
        query: cgxpath;

        XmlRole { name: "arrival_city"; query: "arrival_city/string()" }
        XmlRole { name: "carrier_code"; query: "carrier_code/string()" }
        XmlRole { name: "flight_number"; query: "flight_number/string()" }
        XmlRole { name: "sector_scheduled_departure"; query: "sector_scheduled_departure/string()" }
        XmlRole { name: "departure_remarks"; query: "departure_remarks/string()" }
        XmlRole { name: "departure_gate_designator"; query: "departure_gate_designator/string()" }
        XmlRole { name: "departure_id_terminal"; query: "departure_id_terminal/string()" }
        XmlRole { name: "arrival_airport_name"; query: "arrival_airport_name/string()" }
        XmlRole { name: "arrival_airport_iata_code"; query: "arrival_airport_iata_code/string()" }
        XmlRole { name: "departure_airport_name"; query: "departure_airport_name/string()" }
        XmlRole { name: "departure_airport_iata_code"; query: "departure_airport_iata_code/string()" }
        XmlRole { name: "sector_status"; query: "sector_status/string()" }
        XmlRole { name: "operator_name"; query: "operator_name/string()" }
        XmlRole { name: "sector_estimated_departure"; query: "sector_estimated_departure/string()" }
        XmlRole { name: "departure_id_gate"; query: "departure_id_gate/string()" }
        XmlRole { name: "id_sector"; query: "id_sector/string()"}

        onStatusChanged: {

            if(cgdata==0){errorType=1;cgDataStatus= false;appIsActive = false;return;}

            if(status == XmlListModel.Ready){
                core.info("ConnectingGate.qml | onStatusChanged | cgModel | Model Rows: "+cgModel.count);
                if (cgModel.query==cgxpath && cgModel.source!='') {
                    if (cgModel.count==0) {
                        errorType=2;
                        cgDataStatus= false;
                        connectingFlightReponse=true
                        __sigModelReady(cgModel,cgDataStatus,'cgModel',errorType)
                        if(connectingFlightReponse && arrivalDetailsReponse){__disconnectCallback();connectingFlightReponse=false;arrivalDetailsReponse=false}

                    }else {
                        errorType=0;
                        cgDataStatus= true;
                        __setOnCompleted();
                        connectingFlightReponse=true
                        __sigModelReady(cgModel,cgDataStatus,'cgModel',errorType);
                        if(connectingFlightReponse && arrivalDetailsReponse){__disconnectCallback();connectingFlightReponse=false;arrivalDetailsReponse=false}
                    }
                }
            }else if(status == XmlListModel.Error){
                errorType=3;
                cgDataStatus= false;
                core.info(" ConnectingGate.qml | onStatusChanged  | cgModel | Error | "+cgModel.errorString());
               if(maxRetry==0) __sigModelReady(cgModel,cgDataStatus,'cgModel',errorType)
                if(!retry.running){retry.running=true;retry.start();}
            }
        }
    }

    XmlListModel {
        id: cgModelByLang;

        source: "";
        query: cgxpath;

        XmlRole { name: "arrival_city"; query: "language/translation/city_xlat/string()" }
        XmlRole { name: "arrival_city_default"; query: "arrival_city/string()" }
        XmlRole { name: "carrier_code"; query: "carrier_code/string()" }
        XmlRole { name: "flight_number"; query: "flight_number/string()" }
        XmlRole { name: "sector_scheduled_departure"; query: "sector_scheduled_departure/string()" }
        XmlRole { name: "departure_remarks"; query: "departure_remarks/string()" }
        XmlRole { name: "arrival_airport_name_default"; query:"arrival_airport_name/string()"}
        XmlRole { name: "departure_gate_designator"; query: "departure_gate_designator/string()" }
        XmlRole { name: "arrival_airport_name"; query: "language/translation/airport_xlat/string()" }
        XmlRole { name: "arrival_airport_iata_code"; query: "arrival_airport_iata_code/string()" }
        XmlRole { name: "departure_airport_name"; query: "departure_airport_name/string()" }
        XmlRole { name: "departure_airport_iata_code"; query: "departure_airport_iata_code/string()" }
        XmlRole { name: "sector_status"; query: "sector_status/string()" }
        XmlRole { name: "operator_name"; query: "operator_name/string()" }
        XmlRole { name: "sector_estimated_departure"; query: "sector_estimated_departure/string()" }
        XmlRole { name: "departure_id_gate"; query: "departure_id_gate/string()" }
        XmlRole { name: "departure_id_terminal"; query: "departure_id_terminal/string()" }
        XmlRole { name: "flight_status"; query: "language/translation/flight_status_xlat/string()" }

        onStatusChanged: {

            if(cgdata==0){errorType=1;cgDataStatus= false;appIsActive = false;return;}

            if(status == XmlListModel.Ready){
                core.info("ConnectingGate.qml | onStatusChanged | cgModel | Model Rows: "+cgModelByLang.count);
                if (cgModelByLang.query==cgxpath && cgModelByLang.source!='') {
                    if (cgModelByLang.count==0) {
                        errorType=2;
                        cgDataStatus= false;
                         __sigModelReady(cgModelByLang,cgDataStatus,'cgModelByLang',errorType)
                    }else {
                        errorType=0;
                        cgDataStatus= true;
                        __setOnCompleted();
                         __sigModelReady(cgModelByLang,cgDataStatus,'cgModelByLang',errorType)
                    }
                }
            }else if(status == XmlListModel.Error){
                errorType=3;
                cgDataStatus= false;
                core.info(" ConnectingGate.qml | onStatusChanged  | cgModelByLang | Error | "+cgModelByLang.errorString());
               if(maxRetry==0) __sigModelReady(cgModelByLang,cgDataStatus,'cgModel',errorType)
                if(!retry.running){retry.running=true;retry.start();}
            }
        }
    }

    // Arrival data - XML Model.
    XmlListModel {
        id: arrivalInfoModel;
        source: "";
        query: agxpath;

        XmlRole { name: "gate_designator"; query: "gate_designator/string()" }
        XmlRole { name: "carrier_code"; query: "carrier_code/string()" }
        XmlRole { name: "flight_number"; query: "flight_number/string()" }
        XmlRole { name: "sector_scheduled_arrival"; query: "sector_scheduled_arrival/string()" }
        XmlRole { name: "baggage_area_designator"; query: "baggage_area_designator/string()" }
        XmlRole { name: "gate_number"; query: "gate_number/string()" }
        XmlRole { name: "baggage_area_name"; query: "baggage_area_name/string()" }
        XmlRole { name: "city_name"; query: "city_name/string()" }
        XmlRole { name: "terminal_designator"; query: "terminal_designator/string()" }
        XmlRole { name: "sector_estimated_arrival"; query: "sector_estimated_arrival/string()" }
        XmlRole { name: "remarks"; query: "remarks/string()" }
        onStatusChanged: {

            if (cgdata==0) {errorType=1;cgDataStatus = false;appIsActive  = false;return;}

            if(status == XmlListModel.Ready){
                core.info("ConnectingGate.qml | onStatusChanged  | arrivalInfoModel | Model Rows: "+arrivalInfoModel.count);
                if (arrivalInfoModel.query==agxpath && arrivalInfoModel.source!=''){
                    if(arrivalInfoModel.count==0) {
                         errorType=2;
                         cgDataStatus= false;
                        arrivalDetailsReponse=true
                        __sigModelReady(arrivalInfoModel,cgDataStatus,'arrivalInfoModel',errorType)
                        if(connectingFlightReponse && arrivalDetailsReponse){__disconnectCallback();connectingFlightReponse=false;arrivalDetailsReponse=false}
                    }else {
                        errorType=0;
                        __setOnCompleted();
                        arrivalDetailsReponse=true
                        __sigModelReady(arrivalInfoModel,'true','arrivalInfoModel',errorType)
                        if(connectingFlightReponse && arrivalDetailsReponse){__disconnectCallback();connectingFlightReponse=false;arrivalDetailsReponse=false}
                    }
                }
            }else if(status == XmlListModel.Error){
                cgDataStatus= false;
                errorType=3;
                if(maxRetry==0)__sigModelReady(arrivalInfoModel,cgDataStatus,'arrivalInfoModel',errorType)
                core.info(" ConnectingGate.qml | onStatusChanged  | arrivalInfoModel | Error | "+arrivalInfoModel.errorString());
                if(!retry.running){retry.running=true;retry.start();}
            }
        }
    }

    // getAirportByIataModel - XML Model.
    XmlListModel {
        id: airportByIataModel;
        source: "";
        query: '/response/data';

        XmlRole { name: "airport_name"; query: "airport_name/string()" }
        XmlRole { name: "iata_code"; query: "iata_code/string()" }
        XmlRole { name: "icao_code"; query: "icao_code/string()" }
        XmlRole { name: "city_name"; query: "city_name/string()" }
        XmlRole { name: "country_name"; query: "country_name/string()" }

        onStatusChanged: {
            if(status == XmlListModel.Ready){
                if (airportByIataModel.source!='') {
                    if (airportByIataModel.count==0) {
                        errorType=2;
                        __sigModelReady(airportByIataModel,'false','airportByIataModel',errorType)
                    } else {
                        errorType=0;
                        core.info("ConnectingGate.qml | onStatusChanged  | airportByIataModel | Data Ready | airportByIataModel.count : "+airportByIataModel.count);
                        __sigModelReady(airportByIataModel,'true','airportByIataModel',errorType)
                    }
                }
            }else if(status == XmlListModel.Error){
                errorType=3;
                core.info(" ConnectingGate.qml | onStatusChanged  | airportByIataModel | Error | "+airportByIataModel.errorString());
                __sigModelReady(airportByIataModel,'false','airportByIataModel',errorType)
            }
        }
    }

    Timer{
        id: retry;
        interval: 20000;
        onTriggered: {
            core.info("ConnectingGate.qml | Timer | maxRetry : "+maxRetry+"/10 | Retry On");
            if(maxRetry==10){
                core.info("ShopAndHosp.qml | Timer | maxRetry : "+maxRetry+"/10 | Maximum reached - Quitting Retry");
                retry.stop();
                maxRetry = 0;
                __setOnCompleted();

            }else{
                maxRetry++;appIsActive=false;
                __updateCgAgSource();
            }
        }
    }
    Timer{id:setCGSourceTimer;repeat: false;interval: 500; onTriggered: {cgModel.source="";cgModel.source=__getCGSource();}}

    Component.onCompleted: {resetVariables();cgdata = parseInt(core.pif.getCgDataStatus(),10);core.info ("ConnectingGate.qml | Component.onCompleted | ConnectingGate LOAD COMPLETE");}
}
