import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

DataLoader{
    id:contentStagging
    signal sigDataSavedStatus(string status);

    onSigDataReady: {
        core.info("ContentStagging.qml | dataLoader | onSigDataReady");
        if (errCode==0){
            core.info("ContentStagging.qml | dataLoader | onSigDataReady");
            sigDataSavedStatus("success")

        } else {
            core.info("ContentStagging.qml | dataLoader | Error");
            sigDataSavedStatus("failure");
        }
    }

    function saveResponseData(context,name,value,callback){ // context--AlphaNumeric only, "_" is allowed
        core.info("ContentStagging.qml | saveResponseData | context:"+context+", name :"+name+", value:"+value);
        if(context==undefined || context==""){core.info("ContentStagging.qml | saveResponseData, Context not provided"); return false};
        if(name==undefined || name==""){core.info("ContentStagging.qml | saveResponseData, name not provided");return false};
        var requestId=18;__updateCallback(callback)
        var seatNum = pif.getSeatNumber();
        var getParams = "function=seat_saveResponse&context="+context+"&seat="+seatNum+"&name="+name+"&value="+value;
        __getRemoteData(getParams,requestId);
    }


    function __getRemoteData(getParams,requestId){
        getData(core.settings.remotePath+'/content_staging/api/response_api.php',3,getParams,'post','',requestId);
    }

    function __updateCallback(callback){
        if (Store.callback){sigDataSavedStatus.disconnect(Store.callback);}
        if (callback){sigDataSavedStatus.connect(callback);Store.callback = callback;}
    }

    Component.onCompleted: {
        core.info("ContentStagging.qml | Component OnCOmpleted");
    }
}
