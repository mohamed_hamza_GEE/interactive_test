import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

DataLoader {
    id:pdi
    property variant seatJsonData; //since FE might require direct binding
    property variant customSeatJsonData; //since FE might require direct binding
    signal __sigModelReady(variant model,bool status);

    onSigDataReady: {
        if (errCode==0){
            core.info("Pdi.qml | dataLoader | onSigDataReady");
            __processModel(responseObj,responseString,requestId);
        }else {
            __sigModelReady(seatJsonData,false);
        }
    }

    onSigLocalDataReady:{
        if(errCode==0){
            core.info("Pdi.qml | dataLoader | onSigLocalDataReady ");
            __processModel(responseObj,'fromConfigModel',requestId)
        }else{
            __sigModelReady(seatJsonData,false);
        }
    }

    //public function
    function getFirstName(){return (seatJsonData.person && seatJsonData.person.first_name)?seatJsonData.person.first_name:""}
    function getLastName(){return (seatJsonData.person && seatJsonData.person.last_name)?seatJsonData.person.last_name:""}
    function getMiddleName(){return (seatJsonData.person && seatJsonData.person.middle_name)?seatJsonData.person.middle_name:""}
    function getSalutation(){return (seatJsonData.person && seatJsonData.person.salutation)?seatJsonData.person.salutation:""}
    function getPreferredLanguage(){return (seatJsonData.person && seatJsonData.person.preferred_language)?seatJsonData.person.preferred_language:""}
    function getJsonData(){return seatJsonData}

    function getSeatSummaryInfo(seatNum,callback) {

        if (isAppActive==true){core.info('Pdi.qml| getSeatSummaryInfo | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}

        var requestId=1;__updateCallback(callback);
        core.info('Pdi.qml | getSeatSummaryInfo | seat : '+seatNum);

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/pdi/getSeatSummaryInfo.json'
            core.info('Pdi.qml | getSeatSummaryInfo | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId);
        }else{
            var getParams = "seat_getSeatSummaryInfo&seat="+seatNum+"&output_format=json";
            core.info('Pdi.qml | getSeatSummaryInfo | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            __getRemoteData(getParams,requestId);
        }
    }

    function getSeatDetailInfo(seatNum,callback) {

        if (isAppActive==true){core.info('Pdi.qml| getSeatDetailInfo | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}

        var requestId=2;__updateCallback(callback);
        core.info('Pdi.qml | getSeatDetailInfo | seat : '+seatNum);

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/pdi/getSeatDetailInfo.json'
            core.info('Pdi.qml | getSeatDetailInfo | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId);
        }else{
            var getParams = "function=seat_getSeatDetailInfo&iata="+seatNum+"&output_format=json";
            core.info('Pdi.qml | getSeatDetailInfo | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            __getRemoteData(getParams,requestId);
        }
    }

    function getCustomPdiApi(seatNum,apipath,funcname,callback){
        if (isAppActive==true){core.info('Pdi.qml| getCustomPdiApi | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        var requestId=3;__updateCallback(callback);
        core.info('Pdi.qml | getCustomPdiApi | seat : '+seatNum);
        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/pdi/getCustomSeatDetailInfo.json'
            core.info('Pdi.qml | getSeatDetailInfo | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId);
        }else{
            var getParams = "function="+funcname+"&seat="+seatNum+"&output_format=json";
            core.info('Pdi.qml | getSeatDetailInfo | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            getData(core.settings.remotePath+'/'+apipath+'/api/pdi_pax_api.php',3,getParams,'','',requestId);
        }

    }

    //private function
    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }

    function __processModel(value,data,requestId){
        if(data!='' && value['code'] == 0){
            core.info("Pdi.qml | __processModel | Data ready ")
            __readJsonObject(requestId,value)
        }else{
            core.info("Pdi.qml | __processModel | ERROR | code: "+value['code']+" | desc: "+value['desc'])
            __sigModelReady(seatJsonData,false);
        }
    }

    function __readJsonObject(requestId,value){
        core.info('Pdi.qml | __readJsonObject | id :'+requestId)
        if(requestId==0){core.info('Pdi.qml | __readJsonObject | Data reday = false')}
        if(requestId==1){__loadSeatDetailsModel(value);}
        if(requestId==2){__loadSeatDetailsModel(value);}
        if(requestId==3){__loadCustomSeatData(value);}
    }

    function __loadSeatDetailsModel(value){
        core.info('Pdi.qml | __loadSeatDetailsModel | Called ');

        seatJsonData={};
        if(value['data']){
            seatJsonData=value["data"];
        }else {
             core.info('Pdi.qml | __loadSeatDetailsModel | ERROR | Data not found ');
             __sigModelReady(seatJsonData,false);
            return false;
        }

        core.debug('Pdi.qml | __loadSeatDetailsModel | seatDetailsModel : '+JSON.stringify(seatJsonData));
        isAppActive=false;__sigModelReady(seatJsonData,true);
    }

    function __loadCustomSeatData(value){

        core.info('Pdi.qml | __loadCustomSeatData | Called ');

        customSeatJsonData={};
        if(value['data']){
            customSeatJsonData=value["data"];
        }else {
             core.info('Pdi.qml | __loadCustomSeatData | ERROR | Data not found ');
             __sigModelReady(customSeatJsonData,false);
            return false;
        }

        core.info('Pdi.qml | __loadCustomSeatData | customSeatJsonData : '+JSON.stringify(customSeatJsonData));
        isAppActive=false;__sigModelReady(customSeatJsonData,true);
    }

    function __getRemoteData(getParams,requestId){
        getData(core.settings.remotePath+'/pdi/api/pdi_pax_api.php',3,getParams,'','',requestId);
    }

    function __getLocalData(getParams,requestId){getLocalData(getParams,requestId);}

    function resetVariables(){
        core.info('Pdi.qml | resetVariables Called ');
        isAppActive = false;
        seatJsonData={};
    }

    Component.onCompleted: {seatJsonData={};isAppActive = false;core.info ("Pdi.qml | Component.onCompleted | Pdi LOAD COMPLETE");}
}
