import QtQuick 1.1
import "../components"
import "mediaServices.js" as Services

Item {
    id:mediaServices
    property variant dataController
    //media blocking
    property bool isSMIDStatusChanged: false;
    property bool isMIDStatusChanged: false;
    property bool isIndependentServiceStatusChanged: false;
    signal sigCidBlockChanged(variant cidList, string status, int smid, string serviceCode);
    signal sigMidBlockChanged(int mid, string status);
    signal sigServiceStatusChanged(string status, int smid, string serviceCode);
    signal sigMidPaidStatusChanged(variant midList, int bmid, string status, string cause);
    /*Public function for look up tag_name, custom service*/
    function getCategoryDataByTagName(tag_name){return Services.tagLookUp[tag_name]?Services.tagLookUp[tag_name]:{}}

    function getCategoryDataByService(service){return Services.serviceLookUp[service]?Services.serviceLookUp[service]:''}

    function getPaxusDataByCid(cid){return Services.cidLookUp[cid]?Services.cidLookUp[cid]:'';}
    function getPostersByCid(cid){return Services.cidLookUp[cid]?(Services.cidLookUp[cid]['category_posters']?Services.cidLookUp[cid]['category_posters']:''):'';}

    /*Public function for service/cid/mid blocking*/
    function getCidBlockStatus(cid){if(Services.blockCidArr){if(Services.blockCidArr[cid] >= 0){core.info("service blocked status "+Services.blockCidArr[cid]);return true;}}return false;}

    function getMidBlockStatus(mid,rating) {
        if(dataController.getSeatRating() < rating && dataController.getSeatRating() !=0){return true;
        }else if(Services.midBlockArr){if(Services.midBlockArr[mid] >= 0){return true;}}
        return false;
    }

    function getMidPurchaseStatus(mid){if(Services.payMidArr){if(Services.payMidArr[mid] == 1){return "pay";}else if(Services.payMidArr[mid] ==0 ){return "paidorfree"}return "notexist";}}
    function getPaidOrFreeMids(){
        if(Services.payMidArr){
            var temp = [];
            for(var mid in Services.payMidArr){
                if(Services.payMidArr[mid] == 0)temp.push(mid)
            }
        }
        return temp
    }
    function getServiceBlockStatus(service){if(Services.serviceCodeStatus){core.info("service blocked status "+Services.serviceCodeStatus[service]); return Services.serviceCodeStatus[service];} return 0;}
    function getChildBundleMids(mid){var tmparr=[];if(Services.bundleLookup[mid] != undefined){ for(var key in Services.bundleLookup[mid])tmparr.push(key);}return tmparr;}
    //returns if blocked after open flight(1), pre open flight(2) or is unblocked (0)
    function getServiceStatus(service){
        if(Services.serviceCodeStatus){
            for(var smid in Services.serviceCodeLookup)for(var cid in Services.serviceCodeLookup[smid]){
                    if(Services.serviceCodeLookup[smid][cid] == service){if(Services.blockCidArr[cid] && Services.blockCidArr[cid]>0){core.info("service blocked status "+Services.blockCidArr[cid]);return Services.blockCidArr[cid];}else{return 0}}
                }

        }
        return 0;
    }
    function getTemplateIdByCatergoryId(cid){if(Services.cidLookUp[cid] != undefined){return Services.cidLookUp[cid]['tid']}else{core.info("MediaServices | getTemplateIdByCatergoryId | INVALID CID");return;}}
    function getParentCategoryByCatergoryId(cid){if(Services.cidLookUp[cid] != undefined){return [Services.cidLookUp[cid]['pcid'],Services.cidLookUp[cid]['ptid']]}else{core.info("MediaServices | getParentCategoryByCatergoryId | INVALID CID");return;}}

    function getBlockedMidList(){
        var arr = Services.midBlockList; var temp = arr.split(',');
        var tmp = [];
        for(var k in temp){
            if(temp[k] != ''){tmp.push(temp[k])};
        }
        return tmp.join(',')
    }
    function getBlockedCidList(){
        var arr =  Services.blockCidList; var temp = arr.split(',');
        var tmp = [];
        for(var k in temp){
            if(temp[k] != ''){tmp.push(temp[k])};
        }
        return tmp.join(',')
    }

    function setCustomServiceAccessType(cid, accessType, isXmlQuery, smid){
        core.info("MediaServices | setCustomServiceAccessType | cid "+cid+" | accesstype "+accessType+" | isXmlQuery "+isXmlQuery+" | smid "+smid);
        if(smid == undefined){smid = 0;}
        // __setCidAccessType(accessType,cid,isXmlQuery);// isXmlQuery should be set to 1 after open flight
        __setServiceAccessType(accessType,smid,cid,isXmlQuery);
        Services.__updateCidBlocked();
    }

    function setCustomServiceAccessTypeByName(servicesName, accessType, isXmlQuery){
        core.info("MediaServices | setCustomServiceAccessTypeByName | servicesName "+servicesName+" | accesstype "+accessType+" | isXmlQuery "+isXmlQuery);
        if(servicesName == ''){return false}
        for(var customService in Services.serviceLookUp){
            //            core.log("MediaServices | setCustomServiceAccessTypeByName |customservice :"+customService+" | service name :" + servicesName)
            if(customService==servicesName){
                var cidArray=Services.serviceLookUp[customService].split(",");
                for(var cid in cidArray){/*Services.__setCidAccessType(accessType,cidArray[cid],isXmlQuery);*/__setServiceAccessType(accessType,0,cidArray[cid],servicesName,isXmlQuery);}
            }
        }
        Services.__updateCidBlocked();
    }

     /* isXmlQuery is set to 2 if setMidAccessType() is invoked from XML and set to 1 if setMidAccessType() is invoked after Open Flight*/
    function setMidAccessType(accesstype, mid, isXmlQuery){
        core.info("MediaServices | setMidAccessType | mid "+mid+" | accesstype "+accesstype+" | isXmlQuery "+isXmlQuery);
        if(isXmlQuery == undefined)
            isXmlQuery = 1;
        if(accesstype == 'blocked'){
            if(!Services.midBlockList ){
                Services.midBlockList = "'"+mid+"'"+",";
                isMIDStatusChanged = true;
            }else if(Services.midBlockList.search("'"+mid+"'") == -1 ){   //Do not repeat MIDs in the comma separated list
                Services.midBlockList = Services.midBlockList+"'"+mid+"'"+",";
                isMIDStatusChanged = true;
            }else if(Services.midBlockArr[mid]!=isXmlQuery){
                isMIDStatusChanged = true;
            }

            Services.midBlockArr[mid] = isXmlQuery;

            if(isXmlQuery==2){
                Services.midBlockArrPreOpenFlight[mid]=isXmlQuery;
            }

            if(isMIDStatusChanged == true){
                core.info("MediaServices | setMidAccessType | MID "+ mid +" Status changed ")
                Services.__updateMidBlocked();
                sigMidBlockChanged(mid,accesstype);
                isMIDStatusChanged = false;
            }else {core.info("MediaServices | setMidAccessType | MID "+ mid +" Status not changed No signal emitted")}
        }else{ //Unblocked MIDs
            if(Services.midBlockArr[mid]){delete Services.midBlockArr[mid];}
            if(Services.midBlockList.search("'"+mid+"'") != -1){
                Services.midBlockList = Services.__filterMidFromCsv(Services.midBlockList, mid);
            }
            Services.__updateMidBlocked();
            sigMidBlockChanged(mid,accesstype);
        }


        //core.info("MediaServices | setMidAccessType | FINAL MID LIST : "+Services.midBlockList)
        return true;
    }

    function setServiceAccessType(accessType,smid){
        core.info("MediaServices | setServiceAccessType | CALL: smid: "+smid+" | accessType: "+accessType);
        __setServiceAccessType(accessType,smid);
    }

    function getSigMidPaidStatusChanged(midarr){
        core.info("MediaServices | getSigMidPaidStatusChanged | "+midarr);
        if(midarr){sigMidPaidStatusChanged(midarr,0,'pay','PPV_PAY_INTERACTIVE');}
    }

    function setCidAccessType(accessType,cid,isXmlQuery){
        return Services.__setCidAccessType(accessType,cid,isXmlQuery);
    }

    function updateCidBlocked(){Services.__updateCidBlocked();}

    /* Private functions */
    /*Private function for service/cid/mid blocking*/
    function __processServiceModel(model){
        if(model.count){
            for(var i=0;i< model.count;i++){
                core.info("MediaServices | __processServiceModel | service_mid: "+model.getValue(i,"service_mid")+" | service_cid: "+model.getValue( i,"service_cid")+" | service_code: "+model.getValue( i,"service_code")+" | service_access_type: "+model.getValue(i,"service_access_type"));
                if(Services.cidSmidArr[parseInt(model.getValue( i,"service_cid"),10)] ==  undefined){Services.cidSmidArr[parseInt(model.getValue( i,"service_cid"),10)] = [];}
                Services.__getServiceVals(parseInt(model.getValue( i,"service_cid"),10),model.getValue(i,"service_access_type"),2,parseInt(model.getValue(i,"service_mid"),10), model.getValue( i,"service_code"));
            }

            Services.__updateCidBlocked();Services.__updateMidBlocked();
            //debug: for(var keyAsCid in Services.cidSmidArr){for(var keyAsSmid in Services.cidSmidArr[keyAsCid]){core.info('MediaServices | onSigDataReady | '+Services.serviceCodeLookup[keyAsSmid][keyAsCid]+' smid '+keyAsSmid +' cid '+keyAsCid+' : '+Services.cidSmidArr[keyAsCid][keyAsSmid])}}
            //debug: for(var keyAsCid in Services.serviceCodeLookup) {for(var keyAsSmid in Services.serviceCodeLookup[keyAsCid]){core.info('MediaServices | __processServiceModel |serviceCodeLookup['+keyAsCid +']['+keyAsSmid+'] = ' + Services.serviceCodeLookup[keyAsCid][keyAsSmid])}}
        }
        return true;
    }

    /* This function is used by system interface also
    isXmlQuery is set to 2 if setMidAccessType() is invoked from XML and set to 1 if setMidAccessType() is invoked after Open Flight*/
    function __setServiceAccessType(accessType,smid,cid,servicecode,isXmlQuery)    {
        if(isXmlQuery == undefined)       // isXmlQuery is set to 1 if setMidAccessType() is invoked after Open Flight
            isXmlQuery = 1;
        core.info("MediaServices | __setServiceAccessType | smid :" +smid+" cid :" +cid+ " accessType : "+accessType )
        Services.cidList= [] // This will clear all previous values.
        if (cid == undefined){
            cid = 0; // Assigned value to form the below 2D array cidSmidArr

            var keyAsCid;
            var keyAsSmid;
            var getCid;

            for(keyAsCid in Services.cidSmidArr) {
                for(keyAsSmid in Services.cidSmidArr[keyAsCid])  {
                    if(keyAsSmid == smid) {
                        if(keyAsCid != cid) {
                            cid = keyAsCid;      // To get rid of zero that was assigned to cid above
                            if(accessType == 'blocked') {
                                core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" is linked to cid: "+cid+" calling __setCidAccessType to block this cid")
                                if(parseInt(Services.cidSmidArr[cid][smid],10) !=1){
                                    Services.cidSmidArr[cid][smid] = 1;
                                    isSMIDStatusChanged=true;
                                    core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" | cid: "+cid+" | Status change")
                                    Services.serviceCodeStatus[Services.serviceCodeLookup[smid][cid]] = 1;
                                    getCid = Services.__setCidAccessType(accessType,cid,1);
                                   Services.cidList.push(cid)
                                }
                            }else {
                                var cidStatus = true;
                                for(var cidKey in Services.cidSmidArr[cid]){
                                    if(Services.cidSmidArr[cid][cidKey]==1 && cidKey!=smid){
                                         core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" | cid: "+cid+" | Status not changed No signal emitted")
                                        cidStatus = false;
                                        break;
                                    }
                                }
                                if(cidStatus==true){
                                    if(parseInt(Services.cidSmidArr[cid][smid],10) != 0){
                                        isSMIDStatusChanged=true;
                                        core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" | cid: "+cid+" | Status change")
                                        Services.cidList.push(cid)
                                        getCid = Services.__setCidAccessType(accessType,cid,1);
                                    }
                                }
                                Services.cidSmidArr[cid][smid] = 0; //unblock service mid
                                Services.serviceCodeStatus[Services.serviceCodeLookup[smid][cid]] = 0;
                            }
                        }else if(keyAsCid == 0){
                            if(accessType == 'blocked') {
                                core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" is linked to cid: "+cid+" calling __setCidAccessType to block this cid")
                                if(parseInt(Services.cidSmidArr[cid][smid],10) !=1){
                                    isIndependentServiceStatusChanged = true;
                                    Services.cidSmidArr[cid][smid] = 1;
                                    core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" | cid: "+cid+" | Status change")
                                    Services.serviceCodeStatus[Services.serviceCodeLookup[smid][cid]] = 1;
                                }
                            }else {
                                var cidStatus = true;
                                if(Services.cidSmidArr[cid][smid]==0){
                                    core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" | cid: "+cid+" | Status not changed No signal emitted")
                                    cidStatus = false;
                                }
                                if(cidStatus==true){
                                    if(parseInt(Services.cidSmidArr[cid][smid],10) != 0){
                                       isIndependentServiceStatusChanged = true;
                                        core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" | cid: "+cid+" | Status change")

                                    }
                                }
                                Services.cidSmidArr[cid][smid] = 0; //unblock service mid
                                Services.serviceCodeStatus[Services.serviceCodeLookup[smid][cid]] = 0;
                            }
                        }
                    }
                }
            }
        }
        else{//for custom service
            if(Services.cidSmidArr[cid] == undefined){Services.cidSmidArr[cid] = [];}
            if(accessType == 'blocked'){
                if(parseInt(Services.cidSmidArr[cid][smid],10) != 1){
                    Services.cidSmidArr[cid][smid] = 1;
                    isSMIDStatusChanged=true;
                    //if(smid!=0){Services.serviceCodeStatus[Services.serviceCodeLookup[smid][cid]] = 1;}
                     core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" | cid: "+cid+" | Status change")
                    Services.serviceCodeStatus[Services.serviceCodeLookup[smid][cid]] = 1;
                    Services.cidList.push(cid)
                    Services.__setCidAccessType(accessType,cid,isXmlQuery);
                }
            }
            else{
                var chkcidStatus = true
                for(var smidkey in Services.cidSmidArr[cid]){
                    if(Services.cidSmidArr[cid][smidkey]==1 && smidkey!=smid){
                        chkcidStatus = false;
                        break;
                    }
                }
                if(chkcidStatus==true){
                    if(parseInt(Services.cidSmidArr[cid][smid],10) != 0){
                        isSMIDStatusChanged=true;
                        core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" | cid: "+cid+" | Status change")
                        Services.cidList.push(cid)
                        getCid = Services.__setCidAccessType(accessType,cid,1);
                    }
                }
                if(parseInt(Services.cidSmidArr[cid][smid],10) != 0 ){
                    Services.cidSmidArr[cid][smid] = 0;
                    //isSMIDStatusChanged=true;
                    // if(smid!=0){Services.serviceCodeStatus[Services.serviceCodeLookup[smid][cid]] = 0;}
                    Services.serviceCodeStatus[Services.serviceCodeLookup[smid][cid]] = 0;
                    //Services.__setCidAccessType(accessType,cid,isXmlQuery);
                }
            }
        }
//       debug: for(var keyAsCid1 in Services.cidSmidArr){for(var keyAsSmid1 in Services.cidSmidArr[keyAsCid1]){core.debug('MediaServices | __setServiceAccessType | Services.serviceCodeLookup['+keyAsSmid1+']['+keyAsCid1+']  '+Services.serviceCodeLookup[keyAsSmid1][keyAsCid1]+' smid '+keyAsSmid1 +' cid '+keyAsCid1+' : '+'Services.cidSmidArr['+keyAsCid1+']['+keyAsSmid1+'] ' +Services.cidSmidArr[keyAsCid1][keyAsSmid1])}}
        if( isSMIDStatusChanged == true){
            Services.__updateCidBlocked();Services.__updateMidBlocked();
            if(smid!=0){
                sigCidBlockChanged(Services.cidList,accessType,smid,Services.serviceCodeLookup[smid][cid]);
                sigServiceStatusChanged(accessType,smid,Services.serviceCodeLookup[smid][cid]);
            }
            else{
                sigCidBlockChanged(Services.cidList,accessType,smid,servicecode) // for Cust. Service block.
                sigServiceStatusChanged(accessType,smid,servicecode)
            }
            isSMIDStatusChanged=false;
        }else if(isIndependentServiceStatusChanged == true){
                sigServiceStatusChanged(accessType,smid,Services.serviceCodeLookup[smid][cid])
            isIndependentServiceStatusChanged=false;
        }else {core.info("MediaServices | __setServiceAccessType | serviceMid: "+ smid +" cid: "+cid+" | Status not changed No signal emitted ")}
        return getCid;
    }

    function __setPpvRefundMidData(mid,status,cause){Services.isMidRefundCalled=true;Services.refundMidData["mid"] =mid; Services.refundMidData["status"] =status; Services.refundMidData["cause"] =cause;setPurchaseMidStatus(mid,status,cause);}
    function __setIsRefundCalled(status){Services.isMidRefundCalled=status;}
    /*Private function for category look up
    cidLookUp is Array with CID to title,tid,pcid,ptid mapping for paxus logging
    */
    function __processLookupModel(model){
        core.info("MediaServices | __processLookupModel | model.count :"+model.count);

        if(model.count){
            var tmpCustomServiceArray=[];
            for(var i=0; i<model.count; i++){
                if(Services.templateIdMap){
                    if(Services.templateIdMap['tag_name']){
                        for(var keyTidToTag in Services.templateIdMap['tag_name']){
                            if(keyTidToTag == model.getValue(i,'template_id')){
                                if(Services.tagLookUp[Services.templateIdMap['tag_name'][keyTidToTag]] == undefined){
                                    Services.tagLookUp[Services.templateIdMap['tag_name'][keyTidToTag]] = new Object();
                                    Services.tagLookUp[Services.templateIdMap['tag_name'][keyTidToTag]]={'cid':model.getValue(i,'cid'),'pcid':model.getValue(i,'parentcid'),'tid':model.getValue(i,'template_id'),'ptid':model.getValue(i,'ptid')}
                                }
                            }
                        }
                    }
                }
                if(Services.templateIdMap['customservice']){
                    for(var keyTidToServ in Services.templateIdMap['customservice']){
//                        core.log("MediaServices | __processLookupModel | tid :"+keyTidToServ +" | customservice: " +Services.templateIdMap['customservice'][keyTidToServ])
                        if(keyTidToServ == model.getValue(i,'template_id')){
                            //                            var splitservice = Services.templateIdMap['customservice'][keyTidToServ].split(",");
                            var custservice = Services.templateIdMap['customservice'][keyTidToServ]
                            // for(var k=0; k<splitservice.length; k++){

                            if(Services.serviceLookUp[custservice] && tmpCustomServiceArray[custservice] && !tmpCustomServiceArray[custservice][model.getValue(i,'cid')]){
                                tmpCustomServiceArray[custservice][model.getValue(i,'cid')] = 1;
                                Services.serviceLookUp[custservice] = Services.serviceLookUp[custservice]+","+model.getValue(i,'cid'); // Do not repeat CIDs
                                Services.__setServiceCodeLookup(model.getValue(i,'cid'),0,custservice);
                                setCustomServiceAccessTypeByName(custservice, 'paidorfree', 1);
                            }else {
                                if(!Services.serviceLookUp[custservice]){tmpCustomServiceArray[custservice]=[];tmpCustomServiceArray[custservice][model.getValue(i,'cid')] = 1;Services.serviceLookUp[custservice] = model.getValue(i,'cid'); Services.__setServiceCodeLookup(model.getValue(i,'cid'),0,custservice);setCustomServiceAccessTypeByName(custservice, 'paidorfree', 1);}
                            }
                            //   }
                        }
                    }
                }
                /*pif.getScreenLogStatus() paxus check this temporory not check till function in place*/
                if(Services.cidLookUp[model.getValue(i,'cid')]==undefined)
                {
                    Services.cidLookUp[model.getValue(i,'cid')] = new Array();
                    Services.cidLookUp[model.getValue(i,'cid')]['title'] = model.getValue(i,'title');
                    Services.cidLookUp[model.getValue(i,'cid')]['tid'] = model.getValue(i,'template_id');
                    Services.cidLookUp[model.getValue(i,'cid')]['pcid'] = model.getValue(i,'parentcid');
                    Services.cidLookUp[model.getValue(i,'cid')]['ptid'] = model.getValue(i,'ptid');
                    if(Services.blockCidArr[model.getValue(i,'cid')] != undefined){Services.cidLookUp[model.getValue(i,'cid')]['blocked'] = 1;}
                    else{Services.cidLookUp[model.getValue(i,'cid')]['blocked'] = 0;}
                    Services.cidLookUp[model.getValue(i,'cid')]['childlist'] = Services._getChildList(model,model.getValue(i,'cid'));
                    if(Services.AttrForPaxus!=''){Services.cidLookUp[model.getValue(i,'cid')]['paxus']=model.getValue(i,'paxus');}
                    if(Services.PreloadCategoryPosters){
                       Services.cidLookUp[model.getValue(i,'cid')]['category_posters']=[];
                        if(Services.CustomPosterLabels.length){
                            for(var l in Services.CustomPosterLabels){
                                if(typeof model.getValue(i,Services.CustomPosterLabels[l] != 'undefined')){
                                    Services.cidLookUp[model.getValue(i,'cid')]['category_posters'][Services.CustomPosterLabels[l]]=model.getValue(i,Services.CustomPosterLabels[l]);
                                   //debug : console.log("MediaServices | __processLookupModel | Services.cidLookUp["+model.getValue(i,'cid')+"]['category_posters']["+Services.CustomPosterLabels[l]+"] : "+Services.cidLookUp[model.getValue(i,'cid')]['category_posters'][Services.CustomPosterLabels[l]])
                                }
                            }
                        }else{
                            if(typeof model.getValue(i,'poster')!= 'undefined'){
                                Services.cidLookUp[model.getValue(i,'cid')]['category_posters']['poster']=model.getValue(i,'poster');
                                //debug : console.log("MediaServices | __processLookupModel | Services.cidLookUp["+model.getValue(i,'cid')+"]['category_posters']['poster'] : "+Services.cidLookUp[model.getValue(i,'cid')]['category_posters']['poster'])
                            }
                            if(typeof model.getValue(i,'synopsisposter')!= 'undefined'){
                            Services.cidLookUp[model.getValue(i,'cid')]['category_posters']['synopsisposter']=model.getValue(i,'synopsisposter');
                            }
                        }
                    }
                }

            }
            tmpCustomServiceArray=[];

            for(var tag_name in Services.tagLookUp){core.info("MediaServices | __processLookupModel | tag_name Avaliable | "+tag_name+" : "+Services.tagLookUp[tag_name].cid);}
            for(var customService in Services.serviceLookUp){core.info("MediaServices | __processLookupModel | serviceLookUp Avaliable | "+customService+" : "+Services.serviceLookUp[customService]);}
            if(Services.isParentBlockRequired){for(var cid1 in Services.cidLookUp){ if(Services.cidLookUp[cid1]['blocked'] == 1){/*core.info("blocked cid "+Services.cidLookUp[cid1]['tid']);*/ Services.__setParentCidAccessType(Services.cidLookUp[cid1]['pcid'],'blocked')}}}
        }
    }
    function __processBundleModel(model){
        core.info("MediaServices | __processBundleModel | model.count :"+model.count);
        for(var i=0; i<model.count; i++){
            if(Services.bundleLookup[model.getValue(i,'bundle_parentmid')]==undefined){Services.bundleLookup[model.getValue(i,'bundle_parentmid')] = [];}
                if(Services.bundleLookup[model.getValue(i,'bundle_parentmid')][model.getValue(i,'mid')] == undefined){
                    Services.bundleLookup[model.getValue(i,'bundle_parentmid')][model.getValue(i,'mid')] = model.getValue(i,'mid');
                    if(model.getValue(i,'categorymedia_accesstype') == 'pay'){
                       if(Services.payMidArr[model.getValue(i,'bundle_parentmid')] == undefined){Services.payMidArr[model.getValue(i,'bundle_parentmid')] = 1;}
                        if(Services.payMidArr[model.getValue(i,'mid')] == undefined){Services.payMidArr[model.getValue(i,'mid')] = 1;}
                    }
                }
        }
        if(Services.cidListByBundleEnabled && model.count!=0)core.dataController.__getBundleCidLookupModel(Services.bundleLookup);
    }

    /* PAY = 1  PAIDORFREE = 0*/
    function setPurchaseMidStatus(mid,status,cause){
        core.info("MediaServices.js | setPurchaseMidStatus | mid = "+mid+" | status = "+status);
         var tmparr =[]; var bmid = 0;
        if(status == 'pay'){
                Services.payMidArr[mid] = 1;core.info("MediaServices.js | setPurchaseMidStatus | mid = "+mid+" | Accesstype = Pay | value = "+Services.payMidArr[mid]);
                if(Services.bundleLookup[mid] != undefined){
                    bmid = mid;
                    for(var key in Services.bundleLookup[mid]){Services.payMidArr[key] = 1; tmparr.push(key);core.info("MediaServices.js | setPurchaseMidStatus | bmid = "+mid+" | mid = "+key+"  | Accesstype = Pay | value = 1");}
                }else{tmparr.push(mid);}
                if(cause != 'PPV_REFUND')sigMidPaidStatusChanged(tmparr,bmid,'pay',cause);
        }
        else{
                Services.payMidArr[mid]=0;
                core.info("MediaServices.js | setPurchaseMidStatus | mid = "+mid+" | Accesstype = Paidorfree| value = "+Services.payMidArr[mid])
                if(Services.bundleLookup[mid] != undefined){
                    bmid = mid;
                    for(var key1 in Services.bundleLookup[mid]){Services.payMidArr[key1]=0; tmparr.push(key1);core.info("MediaServices.js | setPurchaseMidStatus | bmid = "+mid+" | mid = "+key1+"  | Accesstype = Paidorfree");}
                }else{tmparr.push(mid);}
                setMidAccessType('paidorfree',mid,1);
                if(cause != 'PPV_REFUND')sigMidPaidStatusChanged(tmparr,bmid,'paidorfree',cause);
        }
    }
    function setPurchaseMidStatusOnRefund(){
        var bmid = 0; var tmparr =[];var mid = Services.refundMidData['mid'];var status =  getMidPurchaseStatus(Services.refundMidData['mid']); var cause = Services.refundMidData['cause'];
        if(status == 'pay' || (status == 'notexist' && Services.refundMidData['status'] == 'pay')){ //need to check if this exists
            if(Services.bundleLookup[mid] != undefined){
                bmid = mid;
                for(var key in Services.bundleLookup[mid]){
                    if(Services.payMidArr[key]==1 || Services.payMidArr[key]==undefined){tmparr.push(key);}
                }
            }else{tmparr.push(mid);}
            core.info("MediaServices.qml | setPurchaseMidStatusOnRefund | Found mid:"+mid+" | FS status : "+status+" emitting : pay");
            sigMidPaidStatusChanged(tmparr,bmid,'pay',cause);
        }else{
            core.info("MediaServices.qml | setPurchaseMidStatusOnRefund | Found  mid:"+mid+" | status : "+status+" not emitting signal");
        }
        Services.isMidRefundCalled=false;
        Services.refundMidData=[];
    }
    function __setcApiRouteData(num){Services.cApiRouteData=num;}
    function __setIsParentBlockRequired(bool){Services.isParentBlockRequired=bool;}
    function __setDataController(dc){mediaServices.dataController=dc;}

    function __getRouteInfo(){
        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/media/getjsonflightinfo.json';
            core.info('MediaServices.qml | __getRouteInfo | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            routeInfoLoader.getLocalData(getParams)
        }else{
            var url=core.settings.remotePath+"/mediadbapi/apijson.php";
            routeInfoLoader.getData(url,-1,"api=getjsonflightinfo&seatnum="+core.pif.getSeatNumber());
        }
    }

    function __setConfiguration(dataSection){
        //node look up
        var tmpArr = coreHelper.updateCfgArray (dataSection.DataCategoryLookUp, "name,tid,code");
        Services.templateIdMap=[];
        for (var i=0; i<tmpArr.length; i++) {
            if(!Services.templateIdMap[tmpArr[i].name]){Services.templateIdMap[tmpArr[i].name]= []}
            Services.templateIdMap[tmpArr[i].name][tmpArr[i].tid] = tmpArr[i].code
        }
         Services.AttrForPaxus = dataSection.DataAttrForPaxus ? dataSection.DataAttrForPaxus:Services.AttrForPaxus;
        //service blocking
        Services.showBlockCid = dataSection.DataShowBlockCid?(dataSection.DataShowBlockCid=="false"?false:true):Services.showBlockCid;
        Services.showBlockMid = dataSection.DataShowBlockMid?(dataSection.DataShowBlockMid=="false"?false:true):Services.showBlockMid;
        Services.blockCidPreOpenFlight =  dataSection.DatablockCidPreOpenFlight?(dataSection.DatablockCidPreOpenFlight=="false"?false:true):Services.blockCidPreOpenFlight;
        Services.exceptionParentBlockTemplates = coreHelper.cfgVariantObjToArray(dataSection.DataExceptionParentBlockTemplates);

        Services.cidListByBundleEnabled = dataSection.DataCidListByBundleEnabled?(dataSection.DataCidListByBundleEnabled=="false"?false:true):Services.cidListByBundleEnabled;
        Services.PreloadCategoryPosters = dataSection.DataPreloadCategoryPosters?(dataSection.DataPreloadCategoryPosters=="false"?false:true):Services.PreloadCategoryPosters;
        Services.CustomPosterLabels = dataSection.DataCustomPosterLabels?coreHelper.cfgVariantObjToArray(dataSection.DataCustomPosterLabels):Services.CustomPosterLabels;
    }
    DataLoader {
        id: routeInfoLoader;
        onSigDataReady: {if(Services.isMidRefundCalled && !(dataController.__getInteractiveLoadingStatus())){__updateRouteInfo(errCode,responseObj,responseString);}else{__processRouteInfo(errCode,responseObj,responseString);}}
        onSigLocalDataReady:{if(Services.isMidRefundCalled && !(dataController.__getInteractiveLoadingStatus())){__updateRouteInfo(errCode,responseObj,'fromConfigModel');}else{__processRouteInfo(errCode,responseObj,'fromConfigModel');}}

    }

    function __processRouteInfo(errCode,responseObj,responseString){
        if (errCode==0 && responseObj && responseObj.system && responseObj.seat){
            core.info("MediaServices | __processRouteInfo | responseString: "+responseString);
            core.info("MediaServices | __processRouteInfo | aircraftType: "+responseObj.system.aircraftType+" | seatRating: "+responseObj.seat.seatrating);

            dataController.__setRouteData({'seatrating':((responseObj.seat.seatrating==0 || responseObj.seat.seatrating==0)?254:responseObj.seat.seatrating),'aircraftType':responseObj.system.aircraftType,'aircraftSubType':responseObj.system.aircraftSubType});
            if(responseObj.seat.midpaid){for(x in responseObj.seat.midpaid){setPurchaseMidStatus(x,'pay','')}}
            if(responseObj.seat.midblocked){for(x in responseObj.seat.midblocked){setMidAccessType('blocked',x,2);}}
            if(responseObj.seat.midpaidorfree){for(x in responseObj.seat.midpaidorfree){setMidAccessType('paidorfree',x,2);setPurchaseMidStatus(x,'paidorfree','');}}
            if(responseObj.seat.serviceblocked){
                for(var kservice in responseObj.seat.serviceblocked){
                    for(var ksmid in Services.serviceCodeLookup){
                        for(var kcid in Services.serviceCodeLookup[ksmid]){
                            if(Services.serviceCodeLookup[ksmid][kcid]==kservice){
                                __setServiceAccessType('blocked',ksmid)
                            }
                        }
                    }
                }
            }
            if(responseObj.seat.servicepaidorfree){
                for(var kservice in responseObj.seat.servicepaidorfree){
                    for(var ksmid in Services.serviceCodeLookup){
                        for(var kcid in Services.serviceCodeLookup[ksmid]){
                            if(Services.serviceCodeLookup[ksmid][kcid]==kservice){
                                __setServiceAccessType('paidorfree',ksmid)
                            }
                        }
                    }
                }
            }

//          debug: for(var keyAsSmid in Services.serviceCodeLookup){for(var keyAsCid in Services.serviceCodeLookup[keyAsSmid]){core.info('MediaServices | onSigDataReady | Services Status | '+Services.serviceCodeLookup[keyAsSmid][keyAsCid]+' smid '+keyAsSmid +' cid '+keyAsCid+' : '+Services.cidSmidArr[keyAsCid][keyAsSmid])}}
            core.info("MediaServices | onSigDataReady | Blocked MID LIST : "+Services.midBlockList)
            core.info("MediaServices | onSigDataReady | Blocked CID LIST : "+Services.blockCidList)
            Services.__updateCidBlocked();Services.__updateMidBlocked();

            interactiveStartup.eventHandler(Services.cApiRouteData);dataController.__setInteractiveLoadingStatus(false);
        }else {
            if(!dataController.__makeHeadendRequest()){interactiveStartup.eventHandler(Services.cApiRouteData);dataController.__setInteractiveLoadingStatus(false)}
            else{routeInfoLoader.requestLastUrl(true);}
        }

    }

    function __updateRouteInfo(errCode,responseObj,responseString){
        if (errCode==0 && responseObj && responseObj.system && responseObj.seat){
            core.info("MediaServices | __processRouteInfoForUpdate | responseString: "+responseString);
            core.info("MediaServices | __processRouteInfoForUpdate | aircraftType: "+responseObj.system.aircraftType+" | seatRating: "+responseObj.seat.seatrating);
            Services.payMidArr=[];
           if(responseObj.seat.midpaid){for(x in responseObj.seat.midpaid){setPurchaseMidStatus(x,'pay','PPV_REFUND')}}
           if(responseObj.seat.midpaidorfree){for(x in responseObj.seat.midpaidorfree){setPurchaseMidStatus(x,'paidorfree','PPV_REFUND');}}
            setPurchaseMidStatusOnRefund();
        }else {
            if(dataController.__makeHeadendRequest()){routeInfoLoader.requestLastUrl(true);}
           
        }
    }

    function __resetVariables(){
        core.info("Mediaservices.qml | __resetVariables |setting Default values");
        //look up arrays
        Services.cidLookUp=[];Services.tagLookUp=[];Services.serviceLookUp=[];
        //Service blocking arrays
        Services.serviceCodeLookup=[];Services.serviceCodeStatus=[];Services.cidList = [];
        Services.blockCidList='';Services.blockCidArr=[];Services.cidSmidArr=[];Services.midBlockList='';Services.midBlockArr=[];Services.midBlockArrPreOpenFlight = [];Services.blockCidListPreOpenFlight="";
        //ppv arrays
        Services.refundMidData=[];Services.payMidArr=[];Services.bundleLookup=[];Services.isMidRefundCalled=false;
        //paxus array
        Services.paxusLookUp=[];
    }    
    Component.onCompleted: {__resetVariables();}
}
