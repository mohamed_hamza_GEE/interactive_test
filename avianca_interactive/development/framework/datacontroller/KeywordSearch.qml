import QtQuick 1.1
import Panasonic.Pif 1.0
import "../blank.js" as Store

Item {
    id: keywordSearch
    property variant dataController

    //public function
    function getData(params,callback,modelInstance){
        if(!params['cid'] || !params['template_id']){core.info("KeywordSearch.qml | getData | ERROR | Incomplete parameters ");return false;}
        var sql = '';
        if(params['isParentAggSearch']){ sql = __getParentAggSearch(params)}
        else if(params['isSearchTableAvailable']){sql= __getSearchBySearchTable(params)}
        else if (params['isSearchByUnion']){ sql = __getSearchByQueryUnion(params)}
        else if (params['isSearchByParentCategory']){sql = __getSearchByParentCategory(params)}
        else { sql = __getKeywordMedia(params);}
        modelInstance=modelInstance?modelInstance:Store.cApiKeywordSearch;
        core.info("KeywordSearch.qml | getData | SQL : "+sql);
        dataController.__modelUpdate(Store.cApiKeywordSearch,modelInstance,sql,callback);
    }

    function isValidInput(inputSearch,type){
        if(typeof(inputSearch)=='string'){
            inputSearch = inputSearch.trim();
            if(inputSearch==""  && type!='NA'){ core.info("KeywordSearch.qml | isValidInput | ERROR | Input String is empty");return false;
            }else if(inputSearch.indexOf('%')>=0 || inputSearch.indexOf('_')>=0){core.info("function.js | __getKeywordMedia | ERROR | Invalid input"); return false;}
        }else {
            for(var x in inputSearch){
                if(inputSearch[x]==""){ core.info("KeywordSearch.qml | isValidInput | ERROR | Input String is empty");return false;
                }else if(inputSearch[x].indexOf('%')>=0 || inputSearch[x].indexOf('_')>=0){core.info("function.js | __getKeywordMedia | ERROR | Invalid input"); return false;}
            }
        }
        return true;
    }



    //private functions
    function __getKeywordMedia(params){

        if(!params['cid'] || !params['template_id']){return false;}

        params['inputSearch']   = params['inputSearch']?params['inputSearch']:''
        params['searchType']    = params['searchType']?params['searchType']:''
        params['fieldList']     = params['fieldList']?__formatInput(params['fieldList'],true):'title'
        params['searchBy']      = params['searchBy']?__formatInput(params['searchBy']):'title'    //field on which to search
        params['orderBy']       = params['orderBy']?__formatInput(params['orderBy']):'title'
        params['orderType']     = params['orderType']?__formatInput(params['orderType']):''
        params['groupBy']       = params['groupBy']?__formatInput(params['groupBy']):''
        params['isAggMidSearch']= params['isAggMidSearch']?params['isAggMidSearch']:false
        params['isParentCid']   = params['isParentCid']?params['isParentCid']:false
        params['languageID']    = params['languageID']?params['languageID']:core.settings.languageID
        params['limit']         = params['limit']?params['limit']:'';
        params['isParentInfoRequired']  = params['isParentInfoRequired']?params['isParentInfoRequired']:false
        params['isMidAndAggMidSearch']  = params['isMidAndAggMidSearch']?params['isMidAndAggMidSearch']:false
        params['blockMIdList']  = params['blockMIdList']?params['blockMIdList']:''
        params['blockCidList']  = params['blockCidList']?params['blockCidList']:''
        params['midList']       = params['midList']?params['midList']:''
        params['isRtl']         = (params['languageID']==core.settings.languageID && core.settings.languageISO=='ara')?true:false;

        /* Do not delete
        for(var x in params){core.info("KeywordSearch.qml | __getKeywordMedia | params["+x+"] : "+params[x])}
        core.info(' KeywordSearch.qml | __getKeywordMedia | searchBy '+ typeof(params['searchBy'])+' | typeOfInputSearch '+typeof(params['inputSearch']));*/

        if(!isValidInput(params['inputSearch'],params['searchType'])){return 'SELECT';} //else query will not execute

        var sql=[];var wc=[];

        sql.push("SELECT");
        if(params['isMidAndAggMidSearch']){sql.push(params['fieldList']+",'NA' as aggregate_parentmid");}else {sql.push(params['fieldList']);}
        sql.push(__getParentInfoField(params)+" FROM "+__getTableName(params));

        if(typeof(params['searchType'])=="string" && params['searchType']=="word"){
           sql.push(__getSearchClauseByMatch(params));
        }if(typeof(params['searchType'])!="string" && params['searchType']=="word"){
           core.info("KeywordSearch.qml | __getKeywordMedia | Invalid Search type");
        }else {
            sql.push("where lid="+params['languageID']);
            wc.push(__getSearchClauseByLike(params));
        }

        wc.push(__getCidClause(params));

        if(params['midList']){wc.push("mid in ("+params['midList']+") ");}

        if(params['blockMIdList']){wc.push("mid not in ("+params['blockMIdList']+")");}

        if(params['blockCidList'] && params['isAggMidSearch']){wc.push("aggregate_parentmid not in ( select mid from v_categorymedia where cid in ("+params['blockCidList']+")) ");
        }else if(params['blockCidList']){wc.push("cid not in ("+params['blockCidList']+") ");}

        if(dataController.__getMidBlockList()){wc.push(dataController.__getMidBlockList());}

        if(params['isParentInfoRequired'] && !params['isAggMidSearch']){wc.push("category_attr_template_id!='' ");} // donot return category that doesnot have template_id since it may not be valid

        if(params['template_id'] && params['template_id']!='NA'){if(dataController.getMenuWhereClauseByTemplateId(params['template_id'])){wc.push(dataController.getMenuWhereClauseByTemplateId(params['template_id']))}}

        if(dataController.__getSeatRatingClause()){wc.push(dataController.__getSeatRatingClause());}

        if(!params['isAggMidSearch']){wc.push(dataController.__getMediaDateClause());wc.push(dataController.__getMediaConfigClause());} //dont include this if its an aggegetare mid

        sql.push('and '+wc.join(' and '));
        if(params['groupBy']!=''){sql.push("group by "+params['groupBy']);}

        if(params['isMidAndAggMidSearch']){
            sql.push(" union ");
            var tmpParams = params
            tmpParams['fieldList'] = tmpParams['fieldList'].replace(",'NA' as aggregate_parentmid ",'');
            tmpParams['fieldList'] = tmpParams['fieldList']+", aggregate_parentmid"
            tmpParams['isAggMidSearch'] = true;
            tmpParams['isParentCid'] = true;
            tmpParams['isMidAndAggMidSearch'] = false;
            sql.push(__getKeywordMedia(tmpParams)); // this will return the aggegerate mid search clause
        }else if(params['orderBy'] != 'NA'){
            //since it is recersive called we get the oeder by clause twice to avoid it we include only once
            sql.push("order by "+params['orderBy']);
            sql.push(params['orderType']);
            if(params['limit']){sql.push("limit "+params['limit']);}
        }

        return sql.join(' ');
    }

    function __formatInput(value,replace){
        if(typeof(value)=='string'){
            if(value.indexOf('[cast]')<0 && value.indexOf('cast')>=0){value = replace?value.replace('cast','[cast] as cast'):value.replace('cast','[cast]');}
        }else {
            for(var x in value){if(value[x].indexOf('[cast]')<0 && value[x].indexOf('cast')>=0){value[x] = replace?value[x].replace('cast','[cast] as cast'):value[x].replace('cast','[cast]');}}
        }
        return value;
    }

    function __getParentAggSearch(params){
        if (params['isParentAggSearch']){
            var sql = []; var tempParams = [];
            //tempParams = params;
            for(var key in params){
                tempParams[key] = params[key];
            }
            tempParams['fieldList'] = 'mid';
            sql.push("Select "+params['fieldList']+" from v_media_aggs where aggregate_parentmid in ("+__getKeywordMedia(tempParams)+")");            
            sql.push(" and lid="+(params['languageID']?params['languageID']:core.settings.languageID));
            if(params['groupBy']!=''){sql.push("group by "+params['groupBy']);}
            if(params['orderBy'] != 'NA'){
                //since it is recursively called we get the order by clause twice to avoid it we include only once
                sql.push("order by "+params['orderBy']);
                sql.push(params['orderType']);
                if(params['limit']){sql.push("limit "+params['limit']);}
            }
             return sql.join(' ');
        }
    }

    function __getParentInfoField(params){
        var parentInfoField = params['isParentInfoRequired']?',(select title from v_categories where cid = v_cm.cid and lid='+core.settings.languageID+') as cid_title,( select '+dataController.getCategoryAttributeForTemplate()+' from v_categories where cid = v_cm.cid) as category_attr_template_id ':''
        parentInfoField     = (params['isParentInfoRequired'] && params['isAggMidSearch'])?',(select title from  v_categorymedia where mid = v_ma.aggregate_parentmid and lid='+core.settings.languageID+') as parent_mid_title,(select cid from  v_categorymedia where mid = v_ma.aggregate_parentmid) as cid ':parentInfoField
        return parentInfoField;
    }

    function __getTableName(params){return params['isSearchByParentCategory']?"v_categories as v_cs":params['isAggMidSearch']?"v_media_aggs as v_ma":"v_categorymedia v_cm";}

    function __getCidClause(params){
        var parentField     = params['isAggMidSearch']?"aggregate_parentmid":"cid";
        var sql = '';
        var cid = params['cid'];

        if(params['isParentCid']){
            cid = String(cid)
            var tmpCidCont = cid.indexOf(',')<0?"cid = "+cid+"":"cid in ("+cid+")"
            sql = parentField+" in ( select mid from v_categorymedia v_cm  where "+tmpCidCont+" and "+dataController.__getMediaDateClause()+" and "+dataController.__getMediaConfigClause()+" )";
        }else {
            if(Number(cid)){sql = parentField+" = "+cid;}
            else if(cid=="NA"){sql = "("+parentField+"!='' and "+parentField+"!=0)";}
            else{sql = parentField+" in ("+cid+")";}
        }
        return sql;
    }

    function __getSearchClauseByMatch(params){
        var sql=[];
        if(params['searchBy']=='title'){
            if(params['isAggMidSearch']){ //for tracks search_audio_labels is used else search_media_labels
                sql.push(' ,(SELECT rowid, search_title FROM search_audio_labels WHERE search_title MATCH ("'+params['inputSearch']+'*")) as s ');
            }else {
                sql.push(' ,(SELECT rowid, search_title FROM search_media_labels WHERE search_title MATCH ("'+params['inputSearch']+'*")) as s ');
            }
        }else if(params['searchBy']=='albumtitle'){
            sql.push(' ,(SELECT rowid, search_title FROM search_album_labels WHERE search_title MATCH ("'+params['inputSearch']+'*")) as s ');
        }else if(params['searchBy']=='director'){
            sql.push(' ,(SELECT rowid, search_director FROM search_media_labels WHERE search_director MATCH ("'+params['inputSearch']+'*")) as s ');
        }else if(params['searchBy']=='[cast]'){
            sql.push(' ,(SELECT rowid, search_cast FROM search_media_labels WHERE search_cast MATCH ("'+params['inputSearch']+'*")) as s ');
        }
        sql.push("where lid="+params['languageID']);
        if(params['searchBy']=='artist'){ // music artist we are not using match so s. is not used insted doing like
            sql.push("and ( lower("+params['searchBy']+") like '"+params['inputSearch']+"%' or lower("+params['searchBy']+") like '% "+params['inputSearch']+"%' )");
        }else if(params['isAggMidSearch']){
            sql.push("and v_ma.media_label_id=s.rowid");
        }else {
            sql.push("and v_cm.media_label_id=s.rowid");
        }

        return sql.join(' ');
    }

    function __getSearchClauseByLike(params){
        var sql=[];
        if(typeof(params['searchBy'])=='string'){params['searchBy']=[params['searchBy']];}

        if(params['searchType']=='NA'){return " "+params['searchBy'][0]+" !='' " };
        var tmpInp = '';sql.push("(");
        for(var i=0;i<params['searchBy'].length;i++){
            if(typeof(params['inputSearch'])=='string'){tmpInp = params['inputSearch'];
            }else {tmpInp = params['inputSearch'][i];}
            tmpInp = tmpInp.replace("'","''");
            var andOr = (i==0)?'( ':' or (';
            if(params['searchType']=="first"){
                sql.push(!params['isRtl']?andOr+params['searchBy'][i]+" like '"+tmpInp+"%' ":andOr+params['searchBy'][i]+" like '%"+tmpInp+"'");
            }else if(params['searchType']=="full"){
                sql.push(andOr+" lower("+params['searchBy'][i]+") = '"+tmpInp+"'");
            }else if(params['searchType']=="word"){
                //this is included after frm clause //sql +=  andOr+" ( lower("+params['searchBy'][i]+") like '"+tmpInp+"%' or lower("+params['searchBy'][i]+") like '% "+tmpInp+"%' ) "
            }else if(params['searchType']=="atLeastOneWord"){
                sql.push(andOr+"(");
                tmpInp=tmpInp.replace(/[&\/\\#,+()$~%.":*?<>{}|]/g, ' ');
                tmpInp = tmpInp.split(" ")
                for(var x in tmpInp ){
                    if(tmpInp[x]!=''){
                        if(x==0){sql.push(!params['isRtl']?"( lower("+params['searchBy'][i]+") like '"+tmpInp[x]+"%' or lower("+params['searchBy'][i]+") like '% "+tmpInp[x]+"%' ) ":"( lower("+params['searchBy'][i]+") like '%"+tmpInp[x]+"' or lower("+params['searchBy'][i]+") like '%"+tmpInp[x]+" %' )");
                        }else{sql.push(!params['isRtl']?" or ( lower("+params['searchBy'][i]+") like '"+tmpInp[x]+"%' or lower("+params['searchBy'][i]+") like '% "+tmpInp[x]+"%' ) ":" or ( lower("+params['searchBy'][i]+") like '%"+tmpInp[x]+"' or lower("+params['searchBy'][i]+") like '%"+tmpInp[x]+" %' )")}
                    }
                }
                sql.push(")");
                if(params['cid']=="NA"){sql.push(andOr+" (media_type = 'audio' or media_type = 'audioAggregate' or media_type = 'video' or media_type = 'videoAggregate')");}
            }else if(params['searchType']=='existInList'){
                sql.push(andOr+"(");
                tmpInp=tmpInp.replace(/[&\/\\#,+()$~%.":*?<>{}|]/g, '#');
                tmpInp = tmpInp.split("#")

                for(var x in tmpInp ){
                    if(tmpInp[x]!=''){
                        if(x==0){sql.push(!params['isRtl']?"( lower("+params['searchBy'][i]+") like '"+tmpInp[x]+",%' or lower("+params['searchBy'][i]+") like '%,"+tmpInp[x]+",%' or lower("+params['searchBy'][i]+") like '%,"+tmpInp[x]+"' or lower("+params['searchBy'][i]+") == '"+tmpInp[x]+"') ":"( lower("+params['searchBy'][i]+") like '%,"+tmpInp[x]+"' or lower("+params['searchBy'][i]+") like '%,"+tmpInp[x]+",%')");
                        }else{sql.push(!params['isRtl']?" or ( lower("+params['searchBy'][i]+") like '"+tmpInp[x]+",%' or lower("+params['searchBy'][i]+") like '%,"+tmpInp[x]+",%' or lower("+params['searchBy'][i]+") like '%,"+tmpInp[x]+"' or lower("+params['searchBy'][i]+") == '"+tmpInp[x]+"') ":" or ( lower("+params['searchBy'][i]+") like '%,"+tmpInp[x]+"' or lower("+params['searchBy'][i]+") like '%,"+tmpInp[x]+",%')")}
                    }
                }
                sql.push(")");
                if(params['cid']=="NA"){sql.push(" and (media_type = 'audio' or media_type = 'audioAggregate' or media_type = 'video' or media_type = 'videoAggregate')");}
            }else if(params['searchType']=="allWords"){
                sql.push(andOr+" (");
                sql.push(!params['isRtl']?"( lower("+params['searchBy'][i]+") like '"+tmpInp+"%' or lower("+params['searchBy'][i]+") like '% "+tmpInp+"%' ) ":"( lower("+params['searchBy'][i]+") like '%"+tmpInp+"' or lower("+params['searchBy'][i]+") like '%"+tmpInp+" %' )");
                sql.push(")");
                if(params['cid']=="NA"){
                    sql.push(andOr+" (media_type = 'audio' or media_type = 'audioAggregate' or media_type = 'video' or media_type = 'videoAggregate')");
                }
            }else if(params['searchType']=="audio"){
               sql.push(andOr+" ( lower("+params['searchBy'][i]+") like '"+tmpInp+"%' or lower("+params['searchBy'][i]+") like '% "+tmpInp+"%' )");
            }else if(params['searchType']=="firstAudio"){
                sql.push(andOr+" lower("+params['searchBy'][i]+") like '"+tmpInp+"%'");
            }else if(params['searchType']=="video"){
                sql.push(andOr+" ( lower("+params['searchBy'][i]+") like '"+tmpInp+"%' or lower("+params['searchBy'][i]+") like '% "+tmpInp+"%' )");
            }else if(params['searchType']=="firstVideo"){
                 sql.push(andOr+" lower("+params['searchBy'][i]+") like '"+tmpInp+"%'");
            }else if(params['searchType']=="NA"){
                //do not do anything
            }else if(params['searchType']=="hashFirst"){
                sql.push(andOr+params['searchBy'][i]+" GLOB \"[@0-9]*\" ");
            }else{ //partial
                sql.push(andOr+params['searchBy'][i]+" like '%"+tmpInp+"%' ");
            }
            if(params['searchType']!="word"){
                sql.push("and "+params['searchBy'][i]+" !='' )");  // null or empty records are not returned
            }
       }
        sql.push(")");

        if(params['searchType']=="audio" || params['searchType']=="firstAudio"){sql.push("and (media_type = 'audio' or media_type = 'audioAggregate')");}
        if(params['searchType']=="video" || params['searchType']=="firstVideo"){sql.push("and (media_type = 'video' or media_type = 'videoAggregate')");}

        return sql.join(' ');
    }

    function __getSearchByQueryUnion(tparams){
        if(typeof(tparams['searchBy'])=='string'){tparams['searchBy']=[tparams['searchBy']];}
        var temp_params = []; var sql = '';
        temp_params['cid'] = tparams['cid']
        temp_params['template_id'] =  tparams['template_id'];
        temp_params['orderBy'] = 'NA';
        temp_params['inputSearch']   = tparams['inputSearch']?tparams['inputSearch']:''
        temp_params['searchType']    = tparams['searchType']?tparams['searchType']:''
        temp_params['orderBy']       = 'NA'
        temp_params['isAggMidSearch']= tparams['isAggMidSearch']?tparams['isAggMidSearch']:false
        temp_params['isParentCid']   = tparams['isParentCid']?tparams['isParentCid']:false
        temp_params['languageID']    = tparams['languageID']?tparams['languageID']:core.settings.languageID
        temp_params['isParentInfoRequired']  = tparams['isParentInfoRequired']?tparams['isParentInfoRequired']:false
        temp_params['isMidAndAggMidSearch']  = tparams['isMidAndAggMidSearch']?tparams['isMidAndAggMidSearch']:false
        temp_params['blockMIdList']  = tparams['blockMIdList']?tparams['blockMIdList']:''
        temp_params['midList']       = tparams['midList']?tparams['midList']:''
        temp_params['isRtl']         = (tparams['languageID']==core.settings.languageID && core.settings.languageISO=='ara')?true:false;
        temp_params['groupBy'] = tparams['groupBy']?tparams['groupBy']:'';
        for(var i in tparams['searchBy']){
            temp_params['searchBy'] = tparams['searchBy'][i];

            //temp_params['isMidAndAggMidSearch']  = tparams['isMidAndAggMidSearch']?tparams['isMidAndAggMidSearch']:false
            temp_params['fieldList'] = tparams['fieldList']+', '+tparams['searchBy'][i]+' as searchby'+",'"+tparams['searchBy'][i]+"' as searchByField ";
            if(temp_params['isAggMidSearch']){
                temp_params['fieldList'] = temp_params['fieldList']+", 'NA' as aggregate_parentmid"
            }
            if(i==0){sql = __getKeywordMedia(temp_params)}else{sql += ' union '+ __getKeywordMedia(temp_params)}
        }

        if(tparams['orderBy']){sql +=' order by '+tparams['orderBy']}
        if(tparams['limit']){sql += " limit "+tparams['limit'];}
        return sql;
    }

    function __getSearchBySearchTable(tparams){
        var temp_params = []; var sql = '';
        temp_params['cid'] = tparams['cid']
        temp_params['template_id'] =  tparams['template_id'];
        temp_params['fieldList']     = tparams['fieldList']?__formatInput(tparams['fieldList'],true):'title'
        temp_params['fieldList']=tparams['snippetNeeded']?(temp_params['fieldList']+",snippet"):(temp_params['fieldList'])
        temp_params['orderBy'] = 'NA';
        temp_params['inputSearch']   = tparams['inputSearch']?tparams['inputSearch']:''
        temp_params['orderBy']       = tparams['orderBy']?tparams['orderBy']:'title'
        temp_params['languageID']    = tparams['languageID']?tparams['languageID']:core.settings.languageID
        temp_params['blockMIdList']  = tparams['blockMIdList']?tparams['blockMIdList']:''
        temp_params['midList']       = tparams['midList']?tparams['midList']:''
        temp_params['srchTable'] = tparams['mediaType']=="vod"?"search_media_labels":tparams['mediaType']=="aod"?"search_audio_labels":"search_media_labels";
        var wc=[]
        if(temp_params['midList']){wc.push("mid in ("+temp_params['midList']+") ");}

        if(temp_params['blockMIdList']){wc.push("mid not in ("+temp_params['blockMIdList']+")");}

        if(temp_params['blockCidList']){wc.push("cid not in ("+temp_params['blockCidList']+") ");}
        wc.push(' lid='+(temp_params['languageID']?temp_params['languageID']:core.settings.languageID))
        wc.push('cid in ('+temp_params['cid']+')')
        if(dataController.__getSeatRatingClause()) wc.push(dataController.__getSeatRatingClause())
        if(dataController.__getMidBlockList()){wc.push(dataController.__getMidBlockList());}
        wc.push(dataController.__getMediaDateClause())
        wc.push(dataController.__getMediaConfigClause())

        if(tparams['snippetNeeded'])
            sql = 'SELECT distinct '+temp_params['fieldList']+' FROM  v_categorymedia v_cm, (SELECT rowid,snippet('+temp_params['srchTable']+') as snippet FROM '+temp_params['srchTable']+' WHERE '+temp_params['srchTable'] +' MATCH ("'+temp_params['inputSearch']+'*")) as s Where  '+wc.join(' and ')+' and v_cm.media_label_id=s.rowid order by '+temp_params['orderBy'];
        else
            sql = 'SELECT distinct '+temp_params['fieldList']+' FROM  v_categorymedia v_cm, (SELECT rowid FROM '+temp_params['srchTable']+' WHERE '+temp_params['srchTable']+' MATCH ("'+temp_params['inputSearch']+'*")) as s Where  '+wc.join(' and ')+' and v_cm.media_label_id=s.rowid order by '+temp_params['orderBy'];

        return sql;
    }

    function __getSearchByParentCategory(params){
        if(!params['cid']) {core.error("KeywordSearch.qml | __getSearchByParentCategory parentcid not provided, exiting")}
        params['searchType']    = params['searchType']?params['searchType']:''
        params['fieldList']     = params['fieldList']?__formatInput(params['fieldList'],true):'title'
        params['searchBy']      = params['searchBy']?__formatInput(params['searchBy']):'title'
        params['orderBy']       = params['orderBy']?__formatInput(params['orderBy']):'title'
        params['orderType']     = params['orderType']?__formatInput(params['orderType']):''
        params['blockCidList']  = params['blockCidList']?params['blockCidList']:''
        params['languageID']    = params['languageID']?params['languageID']:core.settings.languageID

        if(!isValidInput(params['inputSearch'],params['searchType'])){return 'SELECT';}
        var sql=[];var wc=[];

        sql.push("SELECT");
        sql.push(params['fieldList'])
        sql.push(" FROM "+__getTableName(params))
        sql.push("where parentcid in ("+params['cid']+") and lid="+params['languageID']);
        wc.push(__getSearchClauseByLike(params));
        if(params['blockCidList']){wc.push("cid not in ("+params['blockCidList']+") ");}
        if(dataController.__getSeatRatingClause()){wc.push(dataController.__getSeatRatingClause());}
        wc.push(dataController.__getCategoryDateClause());
        wc.push(dataController.__getMediaConfigClause());
        sql.push('and '+wc.join(' and '));
        if(params['orderBy'] != 'NA'){
            sql.push("order by "+params['orderBy']);
            sql.push(params['orderType']);
            if(params['limit']){sql.push("limit "+params['limit']);}
            return sql.join(' ');
        }
    }

    function __setApiId(id){Store.cApiKeywordSearch=id;}
    function __setDataController(dc){keywordSearch.dataController=dc;}

    Component.onCompleted: {Store.cApiKeywordSearch=0;core.info("KeywordSearch.qml | Component.onCompleted | Keyword Search LOAD COMPLETE");}
}
