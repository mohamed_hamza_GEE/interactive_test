import QtQuick 1.0
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

DataLoader{
    id: ltn

    property bool __isSubCategoryLastSelected :false

    signal __sigModelReady(variant model,bool status,string type);

    onSigDataReady: {
        if (errCode==0){
            core.info("Ltn.qml | dataLoader | onSigDataReady");
            __processModel(responseObj,responseString,requestId);
        }else {
            __sigModelReady('',false,"");
        }
    }

    onSigLocalDataReady:{
        if(errCode==0){
            core.info("Ltn.qml | dataLoader | onSigLocalDataReady ");
            __processModel(responseObj,'fromConfigModel',requestId)
        }else{
            __sigModelReady('',false,"");
        }
    }

    //public functions
    function getRootCategories(langIso,imgType,callback){
        if (isAppActive==true){core.info('Ltn.qml| getAllCategories | App is Active, current Request is dropped.... ');return;}else{isAppActive=true;}

        core.info('Ltn.qml | getAllCategories | Is called');
        __updateCallback(callback);
        var requestId=1;
        var getParams = '';
        imgType=imgType?imgType:''
        langIso=langIso?(Store.iso[langIso]?Store.iso[langIso]:(Store.iso[Store.defaultIso]?Store.iso[Store.defaultIso]:'en')):''

        if(!dataController.__makeHeadendRequest()){
            getParams = '../../content/ltn/getLangsPrvdrsCatsHdlns.json'
            core.info('Ltn.qml | getAllCategories | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            __getLocalData(getParams,requestId);
        }else{
            getParams = "function=getLangsPrvdrsCatsHdlns"+("&lang="+langIso)+("&imgtype="+imgType)
            core.info('Ltn.qml | getAllCategories | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
             __getRemoteData(getParams,requestId);
            }
        }
    }

    function updateLtnAvailableLangs(callback){
         __updateCallback(callback);
        var requestId=8;
        var getParams = '';
        if(!dataController.__makeHeadendRequest()){
            getParams = '../../content/ltn/getLangsPrvdrsCatsHdlns.json'
            core.info('Ltn.qml | getAllCategories | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            __getLocalData(getParams,requestId);
        }else{
            getParams = "function=getLangsPrvdrsCatsHdlns"+("&lang=")+("&imgtype=")
            core.info('Ltn.qml | getAllCategories | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
             __getRemoteData(getParams,requestId);
            }
        }
    }

    function getNextList(index,callback){
        core.info('Ltn.qml | getNextList | Is called index :'+index);
        __updateCallback(callback);
        if(__isSubCategoryLastSelected){var subCat = subCategoryModel.get(index).subCats;}
        else{var subCat = categoryModel.get(index).subCats;}

        console.log("Ltn.qml | getNextList  | categoryModel childCount   "+categoryModel.get(index).childCount+"    __isSubCategoryLastSelected     "+__isSubCategoryLastSelected);
        if(categoryModel.get(index).childCount>0 && subCat.count){
            var requestId=4;
            __loadLTNCategory('',categoryModel.get(index).catId);
            __isSubCategoryLastSelected = true;
            core.info('Ltn.qml | getNextList | requestId : '+requestId+' | Reading Sub Category List : subCategory');
            __sigModelReady(subCategoryModel,true,"subCategory");
            return 'subCategory';
        }else if(subCat.count){
            var requestId=4;
            __loadLTNCategory('',subCategoryModel.get(index).catId);
            __isSubCategoryLastSelected = true;
            core.info('Ltn.qml | getNextList | requestId : '+requestId+' | Reading Sub Category List : subCategory');
            __sigModelReady(subSubCategoryModel,true,"subCategory");
            return 'subCategory';
        }else{
            var requestId=5;
            if(__isSubCategoryLastSelected){__loadLTNArticles(subCategoryModel.get(index).cat_index);}else{__loadLTNArticles(categoryModel.get(index).cat_index);}
            core.info('Ltn.qml | getNextList  | requestId : '+requestId+' | Reading Article List : articles');
            __sigModelReady(articles,true,"articles");
            return 'articles';
        }
    }

    function getArticleByCid(cid,imgType,callback){
        if (isAppActive==true){core.info('Ltn.qml| getArticleByCid | App is Active, current Request is dropped.... ');return;}else{isAppActive=true;}

        core.info('Ltn.qml | getArtsByCid | cid = '+cid);
        __updateCallback(callback);
        var requestId = 2;
        var getParams = '';
        imgType=imgType?imgType:''

        if(!dataController.__makeHeadendRequest()){
            getParams = '../../content/ltn/AllArtByCid.json'
            core.info('Ltn.qml | getArticleByCid | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            __getLocalData(getParams,requestId);
        }else {
            var imgParams  = '&imgtype='+imgType
            getParams = "function=getArtImgsByCat&catid="+cid+imgParams
            core.info('Ltn.qml | getArticleByCid | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
             __getRemoteData(getParams,requestId);
            }
        }
    }

    function getFirstArticlesByCid(cId){
        core.log('Ltn.qml | getArticlesHeadLineById | cid :'+cId);

        var articleList = Store.storeArticle;
        var categoryList = Store.storeCategory;
        for(var index=0; index<categoryList.length; index++){
            if(categoryList[index].catId==cId){
                var selCat = categoryList[index];
                if(!selCat.arts && !selCat.arts.length){core.log('Ltn.qml | getArticlesHeadLineById  | cid :'+cId+' | Articles is empty');}
                var articleNumber = selCat.arts[0];
                for(var jIndex = 0; jIndex < articleList.length; jIndex++){
                    if(articleList[jIndex].artId == articleNumber){
                        return articleList[jIndex];
                    }
                }
            }
        }
        core.log('Ltn.qml | getArticlesHeadLineById | cid :'+cId+' | Articles not found');
    }

    function getForecastByCity(icao_code,numOfDays,callback){
        if (isAppActive==true){
            core.info('Ltn.qml| getForecastByCity | App is already Active, current Request is dropped.... ');return;
        }else if(!icao_code){
            core.info('Ltn.qml | getForecastByCity | ERROR | cityid : '+icao_code+' | IACO CODE is not set | Not Processing ');
            return;
        }else{isAppActive=true;}

        core.info('Ltn.qml | getForecastByCity | cityid : '+icao_code+' | numDays : '+numOfDays);

        __updateCallback(callback);
        var requestId=3;
        var getParams = '';
        if(!dataController.__makeHeadendRequest()){
            getParams = '../../content/ltn/getFrcstWtrByCity.json'
            core.info('Ltn.qml | getForecastByCity | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            __getLocalData(getParams,requestId)
        }
        else {
            getParams = "function=getFrcstWtrByCity&cityid="+icao_code+"&numDays="+numOfDays
            core.info('Ltn.qml | getForecastByCity | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    function getForecastByCityRange(startCityAlpha,endCityAlpha,callback){
        if (isAppActive==true){core.info('Ltn.qml| getForecastByCityRange | App is already Active, current Request is dropped.... ');return;}else{isAppActive=true;}

        core.info('Ltn.qml | getForecastByCityRange | startCityAlpha : '+startCityAlpha+' | endCityAlpha : '+endCityAlpha);
        __updateCallback(callback);
        var requestId = 6;
        var getParams = '';
        if(!dataController.__makeHeadendRequest()){
            getParams = '../../content/ltn/getFrcstWtrByCityRange.json'
            core.info('Ltn.qml | getForecastByCityRange | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            __getLocalData(getParams,requestId)
        }else {
            getParams =  "function=getCurrentWtr&startAlpha="+startCityAlpha+"&endAlpha="+endCityAlpha+""
            core.info('Ltn.qml | getForecastByCityRange | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    function getWeatherRegions(callback,startAlpha,endAlpha){
        if (isAppActive==true){core.info('Ltn.qml| getForecastByCityRange | App is already Active, current Request is dropped.... ');return;}else{isAppActive=true;}
        if(!Store.cityByRegionCall){Store.regionsCall=true;}
        core.info('Ltn.qml | getWeatherRegions');
        __updateCallback(callback);
        var requestId = 7;
        var getParams = '';
        if(weatherRegionsModel.count && !Store.cityByRegionCall){
            core.info('Ltn.qml | getWeatherRegions | requestId : '+requestId+' | getForecastByCityRange.count : '+weatherRegionsModel.count)
            Store.regionsCall=false;isAppActive=false;
            __sigModelReady(weatherRegionsModel,true,"");
        }else{
            if(!dataController.__makeHeadendRequest()){
                getParams = '../../content/ltn/getAllWtr.json'
                core.info('Ltn.qml | getWeatherRegions | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
                __getLocalData(getParams,requestId)
            }else{
                if(startAlpha && endAlpha && startAlpha!='' && endAlpha!='')
                    getParams =  "function=getAllWtr&startAlpha='"+startAlpha+"'&endAlpha='"+startAlpha+"'";
                else
                    getParams = "function=getAllWtr";
                core.info('Ltn.qml | getWeatherRegions | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
                if(pif.getFallbackMode()){
                    __resetInFallbackMode();
                }else{
                    __getRemoteData(getParams,requestId);
                }
            }
        }
    }

    function getAllWeatherConditionByRegionId(region_id,callback){
        if (isAppActive==true){core.info('Ltn.qml| getAllWeatherConditionByRegionId | App is already Active, current Request is dropped.... ');return;}else{isAppActive=true;}

        core.info('Ltn.qml | getAllWeatherConditionByRegionId');
        __updateCallback(callback);
        Store.cityByRegionCall = true;

        if(region_id != '' && Store.wthrByRegionArr[region_id]){
            var model = getSubRegion(region_id);
            model.source =  Store.wthrByRegionArr[region_id]? Store.wthrByRegionArr[region_id]:[];
             Store.cityByRegionCall = false;isAppActive=false; __sigModelReady(model,true,"subRegionModel"+region_id);
        }
        else{
            Store.currRegionId = region_id;isAppActive=false;
            getWeatherRegions(callback);
        }

    }

    function getImage(imgName){
        if(core.pc){return core.settings.remotePath+"/ltn/www/services.php?function=getImage&imgName="+imgName;}
        else{return core.settings.remotePath+"/ltn/services.php?function=getImage&imgName="+imgName;}
    }

    function setIsSubCatsLastSelected(value){__isSubCategoryLastSelected = value;}

    //Private functions
    function __getRemoteData(getParams,requestId){
        if(core.pc){getData(core.settings.remotePath+"/ltn/www/services.php",3,getParams,'','',requestId);
        }else{getData(core.settings.remotePath+"/ltn/services.php",3,getParams,'','',requestId)}
    }

    function __getLocalData(getParams,requestId){getLocalData(getParams,requestId);}

    function __processModel(value,data,requestId){
        if(data!='' && value['success'] == 'true'){
            core.info("Ltn.qml | __processModel | Data ready = true | Stoping Timer")
            __readJsonObject(requestId,value)
        }else{
            __readJsonObject(0,value)
            core.info("Ltn.qml | __processModel | ERROR | value "+value+" | data :"+data)
        }
    }

    function __readJsonObject(requestId,responseObj) {
        core.info('Ltn.qml | __readJsonObject | requestId: '+requestId)
        if(requestId==0){
            core.info('Ltn.qml | __readJsonObject | Data reday = false');
            isAppActive=false;
            __sigModelReady('',false,"");
        }else if(requestId==1){
            __loadLTNCategory(responseObj,0);
            isAppActive=false;
            __sigModelReady(categoryModel,true,"");

        }else if(requestId==2){
            articleByCidModel.clear();
            for(var i=0;i<responseObj['data'].length;i++){
                articleByCidModel.append({
                    "headline": responseObj['data'][i].headline,"synopsis": responseObj['data'][i].synopsis, "artId": responseObj['data'][i].artId,"body":responseObj['data'][i].body,"imgs": responseObj['data'][i].imgs, "lastUpdTm": responseObj['data'][i].lastUpdTm
                });
            }

            core.info('Ltn.qml | __readJsonObject | articleByCidModel.count : '+articleByCidModel.count)
            isAppActive=false;
            __sigModelReady(articleByCidModel,true,"");

        }else if(requestId==3){
            forecastCityListModel.clear();
            for(var i=0;i<responseObj['data'].length;i++){
                forecastCityListModel.append({
                    "day": responseObj['data'][i].day,"highTemp": responseObj['data'][i].highTemp, "lowTemp": responseObj['data'][i].lowTemp,"currTemp":responseObj['data'][i].currTemp,"weatherReportTime": responseObj['data'][i].weatherReportTime, "wCondition": responseObj['data'][i].wCondition, "iCon": responseObj['data'][i].iCon
               });
            }
            isAppActive=false;
            __sigModelReady(forecastCityListModel,true,"");

        }else if(requestId==6){
            var tmpCityArr    = new Array();
            var tmpCityIndex  = new Array();
            var counter = 0;

            forecastCityByRangeModel.clear();
            for(var i=0;i<responseObj['data'].length;i++){
                for(var x=0;x<responseObj['data'][i]['wtr'].length;x++){
                    tmpCityArr[counter] = responseObj['data'][i]['wtr'][x]['cName'];
                    tmpCityIndex[responseObj['data'][i]['wtr'][x]['cName']] = [i,x];
                    counter++;
                }
            }

            tmpCityArr = tmpCityArr.sort();
            for(i in tmpCityArr){
                forecastCityByRangeModel.append({"cId":responseObj['data'][tmpCityIndex[tmpCityArr[i]][0]]['wtr'][tmpCityIndex[tmpCityArr[i]][1]]['cId'],
                                                "cName":responseObj['data'][tmpCityIndex[tmpCityArr[i]][0]]['wtr'][tmpCityIndex[tmpCityArr[i]][1]]['cName'],
                                                "wRptTime":responseObj['data'][tmpCityIndex[tmpCityArr[i]][0]]['wtr'][tmpCityIndex[tmpCityArr[i]][1]]['wRptTime'],
                    "curTemp":responseObj['data'][tmpCityIndex[tmpCityArr[i]][0]]['wtr'][tmpCityIndex[tmpCityArr[i]][1]]['curTemp'],
                    "wCnd":responseObj['data'][tmpCityIndex[tmpCityArr[i]][0]]['wtr'][tmpCityIndex[tmpCityArr[i]][1]]['wCnd'],
                    "iCon":responseObj['data'][tmpCityIndex[tmpCityArr[i]][0]]['wtr'][tmpCityIndex[tmpCityArr[i]][1]]['iCon']
                }
                )
            }
            core.info('Ltn.qml | __readJsonObject | requestId : '+requestId+' | getForecastByCityRange.count : '+forecastCityByRangeModel.count)
            isAppActive=false;
            __sigModelReady(forecastCityByRangeModel,true,"");
        }else if(requestId==7){
            weatherRegionsModel.clear();
            Store.wthrByRegionArr = [];
            for(var i=0;i<responseObj['data'].length;i++){
                weatherRegionsModel.append({"rgName":responseObj['data'][i]['rgName'],"rgId":responseObj['data'][i]['rgId']})
                for(var j=0;j<responseObj['data'][i]['ctyId'].length;j++){
                    if(typeof  Store.wthrByRegionArr[responseObj['data'][i]['rgId']]== 'undefined'){
                         Store.wthrByRegionArr[responseObj['data'][i]['rgId']]=[];
                    }
                     Store.wthrByRegionArr[responseObj['data'][i]['rgId']][j]={
                        'cId':responseObj['data'][i]['ctyId'][j]['cId'],
                        'cName':responseObj['data'][i]['ctyId'][j]['cName'],
                        'wRptTime':responseObj['data'][i]['ctyId'][j]['wRptTime'],

                        "curTemp":responseObj['data'][i]['ctyId'][j]['wtr'][0]['curTemp'],
                        "lowTemp":responseObj['data'][i]['ctyId'][j]['wtr'][1]['lowTemp'],
                        "highTemp":responseObj['data'][i]['ctyId'][j]['wtr'][1]['highTemp'],
                        "wtr":responseObj['data'][i]['ctyId'][j]['wtr'],
                        "iCon":responseObj['data'][i]['ctyId'][j]['wtr'][1]['iCon'],
                        'wCnd':responseObj['data'][i]['ctyId'][j]['wtr'][1]['wCnd']
                        //'iCon':responseObj['data'][i]['wtr'][j]['iCon']
                    }
                }
            }
            isAppActive=false;
            if(Store.regionsCall){
                core.info('Ltn.qml | __readJsonObject | requestId : '+requestId+' | getForecastByCityRange.count : '+weatherRegionsModel.count)
                Store.regionsCall=false;
                __sigModelReady(weatherRegionsModel,true,"");
            }
            else {
                if(Store.currRegionId != ''){
                    var model = getSubRegion(Store.currRegionId);
                    model.source =  Store.wthrByRegionArr[Store.currRegionId]? Store.wthrByRegionArr[Store.currRegionId]:[];
                    Store.cityByRegionCall=false;Store.currRegionId=''; __sigModelReady(model,true,"subRegionModel"+Store.currRegionId);
                }
            }
        }else if(requestId==8){
            __loadAllLanguages(responseObj,0);
            isAppActive=false;
            return true;
        }
    }

    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }

    function __loadLTNCategory(value,cid){

        core.info('Ltn.qml | __loadLTNCategory | Called | cid : '+cid);
        if(!__isSubCategoryLastSelected){subCategoryModel.clear();}
        subSubCategoryModel.clear();
        articles.clear();
        articleByCidModel.clear();

        try{
            if(value){
                categoryModel.clear()
                subCategoryModel.clear()
                if(value['data'][0]){
                    Store.storeCategory = value['data'][0]['lang'].prvd[0]['cats'];
                    Store.storeArticle  = value['data'][0]['lang'].prvd[0]['arts'];

                    if(Store.storeCategory.length<=0){
                        core.info('Ltn.qml | __loadLTNCategory | ERROR | categoryList.length : '+Store.storeCategory.length+' | No Root Categories Found Return requestId 0');
                        __sigModelReady('',false,"");
                        return
                    }
                }else {
                    core.info('Ltn.qml | __loadLTNCategory | ERROR | data tag empty | Return requestId 0');
                    __sigModelReady('',false,"");
                    return
                }
            }


            var categoryList = Store.storeCategory;

            if(Store.ltnHideEmptyCategories){
                for(var index=0; index<categoryList.length; index++){

                    if(categoryList[index].arts.length==0 && categoryList[index].subCats.length==0){
                        if(Store.emptyCategoryArr[categoryList[index].parentId] == undefined){Store.emptyCategoryArr[categoryList[index].parentId] = [];Store.emptyCategoryArr[categoryList[index].parentId][categoryList[index].catId] = 1;}
                        else{Store.emptyCategoryArr[categoryList[index].parentId][categoryList[index].catId] = 1;}
                        if(!Store.hideCategoryArr[categoryList[index].catId]){Store.hideCategoryArr[categoryList[index].catId]=1;}
                    }

                }

                for(var index=0; index<categoryList.length; index++){if(Store.emptyCategoryArr[categoryList[index].catId]){
                        var childOccurance = 0;for(var a in Store.emptyCategoryArr[categoryList[index].catId]){childOccurance++;}
                        if(childOccurance == categoryList[index].subCats.length){
                            if(Store.emptyCategoryArr[categoryList[index].parentId] == undefined){Store.emptyCategoryArr[categoryList[index].parentId] = [];Store.emptyCategoryArr[categoryList[index].parentId][categoryList[index].catId] = 1;}
                            else{Store.emptyCategoryArr[categoryList[index].parentId][categoryList[index].catId] = 1;}
                            if(!Store.hideCategoryArr[categoryList[index].catId]){Store.hideCategoryArr[categoryList[index].catId]=1;}
                        }
                    }
                }
            }

        }catch(e){
            core.info('Ltn.qml | __loadLTNCategory | ERROR | Return requestId 0 | error : '+e+' ');
            __sigModelReady('',false,"");
            return
        }

        if(cid==0){__isSubCategoryLastSelected = false;}

        for(var index = 0; index < categoryList.length; index++){
            if(categoryList[index].parentId==cid && !Store.ltnSkipSubCategory){
                if(cid==0){if(!Store.hideCategoryArr[categoryList[index].catId]){
                        categoryModel.append( { "catName": categoryList[index].catName, "cat_index": index , 'catId':categoryList[index].catId, 'childCount':categoryList[index].subCats.length, 'isSubCat':(categoryList[index].subCats.length > 0)?true:false, 'subCats':categoryList[index].subCats} )
                    }
                }else {if(!Store.hideCategoryArr[categoryList[index].catId]){
                        if(__isSubCategoryLastSelected){
                        subSubCategoryModel.append( { "catName": categoryList[index].catName, "cat_index": index , 'catId':categoryList[index].catId, 'childCount':categoryList[index].arts.length, 'isSubCat':(categoryList[index].subCats.length > 0)?true:false, 'subCats':categoryList[index].subCats})
                        }else{
                        subCategoryModel.append( { "catName": categoryList[index].catName, "cat_index": index , 'catId':categoryList[index].catId, 'childCount':categoryList[index].arts.length, 'isSubCat':(categoryList[index].subCats.length > 0)?true:false, 'subCats':categoryList[index].subCats})
                        }
                    }

                }
            }else if(Store.ltnSkipSubCategory){
                if(categoryList[index].subCats.length>0){
                    core.info("Ltn.qml | __loadLTNCategory | Skip Category Found | catId : "+categoryList[index].catId+" | catName "+ categoryList[index].catName)
                }else {if(!Store.hideCategoryArr[categoryList[index].catId]){
                        categoryModel.append( { "catName": categoryList[index].catName, "cat_index": index , 'catId':categoryList[index].catId, 'childCount':categoryList[index].subCats.length} )
                    }
                }
            }
        }

        core.info('Ltn.qml | __loadLTNCategory | Category model count : '+categoryModel.count+' | subCategoryModel model count : '+subCategoryModel.count);
    }


    function __loadAllLanguages(value,cid){
        try{
            if(value){
                Store.availableLangCode =[];
                for (var i in value['data'])
                    if(value['data'][i]){
                        Store.availableLangCode.push(value['data'][i]['lang'].lang);
                    }
                for(var key in Store.iso){
                    if(Store.availableLangCode.indexOf(Store.iso[key]) <0){
                        delete Store.iso[key]
                    }
                }
                __sigModelReady('',true,"languagesUpdated");
            }
            else {
                core.info('Ltn.qml | __loadAllLanguages | ERROR | data tag empty | Return requestId 0');
                __sigModelReady('',false,"languagesUpdated");
                return true
            }

        }catch(e){
            core.info('Ltn.qml | __loadAllLanguages | ERROR | Return requestId 0 | error : '+e+' ');
            return true
        }

    }

    function __loadLTNArticles(c_index){
        core.info('Ltn.qml | __loadLTNArticles | | Is called c_index :'+c_index);

        articles.clear()
        var articleList = Store.storeArticle;
        var selCat = Store.storeCategory[c_index];

        var articleNumber = 0
        for(var index = 0; index < selCat.arts.length; index++){
            articleNumber = selCat.arts[index]

            for(var jIndex = 0; jIndex < articleList.length; jIndex++){
                if(articleList[jIndex].artId == articleNumber){
                    var selArt = articleList[jIndex];
                    articles.append(
                            { "headline": selArt.headline, "jIndex": jIndex, "synopsis": selArt.synopsis, "artId": selArt.artId, "catId": selCat.catId, "imgName": selArt.imgName, "lastUpdTm": selArt.lastUpdTm }
                    );
                    break;
                }
            }
        }
        core.info('Ltn.qml | __loadLTNArticles | articles model count : '+articles.count);
    }

    function resetVariables(){
        isAppActive = false;
        categoryModel.clear();
        articles.clear();
        articleByCidModel.clear();
        subCategoryModel.clear();
        subSubCategoryModel.clear();
        forecastCityListModel.clear();
        forecastCityByRangeModel.clear();
        weatherRegionsModel.clear();
        Store.callback = '';
        Store.availableLangCode=[];
        Store.regionsCall = false;
        Store.cityByRegionCall = false;
        Store.wthrByRegionArr = [];
        Store.storeCategory=[];Store.storeArticle=[];
    }

    function __setConfiguration(configurationData){
        Store.ltnSkipSubCategory = configurationData['skipSubCategory']?(configurationData['skipSubCategory']=="true"?true:false):Store.ltnSkipSubCategory;
        Store.defaultIso = configurationData['defaultIso']?(configurationData['defaultIso']):Store.defaultIso;
        Store.ltnHideEmptyCategories = configurationData['hideEmptyCategories']?(configurationData['hideEmptyCategories']=="true"?true:false):Store.ltnHideEmptyCategories;
        if (configurationData["isoMapping"]){
            var isoMapping = configurationData["isoMapping"].split(',');
            for (var k in isoMapping){var tempArr =  isoMapping[k].split(":");Store.iso[tempArr[0]]=tempArr[1];}
        }
    }

    function __resetInFallbackMode(){
         core.info('Ltn.qml | __resetInFallbackMode | in fallback mode returning blank model');
        __sigModelReady('',false,"");
    }

    function getSubRegion(level){
        if(!level) return;
        if(!Store.subRegionModel){Store.subRegionModel={};}
        if(!Store.subRegionModel[level]){Store.subRegionModel[level]=sModelComp.createObject(ltn);}
        return Store.subRegionModel[level];
    }

    ListModel { id: articleByCidModel }
    ListModel { id: forecastCityListModel }
    ListModel { id: forecastCityByRangeModel; }
    SimpleModel { id : weatherRegionsModel; }
    ListModel { id: categoryModel}
    ListModel { id: subCategoryModel}
    ListModel { id: subSubCategoryModel}
    ListModel { id: articles}
    Component{id:sModelComp;JsonModel{}}
    Component.onCompleted: {
        core.info ("Ltn.qml | Component.onCompleted | LTN LOAD COMPLETE");
        Store.iso=[];Store.iso['eng']='en';Store.iso['ara']='ar';Store.callback = '';Store.storeCategory=[];Store.storeArticle=[];Store.emptyCategoryArr=[];Store.hideCategoryArr=[];Store.ltnHideEmptyCategories=false;Store.ltnSkipSubCategory=false;Store.regionsCall = false;Store.cityByRegionCall = false;Store.wthrByRegionArr = [];
        Store.availableLangCode=[];Store.defaultIso='eng';
    }
}
