import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

DataLoader {
    signal __sigModelReady(variant model,bool status);

    onSigDataReady: {
        if (errCode==0){
            core.info("GeniusPlaylist.qml | dataLoader | onSigDataReady");
            __processModel(responseObj,responseString,requestId);
        }else {
            __sigModelReady(geniusPlaylistModel,false);
        }
    }

    onSigLocalDataReady:{
        if(errCode==0){
            core.info("GeniusPlaylist.qml | dataLoader | onSigLocalDataReady ");
            __processModel(responseObj,'fromConfigModel',requestId)
        }else{
            __sigModelReady(geniusPlaylistModel,false);
        }
    }

    //public function
    function getRelatedMids(mid,callback) {

        if (isAppActive==true){core.info('GeniusPlaylist.qml| getRelatedMids | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}

        var requestId=1;geniusPlaylistModel.clear();__updateCallback(callback);
        core.info('GeniusPlaylist.qml | getRelatedMids | mid : '+mid);

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/genius/seat_getRelatedMids.json'
            core.info('GeniusPlaylist.qml | getRelatedMids | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId)
        }else{
            var getParams = "function=seat_getRelatedMids&mid="+mid+"&fields=trackTitle,trackArtist,albumMID,albumTitle&lid=1&output_format=JSON";
            core.info('GeniusPlaylist.qml | getRelatedMids | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }


    //private function
    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }

    function __processModel(value,data,requestId){
        if(data!='' && value['code'] == 0){
            core.info("GeniusPlaylist.qml | __processModel | Data ready = " + !value['code'])
            __readJsonObject(requestId,value)
        }else{
            core.info("GeniusPlaylist.qml | __processModel | ERROR | value "+value+" | data :"+data)
            __sigModelReady(geniusPlaylistModel,false);
        }
    }

    function __readJsonObject(requestId,value){
        core.info('GeniusPlaylist.qml | __readJsonObject | id :'+requestId)
        if(requestId==0){core.info('GeniusPlaylist.qml | __readJsonObject | Data ready = false')}
        if(requestId==1){__loadGeniusPlaylistModel(value);}
    }

    function __loadGeniusPlaylistModel(value){
        core.info('GeniusPlaylist.qml | __loadGeniusPlaylistModel | Called ');

        geniusPlaylistModel.clear();
        if(value['data']){
            for(var counter=0;counter<value['data']['related_MIDs'].length;counter++){
                geniusPlaylistModel.insert(counter,{"mid": value['data']['related_MIDs'][counter].trackMID,
                                               "title": value['data']['related_MIDs'][counter].trackTitle,
                                               "artist": value['data']['related_MIDs'][counter].trackArtist,
                                               "aggregate_parentmid": value['data']['related_MIDs'][counter].albumMID,
                                               "albumTitle": value['data']['related_MIDs'][counter].albumTitle,});

            }
        }else {
             core.info('GeniusPlaylist.qml | __loadGeniusPlaylistModel | ERROR | Data not found ');
             __sigModelReady(geniusPlaylistModel,false);
        }

        core.info('GeniusPlaylist.qml | __loadGeniusPlaylistModel | geniusPlaylistModel count : '+geniusPlaylistModel.count);
        isAppActive=false;__sigModelReady(geniusPlaylistModel,true);
    }




    function __getRemoteData(getParams,requestId){
        getData(core.settings.remotePath+'/in_flight_genius/api/genius_api.php?',3,getParams,'','',requestId);
    }

    function __getLocalData(getParams,requestId){getLocalData(getParams,requestId);}

    function resetVariables(){
        core.info('GeniusPlaylist.qml | resetVariables Called ');
        isAppActive = false;
        geniusPlaylistModel.clear();
    }

    function __resetInFallbackMode(){
         core.info('Ltn.qml | __resetInFallbackMode | in fallback mode returning blank model');
        __sigModelReady(geniusPlaylistModel,false);
    }
    SimpleModel {id: geniusPlaylistModel}

    Component.onCompleted: {
        core.info ("GeniusPlaylist.qml | Component.onCompleted | GeniusPlaylist LOAD COMPLETE");
    }
}
