// Menu API Data
var ActionCodeArr = [];
var MenuFieldsArr = [];
var MenuWhereClauseArr = [];
var MenuPosterFieldArr = [];

// Other API Data
var ApiActionCodeArr = [];
var ApiFieldsArr = [];
var ApiWhereClauseArr = [];

// Media Synopsis Data
var cApiMediaSyn=100;

// TrackList Data
var cApiAudioTrackList=110;
var cApiVideoTrackList=115;

// Interactive & All Languages
var IntLanguagesArr = [];
var cApiIntLanguages=120;
var IntLanguagesType=2;

var AllLanguagesArr = [];
var cApiAllLanguages=130;
var LanguageLabelsByLidType = 1;
// SoundTrack Data
var cApiSoundTracks=140;
var SoundtrackType=1;

// Subtitle Data
var cApiSubtitle=150;
var SubtitleType=1;
var WithEmbedSubtitle=false;

// Parent Menu Data
var cApiParentMenu=160;

// Media Player Data
var cApiVideoPlayer=170;
var cApiAudioPlayer=175;

var categoryAttributeForTemplate = "category_attr_cidtype";   //  field used to populate category template ID
var cApicategoryAttributeForTemplate=180;

// Sqlite Database references
var DatabasesArr = [];

// Cabin Class (Specific to Components)
var IntCabinClassArr = [];
var IntCabinClass=255;
var CabinClassArr=[];
var ShopppingConfiguration = [];
var HospitalityConfiguration = [];

// List of Dynamic Components
var CompList = [];

// List of WOI apps
var WoiAppList = [];
var WoiAppConfiguration = [];
var WoiAppCustomConfiguration=[];
var WoiAppResolutionMap = [];
var WoiAppAdvResolutionMap = [];
var WoiAppIsoMap = [];
var MenuSDKVersion = '';

//for converting to simple model
var CallbackArray=[];
var cCallbackIndex=0;

// PIF data duplicated here for performance reasons.
var MediaDate=0;
var SeatRating=254;

//for ltn
var LtnConfiguration = [];
//for Survey
var PreloadSurvey = false;
var TriggeredSurveyAppendInModel = false;

//PPV
var PpvRequired = false;

//getCategoryByCid
var cApiCategoryByCid=200;
//getMediaByMid
var cApiMediaByMid=201;
//getCategoryByTemplateId
var cApiCategoryByTemplateId=202;
//getCategoryMediaByMid
var  cApiCategoryMediaByMid=203;
//getMediaByLanguageIso
var  cApiMediaByLanguageIso=263;

//getChildCategoryByTemplateId
var cApiChildCategoryByTemplateId = 212;
//getChildMediaByTemplateId
var cApiChildMediaByTemplateId = 213;
//getPlaylistData
var cApiPlaylistData=214;
//getPlaylistDataWithParentInfo
var cApiPlaylistDataWithParentInfo=215;

//getParentMediaByMid
var cApiParentCategoryByMid = 220;
//getCityByIATA
var cApiCityByIATA = 230;
//getcApiCtToSeatMessages
var cApiCtToSeatMessages = 240;
//media.db refresh
var cApiMediaDbRefresh = 249;
//Route data
var cApiRouteConfiguration = 250;
var isRouteDataAvailable=false;
//Route FS data
var cApiRouteData = 251;
//getVariableConfiguration
var cApiVariableConfiguration = 252;
//getLookUpModel
var cApiLookUpModel = 253;
//getServiceModel
var cApiServiceModel = 254;
//getCDCount
var CDCountArr = [];
var cApiCDCountByCID = 255;
//getCategoryByICAO
var cApiCategoryByICAO = 256;
//keywordSearch.getData()
var cApiKeywordSearch = 257;
//getBundleLookupModel
var cApiBundleLookupModel = 258;
//getBundlesData
var cApiBundlesData = 259;
//getNonRouteChildMediaByTemplateId
var cApiNonRouteChildMediaByTemplateId = 260;
//getMediaByFieldName
var cApiMediaByFieldName = 261;
//getBundlesDataWithMid
var cApiBundlesDataWithMid = 262
//getBundleCidLookupModel
var cApiBundleCidLookupModel =267
//getBundleChildData
var cApiBundleChildData = 264
//getAllLabels
var cApiAllLabels = 266;
//getAllEmptyCategories
var cApiEmptyCategories = 269;
//getCategoryByFieldName
var cApiCategoryByFieldName = 270;
//getAssociatedMidsModel
var cApiAssociatedMidsModel=271;
var associatedMids=[];
//getNbdGroupsMidsModel
var cApiNbdGroupsMidsModel=272;
var nbdGroupsMids=[];
var nbdGroupsMidList=[];

//getMidGroupStatus
var cApiMidGroupStatus=273;

//getChildMediaByAggregateMid
var cApiChildMediaByAggregateMid = 265
//getAggregateMidByChildMid
var cApiAggregateMidByChildMid = 268
var cApiContentTerminalMaps=269

// get List of Available Ratings
var cApiRatingList = 270;

//getKeywordSearchPosters
var cApiKeywordSearchPosters = 280;
var cApiKeywordSearchCategoryPosters = 281;

//Shopping return callback id
var cEventShopping=500;
var cEventHospitality=501;
var CEventSurvey=502;

// Additional
var RouteId=0;
var MediaConfig=0;
var SoundConfig=0;
var ServiceConfig=0;
var SoundConfigClause="sound_config_id = 0";			// for test only set to 1
var MediaConfigClause="media_config_id = 0";
var ServiceConfigClause="service_config_id = 0";
var BlockCIDList="";
var BlockCIDClause="";
var BlockMIDList="";
var BlockMIDClause="";
var BlockByPpvClause="";
var BlockBySeatRating = false;
var BlockBySeatRatingExclusive = false;
var SeatRatingClause = "";
var AircraftType = "";
var AircraftSubType = "";
var CategoryLookUptemplateIdArray=[];
var CategoryPpvLookup = [];
var CategoryLabelLookup =[];
var ExceptionEmptyTemplates=[];
//PPV Refund check
var InteractiveLoadingStatus = false;
// Configuration
var PreloadMedia = false;
var PreloadMediaRootNode = "";
var PreloadLabel = false;
var PreloadCategoryPosters = false;
var MakeHeadendRequest=true;
var IsParentBlockRequired = false;
var IsOnlyTagNameLookup = false;
var AttrForPaxus = '';
var CustomPosterLabels = [];
var LanugageLocaleRequired = false;
var NbdGroupReqd = false;
var HideEmptyCategory = false;
var DefaultRouteId = 0;
function setMediaDate(d) {MediaDate=d; return true;}
function setRouteId(r) {RouteId=(pif.getFallbackMode() && r==0)?DefaultRouteId:r;return true;}
function setSeatRating(r) {if(r==undefined || r == 0) r=254; SeatRating=r; SeatRatingClause=BlockBySeatRatingExclusive?"(rating<"+SeatRating+" or rating>"+254+")":(BlockBySeatRating ? "(rating<="+SeatRating+" or rating>"+254+")" :SeatRatingClause);return true;}
function setMidBlockList(mids){if(mids==undefined) return; BlockMIDList=mids; BlockMIDClause=mids?"mid not in ("+mids+")":""; return true;}
function setCidBlockList(cids){if(cids==undefined) return; BlockCIDList=cids; BlockCIDClause=cids?"cid not in ("+cids+")":""; return true;}
function setPPVBlockClause(status,paid_or_free_mids){
    if(!status){BlockByPpvClause=""}
    else{BlockByPpvClause=paid_or_free_mids?" (categorymedia_accesstype!='pay' or mid in ("+paid_or_free_mids+")) ":" categorymedia_accesstype!='pay' "}
}
function __getSeatRatingAlias(alias){
    core.info("Media | __getSeatRatingAlias")
    var a = SeatRatingClause.replace(/rating/g, alias+'.rating ');
    return a;
}
function __getMenuSDKVersion(){
    return MenuSDKVersion[0];
}
function setRouteData(params){
    AircraftType=params.aircraftType?params.aircraftType:AircraftType;
    AircraftSubType=params.aircraftSubType?params.aircraftSubType:AircraftSubType;
    setSeatRating(params.seatrating);
}

function getLanguageLabelsByLid (lidList){
    core.info("Media | getLanguageLabelsByLid | lidList: "+lidList);
    var tmpArr=[]; lidList=lidList.split(",");
    for(var i=0;i<lidList.length;i++) if(AllLanguagesArr[lidList[i]]) {if(LanguageLabelsByLidType == 2){tmpArr.push(AllLanguagesArr[lidList[i]][lidList[i]]);}else{tmpArr.push(AllLanguagesArr[lidList[i]][core.settings.languageID]);}}
    return tmpArr;
}

function getLanguageLabelsByLidCustomized (lidList,lid){
    core.info("Media | getLanguageLabelsByIntLid | lidList: "+lidList);
    var tmpArr=[]; lidList=lidList.split(",");
    for(var i=0;i<lidList.length;i++){
        if(AllLanguagesArr[lidList[i]]) tmpArr.push(AllLanguagesArr[lidList[i]][lid])
    }
    return tmpArr;
}

function getLanguageIsoByLid (lidList){
    core.info("Media | getLanguageIsoByLid | lidList: "+lidList);
    var tmpArr=[];
    if(typeof lidList=='string' && lidList.indexOf(',')){
        lidList=lidList.split(",");
        for(var i=0;i<lidList.length;i++) if(AllLanguagesArr[lidList[i]]) tmpArr.push(AllLanguagesArr[lidList[i]]["iso"]);
    }
    else {
        if(AllLanguagesArr[lidList])tmpArr.push(AllLanguagesArr[lidList]["iso"]);
    }

    return tmpArr;
}

function getAllLanguages () {
    var sql = "SELECT label_lid as LID ,interactive_lid as nameLID,label_name as name,interactive_iso as ISO639 , language_iso FROM v_languages where "+SoundConfigClause+" order by language_order";
    core.info("Media | getAllLanguages | sql: "+sql);
    __modelUpdate (cApiAllLanguages,1,sql,"","Synchronous");
    var model = getMediaModel(cApiAllLanguages,1);
    var ptr = []; var lid;
    for(var i=0;i<model.count;i++){
        lid = parseInt(model.getValue(i,"LID"),10);
        if (!ptr[lid]) ptr[lid] = [];
        ptr[lid][parseInt(model.getValue(i,"nameLID"),10)] = model.getValue(i,"name");
		
        if (!ptr[lid]["iso"]) {ptr[lid]["iso"] = model.getValue(i,"language_iso");}
    }
    AllLanguagesArr=ptr;
    delete sql;delete model;delete ptr;delete lid;delete i;
    return true;
}

function getLidByLanguageISO(isocode){
    if(!isocode) return '';
    var model = getMediaModel(cApiAllLanguages,1); var sIso;
    for(var i=0;i<model.count;i++){ if(String(model.getValue(i,"language_iso")).toUpperCase() == isocode.toUpperCase()){core.info("Media | getLidByLanguageISO | isocode: "+isocode+" LID: "+parseInt(model.getValue(i,"LID"),10));return parseInt(model.getValue(i,"LID"),10)};}
    delete model;delete sIso;delete i;
    return '';
}

function getIntLanguages () {
    core.info("Media | getIntLanguages");
    var wc = [SoundConfigClause];var lang_locale = '';
    if (IntLanguagesArr[0]) wc.push("ISO639 in ('"+IntLanguagesArr.join("','")+"')");
    wc.push(IntLanguagesType==1 ? "interactive_lid="+core.settings.languageID:"interactive_lid=label_lid");
    wc.push("language_interactive = 1 order by language_order");
    if(LanugageLocaleRequired){lang_locale=',language_locale'}
    var sql = "SELECT label_lid as LID,interactive_lid as nameLID,label_name as name,interactive_iso as ISO639"+lang_locale+" FROM v_languages where "+wc.join(' and ');
    core.info("Media | getIntLanguages | sql: "+sql);
    __modelUpdate (cApiIntLanguages,1,sql,"","Synchronous");
    __setLanguageModel(cApiIntLanguages,IntLanguagesArr);
    delete wc;delete lang_locale;delete sql;
    return true;
}

function getCDCount(){
    core.info('Media | getCDCount');
    var sql = "SELECT count (mid) as cdCount , cid FROM v_categorymedia where media_type='audioAggregate' and lid="+core.settings.languageID+" and "+MediaConfigClause+"  "+__getMediaDateClause("categorymedia_startdate","categorymedia_enddate")+"  group by cid"
    core.info("Media | getCDCount | sql: "+sql);
    __modelUpdate (cApiCDCountByCID,1,sql,"","Synchronous");
    var model = getMediaModel(cApiCDCountByCID,1);
    var ptr = []; var cid; var cdCount;
    for(var i=0;i<model.count;i++){
        cid = parseInt(model.getValue(i,"cid"),10);
        if (!ptr[cid]) ptr[cid] = [];
        ptr[cid] = model.getValue(i,"cdCount");
    }
    CDCountArr=ptr;
    delete sql;delete model;delete ptr;delete cid;delete cdCount;delete i
    return true;
}

function getRouteConfiguration(){
    isRouteDataAvailable=false;
    var sql="SELECT media_config_id,sound_config_id,service_config_id from routes where route_id="+RouteId+" limit 1"
	
    __modelUpdate(cApiRouteConfiguration,1,sql,setRouteConfiguration);
    delete sql;return true;

}

function setRouteConfiguration(model){
    if(model.count){
        core.info("Media | setRouteConfiguration | for route id: "+RouteId+" | media_config_id: "+model.getValue(0,"media_config_id")+" | sound_config_id: "+model.getValue(0,"sound_config_id")+" | service_config_id: "+model.getValue(0,"service_config_id"));
	isRouteDataAvailable=true;
        //update where clauses
        MediaConfig=model.getValue(0,"media_config_id");     MediaConfigClause="media_config_id = "+MediaConfig;
        SoundConfig=model.getValue(0,"sound_config_id");     SoundConfigClause="sound_config_id = "+SoundConfig;
        ServiceConfig=model.getValue(0,"service_config_id"); ServiceConfigClause="service_config_id = "+ServiceConfig;

        interactiveStartup.eventHandler(Media.cApiRouteConfiguration)
        return true;
    }else {
        if(pif.getFallbackMode() && DefaultRouteId){core.info("Media | setRouteConfiguration | in fallback restting route id");setRouteId(RouteId);getRouteConfiguration()}
        else{isRouteDataAvailable=false;core.info("Media | setRouteConfiguration | for route id: "+RouteId+" | ERROR No data");}
    }
}

function getCategoryAttributeForTemplate(){
    var sql = "SELECT sql FROM sqlite_master where tbl_name = 'v_categories' and type = 'view' and sql like '%category_attr_template_id%'";
    core.info("media.js | getCategoryAttributeForTemplate | sql: "+sql);
    __modelUpdate (cApicategoryAttributeForTemplate,1,sql,getCategoryAttributeCallback);
    return true;
}

function getCategoryAttributeCallback(model){
    core.info("media.js | getCategoryAttributeCallback Media | model :   "+model);
    if(model.count>0){
        core.info("Media | getCategoryAttributeCallback | Attribute template_id found, Switching to template_id ");
        categoryAttributeForTemplate = "category_attr_template_id";__setCategoryAttributeForTemplate(categoryAttributeForTemplate);
    }else {core.info("Media | getCategoryAttributeCallback | Attribute template_id NOT found, using cidtype ");__setCategoryAttributeForTemplate(categoryAttributeForTemplate);}
    interactiveStartup.eventHandler(cApiMediaDbRefresh);
}

function getServiceModel(callback) {
    var sql = "SELECT service_mid, service_cid, service_code, service_access_type FROM services where "+ServiceConfigClause+"   "+__getMediaDateClause("service_start_date","service_end_date")+" ";
    core.info("Media | getServiceModel | sql: "+sql);
    __modelUpdate (cApiServiceModel,1,sql,callback,"Synchronous");
    delete sql;
    return true;
}

function getRatingList(callback) {
    var sql = "SELECT * from ratings where rating_interactive=1 and rating!=0";
    core.info("Media | getRatingList | sql: "+sql);
    __modelUpdate (cApiRatingList,1,sql,callback);
    delete sql;
    return true;
}

function getSoundTracks (params,callback,modelInstance) {
    core.info("Media | getSoundTracks | params : "+params+" modelInstance: "+modelInstance);
    var  orderByClause  = [];
    var wc = [SoundConfigClause];
    if(params[2] != undefined){
        var customList = String(params[2]);
        orderByClause.push("order by case a.label_lid");
        var sortOrderArr = customList.split(',');for(var x in sortOrderArr){ orderByClause.push("when '"+sortOrderArr[x]+"' then "+x);}
        orderByClause.push('else '+(sortOrderArr.length + 1)+' End, l.language_order');
    }else {orderByClause.push('ORDER BY a.soundtrack_order');}
    wc.push(SoundtrackType==1 ? "a.soundtrack_lid=a.label_lid and a.interactive_lid="+core.settings.languageID:"a.label_lid=a.interactive_lid");
    wc.push("a.soundtrack_mid="+params[0]+" and a.soundtrack_lid=l.language_lid "+orderByClause.join(' '));
    var sql = "SELECT a.* FROM v_soundtracks as a, languages as l where "+wc.join(' and ');
    core.info("Media | getSoundTracks | sql: "+sql);

   __modelUpdate (cApiSoundTracks,modelInstance,sql,callback);
    delete orderByClause;delete wc;delete customList;delete sortOrderArr;delete x;delete sql;
    return true;
}

function getSubtitles (params,callback,modelInstance) {
    core.info("Media | getSubtitles | params : "+params+" modelInstance: "+modelInstance);
    var  orderByClause  = [];
    var wc = [SoundConfigClause];
    if(params[2] != undefined){
        var customList = String(params[2]);
        orderByClause.push("order by case a.label_lid");
        var sortOrderArr = customList.split(',');for(var x in sortOrderArr) {orderByClause.push("when '"+sortOrderArr[x]+"' then "+x);}
        orderByClause.push('else '+(sortOrderArr.length + 1)+' End, l.language_order');
    }else {orderByClause.push('ORDER BY a.subtitle_order');}

    if (!WithEmbedSubtitle) wc.push("a.subtitle_type!='embedded'");
    wc.push(SubtitleType==1 ? "a.subtitle_lid=a.label_lid and a.interactive_lid="+core.settings.languageID: "a.label_lid=a.interactive_lid");
    wc.push("a.subtitle_lid=l.language_lid and a.subtitle_type!='NA' and a.subtitle_mid="+params[0]+" "+orderByClause.join(' '));
    var sql = "SELECT a.* FROM v_subtitles as a,languages as l where "+wc.join(' and ');
    core.info("Media | getSubtitles | sql: "+sql);

    __modelUpdate (cApiSubtitle,modelInstance,sql,callback);
    delete orderByClause;delete wc;delete customList;delete sortOrderArr;delete x;delete sql;
    return true;
}

function getVariableConfiguration(params,callback, modelInstance){
    core.info("Media | getVariableConfiguration | params : "+params+" modelInstance: "+modelInstance);
    var sql = "select variable_name,variable_value from variables where "+MediaConfigClause+"  "+__getMediaDateClause("variable_startdate","variable_enddate")+" "+(params[0]!="all"?" and variable_name='"+params[0]+"'":'');
    core.info("Media | getVariableConfiguration | sql: "+sql);

    __modelUpdate(cApiVariableConfiguration,modelInstance,sql,callback);
    delete sql;
    return true;
}

function getLookUpModel(callback){
    var wc = [MediaConfigClause+"  "+__getMediaDateClause("category_startdate","category_enddate")+" and v_ct."+categoryAttributeForTemplate+"!='' and ptid!=''"];
    wc.push("v_ct.lid=(select distinct language_lid from v_languages where lower(language_iso)='eng')");
    var attrForPaxus = AttrForPaxus!=''?(',v_ct.'+AttrForPaxus+' as paxus'):'';
    var posterfields = PreloadCategoryPosters; var ssList = ''
    if(PreloadCategoryPosters){
        var ss=MenuPosterFieldArr[100]; // 100 ssid is fixed for getLookUpModel query
           if(CustomPosterLabels.length){
               var p_index = core.settings.resolutionIndex;
               for(var i in CustomPosterLabels){
                   if (ss[p_index]) ssList+=', v_ct.'+ss[p_index]+' as '+CustomPosterLabels[i];
                   p_index = p_index + core.settings.totalResolutions;
               }
           }else{
               var pindx=core.settings.resolutionIndex; var spindx=pindx+core.settings.totalResolutions;
               if (ss[pindx]) ssList=', v_ct.'+ss[pindx]+' as poster'; if (ss[spindx]) ssList+=', v_ct.'+ss[spindx]+' as synopsisposter';
           }
    }
    if(IsOnlyTagNameLookup && CategoryLookUptemplateIdArray[0] != undefined){wc.push("template_id in ('"+CategoryLookUptemplateIdArray.join("','")+"')");}
    var sql = "SELECT v_ct.cid as cid,v_ct.title as title, v_ct.parentcid as parentcid, v_ct."+categoryAttributeForTemplate+" as template_id, (SELECT v_ct1."+categoryAttributeForTemplate+"  from v_categories  as v_ct1 where v_ct1.cid=v_ct.parentcid) as ptid "+attrForPaxus+" "+ssList+" FROM v_categories as v_ct where "+wc.join(' and ');
    core.info("Media | getLookUpModel | sql: "+sql);

    __modelUpdate(cApiLookUpModel,1,sql,callback,"Synchronous");
    delete wc;delete attrForPaxus;delete posterfields;delete ss;delete p_index;delete sql;delete i;delete pindx;
    return true;
}

function getBundleLookupModel(callback){
    var sql = "SELECT b.bundle_parentmid as bundle_parentmid, cm.categorymedia_accesstype categorymedia_accesstype, b.mid as mid from v_media_bundles as b, v_categorymedia as cm where cm."+MediaConfigClause+" and cm.mid= b.bundle_parentmid group by b.mid, b.bundle_parentmid";
    core.info("Media | getBundleLookupModel | sql: "+sql);

    __modelUpdate(cApiBundleLookupModel,1,sql,callback,"Synchronous");
    delete sql;return true;
}

function getBundleCidLookupModel(bundleLookup){
    core.log(" Media | getBundleCidLookupModel | Starting bundleLookup Process")
    for(var bmid in bundleLookup){
        var midlist=core.dataController.mediaServices.getChildBundleMids(bmid);var childList = "";var tempArr = [];var temp=[];
        var sql = "SELECT parentcid,cid from v_categories where "+MediaConfigClause+" and cid in(select cid from categorymedia where mid in ("+midlist+")  "+__getMediaDateClause("categorymedia_startdate","categorymedia_enddate")+" ) group by cid";
        __modelUpdate(cApiBundleCidLookupModel,1,sql,'',"Synchronous");
        var model = getMediaModel(cApiBundleCidLookupModel,1);
        if(CategoryPpvLookup[bmid] == undefined){CategoryPpvLookup[bmid] = [];}
        for(var i=0; i<model.count; i++){if(tempArr[model.getValue(i,'parentcid')] == undefined){tempArr[model.getValue(i,'parentcid')] =1;temp.push(model.getValue(i,'parentcid'));}}

        for(var pcid in tempArr){
            for( var j=0; j<model.count; j++){
                if(pcid == model.getValue(j,'parentcid')){
                    if(!(childList.length != 0 )){childList += model.getValue(j,'cid')+""}
                    else{childList += ","+model.getValue(j,'cid')+""}
                }
            }
            if(CategoryPpvLookup[bmid][pcid] == undefined)CategoryPpvLookup[bmid][pcid]=childList;childList="";
        }

        core.log(" Media | getBundleCidLookupModel | Starting parent of parent lookup Process")
        var sql1 = "SELECT parentcid,cid from v_categories where "+MediaConfigClause+" and parentcid in (SELECT parentcid from v_categories where cid in('"+temp.join("','")+"') )group by cid";
        __modelUpdate(cApiBundleCidLookupModel,1,sql1,'',"Synchronous");
        var model1 = getMediaModel(cApiBundleCidLookupModel,1);
        for(var i=0; i<model1.count; i++){if(tempArr[model1.getValue(i,'parentcid')] == undefined){tempArr[model.getValue(i,'parentcid')] =1;}}
        for(var pcid1 in tempArr){
            for( var k=0; k<model1.count; k++){
                if(pcid1 == model1.getValue(k,'parentcid')){
                    if(CategoryPpvLookup[bmid][model1.getValue(k,'cid')] != undefined){
                    if(!(childList.length != 0 )){childList += model1.getValue(k,'cid')+""}
                    else{childList += ","+model1.getValue(k,'cid')+""}
                    }
                }
            }

            if(CategoryPpvLookup[bmid][pcid1] == undefined)CategoryPpvLookup[bmid][pcid1]=childList;childList="";
        }


        //core.info("Media | getBundleCidLookupModel | sql: "+sql);
    }
    delete bmid;delete midlist;delete childList;delete tempArr;delete temp;delete model;delete i;delete pcid;delete j;delete sql1;delete i;delete pcid1;delete k;
    core.log(" Media | getBundleCidLookupModel | Process complete ");

}

function getAllLabelsModel(tids,langISO){
    core.info("Media | getAllLabelsModel | PreloadLabels : on");
    if(langISO == undefined)langISO=IntLanguagesArr;
    else var langISO = langISO.split(',');
    var wc =[];
    if(tids && tids.length>0){
        var temp = tids.split(',');
        wc.push('v_cat.'+categoryAttributeForTemplate+" in ('"+temp.join("','")+"')")
    }
    wc.push("v_cat."+MediaConfigClause);
    if (BlockCIDClause) wc.push(" v_cat."+BlockCIDClause);
    wc.push("l.language_iso in('"+langISO.join("','")+"')  "+__getMediaDateClause("category_startdate","category_enddate")+" and v_cat.lid = l.language_lid");
    var sql = "Select DISTINCT v_cat.cid as cid,v_cat.lid as lid,v_cat.title as title,v_cat.description as description, v_cat.short_description as short_description, l.language_iso as iso from v_categories as v_cat, languages as l where "+wc.join(' and ');
    core.info("Media | getAllLabelsModel | sql: "+sql);
    __modelUpdate(cApiAllLabels,1,sql,'',"Synchronous");
    var labelmodel = getMediaModel(cApiAllLabels,1);
    core.info("Media | getAllLabelsModel | labelmodel.count: "+labelmodel.count);
    for(var i=0;i<labelmodel.count;i++){
        var iso = (labelmodel.getValue(i,'iso')).toLowerCase(); var cid = labelmodel.getValue(i,'cid');
        if(CategoryLabelLookup[iso] == undefined){CategoryLabelLookup[iso]=[];}
        if(CategoryLabelLookup[iso][cid]==undefined){
            CategoryLabelLookup[iso][cid]=[];
            CategoryLabelLookup[iso][cid]={'title':labelmodel.getValue(i,'title'),'description':labelmodel.getValue(i,'description'),'short_description':labelmodel.getValue(i,'short_description')}
            //core.info("Media | getAllLabelsModel | CategoryLabelLookup["+iso+"]["+cid+"][title]"+CategoryLabelLookup[iso][cid]["title"]);
        }
    }
    delete wc;delete sql;delete labelmodel;delete i;delete iso;delete cid
    destroyModel(cApiAllLabels,1)
}

function getAllEmptyCategories(){
    core.info("Media | getAllEmptyCategories");
    var wc =[];
    var or = '';
    var cidsToBlock=[]; var getCid='';
    wc.push("a."+MediaConfigClause);
    if (BlockCIDClause) wc.push(" a."+BlockCIDClause);
    if(ExceptionEmptyTemplates.length > 0){
        or += "or a."+categoryAttributeForTemplate+" in('"+ExceptionEmptyTemplates.join("','")+"')"
    }
    wc.push("a.lid = "+core.settings.languageID+""+__getMediaDateClause("a.category_startdate","a.category_enddate")+" " );
    var sql = "SELECT a.cid as cid from v_categories a where (a.cid not  in (select b.parentcid from  v_categories b where"+__getMediaDateClause("b.category_startdate","b.category_enddate",true)+" and b."+MediaConfigClause+" and a.cid =b.parentcid  "+or+" ) and a.cid not in (select c.cid from categorymedia c where "+__getMediaDateClause("c.categorymedia_startdate","c.categorymedia_enddate",true)+" and  c."+MediaConfigClause+" and a.cid= c.cid  "+or+") ) "+(wc.length?'and '+wc.join(' and '):'')+ " group by a.cid order by a.cid";
    core.info("Media | getAllEmptyCategories | sql: "+sql);
    __modelUpdate(cApiEmptyCategories,1,sql,'',"Synchronous");
    var hiddenCategoryModel = getMediaModel(cApiEmptyCategories,1);
    core.info("Media | getAllEmptyCategories | labelmodel.count: "+hiddenCategoryModel.count);
    for(var i=0;i<hiddenCategoryModel.count;i++){
        if(cidsToBlock[hiddenCategoryModel.getValue(i,'cid')]==undefined){
            cidsToBlock.push(hiddenCategoryModel.getValue(i,'cid'));
            getCid=mediaServices.setCidAccessType('blocked',hiddenCategoryModel.getValue(i,'cid'),2);
        }
    }
    mediaServices.updateCidBlocked();
    delete wc;delete or;delete cidsToBlock;delete getCid;delete sql;delete hiddenCategoryModel;delete i;
    return true;
}

function getCtToSeatMessage(params,callback){
     core.info("Media | getCtToSeatMessages | params : "+params);
    var aPtr=ActionCodeArr[cApiCtToSeatMessages];var fields="seat_message_id,seat_message_duration,seat_message_title,seat_message_mid,seat_message_lid,seat_message_text";var fdid = 0;
    if (aPtr){fdid=aPtr.fdId;fields=MenuFieldsArr[aPtr.fdId]}
    var sql = "select "+fields+" from seat_messages  where seat_message_lid="+core.settings.languageID+(params[0]!="all"?" and seat_message_id="+params[0]:'');
    core.info("Media | getCtToSeatMessages | sql: "+sql);

    __modelUpdate(cApiCtToSeatMessages,1,sql,callback);
    delete aPtr;delete fields;delete sql;
}

function getCtToSeatMessageWithLid(params,callback){
    if(params[2]==undefined) core.error("Media | getCtToSeatMessageWithLid, lid not provided. Exiting...");
    core.info("Media | getCtToSeatMessageWithLid | params : "+params);
    var aPtr=ActionCodeArr[cApiCtToSeatMessages];var fields="seat_message_id,seat_message_duration,seat_message_title,seat_message_mid,seat_message_lid,seat_message_text";var fdid = 0;
    if (aPtr){fdid=aPtr.fdId;fields=MenuFieldsArr[aPtr.fdId]}
    var sql = "select "+fields+" from seat_messages  where seat_message_lid="+params[2]+(params[0]!="all"?" and seat_message_id="+params[0]:'');
    core.info("Media | getCtToSeatMessageWithLid | sql: "+sql);

    __modelUpdate(cApiCtToSeatMessages,1,sql,callback);
    delete aPtr;delete fields;delete sql;
}

function getSubtitlesWithLabel(params,callback,labelString){

    return Media.getSubtitles(params,function (dModel) {
         console.log("Media | getSubtitlesWithLabel | "+((typeof dModel.getValue(0,"subtitle_type_index")!='undefined')?dModel.getValue(0,"subtitle_type_index"):-1) )
                                  subtitlelModel.clear();
        for (var i=0;i<dModel.count;i++) subtitlelModel.append({"label_name":dModel.getValue(i,"label_name"),"subtitle_lid":dModel.getValue(i,"subtitle_lid"),"subtitle_type":dModel.getValue(i,"subtitle_type"),"subtitle_type_index":(typeof dModel.getValue(i,"subtitle_type_index")!='undefined')?dModel.getValue(i,"subtitle_type_index"):-1});
//                                subtitlelModel.insert(0,{"label_name":labelString,"subtitle_lid":"-1","subtitle_type":dModel.getValue(0,"subtitle_type")});
                                  if(dModel.getValue(0,"subtitle_type") != 'embedded'){subtitlelModel.insert(0,{"label_name":labelString,"subtitle_lid":"-1","subtitle_type":-1,"subtitle_type_index":-1});}
                                  callback(subtitlelModel);
                              },1);
}

function getAssociatedMidsModel(){
   core.debug("Media | getAssociatedMidsModel ");
   //var sql ="select a.mid, m1.uniqueid as parent_uniqueid, m.uniqueid, m2.mid as associate_mid from associates a, categorymedia cm, media m1, media m, media m2, categorymedia cm2 where a.mid = cm.mid and a.mid = m1.mid and a.associate_mid = m.mid and m.uniqueid = m2.uniqueid and m2.mid = cm2.mid and cm."+MediaConfigClause+" and cm2."+MediaConfigClause+" and  "+MediaDate+" between cm.categorymedia_startdate and cm.categorymedia_enddate and  "+MediaDate+" between cm2.categorymedia_startdate and cm2.categorymedia_enddate group by a.mid, m.uniqueid order by a.mid, a.weight desc";
    var sql ="SELECT a.mid, m1.uniqueid as parent_uniqueid, m2.uniqueid, m3.mid as associate_mid FROM associates a, media m1, media m2, media m3, categorymedia cm WHERE a.mid = m1.mid and a.associate_mid = m2.mid and m1.uniqueid IN (SELECT m3.uniqueid FROM media m3, categorymedia cm1 WHERE m3.mid = cm1.mid and cm1."+MediaConfigClause+"  "+__getMediaDateClause("cm1.categorymedia_startdate","cm1.categorymedia_enddate")+" ) and m2.uniqueid = m3.uniqueid and m3.mid = cm.mid and cm."+MediaConfigClause+"  "+__getMediaDateClause("cm.categorymedia_startdate","cm.categorymedia_enddate")+" GROUP BY m1.uniqueid, m2.uniqueid ORDER BY a.mid, a.weight DESC";
    core.info("Media | getAssociatedMidsModel | sql: "+sql);
    __modelUpdate(cApiAssociatedMidsModel,1,sql,setAssociatedMids,"Synchronous");
   delete sql;
}

function setAssociatedMids(dModel){
    core.info("Media | setAssociatedMids | count: "+dModel.count);
    for(var i=0;i<dModel.count;i++){
        if(!associatedMids[dModel.getValue(i,"parent_uniqueid")]){
            associatedMids[dModel.getValue(i,"parent_uniqueid")]=[];associatedMids[dModel.getValue(i,"parent_uniqueid")].push(dModel.getValue(i,"associate_mid"))
        }else{associatedMids[dModel.getValue(i,"parent_uniqueid")].push(dModel.getValue(i,"associate_mid"))}
    }

    /*debug : for(var key in associatedMids){
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>. associatedMids["+key+"] = " +associatedMids[key]);
    }*/
}

function getNbdGroupsMidsModel(){
   core.debug("Media | getNbdGroupsMidsModel ");
    var sql = "SELECT distinct a.mid, nbdgroup FROM categorymedia cm, nbdgroups a WHERE cm."+MediaConfigClause+" "+__getMediaDateClause("cm.categorymedia_startdate","cm.categorymedia_enddate")+"  and cm.mid = a.mid ORDER BY a.mid";
   core.info("Media | getNbdGroupsMidsModel | sql: "+sql);
   __modelUpdate(cApiNbdGroupsMidsModel,1,sql,setNbdGroupsMids);
   delete sql;
}
function setNbdGroupsMids(dModel){
    core.info("Media | setNbdGroupsMids | count: "+dModel.count);
    for(var i=0;i<dModel.count;i++){
        nbdGroupsMids[dModel.getValue(i,"mid")]=dModel.getValue(i,"nbdgroup");
        nbdGroupsMidList.push(dModel.getValue(i,"mid"));
    }
}
function getMidGroupStatus(mid){
    core.debug("Media | getMidGroupStatus ");
    var activeGroupsClause = pif.getActiveNbdGroups().length? "and nbdGroup in ('"+pif.getActiveNbdGroups().join("','")+"')":"";
    var sql = "SELECT vm.mid , uniqueid from v_media vm, nbdgroups n where uniqueid in (select uniqueid from media where mid="+mid+") "+activeGroupsClause+" and n.mid = vm.mid group by vm.mid";
    core.info("Media | getMidGroupStatus | sql: "+sql);
    __modelUpdate(cApiMidGroupStatus,1,sql,'',"Synchronous");
    var model = getMediaModel(cApiMidGroupStatus,1);
    if(model.count>0){return true}
    else return false;
}
function getSimpleModelDataByApi(apiId, params, callback, modelInstance) {
    if(!CallbackArray[apiId]){CallbackArray[apiId]=[];}
    CallbackArray[apiId][modelInstance]=callback;
    return getDataByApi(apiId, params, __setSimpleModel, modelInstance);
}

function __setSimpleModel(dModel){
    core.info("Media | __setSimpleModel | apiId: "+dModel.apiType+" | modelInstance: "+dModel.instanceId);
    dataController.__setSimpleModel(dModel,dModel.apiType,dModel.instanceId,CallbackArray[dModel.apiType][dModel.instanceId]);
    CallbackArray[dModel.apiType][dModel.instanceId]="";
    return true;
}


function getDataByApi(apiId, params, callback, modelInstance) {	// [modelInstance] //params[<CID/MID>,<ActionCode>,<Album Rating>]
    core.info("Media | getDataByApi | apiId: "+apiId+" params : "+params+" modelInstance: "+modelInstance);
    var aPtr=ActionCodeArr[params[1]]; var  dbid = aPtr.dbId;
    if (!aPtr) {core.error("Media | getDataByApi | Params has invalid Backend ActionCode. Exiting Function..."); return false;}
    if(apiId==cApiKeywordSearchPosters){return formAPIQuery(aPtr,params);}
    if(apiId==cApiKeywordSearchCategoryPosters){return formAPIQuery(aPtr,params);}
    var sql = formAPIQuery(aPtr,params);
    if (apiId == cApiCategoryByCid && params[5]){var aPtr1=ActionCodeArr[params[5]];dbid = aPtr1.dbId;}
    var tmp= (sql)?__modelUpdate(apiId,modelInstance,sql,callback,'',dbid):false;
    delete aPtr;delete dbid;delete sql;delete aPtr1;delete tmp;delete aPtr1;
}

function getSoundTrackType(){
    core.info("Media | getSoundTrackType");
    return SoundtrackType;
}

function getSubtitleType(){
    core.info("Media | getSubtitleType");
    return SubtitleType;
}

function setSoundTrackType(type){
    core.info("Media | setSoundTrackType | type :"+type);
    if(type==undefined){core.error("Media.js | setSubtitleType | Type not provided, Exiting");return false;}
    if(type==1)SoundtrackType=1;
    else SoundtrackType=2;
}

function setSubtitleType(type){
    core.info("Media | setSubtileType | type :"+type);
    if(type==undefined){core.error("Media.js | setSubtitleType | Type not provided, Exiting");return false;}
    if(type==1)SubtitleType=1;
    else SubtitleType=2;
}

function formAPIQuery(aPtr,params) {    // ActionCodeArrayReference, params[cid or mid]

    var fields=MenuFieldsArr[aPtr.fdId]; var ssList="";
    if (MenuPosterFieldArr[aPtr.ssId]) {
	 var ss=MenuPosterFieldArr[aPtr.ssId];
        if(CustomPosterLabels.length){
            var p_index = core.settings.resolutionIndex;
            for(var i in CustomPosterLabels){
                if (ss[p_index]) ssList+=', '+ss[p_index]+' as '+CustomPosterLabels[i];
                p_index = p_index + core.settings.totalResolutions;
            }
        }else{
            var pindx=core.settings.resolutionIndex; var spindx=pindx+core.settings.totalResolutions;
            if (ss[pindx]) ssList=', '+ss[pindx]+' as poster'; if (ss[spindx]) ssList+=', '+ss[spindx]+' as synopsisposter';
        }       
//        var pindx=core.settings.resolutionIndex; var spindx=pindx+core.settings.totalResolutions;
//        if (ss[pindx]) ssList=', '+ss[pindx]+' as poster'; if (ss[spindx]) ssList+=', '+ss[spindx]+' as synopsisposter';
    }

    var sqlId=aPtr.psId; var sql="";
    if (MenuWhereClauseArr[aPtr.wrId]) var wc = [MenuWhereClauseArr[aPtr.wrId]]; else var wc = [];

    if (sqlId==cApiMediaSyn) {
        wc.push("lid="+core.settings.languageID+" and mid="+params[0]+" LIMIT 1");
        sql="SELECT "+fields+ssList+" FROM v_media as v_m where "+wc.join(' and ');

    } else if (sqlId==cApiAudioTrackList) {
        wc.push("lid="+core.settings.languageID+" and aggregate_parentmid="+params[0]+" order by aggregate_order");
        sql="select "+fields+ssList+", "+params[2]+" as amidRating from  v_media_aggs as v_ma where "+wc.join(' and ');

    } else if (sqlId==cApiVideoPlayer || sqlId==cApiAudioPlayer) {
        wc.push("lid="+core.settings.languageID+" and mid="+params[0]);
        sql="SELECT "+fields+ssList+" FROM v_media where "+wc.join(' and ');

    }else if (sqlId==cApiCategoryByCid) {
        if(params[5]){
            var aPtr1=ActionCodeArr[params[5]];fields=MenuFieldsArr[aPtr1.fdId];ssList="";
            wc.push("id in ("+params[0]+") and lid="+core.settings.languageID+" order by seq_no");
            sql="SELECT "+fields+ssList+" FROM contentTable where "+wc.join(' and ');
        }
        else{
            wc.push(MediaConfigClause);
            if (BlockCIDClause) wc.push(BlockCIDClause);
            wc.push("lid="+core.settings.languageID+" and cid in ("+params[0]+") "+__getMediaDateClause("category_startdate","category_enddate")+" ");
            sql="SELECT "+fields+ssList+" FROM v_categories where "+wc.join(' and ')+(params[2]?" group by "+params[2]:'')+getOrderClause('category_order','cid',params[0],params[3],params[4]);
        }

    }else if (sqlId==cApiMediaByMid || sqlId==cApiPlaylistData) {
        if (BlockMIDClause && !params[6]) wc.push(BlockMIDClause);
        var nbd_join = ""; var joining_field = "";
        if (SeatRatingClause && !params[6]) wc.push(SeatRatingClause);
        wc.push("lid="+core.settings.languageID+" and mid in ("+params[0]+")");
        if(!params[7]){
            if(NbdGroupReqd && pif.getFallbackMode() && !params[8]) {joining_field="v_media"; nbd_join =__getNbdgroupClause("v_media")}
        	sql="SELECT "+fields+ssList+" FROM v_media "+nbd_join+" where "+wc.join(' and ')+(params[2]?" group by "+params[2]:'')+getOrderClause('mid','mid',params[0],params[3],params[4]);
        }else{
            if(NbdGroupReqd && pif.getFallbackMode()&& !params[8]) {joining_field="v_media";nbd_join =__getNbdgroupClause("v_media_aggs")}
            //when aggregate_parentmid is required and the playlist would have only aggs
            var agg_fd = params[7]!=''?','+params[7]:""
            sql="SELECT "+fields+agg_fd+ssList+" FROM v_media_aggs where "+wc.join(' and ')+(params[2]?" group by "+params[2]:'')+getOrderClause('mid','mid',params[0],params[3],params[4]);
        }
        if(NbdGroupReqd && pif.getFallbackMode() && !params[8] ) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if (sqlId==cApiPlaylistDataWithParentInfo) {
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd &&  pif.getFallbackMode() && !params[7]) {joining_field="a"; nbd_join =__getNbdgroupClause("a")}
        if (BlockMIDClause && !params[6]) wc.push('m.'+BlockMIDClause);
        if (SeatRatingClause && !params[6]){if(fields.indexOf('m.rating')<0){fields+=',m.rating '}; wc.push(__getSeatRatingAlias('m'))}
        var tmp = params[0] //pif.vodPlaylist.getAggregateMidList(-1);
        var tmpConcat=[]; var tmpmids =[];var midarr=[];var tmpaggmids =[]
        for(var x in tmp['midArr']){
            for(var y in tmp['midArr'][x]){
                tmpConcat.push(tmp['midArr'][x][y]+"_"+x);
                if(tmpmids.indexOf("'"+tmp['midArr'][x][y]+"'")<0){tmpmids.push("'"+tmp['midArr'][x][y]+"'");midarr.push(tmp['midArr'][x][y])}
                if(tmpaggmids.indexOf("'"+x+"'")<0){tmpaggmids.push("'"+x+"'");midarr.push(x)}
            }
        }
        var subqueryFields=[]
        var temp = fields.split(',')
        for(var k in temp){
            var temp1 =temp[k].split('.')
            if(temp1[1]!=undefined){
                var temp2 = temp1[1].split(' as ')
                if(temp2[0]=='cast'){subqueryFields.push(temp1[1])}
                else{if(subqueryFields.indexOf(temp2[0])<0)subqueryFields.push(temp2[0])}
            }
        }
        var ssidfields =ssList.split(',')
        for(var k in ssidfields){
            var temp3 =ssidfields[k].split('.')
            if(temp3[1]!=undefined){var temp4 = temp3[1].split(' as ');subqueryFields.push(temp4[0])}
        }
        wc.push("(m.mid||'_'||a.mid) in ('"+tmpConcat.join("','")+"')");
        sql = "select distinct "+fields+ssList+" from (select "+subqueryFields.join(',')+" from  v_media where mid in ("+tmpmids.join(",")+") and lid="+core.settings.languageID+") as m left join";
        sql += " (select "+subqueryFields.join(',')+" from  v_media where mid in ("+tmpaggmids.join(",")+") and lid="+core.settings.languageID+") as a "+nbd_join+" where  "+wc.join(' and ')+" "+getOrderClause('m.mid','m.mid',tmp.sortedMidArr,params[3],params[4]);
    }else if(sqlId ==cApiMediaByLanguageIso){
        if (BlockMIDClause) wc.push(BlockMIDClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode()&& !params[3]) {joining_field="v_media"; nbd_join =__getNbdgroupClause("v_media")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        wc.push(params[2]?"lid=(select distinct language_lid from v_languages where lower(language_iso)='"+params[2]+"')":"lid=(select distinct language_lid from v_languages where lower(language_iso)='eng')");
        wc.push("mid in ("+params[0]+")");
        sql="SELECT "+fields+ssList+" FROM v_media "+nbd_join+" where "+wc.join(' and ');
        if(NbdGroupReqd && pif.getFallbackMode()&& !params[3]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if (sqlId==cApiBundlesData) {
        if (BlockMIDClause) wc.push(BlockMIDClause);
        if(BlockByPpvClause)wc.push(BlockByPpvClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[2]) {joining_field="v_categorymedia"; nbd_join =__getNbdgroupClause("v_categorymedia")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        wc.push("lid="+core.settings.languageID);
        wc.push(MediaConfigClause);
        wc.push("mid in (select DISTINCT bundle_parentmid from v_media_bundles "+(params[0]!="all"?" where mid="+params[0]:"")+")");
        sql="SELECT "+fields+ssList+" FROM v_categorymedia "+nbd_join+" where "+wc.join(' and ');
        if(NbdGroupReqd && pif.getFallbackMode()&& !params[2]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if (sqlId==cApiBundlesDataWithMid) {
        if (BlockMIDClause) wc.push(BlockMIDClause);
        if(BlockByPpvClause)wc.push(BlockByPpvClause)
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode()&& !params[3]) {joining_field="v_categorymedia"; nbd_join =__getNbdgroupClause("v_categorymedia")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        wc.push("lid="+core.settings.languageID);
        sql = "";
        wc.push(MediaConfigClause);
        if(params[0] != '' && params[0]!=undefined && params[2] != '' && params[2] != undefined){sql = "select "+fields+ssList+", 2 as custom_order FROM v_categorymedia where "+wc.join(' and ')+" and mid="+params[0]+" and cid = "+params[2]+" union ";}
        wc.push("mid in (select DISTINCT bundle_parentmid from v_media_bundles "+((params[0]!=""&&params[0]!=undefined)?" where mid="+params[0]:"")+")");
        sql+=" SELECT "+fields+ssList+", 1 as custom_order FROM v_categorymedia "+nbd_join+" where "+wc.join(' and ')+" order by custom_order";
        if(NbdGroupReqd && pif.getFallbackMode()&& !params[3]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if (sqlId==cApiCategoryByTemplateId) {
        wc.push(MediaConfigClause);
        if (BlockCIDClause) wc.push(BlockCIDClause);
        wc.push("lid="+core.settings.languageID+" and "+categoryAttributeForTemplate+" in ("+formatCsv(params[0])+")  "+__getMediaDateClause("category_startdate","category_enddate")+"");
        sql="SELECT "+fields+ssList+" FROM v_categories where "+wc.join(' and ')+(params[2]?" group by "+params[2]:'')+getOrderClause('category_order',categoryAttributeForTemplate,params[0],params[3],params[4]);

    }else if (sqlId==cApiCategoryByFieldName) {
        wc.push(MediaConfigClause);
        if (BlockCIDClause) wc.push(BlockCIDClause);
        if(!params[2]){params[2]='';}
        wc.push("lid="+core.settings.languageID+"  "+__getMediaDateClause("category_startdate","category_enddate")+"");
        if(params[2] && params[2]!=""){wc.push(params[0]+" in ("+formatCsv(params[2])+")")}else {wc.push(params[0]+" is not null or "+params[0]+"!=''")}
        sql="SELECT "+fields+ssList+" FROM v_categories where "+wc.join(' and ')+(params[3]?" group by "+params[3]:'')+getOrderClause('category_order',params[0],params[2],params[4],params[5]);

    }else if (sqlId==cApiCategoryMediaByMid) {
        var groupbyClause = '';var or = [];
        if(params[2]){groupbyClause = " group by mid ";or.push(" cid = "+params[2]);}
        if(fields.indexOf('categorymedia_price')>0){or.push(" (categorymedia_price not null)");}
        if(fields.indexOf('categorymedia_channelnum')>0){or.push("categorymedia_channelnum not null");}
        if(fields.indexOf('categorymedia_channel')>0){or.push("(categorymedia_channel not null  and categorymedia_channel>0)");}
        wc.push('('+or.join(' or ')+')');
        if (BlockMIDClause) wc.push(BlockMIDClause);
        if(BlockByPpvClause) wc.push(BlockByPpvClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[6]) {joining_field="v_categorymedia"; nbd_join =__getNbdgroupClause("v_categorymedia")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        wc.push(MediaConfigClause);
        wc.push("lid="+core.settings.languageID+" and mid in ("+params[0]+")");
        sql="SELECT "+fields+ssList+" FROM v_categorymedia "+nbd_join+" where "+wc.join(' and ')+groupbyClause+getOrderClause('mid','mid',params[0],params[3],params[4]);
         if(NbdGroupReqd && pif.getFallbackMode() && !params[6]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if (sqlId==cApiChildCategoryByTemplateId) {
        wc.push(MediaConfigClause);
        if (BlockCIDClause) wc.push(BlockCIDClause);
        wc.push("lid="+core.settings.languageID+" and parentcid in (select distinct cid from v_categories where "+categoryAttributeForTemplate+" in ("+formatCsv(params[0])+"))  "+__getMediaDateClause("category_startdate","category_enddate")+" order by category_order");
        sql="SELECT "+fields+ssList+" FROM v_categories where "+wc.join(' and ');

    }else if (sqlId==cApiChildMediaByTemplateId) {
        wc.push(MediaConfigClause);
        if (BlockMIDClause) wc.push(BlockMIDClause);
        if(BlockByPpvClause)wc.push(BlockByPpvClause)
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[5]) {joining_field="v_categorymedia"; nbd_join =__getNbdgroupClause("v_categorymedia")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        wc.push("lid="+core.settings.languageID+" and cid in ( select distinct cid from v_categories where "+categoryAttributeForTemplate+" in ("+formatCsv(params[0])+"))  "+__getMediaDateClause("categorymedia_startdate","categorymedia_enddate")+"");
        var  orderByClause  = [];
        if(params[3] != undefined && params[3]!='' && params[4] != undefined && params[4]!=''){
            var customList = String(params[4]);
            if(fields.indexOf(params[3])<0){fields += ','+params[3];}
            orderByClause.push("order by case "+params[3]);
            var sortOrderArr = customList.split(',');for(var x in sortOrderArr){ orderByClause.push("when '"+sortOrderArr[x]+"' then "+x);}
            orderByClause.push('else '+(sortOrderArr.length + 1)+' End,categorymedia_order');
        }else if(params[3] != undefined && params[3]!=''){
             if(fields.indexOf(params[3])<0){fields += ','+params[3];}
            orderByClause.push("order by "+params[3]);
        }else{
            orderByClause.push("order by categorymedia_order")
        }
        sql="SELECT "+fields+ssList+" FROM v_categorymedia "+nbd_join+" where "+wc.join(' and ')+" "+orderByClause.join(" ");
         if(NbdGroupReqd && pif.getFallbackMode() && !params[5]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if (sqlId==cApiNonRouteChildMediaByTemplateId) {
        sql="SELECT "+fields+ssList+" FROM v_categorymedia where cid in ( select distinct cid from v_categories where "+categoryAttributeForTemplate+" in ("+formatCsv(params[0])+")) "+wc.join(' and ')+" group by mid";

    }else if (sqlId==cApiParentCategoryByMid) {
        wc.push("v_cm.cid = v_ct.cid and v_cm.mid = "+params[0]);
        if(!pif.getFallbackMode())wc.push(__getMediaDateClause("v_ct.category_startdate","v_ct.category_enddate",true)+" and "+__getMediaDateClause("v_cm.categorymedia_startdate","v_cm.categorymedia_enddate",true));
        wc.push("v_cm.media_config_id = "+MediaConfig+" and v_ct.media_config_id = "+MediaConfig);
        if (BlockCIDClause) wc.push(" v_cm."+BlockCIDClause);
        sql="SELECT "+fields+ssList+" from v_categorymedia as v_cm, v_categories as v_ct where "+wc.join(' and ')+" limit 1 ";

    }else if (sqlId==cApiCityByIATA) {
        wc.push("city_code in ("+formatCsv(params[0])+")"+(params[2]!='NA'?" and language_id="+core.settings.languageID:""));
        sql="SELECT "+fields+ssList+" from cityCodeTable where "+wc.join(' and ')+getOrderClause('label_text','city_code',params[0],params[3],params[4]);

    }else if (sqlId==cApiCategoryByICAO) {
        wc.push("lid="+core.settings.languageID+" and "+MediaConfigClause+"  "+__getMediaDateClause("category_startdate","category_enddate") );
        sql =  "select "+fields+ssList+",'1' as unionOrder from v_categories as v_ct  where category_attr_dest_icao='"+params[0]+"' and "+wc.join(' and ');
        sql += " union ";
        sql +=  "select "+fields+ssList+",'2' as unionOrder from v_categories as v_ct  where "+categoryAttributeForTemplate+"='HOM0' and "+wc.join(' and ');
        sql += " order by unionOrder ";
    }else if(sqlId==cApiMediaByFieldName){
        wc.push("lid="+core.settings.languageID+" and "+MediaConfigClause+"  "+__getMediaDateClause("categorymedia_startdate","categorymedia_enddate"));
        if(params[2] && params[2]!=""){
            wc.push(params[0]+" in ('"+(typeof params[2]=="string"?params[2]:params[2].join("','"))+"')")
        }else {
            wc.push(params[0]+" is not null or "+params[0]+"!=''")
        }
        if (BlockMIDClause) wc.push(BlockMIDClause);
        if(BlockByPpvClause)wc.push(BlockByPpvClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[3]) {joining_field="v_categorymedia"; nbd_join =(params[4] && params[4]==true)?__getNbdgroupClause("v_categorymedia",true):__getNbdgroupClause("v_categorymedia")}
        if (SeatRatingClause) wc.push(SeatRatingClause);        
        sql="SELECT "+fields+ssList+','+params[0]+" FROM v_categorymedia "+nbd_join+" where "+wc.join(' and ')+((params[5] && params[5]==1)?"":" group by mid ")+" order by categorymedia_order";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[3]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if(sqlId==cApiBundleChildData){
        wc.push("lid="+core.settings.languageID+" and "+MediaConfigClause+"  "+__getMediaDateClause("categorymedia_startdate","categorymedia_enddate"));
        var bmid = params[0];var midArr = dataController.mediaServices.getChildBundleMids(bmid);  wc.push("mid in ("+midArr+")");
        if (BlockMIDClause) wc.push(BlockMIDClause);
        if(BlockByPpvClause)wc.push(BlockByPpvClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[3]) {joining_field="v_categorymedia"; nbd_join =__getNbdgroupClause("v_categorymedia")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        sql="SELECT "+fields+ssList+" FROM v_categorymedia "+nbd_join+" where "+wc.join(' and ')+" group by mid order by categorymedia_order";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[3]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if(sqlId==cApiChildMediaByAggregateMid){
        if (BlockMIDClause) wc.push(BlockMIDClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[3]) {joining_field="v_media_aggs"; nbd_join =__getNbdgroupClause("v_media_aggs")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        var  orderByClause  = [];
        orderByClause.push("order by case aggregate_parentmid");
        var sortOrderArr = params[0].split(',');
        for(var x in sortOrderArr){ orderByClause.push("when '"+sortOrderArr[x]+"' then "+x);}
        orderByClause.push('else '+(sortOrderArr.length + 1)+' End, aggregate_order');
        wc.push("lid="+core.settings.languageID+" and aggregate_parentmid in("+params[0]+") "+orderByClause.join(" "));
        sql =  "select "+fields+ssList+" FROM v_media_aggs "+nbd_join+" where "+wc.join(' and ');
        if(NbdGroupReqd && pif.getFallbackMode() && !params[3]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if(sqlId==cApiAggregateMidByChildMid){
        if (BlockMIDClause) wc.push(BlockMIDClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[3]) {joining_field="v_media_aggs"; nbd_join =__getNbdgroupClause("v_media_aggs")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        wc.push("lid="+core.settings.languageID+" and mid in("+params[0]+") order by aggregate_order");
        sql =  "SELECT DISTINCT "+fields+ssList+" FROM v_media_aggs "+nbd_join+" where "+wc.join(' and ');
        if(NbdGroupReqd && pif.getFallbackMode() && !params[3]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
    }else if(sqlId==cApiContentTerminalMaps){
        wc.push(" a.id = b.pid and a.destination='"+params[0]+"' and a.lid=b.lid  and a.lid="+((params[2])?params[2]:core.settings.languageID));
        sql="SELECT "+fields+ssList+" from contentTable a, contentTable b where "+wc.join(' and ');
    }else if(sqlId==cApiKeywordSearchPosters){
       return ssList;
    }else if(sqlId==cApiKeywordSearchCategoryPosters){
        return ssList;
    }
    else {core.error("Media | formAPIQuery | Unknown psid: "+sqlId+". Exiting Function..."); return false;}
    core.debug("Media | formAPIQuery | sqlId: "+sqlId+" sql: "+sql);
    delete fields;delete ssList;delete ss;delete p_index;delete i;delete pindx;delete spindx;delete sqlId;delete sql;delete wc;delete aPtr1;delete nbd_join;delete joining_field;delete agg_fd
    delete bmid; delete sortOrderArr; delete sortOrderArr;delete x;delete orderByClause;delete customList; delete groupbyClause;delete or;
    return sql;
}

function getParentMenu (params,callback,modelInstance) {
    core.info("Media | getParentMenu | params : "+params+" modelInstance: "+modelInstance);
    if(!params[0]) return false;
    var c="lid="+core.settings.languageID+" "+__getMediaDateClause("category_startdate","category_enddate")+" and ("+categoryAttributeForTemplate+" is not null or "+categoryAttributeForTemplate+"!='')";
    var wc = [MediaConfigClause,c];
    wc.push("a.cid="+params[0]);
    var sql = "SELECT cid, parentcid,(SELECT "+categoryAttributeForTemplate+" FROM  v_categories where cid=a.parentcid and "+c+") as parent_template_id from v_categories a where "+wc.join(' and ');
    core.info("Media | getParentMenu | sql: "+sql);

    __modelUpdate (cApiParentMenu,modelInstance,sql,callback);
    delete c;delete wc;delete sql;return true;
}

function getNextMenu(params, callback) {		//params[<CID/MID>,<ActionCode>]
    core.info("Media | getNextMenu | params : "+params);
    var aPtr=ActionCodeArr[params[1]];var orderByClause=[];
    if (!aPtr) {core.error("Media | getNextMenu | Params has invalid Backend ActionCode. Exiting Function..."); return false;}

    var nodeid = params[0];
    //core.info("Media | getNextMenu | NODEID: "+nodeid);

    // Form sql statement
    var fields=MenuFieldsArr[aPtr.fdId];
    var ssList="";
    if (MenuPosterFieldArr[aPtr.ssId]) {
        var ss=MenuPosterFieldArr[aPtr.ssId];
        if(CustomPosterLabels.length){
            var p_index = core.settings.resolutionIndex;
            for(var i in CustomPosterLabels){
                if (ss[p_index]) ssList+=', '+ss[p_index]+' as '+CustomPosterLabels[i];
                p_index = p_index + core.settings.totalResolutions;
            }
        }else{
            var pindx=core.settings.resolutionIndex; var spindx=pindx+core.settings.totalResolutions;
            if (ss[pindx]) ssList=', '+ss[pindx]+' as poster'; if (ss[spindx]) ssList+=', '+ss[spindx]+' as synopsisposter';
        }
    }

    var sqlId=aPtr.psId; var sql=""; var apiType=0;			// default - cid
    if (MenuWhereClauseArr[aPtr.wrId]) var wc = [MenuWhereClauseArr[aPtr.wrId]]; else var wc = [];

    if (sqlId==0) { //category listing
        if(params[2] && params[2]!=''){var cidList = CategoryPpvLookup[params[2]][nodeid];if(cidList != undefined){core.log("CategoryPpvLookup["+params[2]+"]["+nodeid+"] : "+cidList);wc.push("cid in ("+cidList+")");}}
        wc.push(MediaConfigClause);
        if (BlockCIDClause) wc.push(BlockCIDClause);
        if(params[3] != undefined && params[3]!='' && params[4] != undefined && params[4]!=''){
            var customList = String(params[4]);
            if(fields.indexOf(params[3])<0){fields += ','+params[3];}
            orderByClause.push("order by case "+params[3]);
            var sortOrderArr = customList.split(',');
            for(var x in sortOrderArr){ orderByClause.push("when '"+sortOrderArr[x]+"' then "+x);}
            orderByClause.push('else '+(sortOrderArr.length + 1)+' End, category_order');
        }else if(params[3] != undefined && params[3]!=''){
            if(fields.indexOf(params[3])<0){fields += ','+params[3];}
           orderByClause.push("order by "+params[3]);
       }else{
            orderByClause.push("order by category_order");
        }
        wc.push("parentcid = "+nodeid+" and lid="+core.settings.languageID+" "+__getMediaDateClause("category_startdate","category_enddate")+" "+orderByClause.join(' '));
        sql =  "select "+fields+ssList+" FROM v_categories where "+wc.join(' and ');

    } else if (sqlId==1) { //media listing
        wc.push(MediaConfigClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[5]) {joining_field="v_categorymedia"; nbd_join =__getNbdgroupClause("v_categorymedia")}
        if (SeatRatingClause) wc.push(SeatRatingClause);
        if (BlockMIDClause) wc.push(BlockMIDClause);
        if(BlockByPpvClause)wc.push(BlockByPpvClause);
        if (SeatRatingClause) wc.push(SeatRatingClause);
        if(params[2] && params[2]!=''){var bmid = params[2];var midArr = dataController.mediaServices.getChildBundleMids(bmid);  wc.push("mid in ("+midArr+")");}
        if(params[3] != undefined && params[3]!='' && params[4] != undefined && params[4]!=''){
            var customList = String(params[4]);
            if(fields.indexOf(params[3])<0){fields += ','+params[3];}
            orderByClause.push("order by case "+params[3]);
            var sortOrderArr = customList.split(',');for(var x in sortOrderArr){ orderByClause.push("when '"+sortOrderArr[x]+"' then "+x);}
            orderByClause.push('else '+(sortOrderArr.length + 1)+' End, categorymedia_order');
        }else if(params[3] != undefined && params[3]!=''){
             if(fields.indexOf(params[3])<0){fields += ','+params[3];}
            orderByClause.push("order by "+params[3]);
        }else{
            orderByClause.push("order by categorymedia_order");
        }

        wc.push(" lid="+core.settings.languageID+" and cid="+nodeid+" "+__getMediaDateClause("categorymedia_startdate","categorymedia_enddate")+" "+orderByClause.join(' '));
        sql = " SELECT "+fields+ssList+" FROM  v_categorymedia "+nbd_join+" where "+wc.join(' and ');
        if(NbdGroupReqd && pif.getFallbackMode() && !params[5]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}

    } else if (sqlId==2) { //TV listing
        if (BlockMIDClause) wc.push(BlockMIDClause);
        var nbd_join = ""; var joining_field = "";
        if(NbdGroupReqd && pif.getFallbackMode() && !params[5]) {joining_field="v_media_aggs"; nbd_join =__getNbdgroupClause("v_media_aggs")}
        if (SeatRatingClause) wc.push(SeatRatingClause);

        if(params[3] != undefined && params[3]!='' && params[4] != undefined && params[4]!=''){
            var customList = String(params[4]);
            if(fields.indexOf(params[3])<0){fields += ','+params[3];}
            orderByClause.push("order by case "+params[3]);
            var sortOrderArr = customList.split(',');for(var x in sortOrderArr){ orderByClause.push("when '"+sortOrderArr[x]+"' then "+x);}
            orderByClause.push('else '+(sortOrderArr.length + 1)+' End, aggregate_order');
        }else if(params[3] != undefined && params[3]!=''){
             if(fields.indexOf(params[3])<0){fields += ','+params[3];}
            orderByClause.push("order by "+params[3]);
        }else{
            orderByClause.push("order by aggregate_order");
        }

        wc.push("lid="+core.settings.languageID+" and aggregate_parentmid="+nodeid+" "+orderByClause.join(" "));
        sql =  "select "+fields+ssList+" FROM v_media_aggs "+nbd_join+" where "+wc.join(' and ');
         if(NbdGroupReqd && pif.getFallbackMode() && !params[5]) {sql =__getNbdgroupModifiedQuery(sql, joining_field)}
        apiType=1;

    }else if(sqlId==3){ // AA Arrival Info and Airport Map section - Re arrange category listing based on the sent CID
        wc.push(MediaConfigClause);
        if (BlockCIDClause) wc.push(BlockCIDClause);
        wc.push("parentcid = "+nodeid+" and lid="+core.settings.languageID+" "+__getMediaDateClause("category_startdate","category_enddate")+" "+getOrderClause('category_order','cid',params[2]?params[2]:'',params[2]?'cid':''));
        sql =  "select "+fields+ssList+" FROM v_categories where "+wc.join(' and ');

    }else if(sqlId==4){ // AA Arrival Info and Airport Map section - Re arrange category listing based on the sent MID which is current destination
        wc.push(MediaConfigClause);
        if (BlockMIDClause) wc.push(BlockMIDClause);
        if(BlockByPpvClause)wc.push(BlockByPpvClause);
        if (SeatRatingClause) wc.push(SeatRatingClause);
        wc.push(" lid="+core.settings.languageID+" and cid="+nodeid+" "+__getMediaDateClause("categorymedia_startdate","categorymedia_enddate")+" "+getOrderClause('categorymedia_order','mid',params[2]?params[2]:'',params[2]?'mid':''));
        sql = " SELECT "+fields+ssList+" FROM  v_categorymedia where "+wc.join(' and ');
    }else if (sqlId==21 || sqlId==22 || sqlId==23) {
        wc.push("pid="+nodeid+" and (class & '"+IntCabinClass+"') and lid="+core.settings.languageID+" order by seq_no");
        sql="SELECT "+fields+ssList+" FROM contentTable where "+wc.join(' and ');
        apiType=sqlId;

    }else {core.error("Media | getNextMenu | Unknown psid: "+sqlId+". Exiting Function..."); return false;}
    core.info("Media | getNextMenu | sql: "+sql);

    __modelUpdate (apiType,nodeid,sql,callback,'',aPtr.dbId);;
    delete aPtr;delete orderByClause;delete nodeid;delete fields;delete ssList;delete ss;delete p_index;delete i;delete spindx;
    delete sql;delete apiType;delete wc;delete customList;delete sortOrderArr;delete x; delete nbd_join;delete joining_field
}

function updateMenuWhereClause (arrIndx,operation,whereString) {
    if(operation=="insert") MenuWhereClauseArr[arrIndx]=whereString;
    else if(operation=="append") MenuWhereClauseArr[arrIndx]+=whereString;
    else return false;
    return true;
}

function setConfiguration(dataSection){
    // BE Action Code
    var tmpArr = coreHelper.updateCfgArray (dataSection.DataActionCode, "psId,fdId,wrId,ssId,trId,templateId,dbId,dtId");
    var templateArr;
    for (var i=0; i<tmpArr.length; i++) {
        templateArr = tmpArr[i].templateId.split(',');
        for (var j=0; j<templateArr.length; j++) { ActionCodeArr[templateArr[j]]=tmpArr[i]; }
    }
    // Menu Data
    MenuFieldsArr = coreHelper.updateCfgArray (dataSection.DataMenuFields, "index,fields", "index", "fields");
    MenuWhereClauseArr = coreHelper.updateCfgArray (dataSection.DataMenuWhereClause, "index,whereclause", "index", "whereclause");
    MenuPosterFieldArr = coreHelper.updateCfgArray (dataSection.DataMenuPosterField, "", 0);
    // Databases
    DatabasesArr = coreHelper.updateCfgArray (dataSection.Databases, "index,dbpath,uId", "index");
    __loadDatabases(DatabasesArr);
    CompList = coreHelper.cfgVariantObjToArray(dataSection.DataCompList);
    WoiAppList = coreHelper.cfgVariantObjToArray(dataSection.DataWoiAppList?dataSection.DataWoiAppList:'');
    IntLanguagesArr = coreHelper.cfgVariantObjToArray(dataSection.DataIntLanguageOrder);
    IntLanguagesType = dataSection.DataIntLanguagesType ? dataSection.DataIntLanguagesType:IntLanguagesType;
    SoundtrackType = dataSection.DataSoundtrackType ? dataSection.DataSoundtrackType:SoundtrackType;
    SubtitleType = dataSection.DataSubtitleType ? dataSection.DataSubtitleType:SubtitleType;
    LanguageLabelsByLidType = dataSection.DataLanguageLabelsByLidType ? dataSection.DataLanguageLabelsByLidType:LanguageLabelsByLidType;
    BlockBySeatRating = dataSection.DataBlockBySeatRating ? (dataSection.DataBlockBySeatRating=="false"?false:true):BlockBySeatRating;
    BlockBySeatRatingExclusive = dataSection.DataBlockBySeatRatingExclusive ? (dataSection.DataBlockBySeatRatingExclusive=="false"?false:true):BlockBySeatRatingExclusive;
    WithEmbedSubtitle = dataSection.DataWithEmbedSubtitle ? (dataSection.DataWithEmbedSubtitle=="false"?false:true):WithEmbedSubtitle;
    PreloadMedia = dataSection.DataPreloadMedia ? (dataSection.DataPreloadMedia=="false"?false:true):PreloadMedia;
    var tmpPreloadMediaTemplateIds = coreHelper.cfgVariantObjToArray(dataSection.DataPreloadMediaTemplateIds);
    __setPreloadMediaTemplateIds(tmpPreloadMediaTemplateIds);
    var DestroyModels = dataSection.DataDestroyModels ? (dataSection.DataDestroyModels=="false"?false:true):true;
    __setDestroyModels(DestroyModels);
    PreloadMediaRootNode = coreHelper.cfgVariantObjToArray(dataSection.DataPreloadMediaRootNode);
    if(PreloadMediaRootNode[0]=="")
        getHomeNodeFromDb(PreloadMediaRootNode,populateRootNodeFromDb)
    PreloadLabel = dataSection.DataPreloadLabel ? (dataSection.DataPreloadLabel=="false"?false:true):PreloadLabel;
    PreloadCategoryPosters = dataSection.DataPreloadCategoryPosters ? (dataSection.DataPreloadCategoryPosters=="false"?false:true):PreloadCategoryPosters;
    PreloadSurvey = dataSection.DataPreloadSurvey ? (dataSection.DataPreloadSurvey=="false"?false:true):PreloadSurvey;
    TriggeredSurveyAppendInModel = dataSection.DataTriggeredSurveyAppendInModel ? (dataSection.DataTriggeredSurveyAppendInModel=="false"?false:true):TriggeredSurveyAppendInModel;
    IsParentBlockRequired = dataSection.DataParentBlockEnabled ? (dataSection.DataParentBlockEnabled=="false"?false:true):IsParentBlockRequired;
    if(!(IsParentBlockRequired || pif.getPaxusScreenLogStatus())){ //node look up
        var tmpArray = coreHelper.updateCfgArray (dataSection.DataCategoryLookUp, "name,tid,code");
        IsOnlyTagNameLookup = true;
        for (var i=0; i<tmpArray.length; i++) {if(tmpArray[i].tid){CategoryLookUptemplateIdArray[i] = tmpArray[i].tid}}
    }
    AttrForPaxus = dataSection.DataAttrForPaxus ? dataSection.DataAttrForPaxus:AttrForPaxus;
    PpvRequired = dataSection.DataPpvRequired?(dataSection.DataPpvRequired=="false"?false:true):PpvRequired;
    CustomPosterLabels = dataSection.DataCustomPosterLabels?coreHelper.cfgVariantObjToArray(dataSection.DataCustomPosterLabels):CustomPosterLabels;
    LanugageLocaleRequired = dataSection.DataLanugageLocaleRequired?(dataSection.DataLanugageLocaleRequired=="false"?false:true):LanugageLocaleRequired;
    HideEmptyCategory = dataSection.DataHideEmptyCategory?(dataSection.DataHideEmptyCategory=="false"?false:true):HideEmptyCategory;
    ExceptionEmptyTemplates = coreHelper.cfgVariantObjToArray(dataSection.DataExceptionEmptyTemplates);
    NbdGroupReqd = dataSection.DataNbdGroupReqd?(dataSection.DataNbdGroupReqd=="false"?false:true):NbdGroupReqd;
    DefaultRouteId = dataSection.DataDefaultRouteId ? dataSection.DataDefaultRouteId:DefaultRouteId;
    if(pif.getFallbackMode && DefaultRouteId)setRouteId(RouteId);
    // Set Cabin Class Values
    IntCabinClassArr = coreHelper.updateCfgArray (dataSection.DataIntCabinClass, "index,classvalue", "index","classvalue");
    var c = IntCabinClassArr[core.pif.getCabinClassName()]; if (c) IntCabinClass = c;
    // Shopping
    if(CompList.indexOf("Shopping")!=-1 || CompList.indexOf("Hospitality")!=-1 || CompList.indexOf("Shopping2")!=-1 || CompList.indexOf("Hospitality2")!=-1){CabinClassArr = [];CabinClassArr = coreHelper.updateCfgArray (dataSection.DataCabinClass, "index,classvalue", "index","classvalue");}
    if(CompList.indexOf("Shopping")!=-1 || CompList.indexOf("Shopping2")!=-1){ShopppingConfiguration=[];ShopppingConfiguration = coreHelper.updateCfgArray (dataSection.DataShopppingConfiguration, "name,value", "name","value");}
    if(CompList.indexOf("Hospitality")!=-1 || CompList.indexOf("Hospitality2")!=-1){HospitalityConfiguration=[];HospitalityConfiguration = coreHelper.updateCfgArray (dataSection.DataHospitalityConfiguration, "name,value", "name","value");}
    if(CompList.indexOf("Ltn")!=-1){LtnConfiguration=[];LtnConfiguration = coreHelper.updateCfgArray (dataSection.DataLtnConfiguration, "name,value", "name","value");}

    //Woi APPS
    var temp =[]
    temp = coreHelper.updateCfgArray(dataSection.DataWoiAppConfiguration, "woiAppIndex,fieldId,fields");
    WoiAppConfiguration=[];
    for(var i in temp){
        if( typeof WoiAppConfiguration[temp[i]['woiAppIndex']]=='undefined'){
            WoiAppConfiguration[temp[i]['woiAppIndex']]=[];
        }
        WoiAppConfiguration[temp[i]['woiAppIndex']][temp[i]['fieldId']]=temp[i]['fields'];
    }
       var temp1=[];
       temp1 = coreHelper.updateCfgArray(dataSection.DataWoiAppCustomConfiguration, "woiAppIndex,fieldId,fields");
       WoiAppCustomConfiguration=[];
       for(var i in temp1){
           if( typeof WoiAppCustomConfiguration[temp1[i]['woiAppIndex']]=='undefined'){
               WoiAppCustomConfiguration[temp1[i]['woiAppIndex']]=[];
           }
           WoiAppCustomConfiguration[temp1[i]['woiAppIndex']][temp1[i]['fieldId']]=temp1[i]['fields'];
       }
    WoiAppResolutionMap=[];
    WoiAppResolutionMap = coreHelper.updateCfgArray(dataSection.DataWoiAppResolutionMap,"index,resvalue","index","resvalue")
    WoiAppAdvResolutionMap = [];
       var temp2=[];
       temp2=coreHelper.updateCfgArray(dataSection.DataWoiAppAdvResolutionMap,"woiAppIndex,resolutionIndex,type,resvalue")
    WoiAppIsoMap=[];WoiAppIsoMap=coreHelper.updateCfgArray(dataSection.DataWoiAppIsoMap,"isocode,woiappcode","isocode","woiappcode")
    MenuSDKVersion= coreHelper.updateCfgArray(dataSection.DataMenuSDKVersion);

       for(var i in temp2){           
           if( typeof WoiAppAdvResolutionMap[temp2[i]['woiAppIndex']]=='undefined'){
               WoiAppAdvResolutionMap[temp2[i]['woiAppIndex']]=[];
           }
           var key = "'"+temp2[i]['resolutionIndex']+'-'+temp2[i]['type']+"'"
           WoiAppAdvResolutionMap[parseInt(temp2[i]['woiAppIndex'])][key]=temp2[i]['resvalue'];
       }


    delete tmpArr;delete templateArr;delete i;delete j;delete tmpPreloadMediaTemplateIds; delete tmpArray;delete c;
    return true;
}

function getHomeNodeFromDb(params,callback){
    core.info("Media | Fetching RootNode from Db");
    getCategoryByTemplateId([params[1]],callback);
}

function populateRootNodeFromDb(dModel){
    PreloadMediaRootNode=[dModel.getValue(0,"cid"),dModel.getValue(0,"category_attr_template_id")];
    core.info("Media | RootNode from Db : "+PreloadMediaRootNode);
}


function setSimConfiguration(sData){
    MakeHeadendRequest = sData.MakeHeadendRequest?(sData.MakeHeadendRequest=="false"?false:true):MakeHeadendRequest;
    setRouteData({'seatrating':sData.SeatRating,'aircraftType':sData.AircraftType,'aircraftSubType':sData.AircraftSubType});
}

function __getNbdgroupClause(joining_table,using_uniqueid){
    var jc = "";
    var activeGroupsClause = pif.getActiveNbdGroups().length? "n.nbdGroup in ('"+pif.getActiveNbdGroups().join("','")+"')":"";
    if(using_uniqueid)
        jc = " inner join (select m.mid from media m, media m2, nbdgroups n where "+((activeGroupsClause!="")?activeGroupsClause+" and ":"")+" n.mid = m2.mid and m2.uniqueid = m.uniqueid group by m.mid) as nbdMids on nbdMids.mid="+joining_table+".mid"
    else{
        jc = " inner join (select n.mid, n.nbdGroup from nbdgroups n "+((activeGroupsClause!="")?"where "+activeGroupsClause:"")+") as  nbdMids on nbdMids.mid = "+joining_table+".mid";
    }
    return jc;
}

function __getNbdgroupModifiedQuery(query,joining_table){
    query = (query.indexOf('group by')<0)?((query.indexOf('order')>0)?query.replace(/ order/g, ' group by mid order'):query+' group by mid'):query;
    query = query.replace(/ mid/g, ' '+joining_table+'.mid ');
    query = query.replace(/,mid/g, ','+joining_table+'.mid ');
    if(query.indexOf(joining_table+'.mid ,')>0)
        query=query.replace(joining_table+'.mid ,',joining_table+'.mid as mid,');
    return query;
}

function formatCsv(str){ return "'"+str.split(',').join("','")+"'"; }

function getOrderClause(defaultField,fieldname,customList,sortBy,sortOrder){
    if(sortBy==''){ return " order by "+ defaultField;
    }else if(sortBy && sortOrder){  return " order by "+ sortBy+ " "+sortOrder;
    }else{
        customList = String(customList);
        var sql = " order by case "+fieldname;
        var sortOrderArr = customList.split(',');
        for(x in sortOrderArr) sql += " when '"+sortOrderArr[x]+"' then "+x;
        sql +=' else '+(x+1)+' End';
        return sql;
    }
}

function __getMediaDateClause(start_date_field,end_date_field,is_no_and_req){
    if(pif.getFallbackMode()){
        return "";
    }else{
        return is_no_and_req?"("+MediaDate+" BETWEEN "+start_date_field+" and "+end_date_field+")":" and ("+MediaDate+" BETWEEN "+start_date_field+" and "+end_date_field+")"
        //return "("+start_date_field+"<="+MediaDate+" and "+MediaDate+"<="+end_date_field+")"
    }

}
