import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

DataLoader {
    signal __sigModelReady(variant model,bool status);

    onSigDataReady: {
        if (errCode==0){
            core.info("Ans.qml | dataLoader | onSigDataReady");
            __processModel(responseObj,responseString,requestId);
        }else {
            __sigModelReady(airportModel,false);
        }
    }

    onSigLocalDataReady:{
        if(errCode==0){
            core.info("Ans.qml | dataLoader | onSigLocalDataReady ");
            __processModel(responseObj,'fromConfigModel',requestId)
        }else{
            __sigModelReady(airportModel,false);
        }
    }

    //public function
    function getAirportByICAO(icao,langIso,callback) {

        if (isAppActive==true){core.info('Ans.qml| getAirportByICAO | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}

        var requestId=1;airportModel.clear();__updateCallback(callback);
        core.info('Ans.qml | getAirportByICAO | icao : '+icao+' | langIso : '+langIso);

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/ans/getAirportByICAO.json'
            core.info('Ans.qml | getAirportByICAO | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId)
        }else{
            var getParams = "function=getAirportICAOListByLanguageList&icao="+icao+"&lang="+langIso+"&output_format=JSON";
            core.info('Ans.qml | getAirportByICAO | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    function getAirportByIATA(iata,langIso,callback) {

        if (isAppActive==true){core.info('Ans.qml| getAirportByIATA | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}

        var requestId=2;airportModel.clear();__updateCallback(callback);
        core.info('Ans.qml | getAirportByIATA | iata : '+iata+' | langIso : '+langIso);

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/ans/getAirportByIATA.json'
            core.info('Ans.qml | getAirportByIATA | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId)
        }else{
            var getParams = "function=getAirportIATAListByLanguageList&iata="+iata+"&lang="+langIso+"&output_format=JSON";
            core.info('Ans.qml | getAirportByIATA | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
             __getRemoteData(getParams,requestId);
            }
        }
    }

    function getAirportByIATASimple(iata,langIso,callback) {

        if (isAppActive==true){core.info('Ans.qml| getAirportByIATA | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}

        var requestId=5;airportModelSimple.clear();__updateCallback(callback);
        core.info('Ans.qml | getAirportByIATA | iata : '+iata+' | langIso : '+langIso);

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/ans/getAirportByIATA.json'
            core.info('Ans.qml | getAirportByIATASimple | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId)
        }else{
            var getParams = "function=getAirportIATAListByLanguageList&iata="+iata+"&lang="+langIso+"&output_format=JSON";
            core.info('Ans.qml | getAirportByIATASimple | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
             __getRemoteData(getParams,requestId);
            }
        }
    }

    function getCountryCodesByRange(startCountryAlpha,endCountryAlpha,langIso,callback) {
        if (isAppActive==true){core.info('Ans.qml| getCountryCodesByRange | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        langIso = 'eng'; // Non english not supported for now
        __updateCallback(callback);
        countryRangeModel.clear()
        core.info('Ans.qml | getCountryCodesByRange | langIso : '+langIso+' | startCountryAlpha : '+startCountryAlpha+' | endCountryAlpha : '+endCountryAlpha);
        if(countryCodeModel.count == 0){
            Store.callback = callback;
            Store.countryCodeRange++;Store.startAlpha = startCountryAlpha;Store.endAlpha = endCountryAlpha;Store.langIso = langIso;
            isAppActive=false;getCountryCodes(langIso);return true;
        }
        for (var counter= 0; counter<countryCodeModel.count; counter++){
            var countryName = (countryCodeModel.getValue(counter,"country_name")).toUpperCase();
            //var charac = cn.toUpperCase()
            if(countryName.charAt(0) >=  startCountryAlpha.toUpperCase() && countryName.charAt(0) <=  endCountryAlpha.toUpperCase())
            {
                countryRangeModel.append({"country_name": countryCodeModel.getValue(counter,"country_name"),"dial_code"   : countryCodeModel.getValue(counter,"dial_code"), "country_code": countryCodeModel.getValue(counter,"country_code"),"language_code"  : countryCodeModel.getValue(counter,"language_code")})
            }
        }
        isAppActive=false;__sigModelReady(countryRangeModel,true);

    }

    function getCountryNameByCode(input,langIso,callback){
        if (isAppActive==true){core.info('Ans.qml| getCountryNameByCode | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        if(langIso==''){langIso = 'eng';}
        countryNameModel.clear()
        core.info('Ans.qml | getCountryCodesByRange | langIso : '+langIso+' | input code : '+input);
        if(countryCodeModel.count == 0){
            Store.callback = callback;
            Store.countryNameByCode++;Store.inputCode = input;Store.langIso = langIso;
            isAppActive=false;getCountryCodes(langIso);return true;
        }
        for (var counter= 0; counter<countryCodeModel.count; counter++){
            var dial_code = String(countryCodeModel.getValue(counter,"dial_code"));
            var re = new RegExp("^("+input+")", "i");
            if(re.test(dial_code))
            {
                countryNameModel.append({"country_name": countryCodeModel.getValue(counter,"country_name"),"dial_code"   : countryCodeModel.getValue(counter,"dial_code"), "country_code": countryCodeModel.getValue(counter,"country_code"),"language_code"  : countryCodeModel.getValue(counter,"language_code")})
            }
        }
        __updateCallback(callback);isAppActive=false;__sigModelReady(countryNameModel,true);
    }

    function getCountryCodes(langIso,callback){
         if (isAppActive==true){core.info('Ans.qml| getCountryCodes | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
          core.info('Ans.qml | getCountryCodes | langIso : '+langIso);
         var requestId=3;__updateCallback(callback);
        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/ans/getCountryListByLanguageList.json'
            core.info('Ans.qml | getCountryCodes | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId)
        }else{
            var getParams = "function=getCountryListByLanguageList&lang="+langIso+"&output_format=JSON";
            core.info('Ans.qml | getCountryCodes | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
             __getRemoteData(getParams,requestId);
            }
        }
    }

    function getAllAirports(langIso,callback) {
        if (isAppActive==true){core.info('Ans.qml| getAllAirports | App is already Active, current Request is dropped.... ');return false;}else{isAppActive=true;}
        var requestId=4;    allAirportsModel.clear();   __updateCallback(callback);
        core.info('Ans.qml | getAllAirports  | langIso : '+langIso);

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/ans/getAllAirports.json'
            core.info('Ans.qml | getAllAirports | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | Sources :'+getParams);
            __getLocalData(getParams,requestId)
        }else{
            var getParams = "function=getAllAirports&lang="+langIso+"&output_format=JSON";
            core.info('Ans.qml | getAllAirports | makeHeadendRequest : '+dataController.__makeHeadendRequest()+' | URL :'+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    //private function
    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }

    function __processModel(value,data,requestId){
        if(data!='' && value['success'] == 'true'){
            core.info("Ans.qml | __processModel | Data ready = " + value['success'])
            __readJsonObject(requestId,value)
        }else{
            core.info("Ans.qml | __processModel | ERROR | value "+value+" | data :"+data)
            __sigModelReady(airportModel,false);
        }
    }

    function __readJsonObject(requestId,value){
        core.info('Ans.qml | __readJsonObject | id :'+requestId)
        if(requestId==0){core.info('Ans.qml | __readJsonObject | Data reday = false')}
        if(requestId==1){__loadAnsModel(value);}
        if(requestId==2){__loadAnsModel(value);}
        if(requestId==3){__loadAnsCountryCodeModel(value);}
        if(requestId==4){__loadAnsAllAirportsModel(value);}
        if(requestId==5){__loadAnsModelSimple(value);}
    }

    function __loadAnsModel(value){
        core.info('Ans.qml | loadAnsCats | Called ');

        airportModel.clear();
        if(value['airport']){
            for(var counter=0;counter<value['airport'].length;counter++){
                airportModel.insert(0,{"airport_iata": value['airport'][counter].airport_iata, "airport_icao": value['airport'][counter].airport_icao, 'airport_name':(value['airport'][counter].airport_name?value['airport'][counter].airport_name:''), 'city_name':(value['airport'][counter].city_name?value['airport'][counter].city_name:''), 'state_name':(value['airport'][counter].state_name?value['airport'][counter].state_name:''), 'country_name':(value['airport'][counter].country_name?value['airport'][counter].country_name:''), 'gmt_offset':value['airport'][counter].gmt_offset, 'dst_rule':value['airport'][counter].dst_rule, 'latitude':value['airport'][counter].latitude, 'longitude':value['airport'][counter].longitude});

                if(value['airport'][counter]['language'] && value['airport'][counter]['language'][0]){
                    if(value['airport'][counter]['language'][0].airport_xlat && value['airport'][counter]['language'][0].airport_xlat!=''){
                        airportModel.set(0,{'airport_name':value['airport'][counter]['language'][0].airport_xlat});
                    }
                    if(value['airport'][counter]['language'][0].city_xlat && value['airport'][counter]['language'][0].city_xlat!=''){
                        airportModel.set(0,{'city_name':value['airport'][counter]['language'][0].city_xlat});
                    }
                    if(value['airport'][counter]['language'][0].state_xlat && value['airport'][counter]['language'][0].state_xlat!=''){
                        airportModel.set(0,{'state_name':value['airport'][counter]['language'][0].state_xlat});
                    }
                    if(value['airport'][counter]['language'][0].country_xlat && value['airport'][counter]['language'][0].country_xlat!=''){
                        airportModel.set(0,{'country_name':value['airport'][counter]['language'][0].country_xlat});
                    }
                }
            }
        }else {
             core.info('Ans.qml | __loadAnsModel | ERROR | Data not found ');
             __sigModelReady(airportModel,false);
        }

        core.info('Ans.qml | __loadAnsModel | airportModel count : '+airportModel.count);
        isAppActive=false;__sigModelReady(airportModel,true);
    }

    function __loadAnsModelSimple(value){
        core.info('Ans.qml | loadAnsCats | Called ');

        airportModelSimple.clear();
        if(value['airport']){
            for(var counter=0;counter<value['airport'].length;counter++){
                airportModelSimple.insert(0,{"airport_iata": value['airport'][counter].airport_iata, "airport_icao": value['airport'][counter].airport_icao, 'airport_name':(value['airport'][counter].airport_name?value['airport'][counter].airport_name:''), 'city_name':(value['airport'][counter].city_name?value['airport'][counter].city_name:''), 'state_name':(value['airport'][counter].state_name?value['airport'][counter].state_name:''), 'country_name':(value['airport'][counter].country_name?value['airport'][counter].country_name:''), 'gmt_offset':value['airport'][counter].gmt_offset, 'dst_rule':value['airport'][counter].dst_rule, 'latitude':value['airport'][counter].latitude, 'longitude':value['airport'][counter].longitude});

                if(value['airport'][counter]['language'] && value['airport'][counter]['language'][0]){
                    if(value['airport'][counter]['language'][0].airport_xlat && value['airport'][counter]['language'][0].airport_xlat!=''){
                        airportModelSimple.set(0,{'airport_name':value['airport'][counter]['language'][0].airport_xlat});
                    }
                    if(value['airport'][counter]['language'][0].city_xlat && value['airport'][counter]['language'][0].city_xlat!=''){
                        airportModelSimple.set(0,{'city_name':value['airport'][counter]['language'][0].city_xlat});
                    }
                    if(value['airport'][counter]['language'][0].state_xlat && value['airport'][counter]['language'][0].state_xlat!=''){
                        airportModelSimple.set(0,{'state_name':value['airport'][counter]['language'][0].state_xlat});
                    }
                    if(value['airport'][counter]['language'][0].country_xlat && value['airport'][counter]['language'][0].country_xlat!=''){
                        airportModelSimple.set(0,{'country_name':value['airport'][counter]['language'][0].country_xlat});
                    }
                }
            }
        }else {
             core.info('Ans.qml | __loadAnsModel | ERROR | Data not found ');
             __sigModelReady(airportModelSimple,false);
        }

        core.info('Ans.qml | __loadAnsModel | airportModelSimple count : '+airportModelSimple.count);
        isAppActive=false;__sigModelReady(airportModelSimple,true);
    }

    function __loadAnsAllAirportsModel(value){
        core.info('Ans.qml | __loadAnsAllAirportsModel | Called ');
        allAirportsModel.clear();
        if(value['airports']){
            for(var counter=0;counter<value['airports'].length;counter++){
                allAirportsModel.append({"airport_iata": value['airports'][counter].iata_code, "airport_icao": value['airports'][counter].icao_code,'gmt_offset':value['airports'][counter].gmt_offset, 'dst_rule':value['airports'][counter].dst_rule, 'latitude':value['airports'][counter].latitude, 'longitude':value['airports'][counter].longitude});

                if(value['airports'][counter]['translation'] && value['airports'][counter]['translation']){
                    if(value['airports'][counter]['translation'].airport_name && value['airports'][counter]['translation'].airport_name!=''){
                        allAirportsModel.set(counter,{'airport_name':value['airports'][counter]['translation'].airport_name});
                    }
                    if(value['airports'][counter]['translation'].city_name && value['airports'][counter]['translation'].city_name!=''){
                        allAirportsModel.set(counter,{'city_name':value['airports'][counter]['translation'].city_name});
                    }
                    if(value['airports'][counter]['translation'].state_name && value['airports'][counter]['translation'].state_name!=''){
                        allAirportsModel.set(counter,{'state_name':value['airports'][counter]['translation'].state_name});
                    }
                    if(value['airports'][counter]['translation'].country_name && value['airports'][counter]['translation'].country_name!=''){
                        allAirportsModel.set(counter,{'country_name':value['airports'][counter]['translation'].country_name});
                    }
                    if(value['airports'][counter]['translation'].language_code && value['airports'][counter]['translation'].language_code!=''){
                        allAirportsModel.set(counter,{'language_code':value['airports'][counter]['translation'].language_code});
                    }
                }
            }
        }else {
             core.info('Ans.qml | __loadAnsAllAirportsModel | ERROR | Data not found ');
             __sigModelReady(allAirportsModel,false);
        }
        core.info('Ans.qml | __loadAnsAllAirportsModel | allAirportsModel count : '+allAirportsModel.count);
        isAppActive=false;__sigModelReady(allAirportsModel,true);
    }

    function __loadAnsCountryCodeModel(value){
        core.info('Ans.qml | __loadAnsCountryCodeModel | Called ');
        if(value['country']){
            for(var counter=0;counter<value['country'].length;counter++){
                countryCodeModel.insert(0,{"dial_code": value['country'][counter].dial_code, "country_code": value['country'][counter]['country_code']});
                if(value['country'][counter]['language'] && value['country'][counter]['language'][0])
                    if(value['country'][counter]['language'][0].country_name && value['country'][counter]['language'][0].country_name!=''){
                        countryCodeModel.set(0,{'country_name':value['country'][counter]['language'][0].country_name});
                    }
                    if(value['country'][counter]['language'][0].language_code && value['country'][counter]['language'][0].language_code!=''){
                        countryCodeModel.set(0,{'language_code':value['country'][counter]['language'][0].language_code});
                    }

                }
        }
        countryCodeModel.sort("country_name",true);
        if(Store.countryCodeRange){ isAppActive=false;getCountryCodesByRange( Store.startAlpha, Store.endAlpha,Store.langIso,Store.callback);return true;}
        if(Store.countryNameByCode){ isAppActive=false;getCountryNameByCode( Store.inputCode,Store.langIso,Store.callback);return true;}
        isAppActive=false;__sigModelReady(countryCodeModel,true);
    }


    function __getRemoteData(getParams,requestId){
        getData(core.settings.remotePath+'/ans/'+Store.ansVersion+'/services.php',3,getParams,'','',requestId);
    }

    function __getLocalData(getParams,requestId){getLocalData(getParams,requestId);}

    function resetVariables(){
        core.info('Ans.qml | resetVariables Called ');
        isAppActive = false;
        airportModel.clear();
        airportModelSimple.clear();
        allAirportsModel.clear();
    }

    function __resetInFallbackMode(){
         core.info('Ltn.qml | __resetInFallbackMode | in fallback mode returning blank model');
        __sigModelReady(airportModel,false);
        __sigModelReady(allAirportsModel,false);
    }
    ListModel {id: airportModel}
    SimpleModel {id: airportModelSimple}
    SimpleModel {id: allAirportsModel}
    SimpleModel {id: countryCodeModel}
    SimpleModel {id: countryRangeModel}
    SimpleModel {id: countryNameModel}

    Component.onCompleted: {
        Store.ansVersion=1;
        Store.countryCodeRange = 0; Store.startAlpha = ''; Store.endAlpha = '';Store.langIso = '';
        Store.countryNameByCode = 0; Store.inputCode = '';
        core.info ("Ans.qml | Component.onCompleted | ANS LOAD COMPLETE");
    }
}
