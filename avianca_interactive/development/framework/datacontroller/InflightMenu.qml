import QtQuick 1.1
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

DataLoader {
    id: inflightMenu

    property bool __makeAppRequest:true; // if false requested application is not on avaliable, true FE can make calls to FS
    property bool __catalogReady:false;
    property bool __languageListReady:false;
    property bool __cartSystemStatus: false
    property bool __orderSystemStatus: false
    property string __baseCurrency:'';
    property string __appName:'';
    property string __appIso:'';

    signal __sigModelReady(variant model,bool status,string type,int requestId,string appName);
    signal sigCatalogStatusChanged(string catalog_id,string status);
    signal sigCartStatusChanged(string catalog_id,string status);
    signal sigCartSystemStatusChanged(string status);

    onSigDataReady: {
        if (errCode==0){
            __makeAppRequest=true;
            if(!(requestId==1 || requestId==7 || requestId==15 || requestId==16)){core.debug("InflightMenu.qml | "+__appName+" | WebRequest | responseString: "+responseString);}
            readData(responseString,responseObj,requestId);
        }else {
            if(errCode==203){
                core.info("InflightMenu.qml | "+__appName+" | WebRequest on Error | "+__appName+" files are not on FS. Stop all FS URL")
                __catalogReady=true;
                __makeAppRequest=false;
            }else{core.info("InflightMenu.qml | "+__appName+" | WebRequest on Error | data: "+responseString);}
            isAppActive=false;
            if(Store.storedCallback){__updateCallback(Store.storedCallback);Store.storedCallback='';}
            interactiveStartup.eventHandler(Store.cEventId);
            __sigModelReady(Store.blankModel,false,'',0,__appName);
        }
    }

    onSigLocalDataReady:{
        if(errCode==0){
            core.info("InflightMenu.qml | "+__appName+" | dataLoader | onSigLocalDataReady");
            if(responseObj && responseObj['data'][0]){
                core.info("InflightMenu.qml | "+__appName+" | onSigLocalDataReady | Data ready = True ");
                if(requestId==2){readJsonObject(__appName,requestId, responseObj);}else{readJsonObject(__appName,requestId, responseObj['data'][0]);}
            }else{
                core.info("InflightMenu.qml | "+__appName+" | onSigLocalDataReady | Data ready = False");
                isAppActive=false;
                __sigModelReady(Store.blankModel,false,'',0,__appName);
            }
        }else{
            core.info("InflightMenu.qml | "+__appName+" | dataLoader | onSigLocalDataReady | Error ");
            isAppActive=false;
            if(Store.storedCallback){__updateCallback(Store.storedCallback);Store.storedCallback='';}
            interactiveStartup.eventHandler(Store.cEventId); //if simulation  is on
            __sigModelReady(Store.blankModel,false,'',0,__appName);
        }
    }

    Connections{target: core.pif;
        onSigInflightMenuStatusChanged:{
            if(appName=='shop' && __appName=='shopping' && type=='CATALOG'){updateCatalogModel(catalogId,status);}
            if(appName=='hosp' && __appName=='hospitality' && type=='CATALOG'){updateCatalogModel(catalogId,status);}
            if(appName=='shop' && __appName=='shopping' && type=='CART'){updateCartStatus(catalogId,status);}
            if(appName=='hosp' && __appName=='hospitality' && type=='CART'){updateCartStatus(catalogId,status);}
            if(appName=='shop' && __appName=='shopping' && type=='CARTSYSTEM'){updateCartSystemStatus(status);}
            if(appName=='hosp' && __appName=='hospitality' && type=='CARTSYSTEM'){updateCartSystemStatus(status);}
        }
    }

    //public functions
    function makeApplicationCalls(){return __makeAppRequest;}
    function getCatalogModel(){return Store.catalogModel;}
    function getCategoryModel(){return Store.categoryModel;}
    function getItemModel(){return Store.itemModel;}
    function getItemSynopsisModel(){return Store.itemSynopsis;}
    function getLanguageModel(){return Store.languageModel;}
    function getBaseCurrency(){return __baseCurrency;}
    function getSubCategoryModel(level){if(Store.subCategoryModel && Store.subCategoryModel[level]){return Store.subCategoryModel[level]}else{return Store.blankModel}}
    function getQuantityInInventory(item_id){return (Store.qtyInCart && Store.qtyInCart[item_id])?Store.qtyInCart[item_id]:0;}
    function getQuantityInCart(item_id,catalog_id){return (Store.cartArray && Store.cartArray[catalog_id] && Store.cartArray[catalog_id][item_id])?Store.cartArray[catalog_id][item_id].quantity:0;}

    function fetchData(iso,callback){
       isAppActive = false;
       Store.storedCallback = '';
       if(!makeApplicationCalls()){
            core.info('InflightMenu.qml | '+__appName+' | fetchData |  makeApplicationCalls : '+makeApplicationCalls()+' | Cannot Make FS Calls');
            __updateCallback(callback);
            __sigModelReady(Store.blankModel,false,'catalog',7,__appName);
            return false;
        }else if(__appIso.toLowerCase()==iso.toLowerCase()){
            if(Store.catalogModel.count>0){
                core.info('InflightMenu.qml | '+__appName+' | fetchData |  appIso : '+__appIso+' | passed iso : '+iso+' | No lang change Returning Model');
                __updateCallback(callback);
                __sigModelReady(Store.catalogModel,true,'catalog',7,__appName);
            }else {
                Store.storedCallback = callback;
                getAllCatalogs(iso);
            }
            return false;
        }else {
            Store.storedCallback = callback;
            if(Store.loadCatalogType=='all'){
                if(Store.catalogNonDefaultLanguageDataObj[iso.toUpperCase()]){
                    core.info('InflightMenu.qml | '+__appName+' | fetchData |  Found lang '+iso+' | Calling __changeNonDefaultLanguageCatalog ');
                    __appIso=iso;
                    __changeNonDefaultLanguageCatalog(iso);reloadData();
                    checkPreload();return true;
                }else{
                    core.info('InflightMenu.qml | '+__appName+' | fetchData | Language not found '+iso+' | Calling getAllCatalogs ');
                    getAllCatalogs(iso);
                }
            }else if(Store.loadCatalogType=='onLanguageChange'){
                core.info('InflightMenu.qml | '+__appName+' | fetchData | lang '+iso+' | Calling __getLanguageData ');
                __appIso=iso;
                Store.nonDefaultLanguageIso=iso.toUpperCase();
                __getLanguageData(Store.nonDefaultLanguageIso);reloadData();
            }else {
                core.info('InflightMenu.qml | '+__appName+' | fetchData | Calling getAllCatalogs ');
                getAllCatalogs(iso);
            }
        }
    }

    function getAllCatalogs(iso,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getAllCatalogs | App is already Acitve current Reuqest is droped.... ')
            return false;
        }else if(__appIso.toLowerCase()==iso.toLowerCase()){
            core.info('InflightMenu.qml | '+__appName+' | getAllCatalogs |  appIso : '+__appIso+' | passed iso : '+iso+' | No lang change ')
        }

        isAppActive=true;__updateCallback(callback);
        core.info('InflightMenu.qml | '+__appName+' | getAllCatalogs | Processing ISO: '+iso);
        var requestId=4;var getParams='';__appIso=iso;
        resetVariables();
        getParams="function=isApplicationOn&output_format="+Store.catalogFields['output_format'];
        core.info('InflightMenu.qml | '+__appName+' | isApplicationOn | requestId = '+requestId+ ' | getParams: '+getParams);
        sendRequest(getParams,requestId);
    }

    function getNextList(value,catalog_id,requestCategoryLevel,categoryId,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getNextList | App is already Acitve current Reuqest is droped');
            return false;
        }else{isAppActive=true;}

        __updateCallback(callback);
        core.info('InflightMenu.qml | '+__appName+' | getNextList | requestCategoryLevel='+requestCategoryLevel + ' |  categoryId='+categoryId + ' | catalog_id='+catalog_id +' | value[category_node_id]='+value['category_node_id'] + ' value[category_node_child_nodes] ='+ value['category_node_child_nodes']);
        requestCategoryLevel = requestCategoryLevel?requestCategoryLevel:1
        if(value['category_node'] != undefined || value['category_node_child_nodes'] != null){
            switch(requestCategoryLevel){
                case 1:{
                    var categoryObject = [];
                    categoryObject['category_node'] = [];
                    Store.categoryObject = {};
                    Store.categoryObject=value  //only once
                    for(var i in value['category_node']){
                        if(value['category_node'][i]['category_node_parent_id'] == null || value['category_node'][i]['category_node_parent_id']==''){
                            categoryObject['category_node'].push(value['category_node'][i])
                        }
                    }
                    Store.categoryModel.source = categoryObject['category_node'];
                    isAppActive=false;
                    __sigModelReady(Store.categoryModel,true,'category','',__appName);
                    break;
                }default:{
                    var categoryObject = [];
                    categoryObject['category_node'] = [];
                    for(var i in Store.categoryObject['category_node']){
                        if(Store.categoryObject['category_node'][i]['category_node_parent_id'] == categoryId ){
                            //if(Store.categoryObject['category_node'][i]['item_node']){Store.itemModel.source = Store.categoryObject['category_node'][i]['item_node'];} need to check this
                            categoryObject['category_node'].push(Store.categoryObject['category_node'][i])
                        }
                    }
                    var model = getSubCategory((requestCategoryLevel-1));
                    model.source = categoryObject['category_node'];
                    isAppActive=false;
                    __sigModelReady(model,true,'categoryLevel'+requestCategoryLevel,'',__appName);
                    break;
              }
          }
        }else if(value['item_node'] != undefined){
            Store.itemModel.source = value['item_node'];
            core.info('InflightMenu.qml | '+__appName+' | getNextList | Reading Item List : item');
            if(catalog_id && __cartSystemStatus){
                var tmpItemStr = '';
                for(var i in value['item_node']){
                    tmpItemStr += tmpItemStr!=''?","+value['item_node'][i]['item_id']:value['item_node'][i]['item_id'];
                    if(!Store.qtyInCart[value['item_node'][i]['item_id']]){
                        Store.qtyInCart[value['item_node'][i]['item_id']]=0;
                    }
                }
                isAppActive=false;
                core.info('InflightMenu.qml | '+__appName+' | getNextList | check inventory | catalog_id :'+catalog_id+' | tmpItemStr : '+tmpItemStr);
                __getInventoryByItemId(catalog_id,tmpItemStr)
            }else {
                core.info('InflightMenu.qml | '+__appName+' | getNextList | check Inverntory NOT CALLED ');
            }
            isAppActive=false;
            __sigModelReady(Store.itemModel,true,'item','',__appName);
            return true;
        }else {
            core.info('InflightMenu.qml | '+__appName+' | getNextList | Reading Item Synopsis');
            isAppActive=false;
            __sigModelReady(Store.blankModel,true,'itemSynopsis','',__appName);
            return 'true';
        }
    }

    function getItemDetailById(itemId,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getItemDetailById | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        __updateCallback(callback);
        var requestId=2;var getParams ='';

        if(__appName=='shopping'){getParams= Store.metadataPath + "/shopping/"+itemId+"_"+__appIso.toUpperCase()+"_.qt.json";
        }else if(__appName=='hospitality'){getParams= Store.metadataPath +"/hospitality/"+itemId+"_"+__appIso.toUpperCase()+"_.qt.json";}

        core.info('InflightMenu.qml | '+__appName+' | getItemDetailById | getParams: '+getParams);
        __getItemSynopsisData(getParams,requestId);
    }

    // public function for cart/ordering
    function modifyCartQuantity(iso,submit_order,catalog_id,item_id_list, quantity_list, mixer_id_list,mixer_item_id_lists, mixer_item_quantity_list,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | modifyCartQuantity | App is already Acitve current Reuqest is droped.... ')
            return false;
        }else{isAppActive=true;}

        __updateCallback(callback);
        var getParams='';
        core.info('InflightMenu.qml | '+__appName+' | modifyCartQuantity | catalog_id : '+catalog_id+' | item_id_list : '+item_id_list+'| quantity_list : '+quantity_list);
        var requestId=11;

        getParams="function=modifyCartQuantity&cabin_class="+Store.cabinClass[core.pif.getCabinClassName()]+"&language_code="+iso+"&seat_id="+core.pif.getSeatNumber()+"&catalog_id="+catalog_id+"&item_id_list="+item_id_list+"&quantity_list="+quantity_list+"&submit_order="+submit_order+"&output_format="+Store.catalogFields['output_format']
        if(__appName=='hospitality'){
            mixer_id_list = mixer_id_list?mixer_id_list:''
            mixer_id_list = mixer_id_list?mixer_id_list:''
            mixer_item_quantity_list = mixer_item_quantity_list?mixer_item_quantity_list:''

            getParams+="&mixer_id_list="+mixer_id_list+"&mixer_item_id_lists="+mixer_id_list+"&mixer_item_quantity_list="+mixer_item_quantity_list;
        }

        sendRequest(getParams,requestId)
    }

    //depracated use modify cart directly with only current item to be updated
    function addTocart(iso,submit_order,catalog_id,item_id, quantity, mixer_id_list,mixer_item_id_lists, mixer_item_quantity_list,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | addTocart | App is already Acitve current Reuqest is droped.... ')
            return false;
        }else{isAppActive=true;}

        __updateCallback(callback);
        core.info('InflightMenu.qml | '+__appName+' | addTocart | catalog_id : '+catalog_id+' | item_id : '+item_id+'| quantity : '+quantity);
        var requestId=11;

        Store.isAddToCart = 1

        if(!catalog_id || !item_id){
            core.info('InflightMenu.qml | '+__appName+' | addTocart | ERROR | Catalog id or item_id not set');
            isAppActive=false
            return false;
        }

        var tmpItemId  = ''
        var tmpItemQty = ''

        if(Store.cartArray[catalog_id]){
            if(Store.cartArray[catalog_id][item_id] && quantity==0){
                delete Store.cartArray[catalog_id][item_id]
            }else if(Store.cartArray[catalog_id][item_id]){
                Store.cartArray[catalog_id][item_id] = {'quantity':quantity}
            }else {
                Store.cartArray[catalog_id][item_id] = new Object
                Store.cartArray[catalog_id][item_id] = {'quantity':quantity}
            }

            //prepare for modify cart api
            for(var itm in Store.cartArray[catalog_id]){
                if(Store.cartArray[catalog_id][itm].quantity!='undefined' && Store.cartArray[catalog_id][itm].quantity!=0){
                    tmpItemId  += tmpItemId!=''?","+itm:itm
                    tmpItemQty += tmpItemQty!=''?","+Store.cartArray[catalog_id][itm].quantity:Store.cartArray[catalog_id][itm].quantity
                }
            }//end
        }else {
            core.info('InflightMenu.qml | '+__appName+' | addTocart | catalog or item not set | catalog_id : '+catalog_id+' | item_id : '+item_id+'| quantity : '+quantity);
            return false;
        }
        isAppActive = false;
        modifyCartQuantity(iso,submit_order,catalog_id,tmpItemId, tmpItemQty, mixer_id_list,mixer_item_id_lists, mixer_item_quantity_list,callback);
    }

    function emptyCart(iso,catalog_id,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | emptyCart | App is already Acitve current Reuqest is droped.... ')
            return false;
        }else{isAppActive=true;}

        if(Store.cartArray[catalog_id]){
            var tmpItemId  = '';
			var tmpItemQty = '';
			var mixer_id_list=''; var mixer_item_id_lists='';var  mixer_item_quantity_list='';
            for(var itm in Store.cartArray[catalog_id]){
                if(Store.cartArray[catalog_id][itm].quantity!='undefined' && Store.cartArray[catalog_id][itm].quantity!=0){
                    tmpItemId  += tmpItemId!=''?","+itm:itm
					tmpItemQty += tmpItemQty!=''?",0":"0"
                }
            }
            core.info('InflightMenu.qml | '+__appName+' | emptyCart | catalog_id : '+catalog_id+" |  tmpItemId: "+tmpItemId);            
			var submit_order=0;
	        isAppActive = false;
	        modifyCartQuantity(iso,submit_order,catalog_id,tmpItemId, tmpItemQty, mixer_id_list,mixer_item_id_lists, mixer_item_quantity_list,callback)
        }else {
            core.info('InflightMenu.qml | '+__appName+' | emptyCart | catalog_id : '+catalog_id+" |  Cart Not Found ");
        }

    }

	//depracated use modify cart directly with only current item to be updated
    function removeFromCart(iso,catalog_id,item_id,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | removeFromCart | App is already Acitve current Reuqest is droped.... ')
            return false;
        }else{isAppActive=true;}

        __updateCallback(callback);
        core.info('InflightMenu.qml | '+__appName+' | removeFromCart | catalog_id : '+catalog_id);
        var requestId=11;

        Store.isAddToCart = 0

        if(!catalog_id || !item_id){
            core.info('InflightMenu.qml | '+__appName+' | removeFromCart | ERROR | Catalog id or item_id not set');
            isAppActive=false
            return
        }
		
        item_id = String(item_id)
        var tmpItemQty = ''
        var mixer_id_list=''; var mixer_item_id_lists='';var  mixer_item_quantity_list='';

        if(Store.cartArray[catalog_id]){
            var tmpItemArray = item_id.split(',');
            for(x in tmpItemArray){
                if(Store.cartArray[catalog_id][tmpItemArray[x]]){
                    //delete Store.cartArray[catalog_id][tmpItemArray[x]]
                    tmpItemQty += tmpItemQty!=''?",0":"0"
                }
            }
            //prepare for modify cart api
//			 var tmpItemId  = ''
//            for(var itm in Store.cartArray[catalog_id]){
//                if(Store.cartArray[catalog_id][itm].quantity!='undefined' && Store.cartArray[catalog_id][itm].quantity!=0){
//                    tmpItemId  += tmpItemId!=''?","+itm:itm
//                    tmpItemQty += tmpItemQty!=''?","+Store.cartArray[catalog_id][itm].quantity:Store.cartArray[catalog_id][itm].quantity
//                }
//            }//end
        }else {
            core.info('InflightMenu.qml | '+__appName+' | removeFromCart | catalog or item not set | catalog_id : '+catalog_id+' | item_id : '+item_id+'| quantity : '+quantity);
            return false;
        }

        var submit_order=0;
        isAppActive = false;
        modifyCartQuantity(iso,submit_order,catalog_id,item_id, tmpItemQty, mixer_id_list,mixer_item_id_lists, mixer_item_quantity_list,callback)
    }


    function getCart(iso,catalog_id,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getCart | App is already Acitve current Reuqest is droped.... ')
            return false;
        }

        __updateCallback(callback);
        isAppActive=true;var getParams='';
        core.info('InflightMenu.qml | '+__appName+' | getCart | catalog_id : '+catalog_id);
        var requestId=12;

        if(!catalog_id){
            core.info('InflightMenu.qml | '+__appName+' | getCart | ERROR | Catalog_id not set ');
            isAppActive=false;
            return false;
        }

        getParams="function=getCart&cabin_class="+Store.cabinClass[core.pif.getCabinClassName()]+"&language_code="+iso+"&seat_id="+core.pif.getSeatNumber()+"&catalog_id="+catalog_id+"&show_catalog_statuses=1&output_format="+Store.catalogFields['output_format']
        sendRequest(getParams,requestId)
    }

    function submitOrder(iso,catalog_id,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | submitOrder | App is already Acitve current Reuqest is droped.... ')
            return false;
        }else{isAppActive=true;}

        __updateCallback(callback);
        core.info('InflightMenu.qml | '+__appName+' | submitOrder | catalog_id : '+catalog_id);
        var requestId=11;

        if(!catalog_id){
            core.info('InflightMenu.qml | '+__appName+' | submitOrder | ERROR | Catalog_id not set ');
            isAppActive=false
            return false;
        }

        var tmpItemId  = ''
        var tmpItemQty = ''

        var mixer_id_list=''; var mixer_item_id_lists='';var  mixer_item_quantity_list='';

        if(Store.cartArray[catalog_id]){
            //prepare for modify cart api
            for(var itm in Store.cartArray[catalog_id]){
                core.info("InflightMenu.qml | "+__appName+" | submitOrder | cartArray["+catalog_id+"]["+itm+"] : "+Store.cartArray[catalog_id][itm].quantity)
                if(Store.cartArray[catalog_id][itm].quantity!='undefined' && Store.cartArray[catalog_id][itm].quantity!=0){
                    tmpItemId  += tmpItemId!=''?","+itm:itm
                    tmpItemQty += tmpItemQty!=''?","+Store.cartArray[catalog_id][itm].quantity:Store.cartArray[catalog_id][itm].quantity
                }
            }
        }else {
            core.info('InflightMenu.qml | '+__appName+' | submitOrder | catalog or item not set in preload | catalog_id : '+catalog_id);
            return
        }

        var submit_order = 1;
        isAppActive = false;
        modifyCartQuantity(iso,submit_order,catalog_id,tmpItemId, tmpItemQty, mixer_id_list,mixer_item_id_lists, mixer_item_quantity_list,callback)
    }

    function getOrder(iso,callback){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getOrder | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        __updateCallback(callback);var getParams='';
        core.info('InflightMenu.qml | '+__appName+' | getOrder | iso : '+iso);
        var requestId=13;

        getParams="function=getAllOrdersPerSeat&seat_id="+core.pif.getSeatNumber()+"&cabin_class="+Store.cabinClass[core.pif.getCabinClassName()]+"&language_code="+iso+"&currency_codes="+Store.catalogFields['currency_codes']+"&output_format="+Store.catalogFields['output_format'];
        sendRequest(getParams,requestId);
        core.info('InflightMenu.qml | '+__appName+' | getOrder | getParams : '+getParams);
    }

    function getOrderItem(value){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getOrderItem | App is already Acitve current Reuqest is droped');
            return false;
        }else{isAppActive=true;}

        Store.orderItemModel.source = value['order_item'];
        core.info('InflightMenu.qml | '+__appName+' | getOrderItem | orderItemModel.count : '+Store.orderItemModel.count);

        isAppActive=false
        return 'orderItem';
    }

    function changeLang(langIso,arr,callback){
        core.info('InflightMenu.qml | '+__appName+' | changeLang | appName='+__appName+' LangIso='+langIso+'arr='+arr);

        Store.paramsArray = new Array
        for(var a in arr){
            core.debug("InflightMenu.qml | "+__appName+" | changeLang | appName="+__appName+" | arr["+a+"]=" +arr[a]);
            Store.paramsArray[a]=arr[a]

            if(a=='catalog' && arr['catalog']!='-1'){Store.paramsArray['catalog'] = Store.catalogModel.getValue(arr[a],"catalog_id");}
            if(a=='category' && arr['category']!='-1' && arr['category']!= undefined){Store.paramsArray['category'] = Store.categoryModel.getValue(arr[a],"category_node_id");}
            if(a=='categoryLevel2' && arr['categoryLevel2']!='-1' && arr['categoryLevel2']!=undefined){Store.paramsArray['categoryLevel2'] = getSubCategory(1).getValue(arr[a],"category_node_id");}
            if(a=='categoryLevel3' && arr['categoryLevel3']!='-1'&& arr['categoryLevel3']!=undefined){Store.paramsArray['categoryLevel3'] = getSubCategory(2).getValue(arr[a],"category_node_id");}
            if(a=='categoryLevel4' && arr['categoryLevel4']!='-1'&& arr['categoryLevel4']!=undefined){Store.paramsArray['categoryLevel4'] = getSubCategory(3).getValue(arr[a],"category_node_id");}
            if(a=='item' && arr['item']!='-1'&& arr['item']!=undefined){Store.paramsArray['item'] = getSubCategory(4).getValue(arr[a],"item_id");}
        }
        //resetVariables();
        Store.changeLang = true;
        //getAllCatalogs(langIso,callback);
        fetchData(langIso,callback);
    }

    //Private functions appConfig
    function __setAppAsShopping(){__appName='shopping';}
    function __setAppAsHospitality(){__appName='hospitality';}

    function __updateCallback(callback){
        if (Store.callback){__sigModelReady.disconnect(Store.callback);}
        if (callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }

    function __disconnectCallback(){if(Store.callback){__sigModelReady.disconnect(Store.callback);}}

    function __getRemoteData(getParams,requestId){
        var url='';
        if(requestId==1 || requestId==15 || requestId==16){
            url = core.settings.remotePath+'/SWA/catalog-int/'+getParams;getParams='';
        }else if(__appName == 'shopping'){
            url = core.settings.remotePath+'/SWA/catalog/api/ShoppingController.php'
        }
        else if(__appName == 'hospitality'){
            url = core.settings.remotePath+'/SWA/catalog/api/HospitalityController.php'
        }
        getData(url,3,getParams,'','',requestId);
    }

    function __getLocalData(getParams,requestId){getLocalData(getParams,requestId);}

    function __getItemSynopsisData(getParams,requestId){getLocalDataWithoutDelay(getParams,requestId);}

    function getLanguageList(){
        if(__languageListReady==true){
            core.info('InflightMenu.qml | '+__appName+' | getLanguageList | Lang List is loaded current Reuqest is droped.... ')
            return false;
        }else if(isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getLanguageList | App is already Acitve current Reuqest is droped.... ')
            return false;
        }else{isAppActive=true;}

        __disconnectCallback();
        core.info('InflightMenu.qml | '+__appName+' | getLanguageList | Called ');
        var requestId=6;var getParams='';
        core.info('InflightMenu.qml | '+__appName+' | getLanguageList | requestId = '+requestId+ ' | function=getAllLanguages&output_format = '+Store.catalogFields['output_format']);
        getParams="function=getAllLanguages&output_format="+Store.catalogFields['output_format'];
        sendRequest(getParams,requestId)
    }

    function getCatalogsStatus(catalogList) {
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getCatalogsStatus | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var getParams='';var requestId=3;
        getParams="function=getCatalogsStatus&cabin_class_list="+Store.cabinClass[core.pif.getCabinClassName()]+"&catalog_id_list="+catalogList+"&output_format="+Store.catalogFields['output_format'];
        core.info("InflightMenu.qml | "+__appName+" | getCatalogsStatus | getParams: "+getParams);
        sendRequest(getParams,requestId)
    }

    function getAllCatalogsStatus(){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getAllCatalogsStatus | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var getParams='';var requestId=7;
        core.info('InflightMenu.qml | '+__appName+' | getAllCatalogsStatus | output_format = '+Store.catalogFields['output_format']);
        getParams="function=getAllEnabledCatalogStatuses&output_format="+Store.catalogFields['output_format'];
        sendRequest(getParams,requestId)
    }

    function getIsAppOn(){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getIsAppOn | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var requestId=5;var getParams='';
        if(__appName=='shopping'){
            core.info('InflightMenu.qml | '+__appName+' | isShoppingOn | requestId = '+requestId);
            getParams="function=isShoppingOn&output_format="+Store.catalogFields['output_format'];
        }else if(__appName=='hospitality'){
            core.info('InflightMenu.qml | '+__appName+' | isHospitalityOn | requestId = '+requestId);
            getParams="function=isHospitalityOn&output_format="+Store.catalogFields['output_format'];
        }
        sendRequest(getParams,requestId);
    }

    function getCatalogData(){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getCatalogData | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var requestId=1;var getParams='';
        if(__appName=='shopping'){
            getParams = "SHOP_"+Store.cabinClass[core.pif.getCabinClassName()]+"_"+__appIso.toUpperCase()+".json";
        }else if(__appName=='hospitality'){
            getParams = "HOSP_"+Store.cabinClass[core.pif.getCabinClassName()]+"_"+__appIso.toUpperCase()+".json";
        }
        core.info('InflightMenu.qml | '+__appName+' | getCatalogData | '+getParams);
        sendRequest(getParams,requestId)
    }

    function __getLanguageData(iso){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | __getLanguageData | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var requestId=16;var getParams='';
        if(__appName=='shopping'){
            getParams = "SHOP_"+Store.cabinClass[core.pif.getCabinClassName()]+"_"+iso.toUpperCase()+".json";
        }else if(__appName=='hospitality'){
            getParams = "HOSP_"+Store.cabinClass[core.pif.getCabinClassName()]+"_"+iso.toUpperCase()+".json";
        }
        core.info('InflightMenu.qml | '+__appName+' | getCatalogData | '+getParams);
        sendRequest(getParams,requestId)
    }

    function __getNonDefaultLanguageCatalogs(){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | __getNonDefaultLanguageCatalogs | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var requestId=15;var getParams='';var tmpIsNonDefaultLanguagePresent=false;
        for(var cIso in Store.fetchedNonDefaultLanguageArr){
            tmpIsNonDefaultLanguagePresent=true;
            if(cIso==Store.defaultLanguage){Store.fetchedNonDefaultLanguageArr[cIso]=1;}
            if(Store.fetchedNonDefaultLanguageArr[cIso]==0 && cIso!=Store.defaultLanguage){
                Store.nonDefaultLanguageIso=cIso;
                if(__appName=='shopping'){
                    getParams = "SHOP_"+Store.cabinClass[core.pif.getCabinClassName()]+"_"+cIso.toUpperCase()+".json";
                }else if(__appName=='hospitality'){
                    getParams = "HOSP_"+Store.cabinClass[core.pif.getCabinClassName()]+"_"+cIso.toUpperCase()+".json";
                }
                Store.fetchedNonDefaultLanguageArr[cIso]=1;
                core.info('InflightMenu.qml | '+__appName+' | __getNonDefaultLanguageCatalogs | '+getParams);
                sendRequest(getParams,requestId);
                break;
            }
        }
        if(!tmpIsNonDefaultLanguagePresent){Store.fetchedNonDefaultLanguageData=true;isAppActive=false;checkPreload();}
    }

    function __changeNonDefaultLanguageCatalog(iso){
        core.info('InflightMenu.qml | '+__appName+' | __changeNonDefaultLanguageCatalog | Processing Iso: '+iso);
        Store.__catalogListObject= []
        Store.__catalogListObject['catalog'] = []
        Store.__catalogListObject['catalog'] =  Store.catalogNonDefaultLanguageDataObj[iso.toUpperCase()];
        Store.catalogListObject= []
        Store.catalogListObject['catalog'] = []
        Store.catalogListObject['catalog'] =  Store.catalogNonDefaultLanguageDataObj[iso.toUpperCase()];
        Store.enabledCatalogObject = [];
        Store.enabledCatalogObject['catalog'] = [];

        if(Store.catalogBlockType==1){
            for (var i in Store.catalogListObject['catalog']){
                if(Store.catalogStatusArray[Store.catalogListObject['catalog'][i]['catalog_id']]!=1){
                    core.info('InflightMenu.qml | '+__appName+' | __changeNonDefaultLanguageCatalog | Blocking catalog: '+Store.catalogListObject['catalog'][i]['catalog_id']);
                    //Store.enabledCatalogObject['catalog'].splice(i,1);
                }else{
                    //core.info('InflightMenu.qml | '+__appName+' | __changeNonDefaultLanguageCatalog | Not Blocking catalog: '+Store.catalogListObject['catalog'][i]['catalog_id']);
                    Store.enabledCatalogObject['catalog'].push(Store.catalogListObject['catalog'][i]);
                }
            }
        }
        Store.catalogModel.source =  Store.enabledCatalogObject['catalog'];
        core.info('InflightMenu.qml | '+__appName+' | __changeNonDefaultLanguageCatalog | Done Processing Iso: '+iso);
        return true;
    }

    function getCartStatus() {
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getCartStatus | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var requestId=8;var getParams='';
        core.info('InflightMenu.qml | '+__appName+' | getCartStatus | Called ');
        getParams="function=isCartSystemOn&output_format="+Store.catalogFields['output_format'];
        sendRequest(getParams,requestId)
    }

    function getAllCartsPerSeat(iso){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getAllCartsPerSeat | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var requestId=14;var getParams='';
        core.info('InflightMenu.qml | '+__appName+' | getAllCartsPerSeat');
        getParams="function=getAllCartsPerSeat&language_code="+iso+"&seat_id="+core.pif.getSeatNumber()+"&sort_field=&sort_order=&output_format="+Store.catalogFields['output_format'];
        sendRequest(getParams,requestId)
    }

    function getOrderStatus() {
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | getOrderStatus | App is already Acitve current Reuqest is droped.... ');
            return false;
        }else{isAppActive=true;}

        var requestId=9;var getParams='';
        core.info('InflightMenu.qml | '+__appName+' | getOrderStatus | Called ');
        getParams="function=isOrderSystemOn&output_format="+Store.catalogFields['output_format'];
        sendRequest(getParams,requestId);
    }

    function readJsonObject(appName,requestId,val){
        if(requestId==0){
            core.info('InflightMenu.qml | '+__appName+' | readJsonObject | Data ready = False');
            if(Store.storedCallback){__updateCallback(Store.storedCallback);Store.storedCallback='';}
            __sigModelReady(Store.blankModel,false,'',0,appName);
            interactiveStartup.eventHandler(Store.cEventId);
            isAppActive=false;
            return false;
        }
        core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId: '+requestId);
        var status = ''
        if(requestId==1){
            Store.__catalogListObject= []
            Store.__catalogListObject['catalog'] = []

            for(var i in val['catalog']){Store.__catalogListObject['catalog'][i] = val['catalog'][i];}

            Store.catalogListObject= []
            Store.catalogListObject['catalog'] = []

            for(var i in val['catalog']){Store.catalogListObject['catalog'][i] = val['catalog'][i];}

            if(Store.loadCatalogType=='all'){Store.catalogNonDefaultLanguageDataObj[__appIso.toUpperCase()]= val['catalog'];}
            if(val['base_currency_code']){
                __baseCurrency = val['base_currency_code'];
            }

            if(Store.catalogBlockType!=1)
                __catalogReady=true;

            isAppActive=false;
            getAllCatalogsStatus() //load id 7 will be returned with catlog status
            return true;
        }else if(requestId==15){
            core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 15 | appName : '+appName+' | cIso: '+Store.nonDefaultLanguageIso);
            Store.catalogNonDefaultLanguageDataObj[Store.nonDefaultLanguageIso]=val['catalog'];
            Store.fetchedNonDefaultLanguageData=true;
            for(var cIso in Store.fetchedNonDefaultLanguageArr){
                if(Store.fetchedNonDefaultLanguageArr[cIso]==0){
                    Store.fetchedNonDefaultLanguageData=false;
                }
            }
            isAppActive=false;checkPreload();
            return true;
        }else if(requestId==16){
            core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 16 | appName : '+appName);
            Store.catalogNonDefaultLanguageDataObj=[];
            Store.catalogNonDefaultLanguageDataObj[Store.nonDefaultLanguageIso]=val['catalog'];
            __changeNonDefaultLanguageCatalog(Store.nonDefaultLanguageIso);
            isAppActive=false;checkPreload();
            return true;
        }else if(requestId==2){
            core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 2 | appName : '+appName)
            Store.itemSynopsis.source =  val['data'];
            isAppActive=false;
            __sigModelReady(Store.itemSynopsis,true,'itemSynopsis',requestId,appName);
            return true;
        }else if(requestId==3){
            core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 3 | appName : '+appName)
            Store.catalogStatus.source =  val ['catalog_status']
            isAppActive=false;
            return true;
        }else if(requestId==4){
            if( val['application_status'] == 1){
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | appName: '+appName+'|  application_status = 1');
                isAppActive=false;getIsAppOn();
                return true;
            }else{
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | application_status = OFF');
                readJsonObject(appName, 0, val);
            }
        }else if(requestId==5){
            if(appName == 'shopping'){
                if(val['shopping_status'] == 1){
                    isAppActive=false;getCatalogData(appName);return true;
                }else{
                    core.info('InflightMenu.qml | '+__appName+' | WebRequest onResponse | shopping_status  = OFF');
                    readJsonObject(appName, 0, val);
                }
            }else if(appName == 'hospitality'){
                if(val['hospitality_status'] == 1){
                    isAppActive=false;getCatalogData(appName);return true;
                }
                else{
                    core.info('InflightMenu.qml | '+__appName+' | WebRequest onResponse | hospitality_status  = OFF');
                    readJsonObject(appName, 0, val);
                }
            }
        }else if(requestId==6){
            Store.languageModel.source =  val['language'];
            for(var i=0;i<val['language'].length;i++){
                var currLangIso=val['language'][i]['language_code'].toUpperCase();
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 6 | Found Language iso '+currLangIso);
                if(currLangIso!=Store.defaultLanguage.toUpperCase()){Store.fetchedNonDefaultLanguageArr[currLangIso]=0;}
            }
            __languageListReady=true;isAppActive=false;
            checkPreload();return true;
        }else if(requestId==7){
            Store.catalogStatusArray = [];
            if(Store.preloadCartSystem){Store.cartStatus = [];Store.cartArray = [];}
            var classActivted = 0;
            for(var i in Store.__catalogListObject['catalog']){Store.catalogStatusArray[Store.__catalogListObject['catalog'][i]['catalog_id']]=0;}//default block
            if(val['catalog_statuses_status']!=0){
                for (var i in val['catalog_status']){
                    if(val['catalog_status'][i]['cabin_class'].toLowerCase() == Store.cabinClass[core.pif.getCabinClassName()].toLowerCase()){
                        Store.catalogStatusArray[val['catalog_status'][i]['catalog_id']] = val['catalog_status'][i]['status'];
                        if(Store.preloadCartSystem){
                            Store.cartStatus[val['catalog_status'][i]['catalog_id']] = val['catalog_status'][i]['cart_status']==1?1:0;
                            Store.cartArray[val['catalog_status'][i]['catalog_id']] = [];
                        }
                        classActivted=1
                    }
                }
                if(classActivted==1){
                    Store.enabledCatalogObject = [];
                    Store.enabledCatalogObject['catalog'] = [];                  
                    if(Store.catalogBlockType==1){
                        var counter=0;
                        for (i in Store.catalogListObject['catalog']){
                            if(Store.catalogStatusArray[Store.catalogListObject['catalog'][i]['catalog_id']]==1){
                                Store.enabledCatalogObject['catalog'][counter] = Store.catalogListObject['catalog'][i];
                                counter++;
                            }
                        }
                    }
                    Store.catalogModel.source =  Store.enabledCatalogObject['catalog'];
                }
           }
            __catalogReady=true;
            core.info("InflightMenu.qml | "+__appName+" | readJsonObject | requestId = 7 | catalogModel.count: "+Store.catalogModel.count);

            isAppActive=false;
            if(val['catalog_statuses_status']==0 || classActivted==0){
                core.info("InflightMenu.qml | "+__appName+" | readJsonObject = 7 | ERROR | for appName="+appName);
                __sigModelReady(Store.blankModel,false,'catalog',requestId,appName);
                setIsCompReady();
                return false;
            }

            checkPreload();
            return true;
        }else if(requestId==8){
            if(appName=='shopping'){
                if(val['shopping_cart_system_status']==1){
                    __cartSystemStatus = true
                }
                Store.tmpPreloadCatalogCart = true;
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 8 | cartSystemStatus : '+__cartSystemStatus);
            }else if(appName=='hospitality'){
                if(val['hospitality_cart_system_status']==1){
                    __cartSystemStatus = true
                }
                Store.tmpPreloadCatalogCart = true;
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 8 | cartSystemStatus : '+__cartSystemStatus);
            }
            isAppActive=false;
            checkPreload();return true;
        }else if(requestId==9){
            if(appName=='shopping'){
                if(val['shopping_order_system_status']==1){
                    __orderSystemStatus = true
                }
                Store.tmpPreloadOrder = true;
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 9 | orderSystemStatus : '+__orderSystemStatus);
            }else if(appName=='hospitality'){
                if(val['hospitalityitality_order_system_status']==1){
                    __orderSystemStatus = true
                }
                Store.tmpPreloadOrder = true;
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 9 | orderSystemStatus : '+__orderSystemStatus);
            }
            isAppActive=false;
            checkPreload();return true;
        }else if(requestId==10){
            if(val['item']){
                for (var i in val['item']){
                    core.debug('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 10 | item_id : '+val['item'][i]['item_id']+' | quantity_available : '+val['item'][i]['quantity_available']);
                    Store.qtyInCart[val['item'][i]['item_id']] = val['item'][i]['quantity_available']>0?val['item'][i]['quantity_available']:0;
                }
            }
        }else if(requestId==11){
            Store.cartModel.source='[]';Store.orderModel.source='[]';Store.cartInventoryObject='[]';
            if(val['cart_item']){
                Store.cartModel.source = val['cart_item'];
            }else if(val['order_item']){
                Store.orderModel.source = val;
            }

            Store.cartArray[val['catalog_id']] = [];
            for(var i=0;i<=Store.cartModel.count;i++){
                Store.cartArray[val['catalog_id']][Store.cartModel.getValue(i,'item_id')] = {'quantity':Store.cartModel.getValue(i,'quantity')};
            }

            if(val['inventory']){
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 11 | modify cart failed inventory tag found');
                Store.cartInventoryObject = val['inventory'];
                for(var i in val['inventory']){
                    core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 11 | modify cart failed | item_id : '+val['inventory'][i]['item']['item_id']+' | quantity_available : '+val['inventory'][i]['item']['quantity_available']+' | quantity_requested : '+val['inventory'][i]['item']['quantity_requested']);
                    Store.qtyInCart[val['inventory'][i]['item']['item_id']]=val['inventory'][i]['item']['quantity_available'];
                }
                status = 'failed';
            }else if(Store.isAddToCart && !val['cart_item']){
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 11 | item not added');
                status = 'failed';
            }else {
                core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 11 | modify cart success ');
                status = 'success';
            }

            isAppActive=false;if(val['order_item']){__sigModelReady(Store.orderModel,status,'order',requestId,appName);}else{__sigModelReady(Store.cartModel,status,'cart',requestId,appName);}

        }else if(requestId==12){
            Store.cartModel.source = val['cart_item']?val['cart_item']:'[]';
            core.info('InflightMenu.qml | '+__appName+' | readJsonObject | requestId = 12 | cartModel.count '+Store.cartModel.count);

            //poulate the internal array
            Store.cartArray[val['catalog_id']] = [];
            for(var i=0;i<=Store.cartModel.count;i++){
                Store.cartArray[val['catalog_id']][Store.cartModel.getValue(i,'item_id')] = {'quantity':Store.cartModel.getValue(i,'quantity')};
            }

            isAppActive=false;__sigModelReady(Store.cartModel,true,'cart',requestId,appName);
        }else if(requestId==13){
             Store.orderModel.source = val['order']?val['order']:'[]';
             isAppActive=false;__sigModelReady(Store.orderModel,true,'order',requestId,appName);
        }else if(requestId==14){
            for(x in val['cart']){
                Store.cartArray[val['cart'][x]['catalog_id']] = []
                for(y in val['cart'][x]['cart_item']){Store.cartArray[val['cart'][x]['catalog_id']][val['cart'][x]['cart_item'][y]['item_id']] = {'quantity':val['cart'][x]['cart_item'][y]['quantity']};}
            }
            Store.tmpPreloadCartPerSeat=true;
            isAppActive=false;checkPreload();return true;
        }

        // out of all conditions
        isAppActive=false;
        core.info('InflightMenu.qml | '+__appName+' | isAppActive: '+isAppActive)
    }

    function updateCatalogModel(catalog_id, status){
        core.info('InflightMenu.qml | '+__appName+' | updateCatalogModel | catalog id : '+catalog_id+' | status : '+status)
            if(status=='blocked'){
                for(var getCatalogId in Store.catalogStatusArray){if(getCatalogId == catalog_id){Store.catalogStatusArray[getCatalogId]=0;}} //block catalog
            }else{
                for(var getCatalogId in Store.catalogStatusArray){if(getCatalogId == catalog_id){Store.catalogStatusArray[getCatalogId]=1;}} //Unblock catalog
            }
            Store.catalogListObject['catalog']=[];var counter=0;
             for(var i in Store.__catalogListObject['catalog']){    // Loop through main catalog array
                 if(Store.catalogStatusArray[Store.__catalogListObject['catalog'][i]['catalog_id']] != 0){   // Filter blocked catalogs
                     Store.catalogListObject['catalog'][counter] = Store.__catalogListObject['catalog'][i];    // Assign unblocked catalogs to temp array
                     counter++;
                 }
             }

            Store.catalogModel.source =  Store.catalogListObject['catalog'];
            core.info('InflightMenu.qml | '+__appName+' | updateCatalogModel | catalogModel.count='+Store.catalogModel.count);
            sigCatalogStatusChanged(catalog_id,Store.catalogStatusArray[catalog_id]);
    }

    function updateCartStatus(catalog_id,status){
        core.info('InflightMenu.qml | '+__appName+' | updateCartStatus | catalog id : '+catalog_id+' | status : '+status);
        if(Store.cartStatus){Store.cartStatus[catalog_id] = status?1:0;sigCartStatusChanged(catalog_id,status);}
    }

    function updateCartSystemStatus(status){
        core.info('InflightMenu.qml | '+__appName+' | updateCartSystemStatus | status : '+status);
        __cartSystemStatus = status?1:0;sigCartSystemStatusChanged(status);
    }

    function setIsCompReady(){
         core.info('InflightMenu.qml | '+__appName+' | setIsCompReady | catalogReady='+__catalogReady+' | Store.preloadData='+Store.preloadData)
         if(__catalogReady==true){
            if(Store.preloadCartSystem){Store.qtyInCart = [];}
            core.info('InflightMenu.qml | '+__appName+' | setIsCompReady | Load Complete Data Ready ');
        }else {core.info('InflightMenu.qml | '+__appName+' | setIsCompReady | Data Not Ready ');}
        __makeAppRequest=true;
        interactiveStartup.eventHandler(Store.cEventId);
    }

    function readData(data,value,requestId){
        if(data=='' || !(value && value['data'])){
            core.info("InflightMenu.qml | "+__appName+" | WebRequest onResponse | data is NOT ready | data : "+data)
            readJsonObject(__appName, 0, '');
            return false;
        }

        core.info("InflightMenu.qml | "+__appName+" | WebRequest onResponse | data is ready ");
        readJsonObject(__appName,requestId, value['data'][0]);
    }

    function __getInventoryByItemId(catalog_id,item_id_list){
        if (isAppActive==true){
            core.info('InflightMenu.qml | '+__appName+' | __getInventoryByItemId | App is already Acitve current Reuqest is droped.... ')
            return false;
        }else{isAppActive=true;}

        var requestId=10;var getParams='';
        core.info('InflightMenu.qml | '+__appName+' | __getInventoryByItemId | catalog_id : '+catalog_id+' | item_id_list : '+item_id_list);
        if(!catalog_id || !item_id_list){core.info('InflightMenu.qml | '+__appName+' | __getInventoryByItemId | ERROR | catalog or item list is empty ');}
        getParams="function=getInventoryByItemId&cabin_class="+Store.cabinClass[core.pif.getCabinClassName()]+"&catalog_id="+catalog_id+"&item_id_list="+item_id_list+"&output_format="+Store.catalogFields['output_format'];
        sendRequest(getParams,requestId)
    }

    function startPreload(){
        __appIso=Store.defaultLanguage;
        core.info("InflightMenu.qml | "+__appName+" | startPreload "+Store.preloadData);
        if(Store.preloadData==true){
            core.info("InflightMenu.qml | "+__appName+" | startPreload | preloadData: "+Store.preloadData+" | - Call getAllCatalogs()");
            getAllCatalogs(__appIso);
        }else if(Store.preloadData=='NA'){
            core.info("InflightMenu.qml | "+__appName+" | startPreload() | ERROR | preload is NA, Remove from app list");
            interactiveStartup.eventHandler(Store.cEventId);
        }

        if(Store.preloadData!=true){interactiveStartup.eventHandler(Store.cEventId);}
    }

    function checkPreload(){
        core.info('InflightMenu.qml | '+__appName+' | checkPreload ')

        if(__catalogReady== false && Store.preloadData==true){
            core.info('InflightMenu.qml | '+__appName+' | checkPreload | active - catalog')
            getAllCatalogs(__appIso);
            return true;
        }else if(__languageListReady==false && __catalogReady== true){
            core.info('InflightMenu.qml | '+__appName+' | checkPreload | active - language');
            getLanguageList();
            return true;
        }else if(__languageListReady==true && __catalogReady==true && Store.loadCatalogType=='all' && Store.fetchedNonDefaultLanguageData==false){
            core.info('InflightMenu.qml | '+__appName+' | checkPreload | active - non default languages preload');
            __getNonDefaultLanguageCatalogs();
            return true;
        }else if(Store.preloadCartSystem && !Store.tmpPreloadCatalogCart && __catalogReady){
            core.info('InflightMenu.qml | '+__appName+' | checkPreload | active - cart status ')
            getCartStatus();
            return true;
        }else if(Store.preloadOrderSystem && !Store.tmpPreloadOrder && __catalogReady){
            core.info('InflightMenu.qml | '+__appName+' | checkPreload | active - order status ')
            getOrderStatus();
            return true;
        }else if(!Store.tmpPreloadCartPerSeat && __catalogReady && __cartSystemStatus){
            core.info('InflightMenu.qml | '+__appName+' | checkPreload | active - get cart for seat  ')
            getAllCartsPerSeat(__appIso);
            return true;
        }else{
            setIsCompReady();
            __updateCallback(Store.storedCallback);
            Store.storedCallback='';
            if(__catalogReady==true){
                __sigModelReady(Store.catalogModel,true,'catalog',7,__appName);
            }else{
              __sigModelReady(Store.catalogModel,false,'catalog',7,__appName);
            }
            __disconnectCallback();
            isAppActive=false;
        }
    }

    function sendRequest(getParams,requestId){
        if(dataController.__makeHeadendRequest()){
            core.debug('InflightMenu.qml | '+__appName+' | sendRequest | makeHeadendRequest: '+dataController.__makeHeadendRequest()+' | getParams  : '+getParams)
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }else{
            core.info('InflightMenu.qml | '+__appName+' | sendRequest | makeHeadendRequest: '+dataController.__makeHeadendRequest())
            if(requestId == 4){
                __getLocalData(Store.metadataPath +"/shopping/isApplicationOn.json",requestId)
            }else if(requestId == 5){
                    __getLocalData(Store.metadataPath +"/"+__appName+"/isShoppingOn.json",requestId)
            }else if(requestId == 1){
                __getLocalData(Store.metadataPath +"/"+__appName+"/"+getParams,requestId);
            }else if(requestId == 6){
                __getLocalData(Store.metadataPath +"/"+__appName+"/getAllLanguages.json",requestId);
            }else if(requestId == 7){
                __getLocalData(Store.metadataPath +"/"+__appName+"/getAllEnabledCatalogStatuses.json",requestId)
            }else if(requestId == 8){
                __getLocalData(Store.metadataPath +"/"+__appName+"/isCartSystemOn.json",requestId)
            }else if(requestId == 9){
                __getLocalData(Store.metadataPath +"/"+__appName+"/isOrderSystemOn.json",requestId)
            }
        }
    }

    function reloadData(){

            if(Store.changeLang==true){
                isAppActive=false
                if(Store.paramsArray['catalog']!='-1'){
                    for (var i in Store.enabledCatalogObject['catalog']) {
                        if(Store.paramsArray['catalog'] == Store.enabledCatalogObject['catalog'][i]['catalog_id']){
                            Store.categoryObject=Store.enabledCatalogObject['catalog'][i]
                        }
                    }
                    getNextList(Store.categoryObject,Store.paramsArray['catalog'], 1)

                }
                if(Store.paramsArray['category']!='-1' && Store.paramsArray['category']!= undefined){
                    var cid = Store.paramsArray['category']
                    getNextList(Store.categoryObject,Store.paramsArray['catalog'],2,cid)
                    if(Store.paramsArray['categoryLevel2']!='-1' && Store.paramsArray['categoryLevel2']!= undefined){
                        cid = Store.paramsArray['categoryLevel2']
                        getNextList(Store.categoryObject,Store.paramsArray['catalog'],3,cid)
                        if(Store.paramsArray['categoryLevel3']!='-1' && Store.paramsArray['categoryLevel3']!= undefined){
                            cid=Store.paramsArray['categoryLevel3']
                            getNextList(Store.categoryObject,Store.paramsArray['catalog'],4,cid)
                            if(Store.paramsArray['categoryLevel4']!='-1' && Store.paramsArray['categoryLevel4']!= undefined){
                                cid=Store.paramsArray['categoryLevel4']
                                getNextList(Store.categoryObject,Store.paramsArray['catalog'],5,cid)
                            }
                        }
                    }
                    if(Store.paramsArray['item']!='-1' && Store.paramsArray['item']!= undefined){
                        Store.ParentCid = []
                        for(var j in  Store.categoryObject['category_node']){
                            if(Store.categoryObject['category_node'][j]['category_node_id']==cid){
                                Store.categoryObject2=Store.categoryObject['category_node'][j]
                                getNextList(Store.categoryObject2,Store.paramsArray['catalog'],4,cid)
                            }
                        }

                    }
                }
            }

        if(Store.changeLang==true){
            //__sigModelReady(Store.catalogModel,true,'languageChange',requestId,__appName);
            Store.changeLang=false
        }
    }

    function resetVariables(){
        core.info('InflightMenu.qml | '+__appName+' | resetVariables | appIso : '+__appIso+'  | resetting');

        isAppActive=true;
        __catalogReady= false
        Store.changeLang = false;
        Store.tmpPreloadCatalogCart  = false
        Store.tmpPreloadOrder = false
        Store.tmpPreloadCartPerSeat = false
        __languageListReady=false
        Store.catalogModel.source='[]';
        Store.catalogStatus.source='[]';
        Store.languageModel.source='[]';
        Store.categoryModel.source='[]';
        Store.itemModel.source='[]';
        Store.itemSynopsis.source='[]';
        Store.fetchedNonDefaultLanguageData=false;
        Store.nonDefaultLanguageIso=false;
        Store.catalogNonDefaultLanguageDataObj=[];
        Store.fetchedNonDefaultLanguageArr=[];

        if(Store.preloadCartSystem){Store.cartModel.source='[]';}
        if(Store.preloadOrderSystem){Store.orderModel.source='[]';Store.orderItemModel.source='[]';}
        if(Store.categoryLevel){for(var i=1;i<=Store.categoryLevel;i++){getSubCategory(i).source='[]';} Store.categoryLevel=0}
        isAppActive=false
    }

    function getSubCategory(level){
        if(!level) return;
        Store.categoryLevel=Store.categoryLevel<level?level:Store.categoryLevel;
        if(!Store.subCategoryModel){Store.subCategoryModel={};}
        if(!Store.subCategoryModel[level]){Store.subCategoryModel[level]=sModelComp.createObject(inflightMenu);}
        return Store.subCategoryModel[level];
    }

    function loadDynamicComponents(){
        Store.blankModel=sModelComp.createObject(inflightMenu);
        Store.catalogModel=sModelComp.createObject(inflightMenu);
        Store.languageModel=sModelComp.createObject(inflightMenu);
        Store.catalogStatus=sModelComp.createObject(inflightMenu);
        Store.categoryModel=sModelComp.createObject(inflightMenu);
        Store.itemModel=sModelComp.createObject(inflightMenu);
        Store.itemSynopsis=sModelComp.createObject(inflightMenu);
        if(Store.preloadCartSystem){Store.cartModel=sModelComp.createObject(inflightMenu);}
        if(Store.preloadOrderSystem){Store.orderModel=sModelComp.createObject(inflightMenu);Store.orderItemModel=sModelComp.createObject(inflightMenu);}
        Store.cartInventoryObject=[];
        return true;
    }

    function __setConfiguration(cabinClass,configurationData,eventId){
        if(__appName==''){core.info("InflightMenu.qml | "+__appName+" | __setConfiguration | App name not set ");}
        if(configurationData){
            Store.cabinClass=cabinClass?cabinClass:Store.cabinClass;
            Store.catalogFields['currency_codes'] = configurationData['currency_codes']?configurationData['currency_codes']:Store.catalogFields['currency_codes'];
            Store.catalogFields['output_format'] = configurationData['output_format']?configurationData['output_format']:Store.catalogFields['output_format'];
            Store.preloadData = configurationData['preloadData']?(configurationData['preloadData']=="true"?true:false):Store.preloadData;
            Store.loadCatalogType = configurationData['loadCatalogType']?Store.loadCatalogType=configurationData['loadCatalogType']:Store.loadCatalogType;
            Store.metadataPath = configurationData['metadataPath']?configurationData['metadataPath']:Store.metadataPath;
            Store.catalogBlockType = configurationData['catalogBlockType']?configurationData['catalogBlockType']:Store.catalogBlockType;
            Store.preloadCartSystem = configurationData['preloadCartSystem']?(configurationData['preloadCartSystem']=="true"?true:false):Store.preloadCartSystem;
            Store.defaultLanguage=configurationData['defaultLanguage']?configurationData['defaultLanguage']:Store.defaultLanguage;
            Store.preloadOrderSystem = configurationData['preloadOrderSystem']?(configurationData['preloadOrderSystem']=="true"?true:false):Store.preloadOrderSystem;
            Store.cEventId=eventId;
            loadDynamicComponents();
            return true;
        }
    }

    function __resetInFallbackMode(){
         core.info('InflightMenu.qml | __resetInFallbackMode | in fallback mode returning blank model');
        isAppActive=false;
        if(Store.storedCallback){__updateCallback(Store.storedCallback);Store.storedCallback='';}
        interactiveStartup.eventHandler(Store.cEventId);
        __sigModelReady(Store.blankModel,false,'',0,__appName);
    }
    Component.onCompleted:{
        Store.tmpPreloadCatalogCart  = false
        Store.tmpPreloadOrder = false
        Store.tmpPreloadCartPerSeat = false
        Store.catalogBlockType=1;
        Store.preloadData = false;
        Store.preloadCartSystem  = false
        Store.preloadOrderSystem = false
        Store.categoryLevel=0;
        Store.defaultLanguage='ENG';
        Store.cEventId=0;
        Store.fetchedNonDefaultLanguageData=false;
        Store.fetchedNonDefaultLanguageArr=[];
        Store.storedCallback = '';
        Store.cabinClass=[]
        Store.catalogFields=[];
        Store.cartArray=[];
        Store.qtyInCart=[];
        Store.catalogFields['currency_codes'] = 'USD';
        Store.catalogFields['output_format'] = 'QTJSON';
        Store.metadataPath = "../../content/"
        Store.loadCatalogType='all';// possible values : all/onLanguageChange

        core.info ("InflightMenu.qml | "+__appName+" | Component.onCompleted | InflightMenu LOAD COMPLETE");
    }

    Component{id:sModelComp;JsonModel{id: blankModel;}}
}
