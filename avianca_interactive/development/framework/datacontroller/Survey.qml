import QtQuick 1.0
import Panasonic.Pif 1.0
import "../components"
import "../blank.js" as Store

DataLoader {
    id: survey

    property variant dataController;
    signal __sigModelReady(bool status,variant model,variant model1,variant model2);
    signal sigtTriggerSurveyReady(string event,int surveyId);

    onSigDataReady: {
        if(errCode==0){
            core.info("Survey.qml | dataLoader | onSigDataReady");
            __processModel(responseObj,responseString,requestId);
        }else {
            if(Store.storedCallback){__updateCallback(Store.storedCallback);Store.storedCallback='';}
            interactiveStartup.eventHandler(Store.cEventId);
            __sigModelReady(false,Store.blankModel,'','');
        }
    }

    onSigLocalDataReady:{
        if(errCode==0){
            core.info("Survey.qml | dataLoader | onSigLocalDataReady ");
            __processModel(responseObj,'fromConfigModel',requestId);
        }else{
            core.info("Survey.qml | dataLoader | onSigLocalDataReady | ERROR ");
            if(Store.storedCallback){__updateCallback(Store.storedCallback);Store.storedCallback='';}
            interactiveStartup.eventHandler(Store.cEventId);
            __sigModelReady(false,Store.blankModel,'','');
        }
    }

    Connections{
        target:core.pif;
        //onEntOnChanged not needed since registered to stage
        onTimeToDestinationChanged:{core.debug("Survey.qml | onTimeToDestinationChanged : "+timeToDestination);__process_triggered_survey_event('TTD',timeToDestination);}
        onTimeSinceTakeOffChanged:{core.debug("Survey.qml | onTimeSinceTakeOffChanged : "+timeSinceTakeOff);__process_triggered_survey_event('TAKEOFF',timeSinceTakeOff);}
    }

    //public function
    function getSurveyModel(){return Store.surveyModel;}
    function getEssaySurveyModel(){return Store.surveyByDisplayTypeModel;}
    function getResponseModel(){return Store.responseModel;}

    function getAllSurvey(callback) {
        if(isAppActive==true){core.info('Survey.qml| getAllSurvey | App is already Acitve current Reuqest is droped.... ');return false;}else{isAppActive=true;}

        var requestId=1;__updateCallback(callback);

        if(Store.surveyModel.count>0 && Store.previousISO==core.settings.languageISO){
            core.info('Survey.qml| getAllSurvey | Survey Model Found Returning | Model count: '+Store.surveyModel.count);isAppActive=false;__sigModelReady(true,Store.surveyModel,'','');
            if(Store.preloadSurvey){isAppActive=false;interactiveStartup.eventHandler(Store.cEventId);}
            return true;}
        Store.previousISO=core.settings.languageISO;
        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/survey/getAllSurvey2.json';
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getLocalData(getParams,requestId);
            }
        }
        else {
            var getParams = "controller=SurveyController&action=returnData&func=getallsurvey2&param1="+core.pif.getSeatNumber()+"&param2="+core.pif.getCabinClassName()+"&param3="+core.settings.languageISO+"&type=QML_JSON";
            core.info("Survey.qml| getAllSurvey | "+getParams);
           if(pif.getFallbackMode()){
               __resetInFallbackMode();
           }else{
            __getRemoteData(getParams,requestId);
           }
        }
    }    

    function getSurveys(callback){
        if(isAppActive==true){core.info('Survey.qml| getSurveys | App is already Acitve current Reuqest is droped.... ');return false;}else{isAppActive=true;}
         var requestId=8;__updateCallback(callback);
        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/survey/getSurveys.json';
            __getLocalData(getParams,requestId);
        }else {
            var getParams = "index2.php/api/json/v/1.0/p/Data/m/getSurveys";
            core.info("Survey.qml| getSurveys | "+getParams);
           if(pif.getFallbackMode()){
               __resetInFallbackMode();
           }else{
            __getRemoteData(getParams,requestId);
           }
        }
    }

    function getSurveyImageById(id){
        if(typeof  Store.surveyImageInfo[parseInt(id)]== 'undefined'){
            return false;
        }
        return Store.surveyImageInfo[parseInt(id)]['imageInfo'];
    }

    function getSurveyQuestionImageById(survey_id,qid){
        if(typeof  Store.surveyImageInfo[parseInt(survey_id)]== 'undefined'){
            return false;
        }else if(typeof Store.surveyImageInfo[parseInt(survey_id)][parseInt(qid)]== 'undefined'){
            return false;
        }
        return Store.surveyImageInfo[parseInt(survey_id)][parseInt(qid)]['imageInfo'];
    }

    function advSurvey(params,callback){
        if(isAppActive==true){core.info('Survey.qml| advSurvey | App is already Acitve current Reuqest is droped.... ');return false;}else{isAppActive=true;}

        params['surveyId'] = params['surveyId']?params['surveyId']:'';
        params['qId'] = params['qId']?params['qId']:'';
        params['aId'] = params['aId']?params['aId']:'';
        params['answer'] = params['answer']?params['answer']:'';
        params['questionType'] = params['questionType']?params['questionType']:'';
        params['subQuestionId'] = params['subQuestionId']?params['subQuestionId']:'';
        if(params['surveyId'] != Store.surveyId){Store.responseModel.source = '[]'}
        core.info("Survey.qml| advSurvey | params | surveyId: "+params['surveyId']+" | qId: "+params['qId']+" | aId: "+params['aId']+" | answer: "+params['answer']+" | questionType: "+params['questionType']+" | subQuestionId: "+params['subQuestionId']);

        var requestId=2;__updateCallback(callback);
        Store.isPrevQuestion = false;
        Store.surveyId       = params['surveyId'];

        if(Store.surveyResumeLookup[Store.surveyId] && parseInt(Store.surveyResumeLookup[Store.surveyId]["qId"],10)>0){
            core.info('Survey.qml | advSurvey | Survey Resume Found surveyId :'+Store.surveyId+' | qId :'+Store.surveyResumeLookup[Store.surveyId]["qId"]);

            Store.qId            = Store.surveyResumeLookup[Store.surveyId]["qId"];
            core.info("Survey.qml| advSurvey | Resume >> Calling get_viewed_question() ");
            isAppActive= false
            Store.surveyResumeLookup[Store.surveyId]["qId"] = ''; //clear survey resume
            get_viewed_question({'surveyId':Store.surveyId,'qId':Store.qId},callback);
            return true;
        }else {
            Store.qId        = params['qId'];
            var retStr = __formatParams(params['qId'],params['aId'],params['answer'],params['subQuestionId'],params['questionType']);

            if(!dataController.__makeHeadendRequest()){
                var getParams = '../../content/survey/adv_'+Store.surveyId+'_'+Store.qId+'.json';
                __getLocalData(getParams,requestId);
            }else {
                var getParams = "controller=SurveyController&action=returnData&func=advsurvey&param1="+core.pif.getSeatNumber()+"&param2="+core.pif.getCabinClassName()+"&param3="+Store.surveyId+"&param4="+core.settings.languageISO+"&param5="+retStr.qidStr+"&param6="+retStr.aidStr+"&param7="+retStr.ansStr+"&param8="+params['questionType']+"&param9="+retStr.ridStr+"&param10=&type=QML_JSON";
                core.info("Survey.qml| advSurvey | "+getParams);
                if(pif.getFallbackMode()){
                    __resetInFallbackMode();
                }else{
                __getRemoteData(getParams,requestId);
                }
            }
        }
    }

    function getPreviousQuestion(params,callback){
        if(isAppActive==true){core.info('Survey.qml| getPreviousQuestion | App is already Acitve current Reuqest is droped.... ');return false;}else{isAppActive=true;}

        params['surveyId'] = params['surveyId']?params['surveyId']:'';
        params['qId'] = params['qId']?params['qId']:'';

        core.info("Survey.qml| getPreviousQuestion | params | surveyId: "+params['surveyId']+" | qId: "+params['qId']);
        var requestId=3;__updateCallback(callback);
        Store.isPrevQuestion = true;
        Store.surveyId       = params['surveyId'];
        Store.qId            = params['qId'];

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/survey/prev_'+Store.surveyId+'_'+Store.qId+'.json';
            __getLocalData(getParams,requestId);
        }else {
            var getParams = "controller=SurveyController&action=returnData&func=get_prev_question&param1="+core.pif.getSeatNumber()+"&param2="+core.pif.getCabinClassName()+"&param3="+Store.surveyId+"&param4="+Store.qId+"&param5="+core.settings.languageISO+"&type=QML_JSON";
            core.info("Survey.qml| getPreviousQuestion | "+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    function get_viewed_question(params,callback){
        if(isAppActive==true){core.info('Survey.qml| get_viewed_question | App is already Acitve current Reuqest is droped.... ');return false;}
        else{isAppActive=true;}

        params['surveyId'] = params['surveyId']?params['surveyId']:'';
        params['qId'] = params['qId']?params['qId']:'';

        core.info("Survey.qml| get_viewed_question | params | surveyId: "+params['surveyId']+" | qId: "+params['qId']);
        var requestId=2;__updateCallback(callback);
        Store.surveyId       = params['surveyId'];
        Store.qId            = params['qId'];

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/survey/view_'+Store.surveyId+'_'+Store.qId+'.json';
            __getLocalData(getParams,requestId);
        }else {
            var getParams = "controller=SurveyController&action=returnData&func=get_viewed_question&param1="+core.pif.getSeatNumber()+"&param2="+core.pif.getCabinClassName()+"&param3="+Store.surveyId+"&param4="+Store.qId+"&param5="+core.settings.languageISO+"&type=QML_JSON";
            core.info("Survey.qml| get_viewed_question | "+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    function readSurveyInfo(params,callback) {
        if(isAppActive==true){core.info('Survey.qml| readSurveyInfo | App is already Acitve current Reuqest is droped.... ');return false;}else{isAppActive=true;}

        params['surveyId'] = params['surveyId']?params['surveyId']:'';
        core.info("Survey.qml| readSurveyInfo | params | surveyId: "+params['surveyId']);

        var requestId=4;__updateCallback(callback);
        Store.surveyId = params['surveyId'];

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/survey/readSurveyInfo.json';
            __getLocalData(getParams,requestId);
        }
        else {
            var getParams = "controller=SurveyController&action=returnData&func=readSurveyInfo&param1="+Store.surveyId+"&param2="+core.settings.languageISO+"&param3=true&type=QML_JSON";
            core.info("Survey.qml| readSurveyInfo | "+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    function getAllEssaySurvey(callback){
        var params={'displayType':'essay'};
        return getAllSurveyByDisplayType(params,callback);
    }

    function getAllSurveyByDisplayType(params,callback){
        if(isAppActive==true){core.info('Survey.qml| getAllSurveyByDisplayType | App is already Acitve current Reuqest is droped.... ');return false;}else{isAppActive=true;}

        params['displayType'] = params['displayType']?params['displayType']:'essay';
        var requestId=5;__updateCallback(callback);__createSurveyByDisplayTypeModels();

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/survey/getAllSurveys.json';
            __getLocalData(getParams,requestId);
        }
        else {
            var getParams = "controller=SurveyController&action=returnData&func=getAllSurveys&param1="+core.pif.getSeatNumber()+"&param2="+core.pif.getCabinClassName()+"&param3="+core.settings.languageISO+"&param4="+params['displayType']+"&type=QML_JSON";
            core.info("Survey.qml| getAllSurveyByDisplayType | "+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
           __getRemoteData(getParams,requestId);
            }
        }
    }

    function get_survey_questions(params,callback){
        if(isAppActive==true){core.info('Survey.qml| get_survey_questions | App is already Acitve current Reuqest is droped.... ');return false;}else{isAppActive=true;}

        params['surveyId'] = params['surveyId']?params['surveyId']:'';
        core.info("Survey.qml| get_survey_questions | params | surveyId: "+params['surveyId']);
        var requestId=6;__updateCallback(callback);
        Store.surveyId  = params['surveyId'];

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/survey/get_survey_questions_'+Store.surveyId+'.json';
            __getLocalData(getParams,requestId);
        }else {
            var getParams = "controller=SurveyController&action=returnData&func=get_survey_questions&param1="+core.pif.getSeatNumber()+"&param2="+core.pif.getCabinClassName()+"&param3="+Store.surveyId+"&param4="+core.settings.languageISO+"&type=QML_JSON";
            core.info("Survey.qml| get_survey_questions | "+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    function save_survey_questions(params,callback){
        if(isAppActive==true){core.info('Survey.qml| save_survey_questions | App is already Acitve current Reuqest is droped.... ');return;}else{isAppActive=true;}

        params['surveyId'] = params['surveyId']?params['surveyId']:'';
        params['qId'] = params['qId']?params['qId']:'';
        params['aId'] = params['aId']?params['aId']:'';
        params['answer'] = params['answer']?params['answer']:'';

        core.info("Survey.qml| advSurvey | params | surveyId: "+params['surveyId']+" | qId: "+params['qId']+" | aId: "+params['aId']+" | answer: "+params['answer']);
        var requestId=7;__updateCallback(callback);
        Store.surveyId  = params['surveyId'];

        var retStr = __formatParams_essay(params['qId'],params['aId'],params['answer']);

        if(!dataController.__makeHeadendRequest()){
            var getParams = '../../content/survey/save_survey_questions_'+Store.surveyId+'.json';
            __getLocalData(getParams,requestId);
        }else {
            var getParams = "controller=SurveyController&action=returnData&func=save_survey_questions&param1="+core.pif.getSeatNumber()+"&param2="+core.pif.getCabinClassName()+"&param3="+Store.surveyId+"&param4="+retStr.qidStr+"&param5="+retStr.aidStr+"&param6="+retStr.ansStr+"&param8="+retStr.ridStr+"&type=QML_JSON";
            core.info("Survey.qml| save_survey_questions | "+getParams);
            if(pif.getFallbackMode()){
                __resetInFallbackMode();
            }else{
            __getRemoteData(getParams,requestId);
            }
        }
    }

    /* for FE to know if amswer was previously selected
       aid is mandatory
       qid and answer is used for likert and ordinal
     */
    function getRid(aid,qid,answer,qType){
        if(Store.responseModel.count){
            for(var i=0;i<Store.responseModel.count;i++){
                if(qType=="likertScale" || qType=="ordinalResponse"){
                    if(Store.responseModel.getValue(i,'AID')==aid && Store.responseModel.getValue(i,'QID')==qid && Store.responseModel.getValue(i,'answer')==answer){
                        core.info('Survey.qml | getRid | qType : '+qType+' | Matched | aid : '+aid+' | RID :'+Store.responseModel.getValue(i,'RID'));
                        return Store.responseModel.getValue(i,'RID');
                    }
                }else {
                    if(Store.responseModel.getValue(i,'AID')==aid){
                        core.info('Survey.qml | getRid | qType : '+qType+' | Matched | aid : '+aid+' | RID :'+Store.responseModel.getValue(i,'RID'));
                        return Store.responseModel.getValue(i,'RID');
                    }
                }
            }
            return false;
        }else {
            return false;
        }
    }

    // for FE to get answer logged or check if answer text is logged
    function getAnswerText(aid,qid){
        core.debug('Survey.qml | getAnswerText | Called | aid : '+aid+' | qid : '+qid+' | responseModel count :'+Store.responseModel.count+'');

        if(Store.responseModel.count){
            for(var i=0;i<Store.responseModel.count;i++){
                if(Store.responseModel.getValue(i,'AID')==aid && Store.responseModel.getValue(i,'QID')==qid){
                    core.info('Survey.qml | getAnswerText | Matched | aid : '+aid+' | qid : '+qid+' | | RID :'+Store.responseModel.getValue(i,'RID')+' | AnswerText : '+Store.responseModel.getValue(i,'answer'));
                    return Store.responseModel.getValue(i,'answer');
                }
            }
            return '';
        }else {
            return ''; //return empty
        }
    }


    function resumeLater(){
        core.info('Survey.qml | resumeLater | Called | isPrevQuestion : '+Store.isPrevQuestion+' | surveyId :'+Store.surveyId+' | qId :'+Store.qId+''+' | current qId :'+Store.questionModel.getValue(0,'QID'));

        Store.surveyResumeLookup[Store.surveyId]           =[];
        Store.surveyResumeLookup[Store.surveyId]["qId"]    = Store.questionModel.getValue(0,'QID');

        core.info('Survey.qml | resumeLater | surveyResumeLookup surveyId : '+Store.surveyId+' | Qid : '+Store.surveyResumeLookup[Store.surveyId]["qId"]);
    }

    function getSeatTriggerSurveyId(app_name,seatTrigger){
        core.debug('Survey.qml | getTriggerSurveyId |name= '+app_name+' | seatTrigger='+seatTrigger);
        var temp=[]
        if(Store.triggerSurveyModelArr[app_name]){
            for(var i in Store.triggerSurveyModelArr[app_name]){
                if(Store.triggerSurveyModelArr[app_name][i].trigger==seatTrigger){
                    temp.push(Store.triggerSurveyModelArr[app_name][i].surveyId);
                }
            }
        }
        if(temp.length>0){return temp }
        else {  core.info('Survey.qml || getTriggerSurveyId |name= '+app_name+' | seatTrigger='+seatTrigger+' | NOT FOUND'); return 0}
    }
    function initiateSeatTrigger(name,seatTrigger){
        core.debug("Survey.qml | initiateSeatTrigger called | app name :"+name+" | trigger : "+seatTrigger);
        __process_seat_triggered_survey(name,seatTrigger);
    }

    //Private functions
    function __updateCallback(callback){
        if(Store.callback){__sigModelReady.disconnect(Store.callback);}
        if(callback){__sigModelReady.connect(callback);Store.callback = callback;}
    }

    function __getRemoteData(getParams,requestId){
        if(getParams.indexOf('getSurveys') == -1){
            getData(core.settings.remotePath+"/surveys/",3,getParams,'post','',requestId);
        }else{
            getData(core.settings.remotePath+"/surveys/",3,getParams,'get','',requestId,true);
        }
    }
    function __getLocalData(getParams,requestId){getLocalData(getParams,requestId);}

    function __processModel(value,data,requestId){

        if(data!='' && value && value['data']){
            core.info("Survey.qml| processModel | Data ready = True | requestId = "+requestId);
            __readJsonObject(requestId, value);

        }else{
            core.info("Survey.qml| processModel | data is NOT ready");
            if(Store.storedCallback){__updateCallback(Store.storedCallback);Store.storedCallback='';}
            interactiveStartup.eventHandler(Store.cEventId);
            isAppActive=false;__sigModelReady(false,Store.blankModel,'','');
        }
    }

    function __readJsonObject(requestId, value){
        if(requestId==0){
            isAppActive=false;core.info('Survey.qml| __readJsonObject |  requestId = '+requestId+' | Data ready = False');return false;
        }else if(requestId==1){
            Store.surveyModel.clear();Store.currentSurveyArray=[];Store.surveyModelObj={};/*Store.triggerSurveyModelArr=[];*/Store.triggerSurveyAvailable=false;
            Store.surveyModelObj =  value['data'];
            for(var i=0;i<value['data'].length;i++){
                if(value['data'][i]['flightTriggers'] && value['data'][i]['flightTriggers'].length>0){
                    for(var t=0;t<value['data'][i]['flightTriggers'].length;t++){
                        if(checkIfSurveyTriggered(value['data'][i]['flightTriggers'][t]['name'],value['data'][i]['surveyID'])){
                            core.info('Survey.qml| __readJsonObject |   requestId = '+requestId+' | Trigger already occured for Survey Found | Survey Id: '+value['data'][i]['surveyID']+' | name: '+value['data'][i]['flightTriggers'][t]['name']+' | trigger: '+value['data'][i]['flightTriggers'][t]['trigger']+' | value: '+value['data'][i]['flightTriggers'][t]['value'])
                            __appendTrigeredSurvey(value['data'][i]['surveyID']);
                        }else{
                            if(!Store.triggerSurveyModelArr[value['data'][i]['flightTriggers'][t]['name']]){Store.triggerSurveyModelArr[value['data'][i]['flightTriggers'][t]['name']]=[];}
                            Store.triggerSurveyModelArr[value['data'][i]['flightTriggers'][t]['name']].push({"trigger":value['data'][i]['flightTriggers'][t]['trigger'],"value":parseInt(value['data'][i]['flightTriggers'][t]['value'],10),"surveyId":value['data'][i]['surveyID']});
                            core.info('Survey.qml| __readJsonObject |  requestId = '+requestId+' | Trigger Survey Found | Survey Id: '+value['data'][i]['surveyID']+' | name: '+value['data'][i]['flightTriggers'][t]['name']+' | trigger: '+value['data'][i]['flightTriggers'][t]['trigger']+' | value: '+value['data'][i]['flightTriggers'][t]['value']);
                        }
                    }
                    Store.triggerSurveyAvailable=true;
                    if(Store.triggeredSurveyAppendInModel  ) { //reset trigger surveys are blocked at start up
                        __appendSurveyModel(value,i);
                    }

                }else if(value['data'][i]['seatTriggers'] && value['data'][i]['seatTriggers'].length>0){
                    for(var t=0;t<value['data'][i]['seatTriggers'].length;t++){
                        if(checkIfSurveyTriggered(value['data'][i]['flightTriggers'][t]['name'],value['data'][i]['surveyID'])){
                            core.info('Survey.qml| __readJsonObject |   requestId = '+requestId+' | Trigger already occured for Survey Found | Survey Id: '+value['data'][i]['surveyID']+' | name: '+value['data'][i]['flightTriggers'][t]['name']+' | trigger: '+value['data'][i]['flightTriggers'][t]['trigger']+' | value: '+value['data'][i]['flightTriggers'][t]['value'])
                            __appendTrigeredSurvey(value['data'][i]['surveyID']);
                        }else{
                            if(!Store.triggerSurveyModelArr[value['data'][i]['seatTriggers'][t]['name']]){Store.triggerSurveyModelArr[value['data'][i]['seatTriggers'][t]['name']]=[];}
                            Store.triggerSurveyModelArr[value['data'][i]['seatTriggers'][t]['name']].push({"trigger":value['data'][i]['seatTriggers'][t]['trigger'],"value":parseInt(value['data'][i]['seatTriggers'][t]['value'],10),"surveyId":value['data'][i]['surveyID']});
                            core.info('Survey.qml| __readJsonObject |  requestId = '+requestId+' | Trigger Survey Found | Survey Id: '+value['data'][i]['surveyID']+' | name: '+value['data'][i]['seatTriggers'][t]['name']+' | trigger: '+value['data'][i]['seatTriggers'][t]['trigger']+' | value: '+value['data'][i]['seatTriggers'][t]['value']);
                        }
                    }
                    Store.triggerSurveyAvailable=true;
                    if(Store.triggeredSurveyAppendInModel) { //reset trigger surveys are blocked at start up
                        __appendSurveyModel(value,i);
                    }

                }else if(value['data'][i]['contentTriggers'] && value['data'][i]['contentTriggers'].length>0){
                    for(var t=0;t<value['data'][i]['contentTriggers'].length;t++){
                        if(checkIfSurveyTriggered(value['data'][i]['flightTriggers'][t]['name'],value['data'][i]['surveyID'])){
                            core.info('Survey.qml| __readJsonObject |   requestId = '+requestId+' | Trigger already occured for Survey Found | Survey Id: '+value['data'][i]['surveyID']+' | name: '+value['data'][i]['flightTriggers'][t]['name']+' | trigger: '+value['data'][i]['flightTriggers'][t]['trigger']+' | value: '+value['data'][i]['flightTriggers'][t]['value'])
                            __appendTrigeredSurvey(value['data'][i]['surveyID']);
                        }else{
                            if(!Store.triggerSurveyModelArr[value['data'][i]['contentTriggers'][t]['name']]){Store.triggerSurveyModelArr[value['data'][i]['contentTriggers'][t]['name']]=[];}
                            Store.triggerSurveyModelArr[value['data'][i]['contentTriggers'][t]['name']].push({"trigger":value['data'][i]['contentTriggers'][t]['trigger'],"value":parseInt(value['data'][i]['contentTriggers'][t]['value'],10),"surveyId":value['data'][i]['surveyID']});
                            core.info('Survey.qml| __readJsonObject |  requestId = '+requestId+' | Trigger Survey Found | Survey Id: '+value['data'][i]['surveyID']+' | name: '+value['data'][i]['contentTriggers'][t]['name']+' | trigger: '+value['data'][i]['contentTriggers'][t]['trigger']+' | value: '+value['data'][i]['contentTriggers'][t]['value']);
                        }
                    }
                    Store.triggerSurveyAvailable=true;
                    if(Store.triggeredSurveyAppendInModel) { //reset trigger surveys are blocked at start up
                        __appendSurveyModel(value,i);
                    }

                }else { //reset trigger surveys are blocked at start up
                     __appendSurveyModel(value,i);
                }
            }
            core.info('Survey.qml| __readJsonObject | requestId = '+requestId+' | surveyModel count: '+Store.surveyModel.count+" | triggerSurveyAvailable : "+Store.triggerSurveyAvailable);
            if(Store.preloadSurvey){interactiveStartup.eventHandler(Store.cEventId);}
            isAppActive=false;__sigModelReady(true,Store.surveyModel,'','');
        }else if(requestId==2){
            Store.questionModel.source =  value['data']
            if(value['data'][0] && value['data'][0]['answers']){Store.answerModel.source = value['data'][0]['answers']}else{Store.answerModel.source = value['data']}
            if(value['data'][0] && value['data'][0]['response']){Store.responseModel.source = value['data'][0]['response']}else {Store.responseModel.source = '[]'}
            if(value['data'][0] && value['data'][0]['option']){Store.optionModel.source = value['data'][0]['option']}else {Store.optionModel.source   = '[]'}

            core.info('Survey.qml| __readJsonObject | requestId = '+requestId+' | Question count = '+Store.questionModel.count+' | answerModel count = '+Store.answerModel.count+' | responseModel count = '+Store.responseModel.count+' | optionModel count = '+Store.optionModel.count);
            isAppActive=false;__sigModelReady(true,Store.questionModel,Store.answerModel,Store.optionModel);
        }else if(requestId==3){
            Store.questionModel.source =  value['data'];
            if(value['data'][0] && value['data'][0]['answers']){Store.answerModel.source = value['data'][0]['answers']}else{Store.answerModel.source = value['data']}
            if(value['data'][0] && value['data'][0]['response']){Store.responseModel.source = value['data'][0]['response']}else {Store.responseModel.source = '[]'}
            if(value['data'][0] && value['data'][0]['option']){Store.optionModel.source = value['data'][0]['option']}else {Store.optionModel.source = '[]'}

            core.info('Survey.qml| __readJsonObject | requestId = '+requestId+'  | Question count = '+Store.questionModel.count+' | AnswerModel count = '+Store.answerModel.count+' | responseModel count = '+Store.responseModel.count+' | optionModel count = '+Store.optionModel.count)
            isAppActive=false;__sigModelReady(true,Store.questionModel,Store.answerModel,Store.optionModel);
        }else if(requestId==4){
            readSurveyInfoModel.source =  value['data'];
            core.info('Survey.qml| __readJsonObject | requestId = '+requestId+' | readSurveyInfo count = '+readSurveyInfoModel.count);
            isAppActive=false;__sigModelReady(true,readSurveyInfoModel);
        }else if(requestId==5){
            Store.surveyByDisplayTypeModel.source =  value['data'];
            core.info('Survey.qml| __readJsonObject | requestId = '+requestId+' | surveyByDisplayTypeModel count = '+Store.surveyByDisplayTypeModel.count);
            isAppActive=false;__sigModelReady(true,Store.surveyByDisplayTypeModel,'','');
        }else if(requestId==6){
            Store.questionModel.source = value['data'];
            Store.answerEssayModel.clear();
            Store.responseEssayModel.clear();

            if(Store.questionModel.count){
                for(var i=0;i<Store.questionModel.count;i++){
                    Store.answerEssayArray[i]=value['data'][i]['answers'];
                    if(value['data'][i]['answers'] && value['data'][i]['answers'][0]['AID']){
                        core.debug('Survey.qml | __formatParams_essay  |  '+i+' | AID :'+value['data'][i]['answers'][0]['AID'])
                        Store.answerEssayModel.append({
                            "AID":value['data'][i]['answers'][0]['AID'],
                            "QID":value['data'][i]['answers'][0]['QID'],
                            "answerOrder":value['data'][i]['answers'][0]['answerOrder'],
                            "answer":value['data'][i]['answers'][0]['answer'],
                            "surveyID":value['data'][i]['answers'][0]['surveyID'],
                            "nextQID":value['data'][i]['answers'][0]['nextQID'],
                            "isoCode":value['data'][i]['answers'][0]['isoCode']
                        });
                    }

                    if(value['data'][i]['response'] && value['data'][i]['response'][0]['RID']){
                        core.debug('Survey.qml | __formatParams_essay  |  '+i+' | RID :'+value['data'][i]['response'][0]['RID'])
                        Store.responseEssayModel.append({
                            "RID":value['data'][i]['response'][0]['RID'],
                            "QID":value['data'][i]['response'][0]['QID'],
                            "AID":value['data'][i]['response'][0]['AID'],
                            "answer":value['data'][i]['response'][0]['answer']
                        });
                    }
                }
            }

            core.info('Survey.qml| __readJsonObject | requestId = '+requestId+' | Question count = '+Store.questionModel.count+' | answerEssayModel count = '+Store.answerEssayModel.count+' | responseEssayModel count = '+Store.responseEssayModel.count+'');
            isAppActive=false;__sigModelReady(true,Store.questionModel,Store.answerEssayModel,Store.responseEssayModel);
        }else if(requestId==7){
            Store.questionModel.source   = value['data'];
            Store.answerEssayModel.clear();
            Store.responseEssayModel.clear();

            core.info('Survey.qml| __readJsonObject | requestId = '+requestId+' | Question count = '+Store.questionModel.count);
            isAppActive=false;__sigModelReady(true,Store.questionModel,'','');
        }else if(requestId==8){
            Store.allSurveysModel.source   = value['data'];
            for(var k in value['data']){
                if(typeof Store.surveyImageInfo[parseInt(value['data'][k]["id"])] == 'undefined'){
                    Store.surveyImageInfo[parseInt(value['data'][k]["id"])] =[];
                }
                Store.surveyImageInfo[parseInt(value['data'][k]["id"])]["imageInfo"] = value['data'][k]["image_info"];
                for(var l in value['data'][k]['questions']){
                    if(typeof Store.surveyImageInfo[parseInt(value['data'][k]["id"])][parseInt(value['data'][k]['questions'][l]['qid'])] == 'undefined'){
                        Store.surveyImageInfo[parseInt(value['data'][k]["id"])][parseInt(value['data'][k]['questions'][l]['qid'])] =[];
                    }
                    Store.surveyImageInfo[parseInt(value['data'][k]["id"])][parseInt(value['data'][k]['questions'][l]['qid'])]['imageInfo']=value['data'][k]['questions'][l]['image_info']
                }
            }


            isAppActive=false;__sigModelReady(true,Store.allSurveysModel,'','');
        }
    }

    function testAnswerEssayArray(index){
        return Store.answerEssayArray[index]
    }

    function __appendSurveyModel(value,i){
        Store.currentSurveyArray[value['data'][i]['surveyID']]=1;
        Store.surveyModel.append({
                                     "surveyID":value['data'][i]['surveyID'],
                                     "surveyTitle":value['data'][i]['surveyTitle'],
                                     "shortDescription":value['data'][i]['shortDescription'],
                                     "longDescription":value['data'][i]['longDescription'],
                                     "surveyStatus":value['data'][i]['surveyStatus'],
                                     "isoCode":value['data'][i]['isoCode'],
                                     "questionCount":value['data'][i]['questionCount'],
                                     "displayType":value['data'][i]['displayType']
                                 });
    }

    function startPreload(){
        core.info("Survey.qml| startPreload | Called ");
        Store.surveyResumeLookup =[];
        if(Store.preloadSurvey==true){
            core.info("Survey.qml| startPreload | preloadSurvey true - Call getAllSurvey()");
            getAllSurvey();
        }

        if(!Store.preloadSurvey){interactiveStartup.eventHandler(Store.cEventId);}
    }

    function __formatParams(qidStr,aidStr,ansStr,subQidStr,questionTypeStr){
        core.info('Survey.qml | __formatParams | Called | qidStr :'+qidStr+' | aidStr :'+aidStr+' | ansStr :'+ansStr+' | questionTypeStr: '+questionTypeStr)
        var retAid   = '';
        var retAns   = '';
        var retRid   = '';
        var retQid   = '';
        var tmpAidArr = '';
        var tmpRidStr = '';
        var ridCount = 0;
        var aidCount = 0;

        //type cast as string
        qidStr=String(qidStr);aidStr=String(aidStr);ansStr=String(ansStr);subQidStr=String(subQidStr);

        /* Response check Create comma seperated rid string */
        if(Store.responseModel.count){for(var i=0;i<Store.responseModel.count;i++){ridCount++;tmpRidStr = __appendStr(tmpRidStr,Store.responseModel.getValue(i,'RID'));}}
        core.info('Survey.qml | __formatParams | RID Str :'+tmpRidStr);
        /* response end */

        if(questionTypeStr == 'freeTextResponse'){return {'qidStr':qidStr,'aidStr':aidStr,'ansStr':ansStr,'ridStr':tmpRidStr}}
        /* ans check */
        tmpAidArr = aidStr.split(',');
        if(aidStr==''){aidCount = 0;
        }else if(tmpAidArr.length>1){aidCount = (tmpAidArr.length)-1
        }else {aidCount = tmpAidArr.length
        }/* ans end   */

        core.info(' Survey.qml | __formatParams | aidCount : '+aidCount+' | ridCount : '+ridCount+' | tmpRidStr : '+retRid);

        /* if aid count is less than rid cound than aids have been deleted
           we nned to pass "" in aid where aid has been deleted such that aid ,answer and rid lenght is same
           -- for all other condition aids will be updated or added by pac api need to pass loged rids
          */
         if(aidCount<ridCount){
             core.info('  Survey.qml | __formatParams | Aids are deleted formating ');
             for(var i=0;i<Store.responseModel.count;i++){
                 var isInAnsArr = false;
                 for(x in tmpAidArr){if(Store.responseModel.getValue(i,'AID')==tmpAidArr[x]){isInAnsArr = true;}}
                 if(!isInAnsArr){
                     //aid is deleted
                     core.info('  Survey.qml | __formatParams | AID Deleted | aid : '+Store.responseModel.getValue(i,'AID')+' | ans :'+Store.responseModel.getValue(i,'answer'));
                     aidStr = __appendStr(aidStr,'""');
                     ansStr = __appendStr(ansStr,'""');
                 }
             }
         }

         retRid = __formatStrInJson(tmpRidStr);
         retAid = __formatStrInJson(aidStr);
         retAns = __formatStrInJson(ansStr,true);

         if(subQidStr && subQidStr!=''){retQid = __formatStrInJson(subQidStr); //likertScale will have subquestions qid json rather than actual qid
         }else {retQid = qidStr}

         return {'qidStr':retQid,'aidStr':retAid,'ansStr':retAns,'ridStr':retRid};
     }


    function __appendStr(str,toAppend){if(str.length){str += ","+toAppend}else{str += toAppend}return str;}

    function __formatStrInJson(str,isQuoteReqd){

        core.info('Survey.qml | __formatStrInJson | str : '+str+' | isQuoteReqd :'+isQuoteReqd+'')
        var returnStr = '';
        if(str==''){ return '';}
        if(!isQuoteReqd){returnStr = "["+str+"]";
        }else {
            var tmp = str.split(',');
            for(x in tmp){
                if(tmp[x]!='""'){returnStr = __appendStr(returnStr,'"'+tmp[x]+'"'); //if quote than dont wrap with quote again
                }else{returnStr = __appendStr(returnStr,'""');}
             }
             returnStr = '['+returnStr+']';
         }
        core.info('Survey.qml | __formatStrInJson | returnStr : '+returnStr+'');
        return returnStr;
    }

    function __formatParams_essay(qidStr,aidStr,ansStr){
        core.info('Survey.qml | __formatParams_essay | Called | qidStr :'+qidStr+' | aidStr :'+aidStr+' | ansStr :'+ansStr)

        var retAid    = '';
        var retAns    = '';
        var retRid    = '';
        var retQid    = '';
        var tmpRidStr = '';
        var tmpAnsArr = '';
        var ridCount  = 0;
        var ansCount  = 0;


        /* Response check Create comma seperated rid string */
        if(Store.questionModel.count){
            for(var i=0;i<Store.questionModel.count;i++){
                if(Store.responseEssayModel.getValue(i,'RID')){
                    core.info('Survey.qml | __formatParams_essay  |  '+i+' | RID :'+Store.responseEssayModel.getValue(i,'RID'));
                    tmpRidStr = __appendStr(tmpRidStr,Store.responseEssayModel.getValue(i,'RID'));
                    ridCount++;
                }
            }
        }

        core.info('Survey.qml | __formatParams_essay | ridCount: '+ridCount+' | RID Str :'+tmpRidStr);
        /* response end */

        if(ridCount){
            /* ans check */
            tmpAnsArr = ansStr.split(',');
            ansCount = (tmpAnsArr.length)-1;
            /* ans end   */

            if(ansCount==0){aidStr = '';}
            core.info(' Survey.qml | __formatParams | Delete detected | ansCount : '+ansCount+' | ridCount : '+ridCount+' | aidStr : '+aidStr);
        }

         retRid = __formatStrInJson(tmpRidStr);
         retAid = __formatStrInJson(aidStr);
         retAns = __formatStrInJson(ansStr,true);
         retQid = __formatStrInJson(qidStr);

         return {'qidStr':retQid,'aidStr':retAid,'ansStr':retAns,'ridStr':retRid}
     }

    function checkIfSurveyTriggered(event_name,surveyId){
        if(Store.triggerSurveyModelArr[event_name]){
            for(var i in Store.triggerSurveyModelArr[event_name]){
                if(Store.triggerSurveyModelArr[event_name][i].surveyId==surveyId && Store.triggerSurveyModelArr[event_name][i]["added"]==true ){
                    return true;
                }
            }
        }
        return false;
    }

    function __process_triggered_survey_event(event_name,time){
        if(Store.triggerSurveyModelArr[event_name]){
            for(var i in Store.triggerSurveyModelArr[event_name]){
                if(Store.triggerSurveyModelArr[event_name][i].value==time){
                    core.info('Survey.qml | __process_triggered_survey_event | Trigger Found: '+event_name+' : '+Store.triggerSurveyModelArr[event_name][i].trigger+' : '+Store.triggerSurveyModelArr[event_name][i].value+' | Survey Id: '+Store.triggerSurveyModelArr[event_name][i].surveyId)
                    __appendTrigeredSurvey(Store.triggerSurveyModelArr[event_name][i].surveyId);
                    sigtTriggerSurveyReady(event_name,Store.triggerSurveyModelArr[event_name][i].surveyId);
                   Store.triggerSurveyModelArr[event_name][i]["added"]=true;
                    // delete  Store.triggerSurveyModelArr[event_name][i];
                }
            }
        }
    }

    function __process_seat_triggered_survey(app_name,trigger){
        if(Store.triggerSurveyModelArr[app_name]){
             core.info('Survey.qml | __process_triggered_survey_event | app_name Found:'+app_name);
            for(var i in Store.triggerSurveyModelArr[app_name]){
                if(Store.triggerSurveyModelArr[app_name][i].trigger==trigger){
                    core.info('Survey.qml | __process_triggered_survey_event | Trigger Found: '+app_name+' : '+Store.triggerSurveyModelArr[app_name][i].trigger+' : '+Store.triggerSurveyModelArr[app_name][i].value+' | Survey Id: '+Store.triggerSurveyModelArr[app_name][i].surveyId)
                    __appendTrigeredSurvey(Store.triggerSurveyModelArr[app_name][i].surveyId);
                    sigtTriggerSurveyReady(app_name,Store.triggerSurveyModelArr[app_name][i].surveyId);
                    Store.triggerSurveyModelArr[event_name][i]["added"]=true;
                    //delete  Store.triggerSurveyModelArr[app_name][i];
                }
            }
        }
    }

    function __appendTrigeredSurvey(surveyId){
        if(!Store.currentSurveyArray[surveyId]){
            for(var i=0;i<Store.surveyModelObj.length;i++){
                if(Store.surveyModelObj[i]['surveyID']==surveyId){
                    Store.currentSurveyArray[surveyId]=1;
                    Store.surveyModel.append({
                        "surveyID":Store.surveyModelObj[i]['surveyID'],
                        "surveyTitle":Store.surveyModelObj[i]['surveyTitle'],
                        "shortDescription":Store.surveyModelObj[i]['shortDescription'],
                        "longDescription":Store.surveyModelObj[i]['longDescription'],
                        "surveyStatus":Store.surveyModelObj[i]['surveyStatus'],
                        "isoCode":Store.surveyModelObj[i]['isoCode'],
                        "questionCount":Store.surveyModelObj[i]['questionCount']
                    });
                    core.info('Survey.qml| __appendTrigeredSurvey | Added surveyId = '+surveyId+' |  surveyModel count: '+Store.surveyModel.count);
                    return true;
                }
            }
        }else{core.info('Survey.qml| __appendTrigeredSurvey | Found surveyId = '+surveyId+' | Not Appending');}
    }

    function __process_triggered_survey_timer(event_name,time){
        for(var i in Store.triggerSurveyModelArr[event_name]){
            if(Store.triggerSurveyModelArr[event_name][i].trigger=="After(mins)"){
                if(Store.triggerSurveyModelArr[event_name][i].value==time){
                    core.info('Survey.qml | __process_triggered_survey_timer | Trigger Found: '+event_name+' : '+Store.triggerSurveyModelArr[event_name][i].trigger+' : '+Store.triggerSurveyModelArr[event_name][i].value+' | Survey Id: '+Store.triggerSurveyModelArr[event_name][i].surveyId)
                    __appendTrigeredSurvey(Store.triggerSurveyModelArr[event_name][i].surveyId);
                    sigtTriggerSurveyReady(event_name,Store.triggerSurveyModelArr[event_name][i].surveyId);
                    Store.triggerSurveyModelArr[event_name][i]["added"]=true;
                    //delete  Store.triggerSurveyModelArr[event_name][i];
                    break;
                }
            }
        }
        __setTimerInterval(event_name);
    }

    function __setTimerInterval(event_name){
        triggerTimer.stop();var timerInterval=0;var event_found=false;var counter=0;
        if(Store.triggerSurveyModelArr[event_name]){
            for(var i in Store.triggerSurveyModelArr[event_name]){
                if(counter==0){timerInterval=Store.triggerSurveyModelArr[event_name][i].value
                }else if(Store.triggerSurveyModelArr[event_name][i].value<timerInterval){timerInterval=Store.triggerSurveyModelArr[event_name][i].value;}
                event_found=true;counter++;
            }
            if(event_found){
                triggerTimer.time=timerInterval;
                timerInterval=timerInterval*60000;
                if(triggerTimer.interval!=0){if(timerInterval!=0){timerInterval=timerInterval-triggerTimer.interval;}else{timerInterval=triggerTimer.interval;}}
                core.info('Survey.qml | __setTimerInterval | triggerTimer interval set to: '+timerInterval);
                triggerTimer.event_name=event_name;triggerTimer.interval=timerInterval;triggerTimer.start();
            }
        }
        if(timerInterval<=0 || !event_found){core.info('Survey.qml | __setTimerInterval | Timer Not Started ');triggerTimer.repeat=false;triggerTimer.stop();}
    }

    function processEntOn(){
       core.info('Survey.qml | processEntOn | triggerSurveyAvailable : '+Store.triggerSurveyAvailable);
        if(Store.triggerSurveyAvailable){__setTimerInterval('ENTON');}
    }

    function resetVariables(){
        isAppActive = false;

        Store.isPrevQuestion = false;
        Store.surveyId       = '';
        Store.qId            = '';

        Store.surveyResumeLookup  =[];

        Store.surveyModel.clear();
        Store.currentSurveyArray=[];
        Store.questionModel.source     = '[]';
        Store.allSurveysModel.source = '[]';
        Store.answerModel.source       = '[]';
        Store.optionModel.source       = '[]';
        Store.responseModel.source     = '[]';
        Store.readSurveyInfoModel      = '[]';
        Store.triggerSurveyModelArr=[];
        Store.surveyImageInfo = [];
        Store.triggerSurveyAvailable=false;
        triggerTimer.interval=0;
        triggerTimer.stop();

        if(Store.isDisplayTypeModelsCreated){
            Store.surveyByDisplayTypeModel.source= '[]';
            Store.answerEssayModel.clear();
            Store.responseEssayModel.clear();
        }
        Store.triggerSurveyObject='';
        Store.previousISO="";
    }

    function __createSurveyByDisplayTypeModels(){
        if(!Store.isDisplayTypeModelsCreated){
            Store.surveyByDisplayTypeModel= jModelComp.createObject(survey);
            Store.answerEssayModel        = sModelComp.createObject(survey);
            Store.responseEssayModel      = sModelComp.createObject(survey);
            Store.isDisplayTypeModelsCreated=true;
        }
        return true;
    }

    function loadDynamicComponents(){
        Store.surveyModel=sModelComp.createObject(survey);
        Store.questionModel=jModelComp.createObject(survey);
        Store.allSurveysModel = jModelComp.createObject(survey);
        Store.answerModel=jModelComp.createObject(survey);
        Store.optionModel=jModelComp.createObject(survey);
        Store.responseModel=jModelComp.createObject(survey);
        Store.blankModel=jModelComp.createObject(survey);
        return true;
    }


    function __setConfiguration(preloadSurvey,eventId,triggeredSurveyAppendInModel){Store.preloadSurvey=preloadSurvey;Store.cEventId=eventId;Store.triggeredSurveyAppendInModel=triggeredSurveyAppendInModel;return true;}
    function __setDataController(dc){survey.dataController=dc;}
    function __resetInFallbackMode(){
         core.info('Survey.qml | __resetInFallbackMode | in fallback mode returning blank model');
        if(Store.storedCallback){__updateCallback(Store.storedCallback);Store.storedCallback='';}
        if(Store.surveyModel)Store.surveyModel.clear();
        isAppActive=false
        interactiveStartup.eventHandler(Store.cEventId);
        __sigModelReady(false,Store.blankModel,'','');
    }

    Component{id:jModelComp;JsonModel{}}
    Component{id:sModelComp;SimpleModel{}}
    Timer { id:triggerTimer;repeat:true;property int time: 0;interval: 0;property string event_name: '';onTriggered:{__process_triggered_survey_timer(event_name,time)}}

    Component.onCompleted: {Store.answerEssayArray={};Store.preloadSurvey=false;Store.isDisplayTypeModelsCreated=false;Store.triggeredSurveyAppendInModel=false;Store.previousISO="";loadDynamicComponents();resetVariables();core.info ("Survey.qml | Component.onCompleted | SURVEY LOAD COMPLETE");}
}
