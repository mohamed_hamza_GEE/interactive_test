import QtQuick 1.1
import Panasonic.Pif 1.0
import "datacontroller"
import "pif"
import "viewcontroller"

Config {
    id: core
    property alias pif: pif;
    property alias viewController: viewController;
    property alias dataController: dataController;
    property alias coreHelper: coreHelper;
    property alias interactiveStartup: interactiveStartup;
    signal sigLanguageChanged (int languageID, string languageISO);

    // ************************************* Public Functions ***************************************
    function error(s) {if(__logLevel >= cERROR) console.log("INT:ERROR: "+Qt.formatDateTime(new Date(),"dd-MMM-yyyy hh:mm:ss:zzz")+" | " + s);}
    function warn(s) {if(__logLevel >= cWARN) console.log("INT:WARNING: "+Qt.formatDateTime(new Date(),"dd-MMM-yyyy hh:mm:ss:zzz")+" | " + s);}
    function info(s) {if(__logLevel >= cINFO) console.log("INT:INFO: "+Qt.formatDateTime(new Date(),"dd-MMM-yyyy hh:mm:ss:zzz")+" | " + s)}
    function debug(s) {if(__logLevel >= cDEBUG) console.log("INT:DEBUG: "+Qt.formatDateTime(new Date(),"dd-MMM-yyyy hh:mm:ss:zzz")+" | " + s);}
    function log(s) {if(__logLevel >= cLOG) console.log("INT:LOG: "+Qt.formatDateTime(new Date(),"dd-MMM-yyyy hh:mm:ss:zzz")+" | " + s);}
    function verbose(s) {if(__logLevel >= cVERBOSE) console.log("INT:VERBOSE: "+Qt.formatDateTime(new Date(),"dd-MMM-yyyy hh:mm:ss:zzz")+" | " + s)}
    function timelog(s) { var d = new Date().getTime(); console.log("INT:TIMELOG: "+(d - __prevTime)+" | "+s); __prevTime = d;}
    function setInteractiveLanguage(id,iso,noTagging,localeLang) {
        iso = iso.toLowerCase();
        info ("Core | setInteractiveLanguage | previous lid: "+settings.languageID+" previous iso: "+settings.languageISO+" new lid: "+id+" new iso: "+iso+" : "+iso+" localeLang: "+localeLang);
        if(pif.android){pif.android.setLocale(localeLang);} //Done since locale is set by FE, so at start up it needs to reset
        if (iso==settings.languageISO) return false;
        settings.languageID = parseInt(id,10); settings.languageISO = iso;
        if (translations && !viewController.getViewToSetTranslations()) setTranslations();
        if (configTool) setConfigTool();
        sigLanguageChanged(settings.languageID,settings.languageISO);
        if((!noTagging)&&(pif.paxus)) pif.paxus.interactiveLanguageLog(settings.languageISO);
        return true;
    }

    function setTranslations() {
        var fpath=settings.contentPath+"/translations/"+settings.languageISO+".qm";
        if (translations.source!=fpath) { info ("Core | Translation File: "+fpath); translations.source = fpath; return true;}
        return false;
    }

    function setConfigTool() {
        if (configTool.languageName!=settings.languageISO) { info ("Core | ConfigTool Language: "+settings.languageISO); configTool.languageName = settings.languageISO; return true;}
        return false;
    }

    // ************************************* Private ***************************************
    Component.onCompleted: {
        interactiveStartup.registerToStage(interactiveStartup.cStageLoadConfiguration,[__loadConfiguration]);
        interactiveStartup.eventHandler(interactiveStartup.cCoreLoaded);
    }

    Pif {id: pif}
    ViewController {id: viewController}
    DataController {id: dataController}
    CoreHelper {id: coreHelper}
    StartupSequencer {id: interactiveStartup}
}
