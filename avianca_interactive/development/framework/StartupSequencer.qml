import QtQuick 1.1
import "blank.js" as Startup
// Purpose: Interactive Startup Sequence

Item {
    // Stage list
    property int cStageLoadComponents:1;
    property int cStageLoadConfiguration:5;
    property int cStageLoadDynamicComponents:10;
    property int cStageLoadNonRouteData:15;
    property int cStageLoadRouteData:20;
    property int cStageLoadRouteData2:21;
    property int cStageBlockData:22;
    property int cStageLoadRouteData3:23;
    property int cStageLoadSurvey:24;
    property int cStageLoadShopping:25;
    property int cStageLoadHospitality:26;
    property int cStagePreloadData:27;
    property int cStageSetViewData:28;
    property int cStageInitializeStates:30;
    property int cStageResetStates:40;
    property int cStageFallbackResetStates:41;
    property int cStageShowScreen:85;
    property int cStagePreCloseFlight:89;
    property int cStageFallbackPreCloseFlight:90;
    property int cStageCloseFlightScreen:91;
    property int cStageForceVideoAudioScreen:93;
    property int cStageEntOffScreen:95;
    property int cStageFallbackScreen:98;
    property int cStageEntOnScreen:99;
    // Event list
    property int cEventList:0;
    property int cCallbackList:1;
    property int cStageExit:2;
    property int cEntOn:3;
    property int cEntOff:4;
    property int cOpenFlight:5;
    property int cCloseFlight:6;
    property int cForceVideoAudioOn:7;
    property int cForceVideoAudioOff:8;
    property int cDataControllerLoaded:9;
    property int cViewControllerLoaded:10;
    property int cPifLoaded:11;
    property int cCoreLoaded:12;
    property int cFallbackOn:13;
    property int cOnEntOn:21;
    property int cOnEntOff:22;
    property int cOnOpenFlight:23;
    property int cOnCloseFlight:24;
    property int cOnForceVideoAudioOn:25;
    property int cOnForceVideoAudioOff:26;
    // Runtime
    property string __stageEventList:'';
    property int __stageID:0;
    property int cMinCustomEventNo:51;
    // Signals
    signal sigStageload(int stageid);

    Connections {
        target:pif;
        onEntOnChanged: {if(pif.getFallbackMode() && core.pc){core.info("StartupSequencer.qml | ignoring EntOnChanged in Falback mode");}else eventHandler(entOn==1?cOnEntOn:cOnEntOff);}
        onOpenFlightChanged: {if(pif.getFallbackMode()&& core.pc){core.info("StartupSequencer.qml | ignoring OpenFlightChanged in Falback mode");}else eventHandler(openFlight==1?cOnOpenFlight:cOnCloseFlight);}
        onInteractiveOverrideChanged: {eventHandler(interactiveOverride==1?cOnForceVideoAudioOn:cOnForceVideoAudioOff);}
        onSigFallbackMode: {core.info("onSigFallbackMode: "+fallbackMode)}
    }

    function executeFallback(){
        if(sTimer.newStage<=cStageLoadNonRouteData || sTimerDuplicate.newStage<=cStageLoadNonRouteData) return;
        if(__stageID!=0) __loadStage(cStageFallbackResetStates);
        else if(__stageID==0){
            if(sTimer.running || sTimerDuplicate.running){
                sTimer.newStage=cStageFallbackResetStates;
                sTimerDuplicate.newStage=cStageFallbackResetStates;
            }
        }
    }

    function registerToStage (stageid, callbackArr, eventArr) {
        core.debug("core | registerToStage called: "+stageid);
        var e; var s=Startup.StageArr;
        if (__stageID>cStageLoadNonRouteData||stageid==cStageLoadComponents||!s[stageid]) return false;             // can register on any non-route stage or can't register to cStageLoadComponents or if stage doesnt exist
        var arr = s[stageid][cCallbackList];
        for (var i=0; i<callbackArr.length;i++) arr.push(callbackArr[i]);
        arr = s[stageid][cEventList].split(',');
        for (var i in eventArr) {
            e=String(eventArr[i]).split('|');       // custom eventid|goto stageid
            s[stageid][e[0]]=e[1];
            arr.push(e[0]);
        }
        s[stageid][cEventList]=arr.join(',');
        if(stageid==cStageResetStates ) registerToFallbackStages(cStageFallbackResetStates, callbackArr, eventArr);
        //if(stageid==cStagePreCloseFlight) registerToFallbackStages(cStageFallbackPreCloseFlight, callbackArr, eventArr);
        return true;
    }

    function registerToFallbackStages(stageid, callbackArr, eventArr){
        var e; var s=Startup.StageArr;
        var arr = s[stageid][cCallbackList];
        for (var i=0; i<callbackArr.length;i++) arr.push(callbackArr[i]);
        arr = s[stageid][cEventList].split(',');
        for (var i in eventArr) {
            e=String(eventArr[i]).split('|');       // custom eventid|goto stageid
            s[stageid][e[0]]=e[1];
            arr.push(e[0]);
        }
        s[stageid][cEventList]=arr.join(',');
        return true;
    }

    function eventHandler(eventid) {
        core.info("core | eventHandler: "+eventid);
        core.info("core | __stageID: "+__stageID);
        if (__stageID==0) return false;
        var arr=__stageEventList.split(',');
        var eindex=arr.indexOf(String(eventid));
        if (eindex==-1) return false;
        arr.splice(eindex,1); __stageEventList=arr.join(',');
        var s=Startup.StageArr;
        if (s[__stageID][eventid]) __loadStage(s[__stageID][eventid]);
        else if (!__checkCustomEventsExist()) return __processEventList(__stageID);
        return true;
    }

    function __checkCustomEventsExist () {
        var arr=__stageEventList.split(',');
        for (var i=0;i<arr.length;i++) if (arr[i]>=cMinCustomEventNo) return true;
        return false;
    }

    function __stageExecution (stageid) {
        core.info("core | Executing Stage: "+stageid);
        var s=Startup.StageArr;
        __stageEventList=s[stageid][cEventList];
        var c=s[stageid][cCallbackList];
        for (var i=0; i<c.length;i++) sigStageload.connect(c[i]);
        __stageID = stageid;
        sigStageload(__stageID);
        if (!__checkCustomEventsExist()) return __processEventList(__stageID);
    }

    function __processEventList (stageid) {
        if (!stageid) return false;
        var eventArr=Startup.StageArr[stageid];
        if (eventArr[cFallbackOn] && pif.getFallbackMode()==1) {__loadStage(eventArr[cFallbackOn]);return true;}
        if (eventArr[cCloseFlight] && pif.getOpenFlight()==0) {__loadStage(eventArr[cCloseFlight]); return true;}
        if (eventArr[cOpenFlight] && pif.getOpenFlight()==1) {__loadStage(eventArr[cOpenFlight]); return true;}
        if (eventArr[cForceVideoAudioOn] && pif.getInteractiveOverride()==1) {__loadStage(eventArr[cForceVideoAudioOn]); return true;}
        if (eventArr[cForceVideoAudioOff] && pif.getInteractiveOverride()!=1) {__loadStage(eventArr[cForceVideoAudioOff]); return true;}
        if (eventArr[cEntOff] && pif.getEntOn()==0) {__loadStage(eventArr[cEntOff]); return true;}        
        if (eventArr[cEntOn] && pif.getEntOn()==1) {__loadStage(eventArr[cEntOn]); return true;}
        if (eventArr[cStageExit]) {__loadStage(eventArr[cStageExit]); return true;}
        return false;
    }

    function __loadStage (stageid) {
        core.debug("core | Preparing to load Stage: "+stageid);
        if(__stageID==0) return false;
        var c=Startup.StageArr[__stageID][cCallbackList];
        for (var i=0; i<c.length;i++) sigStageload.disconnect(c[i]);
        __stageID=0;
        sTimer.newStage=stageid; sTimerDuplicate.newStage=stageid;
        if (sTimer.running) sTimerDuplicate.restart(); else sTimer.restart();
        return true;
    }
    function __terminateCurrentStageExecution(){sTimer.newStage=0; sTimer.stop(); sTimerDuplicate.newStage=0; sTimerDuplicate.stop();}
    function __nullifyStageList(stageid){var s=Startup.StageArr[stageid]; for(var i in s){if(i!=cEventList && i!=cCallbackList) s[i]='';}}
    function __setDefaultInteractiveLanguage(){setInteractiveLanguage(settings.defaultLID,settings.defaultISO,true);}
    function __hideLoadingScreen () {if (!viewController.getViewToHideLoadingScreen()) viewController.__hideLoadingScreen();}
    function __initialiseSequence () {
        // [<stageid>][0]=<eventlist>
        // [<stageid>][1]=[<callbacklist>]
        // [<stageid>][<eventid>]=<stageid>         // eventid defined inside or (custom events) outside the file only. Events are executed after all callback and all custom events are executed.
        // <eventlist>="eventid,..."
        // <callbacklist>=Array of callback functions for a stage.
        Startup.StageArr=[]; var s=Startup.StageArr;
        s[cStageLoadComponents]=[[cDataControllerLoaded,cViewControllerLoaded,cPifLoaded,cCoreLoaded].join(','),[],cStageLoadConfiguration];
        s[cStageLoadConfiguration]=['',[],cStageLoadDynamicComponents];
        s[cStageLoadDynamicComponents]=['',[viewController.__seatInfoScreen],cStageLoadNonRouteData];
        s[cStageLoadNonRouteData]=['',[],,,,cStageLoadRouteData,cStagePreCloseFlight,,,,,,,cStageLoadRouteData];
        s[cStageLoadRouteData]=['',[viewController.__showLoadingScreen],cStageLoadRouteData2];
        s[cStageLoadRouteData2]=['',[],cStageBlockData];
        s[cStageBlockData]=['',[],cStageLoadRouteData3];
        s[cStageLoadRouteData3]=['',[],cStageLoadSurvey];
        s[cStageLoadSurvey]=['',[],cStageLoadShopping];
        s[cStageLoadShopping]=['',[],cStageLoadHospitality];
        s[cStageLoadHospitality]=['',[],cStagePreloadData];
        s[cStagePreloadData]=['',[],cStageSetViewData];
        s[cStageSetViewData]=['',[],cStageInitializeStates];
        s[cStageInitializeStates]=['',[viewController.__showLoadingScreen,viewController.__hideForceAVScreen],cStageShowScreen];
        s[cStageResetStates]=['',[],cStageShowScreen,,cStageShowScreen,,cStagePreCloseFlight,cStageShowScreen,,,,,,,,cStageShowScreen];
        s[cStageFallbackResetStates]=['',[],cStagePreloadData];
//        s[cStageFallbackResetStates]=['',[],cStageLoadNonRouteData];
        //s[cStageFallbackPreCloseFlight]=['',[],cStageLoadNonRouteData,,,,,,,,,,,cStageShowScreen];
        //s[cStageFallbackPreCloseFlight]=['',[],cStageLoadNonRouteData];
        s[cStageShowScreen]=['',[],,cStageEntOnScreen,cStageEntOffScreen,,cStageResetStates,cStageForceVideoAudioScreen,,,,,,cStageFallbackScreen];
        s[cStageCloseFlightScreen]=['',[viewController.__hideLoadingScreen,viewController.__hideForceAVScreen],,,,cStageLoadRouteData];
        s[cStagePreCloseFlight]=['',[],cStageCloseFlightScreen];
        s[cStageForceVideoAudioScreen]=['',[__hideLoadingScreen],,,,,cStageResetStates];
        s[cStageEntOffScreen]=['',[__hideLoadingScreen],,cStageResetStates,,,cStageResetStates,cStageResetStates];
        s[cStageEntOnScreen]=['',[__hideLoadingScreen],,,cStageResetStates,,cStageResetStates,cStageResetStates];
        s[cStageFallbackScreen]=['',[__hideLoadingScreen],,,,,,cStageResetStates];
        __stageExecution(cStageLoadComponents);
        registerToStage(cStageCloseFlightScreen,[__setDefaultInteractiveLanguage],[cOnOpenFlight+"|"+cStageLoadRouteData]);
        registerToStage(cStageForceVideoAudioScreen,[],[cOnForceVideoAudioOff+"|"+cStageInitializeStates,cOnCloseFlight+"|"+cStagePreCloseFlight]);
        registerToStage(cStageEntOffScreen,[],[cOnEntOn+"|"+cStageInitializeStates,cOnCloseFlight+"|"+cStageResetStates,cOnForceVideoAudioOn+"|"+cStageResetStates]);
        registerToStage(cStageEntOnScreen,[],[cOnEntOff+"|"+cStageResetStates,cOnCloseFlight+"|"+cStageResetStates,cOnForceVideoAudioOn+"|"+cStageResetStates]);
        registerToStage(cStageFallbackScreen,[],[cOnEntOff+"|"+cStageResetStates,cOnCloseFlight+"|"+cStageResetStates,cOnForceVideoAudioOn+"|"+cStageResetStates]);
        registerToStage(cStageLoadRouteData,[pif.setBacklightOn]);
    }

    Component.onCompleted: __initialiseSequence();
    Timer { id:sTimer; interval: 500; property int newStage:0; onTriggered: __stageExecution(newStage);}
    Timer { id:sTimerDuplicate; interval: 500; property int newStage:0; onTriggered: __stageExecution(newStage);}      // to workaround restarting timer within onTriggered.
}
