@echo off
copy ..\revisions\rev.txt ..\development\
copy ..\revisions\releasehistory.xls ..\development\

backupcomponent tools
if errorlevel == 1 goto end

del ..\Workflow-Output\*.zip
@call revstart.bat
del revstart.bat

:end
